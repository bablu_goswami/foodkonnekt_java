//====================customer login=================
function customerLogin() {
    var customerEmailID = $('#email').val();
    var emailRegex = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
   
    if (customerEmailID == "") {
        $("#email").focus();
        $("#loginError").html("<div class='col-md-12 error-msg' id='loginError'>Please enter email id</div>");
        return false;
    } else if ($("#passowrd").val() == "") {
        $("#password").focus();
        $("#loginError").html("<div class='col-md-12 error-msg' id='loginError'>Please enter password</div>");
        return false;
    } else if (!emailRegex.test(customerEmailID)) {
        $("#email").focus();
        $("#loginError").html("<div class='col-md-12 error-msg' id='loginError'>Please enter valid email id</div>");
        return false;
    } else {
    	 $("body").loadingimagethingy("enable");
    	$.ajax({
            //=================check customer type=====================
            url: "checkCustomerType?emailId=" + customerEmailID +
                "&merchantId=" + merchantId,
            type: "GET",
            contentType: "application/json; charset=utf-8",
            success: function(customerResponse) {

                if (customerResponse == 'php') {
                    window.location.href = "resetPhpCustomerPassword?email=" +
                    customerEmailID + "&merchantId=" + merchantId;
                } else if (customerResponse == 'java') {
                    $(document).ready(
                        function() {
                            var emailId = $('#email').val();
                            var password = $('#password').val();
                            //orderType = $('input[name=order]:checked').val();
                            $.ajax({
                                //===========check credentials enter by customer===============  
                                url: "customerSignIn",
                                type: 'POST',
                                dataType: 'json',
                                data: "{\"emailId\":\"" + emailId + "\",\"vendorId\":\"" + merchantId +
                                    "\",\"password\":\"" + password + "\"}",
                                contentType: 'application/json',
                                mimeType: 'application/json',
                                success: function(data) {
                                    console.log(data);
                                    if (data.success == "Login successfully") {
                                        $("#loginError").html("");
                                        $("body").loadingimagethingy("disable");
                                        window.location.href="userProfileV2"
                                    } else {
                                        $("#loginError").html("<div class='col-md-12 error-msg' id='loginError'>Invalid login credential</div>");
                                        $("body").loadingimagethingy("disable");
                                    }
                                },
                            });
                        });
                } else {
                    $("#email").focus();
                    $("body").loadingimagethingy("disable");
                    $("#loginError").html("<div class='col-md-12 error-msg' id='loginError'>Not registered email id</div>");
                }
            }

        });
    }
}