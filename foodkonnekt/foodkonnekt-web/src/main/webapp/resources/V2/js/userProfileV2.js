//update customer details
function updateCustomerDetails() {
	var emailRegex = /^[A-Za-z0-9._]*\@[A-Za-z]*\.[A-Za-z]{2,5}$/;
	var phoneRegex = /\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/;

	var firstName = $("#first-name").val();
	var lastName = $("#second-name").val();
	var oldPassword = $("#oldPassword").val();
	var email = $("#eMail").val();
	var mobile = $("#mobile").val();
	var password = $("#newPassword").val();

	if ($("#first-name").val() == "") {
		$("#first-name").focus();
		$("#errorBox").html("<div class='col-md-12 error-msg' id='errorBox'>Please enter first name</div>");
		return false;
	} else if (/^[a-zA-Z0-9- ]*$/.test(firstName) == false) {
		$("#first-name").focus();
		$("#errorBox").html("<div class='col-md-12 error-msg' id='errorBox'>Your name contains illegal characters</div>");
		return false;
	}   else if (/^[a-zA-Z0-9- ]*$/.test(lastName) == false) {
		$("#second-name").focus();
		$("#errorBox").html("<div class='col-md-12 error-msg' id='errorBox'>Your last name contains illegal characters</div>");
		return false;
	} else if ($("#eMail").val() == "") {
		$("#eMail").focus();
		$("#errorBox").html("<div class='col-md-12 error-msg' id='errorBox'>Please enter email id</div>");
		return false;
	}  else if ($('#mobile').val() == "") {
		$("#mobile").focus();
		$("#errorBox").html("<div class='col-md-12 error-msg' id='errorBox'>Please enter phone number</div>");
		return false;
	} else if (mobile.length > 12) {
		$("#mobile").focus();
		$("#errorBox").html("<div class='col-md-12 error-msg' id='errorBox'>Please enter valid phone number</div>");
		return false;
	}else if ($(firstName != '' && lastName != '' && email != '' && mobile != ''  && email != '')) {
	    var oldPass = $("#oldPassword").val();
	    var newPass = $("#newPassword").val();
	    var newPass1 = $("#newPassword1").val();
	    if(oldPass != '' && oldPass!=undefined && ((newPass == '' || newPass == undefined) || (newPass1 == '' || newPass1 == undefined))){
	    	if((newPass == '' || newPass == undefined) && (newPass1 == '' || newPass1 == undefined)){
	    		$("#passwordErrorMsg").html("<div class='col-md-12 error-msg' id='passwordErrorMsg'>Please enter new password and confirm password</div>");
	    	}
	    	else if(newPass == '' ){
	    		$("#passwordErrorMsg").html("<div class='col-md-12 error-msg' id='passwordErrorMsg'>Please enter new password</div>");
	    	}
	    	else if(newPass1 == ''){
	    		$("#passwordErrorMsg").html("<div class='col-md-12 error-msg' id='passwordErrorMsg'>Please enter confirm password</div>");
	    	}
	    }
	    else if (oldPass != '' && oldPass != undefined  && newPass != '' && newPass!=undefined && newPass1 != '' && newPass1 !=undefined) {
	      if (newPass == newPass1) {
	              $.ajax({
	                url : "checkOldPassword",
	                type : 'POST',
	                dataType : 'json',
	                data : "{\"password\":\"" + oldPass + "\"}",
	                contentType : 'application/json',
	                mimeType : 'application/json',
	                success : function(data) {
	                  if (data.success == "samePassword") {
	                    $("#newPassword").val(newPass);
	                    $("#errorBox").html("<div class='col-md-12 error-msg' id='errorBox'>Profie updated successfully</div>");
	                    document.myformPage.action = "updateCustomerProfileV2", document.myformPage.submit()
	                  } else {
	                	  $("#passwordErrorMsg").html("<div class='col-md-12 error-msg' id='passwordErrorMsg'>Your old password is wrong</div>");
	                	  return false;
	                  }
	                },
	                error : function(data, status, er) {
	                }
	              });
	      		}else{
	      			 $("#passwordErrorMsg").html("<div class='col-md-12 error-msg' id='passwordErrorMsg'>Your new password and confirm password does not match</div>");
	      		}
	    }else if((oldPass == '' || oldPass == undefined)  && newPass != '' && newPass!=undefined && newPass1 != '' && newPass1 !=undefined){
	    	 $("#passwordErrorMsg").html("<div class='col-md-12 error-msg' id='passwordErrorMsg'>Please enter old password</div>");
	    	 
	    }else if((oldPass == '' || oldPass == undefined)  && (newPass == '' || newPass!=undefined) && newPass1 != '' && newPass1 !=undefined){
	    	 $("#passwordErrorMsg").html("<div class='col-md-12 error-msg' id='passwordErrorMsg'>Please enter old password and new password</div>");
	    }else if((oldPass == '' || oldPass == undefined)  && newPass != '' && newPass!=undefined && (newPass1 == '' || newPass1 ==undefined)){
	    	 $("#passwordErrorMsg").html("<div class='col-md-12 error-msg' id='passwordErrorMsg'>Please enter old password and confirm password</div>");
	    }
	    	else{
	    
			var firstAddress = "";
			var cityChk = "";
			var statChk = "";
			var zipChk = "";
			var aptChk = "";
			
	    	
			$("#address").each(function() {
	    		aptChk=aptChk+","+$(this).val();
	    	});
			
    	    firstAddress=firstAddress+",";
			
	    	$("#city").each(function() {
	    		cityChk=cityChk+","+$(this).val();
	    	});
	    	$("#state").each(function() {
	    		statChk=statChk+","+$(this).val();
	    	});
	    	$("#zip").each(function() {
	    		zipChk=zipChk+","+$(this).val();
	    	});
	    	
	    	$.ajax({
	            url : "checkAddressValidity?aptNumber="+aptChk+"&firstAddress="+firstAddress
	                + "&city=" + cityChk+"&state="+statChk+"&zip="+zipChk,
	            type : "GET",
	            contentType : "application/json; charset=utf-8",
				    success : function(statusValue) {
					//alert(statusValue)
					if (statusValue.statusM == 1) {
						 $("#errorBox").html("<div class='col-md-12 error-msg' id='errorBox'>Your address is not valid</div>");
					} else {
						 $("#errorBox").html("<div class='col-md-12 error-msg' id='errorBox'>Profie updated successfully</div>");
						document.myformPage.action = "updateCustomerProfileV2", document.myformPage.submit()
					}
				},
				error : function() {
				}
			})
		}
	}
}

/*--------------------------------------------------------------------------------------------*/
//get value of address array from database
$(document).ready( function (){
	  $('#previousAddress').change(function(){
		 var address=$(this).val();
		 var addressArray=address.split("#");
		 var address1=addressArray[0];
		 var address2=addressArray[1];
		 var city=addressArray[2];
		 var state=addressArray[3];
		 var zip=addressArray[4];
		 var addressId=addressArray[5];
		 var addressPosId=addressArray[6];
		 
		 // $("#apt-no").val(address1);
		
		 $("#address").val(address1);
		 $("#city").val(city);
		 $("#state").val(state);
		 $("#zip").val(zip);
		 $("#addressId").val(addressId);
		 $("#addressPosId").val(addressPosId);
	  });
	  
	        if(allowMultiPay==""){
	   		 
	 		     	}else{
	 		     	    
	 		           if(allowMultiPay){
	 		        	   if(cardInfo===''){
	 		     		}else{
	 		     			
	 		     			var cardInfos=[] ;
	 		     			cardInfos= JSON.parse(cardInfo);
	 		     			
	 	  		     		if(cardInfos!=""){
	 	  		     		var cardInfosLength = cardInfos.length;
	 	  		     		for(var i=0;i
	 	  		     		<cardInfosLength;i++){
	 	  		     		$('<div class="col-md-3 col-xs-3 card">'+ cardInfos[i].cardType+'</div><div class="col-md-3 col-xs-3 card">'+ cardInfos[i].last4+ '</div><div class="col-md-3 col-xs-3 card">Active</div><div class="col-md-3 col-xs-3 card"><a data-toggle="modal" href="#myModal-card" onclick="cardPopUp('+cardInfos[i].id+')"><button type="button" class="btn btn-active">delete</button></a>').appendTo('#cardBody');
	 	  		     		$("#cardIndoDiv").css("display","block");
	 	  		   			}
	 	  		   			}	
	 		     		}
	 		        	   
	 		        	  if('${sessionScope.expiryCardsInfos}'===''){
	 	 		     		}else{
	 	 		     			var expiryCardsInfos=[] ;
	 	 		     			expiryCardsInfos= JSON.parse(expiryCardsInfo);
	 	 		     			
	 	 	  		     		if(expiryCardsInfos!=""){
	 	 	  		     		var cardInfosLength = expiryCardsInfos.length;
	 	 	  		     		for(var i=0;i<cardInfosLength;i++){

	 	 	  		     	$('<div class="col-md-3 col-xs-3 card">'+ expiryCardsInfos[i].cardType+'</div><div class="col-md-3 col-xs-3 card">'+ expiryCardsInfos[i].last4+ '</div><div class="col-md-3 col-xs-3 card">Active</div><div class="col-md-3 col-xs-3 card"><a data-toggle="modal" href="#myModal-card" onclick="cardPopUp('+cardInfos[i].id+')"><button type="button" class="btn btn-active">delete</button></a>').appendTo('#cardBody');
	 	 	  		     	$("#cardIndoDiv").css("display","block");
	 	 	  		   			}
	 	 	  		   			}	
	 	 	  		     		
	 	 		     		
	 	 		     		}
	 		           }
	 		     		}
	});

function cardPopUp(id){
	//  $('#myModal-card').css('display',"block");
	  cardId = id;
}
function deleteRecords() {
  	
	  $.ajax({
          url : "deleteCardInfo?cardId=" + cardId,
          type : "GET",
          contentType : "application/json; charset=utf-8",
          success : function(result) {
          	if(result.status== true){
          		window.location='userProfileV2';
          	}else{
          		 $('#futureOrderDateCombo').html('<option value="select">Select</option>'); 
          	}
          
             
          },
          error : function() {
            console.log("Error inside future date Ajax call");
          }
       })
	
}
/*--------------------------------------------------------------------------------------------*/
//to show changePassword div
function changePassword(){
	$("#changePassword").css("display","block");
}

function removeFundcode(){
	
	 var fundCode = document.getElementById("fundCodes");
	 var code = fundCode.value;
	 var customerId = $("#customerId").val();
	$.ajax({
        url : "removeFundCode?fundCode=" + code+"&customerId="+customerId,
        type : "GET",
        contentType : "application/json; charset=utf-8",
        success : function(result) {
        	if(result == "success"){
        		fundCode.remove(fundCode.selectedIndex);
        	}
           
        },
        error : function() {
          console.log("Error inside removeFundcode Ajax call");
        }
     })
	
}
