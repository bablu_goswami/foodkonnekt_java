//forgot password call
function forgotPassword(){
	   
	   var emailRegex = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
       var email = $("#emailForgot").val();

       if ($("#emailForgot").val() == "") {
         $("#emailForgot").focus();
         $("#errorForgotPassword").html("enter the email");
         return false;
       } else if (!emailRegex.test(email)) {
         $("#emailForgot").focus();
         $("#errorForgotPassword").html("Enter a valid email id");
         return false;
       }
       else{
    	   $("body").loadingimagethingy("enable");
           $("#errorForgotPassword").html("");
              $.ajax({
                   url : "forgotPasswordActionV2",
                   type : 'POST',
                   dataType : 'json',
                   data : "{\"emailId\":\"" + email + "\"}",
                   contentType : 'application/json',
                   mimeType : 'application/json',
                   success : function(statusValue) {
                	   $("body").loadingimagethingy("disable");
                       $("#errorForgotPassword").html(statusValue.message);
                   },
                  error : function() {
                      $("#errorForgotPassword").html("Some thing went wrong please try again");
                  }
            }) 
        }
}
/*--------------------------------------------------------------------------------*/
//validation for new password
function checkPassword() {
	var password = $("#password").val();
	var confrimPassword = $("#confirmPassword").val();
	if ($("#password").val() == "") {
	    $("#password").focus();
	    $("#loginError").html("enter the Password");
	    return false;
	  }else if ($("#password").val() != "" && password.length < 8) {
		$("#password").focus();
		$("#loginError").html("Password should be a minimum of 8 characters");
	    return false;
	  }
	if ($("#confirmPassword").val() == "") {
	    $("#confirmPassword").focus();
	    $("#loginError").html("enter the confirm password");
	    return false;
	}else if (password!=confrimPassword) {
			$("#loginError").html("Password and confirm password should be same");
		    return false;
	}
}