$('#cartIcon').attr("class", "activeli");
var orderType;
var deliveryFee = "0";
var deliveryTaxPrice = 0;
var avgDeliveryTime="45";
var deliveryPosId;
var deliveryTaxStatus;
var tipAmt = "0";
var tip = "0";
var cardInfosLength1 = 0;
var newCardValue = 0;
var allowMultiPayCheck = 1;
var guestStatus = false;
var returnData;
var voucherCode = "";
var inventoryLevel = "";
var discountType = "";
var couponDiscount = "";
var itemsForDiscount = null;
var listOfALLDiscounts = null;
var itemPoSidsJson = "";
var kouponCount = 0;
var isDeliveryKoupon = false;
var deliveryDiscountAmount = 0;
// var taxNameAndItemPrice = new Map();
var finalTotal=0;
var totalTax = 0;
//var merchantTaxNameFromDB=${merchantTaxs};
var isCouponApply = false;
var minimumDeliveryAmount = 0;
var loyaltyProgramVal = 0;
var addressId;
var zoneId;
var subtotalAfterRemoveToCart='';
var flag=false;
var jwt='';
var tipCheck=false;
var c_name;
var fundCodeBox=""; 
var code=""; 
var codeFlag=false; 
var fundCodeValidStatus=null; 
/* -------------------------------Onload functions---------------------------------------------*/


$( document ).ready(function() {
    if(paymentmode!=null)
    	{
    	$("#paymentModesId1").prop("checked", true);
    	
    	var paymentType = $('input[name=card]:checked').val();
    	
    	if(paymentmode.search('Credit Card')!=-1 && paymentType=='Credit Card' )
    		flag=true;
    	}
    		 
    		 
    		 	if (cust != '') {
    		 		
    		 		//$("#payment").css('display','block');
    		 		$("#couponBox").show();
		$(fundCodeBox).show();
    		 		$("#guest").css('display','none');
    		 		if(paymentType == 'Cash'){
    		 			
    		 			$("#paymentBox").css('display','none');
    		 			
    		 		}else{
    		 			
    		 			if($('#paymentModesId1').val() == 'Credit Card')
    		 				$("#paymentBox").css('display', 'block');
    		 			
    		 		}
    	        }
});


/*---------------------------------------------------------------------------------------------------*/
//remove item from cart
function removeItem(itemUid) {
    var removeUrl = "removeToCart?itemUid=" + itemUid;
    $("body").loadingimagethingy("enable");
    if (posId == '2') {
        var label;
        if ($("#pickUp").is(":checked")) {
            label = "Foodkonnekt Online Pickup";
        } else {
            label = "Foodkonnekt Online Delivery";
        }
        removeUrl = "removeToCart?itemUid=" + itemUid + "&orderType=" + label;
    }
    
    //$("#paymentModesId1").prop("checked", false);
    $('input[name=card]').attr('checked',false);
    $("#paymentBox").css('display','none');
    $.ajax({
        url: removeUrl,
        type: 'Get',
        dataType: 'json',
        contentType: 'application/json',
        mimeType: 'application/json',
        success: function(data) {
            console.log(data);
            $("body").loadingimagethingy("disable");
            $("#" + itemUid).remove();
            document.getElementById('total').innerHTML = (parseFloat(data.totalPrice) + parseFloat(deliveryFee) + parseFloat(deliveryTaxPrice)).toFixed(2) ;

            document.getElementById('subTotal').innerHTML = ((parseFloat(data.subTotal)).toFixed(2));
            
            subtotalAfterRemoveToCart = ((parseFloat(data.subTotal)).toFixed(2));
            document.getElementById('cartCount').innerHTML = parseInt($("#cartCount").text()) - 1;
            orderSubTotal=subtotalAfterRemoveToCart;
            document.getElementById('tax').innerHTML = ((parseFloat(data.Tax)) + parseFloat(deliveryTaxPrice)).toFixed(2);
            
            if(data.ConvenienceFee != null && data.ConvenienceFee != ''){
            	 document.getElementById('ConvenienceFee').innerHTML = ((parseFloat(data.ConvenienceFee)).toFixed(2));
            }
           
            
            $("#tipAmt").val('');
            tipAmt = "0";
            tip="0";
            
            var discountedValue = 0;
            document.getElementById('discount').innerHTML = discountedValue.toFixed(2);
            if (data.auxTax > 0 && data.auxTax != '' && data.auxTax != undefined && data.auxTax != null) {
                document.getElementById('auxTaxRow').innerHTML = "<td><strong>Aux Tax:</strong></td><td></td><td><strong>$<span id='auxTax'>" + parseFloat(data.auxTax).toFixed(2) + "</span></strong></td>";
            } else {
                document.getElementById('auxTaxRow').innerHTML = "";
            }
            
        }
    });
}
/*----------------------------------------------------------------------------*/
//to hide and unhide address,state,city,zip
function guestSignUp() {
	$('input[name=card]').attr('checked',false);
	$("#errorBox12").html("");
	$("#errorBox").html("");
    //show address,city,state,zip when delivery is checked
    if (document.getElementById('delivery').checked) {
        $('#state').css('display', 'block');
        $('#address').css('display', 'block');
        $('#address1').css('display', 'block');
        $('#city').css('display', 'block');
        $('#zipcode').css('display', 'block');
    }

    //hide address,city,state,zip when pickup is checked
    if (document.getElementById('pickUp').checked) {
        value = document.getElementById('pickUp').value;
        $('#state').css('display', 'none');
        $('#address').css('display', 'none');
        $('#address1').css('display', 'none');
        $('#city').css('display', 'none');
        $('#zipcode').css('display', 'none');
    }
}


/*---------------------------------------------------------------------------------------------------*/
//====================customer login=================
function customerLogin() {

    var custEmailID = $('#signInEmail').val();
    var emailRegex = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    if (custEmailID == "") {
        $("#signInEmail").focus();
        $("#loginError").html("<div class='col-md-12 error-msg' id='loginError'>Enter the email id</div>");
        return false;
    } else if ($("#signInPassword").val() == "") {
        $("#signInPassword").focus();
        $("#loginError").html("<div class='col-md-12 error-msg' id='loginError'>Enter the password</div>");
        return false;
    } else if (!emailRegex.test(custEmailID)) {
        $("#signInEmail").focus();
        $("#loginError").html("<div class='col-md-12 error-msg' id='loginError'>Enter a valid email id</div>");
        return false;
    } else {
        $.ajax({

            //=================check customer type=====================
            url: "checkCustomerType?emailId=" + custEmailID +
                "&merchantId=" + merchantId,
            type: "GET",
            contentType: "application/json; charset=utf-8",
            success: function(customerResponse) {

                if (customerResponse == 'php') {
                    window.location.href = "resetPhpCustomerPassword?email=" +
                        custEmailID + "&merchantId=" + merchantId;
                } else if (customerResponse == 'java') {
                    $(document).ready(
                        function() {
                            var emailId = $('#signInEmail').val();
                            var password = $('#signInPassword').val();

                            $("body").loadingimagethingy("enable");

                            orderType = $('input[name=order]:checked').val();
                            $.ajax({
                                //===========check credentials enter by customer===============  
                                url: "customerSignIn",
                                type: 'POST',
                                dataType: 'json',
                                data: "{\"emailId\":\"" + emailId + "\",\"vendorId\":\"" + merchantId +
                                    "\",\"password\":\"" + password + "\",\"orderType\":\"" + orderType +  "\"}",
                                contentType: 'application/json',
                                mimeType: 'application/json',
                                success: function(data) {
                                    console.log("============================="+data);
                                    document.getElementById("active4").style.color = "black";
                                    $("body").loadingimagethingy("disable");

                                    if (data.success == "Login successfully") {
                                    	showFundBox(data.custId);
                                    	if(virtualFundCount>0){
                                    	$('#fundBox').show();
                                    	}
                                    	c_name = data.customerName
                                        $("#paymentBox").css('display', 'none');
                                        if (data.cardInfo != "" && data.cardInfo != undefined) {
                                            toCheckMultiPay(data.cardInfo);
                                        }
                                        $('#submit').prop('disabled', false);
                                        $("#loginError").html("");
                                        var isDeliveryOrder = false;
                                        if ($('#delivery').is(':checked')) {
                                            isDeliveryOrder = true;
                                        }
                                        if (isDeliveryOrder == true) {
                                            addresses = data.address;
                                            $("#guestCustomerDiv").css('display', 'none');
                                            $("#deliverySelectDiv").css('display', 'block');
                                            $("#pupose").css('display', 'block');
                                            $("#custSection").css('display', 'none');

                                            for (var i = 0, l = addresses.length; i < l; i++) {
                                                if (addresses[i].address2 == null) {
                                                    addresses[i].address2 = '';
                                                }
                                                $('<option value="' + addresses[i].id + '#' + addresses[i].address1 + '#' + addresses[i].address2 + '#' + addresses[i].city + '#' + addresses[i].state + '#' + addresses[i].zip + '">' + addresses[i].address1 + " " + addresses[i].address2 + '</option>').appendTo('#returningDeliveryZones');

                                            }
                                        } else {
                                            $("#guest").css('display', 'none');
                                            $("#payment").css('display', 'block');
                                            $("#couponBox").show();
                                            $(fundCodeBox).show();
                                            codeFlag = true;
					    $('#active4').addClass('active');
                                           if(flag==true)
                                            $("#paymentBox").css('display', 'block');
                                            $("#couponBox").show();
                                            guestStatus = true;
                                            // $("#paymentBox").css('display', 'block');
                                        }
                                    } else {
                                        $("#loginError").html("<div class='col-md-12 error-msg' id='loginError'>Invalid login credential</div>");
                                    }
                                },
                            });
                        });
                } else {
                    $("#signInEmail").focus();
                    $("#loginError").html("<div class='col-md-12 error-msg' id='loginError'>Not Registered Email</div>");
                }
            }

        });
    }
}
/*---------------------------------------------------------------------------------------------------*/
//to register a new customer

function registerCustomer() {
    var emailRegex = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    var phoneRegex = /\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/;
    var email = $("#guest-email").val();
    var name = $("#guest-name").val();
    var phone = $("#guest-phone").val();
    var password = $("#password").val();
    var lastname = $("#guest-lastname").val();
    orderType = $('input[name=order]:checked').val();

    var address = $("#guest-address").val();
    var address1 = $("#guest-address1").val();
    var city = $("#guest-city").val();
    var state = $("#guest-state").val();
    var zip = $("#guest-zipcode").val();
    c_name  =name;
    var customerUrl;
    var enableCheckBox = 0;
    var isDeliveryKoupon = "";

    if (password == "") {
        guestSignUpFlag = 1;
    }
    $('#submit').prop('disabled', false);

    if ($("#guest-name").val() == "") {
        $("#guest-name").focus();
        $("#errorBox").html("<div class='col-md-12 error-msg' id='errorBox'>Please enter Customer Name</div>");
        return false;
    } else if (/^[a-zA-Z0-9- ]*$/.test(name) == false) {
        $("#guest-name").focus();
        $("#errorBox").html("<div class='col-md-12 error-msg' id='errorBox'>Your Name Contains illegal Characters</div>");
        return false;
    } else if (name.length < 2) {
        $("#guest-name").focus();
        $("#errorBox").html("<div class='col-md-12 error-msg' id='errorBox'>Name should be a minimum of 2 characters</div>");
        return false;
    }else if (name.length > 20) {
        $("#guest-name").focus();
        $("#errorBox").html("<div class='col-md-12 error-msg' id='errorBox'>Name should be a maximum of 20 characters</div>");
        return false;
    }else if ($("#guest-lastname").val() == "") {
        $("#guest-lastname").focus();
        $("#errorBox").html("<div class='col-md-12 error-msg' id='errorBox'>Please enter customer Last Name</div>");
        return false;
    } else if (/^[a-zA-Z0-9- ]*$/.test(lastname) == false) {
        $("#guest-lastname").focus();
        $("#errorBox").html("<div class='col-md-12 error-msg' id='errorBox'>Your Last Name Contains illegal Characters</div>");
        return false;
    } else if (lastname.length < 2) {
        $("#guest-lastname").focus();
        $("#errorBox").html("<div class='col-md-12 error-msg' id='errorBox'>LastName should be a minimum of 2 characters</div>");
        return false;
    
    } else if (lastname.length > 20) {
        $("#guest-name").focus();
        $("#errorBox").html("<div class='col-md-12 error-msg' id='errorBox'>Last Name should be a maximum of 20 characters</div>");
        return false;
    }else if ($("#guest-email").val() == "") {
        $("#guest-email").focus();
        $("#errorBox").html("<div class='col-md-12 error-msg' id='errorBox'>Please enter email id</div>");
        return false;
    } else if (!emailRegex.test(email)) {
        $("#guest-email").focus();
        $("#errorBox").html("<div class='col-md-12 error-msg' id='errorBox'>Please enter valid email id</div>");
        return false;
    } else if ($("#password").val() != "" && password.length < 8) {
        $("#password").focus();
        $("#errorBox").html("<div class='col-md-12 error-msg' id='errorBox'>Password should be a minimum of 8 characters</div>");
        return false;
    } else if ($('#guest-phone').val() == "") {
        $("#guest-phone").focus();
        $("#errorBox").html("<div class='col-md-12 error-msg' id='errorBox'>Please enter phone number</div>");
        return false;
    } else if (phone.length > 12 || phone.length < 12) {
        $("#guest-phone").focus();
        $("#errorBox").html("<div class='col-md-12 error-msg' id='errorBox'>Please enter valid phone number</div>");
        return false;
    } else if ((orderType == "Delivery" || orderType == "delivery") && ($("#guest-address").val() == '' || $("#guest-address").val() == '')) {
        $("#guest-address").focus();
        $("#errorBox").html("<div class='col-md-12 error-msg' id='errorBox'>Please enter address</div>");
        return false;
    } else if ((orderType == "Delivery" || orderType == "delivery") && (city == "" || city == '')) {
        $("#guest-city").focus();
        $("#errorBox").html("<div class='col-md-12 error-msg' id='errorBox'>Please enter city</div>");
        return false;
    } else if ((orderType == "Delivery" || orderType == "delivery") && $('#guest-state').val() == "State") {
        $("#guest-state").focus();
        $("#errorBox").html("<div class='col-md-12 error-msg' id='errorBox'>Please select state name</div>");
        return false;
    } else if ((orderType == "Delivery" || orderType == "delivery") && (zip == "" || zip == '')) {
        $("#guest-zipcode").focus();
        $("#errorBox").html("<div class='col-md-12 error-msg' id='errorBox'>Please enter zip code</div>");
        return false;
    } else if (orderType == "Delivery" && zip.length != 5) {
        $("#guest-zipcode").focus();
        $("#errorBox").html("<div class='col-md-12 error-msg' id='errorBox'>Please enter valid zip code</div>");
        return false;
    } else if ($(name != '' && email != '' && phone != '' && lastname != '')) {
        $("#errorBox").html("");

        $("body").loadingimagethingy("enable");
        $(document).ready(function() {
            $.ajax({
                url: "checkDuplicateEmailId",
                type: 'POST',
                dataType: 'json',
                data: "{\"emailId\":\"" + email +"\"}",
                contentType: 'application/json',
                mimeType: 'application/json',
                success: function(data) {
                    var msg = data.message;
                    if (msg == "true") {
                        $("#errorBox").html("<div class='col-md-12 error-msg' id='errorBox'>Email id already exists. Please login</div>");
                        $("body").loadingimagethingy("disable");
                        return false;
                    } else {
                    	/*if(merchantId==356){
                    		$.ajax({
                                url: "http://client.texnrewards.net/gateway/contactmanager_keyword.asp",
                                type: 'POST',
                                dataType: 'json',
                                data: "{\"user_guid\":\"CC65DC5C-2C33-45E4-AFCF-11A290D4D736\",\"keyword\":\"romas\"," +
                                		"\"shortcode\":\"55678\",\"mobile\":\"" + phone + "\",\"firstname\":\"" + name +
                                "\",\"lastname\":\"lastname\",\"email\":\"" + email  +"\"}",
                                contentType: 'application/json',
                                mimeType: 'application/json',
                                success: function(data) {
                                	alert(data)
                                }
                        	});
                    	}*/
                    
                        if (orderType == "delivery") {
                            console.log("1st method");
                            $("body").loadingimagethingy("enable");
                            //==========check delivery zone type is googleMap or radius==================
                            $.ajax({
                                url: "findDeliveryZoneType?merchantId=" + merchantId,
                                type: "GET",
                                contentType: "application/json; charset=utf-8",
                                success: function(result) {
                                    $("body").loadingimagethingy("disable");

                                    console.log(result);
                                    console.log(result.deliveryZoneType);
                                    if (result.deliveryZoneType == "googleMap") {

                                        //=======================ajax to fetch delivery zone======================
                                        $("body").loadingimagethingy("enable");
                                        getDeliverZoneForGoogleMap(address, city, state, zip);
                                    } else {
                                        //======================ajax to check delivery is under zone or not========

                                        $("body").loadingimagethingy("enable");

                                        //checkAddressIsInDeliveryZone(addData);
                                        $.ajax({
                                            url: "checkDeliveryZone",
                                            type: 'POST',
                                            dataType: 'json',
                                            data: "{\"address1\":\"" + address + "\",\"address2\":\"" + address1 +
                                                "\",\"city\":\"" + city + "\",\"state\":\"" + state + "\",\"zip\":\"" + zip +
                                                "\",\"isDeliveryKoupon\":\"" + isDeliveryKoupon  +"\"}",

                                            contentType: 'application/json',
                                            mimeType: 'application/json',
                                            success: function(data) {
                                            	
                                            	
                                            	console.log("###########################"+JSON.stringify(data))
                                            	
                                                $("body").loadingimagethingy("disable");

                                                minimumDeliveryAmount = data.minimumDeliveryAmount;
                                              
                                                
                                                minDeliveryAmountTotal = minimumDeliveryAmount
                                                
                                                document.getElementById('test').innerHTML = "$"+minDeliveryAmountTotal;

                                                var subTotalBeforDeliveryFees = $("#subtotal").val();
                                                
                                                if (parseFloat(subTotalBeforDeliveryFees) < parseFloat(minimumDeliveryAmount)) {
                                                    $("#deliveryAmtPopup").modal('show');
                                                    $("#deliveryzoneStatus").removeAttr('checked');
                                                }else{
                                                
                                                if (data.message == "Your zone is in delivery zone") {
                                                    $("#guest").css('display', 'none');
                                                    $("#payment").css('display', 'block');
						    $('#active4').addClass('active');
                                                    $("#couponBox").show();
                                                    if(guestCustomerPassword != ''){
                                                        $(fundCodeBox).show();
                                                        codeFlag = true;
                                                        }
						    if(flag==true)
                                                    $("#paymentBox").css('display', 'block');
						    
                                                    
						    zoneId = data.zoneId;
                                                    
                                                    if (data.itemPrice > 0) {

                                                        document.getElementById('deliveryFeeRow').innerHTML = "<td><strong>Delivery Fee:</strong></td><td></td><td><strong>$<span id='deliveryFee' deliveryPosId='" + data.deliveryItemPosId + "'>" + data.itemPrice + "</span></strong></td>";

                                                        deliveryFee = data.itemPrice;
                                                        avgDeliveryTime=data.avgDeliveryTime;
                                                        deliveryPosId = data.deliveryItemPosId;
                                                        deliveryTaxPrice = data.deliveryTaxPrice;
                                                        
                                                        document.getElementById('deliveryFee').innerHTML = (data.itemPrice);
                                                        document.getElementById('tax').innerHTML = (parseFloat($("#tax").text()) + data.deliveryTaxPrice).toFixed(2);
                                                        document.getElementById('total').innerHTML = (parseFloat($("#total").text()) + data.itemPrice + deliveryTaxPrice).toFixed(2);
                                                        $("body").loadingimagethingy("enable");
                                                    }
                                                    //=================ajax to save new customer in db======================

                                                    var apiData = "{\"address1\":\"" + address + "\",\"address2\":\"" + address1 +
                                                        "\",\"city\":\"" + city + "\",\"state\":\"" + state + "\",\"zip\":\"" + zip + "\",\"emailId\":\"" + email + "\",\"firstName\":\"" + name + "\",\"lastName\":\"" + lastname + "\",\"phoneNumber\":\"" + phone + "\",\"password\":\"" + password + "\",\"freekwentBox\":\"" + enableCheckBox + "\"}";
                                                    callRegisterApi(apiData);
                                                } else {
                                                    if (($("#pickUp").is(":checked"))) {} else {
                                                        $("#myModal122").modal('show');
                                                    }
                                                }
                                            }
                                            }
                                        });
                                    }
                                },
                            });
                        } else {
                            //==============if order type== pickup====================
                            var apiData = "{\"address1\":\"" + address + "\",\"address2\":\"" + address1 +
                                "\",\"city\":\"" + city + "\",\"state\":\"" + state + "\",\"zip\":\"" + zip + "\",\"emailId\":\"" + email + "\",\"firstName\":\"" + name + "\",\"lastName\":\"" + lastname + "\",\"phoneNumber\":\"" + phone + "\",\"password\":\"" + password + "\",\"freekwentBox\":\"" + enableCheckBox + "\",\"loyalityProgram\":\"" + loyaltyProgramVal  +"\"}";
                            callRegisterApi(apiData);
                        }
                    }
                }
            });
        });
    }
}
var paymentMethod='';
var futureDate='';
var futureTime='';
var futreOrderType='';
var cardNumber = '';
var month = '';
var year = '';
var cvv = '';
var cardType = '';
var Card_zipCode = '';
var password = '';
var tax = '';
var total = '';
var discountedConvenienceFee = '';
var payeezytoken;
var payeezyCavvtoken;
var instruction;
function placeOrder() {
	   $('#placeOrderBtn').prop('disabled', true);
code = $("#fundCode").val(); 
	if(code != ""){
		   if(code.length > 12 ) {
			$("#fundBox").focus();
			$("#fundError")
					.html(
							"<div class='col-md-12 error-msg' id='errorBox'>Please enter valid Fundraiser Code</div>");
			document.getElementById('fundCode').value = '';
			return false;
		}else if(fundCodeValidStatus=='Invalid' || fundCodeValidStatus=='isMatching'){
			console.log("placeOrder false");
			$("#fundError")
			.html(
					"<div class='col-md-12 error-msg' id='errorBox'>Please enter valid Fundraiser Code</div>");
			return false;
		}else{
			console.log("placeOrder true");
		   $("#fundError").html("");
		}
	}
	 
	    $("#placeOrderBtn").on("click", function(e){
		e.preventDefault();
		$('html, body').animate({scrollTop: 0}, 'slow');
		});
    document.getElementById('test').innerHTML = "$"+minDeliveryAmountTotal;

    
    $("#orderMsgSpanId").html("");
    var subTotalBeforDeliveryFees = $("#subtotal").val();
    if (parseFloat(subTotalBeforDeliveryFees) < parseFloat(minimumDeliveryAmount)) {
    	$("input:radio").attr("checked", false);
$("#paymentModesId1").prop("checked", false);
        $("#paymentBox").css('display', 'none');
    }else{
		if (isDeliveryKoupon) {
	    	var finalDeliveryFee = deliveryFee;
	    	if(deliveryFee==0){
	    		finalDeliveryFee = deliveryDiscountAmount;
	    	}
	        listOfALLDiscounts = '[{"discount":' + finalDeliveryFee + ',"discountType":"amount","couponUID":"' + voucherCode + '","inventoryLevel":"item","isUsabilitySingle":false,"voucherCode":"' + voucherCode + '"}]';
	        itemsForDiscount = '[{"qunatity":1,"totalQunatity":1,"discount":' + finalDeliveryFee + ',"discountName":" ' + voucherCode + '","price":0.0,"originalPrice":' + finalDeliveryFee + ',"itemModifierPrice":0.0,"itemPosId":"' + deliveryPosId + '"}]';
	    }
	    //$('#placeOrderBtn').off('click');
	    var cartCount3 = parseInt($("#cartCount").text());
	    
	    if (cartCount3 == null || cartCount3 == 0 || cartCount3 == 'undefined' || orderItemsLength.length == 0) {
	        $('#checkItem').modal('show')
	    
	    }  else {
	        orderType = $('input[name=order]:checked').val();
	         cardNumber = $("#cardNumber").val();
	         month = $("#month").val();
	         year = $('#year').val();
	         cvv = $('#cvv').val();
	         cardType = $('#cardType').val();
	         Card_zipCode = $('#Card_zipCode').val();

	        var data1;
	        var paymentJSON;
	        var orderJson;
	         paymentMethod = $('input[name=card]:checked').val();
	         instruction = $("#special-instructions").val();
        //instruction = instruction.replace(/\n|\r/g, " ");
        instruction = instruction.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g," ");
        instruction = instruction.replace(/[^a-zA-Z0-9]/g, " ");
	         password = $("#password").val();
	         tax = $("#tax").text();
	         total = $("#total").text();
         payeezytoken ;
         payeezyCavvtoken ;
	        if (tip != 0 && tip != undefined && tip != null) {
	            total = (parseFloat(total).toFixed(2) - parseFloat(tip).toFixed(2)).toFixed(2);
	        }
	        var futreOrderType = "";
	         futureDate = "select";
	         futureTime = "";

	        if ($('#orderlater').is(':checked')) {
	            futreOrderType = "on";
	            futureDate = $("#futureOrderDateCombo").val();
	            futureTime = $('#futureOpeingTime').val();
	        }


        if (paymentMethod != undefined && paymentMethod != "") {
			
			
            if (paymentMethod == "Credit Card") {
            	if(getPaymentModeValue(paymentMethod)){
					return false;
				}

                if (cardType == "") {
                    $("#card").focus();
                    $("#agreeMsg").html("<div class='col-md-12 error-msg' id='agreeMsg'>Please select card type</div>");
                    $('#placeOrderBtn').prop('disabled', false);
                    return false;
                } else if (cardType == "Select Card") {
                    $("#card").focus();
                    $("#agreeMsg").html("<div class='col-md-12 error-msg' id='agreeMsg'>Please select card type</div>");
                    $('#placeOrderBtn').prop('disabled', false);
                    return false
                } else if (cardNumber == "") {
                    $("#cardNumber").focus();
                    $("#agreeMsg").html("<div class='col-md-12 error-msg' id='agreeMsg'>Please enter card number</div>");
                    $('#placeOrderBtn').prop('disabled', false);
                    return false;
                } else if (cardType == "American Express" && cardNumber.length != 15) {
                    $("#cardNumber").val("")
                    $("#agreeMsg").html("<div class='col-md-12 error-msg' id='agreeMsg'>Card number should be 15 characters</div>");
                    $('#placeOrderBtn').prop('disabled', false);
                    return false
                } else if ((cardType == "Master Card" || cardType == "Visa") && (cardNumber.length != 16)) {
                    $("#cardNumber").val("")
                    $("#agreeMsg").html("<div class='col-md-12 error-msg' id='agreeMsg'>Card number should be 16 characters</div>");
                    $('#placeOrderBtn').prop('disabled', false);
                    return false
                } else if (cardNumber.indexOf('.') > -1) {
                    $("#cardNumber").val("")
                    $("#agreeMsg").html("<div class='col-md-12 error-msg' id='agreeMsg'>Please enter valid card number</div>");
                    $('#placeOrderBtn').prop('disabled', false);
                    return false
                } else if (month == "Month") {
                    $("#month").focus();
                    $("#agreeMsg").html("<div class='col-md-12 error-msg' id='agreeMsg'>Please select card expiry month</div>");
                    $('#placeOrderBtn').prop('disabled', false);
                    return false;
                } else if (year == "Year") {
                    $("#year").focus();
                    $("#agreeMsg").html("<div class='col-md-12 error-msg' id='agreeMsg'>Please select card expiry year</div>");
                    $('#placeOrderBtn').prop('disabled', false);
                    return false;
                } else if (cvv == "") {
                    $("#cvv").focus();
                    $("#agreeMsg").html("Please enter cc code");
                    $("#agreeMsg").html("<div class='col-md-12 error-msg' id='agreeMsg'>Please enter cc code</div>");
                    $('#placeOrderBtn').prop('disabled', false);
                    return false;
                } else if (/[0-9]/.test(cvv) == false) {
                    $("#cvv").focus();
                    $("#agreeMsg").html("<div class='col-md-12 error-msg' id='agreeMsg'>Please enter valid cvv code</div>");
                    $('#placeOrderBtn').prop('disabled', false);
                    return false;
                } else if (cardType == "American Express" && cvv.length != 4) {
                    $("#cvv").val("");
                    $("#agreeMsg").html("<div class='col-md-12 error-msg' id='agreeMsg'>CVV should be atleast 4 characters</div>");
                    $('#placeOrderBtn').prop('disabled', false);
                    return false
                } else if ((cardType == "Master Card" || cardType == "Visa") && (cvv.length != 3)) {
                    $("#cvv").val("");
                    $("#agreeMsg").html("<div class='col-md-12 error-msg' id='agreeMsg'>CVV should be atleast 3 characters</div>");
                    $('#placeOrderBtn').prop('disabled', false);
                    return false
                } else if (cvv.indexOf('.') > -1) {
                    $("#cardNumber").val("")
                    $("#agreeMsg").html("<div class='col-md-12 error-msg' id='agreeMsg'>Please enter valid cc code</div>");
                    $('#placeOrderBtn').prop('disabled', false);
                    return false
                }
                if(!(parseFloat(orderSubTotal).toFixed(2)>parseFloat(0).toFixed(2)))
	        	{
	        	$("#agreeMsg").html("<div class='col-md-12 error-msg' id='agreeMsg'>Order Amount should be greaterThan 0</div>");
                $('#placeOrderBtn').prop('disabled', false);
                return false;
	        	}
            	if(Payeezy3DsModes == 'true'){
            		 $.ajax({
             	        url: webBaseUrl + "/getPaymentGatewayDetails/"+ merchantId,
             	        type: 'GET',
             	        contentType: 'application/json',
             	        success: function(response) {
             	        	console.log(response)
             	        	var parseJson = JSON.parse(response)
             	    	 data1 = '{"payeezyMerchnatToken" : ' + "\""+parseJson.payeezyMerchnatToken + "\"" + ',"token" : ' + "\"" + "9035310800298291" + "\"" + ',"expMonth" : ' +"\""+ month +"\""+ ', "expYear" : "' + year + '","ccType" : "' + cardType + '", "ccNumber" : "' + cardNumber + '", "ccCode" : "' + cvv + '","payeezyMerchnatTokenJs" : "'+ parseJson.payeezyMerchnatTokenJs +'","merchantId" : "'+merchantId+'","payeezy3DTaToken" : "'+parseJson.payeezy3DTaToken+'"}';
             	        	$.ajax({
                     	        url: adminBaseUrl + "/genrateJwtTokenfor3Dseure",
                     	        type: 'POST',
                     	        data: data1,
                     	        contentType: 'application/json',
                     	        success: function(data) {
                     	             console.log(data);
                     	       
                     	             if(data!=''&& data!=null &&data.stuatus =="Success")  {
                     	          
                     	         		payeezytoken =data.token;
                     	         		
                     	         		Cardinal.configure({
                     	                 logging: {
                     	                     level: "on"
                     	                 }
                     	             });
                     	         		if(jwt!=''){
											 startTransaction();
										 }else{
										 jwt = data.jwt;
										
										 Cardinal
											.setup(
													"init",
													{
														jwt : jwt
													});
										Cardinal
												.on(
														'payments.setupComplete',
														function(
																setupCompleteData) {
															console
																	.log("setupCompleteData")
															console
																	.log(setupCompleteData)
															startTransaction();
														})
										 }
                     	         	
              
                     	         	}
                     	         	else{
                     	         		  $("#orderMsgSpanId").html("<div class='col-md-12 error-msg' id='orderMsgSpanId'>"+data.stuatus+"</div>");
     	                                  $("#minCCDiv").css('display', 'none');
     	                                  $("#cvv").val(null);
     	                                  $("body").loadingimagethingy("disable");
     	                                 $('#placeOrderBtn').prop('disabled', false);
                     	         	} 
                     	             var amt;
                     	             function startTransaction() {
                     	              //	 var amt  = Math.round(total) + Math.round(tip) 
                     	                  amt =	 (parseFloat(total) + parseFloat(tip))
                     	            	    amt =  Math.round(amt * 100);
                     	                 Cardinal.start("cca", {
                     	                     OrderDetails: {
                     	                         OrderNumber: Math.random(0, 1000000) + "-shzs", // OrderNumbers need to be unique
                     	                         Amount: amt,
                     	                         CurrencyCode: " 840"
                     	                     },
                     	                     Consumer: {
                     	                         Account: {
                     	                             AccountNumber: cardNumber,
                     	                             ExpirationMonth: month,
                     	                             ExpirationYear:  year,
                     	                             CardCode: cvv,    // Required for Tokenization
                     	                             NameOnAccount:"sumit"
                     	                           }
                     	                     }
                     	                 });
                     	             }
                     	             function payeezyLogger(res) {
                     	            	 console.log("res=="+res)
                     	            	 var data = JSON.stringify(res); // this is a string
                     	            	 console.log("data=="+data)
                                          console.log("http://localhost:8080/foodkonnekt-admin/logs?appName=payeezy3DS&event=customerName_"+c_name+"__merchantId--"+merchantId+"_ActionCode"+res.ActionCode+"&log="+data);

                     	            	 $.ajax({
                     	                     url: "https://www.foodkonnekt.com/admin/logs?appName=payeezy3DS&event=customerName_"+c_name+"__merchantId--"+merchantId+"_ActionCode"+res.ActionCode+"_Amount"+amt+"&log="+data,
                     	                     type: 'GET',
                     	                     contentType: "application/json",
                     	                     success: function(statusValue) {
                     	                    	 console.log(statusValue)
                     	                     }
                     	            	 });
                     	             }	
                     	        }, 
                     	    });
             	       	}
            		 });

            	}else{
            		 if (allowMultiPay != false && cardInfosLength1 > 0 && newCardValue == 0) {

                         var vaultedInfo = $("#carrdSize123").val();
                         allowMultiPayCheck = 0;
                         paymentJSON = '{"vaultedInfo":"' + vaultedInfo + '","ccNumber": "' + cardNumber + '","ccCode":"' + cvv + '","expMonth": "' + '' + '","expYear": "' + '' + '","ccType":"' + cardType + '","paymentMethod":"' + paymentMethod + '","tip":"' + tip + '","saveCard":' + allowMultiPayCheck + ',"futureDate":"' + futureDate + '","futureTime":"' + futureTime + '","futureOrderType":"' + status + '","tax":"' + tax +  '"}';

                         orderJson = '[{"itemid":"' + deliveryPosId + '","price":"' + deliveryFee + '","amount":"1","itemPosId":"' + deliveryPosId + '","modifier":[]}]';
                         
                         if(addressId != undefined && zoneId != undefined){
                         	data1 = '{"itemsForDiscount" : ' + itemsForDiscount + ',"listOfALLDiscounts" : ' + listOfALLDiscounts + ', "discount" : "' + couponDiscount + '", "discountType" : "' + discountType + '","itemPosIds" : "' + itemPoSidsJson + '", "inventoryLevel" : "' + inventoryLevel + '", "voucherCode" : "' + voucherCode + '","orderType" : "' + orderType + '","instruction" : "' + instruction + '","paymentVO" : ' + paymentJSON + ',"deliveryItemPrice" : ' + deliveryFee + ',"orderTotal":"' + total + '","orderJson":' + orderJson + ',"isDeliveryCoupon":' + isDeliveryKoupon + ',"addressId":' + addressId + ',"zoneId":' + zoneId +'}';
                         }else{
                         	data1 = '{"itemsForDiscount" : ' + itemsForDiscount + ',"listOfALLDiscounts" : ' + listOfALLDiscounts + ', "discount" : "' + couponDiscount + '", "discountType" : "' + discountType + '","itemPosIds" : "' + itemPoSidsJson + '", "inventoryLevel" : "' + inventoryLevel + '", "voucherCode" : "' + voucherCode + '","orderType" : "' + orderType + '","instruction" : "' + instruction + '","paymentVO" : ' + paymentJSON + ',"deliveryItemPrice" : ' + deliveryFee + ',"orderTotal":"' + total + '","orderJson":' + orderJson + ',"isDeliveryCoupon":' + isDeliveryKoupon + '}';
                         }

                     } else {

                         if (cardType == "") {
                             $("#card").focus();
                             $("#agreeMsg").html("<div class='col-md-12 error-msg' id='agreeMsg'>Please select card type</div>");
                             $('#placeOrderBtn').prop('disabled', false);
                             return false;
                         } else if (cardType == "Select Card") {
                             $("#card").focus();
                             $("#agreeMsg").html("<div class='col-md-12 error-msg' id='agreeMsg'>Please select card type</div>");
                             $('#placeOrderBtn').prop('disabled', false);
                             return false
                         } else if (cardNumber == "") {
                             $("#cardNumber").focus();
                             $("#agreeMsg").html("<div class='col-md-12 error-msg' id='agreeMsg'>Please enter card number</div>");
                             $('#placeOrderBtn').prop('disabled', false);
                             return false;
                         } else if (cardType == "American Express" && cardNumber.length != 15) {
                             $("#cardNumber").val("")
                             $("#agreeMsg").html("<div class='col-md-12 error-msg' id='agreeMsg'>Card number should be 15 characters</div>");
                             $('#placeOrderBtn').prop('disabled', false);
                             return false
                         } else if ((cardType == "Master Card" || cardType == "Visa") && (cardNumber.length != 16)) {
                             $("#cardNumber").val("")
                             $("#agreeMsg").html("<div class='col-md-12 error-msg' id='agreeMsg'>Card number should be 16 characters</div>");
                             $('#placeOrderBtn').prop('disabled', false);
                             return false
                         } else if (cardNumber.indexOf('.') > -1) {
                             $("#cardNumber").val("")
                             $("#agreeMsg").html("<div class='col-md-12 error-msg' id='agreeMsg'>Please enter valid card number</div>");
                             $('#placeOrderBtn').prop('disabled', false);
                             return false
                         } else if (month == "Month") {
                             $("#month").focus();
                             $("#agreeMsg").html("<div class='col-md-12 error-msg' id='agreeMsg'>Please select card expiry month</div>");
                             $('#placeOrderBtn').prop('disabled', false);
                             return false;
                         } else if (year == "Year") {
                             $("#year").focus();
                             $("#agreeMsg").html("<div class='col-md-12 error-msg' id='agreeMsg'>Please select card expiry year</div>");
                             $('#placeOrderBtn').prop('disabled', false);
                             return false;
                         } else if (cvv == "") {
                             $("#cvv").focus();
                             $("#agreeMsg").html("Please enter cc code");
                             $("#agreeMsg").html("<div class='col-md-12 error-msg' id='agreeMsg'>Please enter cc code</div>");
                             $('#placeOrderBtn').prop('disabled', false);
                             return false;
                         } else if (/[0-9]/.test(cvv) == false) {
                             $("#cvv").focus();
                             $("#agreeMsg").html("<div class='col-md-12 error-msg' id='agreeMsg'>Please enter valid cvv code</div>");
                             $('#placeOrderBtn').prop('disabled', false);
                             return false;
                         } else if (cardType == "American Express" && cvv.length != 4) {
                             $("#cvv").val("");
                             $("#agreeMsg").html("<div class='col-md-12 error-msg' id='agreeMsg'>CC code should be 4 characters</div>");
                             $('#placeOrderBtn').prop('disabled', false);
                             return false
                         } else if ((cardType == "Master Card" || cardType == "Visa") && (cvv.length != 3)) {
                             $("#cvv").val("");
                             $("#agreeMsg").html("<div class='col-md-12 error-msg' id='agreeMsg'>CC code should be 3 characters</div>");
                             $('#placeOrderBtn').prop('disabled', false);
                             return false
                         } else if (cvv.indexOf('.') > -1) {
                             $("#cardNumber").val("")
                             $("#agreeMsg").html("<div class='col-md-12 error-msg' id='agreeMsg'>Please enter valid cc code</div>");
                             $('#placeOrderBtn').prop('disabled', false);
                             return false
                         }
                         if (posId != 1) {
                             if ($("Card_zipCode").val() == "") {
                                 $("#Card_zipCode").focus();
                                 $("#agreeMsg").html("<div class='col-md-12 error-msg' id='agreeMsg'>Please enter zip code</div>");
                                 $('#placeOrderBtn').prop('disabled', false);
                                 return false;
                             } else if (Card_zipCode.length != 5) {
                                 $("#Card_zipCode").focus();
                                 $("#agreeMsg").html("<div class='col-md-12 error-msg' id='agreeMsg'>Please enter valid zip code</div>");
                                 $('#placeOrderBtn').prop('disabled', false);
                                 return false;
                             }
                         }

                             orderJson = '[{"itemid":"' + deliveryPosId + '","price":"' + deliveryFee + '","amount":"1","itemPosId":"' + deliveryPosId + '","modifier":[]}]';
                         if (posId == 1) {
                             paymentJSON = '{"ccNumber": "' + cardNumber + '","ccCode":"' + cvv + '","expMonth": "' + month + '","expYear": "' + year + '","ccType":"' + cardType + '","paymentMethod":"' + paymentMethod + '","tip":"' + tip + '","saveCard":' + allowMultiPayCheck + ',"futureDate":"' + futureDate + '","futureTime":"' + futureTime + '","futureOrderType":"' + futreOrderType + '","tax":"' + tax +  '"}';

                         } else {
                             paymentJSON = '{"ccNumber": "' + cardNumber + '","ccCode":"' + cvv + '","expMonth": "' + month + '","expYear": "' + year + '","ccType":"' + cardType + '","paymentMethod":"' + paymentMethod + '","tip":"' + tip + '","saveCard":' + allowMultiPayCheck + ',"futureDate":"' + futureDate + '","futureTime":"' + futureTime + '","futureOrderType":"' + futreOrderType + '","zipCode":"' + Card_zipCode + '","tax":"' + tax +  '"}';

                         }
                        
                         if(addressId != undefined && zoneId != undefined){
                         	data1 = '{"itemsForDiscount" : ' + itemsForDiscount + ',"listOfALLDiscounts" : ' + listOfALLDiscounts + ', "discount" : "' + couponDiscount + '", "discountType" : "' + discountType + '","itemPosIds" : "' + itemPoSidsJson + '", "inventoryLevel" : "' + inventoryLevel + '", "voucherCode" : "' + voucherCode + '","orderType" : "' + orderType + '","instruction" : "' + instruction + '","paymentVO" : ' + paymentJSON + ',"deliveryItemPrice" : ' + deliveryFee + ',"orderTotal":"' + total + '","orderJson":' + orderJson + ',"isDeliveryCoupon":' + isDeliveryKoupon + ',"addressId":' + addressId + ',"zoneId":' + zoneId +'}';
                         }else{
                         	data1 = '{"itemsForDiscount" : ' + itemsForDiscount + ',"listOfALLDiscounts" : ' + listOfALLDiscounts + ', "discount" : "' + couponDiscount + '", "discountType" : "' + discountType + '","itemPosIds" : "' + itemPoSidsJson + '", "inventoryLevel" : "' + inventoryLevel + '", "voucherCode" : "' + voucherCode + '","orderType" : "' + orderType + '","instruction" : "' + instruction + '","paymentVO" : ' + paymentJSON + ',"deliveryItemPrice" : ' + deliveryFee + ',"orderTotal":"' + total + '","orderJson":' + orderJson + ',"isDeliveryCoupon":' + isDeliveryKoupon + '}';
                         }
                     }
                 $("#agreeMsg").html("");
                 $("#carrdSize").css('display', 'none');
                 }
                 $("#agreeMsg").html("");
                 $("#orderMsgSpanId").html("");
                 
                 console.log(data1);
                 $("body").loadingimagethingy("enable");
                 
                 
                //if(!Payeezy3DsModes) {
                 placeOrderFunction(data1);
                
            } else {
            	if(!(parseFloat(orderSubTotal).toFixed(2)>parseFloat(0).toFixed(2)))
	        	{
	        	$("#agreeMsg").html("<div class='col-md-12 error-msg' id='agreeMsg'>Order Amount should be greaterThan 0</div>");
                $('#placeOrderBtn').prop('disabled', false);
                return false;
	        	}
            	orderJson = '[{"itemid":"' + deliveryPosId + '","price":"' + deliveryFee + '","amount":"1","itemPosId":"' + deliveryPosId + '","modifier":[]}]';
                paymentJSON = '{"paymentMethod":"' + paymentMethod + '","tip":"' + tip + '","futureDate":"' + futureDate + '","futureTime":"' + futureTime + '","futureOrderType":"' + futreOrderType + '","tax":"' + tax +
                '"}';

                if(addressId != undefined && zoneId != undefined){
                	data1 = '{"itemsForDiscount" : ' + itemsForDiscount + ',"listOfALLDiscounts" : ' + listOfALLDiscounts + ', "discount" : "' + couponDiscount + '", "discountType" : "' + discountType + '","itemPosIds" : "' + itemPoSidsJson + '", "inventoryLevel" : "' + inventoryLevel + '", "voucherCode" : "' + voucherCode + '","orderType" : "' + orderType + '","instruction" : "' + instruction + '","paymentVO" : ' + paymentJSON + ',"deliveryItemPrice" : ' + deliveryFee + ',"orderTotal":"' + total + '","orderJson":' + orderJson + ',"isDeliveryCoupon":' + isDeliveryKoupon + ',"addressId":' + addressId + ',"zoneId":' + zoneId +'}';
                }else{
                	data1 = '{"itemsForDiscount" : ' + itemsForDiscount + ',"listOfALLDiscounts" : ' + listOfALLDiscounts + ', "discount" : "' + couponDiscount + '", "discountType" : "' + discountType + '","itemPosIds" : "' + itemPoSidsJson + '", "inventoryLevel" : "' + inventoryLevel + '", "voucherCode" : "' + voucherCode + '","orderType" : "' + orderType + '","instruction" : "' + instruction + '","paymentVO" : ' + paymentJSON + ',"deliveryItemPrice" : ' + deliveryFee + ',"orderTotal":"' + total + '","orderJson":' + orderJson + ',"isDeliveryCoupon":' + isDeliveryKoupon + '}';
                }
                
                $("#agreeMsg").html("");
                $("#carrdSize").css('display', 'none');
                
                $("#agreeMsg").html("");
                $("#orderMsgSpanId").html("");
                
                console.log(data1);
                $("body").loadingimagethingy("enable");
                
                
               //if(!Payeezy3DsModes) {
                placeOrderFunction(data1);
            }
        } else {
            $("#errorBox3").html("<div class='col-md-12 error-msg' id='errorBox3'>Please select payment method</div>");
            $('#placeOrderBtn').prop('disabled', false);
        }
      }
   }
}

Cardinal.on("payments.validated", function(data, jwt) {
	     console.log("payeezy response of 3DS");
	  console.log("payments.validated "+data)
	     //payeezyLogger(data)
	    
	     switch (data.ActionCode) {
	         case "SUCCESS":
	             // Handle successful transaction, send JWT to backend to verify
	        	 console.log(data.Payment.ExtendedData.CAVV)
		            if(data.Payment.ExtendedData.CAVV!=undefined && data.Payment.ExtendedData.CAVV!=null) {
		                payeezyCavvtoken = data.Payment.ExtendedData.CAVV;
		            }
		             
		            
		if (allowMultiPay != false && cardInfosLength1 > 0 && newCardValue == 0) {

		var vaultedInfo = $("#carrdSize123").val();
		allowMultiPayCheck = 0;
		paymentJSON = '{"vaultedInfo":"' + vaultedInfo + '","ccNumber": "' + cardNumber + '","ccCode":"' + cvv + '","expMonth": "' + '' + '","expYear": "' + '' + '","ccType":"' + cardType + '","paymentMethod":"' + paymentMethod + '","tip":"' + tip + '","saveCard":' + allowMultiPayCheck + ',"futureDate":"' + futureDate + '","futureTime":"' + futureTime + '","futureOrderType":"' + status + '","tax":"' + tax + '"}';
		orderJson = '[{"itemid":"' + deliveryPosId + '","price":"' + deliveryFee + '","amount":"1","itemPosId":"' + deliveryPosId + '","modifier":[]}]';
		if(addressId != undefined && zoneId != undefined){
		data1 = '{"itemsForDiscount" : ' + itemsForDiscount + ',"listOfALLDiscounts" : ' + listOfALLDiscounts + ', "discount" : "' + couponDiscount + '", "discountType" : "' + discountType + '","itemPosIds" : "' + itemPoSidsJson + '", "inventoryLevel" : "' + inventoryLevel + '", "voucherCode" : "' + voucherCode + '","orderType" : "' + orderType + '","instruction" : "' + instruction + '","paymentVO" : ' + paymentJSON + ',"deliveryItemPrice" : ' + deliveryFee + ',"orderTotal":"' + total + '","orderJson":' + orderJson + ',"isDeliveryCoupon":' + isDeliveryKoupon + ',"addressId":' + addressId + ',"zoneId":' + zoneId +'}';
		}else{
		data1 = '{"itemsForDiscount" : ' + itemsForDiscount + ',"listOfALLDiscounts" : ' + listOfALLDiscounts + ', "discount" : "' + couponDiscount + '", "discountType" : "' + discountType + '","itemPosIds" : "' + itemPoSidsJson + '", "inventoryLevel" : "' + inventoryLevel + '", "voucherCode" : "' + voucherCode + '","orderType" : "' + orderType + '","instruction" : "' + instruction + '","paymentVO" : ' + paymentJSON + ',"deliveryItemPrice" : ' + deliveryFee + ',"orderTotal":"' + total + '","orderJson":' + orderJson + ',"isDeliveryCoupon":' + isDeliveryKoupon + '}';
		}

		} else {

		if (posId != 1) {
		if ($("Card_zipCode").val() == "") {
		 $("#Card_zipCode").focus();
		 $("#agreeMsg").html("<div class='col-md-12 error-msg' id='agreeMsg'>Please enter zip code/div>");
		 $('#placeOrderBtn').prop('disabled', false);
		 return false;
		} else if (Card_zipCode.length != 5) {
		 $("#Card_zipCode").focus();
		 $("#agreeMsg").html("<div class='col-md-12 error-msg' id='agreeMsg'>Please enter valid zip code</div>");
		 $('#placeOrderBtn').prop('disabled', false);
		 return false;
		}
		}

		orderJson = '[{"itemid":"' + deliveryPosId + '","price":"' + deliveryFee + '","amount":"1","itemPosId":"' + deliveryPosId + '","modifier":[]}]';
		if (posId == 1) {
		paymentJSON = '{"ccNumber": "' + cardNumber + '","ccCode":"' + cvv + '","expMonth": "' + month + '","expYear": "' + year + '","ccType":"' + cardType + '","paymentMethod":"' + paymentMethod + '","tip":"' + tip + '","saveCard":' + allowMultiPayCheck + ',"futureDate":"' + futureDate + '","futureTime":"' + futureTime + '","futureOrderType":"' + futreOrderType + '","tax":"' + tax +  '"}';
		} else {
		                     	 
		paymentJSON = '{"ccNumber": "' + cardNumber + '","ccCode":"' + cvv + '","expMonth": "' + month + '","expYear": "' + year + '","ccType":"' + cardType + '","paymentMethod":"' + paymentMethod + '","tip":"' + tip + '","saveCard":' + allowMultiPayCheck + ',"futureDate":"' + futureDate + '","futureTime":"' + futureTime + '","futureOrderType":"' + futreOrderType + '","zipCode":"' + Card_zipCode + '","tax":"' + tax + '","payeezyToken":"' + payeezytoken + '","cavvPayeezyToken":"' + payeezyCavvtoken +  '"}';
		}

		if(addressId != undefined && zoneId != undefined){
		data1 = '{"itemsForDiscount" : ' + itemsForDiscount + ',"listOfALLDiscounts" : ' + listOfALLDiscounts + ', "discount" : "' + couponDiscount + '", "discountType" : "' + discountType + '","itemPosIds" : "' + itemPoSidsJson + '", "inventoryLevel" : "' + inventoryLevel + '", "voucherCode" : "' + voucherCode + '","orderType" : "' + orderType + '","instruction" : "' + instruction + '","paymentVO" : ' + paymentJSON + ',"deliveryItemPrice" : ' + deliveryFee + ',"orderTotal":"' + total + '","orderJson":' + orderJson + ',"isDeliveryCoupon":' + isDeliveryKoupon + ',"addressId":' + addressId + ',"zoneId":' + zoneId +'}';
		}else{
		data1 = '{"itemsForDiscount" : ' + itemsForDiscount + ',"listOfALLDiscounts" : ' + listOfALLDiscounts + ', "discount" : "' + couponDiscount + '", "discountType" : "' + discountType + '","itemPosIds" : "' + itemPoSidsJson + '", "inventoryLevel" : "' + inventoryLevel + '", "voucherCode" : "' + voucherCode + '","orderType" : "' + orderType + '","instruction" : "' + instruction + '","paymentVO" : ' + paymentJSON + ',"deliveryItemPrice" : ' + deliveryFee + ',"orderTotal":"' + total + '","orderJson":' + orderJson + ',"isDeliveryCoupon":' + isDeliveryKoupon + '}';
		}
		}

		              placeOrderFunction(data1);
		             break;
	         case "NOACTION":
	             var flag =false;
	             
	             $("#orderMsgSpanId").html("<div class='col-md-12 error-msg' id='orderMsgSpanId'>Your card has been declined. Please retry or use another card</div>");
	             $("#minCCDiv").css('display', 'none');
	             $("#cvv").val(null);
	             $("body").loadingimagethingy("disable");
	            $('#placeOrderBtn').prop('disabled', false);
	             break;

	         case "FAILURE":
	        	 $("#orderMsgSpanId").html("<div class='col-md-12 error-msg' id='orderMsgSpanId'>Your card has been declined. Please retry or use another card</div>");
	             $("#minCCDiv").css('display', 'none');
	             $("#cvv").val(null);
	             $("body").loadingimagethingy("disable");
	            $('#placeOrderBtn').prop('disabled', false);
	             if(data.Payment.ExtendedData.SignatureVerification =='N' &&data.Payment.ExtendedData.ECIFlag =='05'){
	                 startTransaction();
	             }
	             if(data.Payment.ExtendedData.SignatureVerification =='Y' &&data.Payment.ExtendedData.ECIFlag =='00'){
	                 //startTransaction();
	             }
	             break;
	         case "ERROR":
	                $("#orderMsgSpanId").html("<div class='col-md-12 error-msg' id='orderMsgSpanId'>Your card has been declined. Please retry or use another card</div>");
	                $("#minCCDiv").css('display', 'none');
	             $("#cvv").val(null);
	             $("body").loadingimagethingy("disable");
	            $('#placeOrderBtn').prop('disabled', false);
	             break;
	     }
	 });

/*------------------------------------------------------------------------------------------------*/
//validation on card number and their cvv code
function getCardTypeValue() {

    orderType = $('input[name=order]:checked').val();

    var typeVal = $('#cardType').val();

    if (typeVal == 'American Express') {
        $("#cardNumber").attr('maxlength', '15');
        $("#cvv").attr('maxlength', '4');
        $("#cardNumber").val("");
        $("#cvv").val("");
    }
    if (typeVal == 'Master Card') {
        $("#cardNumber").attr('maxlength', '16');
        $("#cvv").attr('maxlength', '3');
        $("#cardNumber").val("");
        $("#cvv").val("");
    }
    if (typeVal == 'Visa') {
        $("#cardNumber").attr('maxlength', '16');
        $("#cvv").attr('maxlength', '3');
        $("#cardNumber").val("");
        $("#cvv").val("");
    }

}

/*--------------------------------------------------------------------------------------------*/

var openingClosingHours = "";
var zoneStatus = "";
var closingDayStatus = "";
var timingStatus = "";

$(document).ready(function() {
    orderType = $('input[name=order]:checked').val();
    $('#signInEmail').keyup(function(e) {
        var signInEmail = $('#signInEmail').val();
        if (signInEmail == "") {
            $("#loginError").html("");
        }
    });

    $('#signInPassword').keyup(function(e) {
        var signInPassword = $('#signInPassword').val();
        if (signInPassword == "") {
            $("#loginError").html("");
        }
    });

    $('#guest-email').keyup(function(e) {
        var guestEmail = $('#guest-email').val();
        if (guestEmail == "") {
            $("#errorBox").html("");
        }
    });


    $.ajax({
        url: "getBussinessHoursStatus",
        type: "GET",
        contentType: "application/json; charset=utf-8",
        success: function(menuItems) {

            var result = JSON.parse(menuItems);
            openingClosingHours = result.openingClosingHours;
            $("#openingClosingHours").html(openingClosingHours);

            zoneStatus = result.zoneStatus
            closingDayStatus = result.closingDayStatus
            timingStatus = result.openingClosingStatus

            if(timingStatus != 'Y'){
        		if(allowFutureOrder==1){
        			  $("#ordernow").prop('checked', false);
                      $("#orderlater").prop('checked', true);
                      $("#futureDateCmbo").css("display", "block");
     			      $("#orderlater").click();
                      $('#ordernow').prop('disabled', true);
        		}
        	}else if(allowFutureOrder!=1){
        		$("#ordernow").prop('checked', true);
        		checkItemList();
        		next_step1();
        	}
         

        },
        error: function() {
            console.log("Error inside future date Ajax call");
        }
    });


    customerId = $("#customerId").val();
    customerPswd = $("#customerPswd").val();
    if (customerId != "") {
        $("#guestLogin").hide();
    } else {
        $("#guestLogin").show();
    }
    
    //alert(delievryZoneStatus)
    if(delievryZoneStatus=='N'){
    	  $("#deliveryzoneStatus").hide();
    }
    
    if (delievryZoneStatus == "Y") {
        $("#deliveryzoneStatus").show();
    }
    
});
/*-------------------------------------------------------------------------------------*/
//check deliveryZone for returning customer

function checkDeliveryZone() {

    var address = $("#signInAddress1").val();
    var address1 = $("#signInAddress2").val();
    var city = $("#signInCity").val();
    var state = $("#signInState").val();
    var zip = $("#signInZip").val();

    if (address == '') {
        $("#signInAddress1").focus();
        $("#deliveryError").html("<div class='col-md-12 error-msg' id='deliveryError'>Please Enter Address</div>");
    } else if (city == '') {
        $("#signInCity").focus();
        $("#deliveryError").html("Please Enter City");
        $("#deliveryError").html("<div class='col-md-12 error-msg' id='deliveryError'>Please Enter City</div>");
    } else if (state == '' || state == "State") {
        $("#signInState").focus();
        $("#deliveryError").html("<div class='col-md-12 error-msg' id='deliveryError'>Please Enter State</div>");
    } else if (zip == '') {
        $("#signInZip").focus();
        $("#deliveryError").html("<div class='col-md-12 error-msg' id='deliveryError'>Please Enter Zip</div>");
    } else {
    	 $("#deliveryError").html("");
        $("body").loadingimagethingy("enable");
        $.ajax({
            url: "findDeliveryZoneType?merchantId=" + merchantId,
            type: "GET",
            contentType: "application/json; charset=utf-8",
            success: function(result) {
                console.log(result.deliveryZoneType);
                if (result.deliveryZoneType == "googleMap") {

                	var deliveryAdd ='';
                	
                	if(address!='' && address!=undefined && address!=null){
                		deliveryAdd =deliveryAdd +""+address
                		
                	}
                	if(address1!='' && address1!=undefined && address1!=null) {
                		deliveryAdd =deliveryAdd +","+address1
                		
                	}    
                	//=======================ajax to fetch delivery zone======================
                    getDeliverZoneForGoogleMap(deliveryAdd, city, state, zip);
                } else {
                    //======================ajax to check delivery is under zone or not========

                    //var data = checkAddressIsInDeliveryZone(addData);
                    $.ajax({
                        url: "checkDeliveryZone",
                        type: 'POST',
                        dataType: 'json',
                        data: "{\"address1\":\"" + address + "\",\"address2\":\"" + address1 +
                            "\",\"city\":\"" + city + "\",\"state\":\"" + state + "\",\"zip\":\"" + zip +
                            "\",\"isDeliveryKoupon\":\"" + isDeliveryKoupon + "\"}",
                        contentType: 'application/json',
                        mimeType: 'application/json',
                        success: function(data) {
                            console.log("+++++++++++++++++++++++"+JSON.stringify(data));
                            if (data.message == "Your zone is in delivery zone") {
                                $("#signInAddress1").val('');
                                $("#signInAddress2").val('');
                                $("#signInCity").val('');
                                $("#signInState").val('');
                                $("#signInZip").val('');

                                $("body").loadingimagethingy("disable");
                                $("#payment").css('display', 'block');
                                $("#couponBox").show();
                                $(fundCodeBox).show();
                                codeFlag = true;
				$('#active4').addClass('active');
				 if(flag==true){
                                $("#paymentBox").css('display', 'block');
                                $("#paymentModesId1").prop("checked", true);
				 }
                                $("#guest").css('display', 'none');
                                document.getElementById('deliveryFeeRow').innerHTML = "<td><strong>Delivery Fee:</strong></td><td></td><td><strong>$<span id='deliveryFee' deliveryPosId='" + data.deliveryItemPosId + "'>" + data.itemPrice + "</span></strong></td>";
                                deliveryPosId = data.deliveryItemPosId;
                                deliveryTaxPrice = data.deliveryTaxPrice;
                                addressId = data.addressId;
                                zoneId = data.zoneId;
                                
                                minimumDeliveryAmount = data.minimumDeliveryAmount;
                                minDeliveryAmountTotal = minimumDeliveryAmount
                                
                                document.getElementById('test').innerHTML = "$"+minDeliveryAmountTotal;

                                var subTotalBeforDeliveryFees = $("#subtotal").val();
                                
                                if (parseFloat(subTotalBeforDeliveryFees) < parseFloat(minimumDeliveryAmount)) {
                                    $("#deliveryAmtPopup").modal('show');
                                    $("#deliveryzoneStatus").removeAttr('checked');
                                    $("#second").css('display', 'block');
                                    $("#payment").css('display', 'none');
                                    $("#couponBox").hide();
                                    	$(fundCodeBox).hide();
                                      codeFlag = false;
                                    $("#guest").css('display', 'none');
                                }else{
                                	if (data.itemPrice > 0) {
                                        document.getElementById('deliveryFeeRow').innerHTML = "<td><strong>Delivery Fee:</strong></td><td></td><td><strong>$<span id='deliveryFee' deliveryPosId='" + data.deliveryItemPosId + "'>" + data.itemPrice + "</span></strong></td>";

                                        deliveryFee = data.itemPrice;
                                        avgDeliveryTime=data.avgDeliveryTime;
                                        
                                        document.getElementById('deliveryFee').innerHTML = (data.itemPrice);
                                        document.getElementById('tax').innerHTML = (parseFloat($("#tax").text()) + data.deliveryTaxPrice).toFixed(2);
                                        document.getElementById('total').innerHTML = (parseFloat($("#total").text()) + data.itemPrice + data.deliveryTaxPrice).toFixed(2);;
                                    }
                                }
                            } else {
                                $("body").loadingimagethingy("disable");
                                $("#myModal122").modal('show');
                            }
                        }
                    });
                }
            },
        });
    }
}

/*-----------------------------------------------------------------------------------*/
$(document).ready(function() {
    $('#returningDeliveryZones').change(function() {
        var addresss = $(this).val();
        var addressArray = addresss.split("#");
        var id = addressArray[0];
        if (id >= 3) {
            $("body").loadingimagethingy("enable");
            $.ajax({
                url: "checkDeliveryAfterLogin?addressId=" + id + "&isDeliveryKoupon=" + isDeliveryKoupon,
                type: "GET",
                contentType: "application/json; charset=utf-8",
                success: function(result) {
                   
                	console.log(result)
                	
                    finalTotal = parseFloat($("#total").text());
                    totalTax = parseFloat($("#tax").text());
                    if (result.message == "Your zone is in delivery zone") {
                    	avgDeliveryTime= result.avgDeliveryTime;
                        deliveryFee = result.itemPrice;
                        document.getElementById('deliveryFeeRow').innerHTML = "<td><strong>Delivery Fee:</strong></td><td></td><td><strong>$<span id='deliveryFee' deliveryPosId='" + result.itemPosId + "'>" + parseFloat(deliveryFee).toFixed(2) + "</span></strong></td>";
                        deliveryDiscountAmount = result.deliveryDiscountAmount;
                        deliveryFee = result.itemPrice;
                        deliveryPosId = result.itemPosId;
                        deliveryTaxStatus=result.deliveryTaxStatus;
                        deliveryTaxPrice = result.deliveryTaxPrice;
                        
                        addressId = result.addressId;
                        zoneId = result.zoneId;
                     
                        //   document.getElementById('tax').innerHTML = (parseFloat($("#tax").text()) + result.deliveryTaxPrice).toFixed(2);
                        //   document.getElementById('total').innerHTML = (parseFloat($("#total").text()) + result.itemPrice + (result.deliveryTaxPrice)).toFixed(2);
                      
                        minimumDeliveryAmount = result.minimumDeliveryAmount;
                        minDeliveryAmountTotal = minimumDeliveryAmount;
                        document.getElementById('test').innerHTML = "$"+minDeliveryAmountTotal;
                        var subTotalBeforDeliveryFee = $("#subtotal").val();
                        
                        if (parseFloat(subTotalBeforDeliveryFee) < parseFloat(minimumDeliveryAmount)) {
                            $("#deliveryAmtPopup").modal('show');
                            $("#deliveryzoneStatus").removeAttr('checked');
                            $("#second").css('display', 'block');
                            $("#payment").css('display', 'none');
                            $("#couponBox").hide();
                            $(fundCodeBox).hide();
                            codeFlag = false;
                            $("#guest").css('display', 'none');
                            $("input:radio").attr("checked", false);
                            
                            $("#returningDeliveryZones").val("0");
                            $("#deliveryError").html("");
                            //$('option[value=0]').attr('selected', 'selected');
                            
                        }else{
                        	 var label;
                        	if (posId == '2') {
                    	       
                    	        if ($("#pickUp").is(":checked")) {
                    	            label = "Foodkonnekt Online Pickup";
                    	            
                    	        } else {
                    	            label = "Foodkonnekt Online Delivery";
                    	            
                    	        }
                            }
                        	if(deliveryTaxStatus==1){
                        		if(isCouponApply)	{
                        			kouponCount = kouponCount - 1;
                        			$('.couponButton').trigger('click');
                        	}else{

                            	var deliveryFeeTaxable = true;
                            	var tipAmmounnt =  $("#tipAmt").val();
                                	var item = '{"itemid":"' + deliveryPosId + '","price":"' + deliveryFee + '","amount":"1","itemPosId":"' + deliveryPosId + '","modifier":[]}';
                                	var data1 = item;
                                	 $.ajax({
                                         url: "applyTaxes?deliveryFeeTaxable="+deliveryFeeTaxable+"&label="+label,
                                         type: 'POST',
                                         contentType: "application/json; charset=utf-8",
                                         data: data1,
                                         success: function(response) {
                                        	 var data = JSON.parse(response);
                                        	 
                                        	  //document.getElementById('total').innerHTML = (parseFloat(data.totalPrice));
                                              document.getElementById('subTotal').innerHTML = ((parseFloat(data.subTotal)).toFixed(2));
                                              document.getElementById('tax').innerHTML = ((parseFloat(data.Tax))).toFixed(2);
                                             
                                              if(data.ConvenienceFee > 0 && data.ConvenienceFee != ''  && data.ConvenienceFee != undefined && data.ConvenienceFee != null){
                                            	  document.getElementById('ConvenienceFee').innerHTML = ((parseFloat(data.ConvenienceFee)).toFixed(2));
                                              }						

                                              if (data.auxTax > 0 && data.auxTax != '' && data.auxTax != undefined && data.auxTax != null) {
                                                  document.getElementById('auxTaxRow').innerHTML = "<td><strong>Aux Tax:</strong></td><td></td><td><strong>$<span id='auxTax'>" + parseFloat(data.auxTax).toFixed(2) + "</span></strong></td>";
                                              } else {
                                                  document.getElementById('auxTaxRow').innerHTML = "";
                                              }
                                              
                                              document.getElementById('total').innerHTML = (parseFloat($('#total').text()) + parseFloat(deliveryFee) + parseFloat(deliveryTaxPrice)).toFixed(2);
                                              
                                              }
                                         });
                                	 
                                	 $("body").loadingimagethingy("disable");
                        		}
                                 }else{
                                	 document.getElementById('total').innerHTML = (parseFloat($('#total').text()) + parseFloat(deliveryFee)).toFixed(2);
                                	 $("body").loadingimagethingy("disable");
                                 }
                        	
                            
                            $("#guest").css('display', 'none');
                            $("#payment").css('display', 'block');
                            $("#couponBox").show();
                            if(guestCustomerPassword != ''){
                            $(fundCodeBox).show();
                            codeFlag = true;
                            }
				      $('#active4').addClass('active');
   	                           if(flag==true){
   	                        	$("#paymentModesId1").prop("checked", true);
   	                              $("#paymentBox").css('display', 'block');
   	                           }
                    }
                        $("body").loadingimagethingy("disable");
                    } else {
                        $("#myModal122").modal('show');
                        $("body").loadingimagethingy("disable");
                    }
                }
            });
        }
    });

    //calculate tipAmt and add it in total
    $('#tip').change(function() {
        var cartCount1 = parseInt($("#cartCount").text());
        if (cartCount1 == null || cartCount1 == 0 || cartCount1 == 'undefined' || orderItemsLength.length == 0) {
            $('#checkItem').modal('show')
            tip = $("#tipAmt").val();
        } else {
            tip = $("#tipAmt").val();
            
            if (tip > 0 && Number(tip) >= tipAmt && tip != '') {
                document.getElementById('total').innerHTML = ((parseFloat($("#total").text()) + (parseFloat(tip)) - (parseFloat(tipAmt)))).toFixed(2);
            } else if(Number(tip) > 0) {
                document.getElementById('total').innerHTML = ((parseFloat($("#total").text()) - parseFloat(tipAmt))+parseFloat(tip)).toFixed(2);
            }else document.getElementById('total').innerHTML = ((parseFloat($("#total").text()) - parseFloat(tipAmt))).toFixed(2);
          
           if(tip=="")
            tipAmt = 0;
           else
        	   tipAmt=tip;
           
        }
    });



    //checks whether allowMultiPay is true or not
    var allowMultiPay = $("#allowMultiPay").val();
    if (cardInfo != "") {
        toCheckMultiPay(JSON.parse(cardInfo));
    }


    $("#carrdSize123").change(function() {
        var cardData = this.value;
        if (cardData == 'ADD_NEW_CARD') {
            newCardValue = 1;
            if(flag==true){
               	$("#paymentModesId1").prop("checked", true);
                     $("#paymentBox").css('display', 'block');
                  }
        } else {
            newCardValue = 0;
            $("#paymentBox").css("display", "none");
        }
    });

    //checks save card checkbox is checked or not
    $("#allowMultiPayCheck").change(function() {
        if (document.getElementById('allowMultiPayCheck').checked) {
            allowMultiPayCheck = 1;
        } else {
            allowMultiPayCheck = 0;
        }
    });
});


//validation on tip input box
$(document).on("keyup", ".tip", function() {
    var val = $('#tipAmt').val();
    if (isNaN(val)) {
        val = val.replace(/[^0-9\.]/g, '');
        if (val.split('.').length > 2)
            val = val.replace(/\.+$/, "");
    }
    $('#tipAmt').val(val);

});
/*-----------------------------------------------------------------------------------*/
//hide-show delivery dropdown
function returningSignUp() {
	$('input[name=card]').attr('checked',false);
	$("#errorBox12").html("");
	$("#loginError").html("");
    if (document.getElementById('delivery').checked) {
        if (cust != '') {
            $("#guestCustomerDiv").css('display', 'none');
            $("#deliverySelectDiv").css('display', 'block');
        } else {
            $("#guestCustomerDiv").css('display', 'block');
            $("#deliverySelectDiv").css('display', 'none');
        }
    }
    if (document.getElementById('pickUp').checked) {
        $("#guestCustomerDiv").css('display', 'block');
        $("#deliverySelectDiv").css('display', 'none');
	$("active3").addClass("active");

    }
}

/*-----------------------------------------------------------------------------------*/
//check sub-total is greater then pickup amount
function checkPickUpAmt() {
	$("#tipdiv").show();
	tipCheck = true;
    $("#errorBox1").html("");
    var subTotal = $("#subtotal").val();
    var minOrderAmount = $("#pickUpMinOrderAmount").val();
    if (parseFloat(subTotal) < parseFloat(minOrderAmount)) {
        $("#myModal12").modal('show');
        $("#pickUp").removeAttr('checked');
    }
       else{
    	
    	if(activeTipsForPickup!='' && activeTipsForPickup!=undefined  && activeTipsForPickup =='true') {
    		tip =(subTotal/100) * tipsForPickup
    		$('#tipAmt').val(tip.toFixed(2));

         var tip1 =0
        tip = $("#tipAmt").val();
         
        if (tip > 0 &&  tip != '') {
            document.getElementById('total').innerHTML = ((parseFloat($("#total").text()) + (parseFloat(tip)) - (parseFloat(tipAmt)))).toFixed(2);
        } else {
            document.getElementById('total').innerHTML = ((parseFloat($("#total").text()) - parseFloat(tipAmt))).toFixed(2);
        }
       if(tip=="")
    	   tipAmt = 0;
       else
    	   tipAmt=tip;
    }
    }
}


/*---------------------------------------------------------------------------------*/

//validation on order type(pickup or delivery)
function checkOrderTypeStatus() {
	
	checkLoyaltyProgram();
	$("#tipdiv").show();
	if(!tipCheck){
		 if ($("#pickUp").is(":checked"))
			 checkPickUpAmt()
		 else if($("#delivery").is(":checked"))
			 checkDelivery()
	}
	
	$('input[name=card]').attr('checked',false);
	$("#returning1").prop("checked", false);
	$("#guest1").prop("checked", false);
	$("#returninBox1").css('display','none');
    $("#deliverySelectDiv").css('display','none');
	$("#guestBoxDiv").css('display','none');
	$("#guestCustomerDiv").css('display','none');
	$(".guest").css('display','none');
	$("#paymentBox").css('display','none');
	
	
    var cartCount1 = parseInt($("#cartCount").text());

    if ($("#pickUp").is(":checked") || $("#delivery").is(":checked")) {
        // $("#errorBox1").css("display", "none");
        $("#errorBox1").html("");
        if (cartCount1 == null || cartCount1 == 0 || cartCount1 == 'undefined' || orderItemsLength.length == 0) {
            $('#checkItem').modal('show')
        }
        $('#nextButton').removeAttr('disabled');
        $("#second").css("display", "none");


        if (customerSessionValue) {

            if ($("#delivery").is(":checked")) {
				showFundBox(customerSessionValue);
            	$("#couponError").html("");
                $("#guest").css('display', 'block');
                $("#guest").css('opacity', 1);
                $("#guest").css('left', "0%");
                $("#second").css('display', 'none');
                $("#guestCustomerDiv").css('display', 'none');
                $("#deliverySelectDiv").css('display', 'block');
                $("#returning1").css('display', 'block');
                
                $("#pupose").css('display', 'block');
                $("#custSection").css('display', 'none');
		$("#active3").addClass("active");
            }
            if ($("#pickUp").is(":checked")) {
				showFundBox(customerSessionValue);
                if(isDeliveryKoupon){
                	couponErrorBoxMessage();
                }
            	$("#payment").css('display', 'block');
            	 $("#couponBox").show();
               $(fundCodeBox).show();
               codeFlag = true;
		$('#active4').addClass('active');
		$('#active3').addClass('active'); 
		if(flag==true){
           	$("#paymentModesId1").prop("checked", true);
                 $("#paymentBox").css('display', 'block');
              
                $("#guest").css('display', 'none');
                $("#second").css('display', 'none');
                }
            }
            $("#paymentModesId1").prop("checked", true);
            //document.getElementById("active4").style.color = "black";
        } else {
        	if ($("#pickUp").is(":checked") && isDeliveryKoupon) {
                	couponErrorBoxMessage();
            }else if($("#delivery").is(":checked") && isDeliveryKoupon){
            	$("#couponError").html("");
            }
        	$("#returning1").prop("checked", true);
            $("#guest").css('display', 'block');
        	$("#returninBox1").css('display','block');
        	$("#guestCustomerDiv").css('display','block');
		$("#active3").addClass("active");
        	$(".returning1").css('display','block'); 
            $("#guest").css('opacity', '1');
            $("#guest").css('left', '0%');
            $("#custSection").css('display', 'block');
            $("#payment").css('display', 'none');
            $("#couponBox").hide();
			      $(fundCodeBox).hide();
		      	codeFlag = false;
            $("#paymentModesId1").prop("checked", true);
           // document.getElementById("active3").style.color = "black";
        }

    } else {
        if (cartCount1 == null || cartCount1 == 0 || cartCount1 == 'undefined' || orderItemsLength.length == 0) {
            $('#checkItem').modal('show')
        }
       
        $("#errorBox1").html("<div class='col-md-12 error-msg' id='errorBox1'>Please select order type</div>");
        $('#nextButton').off('click');
        $("#second").css("display", "block");

    }

    //to calculate tax for foodtronix
    if (posId == '2') {
        var label;
        if ($("#pickUp").is(":checked")) {
            label = "Foodkonnekt Online Pickup";
        } else {
            label = "Foodkonnekt Online Delivery";
        }

        $.ajax({
            url: "applyTaxForFoodTronix?label=" + label,
            type: 'GET',
            success: function(result) {
                var data = JSON.parse(result);
                // var tip= $("#tipAmt").val();
                document.getElementById('total').innerHTML = ((parseFloat(data.totalPrice) + parseFloat(tip)).toFixed(2));
                document.getElementById('subTotal').innerHTML = ((data.subTotal).toFixed(2));
                document.getElementById('tax').innerHTML = ((data.Tax).toFixed(2));

                if (data.auxTax > 0) {
                    document.getElementById('auxTaxRow').innerHTML = "<td><strong>Aux Tax:</strong></td><td></td><td><strong>$<span id='auxTax'>" + data.auxTax.toFixed(2) + "</span></strong></td>";
                }
                document.getElementById('ConvenienceFee').innerHTML = ((data.ConvenienceFee).toFixed(2));

            }
        });
    }
}
/*------------------------------------------------------------------------------------*/
//validation on mobile number
function phoneFormatter() {
    $('#guest-phone').on('input', function() {
        var number = $(this).val().replace(/[^\d]/g, '')
        if (number.length == 7) {
            number = number.replace(/(\d{3})(\d{4})/, "$1-$2");
        } else if (number.length == 10) {
            number = number.replace(/(\d{3})(\d{3})(\d{4})/, "$1-$2-$3");
        }
        $(this).val(number)
    });
};

$(phoneFormatter);
/*-------------------------------------------------------------------------------------*/
function checkItemList() {
    var cartCount = parseInt($("#cartCount").text());
    
    var checkSubTotalValue = $("#subtotal").val();
    if (cartCount == null || cartCount == 0 || cartCount == 'undefined' || orderItemsLength.length == 0 || orderItemsLength.length == null) {
        $('#disbleNext').unbind('click');
        $('#disbleNext').prop('disable', true);
        $('#checkItem').modal('show')
    }
    
    else {
        next_step1();
        if (allowFutureOrder == 1) {
            var status = false;
            if ($('#orderlater').is(':checked')) {

                var futureDate = $("#futureOrderDateCombo").val();
                var futureTime = $('#futureOpeingTime').val();
                if (futureDate == '' || futureDate == 'select') {
                    $("#futureOrderDateCombo").focus();
                    $("#orderLaterError").html("<div class='col-md-12 error-msg' id='orderLaterError'>Please select date</div>");
                    $(this).prop('checked', false);
                    status = false;
                    $('#disbleNext').unbind('click');
                    $('#disbleNext').prop('disable', true);
                    $("#first").css("display", "block");
                    $("#second").css("display", "none"); 
                } else if (futureTime == '' || futureTime == 'select') {
                    $("#futureOpeingTime").focus();
                    $("#orderLaterError").html("<div class='col-md-12 error-msg' id='orderLaterError'>Please select time</div>");
                    $(this).prop('checked', false);
                    status = false;
                    $('#disbleNext').unbind('click');
                    $('#disbleNext').prop('disable', true);
                    $("#first").css("display", "block"); 
                    $("#second").css("display", "none"); 
                } else {
                    $("#orderLaterError").html("");
                    status = true;
                    $('#disbleNext').removeAttr('disabled');
                    $('#disbleNext').prop('disabled', false);
		    $('#active2').addClass('active'); 
		    
                }
            }
            if ($('#ordernow').is(':checked')) {
                $("#orderLaterError").html("");
                $('#disbleNext').removeAttr('disabled');
                $('#disbleNext').prop('disabled', false);
		$('#active2').addClass('active'); 
		
            }
        } else {
            $('#disbleNext').removeAttr('disabled');
            $('#disbleNext').prop('disabled', false);
        }


    }
}


//getDeliverystatusWhileFutureOrdering
function checkDeliveryStatusWhileFutureorder()
{
	 var selectDate = document.getElementById('futureOrderDateCombo').value;
	    var selectTime= document.getElementById('futureOpeingTime').value;
	    if(selectDate != "select" && selectTime!= "select"){
	    $.ajax({
	        type: 'GET',
	        url: "getStatusForDelivery?selectTime="+selectTime+ "&selectDate="
			+ selectDate,
	        success:function(data){
	        	if(data =="N"){
	        		  $("#deliveryzoneStatus").css("display", "none");
	        	}
	        	if(data =="Y"){
	      		  $("#deliveryzoneStatus").css("display", "inline");

	        	}
	        	
	        }
		});	
}
}
/*-------------------------------------------------------------------------------------*/
function checkCustomerStatus() {
    checkDeliveryZone();
    
    var cartCount2 = parseInt($("#cartCount").text());
    
    if ($("#guest1").is(":checked") || $("#returning1").is(":checked")) {
        $("#errorBox12").css("display", "none");
        if (cartCount2 == null || cartCount2 == 0 || cartCount2 == 'undefined' || orderItemsLength.length == 0) {
            $('#checkItem').modal('show')
        }
        if ($("#guest1").is(":checked")) {
            registerCustomer();
            if (guestStatus == true) {
                $('#nextButtonCustomer').removeAttr('disabled');
                $('#nextButtonCustomer').prop('disabled', false);
				$("#paymentModesId1").prop("checked", true);
            } else {
                $('#nextButtonCustomer').unbind('click');
                $('#nextButtonCustomer').prop('disable', true);
				$("#paymentModesId1").prop("checked", true);
            }
        }
        
        if ($("#returning1").is(":checked")) {
        	customerLogin();
            if (guestStatus == true) {
                $('#nextButtonCustomer').removeAttr('disabled');
                $('#nextButtonCustomer').prop('disabled', false);
                $('#couponBox').prop('display', 'block');
				$("#paymentModesId1").prop("checked", true);  
                
            } else {
                $('#nextButtonCustomer').unbind('click');
                $('#nextButtonCustomer').prop('disable', true);
            }
        }
        
    } else {
        if (cartCount2 == null || cartCount2 == 0 || cartCount2 == 'undefined' || orderItemsLength.length == 0) {
            $('#checkItem').modal('show')
        }

        if (customerSessionValue) {
            $("#errorBox12").html("")
            $('#nextButtonCustomer').unbind('click');
            $('#nextButtonCustomer').prop('disable', true);
        } else {
          //  $("#errorBox12").html("please select Customer");
        	$("#errorBox12").html("<div class='col-md-12 error-msg' id='errorBox12'>Please select customer</div>");
            

        }
        $('#nextButtonCustomer').unbind('click');
        $('#nextButtonCustomer').prop('disable', true);

    }
}

/*-------------------------------------------------------------------------------------*/
//checks whether subtotal is greater than credit card amt
var data = null;

function getPaymentModeValue(data) {
	
	
    $("#agreeMsg").html("")
    $('#placeOrderBtn').prop('disabled', false);
    var minPickupOrderAmount = $("#pickUpMinOrderAmount").val();
    var mimDeliveryAMount = $("#minDeliveryAmount").val();
    
    minDeliveryAmountTotal = minimumDeliveryAmount;
    document.getElementById('test').innerHTML = "$"+minDeliveryAmountTotal;
    
    var subTotal = document.getElementById('subTotal').innerHTML;
    if (data == "Credit Card") {
    	 $("#paymentBox").css('display', 'block');
        $("#errorBox3").html("");
        //var subTotal = $("#subtotal").val();
       
        var minOrderAmount = $("#ccMinOrderAmount").val();
        if (minOrderAmount != 0 && parseFloat(subTotal) < parseFloat(minOrderAmount)) {
            $("#paymentModesId1").prop("checked", false);
            $('#exampleModal').modal('show')
			return true;
        }else if($("#pickUp").is(":checked") && minPickupOrderAmount !=0 && parseFloat(subTotal) < parseFloat(minPickupOrderAmount)){
        	$("#paymentModesId1").prop("checked", false);
            $("#myModal12").modal('show');
			return true;
        }else if($("#delivery").is(":checked") && mimDeliveryAMount !=0 && parseFloat(subTotal) < parseFloat(mimDeliveryAMount)){
        	$("#paymentModesId1").prop("checked", false);
            $("#deliveryAmtPopup").modal('show');
			
			return true;
        }else {
            if (allowMultiPay != false && allowMultiPay != '') {
                if (data == "Credit Card") {
                    if (cardInfosLength1 > 0 && newCardValue == 0) {
                        $("#paymentBox").css('display', 'none');
                        $("#carrdSize").css("display", "block");
                    } else {
                    	if(flag==true){
	                        	$("#paymentModesId1").prop("checked", true);
	                              $("#paymentBox").css('display', 'block');
	                           }
                        $("#carrdSize").css("display", "none");
                    }
                }
            } else {
            	if (flag == true && data == 'Credit Card')
					$("#paymentBox").css('display', 'block');
				$("#carrdSize").css("display", "none");
            }
        }
    } else {
    	
    	
    	if($("#pickUp").is(":checked") && minPickupOrderAmount !=0 && parseFloat(subTotal) < parseFloat(minPickupOrderAmount)){
        	//$("#paymentModesId1").prop("checked", false);
    		$('input[name=card]').attr('checked',false);
            $("#myModal12").modal('show');
        }else if($("#delivery").is(":checked") && mimDeliveryAMount !=0 && parseFloat(subTotal) < parseFloat(mimDeliveryAMount)){
    		$('input[name=card]').attr('checked',false);
            $("#deliveryAmtPopup").modal('show');
        }else{
        	$("#errorBox3").html("");
            $("#carrdSize").css('display', 'none');
        }
    }
}
/*-------------------------------------------------------------------------------------*/
//to set guest password 	 
function addPasswordGestUser(data) {
    if (data == 'YES') {
        window.location.href = "setGuestPasswordV2";
    } else {
        window.location.href = "cancelAccountRequestV2";
    }
}

/*-------------------------------------------------------------------------------------*/
//shows multipay dropdown
function toCheckMultiPay(cardInfo) {
    if (allowMultiPay == "") {
    	if(flag==true){
           	$("#paymentModesId1").prop("checked", true);
                 $("#paymentBox").css('display', 'block');
              }
        cardInfo = '';
    } else {
        if (allowMultiPay) {
            if ('${cardInfo}' === '') {} else {
                $('#carrdSize123').empty();
                var cardInfos = [];
                cardInfos = (cardInfo);
                if (cardInfos != "") {
                    cardInfosLength1 = cardInfos.length;

                    for (var i = 0; i < cardInfosLength1; i++) {
                        $('<option  value="' + cardInfos[i].id + "_" + cardInfos[i].last4 + '" >' + cardInfos[i].cardType + " " + cardInfos[i].last4 + '</option>').appendTo('#carrdSize123');
                    }
                    $('<option  value="ADD_NEW_CARD" >ADD NEW CARD</option>').appendTo('#carrdSize123');
                }
            }
        }
    }
}
/*-------------------------------------------------------------------------------------*/
function checkAddressIsInDeliveryZone(addData) {

    $.ajax({
        url: "checkDeliveryZone",
        type: 'POST',
        dataType: 'json',
        data: addData,
        contentType: 'application/json',
        mimeType: 'application/json',
        success: function(data) {
            console.log(data);
            returnData = data;
            return returnData;
        }
    });
}

function getDeliverZoneForGoogleMap(address1, city, state, zip,enableCheckBox) {
	
/*	alert(address1)
*/    $.ajax({
        url: "fatchDeliveryZone?address=" + address1 + " " + city + " " + state + " " + zip,
        type: "GET",
        contentType: "application/json; charset=utf-8",
        success: function(result) {
            console.log(result);

            $("body").loadingimagethingy("disable");

            polygonMap = result;
            initMap();
        },
        error: function() {
            console.log("Error inside future date Ajax call");
        }
    })
}




function initMap() 
{
	 var script = document.createElement('script');
	  script.type = 'text/javascript';
	  script.async = false;  
	  script.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDjnluKhome9lmt5LLKnIgqot7HtyDetes&libraries=drawing,geometry&sensor=false&callback=initMapFunction';
	  document.body.appendChild(script);
} 




var address ;
var address1;
var city ;
var state;
var zip ;
var email;
var name ;
var phone ;
var password;
function initMapFunction()
{	
    
    if ($("#guest1").is(":checked")) {
    	 address = $("#guest-address").val();
    	 address1 = $("#guest-address1").val();
    	 city = $("#guest-city").val();
    	 state = $("#guest-state").val();
    	 zip = $("#guest-zipcode").val();
    	 email = $("#guest-email").val();
    	 name = $("#guest-name").val();
    	 phone = $("#guest-phone").val();
    	 password = $("#password").val();
    }
    
    if($("#returning1").is(":checked")) {
    	 address = $("#signInAddress1").val();
         address1 = $("#signInAddress2").val();
         city = $("#signInCity").val();
         state = $("#signInState").val();
         zip = $("#signInZip").val();
    	
    }
    
    
    if($('#custSection').css('display') == 'none')
    {
    	 address = $("#signInAddress1").val();
         address1 = $("#signInAddress2").val();
         city = $("#signInCity").val();
         state = $("#signInState").val();
         zip = $("#signInZip").val();
    }
    
    
    
    /*if (address == '') {
        $("#signInAddress1").focus();
        $("#deliveryError").html("<div class='col-md-12 error-msg' id='deliveryError'>Please Enter Address</div>");
    } else if (city == '') {
        $("#signInCity").focus();
        $("#deliveryError").html("Please Enter City");
        $("#deliveryError").html("<div class='col-md-12 error-msg' id='deliveryError'>Please Enter City</div>");
    } else if (state == '' || state == "State") {
        $("#signInState").focus();
        $("#deliveryError").html("<div class='col-md-12 error-msg' id='deliveryError'>Please Enter State</div>");
    } else if (zip == '') {
        $("#signInZip").focus();
        $("#deliveryError").html("<div class='col-md-12 error-msg' id='deliveryError'>Please Enter Zip</div>");
    } else {*/
    	// $("#deliveryError").html("");
	     var enableCheckBoxForDelivery = 0;
	    
	   var finalLatLng;
	   var  bermudaTriangle;
	   var filterMap = {};
	  customerLat = polygonMap.customerLat;
  	customerLng = polygonMap.customerLng;
		
  	var mapKeys=Object.keys(polygonMap);
    	var mapKeyLength=mapKeys.length;	
    	var latLongList=[];
    	//var deliveryFee=[];
    	for(var i=1;i<=(mapKeyLength-2)/2;i++){
    	mapK=Object.keys(polygonMap);
   
     finalLatLng = new google.maps.LatLng(customerLat,customerLng);
     bermudaTriangle = new google.maps.Polygon({paths: polygonMap['latLongList'+i]});
	 console.log(google.maps.geometry.poly.containsLocation(finalLatLng, bermudaTriangle));
	 if(google.maps.geometry.poly.containsLocation(finalLatLng, bermudaTriangle)== true)
	  {
		filterMap[polygonMap['deliveryFee'+i]] = filterMap[polygonMap['deliveryFee'+i]] || [];
		filterMap[polygonMap['deliveryFee'+i]].push(polygonMap['latLongList'+i]);
	  //filterMap[polygonMap['deliveryFee'+i]] = polygonMap['latLongList'+i];
		break;
	  }
    
    	}
    	var ordered = {};
    	console.log(Object.keys(filterMap));
    	console.log(filterMap);
    	var keyy = Object.keys(filterMap)[0];
	  var valuee = filterMap[keyy];
	  console.log(JSON.stringify(filterMap));
	  
	  Object.keys(filterMap).sort().forEach(function(key) {
		  ordered[key] = filterMap[key];
			  return false;
		});
	  var deliveryFees = Object.keys(filterMap)[0];
	  var plgon = filterMap[deliveryFees];
	  console.log(deliveryFees+"----"+plgon);
	  console.log(JSON.stringify(ordered));
	  console.log(JSON.stringify(plgon));
	  if(google.maps.geometry.poly.containsLocation(finalLatLng, bermudaTriangle)== true)
	  {
		  	console.log("inside delivery zone-true");
		  	deliveryZoneStatus= true;
		  
		  $.ajax({
			  url : "checkDeliveryZoneForGoogleMap?deliveryFee="+deliveryFees+"&plgon="+JSON.stringify(plgon),
              type : 'POST',
              dataType : 'json',
              data : "{\"address1\":\"" + address + "\",\"address2\":\"" + address1
                  + "\",\"city\":\"" + city + "\",\"state\":\"" + state + "\",\"zip\":\"" + zip
                  + "\",\"isDeliveryKoupon\":\"" + isDeliveryKoupon+"\"}",
              contentType : 'application/json',
              mimeType : 'application/json',
              success : function(data) {
              	
              	console.log("###########################"+JSON.stringify(data))
              	
                  $("body").loadingimagethingy("disable");

                  minimumDeliveryAmount = data.minimumDeliveryAmount;
                  
                  var subTotalBeforDeliveryFees = $("#subtotal").val();
                  
                  if (parseFloat(subTotalBeforDeliveryFees) < parseFloat(minimumDeliveryAmount)) {
                      $("#mimimumDeliveryAmountPopup").modal('show');
                      $("#deliveryzoneStatus").removeAttr('checked');
                  }else{
                  if (data.message == "Your zone is in delivery zone") {
                      $("#guest").css('display', 'none');
                      $("#payment").css('display', 'block');
                      $("#couponBox").show();
							   	if(guestCustomerPassword != ''){
								  	codeFlag = true;
									  $(fundCodeBox).show();
							   	}
		$("#active4").addClass ("active"); 
		if(flag==true){
           	$("#paymentModesId1").prop("checked", true);
                 $("#paymentBox").css('display', 'block');
              }
		      
                      if (data.itemPrice != null) {

                          document.getElementById('deliveryFeeRow').innerHTML = "<td><strong>Delivery Fee:</strong></td><td></td><td><strong>$<span id='deliveryFee' deliveryPosId='" + data.deliveryItemPosId + "'>" + data.itemPrice + "</span></strong></td>";

                          deliveryFee = data.itemPrice;
                          avgDeliveryTime=data.avgDeliveryTime;
                          deliveryPosId = data.deliveryItemPosId;
                          deliveryTaxPrice = data.deliveryTaxPrice;
                          
                          addressId = data.addressId;
                          zoneId = data.zoneId;
                          
                          document.getElementById('deliveryFee').innerHTML = (data.itemPrice);
                          document.getElementById('tax').innerHTML = (parseFloat($("#tax").text()) + data.deliveryTaxPrice).toFixed(2);
                          document.getElementById('total').innerHTML = (parseFloat($("#total").text()) + data.itemPrice + deliveryTaxPrice).toFixed(2);
                          
                          
                          //==============if order type== pickup====================
                          var apiData = "{\"address1\":\"" + address + "\",\"address2\":\"" + address1 +
                              "\",\"city\":\"" + city + "\",\"state\":\"" + state + "\",\"zip\":\"" + zip + "\",\"emailId\":\"" + email + "\",\"firstName\":\"" + name + "\",\"phoneNumber\":\"" + phone + "\",\"password\":\"" + password + "\",\"freekwentBox\":\"" + enableCheckBoxForDelivery + "\",\"loyalityProgram\":\"" + loyaltyProgramVal  +"\"}";
                             callRegisterApi(apiData);

                      }
                  } else {
                      if (($("#pickUp").is(":checked"))) {} else {
                          $("#myModal122").modal('show');
                      }
                  }
              }
              },
              error : function(data, status, er) {
            	  var msg = data.message;
    	              if (typeof msg === "undefined") {
    	            	window.location.href = "sessionTimeOut";
    	              }else{
    	            	alert("error: " + data + " status: " + status + " er:" + er);
    	              }
              }
            });
		  
	  }else{
		  console.log("invalid zone");
		  $("body").loadingimagethingy("disable");
          $("#myModal122").modal('show');
          return false;
	  }
    //}
}


function callRegisterApi(apiData) {
    $.ajax({
        url: "registerCustomer",
        type: 'POST',
        dataType: 'json',
        data: apiData,
        contentType: 'application/json',
        mimeType: 'application/json',
        success: function(data) {
            console.log(data);
            
        
            if(data!=undefined && data.addressId!=undefined && data.addressId!='' ) {
            	addressId = data.addressId;
            }

            $("body").loadingimagethingy("disable");
            if (data.message == "Your all already registerded") {
                $("body").loadingimagethingy("disable");
                $("#errorBox").html("<div class='col-md-12 error-msg' id='errorBox'>Your emailId already registerded</div>");
            } else {
                guestStatus = true;
						codeFlag = false;
						showFundBox(data.customerAfterSuccessId);
                $("#guest").css('display', 'none');
                $("#payment").css('display', 'block');
                $("#couponBox").show();
						$(fundCodeBox).show();
		$('#active4').addClass('active');
		if(flag==true){
           	$("#paymentModesId1").prop("checked", true);
                 $("#paymentBox").css('display', 'block');
              }
		
                $("body").loadingimagethingy("disable");
                
            }
        },
    });
}

function customerSessionValues(customerSessionValue) {

    if (customerSessionValue != null && customerSessionValue != '') {
        customerSessionValue = true
        document.getElementById("active3").style.display = "none";
    } else {
        customerSessionValue = false
    }
}

/*window.onscroll = function() {
    myFunction()
};
var header = document.getElementById("myHeader");
var sticky = header.offsetTop;

function myFunction() {
    if (window.pageYOffset >= sticky) {
        header.classList.add("sticky");
    } else {
        header.classList.remove("sticky");
    }
}*/

function privous() {
    if ($("#pickUp").is(":checked")) {
        $("#pickUp").attr("checked", false);
    }
    if ($("#delivery").is(":checked")) {
        $("#delivery").attr("checked", false);
    }

}

$(document).ready(function() {

    var typeVal = $('#cardType').val();
    if (typeVal == 'American Express') {
        $("#cardNumber").attr('maxlength', '15');
        $("#cvv").attr('maxlength', '4');
        $("#cardNumber").val("");
        $("#cvv").val("");
    }

    $('#submit').attr('disabled', 'disabled');
    $('input[type="radio"]').click(function() {
        var inputValue = $(this).attr("value");
        if (inputValue != 'Credit Card') {
            var targetBox = $("." + inputValue);
            $(".box").not(targetBox).hide();
            $(targetBox).show();
        }
    });
    $(function() {
        var button = $("#check"),
            submitButt = $("#submit");
        button.on("click", function(e) {
            submitButt.prop("disabled", false); // NOT a toggle
        });
    });
});


$(document).ready(function() {
    $('#returningDeliveryZones').on('change', function() {
        if (this.value == '1') {
            $("#business").show();
        } else {
            $("#business").hide();
        }
    });
    
    
});

function allowFutureOrders(allowFutureOrder) {
    if (allowFutureOrder == 1) {
        $("#AllowfutureId").css("display", "inline");
    }else {
        $("#AllowfutureId").css("display", "none");
    }
}
$(document).ready(function() {

	$("#pre_btn1").on("click", function() {
		$("#tipdiv").hide();
	});
	
    $("#ordernow").on("click", function() {
        $("#orderlater").prop('checked', false);
        $("#orderLaterError").html("");
        $("#futureDateCmbo").css("display", "none");

    });


    $("#orderlater").on("click", function() {
        $("#ordernow").prop('checked', false);
        $("body").loadingimagethingy("enable");
        if (allowFutureOrder == 1) {
        	if(timingStatus == 'N')
    		{
    		 $('#ordernow').prop('disabled', true);
    		}
            $("#futureDateCmbo").css("display", "block");
            $("#orderLaterError").html("");
            $.ajax({
                url: "getFutureDates",
                type: "GET",
                contentType: "application/json; charset=utf-8",
                success: function(result) {
                    $('#futureOrderDateCombo').html('<option value="select">Select Date</option>');
                    for (var i = 0, l = result.length; i < l; i++) {
                        $('<option value="' + result[i] + '">' + result[i] + '</option>').appendTo('#futureOrderDateCombo');
                    }
                    $("body").loadingimagethingy("disable");
                },
                error: function() {
                	
                    console.log("Error inside future date Ajax call");
                }
            })

        } else {}
    });
    
    $("#futureOpeingTime").on("change", function() {
    	checkDeliveryStatusWhileFutureorder();
    });
    
    $("#futureOrderDateCombo").on("change", function() {
        var futuredate = $(this).val();
        var orderType;
        $("body").loadingimagethingy("enable");
        $("#orderLaterError").html("");
        if ($('#delivery').is(':checked')) {
            orderType = "Delivery";
        } else {
            orderType = "Pickup";
        }
        $.ajax({
            url: "getFutureDateOpeningTime?futureDate=" + futuredate + "&orderType=" + orderType,
            type: "GET",
            contentType: "application/json; charset=utf-8",
            success: function(result) {
                $('#futureOpeingTime').html('<option value="select">Select Time</option>');

                for (var i = 0, l = result.length; i < l; i++) {
                    $('<option value="' + result[i] + '">' + result[i] + '</option>').appendTo('#futureOpeingTime');
                }
                $("body").loadingimagethingy("disable");
            },
            error: function() {
                console.log("Error inside future date Ajax call");
            }
        })
    });

    $(".couponButton").click(function() {
        var couponC = $("#coupon").val();
        var total = 0;
        var isDeliveryTaxable = false;
        var cartArray = {};
        cartArray["tax"] = $("#tax").text();
        cartArray["subtotal"] = $("#subTotal").text();
        cartArray["total"] = $("#total").text();
        cartArray["convFee"] = 0; //parseFloat(convenienceFeePrice);
        cartArray["deliveryFee"] = deliveryFee;
        
        if(deliveryTaxStatus == 1){
        	isDeliveryTaxable = true;
        }
        cartArray["deliveryTaxable"] = isDeliveryTaxable;
        cartArray["deliveryPosId"] = deliveryPosId;

	    if (tip != "" && tip != undefined) {
	        cartArray["tipAmount"] = parseFloat(tip).toFixed(2);
	    } else {
	        cartArray["tipAmount"] = 0;
	    }
	    if (posId == '2') {
	        var label;
	        if ($("#pickUp").is(":checked")) {
	            label = "Foodkonnekt Online Pickup";
	            cartArray["label"] = label;
	        } else {
	            label = "Foodkonnekt Online Delivery";
	            cartArray["label"] = label;
	        }
        }
	    var cartJson = JSON.stringify(cartArray)

        
        var orderJson = "";
        if(isCouponApply){
        	orderJson = "{\"cartJson\":" + cartJson + "}#@{\"couponCode\":\"" + voucherCode + "\"}#@{\"kouponCount\":\"" + kouponCount + "\"}#@{\"callingType\":\"first\"}";
        }else{
        	orderJson = "{\"cartJson\":" + cartJson + "}#@{\"couponCode\":\"" + couponC + "\"}#@{\"kouponCount\":\"" + kouponCount + "\"}#@{\"callingType\":\"first\"}";
        }

	    $("#loader").css("display", "block");
	    $("#couponbutton").css("display", "none");
	    $.ajax({
	        url: "checkCouponValidityV2",
	        type: 'POST',
	        dataType: 'json',
	        data: orderJson,
	        contentType: 'application/json',
	        mimeType: 'application/json',
	        success: function(statusValue) {

	            $("#loader").css("display", "none");
	            $("#couponbutton").css("display", "block");
	            if (statusValue.responsCode == '200') {
	            	isCouponApply = true;
	                var voucherResponse = statusValue.DATA;
	                var voucherAllowMultipleKoupon = statusValue.allowMultipleKoupon;


	                itemsForDiscount = JSON.stringify(voucherResponse.items);
	                listOfALLDiscounts = JSON.stringify(voucherResponse.discountList);

	                if (voucherAllowMultipleKoupon == 0) {
	                    $("#couponBox").attr("disabled", true);
	                    $("#couponbutton").attr("disabled", true);
	                }

	                $("#couponError").html("");
	                $(".couponCodeText1").val("");
	                
	                
	                if(voucherResponse.discount > 0){
	                	document.getElementById('discountId').innerHTML = "<td><strong>Discount:</strong></td><td></td><td><strong>$<span id='discount'>"+voucherResponse.discount+"</span></strong></td>";
	                }
	                
	               // document.getElementById('discount').innerHTML = voucherResponse.discount;
	                if (voucherResponse.auxTax > 0 && voucherResponse.auxTax != '' && voucherResponse.auxTax != undefined && voucherResponse.auxTax != null) {
                        document.getElementById('auxTaxRow').innerHTML = "<td><strong>Aux Tax:</strong></td><td></td><td><strong>$<span id='auxTax'>" + parseFloat(voucherResponse.auxTax).toFixed(2) + "</span></strong></td>";
                    } else {
                        document.getElementById('auxTaxRow').innerHTML = "";
                    }
	                
	                discountType = voucherResponse.discountType;

	              //  voucherCode = voucherResponse.voucherCode;
	                voucherCode = couponC;
	                inventoryLevel = voucherResponse.inventoryLevel;

	                itemPoSidsJson = voucherResponse.itemList;

	               
	                total = (voucherResponse.total).toFixed(2);
	                discountedConvenienceFee = (voucherResponse.ConvenienceFeeAfterDiscount).toFixed(2);
	                /* if (tip != 0 && tip != undefined && tip != null) {
	                 	total = (parseFloat(total) + parseFloat(tip)).toFixed(2);
	                 }*/
	                document.getElementById('total').innerHTML = (parseFloat(total)).toFixed(2);
	                
	                if(discountedConvenienceFee!=''&& discountedConvenienceFee!=null && discountedConvenienceFee>0.0)
	                document.getElementById('ConvenienceFee').innerHTML = (parseFloat(discountedConvenienceFee)).toFixed(2);
	                
	                document.getElementById('tax').innerHTML = (parseFloat(voucherResponse.tax)).toFixed(2);
	                kouponCount = voucherResponse.kouponCount;
	                couponDiscount = voucherResponse.discount;
	                $("body").loadingimagethingy("disable");
	            } else if (statusValue.responsCode == '400') {
	                $("#couponError").html(statusValue.responsMessage);
	                $(".couponCodeText1").val("");
	            } else if (statusValue.responsCode == '800') {
	                $("#couponError").html("<div class='col-md-12 error-msg' id='couponError'>Duplicate Coupon</div>");
	                $(".couponCodeText1").val("");
	            } else if (statusValue.responsCode == '300') {
	            	if ($("#pickUp").is(":checked")) {
	            		couponErrorBoxMessage();
	                } else {
	                	$("#delivery").prop('checked', true);
	                	$("#couponError").html("");
	                    isDeliveryKoupon = statusValue.DATA;
	                    // document.getElementById("deliveryFeeFlag").value = isDeliveryKouponoupon;
	                    kouponCount = kouponCount + 1;
	                    voucherCode = couponC;
	                    document.getElementById('total').innerHTML = (parseFloat($("#total").text()) - deliveryFee - deliveryTaxPrice).toFixed(2);
	                    document.getElementById('tax').innerHTML = (parseFloat($("#tax").text()) - deliveryTaxPrice).toFixed(2);
	                    document.getElementById('deliveryFeeRow').innerHTML = "<td><strong>Delivery Fee:</strong></td><td></td><td><strong>$<span id='deliveryFee' deliveryPosId='" + deliveryPosId + "'>0.00</span></strong></td>";
	                    //   itemsForDiscount = '[{"qunatity":1,"totalQunatity":1,"discount":'+deliveryFee+',"discountName":" '+couponC+'","price":0.0,"originalPrice":'+deliveryFee+',"itemModifierPrice":0.0,"itemPosId":"'+deliveryPosId+'"}]';
	                    /*  deliveryFee = 0;
	                      deliveryPosId = null;
	                      deliveryTaxPrice = 0;*/
	                    listOfALLDiscounts = JSON.stringify(statusValue.discountList);
	                    $("body").loadingimagethingy("disable");
	                }
	            	
	            } else {
	                $("#couponError").html("<div class='col-md-12 error-msg' id='couponError'>Invalid coupon code</div>");
	                $(".couponCodeText1").val("");
	            }
	        },
	        error: function() {
	        	$("#couponError").html("<div class='col-md-12 error-msg' id='couponError'>Invalid coupon code</div>");
	            $(".couponCodeText1").val("");
	        }
	    });
    });
});
function couponErrorBoxMessage(){
	$("#couponError").html("<div class='col-md-12 error-msg' id='couponError'>This coupon is applicable only for delivery</div>");
    $(".couponCodeText1").val("");
}

/*---------------------------------------------------------------------------------------*/
function checkDelivery() {
	$("#tipdiv").show();
	tipCheck = true;
	$('input[name=card]').attr('checked',false);
    $("#errorBox1").html("");
	var subTotal = $("#subtotal").val();
    var minDeliveryAmount = $("#minDeliveryAmount").val();
    document.getElementById('test').innerHTML = "$"+minDeliveryAmount;
    if (parseFloat(subTotal) < parseFloat(minDeliveryAmount)) {
        $("#deliveryAmtPopup").modal('show');
        $("#delivery").removeAttr('checked');
    }
    else{
	if (activeTipForDilevery != '' && activeTipForDilevery != undefined && activeTipForDilevery == 'true') {
		tip = (subTotal / 100) * tipsForDilevery
		$('#tipAmt').val(tip.toFixed(2));
		tip = $("#tipAmt").val();

		if (tip > 0 && tip != '') {
			document.getElementById('total').innerHTML = ((parseFloat($("#total").text())
					+ (parseFloat(tip)) - (parseFloat(tipAmt)))).toFixed(2);
		} else {
			document.getElementById('total').innerHTML = ((parseFloat($("#total").text()) - parseFloat(tipAmt))).toFixed(2);
		}
		if (tip == "")
			tipAmt = 0;
		else
			tipAmt = tip;
	}
}
}



navigator.sayswho= (function(){
	  var ua= navigator.userAgent, tem,
	  M= ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
	  if(/trident/i.test(M[1])){
	      tem=  /\brv[ :]+(\d+)/g.exec(ua) || [];
	      return 'IE '+(tem[1] || '');
	  }
	  if(M[1]=== 'Chrome'){
	      tem= ua.match(/\b(OPR|Edge)\/(\d+)/);
	      if(tem!= null) return tem.slice(1).join(' ').replace('OPR', 'Opera');
	  }
	  M= M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
	  if((tem= ua.match(/version\/(\d+)/i))!= null) M.splice(1, 1, tem[1]);
	  return M.join(' ');
	})();
deviceType = (function(){
	var isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
	var element = document.getElementById('text');

	if (isMobile) {	
		return "Phone";
	} else {
	  return "Desktop";
	}

	})
	var str = navigator.sayswho;
	var res = str.split(" ");

//PLACE ORDER V2 
function placeOrderFunction(data1){
    debugger;
    var futureTime = JSON.parse(data1).paymentVO.futureTime;
    var futureDate = JSON.parse(data1).paymentVO.futureDate;

    $.ajax({
        url: "placeOrderV2?merchantId="+merchantId+"&fundCode="+code,
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        data: data1,
        success: function(statusValue) {
            debugger;
            console.log(futureTime);
            if (statusValue == 'APPROVED') {
                if (document.getElementById('guest1').checked) {
                    if (password == "") {
                        $("#createPasswordPopup").modal('show');
                    } else {
                        window.location = "orderV2?futureDate="+futureDate+"&futureTime="+futureTime+"&message=Your order is placed successfully";
                    }
                } else {
                    debugger;
                    window.location = "orderV2?futureDate="+futureDate+"&futureTime="+futureTime+"&message=Your order is placed successfully";
                }

            } else if (statusValue == 'DECLINED') {
                $("#orderMsgSpanId").html("<div class='col-md-12 error-msg' id='orderMsgSpanId'>Your card has been declined. Please retry or use another card</div>");
                $("#minCCDiv").css('display', 'none');
                $('#placeOrderBtn').prop('disabled', false);
                $("#cvv").val(null);
            } else if (statusValue == 'FAILED') {
                window.location = "orderV2?message=We encountered an error processing your order. Please try again";
            } else {
                window.location = "orderV2?message=We encountered an error processing your order. Please try again";
            }
            $("body").loadingimagethingy("disable");
        },
    });
}
/*-------------------------------------Subscribe Loyality--------------------------------*/
function subscribeLoyalty(){

	loyaltyProgramVal = $("#loyaltyProgram").val();
	if($("#loyaltyProgram").is(":checked")){
		loyaltyProgramVal = 1;
		
		/*if(merchantId==356){
			
			var name = $("#guest-name").val();
			var phone = $("#guest-phone").val();
			var email = $("#guest-email").val();
			
    		$.ajax({
                url: "http://client.texnrewards.net/gateway/contactmanager_keyword.asp",
                type: 'POST',
                dataType: 'json',
                data: "{\"user_guid\":\"CC65DC5C-2C33-45E4-AFCF-11A290D4D736\",\"keyword\":\"romas\"," +
                		"\"shortcode\":\"55678\",\"mobile\":\"" + phone + "\",\"firstname\":\"" + name +
                "\",\"lastname\":\"lastname\",\"email\":\"" + email  +"\"}",
                contentType: 'application/json',
                mimeType: 'application/json',
                success: function(data) {
                	alert(data)
                }
        	}); 
    	}*/
	}else{
		loyaltyProgramVal= 0;
	}
}
function showFundBox(customerId)
{
	
	if(virtualFundCount>0){
	$.ajax({
		type : 'GET',
		url : "getCustomerCodes?customerId=" + customerId,
		success : function(fundCode) {
			
			console.log("fundCode : "+fundCode);
			fundCodeBox = "#fundBox";
			if(codeFlag && guestCustomerPassword!='' && virtualFundCount>0)
			$('#fundBox').show();
			
	if(fundCode !=null && fundCode.length>0)
	{
		for (var i = 0, l = fundCode.length; i < l; i++) {
			 var optionNode = document.createElement("option");
			 
			    // Set the value
			    optionNode.value = fundCode[i].fundCode;
			 
			    // create a text node and append it to the option element
			    //optionNode.appendChild(document.createTextNode("$50 USD"));
			 
			    // Add the optionNode to the datalist
			    document.getElementById("customerfundCodes").appendChild(optionNode);
			    
			    //example To add value in Select tag
		        /*$('<option  value="' + fundCode[i].fundCode + '" >'
							+ fundCode[i].fundCode  + '</option>')
					.appendTo('#fundSelect');*/
			  
		}
		var l = fundCode.length-1;
		$("#fundCode").val(fundCode[l].fundCode);
	}
	
		},
	});	
	}
}

function validateFundCode(fundCode)
{

	var valid = false;
	if(!(/^ *$/.test(fundCode))){
	$.ajax({
		type : 'GET',
		async: false,
		url : "checkFundCodeValidity?fundCode=" + fundCode,
		success : function(result) {
			if(result == "valid"){
				valid=true;
				fundCodeValidStatus = "valid";
				$("#fundError").html("");
				$('#placeOrderBtn').prop('disabled', false);
				console.log("validateFundCode true");
			}else if(result == "isMatching"){
				fundCodeValidStatus = "isMatching";
				$("#fundError").html("");
				$('#placeOrderBtn').prop('disabled', false);
			}else{
				$("#fundError")
				.html(
						"<div class='col-md-12 error-msg' id='errorBox'>Please enter valid Fundraiser Code</div>");
				$('#placeOrderBtn').prop('disabled', true);
				console.log("validateFundCode false");
				fundCodeValidStatus = "Invalid";
			}
	
		},
	});
	}else{
		$("#fundError").html("");
		$('#placeOrderBtn').prop('disabled', false);
		console.log("validateFundCode no code");
	}
	return valid;
}
function checkLoyaltyProgram()
{
	$.ajax({
        url: "ajaxForIsInstallFreekwent",
        type: 'GET',
        success: function(data) {
            if(data == "success"){
            	freekwentStatus="success";
            	   $("#subscribe").css('display', 'block');	
            }else{
            	freekwentStatus="failed";
            	$("#subscribe").css('display','none');	
            }
        }
    })
    
  }
