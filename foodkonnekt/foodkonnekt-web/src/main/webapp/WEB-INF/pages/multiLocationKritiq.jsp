<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en" class="no-js">
	<head>
	<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
		<meta charset="UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge"> 
		<meta name="viewport" content="width=device-width, initial-scale=1"> 
		<title>Foodkonnekt Feedback Form</title>
		<meta name="description" content="Fullscreen Form Interface: A distraction-free form concept with fancy animations" />
		<meta name="keywords" content="fullscreen form, css animations, distraction-free, web design" />
		<meta name="author" content="Codrops" />
		<link rel="stylesheet" type="text/css" href="https://www.foodkonnekt.com/web/resources/kritiq/css/normalize.css" />
		<link rel="stylesheet" type="text/css" href="https://www.foodkonnekt.com/web/resources/kritiq/css/demo.css" />
		<link rel="stylesheet" type="text/css" href="https://www.foodkonnekt.com/web/resources/kritiq/css/component.css" />
		<link rel="stylesheet" type="text/css" href="https://www.foodkonnekt.com/web/resources/kritiq/css/cs-select.css" />
		<link rel="stylesheet" type="text/css" href="https://www.foodkonnekt.com/web/resources/kritiq/css/cs-skin-boxes.css" />
		<link rel="stylesheet" type="text/css" href="https://www.foodkonnekt.com/web/resources/kritiq/css/style3.css" />
		<script src="https://www.foodkonnekt.com/web/resources/kritiq/js/modernizr.custom.js"></script>
		
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109427077-1"></script>
		<script>
 	 	window.dataLayer = window.dataLayer || [];
 	 	function gtag(){dataLayer.push(arguments);}
 	 	gtag('js', new Date());

 	 	gtag('config', 'UA-109427077-1');
		</script>
		
	</head>
	<body class="design4 design3 demo">
	             <div class="fs-title text-center">
				 <img src="/${merchantLogo}" onerror="this.src='https://www.foodkonnekt.com/web/resources/kritiq/img/logo.jpg'"  class="boxMeUp"/>
				 </div> 
				 <div id="statrtingDiv">
				 <div class="boxed-block text-center fs-fields orderBox">
				 <div class="orderIdDiv">
				 <label class="fs-field-label fs-anim-upper" for="orderid" style="font-weight: bold;">Choose your location</label><br/><br/>
				 </div>
				 
				 <select  id="selectedId" name="department" style="width: 35%;" onchange="feedbackForm(this)">
                 <option value="" selected>Select location</option>
                 <c:forEach var="locationMap" items="${locationMap}">
                 
        		 <option value="${locationMap.getKey()}">${locationMap.getValue()}</option>
                 </c:forEach>
                 </select><br></br>
                 </div>
                 </div>
                
                 <div id="orderIdDiv" style="display:none">
                 <div class="boxed-block text-center fs-fields orderBox" style="margin-top: -28px;margin-left: auto;">
				 <input type="hidden" id="merchantId" name="merchantId" value="${merchantId}">
				 <input type="hidden" id="merchantPosId" name="merchantPosId" value="${posMerchantId}">
				 <input type="hidden" id="posId" name="posId" value="${posId}"> 
				 
                 <label class="fs-field-label fs-anim-upper" for="orderid" style="font-weight: bold;" >Enter your Order ID</label><br/><br/>
				 <label class="fs-anim-lower" id="errorBox1" style="color: red;"></label>
				 <input type="text" name="orderid" placeholder="Order Id" maxlength="20" id="orderId-new" onclick="cleanBox()">
                
                 <a href="#" class="fs-continue fs-show" disable="disable" onclick="continuePage()">Continue</a>  
                 </div>
                 </div>
                
                 <div class="boxed-block text-center fs-fields orderBox" id="disabledDiv" style="display:none ;margin-top: -25px;">
				 <label class="fs-field-label fs-anim-upper" for="orderid">Feedback form is not enabled. Please contact the restaurant</label>
				 </div>

		<script src="https://www.foodkonnekt.com/web/resources/kritiq/js/jquery-2.1.1.min.js"></script>
		
		<script type="text/javascript">
		var merchantId;

		function continuePage(){
			var orderID= document.getElementById("orderId-new").value;
		    var merchantUid =$("#selectedId").val();
		 
		 window.location='/kritiq/mfeedbackForm?merchantId='+merchantId+'&orderId='+orderID;
		 //window.location='/web/mfeedbackForm?merchantId='+merchantId+'&orderId='+orderID;
		 }
		 
		function feedbackForm(){
	    	var merchantUid =$("#selectedId").val();
	    	if($("#selectedId").prop('selectedIndex')==0){
	    		$("#orderIdDiv").css("display", "none");
	    		$("#disabledDiv").css("display", "none");
	    	}else{
	    	$.ajax({
		        url : "/web/test?merchantUid=" + merchantUid,
		        //url : "/web/test?merchantUid='+merchantUid;"
		        type : 'GET',
		        dataType : 'json',
		        contentType : 'application/json',
		        mimeType : 'application/json',
		        success : function(data) {
		        	var jsonOutpt = JSON.stringify(data);
		        	var response= JSON.parse(jsonOutpt);
		        	merchantId= response.merchantId;
		        	//orderId=response.orderId;
		        	if(response.merchantKritiqExpiryStatus=="enabled"){
		        		$("#statrtingDiv").css("display", "block");
		        		$("#orderIdDiv").css("display", "block");
	        		    $("#disabledDiv").css("display", "none");
		        	}
		        	else if(response.merchantKritiqExpiryStatus=="disabled") {
		        		$("#statrtingDiv").css("display", "block");
		        		$("#orderIdDiv").css("display", "none");
	        		    $("#disabledDiv").css("display", "block");
		        	}
		         },
		      });
	    	}
		}
		function cleanBox(){
			$("#orderId-new").setval("");
		}
		</script>
	
	</body>
</html>
