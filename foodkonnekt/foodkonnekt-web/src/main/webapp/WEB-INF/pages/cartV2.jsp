<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page session="true"%>
<jsp:include page="headerV2.jsp"></jsp:include>
<html>
<head>
<title>FoodKonnekt</title>
<meta charset="utf-8">
 <script>
        history.forward();
 </script>
<script src="https://songbirdstag.cardinalcommerce.com/cardinalcruise/v1/songbird.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">

<script type="text/javascript" src="resources/js/jquery-1.11.3.min.js"></script>
<link rel="stylesheet" type="text/css" href="resources/css/multistep/style-min.css"/>
<link rel="stylesheet" type="text/css" href="resources/css/styles-min.css"/>
<script type="text/javascript" src="resources/js/bootstrap.min.js"></script>
<script src="resources/js/woco.accordion.min.js"></script>
<script src="resources/V2/js/cartV2.js"></script>
<script src="resources/V2/js/fSelect.js"></script>
<script type="text/javascript" src="resources/js/easing.js"></script>
<script type="text/javascript" src="https://www.foodkonnekt.com/web/resources/V2/js/jquery.loadingimagethingy.js"></script>
<script type="text/javascript" src="resources/js/ping-nose-popup/pignose.popup.min.js"></script>
<script>
var cust='${customer}';

	$(function(){
		$('.btn.pg-orange').bind('click', function(event) {
			event.preventDefault();
			$('#pg-orange').pignosePopup({
				theme : 'orange'
			});
		});
	});
</script>

</head>
<body>
    <div class="content">
        <div class="text-center red">
            <span id="orderMsgSpanId">${message}</span>

            <c:if test="${ccMinOrderAmount>0}">
                <div id="minCCDiv" style="margin-left: 3%; font-size: 15px;">Min. order amount for credit card
                    payment:$${ccMinOrderAmount}</div>
            </c:if>
        </div>
        <div class="container-grey container cart" id="cartPage"  style="padding-right:0; padding-bottom:15px;">
            <!-- MultiStep Form -->




                <div class="col-md-6 col-sm-6" style="margin-left:-15px; ">

<!-- mobile_nav ends here-->

                    <form id="msform" class="or">
                        <!-- progressbar -->
                        <ul id="progressbar">
                            <li class="active" id="active1">Time</li>
                            <li id="active2">Order Type</li>
                            <li id="active3">Customer</li>
                            <li id="active4">Payment Info</li>
                        </ul>
                        <!--fieldsets-->
                     <fieldset id="first" style="position:relative!important;">
                            <h3 class="fs-title">Time</h3>
                            <div class="outer-border">
                                         <div class="row">
								
								
                                <label  id="orderNowId" class="radio-inline"> <input type="radio" name="orderNow" id="ordernow"
                                    checked="checked" />Order Now
                                </label>
                           

						
 								<span id="AllowfutureId" style="display: block;"><label class="radio-inline"> <input type="radio" id="orderlater" name="isfuturOrder">Order Later
                                </label></span>
								
        
                    		</div>
						    </div>
                                <span  id="orderLaterError"></span>
                            <div class="ordernow-futureorder-selector" id="futureDateCmbo" style="display: none">
                                <ul>
                                    <li><select id="futureOrderDateCombo" style="width:95%; margin-bottom:15px;">
                                            <option value="select">Select Date  </option>
                                            <%-- <c:forEach items="${futureOrderDates}" var="fDate" varStatus="status">
                                                                                            <option value="${fDate}">${fDate}</option>
                                                                                          </c:forEach> --%>
                                    </select></li>
                                    <li><select id="futureOpeingTime" style="width:95%; margin-bottom:15px;">
                                            <option value="select">Select Time</option>
                                    </select></li>
                                </ul>
                            </div>
                            <input type="button" id="disbleNext" onclick="checkItemList()" name="next"
                                class="next action-button" value="Next" />
                        </fieldset>

 							
                       
                        <fieldset id="second" style="position:relative!important;">
                            <h3 class="fs-title">Order Type</h3>
                           <span  id="errorBox1"></span>
                          
                           
                            <div class="outer-border">

							<c:if test="${sessionScope.merchant.id!=624}">
                                <label class="radio-inline"> <input type="radio" id="pickUp" name="order"
                                    onclick="checkPickUpAmt()" value="pickUp" checked="checked" /> Pickup
                                  </label>
							</c:if>


                                 <label class="radio-inline" id="deliveryzoneStatus" style="display: none;"> <input
                                    type="radio" id="delivery"  onclick="checkDelivery()" name="order" value="delivery" ${sessionScope.merchant.id eq 624?'checked=checked':false}/> Delivery
                                </label>

                            </div>
                            <input type="button" id="pre_btn1" name="previous" class="previous action-button-previous"
                                value="Previous" onClick="prev_step1()" /> <input type="button"
                                onclick="checkOrderTypeStatus()" name="next" class="next action-button" id="nextButton"
                                value="Next" />
                        </fieldset>

                        <fieldset id="guest" style="display: none;">

                            <h3 class="fs-title">Customer</h3>
                               <span id="errorBox12"></span>
                            <div class="outer-border" id="custSection">

                                <label class="radio-inline"> <input type="radio" name="cust" id="returning1"
                                    value="returning1" onclick="returningSignUp()" checked="checked"/>Returning
                                </label> 

                                 <label class="radio-inline"> <input type="radio" name="cust" id="guest1"
                                    value="guest" onclick="guestSignUp()" />Guest
                                </label>

                            </div>
                            <div id="guestCustomerDiv">
                                <div class="returning1 box">
                                          <span  id="loginError"></span>
                                    <div class="form-inline">
                                        <input type="email" name="email" id="signInEmail" placeholder="Email Id"
                                            required="required" />
                                    </div>

                                    <div class="form-inline">
                                        <input type="password" name="pwd" id="signInPassword" placeholder="Password"
                                            required="required" style="background-color: white;"/>
                                    </div>
                                    <input type="button" id="customerLoginId" name="login" class="login submit action-button" value="Login"
                                        onclick="customerLogin()" /> 
                                        <a href="forgotPasswordV2"><input type="button" name="forgot" class="action-button-forgot" value="Forgot Password" /></a>

                                </div>
                            </div>

                            <div id="deliverySelectDiv" style="display: block">
                                <div class="returning1 box" id="pupose">
                                        <span  id="deliveryError"></span>
                                    <select id='returningDeliveryZones' style="width:100%;margin-left:2px; margin-bottom:15px;">
                                        <option selected="selected" value="0">Select Delivery Address</option>
                                        <option value="1">Add New Address</option>
                                        <c:forEach items="${customerAddresses}" var="view" varStatus="status">
                                            <option
                                                value="${view.id}#${view.address1}#${view.address2}#${view.city}#${view.state}#${view.zip}#${view.id}">${view.address1}
                                                ${view.address2}</option>
                                        </c:forEach>
                                    </select>
                                    <div style='display: none;' id="business">

                                        <div class="form-inline">
                                            <input type="text" name="email" id="signInAddress1" placeholder="Address"
                                                required="required" />
                                        </div>
                                        <div class="form-inline">
                                            <input type="text" name="email" id="signInAddress2" placeholder="SUITE/ROOM NO"
                                                required="required" />
                                        </div>
                                        <div class="form-inline">

                                            <select id="signInState" style="width:100%;margin-left:10px; margin-bottom:15px;">
                                                <option default>State</option>
                                                <option value="AL">Alabama</option>
                                                <option value="AK">Alaska</option>
                                                <option value="AZ">Arizona</option>
                                                <option value="AR">Arkansas</option>
                                                <option value="CA">California</option>
                                                <option value="CO">Colorado</option>
                                                <option value="CT">Connecticut</option>
                                                <option value="DE">Delaware</option>
                                                <option value="DC">District Of Columbia</option>
                                                <option value="FL">Florida</option>
                                                <option value="GA">Georgia</option>
                                                <option value="HI">Hawaii</option>
                                                <option value="ID">Idaho</option>
                                                <option value="IL">Illinois</option>
                                                <option value="IN">Indiana</option>
                                                <option value="IA">Iowa</option>
                                                <option value="KS">Kansas</option>
                                                <option value="KY">Kentucky</option>
                                                <option value="LA">Louisiana</option>
                                                <option value="ME">Maine</option>
                                                <option value="MD">Maryland</option>
                                                <option value="MA">Massachusetts</option>
                                                <option value="MI">Michigan</option>
                                                <option value="MN">Minnesota</option>
                                                <option value="MS">Mississippi</option>
                                                <option value="MO">Missouri</option>
                                                <option value="MT">Montana</option>
                                                <option value="NE">Nebraska</option>
                                                <option value="NV">Nevada</option>
                                                <option value="NH">New Hampshire</option>
                                                <option value="NJ">New Jersey</option>
                                                <option value="NM">New Mexico</option>
                                                <option value="NY">New York</option>
                                                <option value="NC">North Carolina</option>
                                                <option value="ND">North Dakota</option>
                                                <option value="OH">Ohio</option>
                                                <option value="OK">Oklahoma</option>
                                                <option value="OR">Oregon</option>
                                                <option value="PA">Pennsylvania</option>
                                                <option value="RI">Rhode Island</option>
                                                <option value="SC">South Carolina</option>
                                                <option value="SD">South Dakota</option>
                                                <option value="TN">Tennessee</option>
                                                <option value="TX">Texas</option>
                                                <option value="UT">Utah</option>
                                                <option value="VT">Vermont</option>
                                                <option value="VA">Virginia</option>
                                                <option value="WA">Washington</option>
                                                <option value="WV">West Virginia</option>
                                                <option value="WI">Wisconsin</option>
                                                <option value="WY">Wyoming</option>
                                            </select>
                                        </div>
                                        <div class="form-inline">
                                            <input type="text" name="city" id="signInCity" placeholder="City"
                                                required="required" />
                                        </div>
                                        <div class="form-inline">
                                            <input type="text"
                                                oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');"
                                                maxlength="5" name="zipcode" id="signInZip" placeholder="Zip Code"
                                                required="required" />
                                        </div>

                                        <input type="button" name="checkForDelivery" class="submit action-button"
                                            value="CheckForDelivery" style="width: 152px;" onclick="checkDeliveryZone()" />
                                    </div>

                                </div>
                            </div>

                            <div class="guest box">
                                    <span  id="errorBox"></span>
                           <div class="form-inline">
                                    <input type="text" id="guest-name" name="cname" placeholder="First name"
                                        required="required" maxlength="20" />
                                </div>
                           <div class="form-inline">
                              <input type="text" id="guest-lastname" name="clastname" placeholder="Last name" required="required" maxlength="20"/>
                           </div>
                                <div class="form-inline">
                                    <input type="email" id="guest-email" name="email" placeholder="Email Id"
                                        required="required" />
                                </div>
                           <div class="form-inline">
                                    <input type="text" id="guest-phone" name="phone" placeholder="Phone Number"
                                        required="required" maxlength="12" />
                                </div>

                           <div class="form-inline" id="address">
                                    <input type="text" name="address" id="guest-address" placeholder="Address"
                                        required="required" />
                                </div>
                           <div class="form-inline" id="address1">
                                    <input type="text" name="address1" id="guest-address1" placeholder="SUITE/ROOM NO"
                                        required="required" />
                                </div>
                                <div class="form-inline" id="state">

                              <select id="guest-state" style="margin-bottom:13px;margin-left: 11px;">
                                    <option default>State</option>
                                    <option value="AL">Alabama</option>
                                    <option value="AK">Alaska</option>
                                    <option value="AZ">Arizona</option>
                                    <option value="AR">Arkansas</option>
                                    <option value="CA">California</option>
                                    <option value="CO">Colorado</option>
                                    <option value="CT">Connecticut</option>
                                    <option value="DE">Delaware</option>
                                    <option value="DC">District Of Columbia</option>
                                    <option value="FL">Florida</option>
                                    <option value="GA">Georgia</option>
                                    <option value="HI">Hawaii</option>
                                    <option value="ID">Idaho</option>
                                    <option value="IL">Illinois</option>
                                    <option value="IN">Indiana</option>
                                    <option value="IA">Iowa</option>
                                    <option value="KS">Kansas</option>
                                    <option value="KY">Kentucky</option>
                                    <option value="LA">Louisiana</option>
                                    <option value="ME">Maine</option>
                                    <option value="MD">Maryland</option>
                                    <option value="MA">Massachusetts</option>
                                    <option value="MI">Michigan</option>
                                    <option value="MN">Minnesota</option>
                                    <option value="MS">Mississippi</option>
                                    <option value="MO">Missouri</option>
                                    <option value="MT">Montana</option>
                                    <option value="NE">Nebraska</option>
                                    <option value="NV">Nevada</option>
                                    <option value="NH">New Hampshire</option>
                                    <option value="NJ">New Jersey</option>
                                    <option value="NM">New Mexico</option>
                                    <option value="NY">New York</option>
                                    <option value="NC">North Carolina</option>
                                    <option value="ND">North Dakota</option>
                                    <option value="OH">Ohio</option>
                                    <option value="OK">Oklahoma</option>
                                    <option value="OR">Oregon</option>
                                    <option value="PA">Pennsylvania</option>
                                    <option value="RI">Rhode Island</option>
                                    <option value="SC">South Carolina</option>
                                    <option value="SD">South Dakota</option>
                                    <option value="TN">Tennessee</option>
                                    <option value="TX">Texas</option>
                                    <option value="UT">Utah</option>
                                    <option value="VT">Vermont</option>
                                    <option value="VA">Virginia</option>
                                    <option value="WA">Washington</option>
                                    <option value="WV">West Virginia</option>
                                    <option value="WI">Wisconsin</option>
                                    <option value="WY">Wyoming</option>
                                    </select>
                                </div>
                           <div class="form-inline" id="city">	
                                    <input type="text" name="city" id="guest-city" placeholder="City"
                                        required="required" />
                                </div>
                          		 <div class="form-inline" id="zipcode">

                                    <input type="text"
                                        oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');"
                                        maxlength="5" name="zipcode" id="guest-zipcode" placeholder="Zip Code"
                                        required="required" />
                                </div>

                                <div class="form-inline">
                                    <input type="password" id="password" name="pwd" placeholder="Password(Optional)" />
                                </div>


                                <input type="button" name="signup" id="customerSignupId" class="signup-button" value="Signup"
                                    onclick="registerCustomer()" />
								
								<%-- <c:if test="${sessionScope.merchant.id==356}"> --%>
								<div> 
								<label class="container1">Subscribe to loyalty program
								<input  type="checkbox" id="loyaltyProgram" value="true" onclick="subscribeLoyalty()">
								<span class="checkmark"></span>
								</label>
								</div>
								<%-- </c:if> --%>
								
                            </div>
                            <!-- guest box ends here-->
                            <input type="button" id="pre_btn3" name="previous" class="previous action-button-previous"
                                value="Previous" onClick="prev_step2()" /> <input type="button" name="next"
                                class="next action-button" value="Next" onclick="checkCustomerStatus()"
                                id="nextButtonCustomer" />
                        </fieldset>
                        <fieldset id="payment">
                            <h3 class="fs-title">Payment information</h3>
                                <span  id="errorBox3"></span>
                                 <span id="agreeMsg"></span>
                            <div class="outer-border">
                                <c:forEach items="${paymentModes}" var="paymentMode" varStatus="paymentStatus">
                                    <label class="radio-inline"> 
                                    <c:if test="${fn:length(paymentModes) eq 1}">
                                    <input id="paymentModesId1" type="radio"
                                        name="card" onclick="getPaymentModeValue('${paymentMode}')"
                                        value="${paymentMode}" checked="checked"/>${paymentMode}
                                        </c:if>
                                        <c:if test="${fn:length(paymentModes) gt 1}">
										     <c:choose>
                                        <c:when test="${paymentMode=='Credit Card'}">
                                    <input id="paymentModesId1" type="radio"
                                        name="card" onclick="getPaymentModeValue('${paymentMode}')"
                                        value="${paymentMode}" />${paymentMode}
                                        </c:when>
                                         <c:otherwise>
                                         <input id="paymentModesId1" type="radio"
                                        name="card" onclick="getPaymentModeValue('${paymentMode}')"
                                        value="${paymentMode}" />${paymentMode}
                                         </c:otherwise>
                                         </c:choose>
                                        </c:if>
                                    </label>
                                </c:forEach>
                            </div>

                            <span id="agreeMsg" style="color: red"></span> <input type="hidden" value="${allowMultiPay}"
                                id="allowMultiPay">

                            <div>
                                <!-- MULTIPAY sTART -->
                                <c:if test="${merchant.allowMultiPay==true}">
                                    <div id="carrdSize" style="display: none">
                                        <select id="carrdSize123">
                                            <!-- <option>Add Your Address</option> -->
                                        </select>
                                    </div>
                                </c:if>
                                <!-- MULTIPAY END -->
                            </div>

                            <div class="returning1 box" id="paymentBox">

                                <div class="form-inline cardAlign">
                                   <select id="cardType" onchange="getCardTypeValue(this);">
                                        <option default selected>Select Card</option>
                                        <option>Master Card</option>
                                        <c:choose>
                                        <c:when test="${merchant.id==558}" >
                                        <option>Discover</option>
                                        </c:when>
                                        <c:otherwise>
                                        
                                        <option>American Express</option>

                                        </c:otherwise>
                                        </c:choose>
                                        <option>Visa</option>
                                    </select>

                                </div>
                                <div class="form-inline" style="padding-top:15px;padding-bottom:15px;">
                                    <input type="text" name="text" 
                                    oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');"
                                    placeholder="Card Number" id="cardNumber"
                                        required="required" /><br> <br>
                                </div>

                                <div class="form-inline">
                                    <select id="month">
                                        <option default selected>Month</option>
                                        <option val="jan">01</option>
                                        <option val="feb">02</option>
                                        <option val="mar">03</option>
                                        <option val="apr">04</option>
                                        <option val="may">05</option>
                                        <option val="jun">06</option>
                                        <option val="jul">07</option>
                                        <option val="aug">08</option>
                                        <option val="sep">09</option>
                                        <option val="oct">10</option>
                                        <option val="nov">11</option>
                                        <option val="dec">12</option>
                                    </select><br> <br>
                                </div>
                                <div class="form-inline">
                                    <select id="year">
                                        <option default>Year</option>
                                        <option val="18">2018</option>
                                        <option val="19">2019</option>
                                        <option val="20">2020</option>
                                        <option val="21">2021</option>
                                        <option val="22">2022</option>
                                        <option val="23">2023</option>
                                        <option val="24">2024</option>
                                        <option val="25">2025</option>
                                        <option val="26">2026</option>
                                        <option val="27">2027</option>
										<option val="28">2028</option>
										<option val="29">2029</option>
										<option val="30">2030</option>

                                    </select>
                                </div>
                                <br>
                                <div class="form-inline">
                                    <input type="password" name="text" 
                                    oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');"
                                    placeholder="CVV" id="cvv" required="required" /><br>
                                    <br>
                                </div>
                                <br>
                                <div class="form-inline">
                                    <input type="text"
                                        oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');"
                                        placeholder="zipCode" id="Card_zipCode" maxlength="5" required="required" /><br>
                                    <br>
                                </div>
                                <c:if test="${merchant.allowMultiPay==true}">
                                    <div>
                                        <input type="checkbox" id="allowMultiPayCheck" checked="checked" /> <font
                                            size="2.5" style="color: black; padding-left: 0; font-weight: 100;">Save
                                            Card Details</font>
                                        <!-- <p style="width: 150px;height: 22px;">Save Card Details</p> -->
                                        <!-- <input type="checkbox" checked="checked" id="allowMultiPayCheck" value="1"><label>Want To Save Your Card</label> -->
                                    </div>

                                </c:if>
                                <c:if test="${merchant.allowMultiPay==false}">
                                    <!--  <input type="hidden" checked="checked" id="allowMultiPayCheck" value="0"><label>Want To Save Your Card</label> -->
                                    <input type="hidden" id="allowMultiPayCheck" />
                                </c:if>

                            </div>

                            <input type="button" name="login" id="placeOrderBtn" class="submit action-button"
                                value="Place Order" onclick="placeOrder()" />

                        </fieldset>
                        <input type="hidden" id="customerId" value="${sessionScope.customer.id}"> <input
                            type="hidden" id="customerPswd" value="${sessionScope.customer.password}">
                    </form>
                    <div class="clear">&nbsp;</div>
                 <div class="clear">&nbsp;</div>
                </div>


                <input type="hidden" id="sessionValue" value="${sessionScope.orderItems}"> <input type="hidden"
                    value="${sessionScope.subTotal}" id="subtotal">
                <div class="col-md-6 col-sm-6 cart-right1">
                    <input type="hidden" value="${ccMinOrderAmount}" id="ccMinOrderAmount">
                     <input type="hidden"  value="${pickUpMinOrderAmount}" id="pickUpMinOrderAmount">
                     <input type="hidden"  value="${minDeliveryAmount}" id="minDeliveryAmount">

                    <table class="table tablehead">
                        <thead>
                            <tr>
                                <th><div class="itm_d">Item</div></th>
                                <th class="col-md-3 col-sm-3" style="text-align:center">Qty</th>
                                <th class="col-md-3 col-sm-3"text-right">Price</th>

                            </tr>
                        </thead>
                        <tbody>

                            <c:forEach items="${sessionScope.orderItems}" var="sessionValue" varStatus="status">
                                <c:if test="${sessionValue.extraCharge == false && sessionValue.isPizza == false}">
                                    <tr id='${sessionValue.itemUid}'>
                                        <td class="cart-items"><strong style="font-weight:bold"> ${sessionValue.itemName} </strong><br /> <c:forEach
                                                items="${sessionValue.modifier}" var="modifiers" varStatus="status">
                                                <span> ${modifiers.name} <br>
                                                </span>
                                            </c:forEach></td>
                                        <td style="font-weight:bold;text-align:center" class="cart-items">${sessionValue.amount}</td> 
                                        <td  class="cart-items"><strong style="font-weight:bold">$<fmt:formatNumber type="currency" pattern="#####.##" minFractionDigits = "2"
                                                    value="${sessionValue.price}" />
                                        </strong><br> <c:forEach items="${sessionValue.modifier}" var="modifiers"
                                                varStatus="status">
                                                <span> $<fmt:formatNumber type="currency" pattern="#####.##" minFractionDigits = "2"
                                                        value="${modifiers.price}" /> <br>
                                                </span>
                                            </c:forEach></td>
                                        <td class="cart-items"><i class="fa fa-times close1" aria-hidden="true"
                                            onclick="removeItem('${sessionValue.itemUid}')"></i></td>
                                    </tr>
                                </c:if>
                                <c:if test="${sessionValue.extraCharge == false && sessionValue.isPizza == true}">
                                    <tr id='${sessionValue.itemUid}'>
                                        <td class="cart-items"><strong style="font-weight:bold"> ${sessionValue.itemName}
                                                (${sessionValue.pizzaSizeName } $${sessionValue.pizzaSizePrice })</strong><br />
                                            <c:forEach items="${sessionValue.modifier}" var="modifiers"
                                                varStatus="status">
                                                <c:if test="${!modifiers.isCrust}">
                                                <span> ${modifiers.name}  <c:if
                                                        test="${modifiers.toppingSide == '1'}">(First Half)</c:if> <c:if
                                                        test="${modifiers.toppingSide == '2'}">(Second Half)</c:if> <c:if
                                                        test="${modifiers.toppingSide == '3'}">(Full)</c:if> <br>
                                                </span>
                                                </c:if>
                                                 <c:if test="${modifiers.isCrust}">
                                                <span> ${modifiers.name}
                                                </span>
                                                </c:if>
                                            </c:forEach></td> 
                                        <td style="font-weight:bold;text-align:center" class="cart-items">${sessionValue.amount}</td>
                                        <td class="cart-items" ><strong style="font-weight:bold">$<fmt:formatNumber type="currency" pattern="#####.##" minFractionDigits = "2"
                                                    value="${sessionValue.price}" /></strong><br><br> <c:forEach
                                                items="${sessionValue.modifier}" var="modifiers" varStatus="status">
                                                <span> $<fmt:formatNumber type="currency" pattern="#####.##" minFractionDigits = "2"
                                                        value="${modifiers.price}" /> <br> <%-- <c:if
                                                        test="${sessionValue.modifier.size() > 1}"></c:if> --%>
                                                </span>
                                            </c:forEach></td>
                                        <td class="cart-items"><i class="fa fa-times close1" aria-hidden="true" 
                                            onclick="removeItem('${sessionValue.itemUid}')"></i></td>
                                    </tr>
                                </c:if>
                            </c:forEach>

                            <tr>
                                <td class="cart-items" style="color:#4e6c88;"><strong>SubTotal:</strong>
                                <td></td>
                                <td class="cart-items" style="color:#4e6c88;"><strong>$<span id="subTotal"></span></strong></td> 
                            </tr>

                            <c:if test="${sessionScope.ConvenienceFee > 0}">
                                <tr>
                                    <td class="cart-items" style="color:#4e6c88;"><strong>Online Fee:</strong></td>
                                    <td></td>
                                    <td class="cart-items" style="color:#4e6c88;"><strong>$<span id="ConvenienceFee"></span></strong></td>
                                </tr>
                            </c:if>

							<tr id="deliveryFeeRow">
                                <!-- <td><strong>Delivery Fee:</strong></td>
                                    <td></td>
                                    <td><strong>$<span id="deliveryFee">0.0</span></strong></td> -->
                            </tr>

                            <tr>
                                <td class="cart-items" style="color:#4e6c88;"><strong>Tax:</strong></td>
                                <td></td>
                                <td class="cart-items" style="color:#4e6c88;"><strong>$<span id="tax">${sessionScope.Tax}</span></strong></td>
                            </tr>

                            <tr id="deliveryFeeRow">
                                <!-- <td><strong>Delivery Fee:</strong></td>
                                    <td></td>
                                    <td><strong>$<span id="deliveryFee">0.0</span></strong></td> -->
                            </tr>
                            <tr id="auxTaxRow">

                            </tr>

 							<%-- <c:if test="${sessionScope.discount>0}"> --%>
                            <tr id="discountId">
                                <!-- <td><strong>Discount:</strong></td>
                                <td></td>
                                <td><strong>$<span id="discount">0.0</span></strong></td> -->

                            </tr>
                            
                            <%-- </c:if> --%>

                            <tr id="tipdiv">
                                <td style="padding-top:20px;color:#4e6c88!important;" class="cart-items"> <strong>Tip: </strong></td> 
                                <td></td>
                                <td class="cart-items"><span id="tip" class="form-inline" style="color:#4e6c88;"><input type="text" class="tip" id="tipAmt" min="0"
                                        maxlength="5" placeholder="$0.00"></span></td>

                            </tr>

						<tr id="fundBox">
							<span id="newFundError"></span>
							<td class="form-inline" style="padding-top: 15px;"><input
								list="customerfundCodes" placeholder="Fundraiser Code" id="fundCode"
								class="tip tip1 couponCodeText1 couponCodeBox1" type="text" onblur="validateFundCode(this.value)">

								<datalist id="customerfundCodes"></datalist>
						</tr>

						<tr id="couponBox">
                                <span id="couponError"></span><br>
                                <span id="fundError"></span>
                                <td class="form-inline" style="padding-top:15px;" ><input class="tip tip1 couponCodeText1 couponCodeBox1"  type="text" placeholder="Coupon Code"
                                    id="coupon"></td>
                                <td></td>
                                
                                <td class="cart-items">
                                   <div class="pj-loader-4"style="display:none" id="loader">
                                	<img src="resources/img/load2.gif" style="width: 35px;margin-left: 19px;margin-top: auto;">
                          			</div>
                                    <button class="submit_btn submit action-button couponButton" type="button"
                                        id="couponbutton">Apply</button>
                                </td>
                            </tr>

			            <tr style="color: #4e6c88; font-weight:bold!important; font-size:16px;"> 

                               
                                <td class="cart-items"><div class="ct_total"  style="font-weight:bold!important; font-size:18px;""><strong>Total:</strong></div></td>
                                <td></td>
                                <td class="cart-items" style="font-weight:bold!important;"><div class="Ct_total"  style="font-weight:900!important; font-size:18px;"><strong>$<span id="total"></span></strong></div></td>
                            </tr>

                        </tbody>
                    </table>
                    <br>
                    <!-- <input class="extra" type="textarea" placeholder="Special instructions" id="special-instructions"> -->
                    <textarea class="extra" id="special-instructions" placeholder="Special instructions  like no onion."
                        maxlength="140" style="resize: none;"></textarea>
 <div class="clear">&nbsp;</div>
                </div>
                <div class="clear">&nbsp;</div>
                 <div class="clear">&nbsp;</div>
            </div>
          
        </div>
    </div>

    <div class="container container-size container-whitefooter">
        <div class="grey_grad"></div>
        
        
        <!-- <div class="col-md-5 col-sm-6 col-md-offset-1">
            <br> By placing this order you expressly agree and accept the <a
                href="https://www.foodkonnekt.com/end-user-agreement/" target="_blank">restaurant terms & ordering
                system provider EULA </a> including data processing terms.<br>
        </div> -->
        
        
        <c:choose>
		
		 <c:when test="${sessionScope.merchant.id==376}">
		  <div class="col-md-12 col-sm-12" style="text-align: center; padding-bottom:20px;">
            <br> By placing this order you expressly agree and accept the <a
                href="https://www.texnrewards.net/" target="_blank">restaurant terms & ordering
                system provider EULA </a> including data processing terms.<br>
       		 </div>
		 </c:when>
		 
		 <c:otherwise>
		   <div class="col-md-12 col-sm-12" style="text-align: center; padding-bottom:20px;">
            <br> By placing this order you expressly agree and accept the <a
                href="https://www.foodkonnekt.com/end-user-agreement/" target="_blank">restaurant terms & ordering
                system provider EULA </a> including data processing terms.<br>
        </div>
		 </c:otherwise>
		 
		 </c:choose> 
    </div>
    <!-- --------------------------------PopUp---------------------------------------------------------------- -->

    <div id="myModal12" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <button type="button" class="btn bttn" data-dismiss="modal" style="width:40px;">X</button>
                <div class="modal-header">
                   &nbsp;
                </div>
                <div class="modal-body" style="text-align:center">
                <br/>
<img src="resources/V2/images/logo.png" alt="">
                 <br/><br/><br/>
                    <strong> Your order amount is less than the minimum pickUp amount of
                        $${pickUpMinOrderAmount}<br> <br>
                    </strong><br>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn" data-dismiss="modal">close</button>
                </div>
            </div>
        </div>
    </div>

    <div id="deliveryAmtPopup" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <button type="button" class="btn bttn" data-dismiss="modal">X</button>
                <div class="modal-header">
                    <img src="resources/V2/images/logo.png" alt="">
                </div>
                <div class="modal-body" >
                    <strong> <center>Your order amount is less than the minimum delivery amount 
                    
                    <span id ="test"> </span></center><br> <br>
                    </strong><br>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn" data-dismiss="modal">close</button>
                </div>
            </div>
        </div>
    </div>

    <div id="myModal122" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content" style="padding:15px">
                <button type="button" class="btn" data-dismiss="modal" style="width:40px; float:right;">X</button> 
                <div class="modal-header" style="text-align:center;">
                    <img src="resources/V2/images/logo.png" alt="">
                     <br>
                </div>
               
                <div class="modal-body" style="text-align:center; margin-top:15px;">
<br>
                    <p style="color:#333; font-size:16px;">                   
                        Your address is not within delivery zone. Please input a new address or select pick up option<br>
                        <br>
                    </p>
                </div>
                <div class="modal-footer" style="text-align:center;">
                    <button type="button" class="btn" data-dismiss="modal">close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="pg-orange" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <button type="button" class="btn" data-dismiss="modal" style="width:40px; float:right;">X</button>
                <div class="modal-header">
                   &nbsp;
                </div>
               <div class="modal-body" style="text-align:center; margin-top:15px;">
               <br/>
                <img src="resources/V2/images/logo.png" alt="">
                <br><br>
                    <p style="color:#333; font-size:16px;">  
                        Your address is not within delivery zone. Please input a new address or select pick up option<br>
                        <br>
                    </p>
                    <br><br>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn" data-dismiss="modal">Ok</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="exampleModal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <button type="button" class="btn bttn" data-dismiss="modal" style="width:40px;">X</button>
                <div class="modal-header">
                   &nbsp;
                </div>
                 <div class="modal-body" style="text-align:center;">
                 <br/>
                 <img src="resources/V2/images/logo.png" alt="">
                  <br/><br/><br/>
                    <p>
                        Your order amount is less than the minimum credit card amount of $${ccMinOrderAmount}<br>
                    </p>
                    <br> <br>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="checkItem" role="dialog">
        <div class="modal-dialog">

            <div class="modal-content">
                <button type="button" class="btn bttn" data-dismiss="modal" style="width:40px; float:right;">X</button>
                <div class="modal-header">
                </div>
                <div class="modal-body" style="text-align:center;">
                <br> 
                <img src="resources/V2/images/logo.png" alt="">
		<br/><br/>
		<p style="color:#333; font-size:16px;text-align:center;">Please select at least one item</p>
		    <br></div>
                
                <div class="modal-footer" style="padding:5px; text-align:center;">
                    <a class="btn" style="background: #d4d4d4; color:#fff;" href="orderV2?merchantId=${sessionScope.merchant.id}"
                        role="button">Close</a>
                </div>
            </div>
        </div>
    </div>

    <div id="createPasswordPopup" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <button type="button" class="btn bttn" data-dismiss="modal" style="width:20px;">X</button>
                <div class="modal-header" style="text-align:center;"><br/>

                    <img src="resources/V2/images/logo.png" alt="">
                </div>
                <div class="modal-body">

                    <p style="text-align:center;">
                        Do you want to create an account so that you can keep track of the order?<br> <br>
                    </p>
                </div>
                <div class="modal-footer">
                    <br>
                    <button type="button" class="btn yes-btn" data-dismiss="modal" onclick="addPasswordGestUser('YES')">Yes</button>

                    <button type="button" class="btn no-btn" data-dismiss="modal" onclick="addPasswordGestUser('No')">No</button>
                </div>
            </div>
        </div>
    </div>
    
    
	<jsp:include page="footerV2.jsp"></jsp:include>
	
    <script src="resources/js/easing.js"></script>
    <script src="resources/V2/js/msform.js"></script>
    <link rel="stylesheet" type="text/css" href="resources/css/jquery.loadingimagethingy.css" />
    <script type="text/javascript" src="resources/V2/js/jquery.loadingimagethingy.js"></script>
    
    <script>
var mid="${sessionScope.merchant.id}";

/*  if(mid==281){
var my_awesome_script = document.createElement('script');
my_awesome_script.setAttribute('type','text/javascript')

my_awesome_script.setAttribute('src','http://client.texnrewards.net/Form_Wizard/form_generator.aspx?form_guid=27265795-A553-E811-8120-F04DA240EFBE');

document.head.appendChild(my_awesome_script);
}  */

</script>
    <script>
var merchantId="${sessionScope.merchant.id}";
var orderItemsLength="${sessionScope.orderItems}";
var posId="${sessionScope.merchant.owner.pos.posId}";
var allowMultiPay= "${sessionScope.merchant.allowMultiPay}";
var customerSessionValue = "${sessionScope.customer.id}";
var allowFutureOrder = '${sessionScope.merchant.allowFutureOrder}';
var cardInfo='${sessionScope.cardInfo}';
var guestCustomerSessionValue = "${sessionScope.guestCustomer.emailId}";
var ConvenienceFee= parseFloat(${sessionScope.ConvenienceFee}).toFixed(2);
var activeTipsForPickup="${sessionScope.merchant.activeTipsForPickup}";
var tipsForPickup="${sessionScope.merchant.tipsForPickup}";
var activeTipForDilevery="${sessionScope.merchant.activeTipForDilevery}";
var tipsForDilevery="${sessionScope.merchant.tipsForDilevery}";
var minDeliveryAmountTotal = "${minDeliveryAmount}";
var delievryZoneStatus = "${zoneStatus}";
var Payeezy3DsModes="${Payeezy3DsModes}";
var orderSubTotal="${sessionScope.subTotal}";
var webBaseUrl = "${webBaseUrl}";
var adminBaseUrl = "${adminBaseUrl}";
var paymentmode="${paymentModes}";
var orderSubTotal="${sessionScope.subTotal}";
var virtualFundCount = "${virtualFundCount}"
if(ConvenienceFee > 0){
document.getElementById("ConvenienceFee").innerHTML = ConvenienceFee;
}

document.getElementById("subTotal").innerHTML = parseFloat(${sessionScope.subTotal}).toFixed(2);


var total= parseFloat(${sessionScope.totalPrice}).toFixed(2);
document.getElementById("total").innerHTML = total;
${sessionScope.ConvenienceFee}
var guestCustomerPassword = "${sessionScope.guestCustomer.password}";
var guestCustomerEmailId = "${sessionScope.guestCustomer.emailId}";
var guestSignUpFlag=0;

if(customerSessionValue == '' || customerSessionValue == null){
	customerSessionValue = guestCustomerSessionValue;
}
customerSessionValues(customerSessionValue);
allowFutureOrders(allowFutureOrder);
if(posId==1){
    $("#Card_zipCode").css('display', 'none');
}

function checkGuestCustomerOnCart(){
	if((guestCustomerEmailId!='' && guestCustomerPassword=='') || guestSignUpFlag==1){
			/* $("#createPasswordPopup1").modal('show'); */
		}
}
checkGuestCustomerOnCart();
</script>

<script type="text/javascript">
function addPasswordGestUser1(data) {
    if (data == 'YES') {
    	guestSignUpFlag=0;
    	window.location.href = "setGuestPasswordV2";
    } else {
    	guestSignUpFlag=0;
        window.location.href = "logoutV2";
    }
}
</script>

<script type="text/javascript">
if ( $(window).width() < 600) {
$('input[type="number"]').on({
focus: function () {


$('.header').hide();

},
blur: function () {
$('.header').show();
}
});
$('input[type="email"]').on({
focus: function () {


$('.header').hide();

},
blur: function () {
$('.header').show();
}
});
$('input[type="password"]').on({
focus: function () {


$('.header').hide();

},
blur: function () {
$('.header').show();
}
});
$('textarea').on({
focus: function () {


$('.header').hide();

},
blur: function () {
$('.header').show();
}
});
$('input[type="text"]').on({
focus: function () {


$('.header').hide();

},
blur: function () {
$('.header').show();
}
});
}
</script>

<script>
$(".next").on("click", function(e){
e.preventDefault();
$('html, body').animate({scrollTop: 0}, 'slow');
});
</script>
<script>
$(".previous").on("click", function(e){
e.preventDefault();
$('html, body').animate({scrollTop: 0}, 'slow');
});
</script>

<script>
$("#customerLoginId").on("click", function(e){
e.preventDefault();
$('html, body').animate({scrollTop: 0}, 'slow');
});
</script>

<script>
$("#customerSignupId ").on("click", function(e){
e.preventDefault();
$('html, body').animate({scrollTop: 0}, 'slow');
});
</script>

<script>
$("#placeOrderBtn").on("click", function(e){
e.preventDefault();
$('html, body').animate({scrollTop: 0}, 'slow');
});
</script>

<script type="text/javascript">
$(document).ready(function(){
    $(this).scrollTop(0);
    $("#couponBox").hide();
    $("#fundBox").hide();
    $("#tipdiv").hide();
	if (jQuery('.cart').attr("id") == "cartPage") {
        $('#btn3').addClass('current');
    }
});
</script>


<script>
$(".submit .action-button").on("click", function(e){
e.preventDefault();
$('html, body').animate({scrollTop: 0}, 'slow');
});
</script>


<script>
$(".submit").on("click", function(e){
e.preventDefault();
$('html, body').animate({scrollTop: 0}, 'slow');
});  

$("#fundCode").on('input', function () {
	 var code = this.value;
	 //validateFundCode(code);
	 
   //example for checking from the option in datalist
   /*  if($('#customerfundCodes option').filter(function(){
        return this.value.toUpperCase() === val.toUpperCase();        
    }).length) {
    	$("#fundError").html("");
    } */
});
</script>

</body>
</html>
 