<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<jsp:useBean id="now" class="java.util.Date" />

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<script src="resources/js/shop/accordion/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="resources/orderReceipt/assets/css/styles.css">
</head>
<body>
    <div class="container">
        <div class="row">
            
            <c:if test="${order.getMerchant().getId()==356}">
       		 <div class="logo text-center">
              <img src="https://www.foodkonnekt.com/foodkonnekt_merchat_logos/356_RomasItalianRestaurant.png" alt="">

            </div>
        
       	   </c:if>
        
       		 <c:if test="${order.getMerchant().getId()!=356}">
       		<div class="logo text-center">
                
                <c:if test="${order.merchant.merchantLogo == null}">
                  <img src="resources/orderReceipt/assets/images/logo.png" alt="">
                </c:if>
                
                 <c:if test="${order.merchant.merchantLogo != null}">
                 	<img src="https://www.foodkonnekt.com/${order.merchant.merchantLogo}">
                 </c:if>
            </div>
        
       		 </c:if>
            
            
            <!-- logo ends here-->
            <div class="orange-bar"></div>
        </div>
        <!-- row ends here-->
        <div class="row content">
            <div class="col-md-12 col-sm-12">
                <div class="col-md-9 col-xs-6
                ">Order ID: ${order.id}<c:if  test="${order.orderPosId !='' && order.getMerchant().getOwner().getPos().getPosId() != 3}" >
                 (PosID:${order.orderPosId})
                 </c:if>
                 </div>
                <div class="col-md-3 col-xs-6
                 text-right tr">
                    Date:
                    <fmt:formatDate pattern="MM/dd/yyyy" value="${order.createdOn}" />
                </div>
            </div>
        </div>
        <!--content ends here-->
        <div class="orange-line"></div>
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <div class="text-center">
                    <h2>Order type</h2>
                    <h4 class="order_type" style="text-transform: uppercase;">${order.orderType}</h4>
                </div>
            </div>
            <div class="col-md-6 col-sm-6">
                <div class="text-center">
                    <h2>Payment Method</h2>

                    <c:if test="${order.paymentMethod== 'Credit Card'}">
                        <h4 class="cc">${order.paymentMethod}</h4>
                    </c:if>

                    <c:if test="${order.paymentMethod== 'Cash'}">
                        <h4 class="cash">${order.paymentMethod}</h4>
                    </c:if>
                </div>
            </div>
        </div>
        <div class="banner">
            <div class="row">
                <div class="col-md-5 col-sm-5 billing_addr">
                    <h4>
                        <strong>Customer Info</strong>
                    </h4>
                    <div style="color: #000;">${order.customer.firstName} ${order.customer.lastName}</div>
                    <div style="color: #000;">${order.customer.emailId}</div>
                    <div style="color: #000;">
                        <a href="#">${order.customer.phoneNumber}</a>
                    </div>
                </div>
                <div class="col-md-2 col-sm-2"></div>
                <div class="col-md-5 col-sm-5 billing_addr">
                    <c:if test="${order.orderType== 'delivery' || order.orderType== 'Delivery'}">
                        <h4>
                            <strong>Delivery Address</strong>
                        </h4>
                        <p>

                           <c:if test="${order.addressId ==null}">
						<c:forEach var="address" items="${order.customer.addresses}"
							end="0">
                    ${order.address.address1} <br> ${order.address.address2}<br>
				 ${order.address.city },${order.address.state}, ${ order.address.zip}
						</c:forEach>
						</c:if>
						
						<c:if test="${order.addressId !=null}">
					${order.address.address1} <br> ${order.address.address2}<br>
				 ${order.address.city }, ${order.address.state}, ${order.address.zip}
						</c:if>
                            <br>
                        </p>
                    </c:if>

                </div>
            </div>
        </div>
        <h2>order description</h2>

        <table class="table tablehead1">
            <thead>
                <tr>
                    <th class="col-md-3 col-sm-3 text-left">Item</th>
                    <th class="col-md-3 col-sm-3 text-center">Price</th>
                    <th class="col-md-3 col-sm-3 text-center">Qty</th>
                    <th class="col-md-3 col-sm-3 text-right">Total</th>
                </tr>
            </thead>
            <tbody>
                ${orderdetails }
                <tr class="bottom">
                    <td class="text-left lft">Sub total</td>
                    <td></td>
                    <td></td>
                    <td class="text-right rht">$<fmt:formatNumber type="currency" pattern="#####.##" minFractionDigits = "2"
                            value="${order.subTotal}" /></td>
                </tr>
                
                <tr class="bottom">
                    <span style="display: none"><fmt:formatNumber type="currency" currencySymbol="$"
                            value="${order.convenienceFee}" /></span>
                    <c:if test="${order.convenienceFee>0.0}">
                        <td class="text-left lft">Online Fee</td>


                        <td></td>
                        <td></td>
                        <c:if test="${order.convenienceFee!='' && order.convenienceFee!=null}">
                            <td class="text-right rht">$<fmt:formatNumber type="currency" pattern="#####.##" minFractionDigits = "2"
                            value="${order.convenienceFee}" /></td>
                        </c:if>

                    </c:if>
                </tr>
                
                <c:if test="${order.orderType=='delivery' || order.orderType=='Delivery' && order.deliveryFee>0}">
                    <tr class="bottom">
                        <td class="text-left lft">Delivery Fee</td>
                        <td></td>
                        <td></td>
                        <td class="text-right rht">$<fmt:formatNumber type="currency" pattern="#####.##" minFractionDigits = "2"
                            value="${order.deliveryFee}" /></td>
                    </tr>
                </c:if>
                
                <c:if test="${order.orderDiscount>0}">
                <tr class="bottom">
                    <td class="text-left lft st">Discount</td>
                    <td></td>
                    <td></td>
                    <td class="text-right rht st">$<fmt:formatNumber type="currency" pattern="#####.##" minFractionDigits = "2"
                            value="${order.orderDiscount}" /></td>
                </tr></c:if>

                <c:if test="${! empty order.orderDiscountsList}">
                    <tr class="bottom">
                        <td class="text-left lft">Discount Coupon</td>
                        <td></td>
                        <td></td>
                        <c:forEach items="${order.orderDiscountsList}" var="orderDiscount" varStatus="status1">
                            <td class="text-right rht">${orderDiscount.couponCode}</td>
                        </c:forEach>

                    </tr>
                </c:if>
                
                
                <c:if test="${order.tax > 0.0}">
                <tr class="bottom">
                    <td class="text-left lft">Tax</td>
                    <td></td>
                    <td></td>
                    <td class="text-right rht">$<fmt:formatNumber type="currency" pattern="#####.##" minFractionDigits = "2"
                            value="${order.tax}" /></td>
                </tr>
                </c:if>
                      
                 <c:if test="${order.tipAmount > 0.0}">           
                 <tr class="bottom">
                    <td class="text-left lft">Tip</td>
                    <td></td>
                    <td></td>
                    <td class="text-right rht">$<fmt:formatNumber type="currency" pattern="#####.##" minFractionDigits = "2"
                            value="${order.tipAmount}" /></td>
                </tr>
                </c:if>
                        

                <tr class="bottom">
                    <td class="text-left lft">Total</td>
                    <td></td>
                    <td></td>
                    <td class="text-right rht">$<fmt:formatNumber type="currency" pattern="#####.##" minFractionDigits = "2"
                            value="${order.orderPrice}" /></td>
                </tr>
            </tbody>
        </table>

        <input type="hidden" value="${order.fulfilled_on}" id="fulfilled_on"> <input type="hidden"
            value="${order.isFutureOrder}" id="futureOrder">
              <input type="hidden" value="${status}" id="status"> <input type="hidden"
            value="${order.isDefaults}" id="isdefault">
          


        <div id="LoadingImage2" style="display: none" align="middle">
            <img src="resources/img/now-loading.gif" align="middle" />
        </div>

        <c:if test="${order.isDefaults == 0 && status}">
            <div class="row  bttn">
                <div class="col-md-3 col-sm-3 col-xs-6">
                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal1"
                        onclick="popupOpen('Accept')">ACCEPT</button>
                </div>
                <div class="col-md-2"></div>
                <div class=" col-md-3 col-sm-3 col-xs-6">
                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModal1" 
                    	onclick="popupOpen('Decline')">DECLINE</button>
                    
                </div>
            </div>
        </c:if>

        <c:if test="${order.isDefaults == 1&& status}">
            <div class="row  bttn">
             <div class="col-md-2"></div>
              <div class=" col-md-3 col-sm-3 col-xs-6">
                    <button type="button" class="btn btn-success1" onclick="popupOpen('updateOrderTime')">UPDATE TIME</button>
                </div>
                <!-- <div class=" col-md-3 col-sm-3 col-xs-6">
                    <button type="button" class="btn btn-danger" onclick="popupOpen('Decline')">DECLINE</button>
                </div> -->
            </div>
        </c:if>
        <c:if test="${order.isDefaults == 5}">
            <div class="row  bttn">
             <div class="col-md-2"></div>
              <div class=" col-md-3 col-sm-3 col-xs-6" align="center">
           <p>  Order Canceled/Refunded</p>
                </div>
               
            </div>
        </c:if>
        
        
          <c:if test="${order.isDefaults == 6}">
            <div class="row  bttn">
             <div class="col-md-2"></div>
              <div class=" col-md-3 col-sm-3 col-xs-6" align="center">
           <p>  Order Canceled</p>
                </div>
               
            </div>
        </c:if>
        

        <c:if test="${order.isDefaults == 2||order.isDefaults == 0&& status == false}">
            <div class="row  bttn">
                <h4 class="cash">
                    <strong style="font-size: 20px; font-weight: inherit;">ORDER DECLINED</strong>
                    
                </h4>
            </div>
        </c:if>
        
       <!-- >>>>>>>>>>>>>>>ORDER ACCEPTED<<<<<<<<<<<<<< --> 
        <c:if test="${order.isDefaults == 1&&status == false}">
            <div class="row  bttn">
                <h4 class="cash">
                    <strong style="font-size: 20px; font-weight: inherit;">ORDER ACCEPTED</strong>
                </h4>
            </div>
        </c:if>
        
		<!-- >>>>>>>>>>>>>>>>>>>>>>>>>>>END<<<<<<<<<<<<<<<<<<<<<<<<< -->
        <c:if test="${order.isDefaults == 3}">
            <div class="row  bttn">
                <h4 class="cash">
                    <strong style="font-size: 22px; font-weight: inherit;">ORDER FAILED</strong>
                </h4>
            </div>
        </c:if>

        <%-- <c:if test="${order.isDefaults == 4}">
            <div class="row  bttn">
                <div class="col-md-3 col-sm-3 col-xs-6">
                    <button type="button" id="acceptBtn" class="btn btn-success" data-toggle="modal"
                        data-target="#myModal1" onclick="popupOpen('Accept')">ACCEPT</button>
                </div>
                <div class="col-md-2"></div>
                <div class=" col-md-3 col-sm-3 col-xs-6">
                    <button type="button" class="btn btn-danger " onclick="popupOpen('Decline')">DECLINE</button>
                </div>
            </div>
        </c:if> --%>


        <div class="row">
            <div class="copyrights"></div>
        </div>

    </div>
    <!-- container ends here -->

    <!----------------------------------------------------- Modals -------------------------------------------------->


    <div id="myModal1" class="modal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="text-center">ORDER</h3>
                </div>

                <div class="modal-body" id="acceptBody">
                    <label id="errorBox" class="error" style="color: red; font-weight: normal; font-size: 17px;"></label>

                    <p style="text-transform: capitalize;">
                        Change ${order.orderType} time<br>
                        <c:if test="${order.isFutureOrder==1}">
                            <input type="number" id="changeTime" onchange='changeTime(this.value)' name="ccNumber" max="999" min="0"
                                value="${order.orderAvgTime}" style="width: 242px;"
                                oninput="validity.valid||(value='');">
                            <br>

                        </c:if>

                        <c:if test="${order.isFutureOrder==0}">
                            <input type="number" id="changeTime" onchange='changeTime(this.value)' name="ccNumber" max="999" min="0"
                                value="${order.orderAvgTime}" oninput="validity.valid||(value='');">
                            <br>
                        </c:if>

                        <input type="hidden" id="orderId" name="orderId" value="${order.id}">
                         <input type="hidden" id="orderPosId" name="orderPosId" value="${order.orderPosId}">
                    </p>
                </div>
                <div id="LoadingImage" style="display: none;" align="middle">
                    <img src="resources/img/now-loading.gif" align="middle" />
                </div>

                <div class="modal-footer">

                    <div class=" col-md-offset-3 col-md-2 col-xs-5">
                        <button type="button" class="btn btn-primary" id="acceptLink" onclick="acceptOrder('Accept')">Continue</button>
                    </div>
                    <div class="col-md-3 col-xs-5">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="popupClose()">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div id="myModal2" class="modal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="text-center">ORDER</h3>
                </div>
                <div class="modal-body" id="declineBody">


                    <h4 class="text-center">
                        Reasons to decline<br>
                    </h4>
                    <fieldset>
                        <div class="checkbox">
                            <label for="c1"><input type="checkbox" name="reason[]" id="c1"
                                value="Kitchen is out of stock"> Kitchen is out of stock</label>
                        </div>
                        <div class="checkbox">
                            <label for="c2"><input type="checkbox" name="reason[]" id="c2"
                                value="Kitchen hours are not open"> Kitchen hours are not open</label>
                        </div>

                        <div class="checkbox">
                            <label for="c3"><input type="checkbox" name="reason[]" id="c3"
                                value="We are not delivering today"> We are not delivering today</label>
                        </div>
                        <div class="checkbox">
                            <label for="c4"><input type="checkbox" name="other" id="other"
                                onclick="showOtherTextArea()" name="colorCheckbox" value="red"> Others</label>
                        </div>
                        <div class="red box">
                            <input type="text" id="otherTextBox">
                        </div>
                    </fieldset>
                </div>
                <div id="LoadingImage1" style="display: none" align="middle">
                    <img src="resources/img/now-loading.gif" align="middle" />
                </div>
                <div class="modal-footer">
                    <div class=" col-md-offset-3 col-md-2 col-xs-5">
                        <button type="button" class="btn btn-primary" id="acceptLink1" onclick="acceptOrder('Decline')">Continue</button>
                    </div>
                    <div class="col-md-3 col-xs-5">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="popupClose()">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </div>


	<div id="myModal3" class="modal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="text-center">
                    	<img src="resources/V2/images/logo.png" >
                    </h3>
                </div>

                <div class="modal-body" id="acceptBody">
                    <label id="errorBox" class="error" style="color: red; font-weight: normal; font-size: 17px;"></label>

                    <p style="text-transform: capitalize;">
                        Change ${order.orderType} time<br>
                        <c:if test="${order.isFutureOrder==1}">
                            <input type="number" id="changeTime" onchange='changeTime(this.value)' name="ccNumber" max="999" min="0"
                                value="${order.orderAvgTime}" style="width: 242px;"
                                oninput="validity.valid||(value='');">
                            <br>

                        </c:if>

                        <c:if test="${order.isFutureOrder==0}">
                            <input type="number" id="changeTime" name="ccNumber" onchange='changeTime(this.value)' max="999" min="0"
                                value="${order.orderAvgTime}" oninput="validity.valid||(value='');">
                            <br>
                        </c:if>

                        <input type="hidden" id="orderId" name="orderId" value="${order.id}">
                    </p>
                </div>
                <div id="LoadingImage" style="display: none;" align="middle">
                    <img src="resources/img/now-loading.gif" align="middle" />
                </div>

                <div class="modal-footer">

                    <div class=" col-md-offset-3 col-md-2 col-xs-5">
                        <button type="button" class="btn btn-primary" id="acceptLink" onclick="acceptOrder('updateOrderTime')">Update</button>
                    </div>
                    <div class="col-md-3 col-xs-5">
                        <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="popupClose()">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </div>


    </div>
    <!-- container ends here -->


    <div class="modal fade" id="futureorderStatus" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

        <div class="modal-dialog" role="document">
            <div class="modal-content">

                <div class="modal-header">
                
                <c:if test="${order.getMerchant().getId()==356}">
                	<img src="resources/orderReceipt/assets/images/274_PiePizzaria.png" alt="">
                </c:if>
                    
                <c:if test="${order.getMerchant().getId()!=356}">
                	<img src="resources/orderReceipt/assets/images/logo.png" alt="">
                </c:if>

                    <button type="button" class="close" data-dismiss="modal" onclick="modelClose()" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <h4 class="modal-title">
                        <strong>Your Order Status</strong>
                    </h4>
                    <br>
                    <h3 bind="greeting">wait.......</h3>

                </div>
                <div class="modal-footer">
                    <!--  <button type="button" class="btn btn-secondary" onclick="modelClose()" data-dismiss="modal">Close</button> -->

                    <div class="row bttn">
                        <br>
                        <button type="button" style="background-color: #f7941d;" class="btn" data-dismiss="modal"
                            onclick="modelClose()">close</button>
                    </div>

                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>


    <script type="text/javascript">
					var paymentMethod = '${order.paymentMethod}';
					var isDefaults = '${order.isDefaults}';
					var isFutureOrder = '${order.isFutureOrder}';
					//$("#futureorderStatus").modal('show');
				</script>

    <script type="text/javascript">
    var posId="${order.getMerchant().getOwner().getPos().getPosId()}";
					function popupOpen(type) {
						orderAvgChangeTime=-1;
						if (type == 'Accept') {
							$("#myModal1").css("display", "block");
						} else if (type == 'Agree') {
							$("#myModal1").css("display", "none");
							$("#myModal2").css("display", "none");
						} else if(type == 'updateOrderTime'){
							$("#myModal1").css("display", "none");
							$("#myModal2").css("display", "none");
							$("#myModal3").css("display", "block");
						}else {
							$("#myModal2").css("display", "block");
						}
					}

					// When the user clicks anywhere outside of the modal, close it
					window.onclick = function(event) {
						if (event.target == document.getElementById('myModal1')) {
							document.getElementById('myModal1').style.display = "none";
						}

						if (event.target == document.getElementById('myModal2')) {
							document.getElementById('myModal2').style.display = "none";
						}
						
						if (event.target == document.getElementById('myModal3')) {
							document.getElementById('myModal3').style.display = "none";
						}
					}

					function popupClose() {
						document.getElementById('myModal1').style.display = "none";
						$("#myModal2").css("display", "none");
						$("#myModal3").css("display", "none");
					}

					function modelClose() {
						location.reload(true);
					}
					var orderAvgChangeTime=-1;
					function showOtherTextArea() {
						var ch = document.getElementById('other').checked;
						if (ch) {
							$("#otherTextBox").css("display", "block");
						} else {
							$("#otherTextBox").css("display", "none");
						}
					}

					var futureOrder = $("#futureOrder").val();
					var fulfilled_on = $("#fulfilled_on").val();

					var fulfilled_onn = fulfilled_on.split(" ");
					var fulfilled_on_date = fulfilled_onn[0];
					var todayDate = new Date().toISOString().slice(0, 10);

					if (futureOrder == 1 && (fulfilled_on_date == todayDate)) {
						$("#acceptBtn").attr('disabled', false);
					} else {
						$("#acceptBtn").attr('disabled', true);
					}
					function changeTime(time){
						if(time!=null && time!="")
							orderAvgChangeTime=time;
					}
					function acceptOrder(type) {
						
						var changeTime = document.getElementById('changeTime').value;
						
						
						if(orderAvgChangeTime!=-1)
							changeTime = orderAvgChangeTime;
						var orderId = document.getElementById('orderId').value;
						var orderPosId=document.getElementById('orderPosId').value;
						var reason = "";
						if (type == 'Accept') {
							$("#acceptLink").css("display", "none");
							reason = "Order accepted by merchant.";
							$("#LoadingImage").css("display", "block");
							$("#acceptBody").css("display", "none");

						} else if (type == 'Agree') {
							reason = "Order agreed by merchant.";
							$("#futureAgreeDiv").hide();
							$("#LoadingImage2").css("display", "block");

						} else if (type == 'updateOrderTime') {
							reason = "Order time updated by merchant.";
							$("#LoadingImage").css("display", "block");
							$("#acceptBody").css("display", "none");

						} else {
							$("#acceptLink1").css("display", "none");
							var inputElements = document
									.getElementsByName('reason[]');
							for (var i = 0; i < inputElements.length; ++i) {
								if (inputElements[i].checked) {
									reason += inputElements[i].value + ", ";
								}
							}
							reason += document.getElementById("otherTextBox").value;
							$("#LoadingImage1").css("display", "block");
							$("#declineBody").css("display", "none");
						}

						if (type == 'Decline' && isFutureOrder == "1") {
							if (paymentMethod == 'Credit Card' || paymentMethod == 'Cash'
									&& (isDefaults == "1" || isDefaults=="0")
									&& isFutureOrder == "1") {
								
								type = "Decline";
								$
										.ajax({
											/* 	url : "chargeRefundByOrderId?orderId="+ orderId, */
											url : "orderAcceptAndDeclineByReceipt?orderId="
													+ orderId
													+ "&type="
													+ type
													+ "&changedOrderAvgTime="
													+ changeTime
													+ "&reason="
													+ reason,
											type : "GET",
											contentType : "application/json; charset=utf-8",
											success : function(status) {
												document
														.getElementById('myModal1').style.display = "none";
												document
														.getElementById('myModal2').style.display = "none";
												$("h3[bind='greeting']").html(
														status);
												$("#futureorderStatus").modal(
														'show');
											}
										});
							}
						} else {
							var url="";
							if(posId!=1)
								{
								url="orderAcceptAndDeclineByReceipt?orderId="
									+ orderId
									+ "&type="
									+ type
									+ "&changedOrderAvgTime="
									+ changeTime
									+ "&reason="
									+ reason;
								}else if(posId==1)
									{
									url="orderAcceptAndDeclineByReceipt?orderId="
										+ orderPosId
										+ "&type="
										+ type
										+ "&reason="
										+ reason
										+ "&isClover=true";
									}
							$
									.ajax({
										url : url,
										type : "GET",
										contentType : "application/json; charset=utf-8",
										success : function(status) {
											$("#LoadingImage1").css("display", "none");
											document.getElementById('myModal1').style.display = "none";
											document.getElementById('myModal2').style.display = "none";
											if (status != null) {
												$("h3[bind='greeting']").html(
														status);
												$("#futureorderStatus").modal(
														'show');

											} else {
												alert("something went wrong. try again..!");
											}
										}
									});

						}
						//$("#LoadingImage1").css("display", "none");
					}
					
					$(document).ready(function() {
						type='Decline';
						//alert("hello");
						var changeTime = document.getElementById('changeTime').value;
						var orderId = document.getElementById('orderId').value;
						var reason = "";
						var status = $("#status").val();
						var isdefault = $("#isdefault").val();

						//alert(status);
						//alert(isdefault);
					 if (status == 'false' && isdefault=='0') {
							//alert("hello");
	 
							if (paymentMethod == 'Credit Card'||paymentMethod == 'Cash')
							{
							
							 // type = "Decline";
							//$
								//	.ajax({
									//	/* 	url : "chargeRefundByOrderId?orderId="+ orderId, */
										//url : "orderAcceptAndDeclineByReceipt?orderId="
											//	+ orderId
												//+ "&type="
												/* + type
												+ "&changedOrderAvgTime="
												+ changeTime
												+ "&reason="
												+ reason,
										type : "GET",
										contentType : "application/json; charset=utf-8",
										success : function(status) {
											document
													.getElementById('myModal1').style.display = "none";
											document
													.getElementById('myModal2').style.display = "none";
											$("h3[bind='greeting']").html(
													status);
											$("#futureorderStatus").modal(
													'show');
										}
									});  */
						}
					 }
					
					});
					
				</script>
    			<script type="text/javascript">
					$(document).ready(function() {
						$('input[type="checkbox"]').click(function() {
							var inputValue = $(this).attr("value");
							$("." + inputValue).toggle();
						});
					});
				</script>
