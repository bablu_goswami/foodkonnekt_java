<!DOCTYPE html>
<jsp:include page="headerV2.jsp"></jsp:include>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html lang="en">
<head>
<title>FoodKonnekt</title>
<meta charset="utf-8">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<script type="text/javascript" src="resources/js/jquery-1.11.3.min.js"></script>
<script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>
<script type="text/javascript" src="resources/js/bootstrap.min.js"></script>
<script src="resources/js/woco.accordion.min.js"></script>
<link rel="stylesheet" type="text/css" href="resources/css/userprofile-min.css">
<link rel="stylesheet" type="text/css" href="resources/css/style-min.css">
<script type="text/javascript" src="resources/js/ping-nose-popup/pignose.popup.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://www.foodkonnekt.com/web/resources/V2/css/jquery.loadingimagethingy.css"/>


<style>
.dataTables_filter input {
	height: 45px;
	border-color: rgb(204, 204, 204) !important;
	margin-top: 8%;
}

#block_container
{
    text-align:center;
}
#bloc1, #bloc2
{
    display:inline;
}
</style>
<script type="text/javascript">
 $(document).ready(function() {
    var table = $('#example').DataTable();
    var modal = document.getElementById('myo');
} );
 
  $(function(){
     $('#orderModal').modal({   keyboard: true, backdrop: "static",   show:false,  }).on('show', function(){
    	 var modal = document.getElementById('myorder');

     	var getIdFromRow = $(event.target).closest('tr').data('id');
         $(this).find('#orderDetails').html($('<b> Order Id selected: ' + getIdFromRow  + '</b>'))
     });
 }); 
 </script>

<script>
$(document).ready(function() {
    
    $("#changeMerchantLocation").on("change", function () {
         var changeMerchant=$("#changeMerchantLocation").val();
         var merch=[];
         merch=changeMerchant.split(":");
         var merchId = merch[0];
         var merchName = merch[1].replace(" ","");
         
         $(".pj-loader-2").css("display","block");
         $.ajax({
                url : "sendLocationForProfileV2?merchantId="+ merchId +"&merchantName="+merchName,
                type : "GET",
                contentType : "application/json; charset=utf-8",
                success : function(result) {
                    location.reload();
                 //$(".pj-loader-2").css("display","none");
                },
                error : function() {
                  console.log("Error inside future date Ajax call");
                }
             })
        
         
        });
    
});
    
</script>
<script type="text/javascript">
$('#btn4').addClass('current');
$('#userIcon').attr("class", "current");

</script>
<body>
    <div class="container content">

        <div class=" user-profile row">
           <div class=" col-md-12 col-sm-12" style="margin-top: 0px;">
                <ul class=" list-inline">
                    <li>
                        <div style="width: 93% margin-right: 42px;">
                            <c:choose>
                                <c:when test="${ merchantList.size() > 1}">
                                    <h1 class="categchangeMerchant" id="changeLocation">Select Location</h1>
                                    <div class="zeroDiv">
                                        <select id="changeMerchantLocation">
                                            <c:forEach items="${merchantList}" var="view" varStatus="status">
                                                <c:choose>
                                                    <c:when test="${sessionScope.merchant.id ==view.id}">
                                                        <option value="${view.id}:${view.name}" selected>${view.name}</option>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <option value="${view.id}:${view.name}">${view.name}</option>
                                                    </c:otherwise>
                                                </c:choose>
                                            </c:forEach>
                                        </select>
                                    </div>
                                </c:when>
                                <%--  <c:otherwise>
                                                    <span>${sessionScope.merchant.name}</span>
                                                    </c:otherwise> --%>
                            </c:choose>
                        </div>
                    </li>
                    <c:if test="${sessionScope.merchant.id != null && sessionScope.vendor.id != null}">
                        <li><a
                            href="orderV2?merchantId=${sessionScope.merchant.id}&vendorId=${sessionScope.merchant.owner.id}"><button
                                    type="button" class="btn-success">Orders Now</button></a></li>
                    </c:if>
                    <c:if test="${sessionScope.merchant.id != null && sessionScope.vendor.id == null}">
                        <li><a href="orderV2?merchantId=${sessionScope.merchant.id}"><button type="button"
                                    class="btn-success">Order Now</button></a></li>
                    </c:if>
                    <li><a href="logoutV2"><button type="button" class="btn-success">Logout</button></a></li>

                </ul>
            </div>

        </div>

        <div class="row">
   		<div class="col-md-12">
            <div class="orange-bar heading">My orders</div>
         </div></div>
         
         
         <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">

                        <section class="main-section">
                            <div class="useless-inner-container">
                                <div class="sd-page-container">
                                    <div id="sd-form-container">
                                        <div class="liner"></div>
                                        <div class="order-container">
                                            <div class="orders-outbound">
                                                <div class="orders-inbound">
                                                    <div class="orders-inner-container">
                                                        <table width="100%" cellpadding="0" cellspacing="0" id="example"
                                                            summary="food konnekt order table" class="table table-hover">
                                                            <thead class="order">
                                                                <tr class="header-table-orders">
                                                                    <td>SN</td>
                                                                    <td>Date</td>
                                                                    <td>Restaurant</td>
                                                                    <td>Order Status</td>
                                                                    <td>Order</td>
                                                                    <!-- -----------REPEATE ORDER START----------------- -->

                                                                    <!-- -----------REPEATE ORDER END----------------- -->
                                                                </tr>

                                                            </thead>
                                                            <tbody>
                                                                <c:forEach items="${orderList}" var="order"
                                                                    varStatus="status">
                                                                    <tr data-toggle="modal" data-id="1"
                                                                        data-target="#myorder${order.id}">
                                                                        <td>${status.index+ 1 }</td>
                                                                        <td><fmt:formatDate pattern="MM/dd/yyyy"
                                                                                value="${order.createdOn}" /></td>
                                                                        <td>${order.merchant.name}</td>
                                                                        <td><c:choose>
                                                                                <c:when test="${order.isDefaults==1}">
                                                                                Confirmed
                                                                                </c:when>
                                                                                <c:when test="${order.isDefaults==2}">
                                                                                Cancelled
                                                                                </c:when>
                                                                                <c:when test="${order.isDefaults==3}">
                                                                                Failed
                                                                                </c:when>
                                                                                <c:when test="${order.isDefaults==4}">
                                                                                Agreed
                                                                                </c:when>
                                                                                <c:when test="${order.isDefaults==5}">
                                                                                Cancelled/Refunded
                                                                                </c:when>
                                                                                <c:when test="${order.isDefaults==6}">
                                                                                Cancelled
                                                                                </c:when>
                                                                                <c:otherwise>
                                                                                Pending
                                                                                </c:otherwise>
                                                                            </c:choose></a></td>
                                                                        <td><%-- <span
                                                                            style='color: #F8981D !important;'>$<fmt:formatNumber
                                                                                    type="currency" pattern="#####.##"
                                                                                    value=" --%><a href="#" class="orderDetail" data-toggle="modal" data-id="1" data-target="#myorder${order.id}">${order.orderPrice}</a> <%-- " /></span> --%></td>

                                                                    </tr>
                                                                </c:forEach>
                                                            </tbody>
                                                        </table>
                                                        <hr>
                                                        <div class="clearfix"></div>

                                                    </div>
                                                    <!--.orders-inner-container-->
                                                </div>
                                                <!--.orders-inbound-->
                                            </div>
                                            <!--.orders-outbound-->
                                        </div>
                                        <!--.order-container-->
                                    </div>
                                    <!-----#sd-form-container------>

                                </div>
                                <!--- sd-page-container ---->
                            </div>
                            <!--.useless-inner-container-->
                        </section>

                    </div>
                    <!--end of .table-responsive-->
                </div>


            </div>
        </div>
    </div>
    <!-- user-profile ends here-->


</body>
</html>


<c:forEach items="${orderList}" var="order" varStatus="status">
    <div id="myorder${order.id}" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">

                <div class="modal-header">
                    <div class="orange-bar1" style="line-height:40px;">ORDER DETAILS</div>
                </div>





                <div class="modal-body ">

					<div class="row">
						<div class="col-sm-6" style="margin-left: 28px;">
							<c:if test="${customer != null && customers != ''}">

								<h5>
									<b>Customer Name :</b> 
									<c:if test="${customer.firstName != null && customer.firstName != ''}">${customer.firstName}</c:if> 
									<c:if test="${customer.lastName != null && customer.lastName != ''}">${customer.lastName}</c:if> 
								</h5>
								<h5>
									<b> Phone Number :</b>${customer.phoneNumber}
								</h5>
								<h5>
									<b> EmailId :</b> ${customer.emailId }
								</h5>


							</c:if>
						</div>
						<c:if test="${order.address != null && order.address != ''}">


							<div class="col-sm-6" style="width: 40%;"> 
								<c:if test="${order.address != null && order.address != ''}">

									<h5>
										<b> Address1 : </b> ${order.address.address1 } </br> ${order.address.address2}
									</h5>
									<h5>
										<b> State : </b> ${order.address.state  }
									</h5>
									<h5>
										<b> City : </b> ${order.address.city }
									</h5>
									<h5>
										<b> Zip : </b> ${order.address.zip }
									</h5>


								</c:if>
							</div>
						</c:if>

					</div>


					<div class="table-responsive mbody">
                        <table summary="food konnekt order table" class="table table-hover">
                            <thead class="order">
                                <tr>
                                    <th>ITEM</th>
                                    <th>QUANTITY</th>
                                    <th>PRICE</th>

                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach items="${order.orderItemViewVOs}" var="itemView" varStatus="status1">
                                    <tr data-toggle="modal" data-id="1" data-target="#myorder">

                                        <td><strong>${itemView.item.name}</strong></td>
                                        <td><strong>${itemView.quantity}</strong></td>
                                        <td><strong>$<fmt:formatNumber type="currency" pattern="#####.##"
                                                    value="${itemView.item.price}" /></strong></td>


                                        <c:forEach items="${itemView.itemModifierViewVOs}" var="modifierView"
                                            varStatus="status1">
                                            <tr>
                                                <td>${modifierView.modifiers.name }</td>
                                                <td>${itemView.quantity}</td>
                                                <td>$<fmt:formatNumber type="currency" pattern="#####.##"
                                                        value="${modifierView.modifiers.price }" /></td>
                                            </tr>
                                        </c:forEach>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </div>
                    <div class="grey-box-order">
                        <div class="row">
                            <div class="lorder">subtotal</div>
                            <div class="roder">$${order.subTotal}</div>
                        </div>
                        <div class="row">
                            <div class="lorder">online fee</div>
                            <div class="roder">$${order.convenienceFee}</div>
                        </div>
                        <div class="row">
                            <c:if test="${order.deliveryFee != null && order.deliveryFee != '' && order.deliveryFee > 0}">
                                <div class="lorder">delivery fee</div>
                                <div class="roder">$${order.deliveryFee}</div>
                            </c:if>
                        </div>
                        <div class="row">
                            <c:if test="${order.tipAmount != null && order.tipAmount > 0}">
                                <div class="lorder">Tip</div>
                                <div class="roder">$${order.tipAmount}</div>
                            </c:if>
                        </div>
                        <div class="row">
                            <div class="lorder">Tax</div>
                            <div class="roder">$${order.tax}</div>
                        </div>
                        
                         <div class="row">
                          <c:if test="${order.orderDiscount > 0}">
                            <div class="lorder">Discount</div>
                            <div class="roder">$${order.orderDiscount}</div>
                            </c:if>
                        </div>
                        
                        <div class="row">
                         <c:if test="${! empty order.orderDiscountsList}">
                        <div class="lorder">Discount Coupon</div>
                        <c:forEach items="${order.orderDiscountsList}" var="orderDiscount" varStatus="status1">
                            <div class="row">
                            <div class="roder">${orderDiscount.couponCode}</div>
                            </div>
                        </c:forEach>
                        </c:if>
                        </div>
                        
                        <div class="row">
                            <div class="lorder">Total</div> 
                            <div class="roder">
                                $
                                <fmt:formatNumber type="currency" pattern="#####.##" value="${order.orderPrice}" />
                            </div>
                        </div>


                    </div>
                    <div class="grey-ip" align="center">
                        <input type="textarea" value="${order.orderNote}" readonly="readonly" style="height:60px; width:90%; margin:auto; border solid 1px #ccc; resize: none;">
                    </div>
                </div>
                <div class="modal-footer orderclose">
                    <button type="button" class="orderc btn-warning" data-dismiss="modal">close</button>
                </div>
            </div>
        </div>
    </div>
</c:forEach>
<jsp:include page="footerV2.jsp"></jsp:include>
