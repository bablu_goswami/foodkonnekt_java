<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head> -->
<div>&nbsp;</div>
<jsp:include page="headerV2.jsp"></jsp:include>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!-- <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title> -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script type="text/javascript" src="resources/js/jquery-1.11.3.min.js"></script>
<script src="resources/V2/js/userProfileV2.js"></script>
<!-- <script src="https://www.foodkonnekt.com/web/resources/V2/js/jquery-1.11.3.min.js"></script> -->
<script type="text/javascript" src="resources/js/bootstrap.min.js"></script>
<script src="resources/js/woco.accordion.min.js"></script>
<!-- <script src="resources/js/fSelect.js"></script> -->
<script type="text/javascript" src="https://www.foodkonnekt.com/web/resources/V2/js/jquery.loadingimagethingy.js"></script>
<script type="text/javascript" src="https://www.foodkonnekt.com/web/resources/js/ping-nose-popup/pignose.popup.min.js"></script>

<link rel="stylesheet" type="text/css" href="resources/css/userprofile.css"/>
<link rel="stylesheet" type="text/css" href="resources/css/style-min.css"/>
<script type="text/javascript">
$('#btn4').addClass('current');
$('#userIcon').attr("class", "current");

</script>

    <div class="container content" > 
        <div class="row">
            <div class=" col-md-12 col-sm-12" style="padding:0;" >
                <ul class=" list-inline" style="float:right;">
                    <li>
                        <div style="width: 100%;">
                            <c:choose>
                                <c:when test="${ merchantList.size() > 1}">
                                    <h1 class="categchangeMerchant" id="changeLocation">Select Location</h1>
                                    <div class="zeroDiv">
                                        <select id="changeMerchantLocation">
                                            <c:forEach items="${merchantList}" var="view" varStatus="status">
                                                <c:choose>
                                                    <c:when test="${sessionScope.merchant.id ==view.id}">
                                                        <option value="${view.id}:${view.name}" selected>${view.name}</option>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <option value="${view.id}:${view.name}">${view.name}</option>
                                                    </c:otherwise>
                                                </c:choose>
                                            </c:forEach>
                                        </select>
                                    </div>
                                </c:when>
                            </c:choose>
                        </div>
                    </li>


                    <c:if test="${sessionScope.merchant.id != null && sessionScope.vendor.id != null}">
                        <li class="order_now"><a
                            href="orderV2?merchantId=${sessionScope.merchant.id}&vendorId=${sessionScope.merchant.owner.id}"><button
                                    type="button" class="btn-success ">Order Now</button></a></li>
                    </c:if>

                    <c:if test="${sessionScope.merchant.id != null && sessionScope.vendor.id == null}">
                        <li class="order_now"><a href="orderV2?merchantId=${sessionScope.merchant.id}"><button
                                    type="button" class="btn-success ">Order Now</button></a></li>
                    </c:if>

                    <li><a href="myOrdersV2"><button type="button" class="btn-success">My Orders</button></a></li>
                    <li><a href="logoutV2"><button type="button" class="btn-success">Logout</button></a></li>
                </ul>



            </div>
        </div>
        <div class="user-profile container userprofile11" id="userprofilePage">
            <h1>general information</h1>
            <form:form method="POST" modelAttribute="Customer" name="myformPage" id="myformPage" autocomplete="off">
                <form:hidden path="id" id="customerId"/>
                <form:hidden path="merchantt.id" />
                <span id="errorBox"></span>
                <div class="grey-box col-md-12 col-sm-12">
                    <div class="col-md-2 col-sm-2 rimg">
                        <img src="resources/V2/images/user.jpg" alt="">
                    </div>
                     <div class="col-md-9 col-sm-9" id="user">
					<form class="form-horizontal" action=" " method="post"  id="user_form">

                         <fieldset>	
                        <!-- Form Name -->
                        <!-- Text input-->

                        <label class="col-md-4 control-label">First Name</label>
                        <div class="col-md-8" style="padding-left:0;">
                            <div class="input-group">
                                <!-- <input path="firstName" placeholder="First Name" id="firstName" class="form-control"  type="text" required/> -->
                                <form:input path="firstName" id="first-name" placeholder="First Name" maxlength="20"
                                    class="form-control" type="text" required=""/>
                                <span class="input-group-addon"><i class="fa fa-user fa-20x" style="font-size: 14px;color: #FFFFFF;"></i></span>
                            </div>
                        </div>
                        <!-- Text input-->
                        <label class="col-md-4 control-label">Last Name</label>
                        <div class="col-md-8 " style="padding-left:0;">
                            <div class="input-group">
                                <%-- <form:input path="lastName" placeholder="Last Name" id="lastName" class="form-control"  type="text" required/> --%>
                                <form:input path="lastName" placeholder="Last Name" id="second-name" maxlength="20"
                                    class="form-control" type="text" required=""/>
                                <span class="input-group-addon"><i class="fa fa-user fa-20x" style="font-size: 14px;color: #FFFFFF;"></i></span>
                            </div>
                        </div>
                        <!-- Text input-->

                        <label class="col-md-4 control-label">E-Mail</label>
                        <div class="col-md-8 " style="padding-left:0;">
                            <div class="input-group">
                                <!-- <input name="email" placeholder="xyz@sample.com" class="form-control" id="eMail" type="email" required> -->
                                <form:input path="emailId" placeholder="xyz@sample.com" class="form-control" id="eMail"
                                    type="email" />
                                <span class="input-group-addon"><i class="fa fa-envelope fa-16x" style="font-size: 14px;color: #FFFFFF;"></i></span>

                            </div>

                        </div>


                        <!-- Text input-->


                        <label class="col-md-4 control-label">Phone</label>
                        <div class="col-md-8 " style="padding-left:0;">
                            <div class="input-group">
                                <!-- <input name="phone"  placeholder="000-000-0000" class="form-control phone" id="phone" type="tel" required> -->
                                <form:input path="phoneNumber" placeholder="000-000-0000" class="form-control phone"
                                    id="mobile" type="tel" maxlength="10" />
                                <span class="input-group-addon"><i class="fa fa-mobile-phone fa-22x" style="font-size: 14px;color: #FFFFFF;"></i></span>
                            </div>
                        </div>

							<label class="col-md-4 control-label">Your Charity</label>
							<div class="col-md-8 " style="padding-left: 0;">
								<div class="input-group">
									<select id="fundCodes"
										style="width: 339px; padding: 9px 0 9px 12px; margin: -4px 13px 0 0px; size: 87;">
										<c:forEach items="${customerCodes}" var="view"
											varStatus="status">

											<option value="${view.fundCode}" selected>${view.fundCode}</option>
										</c:forEach>
									</select> <input type="button" onclick="removeFundcode()"
										value="Remove Selected"
										style="width: 123px; padding: 8px; color: 4; background-color: #4E6C88; color: white;">
								</div>
							</div>
							</div>


                </div>

                <div class="orange-bar">
                    <div class="col-md-6 col-sm-6">
                        <div class="col-md-4 col-sm-4">
                            <label>Anniversary</label>
                        </div>
                        <div class="col-md-4 col-sm-6" style="margin-bottom:10px; padding-left:0;">
                            <form:select path="anniversaryDate">
                                <c:if test="${Customer.anniversaryDate == null || Customer.anniversaryDate == ''}">
                                    <option selected>Month</option>
                                </c:if>
                                <c:set var="dateParts" value="${fn:split(Customer.anniversaryDate, ',')}" />
                                <c:if test="${Customer.anniversaryDate != null && Customer.anniversaryDate != ''}">
                                    <option selected>${dateParts[0]}</option>
                                </c:if>
                                <option val="jan">January</option>
                                <option val="feb">February</option>
                                <option val="mar">March</option>
                                <option val="apr">April</option>
                                <option val="may">May</option>
                                <option val="jun">June</option>
                                <option val="jul">July</option>
                                <option val="aug">August</option>
                                <option val="sep">September</option>
                                <option val="oct">October</option>
                                <option val="nov">November</option>
                                <option val="dec">December</option>
                            </form:select>
                        </div>
                        <div class="col-md-4 col-sm-6 " style="margin-bottom:10px; padding-left:0;">
                            <form:select path="anniversaryDate">
                                <c:if test="${Customer.anniversaryDate == null || Customer.anniversaryDate == ''}">
                                    <option selected>Date</option>
                                </c:if>
                                <c:set var="dateParts" value="${fn:split(Customer.anniversaryDate, ',')}" />
                                <c:if test="${Customer.anniversaryDate != null && Customer.anniversaryDate != ''}">
                                    <option selected>${dateParts[1]}</option>
                                </c:if>
                                <option val="1">1</option>
                                <option val="2">2</option>
                                <option val="3">3</option>
                                <option val="4">4</option>
                                <option val="5">5</option>
                                <option val="6">6</option>
                                <option val="7">7</option>
                                <option val="8">8</option>
                                <option val="9">9</option>
                                <option val="10">10</option>
                                <option val="11">11</option>
                                <option val="12">12</option>
                                <option val="13">13</option>
                                <option val="14">14</option>
                                <option val="15">15</option>
                                <option val="16">16</option>
                                <option val="17">17</option>
                                <option val="18">18</option>
                                <option val="19">19</option>
                                <option val="20">20</option>
                                <option val="21">21</option>
                                <option val="22">22</option>
                                <option val="23">23</option>
                                <option val="24">24</option>
                                <option val="25">25</option>
                                <option val="26">26</option>
                                <option val="27">27</option>
                                <option val="28">28</option>
                                <option val="29	">29</option>
                                <option val="30">30</option>
                                <option val="31">31</option>
                            </form:select>

                        </div>


                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class="col-md-4 col-sm-4">
                            <label>Date of Birth</label>
                        </div>
                        <div class="col-md-4 col-sm-6" style="margin-bottom:10px; padding-left:0;">
                            <form:select path="birthDate">
                                <c:if test="${Customer.birthDate == null || Customer.birthDate == ''}">
                                    <option selected>Month</option>
                                </c:if>
                                <c:set var="dateParts" value="${fn:split(Customer.birthDate, ',')}" />
                                <c:if test="${Customer.birthDate != null && Customer.birthDate != ''}">
                                    <option selected>${dateParts[0]}</option>
                                </c:if>
                                <option val="jan">January</option>
                                <option val="feb">February</option>
                                <option val="mar">March</option>
                                <option val="apr">April</option>
                                <option val="may">May</option>
                                <option val="jun">June</option>
                                <option val="jul">July</option>
                                <option val="aug">August</option>
                                <option val="sep">September</option>
                                <option val="oct">October</option>
                                <option val="nov">November</option>
                                <option val="dec">December</option>
                            </form:select>
                        </div>
                        <div class="col-md-4 col-sm-6 " style="margin-bottom:10px; padding-left:0;">
                            <form:select path="birthDate">
                                <c:if test="${Customer.birthDate == null || Customer.birthDate == ''}">
                                    <option selected>Date</option>
                                </c:if>
                                <c:set var="dateParts" value="${fn:split(Customer.birthDate, ',')}" />
                                <c:if test="${Customer.birthDate != null && Customer.birthDate != ''}">
                                    <option selected>${dateParts[1]}</option>
                                </c:if>
                                <!-- <option>Date</option> -->
                                <option val="1">1</option>
                                <option val="2">2</option>
                                <option val="3">3</option>
                                <option val="4">4</option>
                                <option val="5">5</option>
                                <option val="6">6</option>
                                <option val="7">7</option>
                                <option val="8">8</option>
                                <option val="9">9</option>
                                <option val="10">10</option>
                                <option val="11">11</option>
                                <option val="12">12</option>
                                <option val="13">13</option>
                                <option val="14">14</option>
                                <option val="15">15</option>
                                <option val="16">16</option>
                                <option val="17">17</option>
                                <option val="18">18</option>
                                <option val="19">19</option>
                                <option val="20">20</option>
                                <option val="21">21</option>
                                <option val="22">22</option>
                                <option val="23">23</option>
                                <option val="24">24</option>
                                <option val="25">25</option>
                                <option val="26">26</option>
                                <option val="27">27</option>
                                <option val="28">28</option>
                                <option val="29	">29</option>
                                <option val="30">30</option>
                                <option val="31">31</option>
                            </form:select>
                        </div>
                    </div>
                </div>
                <!-- orange-bar ends here-->

                <!-- Multipay Saved Cards-->

           <%--      <div class="col-md-12 lb uaddrw">
                    <div id="cardIndoDiv" style="display: none;">
                        <h1>Saved Cards</h1>
                        <div class="clearfix"></div>
                        <label id="errorBox" class="error"></label>
                        <h3 class="sd-fancy-title">SAVE CARDS</h3>
                        <hr>

                        <div class="col-md-4 fieldset" id="fieldset">
                            <div class="fieldset1" id="fieldset1">
                                <!-- <img class="close mith" src="resources/img/close.png" width="50"/> -->

                                <div class=" medium-12  columns">
                                    <label for="apt-no">CARDS DETAIL</label>
                                    <table class="table medium-12 ">

                                        <thead>
                                            <tr>
                                                <th style="text-align: center;">Card Type</th>
                                                <th style="text-align: center;">Last4</th>
                                                <th style="text-align: center;">Status</th>
                                                <th style="text-align: center;">Action</th>
                                            </tr>
                                        </thead>

                                        <tbody id="cardBody">
                                            <tr>
                                                <td>${view.last4}</td>
                                                <td>${view.cardType}</td>
                                                <td>${view.expirationDate}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="clearfix"></div>
                                <hr>
                            </div>
                        </div>
                    </div>
                </div> --%>
                <!---------------------- Multipay Saved Cards End------------>
                
                <!-- card div starts here-->
                <div id="cardIndoDiv" style="display: none;">
                        <h1>save cards</h1>
            <div class="grey-box col-md-12 lb uaddrw">
                <div class="row grey1-box">
                    <div class="col-md-3 col-xs-3 card">Card Type</div>
                    <div class="col-md-3 col-xs-3 card">Last4</div>
                    <div class="col-md-3 col-xs-3 card">Status</div>
                    <div class="col-md-3 col-xs-3 card">Action</div>
                </div>
            </div>
            <div class="grey1-box col-md-12 lb uaddrw">
                <div class="row" id="cardBody">
                    <%-- <div class="col-md-3 col-xs-3 card">${view.cardType}</div>
                    <div class="col-md-3 col-xs-3 card">${view.last4}</div>
                    <div class="col-md-3 col-xs-3 card">${view.cardType}</div>
                    <div class="col-md-3 col-xs-3 card"><a data-toggle="modal" href="#myModal-card"><button type="button" class="btn btn-active">delete</button></a></div> --%>
                </div>
            </div>
            </div>
            <div id="myModal-card" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
                  <button type="button" class="btn" data-dismiss="modal">X</button>
            <div class="modal-header">
            <img src="resources/V2/images/logo.png"  alt=""  >
            </div>
    
      <div class="modal-body">
<p>Are you sure you want to delete the card details?
<br>
    <div class="btnns"><button type="button" class="btn" data-dismiss="modal" onclick='deleteRecords()'>YES</button> &nbsp; &nbsp; <button type="button" class="btn" data-dismiss="modal">NO</button></div><br>
</p>

      </div> 
      <div class="modal-footer">

      </div>    
    </div>
  </div>
</div>  
        
<!--card div ends here-->


                <h1 style="margin-top:30px;">delivery address</h1>
                <div class="grey-box col-md-12 lb uaddrw">
                    <c:if test="${customerAddresses.size() > 1}">
                        <div class="row">
                            <div class="col-md-2"><label class="control-label">Select Address</label>  </div>
                            <div class="col-md-9 col-sm-9 add_select">
                                <select id="previousAddress">
                                    <c:forEach items="${customerAddresses}" var="view" varStatus="status">
                                        <option
                                            value="${view.address1}#${view.address2}#${view.city}#${view.state}#${view.zip}#${view.id}">&nbsp;&nbsp;
                                            ${view.address1} ${view.address2}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </div>
                    </c:if>

                    <div class="row">
					<div class="col-md-2">
                        <label class="control-label">Address</label> </div> 
                        <div class="col-md-9 col-sm-9">
                            <div class="input-group">
                                <form:hidden path="addresses[0].id" id="addressId" />
                                <form:hidden path="addresses[0].addressPosId" id="addressPosId" />
                                <form:input path="addresses[0].address1" id="address" placeholder=""
                                    class="form-control" type="text" />
                                <span class="input-group-addon"><i class="fa fa-location-arrow fa-22x" style="font-size: 14px;color: #FFFFFF;"></i></span>
                            </div>
                        </div>
                    </div>
                    <!--row ends here-->
                    <div class="row">
                        <div class="col-md-6">
			<div class="row">
            		<div class="col-md-4">
                            <label class="control-label">City</label>  </div>
                            <div class="col-md-7 ">
                                <div class="input-group">
                                    <form:input path="addresses[0].city" id="city" placeholder="" class="form-control"
                                        type="text" />
                                    <span class="input-group-addon"><i class="fa fa-map-marker fa-22x" style="font-size: 14px;color: #FFFFFF;"></i></span>
                                </div>
                            </div>
                        </div>
			</div>
                        <div class="col-md-6">
			<div class="row">
           		 <div class="col-md-3">
                            <label class="control-label">Country</label>  </div>
                            <div class="col-md-7 ">
                                <div class="input-group">
                                    <form:input path="addresses[0].country" id="country" placeholder="" class="form-control"
                                        type="text" /> <span class="input-group-addon"><i
                                        class="fa fa-map-marker fa-22x" style="font-size: 14px;color: #FFFFFF;"></i></span>
                                </div>
                            </div>

                        </div>
                    </div>
                    </div><!-- row ends here-->
                    <div class="row">
                        <div class="col-md-6">
			<div class="row">
            		<div class="col-md-4">
                            <label class="control-label">state</label></div>
                            <div class="col-md-7 ">
                                <div class="input-group">
                                    <form:input path="addresses[0].state" id="state" placeholder="" class="form-control"
                                        type="text" />
                                    <span class="input-group-addon"><i class="fa fa-map-marker fa-22x" style="font-size: 14px;color: #FFFFFF;"></i></span>
                                </div>
                            </div>
                        </div>
			</div>
                        <div class="col-md-6">
			<div class="row">
            		<div class="col-md-3 ">
                            <label class="control-label">zip</label></div>
                            <div class="col-md-7 ">
                                <div class="input-group">
                                    <form:input path="addresses[0].zip" placeholder="" id="zip" class="form-control"
                                        type="number" />
                                    <span class="input-group-addon"><i class="fa fa-map-marker fa-22x" style="font-size: 14px;color: #FFFFFF;"></i></span>
                                </div>
                            </div>

                        </div>
                        <!-- row ends here-->
                    </div>
                </div>
		</div><div>&nbsp;</div>
                <!-- user profile ends here-->

                <a href="#" onclick="changePassword()"><h1>change password</h1></a>
                <span id="passwordErrorMsg"></span>
                <div class="show_hide" id="changePassword">
                    <div class="grey-box col-md-12 lb uaddrw">
                        <div class="row">
			<div class="col-md-2">
                            <label class="control-label">old passsword</label></div> 
                            <div class=" col-md-9 col-sm-9">
                                <div class="input-group">
                                    <input type="password" placeholder="" id="oldPassword" class="form-control" /> <span
                                        class="input-group-addon"><i class="fa fa-lock fa-22x" style="font-size: 14px;color: #FFFFFF;"></i></span>
                                </div>
                            </div>
                        </div>
                        <!--row ends here-->
                        <div class="row"><div class="col-md-2">
                            <label class="control-label">new password</label> </div>
                            <div class="  col-md-9 col-sm-9">
                                <div class="input-group">
                                    <form:password path="password" placeholder="" class="form-control" id="newPassword" />
                                    <span class="input-group-addon"><i class="fa fa-lock fa-22x" style="font-size: 14px;color: #FFFFFF;"></i></span>
                                </div>
                            </div>
                        </div>
                        <!--row ends here-->
                        <div class="row">
			<div class="col-md-2">
                            <label class="control-label">confirm password</label> </div>
                            <div class="  col-md-9 col-sm-9">
                                <div class="input-group">
                                    <input type="password" placeholder="" class="form-control" id="newPassword1" /> <span
                                        class="input-group-addon"><i class="fa fa-lock fa-22x" style="font-size: 14px;color: #FFFFFF;"></i></span>
                                </div>
                            </div>
                        </div>
                        <!--row ends here-->
                    </div>
                </div>
               <div class="clearfix"></div>
                
		            <div class="text-center SubscribeBox">				  
                        <label><input type="checkbox" id="same_address" class="text-right" checked="checked">
			Subscribe for updates and deals</label>

                    
                </div>
		        <div class="clearfix"></div>

                <div class="text-center sbtn">
                    <input type="button" class="btn btn-success" onclick="updateCustomerDetails()" value="SAVE CHANGES">
                    <!-- </fieldset> -->
                </div>
            </fieldset>
            </form:form>
        </div>
        <!-- content container ends here-->


    </div>
<script type="text/javascript">
    jQuery(document).ready(function() {
		 
        if (jQuery('.userprofile11').attr("id") == "userprofilePage")  {
        $('#btn4').addClass('current');  
    } 
    });
</script>
<jsp:include page="footerV2.jsp"></jsp:include>


    <script type="text/javascript" src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.10.0/jquery.validate.min.js"></script>


    <script>
    var allowMultiPay="${sessionScope.allowMultiPay}";  
    var expiryCardsInfo='${sessionScope.expiryCardsInfos}';
    var cardInfo='${sessionScope.cardInfo}';
    var cardId;
 
			 function phoneFormatter() {
  $('.phone').on('input', function() {
    var number = $(this).val().replace(/[^\d]/g, '')
    if (number.length == 7) {
      number = number.replace(/(\d{3})(\d{4})/, "$1-$2");
    } else if (number.length == 10) {
      number = number.replace(/(\d{3})(\d{3})(\d{4})/, "$1-$2-$3");
    }
    $(this).val(number)
  });
};

$(phoneFormatter);
</script>
    <script>
function FillBilling(f) {
  if(f.billingtoo.checked == true) {
      f.daddress.value = f.baddress.value;
    f.dcity.value = f.bcity.value;
	f.dstate.value = f.bstate.value;
	f.dcountry.value = f.bcountry.value;
	f.dzip.value = f.bzip.value;
  }
}

</script>
    <script type="text/javascript">
window.onscroll = function() {myFunction()};

var header = document.getElementById("myHeader");
var sticky = header.offsetTop;

function myFunction() {
  if (window.pageYOffset >= sticky) {
    header.classList.add("sticky");
  } else {
    header.classList.remove("sticky");
  }
}



</script>
    <script>
$(document).ready(function() {
    
    $("#changeMerchantLocation").on("change", function () {
         var changeMerchant=$("#changeMerchantLocation").val();
         var merch=[];
         merch=changeMerchant.split(":");
         var merchId = merch[0];
         var merchName = merch[1].replace(" ","");
         
         $(".pj-loader-2").css("display","block");
         $.ajax({
                url : "sendLocationForProfileV2?merchantId="+ merchId +"&merchantName="+merchName,
                type : "GET",
                contentType : "application/json; charset=utf-8",
                success : function(result) {
                 window.location=result;
                 //$(".pj-loader-2").css("display","none");
                },
                error : function() {
                  console.log("Error inside future date Ajax call");
                }
             })
        
         
        });
    
});
    
</script>

<script type="text/javascript">
if ( $(window).width() < 200) {
$('input[type="number"]').on({
focus: function () {


$('.header').hide();

},
blur: function () {
$('.header').show();
}
});
$('input[type="email"]').on({
focus: function () {


$('.header').hide();

},
blur: function () {
$('.header').show();
}
});
$('input[type="password"]').on({
focus: function () {


$('.header').hide();

},
blur: function () {
$('.header').show();
}
});
$('textarea').on({
focus: function () {


$('.header').hide();

},
blur: function () {
$('.header').show();
}
});
$('input[type="text"]').on({
focus: function () {


$('.header').hide();

},
blur: function () {
$('.header').show();
}
});
}
</script>

 
