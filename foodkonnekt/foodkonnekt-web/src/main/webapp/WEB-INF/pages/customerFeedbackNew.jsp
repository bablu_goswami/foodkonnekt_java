<!DOCTYPE html>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
  <meta charset="utf-8">

<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<!-- jQuery library -->
    <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet"><style>
*{font-family: 'Open Sans', sans-serif !important;}
.jq-stars {
  display: inline-block;
}
button{background:transparent !important;}
.no_input{border:1px solid #fff !important;}
.modal-dialog{margin-top:-80px !important;}
.line{margin-top:-65px;}
input[type="text"],input[type="email"]{width:100%;border:0px #fff !important;}
input[type="text"]:focus,input[type="email"]:focus,
input[type="submit"]:focus.select:focus{outline:none !important;}
.n_input{border:1px solid #ccc; width:46%; margin-left:1%; float:left;font-size:13px; padding:10px; margin-bottom:2%;}
.n_input1{border:1px solid #ccc; width:93%; margin-left:1%; float:left;font-size:13px; padding:10px; margin-bottom:2%;}
.dob{font-size:13px;float:left;margin-left:1.1%;margin-right:2%;margin-top:-24px;}
.bottom{margin-top:-27px;}
select{margin-left:1%; width:46%;padding:10px;float:left;font-size:13px;margin-top:-37.5px; background:#3c3c3c;color:#fff;outline:none !important;}
option{background:#fcfff4;color:#3c3c3c; border-bottom:1px solid #3c3c3c; padding:10px;}
.linpu{margin-left:1.4%; margin-right:1%;font-size:13px;}
.inpu{font-size:13px;  float:left;}
.jq-rating-label {
  font-size: 22px;
  display: inline-block;
  position: relative;
  vertical-align: top;
  font-family: helvetica, arial, verdana;
}

.jq-star {
  width: 17px !important;
  display: inline-block;
  cursor: pointer;
}

.jq-star-svg {
  padding-left: 3px;
  width: 100%;
  height: 100% ;
}

.jq-star:hover .fs-star-svg path {
}

.jq-star-svg path {
  /* stroke: #000; */
  stroke-linejoin: round;
}

/* un-used */
.jq-shadow {
  -webkit-filter: drop-shadow( -2px -2px 2px #888 );
  filter: drop-shadow( -2px -2px 2px #888 );
}

.live-rating,.live-rating1,.live-rating2,.live-rating3,.live-rating4,.live-rating5,.live-rating6,.live-rating7,.live-rating8,.live-rating9,.live-rating10,.live-rating11,.live-rating12,.live-rating13,.live-rating14 {
 display:none;
 padding-left:40%;
  font-size: 15px;
  color: #f8981d;
  top: -13px;
  position: relative;
  font-family:inherit;
}
.linpu span{font-size:12px;}
h4{font-size:15px;}
.icon img{}
.three{padding-left:26%;}
.white{color:#fff;}
textarea{font-size:13px !important;
padding-top:10px;
margin-top:10px;
margin-left:1.03%;width:92.9%;
height:80px;
border-radius:2px;
resize:none;
border:1px solid #c1bfc0;
outline:none !important;
}
.left{width:50%; float:left; position:relative; } .right{width:50%;float:right; padding-left:5%;position:relative; padding-top:5px;}
.emoji {
  width: 120px;
  height: 120px;
  margin: 15px 15px 40px;
  background: orange;
  display: inline-block;
  border-radius: 50%;
  position: relative;
}
.emoji:after {
  position: absolute;
  bottom: -40px;
  font-size: 16px;
  width: 160px;
  left: calc(10% - 30px);
  color: #8A8A8A;
}
button{background-color:transparent !important;margin:0px !important;}
.emoji__face, .emoji__eyebrows, .emoji__eyes, .emoji__mouth, .emoji__tongue, .emoji__heart, .emoji__hand, .emoji__thumb {
  position: absolute;
}
.emoji__face:before, .emoji__face:after, .emoji__eyebrows:before, .emoji__eyebrows:after, .emoji__eyes:before, .emoji__eyes:after, .emoji__mouth:before, .emoji__mouth:after, .emoji__tongue:before, .emoji__tongue:after, .emoji__heart:before, .emoji__heart:after, .emoji__hand:before, .emoji__hand:after, .emoji__thumb:before, .emoji__thumb:after {
  position: absolute;
  content: '';
}

.emoji__face {
  width: inherit;
  height: inherit;
}

.emoji--like {
  background: orange;
}
.emoji--like:after {
  content: 'Like';
}
.emoji--like .emoji__hand {
  left: 25px;
  bottom: 30px;
  width: 20px;
  height: 40px;
  background: #FFFFFF;
  border-radius: 5px;
  z-index: 0;
  -webkit-animation: hands-up 2s linear infinite;
          animation: hands-up 2s linear infinite;
}
.emoji--like .emoji__hand:before {
  left: 25px;
  bottom: 5px;
  width: 40px;
  background: inherit;
  height: 10px;
  border-radius: 2px 10px 10px 2px;
  box-shadow: 1px -9px 0 1px #FFFFFF, 2px -19px 0 2px #FFFFFF, 3px -29px 0 3px #FFFFFF;
}
.emoji--like .emoji__thumb {
  border-bottom: 20px solid #FFFFFF;
  border-left: 20px solid transparent;
  top: -25px;
  right: -25px;
  z-index: 2;
  -webkit-transform: rotate(5deg);
          transform: rotate(5deg);
  -webkit-transform-origin: 0% 100%;
          transform-origin: 0% 100%;
  -webkit-animation: thumbs-up 2s linear infinite;
          animation: thumbs-up 2s linear infinite;
}
.emoji--like .emoji__thumb:before {
  border-radius: 50% 50% 0 0;
  background: #FFFFFF;
  width: 10px;
  height: 12px;
  left: -10px;
  top: -8px;
  -webkit-transform: rotate(-15deg);
          transform: rotate(-15deg);
  -webkit-transform-origin: 100% 100%;
          transform-origin: 100% 100%;
  box-shadow: -1px 4px 0 -1px #FFFFFF;
}

.emoji--love {
  background: orange;
}
.emoji--love:after {
  content: 'Love';
}
.emoji--love .emoji__heart {
  left: calc(50% - 40px);
  top: calc(50% - 40px);
  width: 80px;
  height: 80px;
  -webkit-animation: heart-beat 1s linear infinite alternate;
          animation: heart-beat 1s linear infinite alternate;
}
.emoji--love .emoji__heart:before, .emoji--love .emoji__heart:after {
  left: calc(50% - 20px);
  top: calc(50% - 32px);
  width: 40px;
  height: 64px;
  background: #FFFFFF;
  border-radius: 20px 20px 0 0;
}
.emoji--love .emoji__heart:before {
  -webkit-transform: translate(20px) rotate(-45deg);
          transform: translate(20px) rotate(-45deg);
  -webkit-transform-origin: 0 100%;
          transform-origin: 0 100%;
}
.emoji--love .emoji__heart:after {
  -webkit-transform: translate(-20px) rotate(45deg);
          transform: translate(-20px) rotate(45deg);
  -webkit-transform-origin: 100% 100%;
          transform-origin: 100% 100%;
}
.modal-dialog{width:39% !important;}
.emoji--haha:after {
  content: 'Haha';
}
.emoji--haha .emoji__face {
  -webkit-animation: haha-face 2s linear infinite;
          animation: haha-face 2s linear infinite;
}
.emoji--haha .emoji__eyes {
  width: 26px;
  height: 6px;
  border-radius: 2px;
  left: calc(50% - 13px);
  top: 35px;
  -webkit-transform: rotate(20deg);
          transform: rotate(20deg);
  background: transparent;
  box-shadow: -25px 5px 0 0 #000000, 25px -5px 0 0 #000000;
}
.emoji--haha .emoji__eyes:after {
  left: 0;
  top: 0;
  width: 26px;
  height: 6px;
  border-radius: 2px;
  -webkit-transform: rotate(-40deg);
          transform: rotate(-40deg);
  background: transparent;
  box-shadow: -25px -5px 0 0 #000000, 25px 5px 0 0 #000000;
}
.emoji--haha .emoji__mouth {
  width: 80px;
  height: 40px;
  left: calc(50% - 40px);
  top: 50%;
  background: #000000;
  border-radius: 0 0 40px 40px;
  overflow: hidden;
  z-index: 1;
  -webkit-animation: haha-mouth 2s linear infinite;
          animation: haha-mouth 2s linear infinite;
}
.emoji--haha .emoji__tongue {
  width: 70px;
  height: 30px;
  background: orange;
  left: calc(50% - 35px);
  bottom: -10px;
  border-radius: 50%;
}

.emoji--yay:after {
  content: 'Yay';
  -webkit-animation: yay-reverse 1s linear infinite;
          animation: yay-reverse 1s linear infinite;
}
.emoji--yay .emoji__face {
  -webkit-animation: yay 1s linear infinite alternate;
          animation: yay 1s linear infinite alternate;
}
.emoji--yay .emoji__eyebrows {
  left: calc(50% - 3px);
  top: 30px;
  height: 6px;
  width: 6px;
  border-radius: 50%;
  background: transparent;
  box-shadow: -6px 0 0 0 #000000, -36px 0 0 0px #000000, 6px 0 0 0 #000000, 36px 0 0 0px #000000;
}
.emoji--yay .emoji__eyebrows:before, .emoji--yay .emoji__eyebrows:after {
  width: 36px;
  height: 18px;
  border-radius: 60px 60px 0 0;
  background: transparent;
  border: 6px solid black;
  box-sizing: border-box;
  border-bottom: 0;
  bottom: 3px;
  left: calc(50% - 18px);
}
.emoji--yay .emoji__eyebrows:before {
  margin-left: -21px;
}
.emoji--yay .emoji__eyebrows:after {
  margin-left: 21px;
}
.emoji--yay .emoji__mouth {
  top: 60px;
  background: transparent;
  left: 50%;
}
.emoji--yay .emoji__mouth:after {
  width: 80px;
  height: 80px;
  left: calc(50% - 40px);
  top: -75px;
  border-radius: 50%;
  background: transparent;
  border: 6px solid #000000;
  box-sizing: border-box;
  border-top-color: transparent;
  border-left-color: transparent;
  border-right-color: transparent;
  z-index: 1;
}
.emoji--yay .emoji__mouth:before {
  width: 6px;
  height: 6px;
  background: transparent;
  border-radius: 50%;
  bottom: 5px;
  left: calc(50% - 3px);
  box-shadow: -25px 0 0 0 #000000, 25px 0 0 0 #000000, -35px -2px 30px 10px #D5234C, 35px -2px 30px 10px #D5234C;
}

.emoji--wow:after {
  content: 'Madly in Love';
}
.emoji--wow .emoji__face {
  -webkit-animation: wow-face 3s linear infinite;
          animation: wow-face 3s linear infinite;
}
.emoji--wow .emoji__eyebrows {
  left: calc(50% - 3px);
  height: 6px;
  width: 6px;
  border-radius: 50%;
  background: transparent;
  box-shadow: -18px 0 0 0 #000000, -33px 0 0 0 #000000, 18px 0 0 0 #000000, 33px 0 0 0 #000000;
  -webkit-animation: wow-brow 3s linear infinite;
          animation: wow-brow 3s linear infinite;
}
.emoji--wow .emoji__eyebrows:before, .emoji--wow .emoji__eyebrows:after {
  width: 24px;
  height: 20px;
  border: 6px solid #000000;
  box-sizing: border-box;
  border-radius: 50%;
  border-bottom-color: transparent;
  border-left-color: transparent;
  border-right-color: transparent;
  top: -3px;
  left: calc(50% - 12px);
}
.emoji--wow .emoji__eyebrows:before {
  margin-left: -25px;
}
.emoji--wow .emoji__eyebrows:after {
  margin-left: 25px;
}
.emoji--wow .emoji__eyes {
  width: 16px;
  height: 24px;
  left: calc(50% - 8px);
  top: 35px;
  border-radius: 50%;
  background: transparent;
  box-shadow: 25px 0 0 0 #000000, -25px 0 0 0 #000000;
}
.emoji--wow .emoji__mouth {
  width: 30px;
  height: 45px;
  left: calc(50% - 15px);
  top: 50%;
  border-radius: 50%;
  background: #000000;
  -webkit-animation: wow-mouth 3s linear infinite;
          animation: wow-mouth 3s linear infinite;
}

.emoji--sad:after {
  content: 'Hurt';
}
.emoji--sad .emoji__face {
  -webkit-animation: sad-face 2s ease-in infinite;
          animation: sad-face 2s ease-in infinite;
}
.emoji--sad .emoji__eyebrows {
  left: calc(50% - 3px);
  top: 35px;
  height: 6px;
  width: 6px;
  border-radius: 50%;
  background: transparent;
  box-shadow: -40px 9px 0 0 #000000, -25px 0 0 0 #000000, 25px 0 0 0 #000000, 40px 9px 0 0 #000000;
}
.emoji--sad .emoji__eyebrows:before, .emoji--sad .emoji__eyebrows:after {
  width: 30px;
  height: 20px;
  border: 6px solid #000000;
  box-sizing: border-box;
  border-radius: 50%;
  border-bottom-color: transparent;
  border-left-color: transparent;
  border-right-color: transparent;
  top: 2px;
  left: calc(50% - 15px);
}
.emoji--sad .emoji__eyebrows:before {
  margin-left: -30px;
  -webkit-transform: rotate(-30deg);
          transform: rotate(-30deg);
}
.emoji--sad .emoji__eyebrows:after {
  margin-left: 30px;
  -webkit-transform: rotate(30deg);
          transform: rotate(30deg);
}
.emoji--sad .emoji__eyes {
  width: 14px;
  height: 16px;
  left: calc(50% - 7px);
  top: 50px;
  border-radius: 50%;
  background: transparent;
  box-shadow: 25px 0 0 0 #000000, -25px 0 0 0 #000000;
}
.emoji--sad .emoji__eyes:after {
  background: orange;
  width: 12px;
  height: 12px;
  margin-left: 6px;
  border-radius: 0 100% 40% 50% / 0 50% 40% 100%;
  -webkit-transform-origin: 0% 0%;
          transform-origin: 0% 0%;
  -webkit-animation: tear-drop 2s ease-in infinite;
          animation: tear-drop 2s ease-in infinite;
}
.emoji--sad .emoji__mouth {
  width: 60px;
  height: 80px;
  left: calc(50% - 30px);
  top: 80px;
  box-sizing: border-box;
  border: 6px solid #000000;
  border-radius: 50%;
  border-bottom-color: transparent;
  border-left-color: transparent;
  border-right-color: transparent;
  background: transparent;
  -webkit-animation: sad-mouth 2s ease-in infinite;
          animation: sad-mouth 2s ease-in infinite;
}
.emoji--sad .emoji__mouth:after {
  width: 6px;
  height: 6px;
  background: transparent;
  border-radius: 50%;
  top: 4px;
  left: calc(50% - 3px);
  box-shadow: -18px 0 0 0 #000000, 18px 0 0 0 #000000;
}

.emoji--angry {
  background: linear-gradient(#D5234C -10%, orange);
  background-size: 100%;
  -webkit-animation: angry-color 2s ease-in infinite;
          animation: angry-color 2s ease-in infinite;
}
.emoji--angry:after {
  content: 'Heart Broken';
}
.emoji--angry .emoji__face {
  -webkit-animation: angry-face 2s ease-in infinite;
          animation: angry-face 2s ease-in infinite;
}
.emoji--angry .emoji__eyebrows {
  left: calc(50% - 3px);
  top: 55px;
  height: 6px;
  width: 6px;
  border-radius: 50%;
  background: transparent;
  box-shadow: -44px 5px 0 0 #000000, -7px 16px 0 0 #000000, 7px 16px 0 0 #000000, 44px 5px 0 0 #000000;
}
.emoji--angry .emoji__eyebrows:before, .emoji--angry .emoji__eyebrows:after {
  width: 50px;
  height: 20px;
  border: 6px solid #000000;
  box-sizing: border-box;
  border-radius: 50%;
  border-top-color: transparent;
  border-left-color: transparent;
  border-right-color: transparent;
  top: 0;
  left: calc(50% - 25px);
}
.emoji--angry .emoji__eyebrows:before {
  margin-left: -25px;
  -webkit-transform: rotate(15deg);
          transform: rotate(15deg);
}
.emoji--angry .emoji__eyebrows:after {
  margin-left: 25px;
  -webkit-transform: rotate(-15deg);
          transform: rotate(-15deg);
}
.emoji--angry .emoji__eyes {
  width: 12px;
  height: 12px;
  left: calc(50% - 6px);
  top: 70px;
  border-radius: 50%;
  background: transparent;
  box-shadow: 25px 0 0 0 #000000, -25px 0 0 0 #000000;
}
.emoji--angry .emoji__mouth {
  width: 36px;
  height: 18px;
  left: calc(50% - 18px);
  bottom: 15px;
  background: #000000;
  border-radius: 50%;
  -webkit-animation: angry-mouth 2s ease-in infinite;
          animation: angry-mouth 2s ease-in infinite;
}

@-webkit-keyframes heart-beat {
  25% {
    -webkit-transform: scale(1.1);
            transform: scale(1.1);
  }
  75% {
    -webkit-transform: scale(0.6);
            transform: scale(0.6);
  }
}

@keyframes heart-beat {
  25% {
    -webkit-transform: scale(1.1);
            transform: scale(1.1);
  }
  75% {
    -webkit-transform: scale(0.6);
            transform: scale(0.6);
  }
}
@-webkit-keyframes haha-face {
  10%, 30%, 50% {
    -webkit-transform: translateY(25px);
            transform: translateY(25px);
  }
  20%, 40% {
    -webkit-transform: translateY(15px);
            transform: translateY(15px);
  }
  60%, 80% {
    -webkit-transform: translateY(0);
            transform: translateY(0);
  }
  70%, 90% {
    -webkit-transform: translateY(-10px);
            transform: translateY(-10px);
  }
}
@keyframes haha-face {
  10%, 30%, 50% {
    -webkit-transform: translateY(25px);
            transform: translateY(25px);
  }
  20%, 40% {
    -webkit-transform: translateY(15px);
            transform: translateY(15px);
  }
  60%, 80% {
    -webkit-transform: translateY(0);
            transform: translateY(0);
  }
  70%, 90% {
    -webkit-transform: translateY(-10px);
            transform: translateY(-10px);
  }
}
@-webkit-keyframes haha-mouth {
  10%, 30%, 50% {
    -webkit-transform: scale(0.6);
            transform: scale(0.6);
    top: 45%;
  }
  20%, 40% {
    -webkit-transform: scale(0.8);
            transform: scale(0.8);
    top: 45%;
  }
  60%, 80% {
    -webkit-transform: scale(1);
            transform: scale(1);
    top: 50%;
  }
  70% {
    -webkit-transform: scale(1.2);
            transform: scale(1.2);
    top: 50%;
  }
  90% {
    -webkit-transform: scale(1.1);
            transform: scale(1.1);
    top: 50%;
  }
}
@keyframes haha-mouth {
  10%, 30%, 50% {
    -webkit-transform: scale(0.6);
            transform: scale(0.6);
    top: 45%;
  }
  20%, 40% {
    -webkit-transform: scale(0.8);
            transform: scale(0.8);
    top: 45%;
  }
  60%, 80% {
    -webkit-transform: scale(1);
            transform: scale(1);
    top: 50%;
  }
  70% {
    -webkit-transform: scale(1.2);
            transform: scale(1.2);
    top: 50%;
  }
  90% {
    -webkit-transform: scale(1.1);
            transform: scale(1.1);
    top: 50%;
  }
}
@-webkit-keyframes yay {
  25% {
    -webkit-transform: rotate(-15deg);
            transform: rotate(-15deg);
  }
  75% {
    -webkit-transform: rotate(15deg);
            transform: rotate(15deg);
  }
}
@keyframes yay {
  25% {
    -webkit-transform: rotate(-15deg);
            transform: rotate(-15deg);
  }
  75% {
    -webkit-transform: rotate(15deg);
            transform: rotate(15deg);
  }
}
@-webkit-keyframes wow-face {
  15%, 25% {
    -webkit-transform: rotate(20deg) translateX(-25px);
            transform: rotate(20deg) translateX(-25px);
  }
  45%, 65% {
    -webkit-transform: rotate(-20deg) translateX(25px);
            transform: rotate(-20deg) translateX(25px);
  }
  75%, 100% {
    -webkit-transform: rotate(0deg) translateX(0);
            transform: rotate(0deg) translateX(0);
  }
}
@keyframes wow-face {
  15%, 25% {
    -webkit-transform: rotate(20deg) translateX(-25px);
            transform: rotate(20deg) translateX(-25px);
  }
  45%, 65% {
    -webkit-transform: rotate(-20deg) translateX(25px);
            transform: rotate(-20deg) translateX(25px);
  }
  75%, 100% {
    -webkit-transform: rotate(0deg) translateX(0);
            transform: rotate(0deg) translateX(0);
  }
}
@-webkit-keyframes wow-brow {
  15%, 65% {
    top: 25px;
  }
  75%, 100%, 0% {
    top: 15px;
  }
}
@keyframes wow-brow {
  15%, 65% {
    top: 25px;
  }
  75%, 100%, 0% {
    top: 15px;
  }
}
@-webkit-keyframes wow-mouth {
  10%, 30% {
    width: 20px;
    height: 20px;
    left: calc(50% - 10px);
  }
  50%, 70% {
    width: 30px;
    height: 40px;
    left: calc(50% - 15px);
  }
  75%, 100% {
    height: 50px;
  }
}
@keyframes wow-mouth {
  10%, 30% {
    width: 20px;
    height: 20px;
    left: calc(50% - 10px);
  }
  50%, 70% {
    width: 30px;
    height: 40px;
    left: calc(50% - 15px);
  }
  75%, 100% {
    height: 50px;
  }
}
@-webkit-keyframes sad-face {
  25%, 35% {
    top: -15px;
  }
  55%, 95% {
    top: 10px;
  }
  100%, 0% {
    top: 0;
  }
}
@keyframes sad-face {
  25%, 35% {
    top: -15px;
  }
  55%, 95% {
    top: 10px;
  }
  100%, 0% {
    top: 0;
  }
}
@-webkit-keyframes sad-mouth {
  25%, 35% {
    -webkit-transform: scale(0.85);
            transform: scale(0.85);
    top: 70px;
  }
  55%, 100%, 0% {
    -webkit-transform: scale(1);
            transform: scale(1);
    top: 80px;
  }
}
@keyframes sad-mouth {
  25%, 35% {
    -webkit-transform: scale(0.85);
            transform: scale(0.85);
    top: 70px;
  }
  55%, 100%, 0% {
    -webkit-transform: scale(1);
            transform: scale(1);
    top: 80px;
  }
}
@-webkit-keyframes tear-drop {
  0%, 100% {
    display: block;
    left: 35px;
    top: 15px;
    -webkit-transform: rotate(45deg) scale(0);
            transform: rotate(45deg) scale(0);
  }
  25% {
    display: block;
    left: 35px;
    -webkit-transform: rotate(45deg) scale(2);
            transform: rotate(45deg) scale(2);
  }
  49.9% {
    display: block;
    left: 35px;
    top: 65px;
    -webkit-transform: rotate(45deg) scale(0);
            transform: rotate(45deg) scale(0);
  }
  50% {
    display: block;
    left: -35px;
    top: 15px;
    -webkit-transform: rotate(45deg) scale(0);
            transform: rotate(45deg) scale(0);
  }
  75% {
    display: block;
    left: -35px;
    -webkit-transform: rotate(45deg) scale(2);
            transform: rotate(45deg) scale(2);
  }
  99.9% {
    display: block;
    left: -35px;
    top: 65px;
    -webkit-transform: rotate(45deg) scale(0);
            transform: rotate(45deg) scale(0);
  }
}
@keyframes tear-drop {
  0%, 100% {
    display: block;
    left: 35px;
    top: 15px;
    -webkit-transform: rotate(45deg) scale(0);
            transform: rotate(45deg) scale(0);
  }
  25% {
    display: block;
    left: 35px;
    -webkit-transform: rotate(45deg) scale(2);
            transform: rotate(45deg) scale(2);
  }
  49.9% {
    display: block;
    left: 35px;
    top: 65px;
    -webkit-transform: rotate(45deg) scale(0);
            transform: rotate(45deg) scale(0);
  }
  50% {
    display: block;
    left: -35px;
    top: 15px;
    -webkit-transform: rotate(45deg) scale(0);
            transform: rotate(45deg) scale(0);
  }
  75% {
    display: block;
    left: -35px;
    -webkit-transform: rotate(45deg) scale(2);
            transform: rotate(45deg) scale(2);
  }
  99.9% {
    display: block;
    left: -35px;
    top: 65px;
    -webkit-transform: rotate(45deg) scale(0);
            transform: rotate(45deg) scale(0);
  }
}
@-webkit-keyframes hands-up {
  25% {
    -webkit-transform: rotate(15deg);
            transform: rotate(15deg);
  }
  50% {
    -webkit-transform: rotate(-15deg) translateY(-10px);
            transform: rotate(-15deg) translateY(-10px);
  }
  75%, 100% {
    -webkit-transform: rotate(0deg);
            transform: rotate(0deg);
  }
}
@keyframes hands-up {
  25% {
    -webkit-transform: rotate(15deg);
            transform: rotate(15deg);
  }
  50% {
    -webkit-transform: rotate(-15deg) translateY(-10px);
            transform: rotate(-15deg) translateY(-10px);
  }
  75%, 100% {

    -webkit-transform: rotate(0deg);
            transform: rotate(0deg);
  }
}
@-webkit-keyframes thumbs-up {
  25% {
    -webkit-transform: rotate(20deg);
            transform: rotate(20deg);
  }
  50%, 100% {
    -webkit-transform: rotate(5deg);
            transform: rotate(5deg);
  }
}
@keyframes thumbs-up {
  25% {
    -webkit-transform: rotate(20deg);
            transform: rotate(20deg);
  }
  50%, 100% {
    -webkit-transform: rotate(5deg);
            transform: rotate(5deg);
  }
}
@-webkit-keyframes angry-color {
  45%, 60% {
    background-size: 250%;
  }
  85%, 100%, 0% {
    background-size: 100%;
  }
}
@keyframes angry-color {
  45%, 60% {
    background-size: 250%;
  }
  85%, 100%, 0% {
    background-size: 100%;
  }
}
@-webkit-keyframes angry-face {
  35%, 60% {
    -webkit-transform: translateX(0) translateY(10px) scale(0.9);
            transform: translateX(0) translateY(10px) scale(0.9);
  }
  40%, 50% {
    -webkit-transform: translateX(-5px) translateY(10px) scale(0.9);
            transform: translateX(-5px) translateY(10px) scale(0.9);
  }
  45%, 55% {
    -webkit-transform: translateX(5px) translateY(10px) scale(0.9);
            transform: translateX(5px) translateY(10px) scale(0.9);
  }
}
@keyframes angry-face {
  35%, 60% {
    -webkit-transform: translateX(0) translateY(10px) scale(0.9);
            transform: translateX(0) translateY(10px) scale(0.9);
  }
  40%, 50% {
    -webkit-transform: translateX(-5px) translateY(10px) scale(0.9);
            transform: translateX(-5px) translateY(10px) scale(0.9);
  }
  45%, 55% {
    -webkit-transform: translateX(5px) translateY(10px) scale(0.9);
            transform: translateX(5px) translateY(10px) scale(0.9);
  }
}
@-webkit-keyframes angry-mouth {
  25%, 50% {
    height: 6px;
    bottom: 25px;
  }
}
@keyframes angry-mouth {
  25%, 50% {
    height: 6px;
    bottom: 25px;
  }
}

.col-md-6{
width:50%;
float:left;
font-size:20px !important;
}
input[type="checkbox"] { display: none; } input[type="checkbox"] + label { font-weight: 400; font-size: 18px;} input[type="checkbox"] + label span { display: inline-block; width: 12px; height: 12px; margin: -2px 10px 0 0; vertical-align: middle; cursor: pointer;  border: 2px solid #2e333b; } input[type="checkbox"] + label span { background-color:#fff; } input[type="checkbox"]:checked + label { color: #333; font-weight: 700; } input[type="checkbox"]:checked + label span { background-color: #ff8800; border: 2px solid #2e333b; box-shadow: 2px 2px 2px rgba(0,0,0,.1); } input[type="checkbox"] + label span, input[type="checkbox"]:checked + label span { -webkit-transition: background-color 0.24s linear; -o-transition: background-color 0.24s linear; -moz-transition: background-color 0.24s linear; transition: background-color 0.24s linear; }
body{
background-image:url(images/pizza-bg.jpg);
text-align:center;
font-size:24px;}
.content{
border-left:1px solid #000;
border-right:1px solid #000;
border-bottom:1px solid #000;
border-radius:10px;
width:80%;
margin:0px auto;
margin-top:6% !important;
	padding-bottom:60px;
	background-color:#fff;
}
.accordion  img{	
float:right;
width:10%;
margin-right:50%;
}
.accordion {
	width:50%;
    background-color: #b1762b;
    color: #2e333b;
    cursor: pointer;
    padding: 13px;
    border: none;
    text-align: left;
    outline: none;
    font-size: 20px;
	font-weight:bold;
    transition: 0.4s;
	margin-bottom:10px;
	border-radius:5px;
	padding-left:10%;
	}
.active,.accordion:hover {
    background-color: #b1762b;
}
input[type="submit"]
{
outline:none !important;
margin-top:10px;
text-transform:uppercase;
margin-left:34.5%;
border:1px solid #34740e; -webkit-border-radius: 50px; -moz-border-radius: 50px;border-radius: 50px;font-size:18px; padding: 10px 40px; text-decoration:none; display:inline-block;text-shadow: -1px -1px 0 rgba(0,0,0,0.3);font-weight:bold; color: #FFFFFF;
 background-color: #4ba614; background-image: -webkit-gradient(linear, left top, left bottom, from(#4ba614), to(#008c00));
 background-image: -webkit-linear-gradient(top, #4ba614, #008c00);
 background-image: -moz-linear-gradient(top, #4ba614, #008c00);
 background-image: -ms-linear-gradient(top, #4ba614, #008c00);
 background-image: -o-linear-gradient(top, #4ba614, #008c00);
 background-image: linear-gradient(to bottom, #4ba614, #008c00);filter:progid:DXImageTransform.Microsoft.gradient(GradientType=0,startColorstr=#4ba614, endColorstr=#008c00);
}
.container1 img{text-align:center !important;}
.container1 {
padding-top:3px;
  width:33%;
  float:left;
  position: relative;
  padding-left: 35px;
  margin-bottom: 12px;
  cursor: pointer;
  font-size: 14px;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}

/* Hide the browser's default checkbox */
.container1 input {
  position: absolute;
  opacity: 0;
  cursor: pointer;
  height: 0;
  width: 0;
}

/* Create a custom checkbox */
.checkmark {
  position: absolute;
  top: 6px;
  left: 70%;
  height: 25px;
  width: 25px;
  background-color: #fff;
  border:2px solid #3f3f3f;
  border-radius:5px;
}

/* On mouse-over, add a grey background color */
.container1:hover input ~ .checkmark {
  background-color: #ccc;
}

/* When the checkbox is checked, add a blue background */
.container1 input:checked ~ .checkmark {
  background-color: #fff;
}

/* Create the checkmark/indicator (hidden when not checked) */
.checkmark:after {
  content: "";
  position: absolute;
  display: none;
}

/* Show the checkmark when checked */
.container1 input:checked ~ .checkmark:after {
  display: block;
}

/* Style the checkmark/indicator */
.container1 .checkmark:after {
  left: 9px;
  top: 5px;
  width: 5px;
  height: 10px;
  border: solid #3f3f3f;
  border-width: 0 3px 3px 0;
  -webkit-transform: rotate(45deg);
  -ms-transform: rotate(45deg);
  transform: rotate(45deg);
}
.panel p{
font-size:24px;
}
.panel{
padding-left:15% !important;
text-align:left !important;
	width:50%;
	margin:0px auto;
    padding: 1px;
    color: white;
    max-height: 0;
    overflow: hidden;
    transition: max-height 0.2s ease-out;
	margin-bottom:10px !important;
}
.form_content{
background-color:#fff;
}
p{
margin-top:20px;}
.modal-header{
background-color:#3c3c3c;
color:#fff;
font-size:28px;}
.modal-body{text-align:left;
padding-left:6%;
background:#fff !important;
}
.logo-bar{
border-radius:10px 10px 0px 0px;
margin-top:1%;
width:100%;
background-color:#3c3c3c;
text-align:center;
padding:0px 0px;
}
.logo-bar p{
color::#fff;
}
.social ul li{display:inline;
list-style:none;
margin:0px 20px;
}
button{outline:none !important;}
.modal-dialog{
top:15%;}
.close{
color:#fff !important;
opacity:1 !important;}
.btn.active, .btn:active{
box-shadow:0;}

@media only screen and (max-width: 482px) {
footer{font-size:14px;}
footer img{width:5%;}

.bottom{margin-top:-30px;}
.modal{margin-left:3px;}
.close{padding-top:10px !important;}
.live-rating,.live-rating1,.live-rating2,.live-rating3,.live-rating4,.live-rating5,.live-rating6,.live-rating7,.live-rating8,.live-rating9,.live-rating10,.live-rating11,.live-rating12,.live-rating13,.live-rating14 {padding-left:35%; font-size:15px;}

input[type="text"],input[type="email"]{width:90%;}
.left,.right{width:100% !important;
}
.hurt{width:110%; margin-left:-10px !important;}
.jq-star {margin-top:15px;
width:26px !important;}
.modal-content{
width:240%;
}
#month{margin-left:2px;}
textarea{width:93%;margin-top:0px !important;}
 .dob {
margin-top: -22px !important;
font-size: 13px;
float: left;
margin-right: 2%;
}


#day{}
select{margin-top:-37px;width:46.3%;margin-bottom:8px;}
.icon img {
    width: 73%;
    margin-bottom: 20px;
}
.icon2,.icon3{width:96.2% !important;}
.linpu,.inpu {margin-left:3.4%;margin-bottom:10px;}
.modal-body{
padding-left:5%;
}
.content{
width:100%;}
.accordion{
width:95%;}
.accordion img{margin-right:3%;}
.panel{width:100%;}
.panel p{
font-size:16px;}
input[type="checkbox"] + label { font-weight: 400; font-size: 16px;}
input[type="submit"]{
    
    margin-left:25.0%;
    margin-top: 10px;} 	
}
</style>
</head>
<body>
<div class="content">
<div class="logo-bar">
				<img src="${merchantLogo}" alt="">
			</div>
	<p id="paragraph" style="display:none;">
	RATE YOUR EXPERIENCE

<br>
	
	We thank you for your time and  insights.
	</p>
	
	<p id="alreadyGaveFeedback" style="display:none;">
	<br>
		We have already received feedback for this order. Thank You for your time.
	<br>
	</p>
	
	<p id="feedbackFormEnabled" style="display:none;">
	<br>
		Feedback form is not enabled. Please contact the restaurant.
	<br>
	</p>
	
	
<div id="emojis" style="display:none;">	
	
<button type="button" class="btn" id="heartBroken" data-toggle="modal" data-target="#myModal5">
<div class="emoji  emoji--angry">
  <div class="emoji__face">
    <div class="emoji__eyebrows"></div>
    <div class="emoji__eyes"></div>
    <div class="emoji__mouth"></div>
  </div>
  </div>
</button>
<button type="button" class="btn" id="hurtEmoji" data-toggle="modal" data-target="#myModal4">
<div class="emoji  emoji--sad">
  <div class="emoji__face">
    <div class="emoji__eyebrows"></div>
    <div class="emoji__eyes"></div>
    <div class="emoji__mouth"></div>
  </div>
</div>
</button>
<button type="button" class="btn" id="like" data-toggle="modal" data-target="#myModal3">
	<div class="emoji  emoji--like">
  <div class="emoji__hand">
      <div class="emoji__thumb"></div>
  </div>
</div>
</button>
<button type="button" class="btn" id="love" data-toggle="modal" data-target="#myModal2">
<div class="emoji  emoji--love">
  <div class="emoji__heart"></div>
</div>
</button>
<button type="button" class="btn" id="madlyInLove" data-toggle="modal" data-target="#myModal">	<div class="emoji  emoji--wow">
  <div class="emoji__face">
    <div class="emoji__eyebrows"></div>
    <div class="emoji__eyes"></div>
    <div class="emoji__mouth"></div>
  </div>
</div>
</button>
</div>
<div class="footer"><br>
Powered by <a href="https://www.foodkonnekt.com" target="_blank"><img src="/foodkonnekt-web/resources/kritiq/img/kritiq.png"></a>
</div>
</div>
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h3 class="modal-title">Your Feedback Matters</h3>
      </div>
      <div class="modal-body">
        <form:form class="hurt" action="saveCustomerFeedbackV2" method="POST" modelAttribute="CustomerFeedback" onsubmit="return checkRating1(this)">
				<!-- <span id="id1" style="display:none; color: red;text-align:center;">Please select rating</span> -->
		
		<div class="col-md-4 col-sm-4 col-xs-12 icon">
		<div class="col-xs-5">
		
		<img src="/foodkonnekt-web/resources/kritiq/img/icon1.jpg"><br>
		</div>
         
         <form:hidden path="foodQuality"  id="foodQualityRating" />
          <form:hidden path="customer.id"  id="customerId" value="${customer.id }" />
          <form:hidden path="orderR.id" id="orderId" value="${orderR.id}" />
         <form:hidden path="customerService" id="customerServiceRating"/>
         <form:hidden path="orderExperience"  id="orderExperienceRating"/>
         
          <div class="col-md-12 col-xs-7">
		 <span class="my-rating-9"></span>
		<span class="live-rating">0</span>
		</div>
		</div>
	
		<div class="col-md-4 col-sm-4 col-xs-12 icon">
		<div class="col-md-12 col-xs-5">
		<img class="icon2" src="/foodkonnekt-web/resources/kritiq/img/icon2.jpg"><br>
         </div>
         
         <div class="col-md-12 col-xs-7">
		 <span class="my-rating-10"></span>
		<span class="live-rating1">0</span>
		</div>
		</div>
		
		<div class="col-md-4 col-sm-4 col-xs-12 icon">
		<div class="col-md-12 col-xs-5">
		<img class="icon3" src="/foodkonnekt-web/resources/kritiq/img/icon4.jpg"><br>
         </div>
         
           <div class="col-md-12 col-xs-7">
		 <span class="my-rating-11"></span>
			<span class="live-rating2">0</span>
		</div>
		</div>

<img class="line" src="/foodkonnekt-web/resources/kritiq/img/line.png" width="95%">
<span class="dob">Name</span>
<div class="n_input"><form:input path="customer.firstName" readonly="true" type="text" value="${customer.firstName}" placeholder="Name*"/></div>
<span class="dob">Contact Number</span><br>
<div class="n_input"><form:input path="customer.phoneNumber" readonly="true" type="email" value="${customer.phoneNumber}" placeholder="Contact Number*"/></div>
<br>
<br>
<br>

<div class="bottom">
<span class="dob">Email</span>
<div class="n_input1"><form:input path="customer.emailId" readonly="true" type="text" value="${customer.emailId}" placeholder="Email*"/></div>
<br><br><br>
</div>

<div class="bottom">

<span class="dob">BIRTHDAY</span><br><form:select path="bdayDate" id="day"><option id="1">1</option>
<option id="2">2</option>
<option id="3">3</option>
<option id="4">4</option>
<option id="5">5</option>
<option id="6">6</option>
<option id="7">7</option>
<option id="8">8</option>
<option id="9">9</option>
<option id="10">10</option>
<option id="11">11</option>
<option id="12">12</option>
<option id="13">13</option>
<option id="14">14</option>
<option id="15">15</option>
<option id="16">16</option>
<option id="17">17</option>
<option id="18">18</option>
<option id="19">19</option>
<option id="20">20</option>
<option id="21">21</option>
<option id="22">22</option>
<option id="23">23</option>
<option id="24">24</option>
<option id="25">25</option>
<option id="26">26</option>
<option id="27">27</option>
<option id="28">28</option>
<option id="29">29</option>
<option id="30">30</option>
<option id="31">31</option>
</form:select>
<form:select path="bdayMonth" id="month">
<option>JAN</option>
<option>FEB</option>
<option>MAR</option>
<option>APR</option>
<option>MAY</option>
<option>JUN</option>
<option>JUL</option>
<option>AUG</option>
<option>SEP</option>
<option>OCT</option>
<option>NOV</option>
<option>DEC</option>
</form:select>
</div>

<form:textarea path="customerComments" maxlength="300" placeholder="Additional Comments"/>

<!-- <textarea maxlength="300" placeholder="Additional Comments"></textarea> -->
<input type="submit" value="Submit" class="submit">
</form:form>
      </div>
    </div>
  </div>
  </div>
  <div id="myModal2" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" >&times;</button>
        <h3 class="modal-title">Your Feedback Matters</h3>
      </div>
      <div class="modal-body">
        <form:form class="hurt" action="saveCustomerFeedbackV2" method="POST" modelAttribute="CustomerFeedback" onsubmit="return checkRating2(this)">
						<!-- <span id="id2" style="display:none; color: red;text-align:center;">Please select rating</span> -->
		
		<div class="col-md-4 col-sm-4 col-xs-12 icon">
		
		<div class="col-xs-5">
		<img src="/foodkonnekt-web/resources/kritiq/img/icon1.jpg"><br>
		</div>
         
          <form:hidden path="foodQuality"  id="foodQualityRating12" />
          <form:hidden path="customer.id" id="customerId" value="${customer.id }" />
          <form:hidden path="orderR.id"  id="orderId" value="${orderR.id}" />
         <form:hidden path="customerService"  id="customerServiceRating13"/>
         <form:hidden path="orderExperience" id="orderExperienceRating14"/>
         
         <div class="col-md-12 col-xs-7">
		 <span class="my-rating-12"></span>
<span class="live-rating3">0</span>
</div>
		</div>
		<div class="col-md-4 col-sm-4 col-xs-12 icon">
			<div class="col-md-12 col-xs-5">
		<img class="icon2" src="/foodkonnekt-web/resources/kritiq/img/icon2.jpg"><br>    
		</div>
		
		<div class="col-md-12 col-xs-7">     
		 <span class="my-rating-13"></span>
<span class="live-rating4">0</span>
</div>

		</div>
		<div class="col-md-4 col-sm-4 col-xs-12 icon">
		<div class="col-md-12 col-xs-5">
		<img class="icon3" src="/foodkonnekt-web/resources/kritiq/img/icon4.jpg"><br>
		</div>
		
		<div class="col-md-12 col-xs-7">
        <span class="my-rating-14"></span>
         <span class="live-rating5">0</span>
         </div>
		</div>
<img class="line" src="/foodkonnekt-web/resources/kritiq/img/line.png" width="95%">
<span class="dob">Name</span>
<div class="n_input"><form:input path="customer.firstName" readonly="true" type="text" value="${customer.firstName}" placeholder="Name*"/></div>

<span class="dob">Contact Number</span><br>
<div class="n_input"><form:input path="customer.phoneNumber" readonly="true" type="email" value="${customer.phoneNumber}" placeholder="Contact Number*"/></div>
<br>
<br>
<br>

<div class="bottom">
<span class="dob">Email</span>
<div class="n_input1"><form:input path="customer.emailId" readonly="true" type="text" value="${customer.emailId}" placeholder="Email*"/></div>
<br><br><br>
</div>


  

<div class="bottom">
<span class="dob">BIRTHDAY</span><br><form:select path="bdayDate" id="day"><option id="1">1</option>
<option id="2">2</option>
<option id="3">3</option>
<option id="4">4</option>
<option id="5">5</option>
<option id="6">6</option>
<option id="7">7</option>
<option id="8">8</option>
<option id="9">9</option>
<option id="10">10</option>
<option id="11">11</option>
<option id="12">12</option>
<option id="13">13</option>
<option id="14">14</option>
<option id="15">15</option>
<option id="16">16</option>
<option id="17">17</option>
<option id="18">18</option>
<option id="19">19</option>
<option id="20">20</option>
<option id="21">21</option>
<option id="22">22</option>
<option id="23">23</option>
<option id="24">24</option>
<option id="25">25</option>
<option id="26">26</option>
<option id="27">27</option>
<option id="28">28</option>
<option id="29">29</option>
<option id="30">30</option>
<option id="31">31</option>
</form:select>
<form:select path="bdayMonth" id="month">
<option>JAN</option>
<option>FEB</option>
<option>MAR</option>
<option>APR</option>
<option>MAY</option>
<option>JUN</option>
<option>JUL</option>
<option>AUG</option>
<option>SEP</option>
<option>OCT</option>
<option>NOV</option>
<option>DEC</option>
</form:select>
</div>

<form:textarea path="customerComments" maxlength="300" placeholder="Additional Comments"/>
<input type="submit" value="Submit" class="submit">
</form:form>
      </div>
    </div>
  </div>
  </div>
<div id="myModal3" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" >&times;</button>
        <h3 class="modal-title">Your Feedback Matters</h3>
      </div>
      <div class="modal-body">
        <form:form class="hurt" action="saveCustomerFeedbackV2" method="POST" modelAttribute="CustomerFeedback" onsubmit="return checkRating3(this)">
						<!-- <span id="id3" style="display:none; color: red;text-align:center;">Please select rating</span> -->
		
		<div class="col-md-4 col-sm-4 col-xs-12 icon">
		
		<div class="col-xs-5">
		<img src="/foodkonnekt-web/resources/kritiq/img/icon1.jpg"><br>
         </div>
         <form:hidden path="foodQuality"  id="foodQualityRating15" />
          <form:hidden path="customer.id" id="customerId" value="${customer.id }" />
          <form:hidden path="orderR.id" id="orderId" value="${orderR.id}" />
         <form:hidden path="customerService" id="customerServiceRating16"/>
         <form:hidden path="orderExperience" id="orderExperienceRating17"/>
         
         <div class="col-md-12 col-xs-7">
		 <span class="my-rating-15"></span>
<span class="live-rating6">0</span>
</div>
		</div>
		<div class="col-md-4 col-sm-4 col-xs-12 icon">
		<div class="col-xs-5">
		<img class="icon2" src="/foodkonnekt-web/resources/kritiq/img/icon2.jpg"><br>
         </div>
         
         <div class="col-md-12 col-xs-7">
		 <span class="my-rating-16"></span>
<span class="live-rating7">0</span>
</div>
		</div>
		<div class="col-md-4 col-sm-4 col-xs-12 icon">
		<div class="col-xs-5">
		<img class="icon3" src="/foodkonnekt-web/resources/kritiq/img/icon4.jpg"><br>
		</div>
         
         <div class="col-md-12 col-xs-7">
		 <span class="my-rating-17"></span>
<span class="live-rating8">0</span>
</div>
		</div>

<img class="line" src="/foodkonnekt-web/resources/kritiq/img/line.png" width="95%">
<span class="dob">Name</span>
<div class="n_input"><form:input path="customer.firstName" readonly="true" type="text" value="${customer.firstName}" placeholder="Name*"/></div>

<span class="dob">Contact Number</span><br>
<div class="n_input"><form:input path="customer.phoneNumber" readonly="true" type="email" value="${customer.phoneNumber}" placeholder="Contact Number*"/></div>
<br>
<br>
<br>

<div class="bottom">
<span class="dob">Email</span>
<div class="n_input1"><form:input path="customer.emailId" readonly="true" type="text" value="${customer.emailId}" placeholder="Email*"/></div>
<br><br><br>
</div>

<div class="bottom">
<span class="dob">BIRTHDAY</span><br><form:select path="bdayDate" id="day"><option id="1">1</option>
<option id="2">2</option>
<option id="3">3</option>
<option id="4">4</option>
<option id="5">5</option>
<option id="6">6</option>
<option id="7">7</option>
<option id="8">8</option>
<option id="9">9</option>
<option id="10">10</option>
<option id="11">11</option>
<option id="12">12</option>
<option id="13">13</option>
<option id="14">14</option>
<option id="15">15</option>
<option id="16">16</option>
<option id="17">17</option>
<option id="18">18</option>
<option id="19">19</option>
<option id="20">20</option>
<option id="21">21</option>
<option id="22">22</option>
<option id="23">23</option>
<option id="24">24</option>
<option id="25">25</option>
<option id="26">26</option>
<option id="27">27</option>
<option id="28">28</option>
<option id="29">29</option>
<option id="30">30</option>
<option id="31">31</option>
</form:select>
<form:select path="bdayMonth" id="month">
<option>JAN</option>
<option>FEB</option>
<option>MAR</option>
<option>APR</option>
<option>MAY</option>
<option>JUN</option>
<option>JUL</option>
<option>AUG</option>
<option>SEP</option>
<option>OCT</option>
<option>NOV</option>
<option>DEC</option>
</form:select>
</div>
<form:textarea path="customerComments" maxlength="300" placeholder="Additional Comments"/>
<input type="submit" value="Submit" class="submit">
</form:form>
      </div>
    </div>
  </div>
  </div>
<div id="myModal4" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" >&times;</button>
        <h3 class="modal-title">Your Feedback Matters</h3>
      </div>
      <div class="modal-body">
        <form:form class="hurt" action="saveCustomerFeedbackV2" method="POST" modelAttribute="CustomerFeedback" onsubmit="return checkRating4(this)">
							<!-- <span id="id4" style="display:none; color: red;text-align:center;">Please select rating</span> -->
		
		<div class="col-md-4 col-sm-4 col-xs-12 icon">
		
			<div class="col-xs-5">
		<img src="/foodkonnekt-web/resources/kritiq/img/icon1.jpg"><br>
		</div>
         
         <form:hidden path="foodQuality" id="foodQualityRating18" />
          <form:hidden path="customer.id"  id="customerId" value="${customer.id }" />
          <form:hidden path="orderR.id"  id="orderId" value="${orderR.id}" />
         <form:hidden path="customerService"  id="customerServiceRating19"/>
         <form:hidden path="orderExperience"  id="orderExperienceRating20"/>
         
         <div class="col-md-12 col-xs-7">
		 <span class="my-rating-18"></span>
<span class="live-rating9">0</span>
</div>
		</div>
		<div class="col-md-4 col-sm-4 col-xs-12 icon">
		<div class="col-xs-5">
		<img class="icon2" src="/foodkonnekt-web/resources/kritiq/img/icon2.jpg"><br>
		</div>
         
          <div class="col-md-12 col-xs-7">
		 <span class="my-rating-19"></span>
<span class="live-rating10">0</span>
</div>
		</div>
		
		<div class="col-md-4 col-sm-4 col-xs-12 icon">
		<div class="col-xs-5">
		<img class="icon3" src="/foodkonnekt-web/resources/kritiq/img/icon4.jpg"><br>
         </div>
        <div class="col-md-12 col-xs-7"> 
		 <span class="my-rating-20"></span>
	<span class="live-rating11">0</span>
	</div>
</label> 
		</div>

<img class="line" src="/foodkonnekt-web/resources/kritiq/img/line.png" width="95%">
<span class="dob">Name</span>
<div class="n_input"><form:input path="customer.firstName" readonly="true" type="text" value="${customer.firstName}" placeholder="Name*"/></div>

<span class="dob">Contact Number</span><br>
<div class="n_input"><form:input path="customer.phoneNumber" readonly="true" type="email" value="${customer.phoneNumber}" placeholder="Contact Number*"/></div>
<br>
<br>
<br>

<div class="bottom">
<span class="dob">Email</span>
<div class="n_input1"><form:input path="customer.emailId" readonly="true" type="text" value="${customer.emailId}" placeholder="Email*"/></div>
<br><br><br>
</div>

<div class="bottom">
<span class="dob">BIRTHDAY</span><br><form:select path="bdayDate" id="day"><option id="1">1</option>
<option id="2">2</option>
<option id="3">3</option>
<option id="4">4</option>
<option id="5">5</option>
<option id="6">6</option>
<option id="7">7</option>
<option id="8">8</option>
<option id="9">9</option>
<option id="10">10</option>
<option id="11">11</option>
<option id="12">12</option>
<option id="13">13</option>
<option id="14">14</option>
<option id="15">15</option>
<option id="16">16</option>
<option id="17">17</option>
<option id="18">18</option>
<option id="19">19</option>
<option id="20">20</option>
<option id="21">21</option>
<option id="22">22</option>
<option id="23">23</option>
<option id="24">24</option>
<option id="25">25</option>
<option id="26">26</option>
<option id="27">27</option>
<option id="28">28</option>
<option id="29">29</option>
<option id="30">30</option>
<option id="31">31</option>
</form:select>
<form:select path="bdayMonth" id="month">
<option>JAN</option>
<option>FEB</option>
<option>MAR</option>
<option>APR</option>
<option>MAY</option>
<option>JUN</option>
<option>JUL</option>
<option>AUG</option>
<option>SEP</option>
<option>OCT</option>
<option>NOV</option>
<option>DEC</option>
</form:select>
</div>

<form:textarea path="customerComments" maxlength="300" placeholder="Additional Comments"/>
<input type="submit" value="Submit" class="submit">
</form:form>
      </div>
    </div>
  </div>
  </div>
  <div id="myModal5" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" >&times;</button>
        <h3 class="modal-title">Your Feedback Matters</h3>
      </div>
      <div class="modal-body">
        <form:form class="hurt" action="saveCustomerFeedbackV2" method="POST" modelAttribute="CustomerFeedback" onsubmit="return checkRating5(this)">
				<!-- <span id="id5" style="display:none; color: red;text-align:center;">Please select rating</span> -->
		
		<div class="col-md-4 col-sm-4 col-xs-12 icon">
		<div class="col-xs-5">
		<img src="/foodkonnekt-web/resources/kritiq/img/icon1.jpg"><br>
		</div>
         
         <form:hidden path="foodQuality" id="foodQualityRating30" />
          <form:hidden path="customer.id"  id="customerId" value="${customer.id }" />
          <form:hidden path="orderR.id" id="orderId" value="${orderR.id}" />
         <form:hidden path="customerService" id="customerServiceRating31"/>
         <form:hidden path="orderExperience"  id="orderExperienceRating32"/>
         
          <div class="col-md-12 col-xs-7">
		<span class="my-rating-30"></span>
			<span class="live-rating12">0</span>
			</div>
		</div>
		<div class="col-md-4 col-sm-4 col-xs-12 icon">
		<div class="col-xs-5">
		<img class="icon2" src="/foodkonnekt-web/resources/kritiq/img/icon2.jpg"><br>
		</div>
         
          <div class="col-md-12 col-xs-7">
		<span class="my-rating-31"></span>
<span class="live-rating13">0</span>
</div>
		</div>
		<div class="col-md-4 col-sm-4 col-xs-12 icon">
		<div class="col-md-12 col-xs-5">
		<img class="icon3" src="/foodkonnekt-web/resources/kritiq/img/icon4.jpg"><br>
		</div>
         
          <div class="col-md-12 col-xs-7">
		<span class="my-rating-32"></span>
<span class="live-rating14">0</span>
</div>
		</div>

<input type="hidden" value="${feedbackStatus}" id="feedbackStatus">
<input type="hidden" value="${merchantKritiqExpiryStatus}" id="merchantKritiqExpiryStatus">

<img class="line" src="/foodkonnekt-web/resources/kritiq/img/line.png" width="95%">

<span class="dob">Name</span>
<div class="n_input"><form:input path="customer.firstName" readonly="true" type="text" value="${customer.firstName}" placeholder="Name*"/></div>

<span class="dob">Contact Number</span><br>
<div class="n_input"><form:input path="customer.phoneNumber" readonly="true" type="email" value="${customer.phoneNumber}" placeholder="Contact Number*"/></div>
<br>
<br>
<br>

<div class="bottom">
<span class="dob">Email</span>
<div class="n_input1"><form:input path="customer.emailId" readonly="true" type="text" value="${customer.emailId}" placeholder="Email*"/></div>
<br><br><br>
</div>

<div class="bottom">
<span class="dob">BIRTHDAY</span><br><form:select path="bdayDate" id="day"><option id="1">1</option>
<option id="2">2</option>
<option id="3">3</option>
<option id="4">4</option>
<option id="5">5</option>
<option id="6">6</option>
<option id="7">7</option>
<option id="8">8</option>
<option id="9">9</option>
<option id="10">10</option>
<option id="11">11</option>
<option id="12">12</option>
<option id="13">13</option>
<option id="14">14</option>
<option id="15">15</option>
<option id="16">16</option>
<option id="17">17</option>
<option id="18">18</option>
<option id="19">19</option>
<option id="20">20</option>
<option id="21">21</option>
<option id="22">22</option>
<option id="23">23</option>
<option id="24">24</option>
<option id="25">25</option>
<option id="26">26</option>
<option id="27">27</option>
<option id="28">28</option>
<option id="29">29</option>
<option id="30">30</option>
<option id="31">31</option>
</form:select>
<form:select path="bdayMonth" id="month">
<option>JAN</option>
<option>FEB</option>
<option>MAR</option>
<option>APR</option>
<option>MAY</option>
<option>JUN</option>
<option>JUL</option>
<option>AUG</option>
<option>SEP</option>
<option>OCT</option>
<option>NOV</option>
<option>DEC</option>
</form:select>
</div>

<form:textarea path="customerComments" maxlength="300" placeholder="Additional Comments"/>
<input type="submit" value="Submit" class="submit">
</form:form>
      </div>
    </div>
  </div>
  </div>

<script>

var foodQualityRating;
var customerServiceRating;
var orderExperienceRating;

$(function() {

  $(".my-rating-9").starRating({
    initialRating: 0,
    disableAfterRate: false,
    onClick: function(currentIndex, currentRating, $el){
    	foodQualityRating = currentIndex;
    	//document.getElementById('foodQualityRating').innerHTML = foodQualityRating;
    	$( "#foodQualityRating" ).val(foodQualityRating);
    	console.log('my-rating-9 : ',$( "#foodQualityRating" ).val());
    	console.log('foodQualityRating: ', currentIndex, 'currentRating: ', currentRating, ' DOM element ', $el);
      console.log('index: ', currentIndex, 'currentRating: ', currentRating, ' DOM element ', $el);
      $('.live-rating').text(currentIndex);
    },
    onLeave: function(currentIndex, currentRating, $el){
      console.log('index: ', currentIndex, 'currentRating: ', currentRating, ' DOM element ', $el);
      $('.live-rating').text(currentRating);
    }
  });
  $(".my-rating-10").starRating({
    initialRating: 0,
    disableAfterRate: false,
    onClick: function(currentIndex, currentRating, $el){
    	customerServiceRating = currentIndex;
    	$( "#customerServiceRating" ).val(customerServiceRating);
    	
    	console.log("my-rating-10 : "+$( "#customerServiceRating" ).val())
      console.log('index: ', currentIndex, 'currentRating: ', currentRating, ' DOM element ', $el);
      $('.live-rating1').text(currentIndex);
    },
    onLeave: function(currentIndex, currentRating, $el){
      console.log('index: ', currentIndex, 'currentRating: ', currentRating, ' DOM element ', $el);
      $('.live-rating1').text(currentRating);
    }
  });
  $(".my-rating-11").starRating({
    initialRating: 0,
    disableAfterRate: false,
    onClick: function(currentIndex, currentRating, $el){
    	orderExperienceRating = currentIndex;
    	$( "#orderExperienceRating" ).val(orderExperienceRating);
    	console.log("my-rating-11")
      console.log('index: ', currentIndex, 'currentRating: ', currentRating, ' DOM element ', $el);
      $('.live-rating2').text(currentIndex);
    },
    onLeave: function(currentIndex, currentRating, $el){
      console.log('index: ', currentIndex, 'currentRating: ', currentRating, ' DOM element ', $el);
      $('.live-rating2').text(currentRating);
    }
  });
  $(".my-rating-12").starRating({
    initialRating: 0,
    disableAfterRate: false,
    onClick: function(currentIndex, currentRating, $el){
    	foodQualityRating = currentIndex;
    	$( "#foodQualityRating12" ).val(foodQualityRating);
    	console.log("my-rating-12 : "+$( "#foodQualityRating12" ).val())
      console.log('index: ', currentIndex, 'currentRating: ', currentRating, ' DOM element ', $el);
      $('.live-rating3').text(currentIndex);
    },
    onLeave: function(currentIndex, currentRating, $el){
      console.log('index: ', currentIndex, 'currentRating: ', currentRating, ' DOM element ', $el);
      $('.live-rating3').text(currentRating);
    }
  });
  $(".my-rating-13").starRating({
    initialRating: 0,
    disableAfterRate: false,
    onClick: function(currentIndex, currentRating, $el){
    	customerServiceRating = currentIndex;
    	$( "#customerServiceRating13" ).val(customerServiceRating);
    	console.log("my-rating-13 : "+$( "#customerServiceRating13" ).val())
      console.log('index: ', currentIndex, 'currentRating: ', currentRating, ' DOM element ', $el);
      $('.live-rating4').text(currentIndex);
    },
    onLeave: function(currentIndex, currentRating, $el){
      console.log('index: ', currentIndex, 'currentRating: ', currentRating, ' DOM element ', $el);
      $('.live-rating4').text(currentRating);
    }
  });
  $(".my-rating-14").starRating({
    initialRating: 0,
    disableAfterRate: false,
    onClick: function(currentIndex, currentRating, $el){
    	orderExperienceRating = currentIndex;
    	$( "#orderExperienceRating14" ).val(orderExperienceRating);
    	console.log("my-rating-14 : "+$( "#orderExperienceRating14" ).val())
      console.log('index: ', currentIndex, 'currentRating: ', currentRating, ' DOM element ', $el);
      $('.live-rating5').text(currentIndex);
    },
    onLeave: function(currentIndex, currentRating, $el){
      console.log('index: ', currentIndex, 'currentRating: ', currentRating, ' DOM element ', $el);
      $('.live-rating5').text(currentRating);
    }
  });
  $(".my-rating-15").starRating({
    initialRating: 0,
    disableAfterRate: false,
    onClick: function(currentIndex, currentRating, $el){
    	foodQualityRating = currentIndex;
    	$( "#foodQualityRating15" ).val(foodQualityRating);
    	console.log("my-rating-15 : "+$( "#foodQualityRating15" ).val())
      console.log('index: ', currentIndex, 'currentRating: ', currentRating, ' DOM element ', $el);
      $('.live-rating6').text(currentIndex);
    },
    onLeave: function(currentIndex, currentRating, $el){
      foodQualityRating = currentIndex;
      console.log('index: ', currentIndex, 'currentRating: ', currentRating, ' DOM element ', $el);
      $('.live-rating6').text(currentRating);
    }
  });
  $(".my-rating-16").starRating({
    initialRating: 0,
    disableAfterRate: false,
    onClick: function(currentIndex, currentRating, $el){
    	customerServiceRating = currentIndex;
    	$( "#customerServiceRating16" ).val(customerServiceRating);
    	console.log("my-rating-16 : "+$( "#customerServiceRating16" ).val())
      console.log('index: ', currentIndex, 'currentRating: ', currentRating, ' DOM element ', $el);
      $('.live-rating7').text(currentIndex);
    },
    onLeave: function(currentIndex, currentRating, $el){
    	customerServiceRating = currentIndex;
      console.log('index: ', currentIndex, 'currentRating: ', currentRating, ' DOM element ', $el);
      $('.live-rating7').text(currentRating);
    }
  });
  $(".my-rating-17").starRating({
    initialRating: 0,
    disableAfterRate: false,
    onClick: function(currentIndex, currentRating, $el){
    	orderExperienceRating = currentIndex;
    	$( "#orderExperienceRating17" ).val(orderExperienceRating);
    	console.log("my-rating-17 : "+$( "#orderExperienceRating17" ).val())
      console.log('index: ', currentIndex, 'currentRating: ', currentRating, ' DOM element ', $el);
      $('.live-rating8').text(currentIndex);
    },
    onLeave: function(currentIndex, currentRating, $el){
    	orderExperienceRating = currentIndex;
      console.log('index: ', currentIndex, 'currentRating: ', currentRating, ' DOM element ', $el);
      $('.live-rating8').text(currentRating);
    }
  });
  $(".my-rating-18").starRating({
    initialRating: 0,
    disableAfterRate: false,
    onClick: function(currentIndex, currentRating, $el){
    	foodQualityRating = currentIndex;
    	$( "#foodQualityRating18" ).val(foodQualityRating);
    	console.log("my-rating-18 : "+$( "#foodQualityRating18" ).val())
      console.log('index: ', currentIndex, 'currentRating: ', currentRating, ' DOM element ', $el);
      $('.live-rating9').text(currentIndex);
    },
    onLeave: function(currentIndex, currentRating, $el){
      console.log('index: ', currentIndex, 'currentRating: ', currentRating, ' DOM element ', $el);
      $('.live-rating9').text(currentRating);
    }
  });
$(".my-rating-19").starRating({
    initialRating: 0,
    disableAfterRate: false,
    onClick: function(currentIndex, currentRating, $el){
    	customerServiceRating = currentIndex;
    	$( "#customerServiceRating19" ).val(customerServiceRating);
    	console.log("my-rating-19 : "+$( "#customerServiceRating19" ).val())
      console.log('index: ', currentIndex, 'currentRating: ', currentRating, ' DOM element ', $el);
      $('.live-rating10').text(currentIndex);
    },
    onLeave: function(currentIndex, currentRating, $el){
      console.log('index: ', currentIndex, 'currentRating: ', currentRating, ' DOM element ', $el);
      $('.live-rating10').text(currentRating);
    }
  });
  $(".my-rating-20").starRating({
    initialRating: 0,
    disableAfterRate: false,
    onClick: function(currentIndex, currentRating, $el){
    	orderExperienceRating = currentIndex;
    	$( "#orderExperienceRating20" ).val(orderExperienceRating);
    	console.log("my-rating-20 : "+$( "#orderExperienceRating20" ).val())
      console.log('index: ', currentIndex, 'currentRating: ', currentRating, ' DOM element ', $el);
      $('.live-rating11').text(currentIndex);
    },
    onLeave: function(currentIndex, currentRating, $el){
      console.log('index: ', currentIndex, 'currentRating: ', currentRating, ' DOM element ', $el);
      $('.live-rating11').text(currentRating);
    }
  });
  $(".my-rating-30").starRating({
    initialRating: 0,
    disableAfterRate: false,
    onClick: function(currentIndex, currentRating, $el){
    	foodQualityRating = currentIndex;
    	$( "#foodQualityRating30" ).val(foodQualityRating);
    	console.log("my-rating-30 : "+$( "#foodQualityRating30" ).val())
      console.log('index: ', currentIndex, 'currentRating: ', currentRating, ' DOM element ', $el);
      $('.live-rating12').text(currentIndex);
      
    },
    onLeave: function(currentIndex, currentRating, $el){
      console.log('index: ', currentIndex, 'currentRating: ', currentRating, ' DOM element ', $el);
      $('.live-rating12').text(currentRating);
    }
  });
  $(".my-rating-31").starRating({
    initialRating: 0,
    disableAfterRate: false,
    onClick: function(currentIndex, currentRating, $el){
    	customerServiceRating = currentIndex;
    	$( "#customerServiceRating31" ).val(customerServiceRating);
    	console.log("my-rating-31 : "+$( "#customerServiceRating31" ).val())
      console.log('index: ', currentIndex, 'currentRating: ', currentRating, ' DOM element ', $el);
      $('.live-rating13').text(currentIndex);
    },
    onLeave: function(currentIndex, currentRating, $el){
      console.log('index: ', currentIndex, 'currentRating: ', currentRating, ' DOM element ', $el);
      $('.live-rating13').text(currentRating);
    }
  });
  $(".my-rating-32").starRating({
    initialRating: 0,
    disableAfterRate: false,
    onClick: function(currentIndex, currentRating, $el){
    	orderExperienceRating = currentIndex;
    	$( "#orderExperienceRating32" ).val(orderExperienceRating);
    	console.log("my-rating-32 : "+$( "#orderExperienceRating32" ).val())
      console.log('index: ', currentIndex, 'currentRating: ', currentRating, ' DOM element ', $el);
      $('.live-rating14').text(currentIndex);
    },
    onLeave: function(currentIndex, currentRating, $el){
      console.log('index: ', currentIndex, 'currentRating: ', currentRating, ' DOM element ', $el);
      $('.live-rating14').text(currentRating);
    }
  });
  
});


function checkRating1()
{
	$(".submit").css("disable");
	$( "#foodQualityRating" ).val(foodQualityRating);
	var rating1=$('.live-rating').text();
	var rating2=$('.live-rating1').text();
	var rating3=$('.live-rating2').text();
	$( "#foodQualityRating" ).val(rating1);
	$( "#customerServiceRating" ).val(rating2);
	$( "#orderExperienceRating" ).val(rating3);
	
//	if(parseFloat(rating1)>0 && parseFloat(rating2)>0 && parseFloat(rating3)>0)
		return true;

//	$("#id1").css({"display": "block"});
//	return false;
	
	}

function checkRating2()
{
	$(".submit").css("disable");
	var rating1=$('.live-rating3').text();
	var rating2=$('.live-rating4').text();
	var rating3=$('.live-rating5').text();
	$( "#foodQualityRating12" ).val(rating1);
	$( "#customerServiceRating13" ).val(rating2);
	$( "#orderExperienceRating14" ).val(rating3);
//	if(parseFloat(rating1)>0 && parseFloat(rating2)>0 && parseFloat(rating3)>0)
		return true;
	$("#id2").css({"display": "block"});
//	return false;
	}
function checkRating3()
{
	$(".submit").css("disable");
	var rating1=$('.live-rating6').text();
	var rating2=$('.live-rating7').text();
	var rating3=$('.live-rating8').text();
	$( "#foodQualityRating15" ).val(rating1);
	$( "#customerServiceRating16" ).val(rating2);
	$( "#orderExperienceRating17" ).val(rating3);
//	if(parseFloat(rating1)>0 && parseFloat(rating2)>0 && parseFloat(rating3)>0)
		return true;
//	$("#id3").css({"display": "block"});
 //  return false;
	
	}
function checkRating4()
{
	$(".submit").css("disable");
	var rating1=$('.live-rating9').text();
	var rating2=$('.live-rating10').text();
	var rating3=$('.live-rating11').text();
	$( "#foodQualityRating18" ).val(rating1);
	$( "#customerServiceRating19" ).val(rating2);
	$( "#orderExperienceRating20" ).val(rating3);
 //  if(parseFloat(rating1)>0 && parseFloat(rating2)>0 && parseFloat(rating3)>0)
		return true;
//	$("#id4").css({"display": "block"});
//	return false;
	
	}
	
function checkRating5()
{
	$(".submit").css("disable");
	var rating1=$('.live-rating12').text();
	var rating2=$('.live-rating13').text();
	var rating3=$('.live-rating14').text();
	$( "#foodQualityRating30" ).val(rating1);
	$( "#customerServiceRating31" ).val(rating2);
	$( "#orderExperienceRating32" ).val(rating3);
//	if(parseFloat(rating1)>0 && parseFloat(rating2)>0 && parseFloat(rating3)>0)
		return true;
//	$("#id5").css({"display": "block"});
//	return false;
	
	}
	
/* $(document).ready(function(){
$(".close").click(function(){
	$("#id1").css({"display": "none"});
	$("#id2").css({"display": "none"});
	$("#id3").css({"display": "none"});
	$("#id4").css({"display": "none"});
	$("#id5").css({"display": "none"});
  });
}); */


</script>

<script src="https://www.mkonnekt.com/kritiq/jquery.star-rating-svg.js"></script>


<script type="text/javascript">

$(document).ready(function() {
	var feedbackStatus = $("#feedbackStatus").val();
	var merchantKritiqExpiryStatus =$("#merchantKritiqExpiryStatus").val();

	if(merchantKritiqExpiryStatus =='disabled' || merchantKritiqExpiryStatus =='expired'){
		$("#feedbackFormEnabled").css('display','block');
	}else if(feedbackStatus== 'false'){
		$("#paragraph").css('display','block');
		$("#emojis").css('display','block');
	}else{
		$("#paragraph").css('display','none');
		$("#emojis").css('display','none');
		$("#alreadyGaveFeedback").css('display','block');
	}
});
</script>
</body>
</html>

