<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<jsp:include page="headerV2.jsp"></jsp:include>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<link rel="stylesheet" type="text/css" href="https://www.foodkonnekt.com/web/resources/V2/css/jquery.loadingimagethingy.css" />
<link href="https://www.foodkonnekt.com/web/resources/V2/css/fSelect.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="https://www.foodkonnekt.com/web/resources/V2/css/woco-accordion.css">
  <link href="https://www.foodkonnekt.com/web/resources/V2/css/woco-accordion.min.css" rel="stylesheet">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
      crossorigin="anonymous">
  <link href="https://www.foodkonnekt.com/web/resources/V2/css/font-awesome.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="https://www.foodkonnekt.com/web/resources/css/styles-min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
   <link rel="stylesheet" type="text/css" href="https://www.foodkonnekt.com/web/resources/css/multistep/style.css">
 <link rel="stylesheet" type="text/css" href="https://www.foodkonnekt.com/web/resources/css/style-min.css">
  <link rel="stylesheet" type="text/css" href="https://www.foodkonnekt.com/web/resources/css/pizza-modifier.css">
 <link rel="stylesheet" type="text/css" href="https://www.foodkonnekt.com/web/resources/css/pizza-modifier.css">
  <link rel="stylesheet" type="text/css" href="https://www.foodkonnekt.com/web/resources/css/userprofile.css">

        
<script src="https://www.foodkonnekt.com/web/resources/V2/js/jquery-1.11.3.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" crossorigin="anonymous"></script>
<!--  <script src="resources/V2/js/woco.accordion.js"></script> -->
<script src="https://www.foodkonnekt.com/web/resources/V2/js/woco.accordion.min.js"></script>
<script type="text/javascript" src="https://www.foodkonnekt.com/web/resources/js/ping-nose-popup/pignose.popup.min.js"></script>
<script src="resources/V2/js/forgotPasswordV2.js"></script>
<body>
<div class="content">
<div class="container-grey"> 
<!-- MultiStep Form -->
<div class="row">
		<div class="col-md-offset-1 col-md-8 col-sm-7">
	<%--  <form id="msform">  --%>
				<form:form method="POST" modelAttribute="Customer" action="saveResetGuestPasswordV2"
						id="msform" name="myform" class="foodkonnekt-form" autocomplete="off">
				<fieldset>
				
                <label id="loginError" class="error"
                          style="color: red; font-size: 1.77778rem; font-weight: normal; line-height: 1.5;"></label>
                
                <h3 style="color:  #F8981D;text-transform: uppercase;">Set Password</h3>
                
                <div class="outer-border">
                	<!-- <input type="password"  id="password" name="email" placeholder="Password" required="required"/> -->
				<form:password path="password" placeholder="password" id="password" />
				</div>
				
				<div class="outer-border">
                	<input type="password" id="confirmPassword" placeholder="Confirm Password"/>
				</div>
				
				<div class="small-12 medium-12 large-12 columns" style="display: none;">
					<label for="email">Hidden Password</label>
					<form:input path="id" placeholder="Enter password"
						id="id" />
				</div>
					    
					    
					     <input  type="submit" name="forgot" id="setPasswordButton" class="action-button-forgot" onclick="return checkPassword()" 
					     value="Set Password"/>
						
					</form:form>	</div></fieldset>
			<%-- </form> --%>

		
		</div><br><br><br><br><br><br><br><br>

</div>
</div>
<script type="text/javascript">
var merchantId="${sessionScope.merchant.id}";
</script>
</body>
</html>