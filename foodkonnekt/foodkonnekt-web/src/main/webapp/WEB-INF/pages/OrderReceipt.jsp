<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<meta name="viewport" content="width=device-width, initial-scale=1"/>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" crossorigin="anonymous"/>
<link rel="stylesheet" type="text/css" href="resources/orderReceipt/assets/css/styles.css"/>
<script src="resources/js/shop/accordion/jquery-1.11.3.min.js"></script>
</head>
<body>
   <div class="container">
      <div style="text-align: center; margin: 20px auto">
        
        <c:if test="${order.getMerchant().getId()==356}">
        <div class="padding-logo">
             <img src="https://www.foodkonnekt.com/foodkonnekt_merchat_logos/356_RomasItalianRestaurant.png" alt="">

         </div>
        
        </c:if>
        
        <c:if test="${order.getMerchant().getId()!=356}">
        <div class="padding-logo">
                  <c:if test="${order.merchant.merchantLogo == null}">
                	<img src="resources/V2/images/logo.png" alt="foodkonnekt  logo">
            	</c:if>
           
	     	<c:if test="${order.merchant.merchantLogo!=null}">
                <img src="https://www.foodkonnekt.com/${order.merchant.merchantLogo}" alt="foodkonnekt  logo">
            </c:if> 
         </div>
        
        </c:if>
        
        
         
         <div class="orange-bar"></div>
      </div>
      <div class="row content">
         <div class="col-md-12 col-sm-12">
            <div class="col-md-9 col-xs-6
            ">Order ID: ${order.id }</div>
            <div class="col-md-3 col-xs-6
             text-right tar">
               Date: 
               <fmt:formatDate pattern="MM/dd/yyyy" value="${order.createdOn }" />
            </div>
         </div>
      </div>
      <!--content ends here-->
		<div class="orange-line"></div>

		<div class="banner">
			<div class="row">
				<div class="col-md-5 col-sm-5 billing_addr">
					<p>
						<img src="${merchantLogo}" height="120px" width="120px"
							class="text-center" alt="" />
					<h4>
						<strong> ${order.merchant.name}</strong>
					</h4>
					<c:forEach var="address" items="${order.merchant.addresses}"
						end="0">
                  ${address.address1} <br> ${address.address2}<br />
                  ${address.city }, ${address.state }, ${ address.zip}
               </c:forEach>
					<br />
					</p>
				</div>
				<div class="col-md-2 col-sm-2"></div>
				<div class="col-md-5 col-sm-5 billing_addr">
					<h4>
						<strong>Billing</strong>
					</h4>
					<p>
						${order.customer.firstName}
						<c:if test="${order.customer.lastName !=null}"> ${order.customer.lastName}</c:if>
						
						
					<c:if test="${order.orderType== 'delivery' || order.orderType== 'Delivery'}">
						<br />
						<c:if test="${order.addressId ==null}">
						<c:forEach var="address" items="${order.customer.addresses}"
							end="0">
                   			 ${address.address1} <br> ${address.address2}<br>
				 				${address.city },${address.state },  ${ address.zip}
						</c:forEach>
						</c:if>
						
						<c:if test="${order.addressId !=null}">
							${order.address.address1} <br> ${order.address.address2}<br>
								 ${order.address.city }, ${order.address.state}, ${order.address.zip}
						</c:if>
						
					</c:if>
						<br />${order.customer.emailId}<br /> ${order.customer.phoneNumber}<br />
					</p>
				</div>
			</div>
		</div>

		<h2>order description</h2>
      <div class="row content1">
         <div class="col-md-12 col-sm-12">
            <div class="col-md-9 col-xs-6">ORDER TYPE: <font color="#f7941d" style="bold"><b> ${order.orderType} </b></font></div>
            <div class="col-md-3 col-xs-6 text-right">PAYMENT: <font color="#f7941d" ><b> ${order.paymentMethod} </b></font></div>
         </div>
      </div>
      <!-- content ends here-->
      <table class="table tablehead1">
         <thead>
            <tr>
               <th class="col-md-3 col-sm-3 text-left">Item</th>
               <th class="col-md-3 col-sm-3 text-center">Price</th>
               <th class="col-md-3 col-sm-3 text-center">Qty</th>
               <th class="col-md-3 col-sm-3 text-right" >Total</th>
            </tr>
         </thead>
         <tbody>   ${orderdetails}  

                <tr class="bottom">
                    <td class="text-left lft">Sub total</td>
                    <td></td>
                    <td></td>
                    <td class="text-right rht">$<fmt:formatNumber type="currency" pattern="#####.##" minFractionDigits = "2"
                            value="${order.subTotal}" /></td>
                </tr>

				<c:if test="${order.convenienceFee>0.0}">
                	<tr class="bottom">
                    <span style="display: none">
                    <fmt:formatNumber type="currency" minFractionDigits = "2"
                            value="${order.convenienceFee}" /></span>
                        <td class="text-left lft">Online Fee</td>
                        <td></td>
                        <td></td>
                        <c:if test="${order.convenienceFee!='' && order.convenienceFee!=null}">
                            <td class="text-right rht"><fmt:formatNumber type="currency" minFractionDigits = "2"
                            value="${order.convenienceFee}" /></td>
                        </c:if>
                	</tr>
                 </c:if>

				<c:if test="${order.orderType=='delivery' || order.orderType=='Delivery' && order.deliveryFee>0}">
                    <tr class="bottom">
                        <td class="text-left lft">Delivery Fee</td>
                        <td></td>
                        <td></td>
                        <td class="text-right rht">$<fmt:formatNumber type="currency" pattern="#####.##" minFractionDigits = "2"
                            value="${order.deliveryFee}" /></td>
                    </tr>
                </c:if>

				<c:if test="${order.tax > 0.0}">
                <tr class="bottom">
                    <td class="text-left lft">Tax</td>
                    <td></td>
                    <td></td>
                    <td class="text-right rht">$<fmt:formatNumber type="currency" pattern="#####.##" minFractionDigits = "2"
                            value="${order.tax}" /></td>
                </tr>
                </c:if>
                
                <c:if test="${order.tipAmount > 0.0}">
				<tr class="bottom">
                    <td class="text-left lft">Tip</td>
                    <td></td>
                    <td></td>
                    <td class="text-right rht">$<fmt:formatNumber type="currency" pattern="#####.##" minFractionDigits = "2"
                            value="${order.tipAmount}" /></td>
                </tr>
				</c:if>
               <%--  <c:if test="${order.tipAmount > 0.0}">
                <tr class="bottom">
                    <td class="text-left lft">Tip</td>
                    <td></td>
                    <td></td>
                    <td class="text-right rht">$<fmt:formatNumber type="currency" pattern="#####.##" minFractionDigits = "2"
                            value="${order.tipAmount}" /></td>
                </tr>
                </c:if> --%>

 				<c:if test="${order.orderDiscount>0.0 || order.orderDiscount>0}">
                <tr class="bottom">
                  
                    <td class="text-left lft">Discount</td>
                    <td></td>
                    <td></td>
                    <td class="text-right rht">$<fmt:formatNumber type="currency" minFractionDigits = "2"
                            value="${order.orderDiscount}" /></td>
                   
                </tr> 
                </c:if>

               <%--  <c:if test="${! empty order.orderDiscountsList}">
                    <tr class="bottom">
                        <td class="text-left lft">Discount Coupon</td>
                        <td></td>
                        <td></td>
                        <c:forEach items="${order.orderDiscountsList}" var="orderDiscount" varStatus="status1">
                            <td class="text-right rht">${orderDiscount.couponCode}</td>
                        </c:forEach>

                    </tr>
                </c:if> --%>
                
                <tr class="bottom">
                    <td class="text-left lft">Total</td>
                    <td></td>
                    <td></td>
                    <td class="text-right rht">$ <fmt:formatNumber type="currency" pattern="#####.##" minFractionDigits = "2"
                            value="${order.orderPrice}" /></td>
                </tr>
         </tbody>
      </table>
      <div class="row bttn">
         <%-- <button type="button" class="btn btn-success " onclick="window.location='${kritiqUrl}/kritiq/feedbackForm?customerId=${customerId}&orderId=${orderId}'">COMPLETE THE SURVEY</button> --%>
        <button type="button" class="btn btn-success " onclick="window.location='${kritiqUrl}/kritiq/customerFeedbackV2?customerId=${customerId}&orderId=${orderId}'">Share Your Feedback</button>
      </div>
      <div class="row">
         <div class="copyrights">
            <p>copyrights</p>
         </div> 	
      </div>
   </div>
  

   
   

