<%@ page language="java" contentType="text/html; charset=ISO-8859-1" isELIgnored="false"
   pageEncoding="ISO-8859-1"%>
   <%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
   <head>
   
    <style>
   /* The Modal (background) */
    .modal {
        display: none;
        /* Hidden by default */
        position: fixed;
        /* Stay in place */
        z-index: 1;
        /* Sit on top */
        padding-top: 100px;
        /* Location of the box */
        left: 0;
        top: 0;
        width: 100%;
        /* Full width */
        height: 100%;
        /* Full height */
        overflow: auto;
        /* Enable scroll if needed */
        background-color: rgb(0, 0, 0);
        /* Fallback color */
        background-color: rgba(0, 0, 0, 0.4);
        /* Black w/ opacity */
    }
    /* Modal Content */
    
    .modal-content {
        position: relative;
        background-color: #fefefe;
        margin: auto;
        padding: 0;
        border: 1px solid #888;
        width: 80%;
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
        -webkit-animation-name: animatetop;
        -webkit-animation-duration: 0.4s;
        animation-name: animatetop;
        animation-duration: 0.4s
    }
    /* Add Animation */
    
    @ -webkit-keyframes animatetop {
        from {
            top: -300px;
            opacity: 0
        }
        to {
            top: 0;
            opacity: 1
        }
    }
    
    @ keyframes animatetop {
        from {
            top: -300px;
            opacity: 0
        }
        to {
            top: 0;
            opacity: 1
        }
    }
    /* The Close Button */
    
    .close {
        color: white;
        float: right;
        font-size: 28px;
        font-weight: bold;
    }
    
    .close:hover,
    .close:focus {
        color: #000;
        text-decoration: none;
        cursor: pointer;
    }
    
    .modal-header {
        padding: 2px 16px;
        background-color: #ff9936;
        color: white;
    }
    
    .modal-body {
        padding: 2px 16px;
    }
    
     .modal-footer {
    padding: 2px 16px;
    background-color: #fafafa;
    color: white;
} 
  </style>


<script src="resources/js/shop/accordion/jquery-1.11.3.min.js"></script>
<script src="resources/js/shop/accordion/woco.accordion.min.js"></script>
<script src="resources/js/popModal.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>



<script type="text/javascript">

         $(document).ready(function() {
         	var tax = "${order.tax}";
         	var subtotal = "${order.subTotal}";
         	var discount = "${order.orderDiscount}";
         
         	tax = Number(tax).toFixed(2);
         	if (tax == "") {
         		tax = "0.0";
         	}
         	if (subtotal == "") {
         		subtotal = "0.0";
         	}
         	if (discount == "") {
         		discount = "0.0";
         	}
         	subtotal = Number(subtotal).toFixed(2);
         	document.getElementById("tax").innerHTML = "$" + tax;
         	document.getElementById("subtotal").innerHTML = "$" + subtotal;
         	document.getElementById("discount").innerHTML = "$" + discount;
         	var convFee = "0.0";
         	var conv = "${order.convenienceFee}";
         	if (conv == '') {
         		conv = convFee;
         	}
         
         	document.getElementById("convFee").innerHTML = "$" + conv;
         });
      </script>
      <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
      <title>Insert title here</title>
   </head>
   <body>
      <div
         style='width: 700px; height: auto; border: solid 1px #ddd; box-shadow: 0px 0px 6px #ddd; background-color: #f2f2f2; text-align: center; margin: 40px 0px 0px 35px; margin-left: 25%;'>
         <div
            style='width: 700px; height: 85px; padding: 20px 0px; text-align: center;'>
            &nbsp; <img src='${merchantLogo }' height='80' />
         </div>
         <div
            style='background-color: #fff; padding: 20px 0px 0px 0px; width: 90%; margin: 0 auto 65px auto;'>
            <!-- <p style='font-size: 20px; color: #e6880f;'>Dear " + name + ",</p>
               <p style='font-size: 14px;'>Thank you. We have received your order and are in the process of&nbsp;confirming it.&nbsp;</p> -->
            <div style='text-align: left; padding: 20px 20px;'>
               <table>
                  <tbody>
                  	 <tr>
                        <td width='230'>Customer Name</td>
                        <td>:</td>
                        <td>${order.customer.firstName}</td>
                     </tr>
                     <tr>
                        <td width='230'>Order type</td>
                        <td>:</td>
                        <td>${order.orderType}</td>
                     </tr>
                     <tr>
                        <td>Order ID</td>
                        <td>:</td>
                        <td>${order.orderPosId}</td>
                     </tr>
                     <tr>
                        <td>Payment</td>
                        <td>:</td>
                        <td>${order.paymentMethod}</td>
                     </tr>
                  </tbody>
               </table>
               <p>
                  <strong>Order Details:</strong><br />
               </p>
               <table>${orderdetails}
               </table>
               <p style='margin: 0;'>-------------------------------------------------------------------------------</p>
               <table>
                  <tbody>
                     <tr>
                        <td width='230'>Sub-total</td>
                        <td width='100px' ; style="text-align: center"></td>
                        <td id="subtotal"></td>
                     </tr>
                     <tr>
                        <td>Convenience Fee</td>
                        <td>&nbsp;</td>
                        <td id="convFee"></td>
                     </tr>
                     <!-- 		
                        if (orderType.toLowerCase().equals("delivery")) { 
                        msg = msg + "<tr>" + "<td>Delivery Fee</td>" + "<td>&nbsp;</td>" + "<td>$" + deliverFee + "</td>"
                        		+ "</tr>";
                        
                        }
                        
                        if (tipAmount!=null && tipAmount>0) {
                        msg = msg + "<tr>" + "<td>Tip</td>" + "<td>&nbsp;</td>" + "<td>$" + tipAmount + "</td>"
                        		+ "</tr>";
                        
                        } -->
                     <tr>
                        <td>Discount</td>
                        <td>&nbsp;</td>
                        <td id="discount"></td>
                     </tr>
                     <tr>
                        <td>Tax</td>
                        <td>&nbsp;</td>
                        <td id="tax"></td>
                     </tr>
                  </tbody>
               </table>
               <p style='margin: 0;'>-------------------------------------------------------------------------------</p>
               <table>
                  <tbody>
                     <tr>
                        <td width='230'>Total</td>
                        <td width="100px;" style="text-align: center">&nbsp;</td>
                        <td>$${order.orderPrice}</td>
                     </tr>
                  </tbody>
               </table>
               <br />
               <br /> <strong style='text-decoration: underline;'>Special
               Instructions :</strong> note <br />
               <br />
                
                <c:if test= "${order.isDefaults == 0}">
               <strong style='text-decoration: underline;'>Status :</strong> Pending <br />
               </c:if>
                <c:if test= "${order.isDefaults == 1}">
               <strong style='text-decoration: underline;'>Status :</strong> Accept <br />
               </c:if>
                <c:if test= "${order.isDefaults == 2}">
               <strong style='text-decoration: underline;'>Status :</strong> Cancel <br />
               </c:if>
                <c:if test= "${order.isDefaults == 3}">
               <strong style='text-decoration: underline;'>Status :</strong> Failed <br />
               </c:if>
               <br />
               
				<c:if test= "${order.isDefaults == 0}">
               <div id="accept-decline">                     
               <a style='text-decoration: none; margin: 16px 0px; display: inline-block; background-color: #f8981d; color: #fff; padding: 10px 12px; border-radius: 10px; font-weight: 600; font-size: 15px;' href='#' onclick="popupOpen('Accept')">Accept</a>
               <a style='text-decoration: none; margin: 16px 100px; display: inline-block; background-color: #f8981d; color: #fff; padding: 10px 12px; border-radius: 10px; font-weight: 600; font-size: 15px;' href='#' onclick="popupOpen('Decline')">Decline</a><br /> 
               </div>
               </c:if>
               
               <div id="myModal" class="modal">
                    <!-- Modal content -->
                    <div class="modal-content" style="width: 48%">
                        <div class="navbar navbar-default">
                            <span class="close">&times;</span>
                            <h2 class="modal-title">Order</h2>
                        </div>
                        <div class="modal-body">
                            <p>Change Delivery Time</p>
							<input type="number" id="changeTime"
								name="ccNumber"
								placeholder="Enter Time" class="numberInput" maxlength="3" >
								
								<input type="hidden" id="orderId"
								name="orderId" value="${order.id}">
						</div>
                        <div class="modal-footer">
                          <%--   <a style='text-decoration: none; margin: 16px 0px; display: inline-block; background-color: #f8981d; color: #fff; padding: 10px 12px; border-radius: 10px; font-weight: 600; font-size: 15px;' href="" onclick="href='localhost:8080/foodkonnekt-admin/orderConfirmationById?orderId=${order.id}&type=Accept&changedOrderAvgTime='+document.getElementById('changeTime').value">Save/Continue</a> --%>
                            <a style='text-decoration: none; margin: 16px 0px; display: inline-block; background-color: #f8981d; color: #fff; padding: 10px 12px; border-radius: 10px; font-weight: 600; font-size: 15px;'  id='acceptLink' target="#" onclick="acceptOrder('Accept')" href="#">Save/Continue</a>
                            <a style='text-decoration: none; margin: 20px 0px; display: inline-block; background-color: #f8981d; color: #fff; padding: 10px 12px; border-radius: 10px; font-weight: 600; font-size: 15px;' href='#' onclick="popupClose()">Cancel</a>
                        </div>
                    </div>
                </div>
					<div class="pj-loader-3"
					style="display: none; margin-left: 25%; margin-top: -10%;">
					<img src="resources/img/load2.gif"
						style="width: 82px; margin-left: 117px; margin-top: 63px;">
				</div>
   				<div id="declineModal" class="modal">
                    <!-- Modal content -->
                    <div class="modal-content" style="width: 48%">
                        <div class="navbar navbar-default">
                            <span class="close">&times;</span>
                            <h2 class="modal-title">Order</h2>
                        </div>
                        <div class="modal-body">
                            <p>Reason To Decline..</p>
							<input type="checkbox" name="reason[]" value="Kitchen is out of Stock">Kitchen is out of Stock<br/>
							<input type="checkbox" name="reason[]" value="Kitchens hours are open">Kitchens hours are open<br/>
							<input type="checkbox" name="reason[]" value="We are not delivering today">We are not delivering today<br/>
							<input type="checkbox" name="other" value="other" id='other' onclick="showOtherTextArea()">Other<br/>
							<input type="text" id="otherTextBox"
								name="otherText"
								placeholder="Enter Text" style="display: none;">
						</div>
                        <div class="modal-footer">
                          <%--   <a style='text-decoration: none; margin: 16px 0px; display: inline-block; background-color: #f8981d; color: #fff; padding: 10px 12px; border-radius: 10px; font-weight: 600; font-size: 15px;' href="" onclick="href='localhost:8080/foodkonnekt-admin/orderConfirmationById?orderId=${order.id}&type=Accept&changedOrderAvgTime='+document.getElementById('changeTime').value">Save/Continue</a> --%>
                            <a style='text-decoration: none; margin: 16px 0px; display: inline-block; background-color: #f8981d; color: #fff; padding: 10px 12px; border-radius: 10px; font-weight: 600; font-size: 15px;'  id='acceptLink' target="#" onclick="acceptOrder('Decline')" href="#">Continue</a>
                            <a style='text-decoration: none; margin: 20px 0px; display: inline-block; background-color: #f8981d; color: #fff; padding: 10px 12px; border-radius: 10px; font-weight: 600; font-size: 15px;' href='#' onclick="popupClose()">Cancel</a>
                        </div>
                    </div>
                </div>


				<div style='height: 70px; text-align: center;'>
                  &nbsp;<img src='http://www.dev.foodkonnekt.com/app/foodnew.jpg'
                     alt='' width='200' height='63' />
               </div>
            </div>
         </div>
      </div>
      
      <script type="text/javascript">
      
      /*  $(document).ready ( function(){
    	 // $("#accept-decline").css("display","none");
    	   $("#myModal").css("display","none");
      });  */
      function popupOpen(type){
    	  if(type == 'Accept'){
    	 	 $("#myModal").css("display","block");
    	  }else{
    		  $("#declineModal").css("display","block");
    	  }
      }
      
      var span = document.getElementsByClassName("close")[0];
      
      // When the user clicks on <span> (x), close the modal
      span.onclick = function() {
          document.getElementById('myModal').style.display = "none";
          document.getElementById('declineModal').style.display = "none";
      }

      // When the user clicks anywhere outside of the modal, close it
      window.onclick = function(event) {
          if (event.target == document.getElementById('myModal')) {
              document.getElementById('myModal').style.display = "none";
          }
       
          if (event.target == document.getElementById('declineModal')) {
              document.getElementById('declineModal').style.display = "none";
          }
      }
      
      function popupClose(){
    	  document.getElementById('myModal').style.display = "none";
    	  $("#declineModal").css("display","none");
      }
      
	 function showOtherTextArea(){
		 var ch = document.getElementById('other').checked;
		 if(ch){
		 	$("#otherTextBox").css("display","block");
		 }else{
			 $("#otherTextBox").css("display","none");
		 }
	 }     
	 function acceptOrder(type) {
		 $(".pj-loader-3").css("display","block");
		var changeTime = document
			.getElementById('changeTime').value;
		var orderId = document
			.getElementById('orderId').value;
		var reason = "";
		if(type == 'Accept'){
			reason = "Order accepted by merchnat.";
		}else{
			
			 var inputElements = document.getElementsByName('reason[]');
			for(var i=0; i<inputElements.length; ++i){
			      if(inputElements[i].checked){
			    	  reason +=inputElements[i].value+", ";
			      }
			} 
			reason += document.getElementById("otherTextBox").value;
			alert(reason);
		}
	
			$.ajax({
				url : "orderAcceptAndDeclineByReceipt?orderId="+ orderId+ "&type="+ type  +"&changedOrderAvgTime="+ changeTime+"&reason="+reason,
				type : "GET",
				contentType : "application/json; charset=utf-8",
				success : function(status) {
					if(status == "success"){
						$(".pj-loader-3").css("display","none");
					location.reload();
					}else{
						alert("something went wrong. try again..!");
					}
										   }
				});
		
	}
		</script>
   </body>
</html>