<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="resources/css/woco-accordion-min.css"/>
      <link rel="stylesheet" type="text/css" href="resources/css/bootstrap.min.css"/>
    
<link rel="stylesheet" type="text/css" href="resources/css/normalize-min.css">
     <!--  <link rel="stylesheet" type="text/css" href="resources/font-awesome/css/font-awesome.min.css"/>
      <link rel="stylesheet" type="text/css" href="resources/css/font-awesome.min.css"/>  -->
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">      
       <!-- <link rel="stylesheet" type="text/css" href="resources/css/pizza-modifier-min.css"/> -->
       <link rel="stylesheet" type="text/css" href="resources/css/pizza-modifier.css"/>
      <link rel="stylesheet" type="text/css" href="resources/css/styles-min.css"/>
       <link rel="stylesheet" type="text/css" href="resources/js/fSelect.css"/>



<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="https://www.foodkonnekt.com/web/resources/V2/css/styles.css"> -->

 <c:if test="${gaScript!= null}">
	<script async src="https://www.googletagmanager.com/gtag/js?id=${gaScript}"></script>
          </c:if>
	
  <c:if test="${gaScript!= null}">
	<script>
             window.dataLayer = window.dataLayer || [];
             function gtag(){dataLayer.push(arguments);}
              gtag('js', new Date());

              gtag('config', '${gaScript}');
            </script>      
</c:if>

</head>
<body>
    <div class="header" id="myHeader">
        <div class="container">
             <div class="col-md-8 col-sm-3  fd-logo">
            <c:if test="${merchant.merchantLogo == null}">
                <img src="resources/img/logo.png" alt="foodkonnekt  logo" class="top-logo">
            </c:if>
            <c:if test="${merchant.merchantLogo!=null}">
                <img src="https://www.foodkonnekt.com/${merchant.merchantLogo}" alt="foodkonnekt  logo" class="top-logo">
            </c:if>
            </div>
            <!--logo ends here-->
    
            <div class="col-md-4 col-sm-9 ">
	     <div class="icon">
                  
		  
		    <c:if test="${sessionScope.merchant.id != null && sessionScope.vendor.id != null}">
                      <a href="orderV2?merchantId=${sessionScope.merchant.id}&vendorId=${sessionScope.vendor.id}">
				                       <span class="fa-stack fa-2x" aria-hidden="true"> 
				                  <i class="fa fa-cutlery fa-stack-1x fa-inverse" id="btn1"></i>
				                  </span></a>
                    </c:if>
                    <c:if test="${sessionScope.merchant.id != null && sessionScope.vendor.id == null}">
				                        <a href="orderV2?merchantId=${sessionScope.merchant.id}">
				                        <span class="fa-stack fa-2x" aria-hidden="true" >
				                  <i class="fa fa-cutlery fa-stack-1x fa-inverse menu-bar" id="btn1option"></i>
				                  </span></a>  </c:if>
				                  
				                  <a href="infoV2"><span class="fa-stack fa-2x" aria-hidden="true">
				    				<i class="fa fa-info fa-stack-1x fa-inverse menu-bar" id="btn2"></i>
				                  </span></a>
				                  
				                  <a href="cartV2"><span class="w3-badge" id="cartCount">${sessionScope.orderItems.size() }</span>
				                   <span class="fa-stack fa-2x" aria-hidden="true">
								  <i class="fa fa-shopping-cart fa-stack-1x fa-inverse menu-bar"   id="btn3"></i></span></a> 
								  
				                  <a href="#" onclick="checkGuestCustomer()"><span class="fa-stack fa-2x" aria-hidden="true">
				                  <i class="fa fa-user-o fa-stack-1x fa-inverse menu-bar" id="btn4"></i> 
				                 </span></a>
				</div> 
				
               <div class="clear"></div>
            </div>

        </div>
        <!--logo-nav ends here-->
        <div class="container-fluid  blue-bar">
            <div class="container container-size">	
        </div>
    </div>
      </div>
    
        <div id="createPasswordPopup1" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <button type="button" class="btn bttn" data-dismiss="modal" style="width:30px;">X</button>
                <div class="modal-header" style="text-align:center"><br>
<br>
                    <img src="resources/V2/images/logo.png" alt="" align="center"><br><br>
                </div>
                <div class="modal-body">
                    <p style="text-align:center;">

                        Do you want to create an account so that you can keep track of the order?<br> <br>
                    </p>
                </div>
                <div class="modal-footer">
                    <br>
                    <button type="button" class="btn yes-btn" data-dismiss="modal" onclick="addPasswordGestUser1('YES')">Proceed</button>
                    <button type="button" class="btn no-btn" data-dismiss="modal" onclick="addPasswordGestUser1('No')">Skip</button>
                </div>
            </div>
      	 </div>
    	</div>

    	
    	<script type="text/javascript">
    	var merchantId="${sessionScope.merchant.id}";
    	if(merchantId==546 || merchantId==624)
    		{
    		var elements = document.getElementsByClassName('blue-bar'); // get all elements
    		for(var i = 0; i < elements.length; i++){
    			elements[i].style.backgroundColor = "black";
    		}
    		elements = document.getElementsByClassName('menu-bar'); // get all elements
    		 for(var i = 0; i < elements.length; i++){
    			elements[i].style.backgroundColor = "red";
    		} 
    	//	document.getElementsByClassName('blue-bar').style.backgroundColor = "black";
    		}
    	 var guestCustomerPassword = "${sessionScope.guestCustomer.password}";
    	var guestCustomerEmailId = "${sessionScope.guestCustomer.emailId}"; 
    	//if(guestCustomerPassword==null ||guestCustomerPassword=='')
   // $("#sessionSpan").empty();
    var guestSignUpFlag=0;
    var menuPage = "orderV2?merchantId=${sessionScope.merchant.id}";
    function checkGuestCustomer(){
    	if((guestCustomerEmailId!='' && guestCustomerPassword=='') || guestSignUpFlag==1){
    			$("#createPasswordPopup1").modal('show');
				
    			}else{
    	    		window.location.href="userProfileV2";
    	    }
    	}
    	
    function addPasswordGestUser1(data) {
        if (data == 'YES') {
        	guestSignUpFlag=0;
        	window.location.href = "setGuestPasswordV2";
        } else {
        	guestSignUpFlag=0;
            window.location.href = "logoutV2";
        }
    }
    </script>
    	
    	
