<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<jsp:include page="headerV2.jsp"></jsp:include>
<script src="https://code.jquery.com/jquery-1.11.3.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css"/>
<script type="text/javascript">
    var csstype="inline";
    var
        mac_css='.accordion h1{padding-top:5px;}.menu_desc h3{margin-top:20px;}.menu_desc h4{margin-top:20px;} .form_head{padding-top:5px;}' ;
    var pc_css='.accordion h1{padding-top:5px;}' ;
    var mactest=navigator.userAgent.indexOf("Mac")!=-1;
    if (csstype=="inline"){
        document.write('<style type="text/css">')
        if (mactest)
            document.write(mac_css)
        else
            document.write(pc_css)
        document.write('</style>')
    }

</script>

<div class="clear"></div>
<div class="container homemenu " id="menuPage">
    <div class="banner content">
        <!-- Carousel -->
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
            <!--  Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                <li data-target="#carousel-example-generic" data-slide-to="2"></li>
            </ol>
            <!--  Wrapper for slides -->
            <div class="carousel-inner">

                <c:if test= "${merchantSliderList.size()>0}">
                    <c:forEach items="${merchantSliderList}" var="merchantSlider" varStatus="sliderLoop">

                        <c:if test="${sliderLoop.first}">
                            <div class="item active">
                                <img src="${url}${merchantSlider.sliderImage}" alt="first slide">
                            </div>
                        </c:if>

                        <c:if test="${!sliderLoop.first}">
                            <div class="item">
                                <img src="${url}${merchantSlider.sliderImage}" alt="Second slide">
                            </div>
                        </c:if>

                    </c:forEach>
                </c:if>

                <c:if test="${merchantSliderList.size()==0}">
                    <div class="item active">
                        <img src="resources/img/banner.jpg" alt="" class="img-responsive">
                    </div>
                    <div class="item">
                        <img src="resources/img/banner.jpg" alt="" >
                    </div>

                    <div class="item">
                        <img src="resources/img/banner.jpg" alt="" >
                    </div>
                </c:if>


            </div>
            <!-- Controls -->
            <a class="left carousel-control left-arrow" href="#carousel-example-generic" data-slide="prev">
                <span><i class="fa fa-chevron-left" aria-hidden="true"></i></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control left-arrow" href="#carousel-example-generic" data-slide="next">
                <span><i class="fa fa-chevron-right" aria-hidden="true"></i></span>
                <span class="sr-only">Next</span>
            </a>
        </div><!-- /carousel -->
    </div>
</div>



<div class="container row col-md-12 col-sm-12">
    <div class="text-center red" id="orderMsgSpanId"></div>
</div>
<div class="container ">
    <div class="row">
        <div class="col-md-12 col-sm-12 padding-mob" style="padding-left:0;">
            <div class="accordion">

                <!-----------------------------------------MULTIPAY MERCHANT-------------------------------------------------->

                <c:if test="${ merchantList.size() > 1}">
                <h1 class="categchangeMerchant" id="changeLocation" style="background-color: '#68512D';">Select
                    Location</h1>
                <div class="row">
                    <div class="col-md-6 col-sm-6 ">
                        <select id="changeMerchantLocation">
                            <c:forEach items="${merchantList}" var="view" varStatus="status">
                                <c:choose>
                                    <c:when test="${merchantId ==view.id}">
                                        <option value="${view.id}:${view.name}" selected>${view.name}</option>
                                    </c:when>
                                    <c:otherwise>
                                        <option value="${view.id}:${view.name}">${view.name}</option>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>
                        </select>
                    </div>
                </div>
            </div>
            </c:if>

            <!-- -------------------------------------------------------------------------------------------------------- -->
            <c:forEach items="${categories}" var="view" varStatus="status">
                <c:if test="${view.categoryStatus == 0}">
                    <%--<h1 class="categAjaxCall" idAttr="${view.id}" id='cat_${view.id}'>${view.categoryName}</h1>   --%>
                    <div style="background: #fff !important;" class="categAjaxCall" idAttr="${view.id}"
                         isPizza="${view.isPizza}" id='cat_${view.id}'>
                        <h1 id="catName">${view.categoryName}</h1>

                        <div>
                            <div class="row">

                                <input type="hidden" id="${view.id}" value="${view.categoryImage}">

                                <div class="col-md-6 col-md-6" >
                                    <c:if test="${view.categoryImage!=null}">
                                        <div class="menu_imgdsc1">
                                            <div class="menu_large">


                                                <c:if
                                                        test="${view.categoryImage!=null}">
                                                    <img src="https://www.foodkonnekt.com/${view.categoryImage}">
                                                </c:if>

                                            </div>
                                        </div>
                                    </c:if>
                                    <div id="right${view.id}"></div>
                                </div>
                                <div class="col-md-6 col-md-6" >
                                    <div id="left${view.id}"></div>
                                </div>
                                <!-- left half ends here-->
                            </div>
                            <!-- accordion row ends here-->
                        </div>
                    </div>

                    <!-- accordion div ends here-->
                </c:if>
            </c:forEach>




            <!-- ------------------------------------------------------------------------------------------------------------ -->



            <!-- ----------------------------------------------------------- Close Today modal ------------------------------------------------ -->

        </div>
    </div>
</div>
</div>
</div>


<div id="myModal1" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <button type="button" class="btn" data-dismiss="modal">X</button>
            <div class="modal-header"><img src="resources/V2/images/logo.png"	alt=""	></div>
            <!--  <img src="resources/V2/images/logo.png"> -->

            <div class="modal-body">
                <p>We are closed now. Please place your order during our regular operating hours<br>
                    <br>
                    <lable class="text-center" id="openingClosingHours"></lable>
                </p>
            </div>

            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>

<div id="myModal12" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <button type="button" class="btn" data-dismiss="modal" style="width:40px;">X</button>
            <div class="modal-header" >
                &nbsp;
            </div>

            <div class="modal-body" style="text-align:center">
                <br/>
                <img src="resources/V2/images/logo.png" alt="">
                <br/><br/><br/>
                <p>Our online ordering platform is currently closed. Please call the store to place an order<br>
                    <br>
                </p>

            </div>
            <div class="modal-footer">
                <div class="row bttn"  >
                    <br>
                    <button type="button" class="btn" data-dismiss="modal">close</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="IEdisable" role="dialog">
    <div class="modal-dialog">

        <div class="modal-content">
            <div class="modal-header">
                <img src="resources/V2/images/logo.png" alt="">
            </div>
            <div class="modal-body">
                <center><p><b>Our Website is not support this Browser.</b></p>
                    <p><b>Please use Google Chrome/Safari Browser/Firefox or any other Browser.</b></p></center>
            </div>
            <div class="modal-footer">
                <a class="btn" style="background: #f8981d" href="orderV2?merchantId=${sessionScope.merchant.id}"
                   role="button">Close</a>
            </div>
        </div>
    </div>
</div>

<div id="myModal13" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <button type="button" class="btn" data-dismiss="modal" style="width:40px;">X</button>
            <div class="modal-header" ></div>

            <div class="modal-body" style="text-align:center">
                <br/>
                <img src="resources/V2/images/logo.png" alt="">
                <br/><br/><br/>
                <p>You can not unselect the included one.<br>
                    <br>
                </p>

            </div>
            <div class="modal-footer">
                <div class="row bttn"  >
                    <br>
                    <button type="button" class="btn" data-dismiss="modal">close</button>
                </div>
            </div>
        </div>
    </div>
</div>


<div id="myModal20" class="modal fade" id="IEdisable" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <button type="button" class="btn buttonClose"   style="width:40px;">X</button>
            <div class="modal-header" ></div>

            <div class="modal-body" style="text-align:center">
                <br/>
                <img src="resources/V2/images/logo.png" alt="">
                <br/><br/><br/>
                <p id="message"></p><br>
                <br>


            </div>
            <div class="modal-footer">
                <div class="row bttn"  >
                    <br>
                    <button type="button" style="margin-right: 226px;" class="btn buttonClose btn-warning"  >Close
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- ----------------------------------------------------------- Item Modifier modal ------------------------------------------------ -->


<div id="myModal2" class="modal fade" role="dialog"></div>

<div id="myModal" class="modal fade" role="dialog"></div>

<script type="text/javascript">
    function addPasswordGestUser1(data) {
        if (data == 'YES') {
            guestSignUpFlag=0;
            window.location.href = "setGuestPasswordV2";
        } else {
            guestSignUpFlag=0;
            window.location.href = "logoutV2";
        }
    }
</script>
<script src="resources/V2/js/menu.js"></script>

<script src="resources/js/bootstrap.min.js"></script>
<script src="resources/js/woco.accordion.min.js"></script>
<script src="resources/V2/js/fSelect.js"></script>
<script type="text/javascript" src="https://www.foodkonnekt.com/foodkonnekt-web/resources/V2/js/jquery.loadingimagethingy.js"></script>
<script type="text/javascript" src="resources/js/ping-nose-popup/pignose.popup.min.js"></script>

<script>
    var merchantId = "${sessionScope.merchant.id}";
    var manageAccordinStates = 0;
    var timingStatus = 'Y';
    var sessionCutId = '${sessionCustId}';
    var allowFutureOrder = "${allowFutureOrder}";
    var categoryOnloadId = "${categories[0].id}";
    var isPizza = "${categories[0].isPizza}";
    var merchantListLength = "${merchantList.size()}";
    var fontStyle=(merchantId==624?"style='font-weight: bold'":"");
    var isfutureOrder = "${sessionScope.merchant.allowFutureOrder}";
    loadCategory(categoryOnloadId);

    function checkGuestCustomerOnMenu(){
        if((guestCustomerEmailId!='' && guestCustomerPassword=='') || guestSignUpFlag==1){
            /* $("#createPasswordPopup1").modal('show'); */
        }
    }
    checkGuestCustomerOnMenu();
    (function($) {

        $(function() {
            $('.toppings').fSelect();
        });
        $(function() {
            $('.sauce').fSelect();
        });
        $(function() {
            $('.salad').fSelect();
        });
    })(jQuery);


</script>




<script type="text/javascript">
    var csstype="inline";
    var
        mac_css='.accordion h1{padding-top:5px;}.menu_desc h3{margin-top:20px;}.menu_desc h4{margin-top:20px;} .form_head{padding-top:5px;}';
    var pc_css='.accordion h1{padding-top:5px;}' ;
    var mactest=navigator.userAgent.indexOf("Mac")!=-1;
    if (csstype=="inline"){
        document.write('<style type="text/css">')
        if (mactest)
            document.write(mac_css)
        else
            document.write(pc_css)
        document.write('</style>')
    }

    window.onscroll = function() {myFunction()};

    var header = document.getElementById("myHeader");
    var sticky = header.offsetTop;

    function myFunction() {
        if (window.pageYOffset >= sticky) {
            header.classList.add("sticky");
        } else {
            header.classList.remove("sticky");
        }
    }

    $(".accordion").accordion();

    /*  function incrementValue()
     {
         var value = parseInt(document.getElementById('number').value,10);
         value = isNaN(value) ? 0 : value;
         if(value<50){
             value++;
                 document.getElementById('number').value = value;
         }
     }
     function decrementValue( )
     {
         var value = parseInt(document.getElementById('number1').value, 10);
         value = isNaN(value) ? 0 : value;
         if(value>1){
             value--;
                 document.getElementById('number').value = value;
         }
     } */

    $('input[type="checkbox"]').on('change', function() {
        $(this).siblings('input[type="checkbox"]').not(this).prop('checked', false);
    });
</script>

<script>
    (function($) {

        $(function() {
            $('.toppings').fSelect();
        });
        $(function() {
            $('.sauce').fSelect();
        });
        $(function() {
            $('.salad').fSelect();
        });
    })(jQuery);


</script>
<script>
    /*    function incrementValue()
       {
           var value = parseInt(document.getElementById('number').value,10);
           value = isNaN(value) ? 0 : value;
           if(value<50){
               value++;
                   document.getElementById('number').value = value;
           }
       }
       function decrementValue( )
       {
           var value = parseInt(document.getElementById('number1').value, 10);
           value = isNaN(value) ? 0 : value;
           if(value>1){
               value--;
                   document.getElementById('number1').value = value;
           }
       } */
    /* function incrementValue()
    {
        var value = parseInt(document.getElementById('number1').value,10);
        value = isNaN(value) ? 0 : value;
        if(value<50){
            value++;
                document.getElementById('number1').value = value;
        }
    }
    function decrementValue( )
    {
        var value = parseInt(document.getElementById('number1').value, 10);
        value = isNaN(value) ? 0 : value;
        if(value>1){
            value--;
                document.getElementById('number1').value = value;
        }
    } */

    // When the user scrolls the page, execute myFunction
    window.onscroll = function() {myFunction()};

    // Get the header
    var header = document.getElementById("myHeader");

    // Get the offset position of the navbar
    var sticky = header.offsetTop;

    // Add the sticky class to the header when you reach its scroll position. Remove "sticky" when you leave the scroll position
    function myFunction() {
        if (window.pageYOffset > sticky) {
            header.classList.add("sticky");
        } else {
            header.classList.remove("sticky");
        }
    }
</script>

<script type="text/javascript">
    var message="${message}";
    var futureTime="${futureTime}";
    var futureDate="${futureDate}";

    var pick_up_time = parseInt("${pickUpTime}");
    //pick_up_time="";
    jQuery(document).ready(function() {
        if(message!=null && message!="")
            if(message.indexOf("successfully")!=1)
            {
                var current_time = new Date (),
                    final_pick_up_time = new Date ( current_time );

                message="<h3>Order successful !!!<h3> ";

                // if(futureTime != "") {
                //     message += "<h4>You order will be ready by  "+ futureTime+" " +final_pick_up_time.toLocaleTimeString('en-us',{timeZoneName:'short'}).split(' ')[2]+".</h4>" ;
                // }
                //
                // else if(pick_up_time){
                //     final_pick_up_time.setMinutes ( current_time.getMinutes() + (pick_up_time) );
                //     message += "<h4>You order will be ready by  "+ final_pick_up_time.toLocaleTimeString('en-US') + " "+final_pick_up_time.toLocaleTimeString('en-us',{timeZoneName:'short'}).split(' ')[2] +".</h4>" ;
                // }
                //
                // else
                    message += "<h3>Please check your inbox for further details.";
                message += "</h3>";
                $("#message").html(message);
                $('#myModal20').modal('show');
            }
        if (jQuery('.homemenu').attr("id") == "menuPage") {
            $('#btn1').addClass('current');
            $('#btn1option').addClass('current');
        }

        $(".buttonClose").on("click", function(e){
            if(merchantId!=null && merchantId !="")
            {
                $('#myModal20').modal('hide');
                window.location.href=window.location.pathname+"?merchantId="+merchantId;
            }
        })

    });

</script>

<jsp:include page="footerV2.jsp"></jsp:include>

 	
