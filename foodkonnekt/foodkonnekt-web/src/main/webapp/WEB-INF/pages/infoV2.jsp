<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<script type="text/javascript" src="resources/js/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="resources/js/bootstrap.min.js"></script>
 <!-- <script src="resources/V2/js/woco.accordion.js"></script>-->
<script src="resources/js/woco.accordion.min.js"></script>
<script src="https://www.foodkonnekt.com/web/resources/V2/js/woco.accordion.min.js"></script>
<script src="resources/js/woco.accordion.min.js"></script>
<!-- <script src="https://maps.google.com/maps/api/js"></script> -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBC4vTiK4ydIencpmpwwcIUEmZkLgUpyAU" type="text/javascript"></script>
<script src="resources/V2/js/custom.js"></script>
<link rel="stylesheet" type="text/css" href="https://www.foodkonnekt.com/web/resources/V2/css/jquery.loadingimagethingy.css" />
<script type="text/javascript" src="resources/V2/ping-nose-popup/pignose.popup.min.js"></script>
<style>
.sticky+ .content{padding-top:120px!important;}

</style>

<jsp:include page="headerV2.jsp"></jsp:include>
<div class="content">

    <div class="container container-size">

        <div id="map-container-2" class="z-depth-1" style="height: 500px"></div>


    </div>
<script type="text/javascript">
$('#infoIcon').attr("class", "current");
</script>
    <div class="container container-size info" id="infoPage">
        <div class="row blue-border">
            <div class="row ur">
                <div class="col-md-6 col-sm-12">
                <div class="box">
				<h2 class="info-head">
                        address <img src="resources/V2/images/mapin.png">
                    </h2>
                    <div class="info-desc">
                        <address>
                            <c:forEach items="${merchantObj.addresses}" var="address" end="0">
                                <i class="fa fa-map-marker" aria-hidden="true"></i> ${address.address1} ${address.city}<br>
                                
                                <c:if test="${address.country !=null && address.country!=''}">
                                	<i class="fa fa-map-marker" aria-hidden="true">  ${address.country}, ${address.zip} <br>
                                </c:if>
                                
                                <c:if test="${address.country ==null || address.country==''}">
                                	<i class="fa fa-map-marker" aria-hidden="true"></i> ${address.zip} <br>
                                </c:if>
                                
                               
                            </c:forEach>
                            <i class="fa fa-mobile" aria-hidden="true"></i> Ph: ${merchantObj.phoneNumber }
                          <br><i class="fa fa-envelope" aria-hidden="true" style="font-size:8px; margin-top:-3px;"></i>                        
                          
				<c:if test= "${merchantObj.website !=null && merchantObj.website !=''}">
                            ${merchantObj.website}
                          </c:if>
                        </address>
                    </div>
                </div>
		</div>
                <div class="col-md-6 col-sm-12">
                	<div class="box">
                    <h2 class="info-head">
                        Opening Hours<img src="resources/V2/images/timer1.png">
                    </h2>
                    <div class="info-detail">
                        <c:forEach items="${merchantObj.openingClosingDays}" var="openingClosing">

                            <c:if test="${openingClosing.isHoliday==1}">
                                <div class="text-left">${openingClosing.day }
                                    <span>CLOSED</span>
                                </div>
                            </c:if>

                            <c:if test="${openingClosing.isHoliday==0}">
                                <div class="text-left">${openingClosing.day }
                                    <span>${openingClosing.times[0].startTime } -
                                        ${openingClosing.times[0].endTime }</span>
                                </div>
                            </c:if>

                        </c:forEach>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-12">
            <div class="box">
                <h2 class="info-head">
                    Pickup & Delivery Hours<img src="resources/V2/images/timer1.png">
                </h2>
                <div class="info-detail">
                    <c:if test="${merchantObj.delivery == null}">
    Same as Opening hours
</c:if>
                    ${merchantObj.delivery}
                </div>
            </div>
           </div>
            <!-- <div class="col-md-6 col-sm-12">
<h2 class="info-head">Coupon<img src="resources/V2/images/coupon.png"></h2>
<div class="row info-detail">
	<div class="col-md-4 col-sm-2 ">
		<img src="resources/V2/images/coupon-img.png"><br><br>
	</div>
	<div class="col-md-4 col-sm-2 ">
		<strong>Coupon Details</strong>:<br>BNYUI890C6
	</div>
	<div class="col-md-4 col-sm-2 ">
		<strong>Expiry:</strong><br>00/00
	</div>
</div>
</div> -->

        </div>
    </div>
</div>
</div>
<!-- main container ends here-->
<jsp:include page="footerV2.jsp"></jsp:include>
</body>
<!-- <script src="https://maps.google.com/maps/api/js"></script>
 -->
<!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBC4vTiK4ydIencpmpwwcIUEmZkLgUpyAU" type="text/javascript"></script>

<script src="resources/V2/js/custom.js"></script> -->
<script>
window.onscroll = function() {myFunction()};

var header = document.getElementById("myHeader");
var sticky = header.offsetTop;

function myFunction() {
  if (window.pageYOffset >= sticky) {
    header.classList.add("sticky");
  } else {
    header.classList.remove("sticky");
  }
}
initMap('${merchantObj.addresses[0].address1}'+' ${merchantObj.addresses[0].city}');
//document.getElementById('info').set('class','activeli');
$('#info').attr("class","activeli");

function checkGuestCustomerOnInfo(){
	if((guestCustomerEmailId!='' && guestCustomerPassword=='') || guestSignUpFlag==1){
			/* $("#createPasswordPopup1").modal('show'); */
		}
}
checkGuestCustomerOnInfo();
</script>
<script type="text/javascript">
    jQuery(document).ready(function() {
        if (jQuery('.info').attr("id") == "infoPage") {
        $('#btn2').addClass('current');
    }
    });
</script>
