<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!-- <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> -->
<div>&nbsp;</div>
<jsp:include page="headerV2.jsp"></jsp:include>
<!-- <html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
 -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
<script type="text/javascript" src="resources/js/jquery-1.11.3.min.js"></script>
<script src="resources/V2/js/loginV2.js"></script>
<script src="resources/V2/js/forgotPasswordV2.js"></script>
<script src="resources/js/woco.accordion.min.js"></script>
<script type="text/javascript" src="resources/js/bootstrap.min.js"></script>
<!-- <script src="resources/js/fSelect.js"></script> -->
<script type="text/javascript" src="https://www.foodkonnekt.com/web/resources/V2/js/jquery.loadingimagethingy.js"></script>
<script type="text/javascript" src="resources/js/ping-nose-popup/pignose.popup.min.js"></script>
<link rel="stylesheet" type="text/css" href="resources/V2/css/jquery.loadingimagethingy.css" />
<script type="text/javascript" src="resources/V2/js/jquery.loadingimagethingy.js"></script>
<link rel="stylesheet" type="text/css" href="resources/css/userprofile.css"/>
<link rel="stylesheet" type="text/css" href="resources/css/styles-min.css"/>
<link rel="stylesheet" type="text/css" href="resources/css/style-min.css"/>
<link href="https://www.foodkonnekt.com/web/resources/V2/css/fSelect.css" rel="stylesheet"/>

<script type="text/javascript">
$('#userIcon').attr("class", "activeli");
</script>
<body>
<div class="content">
<div class="container content loginbg"> 
<!-- MultiStep Form -->
<div class="row">
		<div class="col-md-offset-1 col-md-8 col-sm-7 logint" id="loginpage"><br><br>
		<form id="msform">
				<fieldset>
            <span  id="loginError"></span>
                <div class="outer-border">
                	<input type="email"  id="email" name="email" placeholder="Email Id" required="required"/>
				</div>
				
				<div class="outer-border">
                	<input type="password" id="password" name="pwd" placeholder="Password" required="required"/>
				</div>
				         <input  type="button"  onclick="customerLogin()" name="login" id="loginButton" class="login submit action-button" value="Login"/>
					     
					     <a href="forgotPasswordV2"><input  type="button" name="forgot" class="previous action-button-forgot" value="Forgot Password"/></a>
						
						</div></fieldset>
				</form>

		
		</div><br><br>

</div>
</div>
<jsp:include page="footerV2.jsp"></jsp:include>

<script type="text/javascript">
var merchantId="${sessionScope.merchant.id}";

</script>

<script type="text/javascript">
    jQuery(document).ready(function() {
        if (jQuery('.logint').attr("id") == "loginpage") {
        $('#btn4').addClass('current');
    }
});
</script>

</body>
</html>