package com.foodkonnekt.contoller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.foodkonnekt.model.Address;
import com.foodkonnekt.model.CardInfo;
import com.foodkonnekt.model.Customer;
import com.foodkonnekt.model.CustomerCode;
import com.foodkonnekt.model.Merchant;
import com.foodkonnekt.model.Vendor;
import com.foodkonnekt.repository.CustomerCodeRepository;
import com.foodkonnekt.repository.CustomerrRepository;
import com.foodkonnekt.service.CustomerService;
import com.foodkonnekt.service.MerchantService;
import com.foodkonnekt.util.DateUtil;
import com.foodkonnekt.util.EncryptionDecryptionUtil;
import com.foodkonnekt.util.IConstant;
import com.foodkonnekt.util.MailSendUtil;
import com.foodkonnekt.util.ProducerUtil;
import com.foodkonnekt.util.UrlConstant;
import com.google.gson.Gson;
import com.itextpdf.text.pdf.PdfStructTreeController.returnType;

@Controller
public class HomeControllerV2 {

	private static int EXPIRY_TIME = 30 * 60 * 1000;

	@Autowired
    private Environment environment;
	
	@Autowired
	private CustomerCodeRepository customerCodeRepository;
	
	@Autowired
	private CustomerService customerService;

	@Autowired
	private MerchantService merchantService;

	@Autowired
	private CustomerrRepository customerrRepository;

	private static final Logger LOGGER = LoggerFactory.getLogger(HomeControllerV2.class);

	@RequestMapping(value = "/userProfileV2", method = RequestMethod.GET)
	public String showProfile(ModelMap model, Map<String, Object> map, HttpServletRequest request) {
		LOGGER.info("HomeController.showProfile() : START");
		try {
			HttpSession session = request.getSession();
			Customer customer = (Customer) session.getAttribute("customer");
			if (customer == null) {
				customer = (Customer) session.getAttribute("guestCustomer");
			}
			// customer.setPassword(EncryptionDecryptionUtil.decryption(customer.getPassword()));
			Merchant merchant = (Merchant) session.getAttribute("merchant");
			if (customer != null) {
				map.put("Customer", new Customer());
				model.addAttribute("Customer", customer);

				if (customer.getId() != null && merchant != null && merchant.getId() != null) {
					String timeZone=(merchant!=null && merchant.getTimeZone()!=null && merchant.getTimeZone().getTimeZoneCode()!=null) 
							? merchant.getTimeZone().getTimeZoneCode() :"America/Chicago";
					String today = DateUtil.getCurrentDateYYYYMMDDForTimeZone(timeZone);
					List<CustomerCode> customersCodes = customerCodeRepository.findByCustomerIdAndMerchantIdAndDate(customer.getId(),merchant.getId(),today,true);
					model.addAttribute("customerCodes", customersCodes);
							
					Customer customer2 = customerService.findByCustomerId(customer.getId());
					if (customer2 != null && customer2.getLoyalityProgram() != null) {
						model.addAttribute("loyalityProgram", customer2.getLoyalityProgram());
					}

					model.addAttribute("customerAddresses",
							customerService.findAllCustomerAddress(customer.getId(), merchant.getId()));
				}

				List<CardInfo> cardInfos = customerService.getCustomerCardInfo(customer.getId());
				List<CardInfo> cardsInfos = new ArrayList<CardInfo>();
				List<CardInfo> expiryCardsInfos = new ArrayList<CardInfo>();
				Calendar calendar = Calendar.getInstance();
				Integer year = Integer.parseInt(("" + calendar.get(Calendar.YEAR)).substring(2));
				Integer month = calendar.get(Calendar.MONTH);

				for (CardInfo cardInfo : cardInfos) {
					cardInfo.setMerchant(null);
					cardInfo.setCustomer(null);
					// cardInfo.getExpirationDate()
					System.out.println("ExpirationDateYear" + cardInfo.getExpirationDate().substring(2, 4));
					System.out.println("ExpirationDateMonth" + cardInfo.getExpirationDate().substring(0, 2));
					if (cardInfo.getToken() != null && !cardInfo.getToken().isEmpty() && cardInfo.getFirst6() != null
							&& !cardInfo.getFirst6().isEmpty() && cardInfo.getLast4() != null
							&& !cardInfo.getLast4().isEmpty() && cardInfo.getExpirationDate() != null
							&& !cardInfo.getExpirationDate().isEmpty() && cardInfo.getToken() != ""
							&& cardInfo.getFirst6() != "" && cardInfo.getLast4() != ""
							&& cardInfo.getExpirationDate() != "") {
						if (Integer.parseInt(cardInfo.getExpirationDate().substring(2, 4)) > year) {
							cardsInfos.add(cardInfo);
						} else if (Integer.parseInt(cardInfo.getExpirationDate().substring(2, 4)) == year
								&& Integer.parseInt(cardInfo.getExpirationDate().substring(0, 2)) > month) {
							cardsInfos.add(cardInfo);
						} else {
							expiryCardsInfos.add(cardInfo);
						}
					}

				}
				Gson gson = new Gson();
				if (merchant != null && merchant.getAllowMultiPay() != null && merchant.getAllowMultiPay() == true) {
					String cardsInfosJson = gson.toJson(cardsInfos);
					String expiryCardsInfosJson = gson.toJson(expiryCardsInfos);
					session.setAttribute("cardInfo", cardsInfosJson);
					session.setAttribute("expiryCardsInfos", expiryCardsInfosJson);
					session.setAttribute("allowMultiPay", true);
				} else {
					session.setAttribute("allowMultiPay", false);
					session.removeAttribute("cardInfo");
					session.removeAttribute("expiryCardsInfos");
				}
				LOGGER.info("HomeController.showProfile() : END");

				return "userProfileV2";
			} else {
				LOGGER.info("HomeController.showProfile() : END");

				return "loginV2";
			}
		} catch (Exception e) {
			LOGGER.info("HomeController.showProfile() : ERROR" + e);

			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
			}
			LOGGER.error("error: " + e.getMessage());
		}
		LOGGER.info("HomeController.showProfile() : END");
		return "userProfileV2";
	}

	@RequestMapping(value = "/updateCustomerProfileV2", method = { RequestMethod.POST })
	public String updateCustomer(@ModelAttribute("Customer") Customer customer, ModelMap model,
			HttpServletRequest request, HttpServletResponse response) {
		LOGGER.info("HomeController.updateCustomer() : START");

		try {
			HttpSession session = request.getSession();
			Merchant merchant = (Merchant) session.getAttribute("merchant");
			
			if(merchant != null) {
			LOGGER.info("HomeController.updateCustomer() : merchant : " + merchant.getId());
				
			Vendor vendor = (Vendor) session.getAttribute("vendor");
			Customer dbCustomer = null;
			if (vendor != null && vendor.getId() != null) {
				customer.setVendor(vendor);
			} /*
				 * else { if(merchant!=null){ vendor = merchant.getOwner(); } }
				 */

			if (customer != null && customer.getId() != null) {
				dbCustomer = customerService.findByCustomerId(customer.getId());
			}

			List<Customer> customers = null;
			if (vendor != null && vendor.getId() != null) {
				if (dbCustomer != null) {
					customers = customerService.findByEmailIDAndVendorId(dbCustomer.getEmailId(), vendor.getId());
					if (customers != null && customers.size() > 0) {
						for (Customer cust : customers) {
							if (cust != null && cust.getPassword() != null && cust.getPassword().length() > 0
									&& cust.getMerchantt() != null && customer.getMerchantt() != null
									&& cust.getMerchantt().getId() != null && customer.getMerchantt().getId() != null
									&& cust.getMerchantt().getId() != customer.getMerchantt().getId()) {
								Gson gson = new Gson();
								cust.setAnniversaryDate(customer.getAnniversaryDate());
								cust.setBirthDate(customer.getBirthDate());
								cust.setFirstName(customer.getFirstName());
								if (customer.getLastName() != null)
									cust.setLastName(customer.getLastName());
								cust.setEmailId(customer.getEmailId());
								cust.setPassword(customer.getPassword());

//                                if(cust.getLoyalityValue() !=null ){
//                                	if(cust.getLoyalityValue()==true){
//                                		cust.setLoyalityProgram(1);
//                                	}else if(cust.getLoyalityValue()==false){
//                                		cust.setLoyalityProgram(0);
//                                	}
//                                }

								Merchant merchantt = new Merchant();
								merchantt.setId(cust.getMerchantt().getId());
								cust.setMerchantt(merchantt);
								cust.setPhoneNumber(customer.getPhoneNumber());
								String customerJson = gson.toJson(cust);
								ProducerUtil.updateCustomer(customerJson,environment);
							}
						}
					}
				}
			}

			if (customer.getPassword() == null || customer.getPassword().isEmpty() || customer.getPassword() == "") {
				customer.setPassword(dbCustomer.getPassword());
			}
			Gson gson = new Gson();
			customer = ProducerUtil.setAddresses(customer);
			String customerJson = gson.toJson(customer);
			String result = ProducerUtil.updateCustomer(customerJson,environment);
			JSONObject jObject = new JSONObject(result);
			if (result.contains("response")
					&& jObject.getString("response").equals(IConstant.RESPONSE_SUCCESS_MESSAGE)) {
				JSONObject Customer = jObject.getJSONObject("DATA");
				Customer customerResult = ProducerUtil.getCustomer(Customer);
				session.setAttribute("customer", customerResult);
				session.setAttribute("guestCustomer", customerResult);
				model.addAttribute("Customer", (Customer) session.getAttribute("customer"));
				model.addAttribute("customerAddresses",
						customerService.findAllCustomerAddress(customer.getId(), merchant.getId()));

				Customer customer2 = customerService.findByCustomerId(customer.getId());
				if (customer2 != null && customer2.getLoyalityProgram() != null) {
					model.addAttribute("loyalityProgram", customer2.getLoyalityProgram());
				}

			}
			}else
				return "redirect:https://www.foodkonnekt.com";
		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
			}
			LOGGER.info("HomeController.updateCustomer() : ERROR" + e);

			LOGGER.error("error: " + e.getMessage());
		}
		LOGGER.info("HomeController.updateCustomer() : END");

		return "userProfileV2";
	}

	@RequestMapping(value = "/logoutV2", method = RequestMethod.GET)
	public String logout(HttpServletRequest request, HttpServletResponse response, ModelMap model) {
		LOGGER.info("HomeController.logout() : Starts");

		Merchant merchant = null;
		HttpSession session = request.getSession();
		if (session != null) {
			merchant = (Merchant) session.getAttribute("merchant");
			if (merchant != null) {
				Vendor vendor = (Vendor) session.getAttribute("vendor");
				session.removeAttribute("customer");
				session.removeAttribute("guestCustomer");
				session.removeAttribute("orderItems");
				session.removeAttribute("Tax");
				session.removeAttribute("totalPrice");
				session.removeAttribute("subTotal");
				session.removeAttribute("ConvenienceFee");

				if (vendor != null) {
					if (vendor.getId() != null) {
						model.addAttribute("vendorId", vendor.getId());
						/*
						 * if(merchant.getId().equals(IConstant.TEXAN_REWARDS_MERCHANT_ID)){ return
						 * "redirect:" + environment.getProperty("TEXAN_BASE_URL") + "/orderV2?merchantId=" +
						 * merchant.getId(); }else{
						 */
						LOGGER.info("HomeController.logout() : END");

						return "redirect:" + environment.getProperty("WEB_BASE_URL") + "/orderV2?merchantId=" + merchant.getId();
						// }

						// return "redirect:" + environment.getProperty("WEB_BASE_URL") + "/orderV2?merchantId=" +
						// merchant.getId();
					}
				}
			}
		}

		/*
		 * if(merchant.getId().equals(IConstant.TEXAN_REWARDS_MERCHANT_ID)){ return
		 * "redirect:" + environment.getProperty("TEXAN_BASE_URL") + "/orderV2?merchantId=" +
		 * merchant.getId(); }else{
		 */
		LOGGER.info("HomeController.logout() : END");

		return "redirect:" + environment.getProperty("WEB_BASE_URL") + "/orderV2?merchantId=" + merchant.getId();
		// }
		// return "redirect:" + environment.getProperty("WEB_BASE_URL") + "/orderV2?merchantId=" +
		// merchant.getId();
	}

	@RequestMapping(value = "/forgotPasswordV2", method = RequestMethod.GET)
	public String forgotPasswordV2() {
		return "forgotPasswordV2";
	}

	@RequestMapping(value = "/forgotPasswordActionV2", method = RequestMethod.POST)
	public @ResponseBody Map<Object, Object> forgotPasswordAction(@RequestBody final Customer customer,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		LOGGER.info("HomeController.forgotPasswordAction() : START");

		Map<Object, Object> forgotPasswordMap = new HashMap<Object, Object>();
		try {
			HttpSession session = request.getSession();
			Merchant merchant = (Merchant) session.getAttribute("merchant");
			Vendor vendor = (Vendor) session.getAttribute("vendor");
			Integer vendorId = 0;
			if (vendor != null && vendor.getId() != null) {
				vendorId = vendor.getId();

			}
			if (merchant != null) {
				boolean status = false;
				if (vendorId > 0) {
					status = customerService.findByEmailAndMerchantIdAndVendorIdV2(customer.getEmailId(),
							merchant.getId(), vendorId);
				} else {
					status = customerService.findByEmailAndMerchantIdV2(customer.getEmailId(), merchant.getId());
				}
				LOGGER.info(" ===status " + status);
				if (status) {
					forgotPasswordMap.put("message",
							"Please check your inbox - we have sent you an email with instructions");
				} else {
					forgotPasswordMap.put("message",
							"An account with the below email does not exist. Please try again");
				}
			} else {
				response.sendRedirect("https://www.foodkonnekt.com");

			}
		} catch (Exception exception) {
			LOGGER.info("HomeController.forgotPasswordAction() : ERROR" + exception);

			if (exception != null) {
				MailSendUtil.sendExceptionByMail(exception,environment);
			}
			exception.printStackTrace();
		}
		LOGGER.info("HomeController.forgotPasswordAction() : END");
		return forgotPasswordMap;
	}

	@RequestMapping(value = "/changePasswordV2", params = { "email", "vendorId", "merchantId",
			"userId" }, method = RequestMethod.GET)
	public String setResetPassword(Map<String, Object> map, HttpServletRequest request,
			@RequestParam(value = "email") String email, @RequestParam(value = "vendorId") String vendorId,
			@RequestParam(value = "merchantId") String merchantId, @RequestParam(value = "userId") String userId,
			@RequestParam(value = "tLog") String tLog) {
		LOGGER.info("HomeController.setResetPassword() : START");
		LOGGER.info(" === userId : " + userId + " tLog : " + tLog);

		try {
			HttpSession session = request.getSession();
			if (merchantId.matches("[0-9]+")) {
				Merchant merchant = merchantService.findById(Integer.valueOf(merchantId));
				if (merchant != null && merchant.getOwner() != null && merchant.getOwner().getId() != null) {
					session.setAttribute("vendor", merchant.getOwner());
				}
			}
			Long time = Long.valueOf(EncryptionDecryptionUtil.decryption(tLog));
			Long currentTime = System.currentTimeMillis();
			int diff = (int) (currentTime - time);
			LOGGER.info(" === Difference between current time and tLog time : " + diff);

			if (diff < EXPIRY_TIME) {

				if (merchantId != null) {
					String id = merchantId;
					Customer customer = customerService.findCustomerByEmailAndMerchantId(email, Integer.valueOf(id));
					if (customer != null
							&& userId.equalsIgnoreCase(EncryptionDecryptionUtil.encryptString(customer.getId() + ""))) {
						customer.setPassword("");
						map.put("Customer", customer);
						LOGGER.info("HomeController.setResetPassword() : END");

						return "changePasswordV2";
					} else {
						LOGGER.info("HomeController.setResetPassword() : END");

						return "redirect:https://www.foodkonnekt.com";
					}
				}
			} else {
				System.out.println("Link is expired");
				map.put("Customer", new Customer());
				LOGGER.info("HomeController.setResetPassword() : END");

				return "forgotPasswordV2";
			}
		} catch (Exception e) {
			LOGGER.info("HomeController.setResetPassword() : ERROR" + e);

			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
			}
			LOGGER.error("error: " + e.getMessage());
		}
		LOGGER.info("HomeController.setResetPassword() : END");
		return null;
	}

	@RequestMapping(value = "/changepasswordV2", params = { "email", "merchantId",
			"userId" }, method = RequestMethod.GET)
	public String setResetPassword(Map<String, Object> map, HttpServletRequest request,
			@RequestParam(value = "email") String email, @RequestParam(value = "merchantId") String merchantId,
			@RequestParam(value = "userId") String userId, @RequestParam(value = "tLog") String tLog) {
		LOGGER.info("HomeController.setResetPassword() : START");
		LOGGER.info(" === userId : " + userId + " tLog : " + tLog);

		try {
			Long time = Long.valueOf(EncryptionDecryptionUtil.decryption(tLog));
			Long currentTime = System.currentTimeMillis();
			int diff = (int) (currentTime - time);
			LOGGER.info(" === Difference between current time and tLog time : " + diff);

			if (diff < EXPIRY_TIME) {

				if (merchantId != null) {
					String id = merchantId;
					Customer customer = customerService.findCustomerByEmailAndMerchantId(email, Integer.valueOf(id));
					if (customer != null
							&& userId.equalsIgnoreCase(EncryptionDecryptionUtil.encryptString(customer.getId() + ""))) {
						customer.setPassword("");
						map.put("Customer", customer);
						LOGGER.info("HomeController.setResetPassword() : END");

						return "changePasswordV2";
					} else {
						LOGGER.info("HomeController.setResetPassword() : END");

						return "redirec" + "t:https://www.foodkonnekt.com";
					}
				}
			} else {
				System.out.println("Link is expired");
				map.put("Customer", new Customer());
				LOGGER.info("HomeController.setResetPassword() : END");

				return "forgotPasswordV2";
			}
		} catch (Exception e) {
			LOGGER.info("HomeController.setResetPassword() : ERROR" + e);

			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
			}
			LOGGER.error("error: " + e.getMessage());
		}
		LOGGER.info("HomeController.setResetPassword() : END");
		return null;
	}

	@RequestMapping(value = "/saveResetGuestPasswordV2", method = RequestMethod.POST)
	public String saveResetGuestPassword(@ModelAttribute("Customer") Customer customer, ModelMap model,
			HttpServletRequest request, HttpServletResponse response) {
		LOGGER.info("HomeController.saveResetGuestPassword() : START");

		try {
			customerService.setGuestCustomerPassword(customer);
			Customer customerInfo = customerService.findByEmailAndCustomerId("", customer.getId());
			LOGGER.info("marchant ID is" + customerInfo.getMerchantt().getId());

			HttpSession session = request.getSession();
			session.setAttribute("customer", customerInfo);
			String merchantName = customerInfo.getMerchantt().getName();
			merchantName = merchantName.replaceAll("\\s+", "");

			Vendor vendor = (Vendor) session.getAttribute("vendor");
			if (vendor != null && vendor.getId() != null) {
				String vendorId = EncryptionDecryptionUtil.encryption(Integer.toString(vendor.getId()));
				String merchId = EncryptionDecryptionUtil
						.encryption(Integer.toString(customerInfo.getMerchantt().getId()));

				LOGGER.info("HomeController.sendLocation() : END");
				// return "redirect:" + environment.getProperty("WEB_BASE_URL") + "/vendor/"+ vendorId + "/" +
				// merchantName+ "/clover/"
				// +merchId ;
				return "redirect:" + environment.getProperty("WEB_BASE_URL") + "/orderV2?merchantId=" + merchId;
			} else {
				LOGGER.info("HomeController.sendLocation() : END");

				/*
				 * return "redirect:" + environment.getProperty("WEB_BASE_URL") + "/" + merchantName +
				 * "/clover/" +
				 * EncryptionDecryptionUtil.encryption(String.valueOf(customerInfo.getMerchantt(
				 * ).getId()));
				 */
				return "redirect:" + environment.getProperty("WEB_BASE_URL") + "/orderV2?merchantId="
						+ customerInfo.getMerchantt().getId();
			}
		} catch (Exception e) {
			LOGGER.info("HomeController.saveResetGuestPassword() : ERROR" + e);

			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
			}
			LOGGER.error("error: " + e.getMessage());
		}
		LOGGER.info("HomeController.saveResetGuestPassword() : END");
		return "redirect:https://www.foodkonnekt.com";
	}

	@RequestMapping(value = "/cancelAccountRequestV2", method = RequestMethod.GET)
	public String cancelAccountRequest(ModelMap model, Map<String, Object> map, HttpServletRequest request) {
		LOGGER.info("HomeController.cancelAccountRequest() : START");

		HttpSession session = request.getSession();
		session.removeAttribute("customer");
		session.removeAttribute("guestCustomer");
		session.removeAttribute("cardInfo");
		session.removeAttribute("expiryCardsInfos");
		// session.invalidate();
		// return "redirect:https://www.foodkonnekt.com";
		Vendor vendor=null;
		
        if(session != null)
		vendor = (Vendor) session.getAttribute("vendor");
		

		if (vendor != null) {
			if (vendor.getId() != null) {
				model.addAttribute("vendorId", vendor.getId());
				LOGGER.info(" === vendor.getId() : " + vendor.getId());
			}
		}
		Merchant merchant = (Merchant) session.getAttribute("merchant");
		LOGGER.info("HomeController.cancelAccountRequest() : END");
		
		return "redirect:" + environment.getProperty("WEB_BASE_URL") + "/orderV2?message=Your order is placed successfully";
		// }
	}

	@RequestMapping(value = "/setGuestPasswordV2", method = RequestMethod.GET)
	public String setGuestPassword(ModelMap model, Map<String, Object> map, HttpServletRequest request) {
		LOGGER.info("HomeController.setGuestPassword() : START");

		map.put("Customer", new Customer());

		LOGGER.info("HomeController.setGuestPassword() : END");
		return "setGuestPasswordV2";
	}

	@RequestMapping(value = "/saveGuestPasswordV2", method = RequestMethod.POST)
	public String saveGuestPassword(@ModelAttribute("Customer") Customer customer, ModelMap model,
			HttpServletRequest request, HttpServletResponse response) {
		LOGGER.info("HomeController.saveGuestPassword() : START");

		try {
			HttpSession session = request.getSession();
			if (session != null) {
				Merchant merchant = (Merchant) session.getAttribute("merchant");
				String merchantLogo = "http://www.dev.foodkonnekt.com/app/foodnew.jpg";
				String locationName = " ";
				if (merchant != null) {
					if (merchant.getMerchantLogo() == null) {
						merchantLogo = "http://www.dev.foodkonnekt.com/app/foodnew.jpg";
					} else {
						merchantLogo = environment.getProperty("BASE_PORT") + merchant.getMerchantLogo();
					}
					if (merchant.getName() != null) {
						locationName = merchant.getName();
					}
				}
				final String customerLogo = merchantLogo;
				final String merchantName = locationName;
				Customer sessionCustomer = (Customer) session.getAttribute("customer");
				if (sessionCustomer == null) {
					sessionCustomer = (Customer) session.getAttribute("guestCustomer");
				}

				if (sessionCustomer != null) {
					LOGGER.info(" === customer.getPassword() : " + customer.getPassword());
					sessionCustomer.setPassword(customer.getPassword());
					final Customer finalCustomer = customerService.setGuestCustomerPassword(sessionCustomer);

					Thread t1 = new Thread(new Runnable() {

						public void run() {
							try {
								Thread.sleep(2000);
							} catch (InterruptedException exception) {
								LOGGER.info("HomeController.saveGuestPassword() : ERROR" + exception);

								MailSendUtil.sendExceptionByMail(exception,environment);
								exception.printStackTrace();

							}
							MailSendUtil.customerRegistartionConfirmation(finalCustomer, customerLogo, merchantName);
							System.out.println("Thread over");
						}
					});
					t1.start();
					session.setAttribute("customer", finalCustomer);
					session.removeAttribute("guestCustomer");
				} else {
					// return "redirect:" + environment.getProperty("WEB_BASE_URL") + "/orderV2";
					/*
					 * if(merchant!=null &&
					 * merchant.getId().equals(IConstant.TEXAN_REWARDS_MERCHANT_ID)){ return
					 * "redirect:" + environment.getProperty("TEXAN_BASE_URL") + "/orderV2"; }else{
					 */
					return "redirect:" + environment.getProperty("WEB_BASE_URL") + "/orderV2";
					// }
				}
			} else {
				return "redirect:" + environment.getProperty("WEB_BASE_URL") + "/orderV2";
			}
		} catch (Exception e) {
			LOGGER.info("HomeController.saveGuestPassword() : ERROR" + e);

			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
			}
			LOGGER.error("error: " + e.getMessage());
		}
		LOGGER.info("HomeController.saveGuestPassword() : END");
		return "redirect:" + environment.getProperty("WEB_BASE_URL") + "/myOrdersV2";
	}

	@RequestMapping(value = "sendLocationV2", method = RequestMethod.GET)
	@ResponseBody
	public String sendLocationV2(@RequestParam("merchantId") String merchantId,
			@RequestParam("merchantName") String merchantName, ModelMap model, HttpServletRequest request) {
		LOGGER.info("HomeController.sendLocation() : START");
		LOGGER.info(" === merchantId : " + merchantId);
		HttpSession session = request.getSession();
		Vendor vendor = (Vendor) session.getAttribute("vendor");
		String vendorId = null;
		if (vendor != null) {
			vendorId = EncryptionDecryptionUtil.encryption(Integer.toString(vendor.getId()));
		}
		String merchId = EncryptionDecryptionUtil.encryption(merchantId);

		LOGGER.info("HomeController.sendLocation() : END");
		return environment.getProperty("WEB_BASE_URL") + "/vendorV2/" + vendorId + "/" + merchantName + "/cloverV2/" + merchId;
	}

	@RequestMapping(value = { "vendorV2/{vendoreId}/{merchantName}/cloverV2/{merchantId}",
			"{merchantName}/foodtronix/{merchantId}" }, method = RequestMethod.GET)
	public String baseUrlForMultiLocation(ModelMap model, Map<String, Object> map,
			@PathVariable("vendoreId") String vendoreId, @PathVariable("merchantName") String merchantName,
			@PathVariable("merchantId") String merchantId) throws IOException {
		LOGGER.info("HomeController.baseUrlForMultiLocation() : START");
		LOGGER.info(" === merchantId : " + merchantId + " vendoreId : " + vendoreId);

		model.addAttribute("merchantId", merchantId);
		model.addAttribute("vendoreId", vendoreId);

		LOGGER.info("HomeController.baseUrlForMultiLocation() : END");
		return "redirect:" + environment.getProperty("WEB_BASE_URL") + "/homeForMultiLocationV2";
	}

	/**
	 * Show home page
	 * 
	 * @param merchantId
	 * @param model
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value = "homeForMultiLocationV2", method = RequestMethod.GET)
	public String showReiProfilePageForMultiLocationV2(ModelMap model, Map<String, Object> map,
			HttpServletRequest request, HttpServletResponse response, @RequestParam(required = false) String merchantId,
			@RequestParam(required = false) String vendoreId) throws IOException {
		LOGGER.info("HomeController.showReiProfilePageForMultiLocation() : START");
LOGGER.info(" ===vendoreId : "+vendoreId+" merchantId : "+merchantId);
		try {
			Integer intMerchantId = 0;
			Integer intVendorId = 0;
			if (merchantId != null) {
				if (merchantId.matches("[0-9]+")) {
					intMerchantId = Integer.parseInt(merchantId);
				} else {
					if (EncryptionDecryptionUtil.decryption(merchantId).matches("[0-9]+"))
						intMerchantId = Integer.parseInt(EncryptionDecryptionUtil.decryption(merchantId));
				}

				HttpSession session = request.getSession();
				List<Merchant> merchantList = new ArrayList<Merchant>();
				Vendor vendor = new Vendor();
				if (vendoreId != null) {

					if (vendoreId.matches("[0-9]+")) {
						intVendorId = Integer.parseInt(vendoreId);
					} else {
						if (EncryptionDecryptionUtil.decryption(vendoreId).matches("[0-9]+"))
							intVendorId = Integer.parseInt(EncryptionDecryptionUtil.decryption(vendoreId));
					}
					vendor = merchantService.findVendorById(intVendorId);
					merchantList = merchantService.findAllMerchantsByVendorId(intVendorId);

					if (vendor != null) {
						session.setAttribute("vendor", vendor);
					}

					session.setAttribute("merchantList", merchantList);
				}

				Merchant merchant = merchantService.findById(intMerchantId);
				if (merchant == null) {
					// return "redirect:" + environment.getProperty("WEB_BASE_URL") +
					// "/sessionTimeOut";
					return "redirect:https://www.foodkonnekt.com";
				}

				// session.setMaxInactiveInterval(2 * 60);
				Customer customer = (Customer) session.getAttribute("customer");
				if (customer != null) {
					if (customer.getMerchantt() != null) {
						Merchant customerMerchant = merchantService.findById(customer.getMerchantt().getId());
						LOGGER.info(" customerMerchant.getOwner().getId().intValue() : "
								+ customerMerchant.getOwner().getId().intValue());

						if (merchant.getOwner().getId().intValue() != customerMerchant.getOwner().getId().intValue()) {
							session.removeAttribute("customer");
						} else {
							if (customerMerchant.getId() == merchant.getId()) {
								customer.setMerchantt(merchant);
								session.setAttribute("customer", customer);
								System.out.println("customer logged in");
							} else {
								customer.setVendor(null);
								customer.setVendorId(merchant.getId());
								LOGGER.info(" === merchant.getId() : " + merchant.getId() + " customer.getEmailId() : "
										+ customer.getEmailId() + " customer.getPassword() : "
										+ customer.getPassword());

								List<Customer> customers = customerrRepository.findByEmailIdAndPasswordAndMerchanttId(
										customer.getEmailId(), customer.getPassword(), merchant.getId());
								if (customers != null && !customers.isEmpty()) {
									/*
									 * JSONObject Customer = jObject.getJSONObject("DATA"); Customer customerResult
									 * = ProducerUtil.getCustomer(Customer);
									 */

									session.setAttribute("customer", customers.get(0));

								} else {
									session.setAttribute("merchant", merchant);
									saveDuplicateCustomerV2(request, response);
								}

							}
						}
					}
				}

				model.addAttribute("merchantList", merchantList);

				if (merchant != null && merchant.getIsInstall() != null
						&& merchant.getIsInstall() != IConstant.SOFT_DELETE) {
					session.setAttribute("merchant", merchant);
					session.setMaxInactiveInterval(24 * 60 * 60);
					if (vendor != null && vendor.getId() != null && vendor.getId() > 0)
						model.addAttribute("vendorId", vendor.getId());

					model.addAttribute("merchantId", merchant.getId());
					map.put("Address", new Address());
				} else {
					LOGGER.info("HomeController.showReiProfilePageForMultiLocation() : END");

					return "redirect:https://www.foodkonnekt.com";
				}
			} else {
				LOGGER.info("HomeController.showReiProfilePageForMultiLocation() : END");

				return "redirect:https://www.foodkonnekt.com";
			}
		} catch (Exception e) {
			LOGGER.info("HomeController.showReiProfilePageForMultiLocation() : ERROR" + e);

			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
			}
			LOGGER.error("error: " + e.getMessage());
		}
		LOGGER.info("HomeController.showReiProfilePageForMultiLocation() : END");
		return "redirect:" + environment.getProperty("WEB_BASE_URL") + "/orderV2";
	}

	public String saveDuplicateCustomerV2(HttpServletRequest request, HttpServletResponse response) {
		LOGGER.info("HomeControllerV2.saveDuplicateCustomer() : START");

		Map<Object, Object> duplicateEmail = new HashMap<Object, Object>();
		try {

			HttpSession session = request.getSession();
			Merchant merchant = (Merchant) session.getAttribute("merchant");
			Customer customer = (Customer) session.getAttribute("customer");

			if (merchant != null && customer != null) {

				if (merchant.getId() != customer.getMerchantt().getId()) {
					if (customer.getId() != null) {
						LOGGER.info(" === customer.getId() : " + customer.getId());

						Customer dbCustomer = customerService.findByCustomerId(customer.getId());
						if (dbCustomer != null) {
							customer.setLastName(dbCustomer.getLastName());
							if (dbCustomer.getLastName() != null)
								customer.setLastName(dbCustomer.getLastName());
						}
					}

					customer.setId(null);
					customer.setMerchantId(merchant.getId());

					if (customer.getVendor() == null) {
						customer.setVendor(merchant.getOwner());
					}
					LOGGER.info(" === merchant.getId() : " + merchant.getId());

					Customer finalCustomer = ProducerUtil.setCusotmerInfo(customer, merchant.getId());

					if (finalCustomer.getPassword() != null) {
						finalCustomer.setPassword("@duplicatepassword#" + finalCustomer.getPassword());
					}
				LOGGER.info(" finalCustomer_password : "+finalCustomer.getPassword());

					Gson gson = new Gson();
					String customerJson = gson.toJson(finalCustomer);
					String result = ProducerUtil.updateCustomer(customerJson,environment);
					if (result != null && result.contains("response")) {
						JSONObject jObject = new JSONObject(result);
						if (jObject.getString("response").equals(IConstant.RESPONSE_SUCCESS_MESSAGE)) {
							JSONObject Customer = jObject.getJSONObject("DATA");
							Customer customerResult = ProducerUtil.getsignUpCustomer(Customer);

							customerResult.setOrderType(customer.getOrderType());
							LOGGER.info("customer-order type--" + customer.getOrderType());
							LOGGER.info("customerResult-order type--" + customerResult.getOrderType());
							customer.setId(customerResult.getId());
							customer.setPhoneNumber(customerResult.getPhoneNumber());
							LOGGER.info("phone------>" + customer.getPhoneNumber() + " id--" + customer.getId()
									+ " list of disc-" + customer.getListOfALLDiscounts());
							List<Map<String, Object>> duplicateCouponMapList = new ArrayList<Map<String, Object>>();
							// Map<Object, Object> duplicate
							duplicateCouponMapList = customerService.checkDuplicatCouponAndRecalculate(customer,
									customer.getListOfALLDiscounts());

							System.out.println("---------------------------");
							for (int i = 0; i < duplicateCouponMapList.size(); i++) {
								System.out.println(duplicateCouponMapList.get(i));
							}

							session.setAttribute("customer", customerResult);
							session.setAttribute("guestCustomer", customerResult);
							duplicateEmail.put("message", "successfully register");
							duplicateEmail.put("customerAfterSuccessId", customerResult.getId());
						}
					} else {
						duplicateEmail.put("message", "Please try again");
					}
				} else {

					session.setAttribute("customer", customer);
				}
			} else {
				duplicateEmail.put("message", "timeIssue");
				// response.sendRedirect("sessionTimeOut");
				LOGGER.info("HomeController.saveDuplicateCustomer() : END");

				return "redirect:https://www.foodkonnekt.com";
			}

		} catch (Exception e) {
			LOGGER.info("HomeController.saveDuplicateCustomer() : ERROR" + e);

			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
			}
			LOGGER.error("error: " + e.getMessage());
		}
		LOGGER.info("HomeController.saveDuplicateCustomer() : END");
		return "success";
	}

	@RequestMapping(value = "sendLocationForProfileV2", method = RequestMethod.GET)
	@ResponseBody
	public String sendLocationForProfile(@RequestParam("merchantId") String merchantId,
			@RequestParam("merchantName") String merchantName, ModelMap model, HttpServletResponse response,
			HttpServletRequest request) {
		LOGGER.info("HomeController.sendLocation() : START");

		HttpSession session = request.getSession();
		Vendor vendor = (Vendor) session.getAttribute("vendor");
		String vendoreId = EncryptionDecryptionUtil.encryption(Integer.toString(vendor.getId()));
		String merchId = EncryptionDecryptionUtil.encryption(merchantId);

		LOGGER.info("HomeController.sendLocation() : END");
		LOGGER.info(" ===vendoreId : "+vendoreId+" merchantId : "+merchantId);

		try {
			Integer intMerchantId = 0;
			Integer intVendorId = 0;
			if (merchantId != null) {
				if (merchantId.matches("[0-9]+")) {
					intMerchantId = Integer.parseInt(merchantId);
				} else {
					if (EncryptionDecryptionUtil.decryption(merchantId).matches("[0-9]+"))
						intMerchantId = Integer.parseInt(EncryptionDecryptionUtil.decryption(merchantId));
				}

				List<Merchant> merchantList = new ArrayList<Merchant>();

				if (vendoreId != null) {

					if (vendoreId.matches("[0-9]+")) {
						intVendorId = Integer.parseInt(vendoreId);
					} else {
						if (EncryptionDecryptionUtil.decryption(vendoreId).matches("[0-9]+"))
							intVendorId = Integer.parseInt(EncryptionDecryptionUtil.decryption(vendoreId));
					}
					vendor = merchantService.findVendorById(intVendorId);
					merchantList = merchantService.findAllMerchantsByVendorId(intVendorId);

					if (vendor != null) {
						session.setAttribute("vendor", vendor);
					}

					session.setAttribute("merchantList", merchantList);
				}

				Merchant merchant = merchantService.findById(intMerchantId);
				if (merchant == null) {
					LOGGER.info("HomeController.showReiProfilePageForMultiLocation() : END");

					// return "redirect:" + environment.getProperty("WEB_BASE_URL") + "/sessionTimeOut";
					return "redirect:https://www.foodkonnekt.com";
				}

				// session.setMaxInactiveInterval(2 * 60);
				Customer customer = (Customer) session.getAttribute("customer");
				if (customer != null) {
					if (customer.getMerchantt() != null) {
						Merchant customerMerchant = merchantService.findById(customer.getMerchantt().getId());
						System.out.println(merchant.getOwner().getId().intValue() != customerMerchant.getOwner().getId()
								.intValue());
						if (merchant.getOwner().getId().intValue() != customerMerchant.getOwner().getId().intValue()) {
							session.removeAttribute("customer");
						} else {
							if (customerMerchant.getId() == merchant.getId()) {
								customer.setMerchantt(merchant);
								session.setAttribute("customer", customer);
								System.out.println("customer logged in");
							} else {
								customer.setVendor(null);
								customer.setVendorId(merchant.getId());

								List<Customer> customers = customerrRepository.findByEmailIdAndPasswordAndMerchanttId(
										customer.getEmailId(), customer.getPassword(), merchant.getId());
								if (customers != null && !customers.isEmpty()) {
									/*
									 * JSONObject Customer = jObject.getJSONObject("DATA"); Customer customerResult
									 * = ProducerUtil.getCustomer(Customer);
									 */

									session.setAttribute("customer", customers.get(0));

								} else {
									session.setAttribute("merchant", merchant);

									saveDuplicateCustomerV2(request, response);
								}

							}
						}
					}
				}

				model.addAttribute("merchantList", merchantList);

				if (merchant != null && merchant.getIsInstall() != null
						&& merchant.getIsInstall() != IConstant.SOFT_DELETE) {
					session.setAttribute("merchant", merchant);
					session.setMaxInactiveInterval(24 * 60 * 60);
					if (vendor != null && vendor.getId() != null && vendor.getId() > 0)
						model.addAttribute("vendorId", vendor.getId());

					model.addAttribute("merchantId", merchant.getId());
				} else {
					LOGGER.info("HomeController.showReiProfilePageForMultiLocation() : END");

					return "redirect:https://www.foodkonnekt.com";
				}
			} else {
				LOGGER.info("HomeController.showReiProfilePageForMultiLocation() : END");

				return "redirect:https://www.foodkonnekt.com";
			}
		} catch (Exception e) {
			LOGGER.info("HomeController.showReiProfilePageForMultiLocation() : ERROR" + e);

			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
			}
			LOGGER.error("error: " + e.getMessage());
		}
		LOGGER.info("HomeController.showReiProfilePageForMultiLocation() : END");

		return environment.getProperty("WEB_BASE_URL") + "/userProfileV2";
	}
}
