package com.foodkonnekt.contoller;



import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.foodkonnekt.model.CustomerFeedback;
import com.foodkonnekt.model.KritiqAnalytics;
import com.foodkonnekt.model.OrderR;
import com.foodkonnekt.util.IConstant;
import com.foodkonnekt.util.MailSendUtil;

@Component
public class TestCronSchedular implements Job {
	private static final Logger LOGGER= LoggerFactory.getLogger(TestCronSchedular.class);

	@Autowired
	public Environment environment;
	
	 public void execute(JobExecutionContext context) throws JobExecutionException {
		// TODO Auto-generated method stub
			LOGGER.info("-----------------TestCronSchedular : Inside execute :: Start---------------------");

		Integer activeCustomerFeedback = IConstant.BOOLEAN_TRUE;
		DateFormat dateFormat = new SimpleDateFormat(IConstant.YYYYMMDD);
		DateFormat dateFormat1 = new SimpleDateFormat(IConstant.YYYYMMDDHHMMSS);
		java.util.Date date = new java.util.Date();
		
		String date1 = dateFormat.format(date).toString();
		LOGGER.info("date1----"+date1);
		String s1 = date1+IConstant.STARTDATE;
		String s2 = date1+IConstant.ENDDATE;
		
		Calendar calendar = Calendar.getInstance();
		Calendar calendarEnd = Calendar.getInstance();
		calendar.setTime(date);
		calendarEnd.setTime(date);
		/*calendar.add(Calendar.DAY_OF_MONTH, -14);
		Date startDateRange = calendar.getTime();
		
		calendar.add(Calendar.DAY_OF_MONTH, 1);
		Date endDateRange = calendar.getTime();*/
		Date startDateRange = null;
		Date endDateRange = null;
		calendar.add(Calendar.DAY_OF_MONTH, -14);
		try {
			startDateRange = new SimpleDateFormat(IConstant.YYYYMMDD).parse(dateFormat.format(calendar.getTime()));
		} catch (ParseException e1) {
			LOGGER.error("===============  TestCronSchedular : Inside execute :: Exception  ============= " + e1);

			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		//startDateRange = calendar.getTime().toString();
		//startDateRange = dateFormat.format(calendar.getTime());
		calendarEnd.add(Calendar.DAY_OF_MONTH, 1);
		//endDateRange= dateFormat.format(calendarEnd.getTime());
		try {
			endDateRange = new SimpleDateFormat(IConstant.YYYYMMDD).parse(dateFormat.format(calendarEnd.getTime()));
		} catch (ParseException e1) {
			LOGGER.error("===============  TestCronSchedular : Inside execute :: Exception  ============= " + e1);

			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		LOGGER.info("TestCronSchedular StartDate::"+ startDateRange + "EndDate::"+ endDateRange);
		
		java.util.Date startDate;
		try {
			List<KritiqAnalytics> kritiqAnalytics = new ArrayList<KritiqAnalytics>();
			startDate = dateFormat1.parse(s1);
			Date endDate = dateFormat1.parse(s2);
			 MailSendUtil.webhookMail("Test CronSchedular","Working at "+date,environment);
			List<OrderR> order=CronScheduler.mermoryOrderRepository.findByStartDateAndEndDateAndMerchantActiveCustomerFeedback(startDate, endDate,activeCustomerFeedback);
			
			for(OrderR order1:order)
			{
				
			Integer customerId=	order1.getCustomer().getId();
			LOGGER.info(" ===customerId : "+customerId);
			CustomerFeedback customerFeedback = CronScheduler.memoryCustomerFeedbackRepository.findByCustomerIdDateRange(customerId,startDateRange,endDateRange);
			
			if(customerFeedback== null){
				//System.out.println("customerFeedback is null");
				
			LOGGER.info(" === startDate : "+startDate+" endDate : "+endDate);
		List<OrderR> orderList=	CronScheduler.mermoryOrderRepository.findByStartDateAndEndDateAndCustomerId(startDate, endDate, customerId);
				
				MailSendUtil.sendOrderReceiptAndFeedbackEmail(order1, orderList,environment);
				
				Boolean status = false;
				
				if(kritiqAnalytics.isEmpty()){
					KritiqAnalytics analytics = new KritiqAnalytics();
					analytics.setMerchant(order1.getMerchant());
					analytics.setCustomerCount(1);
					kritiqAnalytics.add(analytics);
				}else{
					for(KritiqAnalytics kritiqAnalytic : kritiqAnalytics){
						if(kritiqAnalytic.getMerchant()!=null && kritiqAnalytic.getMerchant().getId().equals(order1.getMerchant().getId())){
							kritiqAnalytic.setCustomerCount(kritiqAnalytic.getCustomerCount()+1);
							status = true;
						}
					}
					LOGGER.info(" === status : "+status);
					if(!status){
						KritiqAnalytics analytics = new KritiqAnalytics();
						analytics.setMerchant(order1.getMerchant());
						analytics.setCustomerCount(1);
						kritiqAnalytics.add(analytics);
					}
					
				}
				
			}
				
			}
			
			
			
			
			//code for Birthday and anniversary mail
			/*DateFormat monthFormat = new SimpleDateFormat("MMM");
			DateFormat dayFormat = new SimpleDateFormat("dd");
			
			
			Calendar calendar = Calendar.getInstance(); 
			
			System.out.println("cakl-"+calendar.getTime());
			calendar.add(Calendar.DAY_OF_YEAR, 1);
			System.out.println("tomorrow date-"+calendar.getTime());
		//	Date dates = calendar.getTime();
			
			String month = monthFormat.format(calendar.getTime());
			String day = dayFormat.format(calendar.getTime());
			String wishDate = month+","+day;
			System.out.println(wishDate);
		
			List<Customer> birthdayList=CronScheduler.mermoryCustomerRepository.findByBirthDate(wishDate);
			List<Customer> anniversaryList=CronScheduler.mermoryCustomerRepository.findByAnniversaryDate(wishDate);
			System.out.println("brthdy-"+birthdayList.size());
			System.out.println("anvrsry-"+anniversaryList.size());
			for(Customer customerBirthday:birthdayList)
			{
				//MailSendUtil.birthdayWishMail(wishDate, customerBirthday);
				Boolean status = false;
				
				if(kritiqAnalytics.isEmpty()){
					KritiqAnalytics analytics = new KritiqAnalytics();
					analytics.setMerchant(customerBirthday.getMerchantt());
					analytics.setCustomerCount(1);
					kritiqAnalytics.add(analytics);
				}else{
					for(KritiqAnalytics kritiqAnalytic : kritiqAnalytics){
						if(kritiqAnalytic.getMerchant()!=null && kritiqAnalytic.getMerchant().getId().equals(customerBirthday.getMerchantt().getId())){
							kritiqAnalytic.setCustomerCount(kritiqAnalytic.getCustomerCount()+1);
							status = true;
						}
					}
					if(!status){
						KritiqAnalytics analytics = new KritiqAnalytics();
						analytics.setMerchant(customerBirthday.getMerchantt());
						analytics.setCustomerCount(1);
						kritiqAnalytics.add(analytics);
					}
					
				}
				
			}
			for(Customer customerAnniversary:anniversaryList)
			{
				//MailSendUtil.anniversaryWishMail(wishDate, customerAnniversary);
				
				Boolean status = false;
				
				if(kritiqAnalytics.isEmpty()){
					KritiqAnalytics analytics = new KritiqAnalytics();
					analytics.setMerchant(customerAnniversary.getMerchantt());
					analytics.setCustomerCount(1);
					kritiqAnalytics.add(analytics);
				}else{
					for(KritiqAnalytics kritiqAnalytic : kritiqAnalytics){
						if(kritiqAnalytic.getMerchant()!=null && kritiqAnalytic.getMerchant().getId().equals(customerAnniversary.getMerchantt().getId())){
							kritiqAnalytic.setCustomerCount(kritiqAnalytic.getCustomerCount()+1);
							status = true;
						}
					}
					if(!status){
						KritiqAnalytics analytics = new KritiqAnalytics();
						analytics.setMerchant(customerAnniversary.getMerchantt());
						analytics.setCustomerCount(1);
						kritiqAnalytics.add(analytics);
					}
					
				}
			}*/
			
			for(KritiqAnalytics kritiqAnalytic : kritiqAnalytics){
				kritiqAnalytic.setMailSendDate(new Date());
				CronScheduler.memoryKritiqAnalyticsRepository.save(kritiqAnalytic);
			}
			
			
			
					
			} catch (ParseException e) {
				LOGGER.error("===============  TestCronSchedular : Inside execute :: Exception  ============= " + e);

			// TODO Auto-generated catch block
			LOGGER.error("error: " + e.getMessage());
			}
	 }

}
