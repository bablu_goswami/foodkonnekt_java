package com.foodkonnekt.contoller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.foodkonnekt.model.Customer;
import com.foodkonnekt.model.CustomerFeedback;
import com.foodkonnekt.model.CustomerFeedbackAnswer;
import com.foodkonnekt.model.FeedbackQuestion;
import com.foodkonnekt.model.FeedbackQuestionCategory;
import com.foodkonnekt.model.Merchant;
import com.foodkonnekt.model.OrderR;
import com.foodkonnekt.model.Pos;
import com.foodkonnekt.model.Vendor;
import com.foodkonnekt.service.CustomerService;
import com.foodkonnekt.service.KritiqService;
import com.foodkonnekt.service.MerchantService;
import com.foodkonnekt.util.EncryptionDecryptionToken;
import com.foodkonnekt.util.EncryptionDecryptionUtil;
import com.foodkonnekt.util.IConstant;
import com.foodkonnekt.util.MailSendUtil;
import com.foodkonnekt.util.NonPosUrlUtil;
import com.foodkonnekt.util.ProducerUtil;
import com.foodkonnekt.util.UrlConstant;
import com.google.gson.Gson;

@Controller
@RequestMapping("/kritiq")
public class KritiqController {

	@Autowired
    private Environment environment;
	
	@Autowired
	private KritiqService kritiqService;

	@Autowired
	MerchantService merchantService;
	
	@Autowired
	private CustomerService customerService;

	private static final Logger LOGGER = LoggerFactory.getLogger(KritiqController.class);

	/**
	 * Show Inventory page
	 * 
	 * @return String
	 */
	/*
	 * @RequestMapping(value = "/feedbackForm", method = RequestMethod.GET)
	 * public String feedbackForm(ModelMap model, HttpServletRequest
	 * request,@RequestParam(value = "customerId") String
	 * customerId,@RequestParam(value = "orderId") String orderid) {
	 * 
	 * if(customerId!=null && !customerId.isEmpty() && orderid!=null &&
	 * !orderid.isEmpty() ){ OrderR orderR=
	 * kritiqService.validateCustomerAndOrder(customerId,orderid);
	 * List<CustomerFeedback> custFeedback =
	 * kritiqService.findByCustomerIdAndOrderId(customerId, orderid); boolean
	 * feedBackStatus= false; if(orderR!=null){ String orderType="pickup";
	 * if(orderR.getOrderType()!=null){ orderType=orderR.getOrderType(); }
	 * List<FeedbackQuestionCategory>
	 * feedbackQuestionCategories=kritiqService.getFeedbackQuestionCategorys(
	 * orderType);
	 * 
	 * if(custFeedback.size()>0) { System.out.println(custFeedback.get(0));
	 * feedBackStatus= true; model.addAttribute("FeedbackQuestionCategories",
	 * feedbackQuestionCategories); model.addAttribute("orderR", orderR);
	 * model.addAttribute("CustomerFeedback", new CustomerFeedback());
	 * model.addAttribute("feedBackStatus",feedBackStatus); return
	 * "feedbackForm"; }else{
	 * 
	 * System.out.println("feedBackStatus"+feedBackStatus);
	 * 
	 * model.addAttribute("FeedbackQuestionCategories",
	 * feedbackQuestionCategories); model.addAttribute("orderR", orderR);
	 * model.addAttribute("CustomerFeedback", new CustomerFeedback()); return
	 * "feedbackForm"; } }else{ return null;
	 * 
	 * }
	 * 
	 * }else{ return null; } }
	 */
	/*
	 * @RequestMapping(value = "/saveCustomerFeedback", method =
	 * RequestMethod.POST) public String
	 * saveCustomerFeedback(@ModelAttribute("CustomerFeedback") CustomerFeedback
	 * customerFeedback,ModelMap model, HttpServletRequest request) { try{
	 * System.out.println("custid "+customerFeedback.getCustomer().getId()
	 * +" ordrid-"+customerFeedback.getOrderR().getId()); int customerId =
	 * customerFeedback.getCustomer().getId(); int orderId =
	 * customerFeedback.getOrderR().getId(); String custId =
	 * EncryptionDecryptionUtil.encryption(String.valueOf(customerId)); String
	 * orderid = EncryptionDecryptionUtil.encryption(String.valueOf(orderId));
	 * List<CustomerFeedback> custFeedback =
	 * kritiqService.findByCustomerIdAndOrderId(custId, orderid);
	 * 
	 * if(custFeedback.size()>0) { System.out.println("size is >0"); return
	 * "redirect:feedbackForm?customerId="+custId+"&orderId="+orderid; }
	 * 
	 * kritiqService.saveCustomerFeedback(customerFeedback);
	 * System.out.println("Done"); }catch(Exception e){
	 * 
	 * if (e != null) { MailSendUtil.sendExceptionByMail(e,environment);
	 * 
	 * LOGGER.error("error: " + e.getMessage()); return "redirect:https://kritiq.us"; }
	 * 
	 * } return "feedbackshare"; }
	 */

	@RequestMapping(value = "/saveCustomerFeedbackUsingAjax", method = RequestMethod.POST)
	@ResponseBody
	public String saveCustomerFeedback(@ModelAttribute("CustomerFeedback") CustomerFeedback customerFeedback,
			ModelMap model, HttpServletRequest request) {
		LOGGER.info("KritiqController.saveCustomerFeedback() : START");

		try {
			LOGGER.info("custid " + customerFeedback.getCustomer().getId() + " ordrid-"
					+ customerFeedback.getOrderR().getId());
			int customerId = customerFeedback.getCustomer().getId();
			int orderId = customerFeedback.getOrderR().getId();
			String custId = EncryptionDecryptionUtil.encryption(String.valueOf(customerId));
			String orderid = EncryptionDecryptionUtil.encryption(String.valueOf(orderId));
			List<CustomerFeedback> custFeedback = kritiqService.findByCustomerIdAndOrderId(custId, orderid);

			if (custFeedback.size() > 0) {
				LOGGER.info("size is >0");
				return IConstant.RESPONSE_NO_DATA_MESSAGE;

				// return
				// "redirect:feedbackForm?customerId="+custId+"&orderId="+orderid;
			}

			kritiqService.saveCustomerFeedback(customerFeedback);
			System.out.println("Done");
		} catch (Exception e) {
			LOGGER.info("KritiqController.saveCustomerFeedback() : ERROR" + e);

			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);

				LOGGER.error("error: " + e.getMessage());
				return "redirect:https://kritiq.us";
			}

		}
		LOGGER.info("KritiqController.saveCustomerFeedback() : END");
		return IConstant.RESPONSE_SUCCESS_MESSAGE;
	}

	

	@RequestMapping(value = "/feedbackForm", method = RequestMethod.GET)
	public String feedbackForm(ModelMap model, HttpServletRequest request,
			@RequestParam(value = "customerId") String customerId, @RequestParam(value = "orderId") String orderid) {
		LOGGER.info("KritiqController.feedbackForm() : START");

		System.out.println("inside feedbackForm");
		if (customerId != null && !customerId.isEmpty() && orderid != null && !orderid.isEmpty()) {

			try {
				OrderR orderR = kritiqService.validateCustomerAndOrder(customerId, orderid);
				/* if(orderR.getMerchant().getActiveCustomerFeedback()==1){ */
				List<CustomerFeedback> custFeedback = kritiqService.findByCustomerIdAndOrderId(customerId, orderid);
				OrderR orderDetails = kritiqService.findByOrderId(orderid);

				int custId = 0;
				if (orderDetails != null)
					custId = orderDetails.getCustomer().getId();

				Date date = orderDetails.getCreatedOn();
				System.out.println(date);
				DateFormat dateFormat = new SimpleDateFormat(IConstant.YYYYMMDD);
				DateFormat dateFormat1 = new SimpleDateFormat(IConstant.YYYYMMDDHHMMSS);
				String date1 = dateFormat.format(date).toString();

				String s1 = date1 + IConstant.STARTDATE;
				String s2 = date1 + IConstant.ENDDATE;

				boolean pickup_flag = false;
				boolean delivery_flag = false;
				boolean dineIn_flag = false;

				java.util.Date startDate = dateFormat1.parse(s1);
				java.util.Date endDate = dateFormat1.parse(s2);

				List<OrderR> multipleOrderList = kritiqService.multipleOrdersListOfCustomer(startDate, endDate, custId);
				List<String> oType = new ArrayList<String>();

				for (OrderR multipleOrderList1 : multipleOrderList) {
					String type = multipleOrderList1.getOrderType();
					oType.add(type);
				}

				for (String o : oType) {
					if ("pickup".equals(o.toLowerCase())) {
						pickup_flag = true;
					} else if ("delivery".equals(o.toLowerCase())) {
						delivery_flag = true;
					} else
						dineIn_flag = true;

				}
				boolean feedBackStatus = false;
				if (orderR != null) {
					String orderType = "pickup";
					if (orderR.getOrderType() != null) {
						orderType = orderR.getOrderType();
					}
					List<FeedbackQuestionCategory> feedbackQuestionCategories = kritiqService
							.getFeedbackQuestionCategorys(orderType, pickup_flag, delivery_flag, dineIn_flag);
					// List<FeedbackQuestionCategory>
					// feedbackQuestionCategories=kritiqService.getFeedbackQuestionCategorys(orderType);
					model.addAttribute("merchantLogo", orderR.getMerchant().getMerchantLogo());
					if (orderR.getMerchant() != null) {
						String merchantKritiqExpiryStatus = kritiqService.checkMerchantExpiration(orderR.getMerchant());
						model.addAttribute("merchantKritiqExpiryStatus", merchantKritiqExpiryStatus);
					}
					if (custFeedback.size() > 0) {
						LOGGER.info(" custFeedback : "+custFeedback.get(0));
						feedBackStatus = true;
						model.addAttribute("FeedbackQuestionCategories", feedbackQuestionCategories);
						model.addAttribute("orderR", orderR);
						model.addAttribute("CustomerFeedback", new CustomerFeedback());
						model.addAttribute("feedBackStatus", feedBackStatus);
						LOGGER.info("KritiqController.feedbackForm() : END");
						return "feedbackForm";
					} else {

						LOGGER.info("feedBackStatus" + feedBackStatus);
						model.addAttribute("feedBackStatus", feedBackStatus);
						model.addAttribute("FeedbackQuestionCategories", feedbackQuestionCategories);
						model.addAttribute("orderR", orderR);
						model.addAttribute("CustomerFeedback", new CustomerFeedback());
						LOGGER.info("KritiqController.feedbackForm() : END");
						return "feedbackForm";
					}
				} else {
					LOGGER.info("KritiqController.feedbackForm() : END");
					return "redirect:https://kritiq.us";
				}

				/*
				 * }else{ return "redirect:https://kritiq.us"; }
				 */
			} catch (ParseException e) {
				LOGGER.info("KritiqController.feedbackForm() : ERROR" + e);

				// TODO Auto-generated catch block
				LOGGER.error("error: " + e.getMessage());
				return "redirect:https://kritiq.us";
			}

		} else {
			LOGGER.info("KritiqController.feedbackForm() : END");
			return "redirect:https://kritiq.us";
		}

	}

	@RequestMapping(value = "/feedbackForm1", method = RequestMethod.GET)
	public String feedbackForm1(ModelMap model, HttpServletRequest request,
			@RequestParam(value = "customerId") String customerId, @RequestParam(value = "orderId") String orderid) {
		LOGGER.info("KritiqController.feedbackForm1() : START");

		System.out.println("inside feedbackForm");
		if (customerId != null && !customerId.isEmpty() && orderid != null && !orderid.isEmpty()) {

			try {
				OrderR orderR = kritiqService.validateCustomerAndOrder(customerId, orderid);
				/* if(orderR.getMerchant().getActiveCustomerFeedback()==1){ */
				List<CustomerFeedback> custFeedback = kritiqService.findByCustomerIdAndOrderId(customerId, orderid);
				OrderR orderDetails = kritiqService.findByOrderId(orderid);

				int custId = orderDetails.getCustomer().getId();
				Date date = orderDetails.getCreatedOn();
				System.out.println(date);
				DateFormat dateFormat = new SimpleDateFormat(IConstant.YYYYMMDD);
				DateFormat dateFormat1 = new SimpleDateFormat(IConstant.YYYYMMDDHHMMSS);
				String date1 = dateFormat.format(date).toString();

				String s1 = date1 + IConstant.STARTDATE;
				String s2 = date1 + IConstant.ENDDATE;

				boolean pickup_flag = false;
				boolean delivery_flag = false;
				boolean dineIn_flag = false;

				java.util.Date startDate = dateFormat1.parse(s1);
				java.util.Date endDate = dateFormat1.parse(s2);

				List<OrderR> multipleOrderList = kritiqService.multipleOrdersListOfCustomer(startDate, endDate, custId);
				List<String> oType = new ArrayList<String>();

				for (OrderR multipleOrderList1 : multipleOrderList) {
					String type = multipleOrderList1.getOrderType();
					oType.add(type);
				}

				for (String o : oType) {
					if ("pickup".equals(o.toLowerCase())) {
						pickup_flag = true;
					} else if ("delivery".equals(o.toLowerCase())) {
						delivery_flag = true;
					} else
						dineIn_flag = true;

				}
				boolean feedBackStatus = false;
				if (orderR != null) {
					String orderType = "pickup";
					if (orderR.getOrderType() != null) {
						orderType = orderR.getOrderType();
					}
					List<FeedbackQuestionCategory> feedbackQuestionCategories = kritiqService
							.getFeedbackQuestionCategorys(orderType, pickup_flag, delivery_flag, dineIn_flag);
					// List<FeedbackQuestionCategory>
					// feedbackQuestionCategories=kritiqService.getFeedbackQuestionCategorys(orderType);
					model.addAttribute("merchantLogo", orderR.getMerchant().getMerchantLogo());
					if (orderR.getMerchant() != null) {
						String merchantKritiqExpiryStatus = kritiqService.checkMerchantExpiration(orderR.getMerchant());
						model.addAttribute("merchantKritiqExpiryStatus", merchantKritiqExpiryStatus);
					}
					if (custFeedback.size() > 0) {
						System.out.println(custFeedback.get(0));
						feedBackStatus = true;
						model.addAttribute("FeedbackQuestionCategories", feedbackQuestionCategories);
						model.addAttribute("orderR", orderR);
						model.addAttribute("CustomerFeedback", new CustomerFeedback());
						model.addAttribute("feedBackStatus", feedBackStatus);
						LOGGER.info("KritiqController.feedbackForm1() : END");
						return "feedbackForm1";
					} else {

						System.out.println("feedBackStatus" + feedBackStatus);
						model.addAttribute("feedBackStatus", feedBackStatus);
						model.addAttribute("FeedbackQuestionCategories", feedbackQuestionCategories);
						model.addAttribute("orderR", orderR);
						model.addAttribute("CustomerFeedback", new CustomerFeedback());
						LOGGER.info("KritiqController.feedbackForm1() : END");
						return "feedbackForm1";
					}
				} else {
					LOGGER.info("KritiqController.feedbackForm1() : END");
					return "redirect:https://kritiq.us";
				}

				/*
				 * }else{ return "redirect:https://kritiq.us"; }
				 */
			} catch (ParseException e) {
				LOGGER.info("KritiqController.feedbackForm1() : ERROR" + e);

				// TODO Auto-generated catch block
				LOGGER.error("error: " + e.getMessage());
				return "redirect:https://kritiq.us";
			}

		} else {
			LOGGER.info("KritiqController.feedbackForm1() : END");
			return "redirect:https://kritiq.us";
		}

	}

	@RequestMapping(value = "/vfeedbackForm", method = RequestMethod.GET)
	public String feedbackFormWalkInCustomer(ModelMap model, @RequestParam(value = "vendorId") String vendorId,
			HttpServletRequest request) {
		LOGGER.info("KritiqController.feedbackFormWalkInCustomer() : START");

		System.out.println("inside vfeedbackForm");
		try {
			System.out.println("feedbackFormWalkInCustomer");
			Vendor vendor = kritiqService.validateVendor(vendorId);
			System.out.println("vend id-" + vendor.getId());
			List<Merchant> merchantList = kritiqService.getMerchantsByVendorId(vendor.getId());
			List<FeedbackQuestionCategory> feedbackQuestionCategories = kritiqService.getFeedbackQuestionCategorys();
			String formType = "vendor";
			model.addAttribute("FeedbackQuestionCategories", feedbackQuestionCategories);
			model.addAttribute("merchantList", merchantList);

			// model.addAttribute("orderR", orderR);
			model.addAttribute("formType", formType);
			model.addAttribute("CustomerFeedback", new CustomerFeedback());
			// model.addAttribute("feedBackStatus",feedBackStatus);
		} catch (Exception e) {
			LOGGER.info("KritiqController.feedbackFormWalkInCustomer() : ERROR" + e);

			// TODO Auto-generated catch block
			LOGGER.error("error: " + e.getMessage());
			return "redirect:https://kritiq.us";
		}
		LOGGER.info("KritiqController.feedbackFormWalkInCustomer() : END");
		return "feedbackFormWalkInCustomer";
	}

	/*
	 * @RequestMapping(value = "/mfeedbackForm", method = RequestMethod.GET)
	 * public String feedbackFormWalkInCustomerByMerchant(ModelMap
	 * model, @RequestParam(value = "merchantId") String merchantId,
	 * HttpServletRequest request) { System.out.println("Inside mfeedbackForm");
	 * try{ System.out.println("feedbackFormWalkInCustomer"); Merchant merchant
	 * = kritiqService.validateMerchant(merchantId); // Vendor vendor =
	 * kritiqService.validateVendor(vendorId);
	 * System.out.println("vend id-"+merchant.getId()); // List<Merchant>
	 * merchantList = kritiqService.getMerchantsByVendorId(vendor.getId());
	 * Merchant merchant1 =
	 * kritiqService.getMerchantDetailsByMerchantId(merchant.getId());
	 * System.out.println( "pos id-->"+merchant1.getPosMerchantId());
	 * List<FeedbackQuestionCategory>
	 * feedbackQuestionCategories=kritiqService.getFeedbackQuestionCategorys();
	 * String formType ="merchant"; model.addAttribute("formType",formType);
	 * model.addAttribute("FeedbackQuestionCategories",
	 * feedbackQuestionCategories); model.addAttribute("posMerchantId",
	 * merchant1.getPosMerchantId()); //model.addAttribute("orderR", orderR);
	 * model.addAttribute("CustomerFeedback", new CustomerFeedback());
	 * //model.addAttribute("feedBackStatus",feedBackStatus); } catch (Exception
	 * e) { // TODO Auto-generated catch block LOGGER.error("error: " + e.getMessage()); return
	 * "redirect:https://kritiq.us"; }
	 * 
	 * return "feedbackFormWalkInCustomer"; }
	 */

	@RequestMapping(value = "/mfeedbackForm", method = RequestMethod.GET)
	public String feedbackFormWalkInCustomerByMerchant(ModelMap model,
			@RequestParam(required = false, value = "merchantId") String merchantId,
			@RequestParam(required = false, value = "orderId") String orderId, HttpServletRequest request) {
		LOGGER.info("KritiqController.feedbackFormWalkInCustomerByMerchant() : START");

		System.out.println("Inside mfeedbackForm");
		/*
		 * try{ if(orderId!=null){
		 * System.out.println("feedbackFormWalkInCustomer"); Merchant merchant =
		 * kritiqService.validateMerchant(merchantId);
		 * 
		 * Merchant merchant1 =
		 * kritiqService.getMerchantDetailsByMerchantId(merchant.getId());
		 * String
		 * merchantKritiqExpiryStatus=kritiqService.checkMerchantExpiration(
		 * merchant1); Vendor vendor =
		 * kritiqService.getVendorDetailsByVendorId(merchant1.getOwner().getId()
		 * ); List<FeedbackQuestionCategory>
		 * feedbackQuestionCategories=kritiqService.getFeedbackQuestionCategorys
		 * (); // List<FeedbackQuestionCategory>
		 * feedbackQuestionCategories=kritiqService.
		 * getFeedbackQuestionCategorysByPosId(vendor.getPos().getPosId());
		 * model.addAttribute("merchantLogo", merchant1.getMerchantLogo());
		 * model.addAttribute("FeedbackQuestionCategories",
		 * feedbackQuestionCategories); model.addAttribute("posMerchantId",
		 * merchant1.getPosMerchantId()); model.addAttribute("orderId",orderId);
		 * model.addAttribute("CustomerFeedback", new CustomerFeedback());
		 * model.addAttribute("CustomerFeedback", new CustomerFeedback());
		 * boolean feedBackStatus= false; List<CustomerFeedback> custFeedback =
		 * kritiqService.findByOrderPosID(orderId); if(custFeedback!=null &&
		 * custFeedback.size()>0) { feedBackStatus= true; }
		 * model.addAttribute("feedBackStatus",feedBackStatus);
		 * model.addAttribute("merchantKritiqExpiryStatus",
		 * merchantKritiqExpiryStatus); }else{ return
		 * "redirect:https://kritiq.us"; }
		 * 
		 * } catch (Exception e) { LOGGER.error("error: " + e.getMessage()); //return
		 * "redirect:https://kritiq.us"; }
		 */

		try {
			if (merchantId != null && !merchantId.isEmpty() && merchantId != "" && orderId != null && !orderId.isEmpty()
					&& orderId != "") {
				Merchant merchant = kritiqService.validateMerchant(merchantId);

				if (merchant != null) {
					Merchant merchant1 = kritiqService.getMerchantDetailsByMerchantId(merchant.getId());
					System.out.println(merchant1.getWebsite());
					HttpSession httpSession =request.getSession();
					String website=merchant1.getWebsite();
					if(website!=null){
						/*model.addAttribute("website",website);*/
						httpSession.setAttribute("website",website);
					}else{
						httpSession.setAttribute("website","https://kritiq.us");
					}
					String merchantKritiqExpiryStatus = kritiqService.checkMerchantExpiration(merchant1);
					model.addAttribute("merchantKritiqExpiryStatus", merchantKritiqExpiryStatus);
					OrderR order = kritiqService.getOrderDetailsByOrderPosIdAndMerchantId(orderId,merchant.getId());
					
					boolean feedBackStatus = false;
					if (order == null || order.getCustomer() == null) {
						List<FeedbackQuestionCategory> feedbackQuestionCategories = kritiqService
								.getFeedbackQuestionCategorys();

						List<CustomerFeedback> custFeedback = kritiqService.findByOrderPosIDAndMerchantId(orderId, merchant.getId());
						model.addAttribute("merchantLogo", merchant1.getMerchantLogo());
						model.addAttribute("FeedbackQuestionCategories", feedbackQuestionCategories);
						model.addAttribute("posMerchantId", merchant1.getPosMerchantId());
						model.addAttribute("orderId", orderId);
						// model.addAttribute("orderR", orderR);
						model.addAttribute("CustomerFeedback", new CustomerFeedback());

						if (custFeedback != null && custFeedback.size() > 0) {
							feedBackStatus = true;

						} else {
							model.addAttribute("FeedbackQuestionCategories", feedbackQuestionCategories);
							// model.addAttribute("orderR", orderR);
							model.addAttribute("CustomerFeedback", new CustomerFeedback());

						}
						model.addAttribute("feedBackStatus", feedBackStatus);

						LOGGER.info("KritiqController.feedbackFormWalkInCustomerByMerchant() : END");
						return "feedbackFormWalkInCustomer";

					} else {

						List<CustomerFeedback> custFeedback = kritiqService.findByOrderId(order.getId());
						CustomerFeedback customerFeedback = new CustomerFeedback();
						if (order.getCustomer() != null && order.getCustomer().getId() != null) {
							customerFeedback.setCustomer(order.getCustomer());
						}
						model.addAttribute("merchantLogo", merchant1.getMerchantLogo());
						if (custFeedback.size() > 0) {
							feedBackStatus = true;
							model.addAttribute("feedBackStatus", feedBackStatus);
							model.addAttribute("CustomerFeedback", new CustomerFeedback());
							LOGGER.info("KritiqController.feedbackFormWalkInCustomerByMerchant() : END");
							return "feedbackFormWalkInCustomer";

						} else {
							List<FeedbackQuestionCategory> feedbackQuestionCategories = kritiqService
									.getFeedbackQuestionCategorys();
							// System.out.println("today05-08-2017"+order.getCustomer().getId());
							String custId = EncryptionDecryptionUtil
									.encryption(String.valueOf(order.getCustomer().getId()));
							String orderid = EncryptionDecryptionUtil.encryption(String.valueOf(order.getId()));

							model.addAttribute("FeedbackQuestionCategories", feedbackQuestionCategories);
							model.addAttribute("posMerchantId", merchant1.getPosMerchantId());
							model.addAttribute("orderId", orderId);
							model.addAttribute("orderR", order);
							model.addAttribute("CustomerFeedback", new CustomerFeedback());
							model.addAttribute("feedBackStatus", feedBackStatus);
							LOGGER.info("KritiqController.feedbackFormWalkInCustomerByMerchant() : END");
							return "redirect:feedbackForm?customerId=" + custId + "&orderId=" + orderid;

						}
					}

				} else {
					LOGGER.info("KritiqController.feedbackFormWalkInCustomerByMerchant() : END");
					return "redirect:https://kritiq.us";
				}
				// model.addAttribute("feedBackStatus",feedBackStatus);
			} else {
				LOGGER.info("KritiqController.feedbackFormWalkInCustomerByMerchant() : END");
				return "redirect:https://kritiq.us";
			}
		} catch (Exception e) {
			LOGGER.info("KritiqController.feedbackFormWalkInCustomerByMerchant() : ERROR" + e);

			LOGGER.error("error: " + e.getMessage());
			return "redirect:https://kritiq.us";
		}
		// return "feedbackFormWalkInCustomer";
		// return "redirect:" + environment.getProperty("WEB_BASE_URL") +
		// "/feedbackFormWalkInCustomer";
	}

	@RequestMapping(value = "/ofeedbackForm", method = RequestMethod.GET)
	public String feedbackOrderIdByMerchant(ModelMap model, @RequestParam("merchantId") String merchantId,
			HttpServletRequest request) {
		LOGGER.info("KritiqController.feedbackOrderIdByMerchant() : START");

		try {
			Merchant merchant = kritiqService.validateMerchant(merchantId);
			if (merchant != null) {
				// System.out.println("vend id-"+merchant.getId());
				// List<Merchant> merchantList =
				// kritiqService.getMerchantsByVendorId(vendor.getId());
				Merchant merchant1 = kritiqService.getMerchantDetailsByMerchantId(merchant.getId());
				String merchantKritiqExpiryStatus = kritiqService.checkMerchantExpiration(merchant1);
				Vendor vendor = kritiqService.getVendorDetailsByVendorId(merchant1.getOwner().getId());
				// System.out.println( "pos
				// id-->"+merchant1.getPosMerchantId());
				// List<FeedbackQuestionCategory>
				// feedbackQuestionCategories=kritiqService.getFeedbackQuestionCategorys();
				String formType = "merchant";
				// model.addAttribute("formType",formType);
				// model.addAttribute("FeedbackQuestionCategories",
				// feedbackQuestionCategories);
				HttpSession httpSession =request.getSession();
				String website=merchant1.getWebsite();
				if(website!=null){
					/*model.addAttribute("website",website);*/
					httpSession.setAttribute("website",website);
				}else{
					httpSession.setAttribute("website","https://kritiq.us");
				}
				model.addAttribute("merchantLogo", merchant1.getMerchantLogo());
				model.addAttribute("posMerchantId", merchant1.getPosMerchantId());
				model.addAttribute("merchantKritiqExpiryStatus", merchantKritiqExpiryStatus);
				model.addAttribute("merchantId", merchantId);
				model.addAttribute("posId", vendor.getPos().getPosId());
				// model.addAttribute("orderR", orderR);
				model.addAttribute("CustomerFeedback", new CustomerFeedback());
				// model.addAttribute("feedBackStatus",feedBackStatus);
			} else {
				LOGGER.info("KritiqController.feedbackOrderIdByMerchant() : END");
				return "redirect:https://kritiq.us";
			}
		} catch (Exception e) {
			LOGGER.info("KritiqController.feedbackOrderIdByMerchant() : ERROR" + e);

			// TODO Auto-generated catch block
			LOGGER.error("error: " + e.getMessage());
			return "redirect:https://kritiq.us";
		}
		LOGGER.info("KritiqController.feedbackOrderIdByMerchant() : END");
		return "feedbackOrderId";
	}

	@RequestMapping(value = "/ofeedbackForm5", method = RequestMethod.GET)
	public String feedbackOrderIdByMerchantNew(ModelMap model, @RequestParam("merchantId") String merchantId,
			HttpServletRequest request) {
		LOGGER.info("KritiqController.feedbackOrderIdByMerchantNew() : START");

		try {
			Merchant merchant = kritiqService.validateMerchant(merchantId);
			if (merchant != null) {
				// System.out.println("vend id-"+merchant.getId());
				// List<Merchant> merchantList =
				// kritiqService.getMerchantsByVendorId(vendor.getId());
				Merchant merchant1 = kritiqService.getMerchantDetailsByMerchantId(merchant.getId());
				String merchantKritiqExpiryStatus = kritiqService.checkMerchantExpiration(merchant1);
				Vendor vendor = kritiqService.getVendorDetailsByVendorId(merchant1.getOwner().getId());
				// System.out.println( "pos
				// id-->"+merchant1.getPosMerchantId());
				// List<FeedbackQuestionCategory>
				// feedbackQuestionCategories=kritiqService.getFeedbackQuestionCategorys();
				String formType = "merchant";
				// model.addAttribute("formType",formType);
				// model.addAttribute("FeedbackQuestionCategories",
				// feedbackQuestionCategories);
				model.addAttribute("merchantLogo", merchant1.getMerchantLogo());
				model.addAttribute("posMerchantId", merchant1.getPosMerchantId());
				model.addAttribute("merchantKritiqExpiryStatus", merchantKritiqExpiryStatus);
				model.addAttribute("merchantId", merchantId);
				model.addAttribute("posId", vendor.getPos().getPosId());
				// model.addAttribute("orderR", orderR);
				model.addAttribute("CustomerFeedback", new CustomerFeedback());
				// model.addAttribute("feedBackStatus",feedBackStatus);
			} else {
				LOGGER.info("KritiqController.feedbackOrderIdByMerchantNew() : END");
				return "redirect:https://kritiq.us";
			}
		} catch (Exception e) {
			LOGGER.info("KritiqController.feedbackOrderIdByMerchantNew() : ERROR" + e);

			// TODO Auto-generated catch block
			LOGGER.error("error: " + e.getMessage());
			return "redirect:https://kritiq.us";
		}
		LOGGER.info("KritiqController.feedbackOrderIdByMerchantNew() : END");
		return "feedbackOrderId5";
	}

	@RequestMapping(value = "/mfeedbackForm5", method = RequestMethod.GET)
	public String feedbackFormWalkInCustomerByMerchant5(ModelMap model,
			@RequestParam(required = false, value = "merchantId") String merchantId,
			@RequestParam(required = false, value = "orderId") String orderId, HttpServletRequest request) {
		LOGGER.info("KritiqController.feedbackFormWalkInCustomerByMerchant5() : START");

		System.out.println("Inside mfeedbackForm5");

		try {
			if (merchantId != null && !merchantId.isEmpty() && merchantId != "" && orderId != null && !orderId.isEmpty()
					&& orderId != "") {
				Merchant merchant = kritiqService.validateMerchant(merchantId);

				if (merchant != null) {
					Merchant merchant1 = kritiqService.getMerchantDetailsByMerchantId(merchant.getId());
					String merchantKritiqExpiryStatus = kritiqService.checkMerchantExpiration(merchant1);
					model.addAttribute("merchantKritiqExpiryStatus", merchantKritiqExpiryStatus);
					OrderR order = kritiqService.getOrderDetailsByOrderPosId(orderId);
					boolean feedBackStatus = false;
					if (order == null) {
						List<FeedbackQuestionCategory> feedbackQuestionCategories = kritiqService
								.getFeedbackQuestionCategorys();

						List<CustomerFeedback> custFeedback = kritiqService.findByOrderPosID(orderId);
						model.addAttribute("merchantLogo", merchant1.getMerchantLogo());
						model.addAttribute("FeedbackQuestionCategories", feedbackQuestionCategories);
						model.addAttribute("posMerchantId", merchant1.getPosMerchantId());
						model.addAttribute("orderId", orderId);

						model.addAttribute("CustomerFeedback", new CustomerFeedback());

						if (custFeedback != null && custFeedback.size() > 0) {
							feedBackStatus = true;

						} else {
							model.addAttribute("FeedbackQuestionCategories", feedbackQuestionCategories);

							model.addAttribute("CustomerFeedback", new CustomerFeedback());

						}
						model.addAttribute("feedBackStatus", feedBackStatus);
						LOGGER.info("KritiqController.feedbackFormWalkInCustomerByMerchant5() : END");
						return "feedbackFormWalkInCustomer5";

					} else {

						List<CustomerFeedback> custFeedback = kritiqService.findByOrderId(order.getId());
						CustomerFeedback customerFeedback = new CustomerFeedback();
						if (order.getCustomer() != null && order.getCustomer().getId() != null) {
							customerFeedback.setCustomer(order.getCustomer());
						}
						model.addAttribute("merchantLogo", merchant1.getMerchantLogo());
						if (custFeedback.size() > 0) {
							feedBackStatus = true;
							model.addAttribute("feedBackStatus", feedBackStatus);
							model.addAttribute("CustomerFeedback", new CustomerFeedback());
							LOGGER.info("KritiqController.feedbackFormWalkInCustomerByMerchant5() : END");
							return "feedbackFormWalkInCustomer5";

						} else {
							List<FeedbackQuestionCategory> feedbackQuestionCategories = kritiqService
									.getFeedbackQuestionCategorys();
							System.out.println("today05-08-2017" + order.getCustomer().getId());
							String custId = EncryptionDecryptionUtil
									.encryption(String.valueOf(order.getCustomer().getId()));
							String orderid = EncryptionDecryptionUtil.encryption(String.valueOf(order.getId()));

							model.addAttribute("FeedbackQuestionCategories", feedbackQuestionCategories);
							model.addAttribute("posMerchantId", merchant1.getPosMerchantId());
							model.addAttribute("orderId", orderId);
							model.addAttribute("orderR", order);
							model.addAttribute("CustomerFeedback", new CustomerFeedback());
							model.addAttribute("feedBackStatus", feedBackStatus);
							LOGGER.info("KritiqController.feedbackFormWalkInCustomerByMerchant5() : END");
							return "redirect:feedbackForm?customerId=" + custId + "&orderId=" + orderid;

						}
					}

				} else {
					LOGGER.info("KritiqController.feedbackFormWalkInCustomerByMerchant5() : END");
					return "redirect:https://kritiq.us";
				}

			} else {
				LOGGER.info("KritiqController.feedbackFormWalkInCustomerByMerchant5() : END");
				return "redirect:https://kritiq.us";
			}
		} catch (Exception e) {
			LOGGER.info("KritiqController.feedbackFormWalkInCustomerByMerchant5() : ERROR" + e);

			LOGGER.error("error: " + e.getMessage());
			return "redirect:https://kritiq.us";
		}

	}

	@RequestMapping(value = "/ofeedbackForm6", method = RequestMethod.GET)
	public String feedbackOrderIdByMerchantNew6(ModelMap model, @RequestParam("merchantId") String merchantId,
			HttpServletRequest request) {
		LOGGER.info("KritiqController.ofeedbackForm6() : START");

		try {
			Merchant merchant = kritiqService.validateMerchant(merchantId);
			if (merchant != null) {
				// System.out.println("vend id-"+merchant.getId());
				// List<Merchant> merchantList =
				// kritiqService.getMerchantsByVendorId(vendor.getId());
				Merchant merchant1 = kritiqService.getMerchantDetailsByMerchantId(merchant.getId());
				String merchantKritiqExpiryStatus = kritiqService.checkMerchantExpiration(merchant1);
				Vendor vendor = kritiqService.getVendorDetailsByVendorId(merchant1.getOwner().getId());
				// System.out.println( "pos
				// id-->"+merchant1.getPosMerchantId());
				// List<FeedbackQuestionCategory>
				// feedbackQuestionCategories=kritiqService.getFeedbackQuestionCategorys();
				String formType = "merchant";
				// model.addAttribute("formType",formType);
				// model.addAttribute("FeedbackQuestionCategories",
				// feedbackQuestionCategories);
				model.addAttribute("merchantLogo", merchant1.getMerchantLogo());
				model.addAttribute("posMerchantId", merchant1.getPosMerchantId());
				model.addAttribute("merchantKritiqExpiryStatus", merchantKritiqExpiryStatus);
				model.addAttribute("merchantId", merchantId);
				model.addAttribute("posId", vendor.getPos().getPosId());
				// model.addAttribute("orderR", orderR);
				model.addAttribute("CustomerFeedback", new CustomerFeedback());
				// model.addAttribute("feedBackStatus",feedBackStatus);
			} else {
				LOGGER.info("KritiqController.ofeedbackForm6() : END");
				return "redirect:https://kritiq.us";
			}
		} catch (Exception e) {
			LOGGER.info("KritiqController.ofeedbackForm6() : ERROR" + e);

			// TODO Auto-generated catch block
			LOGGER.error("error: " + e.getMessage());
			return "redirect:https://kritiq.us";
		}
		LOGGER.info("KritiqController.ofeedbackForm6() : END");
		return "feedbackOrderId6";
	}

	@RequestMapping(value = "/mfeedbackForm6", method = RequestMethod.GET)
	public String feedbackFormWalkInCustomerByMerchant6(ModelMap model,
			@RequestParam(required = false, value = "merchantId") String merchantId,
			@RequestParam(required = false, value = "orderId") String orderId, HttpServletRequest request) {
		LOGGER.info("KritiqController.feedbackFormWalkInCustomerByMerchant6() : START");

		System.out.println("Inside mfeedbackForm6");

		try {
			if (merchantId != null && !merchantId.isEmpty() && merchantId != "" && orderId != null && !orderId.isEmpty()
					&& orderId != "") {
				Merchant merchant = kritiqService.validateMerchant(merchantId);

				if (merchant != null) {
					Merchant merchant1 = kritiqService.getMerchantDetailsByMerchantId(merchant.getId());
					String merchantKritiqExpiryStatus = kritiqService.checkMerchantExpiration(merchant1);
					model.addAttribute("merchantKritiqExpiryStatus", merchantKritiqExpiryStatus);
					OrderR order = kritiqService.getOrderDetailsByOrderPosId(orderId);
					boolean feedBackStatus = false;
					if (order == null) {
						List<FeedbackQuestionCategory> feedbackQuestionCategories = kritiqService
								.getFeedbackQuestionCategorys();

						List<CustomerFeedback> custFeedback = kritiqService.findByOrderPosID(orderId);
						model.addAttribute("merchantLogo", merchant1.getMerchantLogo());
						model.addAttribute("FeedbackQuestionCategories", feedbackQuestionCategories);
						model.addAttribute("posMerchantId", merchant1.getPosMerchantId());
						model.addAttribute("orderId", orderId);

						model.addAttribute("CustomerFeedback", new CustomerFeedback());

						if (custFeedback != null && custFeedback.size() > 0) {
							feedBackStatus = true;

						} else {
							model.addAttribute("FeedbackQuestionCategories", feedbackQuestionCategories);

							model.addAttribute("CustomerFeedback", new CustomerFeedback());

						}
						model.addAttribute("feedBackStatus", feedBackStatus);
						LOGGER.info("KritiqController.feedbackFormWalkInCustomerByMerchant6() : END");
						return "feedbackFormWalkInCustomer6";

					} else {

						List<CustomerFeedback> custFeedback = kritiqService.findByOrderId(order.getId());
						CustomerFeedback customerFeedback = new CustomerFeedback();
						if (order.getCustomer() != null && order.getCustomer().getId() != null) {
							customerFeedback.setCustomer(order.getCustomer());
						}
						model.addAttribute("merchantLogo", merchant1.getMerchantLogo());
						if (custFeedback.size() > 0) {
							feedBackStatus = true;
							model.addAttribute("feedBackStatus", feedBackStatus);
							model.addAttribute("CustomerFeedback", new CustomerFeedback());
							LOGGER.info("KritiqController.feedbackFormWalkInCustomerByMerchant6() : END");
							return "feedbackFormWalkInCustomer6";

						} else {
							List<FeedbackQuestionCategory> feedbackQuestionCategories = kritiqService
									.getFeedbackQuestionCategorys();
							System.out.println("today05-08-2017" + order.getCustomer().getId());
							String custId = EncryptionDecryptionUtil
									.encryption(String.valueOf(order.getCustomer().getId()));
							String orderid = EncryptionDecryptionUtil.encryption(String.valueOf(order.getId()));

							model.addAttribute("FeedbackQuestionCategories", feedbackQuestionCategories);
							model.addAttribute("posMerchantId", merchant1.getPosMerchantId());
							model.addAttribute("orderId", orderId);
							model.addAttribute("orderR", order);
							model.addAttribute("CustomerFeedback", new CustomerFeedback());
							model.addAttribute("feedBackStatus", feedBackStatus);
							LOGGER.info("KritiqController.feedbackFormWalkInCustomerByMerchant6() : END");
							return "redirect:feedbackForm?customerId=" + custId + "&orderId=" + orderid;

						}
					}

				} else {
					LOGGER.info("KritiqController.feedbackFormWalkInCustomerByMerchant6() : END");
					return "redirect:https://kritiq.us";
				}

			} else {
				LOGGER.info("KritiqController.feedbackFormWalkInCustomerByMerchant6() : END");
				return "redirect:https://kritiq.us";
			}
		} catch (Exception e) {
			LOGGER.info("KritiqController.feedbackFormWalkInCustomerByMerchant6() : ERROR" + e);

			LOGGER.error("error: " + e.getMessage());
			return "redirect:https://kritiq.us";
		}

	}

	@RequestMapping(value = "/ofeedbackForm7", method = RequestMethod.GET)
	public String feedbackOrderIdByMerchantNew7(ModelMap model, @RequestParam("merchantId") String merchantId,
			HttpServletRequest request) {
		LOGGER.info("KritiqController.feedbackOrderIdByMerchantNew7() : START");

		try {
			Merchant merchant = kritiqService.validateMerchant(merchantId);
			if (merchant != null) {
				// System.out.println("vend id-"+merchant.getId());
				// List<Merchant> merchantList =
				// kritiqService.getMerchantsByVendorId(vendor.getId());
				Merchant merchant1 = kritiqService.getMerchantDetailsByMerchantId(merchant.getId());
				String merchantKritiqExpiryStatus = kritiqService.checkMerchantExpiration(merchant1);
				Vendor vendor = kritiqService.getVendorDetailsByVendorId(merchant1.getOwner().getId());
				// System.out.println( "pos
				// id-->"+merchant1.getPosMerchantId());
				// List<FeedbackQuestionCategory>
				// feedbackQuestionCategories=kritiqService.getFeedbackQuestionCategorys();
				String formType = "merchant";
				// model.addAttribute("formType",formType);
				// model.addAttribute("FeedbackQuestionCategories",
				// feedbackQuestionCategories);
				model.addAttribute("merchantLogo", merchant1.getMerchantLogo());
				model.addAttribute("posMerchantId", merchant1.getPosMerchantId());
				model.addAttribute("merchantKritiqExpiryStatus", merchantKritiqExpiryStatus);
				model.addAttribute("merchantId", merchantId);
				model.addAttribute("posId", vendor.getPos().getPosId());
				// model.addAttribute("orderR", orderR);
				model.addAttribute("CustomerFeedback", new CustomerFeedback());
				// model.addAttribute("feedBackStatus",feedBackStatus);
			} else {
				LOGGER.info("KritiqController.feedbackOrderIdByMerchantNew7() : END");
				return "redirect:https://kritiq.us";
			}
		} catch (Exception e) {
			LOGGER.info("KritiqController.feedbackOrderIdByMerchantNew7() : ERROR" + e);

			// TODO Auto-generated catch block
			LOGGER.error("error: " + e.getMessage());
			return "redirect:https://kritiq.us";
		}
		LOGGER.info("KritiqController.feedbackOrderIdByMerchantNew7() : END");
		return "feedbackOrderId7";
	}

	@RequestMapping(value = "/mfeedbackForm7", method = RequestMethod.GET)
	public String feedbackFormWalkInCustomerByMerchant7(ModelMap model,
			@RequestParam(required = false, value = "merchantId") String merchantId,
			@RequestParam(required = false, value = "orderId") String orderId, HttpServletRequest request) {
		LOGGER.info("KritiqController.feedbackFormWalkInCustomerByMerchant7() : START");

		System.out.println("Inside mfeedbackForm6");

		try {
			if (merchantId != null && !merchantId.isEmpty() && merchantId != "" && orderId != null && !orderId.isEmpty()
					&& orderId != "") {
				Merchant merchant = kritiqService.validateMerchant(merchantId);

				if (merchant != null) {
					Merchant merchant1 = kritiqService.getMerchantDetailsByMerchantId(merchant.getId());
					String merchantKritiqExpiryStatus = kritiqService.checkMerchantExpiration(merchant1);
					model.addAttribute("merchantKritiqExpiryStatus", merchantKritiqExpiryStatus);
					OrderR order = kritiqService.getOrderDetailsByOrderPosId(orderId);
					boolean feedBackStatus = false;
					if (order == null) {
						List<FeedbackQuestionCategory> feedbackQuestionCategories = kritiqService
								.getFeedbackQuestionCategorys();

						List<CustomerFeedback> custFeedback = kritiqService.findByOrderPosID(orderId);
						model.addAttribute("merchantLogo", merchant1.getMerchantLogo());
						model.addAttribute("FeedbackQuestionCategories", feedbackQuestionCategories);
						model.addAttribute("posMerchantId", merchant1.getPosMerchantId());
						model.addAttribute("orderId", orderId);

						model.addAttribute("CustomerFeedback", new CustomerFeedback());

						if (custFeedback != null && custFeedback.size() > 0) {
							feedBackStatus = true;

						} else {
							model.addAttribute("FeedbackQuestionCategories", feedbackQuestionCategories);

							model.addAttribute("CustomerFeedback", new CustomerFeedback());

						}
						model.addAttribute("feedBackStatus", feedBackStatus);
						LOGGER.info("KritiqController.feedbackFormWalkInCustomerByMerchant7() : END");
						return "feedbackFormWalkInCustomer7";

					} else {

						List<CustomerFeedback> custFeedback = kritiqService.findByOrderId(order.getId());
						CustomerFeedback customerFeedback = new CustomerFeedback();
						if (order.getCustomer() != null && order.getCustomer().getId() != null) {
							customerFeedback.setCustomer(order.getCustomer());
						}
						model.addAttribute("merchantLogo", merchant1.getMerchantLogo());
						if (custFeedback.size() > 0) {
							feedBackStatus = true;
							model.addAttribute("feedBackStatus", feedBackStatus);
							model.addAttribute("CustomerFeedback", new CustomerFeedback());
							LOGGER.info("KritiqController.feedbackFormWalkInCustomerByMerchant7() : END");
							return "feedbackFormWalkInCustomer7";

						} else {
							List<FeedbackQuestionCategory> feedbackQuestionCategories = kritiqService
									.getFeedbackQuestionCategorys();
							System.out.println("today05-08-2017" + order.getCustomer().getId());
							String custId = EncryptionDecryptionUtil
									.encryption(String.valueOf(order.getCustomer().getId()));
							String orderid = EncryptionDecryptionUtil.encryption(String.valueOf(order.getId()));

							model.addAttribute("FeedbackQuestionCategories", feedbackQuestionCategories);
							model.addAttribute("posMerchantId", merchant1.getPosMerchantId());
							model.addAttribute("orderId", orderId);
							model.addAttribute("orderR", order);
							model.addAttribute("CustomerFeedback", new CustomerFeedback());
							model.addAttribute("feedBackStatus", feedBackStatus);
							LOGGER.info("KritiqController.feedbackFormWalkInCustomerByMerchant7() : END");
							return "redirect:feedbackForm?customerId=" + custId + "&orderId=" + orderid;

						}
					}

				} else {
					LOGGER.info("KritiqController.feedbackFormWalkInCustomerByMerchant7() : END");
					return "redirect:https://kritiq.us";
				}

			} else {
				LOGGER.info("KritiqController.feedbackFormWalkInCustomerByMerchant7() : END");
				return "redirect:https://kritiq.us";
			}
		} catch (Exception e) {
			LOGGER.info("KritiqController.feedbackFormWalkInCustomerByMerchant7() : ERROR" + e);

			LOGGER.error("error: " + e.getMessage());
			return "redirect:https://kritiq.us";
		}

	}

	@RequestMapping(value = "/cfeedbackForm", method = RequestMethod.GET)
	public String cfeedbackForm(ModelMap model, @RequestParam(value = "m") String m,
			@RequestParam(value = "o") String o, @RequestParam(value = "c") String c, HttpServletRequest request) {
		LOGGER.info("KritiqController.cfeedbackForm() : START");

		System.out.println("inside cfeedbackForm");
		if (m != null && !m.isEmpty() && m != "" && o != null && !o.isEmpty() && o != "") {

			try {

				Merchant merchant = kritiqService.getMerchantDetailsByPosMerchantId(m);

				if (merchant != null) {
					String merchantKritiqExpiryStatus = kritiqService.checkMerchantExpiration(merchant);
					model.addAttribute("merchantKritiqExpiryStatus", merchantKritiqExpiryStatus);
					OrderR order = kritiqService.getOrderDetailsByOrderPosId(o);
					boolean feedBackStatus = false;
					if (order == null) {
						List<FeedbackQuestionCategory> feedbackQuestionCategories = kritiqService
								.getFeedbackQuestionCategorys();
						boolean data = kritiqService.verifyOrderFromClover(o, m);
						if (data) {
							List<CustomerFeedback> custFeedback = kritiqService.findByOrderPosID(o);
							model.addAttribute("FeedbackQuestionCategories", feedbackQuestionCategories);
							model.addAttribute("posMerchantId", m);
							model.addAttribute("orderId", o);
							// model.addAttribute("orderR", orderR);
							model.addAttribute("CustomerFeedback", new CustomerFeedback());

							if (custFeedback != null && custFeedback.size() > 0) {
								feedBackStatus = true;

							} else {
								model.addAttribute("FeedbackQuestionCategories", feedbackQuestionCategories);
								// model.addAttribute("orderR", orderR);
								model.addAttribute("CustomerFeedback", new CustomerFeedback());
							}
							model.addAttribute("feedBackStatus", feedBackStatus);

							LOGGER.info("KritiqController.cfeedbackForm() : END");
							return "feedbackFormWalkInCustomer";
						} else {
							LOGGER.info("KritiqController.cfeedbackForm() : END");
							return "redirect:https://kritiq.us";
						}

					} else {

						List<CustomerFeedback> custFeedback = kritiqService.findByOrderId(order.getId());
						CustomerFeedback customerFeedback = new CustomerFeedback();
						if (order.getCustomer() != null && order.getCustomer().getId() != null) {
							customerFeedback.setCustomer(order.getCustomer());
						}
						if (custFeedback.size() > 0) {
							feedBackStatus = true;
							model.addAttribute("feedBackStatus", feedBackStatus);
							model.addAttribute("CustomerFeedback", new CustomerFeedback());
							LOGGER.info("KritiqController.cfeedbackForm() : END");
							return "feedbackFormWalkInCustomer";

						} else {
							List<FeedbackQuestionCategory> feedbackQuestionCategories = kritiqService
									.getFeedbackQuestionCategorys();
							System.out.println("today05-08-2017" + order.getCustomer().getId());
							String custId = EncryptionDecryptionUtil
									.encryption(String.valueOf(order.getCustomer().getId()));
							String orderid = EncryptionDecryptionUtil.encryption(String.valueOf(order.getId()));

							model.addAttribute("FeedbackQuestionCategories", feedbackQuestionCategories);
							model.addAttribute("posMerchantId", m);
							model.addAttribute("orderId", o);
							model.addAttribute("orderR", order);
							model.addAttribute("CustomerFeedback", new CustomerFeedback());
							model.addAttribute("feedBackStatus", feedBackStatus);
							LOGGER.info("KritiqController.cfeedbackForm() : END");
							return "redirect:feedbackForm?customerId=" + custId + "&orderId=" + orderid;

						}
					}

				} else {
					LOGGER.info("KritiqController.cfeedbackForm() : END");
					return "redirect:https://kritiq.us";
				}
				// model.addAttribute("feedBackStatus",feedBackStatus);
			} catch (Exception e) {
				LOGGER.info("KritiqController.cfeedbackForm() : ERROR" + e);

				LOGGER.error("error: " + e.getMessage());
				return "redirect:https://kritiq.us";
			}
		} else {
			LOGGER.info("KritiqController.cfeedbackForm() : END");
			return "redirect:https://kritiq.us";
		}

	}

	@RequestMapping(value = "/verifyOrderFromClover", method = RequestMethod.GET)
	public @ResponseBody Boolean verifyOrderFromClover(String orderId, String merchantId,
			HttpServletResponse response) {
		LOGGER.info("KritiqController.verifyOrderFromClover() : START");

		System.out.println("inside /verifyOrderFromClover");
		try {
			boolean data = kritiqService.verifyOrderFromClover(orderId, merchantId);

			return data;

		} catch (Exception e) {
			LOGGER.info("KritiqController.verifyOrderFromClover() : ERROR" + e);
			LOGGER.info("KritiqController.verifyOrderFromClover() : END");
			LOGGER.error("error: " + e.getMessage());
			return false;
		}

	}

	/*
	 * @RequestMapping(value = "/saveWalkInCustomerFeedback", method =
	 * RequestMethod.POST) public String
	 * saveWalkInCustomerFeedback(@ModelAttribute("CustomerFeedback")
	 * CustomerFeedback customerFeedback,
	 * 
	 * @RequestParam(value = "merchantPosId") String merchantPosId, ModelMap
	 * model, HttpServletRequest request) { Customer customer = new Customer();
	 * 
	 * Merchant merchantDetail
	 * =kritiqService.getMerchantDetailsByPosMerchantId(merchantPosId);
	 * 
	 * OrderR order =
	 * kritiqService.getOrderDetailsByOrderPosId(customerFeedback.getOrderR().
	 * getOrderPosId()); if(order!=null) { customerFeedback.setOrderR(order);
	 * List<CustomerFeedback> custFeedback =
	 * kritiqService.findByOrderPosID(order.getOrderPosId());
	 * System.out.println("custFeedback-"+custFeedback.size()); boolean
	 * feedBackStatus= false; if(custFeedback!=null && custFeedback.size()>0) {
	 * System.out.println(custFeedback.get(0)); feedBackStatus= true;
	 * 
	 * model.addAttribute("feedBackStatus",feedBackStatus); return
	 * "feedbackFormWalkInCustomer"; }else{
	 * System.out.println(customerFeedback.getCustomer().getId());
	 * kritiqService.saveCustomerFeedback(customerFeedback); // return
	 * "feedbackResponse"; return "feedbackshare"; }
	 * 
	 * 
	 * } //customerFeedback.getCustomer().setMerchantt(merchantDetail);
	 * 
	 * customer =customerFeedback.getCustomer();
	 * customer.setMerchantt(merchantDetail); Customer customer1
	 * =kritiqService.saveWalkInCustomerDetail(customer);
	 * 
	 * System.out.println(customer1.getBirthDate()+" an-"+customer1.
	 * getAnniversaryDate()); OrderR orderR = customerFeedback.getOrderR();
	 * orderR.setCustomer(customer1); orderR =
	 * kritiqService.saveOrderDetails(orderR);
	 * System.out.println("orderRc id-"+orderR.getId());
	 * 
	 * 
	 * kritiqService.saveCustomerFeedback(customerFeedback);
	 * System.out.println("Done"); //return "feedbackResponse"; return
	 * "feedbackshare"; }
	 */

	@RequestMapping(value = "/saveWalkInCustomerFeedbackByAjax", method = RequestMethod.POST)
	@ResponseBody
	public String saveWalkInCustomerFeedback(@ModelAttribute("CustomerFeedback") CustomerFeedback customerFeedback,
			@RequestParam(value = "merchantPosId") String merchantPosId, ModelMap model, HttpServletRequest request,HttpSession httpSession) {
		Customer customer = new Customer();
		try {		LOGGER.info("KritiqController.saveWalkInCustomerFeedback() : START");

			
			Merchant merchantDetail = kritiqService.getMerchantDetailsByPosMerchantId(merchantPosId);
			if (merchantDetail != null) {
				customerFeedback.setMerchant(merchantDetail);
			
			String website=merchantDetail.getWebsite();
			if(website!=null){
				/*model.addAttribute("website",website);*/
				httpSession.setAttribute("website",website);
			}else{
				httpSession.setAttribute("website","https://kritiq.us");
			}
			}
			OrderR order = kritiqService.getOrderDetailsByOrderPosIdAndMerchantId(customerFeedback.getOrderR().getOrderPosId(),merchantDetail.getId());
			if (order != null && customerFeedback != null && customerFeedback.getCustomer() != null
					&& customerFeedback.getCustomer().getId() != null) {
				customerFeedback.setOrderR(order);
				List<CustomerFeedback> custFeedback = kritiqService.findByOrderPosID(order.getOrderPosId());
				System.out.println("custFeedback-" + custFeedback.size());
				boolean feedBackStatus = false;
				if (custFeedback != null && custFeedback.size() > 0) {
					System.out.println(custFeedback.get(0));
					feedBackStatus = true;

					model.addAttribute("feedBackStatus", feedBackStatus);
					LOGGER.info("KritiqController.saveWalkInCustomerFeedback() : End");
					return "feedbackFormWalkInCustomer";
				} else {
					LOGGER.info(" customerFeedback.getCustomer().getId() : "+customerFeedback.getCustomer().getId());
					kritiqService.saveCustomerFeedback(customerFeedback);
					// return "feedbackResponse";
					LOGGER.info("KritiqController.saveWalkInCustomerFeedback() : END");
					return IConstant.RESPONSE_SUCCESS_MESSAGE;
				}

			}
			// customerFeedback.getCustomer().setMerchantt(merchantDetail);

			customer = customerFeedback.getCustomer();
			customer.setMerchantt(merchantDetail);
			Customer customer1 = kritiqService.saveWalkInCustomerDetail(customer);

			System.out.println(customer1.getBirthDate() + " an-" + customer1.getAnniversaryDate());
			OrderR orderR = customerFeedback.getOrderR();
			if (order != null) {
				orderR = order;
				customerFeedback.setOrderR(orderR);
			}
			//order.setMerchant(merchantDetail);
			orderR.setCustomer(customer1);
			orderR.setMerchant(merchantDetail);
			orderR = kritiqService.saveOrderDetails(orderR);
			LOGGER.info("orderRc id-" + orderR.getId());

			kritiqService.saveCustomerFeedback(customerFeedback);
			
			System.out.println("Done");
			LOGGER.info("KritiqController.saveWalkInCustomerFeedback() : END");
			
			// return "feedbackResponse";
			return IConstant.RESPONSE_SUCCESS_MESSAGE;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			LOGGER.error("error: " + e.getMessage());
			LOGGER.info("KritiqController.saveWalkInCustomerFeedback() : Exception : "+e);
			
			return IConstant.RESPONSE_NO_DATA_MESSAGE;
		}
	}

	@RequestMapping(value = "/customerfeedback", method = RequestMethod.GET)
	public String customerFeedbackResult(ModelMap model, @RequestParam("customerFeedbackId") String customerFeedbackId,
			HttpServletRequest request) {
		LOGGER.info("KritiqController.customerFeedbackResult() : STARTS : ");

		if (customerFeedbackId != null && customerFeedbackId != "" && !customerFeedbackId.isEmpty()) {
			Map<Integer, Integer> queAnswer = new HashMap<Integer, Integer>();
			List<FeedbackQuestionCategory> feedbackQuestionCategories = new ArrayList<FeedbackQuestionCategory>();
			// Stack<Integer> categoryId = null;
			Integer custFeedbackId = Integer.parseInt(EncryptionDecryptionUtil.decryption(customerFeedbackId));
			List<CustomerFeedbackAnswer> customerFeedbackAnswers = kritiqService
					.getCustomerFeedbackResult(custFeedbackId);
			if (customerFeedbackAnswers != null && !customerFeedbackAnswers.isEmpty()) {
				for (CustomerFeedbackAnswer listCustomerFeedback : customerFeedbackAnswers) {
					Integer key = listCustomerFeedback.getFeedbackQuestion().getId();
					Integer value = listCustomerFeedback.getAnswer();
					queAnswer.put(key, value);
					FeedbackQuestionCategory feedbackQuestionCategory = kritiqService.getFeedbackQuestionCategory(
							listCustomerFeedback.getFeedbackQuestion().getFeedbackQuestionCategory().getId());
					boolean categoryAlreadyAdded = false;
					for (FeedbackQuestionCategory questionCategory : feedbackQuestionCategories) {
						if (feedbackQuestionCategory.getId() == questionCategory.getId()) {
							categoryAlreadyAdded = true;
							feedbackQuestionCategory = questionCategory;
							break;
						}

					}
					if (!categoryAlreadyAdded) {
						List<FeedbackQuestion> feedbackQuestions = new ArrayList<FeedbackQuestion>();
						feedbackQuestions.add(listCustomerFeedback.getFeedbackQuestion());
						feedbackQuestionCategory.setFeedbackQuestions(feedbackQuestions);
						feedbackQuestionCategories.add(feedbackQuestionCategory);
					} else {
						List<FeedbackQuestion> feedbackQuestions = feedbackQuestionCategory.getFeedbackQuestions();
						feedbackQuestions.add(listCustomerFeedback.getFeedbackQuestion());
						feedbackQuestionCategory.setFeedbackQuestions(feedbackQuestions);
					}

				}
				CustomerFeedback customerFeedback = null;
				customerFeedback = kritiqService.getCustomerFeedback(custFeedbackId);
				if (customerFeedback == null) {
					customerFeedback = new CustomerFeedback();
				} else {
					customerFeedback.setCustomerFeedbackAnswers(customerFeedbackAnswers);
					if (customerFeedback != null) {
						Customer customer = customerFeedback.getCustomer();
						String anniversary = customer.getAnniversaryDate();
						if (anniversary != null) {
							String[] monthDate = anniversary.split(",");
							if (monthDate != null && monthDate.length > 1) {
								String month = monthDate[0];
								String date = monthDate[1];
								if (month != null && date != null) {
									customerFeedback.setAnniversaryMonth(month);
									customerFeedback.setAnniversaryDate(date);
								}
							}
						} else {
							customerFeedback.setAnniversaryMonth("Pick a Month");
							customerFeedback.setAnniversaryDate("Pick a Date");
						}
						String birthDay = customer.getBirthDate();
						if (birthDay != null) {
							String[] monthDate = birthDay.split(",");
							if (monthDate != null && monthDate.length > 1) {
								String month = monthDate[0];
								String date = monthDate[1];
								if (month != null && date != null) {
									customerFeedback.setBdayMonth(month);
									customerFeedback.setBdayDate(date);
								}
							}
						} else {
							customerFeedback.setBdayMonth("Pick a Month");
							customerFeedback.setBdayDate("Pick a Month");

						}
					}
				}

				model.addAttribute("CustomerFeedback", customerFeedback);
				model.addAttribute("queAnswer", queAnswer);
				// feedbackQuestionCategories=kritiqService.getFeedbackQuestionCategorys();
				model.addAttribute("FeedbackQuestionCategories", feedbackQuestionCategories);

			} else {
				LOGGER.info("KritiqController.customerFeedbackResult() : END : ");

				return "redirect:https://kritiq.us";
			}
			LOGGER.info("KritiqController.customerFeedbackResult() : END : ");

			return "displayCustomerFeedback";
		} else {
			LOGGER.info("KritiqController.customerFeedbackResult() : END : ");

			return "redirect:https://kritiq.us";
		}

	}

	
	@RequestMapping(value = "getPositiveAndNegetiveFeedbackCount", method = RequestMethod.GET)
	public @ResponseBody Map<String, Integer> getPositiveAndNegetiveFeedbackCount(
			@RequestParam("merchantUid") String merchantUid, HttpServletResponse response) {
		LOGGER.info("KritiqController.getPositiveAndNegetiveFeedbackCount() : STARTS : ");

		Map<String, Integer> feedbackMap = new HashMap<String, Integer>();
		try {
			feedbackMap = kritiqService.getPositiveAndNegetiveFeedbackCount(merchantUid);
		} catch (Exception e) {
			LOGGER.error("error: " + e.getMessage());
		}
		Gson gson = new Gson();
		String jsonData = gson.toJson(feedbackMap);
		LOGGER.info("jsonData---" + jsonData);
		LOGGER.info("KritiqController.getPositiveAndNegetiveFeedbackCount() : END : ");

		return feedbackMap;
	}

	@RequestMapping(value = "getPositiveAndNegetiveFeedbackCountByDateRange", method = RequestMethod.GET)
	public @ResponseBody Map<String, Integer> getPositiveAndNegetiveFeedbackCountByDateRange(
			@RequestParam("merchantUid") String merchantUid, @RequestParam("startDate") String startDate,
			@RequestParam("endDate") String endDate, HttpServletResponse response) {
		Map<String, Integer> feedbackMap = new HashMap<String, Integer>();
		try {
			LOGGER.info("KritiqController.getPositiveAndNegetiveFeedbackCountByDateRange() : STARTS : ");

			feedbackMap = kritiqService.getPositiveAndNegetiveFeedbackCountByDateRange(merchantUid, startDate, endDate);
		} catch (Exception e) {
			LOGGER.info("KritiqController.getPositiveAndNegetiveFeedbackCountByDateRange() : Exception : "+e);

			LOGGER.error("error: " + e.getMessage());
		}
		Gson gson = new Gson();
		String jsonData = gson.toJson(feedbackMap);
		LOGGER.info("jsonData---" + jsonData);
		LOGGER.info("KritiqController.getPositiveAndNegetiveFeedbackCountByDateRange() : END : ");

		return feedbackMap;
	}

	@RequestMapping(value = "/{brandName}", method = RequestMethod.GET)
	public String multiLocationKritiq(ModelMap model, @PathVariable("brandName") String brandName) {
		LOGGER.info("KritiqController.multiLocationKritiq() : STARTS : ");
LOGGER.info(" === brandName : "+brandName);
		String response = null;
		if(brandName!=null && !brandName.equals("")&& !brandName.isEmpty()){
		try {
			String clientModuleURL = null;

			brandName = URLEncoder.encode(brandName);
			clientModuleURL = environment.getProperty("MkonnektPlatform_BASE_URL") + "entity/getLocationDetailsByBrandName?brandName="
					+ brandName + "&applicationId=3";

			System.out.println("done");

			HttpClient client = HttpClientBuilder.create().build();
			HttpGet httpRequest = new HttpGet(clientModuleURL);
			HttpResponse httpResponse = client.execute(httpRequest);

			BufferedReader rd = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent()));
			StringBuffer result = new StringBuffer();
			String line = "";
			while ((line = rd.readLine()) != null) {
				result.append(line);
			}
			response = result.toString();
		} catch (Exception e) {
			LOGGER.info("KritiqController.multiLocationKritiq() : Exception : "+e);

			System.out.println(e);
		}

		LOGGER.info(" response : "+response);
		if(response!=null && !response.equals("null")&&!response.equals("") &&!response.isEmpty()){
		String locationName = null;
		String locationUid = null;
		Map<String, String> map = new HashMap<String, String>();
		JSONArray jarr = new JSONArray(response);
		if (jarr != null) {
			for (Object object : jarr) {
				JSONObject jsonObject = (JSONObject) object;
				
				if (jsonObject.toString().contains("locationName")) {
					locationName = (String) jsonObject.get("locationName");
				}
				if (jsonObject.toString().contains("locationUid")) {
					locationUid = (String) jsonObject.get("locationUid");
				}
				LOGGER.info(" === locationName : "+locationName);
				LOGGER.info(" === locationUid : "+locationUid);
				map.put(locationUid, locationName);
			}
		
		}
		String base64encode=null;
		Merchant merchant = merchantService.findByMerchantUid(locationUid);
		if(merchant!=null){
		Integer merchantId=merchant.getId();
		String id = merchantId.toString();
		base64encode = EncryptionDecryptionUtil.encryption(id);
		
		System.out.println(map.size());
		if (map.size() == 0) {
			LOGGER.info("KritiqController.multiLocationKritiq() : END : ");

			return "redirect:" + "https://www.kritiq.us";
		} else if (map.size() > 1) {
			model.addAttribute("locationMap", map);
			model.addAttribute("merchantLogo", merchant.getMerchantLogo());
			LOGGER.info("KritiqController.multiLocationKritiq() : END : ");

			return "multiLocationKritiq";
		} else if(map.size()==1){
			LOGGER.info("KritiqController.multiLocationKritiq() : END : ");

			return "redirect:" + environment.getProperty("WEB_KRITIQ_BASE_URL") + "/ofeedbackForm?merchantId=" + base64encode;
		}
		else {
			LOGGER.info("KritiqController.multiLocationKritiq() : END : ");

			return "redirect:" + environment.getProperty("WEB_KRITIQ_BASE_URL") + "/test?merchantUid=" + locationUid;
		}}else{
			LOGGER.info("KritiqController.multiLocationKritiq() : END : ");

			return "redirect:" + "https://www.kritiq.us";
		}
		}else{
			LOGGER.info("KritiqController.multiLocationKritiq() : END : ");

			return "redirect:" + "https://www.kritiq.us";
		}
		}else{
			LOGGER.info("KritiqController.multiLocationKritiq() : END : ");

			return "redirect:" + "https://www.kritiq.us";
		}
	}

	@RequestMapping(value = "/test", method = RequestMethod.GET)
	public @ResponseBody String continueFeedback(@RequestParam String merchantUid, Model model) {
		LOGGER.info("KritiqController.continueFeedback() : starts : ");

		String base64encode = null;
		String merchantId = null;
		Integer id = null;
		String response = null;
		String merchantKritiqExpiryStatus = null;

		if (merchantUid != null) {
			Merchant merchant = merchantService.findByMerchantUid(merchantUid);
			if (merchant != null) {
				id = merchant.getId();
				if (id != null) {
					merchantId = id.toString();
					base64encode = EncryptionDecryptionUtil.encryption(merchantId);

					Merchant merchant1 = kritiqService.getMerchantDetailsByMerchantId(id);
					if (merchant1 != null) {
						merchantKritiqExpiryStatus = kritiqService.checkMerchantExpiration(merchant1);
						// OrderR orderR= kritiqService.findByOrderId(orderid);

						if (merchantKritiqExpiryStatus == "enabled" && merchantKritiqExpiryStatus != null) {
							Vendor vendor = kritiqService.getVendorDetailsByVendorId(merchant1.getOwner().getId());
							if (vendor != null) {
								model.addAttribute("merchantLogo", merchant1.getMerchantLogo());
								model.addAttribute("posMerchantId", merchant1.getPosMerchantId());
								model.addAttribute("merchantKritiqExpiryStatus", merchantKritiqExpiryStatus);
								model.addAttribute("merchantId", base64encode);
								model.addAttribute("posId", vendor.getPos().getPosId());
								// model.addAttribute("orderR", orderR);
								model.addAttribute("CustomerFeedback", new CustomerFeedback());
							}

						} else {
							model.addAttribute("merchantKritiqExpiryStatus", merchantKritiqExpiryStatus);
						}
						Gson gson = new Gson();
						response = gson.toJson(model);
					}
				}
			}
		}
		LOGGER.info("KritiqController.continueFeedback() : END : ");

		return response;
	}

	@RequestMapping(value = "/sendkritiqUrlThroughMail", method = RequestMethod.GET)
	@ResponseBody
	public String sendKritiqUrlThroughMail(@RequestParam String locationUid, @RequestParam String customerUid) {
		LOGGER.info("KritiqController.sendKritiqUrlThroughMail() : starts : ");
LOGGER.info(" === locationUid : "+locationUid+" customerUid : "+customerUid);
		String base64encode = null;
		String response = null;
		String customerEmail = null;
		//String clientModuleURL = null;
		if (locationUid.equals("a46a548d-10ac-4667-8278-f6df02208f83")) {
		Merchant merchant = merchantService.findByMerchantUid(locationUid);
		
		if (merchant == null) {
			
			String merchantDetails =  NonPosUrlUtil.getMerchantDetailsForDev(locationUid,environment);
	    	   if(merchantDetails.contains("locationUid")){
	    		    merchant =  merchantService.saveMerchantLogin(merchantDetails,locationUid, null);
	    		   if(merchant!=null){
	    			  
	    				   Vendor vendor = merchant.getOwner();
	    				   Pos pos = vendor.getPos();
	    				   pos.setPosId(3);
	    				   vendor.setPos(pos);
	    				   merchant.setOwner(vendor);
	    				   merchant.setActiveCustomerFeedback(1);
	    				   merchant.setAllowDeliveryTiming(0);
	    				   merchant.setFutureDaysAhead(0);
	    				   merchant.setAllowReOrder(false);
	    				   merchant.setAllowAuxTax(0);
	    				   merchant.setAllowMultiPay(false);
	    				   merchant.setAllowMultipleKoupon(0);
	    				   
	    				   merchant = merchantService.save(merchant);
	    		   }
	    	  }  
		}
		
		if (merchant != null) {
			
			String orderPosId = null;
				while(true){
					orderPosId = EncryptionDecryptionToken.randomString(6);
					OrderR orderR = kritiqService.getOrderDetailsByOrderPosIdAndMerchantId(orderPosId, merchant.getId());
					if (orderR == null) {
						break;
				}
			}
			System.out.println("inside if");
			try {
				response= ProducerUtil.customerDataThroughUid(customerUid,environment);
				JSONObject jObject = new JSONObject(response);
				if (jObject != null) {
					if (jObject.toString().contains("emailId")) {
						customerEmail = (String) jObject.get("emailId");
						LOGGER.info(" customerEmail : "+customerEmail);
					}
				}
			} catch (Exception e) {
				LOGGER.info("KritiqController.sendKritiqUrlThroughMail() : Exception : "+e);

				LOGGER.error("error: " + e.getMessage());
			}

			
			if (locationUid != null & locationUid != "" && !locationUid.isEmpty()) {
				base64encode = EncryptionDecryptionUtil.encryption(merchant.getId().toString());
				String url = environment.getProperty("WEB_KRITIQ_BASE_URL") + "/mfeedbackForm?merchantId=" + base64encode+"&orderId="+orderPosId;

				String logo = "";
				if (merchant.getMerchantLogo() != null) {
					logo = environment.getProperty("BASE_PORT") + merchant.getMerchantLogo();
				} else {
					logo = environment.getProperty("WEB_KRITIQ_BASE_URL") + "/resources/kritiq/img/logo.jpg";
				}

				String address = "";
				if (merchant.getAddress() != null) {
					address = merchant.getAddress();
				}
			    
				if (customerEmail != null) {
						MailSendUtil.sendKritiqUrlMailToCustomer(customerEmail, url, merchant.getName(), address, logo);
						System.out.println("mail sent");
				}
			}
		} 
	}
		LOGGER.info("KritiqController.sendKritiqUrlThroughMail() : END : ");

		return "success";
	}
	
	@RequestMapping(value="getKritiqFeedback", method = RequestMethod.POST)
	public @ResponseBody String kritiqFeedback(@RequestParam String customerUid){
		LOGGER.info("KritiqController.kritiqFeedback() : STARTS : ");

		List<CustomerFeedback> customerFeedbacks = customerService.getAllFeedbackOfCustomer(customerUid);
		Gson gson = new Gson();
		String responseJson = (String)gson.toJson(customerFeedbacks);
		LOGGER.info("KritiqController.kritiqFeedback() : END : ");

		return responseJson;
	}

	 @RequestMapping(value="/customerFeedbackV2" , method = RequestMethod.GET)
	    public String customerFeedbackNew(@RequestParam(value = "customerId") String customerId, @RequestParam(value = "orderId") String orderid,
	    		ModelMap model,HttpServletRequest request,@ModelAttribute("CustomerFeedback") CustomerFeedback customerFeedback) {
		LOGGER.info("KritiqController.customerFeedbackNew() : START");

		System.out.println("inside customer feedbackForm");
		boolean feedbackStatus = false;
		if (customerId != null && !customerId.isEmpty() && orderid != null
				&& !orderid.isEmpty()) {
			try {
				
				List<CustomerFeedback> custFeedback = kritiqService.findByCustomerIdAndOrderId(customerId, orderid);
				OrderR orderR = kritiqService.validateCustomerAndOrder(customerId, orderid);
				String merchantKritiqExpiryStatus = kritiqService.checkMerchantExpiration(orderR.getMerchant());
				model.addAttribute("merchantKritiqExpiryStatus", merchantKritiqExpiryStatus);
				
				String merchantLogo = "https://www.foodkonnekt.com/foodkonnekt_merchat_logos/190_12%2008.jpg";
				if(orderR.getMerchant()!=null && orderR.getMerchant().getMerchantLogo()!=null){
					merchantLogo = orderR.getMerchant().getMerchantLogo();
				}
				
				model.addAttribute("merchantLogo", merchantLogo);
				if (custFeedback.size() > 0) {
					System.out.println("size is >0");
					feedbackStatus = true;
					model.addAttribute("feedbackStatus", feedbackStatus);
					return "customerFeedbackNew";
				}else{
					model.addAttribute("feedbackStatus", feedbackStatus);
					if(orderR!=null){
						model.addAttribute("orderR", orderR);
					}
					OrderR orderDetails = kritiqService.findByOrderId(orderid);
					if(orderDetails!=null){
						model.addAttribute("orderDetails", orderDetails);
					}
					Integer custId = Integer.parseInt(EncryptionDecryptionUtil.decryption(customerId));
					Customer customer = customerService.findByCustomerId(custId);
					if(customer!=null){
						model.addAttribute("customer", customer);
					}
					LOGGER.info("KritiqController.customerFeedbackNew() : END");

					return "customerFeedbackNew";
				}
			} catch (Exception e) {
				LOGGER.info("KritiqController.customerFeedbackNew() : Exception : "+e);

				LOGGER.error("error: " + e.getMessage());
				return "redirect:https://kritiq.us";
			}
		}
		LOGGER.info("KritiqController.customerFeedbackNew() : END");

		return "redirect:https://kritiq.us";
	}
	 
	 
	 @RequestMapping(value = "/saveCustomerFeedbackV2", method = RequestMethod.POST)
		public String saveCustomerFeedbackNew(@ModelAttribute("CustomerFeedback") CustomerFeedback customerFeedback,
				ModelMap model, HttpServletRequest request) {
			LOGGER.info("KritiqController.saveCustomerFeedbackNew() : Starts");

			kritiqService.saveCustomerFeedbackNew(customerFeedback);
			LOGGER.info("KritiqController.saveCustomerFeedbackNew() : END");

			return "redirect:https://www.foodkonnekt.com";
				
		}
		
	 
}
