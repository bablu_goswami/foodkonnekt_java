package com.foodkonnekt.contoller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;

import com.foodkonnekt.model.Merchant;
import com.foodkonnekt.model.MerchantConfiguration;
import com.foodkonnekt.model.NotificationMethod;
import com.foodkonnekt.model.OrderDiscount;
import com.foodkonnekt.model.OrderItem;
import com.foodkonnekt.model.OrderItemModifier;
import com.foodkonnekt.model.OrderR;
import com.foodkonnekt.util.AppNotification;
import com.foodkonnekt.util.DateUtil;
import com.foodkonnekt.util.EncryptionDecryptionUtil;
import com.foodkonnekt.util.IConstant;
import com.foodkonnekt.util.MailSendUtil;
import com.foodkonnekt.util.OrderUtil;
import com.foodkonnekt.util.ResponseDto;
import com.foodkonnekt.util.ShortCodeUtil;
import com.foodkonnekt.util.UrlConstant;
import com.google.gson.Gson;
@Controller
public class TestSchedular  {

	private static final Logger LOGGER= LoggerFactory.getLogger(TestSchedular.class);
	
	@Autowired
    private Environment environment;
	
	public static  void execute(Integer merchantId,Environment environment) {
		try{
			if(merchantId!=null)
			{
			LOGGER.info("-----------------c : Inside execute :: Start---------------------");
		
			LOGGER.info("TestSchedular :: execute : fire schedular on "+new Date());
			Merchant merchant= CronScheduler.memorymerchantRepository.findById(merchantId);
			ResponseDto responseDto=new ResponseDto();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date date=DateUtil.getCurrentDateForTimeZonee(merchant.getTimeZone().getTimeZoneCode());
    	String currentDate =simpleDateFormat.format(date);
		StringBuilder orderDetails= new StringBuilder();
		String merchantLogo = "";
		String convenienceFeeValue = "0";
		StringBuilder discountCoupon = new StringBuilder();
		Double orderDiscount = 0.0;
		List<Map<String, String>> extraList = null;
		Map<String, String> poduct = null;
		Map<String, String> exctra = null;
		Map<String, Map<String, String>> notf = new HashMap<String, Map<String, String>>();
		Map<String, String> notification = new HashMap<String, String>();
		List<Map<String, String>> productList = new ArrayList<Map<String, String>>();
		Map<String, String> order = new HashMap<String, String>();
		String orderStatus="";
		Boolean enableNotification=false;
		Boolean autoAcceptFutureOrder = false;
		Boolean autoAccpetOrder = false;
		String merchantName="";
		List<NotificationMethod> notificationMethodList=  CronScheduler.schedularNotificationMethodRepository.findByMerchantIdAndIsActive(merchantId);
		
		List<OrderR> orderR=CronScheduler.schedularOrderRepository.findOrdersTodayDateAndMerchent(currentDate,merchantId);
		 
		MerchantConfiguration merchantConfiguration=CronScheduler.schedularMerchantConfigurationRepository.findByMerchantId(merchantId);
		if(merchantConfiguration!=null)
		{
			if(merchantConfiguration.getEnableNotification()!=null)
			{
			enableNotification=merchantConfiguration.getEnableNotification();
			responseDto.setEnableNotification(enableNotification);
			}
			if(merchantConfiguration.getAutoAccept()!=null)
			{
				autoAcceptFutureOrder=merchantConfiguration.getAutoAccept();
				responseDto.setAutoAccept(autoAcceptFutureOrder);
			}
			if(merchantConfiguration.getAutoAcceptOrder()!=null)
			{
				responseDto.setAutoAccept(merchantConfiguration.getAutoAcceptOrder());
			}
		}
		if(orderR!=null && !orderR.isEmpty())
		{
			responseDto.setPendingOrderCount(orderR.size());
			responseDto.setFutureOrderCount(orderR.size());
			responseDto.setMessage("Data Found Success");
			merchant = orderR.get(0).getMerchant();
			if(merchant!=null && merchant.getName()!=null)
			merchantName=merchant.getName();
			if (merchant.getMerchantLogo() == null) {
                merchantLogo = "http://www.dev.foodkonnekt.com/app/foodnew.jpg";
            } else {
                merchantLogo = environment.getProperty("BASE_PORT") + merchant.getMerchantLogo();
            }
		for(int i=0;i<orderR.size();i++)
		{
            LOGGER.info(" === orderR.get(i).getId() : " + orderR.get(i).getId());
            if(orderR.get(i)!=null && orderR.get(i).getId()!=null && orderR.get(i).getIsDefaults()==0 && autoAcceptFutureOrder)
            {
            	if(merchant.getOwner().getPos() != null && merchant.getOwner().getPos().getPosId() != null
						&& merchant.getOwner().getPos().getPosId() != IConstant.FOCUS
						&& merchant.getOwner().getPos().getPosId() != IConstant.POS_FOODTRONIX) {
					if (merchant.getOwner().getPos().getPosId() != IConstant.POS_CLOVER && merchant.getId()!=null  && ( merchant.getId()==624||merchant.getId()==190))
					{
						orderStatus = OrderUtil.orderConfirmationByIdByMerchantUId(orderR.get(i).getId(),
								merchant.getMerchantUid(), enableNotification ,environment);
						
						LOGGER.info("TestSchedular : orderConfirmationByIdByMerchantUId call from schedular ");
					}
					else if(merchant.getModileDeviceId()==null)
						orderStatus = OrderUtil.orderConfirmationByOrderPosId(orderR.get(i).getOrderPosId(),environment);
					LOGGER.info("TestSchedular : orderConfirmationByIdByMerchantUId call from schedular");
				}
            	orderR.set(i, CronScheduler.schedularOrderRepository.findOne(orderR.get(i).getId()));
            	LOGGER.info("TestSchedular : orderConfirmationByIdByMerchantUId order Id "+orderR.get(i).getId());
            	LOGGER.info("TestSchedular : orderConfirmationByIdByMerchantUId orderStatus "+orderStatus);
            	
            }

		List<OrderItem> orderItems=CronScheduler.schedularOrderItemRepository.findByOrderId(orderR.get(i).getId());
		if(orderItems!=null && !orderItems.isEmpty())
		{
			
			for (OrderItem orderItem : orderItems) {
				poduct = new HashMap<String, String>();
                if (orderItem.getItem() != null) {
                    String items = "<tr style='font-weight:600;text-transform:capitalize;font-family:arial'><td width='200px;'>"
                                    + orderItem.getItem().getName()
                                    + "</td><td width='100px;' style='text-align:center'>"
                                    + orderItem.getQuantity()
                                    + "</td><td width='100px;' style='text-align:center'>"
                                    + "$"
                                    + orderItem.getItem().getPrice() + "</td></tr>";
                    orderDetails.append("<b>").append(items).append("</b>");
                    poduct.put("product_id", orderItem.getItem().getPosItemId());
					poduct.put("name", orderItem.getItem().getName());
					poduct.put("price", Double.toString(orderItem.getItem().getPrice()));
					poduct.put("qty", Integer.toString(orderItem.getQuantity()));
                }
                LOGGER.info(" === orderItem.getId() : " + orderItem.getId());
                extraList = new ArrayList<Map<String, String>>();
                List<OrderItemModifier> itemModifiers = CronScheduler.schedularOrderItemModifierRepository.findByOrderItemId(orderItem.getId());
                for (OrderItemModifier itemModifier : itemModifiers) {
                	exctra = new HashMap<String, String>();
                    String modifiers = "<tr style='text-transform:capitalize;font-family:arial;margin-top:5px;font-size:12px'><td width='200px;'>"
                                    + itemModifier.getModifiers().getName()
                                    + "</td><td width='100px;' style='text-align:center'>"
                                    + itemModifier.getQuantity()
                                    + " </td><td width='100px;' style='text-align:center'> "
                                    + "$"
                                    + itemModifier.getModifiers().getPrice() + "</td></tr>";
                    orderDetails.append(modifiers);
                    exctra.put("id", itemModifier.getModifiers().getPosModifierId());
					exctra.put("price", Double.toString(itemModifier.getModifiers().getPrice()));
					exctra.put("name", itemModifier.getModifiers().getName());
					exctra.put("qty", Integer.toString(orderItem.getQuantity()));
					extraList.add(exctra);
                }

				Gson gson = new Gson();
				if (extraList.size() > 0) {
					String extraJson = gson.toJson(extraList);
					poduct.put("extras", extraJson);
					productList.add(poduct);
				} else {
					productList.add(poduct);
				}
            }
            orderDetails = new StringBuilder("<table width='300px;'><tbody>" + orderDetails + "</table></tbody>");
			/*
		for(int j=0;j<orderItem.size();j++)
		{
					List<OrderItemModifier>	orderItemModifier	=OrderController.schedularOrderItemModifierRepository.findByOrderItemId(orderItem.get(j).getId());	
					LOGGER.info(""+orderItemModifier);
					
		}
		*/}
		if (orderR.get(i).getOrderDiscount() != null){
            orderDiscount = orderR.get(i).getOrderDiscount();
            LOGGER.info(" === orderR.get(i).getId() : " + orderR.get(i).getId());

            List<OrderDiscount> orderDiscounts = CronScheduler.schedularOrderDiscountRepository.findByOrderId(orderR.get(i).getId());
            if(orderDiscounts!=null && !orderDiscounts.isEmpty()){
            discountCoupon.append("<tr>")
					.append("<td>Discount Coupon</td>")
					.append("<td>&nbsp;</td>")
					.append("<td>")
					.append(orderDiscounts.get(0).getCouponCode())
					.append("</td>")
					.append("</tr>");
            
            if(orderDiscounts.size()>1){
                for (int k = 1; k < orderDiscounts.size(); k++) {
                    discountCoupon.append("<tr>")
							.append("<td></td>")
							.append("<td>&nbsp;</td>")
							.append("<td>")
							.append(orderDiscounts.get(k).getCouponCode())
							.append("</td>")
							.append("</tr>");
                }
            }
            }
        }
		LOGGER.info("TestCronSchedular before send app notification : "+(merchant!=null && merchant.getModileDeviceId()!=null));
		if(merchant!=null && merchant.getModileDeviceId()!=null)
		{
			 LOGGER.info("TestCronSchedular send app notification");
			Gson gson = new Gson();
			String productJson = gson.toJson(productList);
			order.put("total", Double.toString(orderR.get(i).getOrderPrice()));
			order.put("tax", orderR.get(i).getTax());
			order.put("productItems", productJson);
			String orderJson = gson.toJson(order);
			notification.put("appEvent", "notification");
			notification.put("payload", gson.toJson(responseDto));
			
			notf.put("notification", notification);
			String notificationJson = gson.toJson(notf);
			notificationJson = notificationJson.trim().replaceAll("\\\\+\"", "'").replace("'[", "[")
					.replace("]'", "]");
			if (merchant != null && merchant.getOwner() != null && merchant.getOwner().getPos() != null
					&& merchant.getOwner().getPos().getPosId() != null
					&& merchant.getOwner().getPos().getPosId() == IConstant.POS_CLOVER) {
				LOGGER.info("TestCronSchedular IConstant.POS_CLOVER" + IConstant.POS_CLOVER);
				LOGGER.info("TestCronSchedular call clover notification" );
				LOGGER.info("TestCronSchedular call clover notificationJson" +notificationJson);
				//CronScheduler.schedularOrderService.sendNotification(notificationJson, orderR.get(i).getMerchant().getPosMerchantId(),
				//		orderR.get(i).getMerchant().getAccessToken());
				
			} else{
				try {
					LOGGER.info("TestCronSchedular call non-pos");
					String orderNotificationJson = CronScheduler.schedularOrderService.getNotificationJSON(orderR.get(i).getId(),
							merchant.getModileDeviceId(), merchant.getMerchantUid(),
							merchant.getPosMerchantId());
					 String finalJson="{\"data\": {\"merchantId\":\""+merchant.getId()+"\",\"locationUid\":\""+merchant.getMerchantUid()+"\",\"deviceId\":\""+merchant.getModileDeviceId()+"\",\"payload\":"+gson.toJson(responseDto)+"} ,\"registration_ids\":[\""+merchant.getModileDeviceId()+"\"],\"android\": {\"priority\": \"high\"}}";
					LOGGER.info("OrderServiceImpl :: updateOrderStatus : finalJson " + finalJson);
					if (merchant != null && merchant.getOwner() != null && merchant.getOwner().getPos() != null
							&& merchant.getOwner().getPos().getPosId() != null
							&& merchant.getOwner().getPos().getPosId() == IConstant.NON_POS){
						LOGGER.info("TestCronSchedular call non-pos inside if");
						LOGGER.info("TestCronSchedular call non-pos finalJson"+finalJson);
					//AppNotification.sendPushNotification(merchant.getModileDeviceId(), finalJson,
//							merchant.getPosMerchantId(),environment);
					}else {
						LOGGER.info("TestCronSchedular call non-pos inside else");
						LOGGER.info("TestCronSchedular call non-pos finalJson"+finalJson);
					//	AppNotification.sendPushNotification(merchant.getModileDeviceId(), finalJson,
//								merchant.getPosMerchantId(),environment);
					}
				} catch (Exception e) {
					MailSendUtil.sendExceptionByMail(e,environment);
					LOGGER.error("error: " + e.getMessage());
					LOGGER.error("OrderServiceImpl :: updateOrderStatus : Exception " + e);
				}
			}
		}
		}
		
		LOGGER.info("TestSchedular :: execute : NotificationMethod length "+notificationMethodList.size());
		for(int i=0;i<notificationMethodList.size();i++)
		{
			if(notificationMethodList.get(i).getNotificationMethodType().getId()==1 && orderR != null && !orderR.isEmpty())
			{
				MailSendUtil.futureOnlineOrderNotificationReceiptMail(orderR,notificationMethodList.get(i).getContact(),merchantName,environment);
				LOGGER.info("TestSchedular :: execute : Mail Send successfully on mail id "+notificationMethodList.get(i).getContact());
				
			}
			if(notificationMethodList.get(i).getNotificationMethodType().getId()==2)
			{
				StringBuilder googleShortURL= null;
				for (OrderR orderR2 : orderR) {
				String orderIdd = EncryptionDecryptionUtil.encryption(Integer.toString(orderR2.getId()));
				 
				String customerrId = EncryptionDecryptionUtil.encryption(Integer.toString(orderR2.getCustomer().getId()));
				 
				String orderLink = environment.getProperty("WEB_BASE_URL") + "/receipt?orderid=" + orderIdd + "&customerid=" + customerrId;
				if(googleShortURL!=null)
				googleShortURL.append(ShortCodeUtil.getTinyUrl(orderLink)).append(" , ");
				else googleShortURL = new StringBuilder(" " + ShortCodeUtil.getTinyUrl(orderLink) + " , ");
				LOGGER.info("Google short url : "+googleShortURL);
			}
                HttpPost postRequest = new HttpPost(environment.getProperty("CommunicationPlatform_BASE_URL_SERVER")+"/sendFoodKonnketSMSForOrder");
                String request="{\"mobileNumber\":\""+notificationMethodList.get(i).getContactName()+"\",\"messsage\":\"You got a new online order please view this link "+googleShortURL+"\",\"orgId\":\""+notificationMethodList.get(i).getSmsRequestId()+"\"} ";
                String response=convertToStringJson(postRequest,request,environment);
                LOGGER.info("response for SMS=== "+response);
				LOGGER.info("sms");
			}
		}
		
		
		}
		}}catch(Exception e)
		{
			LOGGER.error("error: " + e.getMessage());
			LOGGER.error("TestSchedular :: execute : Exception "+e);
		}
		LOGGER.info("-----------------TestSchedular : Inside execute :: End---------------------");
	}
	 public static String convertToStringJson(HttpPost postRequest, String customerJson,Environment environment) {
	        StringBuilder responseBuilder = new StringBuilder();
	        try {
	            HttpClient httpClient = HttpClientBuilder.create().build();
	            StringEntity input = new StringEntity(customerJson);
	            input.setContentType("application/json");
	            postRequest.setEntity(input);
	            HttpResponse response = httpClient.execute(postRequest);
	            LOGGER.info("Output from Server .... \n");
	            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
	            String line = "";
	            while ((line = rd.readLine()) != null) {
	                responseBuilder.append(line);
	            }
	        } catch (MalformedURLException e) {
	            MailSendUtil.sendExceptionByMail(e,environment);
	            LOGGER.error("error: " + e.getMessage());
	        } catch (IOException e) {
	            MailSendUtil.sendExceptionByMail(e,environment);
	            LOGGER.error("error: " + e.getMessage());
	        }
	        return responseBuilder.toString();
	    }
	

}
