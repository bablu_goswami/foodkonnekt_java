package com.foodkonnekt.contoller;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.httpclient.util.URIUtil;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.foodkonnekt.model.Address;
import com.foodkonnekt.model.Customer;
import com.foodkonnekt.model.Merchant;
import com.foodkonnekt.model.Zone;
import com.foodkonnekt.service.OrderService;
import com.foodkonnekt.service.ZoneService;
import com.foodkonnekt.model.LatLongHolder;
import com.foodkonnekt.util.IConstant;
import com.foodkonnekt.util.MailSendUtil;
import com.foodkonnekt.util.ProducerUtil;
import com.google.gson.Gson;

@Controller
public class AddressController {

	@Autowired
    private Environment environment;
	
    @Autowired
    private ZoneService zoneService;

    @Autowired
    private OrderService orderService;

    /**
     * Check zone is exist in delivery zone or not
     * 
     * @param address
     * @param request
     * @return
     * @throws Exception
     */
    
    private static final Logger LOGGER = LoggerFactory.getLogger(AddressController.class);
    
    @RequestMapping(value = "/checkDeliveryZone", method = RequestMethod.POST)
    public @ResponseBody Map<Object, Object> checkDelivery(@RequestBody final Address address,
                    HttpServletRequest request) throws Exception {
    	LOGGER.info("AddressController.checkDelivery() : START");
        Map<Object, Object> deliveryZoneMap = new HashMap<Object, Object>();
        try {
        	System.out.println("isDeliveryKoupon+isDeliveryKoupon+");
        	LOGGER.info("AddressController.checkDelivery() : isDeliveryKoupon+isDeliveryKoupon+");
            String addressResponse = ProducerUtil.validateAddress(address);
            boolean status = addressResponse.contains("<Error>");
            LOGGER.info("AddressController.checkDelivery() : status :"+status);
            if (status) {
                deliveryZoneMap.put("message", "Your address is not valid , enter valid address");
                LOGGER.info("AddressController.checkDelivery() : message : Your address is not valid , enter valid address");
            } else {
            	 LOGGER.info("AddressController.checkDelivery() : message : Your address is valid");
                String deliveryCheckStatus = null;
                String deliveryItemPosId = null;
                double deliveryFee = 0;
                int isTaxableStatus = 0;
                double minimumDeliveryAmount = 0;
                String avgDeliveryTime = "0";
                String addressId= null;
                String zoneId= null;
                
                
                HttpSession session = request.getSession();
                Merchant merchant = (Merchant) session.getAttribute("merchant");
                if (merchant != null) {
                    address.setMerchId(merchant.getId());
                    LOGGER.info("AddressController.checkDelivery() : merchant : "+merchant.getId());
                    Customer customer = (Customer) session.getAttribute("customer");
                    if(customer!=null){
                    	customer.setMerchantt(null);
                    	if(customer.getCustomerPosId()!=null){
                    		address.setCustPosId(customer.getCustomerPosId());
                    		 LOGGER.info("AddressController.checkDelivery() : customer : "+customer.getCustomerPosId());
                    	}
                    }
                    address.setCustomer(customer);
                    address.setMerchant(null);
                    Gson gson = new Gson();
                    String addressJson = gson.toJson(address);
                    String result = ProducerUtil.checkDeliveryZone(addressJson,environment);
                    
                    
                    JSONObject jObject = new JSONObject(result);
                    if (jObject.getString("response").equals(IConstant.RESPONSE_SUCCESS_MESSAGE)) {
                        deliveryCheckStatus = jObject.getString("DATA");
                        String[] deliveryResponse = deliveryCheckStatus.split("#");
                        deliveryItemPosId = deliveryResponse[0];
                        deliveryFee = Double.parseDouble(deliveryResponse[1]);
                        if (deliveryResponse[2] != null) {
                            if (!deliveryResponse[2].equals("null")) {
                                isTaxableStatus = Integer.parseInt(deliveryResponse[2]);
                                LOGGER.info("AddressController.checkDelivery() : deliveryResponse : "+deliveryResponse);
                            }
                        }
                        if (deliveryResponse[3] != null) {
                            if (!deliveryResponse[3].equals("null")) {
                                minimumDeliveryAmount = Double.parseDouble(deliveryResponse[3]);
                                LOGGER.info("AddressController.checkDelivery() : minimumDeliveryAmount : "+ Double.parseDouble(deliveryResponse[3]));
                            }
                        }

                        if (deliveryResponse[4] != null) {
                            if (!deliveryResponse[4].equals("null")) {
                                avgDeliveryTime = deliveryResponse[4];
                                LOGGER.info("AddressController.checkDelivery() : avgDeliveryTime : "+ deliveryResponse[4]);
                            }
                        }
                        
                        if (deliveryResponse[5] != null) {
                            if (!deliveryResponse[5].equals("null")) {
                            	zoneId = deliveryResponse[5];
                            	LOGGER.info("AddressController.checkDelivery() : zoneId : "+ zoneId);
                            }
                        }
                        
                        if (deliveryResponse[6] != null) {
                            if (!deliveryResponse[6].equals("null")) {
                            	addressId = deliveryResponse[6];
                            	LOGGER.info("AddressController.checkDelivery() : addressId : "+ addressId);
                            }
                        }
                    }
                    if (deliveryCheckStatus != null) {
                        deliveryZoneMap.put("message", "Your zone is in delivery zone");
                        deliveryZoneMap.put("itemPosId", deliveryItemPosId);
                        LOGGER.info("AddressController.checkDelivery() : message : Your zone is in delivery zone");
                        if(address!=null &&address.getIsDeliveryKoupon()!=null && address.getIsDeliveryKoupon()==true){
                        	System.out.println("inside true zone");
                        	 LOGGER.info("AddressController.checkDelivery() : inside true zone");
                        	deliveryFee = 0.0;
                        	minimumDeliveryAmount = 0.0;
                        deliveryZoneMap.put("itemPrice", deliveryFee);
                        deliveryZoneMap.put("minimumDeliveryAmount", minimumDeliveryAmount);
                        deliveryZoneMap.put("avgDeliveryTime", avgDeliveryTime);
                        }else{
                        	deliveryZoneMap.put("itemPrice", deliveryFee);
                            deliveryZoneMap.put("minimumDeliveryAmount", minimumDeliveryAmount);
                        }
                        deliveryZoneMap.put("avgDeliveryTime", avgDeliveryTime);
                        if (isTaxableStatus == 1) {
                            deliveryZoneMap.put("deliveryTaxStatus", IConstant.BOOLEAN_TRUE);
                            deliveryZoneMap.put("deliveryTaxPrice", orderService.findConvenienceFeeAfterTax(
                                            String.valueOf(deliveryFee), merchant.getId()));
                            deliveryZoneMap.put("deliveryTaxWithComma", orderService.findConvenienceFeeAfterMultiTax(
                                            String.valueOf(deliveryFee), merchant.getId()));
                        } else {
                            deliveryZoneMap.put("deliveryTaxStatus", IConstant.BOOLEAN_FALSE);
                            deliveryZoneMap.put("deliveryTaxPrice", IConstant.BOOLEAN_FALSE);
                        }
                        String adressResult = ProducerUtil.createUpdateCloverAddress(addressJson,environment);
                        if(adressResult!=null && !adressResult.isEmpty()){
                        	JSONObject addressObject = new JSONObject(adressResult);
                        	if(adressResult.contains("id")){
                        	System.out.println(addressObject.getString("id"));
                        	 LOGGER.info("AddressController.checkDelivery() :addressObject.getString(id) "+addressObject.getString("id"));
                        	address.setAddressPosId(addressObject.getString("id"));
                        	}
                        }
                        zoneService.saveAddress(address);
                        deliveryZoneMap.put("addressId", address.getId());
                        deliveryZoneMap.put("zoneId", zoneId);
                    } else {
                        deliveryZoneMap.put("message",
                                        "Your address is not within delivery zone. Please input a new address or select pick up option");
                        LOGGER.info("AddressController.checkDelivery() :message : Your address is not within delivery zone. Please input a new address or select pick up option");
                    }
                } else {
                    deliveryZoneMap.put("message", "timeIssue");
                    LOGGER.info("AddressController.checkDelivery() :message : timeIssue");
                }
            }
        } catch (Exception e) {
        	LOGGER.error("AddressController.checkDelivery() : ERROR"+e);
            if (e != null) {
                MailSendUtil.sendExceptionByMail(e,environment);
            }
            LOGGER.error("error: " + e.getMessage());
        }
        LOGGER.info("AddressController.checkDelivery() : END");
        return deliveryZoneMap;
    }

    @RequestMapping(value = "/checkAddressValidity", method = RequestMethod.GET)
    public @ResponseBody Map<Object, Object> checkAddress(@RequestParam(required = true) String aptNumber,
                    @RequestParam(required = true) String firstAddress, @RequestParam(required = true) String city,
                    @RequestParam(required = true) String state, @RequestParam(required = true) String zip,
                    HttpServletRequest request) throws Exception {
    	LOGGER.info("AddressController.checkAddress() : START");
    	
        Map<Object, Object> deliveryZoneMap = new HashMap<Object, Object>();
        try {
            String aptNum[] = aptNumber.split(",");
            String firstAddressArray[] = firstAddress.split(",");
            String cityArry[] = city.split(",");
            String stateArray[] = state.split(",");
            String zipArray[] = zip.split(",");
            String numb = "";
            boolean msgStatus = false;
            for (int i = 1; i < aptNum.length; i++) {
                Address address = new Address();
                address.setAddress1(aptNum[i]);
                if (firstAddressArray.length >i) {
                    address.setAddress2(firstAddressArray[i]);
                    
                } else {
                    address.setAddress2("");
                }
                if(cityArry.length>i){
                address.setCity(cityArry[i]);
                } else {
                	address.setCity("");
                }
                if(stateArray.length>i){
                address.setState(stateArray[i]);
                } else {
                	address.setState("");
                }
                if(zipArray.length>i){
                address.setZip(zipArray[i]);
                } else{
                	address.setZip("");
                }
                LOGGER.info("AddressController.checkAddress() : call : ProducerUtil.validateAddress1");
                String addressResponse = ProducerUtil.validateAddress1(address);
                boolean status = addressResponse.contains("<Error>");
                if (status) {
                	 LOGGER.info("AddressController.checkAddress() : status : "+status);
                    msgStatus = true;
                    numb = numb + "," + i;
                }
            }
            if (msgStatus) {
                deliveryZoneMap.put("statusM", 1);
                deliveryZoneMap.put("message", "Your " + numb + " address is not valid , enter valid address");
                LOGGER.info("AddressController.checkAddress() : message : Your "+ numb + " address is not valid , enter valid address");
            } else {
                deliveryZoneMap.put("statusM", 0);
            }
        } catch (Exception e) {
        	LOGGER.error("AddressController.checkAddress() : ERROR"+e);
        	
            if (e != null) {
                MailSendUtil.sendExceptionByMail(e,environment);
            }
            LOGGER.error("error: " + e.getMessage());
        }
        LOGGER.info("AddressController.checkAddress() : END");
        return deliveryZoneMap;
    }

    @RequestMapping(value = "/checkDeliveryAfterLogin", method = RequestMethod.GET)
    public @ResponseBody Map<Object, Object> checkLoginAddress(@RequestParam(required = true) Integer addressId,
    														@RequestParam(required = true) Boolean isDeliveryKoupon,
                    HttpServletRequest request) throws Exception {
    	LOGGER.info("AddressController.checkLoginAddress() : START");
        Map<Object, Object> deliveryZoneMap = new HashMap<Object, Object>();
        try {
        	System.out.println("isDeliveryKoupon+"+isDeliveryKoupon);
        	LOGGER.info("AddressController.checkLoginAddress() : isDeliveryKoupon "+isDeliveryKoupon);
            Address address = zoneService.findAddressById(addressId);
            HttpSession session = request.getSession();
            String deliveryCheckStatus = null;
            String deliveryItemPosId = null;
            double deliveryFee = 0;
            int isTaxableStatus = 0;
            double minimumDeliveryAmount = 0;
            String avgDeliveryTime = "0";
            Merchant merchant = (Merchant) session.getAttribute("merchant");
            if (merchant != null) {
            	
            	List<Zone> zones = zoneService.findZoneByMerchantId(merchant.getId());
            	if(zones!=null && !zones.isEmpty()){
            		for (Zone zone : zones) {
            			if(zone.getId()!=null){
            				deliveryZoneMap.put("zoneId", zone.getId());
            				deliveryZoneMap.put("addressId", addressId);
            			}
					}
            	}
            	
                address.setMerchId(merchant.getId());
                Gson gson = new Gson();
                String addressJson = gson.toJson(address);
                String result = ProducerUtil.checkDeliveryZone(addressJson,environment);
                JSONObject jObject = new JSONObject(result);
                if (jObject.getString("response").equals(IConstant.RESPONSE_SUCCESS_MESSAGE)) {
                    deliveryCheckStatus = jObject.getString("DATA");
                    String[] deliveryResponse = deliveryCheckStatus.split("#");
                    deliveryItemPosId = deliveryResponse[0];
                    deliveryFee = Double.parseDouble(deliveryResponse[1]);
                    if (deliveryResponse[2] != null) {
                        if (!deliveryResponse[2].equals("null")) {
                            isTaxableStatus = Integer.parseInt(deliveryResponse[2]);
                        }
                    }
                    if (deliveryResponse[3] != null) {
                        if (!deliveryResponse[3].equals("null")) {
                            minimumDeliveryAmount = Double.parseDouble(deliveryResponse[3]);
                        }
                    }
                    if (deliveryResponse[4] != null) {
                        if (!deliveryResponse[4].equals("null")) {
                            avgDeliveryTime = deliveryResponse[4];
                        }
                    }
                }
                if (deliveryCheckStatus != null) {
                    deliveryZoneMap.put("message", "Your zone is in delivery zone");
                    deliveryZoneMap.put("itemPosId", deliveryItemPosId);
                    deliveryZoneMap.put("avgDeliveryTime", avgDeliveryTime);
                    if(isDeliveryKoupon== true)
                    {
                        deliveryZoneMap.put("deliveryDiscountAmount",deliveryFee);

                    	deliveryFee = 0.0;
                    	minimumDeliveryAmount = 0.0;
                    	System.out.println("inside true"+deliveryFee+" "+minimumDeliveryAmount);
                    	deliveryZoneMap.put("itemPrice", deliveryFee);
                    	deliveryZoneMap.put("minimumDeliveryAmount",minimumDeliveryAmount);
                    	
                    }else{
                        deliveryZoneMap.put("itemPrice", deliveryFee);
                        deliveryZoneMap.put("minimumDeliveryAmount", minimumDeliveryAmount);
                    }
                    System.out.println("delivery fees after condition--"+deliveryFee+" "+minimumDeliveryAmount);
                    deliveryZoneMap.put("avgDeliveryTime", avgDeliveryTime);
                    if (isTaxableStatus == 1) {
                        deliveryZoneMap.put("deliveryTaxStatus", IConstant.BOOLEAN_TRUE);
                        deliveryZoneMap.put(
                                        "deliveryTaxPrice",
                                        orderService.findConvenienceFeeAfterTax(String.valueOf(deliveryFee),
                                                        merchant.getId()));
                        deliveryZoneMap.put("deliveryTaxWithComma", orderService.findConvenienceFeeAfterMultiTax(
                                        String.valueOf(deliveryFee), merchant.getId()));
                    } else {
                        deliveryZoneMap.put("deliveryTaxStatus", IConstant.BOOLEAN_FALSE);
                        deliveryZoneMap.put("deliveryTaxPrice", IConstant.BOOLEAN_FALSE);
                    }
                } else {
                    deliveryZoneMap.put("message",
                                    "Your address is not within delivery zone. Please input a new address or select pick up option");
                }
            }
        } catch (Exception e) {
        	LOGGER.error("AddressController.checkLoginAddress() : ERROR"+e);
        	
            if (e != null) {
                MailSendUtil.sendExceptionByMail(e,environment);
            }
            LOGGER.error("error: " + e.getMessage());
        }
        LOGGER.info("AddressController.checkLoginAddress() : END");
        return deliveryZoneMap;
    }
    
    
    
    
    
    @RequestMapping(value = "/fatchDeliveryZone", method = RequestMethod.GET)
  	public  @ResponseBody Map<Object, Object> fatchDeliveryZone(@RequestParam String address, ModelMap model,HttpServletRequest request) {
 	LOGGER.info("----------------Start :: AddressController : fatchDeliveryZone------------------------");
  		Boolean status = false;
  		Double lat = null;
  		Double lng = null;
  		Map<Object, Object> deliveryZoneMap = new HashMap<Object, Object>();
  		
  		try {

  			URL url = new URL("https://maps.googleapis.com/maps/api/geocode/json?address="
  					+ URIUtil.encodeQuery(address) + "&sensor=true&key="+IConstant.GOOGLE_API_KEY);
//            + URIUtil.encodeQuery(address) + "&sensor=true&key=AIzaSyDjnluKhome9lmt5LLKnIgqot7HtyDetes");

  			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
  			conn.setRequestMethod("GET");
  			conn.setRequestProperty("Accept", "application/json");
  			if (conn.getResponseCode() != 200) {
  				throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
  			}
  			BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
  			String output = "", full = "";
  			while ((output = br.readLine()) != null) {
  				full += output;
  			}
  			if (full != "") {
  				JSONObject jsonObject = new JSONObject(full);
  				if (jsonObject != null && jsonObject.toString().contains("results"))
  				{
  					JSONArray result = (JSONArray) jsonObject.getJSONArray("results");
  					
  					//System.out.println("result.length()---" + result.length());
  					LOGGER.info("AddressController :: fatchDeliveryZone : result.length() "+result.length());
  					if (result.length() > 0 && result.toString().contains("geometry")) {
  						for (int i = 0; i < result.length(); i++) {
  							JSONObject viewPort = (JSONObject) result.getJSONObject(i).get("geometry");
  							if (viewPort != null && viewPort.toString().contains("location")) {
  								JSONObject location = (JSONObject) viewPort.get("location");
  								if (location != null && location.toString().contains("lng")
  										&& location.toString().contains("lat")) {
  									lng = (Double) location.get("lng");
  									lat = (Double) location.get("lat");
  									model.addAttribute("customerLat", lat);
  									model.addAttribute("customerLng", lng);
  									deliveryZoneMap.put("customerLat", lat);
  									deliveryZoneMap.put("customerLng", lng);
  									HttpSession session = request.getSession();
  					                Merchant merchant = (Merchant) session.getAttribute("merchant");
  					               /* LatLongHolder latLongHolder = new LatLongHolder();
  					                latLongHolder.setLat(lat);
  					                latLongHolder.setLng(lng);
  					                latLongList.add(latLongHolder);*/
  					                if (merchant != null) 
  					                {
  					                	List<Zone> savedZone=zoneService.findZoneByMerchantId(merchant.getId());
  					                	int count=0;
  					                   for(Zone zone: savedZone) 
  					                   {
  					                	   
  					                	 List<LatLongHolder> latLongList=new ArrayList<LatLongHolder>();
  					                	   
  					                	   if(zone!=null && zone.getPolygons()!=null && !zone.getPolygons().isEmpty() &&zone.getPolygons()!="" )
  					                	   {
  					                		   String polygones=zone.getPolygons();
  					                		   
  					                		   //System.out.println(polygones);
  					                		   LOGGER.info("AddressController :: fatchDeliveryZone : polygones "+polygones);
  					                    		String pN = polygones.replaceAll("\\(", "").replaceAll("\\)", "");
  					                    		String spiltedPolygon[] =pN.split(",");
  					                    		String poly[] = new String[spiltedPolygon.length/2];
  					                    		int j=0;
  					                    		for(int k=0;k<spiltedPolygon.length;k++)
  					                    		{
  					                    			LatLongHolder holder=new LatLongHolder();
  					                    			if(spiltedPolygon[k]!=null) {
  					                    			holder.setLat(Double.parseDouble(spiltedPolygon[k]));
  					                    			holder.setLng(Double.parseDouble(spiltedPolygon[k+1]));
  					                    			}
  					                    			poly[j]=polygones;
  					                    			k=k+1;
  					                    			j++;
  					                    			latLongList.add(holder);
  					                    		}
  					                	   }
  					                	 count++;
  					                	 deliveryZoneMap.put("latLongList"+count, latLongList);
  					                	 deliveryZoneMap.put("deliveryFee"+count, zone.getDeliveryFee());
  					                   }
  					                
  					           	 	LOGGER.info("----------------End :: AddressController : fatchDeliveryZone------------------------");

  					                   return deliveryZoneMap;
  					                }
  									
  									status = true;
  								}else {
  									status =false;
  								}
  							}

  						}
  					}

  				}

  			} else {
  				deliveryZoneMap.put("customerLat", "");
  				deliveryZoneMap.put("customerLng", "");
  			}
  		} catch (Exception e) {
  			 LOGGER.error("AddressController :: fatchDeliveryZone : Exception "+e);
  			LOGGER.error("error: " + e.getMessage());
  		}
  		LOGGER.info(" === status : "+status);
  	 	LOGGER.info("----------------End :: AddressController : fatchDeliveryZone------------------------");
  		return deliveryZoneMap;

  	}
    
    
  
	
	 @RequestMapping(value = "/checkDeliveryZoneForGoogleMap", method = RequestMethod.POST)
	    public @ResponseBody Map<Object, Object> checkDeliveryForMap(@RequestParam Double deliveryFee, @RequestParam String plgon, @RequestBody final Address address,ModelMap model,
	                    HttpServletRequest request) throws Exception {
	     	LOGGER.info("----------------Start :: AddressController : checkDeliveryZoneForGoogleMap------------------------");
	        Map<Object, Object> deliveryZoneMap = new HashMap<Object, Object>();
	        try {
	        	System.out.println("isDeliveryKoupon+isDeliveryKoupon+");
	        	
	        	LOGGER.info("AddressController :: checkDeliverZoneForGoogleMap : isDeliveryKoupon+isDeliveryKoupon ");
	      //  	deliveryZoneMap.put("message", "Your address is not valid , enter valid address");
	         //  String addressResponse = ProducerUtil.validateAddress(address);
	          /*  boolean status = addressResponse.contains("<Error>");
	            if (!status) {
	                deliveryZoneMap.put("message", "Your address is not valid , enter valid address");
	            } else {*/
	                String deliveryCheckStatus = null;
	                String deliveryItemPosId = null;
	               // double deliveryFee = 0;
	                int isTaxableStatus = 0;
	                double minimumDeliveryAmount = 0;
	                String avgDeliveryTime = "0";
	                HttpSession session = request.getSession();
	                Merchant merchant = (Merchant) session.getAttribute("merchant");
	                if (merchant != null) {
	                    address.setMerchId(merchant.getId());
	                    Customer customer = (Customer) session.getAttribute("customer");
	                    if(customer!=null){
	                    	if(customer.getCustomerPosId()!=null){
	                    		address.setCustPosId(customer.getCustomerPosId());
	                    	}
	                    }
	                    address.setCustomer(customer);
	                    Gson gson = new Gson();
	                    String addressJson = gson.toJson(address);
	                    Zone zones = null;
	                    String list [];   

	                    List<Zone> zoneList = zoneService.findZoneByDeliveryFeesAndMerchantId(deliveryFee, merchant.getId());
	                    for(Zone zone:zoneList)
	                    { 
	                   /* 	plgon=plgon.replace("[", "").replace("]", "").replace("{", "(").replace("}", ")").replace("\"", "").replace("lat:", "").replace("lng:", " ");
	                    	if(zone.getPolygons()!=null  && zone.getPolygons().contains(plgon))
	                    	{
	                    		zones = zone;
	                    	}*/
	                        
	                        plgon=plgon.replace("{", "(").replace("}", ")").replace("\"", "").replace("lat:", "").replace("lng:", " ");
                            plgon =plgon.replace("[[", "[").replace("]]", "]");
                            list =  plgon.split("],");
                            System.out.println(list.length);
                            if(list.length>0) {
                            for (int i = 0; i < list.length; i++) {
                                LOGGER.info(list[0].replace("[", "").replace("]", ""));
                                if(zone.getPolygons()!=null  && (list[i].replace("[", "").replace("]", "").contains(zone.getPolygons())))
                                {
                                    zones = zone;
                                }
                            }
                            }
	                    }
	                   /* String result = ProducerUtil.checkDeliveryZone(addressJson);
	                    
	                    
	                    JSONObject jObject = new JSONObject(result);
	                    if (jObject.getString("response").equals(IConstant.RESPONSE_SUCCESS_MESSAGE)) {
	                        deliveryCheckStatus = jObject.getString("DATA");
	                        String[] deliveryResponse = deliveryCheckStatus.split("#");
	                        deliveryItemPosId = deliveryResponse[0];
	                        deliveryFee = Double.parseDouble(deliveryResponse[1]);
	                        if (deliveryResponse[2] != null) {
	                            if (!deliveryResponse[2].equals("null")) {
	                                isTaxableStatus = Integer.parseInt(deliveryResponse[2]);
	                            }
	                        }
	                        if (deliveryResponse[3] != null) {
	                            if (!deliveryResponse[3].equals("null")) {
	                                minimumDeliveryAmount = Double.parseDouble(deliveryResponse[3]);
	                            }
	                        }

	                        if (deliveryResponse[4] != null) {
	                            if (!deliveryResponse[4].equals("null")) {
	                                avgDeliveryTime = deliveryResponse[4];
	                            }
	                        }
	                    }*/
	                    if(zones!=null) {
	                    	if(zones.getDeliveryLineItemPosId()!=null) {
	                    	 deliveryItemPosId =  zones.getDeliveryLineItemPosId();
	                    	}
	                    	if(zones.getDeliveryFee()!=null) {
	 	                    deliveryFee = zones.getDeliveryFee();
	                    	}
	                    	if(zones.getIsDeliveryZoneTaxable()!=null) {
	 	                    isTaxableStatus = zones.getIsDeliveryZoneTaxable();
	                    	}
	                    	if(zones.getMinDollarAmount()!=null) {
	 	                    minimumDeliveryAmount = Double.parseDouble(zones.getMinDollarAmount());
	                    	}
	                    	if(zones.getAvgDeliveryTime()!=null) {
	 	                    avgDeliveryTime = zones.getAvgDeliveryTime();
	                    	}
	                    	if(zones.getId()!=null){
	                    		LOGGER.info(" ===Zone  "+zones.getId());
	                    		deliveryZoneMap.put("zoneId", zones.getId());
	                    	}
	                    	


	                        deliveryZoneMap.put("message", "Your zone is in delivery zone");
	                        deliveryZoneMap.put("itemPosId", deliveryItemPosId);
	                        if(address!=null &&address.getIsDeliveryKoupon()!=null && address.getIsDeliveryKoupon()==true){
	                        	System.out.println("inside true zone");
	                        	deliveryFee = 0.0;
	                        	minimumDeliveryAmount = 0.0;
	                        	deliveryZoneMap.put("itemPrice", deliveryFee);
	                        deliveryZoneMap.put("minimumDeliveryAmount", minimumDeliveryAmount);
	                        }else{
	                        	deliveryZoneMap.put("itemPrice", deliveryFee);
	                            deliveryZoneMap.put("minimumDeliveryAmount", minimumDeliveryAmount);
	                        }
	                        deliveryZoneMap.put("avgDeliveryTime", avgDeliveryTime);
	                        if (isTaxableStatus == 1) {
	                            deliveryZoneMap.put("deliveryTaxStatus", IConstant.BOOLEAN_TRUE);
	                            deliveryZoneMap.put("deliveryTaxPrice", orderService.findConvenienceFeeAfterTax(
	                                            String.valueOf(deliveryFee), merchant.getId()));
	                            deliveryZoneMap.put("deliveryTaxWithComma", orderService.findConvenienceFeeAfterMultiTax(
	                                            String.valueOf(deliveryFee), merchant.getId()));
	                        } else {
	                            deliveryZoneMap.put("deliveryTaxStatus", IConstant.BOOLEAN_FALSE);
	                            deliveryZoneMap.put("deliveryTaxPrice", IConstant.BOOLEAN_FALSE);
	                        }
	                        String adressResult = ProducerUtil.createUpdateCloverAddress(addressJson,environment);
	                        LOGGER.info(" ===adressResult : "+adressResult);
	                        if(adressResult!=null && !adressResult.isEmpty()){
	                        	JSONObject addressObject = new JSONObject(adressResult);
	                        	if(adressResult.contains("id")){
	                        	System.out.println(addressObject.getString("id"));
	                        	address.setAddressPosId(addressObject.getString("id"));
	                        	}
	                        }
	                        zoneService.saveAddress(address);
	                        deliveryZoneMap.put("addressId", address.getId());
	                        
	                    }
	                   else {
	                        deliveryZoneMap.put("message",
	                                        "Your address is not within delivery zone. Please input a new address or select pick up option");
	                    }
	                } else {
	                    deliveryZoneMap.put("message", "timeIssue");
	                }
	           // }
	        } catch (Exception e) {
	        	LOGGER.error("AddressController :: checkDeliveryZoneForGoogleMapl : Exception "+e);
	            if (e != null) {
	                MailSendUtil.sendExceptionByMail(e,environment);
	            }
	            LOGGER.error("error: " + e.getMessage());
	        }
	        LOGGER.info("----------------End :: AddressController : checkDeliveryZoneForGoogleMap------------------------");
	        return deliveryZoneMap;
	    }
	
	 
	 @RequestMapping(value = "/findDeliveryZoneType", method = RequestMethod.GET)
	  	public  @ResponseBody Map<Object, Object> findDeliveryZoneType(@RequestParam Integer merchantId, ModelMap model,HttpServletRequest request) {
		 LOGGER.info("----------------Start :: AddressController : findDeliveryZoneType------------------------");
		 Map<Object, Object> deliveryZoneType = new HashMap<Object, Object>();	
		 try{
		 if(merchantId!=null) {
			 List<Zone> savedZone=zoneService.findZoneByMerchantId(merchantId);
			 LOGGER.info("AddressController :: findDeliveryZoneType : merchantId "+merchantId);
			 if(savedZone!=null && !savedZone.isEmpty() && savedZone.size()>0) {
				if( savedZone.get(0).getPolygons()!=null &&savedZone.get(0).getPolygons()!="" && !savedZone.get(0).getPolygons().isEmpty()) {
					deliveryZoneType.put("deliveryZoneType", "googleMap");
				}else {
					deliveryZoneType.put("deliveryZoneType", "radius");
				}
			 }
			}
		 }catch(Exception e){
			 LOGGER.error("AddressController :: findDeliveryZoneType : Exception "+e);
		 }
		 LOGGER.info("----------------End :: AddressController : findDeliveryZoneType------------------------");
	  		return deliveryZoneType;
	 }
	 
	 
}
