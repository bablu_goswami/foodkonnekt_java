package com.foodkonnekt.contoller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

//import com.foodkonnekt.clover.vo.SignedRequest;
import com.foodkonnekt.model.Address;
import com.foodkonnekt.model.CardInfo;
import com.foodkonnekt.model.Customer;
import com.foodkonnekt.model.CustomerCode;
import com.foodkonnekt.model.Merchant;
import com.foodkonnekt.model.OrderR;
import com.foodkonnekt.model.Vendor;
import com.foodkonnekt.repository.CustomerCodeRepository;
import com.foodkonnekt.repository.CustomerrRepository;
import com.foodkonnekt.service.CustomerService;
import com.foodkonnekt.service.MerchantService;
import com.foodkonnekt.service.OrderService;
import com.foodkonnekt.util.DateUtil;
import com.foodkonnekt.util.EncryptionDecryptionUtil;
import com.foodkonnekt.util.IConstant;
import com.foodkonnekt.util.MailSendUtil;
import com.foodkonnekt.util.ProducerUtil;
import com.foodkonnekt.util.UrlConstant;
import com.google.gson.Gson;

@Controller
public class HomeController {

	@Autowired
    private Environment environment;
	
	@Autowired
	private CustomerCodeRepository customerCodeRepository;
	
	@Autowired
	private MerchantService merchantService;

	private static int EXPIRY_TIME = 30 * 60 * 1000;

	@Autowired
	private CustomerService customerService;

	@Autowired
	private CustomerrRepository customerrRepository;

	@Autowired
	private OrderService orderService;

	private static final Logger LOGGER = LoggerFactory.getLogger(HomeController.class);

	@RequestMapping(value = "/cayanpayment", method = RequestMethod.POST)
	public String homePost(final @RequestParam("TokenHolder") String token, ModelMap model) {
		LOGGER.info("HomeController.homePost() : START");

		String merchantName = "TEST";
		String merchantSiteId = "XXXXXXXX";
		String merchantKey = "XXXXX-XXXXX-XXXXX-XXXXX-XXXXX";
		String invoiceNumber = "99123";
		String amount = "1.65";
		String vaultToken = token;
		String forceDuplicate = "true";
		String registerNumber = "123";
		String merchantTransactionId = "1234";

		/*
		 * Credit c = new Credit(); CreditSoap cs = c.getCreditSoap(); CreditResponse4
		 * svr = cs.saleVault(merchantName, merchantSiteId, merchantKey, invoiceNumber,
		 * amount, vaultToken, forceDuplicate, registerNumber, merchantTransactionId);
		 * 
		 * model.addAttribute("amount", svr.getAmount());
		 * model.addAttribute("referenceNumber", svr.getToken());
		 */

		LOGGER.info("HomeController.homePost() : END");
		return "home";
	}

	@RequestMapping(value = "/PaymentForm", method = RequestMethod.GET)
	public String homePost(ModelMap model) {
		LOGGER.info("HomeController.homePost() : START");

		return "PaymentForm";
	}

	@RequestMapping(value = { "{merchantName}/foodtronixV2/{merchantId}", "{merchantName}/cloverV2/{merchantId}",
			"{merchantName}/non-posV2/{merchantId}",
			"{merchantName}/focusV2/{merchantId}" }, method = RequestMethod.GET)
	public String baseUrlV2(ModelMap model, Map<String, Object> map, HttpServletRequest request,
			@PathVariable("merchantName") String merchantName, @PathVariable("merchantId") String merchantId)
			throws IOException {
		LOGGER.info("HomeController.baseUrl() : START");

		// model.addAttribute("merchantId", merchantId);
		Integer intMerchantId = 0;
		try {

			if (merchantId != null) {
				LOGGER.info("HomeController.baseUrl() : merchantId : "+merchantId +" merchantName : "+merchantName);
				HttpSession session = request.getSession();
                                Merchant sessionMerchant = (Merchant) session.getAttribute("merchant");
				if(sessionMerchant != null)
				LOGGER.info("HomeController.baseUrl() : sessionMerchantId : "+sessionMerchant.getId() +" sessionMerchantName : "+sessionMerchant.getName());
				
				if (merchantId.matches("[0-9]+")) {
					intMerchantId = Integer.parseInt(merchantId);
				} else {
					if(EncryptionDecryptionUtil.checkForEncode(merchantId)) {
					if (EncryptionDecryptionUtil.decryption(merchantId).matches("[0-9]+"))
						intMerchantId = Integer.parseInt(EncryptionDecryptionUtil.decryption(merchantId));
				}
				}

				Merchant merchant = merchantService.findById(intMerchantId);
				if (merchant == null) {
					// return "redirect:" + environment.getProperty("WEB_BASE_URL") + "/sessionTimeOut";
					return "redirect:https://www.foodkonnekt.com";
				}
				
				if (sessionMerchant != null && (int) sessionMerchant.getId() != (int) intMerchantId) {
					session.removeAttribute("merchant");
					session.removeAttribute("customer");
					session.removeAttribute("guestCustomer");
					session.removeAttribute("orderItems");
					session.removeAttribute("Tax");
					session.removeAttribute("totalPrice");
					session.removeAttribute("subTotal");
					session.removeAttribute("ConvenienceFee");
					session.removeAttribute("convenienceFeeItem");
				}

				session.removeAttribute("vendor");
				session.removeAttribute("merchantList");

				// session.setMaxInactiveInterval(2 * 60);
				Customer customer = (Customer) session.getAttribute("customer");
				if (customer != null) {
					if (customer.getMerchantt() != null) {
						System.out.println(merchant.getId().intValue() != customer.getMerchantt().getId().intValue());
						if (merchant.getId().intValue() != customer.getMerchantt().getId().intValue()) {
							session.removeAttribute("customer");
						} else {
							// session.removeAttribute("customer");
							System.out.println("customer logged in");
						} 
					}
				}
				if (merchant != null && merchant.getIsInstall() != null
						&& merchant.getIsInstall() != IConstant.SOFT_DELETE) {
					session.setAttribute("merchant", merchant);
					session.setMaxInactiveInterval(365 * 24 * 60 * 60);
					model.addAttribute("merchantId", merchant.getId());
					map.put("Address", new Address());
				} else {
					return "redirect:https://www.foodkonnekt.com";
				}
			} else {
				return "redirect:https://www.foodkonnekt.com";
			}
		} catch (Exception e) {
			LOGGER.info("HomeController.baseUrl() : ERROR" + e);

			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
			}
			LOGGER.error("error: " + e.getMessage());
		}
		LOGGER.info("HomeController.baseUrl() : END");

		/*
		 * if(intMerchantId.equals(IConstant.TEXAN_REWARDS_MERCHANT_ID)){ return
		 * "redirect:" + environment.getProperty("TEXAN_BASE_URL") + "/orderV2"; }else{
		 */
		return "redirect:" + environment.getProperty("WEB_BASE_URL") + "/orderV2";
		// }
		// return "redirect:" + environment.getProperty("WEB_BASE_URL") + "/orderV2";

		// return "redirect:" + environment.getProperty("WEB_BASE_URL") + "/home";
	}

	@RequestMapping(value = { "{merchantName}/clover/{merchantId}",
			"{merchantName}/foodtronix/{merchantId}" }, method = RequestMethod.GET)
	public String baseUrl(ModelMap model, Map<String, Object> map, HttpServletRequest request,
			@PathVariable("merchantName") String merchantName, @PathVariable("merchantId") String merchantId)
			throws IOException {
		LOGGER.info("HomeController.baseUrl() : START");

		// model.addAttribute("merchantId", merchantId);
		Integer intMerchantId = 0;
		try {

			if (merchantId != null) {
				LOGGER.info("HomeController.baseUrl() merchantId : "+merchantId);
				if (merchantId.matches("[0-9]+")) {
					intMerchantId = Integer.parseInt(merchantId);
				} else {
					if (EncryptionDecryptionUtil.decryption(merchantId).matches("[0-9]+"))
						intMerchantId = Integer.parseInt(EncryptionDecryptionUtil.decryption(merchantId));
				}

				Merchant merchant = merchantService.findById(intMerchantId);
				if (merchant == null) {
					// return "redirect:" + environment.getProperty("WEB_BASE_URL") + "/sessionTimeOut";
					return "redirect:https://www.foodkonnekt.com";
				}
				HttpSession session = request.getSession();

				session.removeAttribute("vendor");
				session.removeAttribute("merchantList");

				// session.setMaxInactiveInterval(2 * 60);
				Customer customer = (Customer) session.getAttribute("customer");
				if (customer != null) {
					if (customer.getMerchantt() != null) {
						System.out.println(merchant.getId().intValue() != customer.getMerchantt().getId().intValue());
						if (merchant.getId().intValue() != customer.getMerchantt().getId().intValue()) {
							session.removeAttribute("customer");
						} else {
							// session.removeAttribute("customer");
							LOGGER.info("customer logged in");
						}
					}
				}
				if (merchant != null && merchant.getIsInstall() != null
						&& merchant.getIsInstall() != IConstant.SOFT_DELETE) {
					session.setAttribute("merchant", merchant);
					session.setMaxInactiveInterval(365 * 24 * 60 * 60);
					model.addAttribute("merchantId", merchant.getId());
					map.put("Address", new Address());
				} else {
					LOGGER.info("HomeController.baseUrl() : END");

					return "redirect:https://www.foodkonnekt.com";
				}
			} else {
				LOGGER.info("HomeController.baseUrl() : END");

				return "redirect:https://www.foodkonnekt.com";
			}
		} catch (Exception e) {
			LOGGER.info("HomeController.baseUrl() : ERROR" + e);

			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
			}
			LOGGER.error("error: " + e.getMessage());
		}
		LOGGER.info("HomeController.baseUrl() : END");
		/*
		 * if(intMerchantId.equals(IConstant.TEXAN_REWARDS_MERCHANT_ID)){ return
		 * "redirect:" + environment.getProperty("TEXAN_BASE_URL") + "/order"; }else{
		 */
		return "redirect:" + environment.getProperty("WEB_BASE_URL") + "/order";
		// }

		// return "redirect:" + environment.getProperty("WEB_BASE_URL") + "/order";
		// return "redirect:" + environment.getProperty("WEB_BASE_URL") + "/home";
	}

	@RequestMapping(value = "facebookPageTab", method = RequestMethod.POST, consumes = "application/x-www-form-urlencoded")
	public String baseUrlPost(ModelMap model, Map<String, Object> map, HttpServletRequest request,
			@RequestBody(required = false) String signed_request) throws IOException {
		// model.addAttribute("merchantId", merchantId);
		LOGGER.info("HomeController.baseUrlPost() : START");
		LOGGER.info(" ===signed_request : " + signed_request);
		try {
			/*
			 * Gson gson = new Gson(); String json = gson.toJson(signed_request) ;
			 */
			// MailSendUtil.webhookMail("signed_request", "signed_request-->
			// "+signed_request);
			signed_request = signed_request.replace(".", ",");
			String payload = "";
			String pageId = null;
			if (signed_request != null) {
				String[] fbData = signed_request.split(",");
				if (fbData != null && fbData.length > 1) {
					payload = fbData[1];
					payload = EncryptionDecryptionUtil.decryption(payload);
					LOGGER.info(" === payload : " + payload);
					JSONObject jObject = new JSONObject(payload);
					if (jObject.has("page")) {
						JSONObject pageData = jObject.getJSONObject("page");
						if (pageData.has("id")) {
							pageId = pageData.getString("id");
						}
					}

				}
			}
			// MailSendUtil.webhookMail("facebook response on app publish", "payload-->
			// "+payload +" pageId--> "+pageId);

			// Merchant merchant = merchantService.findById(intMerchantId);
			Merchant merchant = null;
			if (pageId != null) {
				List<Merchant> merchants = merchantService.findByFBTabIds(pageId);
				if (merchants != null && !merchants.isEmpty() && merchants.size() > 0) {
					merchant = merchants.get(0);
				}

			}

			if (merchant == null) {
				// return "redirect:" + environment.getProperty("WEB_BASE_URL") + "/sessionTimeOut";
				return "redirect:https://www.foodkonnekt.com";
			}
			HttpSession session = request.getSession();

			session.removeAttribute("vendor");
			session.removeAttribute("merchantList");

			// session.setMaxInactiveInterval(2 * 60);
			Customer customer = (Customer) session.getAttribute("customer");
			if (customer != null) {
				if (customer.getMerchantt() != null) {
					System.out.println(merchant.getId().intValue() != customer.getMerchantt().getId().intValue());
					if (merchant.getId().intValue() != customer.getMerchantt().getId().intValue()) {
						session.removeAttribute("customer");
					} else {
						LOGGER.info("HomeController.baseUrlPost() : END");

						// session.removeAttribute("customer");
						LOGGER.info("customer logged in");
					}
				}
			}
			if (merchant != null && merchant.getIsInstall() != null
					&& merchant.getIsInstall() != IConstant.SOFT_DELETE) {
				session.setAttribute("merchant", merchant);
				session.setMaxInactiveInterval(365 * 24 * 60 * 60);
				model.addAttribute("merchantId", merchant.getId());
				map.put("Address", new Address());
			} else {
				LOGGER.info("HomeController.baseUrlPost() : END");

				return "redirect:https://www.foodkonnekt.com";
			}

		} catch (Exception e) {
			LOGGER.info("HomeController.baseUrlPost() : ERROR" + e);

			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
			}
			LOGGER.error("error: " + e.getMessage());
		}
		LOGGER.info("HomeController.baseUrlPost() : END");
		return "redirect:" + environment.getProperty("WEB_BASE_URL") + "/order";
		// return "redirect:" + environment.getProperty("WEB_BASE_URL") + "/home";
	}

	/**
	 * Show home page
	 * 
	 * @param merchantId
	 * @param model
	 * @return
	 * @throws IOException
	 */
	/*
	 * @RequestMapping(value = "home", method = RequestMethod.GET) public String
	 * showReiProfilePage(ModelMap model, Map<String, Object> map,
	 * HttpServletRequest request, HttpServletResponse
	 * response, @RequestParam(required = false) String merchantId) throws
	 * IOException { try { Integer intMerchantId = 0; if(merchantId!=null){ if
	 * (merchantId.matches("[0-9]+")) { intMerchantId =
	 * Integer.parseInt(merchantId); } else {
	 * if(EncryptionDecryptionUtil.decryption(merchantId).matches("[0-9]+"))
	 * intMerchantId =
	 * Integer.parseInt(EncryptionDecryptionUtil.decryption(merchantId)); }
	 * 
	 * Merchant merchant = merchantService.findById(intMerchantId); if (merchant ==
	 * null) { return "redirect:" + environment.getProperty("WEB_BASE_URL") + "/sessionTimeOut"; }
	 * HttpSession session = request.getSession();
	 * 
	 * session.removeAttribute("vendor"); session.removeAttribute("merchantList");
	 * 
	 * // session.setMaxInactiveInterval(2 * 60); Customer customer = (Customer)
	 * session.getAttribute("customer"); if (customer != null) { if
	 * (customer.getMerchantt() != null) {
	 * System.out.println(merchant.getId().intValue() !=
	 * customer.getMerchantt().getId().intValue()); if (merchant.getId().intValue()
	 * != customer.getMerchantt().getId().intValue()) {
	 * session.removeAttribute("customer"); }else{
	 * //session.removeAttribute("customer");
	 * System.out.println("customer logged in"); } } } if(merchant!=null &&
	 * merchant.getIsInstall()!=null &&
	 * merchant.getIsInstall()!=IConstant.SOFT_DELETE){
	 * session.setAttribute("merchant", merchant); session.setMaxInactiveInterval(24
	 * * 60 * 60); model.addAttribute("vendorId", merchant.getId());
	 * map.put("Address", new Address()); }else{ return
	 * "redirect:https://www.foodkonnekt.com"; } }else{ return
	 * "redirect:https://www.foodkonnekt.com"; } } catch (Exception e) { if (e !=
	 * null) { MailSendUtil.sendExceptionByMail(e,environment); } LOGGER.error("error: " + e.getMessage()); } return
	 * "redirect:" + environment.getProperty("WEB_BASE_URL") + "/order"; }
	 */

//    multilocation changes start

	@RequestMapping(value = "sendLocation", method = RequestMethod.GET)
	@ResponseBody
	public String sendLocation(@RequestParam("merchantId") String merchantId,
			@RequestParam("merchantName") String merchantName, ModelMap model, HttpServletRequest request) {
		LOGGER.info("HomeController.sendLocation() : START");
		LOGGER.info(" === merchantId : " + merchantId);
		HttpSession session = request.getSession();
		Vendor vendor = (Vendor) session.getAttribute("vendor");
		String vendorId = EncryptionDecryptionUtil.encryption(Integer.toString(vendor.getId()));
		String merchId = EncryptionDecryptionUtil.encryption(merchantId);

		LOGGER.info("HomeController.sendLocation() : END");
		return environment.getProperty("WEB_BASE_URL") + "/vendor/" + vendorId + "/" + merchantName + "/clover/" + merchId;
	}

	@RequestMapping(value = "sendLocationForProfile", method = RequestMethod.GET)
	@ResponseBody
	public String sendLocationForProfile(@RequestParam("merchantId") String merchantId,
			@RequestParam("merchantName") String merchantName, ModelMap model, HttpServletResponse response,
			HttpServletRequest request) {
		LOGGER.info("HomeController.sendLocation() : START");
		LOGGER.info(" === merchantId : " + merchantId);
		LOGGER.debug(" === merchantId : " + merchantId);

		HttpSession session = request.getSession();
		Vendor vendor = (Vendor) session.getAttribute("vendor");
		String vendoreId = EncryptionDecryptionUtil.encryption(Integer.toString(vendor.getId()));
		String merchId = EncryptionDecryptionUtil.encryption(merchantId);

		try {
			Integer intMerchantId = 0;
			Integer intVendorId = 0;
			if (merchantId != null) {
				if (merchantId.matches("[0-9]+")) {
					intMerchantId = Integer.parseInt(merchantId);
				} else {
					if (EncryptionDecryptionUtil.decryption(merchantId).matches("[0-9]+"))
						intMerchantId = Integer.parseInt(EncryptionDecryptionUtil.decryption(merchantId));
				}

				List<Merchant> merchantList = new ArrayList<Merchant>();

				if (vendoreId != null) {

					if (vendoreId.matches("[0-9]+")) {
						intVendorId = Integer.parseInt(vendoreId);
					} else {
						if (EncryptionDecryptionUtil.decryption(vendoreId).matches("[0-9]+"))
							intVendorId = Integer.parseInt(EncryptionDecryptionUtil.decryption(vendoreId));
					}
					vendor = merchantService.findVendorById(intVendorId);
					merchantList = merchantService.findAllMerchantsByVendorId(intVendorId);

					if (vendor != null) {
						session.setAttribute("vendor", vendor);
					}

					session.setAttribute("merchantList", merchantList);
				}

				Merchant merchant = merchantService.findById(intMerchantId);
				if (merchant == null) {
					LOGGER.info("HomeController.sendLocation() : END");

					// return "redirect:" + environment.getProperty("WEB_BASE_URL") + "/sessionTimeOut";
					return "redirect:https://www.foodkonnekt.com";
				}

				// session.setMaxInactiveInterval(2 * 60);
				Customer customer = (Customer) session.getAttribute("customer");
				if (customer != null) {
					if (customer.getMerchantt() != null) {
						Merchant customerMerchant = merchantService.findById(customer.getMerchantt().getId());
						System.out.println(merchant.getOwner().getId().intValue() != customerMerchant.getOwner().getId()
								.intValue());
						if (merchant.getOwner().getId().intValue() != customerMerchant.getOwner().getId().intValue()) {
							session.removeAttribute("customer");
						} else {
							if (customerMerchant.getId() == merchant.getId()) {
								customer.setMerchantt(merchant);
								session.setAttribute("customer", customer);
								LOGGER.info("Customer with id = " + customer.getId() + " logged in");
							} else {
								customer.setVendor(null);
								customer.setVendorId(merchant.getId());

								LOGGER.info("Customer id = " + customer.getId());
								List<Customer> customers = customerrRepository.findByEmailIdAndPasswordAndMerchanttId(
										customer.getEmailId(), customer.getPassword(), merchant.getId());
								if (customers != null && !customers.isEmpty()) {
									/*
									 * JSONObject Customer = jObject.getJSONObject("DATA"); Customer customerResult
									 * = ProducerUtil.getCustomer(Customer);
									 */

									session.setAttribute("customer", customers.get(0));

								} else {
									session.setAttribute("merchant", merchant);

									saveDuplicateCustomer(request, response);
								}

							}
						}
					}
				}

				model.addAttribute("merchantList", merchantList);

				if (merchant != null && merchant.getIsInstall() != null
						&& merchant.getIsInstall() != IConstant.SOFT_DELETE) {
					session.setAttribute("merchant", merchant);
					session.setMaxInactiveInterval(24 * 60 * 60);
					if (vendor != null && vendor.getId() != null && vendor.getId() > 0)
						model.addAttribute("vendorId", vendor.getId());

					model.addAttribute("merchantId", merchant.getId());
				} else {
					LOGGER.info("HomeController.sendLocation() : END");

					return "redirect:https://www.foodkonnekt.com";
				}
			} else {
				LOGGER.info("HomeController.sendLocation() : END");

				return "redirect:https://www.foodkonnekt.com";
			}
		} catch (Exception e) {
			LOGGER.info("HomeController.showReiProfilePageForMultiLocation() : ERROR" + e);

			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
			}
			LOGGER.error("error: " + e.getMessage());
		}
		LOGGER.info("HomeController.showReiProfilePageForMultiLocation() : END");

		return environment.getProperty("WEB_BASE_URL") + "/profile";
	}

	@RequestMapping(value = { "vendor/{vendoreId}/{merchantName}/clover/{merchantId}",
			"{merchantName}/foodtronix/{merchantId}" }, method = RequestMethod.GET)
	public String baseUrlForMultiLocation(ModelMap model, Map<String, Object> map,
			@PathVariable("vendoreId") String vendoreId, @PathVariable("merchantName") String merchantName,
			@PathVariable("merchantId") String merchantId) throws IOException {
		LOGGER.info("HomeController.baseUrlForMultiLocation() : START");
		LOGGER.info(" === merchantId : " + merchantId + " vendoreId : " + vendoreId);
		model.addAttribute("merchantId", merchantId);
		model.addAttribute("vendoreId", vendoreId);

		LOGGER.info("HomeController.baseUrlForMultiLocation() : END");
		return "redirect:" + environment.getProperty("WEB_BASE_URL") + "/homeForMultiLocation";
	}

	/**
	 * Show home page
	 * 
	 * @param merchantId
	 * @param model
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value = "homeForMultiLocation", method = RequestMethod.GET)
	public String showReiProfilePageForMultiLocation(ModelMap model, Map<String, Object> map,
			HttpServletRequest request, HttpServletResponse response, @RequestParam(required = false) String merchantId,
			@RequestParam(required = false) String vendoreId) throws IOException {
		LOGGER.info("HomeController.showReiProfilePageForMultiLocation() : START");
		LOGGER.info(" === merchantId : " + merchantId + " vendoreId : " + vendoreId);

		try {
			Integer intMerchantId = 0;
			Integer intVendorId = 0;
			if (merchantId != null) {
				if (merchantId.matches("[0-9]+")) {
					intMerchantId = Integer.parseInt(merchantId);
				} else {
					if (EncryptionDecryptionUtil.decryption(merchantId).matches("[0-9]+"))
						intMerchantId = Integer.parseInt(EncryptionDecryptionUtil.decryption(merchantId));
				}

				HttpSession session = request.getSession();
				List<Merchant> merchantList = new ArrayList<Merchant>();
				Vendor vendor = new Vendor();
				if (vendoreId != null) {

					if (vendoreId.matches("[0-9]+")) {
						intVendorId = Integer.parseInt(vendoreId);
					} else {
						if (EncryptionDecryptionUtil.decryption(vendoreId).matches("[0-9]+"))
							intVendorId = Integer.parseInt(EncryptionDecryptionUtil.decryption(vendoreId));
					}
					vendor = merchantService.findVendorById(intVendorId);
					merchantList = merchantService.findAllMerchantsByVendorId(intVendorId);

					if (vendor != null) {
						session.setAttribute("vendor", vendor);
					}

					session.setAttribute("merchantList", merchantList);
				}

				Merchant merchant = merchantService.findById(intMerchantId);
				if (merchant == null) {
					// return "redirect:" + environment.getProperty("WEB_BASE_URL") + "/sessionTimeOut";
					return "redirect:https://www.foodkonnekt.com";
				}

				// session.setMaxInactiveInterval(2 * 60);
				Customer customer = (Customer) session.getAttribute("customer");
				if (customer != null) {
					if (customer.getMerchantt() != null) {
						Merchant customerMerchant = merchantService.findById(customer.getMerchantt().getId());
						System.out.println(merchant.getOwner().getId().intValue() != customerMerchant.getOwner().getId()
								.intValue());
						if (merchant.getOwner().getId().intValue() != customerMerchant.getOwner().getId().intValue()) {
							session.removeAttribute("customer");
						} else {
							if (customerMerchant.getId() == merchant.getId()) {
								customer.setMerchantt(merchant);
								session.setAttribute("customer", customer);
								System.out.println("customer logged in");
							} else {
								customer.setVendor(null);
								customer.setVendorId(merchant.getId());

								LOGGER.info("Customer id = " + customer.getId());

								List<Customer> customers = customerrRepository.findByEmailIdAndPasswordAndMerchanttId(
										customer.getEmailId(), customer.getPassword(), merchant.getId());
								if (customers != null && !customers.isEmpty()) {
									/*
									 * JSONObject Customer = jObject.getJSONObject("DATA"); Customer customerResult
									 * = ProducerUtil.getCustomer(Customer);
									 */

									session.setAttribute("customer", customers.get(0));

								} else {
									session.setAttribute("merchant", merchant);
									saveDuplicateCustomer(request, response);
								}

							}
						}
					}
				}

				model.addAttribute("merchantList", merchantList);

				if (merchant != null && merchant.getIsInstall() != null
						&& merchant.getIsInstall() != IConstant.SOFT_DELETE) {
					session.setAttribute("merchant", merchant);
					session.setMaxInactiveInterval(24 * 60 * 60);
					if (vendor != null && vendor.getId() != null && vendor.getId() > 0)
						model.addAttribute("vendorId", vendor.getId());

					model.addAttribute("merchantId", merchant.getId());
					map.put("Address", new Address());
				} else {
					LOGGER.info("HomeController.showReiProfilePageForMultiLocation() : END");

					return "redirect:https://www.foodkonnekt.com";
				}
			} else {
				LOGGER.info("HomeController.showReiProfilePageForMultiLocation() : END");

				return "redirect:https://www.foodkonnekt.com";
			}
		} catch (Exception e) {
			LOGGER.info("HomeController.showReiProfilePageForMultiLocation() : ERROR" + e);

			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
			}
			LOGGER.error("error: " + e.getMessage());
		}
		LOGGER.info("HomeController.showReiProfilePageForMultiLocation() : END");
		return "redirect:" + environment.getProperty("WEB_BASE_URL") + "/order";
	}

	/**
	 * Customer login
	 * 
	 * @param customer
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/customerSignIn", method = RequestMethod.POST)
	public @ResponseBody Map<Object, Object> customerSignInByAjax(@RequestBody final Customer customer,
			HttpServletRequest request) throws Exception {
		LOGGER.info("HomeController.customerSignInByAjax() : START");
		LOGGER.info("Customer id = " + customer.getId());

		Map<Object, Object> loginMap = new HashMap<Object, Object>();
		try {
			HttpSession session = request.getSession();
			Merchant merchant = (Merchant) session.getAttribute("merchant");
			Vendor vendor = (Vendor) session.getAttribute("vendor");
			Gson gson = new Gson();
			if (vendor != null && vendor.getId() != null) {
				customer.setVendor(vendor);
			}
			String customerJson = gson.toJson(customer);
			String result = ProducerUtil.signIn(customerJson,environment);

			if (result != null && result != "") {
				JSONObject jObject = new JSONObject(result);

				if (jObject.getString("response").equals(IConstant.RESPONSE_SUCCESS_MESSAGE)) {
					JSONObject Customer = jObject.getJSONObject("DATA");
					Customer customerResult = ProducerUtil.getCustomer(Customer);
					customerResult.setOrderType(customer.getOrderType());
					session.setAttribute("customer", customerResult);
					session.setAttribute("guestCustomer", customerResult);

					customer.setId(customerResult.getId());
					customer.setPhoneNumber(customerResult.getPhoneNumber());

					if (merchant != null && merchant.getId() != null) {
						customer.setMerchantId(merchant.getId());
					}
					if (vendor != null) {
						customer.setVendor(vendor);
					}

					LOGGER.info("phone------>" + customer.getPhoneNumber());
					List<Map<String, Object>> duplicateCouponMapList = new ArrayList<Map<String, Object>>();
					// Map<Object, Object> duplicate
					duplicateCouponMapList = customerService.checkDuplicatCouponAndRecalculate(customer,
							customer.getListOfALLDiscounts());
					/*
					 * List<CardInfo> cardInfos =
					 * customerService.getCustomerCardInfo(customer.getId()); List<CardInfo>
					 * cardsInfos = new ArrayList<CardInfo>(); for(CardInfo cardInfo : cardInfos){
					 * cardInfo.setCustomer(null); cardsInfos.add(cardInfo); } if(merchant!= null&&
					 * merchant.getAllowMultiPay()!=null && merchant.getAllowMultiPay()==true){
					 * //Gson gson = new Gson(); String cardsInfosJson = gson.toJson(cardsInfos);
					 * session.setAttribute("cardInfo", cardsInfosJson);
					 * session.setAttribute("allowMultiPay", true); }
					 */

					if (customerResult != null && customerResult.getId() != null && merchant != null
							&& merchant.getId() != null) {
						List<OrderR> reOrder = orderService.getAllOrder(customerResult.getId(), merchant.getId());
						session.setAttribute("reOrderLength", reOrder.size());
						if (merchant.getAllowReOrder() != null && merchant.getAllowReOrder()) {
							loginMap.put("reOrderLength", reOrder.size());
						}

					}

					List<CardInfo> cardInfos = customerService.getCustomerCardInfo(customer.getId());
					List<CardInfo> cardsInfos = new ArrayList<CardInfo>();
					List<CardInfo> expiryCardsInfos = new ArrayList<CardInfo>();
					Calendar calendar = Calendar.getInstance();
					Integer year = Integer.parseInt(("" + calendar.get(Calendar.YEAR)).substring(2));
					Integer month = calendar.get(Calendar.MONTH);

					for (CardInfo cardInfo : cardInfos) {
						cardInfo.setCustomer(null);
						cardInfo.setMerchant(null);
						// cardInfo.getExpirationDate()
						LOGGER.info("ExpirationDateYear" + cardInfo.getExpirationDate().substring(2, 4));
						LOGGER.info("ExpirationDateMonth" + cardInfo.getExpirationDate().substring(0, 2));
						if (cardInfo.getToken() != null && !cardInfo.getToken().isEmpty()
								&& cardInfo.getFirst6() != null && !cardInfo.getFirst6().isEmpty()
								&& cardInfo.getLast4() != null && !cardInfo.getLast4().isEmpty()
								&& cardInfo.getExpirationDate() != null && !cardInfo.getExpirationDate().isEmpty()
								&& cardInfo.getToken() != "" && cardInfo.getFirst6() != "" && cardInfo.getLast4() != ""
								&& cardInfo.getExpirationDate() != "") {
							if (Integer.parseInt(cardInfo.getExpirationDate().substring(2, 4)) > year) {
								cardsInfos.add(cardInfo);
							} else if (Integer.parseInt(cardInfo.getExpirationDate().substring(2, 4)) == year
									&& Integer.parseInt(cardInfo.getExpirationDate().substring(0, 2)) > month) {
								cardsInfos.add(cardInfo);
							} else {
								expiryCardsInfos.add(cardInfo);
							}
						}
					}
					if (merchant != null && merchant.getAllowMultiPay() != null
							&& merchant.getAllowMultiPay() == true) {
						// Gson gson = new Gson();
						String cardsInfosJson = gson.toJson(cardsInfos);
						String expiryCardsInfosJson = gson.toJson(expiryCardsInfos);
						session.setAttribute("cardInfo", cardsInfosJson);
						session.setAttribute("expiryCardsInfos", expiryCardsInfosJson);
						session.setAttribute("allowMultiPay", true);
					}
					System.out.println("---------------------------");
					for (int i = 0; i < duplicateCouponMapList.size(); i++) {
						LOGGER.info(" duplicateCouponMapList : " + duplicateCouponMapList.get(i));
					}
					if (customerResult.getPassword() != null && !customerResult.getPassword().isEmpty())
						loginMap.put("customerPswd", "Y");
					else
						loginMap.put("customerPswd", "N");

					loginMap.put("success", "Login successfully");
					loginMap.put("duplicateCouponMapList", duplicateCouponMapList);
					loginMap.put("address", customerResult.getAddresses());
					loginMap.put("custId", customerResult.getId());
					loginMap.put("customerName", customerResult.getFirstName().toUpperCase());
					if (merchant != null && merchant.getAllowMultiPay() != null
							&& merchant.getAllowMultiPay() == true) {
						loginMap.put("cardInfo", cardsInfos);
					}
				} else {
					loginMap.put("success", "Invalid login");
				}
			} else {
				loginMap.put("success", "Invalid login");
			}

		} catch (Exception e) {
			LOGGER.info("HomeController.customerSignInByAjax() : ERROR" + e);

			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
			}
			LOGGER.error("error: " + e.getMessage());
		}
		LOGGER.info("HomeController.customerSignInByAjax() : END");
		return loginMap;
	}

	/**
	 * customer registration
	 * 
	 * @param address
	 * @param result
	 * @param model
	 * @param request
	 * @param response
	 * @return Map<Object, Object>
	 */
	@RequestMapping(value = "/registerCustomer", method = { RequestMethod.POST })
	public @ResponseBody Map<Object, Object> saveCustomer(@RequestBody final Customer customer, ModelMap model,
			HttpServletRequest request, HttpServletResponse response) {
		LOGGER.info("HomeController.saveCustomer() : START");
		LOGGER.info("Customer id = " + customer.getId());

		Map<Object, Object> duplicateEmail = new HashMap<Object, Object>();
		try {
			HttpSession session = request.getSession();
			Merchant merchant = (Merchant) session.getAttribute("merchant");
			Vendor vendor = (Vendor) session.getAttribute("vendor");
			Customer sessionCustomer = (Customer) session.getAttribute("customer");
			if (merchant != null) {
				if (sessionCustomer == null) {
					customer.setMerchantId(merchant.getId());
					if (vendor != null && vendor.getId() != null) {
						customer.setVendor(vendor);
					}
					Customer finalCustomer = ProducerUtil.setCusotmerInfo(customer, merchant.getId());

					Gson gson = new Gson();
					String customerJson = gson.toJson(finalCustomer);
					LOGGER.info(" ===customerJson : " + customerJson);
					String result = ProducerUtil.updateCustomer(customerJson,environment);
					if (result != null && result.contains("response")) {
						JSONObject jObject = new JSONObject(result);
						if (jObject.getString("response").equals(IConstant.RESPONSE_SUCCESS_MESSAGE)) {
							JSONObject Customer = jObject.getJSONObject("DATA");
							Customer customerResult = ProducerUtil.getsignUpCustomer(Customer);

							if (customerResult != null && customerResult.getAddresses() != null
									&& customerResult.getAddresses().size() > 0) {
								for (Address address : customerResult.getAddresses()) {
									duplicateEmail.put("addressId", address.getId());
								}
							} else {
								duplicateEmail.put("addressId", "address not found");
							}

							customerResult.setOrderType(customer.getOrderType());
							LOGGER.info("customer-order type--" + customer.getOrderType());
							LOGGER.info("customerResult-order type--" + customerResult.getOrderType());
							customer.setId(customerResult.getId());
							customer.setPhoneNumber(customerResult.getPhoneNumber());
							LOGGER.info("phone------>" + customer.getPhoneNumber() + " id--" + customer.getId()
									+ " list of disc-" + customer.getListOfALLDiscounts());
							List<Map<String, Object>> duplicateCouponMapList = new ArrayList<Map<String, Object>>();
							// Map<Object, Object> duplicate
							duplicateCouponMapList = customerService.checkDuplicatCouponAndRecalculate(customer,
									customer.getListOfALLDiscounts());

							System.out.println("---------------------------");
							for (int i = 0; i < duplicateCouponMapList.size(); i++) {
								System.out.println(duplicateCouponMapList.get(i));
							}

							session.setAttribute("customer", customerResult);
							session.setAttribute("guestCustomer", customerResult);
							if (customerResult.getPassword() != null) {
								if (customerResult.getPassword().isEmpty()) {
									duplicateEmail.put("setPassword", "Y");
								}
							} else {
								duplicateEmail.put("setPassword", "Y");
							}

							duplicateEmail.put("message", "successfully register");
							duplicateEmail.put("customerAfterSuccessId", customerResult.getId());
						}
					} else {
						duplicateEmail.put("message", "Please try again");
					}
				} else {
					duplicateEmail.put("message", "Your all already registerded");
				}
			} else {
				duplicateEmail.put("message", "timeIssue");
				response.sendRedirect("https://www.foodkonnekt.com");
			}
		} catch (Exception e) {
			LOGGER.info("HomeController.saveCustomer() : ERROR" + e);

			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
			}
			LOGGER.error("error: " + e.getMessage());
		}
		LOGGER.info("HomeController.saveCustomer() : END");
		return duplicateEmail;
	}

	public String saveDuplicateCustomer(HttpServletRequest request, HttpServletResponse response) {
		LOGGER.info("HomeController.saveDuplicateCustomer() : START");

		Map<Object, Object> duplicateEmail = new HashMap<Object, Object>();
		try {

			HttpSession session = request.getSession();
			Merchant merchant = (Merchant) session.getAttribute("merchant");
			Customer customer = (Customer) session.getAttribute("customer");

			if (merchant != null && customer != null) {

				if (merchant.getId() != customer.getMerchantt().getId()) {
					if (customer.getId() != null) {
						LOGGER.info("Customer id = " + customer.getId());

						Customer dbCustomer = customerService.findByCustomerId(customer.getId());
						if (dbCustomer != null && dbCustomer.getLastName() != null) {
							customer.setLastName(dbCustomer.getLastName());
						}
					}

					customer.setId(null);
					customer.setMerchantId(merchant.getId());

					if (customer.getVendor() == null) {
						customer.setVendor(merchant.getOwner());
					}

					Customer finalCustomer = ProducerUtil.setCusotmerInfo(customer, merchant.getId());

					if (finalCustomer.getPassword() != null) {
						finalCustomer.setPassword("@duplicatepassword#" + finalCustomer.getPassword());
					}
					System.out.println(finalCustomer.getPassword());

					Gson gson = new Gson();
					String customerJson = gson.toJson(finalCustomer);
					String result = ProducerUtil.updateCustomer(customerJson,environment);
					if (result != null && result.contains("response")) {
						JSONObject jObject = new JSONObject(result);
						if (jObject.getString("response").equals(IConstant.RESPONSE_SUCCESS_MESSAGE)) {
							JSONObject Customer = jObject.getJSONObject("DATA");
							Customer customerResult = ProducerUtil.getsignUpCustomer(Customer);

							customerResult.setOrderType(customer.getOrderType());
							LOGGER.info("customer-order type--" + customer.getOrderType());
							LOGGER.info("customerResult-order type--" + customerResult.getOrderType());
							customer.setId(customerResult.getId());
							customer.setPhoneNumber(customerResult.getPhoneNumber());
							LOGGER.info("phone------>" + customer.getPhoneNumber() + " id--" + customer.getId()
									+ " list of disc-" + customer.getListOfALLDiscounts());
							List<Map<String, Object>> duplicateCouponMapList = new ArrayList<Map<String, Object>>();
							// Map<Object, Object> duplicate
							duplicateCouponMapList = customerService.checkDuplicatCouponAndRecalculate(customer,
									customer.getListOfALLDiscounts());

							System.out.println("---------------------------");
							for (int i = 0; i < duplicateCouponMapList.size(); i++) {
								System.out.println(duplicateCouponMapList.get(i));
							}

							session.setAttribute("customer", customerResult);
							session.setAttribute("guestCustomer", customerResult);
							duplicateEmail.put("message", "successfully register");
							duplicateEmail.put("customerAfterSuccessId", customerResult.getId());
						}
					} else {
						duplicateEmail.put("message", "Please try again");
					}
				} else {

					session.setAttribute("customer", customer);
				}
			} else {
				duplicateEmail.put("message", "timeIssue");
				// response.sendRedirect("sessionTimeOut");
				LOGGER.info("HomeController.saveDuplicateCustomer() : END");

				return "redirect:https://www.foodkonnekt.com";
			}

		} catch (Exception e) {
			LOGGER.info("HomeController.saveDuplicateCustomer() : ERROR" + e);

			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
			}
			LOGGER.error("error: " + e.getMessage());
		}
		LOGGER.info("HomeController.saveDuplicateCustomer() : END");
		return "success";
	}

	/**
	 * customer signIn
	 * 
	 * @param address
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/signIn", method = { RequestMethod.POST })
	public String customerSignInByFormSubmit(@ModelAttribute("Address") Address address, ModelMap model,
			HttpServletRequest request, HttpServletResponse response) {
		LOGGER.info("HomeController.customerSignInByFormSubmit() : START");

		LOGGER.info("HomeController.customerSignInByFormSubmit() : END");
		return "redirect:" + environment.getProperty("WEB_BASE_URL") + "/order";
	}

	/**
	 * Customer logout.
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logout(HttpServletRequest request, HttpServletResponse response, ModelMap model) {
		LOGGER.info("HomeController.logout() : START");
		Integer merchantId = null;
		HttpSession session = request.getSession();
		if (session != null) {
			Merchant merchant = (Merchant) session.getAttribute("merchant");
			if (merchant != null) {
				Vendor vendor = (Vendor) session.getAttribute("vendor");
				merchantId = merchant.getId();
				model.addAttribute("merchantId", merchant.getId());
				// session.invalidate();
				session.removeAttribute("customer");
				session.removeAttribute("cardInfo");
				session.removeAttribute("expiryCardsInfos");
				session.removeAttribute("reOrderLength");

				if (vendor != null) {
					if (vendor.getId() != null) {
						model.addAttribute("vendorId", vendor.getId());
						/*
						 * if(merchantId.equals(IConstant.TEXAN_REWARDS_MERCHANT_ID)){ return
						 * "redirect:" + environment.getProperty("TEXAN_BASE_URL") + "/order"; }else{
						 */
						LOGGER.info("HomeController.logout() : END");

						return "redirect:" + environment.getProperty("WEB_BASE_URL") + "/order";
						// }
						// return "redirect:" + environment.getProperty("WEB_BASE_URL") + "/order";
					}
				}
			}
		}

		LOGGER.info("HomeController.logout() : END");
		/*
		 * if(merchantId.equals(IConstant.TEXAN_REWARDS_MERCHANT_ID)){ return
		 * "redirect:" + environment.getProperty("TEXAN_BASE_URL") + "/order"; }else{
		 */
		return "redirect:" + environment.getProperty("WEB_BASE_URL") + "/order";
		// }

		// return "redirect:" + environment.getProperty("WEB_BASE_URL") + "/order";

	}

	/**
	 * Show profile page
	 * 
	 * @param model
	 * @param map
	 * @return
	 */
	@RequestMapping(value = "/profile", method = RequestMethod.GET)
	public String showProfile(ModelMap model, Map<String, Object> map, HttpServletRequest request) {
		LOGGER.info("HomeController.showProfile() : START");

		try {
			HttpSession session = request.getSession();
			Customer customer = (Customer) session.getAttribute("customer");
			Merchant merchant = (Merchant) session.getAttribute("merchant");
			if (customer != null) {
				map.put("Customer", new Customer());

				model.addAttribute("Customer", customer);
				model.addAttribute("customerAddresses",
						customerService.findAllCustomerAddress(customer.getId(), merchant.getId()));

				List<CardInfo> cardInfos = customerService.getCustomerCardInfo(customer.getId());
				List<CardInfo> cardsInfos = new ArrayList<CardInfo>();
				List<CardInfo> expiryCardsInfos = new ArrayList<CardInfo>();
				Calendar calendar = Calendar.getInstance();
				Integer year = Integer.parseInt(("" + calendar.get(Calendar.YEAR)).substring(2));
				Integer month = calendar.get(Calendar.MONTH);

				for (CardInfo cardInfo : cardInfos) {
					cardInfo.setMerchant(null);
					cardInfo.setCustomer(null);
					// cardInfo.getExpirationDate()
					System.out.println("ExpirationDateYear" + cardInfo.getExpirationDate().substring(2, 4));
					System.out.println("ExpirationDateMonth" + cardInfo.getExpirationDate().substring(0, 2));
					if (cardInfo.getToken() != null && !cardInfo.getToken().isEmpty() && cardInfo.getFirst6() != null
							&& !cardInfo.getFirst6().isEmpty() && cardInfo.getLast4() != null
							&& !cardInfo.getLast4().isEmpty() && cardInfo.getExpirationDate() != null
							&& !cardInfo.getExpirationDate().isEmpty() && cardInfo.getToken() != ""
							&& cardInfo.getFirst6() != "" && cardInfo.getLast4() != ""
							&& cardInfo.getExpirationDate() != "") {
						if (Integer.parseInt(cardInfo.getExpirationDate().substring(2, 4)) > year) {
							cardsInfos.add(cardInfo);
						} else if (Integer.parseInt(cardInfo.getExpirationDate().substring(2, 4)) == year
								&& Integer.parseInt(cardInfo.getExpirationDate().substring(0, 2)) > month) {
							cardsInfos.add(cardInfo);
						} else {
							expiryCardsInfos.add(cardInfo);
						}
					}

				}
				Gson gson = new Gson();
				if (merchant != null && merchant.getAllowMultiPay() != null && merchant.getAllowMultiPay() == true) {
					String cardsInfosJson = gson.toJson(cardsInfos);
					String expiryCardsInfosJson = gson.toJson(expiryCardsInfos);
					session.setAttribute("cardInfo", cardsInfosJson);
					session.setAttribute("expiryCardsInfos", expiryCardsInfosJson);
					session.setAttribute("allowMultiPay", true);
				} else {
					session.setAttribute("allowMultiPay", false);
					session.removeAttribute("cardInfo");
					session.removeAttribute("expiryCardsInfos");
				}
				LOGGER.info("HomeController.showProfile() : END");

				return "profile";
			} else {
				LOGGER.info("HomeController.showProfile() : END");

				// return "redirect:" + environment.getProperty("WEB_BASE_URL") + "/sessionTimeOut";
				return "redirect:https://www.foodkonnekt.com";
			}
		} catch (Exception e) {
			LOGGER.info("HomeController.showProfile() : ERROR" + e);

			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
			}
			LOGGER.error("error: " + e.getMessage());
		}
		LOGGER.info("HomeController.showProfile() : END");
		return "profile";
	}

	/**
	 * Update customer profile and address
	 * 
	 * @param customer
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/updateCustomerProfile", method = { RequestMethod.POST })
	public String updateCustomer(@ModelAttribute("Customer") Customer customer, ModelMap model,
			HttpServletRequest request, HttpServletResponse response) {
		LOGGER.info("HomeController.updateCustomer() : START");

		try {
			HttpSession session = request.getSession();
			Merchant merchant = (Merchant) session.getAttribute("merchant");
			Vendor vendor = (Vendor) session.getAttribute("vendor");

			if(merchant != null) {
			if (vendor != null && vendor.getId() != null) {
				customer.setVendor(vendor);
			}
			List<Customer> customers = null;
			if (vendor != null && vendor.getId() != null) {
				if (customer != null && customer.getId() != null) {
					Customer dbCustomer = customerService.findByCustomerId(customer.getId());
					if (dbCustomer != null) {
						customers = customerService.findByEmailIDAndVendorId(dbCustomer.getEmailId(), vendor.getId());
						if (customers != null && customers.size() > 0) {
							for (Customer cust : customers) {
								if (cust != null && cust.getPassword() != null && cust.getPassword().length() > 0
										&& cust.getMerchantt() != null && customer.getMerchantt() != null
										&& cust.getMerchantt().getId() != null
										&& customer.getMerchantt().getId() != null
										&& cust.getMerchantt().getId() != customer.getMerchantt().getId()) {
									Gson gson = new Gson();
									cust.setAnniversaryDate(customer.getAnniversaryDate());
									cust.setBirthDate(customer.getBirthDate());
									cust.setFirstName(customer.getFirstName());
									if (customer.getLastName() != null)
										cust.setLastName(customer.getLastName());
									cust.setLastName(customer.getLastName());
									cust.setEmailId(customer.getEmailId());
									cust.setPassword(customer.getPassword());
									Merchant merchantt = new Merchant();
									merchantt.setId(cust.getMerchantt().getId());
									cust.setMerchantt(merchantt);
									cust.setPhoneNumber(customer.getPhoneNumber());
									String customerJson = gson.toJson(cust);
									ProducerUtil.updateCustomer(customerJson,environment);
								}
							}
						}
					}
				}
			}
			Gson gson = new Gson();
			customer = ProducerUtil.setAddresses(customer);
			String customerJson = gson.toJson(customer);
			String result = ProducerUtil.updateCustomer(customerJson,environment);
			JSONObject jObject = new JSONObject(result);
			if (result.contains("response")
					&& jObject.getString("response").equals(IConstant.RESPONSE_SUCCESS_MESSAGE)) {
				JSONObject Customer = jObject.getJSONObject("DATA");
				Customer customerResult = ProducerUtil.getCustomer(Customer);
				session.setAttribute("customer", customerResult);
				session.setAttribute("guestCustomer", customerResult);
				model.addAttribute("Customer", (Customer) session.getAttribute("customer"));
				model.addAttribute("customerAddresses",
						customerService.findAllCustomerAddress(customer.getId(), merchant.getId()));
			}
		}else
			return "redirect:https://www.foodkonnekt.com";	
		} catch (Exception e) {
			LOGGER.info("HomeController.updateCustomer() : ERROR" + e);

			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
			}
			LOGGER.error("error: " + e.getMessage());
		}
		LOGGER.info("HomeController.updateCustomer() : END");
		return "profile";
	}

	/**
	 * Show forgot password page
	 * 
	 * @param model
	 * @param map
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/forgotPassword", method = RequestMethod.GET)
	public String showForgot(ModelMap model, Map<String, Object> map, HttpServletRequest request) {
		LOGGER.info("HomeController.showForgot() : START");

		map.put("Customer", new Customer());

		LOGGER.info("HomeController.showForgot() : END");
		return "forgotPassword";
	}

	/**
	 * Send password to email
	 * 
	 * @param customer
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/sendPassword", method = { RequestMethod.POST })
	public String sendPassword(@ModelAttribute("Customer") Customer customer, ModelMap model, Map<String, Object> map,
			HttpServletRequest request, HttpServletResponse response) {
		LOGGER.info("HomeController.sendPassword() : START");

		try {
			Gson gson = new Gson();
			String customerJson = gson.toJson(customer);
			LOGGER.info(" ===customerJson : "+customerJson);
			String result = ProducerUtil.forgotPassword(customerJson,environment);
			JSONObject jObject = new JSONObject(result);
			if (jObject.has("response")) {
				if (jObject.getString("response").equals(IConstant.RESPONSE_SUCCESS_MESSAGE)) {
					model.addAttribute("message",
							"Please check your inbox - we have sent you an email with instructions");
				} else {
					model.addAttribute("message",
							"Please check your inbox - we have sent you an email with instructions");
				}
			}
			map.put("Customer", new Customer());
		} catch (Exception e) {
			LOGGER.info("HomeController.sendPassword() : ERROR" + e);

			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
			}
			LOGGER.error("error: " + e.getMessage());
		}
		LOGGER.info("HomeController.sendPassword() : END");
		return "forgotPassword";
	}

	@RequestMapping(value = "/forgotPasswordAction", method = RequestMethod.POST)
	public @ResponseBody Map<Object, Object> forgotPasswordAction(@RequestBody final Customer customer,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		LOGGER.info("HomeController.forgotPasswordAction() : START");

		Map<Object, Object> forgotPasswordMap = new HashMap<Object, Object>();
		try {
			HttpSession session = request.getSession();
			Merchant merchant = (Merchant) session.getAttribute("merchant");
			Vendor vendor = (Vendor) session.getAttribute("vendor");
			Integer vendorId = 0;
			if (vendor != null && vendor.getId() != null) {
				vendorId = vendor.getId();

			}
			if (merchant != null) {
				boolean status = false;
				if (vendorId > 0) {
					status = customerService.findByEmailAndMerchantIdAndVendorId(customer.getEmailId(),
							merchant.getId(), vendorId);
				} else {
					status = customerService.findByEmailAndMerchantId(customer.getEmailId(), merchant.getId());
				}
				LOGGER.info(" === status : "+status);
				if (status) {
					forgotPasswordMap.put("message",
							"Please check your inbox - we have sent you an email with instructions");
				} else {
					forgotPasswordMap.put("message",
							"An account with the below email does not exist. Please try again");
				}
			} else {
				LOGGER.info("HomeController.forgotPasswordAction() : END");

				response.sendRedirect("https://www.foodkonnekt.com");

			}
		} catch (Exception exception) {
			LOGGER.info("HomeController.forgotPasswordAction() : ERROR" + exception);

			if (exception != null) {
				MailSendUtil.sendExceptionByMail(exception,environment);
			}
			exception.printStackTrace();
		}
		LOGGER.info("HomeController.forgotPasswordAction() : END");
		return forgotPasswordMap;
	}

	@RequestMapping(value = "/checkOldPassword", method = RequestMethod.POST)
	public @ResponseBody Map<Object, Object> checkPassword(@RequestBody final Customer customer,
			HttpServletRequest request) throws Exception {
		LOGGER.info("HomeController.checkPassword() : START");

		Map<Object, Object> loginMap = new HashMap<Object, Object>();
		try {
			HttpSession session = request.getSession();
			Customer customer2 = (Customer) session.getAttribute("customer");
			if(customer2 != null) {
			LOGGER.info(" ===customer.getPassword() : "+customer.getPassword());
			Customer result = customerService.findByEmailAndPasswordAndMerchantId(customer2.getEmailId(),
					EncryptionDecryptionUtil.encryptString(customer.getPassword()), customer2.getMerchantt().getId());
			if (result != null) {
				loginMap.put("success", "samePassword");
			} else {
				loginMap.put("success", "Your old password is not match , please enter correct password");
			}
			}else {
				loginMap.put("success", "session expired");
			}
		} catch (Exception e) {
			LOGGER.info("HomeController.checkPassword() : ERROR" + e);

			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
			}
			LOGGER.error("error: " + e.getMessage());
		}
		LOGGER.info("HomeController.checkPassword() : END");
		return loginMap;
	}

	/**
	 * Check duplicate emailId
	 * 
	 * @param customer
	 * @param model
	 * @param map
	 * @param request
	 * @param response
	 * @return message
	 */
	@RequestMapping(value = "/checkDuplicateEmailId", method = { RequestMethod.POST })
	public @ResponseBody Map<Object, Object> checkEmailId(@RequestBody final Customer customer, ModelMap model,
			Map<String, Object> map, HttpServletRequest request, HttpServletResponse response) {
		LOGGER.info("HomeController.checkEmailId() : START");

		Map<Object, Object> duplicateEmail = new HashMap<Object, Object>();
		try {
			Gson gson = new Gson();
			HttpSession session = request.getSession();
			if (session != null) {
				Merchant merchant = (Merchant) session.getAttribute("merchant");
				if (merchant != null) {
					Vendor vendor = (Vendor) session.getAttribute("vendor");
					Integer vendorId = 0;
					if (vendor != null && vendor.getId() != null) {
						vendorId = vendor.getId();

					}
					String customerJson = gson.toJson(customer);
					LOGGER.info(" ===customerJson : "+customerJson);
					String result = ProducerUtil.duplicateEmail(customerJson, merchant, vendorId,environment);
					if (result != null && result.startsWith("{") && result.endsWith("}") && result.contains("{")) {
						JSONObject jObject = new JSONObject(result);
						if (jObject.getString("response").equals(IConstant.RESPONSE_SUCCESS_MESSAGE)) {
							duplicateEmail.put("message", "true");
						} else {
							duplicateEmail.put("message", "false");
						}
					}
				}
			}
		} catch (Exception e) {
			LOGGER.info("HomeController.checkEmailId() : ERROR" + e);

			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
			}
			LOGGER.error("error: " + e.getMessage());
		}
		LOGGER.info("HomeController.checkEmailId() : END");
		return duplicateEmail;
	}

	/**
	 * Show guest password screen
	 * 
	 * @param model
	 * @param map
	 * @param request
	 * @return String
	 */
	@RequestMapping(value = "/setGuestPassword", method = RequestMethod.GET)
	public String setGuestPassword(ModelMap model, Map<String, Object> map, HttpServletRequest request) {
		LOGGER.info("HomeController.setGuestPassword() : START");

		map.put("Customer", new Customer());

		LOGGER.info("HomeController.setGuestPassword() : END");
		return "setGuestPassword";
	}

	@RequestMapping(value = "/changepassword", params = { "email", "vendorId", "merchantId",
			"userId" }, method = RequestMethod.GET)
	public String setResetPassword(Map<String, Object> map, HttpServletRequest request,
			@RequestParam(value = "email") String email, @RequestParam(value = "vendorId") String vendorId,
			@RequestParam(value = "merchantId") String merchantId, @RequestParam(value = "userId") String userId,
			@RequestParam(value = "tLog") String tLog) {
		LOGGER.info("HomeController.setResetPassword() : START");
        LOGGER.info(" === userId : "+userId+" tLog : "+tLog);
		try {
			HttpSession session = request.getSession();
			if (merchantId.matches("[0-9]+")) {
				Merchant merchant = merchantService.findById(Integer.valueOf(merchantId));
				if (merchant != null && merchant.getOwner() != null && merchant.getOwner().getId() != null) {
					session.setAttribute("vendor", merchant.getOwner());
				}
			}
			Long time = Long.valueOf(EncryptionDecryptionUtil.decryption(tLog));
			Long currentTime = System.currentTimeMillis();
			int diff = (int) (currentTime - time);
LOGGER.info(" === Difference between current time and tLog time : "+diff);
			if (diff < EXPIRY_TIME) {

				if (merchantId != null) {
					String id = merchantId;
					Customer customer = customerService.findCustomerByEmailAndMerchantId(email, Integer.valueOf(id));
					if (customer != null
							&& userId.equalsIgnoreCase(EncryptionDecryptionUtil.encryptString(customer.getId() + ""))) {
						customer.setPassword("");
						LOGGER.info("HomeController.setResetPassword() : END");

						map.put("Customer", customer);
						return "resetGuestPassword";
					} else {
						LOGGER.info("HomeController.setResetPassword() : END");

						return "redirect" + ":https://www.foodkonnekt.com";
					}
				}
			} else {
				LOGGER.info("Link is expired");
				LOGGER.info("HomeController.setResetPassword() : END");

				map.put("Customer", new Customer());
				return "forgotPassword";
			}
		} catch (Exception e) {
			LOGGER.info("HomeController.setResetPassword() : ERROR" + e);

			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
			}
			LOGGER.error("error: " + e.getMessage());
		}
		LOGGER.info("HomeController.setResetPassword() : END");
		return null;
	}

	@RequestMapping(value = "/changepassword", params = { "email", "merchantId", "userId" }, method = RequestMethod.GET)
	public String setResetPassword(Map<String, Object> map, HttpServletRequest request,
			@RequestParam(value = "email") String email, @RequestParam(value = "merchantId") String merchantId,
			@RequestParam(value = "userId") String userId, @RequestParam(value = "tLog") String tLog) {
		LOGGER.info("HomeController.setResetPassword() : START");
        LOGGER.info(" === userId : "+userId+" tLog : "+tLog);

		try {
			Long time = Long.valueOf(EncryptionDecryptionUtil.decryption(tLog));
			Long currentTime = System.currentTimeMillis();
			int diff = (int) (currentTime - time);
			LOGGER.info(" === Difference between current time and tLog time : "+diff);

			if (diff < EXPIRY_TIME) {

				if (merchantId != null) {
					String id = merchantId;
					Customer customer = customerService.findCustomerByEmailAndMerchantId(email, Integer.valueOf(id));
					if (customer != null
							&& userId.equalsIgnoreCase(EncryptionDecryptionUtil.encryptString(customer.getId() + ""))) {
						customer.setPassword("");
						map.put("Customer", customer);
						LOGGER.info("HomeController.setResetPassword() : END");

						return "resetGuestPassword";
					} else {
						LOGGER.info("HomeController.setResetPassword() : END");

						return "redirect" + ":https://www.foodkonnekt.com";
					}
				}
			} else {
				System.out.println("Link is expired");
				map.put("Customer", new Customer());
				LOGGER.info("HomeController.setResetPassword() : END");

				return "forgotPassword";
			}
		} catch (Exception e) {
			LOGGER.info("HomeController.setResetPassword() : ERROR" + e);

			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
			}
			LOGGER.error("error: " + e.getMessage());
		}
		LOGGER.info("HomeController.setResetPassword() : END");
		return null;
	}

	@RequestMapping(value = "/resetPhpCustomerPassword", params = { "email", "merchantId" }, method = RequestMethod.GET)
	public String resetPhpCustomerPassword(Map<String, Object> map, HttpServletRequest request,
			@RequestParam(value = "email") String email, @RequestParam(value = "merchantId") Integer merchantId) {
		LOGGER.info("HomeController.resetPhpCustomerPassword() : START");

		try {
			Customer customer = customerService.findCustomerByEmailAndMerchantId(email, merchantId);
			if (customer != null) {
				customer.setPassword("");
				map.put("Customer", customer);
				map.put("phpcustomerErrorMsg", "Your Password Has Expired. Please Reset");
				LOGGER.info("HomeController.resetPhpCustomerPassword() : END");

				return "resetGuestPassword";
			} else {
				LOGGER.info("HomeController.resetPhpCustomerPassword() : END");

				return "redirect:https://www.foodkonnekt.com";
			}
		} catch (Exception e) {
			LOGGER.info("HomeController.resetPhpCustomerPassword() : ERROR" + e);

			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
			}
			LOGGER.error("error: " + e.getMessage());
		}
		LOGGER.info("HomeController.resetPhpCustomerPassword() : END");
		return "resetGuestPassword";
	}

	@RequestMapping(value = "/saveResetGuestPassword", method = RequestMethod.POST)
	public String saveResetGuestPassword(@ModelAttribute("Customer") Customer customer, ModelMap model,
			HttpServletRequest request, HttpServletResponse response) {
		LOGGER.info("HomeController.saveResetGuestPassword() : START");

		try {
			HttpSession session = request.getSession();
			Merchant merchant = (Merchant) session.getAttribute("merchant");

			String userPage = customer.getUserPage();
			
				customerService.setGuestCustomerPassword(customer);
				Customer customerInfo = customerService.findByEmailAndCustomerId("", customer.getId());
				LOGGER.info("marchant ID is" + customerInfo.getMerchantt().getId());

				session.setAttribute("customer", customerInfo);
				String merchantName = customerInfo.getMerchantt().getName();
				merchantName = merchantName.replaceAll("\\s+", "");

				Vendor vendor = (Vendor) session.getAttribute("vendor");
				if(userPage!=null && userPage.contains("user")) {
					return "redirect:" + environment.getProperty("BASE_URL") + "/support";
				}
				else if (vendor != null && vendor.getId() != null) {
					String vendorId = EncryptionDecryptionUtil.encryption(Integer.toString(vendor.getId()));
					String merchId = EncryptionDecryptionUtil
							.encryption(Integer.toString(customerInfo.getMerchantt().getId()));

					LOGGER.info("HomeController.sendLocation() : END");
					return "redirect:" + environment.getProperty("WEB_BASE_URL") + "/vendor/" + vendorId + "/" + merchantName
							+ "/clover/" + merchId;
				} else {
					LOGGER.info("HomeController.saveResetGuestPassword() : END");

					return "redirect:" + environment.getProperty("WEB_BASE_URL") + "/" + merchantName + "/clover/"
							+ EncryptionDecryptionUtil.encryption(String.valueOf(customerInfo.getMerchantt().getId()));
				}
		} catch (Exception e) {
			LOGGER.info("HomeController.saveResetGuestPassword() : ERROR" + e);

			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
			}
			LOGGER.error("error: " + e.getMessage());
		}
		LOGGER.info("HomeController.saveResetGuestPassword() : END");
		return "redirect:" + environment.getProperty("BASE_URL") + "/support";
	}

	/**
	 * Save guest customer password
	 * 
	 * @param customer
	 * @param model
	 * @param request
	 * @param response
	 * @return String
	 */
	@RequestMapping(value = "/saveGuestPassword", method = RequestMethod.POST)
	public String saveGuestPassword(@ModelAttribute("Customer") Customer customer, ModelMap model,
			HttpServletRequest request, HttpServletResponse response) {
		LOGGER.info("HomeController.saveGuestPassword() : START");

		try {
			HttpSession session = request.getSession();
			if (session != null) {
				Merchant merchant = (Merchant) session.getAttribute("merchant");
				String merchantLogo = "http://www.dev.foodkonnekt.com/app/foodnew.jpg";
				String locationName = " ";
				if (merchant != null) {
					if (merchant.getMerchantLogo() == null) {
						merchantLogo = "http://www.dev.foodkonnekt.com/app/foodnew.jpg";
					} else {
						merchantLogo = environment.getProperty("BASE_PORT") + merchant.getMerchantLogo();
					}
					if (merchant.getName() != null) {
						locationName = merchant.getName();
					}
				}
				final String customerLogo = merchantLogo;
				final String merchantName = locationName;
				Customer sessionCustomer = (Customer) session.getAttribute("customer");
				if (sessionCustomer != null) {
					LOGGER.info(" === customer.getPassword() : "+customer.getPassword());
					sessionCustomer.setPassword(customer.getPassword());
					final Customer finalCustomer = customerService.setGuestCustomerPassword(sessionCustomer);

					Thread t1 = new Thread(new Runnable() {

						public void run() {
							try {
								Thread.sleep(2000);
							} catch (InterruptedException exception) {
								exception.printStackTrace();
							}
							MailSendUtil.customerRegistartionConfirmation(finalCustomer, customerLogo, merchantName);
							System.out.println("Thread over");
						}
					});
					t1.start();
					session.setAttribute("customer", finalCustomer);
				} else {
					LOGGER.info("HomeController.saveGuestPassword() : END");

					return "redirect:" + environment.getProperty("WEB_BASE_URL") + "/order";
				}
			} else {
				LOGGER.info("HomeController.saveGuestPassword() : END");

				return "redirect:" + environment.getProperty("WEB_BASE_URL") + "/order";
			}
		} catch (Exception e) {
			LOGGER.info("HomeController.saveGuestPassword() : ERROR" + e);

			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
			}
			LOGGER.error("error: " + e.getMessage());
		}
		LOGGER.info("HomeController.saveGuestPassword() : END");
		return "redirect:" + environment.getProperty("WEB_BASE_URL") + "/myOrder";
	}

	/**
	 * Cancel account request by guest customer
	 * 
	 * @param model
	 * @param map
	 * @param request
	 * @return String
	 */
	@RequestMapping(value = "/cancelAccountRequest", method = RequestMethod.GET)
	public String cancelAccountRequest(ModelMap model, Map<String, Object> map, HttpServletRequest request) {
		LOGGER.info("HomeController.cancelAccountRequest() : START");

		HttpSession session = request.getSession();
		session.removeAttribute("customer");
		session.removeAttribute("cardInfo");
		session.removeAttribute("expiryCardsInfos");
		// session.invalidate();
		// return "redirect:https://www.foodkonnekt.com";

		Vendor vendor = (Vendor) session.getAttribute("vendor");
LOGGER.info(" === vendor.getId() : " + vendor.getId());

		if (vendor != null) {
			if (vendor.getId() != null) {
				model.addAttribute("vendorId", vendor.getId());

			}
		}

		Merchant merchant = (Merchant) session.getAttribute("merchant");
		LOGGER.info("HomeController.cancelAccountRequest() : END");
		// return "redirect:" + environment.getProperty("WEB_BASE_URL") + "/order";
		/*
		 * if(merchant!=null &&
		 * merchant.getId().equals(IConstant.TEXAN_REWARDS_MERCHANT_ID)){ return
		 * "redirect:" + environment.getProperty("TEXAN_BASE_URL") + "/order"; }else{
		 */
		return "redirect:" + environment.getProperty("WEB_BASE_URL") + "/order";
		// }
	}

	/**
	 * Cancel account request by guest customer
	 * 
	 * @param model
	 * @param map
	 * @param request
	 * @return String
	 */
	@RequestMapping(value = "/sessionTimeOut", method = RequestMethod.GET)
	public String sessionTimeOut(ModelMap model) {
		LOGGER.info("HomeController.sessionTimeOut() : START");

		// return "sessionTimeOut";

		LOGGER.info("HomeController.sessionTimeOut() : END");
		return "redirect:https://www.foodkonnekt.com";
	}

	@RequestMapping(value = "/changeUserpassword", params = { "email", "merchantId", "userId" }, method = RequestMethod.GET)
	public String setUserResetPassword(Map<String, Object> map, HttpServletRequest request,
			@RequestParam(value = "email") String email, @RequestParam(value = "merchantId") String merchantId,
			@RequestParam(value = "userId") String userId, @RequestParam(value = "tLog") String tLog) {
		LOGGER.info("HomeController.setResetPassword() : START");
		LOGGER.info(" === userId : " + userId + " tLog : " + tLog);

		try {
			Long time = Long.valueOf(EncryptionDecryptionUtil.decryption(tLog));
			Long currentTime = System.currentTimeMillis();
			int diff = (int) (currentTime - time);
			LOGGER.info(" === Difference between current time and tLog time : " + diff);

			if (diff < EXPIRY_TIME) {

				if (merchantId != null) {
					String id = merchantId;
					Customer customer = customerService.findCustomerByEmailAndMerchantId(email, Integer.valueOf(id));
					if (customer != null
							&& userId.equalsIgnoreCase(EncryptionDecryptionUtil.encryptString(customer.getId() + ""))) {
						customer.setPassword("");
						customer.setUserPage("user");
						map.put("Customer", customer);
						LOGGER.info("HomeController.setResetPassword() : END");

						return "resetGuestPassword";
					} else {
						LOGGER.info("HomeController.setResetPassword() : END");

						return "redirect" + ":https://www.foodkonnekt.com";
					}
				}
			} else {
				LOGGER.info("Link is expired");
				map.put("Customer", new Customer());
				LOGGER.info("HomeController.setResetPassword() : END");

				return "redirect:" + environment.getProperty("BASE_URL") + "/support";
			}
		} catch (Exception e) {
			LOGGER.info("HomeController.setResetPassword() : ERROR" + e);

			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
			}
			LOGGER.error("error: " + e.getMessage());
		}
		LOGGER.info("HomeController.setResetPassword() : END");
		return null;
	}
	
	@RequestMapping(value = "/getCustomerCodes", method = RequestMethod.GET)
	public @ResponseBody List<CustomerCode> getCustomerCodes(@RequestParam(value = "customerId") Integer customerId,
			HttpServletRequest request){
		LOGGER.info("HomeController.getCustomerCodes() : START");
		
		List<CustomerCode> custCodes = new ArrayList<CustomerCode>();
		HttpSession session = request.getSession();
		if (session != null) {
			Merchant merchant = (Merchant) session.getAttribute("merchant");
			String timeZone=(merchant!=null && merchant.getTimeZone()!=null && merchant.getTimeZone().getTimeZoneCode()!=null) 
					? merchant.getTimeZone().getTimeZoneCode() :"America/Chicago";
			if(merchant != null && customerId!=null) {
			String today = DateUtil.getCurrentDateYYYYMMDDForTimeZone(timeZone);
			custCodes = customerCodeRepository.findByCustomerIdAndMerchantIdAndDate(customerId,merchant.getId(),today,true);
		    for(CustomerCode customercode : custCodes) {
		    	customercode.setCustomer(null);
		    }
			}
		}
		return custCodes;
	}
}
