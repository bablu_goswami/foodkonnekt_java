package com.foodkonnekt.contoller;

import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.foodkonnekt.service.OrderDiscountService;
import com.foodkonnekt.service.OrderService;

@Controller
public class A2aApisController {

	@Autowired
	private OrderService orderService;
	
	@Autowired
	private OrderDiscountService orderDiscountService;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(A2aApisController.class);
	/**
	 * Gets the avg order value by merchant U id.
	 *
	 * @param fromDate the from date
	 * @param toDate the to date
	 * @param locationUId the location U id
	 * @return the avg order value by merchant U id
	 */
	@RequestMapping(value="avgOrderValueByLocationUid", method = RequestMethod.GET)
	@ResponseBody
	public Map<String,Object> getAvgOrderValueByMerchantUId(@RequestParam(required = false) String fromDate, @RequestParam(required = false) String toDate,
            @RequestParam(required = false) String locationUId){
		LOGGER.info("===============  A2aApisController : Inside getAvgOrderValueByMerchantUId :: Start  ============= ");
		LOGGER.info(" ===locationUid : "+locationUId+" fromDate : "+fromDate+" toDate : "+toDate);

		Map<String,Object> responseMap = new HashMap<String, Object>();
		Double avgOrderValue = 0d;
		try {
			avgOrderValue = orderService.avgOrderValueByMerchantUId(locationUId, fromDate, toDate);
		} catch (Exception e) {
			LOGGER.error("===============  A2aApisController : Inside getAvgOrderValueByMerchantUId :: Exception  ============= " + e);

			LOGGER.error("error: " + e.getMessage());
		}
		responseMap.put("avgOrderValue", avgOrderValue!=null?avgOrderValue:0);
		LOGGER.info("===============  A2aApisController : Inside getAvgOrderValueByMerchantUId :: End  ============= ");

		return responseMap;
	}
	
	/**
	 * Gets the most popular menu item for foodkonnekt.
	 *
	 * @param fromDate the from date
	 * @param toDate the to date
	 * @param locationUId the location U id
	 * @return the most popular menu item for foodkonnekt
	 */
	@RequestMapping(value="mostPopularMenuItemForFoodkonnekt", method = RequestMethod.GET)
	@ResponseBody
	public Map<String,Object> getmostPopularMenuItemForFoodkonnekt(@RequestParam(required = false) String fromDate, @RequestParam(required = false) String toDate,
            @RequestParam(required = false) String locationUId){
		LOGGER.info("===============  A2aApisController : Inside getmostPopularMenuItemForFoodkonnekt :: Start  ============= ");
		LOGGER.info(" ===locationUid : "+locationUId+" fromDate : "+fromDate+" toDate : "+toDate);

		Map<String,Object> responseMap = new HashMap<String, Object>();
		String popularItemName = "";
		try {
			popularItemName = orderService.mostPopularMenuItemForFoodkonnekt(locationUId, fromDate, toDate);
			
		} catch (Exception e) {
			LOGGER.error("===============  A2aApisController : Inside getmostPopularMenuItemForFoodkonnekt :: Exception  ============= " + e);

			LOGGER.error("error: " + e.getMessage());
		}
		responseMap.put("popularItemName", popularItemName);
		LOGGER.info("===============  A2aApisController : Inside getmostPopularMenuItemForFoodkonnekt :: End  ============= ");

		return responseMap;
	}
	
	@RequestMapping(value="salesReportOfFoodkonnekt", method = RequestMethod.GET)
	@ResponseBody
	public Map<String,Object>salesReportOfFoodkonnekt( @RequestParam(required = false) String merchantUid,@RequestParam(required = false) String fromDate, @RequestParam(required = false) String toDate
           ){
		LOGGER.info("===============  A2aApisController : Inside salesReportOfFoodkonnekt :: Start  ============= ");
		LOGGER.info(" ===merchantUid : "+merchantUid+" fromDate : "+fromDate+" toDate : "+toDate);

		LOGGER.info("===============  A2aApisController : Inside salesReportOfFoodkonnekt :: End  ============= ");

		return orderService.getSalesReportOfFoodkonnekt(merchantUid, fromDate, toDate);
	}
	
	@RequestMapping(value="offersWithHighestRevenue", method = RequestMethod.GET)
	@ResponseBody
	public Map<String,Object>getOffersWithHighestRevenue( @RequestParam(required = false) String merchantUid,@RequestParam(required = false) String fromDate, @RequestParam(required = false) String toDate
           ){
		LOGGER.info("===============  A2aApisController : Inside getOffersWithHighestRevenue :: Start  ============= ");
		LOGGER.info(" ===merchantUid : "+merchantUid+" fromDate : "+fromDate+" toDate : "+toDate);

		LOGGER.info("===============  A2aApisController : Inside getOffersWithHighestRevenue :: End  ============= ");

		return orderDiscountService.getOfferWithHighestRevenue(merchantUid, fromDate, toDate);
	}
	
	@RequestMapping(value="getOrderNameAndCount", method = RequestMethod.GET)
	@ResponseBody
	public Map<String,Object> getOrderNameAndCount(@RequestParam(required = false) String merchantUid,@RequestParam(required = false) String fromDate, @RequestParam(required = false) String toDate ){
		Map<String,Object> popularItemName =  new HashMap<String, Object>();
		try {
			LOGGER.info("===============  A2aApisController : Inside getOrderNameAndCount :: Start  ============= ");
			LOGGER.info(" ===merchantUid : "+merchantUid+" fromDate : "+fromDate+" toDate : "+toDate);

			popularItemName =orderService.getOrderNameAndCount(merchantUid, fromDate, toDate);
		} catch (Exception e) {
			LOGGER.error("===============  A2aApisController : Inside getOrderNameAndCount :: Exception  ============= " + e);

			LOGGER.error("error: " + e.getMessage());
		}
		LOGGER.info("===============  A2aApisController : Inside getOrderNameAndCount :: End  ============= ");

		return popularItemName;
	}
	
	@RequestMapping(value="getTopMenuReport", method = RequestMethod.GET)
	@ResponseBody
	public Map<String,Object> getTopMenuReport(@RequestParam(required = false) String locationUId,@RequestParam(required = false) String fromDate, @RequestParam(required = false) String toDate ){
		Map<String,Object> popularItemName =  new HashMap<String, Object>();
		try {
			LOGGER.info("===============  A2aApisController : Inside getTopMenuReport :: Start  ============= ");
			LOGGER.info(" ===locationUid : "+locationUId+" fromDate : "+fromDate+" toDate : "+toDate);

			popularItemName =orderService.getTopMenuReport(locationUId, fromDate, toDate);
		} catch (Exception e) {
			LOGGER.error("===============  A2aApisController : Inside getTopMenuReport :: Exception  ============= " + e);

			LOGGER.error("error: " + e.getMessage());
		}
		LOGGER.info("===============  A2aApisController : Inside getTopMenuReport :: End  ============= ");

		return popularItemName;
	}

	
}
