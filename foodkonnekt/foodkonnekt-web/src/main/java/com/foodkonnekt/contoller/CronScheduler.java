package com.foodkonnekt.contoller;
import java.io.File;
import java.io.FileWriter;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.http.client.methods.HttpPost;
import org.json.JSONObject;
import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.csvreader.CsvWriter;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.foodkonnekt.model.Customer;
import com.foodkonnekt.model.GranburyOrders;
import com.foodkonnekt.model.GranburySolution;
import com.foodkonnekt.model.Item;
import com.foodkonnekt.model.Merchant;
import com.foodkonnekt.model.MerchantConfiguration;
import com.foodkonnekt.model.MerchantNotificationApp;
import com.foodkonnekt.model.ModifierGroup;
import com.foodkonnekt.model.ModifierModifierGroupDto;
import com.foodkonnekt.model.Modifiers;
import com.foodkonnekt.model.NotificationAppStatus;
import com.foodkonnekt.model.OpeningClosingDay;
import com.foodkonnekt.model.OpeningClosingTime;
import com.foodkonnekt.model.OrderItem;
import com.foodkonnekt.model.OrderItemModifier;
import com.foodkonnekt.model.OrderNotification;
import com.foodkonnekt.model.OrderR;
import com.foodkonnekt.repository.CustomerFeedbackRepository;
import com.foodkonnekt.repository.CustomerrRepository;
import com.foodkonnekt.repository.GranburyOrdersRepository;
import com.foodkonnekt.repository.GranburySolutionRepository;
import com.foodkonnekt.repository.ItemmRepository;
import com.foodkonnekt.repository.KritiqAnalyticsRepository;
import com.foodkonnekt.repository.MerchantConfigurationRepository;
import com.foodkonnekt.repository.MerchantRepository;
import com.foodkonnekt.repository.ModifierGroupRepository;
import com.foodkonnekt.repository.ModifierModifierGroupRepository;
import com.foodkonnekt.repository.ModifiersRepository;
import com.foodkonnekt.repository.NotificationAppStatusRepository;
import com.foodkonnekt.repository.NotificationMethodRepository;
import com.foodkonnekt.repository.OpeningClosingDayRepository;
import com.foodkonnekt.repository.OpeningClosingTimeRepository;
import com.foodkonnekt.repository.OrderDiscountRepository;
import com.foodkonnekt.repository.OrderItemModifierRepository;
import com.foodkonnekt.repository.OrderItemRepository;
import com.foodkonnekt.repository.OrderRepository;
import com.foodkonnekt.repository.TimeZoneRepository;
import com.foodkonnekt.repository.VendorRepository;
import com.foodkonnekt.service.BusinessService;
import com.foodkonnekt.service.ImportExcelService;
import com.foodkonnekt.service.MerchantNotificationAppService;
import com.foodkonnekt.service.MerchantService;
import com.foodkonnekt.service.NotificationAppStatusService;
import com.foodkonnekt.service.OrderNotificationService;
import com.foodkonnekt.service.OrderService;
import com.foodkonnekt.util.DateUtil;
import com.foodkonnekt.util.EncryptionDecryptionUtil;
import com.foodkonnekt.util.IConstant;
import com.foodkonnekt.util.MailSendUtil;
import com.foodkonnekt.util.ProducerUtil;
import com.foodkonnekt.util.ShortCodeUtil;

@Component
@Controller
@EnableScheduling
public class CronScheduler {

	private static final Logger LOGGER= LoggerFactory.getLogger(CronScheduler.class);
	
	@Autowired
    private Environment environment;
	
    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private OrderItemRepository orderItemRepository;

    @Autowired
    private OrderItemModifierRepository itemModifierRepository;

    @Autowired
    private MerchantRepository merchantRepository;
    
    @Autowired
    private CustomerrRepository customerRepository ;
    
    @Autowired
    private KritiqAnalyticsRepository kritiqAnalyticsRepository;
    
    @Autowired
    private CustomerFeedbackRepository customerFeedbackRepository;
    
    @Autowired
    private ItemmRepository itemmRepository;
    
    @Autowired
    private OrderItemModifierRepository orderItemModifierRepository;
    
    @Autowired
    private ModifiersRepository modifiersRepository;
    
    @Autowired
    private ModifierModifierGroupRepository modifierModifierGroupRepository;
    
    @Autowired
    private ModifierGroupRepository modifierGroupRepository;
    
    @Autowired
    private OrderRepository orderRepostiroy;
    
    @Autowired
    private MerchantService merchantService;
  
    @Autowired
	  private NotificationMethodRepository notificationMethodRepository;
  
    @Autowired
    private OrderDiscountRepository orderDiscountRepository;
    
    @Autowired
    private OrderService orderService;
   
    @Autowired
    private OpeningClosingDayRepository openingCosingDayRepository;
    
    @Autowired
    private OpeningClosingTimeRepository openingClosingTimeRepository;
    
    @Autowired
    private TimeZoneRepository timeZoneRepository;
    
    @Autowired
    private MerchantNotificationAppService merchantNotificationAppService;
    
    @Autowired
    private NotificationAppStatusService notificationAppStatusService;
    
    @Autowired
    private NotificationAppStatusRepository notificationAppStatusRepository;
    
    @Autowired
    private BusinessService buisnessService;
    
    @Autowired
    private GranburySolutionRepository granburySolutionRepository;
    
    @Autowired
    private GranburyOrdersRepository granburyOrdersRepository;
    
    @Autowired 
    private ImportExcelService importExcelService;
    
    @Autowired 
    private MailSendUtil mailSendUtil;
    
    @Autowired
    private MerchantConfigurationRepository merchantConfigurationRepository;
    
    @Autowired
	private OrderNotificationService orderNotificationService;
    
    public static Integer merchantId;
    
    public static OrderRepository mermoryOrderRepository;
    public static OrderItemRepository memoryOrderItemRepository;
    public static OrderItemModifierRepository memoryItemModifierRepository;
    public static MerchantRepository memorymerchantRepository;
    //public static MailSendUtil memoryMailSendUtil;
    public static CustomerrRepository mermoryCustomerRepository;
    public static KritiqAnalyticsRepository memoryKritiqAnalyticsRepository;
    public static CustomerFeedbackRepository memoryCustomerFeedbackRepository;
    public static ItemmRepository memoryItemRepository;
    public static OrderItemModifierRepository memoryOrderItemModifierRepository;
    public static ModifiersRepository memoryModifierRepository;
    public static ModifierModifierGroupRepository memoryModifierModifierGroupRepository;
    public static ModifierGroupRepository memoryModifierGroupRepository;
    public static OrderRepository memoryOrderRepository;
    
    public static NotificationMethodRepository schedularNotificationMethodRepository;
    public static OrderRepository schedularOrderRepository;
    public static OrderService schedularOrderService;
    public static OrderItemRepository schedularOrderItemRepository;
	  public static OrderItemModifierRepository schedularOrderItemModifierRepository;

	  public static OrderDiscountRepository schedularOrderDiscountRepository;
	  
	  public static VendorRepository schedularVendorRepository;
	  public static MerchantConfigurationRepository schedularMerchantConfigurationRepository;
   /* @PostConstruct
    public void updateLatLong() throws Exception {
        mermoryOrderRepository = orderRepository;
        memoryOrderItemRepository = orderItemRepository;
        memoryItemModifierRepository = itemModifierRepository;
        memorymerchantRepository = merchantRepository;

        JobKey jobKeyA = new JobKey("futureOrder", "group1");
        JobDetail futureOrder = JobBuilder.newJob(FutureOrder.class).withIdentity(jobKeyA).build();

        Trigger trigger1 = TriggerBuilder.newTrigger().withIdentity("dummyTriggerName1", "group1")
                        .withSchedule(CronScheduleBuilder.cronSchedule("2 * * * * ?")).build();

        Scheduler scheduler = new StdSchedulerFactory().getScheduler();

        scheduler.start();
        scheduler.scheduleJob(futureOrder, trigger1);

    }

    
    @SuppressWarnings("restriction")
	@PostConstruct
    public void sendMailMerchant() throws Exception {
    	
        mermoryOrderRepository = orderRepository;
        memoryOrderItemRepository = orderItemRepository;
        memoryItemModifierRepository = itemModifierRepository;
        memorymerchantRepository = merchantRepository;
        JobKey jobKeyA = new JobKey("futureOrder", "group2");
        JobDetail futureOrder = JobBuilder.newJob(TestCronSchedular.class).withIdentity(jobKeyA).build();
        // Trigger will fire on 23:59:59 PM daily
        Trigger trigger1 = TriggerBuilder.newTrigger().withIdentity("dummyTriggerName2", "group2")
                        .withSchedule(CronScheduleBuilder.cronSchedule("00 59 23 * * ?")).build();   
        Trigger trigger1 = TriggerBuilder.newTrigger().withIdentity("dummyTriggerName2", "group2")
                .withSchedule(CronScheduleBuilder.cronSchedule("00 59 23 * * ?")).build();   
        Scheduler scheduler = new StdSchedulerFactory().getScheduler();
        scheduler.start();
        scheduler.scheduleJob(futureOrder, trigger1);
    }
    */
    @SuppressWarnings("restriction")
   	@PostConstruct
       public void sendMailMerchant() throws Exception {
       	
           mermoryOrderRepository = orderRepository;
           memoryOrderItemRepository = orderItemRepository;
           memoryItemModifierRepository = itemModifierRepository;
           memorymerchantRepository = merchantRepository;
           mermoryCustomerRepository = customerRepository;
           memoryKritiqAnalyticsRepository = kritiqAnalyticsRepository;
           memoryCustomerFeedbackRepository = customerFeedbackRepository;
           
           JobKey jobKeyA = new JobKey("futureOrder", "group2");
           JobDetail futureOrder = JobBuilder.newJob(TestCronSchedular.class).withIdentity(jobKeyA).build();
           // Trigger will fire on 23:59:59 PM daily
           Trigger trigger1 = TriggerBuilder.newTrigger().withIdentity("dummyTriggerName2", "group2")
                           .withSchedule(CronScheduleBuilder.cronSchedule("00 59 23 * * ?")).build();   
           /*Trigger trigger1 = TriggerBuilder.newTrigger().withIdentity("dummyTriggerName2", "group2")
                   .withSchedule(CronScheduleBuilder.cronSchedule("2 * * * * ?")).build();*/   
           
           Scheduler scheduler = new StdSchedulerFactory().getScheduler();
           scheduler.start();
           scheduler.scheduleJob(futureOrder, trigger1);
           LOGGER.info("CRON SCHEDULAR START WORKING");
       }
    
    
//    @SuppressWarnings("restriction")
//   	@PostConstruct
//       public void sendReport()  {
//       	try{
//           mermoryOrderRepository = orderRepository;
//           memoryOrderItemRepository = orderItemRepository;
//           memorymerchantRepository = merchantRepository;
//           memoryItemRepository = itemmRepository;
//           memoryOrderItemModifierRepository = orderItemModifierRepository;
//           memoryModifierRepository = modifiersRepository;
//           memoryModifierModifierGroupRepository = modifierModifierGroupRepository;
//           memoryModifierGroupRepository= modifierGroupRepository;
//           memoryOrderRepository = orderRepository;
//           
//           JobKey jobKeyA = new JobKey("monthlyOrderReport", "group3");
//           JobDetail futureOrder = JobBuilder.newJob(DaySchedular.class).withIdentity(jobKeyA).build();
//           
//           // Trigger will fire in every 2 hrs
//           Trigger trigger1 = TriggerBuilder.newTrigger().withIdentity("dummyTriggerName3", "group3")
//                           .withSchedule(CronScheduleBuilder.cronSchedule("*/50 * * * * ?")).build(); 
//           
//           Scheduler scheduler = new StdSchedulerFactory().getScheduler();
//           scheduler.start();
//           scheduler.scheduleJob(futureOrder, trigger1);
//           MailSendUtil.webhookMail("CronScheduler", "DAY SCHEDULAR START WORKING");
//           LOGGER.info("DAY SCHEDULAR START WORKING");
//       	}catch(Throwable e){
//       		LOGGER.error("error: " + e.getMessage());
//       		MailSendUtil.webhookMail("CronScheduler Error", "Error"+e);
//       	}
//       }
  @RequestMapping(value="/KidsCateringScheduler",method = RequestMethod.GET)
  @ResponseBody
    @Scheduled(cron = "0 00 13 * * *")
//    @Scheduled(cron = "0 00 14 * * *")
//   @Scheduled(cron = "*/50 * * * * ?")
    public void reportCurrentTime() {
        LOGGER.info("testing scheduler ");
        MailSendUtil.webhookMail("@Scheduled", "testing start",environment);
        mermoryOrderRepository = orderRepository;
        memoryItemRepository = itemmRepository;
        memoryOrderItemRepository = orderItemRepository;
        memoryItemModifierRepository = itemModifierRepository;
        memorymerchantRepository = merchantRepository;
        mermoryCustomerRepository = customerRepository;
        memoryKritiqAnalyticsRepository = kritiqAnalyticsRepository;
        memoryCustomerFeedbackRepository = customerFeedbackRepository;
      mermoryOrderRepository = orderRepository;
      memoryOrderItemRepository = orderItemRepository;
      memorymerchantRepository = merchantRepository;
      memoryItemRepository = itemmRepository;
      memoryOrderItemModifierRepository = orderItemModifierRepository;
      memoryModifierRepository = modifiersRepository;
      memoryModifierModifierGroupRepository = modifierModifierGroupRepository;
      memoryModifierGroupRepository= modifierGroupRepository;
      
        execute();
        MailSendUtil.webhookMail("@Scheduled", "testing end",environment);
    }
    
    
  @RequestMapping(value="/futureOrderMailSchedular",method = RequestMethod.GET)
  @ResponseBody
    //@Scheduled(cron = "0 0 0 * * ?")
//      @Scheduled(cron = "0 0 5 * * ?")
    @Scheduled(cron = "0 */30 * ? * *")
   // @Scheduled(cron = "*/50 * * * * ?")
    public void futureOrderMailSchedular() throws ParseException{
 	 LOGGER.info("-----------------CronSchedular : Inside futureOrderMailSchedular :: Start---------------------");
	
 	 LOGGER.info("CronSchedular :: futureOrderMailSchedular : currentDate "+new Date());
 	
 	schedularNotificationMethodRepository=notificationMethodRepository;
 	schedularOrderRepository=orderRepository;
 	schedularOrderService=orderService;
 	schedularOrderItemRepository=orderItemRepository;
 	schedularOrderItemModifierRepository=orderItemModifierRepository;
 	schedularOrderDiscountRepository= orderDiscountRepository;
 	schedularMerchantConfigurationRepository=merchantConfigurationRepository;
 	memorymerchantRepository=merchantRepository;
 	
 	
 	Date date = new Date();
 	String toDay;
 	
 	SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
 	
 	
   	Calendar cal = Calendar. getInstance();
   	cal.add(Calendar.DATE, -1);
   	String currentDate = simpleDateFormat.format(cal.getTime());
 	List<Integer> merchantsList = orderService.findMerchantsTodayDate(currentDate);
 	
 	if(merchantsList!=null && !merchantsList.isEmpty()){
 		for (Integer merchantList : merchantsList) {
 			
 			String timeZoneCode=timeZoneRepository.findTimeZoneCodeByMerchentId(merchantList);
 			if(timeZoneCode==null || timeZoneCode.isEmpty())
 				timeZoneCode="America/Chicago";
 			 date=DateUtil.getCurrentDateForTimeZonee(timeZoneCode);
 			 toDay=DateUtil.getCurrentDayForTimeZonee(timeZoneCode);
 			 if(toDay==null || toDay.isEmpty())
 				toDay=DateUtil.getCurrentDay();
 			OpeningClosingDay openingClosingDay = openingCosingDayRepository.findByDayAndMerchantId(toDay, merchantList);
				if(openingClosingDay != null && openingClosingDay.getId() != null){
					LOGGER.info(" === openingClosingDay.getId() : " + openingClosingDay.getId());

					List<OpeningClosingTime> openingClosingTimes = openingClosingTimeRepository.findByOpeningClosingDayId(openingClosingDay.getId());
					if(openingClosingTimes != null && !openingClosingTimes.isEmpty()){
						
						
						LOGGER.info("CronSchedular :: futureOrderMailSchedular : timeZoneCode "+timeZoneCode);
						LOGGER.info("CronSchedular :: futureOrderMailSchedular : merchantId "+merchantList);
						
						for (OpeningClosingTime openingClosingTime2 : openingClosingTimes) {
							
							String fullfillDate = openingClosingTime2.getStartTime().toString();
							String FuulfilledDateArray[]=fullfillDate.split(":");
							LOGGER.info("start time is================"+fullfillDate+"merchantID======"+merchantList);
							
						    String currentTme=DateUtil.getCurrentTimeForTimeZone(timeZoneCode);
						    String currentTImeArray[]=currentTme.split(":");
						    LOGGER.info("CronSchedular :: futureOrderMailSchedular : currentTme "+currentTme);
						    LOGGER.info("CronSchedular :: futureOrderMailSchedular : fullfillDate "+fullfillDate);
						   
						    
							if(FuulfilledDateArray[0].equals(currentTImeArray[0]) && ((Integer.parseInt(FuulfilledDateArray[1])+28)>=(Integer.parseInt((currentTImeArray[1]))))){
							LOGGER.info("CronSchedular :: futureOrderMailSchedular : schedular starts now");
							final Integer merchantId=merchantList;
							new Thread(new Runnable() {
								public void run() {
									try {
										LOGGER.info("CronSchedular :: futureOrderMailSchedular : call TestSchedular for merchantId :");
										TestSchedular.execute(merchantId,environment);
									} catch (Exception e) {
										LOGGER.error("error: " + e.getMessage());
										LOGGER.info("CronSchedular :: futureOrderMailSchedular : Exception"+e);
										LOGGER.info(" Exception at future order" + e);
									}

								}
							}).start();
							
					}
			 }}}}
 	  }
   }

	
	public void execute()  {
		try{
		MailSendUtil.webhookMail("KidsCateringScheduler", "schedular start",environment);
		LOGGER.info("=KidsCateringScheduler schedular runs");
		String toDay = DateUtil.getCurrentDay();
		//String today = LocalDate.now().getDayOfWeek().name();
		//DateTimeFormatter dtf = DateTimeFormatter.ofPattern("MM-dd-yyyy");
		//LocalDate localDate = LocalDate.now();

		String toDayAndDate = toDay.substring(0, 1).toUpperCase()
				+ toDay.substring(1).toLowerCase() + "_"
				+ DateUtil.findCurrentDate();
		
		if(toDayAndDate !=null){
			writeExcelFile(toDayAndDate);
		}
		}catch(Throwable t){
			t.printStackTrace();
			MailSendUtil.webhookMail("DaySchedular Error", "Error"+t,environment);
		}
	}
	public static void main(String[] args) {
		String toDay = DateUtil.getCurrentDay();
		
		LOGGER.info("today = "+toDay.substring(0, 1).toUpperCase()
				+ toDay.substring(1).toLowerCase() + "_"
						+ DateUtil.findCurrentDate());
		
		//String outputFile = environment.getProperty("FILEPATH") + "daily_report.csv";
		Merchant merchant=null;
		//MailSendUtil.sendMonthlyReportMail(outputFile,merchant);
		LOGGER.info("sent");
		
	}
	public void writeExcelFile(String toDayAndDate) {
		String outputFile =null;
		try {
			LOGGER.info("===============  CronSchedular : Inside writeExcelFile :: Start  ============= ");
LOGGER.info(" ===toDayAndDate : "+toDayAndDate);
			String emailId= null;
			String vendorEmail= null;
			
			List<Item> items = itemmRepository.findByName(toDayAndDate);
			if(items !=null && items.size() > 0){
				for (Item item : items) {
					boolean newRow=true;
					outputFile = environment.getProperty("FILEPATH") + item.getMerchant().getName()+"_report.csv";
					boolean alreadyExists = new File(outputFile).exists();
					
					CsvWriter csvOutput = new CsvWriter(
					new FileWriter(outputFile, true), ',');
					
			if (!alreadyExists) {
				csvOutput.write("Customer Name");
				csvOutput.write("Created On");
				csvOutput.write("Order Total");
				csvOutput.write("Order Type");
				csvOutput.write("Order Notes");
				csvOutput.write("Customer Email");
				csvOutput.write("Customer Phone");
				csvOutput.write("Payment Type");
				csvOutput.write("Day/Date");
				csvOutput.write("Item Option");
				csvOutput.write("Order Item");
				csvOutput.write("Quantity");
				csvOutput.endRecord();
			}
					Modifiers modifiers=null;
					Merchant merchant =null;
					if(item.getMerchant()!=null){
						merchant = item.getMerchant();
					}
					LOGGER.info(" === item.getId() : " + item.getId());

					List<OrderItem> orderItems = orderItemRepository.findByItemId(item.getId());
					if(orderItems !=null && orderItems.size()>0){
					
						for (OrderItem orderItem : orderItems) {
							if(orderItem.getOrder()!=null && orderItem.getOrder().getId()!=null){
								LOGGER.info(" ===orderItem.getOrder().getId() : "+orderItem.getOrder().getId());
								OrderR orderR= orderRepository.findOne(orderItem.getOrder().getId());
								
								if(orderR.getCustomer()!=null && orderR.getCustomer().getId()!=null && orderR.getMerchant()!=null && 
										orderR.getMerchant().getId()!=null && (orderR.getIsDefaults()==1 || orderR.getIsDefaults()==0)){
									LOGGER.info(" === orderItem.getId() : " + orderItem.getId());

									List<OrderItemModifier> orderItemModifiers = orderItemModifierRepository
											.findByOrderItemId(orderItem.getId());
									
									if (orderItemModifiers != null && !orderItemModifiers.isEmpty()) {
										
										for (OrderItemModifier orderItemModifier : orderItemModifiers) {
											if (orderItemModifier.getModifiers() != null
													&& orderItemModifier.getModifiers().getId() != null) {
							
									if(orderR!=null){
										if (newRow && orderR.getCustomer() != null
												&& orderR.getCustomer().getFirstName() != null && orderR.getMerchant()!=null) {
											csvOutput.write(orderR.getCustomer().getFirstName().toString() );
										} else {
											csvOutput.write(" ");
										}
										
										if (newRow && orderR.getCreatedOn() != null) {
											csvOutput.write(orderR.getCreatedOn().toString().replace(" ", "_"));
										} else {
											csvOutput.write(" ");
										}

										if (newRow && orderR.getOrderPrice() != null) {
											csvOutput.write(orderR.getOrderPrice().toString());
										} else {
											csvOutput.write(" ");
										}
										
										if (newRow && orderR.getOrderType() != null) {
											csvOutput.write(orderR.getOrderType().toString());
										} else {
											csvOutput.write(" ");
										}

										if (newRow && orderR.getOrderNote() != null) {
											csvOutput.write(orderR.getOrderNote());
										} else {
											csvOutput.write(" ");
										}

										if (newRow && orderR.getCustomer() != null
												&& orderR.getCustomer().getEmailId() != null) {
											csvOutput.write(orderR.getCustomer().getEmailId());
										} else {
											csvOutput.write(" ");
										}

										if (newRow && orderR.getCustomer() != null
												&& orderR.getCustomer().getPhoneNumber() != null) {
											csvOutput.write(orderR.getCustomer().getPhoneNumber());
										} else {
											csvOutput.write(" ");
										}

										if (newRow && orderR.getPaymentMethod() != null) {
											csvOutput.write(orderR.getPaymentMethod());
										} else {
											csvOutput.write(" ");
										}
										
										if (item != null && item.getName() != null) {
											csvOutput.write(item.getName());
										} else {
											csvOutput.write(" ");
										}
									}
								}
								LOGGER.info(" === orderItemModifier.getModifiers().getId() : " + orderItemModifier.getModifiers().getId());

											modifiers = modifiersRepository.findOne(orderItemModifier.getModifiers().getId());
											
											List<ModifierModifierGroupDto> modifierModifierGroupDtos=modifierModifierGroupRepository.
													findByModifiersId(modifiers.getId());
											
											for (ModifierModifierGroupDto modifierModifierGroupDto : modifierModifierGroupDtos) {
												if(modifierModifierGroupDto.getModifierGroup()!=null &&
														modifierModifierGroupDto.getModifierGroup().getId() !=null){
													LOGGER.info(" === modifierModifierGroupDto.getModifierGroup().getId() : " + modifierModifierGroupDto.getModifierGroup().getId());

													ModifierGroup modifierGroup=modifierGroupRepository.findOne
															(modifierModifierGroupDto.getModifierGroup().getId());
													if(modifierGroup!=null && modifierGroup.getName()!=null){
														
														String [] groupName = modifierGroup.getName().split("_");
	 			    										
														String [] toDayAndDateSplit = toDayAndDate.split("_");
														
														String parseTodayDate = toDayAndDateSplit[1];
														
	 			    										String modifierGroupName = null;
	 			    										if(groupName!=null && groupName.length> 1) {
	 			    											modifierGroupName = groupName[1];
	 			    										 }
	 			    										if(modifierGroupName.equals(parseTodayDate)){
	 			    											csvOutput.write(modifierGroup.getName());
	 			    										}
													}else{
														csvOutput.write(" ");
													}
												}
											}
											
											if(modifiers.getName()!=null){
												csvOutput.write(modifiers.getName());
											}else{
												csvOutput.write(" ");
											}
											
											if (orderItemModifier.getQuantity() != null) {
												csvOutput.write(orderItemModifier.getQuantity().toString());
											} else {
												csvOutput.write(" ");
											}
											newRow = false;
											csvOutput.write(" ");
											csvOutput.endRecord();
										}
									}
									newRow = true;
									csvOutput.write(" ");
									csvOutput.endRecord();
								}
							}
						}
						csvOutput.close();
						MailSendUtil.sendMonthlyReportMail(outputFile, merchant,environment);
						
						File file=new File(outputFile);
						file.delete();
						LOGGER.info("File Delete Successfully");
					
					}else{
						LOGGER.info("No Orders found");
					}
				}
			}else{
				LOGGER.info("No item found");
			}
		} catch (Throwable e) {
			LOGGER.error("===============  CronSchedular: Inside writeExcelFile :: Exception  ============= " + e);

			LOGGER.error("error: " + e.getMessage());
			MailSendUtil.webhookMail("DaySchedular Error", "Error"+e,environment);
		}
	}
	
	/*public static void main(String[] args) {
		
		String today = LocalDate.now().getDayOfWeek().name();
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("MM-dd-yyyy");
		LocalDate localDate = LocalDate.now();

		String toDayAndDate = today.substring(0, 1).toUpperCase()
				+ today.substring(1).toLowerCase() + "_"
				+ dtf.format(localDate);
		LOGGER.info(toDayAndDate);
	}*/


	  @Scheduled(cron = "0 0 14 * * MON")
//	  @Scheduled(cron = "0 0 15 * * MON")
	 // @Scheduled(cron = "0 0 */1 * * ?")
	 // @Scheduled(cron = "*/50 * * * * ?")
	  public void getWeeklyReport() {
			try {
				LOGGER.info("Weekly Order Report schedular start");
					DateFormat dateFormat = new SimpleDateFormat(IConstant.YYYYMMDD);
					
					Date today = new Date();
					Calendar cal = new GregorianCalendar();
					cal.setTime(today);
					cal.add(Calendar.DAY_OF_MONTH, -7);
					Date today7 = cal.getTime();
					
					String endDate = dateFormat.format(today);
					String startDate = dateFormat.format(today7);
					
					List<Merchant> merchantData = merchantService.getWeeklyReportOfMerchant(startDate, endDate);
					LOGGER.info("merchantData size "+merchantData.size());
					
					if(merchantData.size() > 0){
						 MailSendUtil.sendWeeklyTotalOrders(merchantData,startDate,endDate,environment);
						 LOGGER.info("Weekly Order Report schedular stop");
					}
			} catch (Exception e) {
				LOGGER.error("===============  CronScheduler : Inside Weekly Order Report schedular :: Exception  ============= " + e);

				LOGGER.error("error: " + e.getMessage());
			}
		}
	  
	  
	  @Scheduled(cron = "0 */2 * * * ?")
		public void NotificationAppStatus() {
			try{
			LOGGER.info("==============NotificationAppStatus schedular starts================");

			// find all merchants with their respective app id's from merchant_noti_app
			List<MerchantNotificationApp> listMerchantNotificationApp = merchantNotificationAppService.findAll();
			List<NotificationAppStatus> listNotificationAppStatus = null;

			if(listMerchantNotificationApp != null && !listMerchantNotificationApp.isEmpty()) {
				for (MerchantNotificationApp merchantNotificationApp : listMerchantNotificationApp) {
					if(merchantNotificationApp!=null && merchantNotificationApp.getMerchant()!=null && merchantNotificationApp.getMerchant().getId()!=null)
					{
					// finding merchant entries in noti_app_status using id's from merchant_noti_app
					LOGGER.info("==============NotificationAppStatus schedular merchant Id================"+merchantNotificationApp.getMerchant().getId());

				listNotificationAppStatus = notificationAppStatusService.findByMerchantid(merchantNotificationApp.getMerchant().getId());
		
		// get List of last updated rows
		 NotificationAppStatus notificationAppStatus = notificationAppStatusRepository.lastOccuranceOfMerchantId(merchantNotificationApp.getMerchant().getId());
		if (notificationAppStatus != null && notificationAppStatus.getId()!=null) {
					LOGGER.info("==============NotificationAppStatus schedular last occurence notified status ================"+notificationAppStatus.getIsNotified());
			    LOGGER.info("==============NotificationAppStatus last updated IsRunning() status  ================"+ notificationAppStatus.getIsRunning());

				if (notificationAppStatus.getIsRunning()!=null && notificationAppStatus.getIsRunning()==1) {
					boolean status = buisnessService.getOpenhoursStatus(notificationAppStatus.getMerchantid());
				    LOGGER.info("==============NotificationAppStatus buisness OpenhoursStatus ================"+ status);
						LOGGER.info(" status " + status);
						LOGGER.info("==============NotificationAppStatus last updated IsNotified() status  ================"+ notificationAppStatus.getIsNotified());
							 if(notificationAppStatus.getMerchantid()!=null){
									Merchant merchant = merchantRepository.findById(notificationAppStatus.getMerchantid());
									if(merchant!=null && merchant.getId()!=null && (notificationAppStatus.getIsActive()==null || notificationAppStatus.getIsActive()!= 1))
									{
										if(status == false) {
											LOGGER.info("==============NotificationAppStatus mail not send  ================"+merchant.getEmailId());
											LOGGER.info(" mail not send for merchant Id : " + notificationAppStatus.getMerchantid());
										}else
										{
										OrderNotification orderNotification=orderNotificationService.findMerchantId(merchant.getId());
										String marchantEmail=null;
										if(orderNotification!=null && orderNotification.getEmail()!=null && !orderNotification.getEmail().isEmpty())
										{
											marchantEmail=orderNotification.getEmail();
										}
										MailSendUtil.sendAppStatusNotificationFailedMail(merchant,marchantEmail,environment);
										LOGGER.info("==============NotificationAppStatus send mail to  ================"+merchant.getEmailId());
										LOGGER.info(" send mail for merchant Id : " + notificationAppStatus.getMerchantid());
										}
										LOGGER.info("==============NotificationAppStatus send mail to  ================"+merchant.getEmailId());
										LOGGER.info(" send mail for merchant Id : " + notificationAppStatus.getMerchantid());
										
										
										Date today = (merchant!=null && merchant.getTimeZone() !=null 
												&& merchant.getTimeZone().getTimeZoneCode()!=null) ? 
														DateUtil.getCurrentDateForTimeZonee(merchant.getTimeZone().getTimeZoneCode()) : new Date();
												
										notificationAppStatus.setIsNotified(1);
										notificationAppStatus.setIsRunning(1);
									    notificationAppStatus.setNotifieDate(today);
									    notificationAppStatusRepository.save(notificationAppStatus);
								 }
				    	    }
					} else {
						notificationAppStatus.setIsRunning(1);
						notificationAppStatusRepository.save(notificationAppStatus);
					} 
					for (NotificationAppStatus notificationAppStatusList : listNotificationAppStatus) {
						if ((notificationAppStatus.getMerchantid()).equals(notificationAppStatusList.getMerchantid())) {
							if (!(notificationAppStatusList.getId()).equals(notificationAppStatus.getId()) ) {
								LOGGER.info(" === notificationAppStatusList.getId() : " + notificationAppStatusList.getId());

								notificationAppStatusRepository.delete(notificationAppStatusList);
								}
						}
					} 
			}
		}
			}}
		LOGGER.info("==============NotificationAppStatus schedular ends================");
		
			}catch(Exception e){
			LOGGER.error("error: " + e.getMessage());
		LOGGER.info("==============NotificationAppStatus schedular exception================"+e);
			
		}
	  }
		@Scheduled(cron = "0 0 5 * * ?")
	 	 public void fileDelete() {
			try {
				LOGGER.info("==============Schedular fileDelete() : delete thermal printer and Clould printer : STRAT================");
				LOGGER.info("File Delete schedular start");

				File file = new File(environment.getProperty("THERMAL_RECEIPT"));      
				LOGGER.info("==============Schedular fileDelete() : ThermalPrinter pdf file path================"+environment.getProperty("THERMAL_RECEIPT"));
				LOGGER.info("==============Schedular fileDelete() : TClouldPrinter pdf file path================"+environment.getProperty("FAX_FILE_PATH"));
				File file1 = new File(environment.getProperty("FAX_FILE_PATH"));
			       String[] myFiles; 
			       String[] myFiles1; 
			           if(file.isDirectory() && file1.isDirectory()){
			               myFiles = file.list();
			               myFiles1 = file1.list();
			               for (int i=0; i<myFiles.length; i++) {
			                   File myFile = new File(file, myFiles[i]); 
			                   myFile.delete();
			               }
			               for (int j=0; j<myFiles1.length; j++){
			            	   File myFile1 = new File(file1, myFiles1[j]);
			            	   myFile1.delete();
			               }
			            }
			} catch (Exception e) {
				LOGGER.error("error: " + e.getMessage());
			}
			LOGGER.info("==============Schedular fileDelete() : delete thermal printer and Clould printer : END================");
		}
		
//		@Scheduled(cron = "0 0 */1 * * ?")
//		  public void orderAutoDeclineAfter24Hour()
//		  {
//			  LOGGER.info("==============Auto decline Schedular  starts================");
//			  SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
//			  Calendar calender=Calendar.getInstance();
//			  calender.add(Calendar.DATE, -1);
//			  String yesturdaydate =sdf.format(calender.getTime());
//			  if(yesturdaydate!=null)
//			  {
//				  LOGGER.info("orderAutoDeclineAfter24Hour :yesturdaydate : "+yesturdaydate);
//				  List<OrderR> orders=orderRepository.findOrdersByCreatedDate(yesturdaydate);
//				  if(orders!=null && orders.size()>0)
//				  {
//					  LOGGER.info("orderAutoDeclineAfter24Hour :orders.size : "+orders.size());
//					  for (OrderR orderR : orders) 
//					  {
//						  if(orderR!=null && orderR.getId()!=null && orderR.getCreatedOn()!=null && orderR.getMerchant()!=null)
//						  {
//							  Date date=new Date();
//							  if(orderR.getMerchant().getTimeZone()!=null && orderR.getMerchant().getTimeZone().getTimeZoneCode()!=null)
//							  {
//								  date= DateUtil.findCurrentTimeAccordingToTimeZoneCode(orderR.getMerchant().getTimeZone().getTimeZoneCode());
//							  }
//							  LOGGER.info("orderAutoDeclineAfter24Hour :orderRId : "+orderR.getId());
//							  LOGGER.info("orderAutoDeclineAfter24Hour :orderR.getCreatedOn() : "+orderR.getCreatedOn());
//							  Calendar createdDate=Calendar.getInstance();
//							  Calendar currentTime=Calendar.getInstance();
//							  createdDate.setTime(orderR.getCreatedOn());
//							  createdDate.add(Calendar.HOUR, 24);
//							  LOGGER.info("orderAutoDeclineAfter24Hour :created Date : "+createdDate.getTime());
//							  currentTime.set(Calendar.HOUR,date.getHours());
//							  currentTime.set(Calendar.MINUTE,date.getMinutes());
//							  currentTime.set(Calendar.SECOND,date.getSeconds());
//							  if((date.getHours()<12))
//								  currentTime.set(Calendar.AM_PM, 0); 
//							  else currentTime.set(Calendar.AM_PM, 1); 
//							  LOGGER.info("orderAutoDeclineAfter24Hour :current date : "+currentTime.getTime());
//							  if(createdDate.getTime().before(currentTime.getTime()))
//							  {
//								  LOGGER.info("orderAutoDeclineAfter24Hour :decline is in process : ");
//								  ProducerUtil.acceptAndCancelOrder(orderR.getId(), orderR.getOrderType(), "decline", 0);
//								  LOGGER.info("orderAutoDeclineAfter24Hour : orderR declined: "+orderR.getId());
//							  }else
//							  LOGGER.info("orderAutoDeclineAfter24Hour : orderR not declined: "+orderR.getId());
//						  }
//						
//					  }
//				  }else LOGGER.info("orderAutoDeclineAfter24Hour :no pendding future order found for that date: "+yesturdaydate);
//				  
//			  }
//			  
//		  }
		   @Scheduled(cron = "0 0/1 * * * ?")
		   public void autoOrderDeclineAfterFulfilTime()
		   {
			      LOGGER.info("autoOrderDeclineAfterFulfilTime : Start :");
			      SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
				  Calendar calender=Calendar.getInstance();
				  String fulfilledDate =sdf.format(calender.getTime());
				  List<OrderR> orders=orderRepository.findOrdersByFullFilleddate(fulfilledDate);
				  if(orders!=null && orders.size()>0)
				  {
					  LOGGER.info("autoOrderDeclineAfterFulfilTime :orders.size : "+orders.size());
					  for (OrderR orderR : orders) 
					  {
						  if(orderR!=null && orderR.getId()!=null && orderR.getFulfilled_on()!=null && orderR.getMerchant()!=null)
						  {
							
							  String timeZoneCode="America/Chicago";
							  if(orderR.getMerchant().getTimeZone()!=null && orderR.getMerchant().getTimeZone().getTimeZoneCode()!=null)
							  {
								  timeZoneCode=orderR.getMerchant().getTimeZone().getTimeZoneCode();
							  }
							  Date date= DateUtil.getCurrentDateForTimeZonee(timeZoneCode);
							  LOGGER.info("autoOrderDeclineAfterFulfilTime :orderRId : "+orderR.getId());
							  LOGGER.info("autoOrderDeclineAfterFulfilTime :orderR.getFulfilled_on() : "+orderR.getFulfilled_on());
							  Calendar createdDate=Calendar.getInstance();
							  Calendar currentTime=Calendar.getInstance();
							  currentTime.setTime(date);
							  createdDate.setTime(orderR.getFulfilled_on());
							  LOGGER.info("autoOrderDeclineAfterFulfilTime :getFulfilled Date : "+createdDate.getTime());
							  LOGGER.info("autoOrderDeclineAfterFulfilTime :current date : "+currentTime.getTime());
							  if(createdDate.getTime().before(currentTime.getTime()))
							  {
								  LOGGER.info("autoOrderDeclineAfterFulfilTime :decline is in process : ");
								  try {
									ProducerUtil.acceptAndDeclineOrder(orderR.getId(), orderR.getOrderType(),environment);
								} catch (UnsupportedEncodingException e) {
									// TODO Auto-generated catch block
									LOGGER.error("error: " + e.getMessage());
								}
								  LOGGER.info("autoOrderDeclineAfterFulfilTime : orderR declined: "+orderR.getId());
							  }else
							  LOGGER.info("autoOrderDeclineAfterFulfilTime : orderR not declined: "+orderR.getId());
						  }
					  }
				  }else LOGGER.info("autoOrderDeclineAfterFulfilTime :no pendding future order found for that date: "+fulfilledDate);
		   }
	
		 //  @Scheduled(cron = "0 0/5 * * * ?")
		   @RequestMapping(value = "/readGranburyData", method = RequestMethod.GET)
		   @ResponseBody
		   public void readGranburyData(@RequestParam(required=false) String toDay)
		   {
			   LOGGER.info("readGranburyData : Start :");
			   try{
				   List<GranburySolution>  ganburySolution=  granburySolutionRepository.findAll();
				   if(ganburySolution!=null )
				   {
					   LOGGER.info("readGranburyData : merchant informataion count :"+ganburySolution.size());
					   for (GranburySolution granburySolution : ganburySolution) {
						LOGGER.info("Uid :: "+granburySolution.getLocatinUid());
						LOGGER.info("readGranburyData : merchant LocationUid  :"+granburySolution.getLocatinUid());
						if(granburySolution.getAutorizationToken()!=null && granburySolution.getLocatinUid()!=null && granburySolution.getMerchantId()!=null)
						{	
							Merchant merchant=merchantRepository.findById(granburySolution.getMerchantId());
							if(merchant!=null && merchant.getMerchantUid()!=null)
							{
								Date today = (merchant.getTimeZone() !=null 
										&& merchant.getTimeZone().getTimeZoneCode()!=null) ? 
												DateUtil.getCurrentDateForTimeZonee(merchant.getTimeZone().getTimeZoneCode()) : new Date();
								Merchant merchant2=new Merchant();
								merchant2.setId(merchant.getId());
								merchant2.setMerchantUid(merchant.getMerchantUid());
								merchant2.setActiveCustomerFeedback(merchant.getActiveCustomerFeedback());
								String date="";
							LOGGER.info("readGranburyData : merchant AutorizationToken  :"+granburySolution.getAutorizationToken());
							if(toDay==null || toDay.isEmpty())
							date=DateUtil.getYYYYMMDD(today);
							else date=toDay;
							
							// date=DateUtil.getYesterday();
							LOGGER.info("readGranburyData : Fetch Data for Date  :"+date);
					     StringBuffer response= ProducerUtil.ticketStreamWithDate(granburySolution.getLocatinUid(),granburySolution.getAutorizationToken(),date);
					//		StringBuffer response= ProducerUtil.ticketStream(granburySolution.getLocatinUid(),granburySolution.getAutorizationToken());
						  if(response!=null)
						  {
							 // LOGGER.info("readGranburyData :response Of Api ticketStreamWithDate  :"+response);
						   JsonNode node = new ObjectMapper().readTree(response.toString());
					       Iterator<JsonNode> document = node.findPath("documents").iterator();
					        while(document.hasNext()){
					        	OrderR orderR=new OrderR();
					        	String orderType="ONLINE";
					        	JsonNode jsonNode=document.next();
					        	Customer customer=new Customer();
					        	JSONObject json = new JSONObject();
					        	JsonNode orderMessage=jsonNode.findPath("orderMessage");
					        	String orderId=jsonNode.findPath("orderId").toString().replace("\"", "");
					        	String orderItem=jsonNode.findPath("orderItems").findPath("itemName").toString();
					        	if(orderItem!=null)
					        	orderR.setOrderName(orderItem);
					        	customer.setMerchantt(merchant2);
					        	if(orderMessage.toString().contains("messageName"))
					        	{
					        	 try{
					        		String messageName=orderMessage.findPath("messageName").toString();
					            	messageName=messageName.replace("-", "").replace("/", "").replace("\"", "").trim();
					            	//Long.parseLong(messageName);
					            	customer.setPhoneNumber(messageName);
					            	
					            	
					            	if(messageName!=null && messageName.length()==10)
					            	{
					            		json.put("mobileNumber", messageName);
					            		orderType="OFFLINE";
					            	}
					        	 }catch(Exception e){
						        		LOGGER.info("Exception ::"+ e);
						        		}
					        	}
					            	if(true)
					            	{
					            		//LOGGER.info("readGranburyData :response messageName  :"+response);
					            		String customerOrderDetailJson;
					            		String firstName = jsonNode.findPath("customerInfo").findPath("firstName").toString();
					            		if(firstName!=null)
					            		{
					            			firstName=firstName.replace("\"", "");
					            			customer.setFirstName(firstName);
					            		}
					            		LOGGER.info("firstName ::"+ firstName);
					            		String lastName =jsonNode.findPath("customerInfo").findPath("lastName").toString();
					            		if(lastName!=null)
					            		{
					            			lastName=lastName.replace("\"", "");
					            			customer.setLastName(lastName);
					            		}
					            		LOGGER.info("lastName ::"+ lastName);
					            		String email = jsonNode.findPath("customerInfo").findPath("email").toString();
					            		if(email!=null )
					            		{
					            			email=email.replace("\"", "");
					            			if(!email.equalsIgnoreCase("null"))
					            			customer.setEmailId(email);
					            		}
					            		LOGGER.info("email ::"+ email);
					            		String phone = jsonNode.findPath("customerInfo").findPath("phone").toString();
					            		if(phone!=null && orderType.equals("ONLINE") ){
					            			try{
					            				phone=phone.replace("\"", "");
					            				if(!phone.equalsIgnoreCase("null"))
					            				{
					            			  //Long.parseLong(phone);
					            			  customer.setPhoneNumber(phone);
					            			  json.put("mobileNumber", phone);
					            				}
					            			
					            			}catch(Exception e)
					            			{
					            				LOGGER.info("readGranburyData :Exception While parsing Mobile number  :" + e);
					            			}
					            		}
					            		LOGGER.info("phone ::"+ phone);
					            		String localId = jsonNode.findPath("customerInfo").findPath("localId").toString();
					            		json.put("locationUid", merchant2.getMerchantUid());
					            		LOGGER.info("localId ::"+ localId);
					            		String price=jsonNode.findPath("price").toString();
					            		if(price!=null)
					            		{
					            			price=price.replace("\"", "");
					            			json.put("amount",price);
					            			try{
					            				orderR.setOrderPrice(Double.parseDouble(price.replace("\"", "")));
					            			}catch(Exception e)
					            			{
					            				LOGGER.info("readGranburyData :price Id not valid :");
					            			}
					            			
					            		}
					            		LOGGER.info("price ::"+ price);
					            		String localizedOrderDate=jsonNode.findPath("localizedOrderDate").toString();
					            		LOGGER.info("localizedOrderDate ::"+ localizedOrderDate);
					            		customer.setLoyalityProgram(1);
					            		
					            		if(orderId!=null && !orderId.isEmpty())
					            		{
					            			orderId=orderId.replace("\"", "");
					            			Integer ordeIds=0;
					            			try{
					            				 ordeIds=Integer.parseInt(orderId.trim());
					            				 LOGGER.info("readGranburyData :ordeIds  :"+ordeIds);
					            			}catch(Exception e)
					            			{
					            				LOGGER.info("readGranburyData :order Id not valid :");
					            			}
					            		if(customer.getPhoneNumber()!=null && !customer.getPhoneNumber().isEmpty() && ordeIds!=0)
					            		{
					            			orderR.setOrderType(orderType);
					            			orderR.setOrderPosId(ordeIds.toString());
					            		GranburyOrders granburyOrders=granburyOrdersRepository.findByOrderIdAndMerchantId(ordeIds, merchant2.getId());
					            		if(granburyOrders==null)
					            		{
					            			granburyOrders=new GranburyOrders();
					            			granburyOrders.setOrderId(ordeIds);
					            			granburyOrders.setMerchant(merchant2);
					            			granburyOrders.setCreatedOn(today);
					            			importExcelService.kritiqFeedbackByGranbury(customer, merchant2, orderR);
					            			 if(!orderType.equalsIgnoreCase("ONLINE"))
							            	    {	
								            	    String customerUid= ProducerUtil.createCustomerIntoCentralDb(customer,environment);
								            	    ProducerUtil.createCustomerIntoFreekwent(ProducerUtil.getCustomerEntityDTO(customer), environment);
								            		LOGGER.info("readGranburyData :response  createCustomerIntoCentralDb  :" + (customerUid!=null?customerUid:0));
								            		json.put("orderType",orderType);
								            		customerOrderDetailJson=json.toString();
								            		LOGGER.info("readGranburyData :customerOrderDetailJson  :" +customerOrderDetailJson);
								            		LOGGER.info("readGranburyData :call callFreekWentApi  :");
								            		LOGGER.info("readGranburyData :response callFreekWentApi  :"+ProducerUtil.callFreekWentApi(customerOrderDetailJson,environment));
							            	    }
					            		granburyOrdersRepository.save(granburyOrders);
					            		}
					            		}
					            		}
					        		}
					        
					        
					        }
						}else LOGGER.info("readGranburyData : No Data Found for Data  :"+date);
						}else LOGGER.info("readGranburyData : Merchant not found  :");
					   }
					}
				   }else   LOGGER.info("readGranburyData : merchant informataion not found :");
			   }catch(Exception e)
			   {
				   LOGGER.error("===============  CronSchedular : Inside readGranburyData :: Exception  ============= " + e);

				   LOGGER.error("error: " + e.getMessage());
			   }
		   }
		   
		   @Scheduled(cron = "0 0/1 * * * ?")
		 	 public void checkStatusOfOrder() {
				try {
					LOGGER.info("checkStatusOfOrder : Start :");
					String date=DateUtil.getCurrentDateYYYYMMDDForTimeZone("America/Chicago");
					LOGGER.info("checkStatusOfOrder : todays Date :"+date);
					List<OrderR> orderR=orderRepository.findOrdersTodayDateAndIsDefault(DateUtil.getCurrentDateYYYYMMDDForTimeZone("America/Chicago"));
					if(orderR!= null && orderR.size()>0)
					{
						LOGGER.info("checkStatusOfOrder : orderR.size() :"+orderR.size());
						for (OrderR orderR2 : orderR) {
							if(orderR2!=null && orderR2.getMerchant()!=null && orderR2.getMerchant().getOwner()!=null && orderR2.getMerchant().getOwner().getPos()!=null 
									&& orderR2.getMerchant().getOwner().getPos().getPosId()!=null)
							{
								LOGGER.info("checkStatusOfOrder : Order For MerchantId:"+orderR2.getMerchant().getId());
								LOGGER.info("checkStatusOfOrder : OrderId:"+orderR2.getId());	
								String timeZoneCode="America/Chicago";
								MerchantConfiguration merchantConfiguration=merchantConfigurationRepository.findByMerchantId(orderR2.getMerchant().getId());
								if(merchantConfiguration!=null)
								{
									timeZoneCode=timeZoneRepository.findTimeZoneCodeByMerchentId(orderR2.getMerchant().getId());
									if(timeZoneCode==null || timeZoneCode.isEmpty())
										timeZoneCode="America/Chicago";
									LOGGER.info("checkStatusOfOrder : Order For MerchantId:"+orderR2.getMerchant().getId());
									LOGGER.info("checkStatusOfOrder : Order For MerchantId:"+orderR2.getMerchant().getId());
									if(orderR2.getCreatedOn()!=null && (orderR2.getIsFutureOrder()==null || orderR2.getIsFutureOrder()==0))
									{
										LOGGER.info("checkStatusOfOrder : orderR2.getCreatedOn():"+orderR2.getCreatedOn());
										Boolean status =DateUtil.checkCurrentTimeIsFiveMinutGreterthan(orderR2.getCreatedOn(), timeZoneCode);
										LOGGER.info("checkStatusOfOrder : status for order:"+status);
										
										if(status && (orderR2.getIsnotify()==null || orderR2.getIsnotify()==0))
										{
											String email=null;
											String number=null;
											OrderNotification orderNotification=orderNotificationService.findMerchantId(orderR2.getMerchant().getId());
											if(orderNotification!=null && orderNotification.getEmail()!=null)
											{
												email=orderNotification.getEmail();
												LOGGER.info("checkStatusOfOrder : email:"+email);
											}
											if(orderNotification!=null && orderNotification.getMobile()!=null)
											{
												number=orderNotification.getMobile();
												LOGGER.info("checkStatusOfOrder : number:"+number);
											}
											LOGGER.info("checkStatusOfOrder : send mail for Order Id :"+orderR2.getId());
											mailSendUtil.orderPenddingNotificationMail(orderR2,email);
											orderR2.setIsnotify(1);
											orderRepository.save(orderR2);
											sendPendingOrderSMS(orderR2,number,environment);
										} else LOGGER.info("checkStatusOfOrder : Already Notified :");
								}else LOGGER.info("checkStatusOfOrder : orderR2.getCreatedOn() is null:");
								}else LOGGER.info("checkStatusOfOrder : merchantConfiguration Not found:");
							}
						}
					}else LOGGER.info("checkStatusOfOrder : Order Not Found for Date:"+date);
					
				} catch (Exception e) {
					LOGGER.info("checkStatusOfOrder : Exception :"+e);
					LOGGER.error("error: " + e.getMessage());
				}
				LOGGER.info("checkStatusOfOrder : End :");
			}

	public static void sendPendingOrderSMS(OrderR orderR,String merchantNumber,final Environment environment) {
		LOGGER.info("CronSchedular :: sendPendingOrderSMS : start");
		try {
			if (orderR != null && orderR.getCustomer() != null) {
				LOGGER.info("CronSchedular :: sendPendingOrderSMS : order not equal null: " + orderR.getId());
				final String FINALSMSORDERLINK = environment.getProperty("WEB_BASE_URL") + "/receipt?orderid="
						+ EncryptionDecryptionUtil.encryption(orderR.getId().toString()) + "&customerid="
						+ EncryptionDecryptionUtil.encryption(orderR.getCustomer().getId().toString());
				LOGGER.info("CronSchedular :: sendPendingOrderSMS : FINALSMSORDERLINK :: " + FINALSMSORDERLINK);
				final String merchantName;
				String name = "";
				final String mobileNumber=merchantNumber;
				if (orderR.getMerchant().getName() != null)
					name = orderR.getMerchant().getName();
				merchantName = name;
				LOGGER.info("CronSchedular :: sendPendingOrderSMS : FINALSMSORDERLINK :: " + merchantName);
				new Thread() {
					public void run() {
						String googleShortURL = null;
						// googleShortURL=ShortCodeUtil.generateShortUrl(FINALSMSORDERLINK);
						googleShortURL = ShortCodeUtil.getTinyUrl(FINALSMSORDERLINK);
						LOGGER.info("CronSchedular :: sendPendingOrderSMS : googleShortURL :: " + googleShortURL);
						HttpPost postRequest = new HttpPost(environment.getProperty("CommunicationPlatform_BASE_URL_SERVER") + "/sendFoodKonnketSMSForOrder");
						LOGGER.info("CronSchedular :: sendPendingOrderSMS : URL :: " + environment.getProperty("CommunicationPlatform_BASE_URL_SERVER")
								+ "/sendFoodKonnketSMSForOrder");
						String request = "{\"mobileNumber\":\"" + "619-566-6358" + "\",\"messsage\":\"Pending order for " + merchantName
								+ " please view this link " + googleShortURL + "\",\"orgId\":\"" + "140809" + "\"} ";
						LOGGER.info("CronSchedular :: sendPendingOrderSMS : requestJSON :: " + request);
						String response = ProducerUtil.convertToStringJson(postRequest, request);
						LOGGER.info("response for SMS=== " + response);
						LOGGER.info("OrderController :: updateOrderStatus : response for SMS=== " + response);
						if(mobileNumber!=null && !mobileNumber.isEmpty())
						{
							request = "{\"mobileNumber\":\"" + mobileNumber + "\",\"messsage\":\"Pending order for " + merchantName
									+ " please view this link " + googleShortURL + "\",\"orgId\":\"" + "140809" + "\"} ";
							LOGGER.info("CronSchedular :: sendPendingOrderSMS : requestJSON :: " + request);
						    response = ProducerUtil.convertToStringJson(postRequest, request);
						    LOGGER.info("OrderController :: updateOrderStatus : response for SMS=== " + response);
						}
						LOGGER.info("OrderController :: updateOrderStatus : response for SMS=== " + response);
					}
				}.start();
			}
		} catch (Exception e) {
			MailSendUtil.sendExceptionByMail(e,environment);
		}
	}
	
	//@Scheduled(cron = "0 0 6 1 * ?")
//	  @Scheduled(cron = "0 0 15 * * MON")
//    @Scheduled(cron = "0 0 */1 * * ?")
//	  @Scheduled(cron = "0 0 0/1 * * ?")
//	  public void getMonthlyFundRaiserReport() {
//			try {
//				LOGGER.info("Monthly FundRaiser Report schedular start");
//					DateFormat dateFormat = new SimpleDateFormat(IConstant.YYYYMMDD);
//					
//					Date today = new Date();
//					Calendar cal = new GregorianCalendar();
//					cal.setTime(today);
//					cal.add(Calendar.MONTH, -1);
//					Date today7 = cal.getTime();
//					
//					String endDate = dateFormat.format(today);
//					String startDate = dateFormat.format(today7);
//					
//					List<Merchant> merchantData = merchantService.getMonthlyFundRaiserReport(startDate, endDate);
//					LOGGER.info("merchantData size "+merchantData.size());
//					
//			} catch (Exception e) {
//				LOGGER.error("===============  CronScheduler : Inside Monthly FundRaiser Report schedular :: Exception  ============= " + e);
//
//				e.printStackTrace();
//			}
//		}
		   
}
