package com.foodkonnekt.contoller;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.foodkonnekt.clover.vo.ModifierVO;
import com.foodkonnekt.clover.vo.PaymentVO;
import com.foodkonnekt.model.CardInfo;
import com.foodkonnekt.model.CategoryDto;
import com.foodkonnekt.model.ConvenienceFee;
import com.foodkonnekt.model.Customer;
import com.foodkonnekt.model.DeliveryOpeningClosingTime;
import com.foodkonnekt.model.Item;
import com.foodkonnekt.model.ItemTax;
import com.foodkonnekt.model.Merchant;
import com.foodkonnekt.model.MerchantSliders;
import com.foodkonnekt.model.MerchantSocialPlatform;
import com.foodkonnekt.model.OrderR;
import com.foodkonnekt.model.OrderType;
import com.foodkonnekt.model.PaymentGateWay;
import com.foodkonnekt.model.PaymentMode;
import com.foodkonnekt.model.PickUpTime;
import com.foodkonnekt.model.PizzaCrust;
import com.foodkonnekt.model.PizzaCrustSizes;
import com.foodkonnekt.model.PizzaTemplate;
import com.foodkonnekt.model.PizzaTemplateCategory;
import com.foodkonnekt.model.PizzaTemplateSize;
import com.foodkonnekt.model.PizzaTemplateTax;
import com.foodkonnekt.model.PizzaTemplateTopping;
import com.foodkonnekt.model.PizzaTopping;
import com.foodkonnekt.model.PizzaToppingSize;
import com.foodkonnekt.model.TaxRates;
import com.foodkonnekt.model.Vendor;
import com.foodkonnekt.model.VirtualFund;
import com.foodkonnekt.model.Zone;
import com.foodkonnekt.repository.ConvenienceFeeRepository;
import com.foodkonnekt.repository.CustomerCodeRepository;
import com.foodkonnekt.repository.ItemTaxRepository;
import com.foodkonnekt.repository.OrderTypeRepository;
import com.foodkonnekt.repository.PizzaTemplateCategoryRepository;
import com.foodkonnekt.repository.PizzaTemplateRepository;
import com.foodkonnekt.repository.PizzaTemplateSizeRepository;
import com.foodkonnekt.repository.PizzaTemplateTaxRepository;
import com.foodkonnekt.repository.PizzaTemplateToppingRepository;
import com.foodkonnekt.repository.PizzaToppingRepository;
import com.foodkonnekt.repository.PizzaToppingSizeRepository;
import com.foodkonnekt.repository.TaxRateRepository;
import com.foodkonnekt.repository.VirtualFundRepository;
import com.foodkonnekt.service.BusinessService;
import com.foodkonnekt.service.CustomerService;
import com.foodkonnekt.service.ItemService;
import com.foodkonnekt.service.MerchantService;
import com.foodkonnekt.service.MerchantSliderService;
import com.foodkonnekt.service.MerchantSocialPlatformService;
import com.foodkonnekt.service.OrderService;
import com.foodkonnekt.service.PizzaService;
import com.foodkonnekt.service.ZoneService;
import com.foodkonnekt.util.DateUtil;
import com.foodkonnekt.util.EncryptionDecryptionToken;
import com.foodkonnekt.util.IConstant;
import com.foodkonnekt.util.MailSendUtil;
import com.foodkonnekt.util.OrderUtil;
import com.foodkonnekt.util.UrlConstant;
import com.google.gson.Gson;

@Controller
@SuppressWarnings({ "unchecked", "rawtypes" })
public class OrderControllerV2 {

	private static final Logger LOGGER = LoggerFactory.getLogger(OrderControllerV2.class);
	
	@Autowired
    private Environment environment;
	
	@Autowired
	private VirtualFundRepository virtualFundRepository;
	
	@Autowired
	private CustomerCodeRepository customerCodeRepository;
	
    @Autowired
    private OrderService orderService;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private BusinessService businessService;

    @Autowired
    private MerchantService merchantService;

    @Autowired
    private TaxRateRepository taxRateRepository;

    @Autowired
    private ItemTaxRepository itemTaxRepository;

    @Autowired
    private ConvenienceFeeRepository convenienceFeeRepository;

    @Autowired
    private PizzaService pizzaService;

    @Autowired
    private OrderTypeRepository orderTypeRepository;

    @Autowired
    private ItemService itemService;

    @Autowired
    private PizzaTemplateRepository pizzaTemplateRepository;

    @Autowired
    private PizzaTemplateSizeRepository pizzaTemplateSizeRepository;

    @Autowired
    private ZoneService zoneService;

    @Autowired
    private PizzaTemplateTaxRepository pizzaTemplateTaxRepository;

    @Autowired
    private PizzaTemplateCategoryRepository pizzaTemplateCategoryRepository;
    
    @Autowired
    private PizzaTemplateToppingRepository pizzaTemplateToppingRepository;

    @Autowired
    private MerchantSliderService merchantSliderService;
    
    @Autowired
    private PizzaToppingRepository pizzaToppingRepository;
    
    @Autowired
    private PizzaToppingSizeRepository pizzaToppingSizeRepository;
    
    @Autowired
    private MerchantSocialPlatformService merchantSocialPlatformService;

    
    @RequestMapping(value = "/orderV2", method = RequestMethod.GET)
    public String showOrderPage(@ModelAttribute("PaymentVO") PaymentVO paymentVO, ModelMap model,
                    Map<String, Object> map, HttpServletRequest request,
                    @RequestParam(required = false) String message,
                                @RequestParam(required = false) String futureDate,
                                @RequestParam(required = false) String futureTime,
                                @RequestParam(required = false) Integer vendorId,
                    @RequestParam(required = false) Integer merchantId,
                    @RequestParam(required = false) Integer repeateOrderId) {
    	LOGGER.info("----------------Start :: OrderControllerV2 : orderV2------------------");
        
    	HttpSession session = request.getSession();
        Merchant merchant = (Merchant) session.getAttribute("merchant");
        // Merchant merchant=null;
        Customer sessionCustomer = (Customer) session.getAttribute("customer");
        if (sessionCustomer == null) {
        	sessionCustomer = (Customer) session.getAttribute("guestCustomer");
        }
        if (merchant != null && merchantId != null && ((int) merchantId != (int) merchant.getId() || (sessionCustomer!=null && sessionCustomer.getMerchantt()!=null && sessionCustomer.getMerchantt().getId()!=null 
        		&& (int) sessionCustomer.getMerchantt().getId() != (int) merchant.getId()))) {
        
            session.removeAttribute("merchant");
            session.removeAttribute("customer");
            session.removeAttribute("guestCustomer");
            session.removeAttribute("orderItems");
            session.removeAttribute("Tax");
            session.removeAttribute("totalPrice");
            session.removeAttribute("subTotal");
            session.removeAttribute("ConvenienceFee");
            session.removeAttribute("convenienceFeeItem");
        }

        if (merchantId != null) {
        	LOGGER.info("OrderControllerV2 :: orderV2 : merchantId "+merchantId);
            merchant = merchantService.findById(merchantId);
        }

        if(merchant!=null && merchant.getId() !=null){
        	maptemplateWithTax(merchant.getId());
        	 List<MerchantSliders> merchantSliderList = merchantSliderService.findByMerchantId(merchant.getId());
             if (merchantSliderList != null) {
                 model.addAttribute("merchantSliderList", merchantSliderList);
                 model.addAttribute("url", environment.getProperty("BASE_PORT"));
             }
             try {
     	        MerchantSocialPlatform merchantSocialPlatform = merchantSocialPlatformService.findByMerchantId(merchant.getId());
                 if(merchantSocialPlatform != null && merchantSocialPlatform.getGascript() != null){
                     model.addAttribute("gaScript", merchantSocialPlatform.getGascript());
                     LOGGER.info("OrderControllerV2 :: orderV2 : merchantId "+merchant.getId()+" GA-Script "+merchantSocialPlatform.getGascript());
                 }
             	} catch (Exception e) {
             		LOGGER.info(" OrderControllerV2 : orderV2 : Exception--------------- "+e);
             		LOGGER.error("error: " + e.getMessage());
         		} 
        }
             

        if (vendorId != null) {
            if (merchant != null && merchant.getOwner() != null && merchant.getOwner().getId() != null
                            && merchant.getOwner().getId() == vendorId) {
                Vendor vendor = merchantService.findVendorById(vendorId);
                List<Merchant> merchantList = merchantService.findAllMerchantsByVendorId(vendorId);
                if (vendor != null) {
                    session.setAttribute("vendor", vendor);
                    session.setAttribute("merchantList", merchantList);
                }
            } else {
                session.removeAttribute("vendor");
                session.removeAttribute("merchantList");
            }
        } else {
            session.removeAttribute("vendor");
            session.removeAttribute("merchantList");
        }
        if (merchant != null) {
        	if(merchant.getIsInstall() != null && merchant.getIsInstall() != 1){
        		 LOGGER.info("OrderControllerV2 :: orderV2 : isInstall of merchantId "+merchant.getId()+" is "+merchant.getIsInstall());
        		 return "redirect:https://www.foodkonnekt.com";
        	}else{
        		 try {
        			 LOGGER.info("OrderControllerV2 :: orderV2 : isInstall of merchantId "+merchant.getId()+" is "+merchant.getIsInstall());
                     merchant = merchantService.findById(merchant.getId());
                     session.setAttribute("merchant", merchant);
                    
                     // adding payment gateway type to the session
                     PaymentGateWay gateway = merchantService.findbyMerchantId(merchant.getId(), false);
                     if (gateway != null && "3".equals(gateway.getGateWayType())) {
                         session.setAttribute("paymentGateway", false);
                     } else {
                         session.setAttribute("paymentGateway", true);
                     }
                     // DB-----------0
                     List<CategoryDto> categories = orderService.findCategoriesByMerchantIdV2(merchant);
                     if (categories != null && !categories.isEmpty()) {
                         if (sessionCustomer != null) {
                             Gson gson = new Gson();
                             if (repeateOrderId != null) {
                                 List<OrderR> repeatOrder = orderService.getRepeateOrder(sessionCustomer.getId(), merchant,
                                                 repeateOrderId);
                                 if (repeatOrder != null && !repeatOrder.isEmpty()) {
                                     if (repeatOrder.get(0).getOrderName() == ""
                                                     && repeatOrder.get(0).getOrderName() == null
                                                     && repeatOrder.get(0).getOrderName().isEmpty()) {
                                         repeatOrder.get(0).setOrderName(null);
                                     }
                                     if (repeatOrder.get(0).getOrderNote() == ""
                                                     && repeatOrder.get(0).getOrderNote() == null
                                                     && repeatOrder.get(0).getOrderNote().isEmpty()) {
                                         repeatOrder.get(0).setOrderNote(null);
                                     }
                                     repeatOrder.get(0).setOrderNote(null);
                                     repeatOrder.get(0).setOrderName(null);
                                     repeatOrder.get(0).setCreatedOn(null);
                                     OrderR order = repeatOrder.get(0);
                                     order.setMerchant(null);
                                     order.setCustomer(null);

                                     model.addAttribute("orderList", gson.toJson(order));
                                     LOGGER.info("OrderControllerV2 :: orderV2 : REPEATEDORDERJSAON: "+gson.toJson(order));
                                     System.out.println("REPEATEDORDERJSAON" + gson.toJson(order));
                                 } else {
                                     model.addAttribute("orderList", "empty");
                                 }
                             } else {
                                 model.addAttribute("orderList", "empty");

                             }
                             model.addAttribute("sessionCustId", sessionCustomer.getId());
                             if (sessionCustomer.getPassword() != null) {
                                 if (sessionCustomer.getPassword().isEmpty()) {
                                     model.addAttribute("setPassword", "Y");
                                 }
                             } else {
                                 model.addAttribute("setPassword", "Y");
                             }

                             // DB-------------4
                             String jsonInString = gson.toJson(orderService.findAddessByCustomerId(sessionCustomer.getId()));
                             model.addAttribute("address", jsonInString);

                             List<CardInfo> cardInfos = customerService.getCustomerCardInfo(sessionCustomer.getId());
                             List<CardInfo> cardsInfos = new ArrayList<CardInfo>();
                             List<CardInfo> expiryCardsInfos = new ArrayList<CardInfo>();
                             Calendar calendar = Calendar.getInstance();
                             Integer year = Integer.parseInt(("" + calendar.get(Calendar.YEAR)).substring(2));
                             Integer month = calendar.get(Calendar.MONTH);

                             for (CardInfo cardInfo : cardInfos) {
                                 cardInfo.setCustomer(null);
                                 cardInfo.setMerchant(null);
                                 // cardInfo.getExpirationDate()
                                 String decryptExpirationDate = (cardInfo.getExpirationDate());
                                 if (Integer.parseInt(decryptExpirationDate.substring(2, 4)) > year) {
                                     cardsInfos.add(cardInfo);
                                 } else if (Integer.parseInt(decryptExpirationDate.substring(2, 4)) == year
                                                 && Integer.parseInt(decryptExpirationDate.substring(0, 2)) > month) {
                                     cardsInfos.add(cardInfo);
                                 } else {
                                     expiryCardsInfos.add(cardInfo);
                                 }
                             }
                             if (merchant != null && merchant.getAllowMultiPay() != null
                                             && merchant.getAllowMultiPay() == true) {
                                 String cardsInfosJson = gson.toJson(cardsInfos);
                                 String expiryCardsInfosJson = gson.toJson(expiryCardsInfos);
                                 session.setAttribute("cardInfo", cardsInfosJson);
                                 session.setAttribute("expiryCardsInfos", expiryCardsInfosJson);
                                 session.setAttribute("allowMultiPay", true);
                             }
                         } else {
                             model.addAttribute("orderList", "empty");
                             model.addAttribute("address", "NoData");
                         }

                         // DB-------------------5
                         ConvenienceFee convenienceFee = businessService.findConvenienceFeeByMerchantId(merchant.getId());
                         if (convenienceFee != null) {
                             if (Double.valueOf(convenienceFee.getConvenienceFee()) > 0) {
                                 if (convenienceFee.getIsTaxable() != null) {
                                     if (convenienceFee.getIsTaxable() == 1) {
                                         model.addAttribute("isConvenienceFeePrice", IConstant.BOOLEAN_TRUE);
                                         model.addAttribute("convenienceFeePosId",
                                                         convenienceFee.getConvenienceFeeLineItemPosId());

                                         // Db--------------6
                                         model.addAttribute(
                                                         "convenienceFeeTax",
                                                         orderService.findConvenienceFeeAfterTax(
                                                                         convenienceFee.getConvenienceFee(),
                                                                         merchant.getId()));

                                         // DB---------------7
                                         model.addAttribute(
                                                         "convenienceFeeTaxWithComma",
                                                         orderService.findConvenienceFeeAfterMultiTax(
                                                                         convenienceFee.getConvenienceFee(),
                                                                         merchant.getId()));
                                         model.addAttribute("convenienceFeePrice", convenienceFee.getConvenienceFee());
                                     }
                                 } else {
                                     model.addAttribute("convenienceFeeTax", 0);
                                     model.addAttribute("convenienceFeePosId",
                                                     convenienceFee.getConvenienceFeeLineItemPosId());
                                     model.addAttribute("convenienceFeePrice", convenienceFee.getConvenienceFee());
                                     model.addAttribute("isConvenienceFeePrice", IConstant.BOOLEAN_TRUE);
                                 }
                             } else {
                                 model.addAttribute("isConvenienceFeePrice", IConstant.BOOLEAN_FALSE);
                             }
                         }
                         System.out.println("AllowFutureOrder --> " + merchant.getAllowFutureOrder());
                         model.addAttribute("allowFutureOrder", merchant.getAllowFutureOrder());
                         LOGGER.info("OrderControllerV2 :: orderV2 : AllowFutureOrder: "+merchant.getAllowFutureOrder());
                         // model.addAttribute("allowFutureOrder", 1);
                         if (merchant.getAllowFutureOrder() != null
                                         && IConstant.BOOLEAN_TRUE == merchant.getAllowFutureOrder()) {
                             model.addAttribute("futureOrderDates", DateUtil.find10FutureDates());
                         }

                         // DB----------------8

                         PickUpTime pickUpTime = merchantService.findPickupTime(merchant.getId());
                         if (pickUpTime != null && pickUpTime.getMinOrderAmount() != null
                                         && pickUpTime.getMinOrderAmount() > 0) {
                             model.addAttribute("pickUpMinOrderAmount", pickUpTime.getMinOrderAmount());
                         } else {
                             model.addAttribute("pickUpMinOrderAmount", 0.0);
                         }
    
                         if (pickUpTime != null && pickUpTime.getPickUpTime() != null) {
                             model.addAttribute("pickUpTime", pickUpTime.getPickUpTime().trim());
                         } else {
                             model.addAttribute("pickUpTime","");
                         }

                         PaymentMode creditCardMode = businessService.findByMerchantIdAndLabel(merchant.getId(),
                                         "Credit Card");
                         if (creditCardMode != null && creditCardMode.getMinOrderAmount() != null
                                         && creditCardMode.getMinOrderAmount() > 0) {
                             model.addAttribute("ccMinOrderAmount", creditCardMode.getMinOrderAmount());
                         } else {
                             model.addAttribute("ccMinOrderAmount", 0.0);
                         }
                         model.addAttribute("paymentModes", orderService.findPaymentMode(merchant.getId()));

                         List<CategoryDto> categoryDtos = orderService.getPizzaCategory(merchant);
                         categories.addAll(categoryDtos);

                         Collections.sort(categories);
                         model.addAttribute("categories", categories);
                         model.addAttribute("message", message);
                         model.addAttribute("futureDate", futureDate);
                         model.addAttribute("futureTime", futureTime);
    
                         model.addAttribute("merchantId", merchant.getId());
                         model.addAttribute("merchantUid", merchant.getMerchantUid());
                         // DB-----------------9
                         model.addAttribute("merchantTaxs", orderService.findMerchantTaxs(merchant.getId()));
                     } else {
                      	LOGGER.info("----------------End :: OrderControllerV2 : orderV2------------------");

                         return "categoryEmpty";

                     }
                 } catch (Exception exception) {
                     if (exception != null) {
                     	LOGGER.error("OrderControllerV2 :: orderV2 : Exception "+exception);
                         MailSendUtil.sendExceptionByMail(exception,environment);
                     }
                     exception.printStackTrace();
                 }

                 if (merchant.getOwner() != null && merchant.getOwner().getPos().getPosId() == 2) {
                     System.out.println("inside foodtronix block");
                     LOGGER.info("----------------End :: OrderControllerV2 : orderV2------------------");
                     
                     return "menu";
                 } else {
                 	LOGGER.info("----------------End :: OrderControllerV2 : orderV2------------------");
                     return "menu";
                 }
        	}
        } else {
        	LOGGER.info("----------------End :: OrderControllerV2 : orderV2------------------");
            return "redirect:https://www.foodkonnekt.com";
        }
    }

    @RequestMapping(value = "/addToCart", method = { RequestMethod.POST })
    @ResponseBody
    public String addToCart(@RequestBody com.foodkonnekt.clover.vo.Item item, HttpServletRequest request,
                    HttpSession session) {
    	LOGGER.info("----------------Start :: OrderControllerV2 : addToCart------------------");
        String jsonInString = null;
        String itemUid = null;
        itemUid = EncryptionDecryptionToken.randomString(6);
        List<com.foodkonnekt.clover.vo.Item> orderItems = null;
        if (item != null) {
            item.setItemUid(itemUid);
            item.setExtraCharge(false);
            if (item.getIsPizza() == null) {
                item.setIsPizza(false);
            }
        }
        if (session.getAttribute("orderItems") != null) {
            orderItems = (List<com.foodkonnekt.clover.vo.Item>) session.getAttribute("orderItems");
            for (com.foodkonnekt.clover.vo.Item items : orderItems) {
                if (!items.getExtraCharge()) {

                    if (items.getItemUid().equals(itemUid)) {
                        itemUid = EncryptionDecryptionToken.randomString(6);
                        item.setItemUid(itemUid);
                    }
                }
            }
        }
        if (orderItems == null) {
            orderItems = new ArrayList<com.foodkonnekt.clover.vo.Item>();
        }
        LOGGER.info(" === item : "+item.getId());
        orderItems.add(item);
        session.setAttribute("orderItems", orderItems);

        Gson gson = new Gson();
        jsonInString = gson.toJson(orderItems);
        LOGGER.info("----------------End :: OrderControllerV2 : addToCart------------------");
        return jsonInString;
    }

    @RequestMapping(value = "/removeToCart", method = { RequestMethod.GET })
    @ResponseBody
    public String removeToCart(@RequestParam String itemUid, @RequestParam(required = false) String orderType,
                    HttpServletRequest request, HttpSession session) {
    	LOGGER.info("----------------Start :: OrderControllerV2 : removeToCart------------------");
    	LOGGER.info(" === orderType : "+orderType);
        List<com.foodkonnekt.clover.vo.Item> orderItems = null;
        Map<String, Object> map = null;
        Merchant merchant = (Merchant) session.getAttribute("merchant");
        Map<String, Object> appliedDiscounts = new HashMap<String, Object>();
        LOGGER.info("OrderControllerV2 :: removeToCart : merchantId "+merchant.getId());
        if (session.getAttribute("orderItems") != null) {
            orderItems = (List<com.foodkonnekt.clover.vo.Item>) session.getAttribute("orderItems");
            for (int i = 0; i < orderItems.size(); i++) {
                if (!orderItems.get(i).getExtraCharge() && orderItems.get(i).getItemUid().equals(itemUid)) {
                	LOGGER.info("OrderControllerV2 :: removeToCart : remove Item "+itemUid);
                    orderItems.remove(i);
                }
            }

            if (merchant != null && !orderItems.isEmpty()) {
                List<com.foodkonnekt.clover.vo.Item> newItemList = new ArrayList<com.foodkonnekt.clover.vo.Item>(
                                orderItems);
                if (merchant.getOwner().getPos().getPosId() != 2) {
                    map = applyTaxAndConvenienceFee(newItemList, merchant, null, null,null);
                } else {
                    if (orderType != null) {
                        map = applyTaxForFoodTronix(newItemList, merchant, orderType);
                    }
                }
            }
        }
        session.setAttribute("orderItems", orderItems);
        if (session.getAttribute("appliedDiscounts") != null) {
            session.setAttribute("appliedDiscounts", appliedDiscounts);
        }
        if (map != null) {
            map.put("subTotal", map.get("subTotal"));
            map.put("totalPrice", map.get("totalPrice"));
            map.put("ConvenienceFee", map.get("ConvenienceFee"));
            map.put("Tax", map.get("Tax"));
            if ((Double) map.get("auxTax") != null && (Double) map.get("auxTax") > 0) {
                map.put("auxTax", map.get("auxTax"));
                session.setAttribute("auxTax", map.get("auxTax"));
            }
            session.setAttribute("Tax", map.get("Tax"));
            session.setAttribute("totalPrice", map.get("totalPrice"));
            session.setAttribute("subTotal", map.get("subTotal"));
            session.setAttribute("ConvenienceFee", map.get("ConvenienceFee"));
        } else {
            map = new HashMap<String, Object>();
            map.put("subTotal", 0.0);
            map.put("totalPrice", 0.0);
            map.put("ConvenienceFee", 0.0);
            map.put("Tax", 0.0);
            map.put("auxTax", 0.0);
            session.setAttribute("Tax", 0.0);
            session.setAttribute("totalPrice", 0.0);
            session.setAttribute("subTotal", 0.0);
            session.setAttribute("ConvenienceFee", 0.0);
            session.setAttribute("auxTax", 0.0);
        }
        LOGGER.info("OrderControllerV2 :: removeToCart : itemUid "+itemUid);
        System.out.println(itemUid);
        Gson gson = new Gson();
        String jsonInString = gson.toJson(map);
        LOGGER.info("----------------End :: OrderControllerV2 : removeToCart--------------jsonInString : "+jsonInString);
        return jsonInString;
    }

    @RequestMapping(value = "/cartV2", method = { RequestMethod.GET })
    public String cartV2(HttpSession session, ModelMap model) {
    	LOGGER.info("----------------Start :: OrderControllerV2 : cartV2------------------");
        List<com.foodkonnekt.clover.vo.Item> orderItems = null;
        Map<String, Object> response = null;
        Merchant merchant = null;
        Customer customer = null;
        merchant = (Merchant) session.getAttribute("merchant");
        if (session.getAttribute("orderItems") != null) {
            orderItems = (List<com.foodkonnekt.clover.vo.Item>) session.getAttribute("orderItems");
            if (session.getAttribute("merchant") != null) {

            	List<TaxRates>itemTaxRemove=  taxRateRepository.findByMerchantId(merchant.getId());
                if(itemTaxRemove!=null && !itemTaxRemove.isEmpty())
                for (TaxRates rates : itemTaxRemove) {
                   for(com.foodkonnekt.clover.vo.Item orderItemsid:orderItems)
                   {
                   	if(!orderItemsid.getIsPizza())
                   	{
                   	List<ItemTax> itemTax=itemTaxRepository.findByItemIdAndTaxRatesId(Integer.parseInt(orderItemsid.getItemid()), (rates.getId()));
                   	boolean flag=true;
                   	if(itemTax!=null && !itemTax.isEmpty() && itemTax.size()>1)
                   	{
                   		for(ItemTax itemTaxids:itemTax)
                   		{
                   			if(flag)
                   			{
                   				flag=false;
                   			}	
                   			else{
                   				
                   				itemTaxRepository.delete(itemTaxids.getId());
                   			}
                   			
                   		}
                   		
                   	}
                   }
                   	else
                   	{
                   		List<PizzaTemplateTax> pizzaTemplateTax= pizzaTemplateTaxRepository.findByPizzaTemplateIdAndPizzaTemplateMerchantIdAndIsActive(Integer.parseInt(orderItemsid.getItemid()), merchant.getId(),1);
                   		boolean flag=true;
                       	if(pizzaTemplateTax!=null && !pizzaTemplateTax.isEmpty() && pizzaTemplateTax.size()>1)
                       	{
                       		for(PizzaTemplateTax pizzaTemplateTaxIds:pizzaTemplateTax)
                       		{
                       			if(flag)
                       			{
                       				flag=false;
                       			}
                       			else{
                       				
                       				pizzaTemplateTaxRepository.delete(pizzaTemplateTaxIds.getId());
                       			}
                       			
                       		}
                       		
                       	}
                   	}
                   }
                }
                
                if (merchant != null) {
                	LOGGER.info("OrderControllerV2 :: orderV2 : merchantId "+merchant.getId());
                    model.addAttribute("paymentModes", orderService.findPaymentMode(merchant.getId()));
                    
                    
                    PaymentGateWay details =merchantService.findbyMerchantId(merchant.getId(),false);

                    if(details!=null &&details.getPayeezyMerchnatTokenJs()!=null && details.getPayeezyMerchnatTokenJs()!="" ) {
                    model.addAttribute("Payeezy3DsModes", true);
                    LOGGER.info("OrderControllerV2 :: orderV2 : Payeezy3DsModes "+true);
                    }
                    else {
                        model.addAttribute("Payeezy3DsModes", false);
                        LOGGER.info("OrderControllerV2 :: orderV2 : Payeezy3DsModes "+false);
                    }


                    PickUpTime pickUpTime = merchantService.findPickupTime(merchant.getId());
                    if (pickUpTime != null && pickUpTime.getMinOrderAmount() != null
                                    && pickUpTime.getMinOrderAmount() > 0) {
                        model.addAttribute("pickUpMinOrderAmount", pickUpTime.getMinOrderAmount());
                        LOGGER.info("OrderControllerV2 :: orderV2 : pickUpMinOrderAmount "+pickUpTime.getMinOrderAmount());
                    } else {
                        model.addAttribute("pickUpMinOrderAmount", 0.0);
                        LOGGER.info("OrderControllerV2 :: orderV2 : pickUpMinOrderAmount "+0.0);
                    }

                    PaymentMode creditCardMode = businessService.findByMerchantIdAndLabel(merchant.getId(),
                                    "Credit Card");
                    if (creditCardMode != null && creditCardMode.getMinOrderAmount() != null
                                    && creditCardMode.getMinOrderAmount() > 0) {
                        model.addAttribute("ccMinOrderAmount", creditCardMode.getMinOrderAmount());
                        LOGGER.info("OrderControllerV2 :: orderV2 : ccMinOrderAmount "+creditCardMode.getMinOrderAmount());
                    } else {
                    	LOGGER.info("OrderControllerV2 :: orderV2 : ccMinOrderAmount "+0.0);
                    }

                    String avgDeliveryTime = null;
                    String minDollarAmount=null;
                    Calendar deliveryAvgTimeCalendar = Calendar.getInstance();
                    String timeZoneCode = "America/Chicago";
                    if (merchant != null && merchant.getTimeZone() != null
                                    && merchant.getTimeZone().getTimeZoneCode() != null) {
                        timeZoneCode = merchant.getTimeZone().getTimeZoneCode();
                    }

                    List<Zone> zones = zoneService.findZoneByMerchantIdAndStatus(merchant.getId());
                    try {
                        if (zones != null) {
                            if (!zones.isEmpty() && zones.size() > 0) {

                                avgDeliveryTime = zones.get(0).getAvgDeliveryTime();
                                if (avgDeliveryTime != null) {
                                    java.util.Date deliveryAvgTime = new SimpleDateFormat("HH:mm:ss").parse("00:"
                                                    + avgDeliveryTime + ":00");
                                    deliveryAvgTimeCalendar.setTime(deliveryAvgTime);
                                }
                                
                                minDollarAmount=zones.get(0).getMinDollarAmount();
                                if(minDollarAmount != null)
                                {
                                	model.addAttribute("minDeliveryAmount", minDollarAmount);
                                	LOGGER.info("OrderControllerV2 :: orderV2 : minDeliveryAmount "+minDollarAmount);
                                	
                                }

                                if (merchant.getAllowDeliveryTiming() != null
                                                && merchant.getAllowDeliveryTiming() == IConstant.BOOLEAN_TRUE) {
                                    model.addAttribute("zoneStatus",
                                                    checkDeliveryTimingStatus(merchant.getId(), timeZoneCode, 0));
                                    LOGGER.info("OrderControllerV2 :: orderV2 : zoneStatus :merchantId  "+model.get("zoneStatus"));
                                } else {
                                    model.addAttribute("zoneStatus", "Y");
                                    LOGGER.info("OrderControllerV2 :: orderV2 : zoneStatus "+"Y");
                                }
                            } else {
                                model.addAttribute("zoneStatus", "N");
                                LOGGER.info("OrderControllerV2 :: orderV2 : zoneStatus "+"N");
                            }
                        } else {
                            model.addAttribute("zoneStatus", "N");
                            LOGGER.info("OrderControllerV2 :: orderV2 : zoneStatus "+"N");
                        }
                    } catch (Exception e) {
                    	LOGGER.info("OrderControllerV2 :: orderV2 : Exception "+e);
                        MailSendUtil.sendExceptionByMail(e,environment);
                        LOGGER.error("error: " + e.getMessage());
                    }
                    model.addAttribute("merchantTaxes", orderService.findMerchantTaxs(merchant.getId()));
                    
                    String today = DateUtil.getCurrentDateYYYYMMDDForTimeZone(timeZoneCode);
                    Integer count = virtualFundRepository.findByMerchantIdAndStatusAndDate(merchant.getId(), true, today);
                    model.addAttribute("virtualFundCount", count);
                }
            }
            if (session.getAttribute("customer") != null && merchant != null && merchant.getId() != null) {
                customer = (Customer) session.getAttribute("customer");
                model.addAttribute("customerAddresses",
                                customerService.findAllCustomerAddress(customer.getId(), merchant.getId()));
            }
            if (!orderItems.isEmpty()) {
                List<com.foodkonnekt.clover.vo.Item> newItems = new ArrayList<com.foodkonnekt.clover.vo.Item>(
                                orderItems);
                response = applyTaxAndConvenienceFee(newItems, (Merchant) session.getAttribute("merchant"), null, null,null);
            }
        }
       
        double tax = 0;
        double total = 0;
        double subTotal = 0;
        double convenienceFee = 0;
        double discount = 0;
        if (response != null) {
            tax = (Double) response.get("Tax");
            total = (Double) response.get("totalPrice");
            subTotal = (Double) response.get("subTotal");
            convenienceFee = (Double) response.get("ConvenienceFee");
            session.setAttribute("onlineFeeFlag", response.get("ConvenienceFee"));
            session.setAttribute("convenienceFeeItem", response.get("convenienceFeeItem"));
            model.addAttribute("taxNameAndItemPrice", new Gson().toJson(response.get("taxNameAndItemPrice")));
        }
        LOGGER.info(" ===tax "+tax+" total "+total+"subTotal : "+subTotal+" convenienceFee : "+convenienceFee+" discount : "+discount);
        model.addAttribute("webBaseUrl", environment.getProperty("WEB_BASE_URL"));
        model.addAttribute("adminBaseUrl", environment.getProperty("BASE_URL"));
        session.setAttribute("Tax", tax);
        session.setAttribute("totalPrice", total);
        session.setAttribute("subTotal", subTotal);
        session.setAttribute("ConvenienceFee", convenienceFee);
        session.setAttribute("orderDiscount", discount);
        LOGGER.info("----------------End :: OrderControllerV2 : cartV2------------------");
        return "cartV2";
    }

    public Map<String, Object> applyTaxAndConvenienceFee(List<com.foodkonnekt.clover.vo.Item> items, Merchant merchant,
                    com.foodkonnekt.clover.vo.Item deliveryItem, Boolean deliveryFeeTaxable,String label) {
    	LOGGER.info("----------------Start :: OrderControllerV2 : applyTaxAndConvenienceFee------------------");
    	LOGGER.info(" ===deliveryFeeTaxable "+deliveryFeeTaxable);
    	
        com.foodkonnekt.clover.vo.Item convenienceFeeItem = null;
        double discountedSubTotal = 0;
        double discountedTax = 0;
        double discountedTotal = 0;
        boolean flag = false;
        Boolean onlineFeeFlag = false;
        Boolean isConvenienceFeeTaxable = false;
        double mapConvenienceFee = 0;
        double taxItem = 0;
        double auxTax = 0;
        double auxTaxTotal = 0;
        double ConvenienceFeePercent = 0;
        

        Map<String, ArrayList<com.foodkonnekt.clover.vo.Item>> itemTaxes = new HashMap<String, ArrayList<com.foodkonnekt.clover.vo.Item>>();
        Map<String, Double> taxNameAndItemPrice = new HashMap<String, Double>();
        List<TaxRates> taxRates = taxRateRepository.findByMerchantId(merchant.getId());
        List<TaxRates> defaultTaxRates = taxRateRepository.findByMerchantIdAndIsDefault(merchant.getId(),
                        IConstant.BOOLEAN_TRUE);
        LOGGER.info("OrderControllerV2 : applyTaxAndConvenienceFee :merchant.getId()  "+merchant.getId());
        if (taxRates != null) {
            for (TaxRates rates : taxRates) {
                itemTaxes.put(rates.getName(), new ArrayList<com.foodkonnekt.clover.vo.Item>());
            }
        }

        for (com.foodkonnekt.clover.vo.Item item : items) {
            if (item.getExtraCharge() != null && item.getExtraCharge() && item.getPrice() != null) {
                flag = true;
                mapConvenienceFee = Double.parseDouble(item.getPrice());
            }
        }
        if (!flag) {
            ConvenienceFee convenienceFee = null;
            List<ConvenienceFee> convenienceFees = convenienceFeeRepository.findByMerchantId(merchant.getId());
            if (convenienceFees != null && convenienceFees.size() > 0) {
                int index = convenienceFees.size() - 1;
                convenienceFee = convenienceFees.get(index);
                ConvenienceFeePercent =  (convenienceFee.getConvenienceFeePercent()!=null) ? convenienceFee.getConvenienceFeePercent() : 0;
              }
            	
            if (convenienceFee != null && convenienceFee.getConvenienceFee() != null
                            && Double.valueOf(convenienceFee.getConvenienceFee()) > 0) {

                convenienceFeeItem = new com.foodkonnekt.clover.vo.Item();

                convenienceFeeItem.setItemPosId(convenienceFee.getConvenienceFeeLineItemPosId());
                convenienceFeeItem.setItemid(convenienceFee.getId() + "");
                convenienceFeeItem.setPrice(convenienceFee.getConvenienceFee());
                onlineFeeFlag = true;
                convenienceFeeItem.setModifier(new ArrayList<ModifierVO>());
                mapConvenienceFee = Double.parseDouble(convenienceFee.getConvenienceFee());
                convenienceFeeItem.setAmount("1");
                convenienceFeeItem.setExtraCharge(true);
                items.add(convenienceFeeItem);
                 if (convenienceFee.getIsTaxable() != null) {
                    if (convenienceFee.getIsTaxable() == 1) {
                        isConvenienceFeeTaxable = true;

                    } else {
                        isConvenienceFeeTaxable = false;
                    }
                } else {
                    isConvenienceFeeTaxable = false;
                }
            }

        }
        LOGGER.info("OrderControllerV2 : applyTaxAndConvenienceFee :mapConvenienceFee  "+mapConvenienceFee);
        for (com.foodkonnekt.clover.vo.Item item : items) {
            if (item.getExtraCharge() != null && !item.getExtraCharge()) {

                if (item.getAmount() != null && item.getPrice() != null) {
                    discountedSubTotal = discountedSubTotal + Double.parseDouble(item.getPrice())
                                    * Double.parseDouble(item.getAmount());
                }
                if (!item.getIsPizza()) {
                    Double modifierPrice = 0.0;
                    if (item.getModifier() != null) {
                        for (ModifierVO modifier : item.getModifier()) {
                            if (modifier.getPrice() != null) {
                                modifierPrice += Double.parseDouble(modifier.getPrice());
                            }
                        }
                        item.setModifierPrice(modifierPrice + "");
                        discountedSubTotal += modifierPrice * Double.parseDouble(item.getAmount());
                    }
                    if (item.getItemPosId() != null) {
                        List<ItemTax> itemTaxs = null;
                        LOGGER.info(" ===item.getItemid() "+item.getItemid());
                        itemTaxs = itemTaxRepository.findByItemIdAndItemMerchantId(Integer.parseInt(item.getItemid()),
                                        merchant.getId());
                        if (!itemTaxs.isEmpty()) {
                            for (ItemTax itemTax : itemTaxs) {
                                if (itemTax != null && itemTax.getTaxRates() != null) {
                                    TaxRates rates = itemTax.getTaxRates();
                                    if (itemTaxes.containsKey(rates.getName()) && rates.getName() != null) {
                                        ArrayList<com.foodkonnekt.clover.vo.Item> itemTx = (ArrayList) itemTaxes
                                                        .get(rates.getName());
                                        itemTx.add(item);
                                    }
                                }
                            }
                        }
                    }
                } else {
                    Double toppingPrice = 0.0;
                    if (item.getModifier() != null) {
                        for (ModifierVO modifier : item.getModifier()) {
                            if (modifier.getPrice() != null) {
                                toppingPrice += Double.parseDouble(modifier.getPrice());
                            }
                        }
                        item.setModifierPrice(toppingPrice + "");
                        discountedSubTotal += toppingPrice * Double.parseDouble(item.getAmount());
                    }
                    if (item.getItemid() != null) {
                        List<PizzaTemplateTax> pizzaTemplateTaxs = pizzaTemplateTaxRepository
                                        .findByPizzaTemplateIdAndPizzaTemplateMerchantId(
                                                        Integer.parseInt(item.getItemid()), merchant.getId());

                        if (!pizzaTemplateTaxs.isEmpty()) {
                            for (PizzaTemplateTax itemTax : pizzaTemplateTaxs) {
                                if (itemTax != null && itemTax.getTaxRates() != null) {
                                    TaxRates rates = itemTax.getTaxRates();
                                    if (itemTaxes.containsKey(rates.getName()) && rates.getName() != null) {
                                        ArrayList<com.foodkonnekt.clover.vo.Item> itemTx = (ArrayList) itemTaxes
                                                        .get(rates.getName());
                                        itemTx.add(item);
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                discountedTotal += Double.parseDouble(item.getPrice());
            }
        }

        if (convenienceFeeItem != null && isConvenienceFeeTaxable) {
            for (TaxRates defaultTax : defaultTaxRates) {
                if (defaultTax.getName() != null) {
                    ArrayList<com.foodkonnekt.clover.vo.Item> itemTx = (ArrayList) itemTaxes.get(defaultTax.getName());
                    itemTx.add(convenienceFeeItem);
                }
            }
        }

        if (deliveryItem != null) {
            discountedTotal += Double.parseDouble(deliveryItem.getPrice());
            if (deliveryFeeTaxable) {
                for (TaxRates defaultTax : defaultTaxRates) {
                    if (defaultTax.getName() != null) {
                        ArrayList<com.foodkonnekt.clover.vo.Item> itemTx = (ArrayList) itemTaxes.get(defaultTax
                                        .getName());
                        itemTx.add(deliveryItem);
                    }
                }
            }
        }

        if (merchant != null && merchant.getOwner() != null && merchant.getOwner().getPos() != null
                        && merchant.getOwner().getPos().getPosId() != IConstant.POS_FOODTRONIX) {
            for (TaxRates rates : taxRates) {
                ArrayList<com.foodkonnekt.clover.vo.Item> itemTx = (ArrayList) itemTaxes.get(rates.getName());
                double totalItemPrice = 0;
                if (!itemTx.isEmpty()) {
                    for (com.foodkonnekt.clover.vo.Item itemDto : itemTx) {
                        if (itemDto.getExtraCharge() != null && !itemDto.getExtraCharge() && itemDto != null
                                        && itemDto.getModifierPrice() != null && itemDto.getPrice() != null
                                        && itemDto.getAmount() != null) {
                            totalItemPrice = totalItemPrice
                                            + ((Double.parseDouble(itemDto.getPrice()) + Double.parseDouble(itemDto
                                                            .getModifierPrice())) * Double.parseDouble(itemDto
                                                            .getAmount()));
                        } else {
                            if (itemDto != null && itemDto.getPrice() != null) {
                                totalItemPrice = totalItemPrice + Double.parseDouble(itemDto.getPrice());
                            }
                        }
                    }
                }
                if (rates.getRate() != null) {
                    discountedTax = discountedTax + Math.round((totalItemPrice * rates.getRate() / 100) * 100.0)
                                    / 100.0;
                    taxNameAndItemPrice.put(rates.getName(), totalItemPrice);
                }
            }
        }else if(merchant != null && merchant.getOwner() != null && merchant.getOwner().getPos() != null
                && merchant.getOwner().getPos().getPosId() == IConstant.POS_FOODTRONIX && label!=null && !label.isEmpty())
        {
        	for (com.foodkonnekt.clover.vo.Item item : items) {
                Double modifierPrice = 0.0;
                if (!item.getExtraCharge()) {
                    if (item.getModifier() != null && !item.getModifier().isEmpty()) {
                        for (ModifierVO modifier : item.getModifier()) {
                            modifierPrice += Double.parseDouble(modifier.getPrice());
                        }
                        item.setModifierPrice(modifierPrice + "");
                    }

                    if (item.getItemPosId() != null && item.getIsPizza() != null && !item.getIsPizza()) {
                        Item dbItem = itemService.findItemByItemPosId(item.getItemPosId(),merchant.getId());
                        if (dbItem != null && dbItem.getPrice() != null) {
                           
                            if (dbItem.getTaxAble()) {
                                taxItem = taxItem + (dbItem.getPrice() + modifierPrice)
                                                * Integer.parseInt(item.getAmount());
                            }
                            if (dbItem.getAuxTaxAble()) {
                                auxTax = auxTax + (dbItem.getPrice() + modifierPrice) * Integer.parseInt(item.getAmount());
                            }
                        }
                    } else {
                        if (item.getItemid() != null) {
                            PizzaTemplate pizzaTemplate = pizzaTemplateRepository.findByIdAndMerchantId(
                                            Integer.parseInt(item.getItemid()), merchant.getId());
                            PizzaTemplateSize pizza = pizzaTemplateSizeRepository.findByPizzaSizeIdAndPizzaTemplateId(
                                            Integer.parseInt(item.getPizzaSizeId()), Integer.parseInt(item.getItemid()));
                            if (pizza != null) {
                                discountedSubTotal += (pizza.getPrice() + modifierPrice)
                                                * Double.parseDouble(item.getAmount());
                                if (pizzaTemplate != null && pizza != null) {
                                    if (pizzaTemplate.getTaxable() != null && pizzaTemplate.getTaxable()) {
                                        taxItem = taxItem + (pizza.getPrice() + modifierPrice)
                                                        * Double.parseDouble(item.getAmount());
                                    }
                                    if (pizzaTemplate.getAuxTaxable() != null && pizzaTemplate.getAuxTaxable()) {
                                        auxTaxTotal = auxTax + (pizza.getPrice() + modifierPrice)
                                                        * Double.parseDouble(item.getAmount());
                                    }
                                }
                            }
                        }
                    }
                } else {
                    
                }
            }
        	if(label!=null && !label.isEmpty())
        	{
            OrderType orderType = orderTypeRepository.findByMerchantIdAndLabel(merchant.getId(), label);
            if (orderType != null) {
            	if(orderType.getAuxTax()!=null){
            		 auxTax = auxTaxTotal * orderType.getAuxTax() / 100;
            	}
               
            	if(orderType.getSalesTax()!=null){
            		 discountedTax = taxItem * orderType.getSalesTax() / 100;
            	}
            }
        	}
        }
        double conveniencePercent = (discountedSubTotal*(ConvenienceFeePercent/100.0));
        conveniencePercent = Math.round(conveniencePercent * 100.0) / 100.0;
        auxTax=Math.round(auxTax * 100.0) / 100.0;
        discountedTax = Math.round(discountedTax * 100.0) / 100.0;
        discountedSubTotal = Math.round(discountedSubTotal * 100.0) / 100.0;
        discountedTotal += discountedSubTotal + discountedTax + auxTax + conveniencePercent;
        discountedTotal = Math.round(discountedTotal * 100.0) / 100.0;

        mapConvenienceFee = mapConvenienceFee + conveniencePercent;
        LOGGER.info(" ===discountedTotal "+discountedTotal);
        Map<String, Object> response = new HashMap<String, Object>();
        response.put("totalPrice", discountedTotal);
        response.put("subTotal", discountedSubTotal);
        response.put("Tax", discountedTax);
        response.put("ConvenienceFee", mapConvenienceFee);
        response.put("items", items);
        response.put("onlineFeeFlag", onlineFeeFlag);
        response.put("convenienceFeeItem", convenienceFeeItem);
        response.put("taxNameAndItemPrice", taxNameAndItemPrice);
        response.put("taxNameAndItemPrice", taxNameAndItemPrice);
        LOGGER.info("--------------------End   :: OrderControllerV2 : applyTaxAndConvenienceFee---------");
        return response;
    }

    @RequestMapping(value = "applyTaxForFoodTronix", method = RequestMethod.GET)
    public @ResponseBody String applyFoodTronixTax(@RequestParam String label, HttpSession session) {
    	
    	LOGGER.info("----------------Start :: OrderControllerV2 : applyTaxForFoodTronix------------------");
    	LOGGER.info(" ===label "+label);
        List<com.foodkonnekt.clover.vo.Item> orderItems = null;
        Map<String, Object> response = null;
        Merchant merchant = null;
        if (session.getAttribute("orderItems") != null) {
            orderItems = (List<com.foodkonnekt.clover.vo.Item>) session.getAttribute("orderItems");

            if (session.getAttribute("merchant") != null) {
                merchant = (Merchant) session.getAttribute("merchant");
                LOGGER.info("OrderControllerV2 :: applyTaxForFoodTronix : merchantId "+merchant.getId());
            }
            if (!orderItems.isEmpty() && merchant != null && merchant.getOwner() != null
                            && merchant.getOwner().getPos() != null && merchant.getOwner().getPos().getPosId() == 2
                            && label != null) {
                List<com.foodkonnekt.clover.vo.Item> newItems = new ArrayList<com.foodkonnekt.clover.vo.Item>(
                                orderItems);
                response = applyTaxForFoodTronix(newItems, (Merchant) session.getAttribute("merchant"), label);
            }
        }
        double tax = 0;
        double total = 0;
        double subTotal = 0;
        double convenienceFee = 0;
        double auxTax = 0;
        if (response != null) {
            tax = (Double) response.get("Tax");
            total = (Double) response.get("totalPrice");
            subTotal = (Double) response.get("subTotal");
            convenienceFee = (Double) response.get("ConvenienceFee");
            auxTax = (Double) response.get("auxTax");

        } else {

            response = new HashMap<String, Object>();
            response.put("subTotal", 0.0);
            response.put("totalPrice", 0.0);
            response.put("ConvenienceFee", 0.0);
            response.put("Tax", 0.0);
            response.put("auxTax", 0.0);

            session.setAttribute("Tax", 0.0);
            session.setAttribute("totalPrice", 0.0);
            session.setAttribute("subTotal", 0.0);
            session.setAttribute("ConvenienceFee", 0.0);
        }

        session.setAttribute("Tax", tax);
        session.setAttribute("totalPrice", total);
        session.setAttribute("subTotal", subTotal);
        session.setAttribute("auxTax", auxTax);
        session.setAttribute("ConvenienceFee", convenienceFee);

        Gson gson = new Gson();
        String jsonInString = gson.toJson(response);
        LOGGER.info("OrderControllerV2 :: applyTaxForFoodTronix : jsonInString "+jsonInString);
        LOGGER.info("----------------End :: OrderControllerV2 : applyTaxForFoodTronix------------------");
        return jsonInString;
    }

    public Map<String, Object> applyTaxForFoodTronix(List<com.foodkonnekt.clover.vo.Item> items, Merchant merchant,
                    String label) {
    	LOGGER.info("----------------Start :: OrderControllerV2 : applyTaxForFoodTronix------------------");
        com.foodkonnekt.clover.vo.Item convenienceFeeItem = null;
        double discountedSubTotal = 0;
        double discountedTax = 0;
        double discountedTotal = 0;
        boolean flag = false;
        Boolean isConvenienceFeeTaxable = false;
        double mapConvenienceFee = 0;
        double auxTax = 0;
        double itemTax = 0;
        ConvenienceFee convenienceFee = null;
        for (com.foodkonnekt.clover.vo.Item item : items) {
            if (item.getExtraCharge()) {
                flag = true;
                mapConvenienceFee = Double.parseDouble(item.getPrice());
            }
        }
        if (!flag) {
            
            List<ConvenienceFee> convenienceFees = convenienceFeeRepository.findByMerchantId(merchant.getId());
            LOGGER.info("OrderControllerV2 :: applyTaxForFoodTronix : MerchantId "+merchant.getId());
            if (convenienceFees != null && convenienceFees.size() > 0) {
                int index = convenienceFees.size() - 1;
                convenienceFee = convenienceFees.get(index);
            }

            if (convenienceFee != null && convenienceFee.getConvenienceFee() != null
                            && Double.valueOf(convenienceFee.getConvenienceFee()) > 0) {

                convenienceFeeItem = new com.foodkonnekt.clover.vo.Item();

                convenienceFeeItem.setItemPosId(convenienceFee.getConvenienceFeeLineItemPosId());

                convenienceFeeItem.setPrice(convenienceFee.getConvenienceFee());

                mapConvenienceFee = Double.parseDouble(convenienceFee.getConvenienceFee());
                convenienceFeeItem.setAmount("1");
                convenienceFeeItem.setExtraCharge(true);
               //items.add(convenienceFeeItem);
                if (convenienceFee.getIsTaxable() != null) {
                    if (convenienceFee.getIsTaxable() == 1) {
                        isConvenienceFeeTaxable = true;

                    } else {
                        isConvenienceFeeTaxable = false;
                    }
                } else {
                    isConvenienceFeeTaxable = false;
                }
            }
        }
        System.out.println(isConvenienceFeeTaxable);
        LOGGER.info("OrderControllerV2 :: applyTaxForFoodTronix : isConvenienceFeeTaxable "+isConvenienceFeeTaxable);
        for (com.foodkonnekt.clover.vo.Item item : items) {
            Double modifierPrice = 0.0;
            if (!item.getExtraCharge()) {
                if (item.getModifier() != null && !item.getModifier().isEmpty()) {
                    for (ModifierVO modifier : item.getModifier()) {
                        modifierPrice += Double.parseDouble(modifier.getPrice());
                    }
                    item.setModifierPrice(modifierPrice + "");
                }

                if (item.getItemPosId() != null && item.getIsPizza() != null && !item.getIsPizza()) {
                    Item dbItem = itemService.findItemByItemPosId(item.getItemPosId(),merchant.getId());
                    if (dbItem != null && dbItem.getPrice() != null) {
                        discountedSubTotal += (dbItem.getPrice() + modifierPrice)
                                        * Double.parseDouble(item.getAmount());
                        if (dbItem.getTaxAble()) {
                            itemTax = itemTax + (dbItem.getPrice() + modifierPrice)
                                            * Integer.parseInt(item.getAmount());
                        }
                        if (dbItem.getAuxTaxAble()) {
                            auxTax = auxTax + (dbItem.getPrice() + modifierPrice) * Integer.parseInt(item.getAmount());
                        }
                    }
                } else {
                    if (item.getItemid() != null) {
                        PizzaTemplate pizzaTemplate = pizzaTemplateRepository.findByIdAndMerchantId(
                                        Integer.parseInt(item.getItemid()), merchant.getId());
                        PizzaTemplateSize pizza = pizzaTemplateSizeRepository.findByPizzaSizeIdAndPizzaTemplateId(
                                        Integer.parseInt(item.getPizzaSizeId()), Integer.parseInt(item.getItemid()));
                        if (pizza != null) {
                            discountedSubTotal += (pizza.getPrice() + modifierPrice)
                                            * Double.parseDouble(item.getAmount());
                            if (pizzaTemplate != null && pizza != null) {
                                if (pizzaTemplate.getTaxable() != null && pizzaTemplate.getTaxable()) {
                                    itemTax = itemTax + (pizza.getPrice() + modifierPrice)
                                                    * Double.parseDouble(item.getAmount());
                                }
                                if (pizzaTemplate.getAuxTaxable() != null && pizzaTemplate.getAuxTaxable()) {
                                    auxTax = auxTax + (pizza.getPrice() + modifierPrice)
                                                    * Double.parseDouble(item.getAmount());
                                }
                            }
                        }
                    }
                }
            } else {
                discountedTotal += Double.parseDouble(item.getPrice());
                itemTax += Double.parseDouble(item.getPrice());
            }
        }
LOGGER.info(" ===label "+label);
        OrderType orderType = orderTypeRepository.findByMerchantIdAndLabel(merchant.getId(), label);
        if (orderType != null) {
        	if(orderType.getAuxTax()!=null){
        		 auxTax = auxTax * orderType.getAuxTax() / 100;
        	}
           
        	if(orderType.getSalesTax()!=null){
        		 discountedTax = itemTax * orderType.getSalesTax() / 100;
        	}
        }

        double ConvenienceFeePercent =  (convenienceFee!=null && convenienceFee.getConvenienceFeePercent()!=null) ? convenienceFee.getConvenienceFeePercent() : 0;
        double conveniencePercent = ((discountedSubTotal)*(ConvenienceFeePercent/100.0));
        mapConvenienceFee = mapConvenienceFee + conveniencePercent;
        
        discountedTax = Math.round(discountedTax * 100.0) / 100.0;
        discountedSubTotal = Math.round(discountedSubTotal * 100.0) / 100.0;
        discountedTotal += discountedSubTotal + discountedTax + auxTax + mapConvenienceFee;
        discountedTotal = Math.round(discountedTotal * 100.0) / 100.0;
LOGGER.info(" ===mapConvenienceFee "+mapConvenienceFee+" discountedTotal "+discountedTotal);
        Map<String, Object> response = new HashMap<String, Object>();
        response.put("totalPrice", discountedTotal);
        response.put("subTotal", discountedSubTotal);
        response.put("Tax", discountedTax);
        response.put("auxTax", auxTax);
        response.put("ConvenienceFee", mapConvenienceFee);
        LOGGER.info("----------------End :: OrderControllerV2 : applyTaxForFoodTronix------------------");
        return response;

    }

    @RequestMapping(value = "/myOrdersV2", method = { RequestMethod.GET })
    public String showMyOrder(ModelMap model, Map<String, Object> map, HttpServletRequest request,
                    @RequestParam(required = false) String message) {
    	LOGGER.info("----------------Start :: OrderControllerV2 : myOrdersV2------------------");
        HttpSession session = request.getSession();
        Merchant merchant = (Merchant) session.getAttribute("merchant");
        Customer customer = (Customer) session.getAttribute("customer");
        
        if (customer != null && merchant != null) {
        	
        	 LOGGER.info("OrderControllerV2 : applyTaxes : merchantId "+merchant.getId());
             LOGGER.info("OrderControllerV2 : applyTaxes : customerId "+customer.getId());
        	
            String orderStatus = (String) session.getAttribute("failOrder");
            if ("Y".equals(orderStatus)) {
                model.addAttribute("orderStatus", "Y");
                LOGGER.info("OrderControllerV2 : myOrdersV2 : orderStatus "+"Y");
            }
            session.setAttribute("failOrder", "N");
            LOGGER.info("OrderControllerV2 : myOrdersV2 : failOrder "+"N");
            model.addAttribute("orderList", orderService.getAllOrder(customer.getId(), merchant.getId()));
            model.addAttribute("customer",customerService.getCustomerProfile(customer.getId()));
            LOGGER.info("----------------End :: OrderControllerV2 : myOrdersV2------------------");
            return "myOrderV2";
        } else {
        	LOGGER.info("----------------End :: OrderControllerV2 : myOrdersV2------------------");
            return "redirect:https://www.foodkonnekt.com";
        }
    }

    @RequestMapping(value = "/getPizzaTemplate", method = RequestMethod.GET)
    @ResponseBody
    public String getPizzaTemplate(@RequestParam Integer merchantId, @RequestParam Integer categoryId) {
    	LOGGER.info("----------------Start :: OrderControllerV2 : getPizaTemplate------------------");
        Map<String, Object> response = new HashMap<String, Object>();
        List<PizzaTemplate> pizzaTemplates = new ArrayList<PizzaTemplate>();
        List<PizzaTemplate> pizzaTemplatesList = null;
        List<PizzaTemplateCategory> pizzaTemplateCategories = null;
        List<PizzaTemplateSize> pizzaTemplateSizes = null;
        Integer count=0;
        try {
            if (merchantId != null && categoryId != null) {
                pizzaTemplatesList = new ArrayList<PizzaTemplate>();
              //  pizzaTemplates=pizzaService.findPizzaTemplateByMerchantIdAndCategoryId(merchantId,categoryId);
               // pizzaTemplates = pizzaService.findPizzaTemplateByMerchantId(merchantId);
                pizzaTemplateCategories = pizzaTemplateCategoryRepository.findByCategoryIdAndPizzaTemplateActiveAndTax(categoryId,1,1);
                if(pizzaTemplateCategories!=null && pizzaTemplateCategories.size()>0)
                {
                	for (int i=0 ;i<pizzaTemplateCategories.size();i++) {
						if(pizzaTemplateCategories.get(i)!=null && pizzaTemplateCategories.get(i).getPizzaTemplate()!=null)
						{
							List<PizzaTemplateCategory> pizzaTemplateCategor=new ArrayList<PizzaTemplateCategory>();
                            pizzaTemplateCategor.add((pizzaTemplateCategories.get(i)));
							pizzaTemplates.add(pizzaTemplateCategories.get(i).getPizzaTemplate());
						    pizzaTemplateCategories.get(i).setPizzaTemplate(null);
						    pizzaTemplateCategories.get(i).setCategory(null);
						    pizzaTemplates.get(count++).setPizzaTemplateCategories(pizzaTemplateCategor);
                		
						}
					}
                	
                	
                }
               if(pizzaTemplates!=null && pizzaTemplates.size()>0)
               {
               List<Integer> pizzaTemplatesids=  new ArrayList<Integer>();
               for(int k=0;k<pizzaTemplates.size();k++)
               {
            	   pizzaTemplates.get(k).setMerchant(null);
            	   pizzaTemplatesids.add(pizzaTemplates.get(k).getId());
               }
               if(pizzaTemplatesids!=null && pizzaTemplatesids.size()>0)
              
               pizzaTemplateSizes = pizzaTemplateSizeRepository.findByPizzaTemplateIdsAndSizeActive(pizzaTemplatesids,1,1);
               for (PizzaTemplateSize pizzaTemplateSize : pizzaTemplateSizes) {
                   if(pizzaTemplateSize.getPizzaSize()!=null) {
                       pizzaTemplateSize.getPizzaSize().setMerchant(null);
                       pizzaTemplateSize.getPizzaSize().setPizzaTemplates(null);
                       pizzaTemplateSize.getPizzaSize().setCategories(null);
                       pizzaTemplateSize.getPizzaSize().setPizzaToppingSizes(null);
                       pizzaTemplateSize.getPizzaSize().setPizzaCrustSizes(null);
                       pizzaTemplateSize.getPizzaTemplate().setCategory(null);
                       pizzaTemplateSize.getPizzaTemplate().setMerchant(null);
                       pizzaTemplateSize.getPizzaTemplate().setPizzaCrust(null);
                       pizzaTemplateSize.getPizzaTemplate().setPizzaToppings(null);
                       pizzaTemplateSize.getPizzaTemplate().setPizzaTemplateToppings(null);
                       pizzaTemplateSize.getPizzaTemplate().setPizzaSizes(null);
                       pizzaTemplateSize.getPizzaTemplate().setPizzaTemplateCategories(null);
                       pizzaTemplateSize.getPizzaTemplate().setPizzaTemplateSizes(null);
                       pizzaTemplateSize.getPizzaTemplate().setPizzaTemplateTaxs(null);
                       pizzaTemplateSize.getPizzaTemplate().setTaxes(null);
                       pizzaTemplateSize.setTopping(null);
                      
                     
                   }
               }
                for (PizzaTemplate pizzaTemplate : pizzaTemplates) {
                   
                    if (pizzaTemplate != null && pizzaTemplate.getId() != null) {
                         double pizzaSizePrice = -1;
                         
                         int pizzaSizeId = 0;
                         String pizzaSizeName = "";
                         if (pizzaTemplate.getId() != null) {
                    	//pizzaTemplateSizes = pizzaService.findPizzaTemplateSizeByTemplateId(pizzaTemplate.getId());
                    	List<PizzaTemplateSize> pizzaTemplateSize=new ArrayList<PizzaTemplateSize>();
                        if (pizzaTemplateSizes != null && !pizzaTemplateSizes.isEmpty()
                                        && pizzaTemplateSizes.size() > 0) {
                            for (int i = 0; i < pizzaTemplateSizes.size(); i++) {
                                if(pizzaTemplate.getPizzaTemplateCategories()!=null && pizzaTemplate.getId().equals(pizzaTemplateSizes.get(i).getPizzaTemplate().getId()) && (pizzaSizePrice==-1 ||pizzaSizePrice >= pizzaTemplateSizes.get(i).getPrice())) {
                                    pizzaSizeId = pizzaTemplateSizes.get(i).getPizzaSize().getId();
                                    pizzaSizePrice = pizzaTemplateSizes.get(i).getPrice();
                                    pizzaSizeName = pizzaTemplateSizes.get(i).getPizzaSize().getDescription();
                                    pizzaTemplateSize.add(pizzaTemplateSizes.get(i));
                                }else  if(pizzaTemplate.getId().equals(pizzaTemplateSizes.get(i).getPizzaTemplate().getId()))
                                	pizzaTemplateSize.add(pizzaTemplateSizes.get(i));
                            }
                            pizzaTemplate.setMinimumTemplateSizeId(pizzaSizeId);
                            pizzaTemplate.setMinimumTemplateSizePrice(pizzaSizePrice);
                            pizzaTemplate.setMinimumPizzaSizeName(pizzaSizeName);
                        }
                        pizzaTemplate.setPizzaTemplateSizes(pizzaTemplateSize);
                if (pizzaTemplate.getCategory() != null && pizzaTemplate.getCategory().getMerchant() != null) {
                  
                  pizzaTemplate.getCategory().setMerchant(null);
                  pizzaTemplate.getCategory().setItems(null);
                  pizzaTemplate.getCategory().setPizzaSizes(null);
                  pizzaTemplate.getCategory().setCategoryTimings(null);
                } 
              } 
                    pizzaTemplate.setTaxes(null);
                    pizzaTemplate.setPizzaSizes(null);
                    if(pizzaTemplate.getMerchant()!=null)
                    	pizzaTemplate.getMerchant().setItems(null);
                    if(pizzaTemplate.getMinimumTemplateSizePrice()!=-1)
                    pizzaTemplatesList.add(pizzaTemplate);
                
            }
            }
            }}
        } catch (Exception exception) {
            if (exception != null) {
            	LOGGER.error("OrderControllerV2 :: getPizzaTemplate : Exception "+exception);
                MailSendUtil.sendExceptionByMail(exception,environment);
            LOGGER.error("OrderControllerV2 :: getPizzaTemplate : Exception "+exception);
                MailSendUtil.sendExceptionByMail(exception,environment);
            }
            exception.printStackTrace();
        }
        response.put("pizzaTemplates", pizzaTemplatesList);
        LOGGER.info("----------------End :: OrderController : getPizzaTemplate------------------");
        Gson gson = new Gson();
        return gson.toJson(response);
    }

    @RequestMapping(value = "/getPizzaTopping", method = RequestMethod.GET)
    @ResponseBody
    public String getPizzaTopping(@RequestParam Integer merchantId, @RequestParam Integer templateId,
                    @RequestParam Integer sizeId) {
    	LOGGER.info("----------------Start :: OrderController : getPizzaTopping------------------");
        Map<String, Object> response = new HashMap<String, Object>();
        List<PizzaToppingSize> toppingSizes = null;
        List<PizzaTemplateTopping> pizzaTemplateToppings=null;
        List<PizzaTopping> pizzaToppings = null;
        List<PizzaTopping> pizzaToppingList = null;
        List<Integer> pizzaSizes = new ArrayList<Integer>();
        
        if (merchantId != null) {
            pizzaToppingList = new ArrayList<PizzaTopping>();
            try {
                PizzaTemplate pizzaTemplate = pizzaService.findByTemplateIdAndMerchantId(templateId, merchantId,sizeId);
                LOGGER.info("OrderController :: getPizzaTopping : merchantId "+merchantId);
                if (pizzaTemplate != null) {
                    pizzaToppings = pizzaTemplate.getPizzaToppings();
                }
                if (pizzaToppings != null && !pizzaToppings.isEmpty()) {
                	for(PizzaTopping pizzatopingss :pizzaToppings)
                	{
                		if(pizzatopingss.getId()!=null)
                		pizzaSizes.add(pizzatopingss.getId());
                	}
                    toppingSizes = pizzaService.findByToppingIdAndSizeId(pizzaSizes, sizeId);
                    pizzaTemplateToppings = pizzaTemplateToppingRepository.findByPizzaTemplateIdAndPizzaToppingId(templateId,pizzaSizes);
                    for (PizzaTopping pizzaTopping : pizzaToppings) {
                      Boolean flag = Boolean.valueOf(false);
                        if (pizzaTopping.getId() != null && sizeId != null && pizzaTopping.getActive() == 1) {
                            if (toppingSizes != null && !toppingSizes.isEmpty()) {
                            	List<PizzaToppingSize> PizzaToppingSize=new ArrayList<PizzaToppingSize>();
                                for (PizzaToppingSize toppingSize : toppingSizes) {
                                    if(toppingSize.getPizzaSize() != null && toppingSize.getPizzaTopping()!=null && toppingSize.getPizzaTopping().getId().equals(pizzaTopping.getId())) {
                                    	toppingSize.getPizzaSize().setMerchant(null);
                                    	toppingSize.getPizzaSize().setCategories(null);
                                    	toppingSize.getPizzaSize().setPizzaToppingSizes(null);
                                    	toppingSize.getPizzaSize().setPizzaCrustSizes(null);
                                    	toppingSize.getPizzaSize().setPizzaTemplates(null);
                                        PizzaToppingSize.add(toppingSize);
                                        toppingSize.setPizzaTopping(null);
                                    }
                                }
                                if(pizzaTemplateToppings !=null && pizzaTemplateToppings.size() > 0)
                                for (PizzaTemplateTopping pizzaTemplateTopping : pizzaTemplateToppings) {
                                 if(pizzaTemplateTopping!=null && pizzaTemplateTopping.getActive() == true && pizzaTemplateTopping.getPizzaTopping().getId().equals(pizzaTopping.getId())){
                                	pizzaTemplateTopping.getPizzaTemplate().setCategory(null);
                                	pizzaTemplateTopping.getPizzaTemplate().setMerchant(null);
                                	pizzaTemplateTopping.getPizzaTemplate().setPizzaSizes(null);
                                	pizzaTemplateTopping.getPizzaTemplate().setPizzaTemplateCategories(null);
                                	pizzaTemplateTopping.getPizzaTemplate().setPizzaCrust(null);
                                	pizzaTemplateTopping.getPizzaTemplate().setPizzaTemplateSizes(null);
                					pizzaTemplateTopping.getPizzaTemplate().setPizzaTemplateToppings(null);
                					pizzaTemplateTopping.getPizzaTemplate().setTaxes(null);
                					pizzaTemplateTopping.getPizzaTopping().setMerchant(null);
                					pizzaTemplateTopping.getPizzaTopping().setPizzaTemplateToppings(null);
                					pizzaTemplateTopping.getPizzaTopping().setPizzaToppingSizes(null);
                				
                					if(pizzaTemplateTopping.isIncluded()==true)
                					{
                						for (PizzaToppingSize pizzaToppingSize2 : PizzaToppingSize) {
                							pizzaToppingSize2.setPrice(0);
										}
                					}
                					pizzaTopping.setPizzaTemplateTopping(pizzaTemplateTopping);
                					
                					 pizzaTopping.setPizzaToppingSizes(PizzaToppingSize);
                                     pizzaTopping.setMerchant(null);
                                     pizzaToppingList.add(pizzaTopping);
                                     pizzaTemplateTopping=null;
                                     flag = Boolean.valueOf(true);
                                     break;
                                }
                            }
              if (!flag.booleanValue())
              {
                PizzaTemplateTopping pizzaTemplateTopping = new PizzaTemplateTopping();
                pizzaTemplateTopping.setIncluded(true);
                pizzaTemplateTopping.setIncluded(false);
                pizzaTopping.setPizzaTemplateTopping(pizzaTemplateTopping);
                pizzaTopping.setPizzaToppingSizes(PizzaToppingSize);
                pizzaTopping.setMerchant(null);
                pizzaToppingList.add(pizzaTopping);
                            }
                        }
                    }
                }
                }     
            } catch (Exception exception) {
                if (exception != null) {
                	LOGGER.error("OrderControllerV2 :: getPizzaTopping : Exception "+exception);
                    MailSendUtil.sendExceptionByMail(exception,environment);
                }
                exception.printStackTrace();
            }
            response.put("pizzaTopping", pizzaToppingList);
        }
        Gson gson = new Gson();
        LOGGER.info("----------------End :: OrderControllerV2 : getPizzaTopping-----------------");
        return gson.toJson(response);
    }

    @RequestMapping(value = "/getPizzaTemplateCrust", method = RequestMethod.GET)
    @ResponseBody
    public String getPizzaTemplateCrust(@RequestParam Integer merchantId, @RequestParam Integer templateId,
                    @RequestParam Integer sizeId) {
    	LOGGER.info("----------------Start :: OrderControllerV2 : getPizzaTemplateCrust------------------");
        Map<String, Object> response = new HashMap<String, Object>();
        List<PizzaCrustSizes> crustSizes = null;
        List<PizzaCrust> pizzaCrust = null;
        List<PizzaCrust> pizzaCrustList = null;
        Double minimumCrustPrice = 0.0;
        Integer minimumCrustId = 0;
        String minimumCrustName = "";
        String minimumCrustPosId = "";
        if (merchantId != null) {
            pizzaCrustList = new ArrayList<PizzaCrust>();
            try {
                PizzaTemplate pizzaTemplate = pizzaService.findByTemplateIdAndMerchantId(templateId, merchantId,sizeId);
                LOGGER.info("OrderControllerV2 : getPizzaTemplateCrust : merchantId "+merchantId);
                if (pizzaTemplate != null && pizzaTemplate.getPizzaCrust() != null && !pizzaTemplate.getPizzaCrust().isEmpty()) {
                    pizzaCrust = pizzaTemplate.getPizzaCrust();
                }
                if (pizzaCrust != null && !pizzaCrust.isEmpty() && pizzaCrust.size()>0) {
                    for (PizzaCrust crust : pizzaCrust) {
                    	LOGGER.info("OrderControllerV2 : getPizzaTemplateCrust :crust.Id :  "+crust.getId());
                        if (crust.getId() != null && sizeId != null && new Integer(crust.getActive()) != null && crust.getActive() == 1) {
                            crustSizes = pizzaService.findByCrustIdAndSizeId(crust.getId(), sizeId);
                            if (crustSizes != null && !crustSizes.isEmpty() && crustSizes.size()>0) {
                                minimumCrustPrice = crustSizes.get(0).getPrice();
                                for (PizzaCrustSizes crustSize : crustSizes) {
                                	LOGGER.info("OrderControllerV2 : getPizzaTemplateCrust :minimumCrustPrice :  "+minimumCrustPrice+" : crustSize.getPrice() :"+crustSize.getPrice());
                                    if (minimumCrustPrice <= crustSize.getPrice()) {
                                        minimumCrustPrice = crustSize.getPrice();

                                        if (crustSize != null && crustSize.getPizzaCrust() != null
                                                        && crustSize.getPizzaCrust().getId() != null) {
                                            minimumCrustId = crustSize.getPizzaCrust().getId();
                                        }
                                        minimumCrustPosId = crust.getPosPizzaCrustId();
                                        if (crustSize.getPizzaCrust() != null) {
                                            minimumCrustName = crustSize.getPizzaCrust().getDescription();
                                        }
                                    }
                                    crustSize.setPizzaCrust(null);
                                    if (crustSize.getPizzaSize() != null) {
                                        crustSize.getPizzaSize().setPizzaTemplates(null);
                                        crustSize.getPizzaSize().setPizzaToppingSizes(null);
                                        crustSize.getPizzaSize().setCategories(null);
                                    }
                                }
                            } else {
                                crust = null;
                            }
                            if (crust != null) {
                                crust.setPizzaCrustSizes(crustSizes);
                                crust.setMerchant(null);
                                pizzaCrustList.add(crust);
                            }
                        }
                    }
                }
            } catch (Exception exception) {
                if (exception != null) {
                	LOGGER.error("OrderControllerV2 :: getPizzaTemplateCrust : Exception "+exception);
                    MailSendUtil.sendExceptionByMail(exception,environment);
                }
                exception.printStackTrace();
            }
            response.put("pizzaCrust", pizzaCrustList);
            response.put("minimumPizzaCrustId", minimumCrustId);
            response.put("minimumPizzaCrustName", minimumCrustName);
            response.put("minimumPizzaCrustPrice", minimumCrustPrice);
            response.put("minimumCrustPosId", minimumCrustPosId);
        }
        Gson gson = new Gson();
        LOGGER.info("----------------End :: OrderControllerV2 : getPizzaTemplateCrust------------------");
        return gson.toJson(response);
    }

    @RequestMapping(value = "/getPizzaTemplateCrustById", method = RequestMethod.GET)
    @ResponseBody
    public String getPizzaTemplateCrustById(@RequestParam Integer merchantId, @RequestParam Integer crustId,
                    @RequestParam Integer sizeId) {
    	LOGGER.info("----------------Start :: OrderControllerV2 : getPizzaTemplateCrustById------------------");
        Map<String, Object> response = new HashMap<String, Object>();
        PizzaCrustSizes crustSizes = pizzaService.findByCrustId(crustId, sizeId);
        LOGGER.info("OrderControllerV2 :: getPizzaTemplateCrustById : merchantId "+merchantId);
        LOGGER.info("OrderControllerV2 :: getPizzaTemplateCrustById : curstId "+crustId);
        LOGGER.info("OrderControllerV2 :: getPizzaTemplateCrustById : sizeId "+sizeId);
        if (crustSizes != null) {
            response.put("minimumPizzaCrustId", crustSizes.getPizzaCrust().getId());
            response.put("minimumPizzaCrustName", crustSizes.getPizzaCrust().getDescription());
            response.put("minimumPizzaCrustPrice", crustSizes.getPrice());
            response.put("minimumCrustPosId", crustSizes.getPizzaCrust().getPosPizzaCrustId());
        }
        Gson gson = new Gson();
        LOGGER.info("----------------End :: OrderControllerV2 : getPizzaTemplateCrustById------------------");
        return gson.toJson(response);
    }

    String checkDeliveryTimingStatus(Integer merchantId, String timeZoneCode, int time) {
    	LOGGER.info("----------------Start :: OrderControllerV2 : checkDeliveryTimingStatus------------------");
LOGGER.info(" ===merchantId "+merchantId+" timeZoneCode "+timeZoneCode+" time "+time);
        List<DeliveryOpeningClosingTime> openingClosingTimes = zoneService.findDeliveryOpeningClosingHoursByMerchantId(
                        merchantId, timeZoneCode);

        List<DeliveryOpeningClosingTime> nextOpeningClosingTimes = zoneService
                        .findNextOpeningDeliveryClosingHoursByMerchantId(merchantId, timeZoneCode);
        if (openingClosingTimes != null) {
            if (!openingClosingTimes.isEmpty()) {
            	LOGGER.info("----------------End :: OrderControllerV2 : checkDeliveryTimingStatus------------------");

                return OrderUtil.checkDeliveryOpeningClosingHour(openingClosingTimes, time, timeZoneCode,
                                nextOpeningClosingTimes,environment);
            } else {
            	LOGGER.info("----------------End :: OrderControllerV2 : checkDeliveryTimingStatus------------------");
            	LOGGER.info("--- :: OrderControllerV2 : checkDeliveryTimingStatus :: returns N");
                return "N";
            }
        } else {
        	LOGGER.info("----------------End :: OrderControllerV2 : checkDeliveryTimingStatus------------------");
        	LOGGER.info("--- :: OrderControllerV2 : checkDeliveryTimingStatus :: returns N");
            return "N";
        }

    }

    /**
     * Find opening hour for future order
     * 
     * @param futureDate
     * @return String
     */
    @RequestMapping(value = "/getFutureDateOpeningTimeV2", method = RequestMethod.GET)
    @ResponseBody
    public List<String> findFuturOpeningtime(HttpServletRequest request,
                    @RequestParam(value = "futureDate") String futureDate,
                    @RequestParam(value = "orderType") String orderType) {
        try {
        	LOGGER.info("----------------Start :: OrderControllerV2 : getFutureDateOpeningTimeV2------------------");
            HttpSession session = request.getSession();
            Merchant merchant = (Merchant) session.getAttribute("merchant");
            if (merchant != null) {
            	LOGGER.info("OrderControllerV2 :: getPizzaTemplateCrustById : merchantId "+merchant.getId());
                return businessService.findFutureOrderOpeningHours(futureDate, merchant.getId(), orderType);
            }
            LOGGER.info("----------------End :: OrderControllerV2 : getFutureDateOpeningTimeV2------------------");
            return new ArrayList<String>();
        } catch (Exception exception) {
            if (exception != null) {
            	LOGGER.error("OrderControllerV2 :: getPizzaTemplateCrustById : Exception "+exception);
                MailSendUtil.sendExceptionByMail(exception,environment);
            }
            exception.printStackTrace();
            LOGGER.info("----------------End :: OrderControllerV2 : getFutureDateOpeningTimeV2------------------");
            return new ArrayList<String>();
        }
    }

    @RequestMapping(value = "/getFutureDatesV2", method = RequestMethod.GET)
    @ResponseBody
    public List<String> getFutureDates(HttpServletRequest request) {
        try {
        	LOGGER.info("----------------Start :: OrderControllerV2 : getFutureDatesV2------------------");
            HttpSession session = request.getSession();
            Merchant merchant = (Merchant) session.getAttribute("merchant");
            if (merchant != null) {
            	LOGGER.info("OrderControllerV2 :: getFutureDatesV2 : merchantId "+merchant.getId());
                return businessService.findFutureDates(merchant);
            }
            LOGGER.info("----------------End :: OrderControllerV2 : getFutureDatesV2------------------");
            return new ArrayList<String>();
        } catch (Exception exception) {
            if (exception != null) {
            	LOGGER.error("OrderControllerV2 :: getPizzaTemplateCrustById : Exception "+exception);
                MailSendUtil.sendExceptionByMail(exception,environment);
            }
            exception.printStackTrace();
            LOGGER.info("----------------End :: OrderControllerV2 : getFutureDatesV2------------------");
            return new ArrayList<String>();
        }
    }

    @RequestMapping(value = "/applyTaxes", method = RequestMethod.POST)
    public @ResponseBody String applyTaxes(@RequestBody com.foodkonnekt.clover.vo.Item deliveryItem,
                    @RequestParam Boolean deliveryFeeTaxable,@RequestParam(required=false) String label, HttpSession session, ModelMap model) {
    	LOGGER.info("----------------Start :: OrderControllerV2 : applyTaxes------------------");
        List<com.foodkonnekt.clover.vo.Item> orderItems = null;
        Map<String, Object> response = null;
        Merchant merchant = null;
        Customer customer = null;
        if (session.getAttribute("orderItems") != null) {
            orderItems = (List<com.foodkonnekt.clover.vo.Item>) session.getAttribute("orderItems");
            if (session.getAttribute("merchant") != null) {
                merchant = (Merchant) session.getAttribute("merchant");
                if (merchant != null) {
                	LOGGER.info("OrderControllerV2 :: applyTaxes : merchantId "+merchant.getId());
                    model.addAttribute("paymentModes", orderService.findPaymentMode(merchant.getId()));

                    PickUpTime pickUpTime = merchantService.findPickupTime(merchant.getId());
                    if (pickUpTime != null && pickUpTime.getMinOrderAmount() != null
                                    && pickUpTime.getMinOrderAmount() > 0) {
                        model.addAttribute("pickUpMinOrderAmount", pickUpTime.getMinOrderAmount());
                    } else {
                        model.addAttribute("pickUpMinOrderAmount", 0.0);
                    }

                    PaymentMode creditCardMode = businessService.findByMerchantIdAndLabel(merchant.getId(),
                                    "Credit Card");
                    if (creditCardMode != null && creditCardMode.getMinOrderAmount() != null
                                    && creditCardMode.getMinOrderAmount() > 0) {
                        model.addAttribute("ccMinOrderAmount", creditCardMode.getMinOrderAmount());
                    } else {
                        model.addAttribute("ccMinOrderAmount", 0.0);
                    }

                    String avgDeliveryTime = null;
                    Calendar deliveryAvgTimeCalendar = Calendar.getInstance();
                    String timeZoneCode = "America/Chicago";
                    if (merchant != null && merchant.getTimeZone() != null
                                    && merchant.getTimeZone().getTimeZoneCode() != null) {
                        timeZoneCode = merchant.getTimeZone().getTimeZoneCode();
                    }

                    List<Zone> zones = zoneService.findZoneByMerchantIdAndStatus(merchant.getId());
                    try {
                        if (zones != null) {
                            if (!zones.isEmpty() && zones.size() > 0) {

                                avgDeliveryTime = zones.get(0).getAvgDeliveryTime();
                                if (avgDeliveryTime != null) {
                                    java.util.Date deliveryAvgTime = new SimpleDateFormat("HH:mm:ss").parse("00:"
                                                    + avgDeliveryTime + ":00");
                                    deliveryAvgTimeCalendar.setTime(deliveryAvgTime);
                                }

                                if (merchant.getAllowDeliveryTiming() != null
                                                && merchant.getAllowDeliveryTiming() == IConstant.BOOLEAN_TRUE) {
                                    model.addAttribute("zoneStatus",
                                                    checkDeliveryTimingStatus(merchant.getId(), timeZoneCode, 0));
                                } else {
                                	LOGGER.info(" ===zone status = Y ");
                                    model.addAttribute("zoneStatus", "Y");
                                }
                            } else {
                            	LOGGER.info(" ===zone status = N ");

                                model.addAttribute("zoneStatus", "N");
                            }
                        } else {
                        	LOGGER.info(" ===zone status = N ");

                            model.addAttribute("zoneStatus", "N");
                        }
                    } catch (Exception e) {
                    	LOGGER.error("OrderControllerV2 :: applyTaxes : Exception "+e);
                        MailSendUtil.sendExceptionByMail(e,environment);

                    }
                }
            }
            if (session.getAttribute("customer") != null && merchant != null && merchant.getId() != null) {
                customer = (Customer) session.getAttribute("customer");
                LOGGER.info("OrderControllerV2 :: applyTaxes : customerId "+customer.getId());
                model.addAttribute("customerAddresses",
                                customerService.findAllCustomerAddress(customer.getId(), merchant.getId()));
            }
            if (!orderItems.isEmpty()) {
                List<com.foodkonnekt.clover.vo.Item> newItems = new ArrayList<com.foodkonnekt.clover.vo.Item>(
                                orderItems);
                response = applyTaxAndConvenienceFee(newItems, (Merchant) session.getAttribute("merchant"),
                                deliveryItem, deliveryFeeTaxable,label);
            }
        }
        model.addAttribute("merchantTaxes", orderService.findMerchantTaxs(merchant.getId()));
        double tax = 0;
        double total = 0;
        double subTotal = 0;
        double convenienceFee = 0;
        double discount = 0;
        if (response != null) {
            tax = (Double) response.get("Tax");
            total = (Double) response.get("totalPrice");
            subTotal = (Double) response.get("subTotal");
            convenienceFee = (Double) response.get("ConvenienceFee");
            LOGGER.info(" ===tax "+tax+" total "+total+"subTotal : "+subTotal+" convenienceFee : "+convenienceFee+" discount : "+discount);

            // session.setAttribute("onlineFeeFlag", response.get("ConvenienceFee"));
            // session.setAttribute("convenienceFeeItem", response.get("convenienceFeeItem"));
            model.addAttribute("taxNameAndItemPrice", new Gson().toJson(response.get("taxNameAndItemPrice")));
        }

        /*
         * session.setAttribute("Tax", tax); session.setAttribute("totalPrice", total); session.setAttribute("subTotal",
         * subTotal); session.setAttribute("ConvenienceFee", convenienceFee); session.setAttribute("orderDiscount",
         * discount);
         */
        Gson gson = new Gson();
        String jsonInString = gson.toJson(response);
        LOGGER.info("OrderControllerV2 : jsonInString : "+jsonInString);
        LOGGER.info("----------------End :: OrderControllerV2 : applyTaxes------------------");
        return jsonInString;
    }
    
    
    @RequestMapping(value="/healthCheckForPayeezy",method= RequestMethod.GET)
	public @ResponseBody String healthCheckForPayeezy() throws Exception {
    	LOGGER.info("----------------Starts :: OrderControllerV2 : healthCheckForPayeezy:------------------");

		LOGGER.info("Logger calls..!!");
		String url = environment.getProperty("PAYEEZY_HEALTH_CHECK_URL") + "/healthcheck";
		System.out.println("url for health check : " + url);

		HttpClient client = HttpClientBuilder.create().build();
		HttpGet request = new HttpGet(url);
		// add request header
		HttpResponse response = client.execute(request);
		LOGGER.info("Response Code : "
				+ response.getStatusLine().getStatusCode());

		BufferedReader rd = new BufferedReader(new InputStreamReader(response
				.getEntity().getContent()));

		StringBuffer result = new StringBuffer();
		String line = "";
		while ((line = rd.readLine()) != null) {
			result.append(line);
		}
    	LOGGER.info("----------------End :: OrderControllerV2 : healthCheckForPayeezy:------------------");

		return result.toString();
	}
    
    @RequestMapping(value = "/getOrderDetails", method= RequestMethod.GET)
    public @ResponseBody String getOrderDetails(@RequestParam("orderId") Integer orderId , @RequestParam("locationUid") String merchantUid){
    	LOGGER.info("----------------Start :: OrderControllerV2 : getOrderDetails:------------------");

    	String response = orderService.getOrderDetailsByOrderId(orderId , merchantUid);
    	if(response != null && !response.isEmpty()){
        	LOGGER.info("----------------End :: OrderControllerV2 : getOrderDetails:------------------");

    		return response;
    	}else{
        	LOGGER.info("----------------End :: OrderControllerV2 : getOrderDetails:------------------");

    		return "Order not found";
    	}
    }
    
    
    @RequestMapping(value="/getPaymentGatewayDetails/{merchantId}",method = RequestMethod.GET)
    public @ResponseBody String getPaymentGatewayDetails(@PathVariable Integer merchantId){
    	LOGGER.info("----------------Start :: OrderControllerV2 : getPaymentGatewayDetails/{merchantId}:------------------");
    	PaymentGateWay paymentGateWay = null;
    	String response = null;
    	try{
    		if(merchantId != null){
    			LOGGER.info("OrderControllerV2 : getPaymentGatewayDetails/{merchantId} : MerchantId "+merchantId);
    			paymentGateWay = orderService.getPaymentGatewayDetailsByMerchantId(merchantId);
    			if(paymentGateWay != null){
    				Gson gson = new Gson();
    				response = gson.toJson(paymentGateWay);
    				
    			}
        	}
    	}catch(Exception exception){
    		if(exception != null){
    			LOGGER.info("OrderControllerV2 : getPaymentGatewayDetails/{merchantId} : Exception "+exception);
    			exception.printStackTrace();
    			MailSendUtil.sendExceptionByMail(exception,environment);
    		}
    	}
    	if(response!=null)
    		LOGGER.info("OrderControllerV2 : getPaymentGatewayDetails/{merchantId} : response "+response);
    	LOGGER.info("----------------End :: OrderControllerV2 : getPaymentGatewayDetails/{merchantId}:------------------");
    	return response;
    }
    public String maptemplateWithTax(Integer merchantId)
    {
    	try{
    	LOGGER.info("OrderControllerV2 :: maptemplateWithTax : merchantId "+merchantId);
    	List<PizzaTemplate> pizzaTemplate= pizzaTemplateRepository.findUnmapedTemplate(merchantId);
    	if(pizzaTemplate!=null  && pizzaTemplate.size()>0)
    	{
    		LOGGER.info("OrderControllerV2 :: maptemplateWithTax : pizzaTemplate.size() "+pizzaTemplate.size());
    		List<TaxRates> taxRates= taxRateRepository.findByMerchantIdAndIsDefault(merchantId,1);
    		if(taxRates!=null && taxRates.size()>0)
    		{
    			LOGGER.info("OrderControllerV2 :: maptemplateWithTax : taxRates.size() "+taxRates.size());
    			Set<TaxRates> set = new HashSet(); 
    			  
    	        // Add each element of list into the set 
    	        for (TaxRates t : taxRates) 
    	            set.add(t);
    			for (PizzaTemplate pizzaTemplate2 : pizzaTemplate) {
					if(pizzaTemplate2!=null && pizzaTemplate2.getId()!=null)
					{
						LOGGER.info("OrderControllerV2 :: maptemplateWithTax : pizzaTemplate2.getId() "+pizzaTemplate2.getId());
						pizzaTemplate2.setTaxes(set);
					}
				}
    			
    		}
    		
    		pizzaTemplateRepository.save(pizzaTemplate);  
    		
    		}
    }catch(Exception e)
    {
    	LOGGER.info("OrderControllerV2 :: maptemplateWithTax : Exception "+e);
    	MailSendUtil.sendExceptionByMail(e,environment);
    	return "failed";
    }
    	return "success";
    }
    @RequestMapping(value="/maptemplateWithTaxes",method = RequestMethod.GET)
    public @ResponseBody String maptemplateWithTaxes(@RequestParam Integer merchantId){
    	if(merchantId!=null)
    	{
    		return maptemplateWithTax(merchantId);
    	}
    	return "failed";
    }
	
	@RequestMapping(value = "/checkFundCodeValidity", method = RequestMethod.GET)
	@ResponseBody
	public String checkFundCodeValidity(@RequestParam(required = true) String fundCode, HttpServletRequest request) {
		String result = "invalid";
		try {
			LOGGER.info(
					"===============  MerchantController : Inside checkFundCodeValidity :: Start  ============= fundCode "
							+ fundCode);
			HttpSession session = request.getSession(false);
			if (session != null) {
				Merchant merchant = (Merchant) session.getAttribute("merchant");
				if (merchant != null) {
					String timeZoneCode = (merchant.getTimeZone() != null
							&& merchant.getTimeZone().getTimeZoneCode() != null)
									? merchant.getTimeZone().getTimeZoneCode()
									: "America/Chicago";
									
					String date =DateUtil.getCurrentDateYYYYMMDDForTimeZone(timeZoneCode);
					List<VirtualFund> virtualFundList = virtualFundRepository.findByMerchantIdAndCodeAndStatus(merchant.getId(),
							 true,fundCode,date);
					if(virtualFundList.size()>0) {
						for(VirtualFund virtualFund : virtualFundList) {
							if(virtualFund.getCode().equalsIgnoreCase(fundCode)) {
								result = "valid";
							}
						else if(virtualFund.getCode().length()>fundCode.length() && !virtualFund.getCode().equalsIgnoreCase(fundCode))
						      result = "isMatching";
						}
					}
				}
			}
		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e, environment);
				LOGGER.error("error: " + e.getMessage());
				LOGGER.info(
						"===============  MerchantController : Inside updatefundStatusById :: Exception  ============= "
								+ e);

				return "redirect:" + environment.getProperty("BASE_URL") + "/support";
			}
		}
		LOGGER.info("===============  MerchantController : Inside updatefundStatusById :: End  ============= ");

		return result;
	}
    
    @RequestMapping(value = "/removeFundCode", method = RequestMethod.GET)
   	@ResponseBody
   	public String removeFundCode(@RequestParam(required = true) String fundCode,
   			@RequestParam(required = true) Integer customerId, HttpServletRequest request) {
   		String result = "";
   		try {
   			LOGGER.info("===============  MerchantController : Inside removeFundCode :: Start  ============= fundCode "
   							+ fundCode+" customerId :"+customerId);
   			if(fundCode != null && !fundCode.isEmpty()) {
   			customerCodeRepository.deleteCodeByCustomerId(fundCode, customerId);
   			result="success";	
   			}
   		} catch (Exception e) {
   			if (e != null) {
   				MailSendUtil.sendExceptionByMail(e, environment);
   				LOGGER.error("error: " + e.getMessage());
   				result="failed : "+e.getMessage();
   				LOGGER.info(
   						"===============  MerchantController : Inside updatefundStatusById :: Exception  ============= "
   								+ e);
   			}
   		}
   		LOGGER.info("===============  MerchantController : Inside updatefundStatusById :: End  ============= ");

   		return result;
   	}
    
    @RequestMapping(value="/updateOrderPrintStatus" ,method = RequestMethod.GET)
    public @ResponseBody String checkOrderPrintStatus(@RequestParam Integer orderId, @RequestParam boolean printStatus){
    	LOGGER.info("----------------Start :: OrderControllerV2 : updateOrderPrintStatus:------------------");
    	String response = "successfully Updated";
    	try{
    		if(orderId != null)
    		orderService.updatePrintJobStatus(orderId, printStatus);
    	}catch(Exception exception){
    		if(exception != null){
    			response = "error occured while updating Print Status : "+exception;
    			LOGGER.info("OrderControllerV2 : updateOrderPrintStatus : Exception "+exception);
    			exception.printStackTrace();
    			MailSendUtil.sendExceptionByMail(exception,environment);
    		}
    	}
    		LOGGER.info("OrderControllerV2 : updateOrderPrintStatus End : response "+response);
    	return response;
    }
}
