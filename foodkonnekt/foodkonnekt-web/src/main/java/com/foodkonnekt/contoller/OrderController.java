package com.foodkonnekt.contoller;

import com.TPISoft.SmartPayments.Response;
import com.TPISoft.SmartPayments.SmartPaymentsSoapProxy;
import com.firstData.payeezy.PayeezyClientHelperf;
import com.firstData.paymentGatway.CardConnectRestClient;
import com.firstData.paymentGatway.CardConnectRestClientExample;
import com.firstdata.payeezy.JSONHelper;
import com.firstdata.payeezy.PayeezyClientHelper;
import com.firstdata.payeezy.models.transaction.PayeezyResponse;
import com.firstdata.payeezy.models.transaction.ThreeDomainSecureToken;
import com.firstdata.payeezy.models.transaction.TransactionRequest;
import com.firstdata.payeezy.models.transaction.Transarmor;
import com.foodkonnekt.clover.vo.CouponVO;
import com.foodkonnekt.clover.vo.NotificationVO;
import com.foodkonnekt.clover.vo.Payload;
import com.foodkonnekt.clover.vo.PaymentVO;
import com.foodkonnekt.clover.vo.PlaceOrderVO;
import com.foodkonnekt.model.Address;
import com.foodkonnekt.model.CardInfo;
import com.foodkonnekt.model.CategoryDto;
import com.foodkonnekt.model.ConvenienceFee;
import com.foodkonnekt.model.Customer;
import com.foodkonnekt.model.DeliveryOpeningClosingDay;
import com.foodkonnekt.model.DeliveryOpeningClosingTime;
import com.foodkonnekt.model.FulFilledOrder;
import com.foodkonnekt.model.ItemDto;
import com.foodkonnekt.model.Merchant;
import com.foodkonnekt.model.ModifierGroupDto;
import com.foodkonnekt.model.OpeningClosingTime;
import com.foodkonnekt.model.OrderPaymentDetail;
import com.foodkonnekt.model.OrderR;
import com.foodkonnekt.model.OrderType;
import com.foodkonnekt.model.PaymentGateWay;
import com.foodkonnekt.model.PaymentMode;
import com.foodkonnekt.model.PickUpTime;
import com.foodkonnekt.model.Vendor;
import com.foodkonnekt.model.Zone;
import com.foodkonnekt.repository.AddressRepository;
import com.foodkonnekt.repository.DeliveryOpeningClosingDayRepository;
import com.foodkonnekt.repository.DeliveryOpeningClosingTimeRepository;
import com.foodkonnekt.repository.OrderDiscountRepository;
import com.foodkonnekt.repository.OrderItemModifierRepository;
import com.foodkonnekt.repository.OrderItemRepository;
import com.foodkonnekt.repository.OrderPizzaCrustRepository;
import com.foodkonnekt.repository.OrderPizzaRepository;
import com.foodkonnekt.repository.OrderPizzaToppingsRepository;
import com.foodkonnekt.repository.OrderRepository;
import com.foodkonnekt.service.BusinessService;
import com.foodkonnekt.service.CustomerService;
import com.foodkonnekt.service.KritiqService;
import com.foodkonnekt.service.MerchantService;
import com.foodkonnekt.service.OrderService;
import com.foodkonnekt.service.VoucharService;
import com.foodkonnekt.service.ZoneService;
import com.foodkonnekt.util.AppNotification;
import com.foodkonnekt.util.CloverUrlUtil;
import com.foodkonnekt.util.CommonUtil;
import com.foodkonnekt.util.DateUtil;
import com.foodkonnekt.util.EncryptionDecryptionUtil;
import com.foodkonnekt.util.IConstant;
import com.foodkonnekt.util.ItextFaxUtil;
import com.foodkonnekt.util.MailSendUtil;
import com.foodkonnekt.util.OrderUtil;
import com.foodkonnekt.util.PayeezyUtilProperties;
import com.foodkonnekt.util.ProducerUtil;
import com.google.gson.Gson;
import com.merchantwarehouse.schemas.merchantware.v45.MerchantCredentials;
import com.stripe.Stripe;
import com.stripe.model.Charge;
import com.stripe.model.Token;
import net.authorize.api.contract.v1.CreateTransactionRequest;
import net.authorize.api.contract.v1.CreateTransactionResponse;
import net.authorize.api.contract.v1.CreditCardType;
import net.authorize.api.contract.v1.CustomerAddressType;
import net.authorize.api.contract.v1.CustomerDataType;
import net.authorize.api.contract.v1.CustomerTypeEnum;
import net.authorize.api.contract.v1.MerchantAuthenticationType;
import net.authorize.api.contract.v1.MessageTypeEnum;
import net.authorize.api.contract.v1.PaymentType;
import net.authorize.api.contract.v1.TransactionRequestType;
import net.authorize.api.contract.v1.TransactionResponse;
import net.authorize.api.contract.v1.TransactionTypeEnum;
import net.authorize.api.controller.CreateTransactionController;
import net.authorize.api.controller.base.ApiOperationBase;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.rmi.RemoteException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Controller
@SuppressWarnings({ "unchecked", "deprecation" })
public class OrderController {

	@Autowired
    private Environment environment;
	
    @Autowired
    private KritiqService kritiqService;

    @Autowired
    private OrderService orderService;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private ZoneService zoneService;

    @Autowired
    private VoucharService voucharService;

    @Autowired
    private BusinessService businessService;

    @Autowired
    private MerchantService merchantService;
   
    @Autowired
    private OrderRepository orderRepository;
    
    @Autowired
	private OrderItemRepository orderItemRepository;
  
    @Autowired
	private OrderItemModifierRepository orderItemModifierRepository;

	 @Autowired
	 private OrderDiscountRepository orderDiscountRepository;
  
    @Autowired
	private OrderPizzaToppingsRepository orderPizzaToppingsRepository;
	
	@Autowired
	private OrderPizzaCrustRepository orderPizzaCrustRepository;
	
	@Autowired
	private OrderItemModifierRepository itemModifierRepository;
	
	@Autowired
	private OrderPizzaRepository orderPizzaRepository;
	
	@Autowired
	private AddressRepository addressRepository;
	
	@Autowired
	private  DeliveryOpeningClosingTimeRepository deliveryOpeningClosingTimeRepository;
	
	@Autowired
	private  DeliveryOpeningClosingDayRepository deliveryOpeningClosingDayRepository;
    /**
     * Show place order page
     *
     * @param model
     * @param map
     * @return
     */
    /*
     * @RequestMapping(value = "/order", method = RequestMethod.GET) public String
     * showOrderPage(@ModelAttribute("PaymentVO") PaymentVO paymentVO, ModelMap model, Map<String, Object> map,
     * HttpServletRequest request, @RequestParam(required = false) String message) { HttpSession session =
     * request.getSession(); Merchant merchant = (Merchant) session.getAttribute("merchant"); Customer sessionCustomer =
     * (Customer) session.getAttribute("customer"); if (merchant != null) { try {
     *
     * merchant=merchantService.findById(merchant.getId());
     *
     * String timeZoneCode="America/Chicago"; if(merchant!=null && merchant.getTimeZone()!=null &&
     * merchant.getTimeZone().getTimeZoneCode()!=null){ hourDifference=merchant.getTimeZone().getHourDifference();
     * if(merchant.getTimeZone().getMinutDifference()!=null)
     * minutDifference=merchant.getTimeZone().getMinutDifference();
     * timeZoneCode=merchant.getTimeZone().getTimeZoneCode(); } session.setAttribute("merchant",merchant);
     * List<CategoryDto> categories = orderService.findCategoriesByMerchantId(merchant); if(categories!=null &&
     * !categories.isEmpty()){ String avgDeliveryTime=null; String avgPickUpTime=null; PickUpTime
     * pickUpTime=merchantService.findPickupTime(merchant.getId()); if(pickUpTime!=null &&
     * pickUpTime.getPickUpTime()!=null){ avgPickUpTime=pickUpTime.getPickUpTime(); }
     *
     * //if (categories!=null && !categories.isEmpty()) { List<Zone> zones =
     * zoneService.findZoneByMerchantIdAndStatus(merchant.getId()); if (zones != null) { if (!zones.isEmpty() &&
     * zones.size()>0) { model.addAttribute("zoneStatus", "Y"); avgDeliveryTime=zones.get(0).getAvgDeliveryTime(); }
     * else { model.addAttribute("zoneStatus", "N"); } }
     *
     * Calendar deliveryAvgTimeCalendar = Calendar.getInstance(); Calendar pickUpAvgTimeCalendar =
     * Calendar.getInstance(); int time=30; try{ if(avgDeliveryTime!=null){ java.util.Date deliveryAvgTime = new
     * SimpleDateFormat("HH:mm:ss").parse("00:"+avgDeliveryTime+":00");
     * deliveryAvgTimeCalendar.setTime(deliveryAvgTime); }if(avgPickUpTime!=null){ java.util.Date pickUpAvgTime = new
     * SimpleDateFormat("HH:mm:ss").parse("00:"+avgPickUpTime+":00"); pickUpAvgTimeCalendar.setTime(pickUpAvgTime); }
     *
     * if(avgDeliveryTime!=null && avgPickUpTime!=null){
     * if(pickUpAvgTimeCalendar.getTimeInMillis()>deliveryAvgTimeCalendar. getTimeInMillis()){
     * time=pickUpAvgTimeCalendar.getTime().getMinutes(); }else{ time=deliveryAvgTimeCalendar.getTime().getMinutes(); }
     *
     * }else if(avgDeliveryTime!=null){ time=deliveryAvgTimeCalendar.getTime().getMinutes(); }else
     * if(avgPickUpTime!=null){ time=pickUpAvgTimeCalendar.getTime().getMinutes(); } }catch(Exception e){
     *
     * } System.out.println(time); List<OpeningClosingTime> openingClosingTimes = voucharService
     * .findOpeningClosingHoursByMerchantId(merchant.getId(),timeZoneCode); if (openingClosingTimes != null) { if
     * (!openingClosingTimes.isEmpty()) { model.addAttribute("closingDayStatus", "Y");
     * model.addAttribute("openingClosingStatus",
     * OrderUtil.checkOpeningClosingHour(openingClosingTimes,time,timeZoneCode));
     * model.addAttribute("openingClosingHours", OrderUtil.findOperatingHour(openingClosingTimes)); } else {
     * model.addAttribute("closingDayStatus", "N"); } } else { model.addAttribute("closingDayStatus", "N");
     * model.addAttribute("openingClosingStatus", "N"); } if (sessionCustomer != null) {
     * model.addAttribute("sessionCustId", sessionCustomer.getId()); if (sessionCustomer.getPassword() != null) { if
     * (sessionCustomer.getPassword().isEmpty()) { model.addAttribute("setPassword", "Y"); } } else {
     * model.addAttribute("setPassword", "Y"); } Gson gson = new Gson(); String jsonInString =
     * gson.toJson(orderService.findAddessByCustomerId(sessionCustomer.getId())); model.addAttribute("address",
     * jsonInString); } else { model.addAttribute("address", "NoData"); } ConvenienceFee convenienceFee =
     * businessService.findConvenienceFeeByMerchantId(merchant.getId()); if (convenienceFee != null) { if
     * (Double.valueOf(convenienceFee.getConvenienceFee()) > 0) { if (convenienceFee.getIsTaxable() != null) { if
     * (convenienceFee.getIsTaxable() == 1) { model.addAttribute("isConvenienceFeePrice", IConstant.BOOLEAN_TRUE);
     * model.addAttribute("convenienceFeePosId", convenienceFee.getConvenienceFeeLineItemPosId()); model.addAttribute(
     * "convenienceFeeTax", orderService.findConvenienceFeeAfterTax( convenienceFee.getConvenienceFee(),
     * merchant.getId())); model.addAttribute( "convenienceFeeTaxWithComma",
     * orderService.findConvenienceFeeAfterMultiTax( convenienceFee.getConvenienceFee(), merchant.getId()));
     * model.addAttribute("convenienceFeePrice", convenienceFee.getConvenienceFee()); } } else {
     * model.addAttribute("convenienceFeeTax", 0); model.addAttribute("convenienceFeePosId",
     * convenienceFee.getConvenienceFeeLineItemPosId()); model.addAttribute("convenienceFeePrice",
     * convenienceFee.getConvenienceFee()); model.addAttribute("isConvenienceFeePrice", IConstant.BOOLEAN_TRUE); } }
     * else { model.addAttribute("isConvenienceFeePrice", IConstant.BOOLEAN_FALSE); } } System.out.println(
     * "AllowFutureOrder --> " + merchant.getAllowFutureOrder()); model.addAttribute("allowFutureOrder",
     * merchant.getAllowFutureOrder()); // model.addAttribute("allowFutureOrder", 1); if (merchant.getAllowFutureOrder()
     * != null && IConstant.BOOLEAN_TRUE == merchant.getAllowFutureOrder()) { model.addAttribute("futureOrderDates",
     * DateUtil.find10FutureDates()); } model.addAttribute("paymentModes",
     * orderService.findPaymentMode(merchant.getId())); model.addAttribute("categories", categories);
     * model.addAttribute("message", message); model.addAttribute("merchantId", merchant.getId());
     * model.addAttribute("merchantTaxs", orderService.findMerchantTaxs(merchant.getId()));
     *
     *
     * }else{ return "categoryEmpty";
     *
     * } } catch (Exception exception) { if (exception != null) { MailSendUtil.sendExceptionByMail(exception,environment); }
     * exception.printStackTrace(); } return "order"; } else { return "redirect:" + environment.getProperty("WEB_BASE_URL") +
     * "/sessionTimeOut"; } }
     */

    
    private static final Logger LOGGER = LoggerFactory.getLogger(OrderController.class);
    private static final Logger CLIENTIPLOGGER = LoggerFactory.getLogger("ClientIPLogger");
    
    
    @RequestMapping(value = "/order", method = RequestMethod.GET)
    public String showOrderPage(@ModelAttribute("PaymentVO") PaymentVO paymentVO, ModelMap model,
                    Map<String, Object> map, HttpServletRequest request, @RequestParam(required = false) String message,
					@RequestParam(required = false) Integer vendorId,
					@RequestParam(required = false) Integer merchantId,
					@RequestParam(required = false) Integer repeateOrderId) {
    	LOGGER.info("===============  OrderController : Inside order :: Start  ============= ");
        HttpSession session = request.getSession();
        Merchant merchant = (Merchant) session.getAttribute("merchant");
        Customer sessionCustomer = (Customer) session.getAttribute("customer");
        Vendor vendor = (Vendor) session.getAttribute("vendor");
        	if(merchantId!=null)
        	merchant=merchantService.findById(merchantId);
            if(vendor!=null && vendor.getId()!=null){
            	vendorId=vendor.getId();
            }
        if(vendorId!=null){
        	if(merchant!=null && merchant.getOwner()!=null && merchant.getOwner().getId()!=null
			&& merchant.getOwner().getId()==vendorId){
        		LOGGER.info("===============  OrderController : Inside order  :: vendorId  ============= "+vendorId);
        	 vendor = merchantService.findVendorById(vendorId);
        	List<Merchant> merchantList= merchantService.findAllMerchantsByVendorId(vendorId);
       	 
       	 if(vendor!= null){
       		 session.setAttribute("vendor", vendor);
       		 session.setAttribute("merchantList", merchantList);
       	}
        	}else{
        		session.removeAttribute("vendor");
            	session.removeAttribute("merchantList");
        	}
       		
        }else{
        	session.removeAttribute("vendor");
        	session.removeAttribute("merchantList");
       		
        }
        if (merchant != null) {
        	if(merchant.getIsInstall() != null && merchant.getIsInstall() != 1){
        		return "redirect:https://www.foodkonnekt.com";
        	}else{
        		 try {
        			 LOGGER.info("===============  OrderController : Inside order  :: merchantId  ============= "+merchant.getId());
                 	merchant=merchantService.findById(merchant.getId());
                  
                 	String timeZoneCode="America/Chicago";
                 	
                 	session.setAttribute("merchant",merchant);
                 	 // adding payment gateway type to the session
                     PaymentGateWay gateway = merchantService.findbyMerchantId(merchant.getId(), false);
                     if(gateway != null && "3".equals(gateway.getGateWayType())){
                     	session.setAttribute("paymentGateway", false);
                     }
                     else{
                     	session.setAttribute("paymentGateway", true);
                     }
                     
                     if(merchant!=null && merchant.getTimeZone()!=null && merchant.getTimeZone().getTimeZoneCode()!=null){
                 		/*hourDifference=merchant.getTimeZone().getHourDifference();
                 		if(merchant.getTimeZone().getMinutDifference()!=null)
                 		minutDifference=merchant.getTimeZone().getMinutDifference();*/
                 		timeZoneCode=merchant.getTimeZone().getTimeZoneCode();
                 	}
                 	session.setAttribute("merchant",merchant);
                 	
                 	// DB-----------0
                     List<CategoryDto> categories = orderService.findCategoriesByMerchantId(merchant);
                     if(categories!=null && !categories.isEmpty()){
                     	String avgDeliveryTime=null;
                         String avgPickUpTime=null;
                         Calendar deliveryAvgTimeCalendar = Calendar.getInstance();
                     	 List<Zone> zones = zoneService.findZoneByMerchantIdAndStatus(merchant.getId());
                          if (zones != null) {
                              if (!zones.isEmpty() && zones.size()>0) {
                                  
                                  avgDeliveryTime=zones.get(0).getAvgDeliveryTime();
                                  if(avgDeliveryTime!=null){
                                      java.util.Date deliveryAvgTime = new SimpleDateFormat("HH:mm:ss").parse("00:"+avgDeliveryTime+":00");
                                      deliveryAvgTimeCalendar.setTime(deliveryAvgTime);
                                      }
                                  
                                  if(merchant.getAllowDeliveryTiming()!=null && merchant.getAllowDeliveryTiming()==IConstant.BOOLEAN_TRUE){
                                 	 model.addAttribute("zoneStatus",checkDeliveryTimingStatus(merchant.getId(),timeZoneCode,0));
                                  } else {
                             	 model.addAttribute("zoneStatus", "Y");
                              }}else{
                             	 model.addAttribute("zoneStatus", "N");
                              }
                          }else{
                         	 model.addAttribute("zoneStatus", "N");
                          }
                    
                          
                         
                          Calendar pickUpAvgTimeCalendar = Calendar.getInstance();
                          int time=30;
                          try{
                          
                          if(avgPickUpTime!=null){
                              java.util.Date pickUpAvgTime = new SimpleDateFormat("HH:mm:ss").parse("00:"+avgPickUpTime+":00");
                              pickUpAvgTimeCalendar.setTime(pickUpAvgTime);
                              }
                          
                          if(avgDeliveryTime!=null && avgPickUpTime!=null){
                          	if(pickUpAvgTimeCalendar.getTimeInMillis()>deliveryAvgTimeCalendar.getTimeInMillis()){
                          		time=pickUpAvgTimeCalendar.getTime().getMinutes();
                          	}else{
                          		time=deliveryAvgTimeCalendar.getTime().getMinutes();
                          	}
                           
                          }else if(avgDeliveryTime!=null){
                          	time=deliveryAvgTimeCalendar.getTime().getMinutes();
                          }else if(avgPickUpTime!=null){
                          	time=pickUpAvgTimeCalendar.getTime().getMinutes();
                          }
                          }catch(Exception e){
              				LOGGER.info("OrderController.showOrderPage() : Exception : "+e);

                          }
                          System.out.println(time);
                          List<OpeningClosingTime> openingClosingTimes = voucharService
                                          .findOpeningClosingHoursByMerchantId(merchant.getId(),timeZoneCode);
                          if (openingClosingTimes != null) {
                              if (!openingClosingTimes.isEmpty()) {
                                  model.addAttribute("closingDayStatus", "Y");
                                  List<OpeningClosingTime> nextOpeningClosingTimes = voucharService
                                          .findNextOpeningClosingHoursByMerchantId(merchant.getId(),timeZoneCode);
                                  model.addAttribute("openingClosingStatus",
                                 		 OrderUtil.checkOpeningClosingHour(openingClosingTimes,time,timeZoneCode,nextOpeningClosingTimes,environment));
                                  model.addAttribute("openingClosingHours", OrderUtil.findOperatingHour(openingClosingTimes));
                              } else {
                                  model.addAttribute("closingDayStatus", "N");
                              }
                          } else {
                              model.addAttribute("closingDayStatus", "N");
                              model.addAttribute("openingClosingStatus", "N");
                          }
                    
                         if (sessionCustomer != null) {
                             Gson gson = new Gson();

                             if (repeateOrderId != null) {

                                 List<OrderR> repeatOrder = orderService.getRepeateOrder(sessionCustomer.getId(), merchant,
                                                 repeateOrderId);
                                 if (repeatOrder != null && !repeatOrder.isEmpty()) {
                                     if (repeatOrder.get(0).getOrderName() == "" && repeatOrder.get(0).getOrderName() == null
                                                     && repeatOrder.get(0).getOrderName().isEmpty()) {
                                         repeatOrder.get(0).setOrderName(null);
                                     }
                                     if (repeatOrder.get(0).getOrderNote() == "" && repeatOrder.get(0).getOrderNote() == null
                                                     && repeatOrder.get(0).getOrderNote().isEmpty()) {
                                         repeatOrder.get(0).setOrderNote(null);
                                     }
                                     repeatOrder.get(0).setOrderNote(null);
                                     repeatOrder.get(0).setOrderName(null);
                                     repeatOrder.get(0).setCreatedOn(null);
                                     OrderR order = repeatOrder.get(0);
                                     order.setMerchant(null);
                                     order.setCustomer(null);

                                     model.addAttribute("orderList", gson.toJson(order));
                                     System.out.println("REPEATEDORDERJSAON" + gson.toJson(order));
                                 } else {
                                     model.addAttribute("orderList", "empty");
                                 }
                             } else {
                                 model.addAttribute("orderList", "empty");

                             }
                             /*
                              * List<OrderR> repeatedOrders = new ArrayList<OrderR>(); int flag = 0; for(OrderR reOrder :
                              * orderService.getAllRepeateOrder(sessionCustomer.getId(), merchant)){
                              * repeatedOrders.add(reOrder); flag++; if(flag==3){ break; } }
                              *
                              * model.addAttribute("orderRepeateList", repeatedOrders);
                              */

                             model.addAttribute("sessionCustId", sessionCustomer.getId());
                             if (sessionCustomer.getPassword() != null) {
                                 if (sessionCustomer.getPassword().isEmpty()) {
                                     model.addAttribute("setPassword", "Y");
                                 }
                             } else {
                                 model.addAttribute("setPassword", "Y");
                             }

                             // DB-------------4
                             String jsonInString = gson.toJson(orderService.findAddessByCustomerId(sessionCustomer.getId()));
                             model.addAttribute("address", jsonInString);

                             List<CardInfo> cardInfos = customerService.getCustomerCardInfo(sessionCustomer.getId());
                             List<CardInfo> cardsInfos = new ArrayList<CardInfo>();
                             List<CardInfo> expiryCardsInfos = new ArrayList<CardInfo>();
                             Calendar calendar = Calendar.getInstance();
                             Integer year = Integer.parseInt(("" + calendar.get(Calendar.YEAR)).substring(2));
                             Integer month = calendar.get(Calendar.MONTH);
                       
                             for(CardInfo cardInfo : cardInfos){
                             	cardInfo.setMerchant(null);
                 				cardInfo.setCustomer(null);
                 				//cardInfo.getExpirationDate()
                 				LOGGER.info("===============  OrderController : Inside order :: ExpirationDateYear  ============= "+cardInfo.getExpirationDate().substring(2, 4));
                 				LOGGER.info("===============  OrderController : Inside order :: ExpirationDateMonth  ============= "+cardInfo.getExpirationDate().substring(0, 2));
                 				System.out.println("ExpirationDateYear"+cardInfo.getExpirationDate().substring(2, 4));
                 				System.out.println("ExpirationDateMonth"+cardInfo.getExpirationDate().substring(0, 2));
                 				if(cardInfo.getToken()!= null && !cardInfo.getToken().isEmpty() && cardInfo.getFirst6()!= null && !cardInfo.getFirst6().isEmpty() && cardInfo.getLast4()!= null && !cardInfo.getLast4().isEmpty() && cardInfo.getExpirationDate()!= null && !cardInfo.getExpirationDate().isEmpty()&& cardInfo.getToken()!= "" && cardInfo.getFirst6()!= "" && cardInfo.getLast4()!= "" && cardInfo.getExpirationDate()!= "" ){
                 				if(Integer.parseInt(cardInfo.getExpirationDate().substring(2, 4))>year){
                 					cardsInfos.add(cardInfo);
                 				}else if(Integer.parseInt(cardInfo.getExpirationDate().substring(2, 4))==year && Integer.parseInt(cardInfo.getExpirationDate().substring(0, 2))>month){
                 					cardsInfos.add(cardInfo);
                 				}else{
                 					expiryCardsInfos.add(cardInfo);
                 				}
                 				}
                 				
                 			}
                             if(merchant!= null&& merchant.getAllowMultiPay()!=null && merchant.getAllowMultiPay()==true){
                                 String cardsInfosJson = gson.toJson(cardsInfos);
                                 String expiryCardsInfosJson = gson.toJson(expiryCardsInfos);
                             	session.setAttribute("cardInfo", cardsInfosJson);
                             	session.setAttribute("expiryCardsInfos", expiryCardsInfosJson);
                             	session.setAttribute("allowMultiPay", true);
                             }else{
                             	session.setAttribute("allowMultiPay", false);
                             }

                         } else {
                             model.addAttribute("orderList", "empty");
                             model.addAttribute("address", "NoData");
                         }

                         // DB-------------------5
                         ConvenienceFee convenienceFee = businessService.findConvenienceFeeByMerchantId(merchant.getId());
                         if (convenienceFee != null) {
                             if (Double.valueOf(convenienceFee.getConvenienceFee()) > 0) {
                                 if (convenienceFee.getIsTaxable() != null) {
                                     if (convenienceFee.getIsTaxable() == 1) {
                                         model.addAttribute("isConvenienceFeePrice", IConstant.BOOLEAN_TRUE);
                                         model.addAttribute("convenienceFeePosId",
                                                         convenienceFee.getConvenienceFeeLineItemPosId());

                                         // Db--------------6
                                         model.addAttribute(
                                                         "convenienceFeeTax",
                                                         orderService.findConvenienceFeeAfterTax(
                                                                         convenienceFee.getConvenienceFee(),
                                                                         merchant.getId()));
                                         
                                         //DB---------------7
                                         model.addAttribute(
                                                         "convenienceFeeTaxWithComma",
                                                         orderService.findConvenienceFeeAfterMultiTax(
                                                                         convenienceFee.getConvenienceFee(),
                                                                         merchant.getId()));
                                         model.addAttribute("convenienceFeePrice", convenienceFee.getConvenienceFee());
                                     }
                                 } else {
                                     model.addAttribute("convenienceFeeTax", 0);
                                     model.addAttribute("convenienceFeePosId",
                                                     convenienceFee.getConvenienceFeeLineItemPosId());
                                     model.addAttribute("convenienceFeePrice", convenienceFee.getConvenienceFee());
                                     model.addAttribute("isConvenienceFeePrice", IConstant.BOOLEAN_TRUE);
                                 }
                             } else {
                                 model.addAttribute("isConvenienceFeePrice", IConstant.BOOLEAN_FALSE);
                             }
                         }
                         LOGGER.info("===============  OrderController : Inside order :: AllowFutureOrder  ============= "+merchant.getAllowFutureOrder());
                         System.out.println("AllowFutureOrder --> " + merchant.getAllowFutureOrder());
                         model.addAttribute("allowFutureOrder", merchant.getAllowFutureOrder());
                         // model.addAttribute("allowFutureOrder", 1);
                         if (merchant.getAllowFutureOrder() != null
                                         && IConstant.BOOLEAN_TRUE == merchant.getAllowFutureOrder()) {
                             model.addAttribute("futureOrderDates", DateUtil.find10FutureDates());
                         }
                         
                         PickUpTime pickUpTime = merchantService.findPickupTime(merchant.getId());
                         if(pickUpTime!=null && pickUpTime.getMinOrderAmount()!=null && pickUpTime.getMinOrderAmount()>0){
                         	model.addAttribute("pickUpMinOrderAmount", pickUpTime.getMinOrderAmount());
                         }else{
                         	model.addAttribute("pickUpMinOrderAmount", 0.0);
                         }
                         
                         PaymentMode creditCardMode = businessService.findByMerchantIdAndLabel(merchant.getId(),
     							"Credit Card");
                         if(creditCardMode!=null && creditCardMode.getMinOrderAmount()!=null && creditCardMode.getMinOrderAmount()>0 && creditCardMode.getAllowPaymentMode()>0){
                         	model.addAttribute("ccMinOrderAmount", creditCardMode.getMinOrderAmount());
                         }else{
                         	model.addAttribute("ccMinOrderAmount", 0.0);
                         }
                         model.addAttribute("paymentModes", orderService.findPaymentMode(merchant.getId()));
                         model.addAttribute("categories", categories);
                         model.addAttribute("message", message);
                         model.addAttribute("merchantId", merchant.getId());
                         model.addAttribute("merchantUid", merchant.getMerchantUid());
                         // DB-----------------9
                         model.addAttribute("merchantTaxs", orderService.findMerchantTaxs(merchant.getId()));

                     } else {
                     	LOGGER.info("OrderController :: inside order end");
                         return "categoryEmpty";

                     }
                 } catch (Exception exception) {
                     if (exception != null) {
                     	LOGGER.info("OrderController  :: Inside order Exception"+ exception);
                         MailSendUtil.sendExceptionByMail(exception,environment);
                     }
                     exception.printStackTrace();
                 }

                 if (merchant.getOwner() != null && merchant.getOwner().getPos().getPosId() == 2) {
                 	LOGGER.info("OrderController :: Inside order end");
                     return "foodTronixOrder";
                 } else {
                     return "order";
                 }
        	}
        } else {
        	LOGGER.info("OrderController  : Inside order end");
            //return "redirect:" + environment.getProperty("WEB_BASE_URL") + "/sessionTimeOut";
        	return "redirect:https://www.foodkonnekt.com";
        }
    }
    
String checkDeliveryTimingStatus(Integer merchantId, String timeZoneCode,int time ){
	    LOGGER.info("OrderController  : Inside checkDeliveryTimingStatus start");
        List<DeliveryOpeningClosingTime> openingClosingTimes = zoneService.findDeliveryOpeningClosingHoursByMerchantId(merchantId, timeZoneCode);

List<DeliveryOpeningClosingTime> nextOpeningClosingTimes = zoneService.findNextOpeningDeliveryClosingHoursByMerchantId(merchantId, timeZoneCode);
if (openingClosingTimes != null) {
    if (!openingClosingTimes.isEmpty()) {

        // -------------------
    	LOGGER.info("OrderController  : Inside checkDeliveryTimingStatus end ");
        return OrderUtil.checkDeliveryOpeningClosingHour(openingClosingTimes, time,
                        timeZoneCode, nextOpeningClosingTimes,environment);
    } else {
    	LOGGER.info("OrderController  : Inside checkDeliveryTimingStatus return N");

        return "N";
    }
} else {
	LOGGER.info("OrderController  : Inside checkDeliveryTimingStatus return N");
    return "N";
}
    }
    
    @RequestMapping(value = "/getConvenienceFeeStatus", method = RequestMethod.GET)
    @ResponseBody
    public String getConvenienceFeeStatus( Map<String, Object> map, HttpServletRequest request,
                    @RequestParam(required = false) String message) {
    	LOGGER.info("OrderController  : Inside getConvenienceFeeStatus start");

        HttpSession session = request.getSession();
        Merchant merchant = (Merchant) session.getAttribute("merchant");
        String jsonString = "Not Found";
        if (merchant != null) {
            try {
            	
            	ConvenienceFee convenienceFee = businessService.findConvenienceFeeByMerchantId(merchant.getId());
                if (convenienceFee != null) {
                    if (Double.valueOf(convenienceFee.getConvenienceFee()) > 0) {
                        if (convenienceFee.getIsTaxable() != null) {
                            if (convenienceFee.getIsTaxable() == 1) {
                                map.put("isConvenienceFeePrice", IConstant.BOOLEAN_TRUE);
                                map.put("convenienceFeePosId",
                                                convenienceFee.getConvenienceFeeLineItemPosId());
                                
                                // Db--------------6
                                map.put(
                                                "convenienceFeeTax",
                                                orderService.findConvenienceFeeAfterTax(
                                                                convenienceFee.getConvenienceFee(),
                                                                merchant.getId()));
                                
                                //DB---------------7
                                map.put(
                                                "convenienceFeeTaxWithComma",
                                                orderService.findConvenienceFeeAfterMultiTax(
                                                                convenienceFee.getConvenienceFee(),
                                                                merchant.getId()));
                                map.put("convenienceFeePrice", convenienceFee.getConvenienceFee());
                            }
                        } else {
                            map.put("convenienceFeeTax", 0);
                            map.put("convenienceFeePosId",
                                            convenienceFee.getConvenienceFeeLineItemPosId());
                            map.put("convenienceFeePrice", convenienceFee.getConvenienceFee());
                            map.put("isConvenienceFeePrice", IConstant.BOOLEAN_TRUE);
                        }
                    } else {
                        map.put("isConvenienceFeePrice", IConstant.BOOLEAN_FALSE);
                    }
                }
            	
            	
        Gson gson = new Gson();
        jsonString = gson.toJson(map);
            } catch (Exception exception) {
                if (exception != null) {
                	LOGGER.info("OrderController  : Inside getConvenienceFeeStatus exception"+ exception);

                    MailSendUtil.sendExceptionByMail(exception,environment);
                }
                exception.printStackTrace();
            }
        
        
    }
    	LOGGER.info("OrderController  : Inside getConvenienceFeeStatus end");

        return jsonString;
    }
 
    
    @RequestMapping(value = "/getBussinessHoursStatus", method = RequestMethod.GET)
    @ResponseBody
    public String getBussinessHoursStatus( Map<String, Object> map, HttpServletRequest request,
                    @RequestParam(required = false) String message) {
    	LOGGER.info("----------------Start :: OrderController : getBussinessHoursStatus------------------");

        HttpSession session = request.getSession();
        Merchant merchant = (Merchant) session.getAttribute("merchant");
        String jsonString = "Not Found";
        if (merchant != null) {
            try {
            	merchant = merchantService.findById(merchant.getId());
				session.setAttribute("merchant", merchant);
    
		String avgDeliveryTime=null;
        String avgPickUpTime=null;
        
        String timeZoneCode="America/Chicago";
    	if(merchant!=null && merchant.getTimeZone()!=null && merchant.getTimeZone().getTimeZoneCode()!=null){
    		/*hourDifference=merchant.getTimeZone().getHourDifference();
    		if(merchant.getTimeZone().getMinutDifference()!=null)
    		minutDifference=merchant.getTimeZone().getMinutDifference();*/
    		timeZoneCode=merchant.getTimeZone().getTimeZoneCode();
    	}
        
      
        PickUpTime pickUpTime=merchantService.findPickupTime(merchant.getId());
        if(pickUpTime!=null && pickUpTime.getPickUpTime()!=null){
        	avgPickUpTime=pickUpTime.getPickUpTime();
        }
        
        List<Zone> zones = zoneService.findZoneByMerchantIdAndStatus(merchant.getId());
        if (zones != null) {
            if (!zones.isEmpty() && zones.size()>0) {
            	LOGGER.info(" === ZoneStatus == Y");
            	map.put("zoneStatus", "Y");
                avgDeliveryTime=zones.get(0).getAvgDeliveryTime();
            } else {
            	LOGGER.info(" === ZoneStatus == N");
            	map.put("zoneStatus", "N");
            }
        }
        
        Calendar deliveryAvgTimeCalendar = Calendar.getInstance();
        Calendar pickUpAvgTimeCalendar = Calendar.getInstance();
        int time=30;
        try{
        	LOGGER.info(" ===avgDeliveryTime : "+avgDeliveryTime);
        if(avgDeliveryTime!=null){
        java.util.Date deliveryAvgTime = new SimpleDateFormat("HH:mm:ss").parse("00:"+avgDeliveryTime+":00");
        deliveryAvgTimeCalendar.setTime(deliveryAvgTime);
        }if(avgPickUpTime!=null){
            java.util.Date pickUpAvgTime = new SimpleDateFormat("HH:mm:ss").parse("00:"+avgPickUpTime+":00");
            pickUpAvgTimeCalendar.setTime(pickUpAvgTime);
            }
        
        if(avgDeliveryTime!=null && avgPickUpTime!=null){
        	if(pickUpAvgTimeCalendar.getTimeInMillis()>deliveryAvgTimeCalendar.getTimeInMillis()){
        		time=pickUpAvgTimeCalendar.getTime().getMinutes();
        	}else{
        		time=deliveryAvgTimeCalendar.getTime().getMinutes();
        	}
         
        }else if(avgDeliveryTime!=null){
        	time=deliveryAvgTimeCalendar.getTime().getMinutes();
        }else if(avgPickUpTime!=null){
        	time=pickUpAvgTimeCalendar.getTime().getMinutes();
        }
        }catch(Exception e){
        	  LOGGER.error("OrderController :: getBussinessHoursStatus : Exception "+e);
        }
        //System.out.println(time);
        LOGGER.info("OrderController :: getBussinessHoursStatus : time "+time);
        
        
        // DB-----------3
        List<OpeningClosingTime> openingClosingTimes = voucharService
                        .findOpeningClosingHoursByMerchantId(merchant.getId(),timeZoneCode);
        
        List<OpeningClosingTime> nextOpeningClosingTimes = voucharService
                .findNextOpeningClosingHoursByMerchantId(merchant.getId(),timeZoneCode);
        if (openingClosingTimes != null) {
            if (!openingClosingTimes.isEmpty()) {
            	map.put("closingDayStatus", "Y");
                
                //-------------------
            	map.put("openingClosingStatus",
                                OrderUtil.checkOpeningClosingHour(openingClosingTimes,time,timeZoneCode,nextOpeningClosingTimes,environment));
                
                //---------------------
            	map.put("openingClosingHours", OrderUtil.findOperatingHour(openingClosingTimes));
            } else {
            	map.put("closingDayStatus", "N");
            }
        } else {
        	map.put("closingDayStatus", "N");
        	map.put("openingClosingStatus", "N");
        }
        Gson gson = new Gson();
        jsonString = gson.toJson(map);
            } catch (Exception exception) {
                if (exception != null) {
                	LOGGER.error("OrderController :: getBussinessHoursStatus : Exception "+exception);
                    MailSendUtil.sendExceptionByMail(exception,environment);
                }
            	LOGGER.error("OrderController :: getBussinessHoursStatus : Exception "+exception);
                exception.printStackTrace();
            }
        }
        LOGGER.info("OrderController : getBussinessHoursStatus : json "+jsonString);
        LOGGER.info("----------------End :: OrderController : getBussinessHoursStatus------------------");
        return jsonString;
    }
    
    
    
    
    
    
    
    
    /**
     * Show my order page
     *
     * @param model
     * @param map
     * @return
     */
    @RequestMapping(value = "/myOrder", method = RequestMethod.GET)
    public String showMyOrder(ModelMap model, Map<String, Object> map, HttpServletRequest request,
                    @RequestParam(required = false) String message) {
    	//LOGGER.info("OrderController  : Inside showMyOrder start");
    	LOGGER.info("----------------Start :: OrderController : showMyOrder------------------");

        HttpSession session = request.getSession();
        Merchant merchant = (Merchant) session.getAttribute("merchant");
        Customer customer = (Customer) session.getAttribute("customer");
        if (customer != null && merchant != null) {
            String orderStatus = (String) session.getAttribute("failOrder");
            if ("Y".equals(orderStatus)) {
             	LOGGER.info(" === orderStatus == Y");
                model.addAttribute("orderStatus", "Y");
            }
            session.setAttribute("failOrder", "N");
            model.addAttribute("orderList", orderService.getAllOrder(customer.getId(), merchant.getId()));
        	//LOGGER.info("OrderController  :: Inside showMyOrder end");
            LOGGER.info("----------------End :: OrderController : showMyOrder------------------");

            return "myOrder";
        } else {
        	LOGGER.info("----------------End :: OrderController : showMyOrder------------------");

            // return "redirect:" + environment.getProperty("WEB_BASE_URL") + "/sessionTimeOut";
            return "redirect:https://www.foodkonnekt.com";
        }
    }

    /**
     * Place order clover API call
     *
     * @param request
     * @param orderJson
     * @param orderTotal
     * @param instruction
     * @return
     */
    @RequestMapping(value = "/placeOrder", method = RequestMethod.GET)
    @ResponseBody
    public String placeOrder(HttpServletRequest request, HttpServletResponse response, @RequestParam() String orderJson,
                    @RequestParam String orderTotal, @RequestParam(required = false) String instruction,
                    @RequestParam(required = false) Double discount,
                    @RequestParam(required = false) String discountType,
                    @RequestParam(required = false) String itemPosIds,
                    @RequestParam(required = false) String inventoryLevel,
                    @RequestParam(required = false) String voucherCode, @RequestParam String orderType,
                    @RequestParam(required = false) String convenienceFee, @RequestParam String deliveryItemPrice,
                    @RequestParam String avgDeliveryTime, @RequestParam(required = false) String itemsForDiscount,
                    @RequestParam(required = false) String listOfALLDiscounts,
                    @RequestParam(required = false) PaymentVO paymentVO) {
        LOGGER.info("----------------Start :: OrderController : insideplaceorder------------------");

        // MailSendUtil.sendErrorMailToAdmin("inside placeOrder controller"+orderJson);
        String orderPosId = "Failed";
        try {
            HttpSession session = request.getSession();
            session.setAttribute("orderType", orderType);

            Merchant merchant = (Merchant) session.getAttribute("merchant");
            Customer customer = (Customer) session.getAttribute("customer");

            System.out.println("~~~~&&&" + itemsForDiscount);
            LOGGER.info("===============  OrderController : Inside placeOrder  :: itemsForDiscount  ============= "+itemsForDiscount);
            LOGGER.info("===============  OrderController : Inside placeOrder  :: listOfALLDiscounts  ============= "+listOfALLDiscounts);
            System.out.println("~~~~&&&" + listOfALLDiscounts);
            if (merchant != null) {
                String orderTypePosid = "";
                OrderType type = orderService.findByMerchantIdAndLabel(merchant.getId(), orderType);
                LOGGER.info("OrderController  : insideplaceorder :merchant Id " +merchant.getId());
                if (type != null) {
                    orderTypePosid = type.getPosOrderTypeId();
                    LOGGER.info("===============  OrderController : Inside placeOrder  :: orderTypePosid  ============= "+orderTypePosid);
                }

                String employeePosId = null;

                if (merchant != null && merchant.getEmployeePosId() != null && (!merchant.getEmployeePosId().isEmpty()
                                || !merchant.getEmployeePosId().equals(""))) {
                    employeePosId = merchant.getEmployeePosId();
                }

                if (discount != null && discount > 0) {

                } else {
                    discount = 0.0;
                }
                orderJson = "{\n\"order\":[" + orderJson + "]}";
                LOGGER.info("===============  OrderController : Inside placeOrder  :: orderJson  ============= "+orderJson);
                String finalJson = OrderUtil.findFinalOrderJson(orderJson, orderTotal, instruction, orderTypePosid,
                                customer, employeePosId, discount, discountType, voucherCode, itemPosIds,
                                inventoryLevel, itemsForDiscount, listOfALLDiscounts, paymentVO, merchant);

                if (merchant.getOwner() != null && merchant.getOwner().getPos() != null
                                && merchant.getOwner().getPos().getPosId() != null
                                && merchant.getOwner().getPos().getPosId() == 1) {
                	 LOGGER.info("===============  OrderController : Inside placeOrder  :: finalJson  ============= "+finalJson);
                    String result = ProducerUtil.placeOrder(finalJson, merchant,environment);
                    LOGGER.info("===============  OrderController : Inside placeOrder  :: ProducerUtil.placeOrder  ============= "+result);
                    if (result != null && result.contains("title") && result.startsWith("{")) {
                        JSONObject jObject = new JSONObject(result);
                        orderPosId = orderService.saveOrder(jObject, finalJson, customer, merchant, discount,
                                        convenienceFee, deliveryItemPrice, avgDeliveryTime, orderType,
                                        paymentVO.getAuxTax(), paymentVO.getTax());
                        LOGGER.info("===============  OrderController : Inside placeOrder  :: orderPosId  ============= "+orderPosId);
                    }
                } else {
                    JSONObject jObject = null;
                    orderPosId = orderService.saveOrder(jObject, finalJson, customer, merchant, discount,
                                    convenienceFee, deliveryItemPrice, avgDeliveryTime, orderType,
                                    paymentVO.getAuxTax(), paymentVO.getTax());
                    LOGGER.info("===============  OrderController : Inside placeOrder  :: orderPosId  ============= "+orderPosId);
                }
            } else {
                response.sendRedirect("https://www.foodkonnekt.com");
            }
        } catch (Exception exception) {
            if (exception != null) {
            	LOGGER.info("OrderController :: placeOrder : Exception "+exception);
                MailSendUtil.sendExceptionByMail(exception,environment);
            }
            exception.printStackTrace();
        }
         LOGGER.info("OrderController :: insideplaceorder : orderPosId " +orderPosId);
    	 LOGGER.info("----------------End :: OrderController : insideplaceorder------------------");
         return orderPosId;
    }

    @RequestMapping(value = "/placeOrderOnAWS", method = { RequestMethod.POST })
    @ResponseBody
    public String placeOrderOnAWS(@RequestBody PlaceOrderVO placeOrderVO, ModelMap model, HttpServletRequest request,
                    HttpServletResponse response) {
    	 LOGGER.info("----------------Start :: OrderController : placeOrderOnAWS------------------");

        String orderJson = "";// placeOrderVO.getOrderJson();
        String orderTotal = placeOrderVO.getOrderTotal();
        String instruction = placeOrderVO.getInstruction();
        Double discount = placeOrderVO.getDiscount();
        String discountType = placeOrderVO.getDiscountType();
        String itemPosIds = placeOrderVO.getItemPosIds();
        String inventoryLevel = placeOrderVO.getInventoryLevel();
        String voucherCode = placeOrderVO.getVoucherCode();
        String orderType = placeOrderVO.getOrderType();
        String convenienceFee = placeOrderVO.getConvenienceFee();
        String deliveryItemPrice = placeOrderVO.getDeliveryItemPrice();
        String avgDeliveryTime = placeOrderVO.getAvgDeliveryTime();
        String tax = placeOrderVO.getPaymentVO().getTax();
        String auxTax = placeOrderVO.getPaymentVO().getAuxTax();
        // placeOrderVO.getPaymentVO().getSaveCard();
        List<ItemDto> itemDiscounts = placeOrderVO.getItemsForDiscount();
        Gson gson = new Gson();

        String itemsForDiscount = "";
        if (itemDiscounts != null && itemDiscounts.size() > 0) {
            itemsForDiscount = gson.toJson(itemDiscounts);
        }
        List<CouponVO> discountList = placeOrderVO.getListOfALLDiscounts();
        String listOfALLDiscounts = "";
        if (discountList != null && discountList.size() > 0) {
            listOfALLDiscounts = gson.toJson(discountList);
        }
        PaymentVO paymentVO = placeOrderVO.getPaymentVO();

        orderJson = gson.toJson(placeOrderVO.getOrderJson());
LOGGER.info(" ===orderJson : "+orderJson);
        // MailSendUtil.sendErrorMailToAdmin("inside placeOrder controller"+orderJson);
        String orderPosId = null;
        String responseBody = "FAILED";
        String url = "/order";
        try {
            HttpSession session = request.getSession();
            session.setAttribute("orderType", orderType);

            Merchant merchant = (Merchant) session.getAttribute("merchant");
            Customer customer = (Customer) session.getAttribute("customer");

            Vendor vendor = null;
            if (session != null) {
                vendor = (Vendor) session.getAttribute("vendor");
            }
            if (vendor != null && vendor.getId() != null && merchant != null && merchant.getId() != null) {
                url = "order?vendorId=" + vendor.getId() + "&merchantId=" + merchant.getId();
            } else if (merchant != null && merchant.getId() != null) {
                url = "order?merchantId=" + merchant.getId();
            }
            
            
            LOGGER.info("OrderController :: placeOrderOnAWS : merchantId "+merchant.getId());
            
            System.out.println(url);
            System.out.println("~~~~&&&" + itemsForDiscount);
            LOGGER.info("OrderController :: placeOrderOnAWS : itemsForDiscount "+itemsForDiscount);
            LOGGER.info("OrderController :: placeOrderOnAWS : listOfALLDiscounts "+listOfALLDiscounts);
            System.out.println("~~~~&&&" + listOfALLDiscounts);
            if (merchant != null) {
                String orderTypePosid = "";
                OrderType type = orderService.findByMerchantIdAndLabel(merchant.getId(), orderType);
                if (type != null) {
                    orderTypePosid = type.getPosOrderTypeId();
                }

                String employeePosId = null;

                if (merchant != null && merchant.getEmployeePosId() != null && (!merchant.getEmployeePosId().isEmpty()
                                || !merchant.getEmployeePosId().equals(""))) {
                    employeePosId = merchant.getEmployeePosId();
                }
                if (discount != null && discount > 0) {

                } else {
                    discount = 0.0;
                }
                orderJson = "{\n\"order\":" + orderJson + "}";
                LOGGER.info("OrderController :: placeOrderOnAWS : orderJson "+orderJson);
                String finalJson = OrderUtil.findFinalOrderJson(orderJson, orderTotal, instruction, orderTypePosid,
                                customer, employeePosId, discount, discountType, voucherCode, itemPosIds,
                                inventoryLevel, itemsForDiscount, listOfALLDiscounts, paymentVO, merchant);
                LOGGER.info("OrderController :: placeOrderOnAWS : finalJson "+finalJson);
                if (merchant.getOwner() != null && merchant.getOwner().getPos() != null
                                && merchant.getOwner().getPos().getPosId() != null
                                && merchant.getOwner().getPos().getPosId() == 1) {
                    String result = ProducerUtil.placeOrder(finalJson, merchant,environment);
                    LOGGER.info("OrderController :: placeOrderOnAWS : result "+result);
                    if (result != null && result.contains("title") && result.startsWith("{")) {
                        JSONObject jObject = new JSONObject(result);
                        orderPosId = orderService.saveOrder(jObject, finalJson, customer, merchant, discount,
                                        convenienceFee, deliveryItemPrice, avgDeliveryTime, orderType, auxTax, tax);
                        LOGGER.info("OrderController :: placeOrderOnAWS : orderPosId "+orderPosId);
                    }
                } else {
                    JSONObject jObject = null;
                    orderPosId = orderService.saveOrder(jObject, finalJson, customer, merchant, discount,
                                    convenienceFee, deliveryItemPrice, avgDeliveryTime, orderType, auxTax, tax);
                    LOGGER.info("OrderController :: placeOrderOnAWS : orderPosId "+orderPosId);
                }
                if (paymentVO != null)
                    paymentVO.setOrderPosId(orderPosId);
                responseBody = orderPaymentUsingAjax(paymentVO, model, request, response);
                LOGGER.info("OrderController :: placeOrderOnAWS : orderPaymentUsingAjax Response "+responseBody);
            } else {
            	LOGGER.info("merchant session has been expired before placing an order");
                System.out.println("merchant session has been expired before placing an order");
                response.sendRedirect("https://www.foodkonnekt.com");
            }
        } catch (Exception exception) {
            responseBody = "FAIL";
            if (exception != null) {
            	LOGGER.error("OrderController :: placeOrderOnAWS : Exception "+exception);
                MailSendUtil.sendExceptionByMail(exception,environment);
            }
            exception.printStackTrace();
        }
        LOGGER.info("OrderController :: placeOrderOnAWS : responseBody "+responseBody);
    	 LOGGER.info("----------------End :: OrderController : placeOrderOnAWS------------------");
         return responseBody;
    }

    /**
     * Call clover API of payment for clover order
     *
     * @param paymentVO
     * @param model
     * @param request
     * @param response
     * @return
     */
    /*
     * @RequestMapping(value = "/orderPayment", method = { RequestMethod.POST }) public String
     * orderPayment(@ModelAttribute("PaymentVO") PaymentVO paymentVO, ModelMap model, HttpServletRequest request,
     * HttpServletResponse response) { HttpSession session = request.getSession(); Merchant merchant = (Merchant)
     * session.getAttribute("merchant"); Customer customer = (Customer) session.getAttribute("customer"); try { if
     * (merchant != null) { if (paymentVO.getOrderPosId() != null && (!paymentVO.getOrderPosId().isEmpty() ||
     * !paymentVO.getOrderPosId().equals(""))) { Gson gson = new Gson(); String paymentJson = gson.toJson(paymentVO);
     * String orderType = (String) session.getAttribute("orderType"); String merchantLogo = null; if (merchant != null)
     * { if (merchant.getMerchantLogo() == null) { merchantLogo = "http://www.dev.foodkonnekt.com/app/foodnew.jpg"; }
     * else { merchantLogo = environment.getProperty("BASE_PORT") + merchant.getMerchantLogo(); } } String result ="";
     * if(merchant.getOwner()!=null && merchant.getOwner().getPos()!=null &&
     * merchant.getOwner().getPos().getPosId()!=null && merchant.getOwner().getPos().getPosId()==1){ result =
     * ProducerUtil.orderPayment(paymentJson, merchant); }
     *
     * boolean updateStatus = orderService.updateOrderStatus(paymentVO.getOrderPosId(), result, customer.getId(),
     * paymentVO.getPaymentMethod(), paymentVO.getSubTotal(), paymentVO.getTax(), customer.getFirstName(),
     * customer.getEmailId(), merchant.getName(), merchantLogo, orderType, paymentVO.getTip(), paymentVO.getTotal(),
     * paymentVO.getFutureOrderType(), paymentVO.getFutureDate(), paymentVO.getFutureTime());
     *
     * if (updateStatus) { if (result.contains("result") && !result.contains("DECLINED")) { try { JSONObject orderResult
     * = new JSONObject(result); if (orderResult.toString().contains("result")) { String status =
     * orderResult.getString("result"); if (status.equals("APPROVED") || status.equals("SUCCESS")) {
     * model.addAttribute("message", "Your order is placed successfully"); } } else { String orderDeleteResponse =
     * CloverUrlUtil.deleteOrder(merchant.getPosMerchantId(), merchant.getAccessToken(), paymentVO.getOrderPosId());
     * orderService.deleteAnOrder(customer.getId(), paymentVO.getOrderPosId()); System.out.println("orderDeleteResponse"
     * + orderDeleteResponse); MailSendUtil.orderFaildMail("MerchantName -> " +merchant.getName()+" ,MerchantId-> "
     * +merchant.getPosMerchantId()+", orderId -> "+paymentVO.getOrderPosId() +", Payment API response"+result);
     * model.addAttribute("message", "Your order was not placed. Please try again or call us");
     * session.setAttribute("failOrder", "Y"); } } catch (Exception exception) { if (exception != null) {
     * MailSendUtil.sendExceptionByMail(exception,environment); } exception.printStackTrace(); } OrderServiceImpl impl = new
     * OrderServiceImpl(); impl.start(); } else { String orderDeleteResponse =
     * CloverUrlUtil.deleteOrder(merchant.getPosMerchantId(), merchant.getAccessToken(), paymentVO.getOrderPosId());
     * MailSendUtil.orderFaildMail("MerchantName -> " +merchant.getName()+" ,MerchantId-> "+merchant.getPosMerchantId()+
     * ", orderId -> "+paymentVO.getOrderPosId()+", Payment API response"+result);
     * orderService.deleteAnOrder(customer.getId(), paymentVO.getOrderPosId()); System.out.println("orderDeleteResponse"
     * + orderDeleteResponse);
     *
     * model.addAttribute("message", "Your order was not placed. Please try again or call us");
     * session.setAttribute("failOrder", "Y"); } if (customer.getPassword() != null) { if
     * (!customer.getPassword().isEmpty()) { return "redirect:" + environment.getProperty("WEB_BASE_URL") + "/order"; } else { return
     * "redirect:" + environment.getProperty("WEB_BASE_URL") + "/order"; } } else { return "redirect:" + environment.getProperty("WEB_BASE_URL") +
     * "/order"; } } else { String orderDeleteResponse = CloverUrlUtil.deleteOrder(merchant.getPosMerchantId(),
     * merchant.getAccessToken(), paymentVO.getOrderPosId()); MailSendUtil.orderFaildMail("MerchantName -> "
     * +merchant.getName()+" ,MerchantId-> "+merchant.getPosMerchantId() +", orderId -> "+paymentVO.getOrderPosId()+
     * ", Payment API response"+result); orderService.deleteAnOrder(customer.getId(), paymentVO.getOrderPosId());
     * System.out.println("orderDeleteResponse" + orderDeleteResponse); model.addAttribute("message",
     * "Your order was not placed. Please try again or call us"); return "redirect:" + environment.getProperty("WEB_BASE_URL") +
     * "/order"; }
     *
     * } else { MailSendUtil.orderFaildMail("MerchantName -> " +merchant.getName()+" ,MerchantId-> "
     * +merchant.getPosMerchantId()+", orderId -> "+paymentVO.getOrderPosId()); model.addAttribute("message",
     * "Your order was not placed. Please try again or call us"); return "redirect:" + environment.getProperty("WEB_BASE_URL") +
     * "/order";
     *
     * } } else { return "redirect:" + environment.getProperty("WEB_BASE_URL") + "/sessionTimeOut"; } } catch (Exception exception) {
     * if (exception != null) { MailSendUtil.sendExceptionByMail(exception,environment); String orderDeleteResponse =
     * CloverUrlUtil.deleteOrder(merchant.getPosMerchantId(), merchant.getAccessToken(), paymentVO.getOrderPosId());
     * orderService.deleteAnOrder(customer.getId(), paymentVO.getOrderPosId()); System.out.println("orderDeleteResponse"
     * + orderDeleteResponse); model.addAttribute("message", "Your order was not placed. Please try again or call us");
     * session.setAttribute("failOrder", "Y"); } } return "redirect:" + environment.getProperty("WEB_BASE_URL") + "/order"; }
     */

    @SuppressWarnings("unused")
    @RequestMapping(value = "/orderPaymentUsingAjax", method = { RequestMethod.POST })
    @ResponseBody
    public String orderPaymentUsingAjax(@ModelAttribute("PaymentVO") PaymentVO paymentVO, ModelMap model,
                    HttpServletRequest request, HttpServletResponse response) {
    	LOGGER.info("----------------Start :: OrderController : orderPaymentUsingAjax------------------------");

    	String orderPosId = paymentVO.getOrderPosId();
        System.out.println("orderController listOfDiscount Jason:::" + paymentVO.getListOfALLDiscounts());
        HttpSession session = request.getSession();
        Merchant merchant = (Merchant) session.getAttribute("merchant");
        
        LOGGER.info("OrderController :: orderPaymentUsingAjax : merchantId "+merchant.getId());
        
        Customer customer = (Customer) session.getAttribute("customer");
        if(customer!=null && customer.getId()!=null)
        {
        LOGGER.info("OrderController :: orderPaymentUsingAjax : customerId "+customer.getId());
        
        if(customer.getFirstName() != null){
        	LOGGER.info("OrderController :: orderPaymentUsingAjax : customerName "+customer.getFirstName());
        }
        }
        Customer guestCustomer = (Customer) session.getAttribute("guestCustomer");
        if(guestCustomer != null && guestCustomer.getFirstName() != null){
        	LOGGER.info("OrderController :: orderPaymentUsingAjax : guestCustomerName "+guestCustomer.getFirstName());
        }
        
        String responseMessage = "FAIL";
        Boolean gateWayStatus = false;
        String result = "";
        
        LOGGER.info("OrderController :: orderPaymentUsingAjax : paymentType "+paymentVO.getPaymentMethod());
        
        if (merchant!=null && merchant.getOwner().getPos().getPosId() != IConstant.POS_CLOVER) {
            if (!"Cash".equals(paymentVO.getPaymentMethod())) {
            	if(customer!=null){
                responseMessage = chargeCreditCard(paymentVO, merchant, customer, guestCustomer);
            	}else{
            		responseMessage = "FAIL";
            	}
            } else {
                responseMessage = "{\"result\":\"SUCCESS\"}";
                result = "{\"result\":\"APPROVED\" }";
            }
        }

        LOGGER.info("OrderController :: orderPaymentUsingAjax : chargeCreditCardResponse "+responseMessage);
        
        String responseBody = "";
        try {
            if (merchant != null) {
            	if (paymentVO.getOrderPosId() != null
                                && (!paymentVO.getOrderPosId().isEmpty() || !paymentVO.getOrderPosId().equals(""))) {
                	if(paymentVO.getVaultedInfo()!= null && !paymentVO.getVaultedInfo().equals("ADD_NEW_CARD")
					&& !paymentVO.getVaultedInfo().isEmpty() && paymentVO.getVaultedInfo()!=""){
                	String[] vaultedInfo = paymentVO.getVaultedInfo().split("_");
                	Integer vaultedId = Integer.parseInt(vaultedInfo[0]);
                CardInfo cardInfo =	orderService.getVaultedDetail(vaultedId, vaultedInfo[1]);
                	if(cardInfo.getToken()!= null && !cardInfo.getToken().isEmpty() && cardInfo.getFirst6()!= null && !cardInfo.getFirst6().isEmpty() && cardInfo.getLast4()!= null && !cardInfo.getLast4().isEmpty() && cardInfo.getExpirationDate()!= null && !cardInfo.getExpirationDate().isEmpty()&& cardInfo.getToken()!= "" && cardInfo.getFirst6()!= "" && cardInfo.getLast4()!= "" && cardInfo.getExpirationDate()!= "" ){
                		
                		String decryptValue = orderService.getEncyKey(cardInfo.getToken(), merchant.getId());
                  
                  
                		
                		paymentVO.setToken(decryptValue);
                		paymentVO.setFirst6(cardInfo.getFirst6());
                		paymentVO.setLast4(cardInfo.getLast4());
                		paymentVO.setExpirationDate(cardInfo.getExpirationDate());
                		paymentVO.setIsVaulted(true);
                	}else{
                		MailSendUtil.webhookMail("vaulted payment failed","vaulted payment is is blank for card id "+vaultedId,environment);
                	}
                	}else{
                		paymentVO.setIsVaulted(false);
                	}
                	
                    Gson gson = new Gson();
                    String paymentJson = gson.toJson(paymentVO);
                    String orderType = (String) session.getAttribute("orderType");
                    String merchantLogo = "";
                    if (merchant != null) {
                        if (merchant.getMerchantLogo()!= null && ! merchant.getMerchantLogo().isEmpty()
						 && !merchant.getMerchantLogo().equals("")) {
                            
                            merchantLogo = environment.getProperty("BASE_PORT") + merchant.getMerchantLogo();
                        } else {
                        	merchantLogo = "http://www.dev.foodkonnekt.com/app/foodnew.jpg";
                        }
                    }
                    //String result ="";
                    if(merchant.getOwner()!=null && merchant.getOwner().getPos()!=null
					&& merchant.getOwner().getPos().getPosId()!=null
					&& merchant.getOwner().getPos().getPosId()==IConstant.POS_CLOVER){
                     result = ProducerUtil.orderPayment(paymentJson, merchant,environment);
                     LOGGER.info("----------------Start :: OrderController : ProducerUtil.orderPayment------------------------ " +result);
                    }else{
                    	if(!responseMessage.contains("DECLINED"))
                    	 result ="{\"result\":\"APPROVED\" }";
                    	else
                    	result ="{\"result\":\"DECLINED\" }";
                    }
   
                    boolean updateStatus = false;
                    boolean discountSaveStatus = false;
                    
                    if(customer==null){
                    	customer=guestCustomer;
                    	session.removeAttribute("guestCustomer");
                    }
                    
//                    if(paymentVO.getSaveCard()==null){
//                    	paymentVO.setSaveCard(false);
//                    }
                    String name="";
                    if(customer!=null && customer.getFirstName()!=null)
                     name=customer.getFirstName();
    				if (customer.getLastName()!=null) {
    					name+= " "+customer.getLastName();
    				}
                    
                    if(paymentVO!=null && paymentVO.getOrderPosId()!=null && customer!=null && customer.getId()!=null && paymentVO.getPaymentMethod()!=null && paymentVO.getSubTotal()!=null && paymentVO.getTax()!=null && customer.getFirstName()!=null &&
                    		customer.getEmailId()!=null && merchant.getName()!=null  && orderType!=null  &&  paymentVO.getTotal()!=null  && merchant!=null){
                    	updateStatus=orderService.updateOrderStatus(paymentVO.getOrderPosId(), result,
                                    customer.getId(), paymentVO.getPaymentMethod(), paymentVO.getSubTotal(),
                                    paymentVO.getTax(), name, customer.getEmailId(),
                                    merchant.getName(), merchantLogo, orderType, paymentVO.getTip(),
                                    paymentVO.getTotal(), paymentVO.getFutureOrderType(), paymentVO.getFutureDate(),
                                    paymentVO.getFutureTime(),merchant,paymentVO.getListOfALLDiscounts(), paymentVO.getCcType(), paymentVO.getSaveCard(),paymentVO.getAuxTax());
                    	
                    	LOGGER.info("OrderController :: orderPaymentUsingAjax : updateStatus "+updateStatus);
                    }
                    if (updateStatus) {
                        if (result.contains("result") && !result.contains("DECLINED")) {
                            try {
                                JSONObject orderResult = new JSONObject(result);
                                if (orderResult.toString().contains("result")) {
                                    String status = orderResult.getString("result");
                                    if (status.equals("APPROVED") || status.equals("SUCCESS")) {
                                        model.addAttribute("message", "Your order is placed successfully");
                                        responseBody = "APPROVED";
                                    } else {
                                        responseBody = "DECLINED";
                                        
                                        if(merchant.getOwner()!=null && merchant.getOwner().getPos()!=null && merchant.getOwner().getPos().getPosId()!=null &&
                                        		merchant.getOwner().getPos().getPosId()==1){
                                        	String orderDeleteResponse = CloverUrlUtil.deleteOrder(
                                                    merchant.getPosMerchantId(), merchant.getAccessToken(),
                                                    paymentVO.getOrderPosId(),environment);
                                        	// orderService.deleteAnOrder(customer.getId(), paymentVO.getOrderPosId());
                                        	LOGGER.info("----------------Start :: OrderController : orderDeleteResponse------------------------ " +orderDeleteResponse);
                                    System.out.println("orderDeleteResponse" + orderDeleteResponse);
                                        }
                                        
                                    }
                                } else {
                                    responseBody = "FAILED";
                                    if(merchant.getOwner()!=null && merchant.getOwner().getPos()!=null && merchant.getOwner().getPos().getPosId()!=null &&
                                    		merchant.getOwner().getPos().getPosId()==1){
                                    	String orderDeleteResponse = CloverUrlUtil.deleteOrder(
                                                merchant.getPosMerchantId(), merchant.getAccessToken(),
                                                paymentVO.getOrderPosId(),environment);
                                    	// orderService.deleteAnOrder(customer.getId(), paymentVO.getOrderPosId());
                                    	LOGGER.info("----------------Start :: OrderController : orderDeleteResponse------------------------ " +orderDeleteResponse);
                                        System.out.println("orderDeleteResponse" + orderDeleteResponse);
                                    }
                                    MailSendUtil.orderFaildMail("MerchantName -> " + merchant.getName()
                                                    + " ,MerchantId-> " + merchant.getPosMerchantId() + ", orderId -> "
                                                    + paymentVO.getOrderPosId() + ",Oder Total amount -->"
                                                    + paymentVO.getTotal() + " Payment Type-->"
                                                    + paymentVO.getPaymentMethod() + " Payment API response" + result,environment);
                                    model.addAttribute("message",
                                                    "Your order was not placed. Please try again or call us");
                                    session.setAttribute("failOrder", "Y");
                                }
                            } catch (Exception exception) {
                                responseBody = "FAILED";
                                LOGGER.error("OrderController :: orderPaymentUsingAjax : Exception "+exception);
                                if(merchant.getOwner()!=null && merchant.getOwner().getPos()!=null && merchant.getOwner().getPos().getPosId()!=null &&
                                		merchant.getOwner().getPos().getPosId()==1){
                                	String orderDeleteResponse = CloverUrlUtil.deleteOrder(
                                            merchant.getPosMerchantId(), merchant.getAccessToken(),
                                            paymentVO.getOrderPosId(),environment);
                                	// orderService.deleteAnOrder(customer.getId(), paymentVO.getOrderPosId());
                                	System.out.println("orderDeleteResponse" + orderDeleteResponse);
                                	LOGGER.info("---------------- :: OrderController : orderDeleteResponse------------------------ " +orderDeleteResponse);
                                }
                                if (exception != null) {
                                    MailSendUtil.sendExceptionByMail(exception,environment);
                                }
                                System.out.println(exception);
                            }
                            /*OrderServiceImpl impl = new OrderServiceImpl();
                            impl.start();*/
                            
                        } else {
                        	responseBody="DECLINED";
                        	 if(merchant.getOwner()!=null && merchant.getOwner().getPos()!=null && merchant.getOwner().getPos().getPosId()!=null &&
                             		merchant.getOwner().getPos().getPosId()==1){
                             	String orderDeleteResponse = CloverUrlUtil.deleteOrder(
                                         merchant.getPosMerchantId(), merchant.getAccessToken(),
                                         paymentVO.getOrderPosId(),environment);
                             	// orderService.deleteAnOrder(customer.getId(), paymentVO.getOrderPosId());
                             	LOGGER.info("----------------Start :: OrderController : orderDeleteResponse------------------------ " +orderDeleteResponse);
                             	System.out.println("orderDeleteResponse" + orderDeleteResponse);
                             }
                            
                            model.addAttribute("message", "Your order was not placed. Please try again or call us");
                            session.setAttribute("failOrder", "Y");
                        }
                    } else {
                        responseBody = "FAILED";
                        if(merchant.getOwner()!=null && merchant.getOwner().getPos()!=null && merchant.getOwner().getPos().getPosId()!=null &&
                        		merchant.getOwner().getPos().getPosId()==1){
                        	String orderDeleteResponse = CloverUrlUtil.deleteOrder(
                                    merchant.getPosMerchantId(), merchant.getAccessToken(),
                                    paymentVO.getOrderPosId(),environment);
                        	// orderService.deleteAnOrder(customer.getId(), paymentVO.getOrderPosId());
                        	LOGGER.info("----------------Start :: OrderController : orderDeleteResponse------------------------ " +orderDeleteResponse);
                        	System.out.println("orderDeleteResponse" + orderDeleteResponse);
                        }
                        model.addAttribute("message", "Your order was not placed. Please try again or call us");
                        // return "redirect:" + environment.getProperty("WEB_BASE_URL") + "/order";
                    }

                } else {
                	responseBody="FAILED";
                	if(merchant!=null && paymentVO!=null && customer!=null){
                		 if(merchant.getOwner()!=null && merchant.getOwner().getPos()!=null && merchant.getOwner().getPos().getPosId()!=null &&
                         		merchant.getOwner().getPos().getPosId()==1){
                         	String orderDeleteResponse = CloverUrlUtil.deleteOrder(
                                     merchant.getPosMerchantId(), merchant.getAccessToken(),
                                     paymentVO.getOrderPosId(),environment);
                         	// orderService.deleteAnOrder(customer.getId(), paymentVO.getOrderPosId());
                         	LOGGER.info("----------------Start :: OrderController : orderDeleteResponse------------------------ " +orderDeleteResponse);
                         	System.out.println("orderDeleteResponse" + orderDeleteResponse);
                         }
                	
                	MailSendUtil.orderFaildMail("session has been expired before placing an order <br> MerchantName -> " +merchant.getName()+" ,MerchantId-> "+merchant.getPosMerchantId()+", orderId -> "+paymentVO.getOrderPosId()+",Oder Total amount -->"+paymentVO.getTotal()+" Payment Type-->"+paymentVO.getPaymentMethod(),environment);
                	//MailSendUtil.orderFaildMail("MerchantName -> " +merchant.getName()+" ,MerchantId-> "+merchant.getPosMerchantId()+", orderId -> "+paymentVO.getOrderPosId());
                	}
                	model.addAttribute("message", "Your order was not placed. Please try again or call us");
                   // return "redirect:" + environment.getProperty("WEB_BASE_URL") + "/order";

                }
            } else {
                LOGGER.info("----------------End :: OrderController : orderPaymentUsingAjax------------------------");

                return "redirect:https://www.foeodkonnekt.com";
            }
        } catch (Exception exception) {

        	LOGGER.error("OrderController :: orderPaymentUsingAjax : Exception "+exception);
        	
            if (exception != null) {
                responseBody = "FAILED";
                MailSendUtil.sendExceptionByMail(exception,environment);
                /*
                 * String orderDeleteResponse = CloverUrlUtil.deleteOrder(merchant.getPosMerchantId(),
                 * merchant.getAccessToken(), paymentVO.getOrderPosId());
                 */
                // orderService.deleteAnOrder(customer.getId(), paymentVO.getOrderPosId());
                // System.out.println("orderDeleteResponse" + orderDeleteResponse);

                if(merchant.getOwner()!=null && merchant.getOwner().getPos()!=null && merchant.getOwner().getPos().getPosId()!=null &&
                		merchant.getOwner().getPos().getPosId()==1){
                	String orderDeleteResponse = CloverUrlUtil.deleteOrder(
                            merchant.getPosMerchantId(), merchant.getAccessToken(),
                            paymentVO.getOrderPosId(),environment);
                	// orderService.deleteAnOrder(customer.getId(), paymentVO.getOrderPosId());
                	LOGGER.info("----------------Start :: OrderController : orderDeleteResponse------------------------ " +orderDeleteResponse);
                	System.out.println("orderDeleteResponse" + orderDeleteResponse);
                }

                model.addAttribute("message", "Your order was not placed. Please try again or call us");
                session.setAttribute("failOrder", "Y");
            }
            System.out.println(exception);
        }
        
        LOGGER.info("OrderController : orderPaymentUsingAjax : response "+responseBody);
        if(responseBody.equalsIgnoreCase("FAILED") && orderPosId!=null) {
        	OrderR orderR = orderRepository.findByOrderPosId(orderPosId);
        	if(orderR!=null) {
        		orderR.setIsDefaults(3);
        		orderRepository.save(orderR);
        	}
        }
        LOGGER.info("----------------End :: OrderController : orderPaymentUsingAjax------------------------");
        return responseBody;
    }

    @RequestMapping(value = "/getMenuItems", method = RequestMethod.GET)
    @ResponseBody
    public String getMenuItems(HttpServletRequest request, HttpServletResponse response,
                    @RequestParam Integer categoryId, @RequestParam(required = false) Integer offSet) {
    	 LOGGER.info("----------------Start :: OrderController : getMenuItems------------------");
    	
        String categoryItemsJson = "Not Found";
        try {
            HttpSession session = request.getSession();
            if (session != null) {
                Merchant merchant = (Merchant) session.getAttribute("merchant");
                if (merchant != null) {
                	LOGGER.info("OrderController : getMenuItems : merchantId "+merchant.getId());
                    List<ItemDto> itemDtos = orderService.findCategoryItems(categoryId, merchant, offSet);
                    Gson gson = new Gson();
                    categoryItemsJson = gson.toJson(itemDtos);
                }
            }

        } catch (Exception exception) {
            if (exception != null) {
            	LOGGER.error("OrderController :: getMenuItems : Exception "+exception);
                MailSendUtil.sendExceptionByMail(exception,environment);
            }
            LOGGER.error("OrderController :: getMenuItems : Exception "+exception);
            exception.printStackTrace();
        }
        LOGGER.info("----------------End :: OrderController : getMenuItems------------------");
        LOGGER.info("OrderController :: getMenuItems : categoryItemsJson "+categoryItemsJson);
        return categoryItemsJson;
    }

    @RequestMapping(value = "/getMenuCategories", method = RequestMethod.GET)
    @ResponseBody
    public String getMenuCategories(HttpServletRequest request, HttpServletResponse response,
                    @RequestParam Integer page) {
        LOGGER.info("----------------Start :: OrderController : getMenuCategories-------");

        String categoryItemsJson = "Not Found";
        try {
            HttpSession session = request.getSession();
            if (session != null) {
                Merchant merchant = (Merchant) session.getAttribute("merchant");
                if (merchant != null) {
                    List<CategoryDto> categories = orderService.findCategoriesByMerchantIdWithLimit(merchant.getId(),
                                    page);
                    Gson gson = new Gson();
                    categoryItemsJson = gson.toJson(categories);
                }
            }

        } catch (Exception exception) {
            if (exception != null) {
                MailSendUtil.sendExceptionByMail(exception,environment);
            }
        	LOGGER.error("OrderController :: checkCustomerType : Exception "+exception);

            exception.printStackTrace();
        }
        LOGGER.info("----------------End :: OrderController : getMenuCategories-------categoryItemsJson----------- "+categoryItemsJson);
        return categoryItemsJson;
    }

    @RequestMapping(value = "/checkCustomerType", method = RequestMethod.GET)
    @ResponseBody
    public String checkCustomerType(HttpServletRequest request, HttpServletResponse response,
                    @RequestParam String emailId, @RequestParam Integer merchantId) {
    	LOGGER.info("----------------Start :: OrderController : checkCustomerType------------------");
        String customerResponse = "Not Found";
        try {
            HttpSession session = request.getSession();
            Vendor vendor = null;
            if (session != null) {
                vendor = (Vendor) session.getAttribute("vendor");
            }
            List<Customer> customers = null;
            if (vendor != null && vendor.getId() != null) {
                customers = customerService.findByEmailIDAndVendorId(emailId, vendor.getId());
            } else {
                customers = customerService.findByEmailIDAndMerchantId(emailId, merchantId);
            }
            LOGGER.info("OrderController :: checkCustomerType : merchantId "+merchantId);
            for (Customer customer : customers) {
                if (customer.getCustomerType() != null && customer.getCustomerType().toLowerCase().equals("php")) {
                    customerResponse = "php";
                    LOGGER.info("OrderController :: checkCustomerType : customerResponse "+customerResponse);
                } else {
                    customerResponse = "java";
                    LOGGER.info("OrderController :: checkCustomerType : customerResponse "+customerResponse);
                }
            }

        } catch (Exception exception) {
            if (exception != null) {
            	LOGGER.error("OrderController :: checkCustomerType : Exception "+exception);
                MailSendUtil.sendExceptionByMail(exception,environment);
            }
            LOGGER.error("OrderController :: checkCustomerType : Exception "+exception);
            exception.printStackTrace();
        }
        LOGGER.info("----------------End :: OrderController : customerResponse------------------"+customerResponse);
        return customerResponse;
    }

    @RequestMapping(value = "/getItemModiferGroups", method = RequestMethod.GET)
    @ResponseBody
    public String getItemModiferGroups(HttpServletRequest request, HttpServletResponse response,
                    @RequestParam Integer itemId, @RequestParam Integer allowModifierLimit) {
    	
    	LOGGER.info("----------------Start :: OrderController : getItemModiferGroups------------------");
        String itemModifierGroupJson = "Not Found";
        try {
            HttpSession session = request.getSession();
            if (session != null) {
                Merchant merchant = (Merchant) session.getAttribute("merchant");
                if (merchant != null) {
                	LOGGER.info("OrderController :: getItemModiferGroups : merchantId "+merchant.getId());
                    List<ModifierGroupDto> itemModifierGroupDtos = orderService.getModifierGroup(itemId,
                                    allowModifierLimit, merchant);
                    Gson gson = new Gson();
                    itemModifierGroupJson = gson.toJson(itemModifierGroupDtos);
                }
            }

        } catch (Exception exception) {
            if (exception != null) {
            	LOGGER.error("OrderController :: getItemModiferGroups : Exception "+exception);
                MailSendUtil.sendExceptionByMail(exception,environment);
            }
            exception.printStackTrace();
        }
        LOGGER.info("---------------- :: OrderController : getItemModiferGroups------itemModifierGroupJson------------"+itemModifierGroupJson);
        LOGGER.info("----------------End :: OrderController : getItemModiferGroups------------------");
        return itemModifierGroupJson;
    }

    @RequestMapping(value = "/checkMinDeliveryAmount", method = RequestMethod.GET)
    @ResponseBody
    public Double checkMinDelivery(HttpServletRequest request, HttpServletResponse response,
                    @RequestParam Double subTotalAmount) {
        LOGGER.info("----------------Start :: OrderController : checkMinDelivery------");

        HttpSession session = request.getSession();
        Merchant merchant = (Merchant) session.getAttribute("merchant");
        if (merchant != null) {
            LOGGER.info("----------------End :: OrderController : checkMinDelivery------");

            return zoneService.checkMinDeliveryAmount(subTotalAmount, merchant.getId());
        } else {
            LOGGER.info("----------------End :: OrderController : checkMinDelivery------");

        	return null;
    }
    }

    @RequestMapping(value = "/getOrderFullFill", params = { "date", "merchantId" }, method = RequestMethod.GET)
    @ResponseBody
    public String getOrderFulFilledDate(Map<String, Object> map, HttpServletRequest request,
                    @RequestParam(value = "date") String date, @RequestParam(value = "merchantId") String merchantId) {
    	 LOGGER.info("----------------Start :: OrderController : getOrderFullFill------------------");
        long dateValue = Long.valueOf(date);
        Date dateObj = new Date(dateValue);
        String jsonString = "Not Found";
        try {
            Merchant merchant = merchantService.findbyPosId(merchantId);
            if (merchant != null) {
                List<OrderR> orderData = orderService.findAllOrdersByFulfilledDate(dateObj, merchant);
                List<FulFilledOrder> filledOrders = new ArrayList<FulFilledOrder>();
                for (OrderR orderR : orderData) {
                    if (orderR != null && orderR.getIsDefaults() != null && orderR.getIsDefaults() != 3) {
                        FulFilledOrder filledOrder = new FulFilledOrder();
                        filledOrder.setOrderDate(orderR.getCreatedOn());
                        filledOrder.setFullfieldOrder(orderR.getFulfilled_on());
                        filledOrder.setOrderID(orderR.getOrderPosId() + "");
                        filledOrder.setOrderType(orderR.getOrderType());
                        filledOrder.setPickUpTime(orderR.getFulfilled_on() + "");
                        filledOrder.setFirstName(orderR.getCustomer().getFirstName());
                        if(orderR.getCustomer().getLastName()!=null)
                            filledOrder.setLastName(orderR.getCustomer().getLastName());
                        filledOrder.setPhoneNumber(orderR.getCustomer().getPhoneNumber());
                        filledOrder.setEmailId(orderR.getCustomer().getEmailId());
                        filledOrder.setOrderStatus(orderR.getIsDefaults());
                        filledOrders.add(filledOrder);
                    }
                }
                Gson gson = new Gson();
                jsonString = gson.toJson(filledOrders);
            }
        } catch (Exception exception) {
            if (exception != null) {
                MailSendUtil.sendExceptionByMail(exception,environment);
            }
        	LOGGER.error("OrderController :: getOrderFullFill : Exception "+exception);

            exception.printStackTrace();
        }
        LOGGER.info("----------------:: OrderController : getOrderFullFill--------jsonString---------- "+jsonString);
        LOGGER.info("----------------End :: OrderController : getOrderFullFill------------------ ");
        return jsonString;
    }

    /*
     * @RequestMapping(value = "/getAllOrderFullFill", params = { "date", "locationUid" }, method = RequestMethod.GET)
     *
     * @ResponseBody public String getAllOrderFullFill(Map<String, Object> map, HttpServletRequest request,
     *
     * @RequestParam(value = "date") String date, @RequestParam(value = "locationUid") String locationUid) { long
     * dateValue = Long.valueOf(date); Date dateObj = new Date(dateValue); String jsonString = "Not Found"; try {
     * Merchant merchant = merchantService.findByMerchantUid(locationUid); if (merchant != null) { List<OrderR>
     * orderData = orderService.findAllOrdersByFulfilledDate(dateObj, merchant.getId()); List<FulFilledOrder>
     * filledOrders = new ArrayList<FulFilledOrder>(); for (OrderR orderR : orderData) { FulFilledOrder filledOrder =
     * new FulFilledOrder(); filledOrder.setOrderDate(orderR.getCreatedOn()); filledOrder.setOrderID(orderR.getId() +
     * ""); filledOrder.setOrderType(orderR.getOrderType()); filledOrder.setPickUpTime(orderR.getFulfilled_on() + "");
     *
     * filledOrder.setFirstName(orderR.getCustomer().getFirstName());
     * filledOrder.setLastName(orderR.getCustomer().getLastName());
     * filledOrder.setPhoneNumber(orderR.getCustomer().getPhoneNumber());
     * filledOrder.setEmailId(orderR.getCustomer().getEmailId()); filledOrders.add(filledOrder); } Map<String,Object>
     * orders=new HashMap<String,Object>(); orders.put("allOrders", filledOrders); Gson gson = new Gson(); jsonString =
     * gson.toJson(orders); } } catch (Exception exception) { if (exception != null) {
     * MailSendUtil.sendExceptionByMail(exception,environment); } exception.printStackTrace(); }
     *
     * return jsonString; }
     */

    @RequestMapping(value = "/getAllOrderFullFill", params = { "date", "locationUid" }, method = RequestMethod.GET)
    @ResponseBody
    public String getAllOrderFullFill(Map<String, Object> map, HttpServletRequest request,
                    @RequestParam(value = "date") String date, @RequestParam(value = "locationUid") String locationUid) {
        /*long dateValue = Long.valueOf(date);
        Date dateObj = new Date(dateValue);*/
        String futureOrders = null;
        try {
        	LOGGER.info("----------------Start :: OrderController : getAllOrderFullFill------------------ ");
            Merchant merchant = merchantService.findByMerchantUid(locationUid);
            
            if (merchant != null) {
            	TimeZone.setDefault(TimeZone.getTimeZone("CST"));
            	java.util.Date dateObj= new java.util.Date();
        	        dateObj.setHours(0);
        	        dateObj.setMinutes(0);
        	        dateObj.setSeconds(0);
                List<OrderR> orderData = orderService.findAllOrdersByFulfilledDate(dateObj, merchant);
                List<FulFilledOrder> filledOrders = new ArrayList<FulFilledOrder>();
                /*
                 * for (OrderR orderR : orderData) { FulFilledOrder filledOrder = new FulFilledOrder();
                 * filledOrder.setOrderDate(orderR.getCreatedOn()); filledOrder.setOrderID(orderR.getId() + "");
                 * filledOrder.setOrderType(orderR.getOrderType()); filledOrder.setPickUpTime(orderR.getFulfilled_on() +
                 * "");
                 *
                 * filledOrder.setFirstName(orderR.getCustomer().getFirstName());
                 * filledOrder.setLastName(orderR.getCustomer().getLastName());
                 * filledOrder.setPhoneNumber(orderR.getCustomer().getPhoneNumber());
                 * filledOrder.setEmailId(orderR.getCustomer().getEmailId()); filledOrders.add(filledOrder);
                 *
                 *
                 * }
                 */
                System.out.println(filledOrders);
                List<String> notifications = new ArrayList<String>();
                for (OrderR orderR : orderData) {
                    String orderNotificationJson = orderService.getNotificationJSON(orderR.getId(),
                                    merchant.getModileDeviceId(), merchant.getMerchantUid(),
                                    merchant.getPosMerchantId());
                    notifications.add(orderNotificationJson);
                }
                if (notifications.size() > 0) {
                    futureOrders = "";
                    futureOrders = futureOrders + "{\"code\":200 ,";
                    futureOrders = futureOrders + "\"orders\":" + notifications.toString() + "}";
                    LOGGER.info("----------------Start :: OrderController : getAllOrderFullFill--------futureOrders---------- "+futureOrders);
                    return futureOrders;
                } else {
                    futureOrders = "";
                    futureOrders = futureOrders + "{\"code\":0 ,";
                    futureOrders = futureOrders + "\"orders\":[]}";
                    LOGGER.info("----------------Start :: OrderController : getAllOrderFullFill--------futureOrders---------- "+futureOrders);
                    return futureOrders;
                }
            }
        } catch (Exception exception) {
            if (exception != null) {
            	LOGGER.info("----------------Start :: OrderController : getAllOrderFullFill--------exception---------- "+exception);
                MailSendUtil.sendExceptionByMail(exception,environment);
            }
            exception.printStackTrace();
        }
        if (futureOrders == null) {
            futureOrders = "";
            futureOrders = futureOrders + "{\"code\":0 ,";
            futureOrders = futureOrders + "\"orders\":[]}";
            LOGGER.info("----------------Start :: OrderController : getAllOrderFullFill--------futureOrders---------- "+futureOrders);
            return futureOrders;
        }
        LOGGER.info("----------------End :: OrderController : getAllOrderFullFill------------------ ");
        return futureOrders;
    }

    @RequestMapping(value = "/updateOrder", method = RequestMethod.GET)
    @ResponseBody
    public String updateOrder(Map<String, Object> map, HttpServletRequest request,
                    @RequestParam(value = "date") String date, @RequestParam(value = "merchantId") String merchantId,
                    @RequestParam(value = "reason") String reason, @RequestParam(value = "orderId") Integer orderId) {
    	LOGGER.info("----------------Start :: OrderController : updateOrder------------------ ");
        String jsonString = "Not Found";
        try {
            long dateValue = Long.valueOf(date);
            Date dateObj = new Date(dateValue);
            DateFormat df = new SimpleDateFormat("MM-dd-yyyy hh:mm a");
            System.out.println(df.format(dateObj));
            OrderR orderR = orderService.findOrderByID(orderId);
            orderR.setFulfilled_on(dateObj);
            orderService.saveOrder(orderR);
            orderService.sendMailUser(orderR, reason, df.format(dateObj));
        } catch (Exception exception) {
            if (exception != null) {
            	LOGGER.info("----------------Start :: OrderController : updateOrder-------exception----------- "+exception);
                MailSendUtil.sendExceptionByMail(exception,environment);
            }
            exception.printStackTrace();
        }
        LOGGER.info("----------------End :: OrderController : updateOrder------------------ ");
        return jsonString;
    }

    /**
     * Find opening hour for future order
     *
     * @param futureDate
     * @return String
     */
    @RequestMapping(value = "/getFutureDateOpeningTime", method = RequestMethod.GET)
    @ResponseBody
    public List<String> findFuturOpeningtime(HttpServletRequest request,
                    @RequestParam(value = "futureDate") String futureDate,
                    @RequestParam(value = "orderType") String orderType) {
    	LOGGER.info("----------------Start :: OrderController : getFutureDateOpeningTime------------------");
        try {
            HttpSession session = request.getSession();
            Merchant merchant = (Merchant) session.getAttribute("merchant");
            if (merchant != null) {
            	
                return businessService.findFutureOrderOpeningHours(futureDate, merchant.getId(), orderType);
            }
            return new ArrayList<String>();
        } catch (Exception exception) {
            if (exception != null) {
            	LOGGER.error("OrderController :: getFutureDateOpeningTime :: Exception "+exception);
                MailSendUtil.sendExceptionByMail(exception,environment);
            }
            exception.printStackTrace();
            LOGGER.info("----------------End :: OrderController : getFutureDateOpeningTime------------------");
            return new ArrayList<String>();
        }
    }

    @RequestMapping(value = "/getFutureDates", method = RequestMethod.GET)
    @ResponseBody
    public List<String> getFutureDates(HttpServletRequest request) {
    	
    	LOGGER.info("----------------Start :: OrderController : getFutureDates------------------");
        try {
            HttpSession session = request.getSession();
            Merchant merchant = (Merchant) session.getAttribute("merchant");
            if (merchant != null) {
            	LOGGER.info("OrderController : getFutureDates : merchantId "+merchant.getId());
                return businessService.findFutureDates(merchant);
            }
            return new ArrayList<String>();
        } catch (Exception exception) {
            if (exception != null) {
            	LOGGER.error("OrderController :: getFutureDates : Exception "+exception);
                MailSendUtil.sendExceptionByMail(exception,environment);
            }
            exception.printStackTrace();
            LOGGER.info("----------------End :: OrderController : getFutureDates------------------");
            return new ArrayList<String>();
        }
    }

    @RequestMapping(value = "/deleteCardInfo", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> deleteCardInfo(HttpServletRequest request,
                    @RequestParam(value = "cardId") Integer cardId) {
    	 LOGGER.info("----------------Start :: OrderController : deleteCardInfo------------------");
        Map<String, Object> responce = new HashMap<String, Object>();
        Boolean status = false;
        try {
            HttpSession session = request.getSession();
            Merchant merchant = (Merchant) session.getAttribute("merchant");
            Customer customer = (Customer) session.getAttribute("customer");
            if (merchant != null && customer!= null) {
            	LOGGER.info("----------------Start :: OrderController : deleteCardInfo-------:customer----------- "+customer.getId());
            	LOGGER.info("----------------Start :: OrderController : deleteCardInfo-------:cardId----------- "+cardId);
            	status = orderService.deleteCardDetail(customer.getId(), cardId);
            	LOGGER.info("----------------Start :: OrderController : deleteCardInfo-------:deleteCardDetail status----------- "+status);
            	responce.put("status", status);
            	if(status){
            	List<CardInfo> cardInfos = customerService.getCustomerCardInfo(customer.getId());
                List<CardInfo> cardsInfos = new ArrayList<CardInfo>();
                List<CardInfo> expiryCardsInfos = new ArrayList<CardInfo>();
                Calendar calendar = Calendar.getInstance();
                Integer year =Integer.parseInt((""+calendar.get(Calendar.YEAR)).substring(2));
                Integer month = calendar.get(Calendar.MONTH);
          
                for(CardInfo cardInfo : cardInfos){
    				cardInfo.setCustomer(null);
    				cardInfo.setMerchant(null);
    				//cardInfo.getExpirationDate()
    				LOGGER.info("----------------Start :: OrderController : deleteCardInfo-------:ExpirationDateYear----------- "+cardInfo.getExpirationDate().substring(2, 4));
    				LOGGER.info("----------------Start :: OrderController : deleteCardInfo-------:ExpirationDateMonth----------- "+cardInfo.getExpirationDate().substring(0, 2));
    				System.out.println("ExpirationDateYear"+cardInfo.getExpirationDate().substring(2, 4));
    				System.out.println("ExpirationDateMonth"+cardInfo.getExpirationDate().substring(0, 2));
    				if(Integer.parseInt(cardInfo.getExpirationDate().substring(2, 4))>year){
    					cardsInfos.add(cardInfo);
    				}else if(Integer.parseInt(cardInfo.getExpirationDate().substring(2, 4))==year && Integer.parseInt(cardInfo.getExpirationDate().substring(0, 2))>month){
    					cardsInfos.add(cardInfo);
    				}else{
    					expiryCardsInfos.add(cardInfo);
    				}
    				
    			}
                if(merchant!= null&& merchant.getAllowMultiPay()!=null && merchant.getAllowMultiPay()==true){
                	Gson gson = new Gson();
                    String cardsInfosJson = gson.toJson(cardsInfos);
                    String expiryCardsInfosJson = gson.toJson(expiryCardsInfos);
                	session.setAttribute("cardInfo", cardsInfosJson);
                	session.setAttribute("expiryCardsInfos", expiryCardsInfosJson);
                	session.setAttribute("allowMultiPay", true);
                }else{
                	session.setAttribute("allowMultiPay", false);
                }
            	
            	
            	}
            	
            }
        } catch (Exception exception) {
            if (exception != null) {
            	LOGGER.info("----------------Start :: OrderController : deleteCardInfo-------:exception----------- "+exception);
                MailSendUtil.sendExceptionByMail(exception,environment);
                LOGGER.info("----------------Start :: OrderController : deleteCardInfo-------:status----------- "+status);
                responce.put("status", status);
            }
            exception.printStackTrace();
        }
        LOGGER.info("----------------End :: OrderController : deleteCardInfo------------------ ");
        return responce;
    }

    @RequestMapping(value = "/getRepeateOrderList", method = RequestMethod.GET)
    @ResponseBody
    public List<OrderR> getRepeateOrderList(ModelMap model, Map<String, Object> map, HttpServletRequest request,
                    @RequestParam(required = false) String message) {
        HttpSession session = request.getSession();
        Merchant merchant = (Merchant) session.getAttribute("merchant");
        Customer customer = (Customer) session.getAttribute("customer");
        if (customer != null && merchant != null) {
            List<OrderR> repeatedOrders = new ArrayList<OrderR>();
            int flag = 0;
            for (OrderR reOrder : orderService.getAllOrder(customer.getId(), merchant.getId())) {
                repeatedOrders.add(reOrder);
                flag++;
                if (flag == 3) {
                    break;
                }
            }
            return repeatedOrders;
        } else {
            return null;
        }
    }
    
    public String chargeCreditCard(PaymentVO paymentVo,Merchant merchant,Customer customer,Customer guestCustomer){
    	LOGGER.info("----------------Start :: OrderController : chargeCreditCard------------------------");
    	
    	Token token = null;
    	Charge charge =null;
    	Gson gson=new Gson();
		Double totalInDouble = (paymentVo != null && paymentVo.getTotal() != null)
				? Double.parseDouble(paymentVo.getTotal())
				: 0.0;
		Double tipAMount = (paymentVo != null && new Double(paymentVo.getTip()) != null) ? paymentVo.getTip() : 0.0;
		String paymentResponseMessage = "Error in getting Payment API response";
		String responseStatus = "Error in getting Payment Status";
    	 String responseMessage ="DECLINED";
    	 OrderR orderR=null;
    	 if(paymentVo!=null && paymentVo.getOrderPosId()!=null && customer!=null && customer.getId()!=null){
    		 orderR = orderService.findOrderByOrderPosIdAndCustomerId(paymentVo.getOrderPosId(),customer.getId());
    		 if(orderR != null)
    		 LOGGER.info("OrderController :: chargeCreditCard : orderId "+orderR.getId());
    		 LOGGER.info("OrderController :: chargeCreditCard : sub "+paymentVo.getSubTotal());
    		 LOGGER.info("OrderController :: chargeCreditCard : customerId " + customer.getId());
    	 }
    	 if(orderR!=null && orderR.getId()!=null && merchant!=null && merchant.getId()!=null)
    	 {
			LOGGER.info("OrderController :: chargeCreditCard : merchantId " + merchant.getId());
    	 totalInDouble=totalInDouble+tipAMount;
    	 Double totalForCayan=totalInDouble;
    	 Double totalForAuthorizeNet=totalInDouble;
    	 System.out.println("totalForCayan-"+totalForCayan);
    	 totalInDouble=totalInDouble*100;
    	 int totalInInt = (totalForCayan != null) ? (int) Math.round(totalForCayan * 100) : 0;
    	 LOGGER.info("OrderController :: chargeCreditCard : orderAmount "+orderR.getId() +"::"+totalInInt);
    	 
    	 String expiryDate = Integer.toString(paymentVo.getExpYear());
    	 expiryDate =expiryDate.substring(expiryDate.length() - 2);
     	 String expireMonth = Integer.toString(paymentVo.getExpMonth());
     			if(expireMonth.length()<2){
     				expireMonth = "0"+expireMonth;
     			}
     			//expireMonth+""+expiryDate;
     			//LOGGER.info("OrderController :: chargeCreditCard : expireMonth "+expireMonth);
     			//System.out.println("expireMonth-->"+expireMonth);
     			
     			String expMonthDate = expireMonth.concat(expiryDate);
     			LOGGER.info("OrderController :: chargeCreditCard : expMonthDate "+orderR.getId() +"::"+expMonthDate);
    	PaymentGateWay gateway = merchantService.findbyMerchantId(merchant.getId(), false);
    	if(gateway!=null && gateway.getStripeAPIKey()!=null && gateway.getGateWayType().equals("1") && gateway.getIsActive()==true && gateway.getIsDeleted()==false)
    	{
            if(guestCustomer != null && guestCustomer.getFirstName() != null && orderR!=null ){
            	LOGGER.info("OrderController :: chargeCreditCard : Stripe is configured orderId "+ orderR.getId()+" and Merchnat Name "+merchant.getName()+"  and customer Name "+guestCustomer.getFirstName()+" , now processing credit card ");
            }else if(customer != null && customer.getFirstName() != null){
            	LOGGER.info("OrderController :: chargeCreditCard : Stripe is configured orderId "+ orderR.getId()+" and Merchnat Name "+merchant.getName()+"  and customer Name "+customer.getFirstName()+" , now processing credit card ");
            }else{
            	LOGGER.info("OrderController :: chargeCreditCard : Stripe is configured orderId "+ orderR.getId()+" and Merchnat Name "+merchant.getName() + " now processing credit card ");
            }
    	    

    		Stripe.apiKey =EncryptionDecryptionUtil.decryption(gateway.getStripeAPIKey());
    		token =saveCardAndcreateToken(paymentVo.getCcNumber(),paymentVo.getExpMonth(),paymentVo.getExpYear(),paymentVo.getCcCode(),paymentVo.getCcType(), orderR);
    		
    		LOGGER.info("OrderController :: chargeCreditCard : token "+orderR.getId() +"::"+token);
    		
    		if(token!=null)
    		{
    			// String expDate = paymentVo.getExpMonth().toString()+""+paymentVo.getExpYear().toString().substring(2);
				Map<String, Object> chargeParams = new HashMap<String, Object>();
				chargeParams.put("amount", totalInInt);
				chargeParams.put("currency", "usd");
				chargeParams.put("description", "Charge for foodkonnekt");
				chargeParams.put("source", token.getId());
				chargeParams.put("capture", false);
				try
				{
					charge = Charge.create(chargeParams);
					paymentResponseMessage = (charge != null && charge.getFailureMessage() != null) ? charge.getFailureMessage() : paymentResponseMessage;
					responseStatus = (charge != null && charge.getStatus() != null) ? charge.getStatus() : responseStatus;
					if (charge != null && charge.getStatus() != null && charge.getStatus().equals("succeeded"))
					{
						LOGGER.info("OrderController :: chargeCreditCard : Charge api of stripe is called and received object as "+orderR.getId() +"::"+gson.toJson(charge));
						charge = charge.capture();
                        LOGGER.info("OrderController :: chargeCreditCard : Charge of stripe status "+orderR.getId() +"::"+charge.getStatus());

                        CardInfo cardInfo = new CardInfo();
						cardInfo.setCardType(paymentVo.getCcType());
						cardInfo.setCreateDate(new java.util.Date());
						cardInfo.setCustomer(customer);
						LOGGER.info("OrderController :: chargeCreditCard : expMonthDate "+orderR.getId() +"::"+EncryptionDecryptionUtil.encryption(expMonthDate));
						System.out.println("card type- "+EncryptionDecryptionUtil.encryption(paymentVo.getCcType()));
						
						if (paymentVo.getCcType().equals("VISA")
								|| paymentVo.getCcType().equals("Master Card")) {
							cardInfo.setLast4(EncryptionDecryptionUtil
									.encryption(paymentVo.getCcNumber()
											.substring(12)));
						} else {
							cardInfo.setLast4(EncryptionDecryptionUtil
									.encryption(paymentVo.getCcNumber()
											.substring(12)));
						}

						if (paymentVo.getCcNumber() != null) {
							cardInfo.setFirst6(EncryptionDecryptionUtil
									.encryption(paymentVo.getCcNumber()
											.substring(0, 6)));
						}

						if (token.getId() != null) {
							cardInfo.setToken(EncryptionDecryptionUtil
									.encryption(token.getId()));
						}
						cardInfo.setUpdateDate(new java.util.Date());
						//LOGGER.info("OrderController :: chargeCreditCard : expMonthDate "+EncryptionDecryptionUtil
                        //.encryption(expMonthDate));
						//System.out.println(EncryptionDecryptionUtil
						//		.encryption(expMonthDate));

						if (expMonthDate != null) {
							cardInfo.setExpirationDate(EncryptionDecryptionUtil
									.encryption(expMonthDate));
						}

						if (paymentVo.getZipCode() != null) {
							cardInfo.setZipCode(paymentVo.getZipCode());
						}
						CardInfo info = merchantService.saveCustomerCards(cardInfo);

                        OrderPaymentDetail orderPaymentDetail = new OrderPaymentDetail();
                        orderPaymentDetail.setCustomerId(customer.getId());
                        orderPaymentDetail.setMerchantId(merchant.getId());
                        orderPaymentDetail.setTransactionId(charge.getId());
                        /*
                         * orderPaymentDetail.setTransactionId(orderService.
                         * getEncyKey(charge.getId(),merchant.getId()));
                         * orderPaymentDetail
                         * .setPreAuthToken(orderService.getEncyKey
                         * (charge.getId(),merchant.getId()));
                         */
                        orderPaymentDetail
                                .setTransactionId(EncryptionDecryptionUtil
                                        .encryption(charge.getId()));
                        orderPaymentDetail
                                .setPreAuthToken(EncryptionDecryptionUtil
                                        .encryption(charge.getId()));
                        orderPaymentDetail.setPaymentGatewayId(1);
                        orderPaymentDetail.setOrderId(orderR.getId());
        
                if(info!=null){
                    orderPaymentDetail.setCardInfoId(info);
                }
                merchantService.saveOrderPaymentDetail(orderPaymentDetail);
                responseMessage = "SUCCESS";
            }else{
                MailSendUtil.orderFaildMail("MerchantName -> " + merchant.getName() + " ,MerchantId-> "
                        + merchant.getPosMerchantId() + ", orderId -> " + paymentVo.getOrderPosId()
                        + ",Oder Total amount -->" + paymentVo.getTotal() + " Payment Type-->"
                        + paymentVo.getPaymentMethod() + " Payment API response " + responseStatus+""+paymentResponseMessage,environment);
                
                
                MailSendUtil.sendPaymentGatwayMailToAdmin("MerchantName -> " + merchant.getName() + " ,MerchantId-> "
                        + merchant.getPosMerchantId() + ", orderId -> " + paymentVo.getOrderPosId()
                        + ",Oder Total amount -->" + paymentVo.getTotal() + " Payment Type-->"
                        + paymentVo.getPaymentMethod() + " Payment API response " + responseStatus+""+paymentResponseMessage, "preAuth", "Stripe", orderR,paymentResponseMessage,environment);
                LOGGER.error("Charge Credit card-Stripe  : ERROR" + " Payment API response of stripe" +orderR.getId() +"::"+ responseStatus+""+paymentResponseMessage);
            }
            
        } catch (Exception exception) {
                    LOGGER.error("OrderController :: chargeCreditCard : Stripe : ERROR " + orderR.getId() + " " +
                                         exception.getMessage());
                    if(exception!=null){
                    responseMessage = "DECLINED";
                MailSendUtil.sendExceptionByMail(exception,environment);
                MailSendUtil.sendPaymentExceptionByMail(exception, "preAuth" ,"Stripe" ,orderR,exception.getMessage(),environment);
                LOGGER.info("OrderController :: chargeCreditCard : sendPaymentExceptionByMail send");
    			}
    		}
    	}
    		LOGGER.info("OrderController :: chargeCreditCard : responseMessage for Stripe payment gateway is "+orderR.getId() +"::"+responseMessage);
    		LOGGER.info("OrderController :: chargeCreditCard : Stripe End :orderId "+orderR.getId());
        	return responseMessage;
    	} //else{
    		if(gateway!=null && gateway.getAuthorizeLoginId() !=null && gateway.getAuthorizeTransactionKey()!=null && gateway.getGateWayType().equals("2")
    				&& gateway.getIsActive()==true && gateway.getIsDeleted()==false){
    			//Common code to set for all requests
                if(guestCustomer != null && guestCustomer.getFirstName() != null){
                	LOGGER.info("OrderController :: chargeCreditCard : Authorize is configured orderId "+ orderR.getId()+" and Merchnat Name "+merchant.getName()+"  and customer Name "+guestCustomer.getFirstName()+" , now processing credit card ");
                }else if(customer != null && customer.getFirstName() != null){
                	LOGGER.info("OrderController :: chargeCreditCard : Authorize is configured orderId "+ orderR.getId()+" and Merchnat Name "+merchant.getName()+"  and customer Name "+customer.getFirstName()+" , now processing credit card ");
                }else{
                	LOGGER.info("OrderController :: chargeCreditCard : Authorize is configured orderId "+ orderR.getId()+" and Merchnat Name "+merchant.getName() + " now processing credit card ");
                }
                
    			try
    			{
    				ApiOperationBase.setEnvironment(ProducerUtil.getAuthorizeEnv(environment.getProperty("AuthorizeEnv")));
        	        MerchantAuthenticationType merchantAuthenticationType  = new MerchantAuthenticationType() ;
        	        merchantAuthenticationType.setName(EncryptionDecryptionUtil.decryption(gateway.getAuthorizeLoginId()));
        	        merchantAuthenticationType.setTransactionKey(EncryptionDecryptionUtil.decryption(gateway.getAuthorizeTransactionKey()));
        	        ApiOperationBase.setMerchantAuthentication(merchantAuthenticationType);

    	        // Populate the payment data
    	        PaymentType paymentType = new PaymentType();
    	        CreditCardType creditCard = new CreditCardType();
    	        creditCard.setCardNumber(paymentVo.getCcNumber());
    	        creditCard.setExpirationDate(expMonthDate);
    	        paymentType.setCreditCard(creditCard);
//    	        LOGGER.info("OrderController :: chargeCreditCard : totalInDouble "+totalInDouble);
//                System.out.println(totalInDouble);
    	        // Create the payment transaction request
    	        TransactionRequestType txnRequest = new TransactionRequestType();
    	        txnRequest.setTransactionType(TransactionTypeEnum.AUTH_ONLY_TRANSACTION.value());
    	        txnRequest.setPayment(paymentType);
    	        
//    	        System.out.println(BigDecimal.valueOf(totalForAuthorizeNet));
    	        
    	        Double totalConvertedPrice = Double.parseDouble(String.format("%.2f", totalForAuthorizeNet));
    	        
    	        //txnRequest.setAmount(BigDecimal.valueOf(totalForAuthorizeNet));
    	        
    	        txnRequest.setAmount(BigDecimal.valueOf(totalConvertedPrice));
    	        net.authorize.api.contract.v1.OrderType orderType=new net.authorize.api.contract.v1.OrderType();
    	        orderType.setInvoiceNumber(orderR.getId().toString());
    	        
    	        LOGGER.info("OrderController :: chargeCreditCard : Authorize orderId "+orderR.getId());
    	        
    	        txnRequest.setOrder(orderType);
    	        
    	        CustomerDataType customerDataType=null;
	        	CustomerAddressType value=null;
    	     
			if (orderR.getCustomer() != null) {
				customerDataType = new CustomerDataType();

				if (orderR.getCustomer().getEmailId() != null) {
					customerDataType
							.setEmail(orderR.getCustomer().getEmailId());
					customerDataType.setType(CustomerTypeEnum.INDIVIDUAL);
				}

				if (orderR.getCustomer().getFirstName() != null) {
					value = new CustomerAddressType();
					value.setFirstName(orderR.getCustomer().getFirstName());
					
					if (orderR.getCustomer().getLastName() != null) {
						value.setLastName(orderR.getCustomer().getLastName());
					}
					if (orderR.getCustomer().getAddress1() != null) {
						value.setAddress(orderR.getCustomer().getAddress1());
					}
					if (orderR.getCustomer().getCity() != null) {
						value.setCity(orderR.getCustomer().getCity());
					}
					if (orderR.getCustomer().getState() != null) {
						value.setState(orderR.getCustomer().getState());
					}
					if (orderR.getCustomer().getZip() != null) {
						value.setZip(orderR.getCustomer().getZip());
					}
					LOGGER.info("OrderController :: chargeCreditCard : Authorize customerName "+orderR.getId() +"::"+orderR.getCustomer().getFirstName());
				}
				if (value != null) {
					txnRequest.setBillTo(value);
				}
			}
			if (customerDataType != null) {
				txnRequest.setCustomer(customerDataType);
			}
			
    	       // txnRequest.set
    	        // Make the API Request
    	        CreateTransactionRequest apiRequest = new CreateTransactionRequest();
    	        apiRequest.setTransactionRequest(txnRequest);
    	        CreateTransactionController controller = new CreateTransactionController(apiRequest);
    	        controller.execute();


    	        CreateTransactionResponse response = controller.getApiResponse();
    	        if (response!=null && response.getMessages() != null && response.getMessages().getResultCode() != null) {
                    LOGGER.info("OrderController :: chargeCreditCard : Authorize response "+orderR.getId() +"::" + orderR.getId()  + "=>"+gson.toJson(response));
    	            // If API Response is ok, go ahead and check the transaction response
    	            if(response.getMessages().getResultCode() == MessageTypeEnum.OK) {

        	                TransactionResponse result = response.getTransactionResponse();
        	                
        	                if (result != null && result.getResponseCode().equals("1")) {
        	                	
        	                	LOGGER.info("OrderController :: chargeCreditCard : Authorize response code"+orderR.getId() +"::"+result.getResponseCode());
        	                	CardInfo cardInfo = new CardInfo();
        	            		cardInfo.setCardType(paymentVo.getCcType());
        	            		cardInfo.setCreateDate(new java.util.Date());
        	            		cardInfo.setCustomer(customer);
        	            		cardInfo.setExpirationDate(EncryptionDecryptionUtil.encryption(expMonthDate));
        	            		if(paymentVo.getCcType().equals("VISA") || paymentVo.getCcType().equals("Master Card")){
        	                		cardInfo.setLast4(EncryptionDecryptionUtil.encryption(paymentVo.getCcNumber().substring(12)));
        	                		}else{
        	                			cardInfo.setLast4(EncryptionDecryptionUtil.encryption(paymentVo.getCcNumber().substring(11)));
        	                		}
        	            		cardInfo.setFirst6(EncryptionDecryptionUtil.encryption(paymentVo.getCcNumber().substring(0, 6)));
        	            		cardInfo.setToken(EncryptionDecryptionUtil.encryption(result.getTransId()));
        	            		cardInfo.setUpdateDate(new java.util.Date());
        	            		CardInfo info=	merchantService.saveCustomerCards(cardInfo);
        	                	
        	                	
        	                	OrderPaymentDetail orderPaymentDetail = new OrderPaymentDetail();
        	            		orderPaymentDetail.setCustomerId(customer.getId());
        	            		orderPaymentDetail.setMerchantId(merchant.getId());
        	            		/*orderPaymentDetail.setTransactionId(orderService.getEncyKey(result.getTransId(), merchant.getId()) );
        	            		orderPaymentDetail.setPreAuthToken(orderService.getEncyKey(result.getTransId(), merchant.getId()));*/
        	            		orderPaymentDetail.setTransactionId(EncryptionDecryptionUtil.encryption(result.getTransId()));
        	            		orderPaymentDetail.setPreAuthToken(EncryptionDecryptionUtil.encryption(result.getTransId()));
        	            		orderPaymentDetail.setPaymentGatewayId(2);
        	            		orderPaymentDetail.setOrderId(orderR.getId());
        	            		orderPaymentDetail.setCardInfoId(info);
        	            		orderPaymentDetail.setAuthCode(EncryptionDecryptionUtil.encryption(result.getAuthCode()));
        	            		merchantService.saveOrderPaymentDetail(orderPaymentDetail);
        	            		
        	            		
        	            		LOGGER.info("OrderController :: chargeCreditCard : Authorize End : orderId "+orderR.getId());
        	                	responseMessage = "SUCCESS";
        	                	
                                LOGGER.info("OrderController :: chargeCreditCard : Authorize is configured, SUCCESS "+orderR.getId() +"::"+result.getTransId());
                                LOGGER.info("OrderController :: chargeCreditCard : Authorize is configured, ResponseCode "+orderR.getId() +"::"+result.getResponseCode());

        	                	System.out.println(result.toString());
        	                    System.out.println(result.getResponseCode());
        	                    System.out.println("Successful Credit Card Transaction");
        	                    System.out.println(result.getAuthCode());
        	                    System.out.println(result.getTransId()); //40009206593
        	                }
        	                else
        	                {
        	                	responseStatus = (result != null && result.getResponseCode() != null) ? result.getResponseCode() : responseStatus;
        	                	paymentResponseMessage = (result != null && result.getMessages() != null) ? result.getMessages().toString() : paymentResponseMessage;
        	                	responseMessage = "DECLINED";
                                MailSendUtil.sendPaymentGatwayMailToAdmin(responseStatus+"_"+paymentResponseMessage, "preAuth" , "Authorize" ,orderR,paymentResponseMessage,environment);
                                LOGGER.info("OrderController :: chargeCreditCard : Authorize, Failed  orderId "+ orderR.getId()+" and Merchant Name "+merchant.getName()+"  and customer Name "+guestCustomer.getFirstName()+" errorMessage "+paymentResponseMessage+" responseMessage : "+responseMessage);
        	                }
        	            }
        	            else
        	            {
        	            	responseMessage = "DECLINED";
                            MailSendUtil.sendPaymentGatwayMailToAdmin(response.getMessages().getResultCode().toString(), "preAuth" , "Authorize" ,orderR,response.getMessages().toString(),environment);
                            LOGGER.info("OrderController :: chargeCreditCard : Authorize, now processing credit card Failed orderId "+ orderR.getId()+" and Merchnat Name "+merchant.getName()+"  and customer Name "+guestCustomer.getFirstName()+" errorMessage "+response.getMessages().getResultCode().toString()+" responseMessage : "+responseMessage);
        	            }
        	        }else{
        	        	responseMessage ="DECLINED";
        	        	MailSendUtil.sendPaymentGatwayMailToAdmin(responseStatus, "preAuth" , "Authorize" ,orderR,paymentResponseMessage,environment);
                        LOGGER.info("OrderController :: chargeCreditCard : Authorize, now processing credit card Failed orderId "+ orderR.getId()+" and Merchnat Name "+merchant.getName()+"  and customer Name "+guestCustomer.getFirstName()+" errorMessage "+responseStatus+" responseMessage : "+responseMessage);
        	        }
    			}
    			catch (Exception exception)
    			{
                    LOGGER.error("OrderController :: chargeCreditCard : Authorize.net : Exception " +orderR.getId() +"::"+ exception.getMessage());
                    
        			if(exception!=null){
        				responseMessage = "DECLINED";
        			MailSendUtil.sendExceptionByMail(exception,environment);
                    MailSendUtil.sendPaymentExceptionByMail(exception, "preeAuth" , "Authorize",orderR,exception.getMessage(),environment);
        			}
    			}
    			LOGGER.info("OrderController :: chargeCreditCard : Authorize.net : end with responseMsg "+responseMessage);
    			return responseMessage;
    			
    		}
		if(gateway!=null && gateway.getiGateUserName() != null && gateway.getiGatePassword() != null
    				&&  gateway.getGateWayType().equals("4")
    				&& gateway.getIsActive() == true && gateway.getIsDeleted() == false){
    			try
    			{if(orderR!=null && orderR.getId()!=null && merchant!=null && merchant.getId()!=null)
    			{
    				 if(guestCustomer != null && guestCustomer.getFirstName() != null && orderR.getId()!=null && merchant.getName()!=null){
    	                	LOGGER.info("OrderController :: chargeCreditCard : IGate is configured orderId "+ orderR.getId()+" and Merchnat Name "+merchant.getName()+"  and customer Name "+guestCustomer.getFirstName()+" , now processing credit card ");
    	                }else if(customer != null && customer.getFirstName() != null&& merchant.getName()!=null){
    	                	LOGGER.info("OrderController :: chargeCreditCard : IGate is configured orderId "+ orderR.getId()+" and Merchnat Name "+merchant.getName()+"  and customer Name "+customer.getFirstName()+" , now processing credit card ");
    	                }
    				
    				System.setProperty("org.apache.commons.logging.LogFactory",
    						"org.apache.commons.logging.impl.LogFactoryImpl");
    						System.setProperty("org.apache.commons.logging.Log",
    						"org.apache.commons.logging.impl.Log4JLogger");
    						String totalIn = String.valueOf(totalForCayan);
    			SmartPaymentsSoapProxy soapProxy=new SmartPaymentsSoapProxy(environment.getProperty("ENDPOINT_PROD"));
    			
    			Response response = null;
    			response=  soapProxy.processCreditCard(EncryptionDecryptionUtil.decryption(gateway.getiGateUserName()),
    						 EncryptionDecryptionUtil.decryption(gateway.getiGatePassword()), "auth",
    						paymentVo.getCcNumber(), expMonthDate, "Payment Card Track II Mag-Stripe Data",
    						orderR.getCustomer().getFirstName(), totalIn, orderR.getId().toString(), "", "", "", paymentVo.getCcCode(), "<Force>T</Force>");
    				
    		  
    			
    			if(response!=null && response.getMessage()!=null && response.getMessage().contains("APPROVED"))
    			{
    				LOGGER.info("OrderController :: chargeCreditCard : IGate processCreditCard response "+orderR.getId() +"::"+gson.toJson(response));
    				LOGGER.info("OrderController :: chargeCreditCard : IGate processCreditCard response "+orderR.getId() +"::"+response.getMessage());
    				CardInfo cardInfo = new CardInfo();
    				cardInfo.setCardType(paymentVo.getCcType());
    				cardInfo.setCreateDate(new java.util.Date());
    				cardInfo.setCustomer(customer);
    				cardInfo.setExpirationDate(EncryptionDecryptionUtil.encryption(expMonthDate));
    				
    				LOGGER.info("OrderController :: chargeCreditCard : IGate : cardType "+orderR.getId() +"::"+paymentVo.getCcType());
    				
    				if(paymentVo.getCcType().equals("VISA") || paymentVo.getCcType().equals("Master Card"))
    				{
                		cardInfo.setLast4(EncryptionDecryptionUtil.encryption(paymentVo.getCcNumber().substring(12)));
    				}
    				else
    				{
    					cardInfo.setLast4(EncryptionDecryptionUtil.encryption(paymentVo.getCcNumber().substring(11)));
    				}
    				cardInfo.setFirst6(EncryptionDecryptionUtil.encryption(paymentVo.getCcNumber().substring(0, 6)));
    				cardInfo.setToken(EncryptionDecryptionUtil.encryption(response.getAuthCode()));
    				cardInfo.setUpdateDate(new java.util.Date());
    				CardInfo info=	merchantService.saveCustomerCards(cardInfo);
					
					
					OrderPaymentDetail orderPaymentDetail = new OrderPaymentDetail();
					orderPaymentDetail.setCustomerId(customer.getId());
					
					 LOGGER.info("OrderController :: chargeCreditCard : IGate : customerId "+orderR.getId() +"::"+customer.getId());
					 LOGGER.info("OrderController :: chargeCreditCard : IGate : merchantId "+orderR.getId() +"::"+merchant.getId());
					
					orderPaymentDetail.setMerchantId(merchant.getId());
					orderPaymentDetail.setTransactionId(EncryptionDecryptionUtil.encryption(response.getAuthCode()));
					orderPaymentDetail.setPaymentGatewayId(4);
					orderPaymentDetail.setPreAuthToken(EncryptionDecryptionUtil.encryption(response.getPNRef()));
					orderPaymentDetail.setOrderId(orderR.getId());
					orderPaymentDetail.setCardInfoId(info);
					orderPaymentDetail.setAuthCode(EncryptionDecryptionUtil.encryption(response.getAuthCode()));
					merchantService.saveOrderPaymentDetail(orderPaymentDetail);
    				responseMessage = "SUCCESS";
    			}
    			
    			else if(response!=null && response.getMessage()!=null && response.getMessage().contains("APPROVAL"))
        			{
    				LOGGER.info("OrderController :: chargeCreditCard : IGate processCreditCard response "+orderR.getId() +"::"+gson.toJson(response));
    				LOGGER.info("OrderController :: chargeCreditCard : IGate processCreditCard response "+orderR.getId() +"::"+response.getMessage());
        				CardInfo cardInfo = new CardInfo();
        				cardInfo.setCardType(paymentVo.getCcType());
        				cardInfo.setCreateDate(new java.util.Date());
        				cardInfo.setCustomer(customer);
        				cardInfo.setExpirationDate(EncryptionDecryptionUtil.encryption(expMonthDate));
        				if(paymentVo.getCcType().equals("VISA") || paymentVo.getCcType().equals("Master Card"))
        				{
                    		cardInfo.setLast4(EncryptionDecryptionUtil.encryption(paymentVo.getCcNumber().substring(12)));
        				}
        				else
        				{
        					cardInfo.setLast4(EncryptionDecryptionUtil.encryption(paymentVo.getCcNumber().substring(11)));
        				}
        				cardInfo.setFirst6(EncryptionDecryptionUtil.encryption(paymentVo.getCcNumber().substring(0, 6)));
        				cardInfo.setToken(EncryptionDecryptionUtil.encryption(response.getAuthCode()));
        				cardInfo.setUpdateDate(new java.util.Date());
        				CardInfo info=	merchantService.saveCustomerCards(cardInfo);
    					
    					
    					OrderPaymentDetail orderPaymentDetail = new OrderPaymentDetail();
    					orderPaymentDetail.setCustomerId(customer.getId());
    					orderPaymentDetail.setMerchantId(merchant.getId());
    					orderPaymentDetail.setTransactionId(EncryptionDecryptionUtil.encryption(response.getAuthCode()));
    					orderPaymentDetail.setPaymentGatewayId(4);
    					orderPaymentDetail.setPreAuthToken(EncryptionDecryptionUtil.encryption(response.getPNRef()));
    					orderPaymentDetail.setOrderId(orderR.getId());
    					orderPaymentDetail.setCardInfoId(info);
    					merchantService.saveOrderPaymentDetail(orderPaymentDetail);
        				responseMessage = "SUCCESS";
        			}
                else {
                	
                	paymentResponseMessage = (response != null && response.getRespMSG() != null) ? response.getRespMSG()
							: paymentResponseMessage;
                    	LOGGER.info("OrderController :: chargeCreditCard : Authorize, Failed  orderId "+ orderR.getId()+" and Merchant Name "+merchant.getName()+" and customer Name "+guestCustomer.getFirstName()+" response "+paymentResponseMessage);
                    	MailSendUtil.sendPaymentGatwayMailToAdmin(paymentResponseMessage, "preAuth" , "Authorize" ,orderR,paymentResponseMessage,environment);
                        LOGGER.info("OrderController :: chargeCreditCard : IGate : sendPaymentGatwayMailToAdmin send orderId"+orderR.getId());
                    
                }
    			}}
    			catch (RemoteException exception)
    			{
    				exception.printStackTrace();
    				LOGGER.error("OrderController :: chargeCreditCard : IGate : Exception "+orderR.getId() +"::" + exception.getMessage());
    				MailSendUtil.sendExceptionByMail(exception,environment);
                    MailSendUtil.sendPaymentExceptionByMail(exception, "preAuth" , "igate", orderR , exception.getMessage(),environment);
    			}
    			LOGGER.error("OrderController :: chargeCreditCard : IGate : end with responseMessage " +orderR.getId() +"::"+ responseMessage);
    			return responseMessage;
    		}

			if (gateway != null && gateway.getCayanMerchantName() != null && gateway.getFirstDataMerchantId() != null
				&& gateway.getFirstDataPassword() != null && gateway.getMerchantId() != null
				&& gateway.getGateWayType().equals("5") && gateway.getIsActive() == true
				&& gateway.getIsDeleted() == false) {
				
				 if(guestCustomer != null && guestCustomer.getFirstName() != null){
	                	LOGGER.info("OrderController :: chargeCreditCard : FirstData is configured orderId "+ orderR.getId()+" and Merchnat Name "+merchant.getName()+"  and customer Name "+guestCustomer.getFirstName()+" , now processing credit card ");
	                }else if(customer != null && customer.getFirstName() != null){
	                	LOGGER.info("OrderController :: chargeCreditCard : FirstData is configured orderId "+ orderR.getId()+" and Merchnat Name "+merchant.getName()+"  and customer Name "+customer.getFirstName()+" , now processing credit card ");
	                }else{
	                	LOGGER.info("OrderController :: chargeCreditCard : FirstData is configured orderId "+ orderR.getId()+" and Merchnat Name "+merchant.getName() + " now processing credit card ");
	                }
				
				System.setProperty("org.apache.commons.logging.LogFactory",	"org.apache.commons.logging.impl.LogFactoryImpl");
				System.setProperty("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.Log4JLogger");
			try {
				MerchantCredentials credentials = new MerchantCredentials();

				String merchantId = EncryptionDecryptionUtil.decryption(gateway.getFirstDataMerchantId());
				String username = EncryptionDecryptionUtil.decryption(gateway.getFirstDataUserName());
				String password = EncryptionDecryptionUtil.decryption(gateway.getFirstDataPassword());

				org.json.simple.JSONObject request = new org.json.simple.JSONObject();
				LOGGER.info("OrderController :: chargeCreditCard : IGate : totalInInt "+totalInInt);
				System.out.println("totalInInt "+totalInInt);
				request.put("merchid", merchantId);
				request.put("accttype", "VI");
				request.put("account", paymentVo.getCcNumber());
				request.put("expiry", expMonthDate);
				request.put("cvv2", paymentVo.getCcCode());
				request.put("amount", totalInInt);
				request.put("currency", "USD");
				request.put("orderid", orderR.getId());
				request.put("name", "gfgf");
				request.put("Street", "123 Test St");
				request.put("city", "TestCity");
				request.put("region", "TestState");
				request.put("country", "US");
				/*request.put("postal", paymentVo.getZipCode());*/
				request.put("tokenize", "Y");

				CardConnectRestClient client = new CardConnectRestClient(IConstant.firstDataEndPoint, username, password);
				org.json.simple.JSONObject response = client.authorizeTransaction(request);
				
				LOGGER.error("OrderController :: chargeCreditCard : FirstDate : response " +orderR.getId() +"::"+ response+ " orderId "+orderR.getId());
				
				if (response != null)
				{
					if (response.containsKey("resptext"))
					{
						String apiResponse = (String) response.get("resptext");
						LOGGER.error("OrderController :: chargeCreditCard : FirstDate : api response for first data is "+orderR.getId() +"::" + apiResponse);
						
						if (apiResponse != null && apiResponse.equals("Approval"))
						{
							String authcode = (String) response.get("authcode");
							String retref = (String) response.get("retref");
							String firstDataToken = (String) response.get("token");
							String amount = (String) response.get("amount");
							CardInfo cardInfo = new CardInfo();
							cardInfo.setCardType(paymentVo.getCcType());
							cardInfo.setCreateDate(new java.util.Date());
							cardInfo.setCustomer(customer);
							cardInfo.setFirstDataAuthcode(authcode);
							cardInfo.setFirstDataRetref(retref);
							cardInfo.setExpirationDate(EncryptionDecryptionUtil.encryption(expMonthDate));
							org.json.simple.JSONObject request1 = new org.json.simple.JSONObject();
							request1.put("merchid", merchantId);
							request1.put("amount",amount);
							request1.put("currency", "USD");
							request1.put("retref", retref);
							request1.put("authcode",authcode);
							// Invoice ID
							request1.put("invoiceid", orderR.getId());
							SimpleDateFormat  dateFormat =  new  SimpleDateFormat("yyyyMMdd");
							request1.put("orderdate", dateFormat.format(new java.util.Date()));
							org.json.simple.JSONObject response1=CardConnectRestClientExample.captureTransaction(retref,username, password,request1);
							LOGGER.info("OrderController :: chargeCreditCard : IGate : request1 "+request1);
							System.out.println("response1 for first data"+response1);

							if(response1!=null)
							{
								String setlstat=response1.get("setlstat").toString();
								LOGGER.info("OrderController :: chargeCreditCard : : setlstat(Card captured request passed and return status as) "+orderR.getId() +"::"+setlstat);
								System.out.println("setlstat -----> "+setlstat);
								System.out.println("Card captured request passed and return status as "+setlstat);

								if(setlstat=="Queued for Capture" || setlstat.equals("Queued for Capture") )
								{
								
								}
								else if(setlstat=="Accepted" || setlstat.equals("Accepted"))
								{
								
								}
								else
								{
									LOGGER.info("OrderController :: chargeCreditCard : : setlstat(Card captured request failed and return status as) "+orderR.getId() +"::"+setlstat);
									System.out.println("Card captured request failed and return status as "+setlstat);
									return responseMessage=setlstat;
								}
							
							}
							else
							{
								LOGGER.info("OrderController :: chargeCreditCard : Card captured request failed with empty details "+orderR.getId() +"::");
								System.out.println("Card captured request failed with empty details");
								return "Declined";
							}
							if (paymentVo.getCcType().equals("VISA") || paymentVo.getCcType().equals("Master Card")) {
								cardInfo.setLast4(EncryptionDecryptionUtil.encryption(paymentVo.getCcNumber().substring(12)));
							} else {
								cardInfo.setLast4(EncryptionDecryptionUtil.encryption(paymentVo.getCcNumber().substring(12)));
							}
							cardInfo.setFirst6(EncryptionDecryptionUtil.encryption(paymentVo.getCcNumber().substring(0, 6)));
							cardInfo.setFirstDataToken(EncryptionDecryptionUtil.encryption(firstDataToken));
							cardInfo.setUpdateDate(new java.util.Date());
							CardInfo info = merchantService.saveCustomerCards(cardInfo);
							OrderPaymentDetail orderPaymentDetail = new OrderPaymentDetail();
							orderPaymentDetail.setCustomerId(customer.getId());
							orderPaymentDetail.setMerchantId(merchant.getId());
							orderPaymentDetail.setTransactionId(EncryptionDecryptionUtil.encryption(firstDataToken));
							orderPaymentDetail.setPreAuthToken(EncryptionDecryptionUtil.encryption(authcode));
							orderPaymentDetail.setPaymentGatewayId(3);
							orderPaymentDetail.setFirstDataRetRef(retref);
							orderPaymentDetail.setOrderId(orderR.getId());
							orderPaymentDetail.setCardInfoId(info);
							merchantService.saveOrderPaymentDetail(orderPaymentDetail);

							responseMessage = "SUCCESS";
						} else {
							responseMessage = (String) response.get("resptext");
							 LOGGER.info("OrderController :: chargeCreditCard : FirstData is configured, Failed "+orderR.getId() +"::"+response.get("resptext"));
	                        MailSendUtil.sendPaymentGatwayMailToAdmin(apiResponse, "preAuth" , "FirstData" , orderR,apiResponse,environment);

                        }
                    }
                } else {
                    responseMessage = "DECLINED";
                   // MailSendUtil.sendPaymentGatwayMailToAdmin(response.get("resptext").toString(), "preAuth" , "FirstData", orderR);
					
                    MailSendUtil.sendPaymentGatwayMailToAdmin(paymentResponseMessage, "preAuth" , "FirstData" , orderR,paymentResponseMessage,environment);
                    LOGGER.info("OrderController :: chargeCreditCard : FirstData is configured, Failed "+orderR.getId() +"::"+paymentResponseMessage);
					
                }

            } catch (Exception exception) {
                LOGGER.info("OrderController :: chargeCreditCard :Exception "+orderR.getId() +"::"+exception);
                responseMessage = "DECLINED";
                exception.printStackTrace();
                MailSendUtil.sendExceptionByMail(exception,environment);
                MailSendUtil.sendPaymentExceptionByMail(exception, "preAuth" , "FirstData", orderR , exception.getMessage(),environment);
            }
			LOGGER.info("OrderController :: chargeCreditCard : FirstData end with responseMsg "+orderR.getId() +"::"+responseMessage);
            return responseMessage;
        }
        
        if (gateway != null && gateway.getPayeezyMerchnatToken() != null  && gateway.getGateWayType().equals("6")
                        && gateway.getIsActive() == true && gateway.getIsDeleted() == false  && gateway.getPayeezyMerchnatTokenJs() == null) {
            try
            {
				 if(guestCustomer != null && guestCustomer.getFirstName() != null){
	                	LOGGER.info("OrderController :: chargeCreditCard : Payeezy is configured orderId "+ orderR.getId()+" and Merchnat Name "+merchant.getName()+"  and customer Name "+guestCustomer.getFirstName()+" , now processing credit card ");
	                }else if(customer != null && customer.getFirstName() != null){
	                	LOGGER.info("OrderController :: chargeCreditCard : Payeezy is configured orderId "+ orderR.getId()+" and Merchnat Name "+merchant.getName()+"  and customer Name "+customer.getFirstName()+" , now processing credit card ");
	                }else{
	                	LOGGER.info("OrderController :: chargeCreditCard : Payeezy is configured orderId "+ orderR.getId()+" and Merchnat Name "+merchant.getName() + " now processing credit card ");
	                }
                
                
                String response = null;
                String merchantToken = EncryptionDecryptionUtil.decryption(gateway.getPayeezyMerchnatToken());
                PayeezyClientHelper client = PayeezyUtilProperties.getPayeezyProperties(merchantToken,environment);
                TransactionRequest request = new TransactionRequest();
                request.setAmount(String.valueOf(totalInInt));
                request.setCurrency("USD");
                request.setPaymentMethod("credit_card");
                request.setTransactionType("AUTHORIZE");
                if(merchant.getName()!=null)
                {
                    request.setReferenceNo("Online order for -  "+ProducerUtil.removeSpecialCharecter(merchant.getName()));
                }
                com.firstdata.payeezy.models.transaction.Card card = new com.firstdata.payeezy.models.transaction.Card();
                card.setCvv(paymentVo.getCcCode());
                card.setExpiryDt(expMonthDate);
                card.setName(customer.getFirstName());
               
                LOGGER.info("OrderController :: chargeCreditCard : Payeezy customer Name "+orderR.getId() +"::"+customer.getFirstName());
                LOGGER.info("OrderController :: chargeCreditCard : Payeezy : paymentT;ype "+orderR.getId() +"::"+paymentVo.getCcType());
                if(paymentVo.getCcType().toLowerCase().equalsIgnoreCase("master card"))
                {
                    card.setType("mastercard");
                }
                else
                {
                	card.setType(paymentVo.getCcType().toLowerCase());
                }
                card.setNumber(paymentVo.getCcNumber());
                request.setCard(card);
                com.firstdata.payeezy.models.transaction.Address address = new com.firstdata.payeezy.models.transaction.Address();
                request.setBilling(address);
                
                if(customer.getState()!=null){
                	address.setState(customer.getState());
                }
               
                if(customer.getAddress1()!=null){
                	address.setAddressLine1(customer.getAddress1());
                }
                
                if(paymentVo.getZipCode()!=null){
                	address.setZip(paymentVo.getZipCode());
                }
                address.setCountry("US");
                
                 JSONHelper jsonHelper = new JSONHelper();
                 String payload = jsonHelper.getJSONObject(request);
               
                 System.out.println("Payeezy payload for authorize transaction : "+payload);
                 LOGGER.info("OrderController :: chargeCreditCard : Payeezy payload for authorize(freeze) transaction :  "+orderR.getId() +"::"+payload);
                 PayeezyResponse payeezyResponse = client.doPrimaryTransaction(request);
                 
                LOGGER.info("OrderController :: chargeCreditCard : Payeezy : payeezyResponse "+orderR.getId() +"::"+gson.toJson(payeezyResponse));
                
                if (payeezyResponse != null) {
                JSONObject jObject = new JSONObject(payeezyResponse.getResponseBody());
                StringBuilder sb = new StringBuilder();

               if(jObject != null && jObject.has("Error") && !jObject.isNull("Error")){
                JSONObject ja_data = jObject.getJSONObject("Error");
                JSONArray arr = ja_data.getJSONArray("messages");
                System.out.println(arr.length());
                for (int i = 0; i <arr.length(); i++) {
                    System.out.println(arr.getJSONObject(i).getString("code"));
                    sb.append(arr.getJSONObject(i).getString("code"));
                    sb.append(" "+arr.getJSONObject(i).getString("description"));
                    sb.append("\n");
                }
               }
                if (jObject != null && jObject.has("transaction_status") && !jObject.isNull("transaction_status")) {
                    response = jObject.getString("transaction_status");
                    if (response != null && response.equals("approved")) {
                        LOGGER.info("OrderController :: chargeCreditCard : Payeezy : amountFreezed "+orderR.getId() +"::"+payeezyResponse.getResponseBody());
                        
                        String transctiontag = jObject.getString("transaction_tag");
                        String transaction_id = jObject.getString("transaction_id");
                        LOGGER.info("OrderController :: chargeCreditCard : Payeezy : transctiontag "+orderR.getId() +"::"+transctiontag);
                        LOGGER.info("OrderController :: chargeCreditCard : Payeezy : transaction_id "+orderR.getId() +"::"+transaction_id);
                            CardInfo cardInfo = new CardInfo();
                            cardInfo.setCardType(paymentVo.getCcType());
                            cardInfo.setCreateDate(new java.util.Date());
                            cardInfo.setCustomer(customer);
                            cardInfo.setExpirationDate(EncryptionDecryptionUtil.encryption(expMonthDate));
                            if (paymentVo.getCcType().equals("VISA") || paymentVo.getCcType().equals("Master Card")) {
                                cardInfo.setLast4(EncryptionDecryptionUtil   .encryption(paymentVo.getCcNumber().substring(12)));
                            } else {
                                cardInfo.setLast4(EncryptionDecryptionUtil    .encryption(paymentVo.getCcNumber().substring(11)));
                            }
                            cardInfo.setFirst6(EncryptionDecryptionUtil .encryption(paymentVo.getCcNumber().substring(0, 6)));
                            cardInfo.setToken(EncryptionDecryptionUtil  .encryption(transaction_id));
                            cardInfo.setUpdateDate(new java.util.Date());
                            CardInfo info = merchantService.saveCustomerCards(cardInfo);
                            OrderPaymentDetail orderPaymentDetail = new OrderPaymentDetail();
                            orderPaymentDetail.setCustomerId(customer.getId());
                            orderPaymentDetail.setMerchantId(merchant.getId());
                            orderPaymentDetail.setTransactionId(EncryptionDecryptionUtil  .encryption(transaction_id));
                            orderPaymentDetail.setPaymentGatewayId(6);
                            orderPaymentDetail.setPreAuthToken(EncryptionDecryptionUtil   .encryption(transctiontag));
                            orderPaymentDetail.setOrderId(orderR.getId());
                            orderPaymentDetail.setCardInfoId(info);
                            merchantService.saveOrderPaymentDetail(orderPaymentDetail);
                            responseMessage = "SUCCESS";
                        }
                    
                    else{
                        System.out.println("responsefinal"+sb.toString());
                        LOGGER.info("OrderController :: chargeCreditCard : Payeezy : responsefinal "+orderR.getId() +"::"+sb.toString());
                        LOGGER.error("OrderController :: chargeCreditCard : payezzy not freezd  Failed "+orderR.getId() +"::"+payeezyResponse.getResponseBody());
                         MailSendUtil.sendPaymentGatwayMailToAdmin(payeezyResponse.getResponseBody(), "preAuth" , "payeezy" , orderR,sb.toString(),environment);
                        LOGGER.info("OrderController :: chargeCreditCard : payezzy : sendPaymentGatwayMailToAdmin mail send for orderId"+orderR.getId());
                    }
                    }else if (jObject.has("Error")) {
                        System.out.println("responsefinal"+sb.toString());
                        LOGGER.error("OrderController :: chargeCreditCard : payezzy not freezd  Failed "+orderR.getId() +"::"+payeezyResponse.getResponseBody());
                         MailSendUtil.sendPaymentGatwayMailToAdmin(payeezyResponse.getResponseBody(), "preAuth" , "payeezy" , orderR,sb.toString(),environment);
                        LOGGER.info("OrderController :: chargeCreditCard : payezzy : sendPaymentGatwayMailToAdmin mail send for orderId"+orderR.getId());
                       }
                    else{
                        response = payeezyResponse.getResponseBody();
                        LOGGER.error("OrderController :: chargeCreditCard : payezzy not freezd  Failed "+orderR.getId() +"::"+payeezyResponse.getResponseBody());
                        MailSendUtil.sendPaymentGatwayMailToAdmin(payeezyResponse.getResponseBody(), "preAuth" , "payeezy" , orderR,sb.toString(),environment);
                    	LOGGER.info(
								"OrderController :: chargeCreditCard : payezzy sendPaymentGatwayMailToAdmin mail send for orderId "
										+ orderR.getId());
						responseMessage = "DECLINED";
					}
				} else {

					LOGGER.error("OrderController :: chargeCreditCard : payezzy not freezd  Failed "+orderR.getId() +"::"
							+ paymentResponseMessage);
					MailSendUtil.sendPaymentGatwayMailToAdmin(paymentResponseMessage, "preAuth", "payeezy", orderR,
							paymentResponseMessage,environment);
                        LOGGER.info("OrderController :: chargeCreditCard : payezzy sendPaymentGatwayMailToAdmin mail send for orderId "+orderR.getId());
                        responseMessage = "DECLINED";
                    }
            } catch (Exception exception) {
                LOGGER.error("OrderController :: chargeCreditCard : payeezy : Exception for orderId "+orderR.getId());
                responseMessage = "DECLINED";
                MailSendUtil.sendExceptionByMail(exception,environment);
                MailSendUtil.sendPaymentExceptionByMail(exception, "preAuth" , "payeezy", orderR,exception.getMessage(),environment);
            }
            LOGGER.info("OrderController :: chargeCreditCard : payeezy : end woith reponse "+responseMessage);
            LOGGER.info("----------------End :: OrderController : chargeCreditCard------------------------");
            return responseMessage;
        }
        
        //payeezy 3ds method----------------
        if (gateway != null && gateway.getPayeezyMerchnatToken() != null  && gateway.getGateWayType().equals("6")
                && gateway.getIsActive() == true && gateway.getIsDeleted() == false && gateway.getPayeezyMerchnatTokenJs() != null) {
        	try
        	{
        
        		LOGGER.info("====== OrderController : Inside chargeCreditCard :: Payeezy 3DS is configured, now processing credit card ====== ");
        		String response = null;
        		String merchantToken = EncryptionDecryptionUtil.decryption(gateway.getPayeezyMerchnatToken());
        	gateway.setPayeezyMerchnatToken(merchantToken);
        	PayeezyClientHelperf client = PayeezyUtilProperties.getPayeezyProperty(gateway, environment);
        	TransactionRequest transactionRequest = new TransactionRequest();
        	// transactionRequest.setReferenceNo("ORD-1234777");
        	transactionRequest.setAmount(String.valueOf(totalInInt));
        	transactionRequest.setPaymentMethod("3DS");
        	transactionRequest.setCurrency("USD");
        	transactionRequest.setTransactionType("AUTHORIZE");
        	if(merchant.getName()!=null) {
        		transactionRequest.setReferenceNo(ProducerUtil.removeSpecialCharecter(merchant.getName()));
            	}
        	com.firstdata.payeezy.models.transaction.Token tokenobj = new  com.firstdata.payeezy.models.transaction.Token();
        	tokenobj.setTokenType("FDToken");
        	Transarmor tokenData = new Transarmor();
        	if(paymentVo.getCcType().toLowerCase().equalsIgnoreCase("master card")){
        		tokenData.setType("mastercard");
        	}else{
        		tokenData.setType(paymentVo.getCcType().toLowerCase());
        	}
        	tokenData.setValue(paymentVo.getPayeezyToken());
        	tokenData.setExpiryDt(expMonthDate);
        	tokenData.setName(customer.getFirstName());
        	tokenobj.setTokenData(tokenData);
        	transactionRequest.setToken(tokenobj);
        	ThreeDomainSecureToken threeDomainSecureToken = new ThreeDomainSecureToken();
        	threeDomainSecureToken.setCavv(paymentVo.getCavvPayeezyToken());
        	threeDomainSecureToken.setType("D");
        	transactionRequest.setThreeDomainSecureToken(threeDomainSecureToken);
        	
        	PayeezyResponse payeezyResponse = client.do3DSecureTransactionFinal(transactionRequest);
        	
        	if (payeezyResponse != null) {
						LOGGER.info("====== OrderController : Inside chargeCreditCard :: payeezyResponse ====== "
								+ gson.toJson(payeezyResponse));
            JSONObject jObject = new JSONObject(payeezyResponse.getResponseBody());
        	if (jObject != null && jObject.has("transaction_status") && !jObject.isNull("transaction_status")) {
        		response = jObject.getString("transaction_status");
            if (response != null && response.equals("approved")) {
                System.out.println("payeeezy amount is freeze----------------------------------"+payeezyResponse.getResponseBody());
                String transctiontag = jObject.getString("transaction_tag");
                String transaction_id = jObject.getString("transaction_id");
                LOGGER.info("====== OrderController : Inside chargeCreditCard :: transctiontag ====== "+transctiontag);
                LOGGER.info("====== OrderController : Inside chargeCreditCard :: transaction_id ====== "+transaction_id);
                    CardInfo cardInfo = new CardInfo();
                    cardInfo.setCardType(paymentVo.getCcType());
                    cardInfo.setCreateDate(new java.util.Date());
                    cardInfo.setCustomer(customer);
                    cardInfo.setExpirationDate(EncryptionDecryptionUtil.encryption(expMonthDate));
                    if (paymentVo.getCcType().equals("VISA") || paymentVo.getCcType().equals("Master Card"))
                    {
                        cardInfo.setLast4(EncryptionDecryptionUtil   .encryption(paymentVo.getCcNumber().substring(12)));
                    } else {
                        cardInfo.setLast4(EncryptionDecryptionUtil    .encryption(paymentVo.getCcNumber().substring(11)));
                    }
                    cardInfo.setFirst6(EncryptionDecryptionUtil .encryption(paymentVo.getCcNumber().substring(0, 6)));
                    cardInfo.setToken(EncryptionDecryptionUtil  .encryption(transaction_id));
                    cardInfo.setUpdateDate(new java.util.Date());
                    CardInfo info = merchantService.saveCustomerCards(cardInfo);
                    OrderPaymentDetail orderPaymentDetail = new OrderPaymentDetail();
                    orderPaymentDetail.setCustomerId(customer.getId());
                    orderPaymentDetail.setMerchantId(merchant.getId());
                    orderPaymentDetail.setTransactionId(EncryptionDecryptionUtil  .encryption(transaction_id));
                    orderPaymentDetail.setPaymentGatewayId(6);
                    orderPaymentDetail.setPreAuthToken(EncryptionDecryptionUtil   .encryption(transctiontag));
                    orderPaymentDetail.setOrderId(orderR.getId());
                    orderPaymentDetail.setCardInfoId(info);
                    merchantService.saveOrderPaymentDetail(orderPaymentDetail);
                    responseMessage = "SUCCESS";
                }
            } else {
                response = payeezyResponse.getResponseBody();
                System.out.println("response payezzy not freezd---------" + response);
                MailSendUtil.sendPaymentGatwayMailToAdmin(response, "preAuth" , "payeezy",orderR,environment);
                responseMessage = "DECLINED";
            }
        	} else {
				LOGGER.info("response payezzy not freezd---------" + paymentResponseMessage);
				MailSendUtil.sendPaymentGatwayMailToAdmin(paymentResponseMessage, "preAuth", "payeezy", orderR,environment);
					responseMessage = "DECLINED";
				}
    } catch (Exception exception) {
        System.out.println("Charge Credit card-payeezy 3DS  : ERROR" + exception);
        LOGGER.info("====== OrderController : Inside chargeCreditCard :: exception ====== "+exception);
        responseMessage = "DECLINED";
        MailSendUtil.sendExceptionByMail(exception,environment);
        MailSendUtil.sendPaymentExceptionByMail(exception, "preAuth" , "payeezy",orderR,environment);
    }
    return responseMessage;
}
        else {
            responseMessage = "DECLINED";
            LOGGER.info("OrderController :: chargeCreditCard : payeezy : end woith reponse "+responseMessage);
            LOGGER.info("----------------End :: OrderController : chargeCreditCard------------------------");
            return responseMessage;
        }
    }
        return responseMessage;
    }
    
    public Token saveCardAndcreateToken(String cardNumber,Integer expMonth, Integer expYear,String cvc,String cardType, OrderR orderR){
    	LOGGER.info("----------------Start :: OrderController : saveCardAndcreateToken------------------------");
    	LOGGER.info("OrderController :: saveCardAndcreateToken : Charge Credit card-Stripe saveCardAndcreateToken() : orderId "+orderR.getId());
    	Token token = null;
        try{
        Map<String, Object> tokenParams = new HashMap<String, Object>();
        Map<String, Object> cardParams = new HashMap<String, Object>();
        cardParams.put("number", cardNumber);
        cardParams.put("exp_month", expMonth);
        cardParams.put("exp_year", expYear);
        cardParams.put("cvc", cvc);
        tokenParams.put("card", cardParams);

         token =  Token.create(tokenParams);
         LOGGER.info("OrderController :: saveCardAndcreateToken : token :  "+token);
         System.out.println(token);
       
      }
      catch(Exception exception){
    	  exception.printStackTrace();
          MailSendUtil.sendExceptionByMail(exception,environment);
          MailSendUtil.sendPaymentExceptionByMail(exception, "preAuth" , "Stripe", orderR, exception.getMessage(),environment);
          LOGGER.error("Charge Credit card-Stripe saveCardAndcreateToken()  : ERROR" + exception + ":: orderId "+orderR.getId());
      }
        LOGGER.info("----------------End :: OrderController : saveCardAndcreateToken------------------------");
        return token;
    }

	@RequestMapping(value = "/getFoodTronixTax", method = RequestMethod.GET)
	@ResponseBody
	public List<Double> getFoodTronixTax(@RequestParam(required = false) Integer merchantId,
			@RequestParam(required = false) String orderType) {
		LOGGER.info("----------------Start :: OrderController : getFoodTronixTax------------------------");
		List<Double> response = new ArrayList<Double>();
		try {
			response = orderService.getFoodTronixTax(merchantId, orderType);
			LOGGER.info("End : OrderController : getFoodTronixTax-----");
			return response;
			
		} catch (Exception exception) {
			if (exception != null) {
				MailSendUtil.sendExceptionByMail(exception,environment);
			}
			LOGGER.info(": OrderController : getFoodTronixTax : Exception : "+exception);
			exception.printStackTrace();
			return response;
		}
	}

	@RequestMapping(value = "/getOrderNotification", method = RequestMethod.GET)
	@ResponseBody
	public String getOrderNotification(@RequestParam(required = false) Integer orderId) {
		LOGGER.info("Start: OrderController : getFoodTronixTax------------------------ ");
		NotificationVO response = new NotificationVO();
		OrderR orderR = null;
		try {
			String merchantUid = "";
			String deviceId = "";
			String merchantId = "";
			String merchantPoSId = "";
			orderR = orderService.findOrderDetailById(orderId);
			if (orderR.getMerchant() != null) {
				Merchant merchant = orderR.getMerchant();
				if (merchant.getModileDeviceId() != null) {
					deviceId = merchant.getModileDeviceId();
					LOGGER.info(" OrderController : getFoodTronixTax---------:deviceId--------------- "+deviceId);
				}
				if (merchant.getMerchantUid() != null) {
					merchantUid = merchant.getMerchantUid();
					LOGGER.info(" OrderController : getFoodTronixTax---------:merchantUid--------------- "+merchantUid);
				}
				if (merchant.getId() != null) {
					merchantId = merchant.getId().toString();
					LOGGER.info(" OrderController : getFoodTronixTax---------:merchantId--------------- "+merchantId);
				}
				if (merchant.getPosMerchantId() != null) {
					merchantPoSId = merchant.getPosMerchantId();
					LOGGER.info(" OrderController : getFoodTronixTax---------:merchantPoSId--------------- "+merchantPoSId);
				}
			}

			Customer customer = orderR.getCustomer();
			customer.setAddress1(null);
			customer.setId(null);
			customer.setAddresses(null);
			customer.setMerchantt(null);
			customer.setVendor(null);
			customer.setCreatedDate(null);
			customer.setUpdatedDate(null);
			customer.setPassword(null);
			customer.setAnniversaryDate(null);
			customer.setBirthDate(null);

			orderR.setCustomer(customer);
			orderR.setMerchant(null);
			response.setDeviceId(deviceId);
			response.setLocationUid(merchantUid);
			response.setMerchantId(merchantId);
			Payload payload = new Payload();
			payload.setOrderR(orderR);
			response.setPayload(payload);
			Gson gson = new Gson();
			String extraJson = gson.toJson(response);
			/*
			 * AppNotification.sendPushNotification(deviceId, extraJson,
			 * merchantPoSId);
			 */
			String finalJson = "";
			finalJson = finalJson + "{\"data\": " + extraJson + " ,\"registration_ids\":[\"" + deviceId + "\"]}";
			LOGGER.info(" OrderController : getFoodTronixTax---------:finalJson--------------- "+finalJson);
			System.out.println(finalJson);
			LOGGER.info(" End :OrderController : getFoodTronixTax------------------------ ");
			return finalJson;
		} catch (Exception exception) {
			if (exception != null) {
				LOGGER.info("End : OrderController : getOrderNotification-----exception : "+exception);

				MailSendUtil.sendExceptionByMail(exception,environment);
			}
			exception.printStackTrace();
			return null;
		}
	}
    
    @RequestMapping(value = "/sendOrderNotification", method = RequestMethod.GET)
    @ResponseBody
    public String sendOrderNotification(@RequestParam(required = false) Integer orderId) {
    	LOGGER.info(" Start :OrderController : sendOrderNotification------------------------ ");
    	NotificationVO response= new NotificationVO();
        OrderR orderR= null;
        try {
        	String merchantUid="";
        	String deviceId="";
        	String merchantId="";
        	String merchantPoSId="";
        	orderR = orderService.findOrderDetailById(orderId);
        	if(orderR.getMerchant()!=null){
        		Merchant merchant=orderR.getMerchant();
        		if(merchant.getModileDeviceId()!=null){
        			deviceId=merchant.getModileDeviceId();
        		}
                if(merchant.getMerchantUid()!=null){
                	merchantUid=merchant.getMerchantUid();
        		}
                if(merchant.getId()!=null){
                	merchantId=merchant.getId().toString();
        		}
                if(merchant.getPosMerchantId()!=null){
                	merchantPoSId=merchant.getPosMerchantId();
        		}
        	}
        	Customer customer = orderR.getCustomer();
			customer.setAddress1(null);
			customer.setId(null);
			customer.setAddresses(null);
			customer.setMerchantt(null);
			customer.setVendor(null);
			customer.setCreatedDate(null);
			customer.setUpdatedDate(null);
			customer.setPassword(null);
			customer.setAnniversaryDate(null);
			customer.setBirthDate(null);

			orderR.setCustomer(customer);
        	orderR.setMerchant(null);
        	response.setDeviceId(deviceId);
        	response.setLocationUid(merchantUid);
        	response.setMerchantId(merchantId);
            Payload payload= new Payload();
            payload.setOrderR(orderR);
        	response.setPayload(payload);
        	Gson gson = new Gson();
			String extraJson = gson.toJson(response);
			String finalJson="";
			finalJson=finalJson+"{\"data\": "+extraJson+" ,\"registration_ids\":[\""+deviceId+"\"]}";
			LOGGER.info(" Start :OrderController :sendOrderNotification: finalJson------------------------ "+finalJson);
			System.out.println(finalJson);
			 AppNotification.sendPushNotification(deviceId, finalJson,merchantPoSId,environment);
			 LOGGER.info(" End :OrderController :sendOrderNotification ------------------------ ");
            return "Notification sent";
        } catch (Exception exception) {
            if (exception != null) {
            	LOGGER.info("  :OrderController :sendOrderNotification:Exception ------------------------ "+exception);
                MailSendUtil.sendExceptionByMail(exception,environment);
            }
            exception.printStackTrace();
            return "failed";
        }
    }
    
    @RequestMapping(value = "/getInstalledProductStatus", method = RequestMethod.GET)
    public @ResponseBody String getInstalledProductStatus(@RequestParam("locationId") String locationId,@RequestParam("productId") Integer productId){
    	LOGGER.info(" Start :OrderController :getInstalledProductStatus ------------------------ ");
    	LOGGER.info(" === locationId : "+locationId+" productId : "+productId);
    	String reviewResponse = null;
    	 String url =IConstant.CLIENT_MODULE_BASE_URL+"entity/getInstallStatusForAnyProduct?locationId="+locationId+"&productId="+productId;
    	 LOGGER.info(" Start :OrderController :getInstalledProductStatus :url ------------------------ "+url);
    	try{
    	 HttpClient client = HttpClientBuilder.create().build();
         HttpGet httpRequest = new HttpGet(url);
         HttpResponse httpResponse = client.execute(httpRequest);

         BufferedReader rd = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent()));
         StringBuffer result = new StringBuffer();
         String line = "";
         while ((line = rd.readLine()) != null) {
             result.append(line);
         }
         reviewResponse = result.toString();
         LOGGER.info(" Start :OrderController :getInstalledProductStatus :reviewResponse ------------------------ "+reviewResponse);
         System.out.println("reviewResponse-->" + reviewResponse);
    	}
    	catch(Exception e){
    		 LOGGER.info(" Start :OrderController :getInstalledProductStatus :Exception ------------------------ "+e);
    		 MailSendUtil.sendExceptionByMail(e,environment);
    		System.out.println(e);
    	}
    	 LOGGER.info(" End :OrderController :getInstalledProductStatus : ------------------------ ");
     return reviewResponse;
    }
    
    @RequestMapping(value = "/orderRec", method = RequestMethod.GET)
    public String orderRec(HttpServletRequest request, @RequestParam String orderid, @RequestParam String customerid,
                    ModelMap model) {
        try {
        	LOGGER.info(" Start :OrderController :orderRec : ------------------------ ");
        	LOGGER.info(" Start :OrderController :orderRec :orderid  "+orderid);
        	LOGGER.info(" Start :OrderController :orderRec :customerid "+customerid);
            Integer orderId = 0;
            Integer customerID = 0;
            if (orderid != null) {
            	LOGGER.info(" Start :OrderController :orderRec :orderid ------------------------ "+orderid);
                if (orderid.matches("[0-9]+")) {
                    return "redirect:https://www.foodkonnekt.com";
                } else {
                    if (EncryptionDecryptionUtil.decryption(orderid).matches("[0-9]+"))
                        orderId = Integer.parseInt(EncryptionDecryptionUtil.decryption(orderid));
                }
            }

            if (customerid != null) {
            	LOGGER.info(" Start :OrderController :orderRec :customerid ------------------------ "+customerid);
                if (customerid.matches("[0-9]+")) {
                    return "redirect:https://www.foodkonnekt.com";
                } else {
                    if (EncryptionDecryptionUtil.decryption(customerid).matches("[0-9]+"))
                        customerID = Integer.parseInt(EncryptionDecryptionUtil.decryption(customerid));
                }
            }
            LOGGER.info(" Start :OrderController :orderRec :orderid  "+orderid);
        	LOGGER.info(" Start :OrderController :orderRec :customerid "+customerid);
            OrderR order = kritiqService.getAllOrderInfo(customerID, orderId);

            if (order != null && order.getMerchant() != null) {
                List<Address> addresses = merchantService.findAddressByMerchantId(order.getMerchant().getId());
                if (addresses != null && !addresses.isEmpty()) {
                    order.getMerchant().setAddresses(addresses);
                }
                if (order.getAddressId() != null) {
                	Address address = orderService.findByAddressId(order.getAddressId());
                	if(address!=null){
                		order.setAddress(address);
                	}
                }else{
                	 addresses = null;
                    addresses = customerService.findAllCustomerAddress(order.getCustomer().getId(),
                                    order.getMerchant().getId());
                    if (addresses != null && !addresses.isEmpty()) {
                        order.getCustomer().setAddresses(addresses);
                    }
                	
                }
            
            String orderDetail = kritiqService.getOrderDetail(orderId);
            LOGGER.info(" Start :OrderController :orderRec :orderDetail ------------------------ "+orderDetail);
            model.addAttribute("order", order);
            model.addAttribute("orderdetails", orderDetail);
            model.addAttribute("orderId", orderid);
            model.addAttribute("customerId", customerid);
            if(order!=null && order.getMerchant()!=null && order.getMerchant().getId()!=null){
            	/*if(order.getMerchant().getId().equals(IConstant.TEXAN_REWARDS_MERCHANT_ID)){
            		 model.addAttribute("kritiqLink", environment.getProperty("TEXAN_BASE_URL") + "/feedbackForm?customerId=" + customerid
                             + "&orderId=" + orderid);
            		 model.addAttribute("kritiqUrl", environment.getProperty("TEXAN_BASE_URL"));
            	}else{*/
            		model.addAttribute("kritiqLink", environment.getProperty("WEB_KRITIQ_BASE_URL") + "/feedbackForm?customerId=" + customerid
                            + "&orderId=" + orderid);
            		
            		 model.addAttribute("kritiqUrl", environment.getProperty("WEB_BASE_URL"));
            	//}
            	
            }
           

           
            if (order.getMerchant() != null) {
                Merchant merchant = order.getMerchant();
                String merchantLogo = "http://www.dev.foodkonnekt.com/app/foodnew.jpg";
                if (merchant != null) {
                    if (merchant.getMerchantLogo() == null) {
                        merchantLogo = "http://www.dev.foodkonnekt.com/app/foodnew.jpg";
                    } else {
                        merchantLogo = environment.getProperty("BASE_PORT") + merchant.getMerchantLogo();
                    }
                }
                model.addAttribute("merchantLogo", merchantLogo);
            }
            }else
            	return "redirect:https://www.foodkonnekt.com";
        } catch (Exception e) {
            if (e != null) {
            	LOGGER.info(" Start :OrderController :orderRec :Exception ------------------------ "+e);
                MailSendUtil.sendExceptionByMail(e,environment);
                LOGGER.error("error: " + e.getMessage());
                return "redirect:https://www.foodkonnekt.com";
            }
        }
        LOGGER.info(" End :OrderController :orderRec :");
        return "OrderReceipt";
    }

    @RequestMapping(value = "/orderRecFax", method = RequestMethod.GET)
    public @ResponseBody String orderRecFax(HttpServletRequest request, @RequestParam String orderid,
                    @RequestParam String customerid, ModelMap model) {
        return "sucess";
    }

    @RequestMapping(value = "/sendFax", method = RequestMethod.GET)
    public @ResponseBody String sendFax(@RequestParam String orderid,
                    @RequestParam String customerid) {
    	LOGGER.info(" Start :OrderController : sendFax ------------------------ ");
        System.out.println("sendFax-----------------------------------------------------------------");
        OrderR order = null;
        String orderDetail = null;
        String merchantLogo = null;
        System.out.println(orderid);
        System.out.println(customerid);
        Integer orderId = 0;
        Integer customerID = 0;
        if (orderid != null) {
        	LOGGER.info(" Start :OrderController : orderid ------------------------ "+orderid);
            if (orderid.matches("[0-9]+")) {
                return "redirect:https://www.foodkonnekt.com";
            } else {
                if (EncryptionDecryptionUtil.decryption(orderid).matches("[0-9]+"))
                    orderId = Integer.parseInt(EncryptionDecryptionUtil.decryption(orderid));
            }
        }

        if (customerid != null) {
        	LOGGER.info(" Start :OrderController : sendFax : customerid ------------------------ "+customerid);
            if (customerid.matches("[0-9]+")) {
                return "redirect:https://www.foodkonnekt.com";
            } else {
                if (EncryptionDecryptionUtil.decryption(customerid).matches("[0-9]+"))
                    customerID = Integer.parseInt(EncryptionDecryptionUtil.decryption(customerid));
            }
        }
        System.out.println(orderId);
        System.out.println(customerID);
        order = kritiqService.getAllOrderInfo(customerID, orderId);

        if (order != null && order.getMerchant() != null) {
            List<Address> addresses = merchantService.findAddressByMerchantId(order.getMerchant().getId());
            if (addresses != null && !addresses.isEmpty()) {
                order.getMerchant().setAddresses(addresses);
            }
            if (order.getCustomer() != null) {
                addresses = null;
                addresses = customerService.findAllCustomerAddress(order.getCustomer().getId(),
                                order.getMerchant().getId());
                if (addresses != null && !addresses.isEmpty()) {
                    order.getCustomer().setAddresses(addresses);
                }
            }
        }
        com.itextpdf.layout.element.Table   table = kritiqService.getOrderDetailForFax(orderId);
        
        if(order!=null && order.getAddressId()!=null){
        	  Address address = orderService.findByAddressId(order.getAddressId());
              if(address!=null){
              	order.setAddress(address);
              }
        }
       
        Merchant merchant =null;
        
        
        if (order!=null && order.getMerchant() != null) {
            merchant    =   new Merchant();
            merchant = order.getMerchant();
            merchantLogo = "http://www.dev.foodkonnekt.com/app/foodnew.jpg";
            if (merchant != null) {
                if (merchant.getMerchantLogo() == null) {
                    merchantLogo = "http://www.dev.foodkonnekt.com/app/foodnew.jpg";
                } else {
                    merchantLogo = environment.getProperty("BASE_PORT") + merchant.getMerchantLogo();
                }
            }
        }
 /*       String html_pdf =   FaxUtility.htmltoPdf(order , merchantLogo, orderDetail);
        String response =  FaxUtility.sendPdfFileInFax(merchantFaxNumber, html_pdf, orderId.toString());
        return response;
        */
        Address address=null;
    if(merchant!=null && merchant.getId()!=null)
          address =    businessService.findLocationByMerchantId(merchant.getId());

		if (order.getUberorderId() != null) {
			ItextFaxUtil.sendUberPdfFax(order, table, address,environment);
		} else {
        ItextFaxUtil.sendPdfFax(order, table,address,environment);
		}
        LOGGER.info(" End :OrderController : sendFax : customerid ------------------------ "+customerid);
        String  response =null;
        return response;
    }
    
    
    

    @RequestMapping(value = "/receipt", method = RequestMethod.GET)
    public String receipt(HttpServletRequest request, @RequestParam String orderid, @RequestParam String customerid,
                    ModelMap model) {
		String returnPage = "receipt";
        try {
        	LOGGER.info(" Start :OrderController : receipt :  ------------------------ ");
            Integer orderId = 0;
            Integer customerID = 0;
            if (orderid != null) {
            	LOGGER.info(" Start :OrderController : receipt : orderid ------------------------ "+orderid);
                if (orderid.matches("[0-9]+")) {
                    return "redirect:https://www.foodkonnekt.com";
                } else {
                    if (EncryptionDecryptionUtil.decryption(orderid).matches("[0-9]+"))
                        orderId = Integer.parseInt(EncryptionDecryptionUtil.decryption(orderid));
                }
            }

			if (customerid != null) {
				LOGGER.info(" Start :OrderController : receipt : customerid ------------------------ "+customerid);
				if (customerid.matches("[0-9]+")) {
					return "redirect:https://www.foodkonnekt.com";
				} else {
					if (EncryptionDecryptionUtil.decryption(customerid).matches("[0-9]+"))
						customerID = Integer.parseInt(EncryptionDecryptionUtil.decryption(customerid));
				}

			}

			System.out.println(orderId);
			System.out.println(customerID);

			OrderR order = null;
			if(customerID!=null && orderId!=null)
			order=	kritiqService.getAllOrderInfo(customerID, orderId);
			
			if(order!=null && order.getMerchant()!=null){
				if (order.getUberorderId() != null) {
					if (!order.getUberorderId().isEmpty()) {
						Double uberCharges = order.getOrderPrice() - new Double(order.getSubTotal())
								- new Double(order.getTax());
						model.addAttribute("uberCharges", uberCharges);
						returnPage = "uberReceipt";
					}
				}

				List<Address> addresses = merchantService.findAddressByMerchantId(order.getMerchant().getId());
				if(addresses!=null && !addresses.isEmpty()){
					order.getMerchant().setAddresses(addresses);
				}
				/*if(order.getCustomer()!=null){
					addresses = null;
					addresses = customerService.findAllCustomerAddress(order.getCustomer().getId(), order.getMerchant().getId());
					if(addresses!=null && !addresses.isEmpty()){
						order.getCustomer().setAddresses(addresses);
					}
				}*/
				
				if (order.getAddressId() != null) {
                	Address address = orderService.findByAddressId(order.getAddressId());
                	if(address!=null){
                		order.setAddress(address);
                	}
                }else{
                	 addresses = null;
                    addresses = customerService.findAllCustomerAddress(order.getCustomer().getId(),
                                    order.getMerchant().getId());
                    if (addresses != null && !addresses.isEmpty()) {
                        order.getCustomer().setAddresses(addresses);
                    }
                	
                }
			}
			
	            String timeZoneCode = "America/Chicago";
	            if(order != null && order.getMerchant() != null && order.getMerchant().getTimeZone() != null && order.getMerchant().getTimeZone().getTimeZoneCode() != null){
	            	timeZoneCode = order.getMerchant().getTimeZone().getTimeZoneCode();
	            }
	            String startdate=DateUtil.getCurrentTimeForTimeZoneForReciept(timeZoneCode);
				if(order!=null && order.getFulfilled_on()!=null)
				 	 startdate=order.getFulfilled_on().toString();
	        	boolean status=DateUtil.checkCurrentAndFullFillTime(startdate, timeZoneCode);
			
			
			if(order!=null){
			String orderDetail = kritiqService.getOrderDetail(orderId);
			model.addAttribute("order", order);
			model.addAttribute("orderdetails", orderDetail);
			model.addAttribute("orderId", orderid);
			model.addAttribute("customerId", customerid);
			model.addAttribute("kritiqLink",
					environment.getProperty("WEB_KRITIQ_BASE_URL") + "/feedbackForm?customerId=" + customerid + "&orderId=" + orderid);
			model.addAttribute("status", status);
			
			String merchantLogo = null;
			if(order.getMerchant()!=null){
			Merchant merchant = order.getMerchant();
			 merchantLogo = "http://www.dev.foodkonnekt.com/app/foodnew.jpg";
			if (merchant != null) {

				if (merchant.getMerchantLogo() == null) {
					merchantLogo = "http://www.dev.foodkonnekt.com/app/foodnew.jpg";
				} else {
					merchantLogo = environment.getProperty("BASE_PORT") + merchant.getMerchantLogo();
				}
			}

			model.addAttribute("merchantLogo", merchantLogo);
			}
		}


		} catch (Exception e) {
			//LOGGER.info("KritiqController.orderRec() : ERROR" + e);

			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
				LOGGER.info(" Start :OrderController : receipt : Exception ------------------------ "+e);
				LOGGER.error("error: " + e.getMessage());
				return "redirect:https://www.foodkonnekt.com";
			}

		}
		//LOGGER.info("KritiqController.orderRec() : END");
        LOGGER.info(" End :OrderController : receipt :  ------------------------ ");
		return returnPage;
	}
 

 
	@RequestMapping(value = "/receiptMail", method = RequestMethod.GET)
	public @ResponseBody String receiptMailSend(HttpServletRequest request,
			@RequestParam String orderid, @RequestParam String customerid,
			@RequestParam String customerName, @RequestParam Double orderPrice,
			@RequestParam String orderType, @RequestParam String paymentType,
			@RequestParam Date orderDate, @RequestParam String orderLink) {
LOGGER.info("===============  OrderController : Inside receiptMailSend :: Start  ============= ");

		MailSendUtil.onlineOrderNotificationReceiptMail(orderid, customerid, customerName,
				orderPrice, orderType, paymentType, orderDate, orderLink,
				"praveen.raghuvanshii@gmail.com");
		LOGGER.info("===============  OrderController : Inside receiptMailSend :: End  ============= ");

		return "success";
	}
 
	@RequestMapping(value = "/orderAcceptAndDeclineByReceipt", method = RequestMethod.GET)
	public @ResponseBody String orderAcceptAndDeclineByReceipt(
			@RequestParam("orderId") String orderId,
			@RequestParam("type") String type,
			@RequestParam(required = false) String reason,
			@RequestParam(required = false) String changedOrderAvgTime,
			@RequestParam(required = false) Boolean isClover,
			HttpServletResponse response) {
		 LOGGER.info(" Start :OrderController : orderAcceptAndDeclineByReceipt :  ------------------------ ");
        String reviewResponse = null;
        String url="";
        if(isClover!=null && isClover==IConstant.TRUE_STATUS)
         url = environment.getProperty("IP_BASE_URL") + "/orderConfirmation?orderId=" + orderId + "&type=" + type
                        + "&reason=" + URLEncoder.encode(reason);
        else  url = environment.getProperty("IP_BASE_URL") + "/orderConfirmationById?orderId=" + orderId + "&type=" + type
                + "&changedOrderAvgTime=" + changedOrderAvgTime + "&reason=" + URLEncoder.encode(reason);
        LOGGER.info(" Start :OrderController : orderAcceptAndDeclineByReceipt : url ------------------------ " +url);
        try {
            HttpClient client = HttpClientBuilder.create().build();
            HttpGet httpRequest = new HttpGet(url);
            HttpResponse httpResponse = client.execute(httpRequest);

            BufferedReader rd = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent()));
            StringBuffer result = new StringBuffer();
            String line = "";
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }
            reviewResponse = result.toString();
            LOGGER.info(" Start :OrderController : orderAcceptAndDeclineByReceipt : reviewResponse ------------------------ " +reviewResponse);
        } catch (Exception e) {
        	MailSendUtil.sendExceptionByMail(e,environment);
        	LOGGER.info(" Start :OrderController : orderAcceptAndDeclineByReceipt : Exception ------------------------ " +e);
            LOGGER.error("error: " + e.getMessage());
        }
        LOGGER.info(" End :OrderController : orderAcceptAndDeclineByReceipt :  ------------------------ " );
        return reviewResponse;
    }

    @RequestMapping(value = "/placeOrderOnAWSV2", method = { RequestMethod.POST })
    @ResponseBody
    public String placeOrderOnAWSV2(@RequestBody PlaceOrderVO placeOrderVO,String fundCode, ModelMap model, HttpServletRequest request,
                    HttpServletResponse response) {
    	
    	LOGGER.info("----------------Start :: OrderController : placeOrderOnAWSV2------------------------");
    	
        String orderJson = "";
        String orderTotal = placeOrderVO.getOrderTotal();
        String instruction = placeOrderVO.getInstruction();
        Double discount = placeOrderVO.getDiscount();
        String discountType = placeOrderVO.getDiscountType();
        String itemPosIds = placeOrderVO.getItemPosIds();
        String inventoryLevel = placeOrderVO.getInventoryLevel();
        String voucherCode = placeOrderVO.getVoucherCode();
        String orderType = placeOrderVO.getOrderType();
        String convenienceFee = placeOrderVO.getConvenienceFee();
        String deliveryItemPrice = placeOrderVO.getDeliveryItemPrice();
        String avgDeliveryTime = placeOrderVO.getAvgDeliveryTime();
        String tax = placeOrderVO.getPaymentVO().getTax();
        String auxTax = placeOrderVO.getPaymentVO().getAuxTax();
        List<ItemDto> itemDiscounts = placeOrderVO.getItemsForDiscount();
        Integer addressId= placeOrderVO.getAddressId();
        Integer zoneId= placeOrderVO.getZoneId();
        Gson gson = new Gson();

        String itemsForDiscount = "";
        if (itemDiscounts != null && !itemDiscounts.isEmpty() && itemDiscounts.size() > 0) {
            itemsForDiscount = gson.toJson(itemDiscounts);
        }
        List<CouponVO> discountList = placeOrderVO.getListOfALLDiscounts();
        String listOfALLDiscounts = "";
        if (discountList != null && !discountList.isEmpty() && discountList.size() > 0) {
            listOfALLDiscounts = gson.toJson(discountList);
        }
        PaymentVO paymentVO = placeOrderVO.getPaymentVO();

        orderJson = gson.toJson(placeOrderVO.getOrderJson());
        LOGGER.info("OrderController :: placeOrderOnAWSV2 : orderJson : "+orderJson);
        String orderPosId = null;
        String responseBody = "FAILED";
        String url = "/order";
        try {
        	if(placeOrderVO.getOrderJson().size()>0 
        			&& !placeOrderVO.getOrderJson().get(0).getItemid().equalsIgnoreCase("undefined") 
        			  &&  !placeOrderVO.getOrderJson().get(0).getItemPosId().equalsIgnoreCase("undefined")) {
            if (placeOrderVO.getOrderJson() != null && !placeOrderVO.getOrderJson().isEmpty() && orderJson != null) {
            	
                HttpSession session = request.getSession();
                session.setAttribute("orderType", orderType);
                Merchant merchant = (Merchant) session.getAttribute("merchant");
                Customer customer = (Customer) session.getAttribute("customer");

                Vendor vendor = null;
                if (session != null) {
                    vendor = (Vendor) session.getAttribute("vendor");
                }

                if (vendor != null && vendor.getId() != null && merchant != null && merchant.getId() != null) {
                    url = "order?vendorId=" + vendor.getId() + "&merchantId=" + merchant.getId();
                } else if (merchant != null && merchant.getId() != null) {
                    url = "order?merchantId=" + merchant.getId();
                }
                
                
               
                System.out.println("orderUrl "+url);
                LOGGER.error(" itemsForDiscount : " + itemsForDiscount);
                LOGGER.error(" listOfALLDiscounts : " + listOfALLDiscounts);
                if (merchant != null) {
                    String orderTypePosid = "";
                    OrderType type = orderService.findByMerchantIdAndLabel(merchant.getId(), orderType);
                    if (type != null) {
                        orderTypePosid = type.getPosOrderTypeId();
                    }

                    String employeePosId = null;

                    if (merchant != null && merchant.getEmployeePosId() != null
                                    && (!merchant.getEmployeePosId().isEmpty()
                                                    || !merchant.getEmployeePosId().equals(""))) {
                        employeePosId = merchant.getEmployeePosId();
                    }

                    if (discount != null && discount > 0) {

                    } else {
                        discount = 0.0;
                    }
                    orderJson = "{\n\"order\":" + orderJson + "}";
                    LOGGER.info("OrderController :: placeOrderOnAWSV2 : orderJson "+orderJson);
                    
                    String finalJson = OrderUtil.findFinalOrderJson(orderJson, orderTotal, instruction, orderTypePosid,
                                    customer, employeePosId, discount, discountType, voucherCode, itemPosIds,
                                    inventoryLevel, itemsForDiscount, listOfALLDiscounts, paymentVO, merchant);
                    
                    LOGGER.info("OrderController :: placeOrderOnAWSV2 : finalJson "+finalJson);
                    
                    if (merchant.getOwner() != null && merchant.getOwner().getPos() != null
                                    && merchant.getOwner().getPos().getPosId() != null
                                    && merchant.getOwner().getPos().getPosId() == 1) {
                        System.out.println("If posid is 1");
                        String result = ProducerUtil.placeOrder(finalJson, merchant,environment);
                        LOGGER.info("OrderController :: placeOrderOnAWSV2 : result "+result);
                        if (result != null && result.contains("title") && result.startsWith("{")) {
                            JSONObject jObject = new JSONObject(result);
                            orderPosId = orderService.savePizzaOrder(jObject, finalJson, customer, merchant, discount,
                                            convenienceFee, deliveryItemPrice, avgDeliveryTime, orderType, auxTax, tax,addressId,zoneId,fundCode);
                        }
                    } else {
                        System.out.println("else pos part");
                        JSONObject jObject = null;
                        orderPosId = orderService.savePizzaOrder(jObject, finalJson, customer, merchant, discount,
                                        convenienceFee, deliveryItemPrice, avgDeliveryTime, orderType, auxTax, tax,addressId,zoneId,fundCode);
                    }
                    LOGGER.info("OrderController :: placeOrderOnAWSV2 : orderPosId "+orderPosId);
                	if (orderPosId != null) {
						if (paymentVO != null) {
                    	paymentVO.setOrderPosId(orderPosId);
						}
                    	responseBody = orderPaymentUsingAjax(paymentVO, model, request, response);
                    	LOGGER.info("OrderController :: placeOrderOnAWSV2 : responseBody "+responseBody);
                } else {
					responseBody = "FAILED";
				}
                } else {
                		System.out.println("merchant session has been expired before placing an order");
                		LOGGER.info("merchant session has been expired before placing an order");
                		response.sendRedirect("https://www.foodkonnekt.com");
                	}
            	}
        }
        	} catch (Exception exception) {
        		LOGGER.error(" OrderController :: placeOrderOnAWSV2 : Exception "+exception);
        		responseBody = "FAIL";
        		if (exception != null) {
        			MailSendUtil.sendExceptionByMail(exception,environment);
        		}
        		exception.printStackTrace();
        	}
        	LOGGER.info("OrderController :: placeOrderOnAWSV2 : response "+responseBody);
        	LOGGER.info("----------------End :: OrderController : placeOrderOnAWSV2------------------------");
        	return responseBody;
    	}

    @ResponseBody
    @RequestMapping(value = "placeOrderV2", method = RequestMethod.POST)
    public String placeOrder(@RequestBody PlaceOrderVO orderVO, HttpServletRequest request, HttpSession session,
                    HttpServletResponse response, ModelMap model, @RequestParam(required=true) Integer merchantId,@RequestParam String fundCode) {
    	LOGGER.info("----------------Start :: OrderController : placeOrderV2------------------------");
    	
    	
        String convenienceFee = "0";
        String subTotal = "0";
        String result = "";
        Merchant merchant = null;
        com.foodkonnekt.clover.vo.Item deliveryItem = null;
        if (session != null && orderVO != null) {
            if (session.getAttribute("merchant") != null) {
            	merchant = (Merchant) session.getAttribute("merchant");
            	LOGGER.info("OrderController : placeOrderV2 : merchantId "+merchant.getId());
				if (merchant != null) {
					if (request != null) {
						String remoteAddr = "";
						String geoLocation = "";
						remoteAddr = CommonUtil.getIPAddressFromRequest(request);											
						// In some cases we are getting multiple IP addresses referring to same request						
						String[] remoteAddresses = remoteAddr.split(",");
						if (remoteAddresses != null) {
							if(remoteAddresses.length>1) {
								CLIENTIPLOGGER.info("Note: Below "+remoteAddresses.length + " IP Addresses refers to the same request");
							}
							for (int i = 0; i < remoteAddresses.length; i++) {
								remoteAddr = remoteAddresses[i].trim();								
								geoLocation = CommonUtil.getGeoLocationByIPaddress(remoteAddr);
								CLIENTIPLOGGER.info("IP Address: " + remoteAddr + " Merchant ID " + merchant.getId()
										+ " Location " + geoLocation);
							}
						}
					}
				}              
            }

            if(!(merchant!=null && merchant.getId().equals(merchantId)))
            	return "FAILED";
            
            List<com.foodkonnekt.clover.vo.Item> item = null;
            if (session.getAttribute("orderItems") != null) {
                item = new ArrayList<com.foodkonnekt.clover.vo.Item>(
                                (List<com.foodkonnekt.clover.vo.Item>) session.getAttribute("orderItems"));
                deliveryItem = orderVO.getOrderJson().get(0);
                orderVO.setOrderJson(null);
                if (deliveryItem != null && deliveryItem.getItemPosId() != null && deliveryItem.getPrice() != null) {
                    if (merchant != null && merchant.getOwner() != null && merchant.getOwner().getPos() != null
                                    && merchant.getOwner().getPos().getPosId() == IConstant.POS_CLOVER
                                    && deliveryItem.getId() == null) {
                        deliveryItem.setId(deliveryItem.getItemPosId());
                    }
                    deliveryItem.setExtraCharge(true);
                    item.add(deliveryItem);
                }
                if (session.getAttribute("convenienceFeeItem") != null) {
                    item.add((com.foodkonnekt.clover.vo.Item) session.getAttribute("convenienceFeeItem"));
                }
                orderVO.setOrderJson(item);
            }
            if (session.getAttribute("ConvenienceFee") != null) {
                convenienceFee = ((Double) session.getAttribute("ConvenienceFee")).toString();
            }
            if (session.getAttribute("subTotal") != null) {
                subTotal = ((Double) session.getAttribute("subTotal")).toString();
            }
            LOGGER.info(" === order"+orderVO.getIsDeliveryCoupon());
            if (orderVO.getItemsForDiscount() != null && !orderVO.getItemsForDiscount().isEmpty() && merchant != null
                            && merchant.getOwner() != null && merchant.getOwner().getPos() != null
                            && merchant.getOwner().getPos().getPosId() == 1) {
                if (orderVO.getIsDeliveryCoupon()) {
                    List<ItemDto> itemDtos = orderVO.getItemsForDiscount();
                    List<com.foodkonnekt.clover.vo.Item> discountItem = new ArrayList<com.foodkonnekt.clover.vo.Item>(
                                    (List<com.foodkonnekt.clover.vo.Item>) session.getAttribute("orderItems"));
                    ItemDto deliveryItemDto = null;
                    if (!itemDtos.isEmpty() && itemDtos != null) {
                        deliveryItemDto = itemDtos.get(0);
                        itemDtos = new ArrayList<ItemDto>();
                    }

                    for (com.foodkonnekt.clover.vo.Item item2 : discountItem) {
                        ItemDto dto = new ItemDto();
                        dto.setDiscount(0.0);
                        dto.setDiscountName("");
                        dto.setItemModifierPrice(0.0);
                        dto.setItemPosId(item2.getItemPosId());
                        dto.setOriginalPrice(Double.parseDouble(item2.getPrice()));
                        dto.setPrice(Double.parseDouble(item2.getPrice()) * Integer.parseInt(item2.getAmount()));
                        dto.setQunatity(Integer.parseInt(item2.getAmount()));
                        dto.setTotalQunatity(Integer.parseInt(item2.getAmount()));
                        itemDtos.add(dto);
                    }

                    if (deliveryItemDto != null) {
                        itemDtos.add(deliveryItemDto);
                    }

                    if (session.getAttribute("convenienceFeeItem") != null) {
                        com.foodkonnekt.clover.vo.Item convenienceFeeItem = (com.foodkonnekt.clover.vo.Item) session
                                        .getAttribute("convenienceFeeItem");
                        ItemDto dto = new ItemDto();
                        dto.setDiscount(0.0);
                        dto.setDiscountName("");
                        dto.setItemModifierPrice(0.0);
                        dto.setItemPosId(convenienceFeeItem.getItemPosId());
                        dto.setOriginalPrice(Double.parseDouble(convenienceFeeItem.getPrice()));
                        dto.setPrice(Double.parseDouble(convenienceFeeItem.getPrice()));
                        dto.setQunatity(Integer.parseInt(convenienceFeeItem.getAmount()));
                        dto.setTotalQunatity(Integer.parseInt(convenienceFeeItem.getAmount()));
                        itemDtos.add(dto);
                    }
                    orderVO.setItemsForDiscount(itemDtos);
                } else {
                    List<ItemDto> itemDtos = orderVO.getItemsForDiscount();
                    ItemDto dto = new ItemDto();
                    dto.setDiscount(0.0);
                    dto.setDiscountName("");
                    dto.setItemModifierPrice(0.0);
                    dto.setItemPosId(deliveryItem.getItemPosId());
                    dto.setOriginalPrice(Double.parseDouble(deliveryItem.getPrice()));
                    dto.setPrice(Double.parseDouble(deliveryItem.getPrice()));
                    dto.setQunatity(Integer.parseInt(deliveryItem.getAmount()));
                    dto.setTotalQunatity(Integer.parseInt(deliveryItem.getAmount()));
                    itemDtos.add(itemDtos.size() - 1, dto);
                    orderVO.setItemsForDiscount(itemDtos);
                }
            }

            if (orderVO.getListOfALLDiscounts() != null && !orderVO.getListOfALLDiscounts().isEmpty()
                            && orderVO.getPaymentVO() != null) {
                orderVO.getPaymentVO().setListOfALLDiscounts(new Gson().toJson(orderVO.getListOfALLDiscounts()));
            }

            orderVO.setConvenienceFee(convenienceFee);
            PaymentVO paymentVO = orderVO.getPaymentVO();
            if (paymentVO != null) {
                paymentVO.setTotal(orderVO.getOrderTotal());
                paymentVO.setSubTotal(subTotal);
            }
            result = placeOrderOnAWSV2(orderVO,fundCode, model, request, response);
            
            LOGGER.info("OrderController : placeOrderV2 : result "+result);
            
            if (result != "DECLINED") {
                session.removeAttribute("orderItems");
                session.removeAttribute("Tax");
                session.removeAttribute("totalPrice");
                session.removeAttribute("subTotal");
                session.removeAttribute("ConvenienceFee");
            }
        }
        
    	LOGGER.info("---------------End :: OrderController : placeOrderV2----------------------");
        return result;
    }

    @RequestMapping(value = "/chargeRefundByOrderId", method = RequestMethod.GET)
    public @ResponseBody String chargeRefundByOrderId(@RequestParam("orderId") Integer orderId) {
    	LOGGER.info("---------------Start :: OrderController : chargeRefundByOrderId ---------------------- ");
        String reviewResponse = null;
        try {
            reviewResponse = orderService.chargeRefundByOrderId(orderId);
        } catch (Exception e) {
            LOGGER.error("error: " + e.getMessage());
        }
       
        LOGGER.info("---------------End :: OrderController : chargeRefundByOrderId ---------------------- ");
        return reviewResponse;
    }

    @RequestMapping(value = "/getItemCount", method = RequestMethod.GET)
    public @ResponseBody Integer getItemCount(@RequestParam("categoryId") Integer categoryId,
                    HttpServletRequest request) {
    	LOGGER.info("---------------Start :: OrderController : getItemCount---------------");
        Integer itemCount = null;
        try {

            HttpSession session = request.getSession();
            if (session != null) {
                Merchant merchant = (Merchant) session.getAttribute("merchant");
                if (merchant != null) {
                    itemCount = orderService.findItemCountByCategoryId(categoryId, merchant);
                }
            }

        } catch (Exception exception) {
            if (exception != null) {
            	LOGGER.error("OrderController : Inside getItemCount :: Exception "+exception);
                MailSendUtil.sendExceptionByMail(exception,environment);
            }
            exception.printStackTrace();
        }
    	LOGGER.info("---------------End :: OrderController : getItemCount---------------");
        return itemCount;
    }
    @RequestMapping(value="/getOnlineOrders",method=RequestMethod.GET)
    public @ResponseBody HashMap<String, Object> getOnlineOrders(@RequestParam("locationUid") String locationUid, @RequestParam("fromDate") Date fromDate,
            @RequestParam("toDate") Date toDate) throws ParseException {
    	LOGGER.info("---------------Start :: OrderController : getOnlineOrders---------------");
    	LOGGER.info(" === locationUid : "+locationUid+" fromDate : "+fromDate+" toDate : "+toDate);
        List<OrderR> orderRs = null;
         Date fromDateS =fromDate;
        Integer count = 0;
        List<Integer> finalData = new ArrayList<Integer>();
        HashMap<String, Object> map = new HashMap<String, Object>();
        
        if (locationUid != null) {
            Merchant merchant = merchantService.findByMerchantUid(locationUid);
            if (merchant != null && merchant.getId() != null) {
            	LOGGER.info("---------------Start :: OrderController : getOnlineOrders : merchantId--------------- "+merchant.getId());
                orderRs = orderService.findByMerchantIdAndStartDateAndEndDate(merchant.getId(), fromDate, toDate);

                
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
                    List<String> dbDates = new ArrayList<String>();
                    for (OrderR orderR : orderRs) {
                        java.util.Date date = orderR.getCreatedOn();
                        String formatDate= sdf.format(date);
                        dbDates.add(formatDate);
                    }
                    List<String> finalDates = new ArrayList<String>();
                    List<String> sortedDates = new ArrayList<String>();
                
                    for (String checkDate : dbDates) {
                        if (!sortedDates.contains(checkDate)) {
                            int dateCount = 0;
                            for (String checkDate2 : dbDates) {
                                if (checkDate2.compareTo(checkDate) == 0) {
                                    dateCount++;
                                }
                            }
                            sortedDates.add(checkDate);
                            finalData.add(dateCount);
                        }
                    }
                    System.out.println(finalDates.size());
                    System.out.println(finalDates);
                    LOGGER.info("---------------Start :: OrderController : getOnlineOrders : finalDates.size()--------------- "+finalDates.size());
                    LOGGER.info("---------------Start :: OrderController : getOnlineOrders : finalDates--------------- "+finalDates);
                    Date loopDate = fromDateS;
                    
                    for (String strDate : sortedDates) {
                        
                        if (loopDate.compareTo(sdf.parse(strDate)) == 0) {
                            loopDate = org.apache.commons.lang.time.DateUtils.addDays(loopDate, 1);
                            count++;
                        } else {
                            while (loopDate.compareTo(sdf.parse(strDate)) != 0) {
                                finalData.add(count, 0);
                                loopDate = org.apache.commons.lang.time.DateUtils.addDays(loopDate, 1);
                                count++;
                            }
                            loopDate = org.apache.commons.lang.time.DateUtils.addDays(loopDate, 1);
                            count++;
                        }
                    }
                    
                    map.put("onlineOrdersList",  finalData);
                    map.put("onlineOrders", orderRs.size());
                    map.put("status", 0);
            }else{
                    map.put("status", 1);
                    map.put("onlineOrdersList",  finalData);
                }
            }
    	LOGGER.info("---------------End :: OrderController : getOnlineOrders---------------");

        return map;
    }

    @RequestMapping(value = "/getTop3ItemReport", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getTop3Itemeport(
            @RequestParam(required = false) String locationUId,
            @RequestParam(required = false) String fromDate,
            @RequestParam(required = false) String toDate) {
    	LOGGER.info("===============  OrderController : Inside getTop3Itemeport :: Start  ============= ");
    	LOGGER.info(" === locationUid : "+locationUId+" fromDate : "+fromDate+" toDate : "+toDate);

        Map<String, Object> popularItemName = new HashMap<String, Object>();
        try {
            popularItemName = orderService.getTop3ItemReport(locationUId,
                    fromDate, toDate);
        } catch (Exception e) {
            LOGGER.error("error: " + e.getMessage());
        }
    	LOGGER.info("===============  OrderController : Inside getTop3Itemeport :: Start  ============= ");

        return popularItemName;
    }

    @RequestMapping(value = "/getWeeklyReport", method = RequestMethod.GET)
	@ResponseBody
	public String getWeeklyReport(HttpServletRequest request,ModelMap model) {
    	LOGGER.info(" Start-----------:OrderController : getWeeklyReport : --------------- ");
		String response = null;
		try {
			
				DateFormat dateFormat = new SimpleDateFormat(IConstant.YYYYMMDD);
				Date today = new Date();
				Calendar cal = new GregorianCalendar();
				cal.setTime(today);
				cal.add(Calendar.DAY_OF_MONTH, -7);
				Date today7 = cal.getTime();
				String endDate = dateFormat.format(today);
				String startDate = dateFormat.format(today7);
				List<Merchant> merchantData = merchantService.getWeeklyReportOfMerchant(startDate, endDate);
				System.out.println(merchantData.size());
			    MailSendUtil.sendWeeklyTotalOrders(merchantData,startDate,endDate,environment);
				model.addAttribute("startDate", startDate);
				model.addAttribute("endDate", endDate);
				model.addAttribute("merchantData", merchantData);
				
				Gson gson = new Gson();
				response = gson.toJson(merchantData);
				if(response!=null)
					LOGGER.info(" OrderController : getWeeklyReport : response--------------- "+response);
		} catch (Exception e) {
			LOGGER.info(" OrderController : getWeeklyReport : Exception--------------- "+e);
			MailSendUtil.sendExceptionByMail(e,environment);
			LOGGER.error("error: " + e.getMessage());
		}
		LOGGER.info("End--------OrderController : getWeeklyReport :--------------- ");
		return response;
	}
    @ResponseBody
    @RequestMapping(value = "/setOrderCommunication")
    public String setOrderCommunication(@RequestParam("orderId") Integer id, @RequestParam("merchantId") Integer merchantId,
			@RequestParam("isCommunicated") Boolean isCommunicated) {
		LOGGER.info("Starts--------OrderController : setOrderCommunication :--------------- ");

		if (id != null && merchantId != null && isCommunicated != null) {
			OrderR orderR = orderService.findByOrderIdAndMerchantId(id,merchantId);
			if (orderR != null) {
				orderR.setCommunicated(isCommunicated);
				orderRepository.save(orderR);
			}
		}
		LOGGER.info("End--------OrderController : setOrderCommunication :--------------- ");

		return "success";
	}
    
    
    @RequestMapping(value = "/thermalPrinterReceiptforOrder", method = RequestMethod.GET)
    public @ResponseBody void thermalPrinterReceiptforOrder(@RequestParam String orderid,@RequestParam Integer merchantId,@RequestParam String customerId) throws java.io.IOException
    {
    	LOGGER.info("Start--------OrderController : thermalPrinterReceiptforOrder :--------------- ");
    	System.out.println("API Call");
    	OrderR order = null;
    	Integer orderId = 0;
        Integer customerID = 0;
        
    	if (orderid != null)
        {
            if (orderid.matches("[0-9]+"))
            {
                	orderId = Integer.parseInt(orderid);
                	LOGGER.info(" OrderController : thermalPrinterReceiptforOrder : orderId --------------- "+orderId);
            }
        }
        if (customerId != null)
        {
            if (customerId.matches("[0-9]+"))
            {
                    customerID = Integer.parseInt(customerId);
                    LOGGER.info(" OrderController : thermalPrinterReceiptforOrder : customerId--------------- "+customerId);
            }
        }
        
    	order = kritiqService.getAllOrderInfo(customerID, orderId);
    	
    	 if (order != null && order.getMerchant() != null)
         {
             List<Address> addresses = merchantService.findAddressByMerchantId(order.getMerchant().getId());
             if (addresses != null && !addresses.isEmpty())
             {
                 order.getMerchant().setAddresses(addresses);
             }
             if (order.getCustomer() != null)
             {
                 addresses = null;
                 addresses = customerService.findAllCustomerAddress(order.getCustomer().getId(),order.getMerchant().getId());
                 if (addresses != null && !addresses.isEmpty())
                 {
                     order.getCustomer().setAddresses(addresses);
                 }
             }
         }
    	 if(order!=null && order.getAddressId()!=null)
         {
         	 Address address = orderService.findByAddressId(order.getAddressId());
              if(address!=null)
              {
              	order.setAddress(address);
              }
         }
    	Address address =    businessService.findLocationByMerchantId(merchantId);
		if (order != null && order.getUberorderId() != null && (!order.getUberorderId().isEmpty())) {
			
				ItextFaxUtil.getUberOrderDetails(order, merchantId, address, orderItemRepository,
						itemModifierRepository, orderPizzaRepository, orderPizzaToppingsRepository,
						orderPizzaCrustRepository, addressRepository,environment);
		} else {
    	ItextFaxUtil.getOrderDetails(order,merchantId,address,orderItemRepository,itemModifierRepository,orderPizzaRepository,orderPizzaToppingsRepository,orderPizzaCrustRepository,addressRepository,environment);
		}
    	LOGGER.info("End---------------------: OrderController : thermalPrinterReceiptforOrder--------------- ");
    }
    
    @ResponseBody
   	@RequestMapping(value = "/getStatusForDelivery")
   	public String getStatusFoeDelivery(
   			@RequestParam("selectTime") String selectTime,
   			@RequestParam("selectDate") String selectDate,
   			HttpServletRequest request) {
       	try
       	{
   		LOGGER.info("===============Start::OrderController : Inside getStatusForDelivery :============== ");
   		LOGGER.info("OrderController : Inside getStatusForDelivery :selectTime:"
   				+ selectTime + ":selectDate:" + selectDate);
   		HttpSession session = request.getSession();
   		Merchant merchant = (Merchant) session.getAttribute("merchant");
   		if (merchant != null && selectTime!=null&& selectDate!=null) {
   			LOGGER.info("OrderController : Inside getStatusForDelivery :merchantId:"
   					+ merchant.getId());
   			Integer merchantId = merchant.getId();
   			DeliveryOpeningClosingDay openingClosingDay = deliveryOpeningClosingDayRepository
   					.findByDayAndMerchantId(
   							DateUtil.findDayNameFromDate(selectDate),
   							merchantId);
   			if (openingClosingDay != null) {
   				LOGGER.info("OrderController : Inside getStatusForDelivery :dayId:"
   						+ openingClosingDay.getId());
   				List<DeliveryOpeningClosingTime> delivery = deliveryOpeningClosingTimeRepository
   						.findByDeliveryOpeningClosingDayId(openingClosingDay
   								.getId());
   				if (delivery != null) {
   					String compairTime = DateUtil
   							.convert12hoursTo24HourseFormate(selectTime);
   					for (int i = 0; i < delivery.size(); i++) {
   						String endTime = delivery.get(i).getEndTime();
   						String openingTime = delivery.get(i).getStartTime();
   						LOGGER.info("OrderController : Inside getStatusForDelivery :deliveryOpeningTime:"
   								+ openingTime
   								+ ":deliveryClosingTime:"
   								+ endTime);
   						boolean status = DateUtil
   								.checkDeliveryTimeExistBetweenDeliveryTime(
   										compairTime, openingTime, endTime);
   						LOGGER.info("OrderController : Inside getStatusForDelivery :status:"
   								+ status);
   						if (status == true) {
   							LOGGER.info(" End :: OrderController : Inside getStatusForDelivery :return Y ");
   							return "Y";
   						}
   					}
   					LOGGER.info(" End :: OrderController : Inside getStatusForDelivery :return N ");
   					return "N";
   				}
   				LOGGER.info(" End :: OrderController : Inside getStatusForDelivery :return N ");
   				return "N";
   			}
   			LOGGER.info(" End :: OrderController : Inside getStatusForDelivery :return N ");
   			return "N";
   		}
   		LOGGER.info(" End :: OrderController : Inside getStatusForDelivery :return N ");
   		return "N";
       }catch(Exception e)
       	{
       	LOGGER.info(" OrderController : Inside getStatusForDelivery :Exceptionm "+e);
       	LOGGER.error("error: " + e.getMessage());
       	MailSendUtil.sendExceptionByMail(e,environment);
       	return "N";
       	}
       }
    @RequestMapping(value = "/getOrdersByPosIdAndStatus", method = RequestMethod.GET)
	public @ResponseBody String getOrdersByPosIdAndStatus(@RequestParam("merchantPosId") String merchantPosId,
			@RequestParam("orderStatus") Integer orderStatus) {
		LOGGER.info("===============  AdminHomeController : Inside getBusinessHoursByLocationUid ::"
				+ " Start  ============= posId " + merchantPosId + " , orderStatus : " + orderStatus);
		String orderJson = null;
		try {
			if(merchantPosId!=null && orderStatus!=null)
			{
				Merchant merchant=	merchantService.findByPosMerchantId(merchantPosId);
				if(merchant!=null)
				{
					String timeZoneCode="America/Chicago";
					if(merchant.getOwner()!=null && merchant.getTimeZone()!=null && merchant.getTimeZone().getTimeZoneCode()!=null)
						timeZoneCode=merchant.getTimeZone().getTimeZoneCode();
					orderJson = orderService.getOrdersByPosIdAndStatus(merchantPosId, orderStatus,timeZoneCode);
				}
			}
		} catch (Exception e) {
			LOGGER.info("Exception in getOrdersByPosIdAndStatus : " + e);
		}

		return orderJson;
	}
    
    @RequestMapping(value = "/ajaxForIsInstallFreekwent", method = RequestMethod.GET)
	public @ResponseBody String ajaxForIsInstallFreekwent(HttpServletRequest request) {
		LOGGER.info("===============  Ordercontroller : Inside ajaxForIsInstallFreekwent :  Start ============= ");
		HttpSession session = request.getSession(false);
		Merchant merchant = (Merchant) session.getAttribute("merchant");
		LOGGER.info("Ordercontroller : Inside ajaxForIsInstallFreekwent : merchantId :"
				+ merchant.getId());
		if (merchant != null) {
			try {
						
				boolean status = ProducerUtil.isFreekwentInstallOrNot(
						merchant.getMerchantUid(), IConstant.FREEKWENT,environment);
				LOGGER.info("Ordercontroller : Inside ajaxForIsInstallFreekwent : status :"
						+ status);

				if (status == true) {
					LOGGER.info("Ordercontroller : Inside ajaxForIsInstallFreekwent : return success :");

					return "success";
				}

			} catch (Exception e) {
				LOGGER.error("error: " + e.getMessage());
				LOGGER.info("Ordercontroller : Inside ajaxForIsInstallFreekwent : UnsupportedEncodingException :"+e);

			}
		}
		LOGGER.info("===============  Ordercontroller : Inside ajaxForIsInstallFreekwent : return unsuccess  End: ============= ");
		return "unsuccess";
	}   
    
	     
    
}
