package com.foodkonnekt.contoller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.httpclient.util.URIUtil;
import org.apache.commons.lang.WordUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.foodkonnekt.model.Address;
import com.foodkonnekt.model.Merchant;
import com.foodkonnekt.model.OpeningClosingDay;
import com.foodkonnekt.model.OpeningClosingTime;
import com.foodkonnekt.service.MerchantService;
import com.foodkonnekt.util.MailSendUtil;
import com.foodkonnekt.util.OrderUtil;

@Controller
public class MerchantControllerV2 {

	@Autowired
    private Environment environment;
	
	@Autowired
	MerchantService merchantService;
	private static final Logger LOGGER= LoggerFactory.getLogger(MerchantControllerV2.class);

    @RequestMapping(value = "infoV2", method = RequestMethod.GET)
    public String merchantDetails(HttpServletRequest request, ModelMap model) {
    	LOGGER.info("===============  MerchantControllerV2 : Inside merchantDetails :: Start  ============= ");

        HttpSession session = request.getSession();
        Merchant merchant = (Merchant) session.getAttribute("merchant");
        if (merchant != null) {
            List<Address> addresses = merchantService.findAddressByMerchantId(merchant.getId());
            if (addresses != null && !addresses.isEmpty()) {
                for (Address address : addresses) {
                    address.setMerchant(null);
                    address.setCustomer(null);
                    address.setZones(null);
                }
            }

            List<OpeningClosingDay> days = merchantService.findOpeningClosingDayByMerchantId(merchant.getId());
            if (days != null && !days.isEmpty()) {
                for (OpeningClosingDay openingClosingDay : days) {
                    openingClosingDay.setMerchant(null);
                    String day = WordUtils.capitalize(openingClosingDay.getDay());
                    openingClosingDay.setDay(day);
                    List<OpeningClosingTime> closingTimes = openingClosingDay.getTimes();
                    if (closingTimes != null && !closingTimes.isEmpty()) {
                        for (OpeningClosingTime openingClosingTime : closingTimes) {
                            openingClosingTime.setEndTime(OrderUtil.convert24HoursTo12HourseFormate(openingClosingTime
                                            .getEndTime() + ":00"));
                            openingClosingTime
                                            .setStartTime(OrderUtil.convert24HoursTo12HourseFormate(openingClosingTime
                                                            .getStartTime() + ":00"));
                            openingClosingTime.setOpeningClosingDay(null);
                        }
                    }
                }
            }
            merchant.setAddresses(addresses);
            merchant.setOpeningClosingDays(days);
            model.addAttribute("merchantObj", merchant);
        }
    	LOGGER.info("===============  MerchantControllerV2 : Inside merchantDetails :: End  ============= ");

        return "infoV2";
    }

	public static void main(String[] args) {
		try {
			URL url = new URL(
					"http://maps.googleapis.com/maps/api/geocode/json?address="
							+ URIUtil
									.encodeQuery("Sayaji Hotel, Near balewadi stadium, pune")
							+ "&sensor=false");
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");

			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ conn.getResponseCode());
			}
			BufferedReader br = new BufferedReader(new InputStreamReader(
					(conn.getInputStream())));

			String output = "", full = "";
			while ((output = br.readLine()) != null) {
				System.out.println(output);
				full += output;
			}
			System.out.println(full);
			JSONObject jsonObject = new JSONObject(full);
			JSONArray json = jsonObject.getJSONArray("result");
			JSONObject jsonObject1 = (JSONObject) json.get(2);
			System.out.println(jsonObject1);

			conn.disconnect();
		} catch (MalformedURLException e) {
			//MailSendUtil.sendExceptionByMail(e,environment);
LOGGER.error("===============  MerchantController : Inside main :: Exception  ============= " + e);

			LOGGER.error("error: " + e.getMessage());
		} catch (IOException e) {
			LOGGER.error("===============  MerchantController : Inside main :: Exception  ============= " + e);

			//MailSendUtil.sendExceptionByMail(e,environment);
			LOGGER.error("error: " + e.getMessage());
		}
	}
}
