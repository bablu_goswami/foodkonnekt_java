package com.foodkonnekt.contoller;

import java.io.File;
import java.io.FileWriter;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

import com.csvreader.CsvWriter;
import com.foodkonnekt.contoller.CronScheduler;
import com.foodkonnekt.model.Item;
import com.foodkonnekt.model.Merchant;
import com.foodkonnekt.model.ModifierGroup;
import com.foodkonnekt.model.ModifierModifierGroupDto;
import com.foodkonnekt.model.Modifiers;
import com.foodkonnekt.model.OrderItem;
import com.foodkonnekt.model.OrderItemModifier;
import com.foodkonnekt.model.OrderR;
import com.foodkonnekt.util.MailSendUtil;
import com.foodkonnekt.util.UrlConstant;

public class DaySchedular implements Job {
	
	@Autowired
    private Environment environment;
	
private static final Logger LOGGER =LoggerFactory.getLogger(DaySchedular.class);
	public void execute(JobExecutionContext context) throws JobExecutionException {
		try{
		MailSendUtil.webhookMail("DaySchedular", "schedular start",environment);
LOGGER.info(" DaySchedular starts ");

		String today = LocalDate.now().getDayOfWeek().name();
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("MM-dd-yyyy");
		LocalDate localDate = LocalDate.now();

		String toDayAndDate = today.substring(0, 1).toUpperCase()
				+ today.substring(1).toLowerCase() + "_"
				+ dtf.format(localDate);
		
		if(toDayAndDate !=null){
			writeExcelFile(toDayAndDate);
		}
		}catch(Throwable t){
			LOGGER.error("===============  DaySchedular : Inside execute :: Exception  ============= " + t);

			t.printStackTrace();
			MailSendUtil.webhookMail("DaySchedular Error", "Error"+t,environment);
		}
	}
	
	public void writeExcelFile(String toDayAndDate) {
		String outputFile =null;
		try {
			LOGGER.info("===============  DaySchedular : Inside writeExcelFile :: Start  ============= ");
			LOGGER.info(" ===toDayAndDate : "+toDayAndDate);

			String emailId= null;
			String vendorEmail= null;
			
			List<Item> items = CronScheduler.memoryItemRepository.findByName(toDayAndDate);
			if(items !=null && items.size()>1){
				for (Item item : items) {
					boolean newRow=true;
					outputFile = environment.getProperty("FILEPATH") + item.getMerchant().getName()+"_report.csv";
					boolean alreadyExists = new File(outputFile).exists();
					
					CsvWriter csvOutput = new CsvWriter(
					new FileWriter(outputFile, true), ',');
					LOGGER.info(" === alreadyExists : "+alreadyExists);
			if (!alreadyExists) {
				csvOutput.write("Customer Name");
				csvOutput.write("Created On");
				csvOutput.write("Order Total");
				csvOutput.write("Order Type");
				csvOutput.write("Order Notes");
				csvOutput.write("Customer Email");
				csvOutput.write("Customer Phone");
				csvOutput.write("Payment Type");
				csvOutput.write("Day/Date");
				csvOutput.write("Item Option");
				csvOutput.write("Order Item");
				csvOutput.write("Quantity");

				csvOutput.endRecord();
			}

					Modifiers modifiers=null;
					Merchant merchant =null;
					if(item.getMerchant()!=null){
						//emailId = (item.getMerchant().getEmailId());
						merchant = item.getMerchant();
						
					/*	if(item.getMerchant().getOwner()!=null && item.getMerchant().getOwner().getEmail()!=null){
							vendorEmail = item.getMerchant().getOwner().getEmail();
						}*/
					}
					LOGGER.info(" === item.getId() : " + item.getId());

					
					List<OrderItem> orderItems = CronScheduler.memoryOrderItemRepository.findByItemId(item.getId());
					if(orderItems !=null && orderItems.size()>0){
					
						for (OrderItem orderItem : orderItems) {
							if(orderItem.getOrder()!=null && orderItem.getOrder().getId()!=null){
								LOGGER.info(" === orderItem.getId() : " + orderItem.getId());

								List<OrderItemModifier> orderItemModifiers = CronScheduler.memoryOrderItemModifierRepository
										.findByOrderItemId(orderItem.getId());
								
								if (orderItemModifiers != null && !orderItemModifiers.isEmpty()) {
									
									for (OrderItemModifier orderItemModifier : orderItemModifiers) {
										if (orderItemModifier.getModifiers() != null
												&& orderItemModifier.getModifiers().getId() != null) {
											LOGGER.info(" ===orderItem.getOrder().getId() : "+orderItem.getOrder().getId());

								OrderR orderR= CronScheduler.memoryOrderRepository.findOne(orderItem.getOrder().getId());
								if(orderR!=null){
									if (newRow && orderR.getCustomer() != null
											&& orderR.getCustomer().getFirstName() != null) {
										csvOutput.write(orderR.getCustomer().getFirstName().toString());
									} else {
										csvOutput.write(" ");
									}
									
									if (newRow && orderR.getCreatedOn() != null) {
										csvOutput.write(orderR.getCreatedOn().toString().replace(" ", "_"));
									} else {
										csvOutput.write(" ");
									}

									if (newRow && orderR.getOrderPrice() != null) {
										csvOutput.write(orderR.getOrderPrice().toString());
									} else {
										csvOutput.write(" ");
									}
									
									if (newRow && orderR.getOrderType() != null) {
										csvOutput.write(orderR.getOrderType().toString());
									} else {
										csvOutput.write(" ");
									}

									if (newRow && orderR.getOrderName() != null) {
										csvOutput.write(orderR.getOrderName().toString());
									} else {
										csvOutput.write(" ");
									}

									if (newRow && orderR.getCustomer() != null
											&& orderR.getCustomer().getEmailId() != null) {
										csvOutput.write(orderR.getCustomer().getEmailId());
									} else {
										csvOutput.write(" ");
									}

									if (newRow && orderR.getCustomer() != null
											&& orderR.getCustomer().getPhoneNumber() != null) {
										csvOutput.write(orderR.getCustomer().getPhoneNumber());
									} else {
										csvOutput.write(" ");
									}

									if (newRow && orderR.getPaymentMethod() != null) {
										csvOutput.write(orderR.getPaymentMethod());
									} else {
										csvOutput.write(" ");
									}
									
									if (item != null && item.getName() != null) {
										csvOutput.write(item.getName());
									} else {
										csvOutput.write(" ");
									}
								}
							}
										LOGGER.info(" === orderItemModifier.getModifiers().getId() : " + orderItemModifier.getModifiers().getId());

										modifiers = CronScheduler.memoryModifierRepository.findOne(orderItemModifier.getModifiers().getId());
										
										List<ModifierModifierGroupDto> modifierModifierGroupDtos=CronScheduler.memoryModifierModifierGroupRepository.
												findByModifiersId(modifiers.getId());
										
										for (ModifierModifierGroupDto modifierModifierGroupDto : modifierModifierGroupDtos) {
											if(modifierModifierGroupDto.getModifierGroup()!=null &&
													modifierModifierGroupDto.getModifierGroup().getId() !=null){
												LOGGER.info(" === modifierModifierGroupDto.getModifierGroup().getId() : " + modifierModifierGroupDto.getModifierGroup().getId());

												ModifierGroup modifierGroup=CronScheduler.memoryModifierGroupRepository.findOne
														(modifierModifierGroupDto.getModifierGroup().getId());
												
												if(modifierGroup!=null && modifierGroup.getName()!=null){
													csvOutput.write(modifierGroup.getName());
												}else{
													csvOutput.write(" ");
												}
											}
										}
										
										if(modifiers.getName()!=null){
											csvOutput.write(modifiers.getName());
										}else{
											csvOutput.write(" ");
										}
										
										if (orderItemModifier.getQuantity() != null) {
											csvOutput.write(orderItemModifier.getQuantity().toString());
										} else {
											csvOutput.write(" ");
										}
										newRow = false;
										csvOutput.write(" ");
										csvOutput.endRecord();
									}
								}
							}
							newRow = true;
							csvOutput.write(" ");
							csvOutput.endRecord();
						}
						csvOutput.close();
						MailSendUtil.sendMonthlyReportMail(outputFile, merchant,environment);
						
						File file=new File(outputFile);
						file.delete();
						LOGGER.info("==File Delete Successfully");
					
					}else{
						LOGGER.info("==No Orders found");
					}
					
					
				}
			}
		} catch (Exception e) {
			LOGGER.error("===============  DaySchedular: Inside writeExcelFile :: Exception  ============= " + e);

			LOGGER.error("error: " + e.getMessage());
		}
	}
	
	public static void main(String[] args) {
		
		String today = LocalDate.now().getDayOfWeek().name();
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("MM-dd-yyyy");
		LocalDate localDate = LocalDate.now();

		String toDayAndDate = today.substring(0, 1).toUpperCase()
				+ today.substring(1).toLowerCase() + "_"
				+ dtf.format(localDate);
		System.out.println(toDayAndDate);
	}
}
