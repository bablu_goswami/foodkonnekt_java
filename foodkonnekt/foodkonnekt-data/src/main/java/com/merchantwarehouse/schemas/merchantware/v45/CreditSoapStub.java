/**
 * CreditSoapStub.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.merchantwarehouse.schemas.merchantware.v45;

public class CreditSoapStub extends org.apache.axis.client.Stub implements com.merchantwarehouse.schemas.merchantware.v45.CreditSoap {
    private java.util.Vector cachedSerClasses = new java.util.Vector();
    private java.util.Vector cachedSerQNames = new java.util.Vector();
    private java.util.Vector cachedSerFactories = new java.util.Vector();
    private java.util.Vector cachedDeserFactories = new java.util.Vector();

    static org.apache.axis.description.OperationDesc [] _operations;

    static {
        _operations = new org.apache.axis.description.OperationDesc[13];
        _initOperationDesc1();
        _initOperationDesc2();
    }

    private static void _initOperationDesc1(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("AdjustTip");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "Credentials"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "MerchantCredentials"), com.merchantwarehouse.schemas.merchantware.v45.MerchantCredentials.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "Request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "TipRequest"), com.merchantwarehouse.schemas.merchantware.v45.TipRequest.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "TransactionResponse45"));
        oper.setReturnClass(com.merchantwarehouse.schemas.merchantware.v45.TransactionResponse45.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "AdjustTipResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[0] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("AttachSignature");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "Credentials"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "MerchantCredentials"), com.merchantwarehouse.schemas.merchantware.v45.MerchantCredentials.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "Request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "SignatureRequest"), com.merchantwarehouse.schemas.merchantware.v45.SignatureRequest.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "SignatureResponse45"));
        oper.setReturnClass(com.merchantwarehouse.schemas.merchantware.v45.SignatureResponse45.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "AttachSignatureResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[1] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("Authorize");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "Credentials"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "MerchantCredentials"), com.merchantwarehouse.schemas.merchantware.v45.MerchantCredentials.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "PaymentData"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "PaymentData"), com.merchantwarehouse.schemas.merchantware.v45.PaymentData.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "Request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "AuthorizationRequest"), com.merchantwarehouse.schemas.merchantware.v45.AuthorizationRequest.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "TransactionResponse45"));
        oper.setReturnClass(com.merchantwarehouse.schemas.merchantware.v45.TransactionResponse45.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "AuthorizeResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[2] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("BoardCard");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "Credentials"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "MerchantCredentials"), com.merchantwarehouse.schemas.merchantware.v45.MerchantCredentials.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "PaymentData"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "PaymentData"), com.merchantwarehouse.schemas.merchantware.v45.PaymentData.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "Request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "BoardingRequest"), com.merchantwarehouse.schemas.merchantware.v45.BoardingRequest.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "VaultBoardingResponse45"));
        oper.setReturnClass(com.merchantwarehouse.schemas.merchantware.v45.VaultBoardingResponse45.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "BoardCardResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[3] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("UpdateBoardedCard");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "Credentials"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "MerchantCredentials"), com.merchantwarehouse.schemas.merchantware.v45.MerchantCredentials.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "Request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "UpdateBoardedCardRequest"), com.merchantwarehouse.schemas.merchantware.v45.UpdateBoardedCardRequest.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "VaultBoardingResponse45"));
        oper.setReturnClass(com.merchantwarehouse.schemas.merchantware.v45.VaultBoardingResponse45.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "UpdateBoardedCardResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[4] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("Capture");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "Credentials"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "MerchantCredentials"), com.merchantwarehouse.schemas.merchantware.v45.MerchantCredentials.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "Request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "CaptureRequest"), com.merchantwarehouse.schemas.merchantware.v45.CaptureRequest.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "TransactionResponse45"));
        oper.setReturnClass(com.merchantwarehouse.schemas.merchantware.v45.TransactionResponse45.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "CaptureResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[5] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("FindBoardedCard");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "Credentials"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "MerchantCredentials"), com.merchantwarehouse.schemas.merchantware.v45.MerchantCredentials.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "Request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "VaultTokenRequest"), com.merchantwarehouse.schemas.merchantware.v45.VaultTokenRequest.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "VaultTokenResponse45"));
        oper.setReturnClass(com.merchantwarehouse.schemas.merchantware.v45.VaultTokenResponse45.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "FindBoardedCardResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[6] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("ForceCapture");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "Credentials"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "MerchantCredentials"), com.merchantwarehouse.schemas.merchantware.v45.MerchantCredentials.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "PaymentData"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "PaymentData"), com.merchantwarehouse.schemas.merchantware.v45.PaymentData.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "Request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "ForceCaptureRequest"), com.merchantwarehouse.schemas.merchantware.v45.ForceCaptureRequest.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "TransactionResponse45"));
        oper.setReturnClass(com.merchantwarehouse.schemas.merchantware.v45.TransactionResponse45.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "ForceCaptureResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[7] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("Refund");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "Credentials"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "MerchantCredentials"), com.merchantwarehouse.schemas.merchantware.v45.MerchantCredentials.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "PaymentData"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "PaymentData"), com.merchantwarehouse.schemas.merchantware.v45.PaymentData.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "Request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "RefundRequest"), com.merchantwarehouse.schemas.merchantware.v45.RefundRequest.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "TransactionResponse45"));
        oper.setReturnClass(com.merchantwarehouse.schemas.merchantware.v45.TransactionResponse45.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "RefundResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[8] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("Sale");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "Credentials"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "MerchantCredentials"), com.merchantwarehouse.schemas.merchantware.v45.MerchantCredentials.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "PaymentData"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "PaymentData"), com.merchantwarehouse.schemas.merchantware.v45.PaymentData.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "Request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "SaleRequest"), com.merchantwarehouse.schemas.merchantware.v45.SaleRequest.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "TransactionResponse45"));
        oper.setReturnClass(com.merchantwarehouse.schemas.merchantware.v45.TransactionResponse45.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "SaleResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[9] = oper;

    }

    private static void _initOperationDesc2(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("SettleBatch");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "Credentials"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "MerchantCredentials"), com.merchantwarehouse.schemas.merchantware.v45.MerchantCredentials.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "BatchResponse45"));
        oper.setReturnClass(com.merchantwarehouse.schemas.merchantware.v45.BatchResponse45.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "SettleBatchResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[10] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("UnboardCard");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "Credentials"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "MerchantCredentials"), com.merchantwarehouse.schemas.merchantware.v45.MerchantCredentials.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "Request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "VaultTokenRequest"), com.merchantwarehouse.schemas.merchantware.v45.VaultTokenRequest.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "VaultBoardingResponse45"));
        oper.setReturnClass(com.merchantwarehouse.schemas.merchantware.v45.VaultBoardingResponse45.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "UnboardCardResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[11] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("Void");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "Credentials"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "MerchantCredentials"), com.merchantwarehouse.schemas.merchantware.v45.MerchantCredentials.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "Request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "VoidRequest"), com.merchantwarehouse.schemas.merchantware.v45.VoidRequest.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "TransactionResponse45"));
        oper.setReturnClass(com.merchantwarehouse.schemas.merchantware.v45.TransactionResponse45.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "VoidResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[12] = oper;

    }

    public CreditSoapStub() throws org.apache.axis.AxisFault {
         this(null);
    }

    public CreditSoapStub(java.net.URL endpointURL, javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
         this(service);
         super.cachedEndpoint = endpointURL;
    }

    public CreditSoapStub(javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
        if (service == null) {
            super.service = new org.apache.axis.client.Service();
        } else {
            super.service = service;
        }
        ((org.apache.axis.client.Service)super.service).setTypeMappingVersion("1.2");
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
            qName = new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "ArrayOfLineItem");
            cachedSerQNames.add(qName);
            cls = com.merchantwarehouse.schemas.merchantware.v45.LineItem[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "LineItem");
            qName2 = new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "LineItem");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "AuthorizationRequest");
            cachedSerQNames.add(qName);
            cls = com.merchantwarehouse.schemas.merchantware.v45.AuthorizationRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "BatchResponse45");
            cachedSerQNames.add(qName);
            cls = com.merchantwarehouse.schemas.merchantware.v45.BatchResponse45.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "BoardingRequest");
            cachedSerQNames.add(qName);
            cls = com.merchantwarehouse.schemas.merchantware.v45.BoardingRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "CaptureRequest");
            cachedSerQNames.add(qName);
            cls = com.merchantwarehouse.schemas.merchantware.v45.CaptureRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "ForceCaptureRequest");
            cachedSerQNames.add(qName);
            cls = com.merchantwarehouse.schemas.merchantware.v45.ForceCaptureRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "FraudScoring");
            cachedSerQNames.add(qName);
            cls = com.merchantwarehouse.schemas.merchantware.v45.FraudScoring.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "HealthCareAmountDetails");
            cachedSerQNames.add(qName);
            cls = com.merchantwarehouse.schemas.merchantware.v45.HealthCareAmountDetails.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "Invoice");
            cachedSerQNames.add(qName);
            cls = com.merchantwarehouse.schemas.merchantware.v45.Invoice.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "LineItem");
            cachedSerQNames.add(qName);
            cls = com.merchantwarehouse.schemas.merchantware.v45.LineItem.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "MerchantCredentials");
            cachedSerQNames.add(qName);
            cls = com.merchantwarehouse.schemas.merchantware.v45.MerchantCredentials.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "PaymentData");
            cachedSerQNames.add(qName);
            cls = com.merchantwarehouse.schemas.merchantware.v45.PaymentData.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "RefundRequest");
            cachedSerQNames.add(qName);
            cls = com.merchantwarehouse.schemas.merchantware.v45.RefundRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "SaleRequest");
            cachedSerQNames.add(qName);
            cls = com.merchantwarehouse.schemas.merchantware.v45.SaleRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "SignatureRequest");
            cachedSerQNames.add(qName);
            cls = com.merchantwarehouse.schemas.merchantware.v45.SignatureRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "SignatureResponse45");
            cachedSerQNames.add(qName);
            cls = com.merchantwarehouse.schemas.merchantware.v45.SignatureResponse45.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "TipRequest");
            cachedSerQNames.add(qName);
            cls = com.merchantwarehouse.schemas.merchantware.v45.TipRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "TransactionResponse45");
            cachedSerQNames.add(qName);
            cls = com.merchantwarehouse.schemas.merchantware.v45.TransactionResponse45.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "UpdateBoardedCardRequest");
            cachedSerQNames.add(qName);
            cls = com.merchantwarehouse.schemas.merchantware.v45.UpdateBoardedCardRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "VaultBoardingResponse45");
            cachedSerQNames.add(qName);
            cls = com.merchantwarehouse.schemas.merchantware.v45.VaultBoardingResponse45.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "VaultTokenRequest");
            cachedSerQNames.add(qName);
            cls = com.merchantwarehouse.schemas.merchantware.v45.VaultTokenRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "VaultTokenResponse45");
            cachedSerQNames.add(qName);
            cls = com.merchantwarehouse.schemas.merchantware.v45.VaultTokenResponse45.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "VoidRequest");
            cachedSerQNames.add(qName);
            cls = com.merchantwarehouse.schemas.merchantware.v45.VoidRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

    }

    protected org.apache.axis.client.Call createCall() throws java.rmi.RemoteException {
        try {
            org.apache.axis.client.Call _call = super._createCall();
            if (super.maintainSessionSet) {
                _call.setMaintainSession(super.maintainSession);
            }
            if (super.cachedUsername != null) {
                _call.setUsername(super.cachedUsername);
            }
            if (super.cachedPassword != null) {
                _call.setPassword(super.cachedPassword);
            }
            if (super.cachedEndpoint != null) {
                _call.setTargetEndpointAddress(super.cachedEndpoint);
            }
            if (super.cachedTimeout != null) {
                _call.setTimeout(super.cachedTimeout);
            }
            if (super.cachedPortName != null) {
                _call.setPortName(super.cachedPortName);
            }
            java.util.Enumeration keys = super.cachedProperties.keys();
            while (keys.hasMoreElements()) {
                java.lang.String key = (java.lang.String) keys.nextElement();
                _call.setProperty(key, super.cachedProperties.get(key));
            }
            // All the type mapping information is registered
            // when the first call is made.
            // The type mapping information is actually registered in
            // the TypeMappingRegistry of the service, which
            // is the reason why registration is only needed for the first call.
            synchronized (this) {
                if (firstCall()) {
                    // must set encoding style before registering serializers
                    _call.setEncodingStyle(null);
                    for (int i = 0; i < cachedSerFactories.size(); ++i) {
                        java.lang.Class cls = (java.lang.Class) cachedSerClasses.get(i);
                        javax.xml.namespace.QName qName =
                                (javax.xml.namespace.QName) cachedSerQNames.get(i);
                        java.lang.Object x = cachedSerFactories.get(i);
                        if (x instanceof Class) {
                            java.lang.Class sf = (java.lang.Class)
                                 cachedSerFactories.get(i);
                            java.lang.Class df = (java.lang.Class)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                        else if (x instanceof javax.xml.rpc.encoding.SerializerFactory) {
                            org.apache.axis.encoding.SerializerFactory sf = (org.apache.axis.encoding.SerializerFactory)
                                 cachedSerFactories.get(i);
                            org.apache.axis.encoding.DeserializerFactory df = (org.apache.axis.encoding.DeserializerFactory)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                    }
                }
            }
            return _call;
        }
        catch (java.lang.Throwable _t) {
            throw new org.apache.axis.AxisFault("Failure trying to get the Call object", _t);
        }
    }

    public com.merchantwarehouse.schemas.merchantware.v45.TransactionResponse45 adjustTip(com.merchantwarehouse.schemas.merchantware.v45.MerchantCredentials credentials, com.merchantwarehouse.schemas.merchantware.v45.TipRequest request) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[0]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.merchantwarehouse.com/merchantware/v45/AdjustTip");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "AdjustTip"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {credentials, request});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.merchantwarehouse.schemas.merchantware.v45.TransactionResponse45) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.merchantwarehouse.schemas.merchantware.v45.TransactionResponse45) org.apache.axis.utils.JavaUtils.convert(_resp, com.merchantwarehouse.schemas.merchantware.v45.TransactionResponse45.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.merchantwarehouse.schemas.merchantware.v45.SignatureResponse45 attachSignature(com.merchantwarehouse.schemas.merchantware.v45.MerchantCredentials credentials, com.merchantwarehouse.schemas.merchantware.v45.SignatureRequest request) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[1]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.merchantwarehouse.com/merchantware/v45/AttachSignature");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "AttachSignature"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {credentials, request});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.merchantwarehouse.schemas.merchantware.v45.SignatureResponse45) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.merchantwarehouse.schemas.merchantware.v45.SignatureResponse45) org.apache.axis.utils.JavaUtils.convert(_resp, com.merchantwarehouse.schemas.merchantware.v45.SignatureResponse45.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.merchantwarehouse.schemas.merchantware.v45.TransactionResponse45 authorize(com.merchantwarehouse.schemas.merchantware.v45.MerchantCredentials credentials, com.merchantwarehouse.schemas.merchantware.v45.PaymentData paymentData, com.merchantwarehouse.schemas.merchantware.v45.AuthorizationRequest request) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[2]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.merchantwarehouse.com/merchantware/v45/Authorize");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "Authorize"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {credentials, paymentData, request});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.merchantwarehouse.schemas.merchantware.v45.TransactionResponse45) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.merchantwarehouse.schemas.merchantware.v45.TransactionResponse45) org.apache.axis.utils.JavaUtils.convert(_resp, com.merchantwarehouse.schemas.merchantware.v45.TransactionResponse45.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.merchantwarehouse.schemas.merchantware.v45.VaultBoardingResponse45 boardCard(com.merchantwarehouse.schemas.merchantware.v45.MerchantCredentials credentials, com.merchantwarehouse.schemas.merchantware.v45.PaymentData paymentData, com.merchantwarehouse.schemas.merchantware.v45.BoardingRequest request) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[3]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.merchantwarehouse.com/merchantware/v45/BoardCard");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "BoardCard"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {credentials, paymentData, request});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.merchantwarehouse.schemas.merchantware.v45.VaultBoardingResponse45) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.merchantwarehouse.schemas.merchantware.v45.VaultBoardingResponse45) org.apache.axis.utils.JavaUtils.convert(_resp, com.merchantwarehouse.schemas.merchantware.v45.VaultBoardingResponse45.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.merchantwarehouse.schemas.merchantware.v45.VaultBoardingResponse45 updateBoardedCard(com.merchantwarehouse.schemas.merchantware.v45.MerchantCredentials credentials, com.merchantwarehouse.schemas.merchantware.v45.UpdateBoardedCardRequest request) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[4]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.merchantwarehouse.com/merchantware/v45/UpdateBoardedCard");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "UpdateBoardedCard"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {credentials, request});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.merchantwarehouse.schemas.merchantware.v45.VaultBoardingResponse45) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.merchantwarehouse.schemas.merchantware.v45.VaultBoardingResponse45) org.apache.axis.utils.JavaUtils.convert(_resp, com.merchantwarehouse.schemas.merchantware.v45.VaultBoardingResponse45.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.merchantwarehouse.schemas.merchantware.v45.TransactionResponse45 capture(com.merchantwarehouse.schemas.merchantware.v45.MerchantCredentials credentials, com.merchantwarehouse.schemas.merchantware.v45.CaptureRequest request) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[5]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.merchantwarehouse.com/merchantware/v45/Capture");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "Capture"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {credentials, request});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.merchantwarehouse.schemas.merchantware.v45.TransactionResponse45) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.merchantwarehouse.schemas.merchantware.v45.TransactionResponse45) org.apache.axis.utils.JavaUtils.convert(_resp, com.merchantwarehouse.schemas.merchantware.v45.TransactionResponse45.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.merchantwarehouse.schemas.merchantware.v45.VaultTokenResponse45 findBoardedCard(com.merchantwarehouse.schemas.merchantware.v45.MerchantCredentials credentials, com.merchantwarehouse.schemas.merchantware.v45.VaultTokenRequest request) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[6]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.merchantwarehouse.com/merchantware/v45/FindBoardedCard");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "FindBoardedCard"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {credentials, request});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.merchantwarehouse.schemas.merchantware.v45.VaultTokenResponse45) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.merchantwarehouse.schemas.merchantware.v45.VaultTokenResponse45) org.apache.axis.utils.JavaUtils.convert(_resp, com.merchantwarehouse.schemas.merchantware.v45.VaultTokenResponse45.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.merchantwarehouse.schemas.merchantware.v45.TransactionResponse45 forceCapture(com.merchantwarehouse.schemas.merchantware.v45.MerchantCredentials credentials, com.merchantwarehouse.schemas.merchantware.v45.PaymentData paymentData, com.merchantwarehouse.schemas.merchantware.v45.ForceCaptureRequest request) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[7]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.merchantwarehouse.com/merchantware/v45/ForceCapture");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "ForceCapture"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {credentials, paymentData, request});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.merchantwarehouse.schemas.merchantware.v45.TransactionResponse45) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.merchantwarehouse.schemas.merchantware.v45.TransactionResponse45) org.apache.axis.utils.JavaUtils.convert(_resp, com.merchantwarehouse.schemas.merchantware.v45.TransactionResponse45.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.merchantwarehouse.schemas.merchantware.v45.TransactionResponse45 refund(com.merchantwarehouse.schemas.merchantware.v45.MerchantCredentials credentials, com.merchantwarehouse.schemas.merchantware.v45.PaymentData paymentData, com.merchantwarehouse.schemas.merchantware.v45.RefundRequest request) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[8]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.merchantwarehouse.com/merchantware/v45/Refund");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "Refund"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {credentials, paymentData, request});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.merchantwarehouse.schemas.merchantware.v45.TransactionResponse45) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.merchantwarehouse.schemas.merchantware.v45.TransactionResponse45) org.apache.axis.utils.JavaUtils.convert(_resp, com.merchantwarehouse.schemas.merchantware.v45.TransactionResponse45.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.merchantwarehouse.schemas.merchantware.v45.TransactionResponse45 sale(com.merchantwarehouse.schemas.merchantware.v45.MerchantCredentials credentials, com.merchantwarehouse.schemas.merchantware.v45.PaymentData paymentData, com.merchantwarehouse.schemas.merchantware.v45.SaleRequest request) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[9]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.merchantwarehouse.com/merchantware/v45/Sale");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "Sale"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {credentials, paymentData, request});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.merchantwarehouse.schemas.merchantware.v45.TransactionResponse45) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.merchantwarehouse.schemas.merchantware.v45.TransactionResponse45) org.apache.axis.utils.JavaUtils.convert(_resp, com.merchantwarehouse.schemas.merchantware.v45.TransactionResponse45.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.merchantwarehouse.schemas.merchantware.v45.BatchResponse45 settleBatch(com.merchantwarehouse.schemas.merchantware.v45.MerchantCredentials credentials) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[10]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.merchantwarehouse.com/merchantware/v45/SettleBatch");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "SettleBatch"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {credentials});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.merchantwarehouse.schemas.merchantware.v45.BatchResponse45) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.merchantwarehouse.schemas.merchantware.v45.BatchResponse45) org.apache.axis.utils.JavaUtils.convert(_resp, com.merchantwarehouse.schemas.merchantware.v45.BatchResponse45.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.merchantwarehouse.schemas.merchantware.v45.VaultBoardingResponse45 unboardCard(com.merchantwarehouse.schemas.merchantware.v45.MerchantCredentials credentials, com.merchantwarehouse.schemas.merchantware.v45.VaultTokenRequest request) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[11]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.merchantwarehouse.com/merchantware/v45/UnboardCard");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "UnboardCard"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {credentials, request});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.merchantwarehouse.schemas.merchantware.v45.VaultBoardingResponse45) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.merchantwarehouse.schemas.merchantware.v45.VaultBoardingResponse45) org.apache.axis.utils.JavaUtils.convert(_resp, com.merchantwarehouse.schemas.merchantware.v45.VaultBoardingResponse45.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.merchantwarehouse.schemas.merchantware.v45.TransactionResponse45 _void(com.merchantwarehouse.schemas.merchantware.v45.MerchantCredentials credentials, com.merchantwarehouse.schemas.merchantware.v45.VoidRequest request) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[12]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.merchantwarehouse.com/merchantware/v45/Void");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "Void"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {credentials, request});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.merchantwarehouse.schemas.merchantware.v45.TransactionResponse45) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.merchantwarehouse.schemas.merchantware.v45.TransactionResponse45) org.apache.axis.utils.JavaUtils.convert(_resp, com.merchantwarehouse.schemas.merchantware.v45.TransactionResponse45.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

}
