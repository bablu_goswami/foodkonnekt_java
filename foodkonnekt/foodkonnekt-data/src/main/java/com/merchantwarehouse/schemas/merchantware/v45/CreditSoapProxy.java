package com.merchantwarehouse.schemas.merchantware.v45;

public class CreditSoapProxy implements com.merchantwarehouse.schemas.merchantware.v45.CreditSoap {
  private String _endpoint = null;
  private com.merchantwarehouse.schemas.merchantware.v45.CreditSoap creditSoap = null;
  
  public CreditSoapProxy() {
    _initCreditSoapProxy();
  }
  
  public CreditSoapProxy(String endpoint) {
    _endpoint = endpoint;
    _initCreditSoapProxy();
  }
  
  private void _initCreditSoapProxy() {
    try {
      creditSoap = (new com.merchantwarehouse.schemas.merchantware.v45.CreditLocator()).getCreditSoap();
      if (creditSoap != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)creditSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)creditSoap)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (creditSoap != null)
      ((javax.xml.rpc.Stub)creditSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.merchantwarehouse.schemas.merchantware.v45.CreditSoap getCreditSoap() {
    if (creditSoap == null)
      _initCreditSoapProxy();
    return creditSoap;
  }
  
  public com.merchantwarehouse.schemas.merchantware.v45.TransactionResponse45 adjustTip(com.merchantwarehouse.schemas.merchantware.v45.MerchantCredentials credentials, com.merchantwarehouse.schemas.merchantware.v45.TipRequest request) throws java.rmi.RemoteException{
    if (creditSoap == null)
      _initCreditSoapProxy();
    return creditSoap.adjustTip(credentials, request);
  }
  
  public com.merchantwarehouse.schemas.merchantware.v45.SignatureResponse45 attachSignature(com.merchantwarehouse.schemas.merchantware.v45.MerchantCredentials credentials, com.merchantwarehouse.schemas.merchantware.v45.SignatureRequest request) throws java.rmi.RemoteException{
    if (creditSoap == null)
      _initCreditSoapProxy();
    return creditSoap.attachSignature(credentials, request);
  }
  
  public com.merchantwarehouse.schemas.merchantware.v45.TransactionResponse45 authorize(com.merchantwarehouse.schemas.merchantware.v45.MerchantCredentials credentials, com.merchantwarehouse.schemas.merchantware.v45.PaymentData paymentData, com.merchantwarehouse.schemas.merchantware.v45.AuthorizationRequest request) throws java.rmi.RemoteException{
    if (creditSoap == null)
      _initCreditSoapProxy();
    return creditSoap.authorize(credentials, paymentData, request);
  }
  
  public com.merchantwarehouse.schemas.merchantware.v45.VaultBoardingResponse45 boardCard(com.merchantwarehouse.schemas.merchantware.v45.MerchantCredentials credentials, com.merchantwarehouse.schemas.merchantware.v45.PaymentData paymentData, com.merchantwarehouse.schemas.merchantware.v45.BoardingRequest request) throws java.rmi.RemoteException{
    if (creditSoap == null)
      _initCreditSoapProxy();
    return creditSoap.boardCard(credentials, paymentData, request);
  }
  
  public com.merchantwarehouse.schemas.merchantware.v45.VaultBoardingResponse45 updateBoardedCard(com.merchantwarehouse.schemas.merchantware.v45.MerchantCredentials credentials, com.merchantwarehouse.schemas.merchantware.v45.UpdateBoardedCardRequest request) throws java.rmi.RemoteException{
    if (creditSoap == null)
      _initCreditSoapProxy();
    return creditSoap.updateBoardedCard(credentials, request);
  }
  
  public com.merchantwarehouse.schemas.merchantware.v45.TransactionResponse45 capture(com.merchantwarehouse.schemas.merchantware.v45.MerchantCredentials credentials, com.merchantwarehouse.schemas.merchantware.v45.CaptureRequest request) throws java.rmi.RemoteException{
    if (creditSoap == null)
      _initCreditSoapProxy();
    return creditSoap.capture(credentials, request);
  }
  
  public com.merchantwarehouse.schemas.merchantware.v45.VaultTokenResponse45 findBoardedCard(com.merchantwarehouse.schemas.merchantware.v45.MerchantCredentials credentials, com.merchantwarehouse.schemas.merchantware.v45.VaultTokenRequest request) throws java.rmi.RemoteException{
    if (creditSoap == null)
      _initCreditSoapProxy();
    return creditSoap.findBoardedCard(credentials, request);
  }
  
  public com.merchantwarehouse.schemas.merchantware.v45.TransactionResponse45 forceCapture(com.merchantwarehouse.schemas.merchantware.v45.MerchantCredentials credentials, com.merchantwarehouse.schemas.merchantware.v45.PaymentData paymentData, com.merchantwarehouse.schemas.merchantware.v45.ForceCaptureRequest request) throws java.rmi.RemoteException{
    if (creditSoap == null)
      _initCreditSoapProxy();
    return creditSoap.forceCapture(credentials, paymentData, request);
  }
  
  public com.merchantwarehouse.schemas.merchantware.v45.TransactionResponse45 refund(com.merchantwarehouse.schemas.merchantware.v45.MerchantCredentials credentials, com.merchantwarehouse.schemas.merchantware.v45.PaymentData paymentData, com.merchantwarehouse.schemas.merchantware.v45.RefundRequest request) throws java.rmi.RemoteException{
    if (creditSoap == null)
      _initCreditSoapProxy();
    return creditSoap.refund(credentials, paymentData, request);
  }
  
  public com.merchantwarehouse.schemas.merchantware.v45.TransactionResponse45 sale(com.merchantwarehouse.schemas.merchantware.v45.MerchantCredentials credentials, com.merchantwarehouse.schemas.merchantware.v45.PaymentData paymentData, com.merchantwarehouse.schemas.merchantware.v45.SaleRequest request) throws java.rmi.RemoteException{
    if (creditSoap == null)
      _initCreditSoapProxy();
    return creditSoap.sale(credentials, paymentData, request);
  }
  
  public com.merchantwarehouse.schemas.merchantware.v45.BatchResponse45 settleBatch(com.merchantwarehouse.schemas.merchantware.v45.MerchantCredentials credentials) throws java.rmi.RemoteException{
    if (creditSoap == null)
      _initCreditSoapProxy();
    return creditSoap.settleBatch(credentials);
  }
  
  public com.merchantwarehouse.schemas.merchantware.v45.VaultBoardingResponse45 unboardCard(com.merchantwarehouse.schemas.merchantware.v45.MerchantCredentials credentials, com.merchantwarehouse.schemas.merchantware.v45.VaultTokenRequest request) throws java.rmi.RemoteException{
    if (creditSoap == null)
      _initCreditSoapProxy();
    return creditSoap.unboardCard(credentials, request);
  }
  
  public com.merchantwarehouse.schemas.merchantware.v45.TransactionResponse45 _void(com.merchantwarehouse.schemas.merchantware.v45.MerchantCredentials credentials, com.merchantwarehouse.schemas.merchantware.v45.VoidRequest request) throws java.rmi.RemoteException{
    if (creditSoap == null)
      _initCreditSoapProxy();
    return creditSoap._void(credentials, request);
  }
  
  
}