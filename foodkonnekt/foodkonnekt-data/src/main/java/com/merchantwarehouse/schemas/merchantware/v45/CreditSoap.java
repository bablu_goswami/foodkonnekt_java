/**
 * CreditSoap.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.merchantwarehouse.schemas.merchantware.v45;

public interface CreditSoap extends java.rmi.Remote {
    public com.merchantwarehouse.schemas.merchantware.v45.TransactionResponse45 adjustTip(com.merchantwarehouse.schemas.merchantware.v45.MerchantCredentials credentials, com.merchantwarehouse.schemas.merchantware.v45.TipRequest request) throws java.rmi.RemoteException;
    public com.merchantwarehouse.schemas.merchantware.v45.SignatureResponse45 attachSignature(com.merchantwarehouse.schemas.merchantware.v45.MerchantCredentials credentials, com.merchantwarehouse.schemas.merchantware.v45.SignatureRequest request) throws java.rmi.RemoteException;
    public com.merchantwarehouse.schemas.merchantware.v45.TransactionResponse45 authorize(com.merchantwarehouse.schemas.merchantware.v45.MerchantCredentials credentials, com.merchantwarehouse.schemas.merchantware.v45.PaymentData paymentData, com.merchantwarehouse.schemas.merchantware.v45.AuthorizationRequest request) throws java.rmi.RemoteException;
    public com.merchantwarehouse.schemas.merchantware.v45.VaultBoardingResponse45 boardCard(com.merchantwarehouse.schemas.merchantware.v45.MerchantCredentials credentials, com.merchantwarehouse.schemas.merchantware.v45.PaymentData paymentData, com.merchantwarehouse.schemas.merchantware.v45.BoardingRequest request) throws java.rmi.RemoteException;
    public com.merchantwarehouse.schemas.merchantware.v45.VaultBoardingResponse45 updateBoardedCard(com.merchantwarehouse.schemas.merchantware.v45.MerchantCredentials credentials, com.merchantwarehouse.schemas.merchantware.v45.UpdateBoardedCardRequest request) throws java.rmi.RemoteException;
    public com.merchantwarehouse.schemas.merchantware.v45.TransactionResponse45 capture(com.merchantwarehouse.schemas.merchantware.v45.MerchantCredentials credentials, com.merchantwarehouse.schemas.merchantware.v45.CaptureRequest request) throws java.rmi.RemoteException;
    public com.merchantwarehouse.schemas.merchantware.v45.VaultTokenResponse45 findBoardedCard(com.merchantwarehouse.schemas.merchantware.v45.MerchantCredentials credentials, com.merchantwarehouse.schemas.merchantware.v45.VaultTokenRequest request) throws java.rmi.RemoteException;
    public com.merchantwarehouse.schemas.merchantware.v45.TransactionResponse45 forceCapture(com.merchantwarehouse.schemas.merchantware.v45.MerchantCredentials credentials, com.merchantwarehouse.schemas.merchantware.v45.PaymentData paymentData, com.merchantwarehouse.schemas.merchantware.v45.ForceCaptureRequest request) throws java.rmi.RemoteException;
    public com.merchantwarehouse.schemas.merchantware.v45.TransactionResponse45 refund(com.merchantwarehouse.schemas.merchantware.v45.MerchantCredentials credentials, com.merchantwarehouse.schemas.merchantware.v45.PaymentData paymentData, com.merchantwarehouse.schemas.merchantware.v45.RefundRequest request) throws java.rmi.RemoteException;
    public com.merchantwarehouse.schemas.merchantware.v45.TransactionResponse45 sale(com.merchantwarehouse.schemas.merchantware.v45.MerchantCredentials credentials, com.merchantwarehouse.schemas.merchantware.v45.PaymentData paymentData, com.merchantwarehouse.schemas.merchantware.v45.SaleRequest request) throws java.rmi.RemoteException;
    public com.merchantwarehouse.schemas.merchantware.v45.BatchResponse45 settleBatch(com.merchantwarehouse.schemas.merchantware.v45.MerchantCredentials credentials) throws java.rmi.RemoteException;
    public com.merchantwarehouse.schemas.merchantware.v45.VaultBoardingResponse45 unboardCard(com.merchantwarehouse.schemas.merchantware.v45.MerchantCredentials credentials, com.merchantwarehouse.schemas.merchantware.v45.VaultTokenRequest request) throws java.rmi.RemoteException;
    public com.merchantwarehouse.schemas.merchantware.v45.TransactionResponse45 _void(com.merchantwarehouse.schemas.merchantware.v45.MerchantCredentials credentials, com.merchantwarehouse.schemas.merchantware.v45.VoidRequest request) throws java.rmi.RemoteException;
}
