/**
 * AuthorizationRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.merchantwarehouse.schemas.merchantware.v45;

public class AuthorizationRequest  implements java.io.Serializable {
    private java.lang.String amount;

    private java.lang.String invoiceNumber;

    private java.lang.String registerNumber;

    private java.lang.String merchantTransactionId;

    private java.lang.String cardAcceptorTerminalId;

    private java.lang.String cardCaptureCapability;

    private java.lang.String ecommerceTransactionIndicator;

    private java.lang.String pinAuthenticationCapability;

    private java.lang.String posConditionCode;

    private java.lang.String posEntryMode;

    private java.lang.String terminalCategoryCode;

    private java.lang.String terminalEntryCapability;

    private java.lang.String terminalLocationIndicator;

    private java.lang.String enablePartialAuthorization;

    private java.lang.String taxAmount;

    private java.lang.String purchaseOrderNumber;

    private java.lang.String customerCode;

    private com.merchantwarehouse.schemas.merchantware.v45.Invoice invoice;

    public AuthorizationRequest() {
    }

    public AuthorizationRequest(
           java.lang.String amount,
           java.lang.String invoiceNumber,
           java.lang.String registerNumber,
           java.lang.String merchantTransactionId,
           java.lang.String cardAcceptorTerminalId,
           java.lang.String cardCaptureCapability,
           java.lang.String ecommerceTransactionIndicator,
           java.lang.String pinAuthenticationCapability,
           java.lang.String posConditionCode,
           java.lang.String posEntryMode,
           java.lang.String terminalCategoryCode,
           java.lang.String terminalEntryCapability,
           java.lang.String terminalLocationIndicator,
           java.lang.String enablePartialAuthorization,
           java.lang.String taxAmount,
           java.lang.String purchaseOrderNumber,
           java.lang.String customerCode,
           com.merchantwarehouse.schemas.merchantware.v45.Invoice invoice) {
           this.amount = amount;
           this.invoiceNumber = invoiceNumber;
           this.registerNumber = registerNumber;
           this.merchantTransactionId = merchantTransactionId;
           this.cardAcceptorTerminalId = cardAcceptorTerminalId;
           this.cardCaptureCapability = cardCaptureCapability;
           this.ecommerceTransactionIndicator = ecommerceTransactionIndicator;
           this.pinAuthenticationCapability = pinAuthenticationCapability;
           this.posConditionCode = posConditionCode;
           this.posEntryMode = posEntryMode;
           this.terminalCategoryCode = terminalCategoryCode;
           this.terminalEntryCapability = terminalEntryCapability;
           this.terminalLocationIndicator = terminalLocationIndicator;
           this.enablePartialAuthorization = enablePartialAuthorization;
           this.taxAmount = taxAmount;
           this.purchaseOrderNumber = purchaseOrderNumber;
           this.customerCode = customerCode;
           this.invoice = invoice;
    }


    /**
     * Gets the amount value for this AuthorizationRequest.
     * 
     * @return amount
     */
    public java.lang.String getAmount() {
        return amount;
    }


    /**
     * Sets the amount value for this AuthorizationRequest.
     * 
     * @param amount
     */
    public void setAmount(java.lang.String amount) {
        this.amount = amount;
    }


    /**
     * Gets the invoiceNumber value for this AuthorizationRequest.
     * 
     * @return invoiceNumber
     */
    public java.lang.String getInvoiceNumber() {
        return invoiceNumber;
    }


    /**
     * Sets the invoiceNumber value for this AuthorizationRequest.
     * 
     * @param invoiceNumber
     */
    public void setInvoiceNumber(java.lang.String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }


    /**
     * Gets the registerNumber value for this AuthorizationRequest.
     * 
     * @return registerNumber
     */
    public java.lang.String getRegisterNumber() {
        return registerNumber;
    }


    /**
     * Sets the registerNumber value for this AuthorizationRequest.
     * 
     * @param registerNumber
     */
    public void setRegisterNumber(java.lang.String registerNumber) {
        this.registerNumber = registerNumber;
    }


    /**
     * Gets the merchantTransactionId value for this AuthorizationRequest.
     * 
     * @return merchantTransactionId
     */
    public java.lang.String getMerchantTransactionId() {
        return merchantTransactionId;
    }


    /**
     * Sets the merchantTransactionId value for this AuthorizationRequest.
     * 
     * @param merchantTransactionId
     */
    public void setMerchantTransactionId(java.lang.String merchantTransactionId) {
        this.merchantTransactionId = merchantTransactionId;
    }


    /**
     * Gets the cardAcceptorTerminalId value for this AuthorizationRequest.
     * 
     * @return cardAcceptorTerminalId
     */
    public java.lang.String getCardAcceptorTerminalId() {
        return cardAcceptorTerminalId;
    }


    /**
     * Sets the cardAcceptorTerminalId value for this AuthorizationRequest.
     * 
     * @param cardAcceptorTerminalId
     */
    public void setCardAcceptorTerminalId(java.lang.String cardAcceptorTerminalId) {
        this.cardAcceptorTerminalId = cardAcceptorTerminalId;
    }


    /**
     * Gets the cardCaptureCapability value for this AuthorizationRequest.
     * 
     * @return cardCaptureCapability
     */
    public java.lang.String getCardCaptureCapability() {
        return cardCaptureCapability;
    }


    /**
     * Sets the cardCaptureCapability value for this AuthorizationRequest.
     * 
     * @param cardCaptureCapability
     */
    public void setCardCaptureCapability(java.lang.String cardCaptureCapability) {
        this.cardCaptureCapability = cardCaptureCapability;
    }


    /**
     * Gets the ecommerceTransactionIndicator value for this AuthorizationRequest.
     * 
     * @return ecommerceTransactionIndicator
     */
    public java.lang.String getEcommerceTransactionIndicator() {
        return ecommerceTransactionIndicator;
    }


    /**
     * Sets the ecommerceTransactionIndicator value for this AuthorizationRequest.
     * 
     * @param ecommerceTransactionIndicator
     */
    public void setEcommerceTransactionIndicator(java.lang.String ecommerceTransactionIndicator) {
        this.ecommerceTransactionIndicator = ecommerceTransactionIndicator;
    }


    /**
     * Gets the pinAuthenticationCapability value for this AuthorizationRequest.
     * 
     * @return pinAuthenticationCapability
     */
    public java.lang.String getPinAuthenticationCapability() {
        return pinAuthenticationCapability;
    }


    /**
     * Sets the pinAuthenticationCapability value for this AuthorizationRequest.
     * 
     * @param pinAuthenticationCapability
     */
    public void setPinAuthenticationCapability(java.lang.String pinAuthenticationCapability) {
        this.pinAuthenticationCapability = pinAuthenticationCapability;
    }


    /**
     * Gets the posConditionCode value for this AuthorizationRequest.
     * 
     * @return posConditionCode
     */
    public java.lang.String getPosConditionCode() {
        return posConditionCode;
    }


    /**
     * Sets the posConditionCode value for this AuthorizationRequest.
     * 
     * @param posConditionCode
     */
    public void setPosConditionCode(java.lang.String posConditionCode) {
        this.posConditionCode = posConditionCode;
    }


    /**
     * Gets the posEntryMode value for this AuthorizationRequest.
     * 
     * @return posEntryMode
     */
    public java.lang.String getPosEntryMode() {
        return posEntryMode;
    }


    /**
     * Sets the posEntryMode value for this AuthorizationRequest.
     * 
     * @param posEntryMode
     */
    public void setPosEntryMode(java.lang.String posEntryMode) {
        this.posEntryMode = posEntryMode;
    }


    /**
     * Gets the terminalCategoryCode value for this AuthorizationRequest.
     * 
     * @return terminalCategoryCode
     */
    public java.lang.String getTerminalCategoryCode() {
        return terminalCategoryCode;
    }


    /**
     * Sets the terminalCategoryCode value for this AuthorizationRequest.
     * 
     * @param terminalCategoryCode
     */
    public void setTerminalCategoryCode(java.lang.String terminalCategoryCode) {
        this.terminalCategoryCode = terminalCategoryCode;
    }


    /**
     * Gets the terminalEntryCapability value for this AuthorizationRequest.
     * 
     * @return terminalEntryCapability
     */
    public java.lang.String getTerminalEntryCapability() {
        return terminalEntryCapability;
    }


    /**
     * Sets the terminalEntryCapability value for this AuthorizationRequest.
     * 
     * @param terminalEntryCapability
     */
    public void setTerminalEntryCapability(java.lang.String terminalEntryCapability) {
        this.terminalEntryCapability = terminalEntryCapability;
    }


    /**
     * Gets the terminalLocationIndicator value for this AuthorizationRequest.
     * 
     * @return terminalLocationIndicator
     */
    public java.lang.String getTerminalLocationIndicator() {
        return terminalLocationIndicator;
    }


    /**
     * Sets the terminalLocationIndicator value for this AuthorizationRequest.
     * 
     * @param terminalLocationIndicator
     */
    public void setTerminalLocationIndicator(java.lang.String terminalLocationIndicator) {
        this.terminalLocationIndicator = terminalLocationIndicator;
    }


    /**
     * Gets the enablePartialAuthorization value for this AuthorizationRequest.
     * 
     * @return enablePartialAuthorization
     */
    public java.lang.String getEnablePartialAuthorization() {
        return enablePartialAuthorization;
    }


    /**
     * Sets the enablePartialAuthorization value for this AuthorizationRequest.
     * 
     * @param enablePartialAuthorization
     */
    public void setEnablePartialAuthorization(java.lang.String enablePartialAuthorization) {
        this.enablePartialAuthorization = enablePartialAuthorization;
    }


    /**
     * Gets the taxAmount value for this AuthorizationRequest.
     * 
     * @return taxAmount
     */
    public java.lang.String getTaxAmount() {
        return taxAmount;
    }


    /**
     * Sets the taxAmount value for this AuthorizationRequest.
     * 
     * @param taxAmount
     */
    public void setTaxAmount(java.lang.String taxAmount) {
        this.taxAmount = taxAmount;
    }


    /**
     * Gets the purchaseOrderNumber value for this AuthorizationRequest.
     * 
     * @return purchaseOrderNumber
     */
    public java.lang.String getPurchaseOrderNumber() {
        return purchaseOrderNumber;
    }


    /**
     * Sets the purchaseOrderNumber value for this AuthorizationRequest.
     * 
     * @param purchaseOrderNumber
     */
    public void setPurchaseOrderNumber(java.lang.String purchaseOrderNumber) {
        this.purchaseOrderNumber = purchaseOrderNumber;
    }


    /**
     * Gets the customerCode value for this AuthorizationRequest.
     * 
     * @return customerCode
     */
    public java.lang.String getCustomerCode() {
        return customerCode;
    }


    /**
     * Sets the customerCode value for this AuthorizationRequest.
     * 
     * @param customerCode
     */
    public void setCustomerCode(java.lang.String customerCode) {
        this.customerCode = customerCode;
    }


    /**
     * Gets the invoice value for this AuthorizationRequest.
     * 
     * @return invoice
     */
    public com.merchantwarehouse.schemas.merchantware.v45.Invoice getInvoice() {
        return invoice;
    }


    /**
     * Sets the invoice value for this AuthorizationRequest.
     * 
     * @param invoice
     */
    public void setInvoice(com.merchantwarehouse.schemas.merchantware.v45.Invoice invoice) {
        this.invoice = invoice;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AuthorizationRequest)) return false;
        AuthorizationRequest other = (AuthorizationRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.amount==null && other.getAmount()==null) || 
             (this.amount!=null &&
              this.amount.equals(other.getAmount()))) &&
            ((this.invoiceNumber==null && other.getInvoiceNumber()==null) || 
             (this.invoiceNumber!=null &&
              this.invoiceNumber.equals(other.getInvoiceNumber()))) &&
            ((this.registerNumber==null && other.getRegisterNumber()==null) || 
             (this.registerNumber!=null &&
              this.registerNumber.equals(other.getRegisterNumber()))) &&
            ((this.merchantTransactionId==null && other.getMerchantTransactionId()==null) || 
             (this.merchantTransactionId!=null &&
              this.merchantTransactionId.equals(other.getMerchantTransactionId()))) &&
            ((this.cardAcceptorTerminalId==null && other.getCardAcceptorTerminalId()==null) || 
             (this.cardAcceptorTerminalId!=null &&
              this.cardAcceptorTerminalId.equals(other.getCardAcceptorTerminalId()))) &&
            ((this.cardCaptureCapability==null && other.getCardCaptureCapability()==null) || 
             (this.cardCaptureCapability!=null &&
              this.cardCaptureCapability.equals(other.getCardCaptureCapability()))) &&
            ((this.ecommerceTransactionIndicator==null && other.getEcommerceTransactionIndicator()==null) || 
             (this.ecommerceTransactionIndicator!=null &&
              this.ecommerceTransactionIndicator.equals(other.getEcommerceTransactionIndicator()))) &&
            ((this.pinAuthenticationCapability==null && other.getPinAuthenticationCapability()==null) || 
             (this.pinAuthenticationCapability!=null &&
              this.pinAuthenticationCapability.equals(other.getPinAuthenticationCapability()))) &&
            ((this.posConditionCode==null && other.getPosConditionCode()==null) || 
             (this.posConditionCode!=null &&
              this.posConditionCode.equals(other.getPosConditionCode()))) &&
            ((this.posEntryMode==null && other.getPosEntryMode()==null) || 
             (this.posEntryMode!=null &&
              this.posEntryMode.equals(other.getPosEntryMode()))) &&
            ((this.terminalCategoryCode==null && other.getTerminalCategoryCode()==null) || 
             (this.terminalCategoryCode!=null &&
              this.terminalCategoryCode.equals(other.getTerminalCategoryCode()))) &&
            ((this.terminalEntryCapability==null && other.getTerminalEntryCapability()==null) || 
             (this.terminalEntryCapability!=null &&
              this.terminalEntryCapability.equals(other.getTerminalEntryCapability()))) &&
            ((this.terminalLocationIndicator==null && other.getTerminalLocationIndicator()==null) || 
             (this.terminalLocationIndicator!=null &&
              this.terminalLocationIndicator.equals(other.getTerminalLocationIndicator()))) &&
            ((this.enablePartialAuthorization==null && other.getEnablePartialAuthorization()==null) || 
             (this.enablePartialAuthorization!=null &&
              this.enablePartialAuthorization.equals(other.getEnablePartialAuthorization()))) &&
            ((this.taxAmount==null && other.getTaxAmount()==null) || 
             (this.taxAmount!=null &&
              this.taxAmount.equals(other.getTaxAmount()))) &&
            ((this.purchaseOrderNumber==null && other.getPurchaseOrderNumber()==null) || 
             (this.purchaseOrderNumber!=null &&
              this.purchaseOrderNumber.equals(other.getPurchaseOrderNumber()))) &&
            ((this.customerCode==null && other.getCustomerCode()==null) || 
             (this.customerCode!=null &&
              this.customerCode.equals(other.getCustomerCode()))) &&
            ((this.invoice==null && other.getInvoice()==null) || 
             (this.invoice!=null &&
              this.invoice.equals(other.getInvoice())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAmount() != null) {
            _hashCode += getAmount().hashCode();
        }
        if (getInvoiceNumber() != null) {
            _hashCode += getInvoiceNumber().hashCode();
        }
        if (getRegisterNumber() != null) {
            _hashCode += getRegisterNumber().hashCode();
        }
        if (getMerchantTransactionId() != null) {
            _hashCode += getMerchantTransactionId().hashCode();
        }
        if (getCardAcceptorTerminalId() != null) {
            _hashCode += getCardAcceptorTerminalId().hashCode();
        }
        if (getCardCaptureCapability() != null) {
            _hashCode += getCardCaptureCapability().hashCode();
        }
        if (getEcommerceTransactionIndicator() != null) {
            _hashCode += getEcommerceTransactionIndicator().hashCode();
        }
        if (getPinAuthenticationCapability() != null) {
            _hashCode += getPinAuthenticationCapability().hashCode();
        }
        if (getPosConditionCode() != null) {
            _hashCode += getPosConditionCode().hashCode();
        }
        if (getPosEntryMode() != null) {
            _hashCode += getPosEntryMode().hashCode();
        }
        if (getTerminalCategoryCode() != null) {
            _hashCode += getTerminalCategoryCode().hashCode();
        }
        if (getTerminalEntryCapability() != null) {
            _hashCode += getTerminalEntryCapability().hashCode();
        }
        if (getTerminalLocationIndicator() != null) {
            _hashCode += getTerminalLocationIndicator().hashCode();
        }
        if (getEnablePartialAuthorization() != null) {
            _hashCode += getEnablePartialAuthorization().hashCode();
        }
        if (getTaxAmount() != null) {
            _hashCode += getTaxAmount().hashCode();
        }
        if (getPurchaseOrderNumber() != null) {
            _hashCode += getPurchaseOrderNumber().hashCode();
        }
        if (getCustomerCode() != null) {
            _hashCode += getCustomerCode().hashCode();
        }
        if (getInvoice() != null) {
            _hashCode += getInvoice().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AuthorizationRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "AuthorizationRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("amount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "Amount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("invoiceNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "InvoiceNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("registerNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "RegisterNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("merchantTransactionId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "MerchantTransactionId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardAcceptorTerminalId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "CardAcceptorTerminalId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardCaptureCapability");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "CardCaptureCapability"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ecommerceTransactionIndicator");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "EcommerceTransactionIndicator"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pinAuthenticationCapability");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "PinAuthenticationCapability"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("posConditionCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "PosConditionCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("posEntryMode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "PosEntryMode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("terminalCategoryCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "TerminalCategoryCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("terminalEntryCapability");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "TerminalEntryCapability"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("terminalLocationIndicator");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "TerminalLocationIndicator"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("enablePartialAuthorization");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "EnablePartialAuthorization"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("taxAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "TaxAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("purchaseOrderNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "PurchaseOrderNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customerCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "CustomerCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("invoice");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "Invoice"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "Invoice"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
