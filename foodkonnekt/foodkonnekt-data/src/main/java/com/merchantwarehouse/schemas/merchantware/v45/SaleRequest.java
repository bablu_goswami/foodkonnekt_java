/**
 * SaleRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.merchantwarehouse.schemas.merchantware.v45;

public class SaleRequest  implements java.io.Serializable {
    private java.lang.String amount;

    private java.lang.String cashbackAmount;

    private java.lang.String surchargeAmount;

    private java.lang.String taxAmount;

    private com.merchantwarehouse.schemas.merchantware.v45.HealthCareAmountDetails healthCareAmountDetails;

    private java.lang.String invoiceNumber;

    private java.lang.String purchaseOrderNumber;

    private java.lang.String customerCode;

    private java.lang.String registerNumber;

    private java.lang.String merchantTransactionId;

    private java.lang.String cardAcceptorTerminalId;

    private java.lang.String enablePartialAuthorization;

    private java.lang.String forceDuplicate;

    private java.lang.String cardCaptureCapability;

    private java.lang.String ecommerceTransactionIndicator;

    private java.lang.String pinAuthenticationCapability;

    private java.lang.String posConditionCode;

    private java.lang.String posEntryMode;

    private java.lang.String terminalCategoryCode;

    private java.lang.String terminalEntryCapability;

    private java.lang.String terminalLocationIndicator;

    private com.merchantwarehouse.schemas.merchantware.v45.Invoice invoice;

    public SaleRequest() {
    }

    public SaleRequest(
           java.lang.String amount,
           java.lang.String cashbackAmount,
           java.lang.String surchargeAmount,
           java.lang.String taxAmount,
           com.merchantwarehouse.schemas.merchantware.v45.HealthCareAmountDetails healthCareAmountDetails,
           java.lang.String invoiceNumber,
           java.lang.String purchaseOrderNumber,
           java.lang.String customerCode,
           java.lang.String registerNumber,
           java.lang.String merchantTransactionId,
           java.lang.String cardAcceptorTerminalId,
           java.lang.String enablePartialAuthorization,
           java.lang.String forceDuplicate,
           java.lang.String cardCaptureCapability,
           java.lang.String ecommerceTransactionIndicator,
           java.lang.String pinAuthenticationCapability,
           java.lang.String posConditionCode,
           java.lang.String posEntryMode,
           java.lang.String terminalCategoryCode,
           java.lang.String terminalEntryCapability,
           java.lang.String terminalLocationIndicator,
           com.merchantwarehouse.schemas.merchantware.v45.Invoice invoice) {
           this.amount = amount;
           this.cashbackAmount = cashbackAmount;
           this.surchargeAmount = surchargeAmount;
           this.taxAmount = taxAmount;
           this.healthCareAmountDetails = healthCareAmountDetails;
           this.invoiceNumber = invoiceNumber;
           this.purchaseOrderNumber = purchaseOrderNumber;
           this.customerCode = customerCode;
           this.registerNumber = registerNumber;
           this.merchantTransactionId = merchantTransactionId;
           this.cardAcceptorTerminalId = cardAcceptorTerminalId;
           this.enablePartialAuthorization = enablePartialAuthorization;
           this.forceDuplicate = forceDuplicate;
           this.cardCaptureCapability = cardCaptureCapability;
           this.ecommerceTransactionIndicator = ecommerceTransactionIndicator;
           this.pinAuthenticationCapability = pinAuthenticationCapability;
           this.posConditionCode = posConditionCode;
           this.posEntryMode = posEntryMode;
           this.terminalCategoryCode = terminalCategoryCode;
           this.terminalEntryCapability = terminalEntryCapability;
           this.terminalLocationIndicator = terminalLocationIndicator;
           this.invoice = invoice;
    }


    /**
     * Gets the amount value for this SaleRequest.
     * 
     * @return amount
     */
    public java.lang.String getAmount() {
        return amount;
    }


    /**
     * Sets the amount value for this SaleRequest.
     * 
     * @param amount
     */
    public void setAmount(java.lang.String amount) {
        this.amount = amount;
    }


    /**
     * Gets the cashbackAmount value for this SaleRequest.
     * 
     * @return cashbackAmount
     */
    public java.lang.String getCashbackAmount() {
        return cashbackAmount;
    }


    /**
     * Sets the cashbackAmount value for this SaleRequest.
     * 
     * @param cashbackAmount
     */
    public void setCashbackAmount(java.lang.String cashbackAmount) {
        this.cashbackAmount = cashbackAmount;
    }


    /**
     * Gets the surchargeAmount value for this SaleRequest.
     * 
     * @return surchargeAmount
     */
    public java.lang.String getSurchargeAmount() {
        return surchargeAmount;
    }


    /**
     * Sets the surchargeAmount value for this SaleRequest.
     * 
     * @param surchargeAmount
     */
    public void setSurchargeAmount(java.lang.String surchargeAmount) {
        this.surchargeAmount = surchargeAmount;
    }


    /**
     * Gets the taxAmount value for this SaleRequest.
     * 
     * @return taxAmount
     */
    public java.lang.String getTaxAmount() {
        return taxAmount;
    }


    /**
     * Sets the taxAmount value for this SaleRequest.
     * 
     * @param taxAmount
     */
    public void setTaxAmount(java.lang.String taxAmount) {
        this.taxAmount = taxAmount;
    }


    /**
     * Gets the healthCareAmountDetails value for this SaleRequest.
     * 
     * @return healthCareAmountDetails
     */
    public com.merchantwarehouse.schemas.merchantware.v45.HealthCareAmountDetails getHealthCareAmountDetails() {
        return healthCareAmountDetails;
    }


    /**
     * Sets the healthCareAmountDetails value for this SaleRequest.
     * 
     * @param healthCareAmountDetails
     */
    public void setHealthCareAmountDetails(com.merchantwarehouse.schemas.merchantware.v45.HealthCareAmountDetails healthCareAmountDetails) {
        this.healthCareAmountDetails = healthCareAmountDetails;
    }


    /**
     * Gets the invoiceNumber value for this SaleRequest.
     * 
     * @return invoiceNumber
     */
    public java.lang.String getInvoiceNumber() {
        return invoiceNumber;
    }


    /**
     * Sets the invoiceNumber value for this SaleRequest.
     * 
     * @param invoiceNumber
     */
    public void setInvoiceNumber(java.lang.String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }


    /**
     * Gets the purchaseOrderNumber value for this SaleRequest.
     * 
     * @return purchaseOrderNumber
     */
    public java.lang.String getPurchaseOrderNumber() {
        return purchaseOrderNumber;
    }


    /**
     * Sets the purchaseOrderNumber value for this SaleRequest.
     * 
     * @param purchaseOrderNumber
     */
    public void setPurchaseOrderNumber(java.lang.String purchaseOrderNumber) {
        this.purchaseOrderNumber = purchaseOrderNumber;
    }


    /**
     * Gets the customerCode value for this SaleRequest.
     * 
     * @return customerCode
     */
    public java.lang.String getCustomerCode() {
        return customerCode;
    }


    /**
     * Sets the customerCode value for this SaleRequest.
     * 
     * @param customerCode
     */
    public void setCustomerCode(java.lang.String customerCode) {
        this.customerCode = customerCode;
    }


    /**
     * Gets the registerNumber value for this SaleRequest.
     * 
     * @return registerNumber
     */
    public java.lang.String getRegisterNumber() {
        return registerNumber;
    }


    /**
     * Sets the registerNumber value for this SaleRequest.
     * 
     * @param registerNumber
     */
    public void setRegisterNumber(java.lang.String registerNumber) {
        this.registerNumber = registerNumber;
    }


    /**
     * Gets the merchantTransactionId value for this SaleRequest.
     * 
     * @return merchantTransactionId
     */
    public java.lang.String getMerchantTransactionId() {
        return merchantTransactionId;
    }


    /**
     * Sets the merchantTransactionId value for this SaleRequest.
     * 
     * @param merchantTransactionId
     */
    public void setMerchantTransactionId(java.lang.String merchantTransactionId) {
        this.merchantTransactionId = merchantTransactionId;
    }


    /**
     * Gets the cardAcceptorTerminalId value for this SaleRequest.
     * 
     * @return cardAcceptorTerminalId
     */
    public java.lang.String getCardAcceptorTerminalId() {
        return cardAcceptorTerminalId;
    }


    /**
     * Sets the cardAcceptorTerminalId value for this SaleRequest.
     * 
     * @param cardAcceptorTerminalId
     */
    public void setCardAcceptorTerminalId(java.lang.String cardAcceptorTerminalId) {
        this.cardAcceptorTerminalId = cardAcceptorTerminalId;
    }


    /**
     * Gets the enablePartialAuthorization value for this SaleRequest.
     * 
     * @return enablePartialAuthorization
     */
    public java.lang.String getEnablePartialAuthorization() {
        return enablePartialAuthorization;
    }


    /**
     * Sets the enablePartialAuthorization value for this SaleRequest.
     * 
     * @param enablePartialAuthorization
     */
    public void setEnablePartialAuthorization(java.lang.String enablePartialAuthorization) {
        this.enablePartialAuthorization = enablePartialAuthorization;
    }


    /**
     * Gets the forceDuplicate value for this SaleRequest.
     * 
     * @return forceDuplicate
     */
    public java.lang.String getForceDuplicate() {
        return forceDuplicate;
    }


    /**
     * Sets the forceDuplicate value for this SaleRequest.
     * 
     * @param forceDuplicate
     */
    public void setForceDuplicate(java.lang.String forceDuplicate) {
        this.forceDuplicate = forceDuplicate;
    }


    /**
     * Gets the cardCaptureCapability value for this SaleRequest.
     * 
     * @return cardCaptureCapability
     */
    public java.lang.String getCardCaptureCapability() {
        return cardCaptureCapability;
    }


    /**
     * Sets the cardCaptureCapability value for this SaleRequest.
     * 
     * @param cardCaptureCapability
     */
    public void setCardCaptureCapability(java.lang.String cardCaptureCapability) {
        this.cardCaptureCapability = cardCaptureCapability;
    }


    /**
     * Gets the ecommerceTransactionIndicator value for this SaleRequest.
     * 
     * @return ecommerceTransactionIndicator
     */
    public java.lang.String getEcommerceTransactionIndicator() {
        return ecommerceTransactionIndicator;
    }


    /**
     * Sets the ecommerceTransactionIndicator value for this SaleRequest.
     * 
     * @param ecommerceTransactionIndicator
     */
    public void setEcommerceTransactionIndicator(java.lang.String ecommerceTransactionIndicator) {
        this.ecommerceTransactionIndicator = ecommerceTransactionIndicator;
    }


    /**
     * Gets the pinAuthenticationCapability value for this SaleRequest.
     * 
     * @return pinAuthenticationCapability
     */
    public java.lang.String getPinAuthenticationCapability() {
        return pinAuthenticationCapability;
    }


    /**
     * Sets the pinAuthenticationCapability value for this SaleRequest.
     * 
     * @param pinAuthenticationCapability
     */
    public void setPinAuthenticationCapability(java.lang.String pinAuthenticationCapability) {
        this.pinAuthenticationCapability = pinAuthenticationCapability;
    }


    /**
     * Gets the posConditionCode value for this SaleRequest.
     * 
     * @return posConditionCode
     */
    public java.lang.String getPosConditionCode() {
        return posConditionCode;
    }


    /**
     * Sets the posConditionCode value for this SaleRequest.
     * 
     * @param posConditionCode
     */
    public void setPosConditionCode(java.lang.String posConditionCode) {
        this.posConditionCode = posConditionCode;
    }


    /**
     * Gets the posEntryMode value for this SaleRequest.
     * 
     * @return posEntryMode
     */
    public java.lang.String getPosEntryMode() {
        return posEntryMode;
    }


    /**
     * Sets the posEntryMode value for this SaleRequest.
     * 
     * @param posEntryMode
     */
    public void setPosEntryMode(java.lang.String posEntryMode) {
        this.posEntryMode = posEntryMode;
    }


    /**
     * Gets the terminalCategoryCode value for this SaleRequest.
     * 
     * @return terminalCategoryCode
     */
    public java.lang.String getTerminalCategoryCode() {
        return terminalCategoryCode;
    }


    /**
     * Sets the terminalCategoryCode value for this SaleRequest.
     * 
     * @param terminalCategoryCode
     */
    public void setTerminalCategoryCode(java.lang.String terminalCategoryCode) {
        this.terminalCategoryCode = terminalCategoryCode;
    }


    /**
     * Gets the terminalEntryCapability value for this SaleRequest.
     * 
     * @return terminalEntryCapability
     */
    public java.lang.String getTerminalEntryCapability() {
        return terminalEntryCapability;
    }


    /**
     * Sets the terminalEntryCapability value for this SaleRequest.
     * 
     * @param terminalEntryCapability
     */
    public void setTerminalEntryCapability(java.lang.String terminalEntryCapability) {
        this.terminalEntryCapability = terminalEntryCapability;
    }


    /**
     * Gets the terminalLocationIndicator value for this SaleRequest.
     * 
     * @return terminalLocationIndicator
     */
    public java.lang.String getTerminalLocationIndicator() {
        return terminalLocationIndicator;
    }


    /**
     * Sets the terminalLocationIndicator value for this SaleRequest.
     * 
     * @param terminalLocationIndicator
     */
    public void setTerminalLocationIndicator(java.lang.String terminalLocationIndicator) {
        this.terminalLocationIndicator = terminalLocationIndicator;
    }


    /**
     * Gets the invoice value for this SaleRequest.
     * 
     * @return invoice
     */
    public com.merchantwarehouse.schemas.merchantware.v45.Invoice getInvoice() {
        return invoice;
    }


    /**
     * Sets the invoice value for this SaleRequest.
     * 
     * @param invoice
     */
    public void setInvoice(com.merchantwarehouse.schemas.merchantware.v45.Invoice invoice) {
        this.invoice = invoice;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SaleRequest)) return false;
        SaleRequest other = (SaleRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.amount==null && other.getAmount()==null) || 
             (this.amount!=null &&
              this.amount.equals(other.getAmount()))) &&
            ((this.cashbackAmount==null && other.getCashbackAmount()==null) || 
             (this.cashbackAmount!=null &&
              this.cashbackAmount.equals(other.getCashbackAmount()))) &&
            ((this.surchargeAmount==null && other.getSurchargeAmount()==null) || 
             (this.surchargeAmount!=null &&
              this.surchargeAmount.equals(other.getSurchargeAmount()))) &&
            ((this.taxAmount==null && other.getTaxAmount()==null) || 
             (this.taxAmount!=null &&
              this.taxAmount.equals(other.getTaxAmount()))) &&
            ((this.healthCareAmountDetails==null && other.getHealthCareAmountDetails()==null) || 
             (this.healthCareAmountDetails!=null &&
              this.healthCareAmountDetails.equals(other.getHealthCareAmountDetails()))) &&
            ((this.invoiceNumber==null && other.getInvoiceNumber()==null) || 
             (this.invoiceNumber!=null &&
              this.invoiceNumber.equals(other.getInvoiceNumber()))) &&
            ((this.purchaseOrderNumber==null && other.getPurchaseOrderNumber()==null) || 
             (this.purchaseOrderNumber!=null &&
              this.purchaseOrderNumber.equals(other.getPurchaseOrderNumber()))) &&
            ((this.customerCode==null && other.getCustomerCode()==null) || 
             (this.customerCode!=null &&
              this.customerCode.equals(other.getCustomerCode()))) &&
            ((this.registerNumber==null && other.getRegisterNumber()==null) || 
             (this.registerNumber!=null &&
              this.registerNumber.equals(other.getRegisterNumber()))) &&
            ((this.merchantTransactionId==null && other.getMerchantTransactionId()==null) || 
             (this.merchantTransactionId!=null &&
              this.merchantTransactionId.equals(other.getMerchantTransactionId()))) &&
            ((this.cardAcceptorTerminalId==null && other.getCardAcceptorTerminalId()==null) || 
             (this.cardAcceptorTerminalId!=null &&
              this.cardAcceptorTerminalId.equals(other.getCardAcceptorTerminalId()))) &&
            ((this.enablePartialAuthorization==null && other.getEnablePartialAuthorization()==null) || 
             (this.enablePartialAuthorization!=null &&
              this.enablePartialAuthorization.equals(other.getEnablePartialAuthorization()))) &&
            ((this.forceDuplicate==null && other.getForceDuplicate()==null) || 
             (this.forceDuplicate!=null &&
              this.forceDuplicate.equals(other.getForceDuplicate()))) &&
            ((this.cardCaptureCapability==null && other.getCardCaptureCapability()==null) || 
             (this.cardCaptureCapability!=null &&
              this.cardCaptureCapability.equals(other.getCardCaptureCapability()))) &&
            ((this.ecommerceTransactionIndicator==null && other.getEcommerceTransactionIndicator()==null) || 
             (this.ecommerceTransactionIndicator!=null &&
              this.ecommerceTransactionIndicator.equals(other.getEcommerceTransactionIndicator()))) &&
            ((this.pinAuthenticationCapability==null && other.getPinAuthenticationCapability()==null) || 
             (this.pinAuthenticationCapability!=null &&
              this.pinAuthenticationCapability.equals(other.getPinAuthenticationCapability()))) &&
            ((this.posConditionCode==null && other.getPosConditionCode()==null) || 
             (this.posConditionCode!=null &&
              this.posConditionCode.equals(other.getPosConditionCode()))) &&
            ((this.posEntryMode==null && other.getPosEntryMode()==null) || 
             (this.posEntryMode!=null &&
              this.posEntryMode.equals(other.getPosEntryMode()))) &&
            ((this.terminalCategoryCode==null && other.getTerminalCategoryCode()==null) || 
             (this.terminalCategoryCode!=null &&
              this.terminalCategoryCode.equals(other.getTerminalCategoryCode()))) &&
            ((this.terminalEntryCapability==null && other.getTerminalEntryCapability()==null) || 
             (this.terminalEntryCapability!=null &&
              this.terminalEntryCapability.equals(other.getTerminalEntryCapability()))) &&
            ((this.terminalLocationIndicator==null && other.getTerminalLocationIndicator()==null) || 
             (this.terminalLocationIndicator!=null &&
              this.terminalLocationIndicator.equals(other.getTerminalLocationIndicator()))) &&
            ((this.invoice==null && other.getInvoice()==null) || 
             (this.invoice!=null &&
              this.invoice.equals(other.getInvoice())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAmount() != null) {
            _hashCode += getAmount().hashCode();
        }
        if (getCashbackAmount() != null) {
            _hashCode += getCashbackAmount().hashCode();
        }
        if (getSurchargeAmount() != null) {
            _hashCode += getSurchargeAmount().hashCode();
        }
        if (getTaxAmount() != null) {
            _hashCode += getTaxAmount().hashCode();
        }
        if (getHealthCareAmountDetails() != null) {
            _hashCode += getHealthCareAmountDetails().hashCode();
        }
        if (getInvoiceNumber() != null) {
            _hashCode += getInvoiceNumber().hashCode();
        }
        if (getPurchaseOrderNumber() != null) {
            _hashCode += getPurchaseOrderNumber().hashCode();
        }
        if (getCustomerCode() != null) {
            _hashCode += getCustomerCode().hashCode();
        }
        if (getRegisterNumber() != null) {
            _hashCode += getRegisterNumber().hashCode();
        }
        if (getMerchantTransactionId() != null) {
            _hashCode += getMerchantTransactionId().hashCode();
        }
        if (getCardAcceptorTerminalId() != null) {
            _hashCode += getCardAcceptorTerminalId().hashCode();
        }
        if (getEnablePartialAuthorization() != null) {
            _hashCode += getEnablePartialAuthorization().hashCode();
        }
        if (getForceDuplicate() != null) {
            _hashCode += getForceDuplicate().hashCode();
        }
        if (getCardCaptureCapability() != null) {
            _hashCode += getCardCaptureCapability().hashCode();
        }
        if (getEcommerceTransactionIndicator() != null) {
            _hashCode += getEcommerceTransactionIndicator().hashCode();
        }
        if (getPinAuthenticationCapability() != null) {
            _hashCode += getPinAuthenticationCapability().hashCode();
        }
        if (getPosConditionCode() != null) {
            _hashCode += getPosConditionCode().hashCode();
        }
        if (getPosEntryMode() != null) {
            _hashCode += getPosEntryMode().hashCode();
        }
        if (getTerminalCategoryCode() != null) {
            _hashCode += getTerminalCategoryCode().hashCode();
        }
        if (getTerminalEntryCapability() != null) {
            _hashCode += getTerminalEntryCapability().hashCode();
        }
        if (getTerminalLocationIndicator() != null) {
            _hashCode += getTerminalLocationIndicator().hashCode();
        }
        if (getInvoice() != null) {
            _hashCode += getInvoice().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SaleRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "SaleRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("amount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "Amount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cashbackAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "CashbackAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("surchargeAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "SurchargeAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("taxAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "TaxAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("healthCareAmountDetails");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "HealthCareAmountDetails"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "HealthCareAmountDetails"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("invoiceNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "InvoiceNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("purchaseOrderNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "PurchaseOrderNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customerCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "CustomerCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("registerNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "RegisterNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("merchantTransactionId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "MerchantTransactionId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardAcceptorTerminalId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "CardAcceptorTerminalId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("enablePartialAuthorization");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "EnablePartialAuthorization"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("forceDuplicate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "ForceDuplicate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardCaptureCapability");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "CardCaptureCapability"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ecommerceTransactionIndicator");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "EcommerceTransactionIndicator"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pinAuthenticationCapability");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "PinAuthenticationCapability"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("posConditionCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "PosConditionCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("posEntryMode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "PosEntryMode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("terminalCategoryCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "TerminalCategoryCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("terminalEntryCapability");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "TerminalEntryCapability"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("terminalLocationIndicator");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "TerminalLocationIndicator"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("invoice");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "Invoice"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "Invoice"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
