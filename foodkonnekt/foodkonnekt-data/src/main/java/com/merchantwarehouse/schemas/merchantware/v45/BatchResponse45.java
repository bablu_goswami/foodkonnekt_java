/**
 * BatchResponse45.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.merchantwarehouse.schemas.merchantware.v45;

public class BatchResponse45  implements java.io.Serializable {
    private java.lang.String batchStatus;

    private java.lang.String authorizationCode;

    private java.lang.String batchAmount;

    private int transactionCount;

    private java.lang.String transactionDate;

    private java.lang.String errorMessage;

    private java.lang.String extraData;

    public BatchResponse45() {
    }

    public BatchResponse45(
           java.lang.String batchStatus,
           java.lang.String authorizationCode,
           java.lang.String batchAmount,
           int transactionCount,
           java.lang.String transactionDate,
           java.lang.String errorMessage,
           java.lang.String extraData) {
           this.batchStatus = batchStatus;
           this.authorizationCode = authorizationCode;
           this.batchAmount = batchAmount;
           this.transactionCount = transactionCount;
           this.transactionDate = transactionDate;
           this.errorMessage = errorMessage;
           this.extraData = extraData;
    }


    /**
     * Gets the batchStatus value for this BatchResponse45.
     * 
     * @return batchStatus
     */
    public java.lang.String getBatchStatus() {
        return batchStatus;
    }


    /**
     * Sets the batchStatus value for this BatchResponse45.
     * 
     * @param batchStatus
     */
    public void setBatchStatus(java.lang.String batchStatus) {
        this.batchStatus = batchStatus;
    }


    /**
     * Gets the authorizationCode value for this BatchResponse45.
     * 
     * @return authorizationCode
     */
    public java.lang.String getAuthorizationCode() {
        return authorizationCode;
    }


    /**
     * Sets the authorizationCode value for this BatchResponse45.
     * 
     * @param authorizationCode
     */
    public void setAuthorizationCode(java.lang.String authorizationCode) {
        this.authorizationCode = authorizationCode;
    }


    /**
     * Gets the batchAmount value for this BatchResponse45.
     * 
     * @return batchAmount
     */
    public java.lang.String getBatchAmount() {
        return batchAmount;
    }


    /**
     * Sets the batchAmount value for this BatchResponse45.
     * 
     * @param batchAmount
     */
    public void setBatchAmount(java.lang.String batchAmount) {
        this.batchAmount = batchAmount;
    }


    /**
     * Gets the transactionCount value for this BatchResponse45.
     * 
     * @return transactionCount
     */
    public int getTransactionCount() {
        return transactionCount;
    }


    /**
     * Sets the transactionCount value for this BatchResponse45.
     * 
     * @param transactionCount
     */
    public void setTransactionCount(int transactionCount) {
        this.transactionCount = transactionCount;
    }


    /**
     * Gets the transactionDate value for this BatchResponse45.
     * 
     * @return transactionDate
     */
    public java.lang.String getTransactionDate() {
        return transactionDate;
    }


    /**
     * Sets the transactionDate value for this BatchResponse45.
     * 
     * @param transactionDate
     */
    public void setTransactionDate(java.lang.String transactionDate) {
        this.transactionDate = transactionDate;
    }


    /**
     * Gets the errorMessage value for this BatchResponse45.
     * 
     * @return errorMessage
     */
    public java.lang.String getErrorMessage() {
        return errorMessage;
    }


    /**
     * Sets the errorMessage value for this BatchResponse45.
     * 
     * @param errorMessage
     */
    public void setErrorMessage(java.lang.String errorMessage) {
        this.errorMessage = errorMessage;
    }


    /**
     * Gets the extraData value for this BatchResponse45.
     * 
     * @return extraData
     */
    public java.lang.String getExtraData() {
        return extraData;
    }


    /**
     * Sets the extraData value for this BatchResponse45.
     * 
     * @param extraData
     */
    public void setExtraData(java.lang.String extraData) {
        this.extraData = extraData;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof BatchResponse45)) return false;
        BatchResponse45 other = (BatchResponse45) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.batchStatus==null && other.getBatchStatus()==null) || 
             (this.batchStatus!=null &&
              this.batchStatus.equals(other.getBatchStatus()))) &&
            ((this.authorizationCode==null && other.getAuthorizationCode()==null) || 
             (this.authorizationCode!=null &&
              this.authorizationCode.equals(other.getAuthorizationCode()))) &&
            ((this.batchAmount==null && other.getBatchAmount()==null) || 
             (this.batchAmount!=null &&
              this.batchAmount.equals(other.getBatchAmount()))) &&
            this.transactionCount == other.getTransactionCount() &&
            ((this.transactionDate==null && other.getTransactionDate()==null) || 
             (this.transactionDate!=null &&
              this.transactionDate.equals(other.getTransactionDate()))) &&
            ((this.errorMessage==null && other.getErrorMessage()==null) || 
             (this.errorMessage!=null &&
              this.errorMessage.equals(other.getErrorMessage()))) &&
            ((this.extraData==null && other.getExtraData()==null) || 
             (this.extraData!=null &&
              this.extraData.equals(other.getExtraData())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getBatchStatus() != null) {
            _hashCode += getBatchStatus().hashCode();
        }
        if (getAuthorizationCode() != null) {
            _hashCode += getAuthorizationCode().hashCode();
        }
        if (getBatchAmount() != null) {
            _hashCode += getBatchAmount().hashCode();
        }
        _hashCode += getTransactionCount();
        if (getTransactionDate() != null) {
            _hashCode += getTransactionDate().hashCode();
        }
        if (getErrorMessage() != null) {
            _hashCode += getErrorMessage().hashCode();
        }
        if (getExtraData() != null) {
            _hashCode += getExtraData().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(BatchResponse45.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "BatchResponse45"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("batchStatus");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "BatchStatus"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("authorizationCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "AuthorizationCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("batchAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "BatchAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transactionCount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "TransactionCount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transactionDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "TransactionDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("errorMessage");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "ErrorMessage"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("extraData");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "ExtraData"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
