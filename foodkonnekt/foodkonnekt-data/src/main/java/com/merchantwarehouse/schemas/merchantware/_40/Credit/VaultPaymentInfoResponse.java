/**
 * VaultPaymentInfoResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.merchantwarehouse.schemas.merchantware._40.Credit;

public class VaultPaymentInfoResponse  implements java.io.Serializable {
    private java.lang.String avsStreetAddress;

    private java.lang.String avsZipCode;

    private java.lang.String cardholder;

    private java.lang.String cardNumber;

    private java.lang.String cardType;

    private java.lang.String errorCode;

    private java.lang.String errorMessage;

    private java.lang.String expirationDate;

    public VaultPaymentInfoResponse() {
    }

    public VaultPaymentInfoResponse(
           java.lang.String avsStreetAddress,
           java.lang.String avsZipCode,
           java.lang.String cardholder,
           java.lang.String cardNumber,
           java.lang.String cardType,
           java.lang.String errorCode,
           java.lang.String errorMessage,
           java.lang.String expirationDate) {
           this.avsStreetAddress = avsStreetAddress;
           this.avsZipCode = avsZipCode;
           this.cardholder = cardholder;
           this.cardNumber = cardNumber;
           this.cardType = cardType;
           this.errorCode = errorCode;
           this.errorMessage = errorMessage;
           this.expirationDate = expirationDate;
    }


    /**
     * Gets the avsStreetAddress value for this VaultPaymentInfoResponse.
     * 
     * @return avsStreetAddress
     */
    public java.lang.String getAvsStreetAddress() {
        return avsStreetAddress;
    }


    /**
     * Sets the avsStreetAddress value for this VaultPaymentInfoResponse.
     * 
     * @param avsStreetAddress
     */
    public void setAvsStreetAddress(java.lang.String avsStreetAddress) {
        this.avsStreetAddress = avsStreetAddress;
    }


    /**
     * Gets the avsZipCode value for this VaultPaymentInfoResponse.
     * 
     * @return avsZipCode
     */
    public java.lang.String getAvsZipCode() {
        return avsZipCode;
    }


    /**
     * Sets the avsZipCode value for this VaultPaymentInfoResponse.
     * 
     * @param avsZipCode
     */
    public void setAvsZipCode(java.lang.String avsZipCode) {
        this.avsZipCode = avsZipCode;
    }


    /**
     * Gets the cardholder value for this VaultPaymentInfoResponse.
     * 
     * @return cardholder
     */
    public java.lang.String getCardholder() {
        return cardholder;
    }


    /**
     * Sets the cardholder value for this VaultPaymentInfoResponse.
     * 
     * @param cardholder
     */
    public void setCardholder(java.lang.String cardholder) {
        this.cardholder = cardholder;
    }


    /**
     * Gets the cardNumber value for this VaultPaymentInfoResponse.
     * 
     * @return cardNumber
     */
    public java.lang.String getCardNumber() {
        return cardNumber;
    }


    /**
     * Sets the cardNumber value for this VaultPaymentInfoResponse.
     * 
     * @param cardNumber
     */
    public void setCardNumber(java.lang.String cardNumber) {
        this.cardNumber = cardNumber;
    }


    /**
     * Gets the cardType value for this VaultPaymentInfoResponse.
     * 
     * @return cardType
     */
    public java.lang.String getCardType() {
        return cardType;
    }


    /**
     * Sets the cardType value for this VaultPaymentInfoResponse.
     * 
     * @param cardType
     */
    public void setCardType(java.lang.String cardType) {
        this.cardType = cardType;
    }


    /**
     * Gets the errorCode value for this VaultPaymentInfoResponse.
     * 
     * @return errorCode
     */
    public java.lang.String getErrorCode() {
        return errorCode;
    }


    /**
     * Sets the errorCode value for this VaultPaymentInfoResponse.
     * 
     * @param errorCode
     */
    public void setErrorCode(java.lang.String errorCode) {
        this.errorCode = errorCode;
    }


    /**
     * Gets the errorMessage value for this VaultPaymentInfoResponse.
     * 
     * @return errorMessage
     */
    public java.lang.String getErrorMessage() {
        return errorMessage;
    }


    /**
     * Sets the errorMessage value for this VaultPaymentInfoResponse.
     * 
     * @param errorMessage
     */
    public void setErrorMessage(java.lang.String errorMessage) {
        this.errorMessage = errorMessage;
    }


    /**
     * Gets the expirationDate value for this VaultPaymentInfoResponse.
     * 
     * @return expirationDate
     */
    public java.lang.String getExpirationDate() {
        return expirationDate;
    }


    /**
     * Sets the expirationDate value for this VaultPaymentInfoResponse.
     * 
     * @param expirationDate
     */
    public void setExpirationDate(java.lang.String expirationDate) {
        this.expirationDate = expirationDate;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof VaultPaymentInfoResponse)) return false;
        VaultPaymentInfoResponse other = (VaultPaymentInfoResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.avsStreetAddress==null && other.getAvsStreetAddress()==null) || 
             (this.avsStreetAddress!=null &&
              this.avsStreetAddress.equals(other.getAvsStreetAddress()))) &&
            ((this.avsZipCode==null && other.getAvsZipCode()==null) || 
             (this.avsZipCode!=null &&
              this.avsZipCode.equals(other.getAvsZipCode()))) &&
            ((this.cardholder==null && other.getCardholder()==null) || 
             (this.cardholder!=null &&
              this.cardholder.equals(other.getCardholder()))) &&
            ((this.cardNumber==null && other.getCardNumber()==null) || 
             (this.cardNumber!=null &&
              this.cardNumber.equals(other.getCardNumber()))) &&
            ((this.cardType==null && other.getCardType()==null) || 
             (this.cardType!=null &&
              this.cardType.equals(other.getCardType()))) &&
            ((this.errorCode==null && other.getErrorCode()==null) || 
             (this.errorCode!=null &&
              this.errorCode.equals(other.getErrorCode()))) &&
            ((this.errorMessage==null && other.getErrorMessage()==null) || 
             (this.errorMessage!=null &&
              this.errorMessage.equals(other.getErrorMessage()))) &&
            ((this.expirationDate==null && other.getExpirationDate()==null) || 
             (this.expirationDate!=null &&
              this.expirationDate.equals(other.getExpirationDate())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAvsStreetAddress() != null) {
            _hashCode += getAvsStreetAddress().hashCode();
        }
        if (getAvsZipCode() != null) {
            _hashCode += getAvsZipCode().hashCode();
        }
        if (getCardholder() != null) {
            _hashCode += getCardholder().hashCode();
        }
        if (getCardNumber() != null) {
            _hashCode += getCardNumber().hashCode();
        }
        if (getCardType() != null) {
            _hashCode += getCardType().hashCode();
        }
        if (getErrorCode() != null) {
            _hashCode += getErrorCode().hashCode();
        }
        if (getErrorMessage() != null) {
            _hashCode += getErrorMessage().hashCode();
        }
        if (getExpirationDate() != null) {
            _hashCode += getExpirationDate().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(VaultPaymentInfoResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/40/Credit/", "VaultPaymentInfoResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("avsStreetAddress");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/40/Credit/", "AvsStreetAddress"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("avsZipCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/40/Credit/", "AvsZipCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardholder");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/40/Credit/", "Cardholder"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/40/Credit/", "CardNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/40/Credit/", "CardType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("errorCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/40/Credit/", "ErrorCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("errorMessage");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/40/Credit/", "ErrorMessage"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("expirationDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/40/Credit/", "ExpirationDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
