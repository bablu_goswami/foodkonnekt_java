/**
 * BatchResponse4.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.merchantwarehouse.schemas.merchantware._40.Credit;

public class BatchResponse4  implements java.io.Serializable {
    private java.lang.String authorizationCode;

    private java.lang.String batchAmount;

    private java.lang.String batchStatus;

    private java.lang.String errorMessage;

    private java.lang.String extraData;

    private java.lang.String token;

    private int transactionCount;

    private java.lang.String transactionDate;

    public BatchResponse4() {
    }

    public BatchResponse4(
           java.lang.String authorizationCode,
           java.lang.String batchAmount,
           java.lang.String batchStatus,
           java.lang.String errorMessage,
           java.lang.String extraData,
           java.lang.String token,
           int transactionCount,
           java.lang.String transactionDate) {
           this.authorizationCode = authorizationCode;
           this.batchAmount = batchAmount;
           this.batchStatus = batchStatus;
           this.errorMessage = errorMessage;
           this.extraData = extraData;
           this.token = token;
           this.transactionCount = transactionCount;
           this.transactionDate = transactionDate;
    }


    /**
     * Gets the authorizationCode value for this BatchResponse4.
     * 
     * @return authorizationCode
     */
    public java.lang.String getAuthorizationCode() {
        return authorizationCode;
    }


    /**
     * Sets the authorizationCode value for this BatchResponse4.
     * 
     * @param authorizationCode
     */
    public void setAuthorizationCode(java.lang.String authorizationCode) {
        this.authorizationCode = authorizationCode;
    }


    /**
     * Gets the batchAmount value for this BatchResponse4.
     * 
     * @return batchAmount
     */
    public java.lang.String getBatchAmount() {
        return batchAmount;
    }


    /**
     * Sets the batchAmount value for this BatchResponse4.
     * 
     * @param batchAmount
     */
    public void setBatchAmount(java.lang.String batchAmount) {
        this.batchAmount = batchAmount;
    }


    /**
     * Gets the batchStatus value for this BatchResponse4.
     * 
     * @return batchStatus
     */
    public java.lang.String getBatchStatus() {
        return batchStatus;
    }


    /**
     * Sets the batchStatus value for this BatchResponse4.
     * 
     * @param batchStatus
     */
    public void setBatchStatus(java.lang.String batchStatus) {
        this.batchStatus = batchStatus;
    }


    /**
     * Gets the errorMessage value for this BatchResponse4.
     * 
     * @return errorMessage
     */
    public java.lang.String getErrorMessage() {
        return errorMessage;
    }


    /**
     * Sets the errorMessage value for this BatchResponse4.
     * 
     * @param errorMessage
     */
    public void setErrorMessage(java.lang.String errorMessage) {
        this.errorMessage = errorMessage;
    }


    /**
     * Gets the extraData value for this BatchResponse4.
     * 
     * @return extraData
     */
    public java.lang.String getExtraData() {
        return extraData;
    }


    /**
     * Sets the extraData value for this BatchResponse4.
     * 
     * @param extraData
     */
    public void setExtraData(java.lang.String extraData) {
        this.extraData = extraData;
    }


    /**
     * Gets the token value for this BatchResponse4.
     * 
     * @return token
     */
    public java.lang.String getToken() {
        return token;
    }


    /**
     * Sets the token value for this BatchResponse4.
     * 
     * @param token
     */
    public void setToken(java.lang.String token) {
        this.token = token;
    }


    /**
     * Gets the transactionCount value for this BatchResponse4.
     * 
     * @return transactionCount
     */
    public int getTransactionCount() {
        return transactionCount;
    }


    /**
     * Sets the transactionCount value for this BatchResponse4.
     * 
     * @param transactionCount
     */
    public void setTransactionCount(int transactionCount) {
        this.transactionCount = transactionCount;
    }


    /**
     * Gets the transactionDate value for this BatchResponse4.
     * 
     * @return transactionDate
     */
    public java.lang.String getTransactionDate() {
        return transactionDate;
    }


    /**
     * Sets the transactionDate value for this BatchResponse4.
     * 
     * @param transactionDate
     */
    public void setTransactionDate(java.lang.String transactionDate) {
        this.transactionDate = transactionDate;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof BatchResponse4)) return false;
        BatchResponse4 other = (BatchResponse4) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.authorizationCode==null && other.getAuthorizationCode()==null) || 
             (this.authorizationCode!=null &&
              this.authorizationCode.equals(other.getAuthorizationCode()))) &&
            ((this.batchAmount==null && other.getBatchAmount()==null) || 
             (this.batchAmount!=null &&
              this.batchAmount.equals(other.getBatchAmount()))) &&
            ((this.batchStatus==null && other.getBatchStatus()==null) || 
             (this.batchStatus!=null &&
              this.batchStatus.equals(other.getBatchStatus()))) &&
            ((this.errorMessage==null && other.getErrorMessage()==null) || 
             (this.errorMessage!=null &&
              this.errorMessage.equals(other.getErrorMessage()))) &&
            ((this.extraData==null && other.getExtraData()==null) || 
             (this.extraData!=null &&
              this.extraData.equals(other.getExtraData()))) &&
            ((this.token==null && other.getToken()==null) || 
             (this.token!=null &&
              this.token.equals(other.getToken()))) &&
            this.transactionCount == other.getTransactionCount() &&
            ((this.transactionDate==null && other.getTransactionDate()==null) || 
             (this.transactionDate!=null &&
              this.transactionDate.equals(other.getTransactionDate())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAuthorizationCode() != null) {
            _hashCode += getAuthorizationCode().hashCode();
        }
        if (getBatchAmount() != null) {
            _hashCode += getBatchAmount().hashCode();
        }
        if (getBatchStatus() != null) {
            _hashCode += getBatchStatus().hashCode();
        }
        if (getErrorMessage() != null) {
            _hashCode += getErrorMessage().hashCode();
        }
        if (getExtraData() != null) {
            _hashCode += getExtraData().hashCode();
        }
        if (getToken() != null) {
            _hashCode += getToken().hashCode();
        }
        _hashCode += getTransactionCount();
        if (getTransactionDate() != null) {
            _hashCode += getTransactionDate().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(BatchResponse4.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/40/Credit/", "BatchResponse4"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("authorizationCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/40/Credit/", "AuthorizationCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("batchAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/40/Credit/", "BatchAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("batchStatus");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/40/Credit/", "BatchStatus"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("errorMessage");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/40/Credit/", "ErrorMessage"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("extraData");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/40/Credit/", "ExtraData"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("token");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/40/Credit/", "Token"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transactionCount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/40/Credit/", "TransactionCount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transactionDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/40/Credit/", "TransactionDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
