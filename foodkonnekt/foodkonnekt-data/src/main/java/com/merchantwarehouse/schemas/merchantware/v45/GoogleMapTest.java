package com.merchantwarehouse.schemas.merchantware.v45;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.commons.httpclient.util.URIUtil;
import org.json.JSONArray;
import org.json.JSONObject;

import com.google.gson.Gson;

public class GoogleMapTest {
public static void main(String[] args) {
	try {
        URL url = new URL(
                "https://maps.googleapis.com/maps/api/geocode/json?address="
                        + URIUtil.encodeQuery("600 Marathon Dr Campbell 75382") + "&sensor=true&key=AIzaSyDjnluKhome9lmt5LLKnIgqot7HtyDetes");
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");
        conn.setRequestProperty("Accept", "application/json");

        if (conn.getResponseCode() != 200) {
            throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
        }
        BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

        String output = "", full = "";
        //System.out.println("output-###-");
        while ((output = br.readLine()) != null) {
        	  
        	//System.out.println(output);
            full += output;
        }
        if(full!="") {
        	 JSONObject jsonObject = new JSONObject(full);
        	 if(jsonObject!=null && jsonObject.toString().contains("results")) {
        		 JSONArray  result  =  (JSONArray) jsonObject.getJSONArray("results");
        		 System.out.println("result.length()---"+result.length());
        		 if(result.length()>0 && result.toString().contains("geometry")) {
        			 for(int i=0;i<result.length();i++) {
         	        	System.out.println("##############");
         	      JSONObject viewPort=  	(JSONObject)result.getJSONObject(i).get("geometry");
         	        	if(viewPort!=null && viewPort.toString().contains("location"))
         	        	{
         	        		 JSONObject location =	(JSONObject)viewPort.get("location");
         	        		 if(location!=null && location.toString().contains("lng") && location.toString().contains("lat")) {
         	        			 System.out.println( location.get("lng")+"---###----"+location.get("lat"));
         	        			/* location.getString("lng");
         	        			 location.getString("lat");*/
         	        		 }
         	        	}
         	    //  System.out.println(result.getJSONObject(i).get("geometry"));
         	        	
         	        } 
        		 }
        	       
        	 }
        }
       
        
       // geometry
        //System.out.println(result);
	 } catch (Exception e) {
	    //    LOGGER.error("error: " + e.getMessage());
	    }
}
}
