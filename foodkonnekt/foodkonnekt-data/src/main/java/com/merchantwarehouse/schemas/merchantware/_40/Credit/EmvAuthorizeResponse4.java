/**
 * EmvAuthorizeResponse4.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.merchantwarehouse.schemas.merchantware._40.Credit;

public class EmvAuthorizeResponse4  implements java.io.Serializable {
    private java.lang.String approvalStatus;

    private java.lang.String amountApproved;

    private java.lang.String authorizationCode;

    private java.lang.String errorMessage;

    private java.lang.String token;

    private java.lang.String transactionDate;

    private java.lang.String tlvData;

    private java.lang.String cashbackAmount;

    private java.lang.String donationAmount;

    private java.lang.String userTipAmount;

    private java.lang.String surchargeAmount;

    private java.lang.String debitTraceNumber;

    public EmvAuthorizeResponse4() {
    }

    public EmvAuthorizeResponse4(
           java.lang.String approvalStatus,
           java.lang.String amountApproved,
           java.lang.String authorizationCode,
           java.lang.String errorMessage,
           java.lang.String token,
           java.lang.String transactionDate,
           java.lang.String tlvData,
           java.lang.String cashbackAmount,
           java.lang.String donationAmount,
           java.lang.String userTipAmount,
           java.lang.String surchargeAmount,
           java.lang.String debitTraceNumber) {
           this.approvalStatus = approvalStatus;
           this.amountApproved = amountApproved;
           this.authorizationCode = authorizationCode;
           this.errorMessage = errorMessage;
           this.token = token;
           this.transactionDate = transactionDate;
           this.tlvData = tlvData;
           this.cashbackAmount = cashbackAmount;
           this.donationAmount = donationAmount;
           this.userTipAmount = userTipAmount;
           this.surchargeAmount = surchargeAmount;
           this.debitTraceNumber = debitTraceNumber;
    }


    /**
     * Gets the approvalStatus value for this EmvAuthorizeResponse4.
     * 
     * @return approvalStatus
     */
    public java.lang.String getApprovalStatus() {
        return approvalStatus;
    }


    /**
     * Sets the approvalStatus value for this EmvAuthorizeResponse4.
     * 
     * @param approvalStatus
     */
    public void setApprovalStatus(java.lang.String approvalStatus) {
        this.approvalStatus = approvalStatus;
    }


    /**
     * Gets the amountApproved value for this EmvAuthorizeResponse4.
     * 
     * @return amountApproved
     */
    public java.lang.String getAmountApproved() {
        return amountApproved;
    }


    /**
     * Sets the amountApproved value for this EmvAuthorizeResponse4.
     * 
     * @param amountApproved
     */
    public void setAmountApproved(java.lang.String amountApproved) {
        this.amountApproved = amountApproved;
    }


    /**
     * Gets the authorizationCode value for this EmvAuthorizeResponse4.
     * 
     * @return authorizationCode
     */
    public java.lang.String getAuthorizationCode() {
        return authorizationCode;
    }


    /**
     * Sets the authorizationCode value for this EmvAuthorizeResponse4.
     * 
     * @param authorizationCode
     */
    public void setAuthorizationCode(java.lang.String authorizationCode) {
        this.authorizationCode = authorizationCode;
    }


    /**
     * Gets the errorMessage value for this EmvAuthorizeResponse4.
     * 
     * @return errorMessage
     */
    public java.lang.String getErrorMessage() {
        return errorMessage;
    }


    /**
     * Sets the errorMessage value for this EmvAuthorizeResponse4.
     * 
     * @param errorMessage
     */
    public void setErrorMessage(java.lang.String errorMessage) {
        this.errorMessage = errorMessage;
    }


    /**
     * Gets the token value for this EmvAuthorizeResponse4.
     * 
     * @return token
     */
    public java.lang.String getToken() {
        return token;
    }


    /**
     * Sets the token value for this EmvAuthorizeResponse4.
     * 
     * @param token
     */
    public void setToken(java.lang.String token) {
        this.token = token;
    }


    /**
     * Gets the transactionDate value for this EmvAuthorizeResponse4.
     * 
     * @return transactionDate
     */
    public java.lang.String getTransactionDate() {
        return transactionDate;
    }


    /**
     * Sets the transactionDate value for this EmvAuthorizeResponse4.
     * 
     * @param transactionDate
     */
    public void setTransactionDate(java.lang.String transactionDate) {
        this.transactionDate = transactionDate;
    }


    /**
     * Gets the tlvData value for this EmvAuthorizeResponse4.
     * 
     * @return tlvData
     */
    public java.lang.String getTlvData() {
        return tlvData;
    }


    /**
     * Sets the tlvData value for this EmvAuthorizeResponse4.
     * 
     * @param tlvData
     */
    public void setTlvData(java.lang.String tlvData) {
        this.tlvData = tlvData;
    }


    /**
     * Gets the cashbackAmount value for this EmvAuthorizeResponse4.
     * 
     * @return cashbackAmount
     */
    public java.lang.String getCashbackAmount() {
        return cashbackAmount;
    }


    /**
     * Sets the cashbackAmount value for this EmvAuthorizeResponse4.
     * 
     * @param cashbackAmount
     */
    public void setCashbackAmount(java.lang.String cashbackAmount) {
        this.cashbackAmount = cashbackAmount;
    }


    /**
     * Gets the donationAmount value for this EmvAuthorizeResponse4.
     * 
     * @return donationAmount
     */
    public java.lang.String getDonationAmount() {
        return donationAmount;
    }


    /**
     * Sets the donationAmount value for this EmvAuthorizeResponse4.
     * 
     * @param donationAmount
     */
    public void setDonationAmount(java.lang.String donationAmount) {
        this.donationAmount = donationAmount;
    }


    /**
     * Gets the userTipAmount value for this EmvAuthorizeResponse4.
     * 
     * @return userTipAmount
     */
    public java.lang.String getUserTipAmount() {
        return userTipAmount;
    }


    /**
     * Sets the userTipAmount value for this EmvAuthorizeResponse4.
     * 
     * @param userTipAmount
     */
    public void setUserTipAmount(java.lang.String userTipAmount) {
        this.userTipAmount = userTipAmount;
    }


    /**
     * Gets the surchargeAmount value for this EmvAuthorizeResponse4.
     * 
     * @return surchargeAmount
     */
    public java.lang.String getSurchargeAmount() {
        return surchargeAmount;
    }


    /**
     * Sets the surchargeAmount value for this EmvAuthorizeResponse4.
     * 
     * @param surchargeAmount
     */
    public void setSurchargeAmount(java.lang.String surchargeAmount) {
        this.surchargeAmount = surchargeAmount;
    }


    /**
     * Gets the debitTraceNumber value for this EmvAuthorizeResponse4.
     * 
     * @return debitTraceNumber
     */
    public java.lang.String getDebitTraceNumber() {
        return debitTraceNumber;
    }


    /**
     * Sets the debitTraceNumber value for this EmvAuthorizeResponse4.
     * 
     * @param debitTraceNumber
     */
    public void setDebitTraceNumber(java.lang.String debitTraceNumber) {
        this.debitTraceNumber = debitTraceNumber;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof EmvAuthorizeResponse4)) return false;
        EmvAuthorizeResponse4 other = (EmvAuthorizeResponse4) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.approvalStatus==null && other.getApprovalStatus()==null) || 
             (this.approvalStatus!=null &&
              this.approvalStatus.equals(other.getApprovalStatus()))) &&
            ((this.amountApproved==null && other.getAmountApproved()==null) || 
             (this.amountApproved!=null &&
              this.amountApproved.equals(other.getAmountApproved()))) &&
            ((this.authorizationCode==null && other.getAuthorizationCode()==null) || 
             (this.authorizationCode!=null &&
              this.authorizationCode.equals(other.getAuthorizationCode()))) &&
            ((this.errorMessage==null && other.getErrorMessage()==null) || 
             (this.errorMessage!=null &&
              this.errorMessage.equals(other.getErrorMessage()))) &&
            ((this.token==null && other.getToken()==null) || 
             (this.token!=null &&
              this.token.equals(other.getToken()))) &&
            ((this.transactionDate==null && other.getTransactionDate()==null) || 
             (this.transactionDate!=null &&
              this.transactionDate.equals(other.getTransactionDate()))) &&
            ((this.tlvData==null && other.getTlvData()==null) || 
             (this.tlvData!=null &&
              this.tlvData.equals(other.getTlvData()))) &&
            ((this.cashbackAmount==null && other.getCashbackAmount()==null) || 
             (this.cashbackAmount!=null &&
              this.cashbackAmount.equals(other.getCashbackAmount()))) &&
            ((this.donationAmount==null && other.getDonationAmount()==null) || 
             (this.donationAmount!=null &&
              this.donationAmount.equals(other.getDonationAmount()))) &&
            ((this.userTipAmount==null && other.getUserTipAmount()==null) || 
             (this.userTipAmount!=null &&
              this.userTipAmount.equals(other.getUserTipAmount()))) &&
            ((this.surchargeAmount==null && other.getSurchargeAmount()==null) || 
             (this.surchargeAmount!=null &&
              this.surchargeAmount.equals(other.getSurchargeAmount()))) &&
            ((this.debitTraceNumber==null && other.getDebitTraceNumber()==null) || 
             (this.debitTraceNumber!=null &&
              this.debitTraceNumber.equals(other.getDebitTraceNumber())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getApprovalStatus() != null) {
            _hashCode += getApprovalStatus().hashCode();
        }
        if (getAmountApproved() != null) {
            _hashCode += getAmountApproved().hashCode();
        }
        if (getAuthorizationCode() != null) {
            _hashCode += getAuthorizationCode().hashCode();
        }
        if (getErrorMessage() != null) {
            _hashCode += getErrorMessage().hashCode();
        }
        if (getToken() != null) {
            _hashCode += getToken().hashCode();
        }
        if (getTransactionDate() != null) {
            _hashCode += getTransactionDate().hashCode();
        }
        if (getTlvData() != null) {
            _hashCode += getTlvData().hashCode();
        }
        if (getCashbackAmount() != null) {
            _hashCode += getCashbackAmount().hashCode();
        }
        if (getDonationAmount() != null) {
            _hashCode += getDonationAmount().hashCode();
        }
        if (getUserTipAmount() != null) {
            _hashCode += getUserTipAmount().hashCode();
        }
        if (getSurchargeAmount() != null) {
            _hashCode += getSurchargeAmount().hashCode();
        }
        if (getDebitTraceNumber() != null) {
            _hashCode += getDebitTraceNumber().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(EmvAuthorizeResponse4.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/40/Credit/", "EmvAuthorizeResponse4"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("approvalStatus");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/40/Credit/", "ApprovalStatus"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("amountApproved");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/40/Credit/", "AmountApproved"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("authorizationCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/40/Credit/", "AuthorizationCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("errorMessage");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/40/Credit/", "ErrorMessage"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("token");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/40/Credit/", "Token"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transactionDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/40/Credit/", "TransactionDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tlvData");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/40/Credit/", "TlvData"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cashbackAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/40/Credit/", "CashbackAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("donationAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/40/Credit/", "DonationAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("userTipAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/40/Credit/", "UserTipAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("surchargeAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/40/Credit/", "SurchargeAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("debitTraceNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/40/Credit/", "DebitTraceNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
