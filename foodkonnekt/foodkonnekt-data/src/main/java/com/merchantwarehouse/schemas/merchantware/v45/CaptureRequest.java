/**
 * CaptureRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.merchantwarehouse.schemas.merchantware.v45;

public class CaptureRequest  implements java.io.Serializable {
    private java.lang.String token;

    private java.lang.String amount;

    private java.lang.String invoiceNumber;

    private java.lang.String registerNumber;

    private java.lang.String merchantTransactionId;

    private java.lang.String cardAcceptorTerminalId;

    public CaptureRequest() {
    }

    public CaptureRequest(
           java.lang.String token,
           java.lang.String amount,
           java.lang.String invoiceNumber,
           java.lang.String registerNumber,
           java.lang.String merchantTransactionId,
           java.lang.String cardAcceptorTerminalId) {
           this.token = token;
           this.amount = amount;
           this.invoiceNumber = invoiceNumber;
           this.registerNumber = registerNumber;
           this.merchantTransactionId = merchantTransactionId;
           this.cardAcceptorTerminalId = cardAcceptorTerminalId;
    }


    /**
     * Gets the token value for this CaptureRequest.
     * 
     * @return token
     */
    public java.lang.String getToken() {
        return token;
    }


    /**
     * Sets the token value for this CaptureRequest.
     * 
     * @param token
     */
    public void setToken(java.lang.String token) {
        this.token = token;
    }


    /**
     * Gets the amount value for this CaptureRequest.
     * 
     * @return amount
     */
    public java.lang.String getAmount() {
        return amount;
    }


    /**
     * Sets the amount value for this CaptureRequest.
     * 
     * @param amount
     */
    public void setAmount(java.lang.String amount) {
        this.amount = amount;
    }


    /**
     * Gets the invoiceNumber value for this CaptureRequest.
     * 
     * @return invoiceNumber
     */
    public java.lang.String getInvoiceNumber() {
        return invoiceNumber;
    }


    /**
     * Sets the invoiceNumber value for this CaptureRequest.
     * 
     * @param invoiceNumber
     */
    public void setInvoiceNumber(java.lang.String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }


    /**
     * Gets the registerNumber value for this CaptureRequest.
     * 
     * @return registerNumber
     */
    public java.lang.String getRegisterNumber() {
        return registerNumber;
    }


    /**
     * Sets the registerNumber value for this CaptureRequest.
     * 
     * @param registerNumber
     */
    public void setRegisterNumber(java.lang.String registerNumber) {
        this.registerNumber = registerNumber;
    }


    /**
     * Gets the merchantTransactionId value for this CaptureRequest.
     * 
     * @return merchantTransactionId
     */
    public java.lang.String getMerchantTransactionId() {
        return merchantTransactionId;
    }


    /**
     * Sets the merchantTransactionId value for this CaptureRequest.
     * 
     * @param merchantTransactionId
     */
    public void setMerchantTransactionId(java.lang.String merchantTransactionId) {
        this.merchantTransactionId = merchantTransactionId;
    }


    /**
     * Gets the cardAcceptorTerminalId value for this CaptureRequest.
     * 
     * @return cardAcceptorTerminalId
     */
    public java.lang.String getCardAcceptorTerminalId() {
        return cardAcceptorTerminalId;
    }


    /**
     * Sets the cardAcceptorTerminalId value for this CaptureRequest.
     * 
     * @param cardAcceptorTerminalId
     */
    public void setCardAcceptorTerminalId(java.lang.String cardAcceptorTerminalId) {
        this.cardAcceptorTerminalId = cardAcceptorTerminalId;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CaptureRequest)) return false;
        CaptureRequest other = (CaptureRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.token==null && other.getToken()==null) || 
             (this.token!=null &&
              this.token.equals(other.getToken()))) &&
            ((this.amount==null && other.getAmount()==null) || 
             (this.amount!=null &&
              this.amount.equals(other.getAmount()))) &&
            ((this.invoiceNumber==null && other.getInvoiceNumber()==null) || 
             (this.invoiceNumber!=null &&
              this.invoiceNumber.equals(other.getInvoiceNumber()))) &&
            ((this.registerNumber==null && other.getRegisterNumber()==null) || 
             (this.registerNumber!=null &&
              this.registerNumber.equals(other.getRegisterNumber()))) &&
            ((this.merchantTransactionId==null && other.getMerchantTransactionId()==null) || 
             (this.merchantTransactionId!=null &&
              this.merchantTransactionId.equals(other.getMerchantTransactionId()))) &&
            ((this.cardAcceptorTerminalId==null && other.getCardAcceptorTerminalId()==null) || 
             (this.cardAcceptorTerminalId!=null &&
              this.cardAcceptorTerminalId.equals(other.getCardAcceptorTerminalId())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getToken() != null) {
            _hashCode += getToken().hashCode();
        }
        if (getAmount() != null) {
            _hashCode += getAmount().hashCode();
        }
        if (getInvoiceNumber() != null) {
            _hashCode += getInvoiceNumber().hashCode();
        }
        if (getRegisterNumber() != null) {
            _hashCode += getRegisterNumber().hashCode();
        }
        if (getMerchantTransactionId() != null) {
            _hashCode += getMerchantTransactionId().hashCode();
        }
        if (getCardAcceptorTerminalId() != null) {
            _hashCode += getCardAcceptorTerminalId().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CaptureRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "CaptureRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("token");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "Token"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("amount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "Amount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("invoiceNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "InvoiceNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("registerNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "RegisterNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("merchantTransactionId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "MerchantTransactionId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardAcceptorTerminalId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "CardAcceptorTerminalId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
