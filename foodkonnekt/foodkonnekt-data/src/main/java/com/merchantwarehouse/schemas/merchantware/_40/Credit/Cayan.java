package com.merchantwarehouse.schemas.merchantware._40.Credit;

import javax.xml.bind.annotation.XmlRootElement;

public class Cayan {
	
	private String merchantName;
	private String merchantSiteId;
	private String merchantKey;
	private String invoiceNumber;
	private String amount;
	private String cardNumber;
	private String expirationDate;
	
	private String cardholder;
	private String avsStreetAddress;
	private String avsStreetZipCode;
	private String cardSecurityCode;
	private String registerNumber;
	private String merchantTransactionId;
	public String getMerchantName() {
		return merchantName;
	}
	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}
	public String getMerchantSiteId() {
		return merchantSiteId;
	}
	public void setMerchantSiteId(String merchantSiteId) {
		this.merchantSiteId = merchantSiteId;
	}
	public String getMerchantKey() {
		return merchantKey;
	}
	public void setMerchantKey(String merchantKey) {
		this.merchantKey = merchantKey;
	}
	public String getInvoiceNumber() {
		return invoiceNumber;
	}
	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getCardNumber() {
		return cardNumber;
	}
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}
	public String getExpirationDate() {
		return expirationDate;
	}
	public void setExpirationDate(String expirationDate) {
		this.expirationDate = expirationDate;
	}
	public String getCardholder() {
		return cardholder;
	}
	public void setCardholder(String cardholder) {
		this.cardholder = cardholder;
	}
	public String getAvsStreetAddress() {
		return avsStreetAddress;
	}
	public void setAvsStreetAddress(String avsStreetAddress) {
		this.avsStreetAddress = avsStreetAddress;
	}
	public String getAvsStreetZipCode() {
		return avsStreetZipCode;
	}
	public void setAvsStreetZipCode(String avsStreetZipCode) {
		this.avsStreetZipCode = avsStreetZipCode;
	}
	public String getCardSecurityCode() {
		return cardSecurityCode;
	}
	public void setCardSecurityCode(String cardSecurityCode) {
		this.cardSecurityCode = cardSecurityCode;
	}
	public String getRegisterNumber() {
		return registerNumber;
	}
	public void setRegisterNumber(String registerNumber) {
		this.registerNumber = registerNumber;
	}
	public String getMerchantTransactionId() {
		return merchantTransactionId;
	}
	public void setMerchantTransactionId(String merchantTransactionId) {
		this.merchantTransactionId = merchantTransactionId;
	}

}
