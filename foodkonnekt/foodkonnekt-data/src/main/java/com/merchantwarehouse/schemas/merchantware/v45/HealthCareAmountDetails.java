/**
 * HealthCareAmountDetails.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.merchantwarehouse.schemas.merchantware.v45;

public class HealthCareAmountDetails  implements java.io.Serializable {
    private java.lang.String copayAmount;

    private java.lang.String clinicalAmount;

    private java.lang.String dentalAmount;

    private java.lang.String healthCareTotalAmount;

    private java.lang.String prescriptionAmount;

    private java.lang.String visionAmount;

    public HealthCareAmountDetails() {
    }

    public HealthCareAmountDetails(
           java.lang.String copayAmount,
           java.lang.String clinicalAmount,
           java.lang.String dentalAmount,
           java.lang.String healthCareTotalAmount,
           java.lang.String prescriptionAmount,
           java.lang.String visionAmount) {
           this.copayAmount = copayAmount;
           this.clinicalAmount = clinicalAmount;
           this.dentalAmount = dentalAmount;
           this.healthCareTotalAmount = healthCareTotalAmount;
           this.prescriptionAmount = prescriptionAmount;
           this.visionAmount = visionAmount;
    }


    /**
     * Gets the copayAmount value for this HealthCareAmountDetails.
     * 
     * @return copayAmount
     */
    public java.lang.String getCopayAmount() {
        return copayAmount;
    }


    /**
     * Sets the copayAmount value for this HealthCareAmountDetails.
     * 
     * @param copayAmount
     */
    public void setCopayAmount(java.lang.String copayAmount) {
        this.copayAmount = copayAmount;
    }


    /**
     * Gets the clinicalAmount value for this HealthCareAmountDetails.
     * 
     * @return clinicalAmount
     */
    public java.lang.String getClinicalAmount() {
        return clinicalAmount;
    }


    /**
     * Sets the clinicalAmount value for this HealthCareAmountDetails.
     * 
     * @param clinicalAmount
     */
    public void setClinicalAmount(java.lang.String clinicalAmount) {
        this.clinicalAmount = clinicalAmount;
    }


    /**
     * Gets the dentalAmount value for this HealthCareAmountDetails.
     * 
     * @return dentalAmount
     */
    public java.lang.String getDentalAmount() {
        return dentalAmount;
    }


    /**
     * Sets the dentalAmount value for this HealthCareAmountDetails.
     * 
     * @param dentalAmount
     */
    public void setDentalAmount(java.lang.String dentalAmount) {
        this.dentalAmount = dentalAmount;
    }


    /**
     * Gets the healthCareTotalAmount value for this HealthCareAmountDetails.
     * 
     * @return healthCareTotalAmount
     */
    public java.lang.String getHealthCareTotalAmount() {
        return healthCareTotalAmount;
    }


    /**
     * Sets the healthCareTotalAmount value for this HealthCareAmountDetails.
     * 
     * @param healthCareTotalAmount
     */
    public void setHealthCareTotalAmount(java.lang.String healthCareTotalAmount) {
        this.healthCareTotalAmount = healthCareTotalAmount;
    }


    /**
     * Gets the prescriptionAmount value for this HealthCareAmountDetails.
     * 
     * @return prescriptionAmount
     */
    public java.lang.String getPrescriptionAmount() {
        return prescriptionAmount;
    }


    /**
     * Sets the prescriptionAmount value for this HealthCareAmountDetails.
     * 
     * @param prescriptionAmount
     */
    public void setPrescriptionAmount(java.lang.String prescriptionAmount) {
        this.prescriptionAmount = prescriptionAmount;
    }


    /**
     * Gets the visionAmount value for this HealthCareAmountDetails.
     * 
     * @return visionAmount
     */
    public java.lang.String getVisionAmount() {
        return visionAmount;
    }


    /**
     * Sets the visionAmount value for this HealthCareAmountDetails.
     * 
     * @param visionAmount
     */
    public void setVisionAmount(java.lang.String visionAmount) {
        this.visionAmount = visionAmount;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof HealthCareAmountDetails)) return false;
        HealthCareAmountDetails other = (HealthCareAmountDetails) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.copayAmount==null && other.getCopayAmount()==null) || 
             (this.copayAmount!=null &&
              this.copayAmount.equals(other.getCopayAmount()))) &&
            ((this.clinicalAmount==null && other.getClinicalAmount()==null) || 
             (this.clinicalAmount!=null &&
              this.clinicalAmount.equals(other.getClinicalAmount()))) &&
            ((this.dentalAmount==null && other.getDentalAmount()==null) || 
             (this.dentalAmount!=null &&
              this.dentalAmount.equals(other.getDentalAmount()))) &&
            ((this.healthCareTotalAmount==null && other.getHealthCareTotalAmount()==null) || 
             (this.healthCareTotalAmount!=null &&
              this.healthCareTotalAmount.equals(other.getHealthCareTotalAmount()))) &&
            ((this.prescriptionAmount==null && other.getPrescriptionAmount()==null) || 
             (this.prescriptionAmount!=null &&
              this.prescriptionAmount.equals(other.getPrescriptionAmount()))) &&
            ((this.visionAmount==null && other.getVisionAmount()==null) || 
             (this.visionAmount!=null &&
              this.visionAmount.equals(other.getVisionAmount())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCopayAmount() != null) {
            _hashCode += getCopayAmount().hashCode();
        }
        if (getClinicalAmount() != null) {
            _hashCode += getClinicalAmount().hashCode();
        }
        if (getDentalAmount() != null) {
            _hashCode += getDentalAmount().hashCode();
        }
        if (getHealthCareTotalAmount() != null) {
            _hashCode += getHealthCareTotalAmount().hashCode();
        }
        if (getPrescriptionAmount() != null) {
            _hashCode += getPrescriptionAmount().hashCode();
        }
        if (getVisionAmount() != null) {
            _hashCode += getVisionAmount().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(HealthCareAmountDetails.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "HealthCareAmountDetails"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("copayAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "CopayAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("clinicalAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "ClinicalAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dentalAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "DentalAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("healthCareTotalAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "HealthCareTotalAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("prescriptionAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "PrescriptionAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("visionAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "VisionAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
