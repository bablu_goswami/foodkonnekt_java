package com.merchantwarehouse.schemas.merchantware._40.Credit;

import com.foodkonnekt.util.CloudPrintHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CayanTest {
	private static final Logger LOGGER = LoggerFactory.getLogger(CayanTest.class);
	
	/*static{
		System.setProperty("org.apache.commons.logging.LogFactory", 
				"org.apache.commons.logging.impl.LogFactoryImpl");
				System.setProperty("org.apache.commons.logging.Log", 
				"org.apache.commons.logging.impl.Log4JLogger");
	}
	*/
	public static void main(String[] args) {
		try {
			/*CreateTransaction createTransactions = new CreateTransaction();
			createTransactions.setMerchantKey("FY8TN-14F2S-ZCLLC-HWB8A-SCIXP");
			createTransactions.setMerchantName("Test Foodkonnect");
			createTransactions.setMerchantSiteId("JCYYKL8S");
			TransportRequest request =new TransportRequest();
			request.setAmount(new BigDecimal("1.00"));
			request.setTaxAmount(new BigDecimal("1.00"));
			request.setTransactionType("SALE");
			request.setClerkId("ABC123");
			request.setDba("Test Brands");
			request.setOrderNumber("ABC123");
			request.setSoftwareName("mkonnekt");
			request.setSoftwareVersion("1.0.0.0");
			EntryMode  entryMode = new EntryMode("Undefined", false);    //Undefined or Keyed or Swiped or KeyedSwiped
			
			request.setEntryMode(entryMode);
			createTransactions.setRequest(request);
			TransportServiceStub serviceStub = new TransportServiceStub();
			CreateTransactionResponse createTransactionResponse= serviceStub.createTransaction(createTransactions);
			
			
			System.out.println("transport key---"+createTransactionResponse.getCreateTransactionResult().getTransportKey());
			System.out.println("validationKey key---"+createTransactionResponse.getCreateTransactionResult().getValidationKey());*/
			
			
			System.setProperty("org.apache.commons.logging.LogFactory", 
					"org.apache.commons.logging.impl.LogFactoryImpl");
					System.setProperty("org.apache.commons.logging.Log", 
					"org.apache.commons.logging.impl.Log4JLogger");
			
			CreditSoapProxy creditSoapProxy = new CreditSoapProxy();
			
			CreditResponse4 creditResponse4 = creditSoapProxy.preAuthorizationKeyed("Test Foodkonnect", "JCYYKL8S", "FY8TN-14F2S-ZCLLC-HWB8A-SCIXP", "341ABC",
					"1", "4012000033330026", "1218", "daud amb", "1 fast street", "02109", "123", "HUY87H", "166901");
			
			System.out.println(creditResponse4.getAmount());
			System.out.println(creditResponse4.getApprovalStatus());
			System.out.println(creditResponse4.getAuthorizationCode());
			System.out.println(creditResponse4.getAvsResponse());
			System.out.println(creditResponse4.getCardholder());
			System.out.println(creditResponse4.getCardNumber());
			
			System.out.println(creditResponse4.getToken());
			System.out.println();
			System.out.println();
			
			
			
		} catch (Exception e) {
			LOGGER.error("error: " + e.getMessage());
		}
		
		
	}
		
		
	
}
