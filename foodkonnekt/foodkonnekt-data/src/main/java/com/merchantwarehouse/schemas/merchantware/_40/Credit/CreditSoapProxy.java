package com.merchantwarehouse.schemas.merchantware._40.Credit;

public class CreditSoapProxy implements com.merchantwarehouse.schemas.merchantware._40.Credit.CreditSoap {
  private String _endpoint = null;
  private com.merchantwarehouse.schemas.merchantware._40.Credit.CreditSoap creditSoap = null;
  
  public CreditSoapProxy() {
    _initCreditSoapProxy();
  }
  
  public CreditSoapProxy(String endpoint) {
    _endpoint = endpoint;
    _initCreditSoapProxy();
  }
  
  private void _initCreditSoapProxy() {
    try {
      creditSoap = (new com.merchantwarehouse.schemas.merchantware._40.Credit.CreditLocator()).getCreditSoap();
      if (creditSoap != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)creditSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)creditSoap)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (creditSoap != null)
      ((javax.xml.rpc.Stub)creditSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.merchantwarehouse.schemas.merchantware._40.Credit.CreditSoap getCreditSoap() {
    if (creditSoap == null)
      _initCreditSoapProxy();
    return creditSoap;
  }
  
  public com.merchantwarehouse.schemas.merchantware._40.Credit.CreditResponse4 applyTip(java.lang.String merchantName, java.lang.String merchantSiteId, java.lang.String merchantKey, java.lang.String token, java.lang.String tipAmount) throws java.rmi.RemoteException{
    if (creditSoap == null)
      _initCreditSoapProxy();
    return creditSoap.applyTip(merchantName, merchantSiteId, merchantKey, token, tipAmount);
  }
  
  public com.merchantwarehouse.schemas.merchantware._40.Credit.SignatureResponse4 captureSignatureTiff(java.lang.String merchantName, java.lang.String merchantSiteId, java.lang.String merchantKey, java.lang.String token, java.lang.String imageData) throws java.rmi.RemoteException{
    if (creditSoap == null)
      _initCreditSoapProxy();
    return creditSoap.captureSignatureTiff(merchantName, merchantSiteId, merchantKey, token, imageData);
  }
  
  public com.merchantwarehouse.schemas.merchantware._40.Credit.SignatureResponse4 captureSignatureVector(java.lang.String merchantName, java.lang.String merchantSiteId, java.lang.String merchantKey, java.lang.String token, java.lang.String imageData) throws java.rmi.RemoteException{
    if (creditSoap == null)
      _initCreditSoapProxy();
    return creditSoap.captureSignatureVector(merchantName, merchantSiteId, merchantKey, token, imageData);
  }
  
  public com.merchantwarehouse.schemas.merchantware._40.Credit.CreditResponse4 debitSale(java.lang.String merchantName, java.lang.String merchantSiteId, java.lang.String merchantKey, java.lang.String invoiceNumber, java.lang.String amount, java.lang.String trackData, java.lang.String pinBlock, java.lang.String pinKsn, java.lang.String surchargeAmount, java.lang.String cashbackAmount, java.lang.String forceDuplicate, java.lang.String registerNumber) throws java.rmi.RemoteException{
    if (creditSoap == null)
      _initCreditSoapProxy();
    return creditSoap.debitSale(merchantName, merchantSiteId, merchantKey, invoiceNumber, amount, trackData, pinBlock, pinKsn, surchargeAmount, cashbackAmount, forceDuplicate, registerNumber);
  }
  
  public com.merchantwarehouse.schemas.merchantware._40.Credit.CreditResponse4 forceSale(java.lang.String merchantName, java.lang.String merchantSiteId, java.lang.String merchantKey, java.lang.String invoiceNumber, java.lang.String authorizationCode, java.lang.String amount, java.lang.String cardNumber, java.lang.String expirationDate, java.lang.String cardholder, java.lang.String registerNumber, java.lang.String merchantTransactionId) throws java.rmi.RemoteException{
    if (creditSoap == null)
      _initCreditSoapProxy();
    return creditSoap.forceSale(merchantName, merchantSiteId, merchantKey, invoiceNumber, authorizationCode, amount, cardNumber, expirationDate, cardholder, registerNumber, merchantTransactionId);
  }
  
  public com.merchantwarehouse.schemas.merchantware._40.Credit.CreditLevel2Response4 level2Sale(java.lang.String merchantName, java.lang.String merchantSiteId, java.lang.String merchantKey, java.lang.String invoiceNumber, java.lang.String amount, java.lang.String trackData, java.lang.String customerCode, java.lang.String poNumber, java.lang.String taxAmount, java.lang.String forceDuplicate, java.lang.String registerNumber, java.lang.String merchantTransactionId, java.lang.String entryMode) throws java.rmi.RemoteException{
    if (creditSoap == null)
      _initCreditSoapProxy();
    return creditSoap.level2Sale(merchantName, merchantSiteId, merchantKey, invoiceNumber, amount, trackData, customerCode, poNumber, taxAmount, forceDuplicate, registerNumber, merchantTransactionId, entryMode);
  }
  
  public com.merchantwarehouse.schemas.merchantware._40.Credit.CreditLevel2Response4 level2SaleKeyed(java.lang.String merchantName, java.lang.String merchantSiteId, java.lang.String merchantKey, java.lang.String invoiceNumber, java.lang.String amount, java.lang.String cardNumber, java.lang.String expirationDate, java.lang.String cardholder, java.lang.String avsStreetAddress, java.lang.String avsStreetZipCode, java.lang.String cardSecurityCode, java.lang.String customerCode, java.lang.String poNumber, java.lang.String taxAmount, java.lang.String forceDuplicate, java.lang.String registerNumber, java.lang.String merchantTransactionId) throws java.rmi.RemoteException{
    if (creditSoap == null)
      _initCreditSoapProxy();
    return creditSoap.level2SaleKeyed(merchantName, merchantSiteId, merchantKey, invoiceNumber, amount, cardNumber, expirationDate, cardholder, avsStreetAddress, avsStreetZipCode, cardSecurityCode, customerCode, poNumber, taxAmount, forceDuplicate, registerNumber, merchantTransactionId);
  }
  
  public com.merchantwarehouse.schemas.merchantware._40.Credit.CreditResponse4 postAuthorization(java.lang.String merchantName, java.lang.String merchantSiteId, java.lang.String merchantKey, java.lang.String invoiceNumber, java.lang.String token, java.lang.String amount, java.lang.String registerNumber, java.lang.String merchantTransactionId) throws java.rmi.RemoteException{
    if (creditSoap == null)
      _initCreditSoapProxy();
    return creditSoap.postAuthorization(merchantName, merchantSiteId, merchantKey, invoiceNumber, token, amount, registerNumber, merchantTransactionId);
  }
  
  public com.merchantwarehouse.schemas.merchantware._40.Credit.CreditResponse4 preAuthorization(java.lang.String merchantName, java.lang.String merchantSiteId, java.lang.String merchantKey, java.lang.String invoiceNumber, java.lang.String amount, java.lang.String trackData, java.lang.String registerNumber, java.lang.String merchantTransactionId, java.lang.String entryMode) throws java.rmi.RemoteException{
    if (creditSoap == null)
      _initCreditSoapProxy();
    return creditSoap.preAuthorization(merchantName, merchantSiteId, merchantKey, invoiceNumber, amount, trackData, registerNumber, merchantTransactionId, entryMode);
  }
  
  public com.merchantwarehouse.schemas.merchantware._40.Credit.CreditResponse4 preAuthorizationKeyed(java.lang.String merchantName, java.lang.String merchantSiteId, java.lang.String merchantKey, java.lang.String invoiceNumber, java.lang.String amount, java.lang.String cardNumber, java.lang.String expirationDate, java.lang.String cardholder, java.lang.String avsStreetAddress, java.lang.String avsStreetZipCode, java.lang.String cardSecurityCode, java.lang.String registerNumber, java.lang.String merchantTransactionId) throws java.rmi.RemoteException{
    if (creditSoap == null)
      _initCreditSoapProxy();
    return creditSoap.preAuthorizationKeyed(merchantName, merchantSiteId, merchantKey, invoiceNumber, amount, cardNumber, expirationDate, cardholder, avsStreetAddress, avsStreetZipCode, cardSecurityCode, registerNumber, merchantTransactionId);
  }
  
  public com.merchantwarehouse.schemas.merchantware._40.Credit.CreditResponse4 refund(java.lang.String merchantName, java.lang.String merchantSiteId, java.lang.String merchantKey, java.lang.String invoiceNumber, java.lang.String token, java.lang.String overrideAmount, java.lang.String registerNumber, java.lang.String merchantTransactionId) throws java.rmi.RemoteException{
    if (creditSoap == null)
      _initCreditSoapProxy();
    return creditSoap.refund(merchantName, merchantSiteId, merchantKey, invoiceNumber, token, overrideAmount, registerNumber, merchantTransactionId);
  }
  
  public com.merchantwarehouse.schemas.merchantware._40.Credit.CreditResponse4 repeatSale(java.lang.String merchantName, java.lang.String merchantSiteId, java.lang.String merchantKey, java.lang.String invoiceNumber, java.lang.String token, java.lang.String overrideAmount, java.lang.String expirationDate, java.lang.String avsStreetAddress, java.lang.String avsStreetZipCode, java.lang.String registerNumber, java.lang.String merchantTransactionId) throws java.rmi.RemoteException{
    if (creditSoap == null)
      _initCreditSoapProxy();
    return creditSoap.repeatSale(merchantName, merchantSiteId, merchantKey, invoiceNumber, token, overrideAmount, expirationDate, avsStreetAddress, avsStreetZipCode, registerNumber, merchantTransactionId);
  }
  
  public com.merchantwarehouse.schemas.merchantware._40.Credit.CreditResponse4 sale(java.lang.String merchantName, java.lang.String merchantSiteId, java.lang.String merchantKey, java.lang.String invoiceNumber, java.lang.String amount, java.lang.String trackData, java.lang.String forceDuplicate, java.lang.String registerNumber, java.lang.String merchantTransactionId, java.lang.String entryMode) throws java.rmi.RemoteException{
    if (creditSoap == null)
      _initCreditSoapProxy();
    return creditSoap.sale(merchantName, merchantSiteId, merchantKey, invoiceNumber, amount, trackData, forceDuplicate, registerNumber, merchantTransactionId, entryMode);
  }
  
  public com.merchantwarehouse.schemas.merchantware._40.Credit.CreditResponse4 saleKeyed(java.lang.String merchantName, java.lang.String merchantSiteId, java.lang.String merchantKey, java.lang.String invoiceNumber, java.lang.String amount, java.lang.String cardNumber, java.lang.String expirationDate, java.lang.String cardholder, java.lang.String avsStreetAddress, java.lang.String avsStreetZipCode, java.lang.String cardSecurityCode, java.lang.String forceDuplicate, java.lang.String registerNumber, java.lang.String merchantTransactionId) throws java.rmi.RemoteException{
    if (creditSoap == null)
      _initCreditSoapProxy();
    return creditSoap.saleKeyed(merchantName, merchantSiteId, merchantKey, invoiceNumber, amount, cardNumber, expirationDate, cardholder, avsStreetAddress, avsStreetZipCode, cardSecurityCode, forceDuplicate, registerNumber, merchantTransactionId);
  }
  
  public com.merchantwarehouse.schemas.merchantware._40.Credit.BatchResponse4 settleBatch(java.lang.String merchantName, java.lang.String merchantSiteId, java.lang.String merchantKey) throws java.rmi.RemoteException{
    if (creditSoap == null)
      _initCreditSoapProxy();
    return creditSoap.settleBatch(merchantName, merchantSiteId, merchantKey);
  }
  
  public com.merchantwarehouse.schemas.merchantware._40.Credit.CreditResponse4 voidPreAuthorization(java.lang.String merchantName, java.lang.String merchantSiteId, java.lang.String merchantKey, java.lang.String token, java.lang.String registerNumber, java.lang.String merchantTransactionId) throws java.rmi.RemoteException{
    if (creditSoap == null)
      _initCreditSoapProxy();
    return creditSoap.voidPreAuthorization(merchantName, merchantSiteId, merchantKey, token, registerNumber, merchantTransactionId);
  }
  
  public com.merchantwarehouse.schemas.merchantware._40.Credit.CreditResponse4 _void(java.lang.String merchantName, java.lang.String merchantSiteId, java.lang.String merchantKey, java.lang.String token, java.lang.String registerNumber, java.lang.String merchantTransactionId) throws java.rmi.RemoteException{
    if (creditSoap == null)
      _initCreditSoapProxy();
    return creditSoap._void(merchantName, merchantSiteId, merchantKey, token, registerNumber, merchantTransactionId);
  }
  
  public com.merchantwarehouse.schemas.merchantware._40.Credit.CreditLevel2Response4 level2SaleVault(java.lang.String merchantName, java.lang.String merchantSiteId, java.lang.String merchantKey, java.lang.String invoiceNumber, java.lang.String amount, java.lang.String vaultToken, java.lang.String customerCode, java.lang.String poNumber, java.lang.String taxAmount, java.lang.String forceDuplicate, java.lang.String registerNumber, java.lang.String merchantTransactionId) throws java.rmi.RemoteException{
    if (creditSoap == null)
      _initCreditSoapProxy();
    return creditSoap.level2SaleVault(merchantName, merchantSiteId, merchantKey, invoiceNumber, amount, vaultToken, customerCode, poNumber, taxAmount, forceDuplicate, registerNumber, merchantTransactionId);
  }
  
  public com.merchantwarehouse.schemas.merchantware._40.Credit.CreditResponse4 preAuthorizationVault(java.lang.String merchantName, java.lang.String merchantSiteId, java.lang.String merchantKey, java.lang.String invoiceNumber, java.lang.String amount, java.lang.String vaultToken, java.lang.String registerNumber, java.lang.String merchantTransactionId) throws java.rmi.RemoteException{
    if (creditSoap == null)
      _initCreditSoapProxy();
    return creditSoap.preAuthorizationVault(merchantName, merchantSiteId, merchantKey, invoiceNumber, amount, vaultToken, registerNumber, merchantTransactionId);
  }
  
  public com.merchantwarehouse.schemas.merchantware._40.Credit.CreditResponse4 saleVault(java.lang.String merchantName, java.lang.String merchantSiteId, java.lang.String merchantKey, java.lang.String invoiceNumber, java.lang.String amount, java.lang.String vaultToken, java.lang.String forceDuplicate, java.lang.String registerNumber, java.lang.String merchantTransactionId) throws java.rmi.RemoteException{
    if (creditSoap == null)
      _initCreditSoapProxy();
    return creditSoap.saleVault(merchantName, merchantSiteId, merchantKey, invoiceNumber, amount, vaultToken, forceDuplicate, registerNumber, merchantTransactionId);
  }
  
  public com.merchantwarehouse.schemas.merchantware._40.Credit.VaultBoardingResponse vaultBoardCredit(java.lang.String merchantName, java.lang.String merchantSiteId, java.lang.String merchantKey, java.lang.String merchantDefinedToken, java.lang.String trackData, java.lang.String avsStreetAddress, java.lang.String avsStreetZipCode) throws java.rmi.RemoteException{
    if (creditSoap == null)
      _initCreditSoapProxy();
    return creditSoap.vaultBoardCredit(merchantName, merchantSiteId, merchantKey, merchantDefinedToken, trackData, avsStreetAddress, avsStreetZipCode);
  }
  
  public com.merchantwarehouse.schemas.merchantware._40.Credit.VaultBoardingResponse vaultBoardCreditKeyed(java.lang.String merchantName, java.lang.String merchantSiteId, java.lang.String merchantKey, java.lang.String merchantDefinedToken, java.lang.String cardNumber, java.lang.String expirationDate, java.lang.String cardholder, java.lang.String avsStreetAddress, java.lang.String avsStreetZipCode) throws java.rmi.RemoteException{
    if (creditSoap == null)
      _initCreditSoapProxy();
    return creditSoap.vaultBoardCreditKeyed(merchantName, merchantSiteId, merchantKey, merchantDefinedToken, cardNumber, expirationDate, cardholder, avsStreetAddress, avsStreetZipCode);
  }
  
  public com.merchantwarehouse.schemas.merchantware._40.Credit.VaultBoardingResponse vaultBoardCreditByReference(java.lang.String merchantName, java.lang.String merchantSiteId, java.lang.String merchantKey, java.lang.String merchantDefinedToken, java.lang.String referenceNumber) throws java.rmi.RemoteException{
    if (creditSoap == null)
      _initCreditSoapProxy();
    return creditSoap.vaultBoardCreditByReference(merchantName, merchantSiteId, merchantKey, merchantDefinedToken, referenceNumber);
  }
  
  public com.merchantwarehouse.schemas.merchantware._40.Credit.VaultBoardingResponse vaultDeleteToken(java.lang.String merchantName, java.lang.String merchantSiteId, java.lang.String merchantKey, java.lang.String vaultToken) throws java.rmi.RemoteException{
    if (creditSoap == null)
      _initCreditSoapProxy();
    return creditSoap.vaultDeleteToken(merchantName, merchantSiteId, merchantKey, vaultToken);
  }
  
  public com.merchantwarehouse.schemas.merchantware._40.Credit.VaultPaymentInfoResponse vaultFindPaymentInfo(java.lang.String merchantName, java.lang.String merchantSiteId, java.lang.String merchantKey, java.lang.String vaultToken) throws java.rmi.RemoteException{
    if (creditSoap == null)
      _initCreditSoapProxy();
    return creditSoap.vaultFindPaymentInfo(merchantName, merchantSiteId, merchantKey, vaultToken);
  }
  
  public com.merchantwarehouse.schemas.merchantware._40.Credit.EmvAuthorizeResponse4 emvAuthorize(java.lang.String merchantName, java.lang.String merchantSiteId, java.lang.String merchantKey, java.lang.String invoiceNumber, java.lang.String amount, java.lang.String customerCode, java.lang.String poNumber, java.lang.String taxAmount, java.lang.String registerNumber, java.lang.String merchantTransactionId, java.lang.String entryMode, java.lang.String messageCorrelationID, java.lang.String cardAcceptorTerminalID, java.lang.String pinBlock, java.lang.String pinKsn, java.lang.String cashbackAmount, java.lang.String donationAmount, java.lang.String userTipAmount, java.lang.String surchargeAmount, java.lang.String kernelSoftwareVersion, java.lang.String tlvData, java.lang.String transactionType) throws java.rmi.RemoteException{
    if (creditSoap == null)
      _initCreditSoapProxy();
    return creditSoap.emvAuthorize(merchantName, merchantSiteId, merchantKey, invoiceNumber, amount, customerCode, poNumber, taxAmount, registerNumber, merchantTransactionId, entryMode, messageCorrelationID, cardAcceptorTerminalID, pinBlock, pinKsn, cashbackAmount, donationAmount, userTipAmount, surchargeAmount, kernelSoftwareVersion, tlvData, transactionType);
  }
  
  public com.merchantwarehouse.schemas.merchantware._40.Credit.EmvCompleteResponse4 emvComplete(java.lang.String merchantName, java.lang.String merchantSiteId, java.lang.String merchantKey, java.lang.String invoiceNumber, java.lang.String amount, java.lang.String customerCode, java.lang.String poNumber, java.lang.String taxAmount, java.lang.String registerNumber, java.lang.String token, java.lang.String merchantTransactionId, java.lang.String entryMode, java.lang.String messageCorrelationID, java.lang.String cardAcceptorTerminalID, java.lang.String pinBlock, java.lang.String pinKsn, java.lang.String cashbackAmount, java.lang.String donationAmount, java.lang.String userTipAmount, java.lang.String surchargeAmount, java.lang.String kernelSoftwareVersion, java.lang.String tlvData, java.lang.String transactionType) throws java.rmi.RemoteException{
    if (creditSoap == null)
      _initCreditSoapProxy();
    return creditSoap.emvComplete(merchantName, merchantSiteId, merchantKey, invoiceNumber, amount, customerCode, poNumber, taxAmount, registerNumber, token, merchantTransactionId, entryMode, messageCorrelationID, cardAcceptorTerminalID, pinBlock, pinKsn, cashbackAmount, donationAmount, userTipAmount, surchargeAmount, kernelSoftwareVersion, tlvData, transactionType);
  }
  
  
}