/**
 * VaultTokenResponse45.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.merchantwarehouse.schemas.merchantware.v45;

public class VaultTokenResponse45  implements java.io.Serializable {
    private java.lang.String cardNumber;

    private java.lang.String expirationDate;

    private java.lang.String cardholder;

    private java.lang.String cardType;

    private java.lang.String avsStreetAddress;

    private java.lang.String avsZipCode;

    private java.lang.String errorCode;

    private java.lang.String errorMessage;

    private java.lang.String rfmiq;

    public VaultTokenResponse45() {
    }

    public VaultTokenResponse45(
           java.lang.String cardNumber,
           java.lang.String expirationDate,
           java.lang.String cardholder,
           java.lang.String cardType,
           java.lang.String avsStreetAddress,
           java.lang.String avsZipCode,
           java.lang.String errorCode,
           java.lang.String errorMessage,
           java.lang.String rfmiq) {
           this.cardNumber = cardNumber;
           this.expirationDate = expirationDate;
           this.cardholder = cardholder;
           this.cardType = cardType;
           this.avsStreetAddress = avsStreetAddress;
           this.avsZipCode = avsZipCode;
           this.errorCode = errorCode;
           this.errorMessage = errorMessage;
           this.rfmiq = rfmiq;
    }


    /**
     * Gets the cardNumber value for this VaultTokenResponse45.
     * 
     * @return cardNumber
     */
    public java.lang.String getCardNumber() {
        return cardNumber;
    }


    /**
     * Sets the cardNumber value for this VaultTokenResponse45.
     * 
     * @param cardNumber
     */
    public void setCardNumber(java.lang.String cardNumber) {
        this.cardNumber = cardNumber;
    }


    /**
     * Gets the expirationDate value for this VaultTokenResponse45.
     * 
     * @return expirationDate
     */
    public java.lang.String getExpirationDate() {
        return expirationDate;
    }


    /**
     * Sets the expirationDate value for this VaultTokenResponse45.
     * 
     * @param expirationDate
     */
    public void setExpirationDate(java.lang.String expirationDate) {
        this.expirationDate = expirationDate;
    }


    /**
     * Gets the cardholder value for this VaultTokenResponse45.
     * 
     * @return cardholder
     */
    public java.lang.String getCardholder() {
        return cardholder;
    }


    /**
     * Sets the cardholder value for this VaultTokenResponse45.
     * 
     * @param cardholder
     */
    public void setCardholder(java.lang.String cardholder) {
        this.cardholder = cardholder;
    }


    /**
     * Gets the cardType value for this VaultTokenResponse45.
     * 
     * @return cardType
     */
    public java.lang.String getCardType() {
        return cardType;
    }


    /**
     * Sets the cardType value for this VaultTokenResponse45.
     * 
     * @param cardType
     */
    public void setCardType(java.lang.String cardType) {
        this.cardType = cardType;
    }


    /**
     * Gets the avsStreetAddress value for this VaultTokenResponse45.
     * 
     * @return avsStreetAddress
     */
    public java.lang.String getAvsStreetAddress() {
        return avsStreetAddress;
    }


    /**
     * Sets the avsStreetAddress value for this VaultTokenResponse45.
     * 
     * @param avsStreetAddress
     */
    public void setAvsStreetAddress(java.lang.String avsStreetAddress) {
        this.avsStreetAddress = avsStreetAddress;
    }


    /**
     * Gets the avsZipCode value for this VaultTokenResponse45.
     * 
     * @return avsZipCode
     */
    public java.lang.String getAvsZipCode() {
        return avsZipCode;
    }


    /**
     * Sets the avsZipCode value for this VaultTokenResponse45.
     * 
     * @param avsZipCode
     */
    public void setAvsZipCode(java.lang.String avsZipCode) {
        this.avsZipCode = avsZipCode;
    }


    /**
     * Gets the errorCode value for this VaultTokenResponse45.
     * 
     * @return errorCode
     */
    public java.lang.String getErrorCode() {
        return errorCode;
    }


    /**
     * Sets the errorCode value for this VaultTokenResponse45.
     * 
     * @param errorCode
     */
    public void setErrorCode(java.lang.String errorCode) {
        this.errorCode = errorCode;
    }


    /**
     * Gets the errorMessage value for this VaultTokenResponse45.
     * 
     * @return errorMessage
     */
    public java.lang.String getErrorMessage() {
        return errorMessage;
    }


    /**
     * Sets the errorMessage value for this VaultTokenResponse45.
     * 
     * @param errorMessage
     */
    public void setErrorMessage(java.lang.String errorMessage) {
        this.errorMessage = errorMessage;
    }


    /**
     * Gets the rfmiq value for this VaultTokenResponse45.
     * 
     * @return rfmiq
     */
    public java.lang.String getRfmiq() {
        return rfmiq;
    }


    /**
     * Sets the rfmiq value for this VaultTokenResponse45.
     * 
     * @param rfmiq
     */
    public void setRfmiq(java.lang.String rfmiq) {
        this.rfmiq = rfmiq;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof VaultTokenResponse45)) return false;
        VaultTokenResponse45 other = (VaultTokenResponse45) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.cardNumber==null && other.getCardNumber()==null) || 
             (this.cardNumber!=null &&
              this.cardNumber.equals(other.getCardNumber()))) &&
            ((this.expirationDate==null && other.getExpirationDate()==null) || 
             (this.expirationDate!=null &&
              this.expirationDate.equals(other.getExpirationDate()))) &&
            ((this.cardholder==null && other.getCardholder()==null) || 
             (this.cardholder!=null &&
              this.cardholder.equals(other.getCardholder()))) &&
            ((this.cardType==null && other.getCardType()==null) || 
             (this.cardType!=null &&
              this.cardType.equals(other.getCardType()))) &&
            ((this.avsStreetAddress==null && other.getAvsStreetAddress()==null) || 
             (this.avsStreetAddress!=null &&
              this.avsStreetAddress.equals(other.getAvsStreetAddress()))) &&
            ((this.avsZipCode==null && other.getAvsZipCode()==null) || 
             (this.avsZipCode!=null &&
              this.avsZipCode.equals(other.getAvsZipCode()))) &&
            ((this.errorCode==null && other.getErrorCode()==null) || 
             (this.errorCode!=null &&
              this.errorCode.equals(other.getErrorCode()))) &&
            ((this.errorMessage==null && other.getErrorMessage()==null) || 
             (this.errorMessage!=null &&
              this.errorMessage.equals(other.getErrorMessage()))) &&
            ((this.rfmiq==null && other.getRfmiq()==null) || 
             (this.rfmiq!=null &&
              this.rfmiq.equals(other.getRfmiq())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCardNumber() != null) {
            _hashCode += getCardNumber().hashCode();
        }
        if (getExpirationDate() != null) {
            _hashCode += getExpirationDate().hashCode();
        }
        if (getCardholder() != null) {
            _hashCode += getCardholder().hashCode();
        }
        if (getCardType() != null) {
            _hashCode += getCardType().hashCode();
        }
        if (getAvsStreetAddress() != null) {
            _hashCode += getAvsStreetAddress().hashCode();
        }
        if (getAvsZipCode() != null) {
            _hashCode += getAvsZipCode().hashCode();
        }
        if (getErrorCode() != null) {
            _hashCode += getErrorCode().hashCode();
        }
        if (getErrorMessage() != null) {
            _hashCode += getErrorMessage().hashCode();
        }
        if (getRfmiq() != null) {
            _hashCode += getRfmiq().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(VaultTokenResponse45.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "VaultTokenResponse45"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "CardNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("expirationDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "ExpirationDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardholder");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "Cardholder"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "CardType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("avsStreetAddress");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "AvsStreetAddress"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("avsZipCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "AvsZipCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("errorCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "ErrorCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("errorMessage");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "ErrorMessage"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rfmiq");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "Rfmiq"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
