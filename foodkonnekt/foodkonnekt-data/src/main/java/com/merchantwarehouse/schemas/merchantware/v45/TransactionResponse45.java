/**
 * TransactionResponse45.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.merchantwarehouse.schemas.merchantware.v45;

public class TransactionResponse45  implements java.io.Serializable {
    private java.lang.String approvalStatus;

    private java.lang.String token;

    private java.lang.String authorizationCode;

    private java.lang.String transactionDate;

    private java.lang.String amount;

    private java.lang.String remainingCardBalance;

    private java.lang.String cardNumber;

    private java.lang.String cardholder;

    private java.lang.String cardType;

    private java.lang.String fsaCard;

    private java.lang.String readerEntryMode;

    private java.lang.String avsResponse;

    private java.lang.String cvResponse;

    private java.lang.String errorMessage;

    private java.lang.String extraData;

    private com.merchantwarehouse.schemas.merchantware.v45.FraudScoring fraudScoring;

    private java.lang.String rfmiq;

    private java.lang.String debitTraceNumber;

    private com.merchantwarehouse.schemas.merchantware.v45.Invoice invoice;

    public TransactionResponse45() {
    }

    public TransactionResponse45(
           java.lang.String approvalStatus,
           java.lang.String token,
           java.lang.String authorizationCode,
           java.lang.String transactionDate,
           java.lang.String amount,
           java.lang.String remainingCardBalance,
           java.lang.String cardNumber,
           java.lang.String cardholder,
           java.lang.String cardType,
           java.lang.String fsaCard,
           java.lang.String readerEntryMode,
           java.lang.String avsResponse,
           java.lang.String cvResponse,
           java.lang.String errorMessage,
           java.lang.String extraData,
           com.merchantwarehouse.schemas.merchantware.v45.FraudScoring fraudScoring,
           java.lang.String rfmiq,
           java.lang.String debitTraceNumber,
           com.merchantwarehouse.schemas.merchantware.v45.Invoice invoice) {
           this.approvalStatus = approvalStatus;
           this.token = token;
           this.authorizationCode = authorizationCode;
           this.transactionDate = transactionDate;
           this.amount = amount;
           this.remainingCardBalance = remainingCardBalance;
           this.cardNumber = cardNumber;
           this.cardholder = cardholder;
           this.cardType = cardType;
           this.fsaCard = fsaCard;
           this.readerEntryMode = readerEntryMode;
           this.avsResponse = avsResponse;
           this.cvResponse = cvResponse;
           this.errorMessage = errorMessage;
           this.extraData = extraData;
           this.fraudScoring = fraudScoring;
           this.rfmiq = rfmiq;
           this.debitTraceNumber = debitTraceNumber;
           this.invoice = invoice;
    }


    /**
     * Gets the approvalStatus value for this TransactionResponse45.
     * 
     * @return approvalStatus
     */
    public java.lang.String getApprovalStatus() {
        return approvalStatus;
    }


    /**
     * Sets the approvalStatus value for this TransactionResponse45.
     * 
     * @param approvalStatus
     */
    public void setApprovalStatus(java.lang.String approvalStatus) {
        this.approvalStatus = approvalStatus;
    }


    /**
     * Gets the token value for this TransactionResponse45.
     * 
     * @return token
     */
    public java.lang.String getToken() {
        return token;
    }


    /**
     * Sets the token value for this TransactionResponse45.
     * 
     * @param token
     */
    public void setToken(java.lang.String token) {
        this.token = token;
    }


    /**
     * Gets the authorizationCode value for this TransactionResponse45.
     * 
     * @return authorizationCode
     */
    public java.lang.String getAuthorizationCode() {
        return authorizationCode;
    }


    /**
     * Sets the authorizationCode value for this TransactionResponse45.
     * 
     * @param authorizationCode
     */
    public void setAuthorizationCode(java.lang.String authorizationCode) {
        this.authorizationCode = authorizationCode;
    }


    /**
     * Gets the transactionDate value for this TransactionResponse45.
     * 
     * @return transactionDate
     */
    public java.lang.String getTransactionDate() {
        return transactionDate;
    }


    /**
     * Sets the transactionDate value for this TransactionResponse45.
     * 
     * @param transactionDate
     */
    public void setTransactionDate(java.lang.String transactionDate) {
        this.transactionDate = transactionDate;
    }


    /**
     * Gets the amount value for this TransactionResponse45.
     * 
     * @return amount
     */
    public java.lang.String getAmount() {
        return amount;
    }


    /**
     * Sets the amount value for this TransactionResponse45.
     * 
     * @param amount
     */
    public void setAmount(java.lang.String amount) {
        this.amount = amount;
    }


    /**
     * Gets the remainingCardBalance value for this TransactionResponse45.
     * 
     * @return remainingCardBalance
     */
    public java.lang.String getRemainingCardBalance() {
        return remainingCardBalance;
    }


    /**
     * Sets the remainingCardBalance value for this TransactionResponse45.
     * 
     * @param remainingCardBalance
     */
    public void setRemainingCardBalance(java.lang.String remainingCardBalance) {
        this.remainingCardBalance = remainingCardBalance;
    }


    /**
     * Gets the cardNumber value for this TransactionResponse45.
     * 
     * @return cardNumber
     */
    public java.lang.String getCardNumber() {
        return cardNumber;
    }


    /**
     * Sets the cardNumber value for this TransactionResponse45.
     * 
     * @param cardNumber
     */
    public void setCardNumber(java.lang.String cardNumber) {
        this.cardNumber = cardNumber;
    }


    /**
     * Gets the cardholder value for this TransactionResponse45.
     * 
     * @return cardholder
     */
    public java.lang.String getCardholder() {
        return cardholder;
    }


    /**
     * Sets the cardholder value for this TransactionResponse45.
     * 
     * @param cardholder
     */
    public void setCardholder(java.lang.String cardholder) {
        this.cardholder = cardholder;
    }


    /**
     * Gets the cardType value for this TransactionResponse45.
     * 
     * @return cardType
     */
    public java.lang.String getCardType() {
        return cardType;
    }


    /**
     * Sets the cardType value for this TransactionResponse45.
     * 
     * @param cardType
     */
    public void setCardType(java.lang.String cardType) {
        this.cardType = cardType;
    }


    /**
     * Gets the fsaCard value for this TransactionResponse45.
     * 
     * @return fsaCard
     */
    public java.lang.String getFsaCard() {
        return fsaCard;
    }


    /**
     * Sets the fsaCard value for this TransactionResponse45.
     * 
     * @param fsaCard
     */
    public void setFsaCard(java.lang.String fsaCard) {
        this.fsaCard = fsaCard;
    }


    /**
     * Gets the readerEntryMode value for this TransactionResponse45.
     * 
     * @return readerEntryMode
     */
    public java.lang.String getReaderEntryMode() {
        return readerEntryMode;
    }


    /**
     * Sets the readerEntryMode value for this TransactionResponse45.
     * 
     * @param readerEntryMode
     */
    public void setReaderEntryMode(java.lang.String readerEntryMode) {
        this.readerEntryMode = readerEntryMode;
    }


    /**
     * Gets the avsResponse value for this TransactionResponse45.
     * 
     * @return avsResponse
     */
    public java.lang.String getAvsResponse() {
        return avsResponse;
    }


    /**
     * Sets the avsResponse value for this TransactionResponse45.
     * 
     * @param avsResponse
     */
    public void setAvsResponse(java.lang.String avsResponse) {
        this.avsResponse = avsResponse;
    }


    /**
     * Gets the cvResponse value for this TransactionResponse45.
     * 
     * @return cvResponse
     */
    public java.lang.String getCvResponse() {
        return cvResponse;
    }


    /**
     * Sets the cvResponse value for this TransactionResponse45.
     * 
     * @param cvResponse
     */
    public void setCvResponse(java.lang.String cvResponse) {
        this.cvResponse = cvResponse;
    }


    /**
     * Gets the errorMessage value for this TransactionResponse45.
     * 
     * @return errorMessage
     */
    public java.lang.String getErrorMessage() {
        return errorMessage;
    }


    /**
     * Sets the errorMessage value for this TransactionResponse45.
     * 
     * @param errorMessage
     */
    public void setErrorMessage(java.lang.String errorMessage) {
        this.errorMessage = errorMessage;
    }


    /**
     * Gets the extraData value for this TransactionResponse45.
     * 
     * @return extraData
     */
    public java.lang.String getExtraData() {
        return extraData;
    }


    /**
     * Sets the extraData value for this TransactionResponse45.
     * 
     * @param extraData
     */
    public void setExtraData(java.lang.String extraData) {
        this.extraData = extraData;
    }


    /**
     * Gets the fraudScoring value for this TransactionResponse45.
     * 
     * @return fraudScoring
     */
    public com.merchantwarehouse.schemas.merchantware.v45.FraudScoring getFraudScoring() {
        return fraudScoring;
    }


    /**
     * Sets the fraudScoring value for this TransactionResponse45.
     * 
     * @param fraudScoring
     */
    public void setFraudScoring(com.merchantwarehouse.schemas.merchantware.v45.FraudScoring fraudScoring) {
        this.fraudScoring = fraudScoring;
    }


    /**
     * Gets the rfmiq value for this TransactionResponse45.
     * 
     * @return rfmiq
     */
    public java.lang.String getRfmiq() {
        return rfmiq;
    }


    /**
     * Sets the rfmiq value for this TransactionResponse45.
     * 
     * @param rfmiq
     */
    public void setRfmiq(java.lang.String rfmiq) {
        this.rfmiq = rfmiq;
    }


    /**
     * Gets the debitTraceNumber value for this TransactionResponse45.
     * 
     * @return debitTraceNumber
     */
    public java.lang.String getDebitTraceNumber() {
        return debitTraceNumber;
    }


    /**
     * Sets the debitTraceNumber value for this TransactionResponse45.
     * 
     * @param debitTraceNumber
     */
    public void setDebitTraceNumber(java.lang.String debitTraceNumber) {
        this.debitTraceNumber = debitTraceNumber;
    }


    /**
     * Gets the invoice value for this TransactionResponse45.
     * 
     * @return invoice
     */
    public com.merchantwarehouse.schemas.merchantware.v45.Invoice getInvoice() {
        return invoice;
    }


    /**
     * Sets the invoice value for this TransactionResponse45.
     * 
     * @param invoice
     */
    public void setInvoice(com.merchantwarehouse.schemas.merchantware.v45.Invoice invoice) {
        this.invoice = invoice;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TransactionResponse45)) return false;
        TransactionResponse45 other = (TransactionResponse45) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.approvalStatus==null && other.getApprovalStatus()==null) || 
             (this.approvalStatus!=null &&
              this.approvalStatus.equals(other.getApprovalStatus()))) &&
            ((this.token==null && other.getToken()==null) || 
             (this.token!=null &&
              this.token.equals(other.getToken()))) &&
            ((this.authorizationCode==null && other.getAuthorizationCode()==null) || 
             (this.authorizationCode!=null &&
              this.authorizationCode.equals(other.getAuthorizationCode()))) &&
            ((this.transactionDate==null && other.getTransactionDate()==null) || 
             (this.transactionDate!=null &&
              this.transactionDate.equals(other.getTransactionDate()))) &&
            ((this.amount==null && other.getAmount()==null) || 
             (this.amount!=null &&
              this.amount.equals(other.getAmount()))) &&
            ((this.remainingCardBalance==null && other.getRemainingCardBalance()==null) || 
             (this.remainingCardBalance!=null &&
              this.remainingCardBalance.equals(other.getRemainingCardBalance()))) &&
            ((this.cardNumber==null && other.getCardNumber()==null) || 
             (this.cardNumber!=null &&
              this.cardNumber.equals(other.getCardNumber()))) &&
            ((this.cardholder==null && other.getCardholder()==null) || 
             (this.cardholder!=null &&
              this.cardholder.equals(other.getCardholder()))) &&
            ((this.cardType==null && other.getCardType()==null) || 
             (this.cardType!=null &&
              this.cardType.equals(other.getCardType()))) &&
            ((this.fsaCard==null && other.getFsaCard()==null) || 
             (this.fsaCard!=null &&
              this.fsaCard.equals(other.getFsaCard()))) &&
            ((this.readerEntryMode==null && other.getReaderEntryMode()==null) || 
             (this.readerEntryMode!=null &&
              this.readerEntryMode.equals(other.getReaderEntryMode()))) &&
            ((this.avsResponse==null && other.getAvsResponse()==null) || 
             (this.avsResponse!=null &&
              this.avsResponse.equals(other.getAvsResponse()))) &&
            ((this.cvResponse==null && other.getCvResponse()==null) || 
             (this.cvResponse!=null &&
              this.cvResponse.equals(other.getCvResponse()))) &&
            ((this.errorMessage==null && other.getErrorMessage()==null) || 
             (this.errorMessage!=null &&
              this.errorMessage.equals(other.getErrorMessage()))) &&
            ((this.extraData==null && other.getExtraData()==null) || 
             (this.extraData!=null &&
              this.extraData.equals(other.getExtraData()))) &&
            ((this.fraudScoring==null && other.getFraudScoring()==null) || 
             (this.fraudScoring!=null &&
              this.fraudScoring.equals(other.getFraudScoring()))) &&
            ((this.rfmiq==null && other.getRfmiq()==null) || 
             (this.rfmiq!=null &&
              this.rfmiq.equals(other.getRfmiq()))) &&
            ((this.debitTraceNumber==null && other.getDebitTraceNumber()==null) || 
             (this.debitTraceNumber!=null &&
              this.debitTraceNumber.equals(other.getDebitTraceNumber()))) &&
            ((this.invoice==null && other.getInvoice()==null) || 
             (this.invoice!=null &&
              this.invoice.equals(other.getInvoice())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getApprovalStatus() != null) {
            _hashCode += getApprovalStatus().hashCode();
        }
        if (getToken() != null) {
            _hashCode += getToken().hashCode();
        }
        if (getAuthorizationCode() != null) {
            _hashCode += getAuthorizationCode().hashCode();
        }
        if (getTransactionDate() != null) {
            _hashCode += getTransactionDate().hashCode();
        }
        if (getAmount() != null) {
            _hashCode += getAmount().hashCode();
        }
        if (getRemainingCardBalance() != null) {
            _hashCode += getRemainingCardBalance().hashCode();
        }
        if (getCardNumber() != null) {
            _hashCode += getCardNumber().hashCode();
        }
        if (getCardholder() != null) {
            _hashCode += getCardholder().hashCode();
        }
        if (getCardType() != null) {
            _hashCode += getCardType().hashCode();
        }
        if (getFsaCard() != null) {
            _hashCode += getFsaCard().hashCode();
        }
        if (getReaderEntryMode() != null) {
            _hashCode += getReaderEntryMode().hashCode();
        }
        if (getAvsResponse() != null) {
            _hashCode += getAvsResponse().hashCode();
        }
        if (getCvResponse() != null) {
            _hashCode += getCvResponse().hashCode();
        }
        if (getErrorMessage() != null) {
            _hashCode += getErrorMessage().hashCode();
        }
        if (getExtraData() != null) {
            _hashCode += getExtraData().hashCode();
        }
        if (getFraudScoring() != null) {
            _hashCode += getFraudScoring().hashCode();
        }
        if (getRfmiq() != null) {
            _hashCode += getRfmiq().hashCode();
        }
        if (getDebitTraceNumber() != null) {
            _hashCode += getDebitTraceNumber().hashCode();
        }
        if (getInvoice() != null) {
            _hashCode += getInvoice().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TransactionResponse45.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "TransactionResponse45"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("approvalStatus");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "ApprovalStatus"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("token");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "Token"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("authorizationCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "AuthorizationCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transactionDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "TransactionDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("amount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "Amount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("remainingCardBalance");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "RemainingCardBalance"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "CardNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardholder");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "Cardholder"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "CardType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fsaCard");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "FsaCard"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("readerEntryMode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "ReaderEntryMode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("avsResponse");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "AvsResponse"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cvResponse");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "CvResponse"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("errorMessage");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "ErrorMessage"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("extraData");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "ExtraData"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fraudScoring");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "FraudScoring"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "FraudScoring"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rfmiq");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "Rfmiq"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("debitTraceNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "DebitTraceNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("invoice");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "Invoice"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "Invoice"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
