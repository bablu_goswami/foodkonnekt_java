/**
 * CreditLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.merchantwarehouse.schemas.merchantware.v45;

public class CreditLocator extends org.apache.axis.client.Service implements com.merchantwarehouse.schemas.merchantware.v45.Credit {

/**
 * Provides payment, processing, and related services for both credit
 * and debit cards.
 */

    public CreditLocator() {
    }


    public CreditLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public CreditLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for CreditSoap
    private java.lang.String CreditSoap_address = "https://ps1.merchantware.net/Merchantware/ws/RetailTransaction/v45/Credit.asmx";

    public java.lang.String getCreditSoapAddress() {
        return CreditSoap_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String CreditSoapWSDDServiceName = "CreditSoap";

    public java.lang.String getCreditSoapWSDDServiceName() {
        return CreditSoapWSDDServiceName;
    }

    public void setCreditSoapWSDDServiceName(java.lang.String name) {
        CreditSoapWSDDServiceName = name;
    }

    public com.merchantwarehouse.schemas.merchantware.v45.CreditSoap getCreditSoap() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(CreditSoap_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getCreditSoap(endpoint);
    }

    public com.merchantwarehouse.schemas.merchantware.v45.CreditSoap getCreditSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.merchantwarehouse.schemas.merchantware.v45.CreditSoapStub _stub = new com.merchantwarehouse.schemas.merchantware.v45.CreditSoapStub(portAddress, this);
            _stub.setPortName(getCreditSoapWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setCreditSoapEndpointAddress(java.lang.String address) {
        CreditSoap_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.merchantwarehouse.schemas.merchantware.v45.CreditSoap.class.isAssignableFrom(serviceEndpointInterface)) {
                com.merchantwarehouse.schemas.merchantware.v45.CreditSoapStub _stub = new com.merchantwarehouse.schemas.merchantware.v45.CreditSoapStub(new java.net.URL(CreditSoap_address), this);
                _stub.setPortName(getCreditSoapWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("CreditSoap".equals(inputPortName)) {
            return getCreditSoap();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "Credit");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "CreditSoap"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("CreditSoap".equals(portName)) {
            setCreditSoapEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
