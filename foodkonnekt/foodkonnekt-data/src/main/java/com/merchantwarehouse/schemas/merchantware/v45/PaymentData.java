/**
 * PaymentData.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.merchantwarehouse.schemas.merchantware.v45;

public class PaymentData  implements java.io.Serializable {
    private java.lang.String source;

    private java.lang.String trackData;

    private java.lang.String pinBlock;

    private java.lang.String pinKsn;

    private java.lang.String readerEntryMode;

    private java.lang.String cardNumber;

    private java.lang.String expirationDate;

    private java.lang.String cardHolder;

    private java.lang.String avsStreetAddress;

    private java.lang.String avsZipCode;

    private java.lang.String cardVerificationValue;

    private java.lang.String token;

    private java.lang.String vaultToken;

    private java.lang.String customerEmailAddress;

    private java.lang.String walletId;

    private java.lang.String encryptedKeyedData;

    private java.lang.String paymentCryptogram;

    private java.lang.String paymentCryptogramType;

    private java.lang.String encryptedPaymentData;

    private java.lang.String eciIndicator;

    public PaymentData() {
    }

    public PaymentData(
           java.lang.String source,
           java.lang.String trackData,
           java.lang.String pinBlock,
           java.lang.String pinKsn,
           java.lang.String readerEntryMode,
           java.lang.String cardNumber,
           java.lang.String expirationDate,
           java.lang.String cardHolder,
           java.lang.String avsStreetAddress,
           java.lang.String avsZipCode,
           java.lang.String cardVerificationValue,
           java.lang.String token,
           java.lang.String vaultToken,
           java.lang.String customerEmailAddress,
           java.lang.String walletId,
           java.lang.String encryptedKeyedData,
           java.lang.String paymentCryptogram,
           java.lang.String paymentCryptogramType,
           java.lang.String encryptedPaymentData,
           java.lang.String eciIndicator) {
           this.source = source;
           this.trackData = trackData;
           this.pinBlock = pinBlock;
           this.pinKsn = pinKsn;
           this.readerEntryMode = readerEntryMode;
           this.cardNumber = cardNumber;
           this.expirationDate = expirationDate;
           this.cardHolder = cardHolder;
           this.avsStreetAddress = avsStreetAddress;
           this.avsZipCode = avsZipCode;
           this.cardVerificationValue = cardVerificationValue;
           this.token = token;
           this.vaultToken = vaultToken;
           this.customerEmailAddress = customerEmailAddress;
           this.walletId = walletId;
           this.encryptedKeyedData = encryptedKeyedData;
           this.paymentCryptogram = paymentCryptogram;
           this.paymentCryptogramType = paymentCryptogramType;
           this.encryptedPaymentData = encryptedPaymentData;
           this.eciIndicator = eciIndicator;
    }


    /**
     * Gets the source value for this PaymentData.
     * 
     * @return source
     */
    public java.lang.String getSource() {
        return source;
    }


    /**
     * Sets the source value for this PaymentData.
     * 
     * @param source
     */
    public void setSource(java.lang.String source) {
        this.source = source;
    }


    /**
     * Gets the trackData value for this PaymentData.
     * 
     * @return trackData
     */
    public java.lang.String getTrackData() {
        return trackData;
    }


    /**
     * Sets the trackData value for this PaymentData.
     * 
     * @param trackData
     */
    public void setTrackData(java.lang.String trackData) {
        this.trackData = trackData;
    }


    /**
     * Gets the pinBlock value for this PaymentData.
     * 
     * @return pinBlock
     */
    public java.lang.String getPinBlock() {
        return pinBlock;
    }


    /**
     * Sets the pinBlock value for this PaymentData.
     * 
     * @param pinBlock
     */
    public void setPinBlock(java.lang.String pinBlock) {
        this.pinBlock = pinBlock;
    }


    /**
     * Gets the pinKsn value for this PaymentData.
     * 
     * @return pinKsn
     */
    public java.lang.String getPinKsn() {
        return pinKsn;
    }


    /**
     * Sets the pinKsn value for this PaymentData.
     * 
     * @param pinKsn
     */
    public void setPinKsn(java.lang.String pinKsn) {
        this.pinKsn = pinKsn;
    }


    /**
     * Gets the readerEntryMode value for this PaymentData.
     * 
     * @return readerEntryMode
     */
    public java.lang.String getReaderEntryMode() {
        return readerEntryMode;
    }


    /**
     * Sets the readerEntryMode value for this PaymentData.
     * 
     * @param readerEntryMode
     */
    public void setReaderEntryMode(java.lang.String readerEntryMode) {
        this.readerEntryMode = readerEntryMode;
    }


    /**
     * Gets the cardNumber value for this PaymentData.
     * 
     * @return cardNumber
     */
    public java.lang.String getCardNumber() {
        return cardNumber;
    }


    /**
     * Sets the cardNumber value for this PaymentData.
     * 
     * @param cardNumber
     */
    public void setCardNumber(java.lang.String cardNumber) {
        this.cardNumber = cardNumber;
    }


    /**
     * Gets the expirationDate value for this PaymentData.
     * 
     * @return expirationDate
     */
    public java.lang.String getExpirationDate() {
        return expirationDate;
    }


    /**
     * Sets the expirationDate value for this PaymentData.
     * 
     * @param expirationDate
     */
    public void setExpirationDate(java.lang.String expirationDate) {
        this.expirationDate = expirationDate;
    }


    /**
     * Gets the cardHolder value for this PaymentData.
     * 
     * @return cardHolder
     */
    public java.lang.String getCardHolder() {
        return cardHolder;
    }


    /**
     * Sets the cardHolder value for this PaymentData.
     * 
     * @param cardHolder
     */
    public void setCardHolder(java.lang.String cardHolder) {
        this.cardHolder = cardHolder;
    }


    /**
     * Gets the avsStreetAddress value for this PaymentData.
     * 
     * @return avsStreetAddress
     */
    public java.lang.String getAvsStreetAddress() {
        return avsStreetAddress;
    }


    /**
     * Sets the avsStreetAddress value for this PaymentData.
     * 
     * @param avsStreetAddress
     */
    public void setAvsStreetAddress(java.lang.String avsStreetAddress) {
        this.avsStreetAddress = avsStreetAddress;
    }


    /**
     * Gets the avsZipCode value for this PaymentData.
     * 
     * @return avsZipCode
     */
    public java.lang.String getAvsZipCode() {
        return avsZipCode;
    }


    /**
     * Sets the avsZipCode value for this PaymentData.
     * 
     * @param avsZipCode
     */
    public void setAvsZipCode(java.lang.String avsZipCode) {
        this.avsZipCode = avsZipCode;
    }


    /**
     * Gets the cardVerificationValue value for this PaymentData.
     * 
     * @return cardVerificationValue
     */
    public java.lang.String getCardVerificationValue() {
        return cardVerificationValue;
    }


    /**
     * Sets the cardVerificationValue value for this PaymentData.
     * 
     * @param cardVerificationValue
     */
    public void setCardVerificationValue(java.lang.String cardVerificationValue) {
        this.cardVerificationValue = cardVerificationValue;
    }


    /**
     * Gets the token value for this PaymentData.
     * 
     * @return token
     */
    public java.lang.String getToken() {
        return token;
    }


    /**
     * Sets the token value for this PaymentData.
     * 
     * @param token
     */
    public void setToken(java.lang.String token) {
        this.token = token;
    }


    /**
     * Gets the vaultToken value for this PaymentData.
     * 
     * @return vaultToken
     */
    public java.lang.String getVaultToken() {
        return vaultToken;
    }


    /**
     * Sets the vaultToken value for this PaymentData.
     * 
     * @param vaultToken
     */
    public void setVaultToken(java.lang.String vaultToken) {
        this.vaultToken = vaultToken;
    }


    /**
     * Gets the customerEmailAddress value for this PaymentData.
     * 
     * @return customerEmailAddress
     */
    public java.lang.String getCustomerEmailAddress() {
        return customerEmailAddress;
    }


    /**
     * Sets the customerEmailAddress value for this PaymentData.
     * 
     * @param customerEmailAddress
     */
    public void setCustomerEmailAddress(java.lang.String customerEmailAddress) {
        this.customerEmailAddress = customerEmailAddress;
    }


    /**
     * Gets the walletId value for this PaymentData.
     * 
     * @return walletId
     */
    public java.lang.String getWalletId() {
        return walletId;
    }


    /**
     * Sets the walletId value for this PaymentData.
     * 
     * @param walletId
     */
    public void setWalletId(java.lang.String walletId) {
        this.walletId = walletId;
    }


    /**
     * Gets the encryptedKeyedData value for this PaymentData.
     * 
     * @return encryptedKeyedData
     */
    public java.lang.String getEncryptedKeyedData() {
        return encryptedKeyedData;
    }


    /**
     * Sets the encryptedKeyedData value for this PaymentData.
     * 
     * @param encryptedKeyedData
     */
    public void setEncryptedKeyedData(java.lang.String encryptedKeyedData) {
        this.encryptedKeyedData = encryptedKeyedData;
    }


    /**
     * Gets the paymentCryptogram value for this PaymentData.
     * 
     * @return paymentCryptogram
     */
    public java.lang.String getPaymentCryptogram() {
        return paymentCryptogram;
    }


    /**
     * Sets the paymentCryptogram value for this PaymentData.
     * 
     * @param paymentCryptogram
     */
    public void setPaymentCryptogram(java.lang.String paymentCryptogram) {
        this.paymentCryptogram = paymentCryptogram;
    }


    /**
     * Gets the paymentCryptogramType value for this PaymentData.
     * 
     * @return paymentCryptogramType
     */
    public java.lang.String getPaymentCryptogramType() {
        return paymentCryptogramType;
    }


    /**
     * Sets the paymentCryptogramType value for this PaymentData.
     * 
     * @param paymentCryptogramType
     */
    public void setPaymentCryptogramType(java.lang.String paymentCryptogramType) {
        this.paymentCryptogramType = paymentCryptogramType;
    }


    /**
     * Gets the encryptedPaymentData value for this PaymentData.
     * 
     * @return encryptedPaymentData
     */
    public java.lang.String getEncryptedPaymentData() {
        return encryptedPaymentData;
    }


    /**
     * Sets the encryptedPaymentData value for this PaymentData.
     * 
     * @param encryptedPaymentData
     */
    public void setEncryptedPaymentData(java.lang.String encryptedPaymentData) {
        this.encryptedPaymentData = encryptedPaymentData;
    }


    /**
     * Gets the eciIndicator value for this PaymentData.
     * 
     * @return eciIndicator
     */
    public java.lang.String getEciIndicator() {
        return eciIndicator;
    }


    /**
     * Sets the eciIndicator value for this PaymentData.
     * 
     * @param eciIndicator
     */
    public void setEciIndicator(java.lang.String eciIndicator) {
        this.eciIndicator = eciIndicator;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PaymentData)) return false;
        PaymentData other = (PaymentData) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.source==null && other.getSource()==null) || 
             (this.source!=null &&
              this.source.equals(other.getSource()))) &&
            ((this.trackData==null && other.getTrackData()==null) || 
             (this.trackData!=null &&
              this.trackData.equals(other.getTrackData()))) &&
            ((this.pinBlock==null && other.getPinBlock()==null) || 
             (this.pinBlock!=null &&
              this.pinBlock.equals(other.getPinBlock()))) &&
            ((this.pinKsn==null && other.getPinKsn()==null) || 
             (this.pinKsn!=null &&
              this.pinKsn.equals(other.getPinKsn()))) &&
            ((this.readerEntryMode==null && other.getReaderEntryMode()==null) || 
             (this.readerEntryMode!=null &&
              this.readerEntryMode.equals(other.getReaderEntryMode()))) &&
            ((this.cardNumber==null && other.getCardNumber()==null) || 
             (this.cardNumber!=null &&
              this.cardNumber.equals(other.getCardNumber()))) &&
            ((this.expirationDate==null && other.getExpirationDate()==null) || 
             (this.expirationDate!=null &&
              this.expirationDate.equals(other.getExpirationDate()))) &&
            ((this.cardHolder==null && other.getCardHolder()==null) || 
             (this.cardHolder!=null &&
              this.cardHolder.equals(other.getCardHolder()))) &&
            ((this.avsStreetAddress==null && other.getAvsStreetAddress()==null) || 
             (this.avsStreetAddress!=null &&
              this.avsStreetAddress.equals(other.getAvsStreetAddress()))) &&
            ((this.avsZipCode==null && other.getAvsZipCode()==null) || 
             (this.avsZipCode!=null &&
              this.avsZipCode.equals(other.getAvsZipCode()))) &&
            ((this.cardVerificationValue==null && other.getCardVerificationValue()==null) || 
             (this.cardVerificationValue!=null &&
              this.cardVerificationValue.equals(other.getCardVerificationValue()))) &&
            ((this.token==null && other.getToken()==null) || 
             (this.token!=null &&
              this.token.equals(other.getToken()))) &&
            ((this.vaultToken==null && other.getVaultToken()==null) || 
             (this.vaultToken!=null &&
              this.vaultToken.equals(other.getVaultToken()))) &&
            ((this.customerEmailAddress==null && other.getCustomerEmailAddress()==null) || 
             (this.customerEmailAddress!=null &&
              this.customerEmailAddress.equals(other.getCustomerEmailAddress()))) &&
            ((this.walletId==null && other.getWalletId()==null) || 
             (this.walletId!=null &&
              this.walletId.equals(other.getWalletId()))) &&
            ((this.encryptedKeyedData==null && other.getEncryptedKeyedData()==null) || 
             (this.encryptedKeyedData!=null &&
              this.encryptedKeyedData.equals(other.getEncryptedKeyedData()))) &&
            ((this.paymentCryptogram==null && other.getPaymentCryptogram()==null) || 
             (this.paymentCryptogram!=null &&
              this.paymentCryptogram.equals(other.getPaymentCryptogram()))) &&
            ((this.paymentCryptogramType==null && other.getPaymentCryptogramType()==null) || 
             (this.paymentCryptogramType!=null &&
              this.paymentCryptogramType.equals(other.getPaymentCryptogramType()))) &&
            ((this.encryptedPaymentData==null && other.getEncryptedPaymentData()==null) || 
             (this.encryptedPaymentData!=null &&
              this.encryptedPaymentData.equals(other.getEncryptedPaymentData()))) &&
            ((this.eciIndicator==null && other.getEciIndicator()==null) || 
             (this.eciIndicator!=null &&
              this.eciIndicator.equals(other.getEciIndicator())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getSource() != null) {
            _hashCode += getSource().hashCode();
        }
        if (getTrackData() != null) {
            _hashCode += getTrackData().hashCode();
        }
        if (getPinBlock() != null) {
            _hashCode += getPinBlock().hashCode();
        }
        if (getPinKsn() != null) {
            _hashCode += getPinKsn().hashCode();
        }
        if (getReaderEntryMode() != null) {
            _hashCode += getReaderEntryMode().hashCode();
        }
        if (getCardNumber() != null) {
            _hashCode += getCardNumber().hashCode();
        }
        if (getExpirationDate() != null) {
            _hashCode += getExpirationDate().hashCode();
        }
        if (getCardHolder() != null) {
            _hashCode += getCardHolder().hashCode();
        }
        if (getAvsStreetAddress() != null) {
            _hashCode += getAvsStreetAddress().hashCode();
        }
        if (getAvsZipCode() != null) {
            _hashCode += getAvsZipCode().hashCode();
        }
        if (getCardVerificationValue() != null) {
            _hashCode += getCardVerificationValue().hashCode();
        }
        if (getToken() != null) {
            _hashCode += getToken().hashCode();
        }
        if (getVaultToken() != null) {
            _hashCode += getVaultToken().hashCode();
        }
        if (getCustomerEmailAddress() != null) {
            _hashCode += getCustomerEmailAddress().hashCode();
        }
        if (getWalletId() != null) {
            _hashCode += getWalletId().hashCode();
        }
        if (getEncryptedKeyedData() != null) {
            _hashCode += getEncryptedKeyedData().hashCode();
        }
        if (getPaymentCryptogram() != null) {
            _hashCode += getPaymentCryptogram().hashCode();
        }
        if (getPaymentCryptogramType() != null) {
            _hashCode += getPaymentCryptogramType().hashCode();
        }
        if (getEncryptedPaymentData() != null) {
            _hashCode += getEncryptedPaymentData().hashCode();
        }
        if (getEciIndicator() != null) {
            _hashCode += getEciIndicator().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PaymentData.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "PaymentData"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("source");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "Source"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("trackData");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "TrackData"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pinBlock");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "PinBlock"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pinKsn");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "PinKsn"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("readerEntryMode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "ReaderEntryMode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "CardNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("expirationDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "ExpirationDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardHolder");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "CardHolder"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("avsStreetAddress");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "AvsStreetAddress"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("avsZipCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "AvsZipCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardVerificationValue");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "CardVerificationValue"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("token");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "Token"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vaultToken");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "VaultToken"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customerEmailAddress");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "CustomerEmailAddress"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("walletId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "WalletId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("encryptedKeyedData");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "EncryptedKeyedData"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paymentCryptogram");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "PaymentCryptogram"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paymentCryptogramType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "PaymentCryptogramType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("encryptedPaymentData");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "EncryptedPaymentData"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("eciIndicator");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "EciIndicator"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
