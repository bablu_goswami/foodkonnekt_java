package com.merchantwarehouse.schemas.merchantware.v45;

import java.rmi.RemoteException;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;

public class Cayan45Test {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {

			/*System.setProperty("org.apache.commons.logging.LogFactory", 
				"org.apache.commons.logging.impl.LogFactoryImpl");
				System.setProperty("org.apache.commons.logging.Log", 
				"org.apache.commons.logging.impl.Log4JLogger");*/
		
		CreditSoapProxy creditSoapProxy = new CreditSoapProxy();
		
		MerchantCredentials credentials = new MerchantCredentials();
		credentials.setMerchantKey("FY8TN-14F2S-ZCLLC-HWB8A-SCIXP");  
		credentials.setMerchantName("Test Foodkonnect");
		credentials.setMerchantSiteId("JCYYKL8S");
		
		PaymentData paymentData = new PaymentData();
		paymentData.setAvsStreetAddress("");
		paymentData.setCardHolder("");
		paymentData.setCardNumber("4012000033330026"); //4012000033330026
		paymentData.setExpirationDate("1218");
		paymentData.setCardVerificationValue("123");
		paymentData.setSource("Keyed");
		
		AuthorizationRequest request = new AuthorizationRequest();
		request.setAmount("100");
		request.setMerchantTransactionId("ABC232");
		request.setInvoiceNumber("123456");
		
		TransactionResponse45 response45=	creditSoapProxy.authorize(credentials, paymentData, request);
			System.out.println("authorize response---");
		System.out.println("getAuthorizationCode-"+response45.getAuthorizationCode());
			System.out.println("getApprovalStatus-"+response45.getApprovalStatus());
			System.out.println("getToken-"+response45.getToken());//1001233454
			System.out.println("getTransactionDate-"+response45.getTransactionDate());
			paymentData.setToken(response45.getToken());
			
			
			System.out.println("---------------------------------------");
			CaptureRequest captureRequest = new CaptureRequest();
			captureRequest.setAmount("100");
			captureRequest.setToken(response45.getToken());
			TransactionResponse45 responseCapture = creditSoapProxy.capture(credentials, captureRequest);
			System.out.println("capture response---");
			System.out.println("getApprovalStatus-"+responseCapture.getApprovalStatus());
			System.out.println("getAuthorizationCode-"+responseCapture.getAuthorizationCode());
			System.out.println("getApprovalStatus-"+responseCapture.getApprovalStatus());
			System.out.println("getToken-"+responseCapture.getToken());
			System.out.println("getTransactionDate-"+responseCapture.getTransactionDate());
			System.out.println("response46.getAmount-"+responseCapture.getAmount());
			System.out.println("---------------------------------------");
			
		
			
			/*RefundRequest refundRequest= new RefundRequest();
			refundRequest.setAmount("101");
			refundRequest.setInvoiceNumber("123456");
			paymentData.setToken(responseCapture.getToken());
			PaymentData paymentData1 = new PaymentData();
			paymentData1.setToken(responseCapture.getToken());
			paymentData1.setSource("Keyed");
			paymentData1.setExpirationDate("1218");
			
			
			TransactionResponse45 response46=creditSoapProxy.refund(credentials, paymentData, refundRequest);
			System.out.println("refund response---");
			System.out.println("getAuthorizationCode-"+response46.getAuthorizationCode());
			System.out.println("getApprovalStatus-"+response46.getApprovalStatus());
			System.out.println("getToken-"+response46.getToken());
			System.out.println("getTransactionDate-"+response46.getTransactionDate());
			System.out.println("response46.getAmount-"+response46.getAmount());
			
			
			System.out.println("error message-"+response46.getErrorMessage());*/
			
			VoidRequest voidRequest = new VoidRequest();
			voidRequest.setToken(responseCapture.getToken());
			TransactionResponse45 response48 = 	creditSoapProxy._void(credentials,voidRequest );
			System.out.println("_void response---");
			System.out.println("getAuthorizationCode-"+response48.getAuthorizationCode());
			System.out.println("getApprovalStatus-"+response48.getApprovalStatus());
			System.out.println("getToken-"+response48.getToken());
			System.out.println("getTransactionDate-"+response48.getTransactionDate());
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//LOGGER.error("error: " + e.getMessage());
		}
		
		
		

	}

}
