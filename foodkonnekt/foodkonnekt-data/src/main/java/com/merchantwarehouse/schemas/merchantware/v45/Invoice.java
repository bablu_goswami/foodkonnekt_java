/**
 * Invoice.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.merchantwarehouse.schemas.merchantware.v45;

public class Invoice  implements java.io.Serializable {
    private java.lang.String taxIndicator;

    private java.lang.String productDescription;

    private java.lang.String discountAmount;

    private java.lang.String shippingAmount;

    private java.lang.String dutyAmount;

    private java.lang.String destinationPostalCode;

    private java.lang.String destinationCountryCode;

    private java.lang.String shipFromPostalCode;

    private java.lang.String alternateTaxAmount;

    private java.lang.String alternateTaxRate;

    private com.merchantwarehouse.schemas.merchantware.v45.LineItem[] lineItems;

    public Invoice() {
    }

    public Invoice(
           java.lang.String taxIndicator,
           java.lang.String productDescription,
           java.lang.String discountAmount,
           java.lang.String shippingAmount,
           java.lang.String dutyAmount,
           java.lang.String destinationPostalCode,
           java.lang.String destinationCountryCode,
           java.lang.String shipFromPostalCode,
           java.lang.String alternateTaxAmount,
           java.lang.String alternateTaxRate,
           com.merchantwarehouse.schemas.merchantware.v45.LineItem[] lineItems) {
           this.taxIndicator = taxIndicator;
           this.productDescription = productDescription;
           this.discountAmount = discountAmount;
           this.shippingAmount = shippingAmount;
           this.dutyAmount = dutyAmount;
           this.destinationPostalCode = destinationPostalCode;
           this.destinationCountryCode = destinationCountryCode;
           this.shipFromPostalCode = shipFromPostalCode;
           this.alternateTaxAmount = alternateTaxAmount;
           this.alternateTaxRate = alternateTaxRate;
           this.lineItems = lineItems;
    }


    /**
     * Gets the taxIndicator value for this Invoice.
     * 
     * @return taxIndicator
     */
    public java.lang.String getTaxIndicator() {
        return taxIndicator;
    }


    /**
     * Sets the taxIndicator value for this Invoice.
     * 
     * @param taxIndicator
     */
    public void setTaxIndicator(java.lang.String taxIndicator) {
        this.taxIndicator = taxIndicator;
    }


    /**
     * Gets the productDescription value for this Invoice.
     * 
     * @return productDescription
     */
    public java.lang.String getProductDescription() {
        return productDescription;
    }


    /**
     * Sets the productDescription value for this Invoice.
     * 
     * @param productDescription
     */
    public void setProductDescription(java.lang.String productDescription) {
        this.productDescription = productDescription;
    }


    /**
     * Gets the discountAmount value for this Invoice.
     * 
     * @return discountAmount
     */
    public java.lang.String getDiscountAmount() {
        return discountAmount;
    }


    /**
     * Sets the discountAmount value for this Invoice.
     * 
     * @param discountAmount
     */
    public void setDiscountAmount(java.lang.String discountAmount) {
        this.discountAmount = discountAmount;
    }


    /**
     * Gets the shippingAmount value for this Invoice.
     * 
     * @return shippingAmount
     */
    public java.lang.String getShippingAmount() {
        return shippingAmount;
    }


    /**
     * Sets the shippingAmount value for this Invoice.
     * 
     * @param shippingAmount
     */
    public void setShippingAmount(java.lang.String shippingAmount) {
        this.shippingAmount = shippingAmount;
    }


    /**
     * Gets the dutyAmount value for this Invoice.
     * 
     * @return dutyAmount
     */
    public java.lang.String getDutyAmount() {
        return dutyAmount;
    }


    /**
     * Sets the dutyAmount value for this Invoice.
     * 
     * @param dutyAmount
     */
    public void setDutyAmount(java.lang.String dutyAmount) {
        this.dutyAmount = dutyAmount;
    }


    /**
     * Gets the destinationPostalCode value for this Invoice.
     * 
     * @return destinationPostalCode
     */
    public java.lang.String getDestinationPostalCode() {
        return destinationPostalCode;
    }


    /**
     * Sets the destinationPostalCode value for this Invoice.
     * 
     * @param destinationPostalCode
     */
    public void setDestinationPostalCode(java.lang.String destinationPostalCode) {
        this.destinationPostalCode = destinationPostalCode;
    }


    /**
     * Gets the destinationCountryCode value for this Invoice.
     * 
     * @return destinationCountryCode
     */
    public java.lang.String getDestinationCountryCode() {
        return destinationCountryCode;
    }


    /**
     * Sets the destinationCountryCode value for this Invoice.
     * 
     * @param destinationCountryCode
     */
    public void setDestinationCountryCode(java.lang.String destinationCountryCode) {
        this.destinationCountryCode = destinationCountryCode;
    }


    /**
     * Gets the shipFromPostalCode value for this Invoice.
     * 
     * @return shipFromPostalCode
     */
    public java.lang.String getShipFromPostalCode() {
        return shipFromPostalCode;
    }


    /**
     * Sets the shipFromPostalCode value for this Invoice.
     * 
     * @param shipFromPostalCode
     */
    public void setShipFromPostalCode(java.lang.String shipFromPostalCode) {
        this.shipFromPostalCode = shipFromPostalCode;
    }


    /**
     * Gets the alternateTaxAmount value for this Invoice.
     * 
     * @return alternateTaxAmount
     */
    public java.lang.String getAlternateTaxAmount() {
        return alternateTaxAmount;
    }


    /**
     * Sets the alternateTaxAmount value for this Invoice.
     * 
     * @param alternateTaxAmount
     */
    public void setAlternateTaxAmount(java.lang.String alternateTaxAmount) {
        this.alternateTaxAmount = alternateTaxAmount;
    }


    /**
     * Gets the alternateTaxRate value for this Invoice.
     * 
     * @return alternateTaxRate
     */
    public java.lang.String getAlternateTaxRate() {
        return alternateTaxRate;
    }


    /**
     * Sets the alternateTaxRate value for this Invoice.
     * 
     * @param alternateTaxRate
     */
    public void setAlternateTaxRate(java.lang.String alternateTaxRate) {
        this.alternateTaxRate = alternateTaxRate;
    }


    /**
     * Gets the lineItems value for this Invoice.
     * 
     * @return lineItems
     */
    public com.merchantwarehouse.schemas.merchantware.v45.LineItem[] getLineItems() {
        return lineItems;
    }


    /**
     * Sets the lineItems value for this Invoice.
     * 
     * @param lineItems
     */
    public void setLineItems(com.merchantwarehouse.schemas.merchantware.v45.LineItem[] lineItems) {
        this.lineItems = lineItems;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Invoice)) return false;
        Invoice other = (Invoice) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.taxIndicator==null && other.getTaxIndicator()==null) || 
             (this.taxIndicator!=null &&
              this.taxIndicator.equals(other.getTaxIndicator()))) &&
            ((this.productDescription==null && other.getProductDescription()==null) || 
             (this.productDescription!=null &&
              this.productDescription.equals(other.getProductDescription()))) &&
            ((this.discountAmount==null && other.getDiscountAmount()==null) || 
             (this.discountAmount!=null &&
              this.discountAmount.equals(other.getDiscountAmount()))) &&
            ((this.shippingAmount==null && other.getShippingAmount()==null) || 
             (this.shippingAmount!=null &&
              this.shippingAmount.equals(other.getShippingAmount()))) &&
            ((this.dutyAmount==null && other.getDutyAmount()==null) || 
             (this.dutyAmount!=null &&
              this.dutyAmount.equals(other.getDutyAmount()))) &&
            ((this.destinationPostalCode==null && other.getDestinationPostalCode()==null) || 
             (this.destinationPostalCode!=null &&
              this.destinationPostalCode.equals(other.getDestinationPostalCode()))) &&
            ((this.destinationCountryCode==null && other.getDestinationCountryCode()==null) || 
             (this.destinationCountryCode!=null &&
              this.destinationCountryCode.equals(other.getDestinationCountryCode()))) &&
            ((this.shipFromPostalCode==null && other.getShipFromPostalCode()==null) || 
             (this.shipFromPostalCode!=null &&
              this.shipFromPostalCode.equals(other.getShipFromPostalCode()))) &&
            ((this.alternateTaxAmount==null && other.getAlternateTaxAmount()==null) || 
             (this.alternateTaxAmount!=null &&
              this.alternateTaxAmount.equals(other.getAlternateTaxAmount()))) &&
            ((this.alternateTaxRate==null && other.getAlternateTaxRate()==null) || 
             (this.alternateTaxRate!=null &&
              this.alternateTaxRate.equals(other.getAlternateTaxRate()))) &&
            ((this.lineItems==null && other.getLineItems()==null) || 
             (this.lineItems!=null &&
              java.util.Arrays.equals(this.lineItems, other.getLineItems())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getTaxIndicator() != null) {
            _hashCode += getTaxIndicator().hashCode();
        }
        if (getProductDescription() != null) {
            _hashCode += getProductDescription().hashCode();
        }
        if (getDiscountAmount() != null) {
            _hashCode += getDiscountAmount().hashCode();
        }
        if (getShippingAmount() != null) {
            _hashCode += getShippingAmount().hashCode();
        }
        if (getDutyAmount() != null) {
            _hashCode += getDutyAmount().hashCode();
        }
        if (getDestinationPostalCode() != null) {
            _hashCode += getDestinationPostalCode().hashCode();
        }
        if (getDestinationCountryCode() != null) {
            _hashCode += getDestinationCountryCode().hashCode();
        }
        if (getShipFromPostalCode() != null) {
            _hashCode += getShipFromPostalCode().hashCode();
        }
        if (getAlternateTaxAmount() != null) {
            _hashCode += getAlternateTaxAmount().hashCode();
        }
        if (getAlternateTaxRate() != null) {
            _hashCode += getAlternateTaxRate().hashCode();
        }
        if (getLineItems() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getLineItems());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getLineItems(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Invoice.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "Invoice"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("taxIndicator");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "TaxIndicator"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("productDescription");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "ProductDescription"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("discountAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "DiscountAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("shippingAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "ShippingAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dutyAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "DutyAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("destinationPostalCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "DestinationPostalCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("destinationCountryCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "DestinationCountryCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("shipFromPostalCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "ShipFromPostalCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("alternateTaxAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "AlternateTaxAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("alternateTaxRate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "AlternateTaxRate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lineItems");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "LineItems"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "LineItem"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "LineItem"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
