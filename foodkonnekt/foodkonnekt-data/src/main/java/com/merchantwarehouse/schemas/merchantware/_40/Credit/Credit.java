/**
 * Credit.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.merchantwarehouse.schemas.merchantware._40.Credit;

public interface Credit extends javax.xml.rpc.Service {

/**
 * Provides payment, processing, and related services for both credit
 * and debit cards.
 */
    public java.lang.String getCreditSoapAddress();

    public com.merchantwarehouse.schemas.merchantware._40.Credit.CreditSoap getCreditSoap() throws javax.xml.rpc.ServiceException;

    public com.merchantwarehouse.schemas.merchantware._40.Credit.CreditSoap getCreditSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
