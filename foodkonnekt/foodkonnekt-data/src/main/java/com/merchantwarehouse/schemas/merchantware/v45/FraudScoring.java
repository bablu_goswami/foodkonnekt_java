/**
 * FraudScoring.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.merchantwarehouse.schemas.merchantware.v45;

public class FraudScoring  implements java.io.Serializable {
    private java.lang.String externalReference;

    private java.lang.String recommendation;

    private java.lang.String score;

    private java.lang.String status;

    public FraudScoring() {
    }

    public FraudScoring(
           java.lang.String externalReference,
           java.lang.String recommendation,
           java.lang.String score,
           java.lang.String status) {
           this.externalReference = externalReference;
           this.recommendation = recommendation;
           this.score = score;
           this.status = status;
    }


    /**
     * Gets the externalReference value for this FraudScoring.
     * 
     * @return externalReference
     */
    public java.lang.String getExternalReference() {
        return externalReference;
    }


    /**
     * Sets the externalReference value for this FraudScoring.
     * 
     * @param externalReference
     */
    public void setExternalReference(java.lang.String externalReference) {
        this.externalReference = externalReference;
    }


    /**
     * Gets the recommendation value for this FraudScoring.
     * 
     * @return recommendation
     */
    public java.lang.String getRecommendation() {
        return recommendation;
    }


    /**
     * Sets the recommendation value for this FraudScoring.
     * 
     * @param recommendation
     */
    public void setRecommendation(java.lang.String recommendation) {
        this.recommendation = recommendation;
    }


    /**
     * Gets the score value for this FraudScoring.
     * 
     * @return score
     */
    public java.lang.String getScore() {
        return score;
    }


    /**
     * Sets the score value for this FraudScoring.
     * 
     * @param score
     */
    public void setScore(java.lang.String score) {
        this.score = score;
    }


    /**
     * Gets the status value for this FraudScoring.
     * 
     * @return status
     */
    public java.lang.String getStatus() {
        return status;
    }


    /**
     * Sets the status value for this FraudScoring.
     * 
     * @param status
     */
    public void setStatus(java.lang.String status) {
        this.status = status;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof FraudScoring)) return false;
        FraudScoring other = (FraudScoring) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.externalReference==null && other.getExternalReference()==null) || 
             (this.externalReference!=null &&
              this.externalReference.equals(other.getExternalReference()))) &&
            ((this.recommendation==null && other.getRecommendation()==null) || 
             (this.recommendation!=null &&
              this.recommendation.equals(other.getRecommendation()))) &&
            ((this.score==null && other.getScore()==null) || 
             (this.score!=null &&
              this.score.equals(other.getScore()))) &&
            ((this.status==null && other.getStatus()==null) || 
             (this.status!=null &&
              this.status.equals(other.getStatus())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getExternalReference() != null) {
            _hashCode += getExternalReference().hashCode();
        }
        if (getRecommendation() != null) {
            _hashCode += getRecommendation().hashCode();
        }
        if (getScore() != null) {
            _hashCode += getScore().hashCode();
        }
        if (getStatus() != null) {
            _hashCode += getStatus().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(FraudScoring.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "FraudScoring"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("externalReference");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "ExternalReference"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("recommendation");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "Recommendation"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("score");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "Score"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("status");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.merchantwarehouse.com/merchantware/v45/", "Status"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
