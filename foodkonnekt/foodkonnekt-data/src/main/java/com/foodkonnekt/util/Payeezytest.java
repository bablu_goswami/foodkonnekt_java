package com.foodkonnekt.util;

import java.util.Properties;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.firstData.payeezy.PayeezyClientHelperf;
import com.firstdata.payeezy.models.transaction.PayeezyResponse;
import com.firstdata.payeezy.models.transaction.Token;
import com.firstdata.payeezy.models.transaction.TransactionRequest;
import com.firstdata.payeezy.models.transaction.Transarmor;

public class Payeezytest {
	private static final Logger LOGGER = LoggerFactory.getLogger(Payeezytest.class);

    public static void main(String[] args) throws Exception {
        Properties p = new Properties();
        //p.put("url", environment.getProperty("PAYEEZY_URL"));
        p.put("apikey", "BrESAKjlxZhYGq8oACMllFTgkIaGJ7ym");
        p.put("token", "fdoa-1cdcd38f07b9d340ce4a02c18cef2cda1cdcd38f07b9d340");
        p.put("pzsecret", "aa5f9c78d4c46945752403795993fdaac2006acd9b9daab217f5113c741ab049");

        PayeezyClientHelperf client = new PayeezyClientHelperf(p);
  
//        TransactionRequest request = new TransactionRequest();
//        PayeezyResponse payeezyResponse = client.doPrimaryTransaction(request);
//        LOGGER.info("payeezyResponse "+payeezyResponse.getResponseBody());
//        JSONObject jObject = new JSONObject(payeezyResponse.getResponseBody());
//         if (jObject.has("Error")) {
//            LOGGER.info("eror");
//            JSONObject ja_data = jObject.getJSONObject("Error");
//            LOGGER.info("eror"+ja_data);
//            JSONArray arr = ja_data.getJSONArray("messages");
//            LOGGER.info("arr"+arr);
//            LOGGER.info("arr"+arr);
//            for (int i = 0; i< arr.length(); i++){
//            LOGGER.info(arr.getJSONObject(i).getString("code"));
//            }
//     
//        }
//        
        
        
        
        
        
        
        
        
        
        
        

        TransactionRequest request1 = new   TransactionRequest()  ;
        request1.setReferenceNo("ORD-1234777");
        request1.setAmount("200");
        request1.setPaymentMethod("token");
        request1.setCurrency("USD");
        Token token  = new Token();
        token.setTokenType("FDToken");
        Transarmor tokenData = new Transarmor();
        tokenData.setType("visa");
        tokenData.setValue("7584652577170002");
        tokenData.setExpiryDt("0521");
        tokenData.setName("John Smith");
        token.setTokenData(tokenData);
        request1.setToken(token);
        
        
      //  do3DSecureTransactionFinal(client,request1);
        

        String transctiontag = "4317604754";
        String transctionId = "ET171040";
        doAuthorizePayment(client);
      // voidTranscation(client, transctiontag, transctionId);
      //   doCapturePayment(client, transctiontag, transctionId);
       //  doRefundPayment(client, transctiontag, transctionId);
    }

    
    
    public static void do3DSecureTransactionFinal(PayeezyClientHelperf client, TransactionRequest request1) {
        
        LOGGER.info("======PayeezyTest : inside do3DSecureTransactionFinal starts====");
        try 
        {
          // TransactionRequest request = Payeezytest.getPrimaryTransaction();
            PayeezyResponse payeezyResponse = client.do3DSecureTransaction( request1);
            LOGGER.info("payeezyResponse "+payeezyResponse);
            JSONObject jObject = new JSONObject(payeezyResponse.getResponseBody());
            LOGGER.info("jObject "+jObject);
            if(jObject.has("transaction_status")){
            String transaction_status = jObject.getString("transaction_status");
            LOGGER.info("transaction_status"+transaction_status);
            }
            LOGGER.info("payeezyResponse-----" + payeezyResponse.getResponseBody());
        } catch (Exception e) {
            LOGGER.info("======PayeezyTest : inside do3DSecureTransactionFinal Exception ===="+e);

            LOGGER.error("error: " + e.getMessage());
        }
    }
    public static void doAuthorizePayment(PayeezyClientHelperf client) {
        
        LOGGER.info("======PayeezyTest : inside doAuthorizePayment starts====");

        try {
            TransactionRequest request = Payeezytest.getPrimaryTransaction();
            
            
            String merchnatName ="Besa's Italian Restaurant";
        	String alphaAndDigits = merchnatName.replaceAll("[^a-zA-Z0-9]+","");
        	
            request.setReferenceNo(alphaAndDigits);
            LOGGER.info("request "+request);
            PayeezyResponse payeezyResponse = client.doPrimaryTransaction(request);
            JSONObject jObject = new JSONObject(payeezyResponse.getResponseBody());
            if(jObject.has("transaction_status")){
            String transaction_status = jObject.getString("transaction_status");
            LOGGER.info("transaction_status"+transaction_status);
            }
        
            LOGGER.info("payeezyResponse-----" + payeezyResponse.getResponseBody());
        } catch (Exception e) {
        	LOGGER.error("===============  PayeezyTest : Inside doAuthorizePayment :: Exception  ============= " + e);

            LOGGER.error("error: " + e.getMessage());
        }
    }

    public static TransactionRequest voidTranscation(PayeezyClientHelperf client, String transctiontag, String transctionId) {
        TransactionRequest request = Payeezytest.getSecondaryTransaction();
        request.setTransactionType("VOID");
        request.setTransactionTag(transctiontag);
        request.setAmount("2889");
        try {
            PayeezyResponse payeezyResponse = client.doSecondaryTransaction(transctionId, request);
            LOGGER.info(" inside voidTranscation payeezyResponse : "+payeezyResponse.getResponseBody());
        } catch (Exception e) {
        	LOGGER.error("===============  PayeezyTest : Inside voidTranscation :: Exception  ============= " + e);

            LOGGER.error("error: " + e.getMessage());
        }
        return request;
    }

    public static TransactionRequest doRefundPayment(PayeezyClientHelperf client, String transctiontag, String transctionId) {
        LOGGER.info("======PayeezyTest : inside doRefundPayment starts====");

    	TransactionRequest request = Payeezytest.getSecondaryTransaction();
        request.setTransactionType("REFUND");
        request.setTransactionTag(transctiontag);
        request.setAmount("1100");
        try {
            PayeezyResponse payeezyResponse = client.doSecondaryTransaction(transctionId, request);
            LOGGER.info("REFUND----"+payeezyResponse.getResponseBody());
        } catch (Exception e) {
            LOGGER.info("======PayeezyTest : inside doRefundPayment Exception===="+e);

            LOGGER.error("error: " + e.getMessage());
        }
        LOGGER.info("======PayeezyTest : inside doRefundPayment End====");

        return request;
    }

    public static TransactionRequest doCapturePayment(PayeezyClientHelperf client, String transctiontag, String transctionId) {
        TransactionRequest request = Payeezytest.getSecondaryTransaction();
        request.setTransactionType("CAPTURE");
        request.setTransactionTag(transctiontag);
        request.setAmount("115");
        try {
            PayeezyResponse payeezyResponse = client.doSecondaryTransaction(transctionId, request);
            LOGGER.info(" payeezyresponse "+payeezyResponse.getResponseBody());
        } catch (Exception e) {
            LOGGER.info("======PayeezyTest : inside doCapturePayment Exception===="+e);

            LOGGER.error("error: " + e.getMessage());
        }
        return request;
    }

    public static TransactionRequest getPrimaryTransaction() {
        TransactionRequest request = new TransactionRequest();
        request.setAmount("110");
        request.setCurrency("USD");
        request.setPaymentMethod("credit_card");
        request.setTransactionType("AUTHORIZE");
        com.firstdata.payeezy.models.transaction.Card card = new com.firstdata.payeezy.models.transaction.Card();
        card.setCvv("123");
        card.setExpiryDt("1221");
        card.setName("Test data ");
        card.setType("MasterCard");
        card.setNumber("5500000000000004");
        request.setCard(card);
        com.firstdata.payeezy.models.transaction.Address address = new com.firstdata.payeezy.models.transaction.Address();
        request.setBilling(address);
        address.setState("NY");
        address.setAddressLine1("sss");
        address.setZip("11747");
        address.setCountry("US");
        return request;
    }

    public static TransactionRequest getSecondaryTransaction() {
        TransactionRequest trans = new TransactionRequest();
        trans.setPaymentMethod("credit_card");
        trans.setCurrency("USD");
        return trans;
    }

}

