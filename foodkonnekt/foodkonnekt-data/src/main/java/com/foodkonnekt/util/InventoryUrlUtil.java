package com.foodkonnekt.util;

import org.apache.http.client.methods.HttpGet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;

import com.foodkonnekt.model.Merchant;

public class InventoryUrlUtil {

    /**
     * Get lineItem with expand categories,modifierGroups and taxRates
     * 
     * @param merchant
     * @param lineItemId
     * @return String
     */
	private static final Logger LOGGER= LoggerFactory.getLogger(InventoryUrlUtil.class);

	public static String getLineItemWithExpands(Merchant merchant, String lineItemId,Environment environment) {
    	LOGGER.info("==== InventoryUrlUtil : inside getLineItemWithExpands starts: lineItemId "+lineItemId);
        HttpGet request = new HttpGet(environment.getProperty("CLOVER_URL") + IConstant.CLOVER_INSTANCE_URL
                        + merchant.getPosMerchantId() + "/items/" + lineItemId
                        + "?expand=categories,modifierGroups,taxRates&access_token=" + merchant.getAccessToken());
    	LOGGER.info("==== InventoryUrlUtil : inside getLineItemWithExpands End ");
        return CloverUrlUtil.convertToStringJson(request);
    }

    /**
     * Get modifiers by modifierGroupsId
     * 
     * @param merchant
     * @param posModifierGroupId
     * @return String
     */
    public static String getModiferFromClover(Merchant merchant, String posModifierGroupId,Environment environment) {
    	LOGGER.info("==== InventoryUrlUtil : inside getModiferFromClover starts: posModifierGroupId "+posModifierGroupId);

    	HttpGet request = new HttpGet(environment.getProperty("CLOVER_URL") + IConstant.CLOVER_INSTANCE_URL
                        + merchant.getPosMerchantId() + "/modifier_groups/" + posModifierGroupId
                        + "?expand=modifiers&access_token=" + merchant.getAccessToken());
    	LOGGER.info("==== InventoryUrlUtil : inside getModiferFromClover End ");
        return CloverUrlUtil.convertToStringJson(request);
    }

    /**
     * check modiferGroup exist or not in clover
     * 
     * @param merchant
     * @param posId
     * @return String
     */
    public static String checkModiferGroup(Merchant merchant, String posModifierGroupId,Environment environment) {
    	LOGGER.info("==== InventoryUrlUtil : inside checkModiferGroup starts: posModifierGroupId "+posModifierGroupId);

        HttpGet request = new HttpGet(environment.getProperty("CLOVER_URL") + IConstant.CLOVER_INSTANCE_URL
                        + merchant.getPosMerchantId() + "/modifier_groups/" + posModifierGroupId + "?access_token="
                        + merchant.getAccessToken());
    	LOGGER.info("==== InventoryUrlUtil : inside checkModiferGroup End ");

        return CloverUrlUtil.convertToStringJson(request);
    }
    
    public static String checkCategory(Merchant merchant, String posCategoryId,Environment environment) {
    	LOGGER.info("==== InventoryUrlUtil : inside checkCategory starts: posCategoryId "+posCategoryId);

        HttpGet request = new HttpGet(environment.getProperty("CLOVER_URL") + IConstant.CLOVER_INSTANCE_URL
                        + merchant.getPosMerchantId() + "/categories/" + posCategoryId + "?access_token="
                        + merchant.getAccessToken());
    	LOGGER.info("==== InventoryUrlUtil : inside checkCategory End ");

        return CloverUrlUtil.convertToStringJson(request);
    }
}
