package com.foodkonnekt.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;

import com.foodkonnekt.model.Clover;
import com.foodkonnekt.model.Merchant;
import com.google.gson.Gson;

public class CloverUrlUtil {

	/**
	 * Find merchant details based on merchantId
	 * 
	 * @param clover
	 * @return
	 */
	private static final Logger LOGGER= LoggerFactory.getLogger(CloverUrlUtil.class);

	public static void enableFuturOrderOnClover(String allowFutureOrder,Merchant merchant,Environment environment){
		LOGGER.info("===============  CloverUrlUtil : Inside enableFuturOrderOnClover :: Start  ============= ");

		Gson gson = new Gson();
   	 Map<String, Map<String, String>> notf = new HashMap<String, Map<String, String>>();
   	Map<String, String> notification = new HashMap<String, String>();
       notification.put("appEvent", "notification");
       notification.put("payload", "allowFutureOrder@#"+allowFutureOrder);

// +"@#"+tip add this to notification payload if we add tip amount to show on popup on
// notification app side
notf.put("notification", notification);
String notificationJson = gson.toJson(notf);

notificationJson = notificationJson.trim().replaceAll("\\\\+\"", "'").replace("'[", "[")
       .replace("]'", "]");

sendNotification(notificationJson,merchant.getPosMerchantId(),merchant.getAccessToken(),environment);
	}
	
	private static void sendNotification(String notificationJson, String merchantId, String accessToken,Environment environment) {
        HttpPost postRequest = new HttpPost(environment.getProperty("CLOVER_V2_URL") + merchantId + "/apps/" + environment.getProperty("CLOVER_APP_ID")
                        + "/notify?access_token=" + accessToken);
        LOGGER.info("=====CloverUrlUtil : sendNotification ==== " + convertToStringJson(postRequest, notificationJson));
    }
	public static String getMerchantDetails(Clover clover) {
		LOGGER.info("===============  CloverUrlUtil : Inside getMerchantDetails :: Start  ============= ");

		HttpGet request = new HttpGet(clover.getUrl() + clover.getInstantUrl() + clover.getMerchantId()
				+ "?expand=owner,employees&access_token=" + clover.getAuthToken());
		
		LOGGER.info("===============  CloverUrlUtil : Inside getMerchantDetails :: End  ============= ");

		return convertToStringJson(request);
	}

	/**
	 * Find address of merchant
	 * 
	 * @param clover
	 * @return
	 */
	public static String getMerchantAddress(Clover clover) {
		LOGGER.info("===============  CloverUrlUtil : Inside getMerchantAddress :: Start  ============= ");

		HttpGet request = new HttpGet(clover.getUrl() + clover.getInstantUrl() + clover.getMerchantId()
				+ "/address?access_token=" + clover.getAuthToken());
		
		LOGGER.info("===============  CloverUrlUtil : Inside getMerchantAddress :: End  ============= ");

		return convertToStringJson(request);
	}

	/**
	 * find billing information
	 * 
	 * @param clover
	 * @return
	 */
	public static String getBillingInfo(Clover clover) {
		LOGGER.info("===============  CloverUrlUtil : Inside getBillingInfo :: Start  ============= ");

		HttpGet request = new HttpGet(clover.getUrl() + clover.getInstantUrl() + clover.getMerchantId()
				+ "/address?access_token=" + clover.getAuthToken());
		
		LOGGER.info("===============  CloverUrlUtil : Inside getBillingInfo :: End  ============= ");

		return convertToStringJson(request);
	}

	/**
	 * Convert response to json output
	 * 
	 * @param request
	 * @return
	 */
	public static String convertToStringJson(HttpGet request) {
		HttpClient client = HttpClientBuilder.create().build();
		HttpResponse response = null;
		StringBuilder responseBuilder = new StringBuilder();
		try {
			response = client.execute(request);
			BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			String line = "";
			while ((line = rd.readLine()) != null) {
				responseBuilder.append(line);
			}
		} catch (IOException e) {
			LOGGER.error("error: " + e.getMessage());
		}
		return responseBuilder.toString();
	}

	public static String convertToStringJson(HttpPost postRequest, String customerJson) {
		StringBuilder responseBuilder = new StringBuilder();
		try {
			HttpClient httpClient = HttpClientBuilder.create().build();
			StringEntity input = new StringEntity(customerJson);
			input.setContentType("application/json");
			postRequest.setEntity(input);

			HttpResponse response = httpClient.execute(postRequest);
			LOGGER.info("Output from Server .... \n" + response.getEntity().toString());
			BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			String line = "";
			while ((line = rd.readLine()) != null) {
				responseBuilder.append(line);
			}
			// LOGGER.info(responseBuilder.toString());
		} catch (MalformedURLException e) {
			LOGGER.error("error: " + e.getMessage());
		} catch (IOException e) {
			LOGGER.error("error: " + e.getMessage());
		}
		return responseBuilder.toString();
	}

	public static String convertToStringJson(HttpPost request) {
		HttpClient client = HttpClientBuilder.create().build();
		HttpResponse response = null;
		StringBuilder responseBuilder = new StringBuilder();
		try {
			response = client.execute(request);
			BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			String line = "";
			while ((line = rd.readLine()) != null) {
				responseBuilder.append(line);
			}
		} catch (IOException e) {
			LOGGER.error("error: " + e.getMessage());
		}
		return responseBuilder.toString();
	}

	/**
	 * Get oder types
	 * 
	 * @param clover
	 * @return
	 */
	public static String getOderType(Clover clover) {
		LOGGER.info("===============  CloverUrlUtil : Inside getOderType :: Start  ============= ");

		HttpGet request = new HttpGet(clover.getUrl() + clover.getInstantUrl() + clover.getMerchantId()
				+ "/order_types?access_token=" + clover.getAuthToken());
		LOGGER.info("===============  CloverUrlUtil : Inside getOderType :: End  ============= ");

		return convertToStringJson(request);
	}
	
	public static String getMerchantAccessToken(String code,Environment environment) {
		LOGGER.info("===============  CloverUrlUtil : Inside getMerchantAccessToken :: Start  ============= ");

		HttpGet request = new HttpGet(environment.getProperty("CLOVER_MAIN_URL")+"/oauth/token?client_id="+environment.getProperty("CLOVER_APP_ID")+"&client_secret="+environment.getProperty("CLOVER_APP_SECRET_KEY")+"&code="+code);
		JSONObject jObject = new JSONObject(convertToStringJson(request));
		 if (jObject!=null && jObject.toString().contains("access_token") && jObject.has("access_token") ){
			 String accessToken=jObject.getString("access_token");
			LOGGER.info("===============  CloverUrlUtil : Inside getMerchantAccessToken :: End  ============= ");

			 return accessToken;
		 }else {
			 LOGGER.info("===============  CloverUrlUtil : Inside getMerchantAccessToken :: End  ============= ");

			 return "Failed";
		 }
		
	}

	/**
	 * Find tax rates
	 * 
	 * @param clover
	 * @return
	 */
	public static String getTaxRate(Clover clover) {
		LOGGER.info("===============  CloverUrlUtil : Inside getTaxRate :: Start  ============= ");

		HttpGet request = new HttpGet(clover.getUrl() + clover.getInstantUrl() + clover.getMerchantId()
				+ "/tax_rates?access_token=" + clover.getAuthToken());
		LOGGER.info("===============  CloverUrlUtil : Inside getTaxRate :: End  ============= ");

		return convertToStringJson(request);
	}

	/**
	 * Find opening hour
	 * 
	 * @param clover
	 * @return
	 */
	public static String getOpeningHour(Clover clover) {
		LOGGER.info("===============  CloverUrlUtil : Inside getOpeningHour :: Start  ============= ");

		HttpGet request = new HttpGet(clover.getUrl() + clover.getInstantUrl() + clover.getMerchantId()
				+ "/opening_hours?access_token=" + clover.getAuthToken());
		LOGGER.info("===============  CloverUrlUtil : Inside getOpeningHour :: End  ============= ");

		return convertToStringJson(request);
	}

	/**
	 * Find payment modes
	 * 
	 * @param clover
	 * @return
	 */
	public static String getPaymentModes(Clover clover) {
		LOGGER.info("===============  CloverUrlUtil : Inside getPaymentModes :: Start  ============= ");

		HttpGet request = new HttpGet(clover.getUrl() + clover.getInstantUrl() + clover.getMerchantId()
				+ "/tenders?access_token=" + clover.getAuthToken());
		
		LOGGER.info("===============  CloverUrlUtil : Inside getPaymentModes :: End  ============= ");

		return convertToStringJson(request);
	}

	public static String getCustomer(Clover clover, String customerId) {
		LOGGER.info("===============  CloverUrlUtil : Inside getCustomer :: Start  ====== customerId : "+customerId);

		HttpGet request = new HttpGet(clover.getUrl() + clover.getInstantUrl() + clover.getMerchantId() + "/customers/"
				+ customerId + "?expand=emailAddresses,phoneNumbers&access_token=" + clover.getAuthToken());
		LOGGER.info("===============  CloverUrlUtil : Inside getCustomer :: End  ============= ");

		return convertToStringJson(request);
	}
	
	public static String getAllCloverCustomers(Clover clover,int offset) {
		LOGGER.info("===============  CloverUrlUtil : Inside getAllCloverCustomers :: Start  ============= ");

		HttpGet request = new HttpGet(clover.getUrl() + clover.getInstantUrl() + clover.getMerchantId() + "/customers/?limit=1000&offset="+offset+"&expand=emailAddresses,phoneNumbers&access_token=" + clover.getAuthToken());
		LOGGER.info("===============  CloverUrlUtil : Inside getAllCloverCustomers :: End  ============= ");

		return convertToStringJson(request);
	}
	
	public static String getAllCloverPayments(Clover clover) {
		LOGGER.info("===============  CloverUrlUtil : Inside getAllCloverPayments :: Start  ============= ");

		HttpGet request = new HttpGet(clover.getUrl() + clover.getInstantUrl() + clover.getMerchantId() + "/payments?limit=1000&access_token=" + clover.getAuthToken());
		LOGGER.info("===============  CloverUrlUtil : Inside getAllCloverPayments :: End  ============= ");

		return convertToStringJson(request);
	}
	
	public static String getAllCloverOrders(Clover clover) {
		LOGGER.info("===============  CloverUrlUtil : Inside getAllCloverOrders :: Start  ============= ");

		HttpGet request = new HttpGet(clover.getUrl() + clover.getInstantUrl() + clover.getMerchantId() + "/orders/?limit=1000&expand=lineItems,customers,payments&access_token=" + clover.getAuthToken());
		LOGGER.info("===============  CloverUrlUtil : Inside getAllCloverOrders :: End  ============= ");

		return convertToStringJson(request);
	}

	public static String getOrderItemsWithModifier(Clover clover, String orderId, String itemPosId) {
		HttpGet request = new HttpGet(clover.getUrl() + clover.getInstantUrl() + clover.getMerchantId() + "/orders/"
				+ orderId + "/line_items/" + itemPosId + "?expand=modifications&access_token=" + clover.getAuthToken());
		return convertToStringJson(request);
	}

	public static String getCloverOrder(Clover clover, String orderId) {
		LOGGER.info("===============  CloverUrlUtil : Inside getCloverOrder :: Start  ============= ");

		HttpGet request = new HttpGet(clover.getUrl() + clover.getInstantUrl() + clover.getMerchantId() + "/orders/"
				+ orderId + "?expand=payment,lineItems&access_token=" + clover.getAuthToken());
	
		LOGGER.info("===============  CloverUrlUtil : Inside getCloverOrder :: End  ============= ");

		return convertToStringJson(request);
	}
	public static String getOrderItems(Clover clover, String orderId) {
		LOGGER.info("===============  CloverUrlUtil : Inside getOrderItems :: Start  ============= ");

		HttpGet request = new HttpGet(clover.getUrl() + clover.getInstantUrl() + clover.getMerchantId() + "/orders/"
				+ orderId + "/line_items/?expand=modifications&access_token=" + clover.getAuthToken());
		LOGGER.info("===============  CloverUrlUtil : Inside getOrderItems :: End  ============= ");

		return convertToStringJson(request);
	}

	public static String getMerchantBilling(Clover clover,Environment environment) {
		LOGGER.info("===============  CloverUrlUtil : Inside getMerchantBilling :: Start  ============= ");

		HttpGet request = new HttpGet(environment.getProperty("CLOVER_APP_URL") + environment.getProperty("CLOVER_APP_ID") + "/merchants/"
				+ clover.getMerchantId() + "/billing_info?access_token=" + environment.getProperty("CLOVER_APP_SECRET_KEY"));
		LOGGER.info("===============  CloverUrlUtil : Inside getMerchantBilling :: End  ============= ");

		return convertToStringJson(request);
	}

	/**
	 * Find categories
	 * 
	 * @param clover
	 * @return
	 */
	public static String getCategories(Clover clover) {
		LOGGER.info("===============  CloverUrlUtil : Inside getCategories :: Start  ============= ");

		HttpGet request = new HttpGet(clover.getUrl() + clover.getInstantUrl() + clover.getMerchantId()
				+ "/categories?access_token=" + clover.getAuthToken());
		LOGGER.info("===============  CloverUrlUtil : Inside getCategories :: End  ============= ");

		return convertToStringJson(request);
	}

	/**
	 * Covert json string to object
	 * 
	 * @param request
	 * @return
	 */
	public static Merchant convertJsonStringToObject(HttpGet request) {
		HttpClient client = HttpClientBuilder.create().build();
		HttpResponse response = null;
		Merchant merchant = null;
		try {
			response = client.execute(request);
			BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			Gson gson = new Gson();
			merchant = gson.fromJson(rd, Merchant.class);
		} catch (IOException e) {
			LOGGER.error("error: " + e.getMessage());
		}
		return merchant;
	}

	public static Merchant getVendorDetails(Clover clover) {
		LOGGER.info("===============  CloverUrlUtil : Inside getVendorDetails :: Start  ============= ");

		HttpGet request = new HttpGet(clover.getUrl() + clover.getInstantUrl() + clover.getMerchantId()
				+ "?expand=owner&access_token=" + clover.getAuthToken());
		LOGGER.info("===============  CloverUrlUtil : Inside getVendorDetails :: End  ============= ");

		return convertJsonStringToObject(request);
	}

	/**
	 * Get modifier and modifier group
	 * 
	 * @param clover
	 * @return
	 */
	public static String getModifierAndModifierGroup(Clover clover) {
		LOGGER.info("===============  CloverUrlUtil : Inside getModifierAndModifierGroup :: Start  ============= ");

		HttpGet request = new HttpGet(clover.getUrl() + clover.getInstantUrl() + clover.getMerchantId()
				+ "/modifier_groups?expand=modifiers%2Citems&access_token=" + clover.getAuthToken());
		LOGGER.info("===============  CloverUrlUtil : Inside getModifierAndModifierGroup :: End  ============= ");

		return convertToStringJson(request);
	}

	/**
	 * Get items
	 * 
	 * @param clover
	 * @return ItemJsonString
	 */
	public static String getItems(Clover clover) {
		LOGGER.info("===============  CloverUrlUtil : Inside getItems :: Start  ============= ");

		HttpGet request = new HttpGet(clover.getUrl() + clover.getInstantUrl() + clover.getMerchantId()
				+ "/items?access_token=" + clover.getAuthToken());
		LOGGER.info("===============  CloverUrlUtil : Inside getItems :: End  ============= ");

		return convertToStringJson(request);
	}

	public static String getAllCategories(Clover clover) {
		HttpGet request = null;
		String responseString = null;
		try {
			LOGGER.info("===============  CloverUrlUtil : Inside getAllCategories :: Start  ============= ");

			request = new HttpGet(clover.getUrl() + clover.getInstantUrl() + clover.getMerchantId()
					+ "/categories?access_token=" + clover.getAuthToken() + "&expand=items");
			HttpClient client = HttpClientBuilder.create().build();
			HttpResponse response = null;
			response = client.execute(request);
			HttpEntity entity = response.getEntity();
			responseString = EntityUtils.toString(entity, "UTF-8");
			LOGGER.info("===============  CloverUrlUtil : Inside getAllCategories :: End  ============= ");

			return responseString;
		} catch (Exception e) {
			LOGGER.error("===============  CloverUrlUtil : Inside getAllCategories :: Exception  ============= " + e);

			// logger.error("Exception occure while getting data from clover is
			// : {}", e.getStackTrace().toString());
		}
LOGGER.info("===============  CloverUrlUtil : Inside getAllCategories :: End  ============= ");

		return responseString;
	}

	/**
	 * 
	 * @param clover
	 * @return
	 */
	public static String getAllItem(Clover clover, Merchant merchant) {
		String responseString = null;
		try {
			LOGGER.info("===============  CloverUrlUtil : Inside getAllItem :: Start  ============= ");

			HttpGet request = new HttpGet(clover.getUrl() + clover.getInstantUrl() + clover.getMerchantId()
					+ "/items?limit=1000&expand=taxRates&access_token=" + clover.getAuthToken());
			HttpClient client = HttpClientBuilder.create().build();
			HttpResponse response = null;
			response = client.execute(request);
			HttpEntity entity = response.getEntity();
			responseString = EntityUtils.toString(entity, "UTF-8");
			return responseString;

		} catch (Exception e) {
			LOGGER.error("===============  CloverUrlUtil : Inside getAllItem :: Exception  ============= " + e);

			// logger.error("Exception occure while getting data from clover is
			// : {}", e.getStackTrace().toString());
		}
		LOGGER.info("===============  CloverUrlUtil : Inside getAllItem :: End  ============= ");

		return responseString;
	}

	public static String getCloverItem(Clover clover, Merchant merchant, String itemPOSId) {
		String responseString = null;
		try {
			LOGGER.info("===============  CloverUrlUtil : Inside getCloverItem :: Start  ============= itemPOSId : "+itemPOSId);

			HttpGet request = new HttpGet(clover.getUrl() + clover.getInstantUrl() + clover.getMerchantId() + "/items/"
					+ itemPOSId + "?expand=taxRates&access_token=" + clover.getAuthToken());
			HttpClient client = HttpClientBuilder.create().build();
			HttpResponse response = null;
			response = client.execute(request);
			HttpEntity entity = response.getEntity();
			responseString = EntityUtils.toString(entity, "UTF-8");
			return responseString;

		} catch (Exception e) {
			LOGGER.error("===============  CloverUrlUtil : Inside getCloverItem :: Exception  ============= " + e);

			// logger.error("Exception occure while getting data from clover is
			// : {}", e.getStackTrace().toString());
		}
		LOGGER.info("===============  CloverUrlUtil : Inside getCloverItem :: End  ============= ");

		return responseString;
	}

	public static String addMteredPrice(Merchant merchant,Environment environment) {
		LOGGER.info("===============  CloverUrlUtil : Inside addMteredPrice :: Start  ============= ");

		HttpPost request = new HttpPost(environment.getProperty("CLOVER_APP_URL") + environment.getProperty("CLOVER_APP_ID") + "/merchants/"
				+ merchant.getPosMerchantId() + "/metereds/" + merchant.getSubscription().getMeteredId()
				+ "?access_token=" + environment.getProperty("CLOVER_APP_SECRET_KEY"));
		LOGGER.info("===============  CloverUrlUtil : Inside addMteredPrice :: End  ============= ");

		return convertToStringJson(request);
	}

	public static String getMeteredCount(Merchant merchant,Environment environment) {
		LOGGER.info("===============  CloverUrlUtil : Inside getMeteredCount :: Start  ============= ");

		HttpGet request = new HttpGet(environment.getProperty("CLOVER_APP_URL") + environment.getProperty("CLOVER_APP_ID") + "/merchants/"
				+ merchant.getPosMerchantId() + "/metereds/" + merchant.getSubscription().getMeteredId()
				+ "?access_token=" + environment.getProperty("CLOVER_APP_SECRET_KEY"));
		LOGGER.info("===============  CloverUrlUtil : Inside getMeteredCount :: End  ============= ");

		return convertToStringJson(request);
	}

	public static String postOrderOnClover(String orderJson, Clover clover) {
LOGGER.info("===============  CloverUrlUtil : Inside postOrderOnClover :: Start  ============= ");

		HttpPost postRequest = new HttpPost(clover.getUrl() + clover.getInstantUrl() + clover.getMerchantId()
				+ "/orders?access_token=" + clover.getAuthToken());

		LOGGER.info("===============  CloverUrlUtil : Inside postOrderOnClover :: End  ============= ");

		return postOnClover(postRequest, orderJson);

	}

	public static String getEmployeeFromClover(Clover clover, String employeeId) {
		LOGGER.info("===============  CloverUrlUtil : Inside getEmployeeFromClover :: Start  ============= ");

		HttpGet request = new HttpGet(clover.getUrl() + clover.getInstantUrl() + clover.getMerchantId() + "/employees/"
				+ employeeId + "?access_token=" + clover.getAuthToken());
		LOGGER.info("===============  CloverUrlUtil : Inside getEmployeeFromClover :: End  ============= ");

		return convertToStringJson(request);
	}
	
	public static String getEmployeesFromClover(Clover clover) {
		LOGGER.info("===============  CloverUrlUtil : Inside getEmployeesFromClover :: Start  ============= ");

		HttpGet request = new HttpGet(clover.getUrl() + clover.getInstantUrl() + clover.getMerchantId() + "/employees/"
				+  "?access_token=" + clover.getAuthToken());
		LOGGER.info("===============  CloverUrlUtil : Inside getEmployeesFromClover :: End  ============= ");

		return convertToStringJson(request);
	}

	public static String deleteEmployee(Clover clover, String employeeId) {
		LOGGER.info("===============  CloverUrlUtil : Inside deleteEmployee :: Start  ============= ");

		HttpDelete deleteRequest = new HttpDelete(clover.getUrl() + clover.getInstantUrl() + clover.getMerchantId()
				+ "/employees/" + employeeId + "?access_token=" + clover.getAuthToken());

		HttpClient client = HttpClientBuilder.create().build();
		HttpResponse response = null;
		try {
			response = client.execute(deleteRequest);
			HttpEntity entity = response.getEntity();
			String responseString = EntityUtils.toString(entity, "UTF-8");
			return responseString;
		} catch (ClientProtocolException e) {
			LOGGER.error("===============  CloverUrlUtil : Inside deleteEmployee :: Exception  ============= " + e);

			// TODO Auto-generated catch block
			LOGGER.error("error: " + e.getMessage());
		} catch (IOException e) {
			LOGGER.error("===============  CloverUrlUtil : Inside deleteEmployee :: Exception  ============= " + e);

			// TODO Auto-generated catch block
			LOGGER.error("error: " + e.getMessage());
		}
LOGGER.info("===============  CloverUrlUtil : Inside deleteEmployee :: End  ============= ");

		return null;
	}

	public static String createEmployeeOnClover(Clover clover) {
		HttpPost postRequest = new HttpPost(clover.getUrl() + clover.getInstantUrl() + clover.getMerchantId()
				+ "/employees?access_token=" + clover.getAuthToken());
		try {
			LOGGER.info("===============  CloverUrlUtil : Inside createEmployeeOnClover :: Start  ============= ");

			HttpClient httpClient = HttpClientBuilder.create().build();
			String customerJson = "{" + " \"role\": \"EMPLOYEE\"," + " \"name\": \"FoodKonnekt Employee\"" + " }";
			StringEntity input = new StringEntity(customerJson);
			input.setContentType("application/json");
			postRequest.setEntity(input);
			HttpResponse response = httpClient.execute(postRequest);
			HttpEntity entity = response.getEntity();
			String responseString = EntityUtils.toString(entity, "UTF-8");
			LOGGER.info("Output from Server .... \n" + responseString);
			String employeePosId = null;

			JSONObject jObject = new JSONObject(responseString);
			if (jObject.has("id")) {

				employeePosId = jObject.getString("id");

			}
LOGGER.info("===============  CloverUrlUtil : Inside createEmployeeOnClover :: End  ============= ");

			return employeePosId;

		} catch (Exception e) {
			LOGGER.error("===============  CloverUrlUtil : Inside createEmployeeOnClover :: Exception  ============= " + e);

			return null;
		}

	}

	public static String updatetOrderOnClover(String orderJson, Clover clover, String cloverOrderId) {
LOGGER.info("===============  CloverUrlUtil : Inside updatetOrderOnClover :: Start  ============= ");

		HttpPost postRequest = new HttpPost(clover.getUrl() + clover.getInstantUrl() + clover.getMerchantId()
				+ "/orders/" + cloverOrderId + "?access_token=" + clover.getAuthToken());
LOGGER.info("===============  CloverUrlUtil : Inside updatetOrderOnClover :: End  ============= ");

		return postOnClover(postRequest, orderJson);

	}

	public static String postOrderLineItemOnClover(String orderLineItemJson, Clover clover, String cloverOrderId) {

		HttpPost postRequest = new HttpPost(clover.getUrl() + clover.getInstantUrl() + clover.getMerchantId()
				+ "/orders/" + cloverOrderId + "/line_items?access_token=" + clover.getAuthToken());

		LOGGER.info("===============  CloverUrlUtil : Inside postOrderLineItemOnClover :: End  ============= ");

		return postOnClover(postRequest, orderLineItemJson);

	}

	public static String postModifiersWithLineItemOnClover(String modificationsJson, Clover clover,
			String orderLineItemId, String cloverOrderId) {
LOGGER.info("===============  CloverUrlUtil : Inside postModifiersWithLineItemOnClover :: Start  ============= ");

		HttpPost postRequest = new HttpPost(
				clover.getUrl() + clover.getInstantUrl() + clover.getMerchantId() + "/orders/" + cloverOrderId
						+ "/line_items/" + orderLineItemId + "/modifications?access_token=" + clover.getAuthToken());
LOGGER.info("===============  CloverUrlUtil : Inside postModifiersWithLineItemOnClover :: End  ============= ");

		return postOnClover(postRequest, modificationsJson);

	}
	
	public static String postOrderDiscountOnClover(String orderDiscountJson, Clover clover,String cloverOrderId) {
LOGGER.info("===============  CloverUrlUtil : Inside postOrderDiscountOnClover :: Start  ============= ");

		HttpPost postRequest = new HttpPost(
				clover.getUrl() + clover.getInstantUrl() + clover.getMerchantId() + "/orders/" + cloverOrderId
						+ "/discounts?access_token=" + clover.getAuthToken());
LOGGER.info("===============  CloverUrlUtil : Inside postOrderDiscountOnClover :: End  ============= ");

		return postOnClover(postRequest, orderDiscountJson);

	}
	
	public static String postItemDiscountOnClover(String itemDiscountJson, Clover clover,String cloverOrderId,String orderLineItemId) {
LOGGER.info("===============  CloverUrlUtil : Inside postItemDiscountOnClover :: Start  ============= ");

		HttpPost postRequest = new HttpPost(
				clover.getUrl() + clover.getInstantUrl() + clover.getMerchantId() + "/orders/" + cloverOrderId
						+ "/line_items/"+orderLineItemId+"/discounts?access_token=" + clover.getAuthToken());
LOGGER.info("===============  CloverUrlUtil : Inside postItemDiscountOnClover :: End  ============= ");

		return postOnClover(postRequest, itemDiscountJson);

	}
	
	

	public static String cashPayment(String paymentJson, String merchantId, String accessToken, String orderId,Environment environment) {
		LOGGER.info("===============  CloverUrlUtil : Inside cashPayment :: Start  ============= ");

		HttpPost postRequest = new HttpPost(environment.getProperty("CLOVER_URL") + IConstant.CLOVER_INSTANCE_URL + merchantId
				+ "/orders/" + orderId + "/payments?access_token=" + accessToken);
		LOGGER.info("===============  CloverUrlUtil : Inside cashPayment :: End  ============= ");

		return postOnClover(postRequest, paymentJson);
	}

	public static String postOnClover(HttpPost postRequest, String jsonObject) {
		StringBuilder responseBuilder = new StringBuilder();
		try {
			LOGGER.info("===============  CloverUrlUtil : Inside postOnClover :: Start  ============= ");

			HttpClient httpClient = HttpClientBuilder.create().build();

			StringEntity input = new StringEntity(jsonObject);
			input.setContentType("application/json");
			postRequest.setEntity(input);

			HttpResponse response = httpClient.execute(postRequest);
			LOGGER.info("Output from Server .... \n");
			BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			String line = "";
			while ((line = rd.readLine()) != null) {
				responseBuilder.append(line);
			}
			LOGGER.info("responseBuilder === "+responseBuilder.toString());
		} catch (MalformedURLException e) {
			LOGGER.error("===============  CloverUrlUtil : Inside postOnClover :: Exception  ============= " + e);

			LOGGER.error("error: " + e.getMessage());
		} catch (IOException e) {
			LOGGER.error("===============  CloverUrlUtil : Inside postOnClover :: Exception  ============= " + e);

			LOGGER.error("error: " + e.getMessage());
		}
		LOGGER.info("===============  CloverUrlUtil : Inside postOnClover :: End  ============= ");

		return responseBuilder.toString();
	}

	/**
	 * Create line item on clover
	 * 
	 * @param accessToken
	 * @param merchantPosId
	 * @param feeName
	 * 
	 * @param double1
	 * @return String
	 */
	public static String addDeliveryFeeLineItem(Double fees, String merchantPosId, String accessToken, String feeName,
			Integer isTaxable,Environment environment) {
		LOGGER.info("===============  CloverUrlUtil : Inside addDeliveryFeeLineItem :: Start  ============= ");

		HttpPost postRequest = new HttpPost(environment.getProperty("CLOVER_URL") + IConstant.CLOVER_INSTANCE_URL + merchantPosId
				+ "/items?access_token=" + accessToken);

		if (fees != null) {
			JSONObject obj = new JSONObject();
			obj.put("price", fees * 100);
			obj.put("name", feeName);
			obj.put("hidden", "true");
			if (isTaxable != null && isTaxable == 1)
				obj.put("defaultTaxRates", true);
			else
				obj.put("defaultTaxRates", false);
			LOGGER.info("===============  CloverUrlUtil : Inside addDeliveryFeeLineItem :: End  ============= ");

			return postOnClover(postRequest, obj.toString());
		}
		LOGGER.info("===============  CloverUrlUtil : Inside addDeliveryFeeLineItem :: End  ============= ");

		return null;
	}

	public static String updateDeliveryFeeLineItem(Double fees, String merchantPosId, String accessToken,
			String feeName, String lineItemId, Integer isTaxable,Environment environment) {
		LOGGER.info("===============  CloverUrlUtil : Inside updateDeliveryFeeLineItem :: Start  ============= ");

		HttpPost postRequest = new HttpPost(environment.getProperty("CLOVER_URL") + IConstant.CLOVER_INSTANCE_URL + merchantPosId
				+ "/items/" + lineItemId + "?access_token=" + accessToken);

		if (fees != null) {
			JSONObject obj = new JSONObject();
			obj.put("price", fees * 100);
			obj.put("name", feeName);
			obj.put("hidden", "true");
			if (isTaxable != null && isTaxable == 1)
				obj.put("defaultTaxRates", true);
			else
				obj.put("defaultTaxRates", false);

			String itemJson = obj.toString();
			LOGGER.info(" item === "+itemJson);
			LOGGER.info("===============  CloverUrlUtil : Inside updateDeliveryFeeLineItem :: End  ============= ");

			return postOnClover(postRequest, obj.toString());
		}
		LOGGER.info("===============  CloverUrlUtil : Inside updateDeliveryFeeLineItem :: End  ============= ");

		return null;
	}

	public static void applyCoupon(Double discount, Merchant merchant, String orderPosId,Environment environment) {
		LOGGER.info("===============  CloverUrlUtil : Inside applyCoupon :: Start  ============= ");

		double roundOff = discount * 100;
		long disCountAmount = new Double(roundOff).longValue();
		JSONObject obj = new JSONObject();
		obj.put("amount", "-" + disCountAmount);
		obj.put("percentage", "");
		obj.put("name", "Discount");
		JSONObject discountObj = new JSONObject();
		discountObj.put("id", "");
		obj.put("discount", discountObj);
		String jsonText = obj.toString();
		LOGGER.info("==== Discount ===== "+jsonText);
		HttpPost postRequest = new HttpPost(
				environment.getProperty("CLOVER_URL") + IConstant.CLOVER_INSTANCE_URL + merchant.getPosMerchantId() + "/orders/"
						+ orderPosId + "/discounts?access_token=" + merchant.getAccessToken());
		LOGGER.info(postOnClover(postRequest, jsonText));

	}

	public static String deleteItem(String merchantPosId, String accessToken, String lineItemId,Environment environment) {
		HttpDelete deleteRequest = new HttpDelete(environment.getProperty("CLOVER_URL") + IConstant.CLOVER_INSTANCE_URL + merchantPosId
				+ "/items/" + lineItemId + "?access_token=" + accessToken);
		LOGGER.info("===============  CloverUrlUtil : Inside deleteItem :: Start  ============= ");

		HttpClient client = HttpClientBuilder.create().build();
		HttpResponse response = null;
		try {

			response = client.execute(deleteRequest);
			HttpEntity entity = response.getEntity();
			String responseString = EntityUtils.toString(entity, "UTF-8");
			return responseString;
		} catch (ClientProtocolException e) {
			LOGGER.error("===============  CloverUrlUtil : Inside deleteItem :: Exception  ============= " + e);

			LOGGER.error("error: " + e.getMessage());
		} catch (IOException e) {
			LOGGER.error("===============  CloverUrlUtil : Inside deleteItem :: Exception  ============= " + e);

			LOGGER.error("error: " + e.getMessage());
		}
LOGGER.info("===============  CloverUrlUtil : Inside deleteItem :: End  ============= ");

		return null;
	}

	public static String deleteOrderType(String merchantPosId, String accessToken, String orderTypeId,Environment environment) {
		LOGGER.info("===============  CloverUrlUtil : Inside deleteOrderType :: Start  ============= ");

		HttpDelete deleteRequest = new HttpDelete(environment.getProperty("CLOVER_URL") + IConstant.CLOVER_INSTANCE_URL + merchantPosId
				+ "/order_types/" + orderTypeId + "?access_token=" + accessToken);

		HttpClient client = HttpClientBuilder.create().build();
		HttpResponse response = null;
		try {

			response = client.execute(deleteRequest);
			HttpEntity entity = response.getEntity();
			String responseString = EntityUtils.toString(entity, "UTF-8");
			return responseString;
		} catch (ClientProtocolException e) {
			LOGGER.error("===============  CloverUrlUtil : Inside deleteOrderType :: Exception  ============= " + e);

			// TODO Auto-generated catch block
			LOGGER.error("error: " + e.getMessage());
		} catch (IOException e) {
			LOGGER.error("===============  CloverUrlUtil : Inside deleteOrderType :: Exception  ============= " + e);

			// TODO Auto-generated catch block
			LOGGER.error("error: " + e.getMessage());
		}
LOGGER.info("===============  CloverUrlUtil : Inside deleteOrderType :: End  ============= ");

		return null;
	}

	public static String deleteOrder(String merchantPosId, String accessToken, String orderPOSId,Environment environment) {
		LOGGER.info("===============  CloverUrlUtil : Inside deleteOrder :: Start  ============= ");

		HttpDelete deleteRequest = new HttpDelete(environment.getProperty("CLOVER_URL") + IConstant.CLOVER_INSTANCE_URL + merchantPosId
				+ "/orders/" + orderPOSId + "?access_token=" + accessToken);

		HttpClient client = HttpClientBuilder.create().build();
		HttpResponse response = null;
		try {
			response = client.execute(deleteRequest);
			HttpEntity entity = response.getEntity();
			String responseString = EntityUtils.toString(entity, "UTF-8");
			return responseString;
		} catch (ClientProtocolException e) {
			LOGGER.error("===============  CloverUrlUtil : Inside deleteOrder :: Exception  ============= " + e);

			LOGGER.error("error: " + e.getMessage());
		} catch (IOException e) {
			LOGGER.error("===============  CloverUrlUtil : Inside deleteOrder :: Exception  ============= " + e);

			LOGGER.error("error: " + e.getMessage());
		}
LOGGER.info("===============  CloverUrlUtil : Inside deleteOrder :: End  ============= ");

		return null;
	}

	public static String createUpdateCustomerAddress(Clover clover, String addressJson, String customerPosId) {
		LOGGER.info("===============  CloverUrlUtil : Inside createUpdateCustomerAddress :: Start  ============= ");

		HttpPost postRequest = new HttpPost(clover.getUrl() + clover.getInstantUrl() + clover.getMerchantId()
				+ "/customers/" + customerPosId + "/addresses?access_token=" + clover.getAuthToken());
LOGGER.info("===============  CloverUrlUtil : Inside createUpdateCustomerAddress :: End  ============= ");

		return postOnClover(postRequest, addressJson);
	}

	public static void main(String[] args) {
		double roundOff = 2.3 * 100;
		long price = new Double(roundOff).longValue();
		LOGGER.info("price ==== "+price);
	}

	public static String getFeedbackClover(Clover clover,String orderId) {
LOGGER.info("===============  CloverUrlUtil : Inside getFeedbackClover :: Start  ============= ");
		HttpGet getRequest = new HttpGet(clover.getUrl() + clover.getInstantUrl() + clover.getMerchantId()
				+ "/orders/"+orderId+"?access_token=" + clover.getAuthToken());
		LOGGER.info("getRequest=== "+getRequest.toString());
		LOGGER.info("===============  CloverUrlUtil : Inside getFeedbackClover :: End  ============= ");

		return convertToStringJson(getRequest);

	}
	
	
	 public static String getPaymentDetails(Clover clover, String paymentId) {
		 LOGGER.info("===============  CloverUrlUtil : Inside getPaymentDetails :: Start  ============= ");

         //   String url = "https://apisandbox.dev.clover.com:443/v3/merchants/HV4QCRSDYE536/payments/R1V06P8W3JZ1R?expand=cardTransaction,order,tender";
            HttpGet request = new HttpGet(clover.getUrl() + clover.getInstantUrl() + clover.getMerchantId() + "/payments/"+paymentId+"?expand=cardTransaction,order,tender&access_token=" + clover.getAuthToken());
           LOGGER.info("===============  CloverUrlUtil : Inside getPaymentDetails :: End  ============= ");

            return convertToStringJson(request);
        }
	 
		public static String getFreekWentMerchantAccessToken(String code,Environment environment) {
			LOGGER.info("===============  CloverUrlUtil : Inside getFreekWentMerchantAccessToken :: Start  ============= code : "+code);

	        HttpGet request = new HttpGet(environment.getProperty("CLOVER_MAIN_URL")+"/oauth/token?client_id="+IConstant.FREEKWENT_APP_ID+"&client_secret="+IConstant.FREEKWENT_APP_SECRET_KEY+"&code="+code);
	        JSONObject jObject = new JSONObject(convertToStringJson(request));
	         if (jObject!=null && jObject.toString().contains("access_token") && jObject.has("access_token") ){
	             String accessToken=jObject.getString("access_token");
	            LOGGER.info("===============  CloverUrlUtil : Inside getFreekWentMerchantAccessToken :: End  ============= ");

	             return accessToken;
	         }else {
	        	 LOGGER.info("===============  CloverUrlUtil : Inside getFreekWentMerchantAccessToken :: End  ============= ");

	             return "Failed";
	         }
	        
	    }
}
