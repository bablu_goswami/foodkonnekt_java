package com.foodkonnekt.util;

public class TilesIConstant {

    // Application header and footer pages
    public static final String DEFAULT_LAYOUT = "/WEB-INF/layout/defaultLayout.jsp";
    public static final String TITLE = "FoodKonnekt";
    public static final String HEADER = "/WEB-INF/layout/header.jsp";
    public static final String FOOTER = "/WEB-INF/layout/footer.jsp";

    public static final String FRONT_HEADER = "/WEB-INF/layout/frontPageHeader.jsp";
    public static final String FRONT_FOOTER = "/WEB-INF/layout/frontPageFooter.jsp";

    // Application jsp pages
    public static final String HOME_PAGE = "/WEB-INF/pages/home.jsp";
    public static final String WELCOME_PAGE = "/WEB-INF/pages/welcome.jsp";
    public static final String CATEGORY_EMPTY = "/WEB-INF/pages/categoryEmpty.jsp";
    public static final String ORDER_PAGE = "/WEB-INF/pages/order.jsp";
    public static final String FOODTRONIX_ORDER_PAGE = "/WEB-INF/pages/foodTronixOrder.jsp";
    public static final String MY_ORDER_PAGE = "/WEB-INF/pages/myOrder.jsp";
    public static final String PROFILE_PAGE = "/WEB-INF/pages/profile.jsp";
    public static final String FORGOT_PAGE = "/WEB-INF/pages/forgotPassword.jsp";
    public static final String EXCEPTION_PAGE = "/WEB-INF/pages/exception.jsp";
    public static final String PAYMENT_FORM = "/WEB-INF/pages/PaymentForm.jsp";

    public static final String FEEDBACK = "/WEB-INF/pages/feedbackForm.jsp";
    public static final String FEEDBACK_RESPONSE = "/WEB-INF/pages/feedbackResponse.jsp";
    public static final String FEEDBACK_SHARE = "/WEB-INF/pages/feedbackshare.jsp";
    public static final String FEEDBACK_ORDER_ID = "/WEB-INF/pages/feedbackOrderId.jsp";
    public static final String FEEDBACK_ORDER_ID5 = "/WEB-INF/pages/feedbackOrderId5.jsp";
    public static final String FEEDBACK_ORDER_ID6 = "/WEB-INF/pages/feedbackOrderId6.jsp";
    public static final String FEEDBACK_ORDER_ID7 = "/WEB-INF/pages/feedbackOrderId7.jsp";
    public static final String FEEDBACK1 = "/WEB-INF/pages/feedbackForm1.jsp";

    public static final String ORDER_RECEIPT = "/WEB-INF/pages/OrderReceipt.jsp";

    // Admin part
    public static final String ADMIN_HEADER = "/WEB-INF/pages/adminHeader.jsp";
    public static final String ADMIN_FOOTER = "/WEB-INF/pages/adminFooter.jsp";
    public static final String CREATE_VOUCHAR = "/WEB-INF/pages/createVouchar.jsp";
    public static final String VOUCHARS = "/WEB-INF/pages/vouchars.jsp";
    public static final String ADMIN_HOME = "/WEB-INF/pages/adminHome.jsp";
    public static final String INVENTORY = "/WEB-INF/pages/inventory.jsp";
    public static final String ADD_LINE_ITEM = "/WEB-INF/pages/addLineItem.jsp";
     public static final String EDIT_MODIFIER = "/WEB-INF/pages/editModifier.jsp";
    public static final String EDIT_MODIFIER_GROUP = "/WEB-INF/pages/editModifierGroup.jsp";
    public static final String EDIT_CATEGORY = "/WEB-INF/pages/editCategory.jsp";
    public static final String ADMIN_LOGIN = "/WEB-INF/pages/adminLogin.jsp";
    public static final String ADD_CATEGORY = "/WEB-INF/pages/addCategory.jsp";
    public static final String ALL_ORDERS = "/WEB-INF/pages/allOrders.jsp";
    public static final String CUSTOMER_ORDERS = "/WEB-INF/pages/customerOrders.jsp";
    public static final String MERCHANT = "/WEB-INF/pages/merchants.jsp";
    public static final String MODIFIERS_GROUPS = "/WEB-INF/pages/modifierGroups.jsp";
    public static final String MODIFIERS = "/WEB-INF/pages/modifiers.jsp";
    public static final String CATEGORY = "/WEB-INF/pages/category.jsp";
    public static final String LOG_OUT = "/WEB-INF/pages/logOut.jsp";
    public static final String GET_ALL_MERCHANT = "/WEB-INF/pages/getAllMerchants.jsp";
    public static final String KRITIQ_MERCHANT_DETAIL = "/WEB-INF/pages/kritiqMerchantDetail.jsp";
    public static final String KRITIQ_CUSTOMER_DETAIL = "/WEB-INF/pages/kritiqCustomerDetail.jsp";
    public static final String GET_ALL_MERCHANT_BY_VENDOR = "/WEB-INF/pages/getAllMerchantsByVendor.jsp";
    public static final String UPLOAD_INVENTORY = "/WEB-INF/pages/uploadInventory.jsp";

    public static final String CATEGORY_ITEMS = "/WEB-INF/pages/categoryItems.jsp";

    public static final String DELIVERY_TIMINGS = "/WEB-INF/pages/deliveryTimings.jsp";

    // Configuration screens
    public static final String WELCOME = "/WEB-INF/pages/welcome.jsp";
    public static final String UPLOAD_LOGO = "/WEB-INF/pages/uploadLogo.jsp";
    public static final String SET_DELIVERY_ZONE = "/WEB-INF/pages/setDeliveryZone.jsp";
    public static final String SET_PICKUP_TIME = "/WEB-INF/pages/setPickupTime.jsp";
    public static final String SET_CONVENIENCE_FEE = "/WEB-INF/pages/setConvenienceFee.jsp";
    public static final String SET_ADMIN_PANEL = "/WEB-INF/pages/adminPanel.jsp";
    public static final String ONLINE_ORDER_LINK = "/WEB-INF/pages/onLineOrderLink.jsp";

    public static final String ADMIN_LOGO = "/WEB-INF/pages/adminLogo.jsp";

    public static final String ADMIN_DELIVERY_ZONE = "/WEB-INF/pages/addDeliveryZone.jsp";

    public static final String ADMIN_PICKTME = "/WEB-INF/pages/pickUpTime.jsp";

    public static final String ADMIN_CONFENCE_FEE = "/WEB-INF/pages/convenienceFee.jsp";
    public static final String CUSTOMER = "/WEB-INF/pages/customers.jsp";
    public static final String UPDATE_DELIVERY_ZONE = "/WEB-INF/pages/updateDeliveryZone.jsp";
    public static final String DELIVERY_ZONE = "/WEB-INF/pages/deliveryZones.jsp";
    public static final String LOGIN = "/WEB-INF/pages/login.jsp";
    public static final String FORGOT_PASSWORD = "/WEB-INF/pages/forgotpassword.jsp";
    public static final String CHANGE_PASSWORD = "/WEB-INF/pages/changepassword.jsp";
    public static final String MERCHANT_SIGN_UP = "/WEB-INF/pages/signup.jsp";

    public static final String UPDATE_CATEGORY = "/WEB-INF/pages/updateCategory.jsp";

    public static final String UPDATE_VOUCHAR = "/WEB-INF/pages/updateVouchar.jsp";
    public static final String GUEST_PASSWORD = "/WEB-INF/pages/setGuestPassword.jsp";
    public static final String RESET_PASSWORD = "/WEB-INF/pages/resetGuestPassword.jsp";
    public static final String RESET_ADMIN_PASSWORD = "/WEB-INF/pages/resetAdminPassword.jsp";
    public static final String ADMIN_SESSION_OUT = "/WEB-INF/pages/adminSessionTimeOut.jsp";
    public static final String FEEDBACK_WALKIN_CUSTOMER = "/WEB-INF/pages/feedbackFormWalkInCustomer.jsp";
    public static final String DISPLAY_CUSTOMER_FEEDBACK = "/WEB-INF/pages/displayCustomerFeedback.jsp";
    public static final String FEEDBACK_WALKIN_CUSTOMER5 = "/WEB-INF/pages/feedbackFormWalkInCustomer5.jsp";
    public static final String FEEDBACK_WALKIN_CUSTOMER6 = "/WEB-INF/pages/feedbackFormWalkInCustomer6.jsp";
    public static final String FEEDBACK_WALKIN_CUSTOMER7 = "/WEB-INF/pages/feedbackFormWalkInCustomer7.jsp";
    public static final String SET_PAYMENT_GATEWAY = "/WEB-INF/pages/setupPaymentGateway.jsp";
    public static final String SETPAYMENT_GATEWAY = "/WEB-INF/pages/setPaymentGateway.jsp";
    public static final String PIZZATAMPLATE = "/WEB-INF/pages/pizzaTamplate.jsp";
    public static final String PIZZATOPPINGS = "/WEB-INF/pages/pizzaTopping.jsp";
    public static final String EDITPIZZATEMPLATE = "/WEB-INF/pages/editPizzaTemplate.jsp";
    public static final String DELIVERYZONEWITHMAP = "/WEB-INF/pages/deliveryZonesWithMap.jsp";

    public static final String MULTILOCATIONKRITIQ = "/WEB-INF/pages/multiLocationKritiq.jsp";
    public static final String NOTIFICATION_METHOD_LINK = "/WEB-INF/pages/notificationMethodLink.jsp";

    public static final String RECEIPT = "/WEB-INF/pages/Receipt.jsp";
    public static final String MENUV2 = "/WEB-INF/pages/menuV2.jsp";
    public static final String HEADERV2 = "/WEB-INF/pages/headerV2.jsp";
    public static final String INFOV2 = "/WEB-INF/pages/infoV2.jsp";
    public static final String CARTV2 = "/WEB-INF/pages/cartV2.jsp";
    public static final String MYORDERV2 = "/WEB-INF/pages/myOrderV2.jsp";
    public static final String USERPROFILEV2 = "/WEB-INF/pages/userProfileV2.jsp";
    public static final String LOGINV2 = "/WEB-INF/pages/loginV2.jsp";
    public static final String FORGOTPASSWORDV2 = "/WEB-INF/pages/forgotPasswordV2.jsp";
    public static final String CHANGEPASSWORDV2 = "/WEB-INF/pages/changePasswordV2.jsp";
    public static final String SETGUESTPASSWORDV2 = "/WEB-INF/pages/setGuestPasswordV2.jsp";
    public static final String FOOTERV2 = "/WEB-INF/pages/footerV2.jsp";
    
    public static final String GETALLTAXES = "/WEB-INF/pages/merchantTaxes.jsp";
    public static final String EDITTAXRATES = "/WEB-INF/pages/editTaxRates.jsp";
    
    public static final String MERCHANTSLIDERS = "/WEB-INF/pages/merchantSliders.jsp";
    public static final String FREEKWENT_CLOVER = "/WEB-INF/pages/freekwentClover.jsp";
    
    public static final String INVENTORY_CATEGORY="/WEB-INF/pages/itemCategory.jsp";
    public static final String INVENTORY_MODIFIERGROUP= "/WEB-INF/pages/itemModifierGroup.jsp";
    public static final String MASTER_FORM_OF_ITEM_MODIFIRE_GROUP = "/WEB-INF/pages/masterFormOfitemModifierGroup.jsp";
    public static final String MASTER_FORM_ITEM_TEXES = "/WEB-INF/pages/masterFormOfItemTexes.jsp";
    
    public static final String CUSTOMER_FEEDBACK_FORM_NEW="/WEB-INF/pages/customerFeedbackNew.jsp";
    
    public static final String CREATE_ITEM="/WEB-INF/pages/createItem.jsp";
    public static final String CREATE_MODIFIER_GROUPS="/WEB-INF/pages/createModifierGroup.jsp";
    public static final String CREATE_CATEGORY = "/WEB-INF/pages/createCategory.jsp";
    public static final String CREATE_MODIFIER = "/WEB-INF/pages/createModifier.jsp";
    public static final String CREATE_PIZZA_TEMPLATE = "/WEB-INF/pages/createPizzaTemplate.jsp";
    public static final String PIZZA_TEMPLATE_MAPPING = "/WEB-INF/pages/pizzaTemplateMpping.jsp";
    public static final String CREATE_PIZZA_TOPPING = "/WEB-INF/pages/createPizzaTopping.jsp";
    public static final String PIZZA_CATEGORY_MAPPING = "/WEB-INF/pages/pizzaCategoryMap.jsp";
    public static final String EDIT_PIZZA_TOPPING = "/WEB-INF/pages/editPizzaTopping.jsp";
    public static final String PIZZA_CRUST = "/WEB-INF/pages/pizzaCrust.jsp";
    public static final String TEMPLATE_TAX_MAP = "/WEB-INF/pages/templateTaxMap.jsp";
    public static final String ADD_PRINTER = "/WEB-INF/pages/addPrinter.jsp";
    public static final String ADD_CLOUD_PRINTER = "/WEB-INF/pages/addCloudPrinter.jsp";
    public static final String DOWNLOAD_UPDATED_VERSION = "/WEB-INF/pages/desktopVerson.jsp";
    public static final String DOWNLOAD_CURRENT_AND_UPDATED_VERSION = "/WEB-INF/pages/currentAndUpdatedVerson.jsp";
  
    public static final String OFFLINECUSTOMERS = "/WEB-INF/pages/offlineCustomer.jsp";    
    
    public static final String CREATE_SIZE = "/WEB-INF/pages/createPizzaSize.jsp";
    
    public static final String CREATE_NEW_TAX = "/WEB-INF/pages/createNewTax.jsp";

    public static final String SET_ORDER_NOTIFICSTION = "/WEB-INF/pages/orderNotification.jsp";
    
    public static final String PIZZA_CATEGORY = "/WEB-INF/pages/pizzaCategory.jsp";
    
    public static final String EDIT_PIZZA_CRUST = "/WEB-INF/pages/editPizzaCrust.jsp";
    
    public static final String CREATE_PIZZA_CRUST = "/WEB-INF/pages/createPizzaCrust.jsp";
    
    public static final String TEMPLATE_CRUST_MAP = "/WEB-INF/pages/templateCrustMap.jsp";
    
    public static final String ITEM_TAX_MAP = "/WEB-INF/pages/itemTaxMap.jsp";
    
    public static final String PIZZA_SIZE = "/WEB-INF/pages/pizzaSize.jsp";
    
    public static final String EDIT_PIZZA_SIZE = "/WEB-INF/pages/editPizzaSize.jsp";

    public static final String USER = "/WEB-INF/pages/user.jsp";
    
    public static final String EDIT_USER = "/WEB-INF/pages/editUser.jsp";
    
    public static final String ADD_USER = "/WEB-INF/pages/addUser.jsp";
    
    public static final String UBER_STORE = "/WEB-INF/pages/uberStore.jsp";
    
    public static final String UBER_RECEIPT = "/WEB-INF/pages/uberReceipt.jsp";
    
    public static final String ENVIRONMENT = "/WEB-INF/pages/environment.jsp";
    
    public static final String VIRTUALFUND = "/WEB-INF/pages/virtualFund.jsp";
    
    public static final String ADD_VIRTUALFUND = "/WEB-INF/pages/addVirtualFund.jsp";
    
    public static final String EDIT_VIRTUALFUND = "/WEB-INF/pages/editVirtualFund.jsp";
    public static final String MERCHANT_SOCIAL_PLATFORM = "/WEB-INF/pages/merchantSocialPlatform.jsp";
    
    public static final String APP_STATUS = "/WEB-INF/pages/appStatus.jsp";
    
}
