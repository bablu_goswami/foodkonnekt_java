package com.foodkonnekt.util;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;

import com.google.api.client.auth.oauth2.AuthorizationCodeTokenRequest;
import com.google.api.client.auth.oauth2.ClientParametersAuthentication;
import com.google.api.client.auth.oauth2.RefreshTokenRequest;
import com.google.api.client.auth.oauth2.TokenResponse;
import com.google.api.client.auth.oauth2.TokenResponseException;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;

public class GoogleOAuthToken {

	private static final Logger LOGGER= LoggerFactory.getLogger(CloudPrintUtil.class);
	
	 public static final String CLIENT_ID = "939124860616-b6q4pjrq24bce3f6o0de6o3jljssv64t.apps.googleusercontent.com";
	 public static final String CLIENT_SECRET = "pFcX_WGp8XDfojaxzq6sMrH-";		
	
	public static final String REDIRECT_URI = "http://localhost:8080/foodkonnekt-admin/authCode";
	
	
	//url to recieve refresh token
	public  static final String TOKEN_SERVER_URL = "https://www.googleapis.com/oauth2/v4/token";
	
	//url to recieve auth code
	public  static final String AUTH_CODE_SERVER_URL = "https://accounts.google.com/o/oauth2/v2/auth";
	 
	 public static final String SCOPE = "https://www.googleapis.com/auth/cloudprint";
	 


//======================================REQUEST REFRESH TOKEN==============================//

public static String requestRefreshToken(String code) throws IOException {
	String refreshToken="";
	 
	    try {
	      TokenResponse response =
	          new AuthorizationCodeTokenRequest(new NetHttpTransport(), new JacksonFactory(),
	              new GenericUrl(TOKEN_SERVER_URL), code)
	              .setRedirectUri(REDIRECT_URI)
	              .setClientAuthentication(
	                  new ClientParametersAuthentication(CLIENT_ID, CLIENT_SECRET)).execute();
	      refreshToken = response.getRefreshToken();
          System.out.println("refresh token :" + refreshToken);
	    } catch (TokenResponseException e) {
	      if (e.getDetails() != null) {
	        System.err.println("Error: " + e.getDetails().getError());
	        if (e.getDetails().getErrorDescription() != null) {
	          System.err.println(e.getDetails().getErrorDescription());
	        }
	        if (e.getDetails().getErrorUri() != null) {
	          System.err.println(e.getDetails().getErrorUri());
	        }
	      } else {
	        System.err.println(e.getMessage());
	      }
	    }
		return refreshToken;
	  }
	
///===============================REFRESH ACCESS TOKEN===============================================================


private static String refreshAccessToken(String refreshToken) throws IOException {
	  String accessToken=null;
	    try {
	      TokenResponse response =
	          new RefreshTokenRequest(new NetHttpTransport(), new JacksonFactory(), new GenericUrl(
	              TOKEN_SERVER_URL), refreshToken)
	              .setClientAuthentication(
	                  new ClientParametersAuthentication(CLIENT_ID, CLIENT_SECRET)).execute();
	      
	     System.out.println("Access token: " + response.getAccessToken());
	      accessToken = response.getAccessToken();	    
	      
	    } catch (TokenResponseException e) {
	      if (e.getDetails() != null) {
	        System.err.println("Error: " + e.getDetails().getError());
	        if (e.getDetails().getErrorDescription() != null) {
	          System.err.println(e.getDetails().getErrorDescription());
	        }
	        if (e.getDetails().getErrorUri() != null) {
	          System.err.println(e.getDetails().getErrorUri());
	        }
	      } else {
	        System.err.println(e.getMessage());
	      }
	    }
	    
	    return accessToken;
	  }


//================================= Generate Access Token==========================================

public static String generateAccessToken(String refreshToken,Environment environment) {
	LOGGER.info("----------------Start :: GoogleOAuthToken : generateAccessToken------------------");
	LOGGER.info("GoogleOAuthToken : generateAccessToken : refreshToken "+refreshToken);
	String accessToken="";
	try {
		accessToken = refreshAccessToken(refreshToken);
		LOGGER.info("GoogleOAuthToken : generateAccessToken : accessToken "+accessToken);
	} catch (IOException e) {
		if(e != null){
			LOGGER.error("error: " + e.getMessage());
			LOGGER.error("GoogleOAuthToken : generateAccessToken : Exception "+e);
			MailSendUtil.sendExceptionByMail(e,environment);
		}
	}
	LOGGER.info("----------------End :: GoogleOAuthToken : generateAccessToken------------------");
	return accessToken;
}

	
}
