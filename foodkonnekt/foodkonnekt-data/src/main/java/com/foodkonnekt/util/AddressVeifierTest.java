package com.foodkonnekt.util;

import static com.foodkonnekt.util.ProducerUtil.convertToStringJson;

import org.apache.http.client.methods.HttpGet;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class AddressVeifierTest {
    
    
    public static void testReciept() {
        System.out.println("g");
        String string1 = "2020-09-04 20:55:19.0";
        string1 = string1.substring(10, 16);
        
        String[] tt = {"America/Chicago",
                "Pacific/Samoa",
                "US/Samoa",
                "Pacific/Honolulu",
                "America/Anchorage",
                "America/Los_Angeles",
                "America/Denver",
                "America/Phoenix",
                "America/New_York",
                "America/Puerto_Rico",
                "IST",
                "America/Detroit",
                "America/Indiana/Indianapolis",
                "America/Boise"
        };
        for(String t: tt){
            String startdate=DateUtil.getCurrentTimeForTimeZoneForReciept(t);
            string1 = startdate.substring(10, 16);
            
            System.out.println(startdate + " " + string1);
        }
        
        
    }
    public static void main(String[] args) throws UnsupportedEncodingException {
        
//        String add1 = "439 N WASHINGTON ST,";
//        String add2 = "";
//        String city = "KNIGHTSTOWN";
//        String state = "IN";
//        String zip5 = "46148";
//        String zip4 = "";
//
//        getResponse(add1, add2, city, state, zip5, zip4);
//        getResponse("15 North Washington Street", "", "KNIGHTSTOWN", "IN", "46148", "");
    
        testReciept();
    }
    
    private static void getResponse(String add1, String add2, String city, String state, String zip5, String zip4)
            throws UnsupportedEncodingException {
        String userId = "327MKONN4730";
        String encodeUrl = "<AddressValidateRequest USERID='" + userId + "'><Address>" +
                "<Address1>" + add1 + "</Address1> " +
                "<Address2>" + add2 + "</Address2> " +
                "<City>" + city + "</City>" +
                "<State>" + state + "</State>" +
                "<Zip5>" + zip5 + "</Zip5> " +
                "<Zip4>" + zip4 + "</Zip4>" +
                "</Address></AddressValidateRequest>";
        HttpGet request = new HttpGet("http://production.shippingapis.com/ShippingAPI.dll?API=Verify&XML="
                                              + URLEncoder.encode(encodeUrl, "UTF-8"));
        System.out.println(convertToStringJson(request));
    }
    
}
