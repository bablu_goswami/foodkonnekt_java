package com.foodkonnekt.util;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.core.env.Environment;
import net.authorize.api.contract.v1.*;
import net.authorize.api.controller.base.ApiOperationBase;
import net.authorize.api.controller.CreateCustomerProfileController;
import net.authorize.api.controller.CreateTransactionController;
import net.authorize.api.controller.GetCustomerProfileController;
public class AuthorizeTest {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(AuthorizeTest.class);

	public static void main(String[] args) {
		
		/*String a ="2018";
		LOGGER.info("last character: " +
                a.substring(a.length() - 2)); */
		//authorizeCreditCard();
		//captureCreditCard();
		//40011075497
		//voidTransaction("957GpXaUX","832DbfKv2z6Ew59r","40011103892");
		
		//createCustomerProfile("957GpXaUX","832DbfKv2z6Ew59r", "abc@gmail.com");
		//getCustomerProfileById("957GpXaUX","832DbfKv2z6Ew59r","1503118872");
	//	refundTransaction("957GpXaUX","832DbfKv2z6Ew59r",500.00,"40011076675");
		// TODO Auto-generated method stub

	}
	 public static void authorizeCreditCard(Environment environment) {
         LOGGER.info("AuthorizeTest : inside authorizeCreditCard : start ======= ");

		 //Common code to set for all requests
	        ApiOperationBase.setEnvironment(ProducerUtil.getAuthorizeEnv(environment.getProperty("AuthorizeEnv")));

	        MerchantAuthenticationType merchantAuthenticationType  = new MerchantAuthenticationType() ;
	        merchantAuthenticationType.setName("957GpXaUX");
	        merchantAuthenticationType.setTransactionKey("832DbfKv2z6Ew59r");
	        ApiOperationBase.setMerchantAuthentication(merchantAuthenticationType);

	        // Populate the payment data
	        PaymentType paymentType = new PaymentType();
	        CreditCardType creditCard = new CreditCardType();
	        creditCard.setCardNumber("4242424242424242");
	        creditCard.setExpirationDate("0822");
	        paymentType.setCreditCard(creditCard);

	        // Create the payment transaction request
	        TransactionRequestType txnRequest = new TransactionRequestType();
	        txnRequest.setTransactionType(TransactionTypeEnum.AUTH_ONLY_TRANSACTION.value());
	        txnRequest.setPayment(paymentType);
	        txnRequest.setAmount(new BigDecimal(500.00));

	        // Make the API Request
	        CreateTransactionRequest apiRequest = new CreateTransactionRequest();
	        apiRequest.setTransactionRequest(txnRequest);
	        CreateTransactionController controller = new CreateTransactionController(apiRequest);
	        controller.execute();


	        CreateTransactionResponse response = controller.getApiResponse();

	        if (response!=null) {

	            // If API Response is ok, go ahead and check the transaction response
	            if (response.getMessages().getResultCode() == MessageTypeEnum.OK) {

	                TransactionResponse result = response.getTransactionResponse();
	                LOGGER.info("AuthorizeTest : inside authorizeCreditCard : TransactionResponse : ");
	                if (result.getResponseCode().equals("1")) {
	                	LOGGER.info(result.toString());
	                    LOGGER.info(" ResponseCode === "+result.getResponseCode());
	                    LOGGER.info("Successful Credit Card Transaction");
	                    LOGGER.info(" AuthCode === "+result.getAuthCode());
	                    LOGGER.info(" TransId === "+result.getTransId()); //40009206593
	                    
	                }
	                else
	                {
	                    LOGGER.info("Failed Transaction"+result.getResponseCode());
	                }
	            }
	            else
	            {
	                LOGGER.info("Failed Transaction:  "+response.getMessages().getResultCode());
	            }
	        }
	 }
	 
	 
	 
	 public static void captureCreditCard(Environment environment) {
         LOGGER.info("AuthorizeTest : inside captureCreditCard : start ======= ");

		 //Common code to set for all requests
	        ApiOperationBase.setEnvironment(ProducerUtil.getAuthorizeEnv(environment.getProperty("AuthorizeEnv")));

	        MerchantAuthenticationType merchantAuthenticationType  = new MerchantAuthenticationType() ;
	        merchantAuthenticationType.setName("957GpXaUX");
	        merchantAuthenticationType.setTransactionKey("832DbfKv2z6Ew59r");
	        ApiOperationBase.setMerchantAuthentication(merchantAuthenticationType);

	        TransactionRequestType txnRequest = new TransactionRequestType();
	        txnRequest.setTransactionType(TransactionTypeEnum.PRIOR_AUTH_CAPTURE_TRANSACTION.value());
	        txnRequest.setRefTransId("40011076675"); //id recieved from previous authorizeCreditcard method

	       

	        // Make the API Request
	        CreateTransactionRequest apiRequest = new CreateTransactionRequest();
	        apiRequest.setTransactionRequest(txnRequest);
	        CreateTransactionController controller = new CreateTransactionController(apiRequest);
	        controller.execute(); 
	       


	        CreateTransactionResponse response = controller.getApiResponse();

	        if (response!=null) {

	            // If API Response is ok, go ahead and check the transaction response
	            if (response.getMessages().getResultCode() == MessageTypeEnum.OK) {

	                TransactionResponse result = response.getTransactionResponse();
	                if (result.getResponseCode().equals("1")) {
	                	LOGGER.info(result.toString());
	                	LOGGER.info(" ResponseCode === "+result.getResponseCode());
	                    LOGGER.info("Successful Credit Card Transaction");
	                    LOGGER.info(" AuthCode === "+result.getAuthCode());
	                    LOGGER.info(" TransId === "+result.getTransId()); //40009206593
	                }
	                else
	                {
	                    LOGGER.info("Failed Transaction"+result.getResponseCode());
	                }
	            }
	            else
	            {
	                LOGGER.info("Failed Transaction:  "+response.getMessages().getResultCode());
	            }
	        }
	 }
	 
	 
	 
	 
	 public static ANetApiResponse voidTransaction(String apiLoginId, String transactionKey, String transactionID,Environment environment) {
         LOGGER.info("AuthorizeTest : inside voidTransaction : start ======= ");

	        //Common code to set for all requests
	        ApiOperationBase.setEnvironment(ProducerUtil.getAuthorizeEnv(environment.getProperty("AuthorizeEnv")));

	        MerchantAuthenticationType merchantAuthenticationType  = new MerchantAuthenticationType() ;
	        merchantAuthenticationType.setName(apiLoginId);
	        merchantAuthenticationType.setTransactionKey(transactionKey);
	        ApiOperationBase.setMerchantAuthentication(merchantAuthenticationType);

	        // Create the payment transaction request
	        TransactionRequestType txnRequest = new TransactionRequestType();
	        txnRequest.setTransactionType(TransactionTypeEnum.VOID_TRANSACTION.value());
	        txnRequest.setRefTransId(transactionID);

	        // Make the API Request
	        CreateTransactionRequest apiRequest = new CreateTransactionRequest();
	        apiRequest.setTransactionRequest(txnRequest);
	        CreateTransactionController controller = new CreateTransactionController(apiRequest);
	        controller.execute(); 

	        CreateTransactionResponse response = controller.getApiResponse();

	        if (response!=null) {
	        	// If API Response is ok, go ahead and check the transaction response
	        	if (response.getMessages().getResultCode() == MessageTypeEnum.OK) {
	        		TransactionResponse result = response.getTransactionResponse();
	        		if (result.getMessages() != null) {
	        			LOGGER.info("Successfully created transaction with Transaction ID: " + result.getTransId());
	        			LOGGER.info("Response Code: " + result.getResponseCode());
	        			LOGGER.info("Message Code: " + result.getMessages().getMessage().get(0).getCode());
	        			LOGGER.info("Description: " + result.getMessages().getMessage().get(0).getDescription());
	        			LOGGER.info("Auth Code: " + result.getAuthCode());
	        		}
	        		else {
	        			LOGGER.info("Failed Transaction.");
	        			if (response.getTransactionResponse().getErrors() != null) {
	        				LOGGER.info("Error Code: " + response.getTransactionResponse().getErrors().getError().get(0).getErrorCode());
	        				LOGGER.info("Error message: " + response.getTransactionResponse().getErrors().getError().get(0).getErrorText());
	        			}
	        		}
	        	}
	        	else {
	        		LOGGER.info("Failed Transaction.");
	        		if (response.getTransactionResponse() != null && response.getTransactionResponse().getErrors() != null) {
	        			LOGGER.info("Error Code: " + response.getTransactionResponse().getErrors().getError().get(0).getErrorCode());
	        			LOGGER.info("Error message: " + response.getTransactionResponse().getErrors().getError().get(0).getErrorText());
	        		}
	        		else {
	        			LOGGER.info("Error Code: " + response.getMessages().getMessage().get(0).getCode());
	        			LOGGER.info("Error message: " + response.getMessages().getMessage().get(0).getText());
	        		}
	        	}
	        }
	        else {
	        	LOGGER.info("Null Response.");
	        }
	        
			return response;

	    }
	 
	 
	 public static ANetApiResponse refundTransaction(String apiLoginId, String transactionKey, Double transactionAmount, String transactionID,String creditCardNumber,String expiryDate,Environment environment) {
         LOGGER.info("AuthorizeTest : inside refundTransaction : start ======= ");

	        //Common code to set for all requests
	        ApiOperationBase.setEnvironment(ProducerUtil.getAuthorizeEnv(environment.getProperty("AuthorizeEnv")));

	        MerchantAuthenticationType merchantAuthenticationType  = new MerchantAuthenticationType() ;
	        merchantAuthenticationType.setName(apiLoginId);
	        merchantAuthenticationType.setTransactionKey(transactionKey);
	        ApiOperationBase.setMerchantAuthentication(merchantAuthenticationType);

	        // Create a payment object, last 4 of the credit card and expiration date are required
	        PaymentType paymentType = new PaymentType();
	        CreditCardType creditCard = new CreditCardType();
	        creditCard.setCardNumber(creditCardNumber);
	        creditCard.setExpirationDate(expiryDate);
	        paymentType.setCreditCard(creditCard);

	        // Create the payment transaction request
	        TransactionRequestType txnRequest = new TransactionRequestType();
	        txnRequest.setTransactionType(TransactionTypeEnum.REFUND_TRANSACTION.value());
	        txnRequest.setRefTransId(transactionID);
	        txnRequest.setAmount(new BigDecimal(transactionAmount.toString()));
	        txnRequest.setPayment(paymentType);

	        // Make the API Request
	        CreateTransactionRequest apiRequest = new CreateTransactionRequest();
	        apiRequest.setTransactionRequest(txnRequest);
	        CreateTransactionController controller = new CreateTransactionController(apiRequest);
	        controller.execute(); 

	        CreateTransactionResponse response = controller.getApiResponse();

	        if (response!=null) {
	        	// If API Response is ok, go ahead and check the transaction response
	        	if (response.getMessages().getResultCode() == MessageTypeEnum.OK) {
	        		TransactionResponse result = response.getTransactionResponse();
	        		if (result.getMessages() != null) {
	        			LOGGER.info("Successfully created transaction with Transaction ID: " + result.getTransId());
	        			LOGGER.info("Response Code: " + result.getResponseCode());
	        			LOGGER.info("Message Code: " + result.getMessages().getMessage().get(0).getCode());
	        			LOGGER.info("Description: " + result.getMessages().getMessage().get(0).getDescription());
	        			LOGGER.info("Auth Code: " + result.getAuthCode());
	                } else {
	        			LOGGER.info("Failed Transaction.");
	        			if (response.getTransactionResponse().getErrors() != null) {
	        				LOGGER.info("Error Code: " + response.getTransactionResponse().getErrors().getError().get(0).getErrorCode());
	        				LOGGER.info("Error message: " + response.getTransactionResponse().getErrors().getError().get(0).getErrorText());
	        			}
	        		}
	            } else {
	        		LOGGER.info("Failed Transaction.");
	        		if (response.getTransactionResponse() != null && response.getTransactionResponse().getErrors() != null) {
	        			LOGGER.info("Error Code: " + response.getTransactionResponse().getErrors().getError().get(0).getErrorCode());
	        			LOGGER.info("Error message: " + response.getTransactionResponse().getErrors().getError().get(0).getErrorText());
	                } else {
	        			LOGGER.info("Error Code: " + response.getMessages().getMessage().get(0).getCode());
	        			LOGGER.info("Error message: " + response.getMessages().getMessage().get(0).getText());
	        		}
	        	}
	        } else {
	        	LOGGER.info("Null Response.");
	        }
	        
			return response;

	    }
	 
	 
	 
	 public static ANetApiResponse createCustomerProfile(String apiLoginId, String transactionKey, String eMail,Environment environment) {
         LOGGER.info("AuthorizeTest : inside createCustomerProfile : start ======= ");

	        // Set the request to operate in either the sandbox or production environment
	        ApiOperationBase.setEnvironment(ProducerUtil.getAuthorizeEnv(environment.getProperty("AuthorizeEnv")));
	        
	        // Create object with merchant authentication details
	        MerchantAuthenticationType merchantAuthenticationType  = new MerchantAuthenticationType() ;
	        merchantAuthenticationType.setName(apiLoginId);
	        merchantAuthenticationType.setTransactionKey(transactionKey);

	        // Populate the payment data
	        CreditCardType creditCard = new CreditCardType();
	        creditCard.setCardNumber("4111111111111111");
	        creditCard.setExpirationDate("1220");
	        PaymentType paymentType = new PaymentType();
	        paymentType.setCreditCard(creditCard);

	        // Set payment profile data
	        CustomerPaymentProfileType customerPaymentProfileType = new CustomerPaymentProfileType();
	        customerPaymentProfileType.setCustomerType(CustomerTypeEnum.INDIVIDUAL);
	        customerPaymentProfileType.setPayment(paymentType);

	        // Set customer profile data
	        CustomerProfileType customerProfileType = new CustomerProfileType();
	        customerProfileType.setMerchantCustomerId("M_" + eMail);
	        customerProfileType.setDescription("Profile description for " + eMail);
	        customerProfileType.setEmail(eMail);
	        customerProfileType.getPaymentProfiles().add(customerPaymentProfileType);

	        // Create the API request and set the parameters for this specific request
	        CreateCustomerProfileRequest apiRequest = new CreateCustomerProfileRequest();
	        apiRequest.setMerchantAuthentication(merchantAuthenticationType);
	        apiRequest.setProfile(customerProfileType);
	        apiRequest.setValidationMode(ValidationModeEnum.TEST_MODE);

	        // Call the controller
	        CreateCustomerProfileController controller = new CreateCustomerProfileController(apiRequest);
	        controller.execute();

	        // Get the response
	        CreateCustomerProfileResponse response = new CreateCustomerProfileResponse();
	        response = controller.getApiResponse();
	        
	        // Parse the response to determine results
	        if (response!=null) {
	            // If API Response is OK, go ahead and check the transaction response
	            if (response.getMessages().getResultCode() == MessageTypeEnum.OK) {
	                LOGGER.info("customer profile Id==="+response.getCustomerProfileId());
	                if (!response.getCustomerPaymentProfileIdList().getNumericString().isEmpty()) {
	                    LOGGER.info(response.getCustomerPaymentProfileIdList().getNumericString().get(0));
	                }
	                if (!response.getCustomerShippingAddressIdList().getNumericString().isEmpty()) {
	                    LOGGER.info(response.getCustomerShippingAddressIdList().getNumericString().get(0));
	                }
	                if (!response.getValidationDirectResponseList().getString().isEmpty()) {
	                    LOGGER.info(response.getValidationDirectResponseList().getString().get(0));
	                }
	            }
	            else
	            {
	                LOGGER.info("Failed to create customer profile:  " + response.getMessages().getResultCode());
	            }
	        } else {
	            // Display the error code and message when response is null 
	            ANetApiResponse errorResponse = controller.getErrorResponse();
	            LOGGER.info("Failed to get response");
	            if (!errorResponse.getMessages().getMessage().isEmpty()) {
	                LOGGER.info("Error: "+errorResponse.getMessages().getMessage().get(0).getCode()+" \n"+ errorResponse.getMessages().getMessage().get(0).getText());
	            }
	        }
	        return response;

	    }

	 
	 public static ANetApiResponse getCustomerProfileById(String apiLoginId, String transactionKey, String customerProfileId,Environment environment) {
         LOGGER.info("AuthorizeTest : inside getCustomerProfileById : start ======= ");

	        ApiOperationBase.setEnvironment(ProducerUtil.getAuthorizeEnv(environment.getProperty("AuthorizeEnv")));

	        MerchantAuthenticationType merchantAuthenticationType  = new MerchantAuthenticationType() ;
	        merchantAuthenticationType.setName(apiLoginId);
	        merchantAuthenticationType.setTransactionKey(transactionKey);
	        ApiOperationBase.setMerchantAuthentication(merchantAuthenticationType);

	        GetCustomerProfileRequest apiRequest = new GetCustomerProfileRequest();
	        apiRequest.setCustomerProfileId(customerProfileId);

	        GetCustomerProfileController controller = new GetCustomerProfileController(apiRequest);
	        controller.execute();
	       
			GetCustomerProfileResponse response = new GetCustomerProfileResponse();
			response = controller.getApiResponse();

			if (response!=null) {

	            if (response.getMessages().getResultCode() == MessageTypeEnum.OK) {

	 				LOGGER.info(" Code=== "+response.getMessages().getMessage().get(0).getCode());
	                LOGGER.info("Text==="+response.getMessages().getMessage().get(0).getText());
	                
	                LOGGER.info(" MerchantCustomerId== "+response.getProfile().getMerchantCustomerId());
	                LOGGER.info(" Description== "+response.getProfile().getDescription());
	                LOGGER.info(" Email=== "+response.getProfile().getEmail());
	                LOGGER.info(" CustomerProfileId==== "+response.getProfile().getCustomerProfileId());

	                if ((!response.getProfile().getPaymentProfiles().isEmpty()) &&
	                		(response.getProfile().getPaymentProfiles().get(0).getBillTo() != null)) {
	                    LOGGER.info("FirstName ==== "+response.getProfile().getPaymentProfiles().get(0).getBillTo().getFirstName());
	                    LOGGER.info("LastName ==== "+response.getProfile().getPaymentProfiles().get(0).getBillTo().getLastName());
	                    LOGGER.info("company ==== "+response.getProfile().getPaymentProfiles().get(0).getBillTo().getCompany());
	                    LOGGER.info("Address ==== "+response.getProfile().getPaymentProfiles().get(0).getBillTo().getAddress());
	                    LOGGER.info("City ==== "+response.getProfile().getPaymentProfiles().get(0).getBillTo().getCity());
	                    LOGGER.info("State ==== "+response.getProfile().getPaymentProfiles().get(0).getBillTo().getState());
	                    LOGGER.info("Zip ==== "+response.getProfile().getPaymentProfiles().get(0).getBillTo().getZip());
	                    LOGGER.info("Country ==== "+response.getProfile().getPaymentProfiles().get(0).getBillTo().getCountry());
	                    LOGGER.info("PhoneNumber ==== "+response.getProfile().getPaymentProfiles().get(0).getBillTo().getPhoneNumber());
	                    LOGGER.info("FaxNumber ==== "+response.getProfile().getPaymentProfiles().get(0).getBillTo().getFaxNumber());

	                    LOGGER.info("CustomerPaymentProfileId ==== "+response.getProfile().getPaymentProfiles().get(0).getCustomerPaymentProfileId());

	                    LOGGER.info("CardNumber ==== "+response.getProfile().getPaymentProfiles().get(0).getPayment().getCreditCard().getCardNumber());
	                    LOGGER.info("ExpirationDate ==== "+response.getProfile().getPaymentProfiles().get(0).getPayment().getCreditCard().getExpirationDate());
	                }

	                if (!response.getProfile().getShipToList().isEmpty()) {
	                    LOGGER.info("FirstName ==== "+response.getProfile().getShipToList().get(0).getFirstName());
	                    LOGGER.info("LastName ==== "+response.getProfile().getShipToList().get(0).getLastName());
	                    LOGGER.info("company ==== "+response.getProfile().getShipToList().get(0).getCompany());
	                    LOGGER.info("Address ==== "+response.getProfile().getShipToList().get(0).getAddress());
	                    LOGGER.info("City ==== "+response.getProfile().getShipToList().get(0).getCity());
	                    LOGGER.info("State ==== "+response.getProfile().getShipToList().get(0).getState());
	                    LOGGER.info("Zip ==== "+response.getProfile().getShipToList().get(0).getZip());
	                    LOGGER.info("Country ==== "+response.getProfile().getShipToList().get(0).getCountry());
	                    LOGGER.info("PhoneNumber ==== "+response.getProfile().getShipToList().get(0).getPhoneNumber());
	                    LOGGER.info("FaxNumber ==== "+response.getProfile().getShipToList().get(0).getFaxNumber());
	                }
	                
	                if ((response.getSubscriptionIds() != null) && (response.getSubscriptionIds().getSubscriptionId() != null) && 
	                		(!response.getSubscriptionIds().getSubscriptionId().isEmpty())) {
	                	LOGGER.info("List of subscriptions:");
	                	for (String subscriptionid : response.getSubscriptionIds().getSubscriptionId())
	                		LOGGER.info(subscriptionid);
	                }

	            } else {
	                LOGGER.info("Failed to get customer profile:  " + response.getMessages().getResultCode());
	            }
	        }
			return response;
	    }

}
