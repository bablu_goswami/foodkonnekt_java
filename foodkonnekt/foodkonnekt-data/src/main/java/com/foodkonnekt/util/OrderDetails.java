package com.foodkonnekt.util;

public class OrderDetails 
{
	private String OrderNumber;
	private int Amount;
	private int CurrencyCode;
	
	
	public String getOrderNumber() {
		return OrderNumber;
	}
	public void setOrderNumber(String orderNumber) {
		OrderNumber = orderNumber;
	}
	public int getAmount() {
		return Amount;
	}
	public void setAmount(int amount) {
		Amount = amount;
	}
	public int getCurrencyCode() {
		return CurrencyCode;
	}
	public void setCurrencyCode(int currencyCode) {
		CurrencyCode = currencyCode;
	}

}
