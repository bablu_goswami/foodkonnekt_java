package com.foodkonnekt.util;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.stripe.Stripe;
import com.stripe.model.Balance;
import com.stripe.model.BalanceTransaction;
import com.stripe.model.BalanceTransactionCollection;
import com.stripe.model.Charge;
import com.stripe.model.Customer;
import com.stripe.model.Refund;

public class StripeTests {
	private static final Logger LOGGER = LoggerFactory.getLogger(StripeTests.class);

	public static void main(String[] args) {
		Stripe.apiKey = "sk_test_ytGbEHvrNgJPs03sPJvXKP6j";

//chargeRequest();
//captureRequest();
	refundRequest();
		// retriveChargesByChargeId();
		// retriveBalance();
		// retriveBalanceByTransactionId();
		// retriveAllBalanceHistory();
		// createCustomer();
		// retrieveCustomerByCustomerid();

	}

	public static Charge captureRequest() {
		LOGGER.info("===============  StripeTests : Inside captureRequest :: Start  ============= ");

		Charge ch = null;
		try {
			ch = Charge.retrieve("ch_1C4rW6EOUDb088Q9JRuyT14L");

			ch.capture();
			System.out.println(ch);
		} catch (Exception e) {
			LOGGER.error("===============  StripeTests : Inside captureRequest :: Exception  ============= " + e);

			// TODO Auto-generated catch block
			LOGGER.error("error: " + e.getMessage());
		}
		LOGGER.info("===============  StripeTests : Inside captureRequest :: End  ============= ");

		return ch;

	}

	public static Refund refundRequest() {
		LOGGER.info("===============  StripeTests : Inside refundRequest :: Start  ============= ");

		Refund r = null;
		Map<String, Object> refundParams = new HashMap<String, Object>();
		//refundParams.put("charge", "ch_1C4rW6EOUDb088Q9JRuyT14L");
		refundParams.put("amount", 1500);

		try {
			r = Refund.retrieve("ch_1C4rogEOUDb088Q9K5CyDTZQ");

			System.out.println(r);
		} catch (Exception e) {
			LOGGER.error("===============  StripeTests : Inside refundRequest :: Exception  ============= " + e);

			// TODO Auto-generated catch block
			LOGGER.error("error: " + e.getMessage());
		}
		LOGGER.info("===============  StripeTests : Inside refundRequest :: End  ============= ");

		return r;
	}

	public static Charge chargeRequest() {
		LOGGER.info("===============  StripeTests : Inside chargeRequest :: Start  ============= ");

		Charge c = null;
		try {
			Map<String, Object> chargeParams = new HashMap<String, Object>();
			chargeParams.put("amount", 500);
			chargeParams.put("currency", "usd");
			chargeParams.put("source", "tok_mastercard");
			chargeParams.put("capture", false);
			// ^ obtained with Stripe.js
			Map<String, String> initialMetadata = new HashMap<String, String>();
			initialMetadata.put("order_id", "6735");
			chargeParams.put("metadata", initialMetadata);

			c = Charge.create(chargeParams);
			System.out.println(c);

		} catch (Exception e) {
			LOGGER.error("===============  StripeTests : Inside chargeRequest :: Exception  ============= " + e);

			// TODO Auto-generated catch block
			LOGGER.error("error: " + e.getMessage());

		}
		LOGGER.info("===============  StripeTests : Inside chargeRequest :: End  ============= ");

		return c;
	}

	public static Charge retriveChargesByChargeId() {
		LOGGER.info("===============  StripeTests : Inside retriveChargesByChargeId :: Start  ============= ");

		Charge c = null;
		try {
			// charge ids--
			// ch_1BbPALEOUDb088Q9qsLwJMHR4
			// ch_1BbSTpEOUDb088Q92PnQS8KI
			// ch_1BbPALEOUDb088Q9qsLwJMHR
			List<String> expandList = new LinkedList<String>();
			expandList.add("customer");
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("expand", expandList);
			c = Charge.retrieve("ch_1BbSTpEOUDb088Q92PnQS8KI", params, null);
			System.out.println(c);
		} catch (Exception e) {
			LOGGER.error("===============  StripeTests : Inside retriveChargesByChargeId :: Exception  ============= " + e);

			LOGGER.error("error: " + e.getMessage());
		}
		LOGGER.info("===============  StripeTests : Inside retriveChargesByChargeId :: End  ============= ");

		return c;
	}

	public static Balance retriveBalance() {
		LOGGER.info("===============  StripeTests : Inside retriveBalance :: Start  ============= ");

		Balance b = null;
		try {
			b = Balance.retrieve();
			System.out.println(b);
		} catch (Exception e) {
			LOGGER.error("===============  StripeTests : Inside retriveBalance :: Exception  ============= " + e);

			// TODO Auto-generated catch block
			LOGGER.error("error: " + e.getMessage());
		}
		LOGGER.info("===============  StripeTests : Inside retriveBalance :: End  ============= ");

		return b;
	}

	public static BalanceTransaction retriveBalanceByTransactionId() {
		LOGGER.info("===============  StripeTests : Inside retriveBalanceByTransactionId :: Start  ============= ");

		BalanceTransaction b = null;
		try {
			b = BalanceTransaction.retrieve("txn_1BbP9AEOUDb088Q99gtg37qy");
			System.out.println(b);
		} catch (Exception e) {
			LOGGER.error("===============  StripeTests : Inside retriveBalanceByTransactionId :: Exception  ============= " + e);

			// TODO Auto-generated catch block
			LOGGER.error("error: " + e.getMessage());
		}
		LOGGER.info("===============  StripeTests : Inside retriveBalanceByTransactionId :: End  ============= ");

		return b;
	}

	public static BalanceTransactionCollection retriveAllBalanceHistory() {
		BalanceTransactionCollection b = null;
		try {
			LOGGER.info("===============  StripeTests : Inside retriveAllBalanceHistory :: Start  ============= ");

			Map<String, Object> balanceTransactionParams = new HashMap<String, Object>();
			balanceTransactionParams.put("limit", 3);
			b = BalanceTransaction.all(balanceTransactionParams);
			System.out.println(b);
		} catch (Exception e) {
			LOGGER.error("===============  StripeTests : Inside retriveAllBalanceHistory :: Exception  ============= " + e);

			// TODO Auto-generated catch block
			LOGGER.error("error: " + e.getMessage());
		}
		LOGGER.info("===============  StripeTests : Inside retriveAllBalanceHistory :: End  ============= ");

		return b;
	}

	public static Charge updateCharges() {
		Charge c = null;
		try {
			LOGGER.info("===============  StripeTests : Inside updateCharges :: Start  ============= ");

			Charge ch = Charge.retrieve("ch_1BbSTpEOUDb088Q92PnQS8KI");
			Map<String, Object> updateParams = new HashMap<String, Object>();
			updateParams.put("description", "Charge for 22arpitmaru@gmail.com");

			c = ch.update(updateParams);
		} catch (Exception e) {
			LOGGER.error("===============  StripeTests : Inside updateCharges :: Exception  ============= " + e);

			LOGGER.error("error: " + e.getMessage());
		}
		LOGGER.info("===============  StripeTests : Inside updateCharges :: End  ============= ");

		return c;
	}

	public static Customer createCustomer() {
		Customer c = null;
		try {
			LOGGER.info("===============  StripeTests : Inside createCustomer :: Start  ============= ");

			Map<String, Object> customerParams = new HashMap<String, Object>();
			customerParams.put("description", "Customer for 22arpitmaru@gmail.com");
			customerParams.put("source", "tok_amex");
			// ^ obtained with Stripe.js
			c = Customer.create(customerParams);
			System.out.println(c);
		} catch (Exception e) {
			LOGGER.error("===============  StripeTests : Inside createCustomer :: Exception  ============= " + e);

			LOGGER.error("error: " + e.getMessage());
		}
		LOGGER.info("===============  StripeTests : Inside createCustomer :: End  ============= ");

		return c;
	}

	public static Customer retrieveCustomerByCustomerid() {
		Customer c = null;
		try {
			LOGGER.info("===============  StripeTests : Inside retrieveCustomerByCustomerid :: Start  ============= ");

			c = Customer.retrieve("cus_BzTC3ypznRfLjr");
			System.out.println(c);
		} catch (Exception e) {
			LOGGER.error("===============  StripeTests : Inside retrieveCustomerByCustomerid :: Exception  ============= " + e);

			LOGGER.error("error: " + e.getMessage());
		}
		LOGGER.info("===============  StripeTests : Inside retrieveCustomerByCustomerid :: End  ============= ");

		return c;
	}

}
