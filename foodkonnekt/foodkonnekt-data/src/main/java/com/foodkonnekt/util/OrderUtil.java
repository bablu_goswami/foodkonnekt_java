package com.foodkonnekt.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;

import com.foodkonnekt.clover.vo.CloverDiscountVO;
import com.foodkonnekt.clover.vo.CloverOrderVO;
import com.foodkonnekt.clover.vo.Employee;
import com.foodkonnekt.clover.vo.Item;
import com.foodkonnekt.clover.vo.Modifications;
import com.foodkonnekt.clover.vo.Modifier;
import com.foodkonnekt.clover.vo.ModifierVO;
import com.foodkonnekt.clover.vo.OrderItemVO;
import com.foodkonnekt.clover.vo.OrderTypeVO;
import com.foodkonnekt.clover.vo.OrderVO;
import com.foodkonnekt.clover.vo.PaymentVO;
import com.foodkonnekt.model.Customer;
import com.foodkonnekt.model.DeliveryOpeningClosingTime;
import com.foodkonnekt.model.DeliveryZoneTiming;
import com.foodkonnekt.model.Merchant;
import com.foodkonnekt.model.OpeningClosingTime;
import com.foodkonnekt.model.OrderR;
import com.google.gson.Gson;

public class OrderUtil {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(OrderUtil.class);
	
	public static String checkDeliveryOpeningClosingHour(List<DeliveryOpeningClosingTime> openingClosingTimes,int avgTime,String timeZoneCode, List<DeliveryOpeningClosingTime> nextOpeningClosingTimes,Environment environment) {
		LOGGER.info("===============  OrderUtil : Inside checkDeliveryOpeningClosingHour :: Start  ============= ");
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
       
        sdf.setTimeZone(TimeZone.getTimeZone(timeZoneCode));
        String currentTime = sdf.format(new Date());
        int count = 0;
        
        
        for (DeliveryOpeningClosingTime time : openingClosingTimes) {
        	
        	if(time.getEndTime().equals("00:00")){
        		for (DeliveryOpeningClosingTime nextTime : nextOpeningClosingTimes) {
                	if(nextTime.getStartTime().equals("00:00")){
                		try {
                            boolean status = isTimeBetweenTwoTime(time.getStartTime() + ":00", time.getEndTime() + ":00",
                                            currentTime,0,environment);
                            if (status) {
                                count++;
                            }
                        } catch (Exception e) {
                        	LOGGER.error("===============  OrderUtil : Inside checkDeliveryOpeningClosingHour :: Exception  ============= " + e);

                            LOGGER.error("error: " + e.getMessage());
                        }
                	}else{
                		try {
                            boolean status = isTimeBetweenTwoTime(time.getStartTime() + ":00", time.getEndTime() + ":00",
                                            currentTime,avgTime,environment);
                            if (status) {
                                count++;
                            }
                        } catch (Exception e) {
                        	LOGGER.error("===============  OrderUtil : Inside checkDeliveryOpeningClosingHour :: Exception  ============= " + e);

                            LOGGER.error("error: " + e.getMessage());
                        }
                	}
                }
        	}else{
        		try {
                    boolean status = isTimeBetweenTwoTime(time.getStartTime() + ":00", time.getEndTime() + ":00",
                                    currentTime,avgTime,environment);
                    if (status) {
                        count++;
                    }
                } catch (Exception e) {
                	LOGGER.error("===============  OrderUtil : Inside checkDeliveryOpeningClosingHour :: Exception  ============= " + e);

                    LOGGER.error("error: " + e.getMessage());
                }
        	}
        	
            
        }
        if (count > 0) {
        	LOGGER.info("===============  OrderUtil : Inside checkDeliveryOpeningClosingHour :: End  ============= ");

            return "Y";
        } else {
        	LOGGER.info("===============  OrderUtil : Inside checkDeliveryOpeningClosingHour :: End  ============= ");

            return "N";
        }
    }

    public static String findFinalOrderJson(String orderJson, String orderTotal, String instruction,
                    String orderTypePosid, Customer orderCustomer, String employeePosId, double discount,
                    String discountType, String voucherCode, String itemPoSIdsjson, String inventoryLevel,
                    String itemsForDiscount, String listOfALLDiscounts, PaymentVO paymentVO,Merchant merchant) {
    	
    	LOGGER.info("----------------Start :: OrderUtil : findFinalOrderJson------------------------");
    	
    	LOGGER.info("OrderUtil :: findFinalOrderJson : merchantId "+merchant.getId());
    	
        CloverOrderVO cloverOrderVO = new CloverOrderVO();
        CloverDiscountVO cloverDiscountVO= new CloverDiscountVO();
        OrderVO orderVO = new OrderVO();
        OrderTypeVO orderType = new OrderTypeVO();
        List<com.foodkonnekt.clover.vo.Customer> customers = new ArrayList<com.foodkonnekt.clover.vo.Customer>();
        com.foodkonnekt.clover.vo.Customer customer = new com.foodkonnekt.clover.vo.Customer();
        cloverOrderVO.setItemsForDiscount(itemsForDiscount);
        cloverOrderVO.setListOfALLDiscounts(listOfALLDiscounts);
        if(discount>0){
        	Long orderDiscount=(long) (discount*100);
        	if(discountType!=null && !discountType.isEmpty() && discountType!=""){
        		if(discountType.equals("amount")){
        			
        			cloverDiscountVO.setAmount("-"+orderDiscount.toString());
        			cloverDiscountVO.setPercentage("");
        		}else{
        			cloverDiscountVO.setAmount("");
        			long discountLong= (long)discount;
        			cloverDiscountVO.setPercentage(discountLong+"");
        		}
        	}else{
        		discountType="amount";
        		cloverDiscountVO.setAmount(orderDiscount.toString());
        	}
        	
        	
        	if(voucherCode!=null && !voucherCode.isEmpty() && voucherCode!="")
        	cloverDiscountVO.setName("Discount:"+voucherCode);
        	
        	cloverOrderVO.setCloverDiscountVO(cloverDiscountVO);
        }

        
        /*
         * List<AddressVO> addresses = new ArrayList<AddressVO>(); AddressVO address = new AddressVO();
         */

        Employee employee = new Employee();

        employee.setId(employeePosId);
        String customerPosId = "";
        if (orderCustomer != null && orderCustomer.getCustomerPosId() != null) {
            customerPosId = orderCustomer.getCustomerPosId();
            /*
             * if(orderCustomer.getAddresses()!=null && orderCustomer.getAddresses().size()>0){
             * address.setId(orderCustomer.getAddresses().get(0).getAddressPosId()); addresses.add(address);
             * customer.setAddresses(addresses); }
             */

        }

        customer.setId(customerPosId);

        customers.add(customer);
        orderVO.setCustomers(customers);
        orderVO.setEmployee(employee);
        orderType.setId(orderTypePosid);

        orderVO.setCurrency("USD");
        orderVO.setNote("Special Instructions : "+instruction);
        orderVO.setState("Open");
        orderVO.setTaxRemoved(false);
        orderVO.setTestMode(false);
        if (orderTypePosid != null && !orderTypePosid.equals(""))
            orderVO.setOrderType(orderType);

        orderVO.setTitle("");

        Double roundOff = null;
        LOGGER.info("orderTotal- "+orderTotal);
        if(orderTotal != null){
            orderTotal = orderTotal.replaceAll(",", "");
            roundOff = Math.round(Double.valueOf(orderTotal) * 100.0) / 100.0;
        }else{
            roundOff = 0.0;
        }
        roundOff = roundOff * 100;
        roundOff = Math.round(Double.valueOf(roundOff) * 100.0) / 100.0;
        long price = new Double(roundOff).longValue();
        LOGGER.info("price- "+price);
        orderVO.setTotal(String.valueOf(price));

        cloverOrderVO.setOrderVO(orderVO);
      //  String orderJ = "{\n\"order\":[" + orderJson + "]}";
        LOGGER.info("orderjson- "+orderJson);
        if (orderJson != null && !"{\"order\":null}".equalsIgnoreCase(orderJson)
                        && !orderJson.contains("{\"order\":null}")) {
            JSONObject jObject = new JSONObject(orderJson);
            JSONArray orderArray = jObject.getJSONArray("order");

            List<OrderItemVO> itemVOs = new ArrayList<OrderItemVO>();
            for (Object jObj : orderArray) {
                JSONObject orderJObject = (JSONObject) jObj;
                OrderItemVO orderItemVO = new OrderItemVO();
                List<Modifications> modifications = null;
                orderItemVO.setUnitQty(orderJObject.getString("amount"));
                orderItemVO.setQuantitySold(Integer.parseInt(orderJObject.getString("amount")));
                orderItemVO.setDiscountAmount("");
                Item item = new Item();

                if (orderJObject.has("itemid") && orderJObject.getString("itemid") != null
                                && orderJObject.getString("itemid") != ""
                                && !"undefined".equalsIgnoreCase(orderJObject.getString("itemid"))
                                && !"null".equalsIgnoreCase(orderJObject.getString("itemid"))) {

                    if (orderJObject.has("isPizza")) {
                        if (orderJObject.getBoolean("isPizza")) {
                            item.setIsPizza(orderJObject.getBoolean("isPizza"));
                            item.setPizzaSizeId(orderJObject.getString("pizzaSizeId"));
                            item.setModifier(getModifierObjectsV2(orderJObject.getJSONArray("modifier")));
                        } else {
                            item.setIsPizza(false);
                        }
                    } else {
                        item.setIsPizza(false);
                       
                    }
                    if(orderJObject.has("extraCharge")){
                        item.setExtraCharge(orderJObject.getBoolean("extraCharge"));
                    }else{
                        item.setExtraCharge(false);
                    }
                    item.setId(orderJObject.getString("itemid"));
                    modifications = getModifierObjects(orderJObject.getJSONArray("modifier"));
                    if(orderJObject.has("itemPosId")){
                        item.setItemPosId(orderJObject.getString("itemPosId"));
                   
                    if (merchant.getOwner() != null && merchant.getOwner().getPos() != null
    						&& merchant.getOwner().getPos().getPosId() != null
    						&& merchant.getOwner().getPos().getPosId() == 1) {
                	   item.setId(orderJObject.getString("itemPosId"));
                	   modifications = getModifierObjectsForCloverV2(orderJObject.getJSONArray("modifier"));
                	   
                	   
                   }
                    }
                    orderItemVO.setModifications(modifications);
                    orderItemVO.setItem(item);
                    itemVOs.add(orderItemVO);
                }
            }
            cloverOrderVO.setOrderItemVOs(itemVOs);
            cloverOrderVO.setPaymentVO(paymentVO);
        }
        Gson gson = new Gson();
        LOGGER.info("OrderUtil :: findFinalOrderJson : merchantId "+merchant.getId());
        LOGGER.info("----------------End :: OrderUtil : findFinalOrderJson------------------------");
        return gson.toJson(cloverOrderVO);
    }

    /**
     * Set modifier in modifierVO
     * 
     * @param jsonArray
     * @return
     */
    private static List<Modifications> getModifierObjects(JSONArray jsonArray) {
    	LOGGER.info("----------------Start :: OrderUtil : getModifierObjects------------------------");
        List<Modifications> modifications = new ArrayList<Modifications>();
        for (Object jObj : jsonArray) {
            JSONObject modifierObject = (JSONObject) jObj;
            Modifications modifier = new Modifications();
            Modifier modifierObj = new Modifier();
           // modifierObj.setId(modifierObject.getString("id"));
            if (modifierObject.has("posId")) {
            	 modifierObj.setId(modifierObject.getString("posId"));
            }else{
            	 modifierObj.setId(modifierObject.getString("id"));
            }
           
            modifier.setModifier(modifierObj);
            modifications.add(modifier);
        }
        LOGGER.info("----------------End :: OrderUtil : getModifierObjects------------------------");
        return modifications;
    }
    
    private static List<Modifications> getModifierObjectsForCloverV2(JSONArray jsonArray) {
    	LOGGER.info("----------------Start :: OrderUtil : getModifierObjectsForCloverV2------------------------");
        List<Modifications> modifications = new ArrayList<Modifications>();
        for (Object jObj : jsonArray) {
            JSONObject modifierObject = (JSONObject) jObj;
            Modifications modifier = new Modifications();
            Modifier modifierObj = new Modifier();
			if (modifierObject.has("posId")) {
				modifierObj.setId(modifierObject.getString("posId"));
			}
            
            modifier.setModifier(modifierObj);
            modifications.add(modifier);
        }
        LOGGER.info("----------------End :: OrderUtil : getModifierObjectsForCloverV2------------------------");
        return modifications;
    }
    
    public static void main(String[] args) {
        double a = 38.3322;
        double roundOff = Math.round(a * 100.0) / 100.0;
        roundOff = roundOff * 100;
    
        long l = new Double(roundOff).longValue();
        
        String s = "";
        for (int i = 0; i < 2; i++) {
            s = s + "&" + i;
        }
        
    }

    public static OrderR getObjectFromJson(JSONObject result, Customer customer, Merchant merchant) {
        int status = IConstant.BOOLEAN_FALSE;
        OrderR orderR = new OrderR();
        orderR.setCustomer(customer);
        orderR.setMerchant(merchant);
        orderR.setIsDefaults(3);
        if (result.toString().contains("title")) {
            orderR.setOrderName(result.getString("title"));
            orderR.setOrderNote(result.getString("title"));
            status = IConstant.BOOLEAN_TRUE;
        }
        if (result.toString().contains("note")) {
            orderR.setOrderNote(result.getString("note").replaceAll("Special Instructions :", ""));
        }
        if (result.toString().contains("total")) {
            orderR.setOrderPrice(result.getDouble("total") / 100);
            status = IConstant.BOOLEAN_TRUE;
        }
        if (result.toString().contains("id")) {
            orderR.setOrderPosId(result.getString("id"));
            status = IConstant.BOOLEAN_TRUE;
        }
        if (status == IConstant.BOOLEAN_FALSE) {
            return null;
        }
        return orderR;
    }

    public static String checkOpeningClosingHour(List<OpeningClosingTime> openingClosingTimes, int avgTime,
                    String timeZoneCode, List<OpeningClosingTime> nextOpeningClosingTimes,Environment environment) {
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
LOGGER.info("===============  OrderUtil : Inside checkOpeningClosingHour :: Start  ============= ");

        sdf.setTimeZone(TimeZone.getTimeZone(timeZoneCode));
        String currentTime = sdf.format(new Date());
        int count = 0;

        for (OpeningClosingTime time : openingClosingTimes) {

            if (time.getEndTime().equals("00:00")) {
                for (OpeningClosingTime nextTime : nextOpeningClosingTimes) {
                    if (nextTime.getStartTime().equals("00:00")) {
                        try {
                            boolean status = isTimeBetweenTwoTime(time.getStartTime() + ":00", time.getEndTime()
                                            + ":00", currentTime, 0,environment);
                            if (status) {
                                count++;
                            }
                        } catch (Exception e) {
                        	LOGGER.error("===============  OrderUtil : Inside checkOpeningClosingHour :: Exception  ============= " + e);

                            LOGGER.error("error: " + e.getMessage());
                        }
                    } else {
                        try {
                            boolean status = isTimeBetweenTwoTime(time.getStartTime() + ":00", time.getEndTime()
                                            + ":00", currentTime, avgTime,environment);
                            if (status) {
                                count++;
                            }
                        } catch (Exception e) {
                        	LOGGER.error("===============  OrderUtil : Inside checkOpeningClosingHour :: Exception  ============= " + e);

                            LOGGER.error("error: " + e.getMessage());
                        }
                	}
                }
        	}else{
        		try {
                    boolean status = isTimeBetweenTwoTime(time.getStartTime() + ":00", time.getEndTime() + ":00",
                                    currentTime,avgTime,environment);
                    if (status) {
                        count++;
                    }
                } catch (Exception e) {
                	LOGGER.error("===============  OrderUtil : Inside checkOpeningClosingHour :: Exception  ============= " + e);

                    LOGGER.error("error: " + e.getMessage());
                }
        	}
        	
            
        }
        if (count > 0) {
        	LOGGER.info("===============  OrderUtil : Inside checkOpeningClosingHour :: End  ============= ");

            return "Y";
        } else {
        	LOGGER.info("===============  OrderUtil : Inside checkOpeningClosingHour :: End  ============= ");

            return "N";
        }
    }

    /**
     * Check current time is existing between operating hours
     * 
     * @param initialTime
     * @param finalTime
     * @param currentTime
     * @return String
     * @throws ParseException
     */
    private static boolean isTimeBetweenTwoTime(String initialTime, String finalTime, String currentTime,int avgTime,Environment environment) {
        try {
        	LOGGER.info("===============  OrderUtil : Inside isTimeBetweenTwoTime :: Start  ============= ");

            LOGGER.info(" initialTime "+initialTime + "-" +" finalTime "+ finalTime + "-" +" currentTime "+ currentTime);
            String reg = "^([0-1][0-9]|2[0-4]):([0-5][0-9]):([0-5][0-9])$";
            if (initialTime.matches(reg) && finalTime.matches(reg) && currentTime.matches(reg)) {
                boolean valid = false;
                if (finalTime.equals("00:00:00") || finalTime.equals("24:00:00")) {
                    finalTime = "23:59:30";
                }

                // Start Time
                java.util.Date inTime = new SimpleDateFormat("HH:mm:ss").parse(initialTime);
                Calendar calendar1 = Calendar.getInstance();
                calendar1.setTime(inTime);

                // Current Time
                java.util.Date checkTime = new SimpleDateFormat("HH:mm:ss").parse(currentTime);
                Calendar calendar3 = Calendar.getInstance();
                calendar3.setTime(checkTime);
                /*calendar3.add(Calendar.HOUR, hourDifference);
                calendar3.add(Calendar.MINUTE, minutDifference);*/

                // End Time
                java.util.Date finTime = new SimpleDateFormat("HH:mm:ss").parse(finalTime);
                Calendar calendar2 = Calendar.getInstance();
                calendar2.setTime(finTime);
                calendar2.add(Calendar.MINUTE, -avgTime);

                if (finalTime.compareTo(initialTime) < 0) {
                    calendar2.add(Calendar.DATE, 1);
                    calendar3.add(Calendar.DATE, 1);
                }

                java.util.Date actualTime = calendar3.getTime();
                if ((actualTime.after(calendar1.getTime()) || actualTime.compareTo(calendar1.getTime()) == 0)
                                && actualTime.before(calendar2.getTime())) {
                    valid = true;
                }
                LOGGER.info("===============  OrderUtil : Inside isTimeBetweenTwoTime :: End  ============= ");

                return valid;
            }
        } catch (Exception e) {
            if (e != null) {
                MailSendUtil.sendExceptionByMail(e,environment);
            }
            LOGGER.error("===============  OrderUtil : Inside isTimeBetweenTwoTime :: Exception  ============= " + e);

            LOGGER.error("error: " + e.getMessage());
        }
        LOGGER.info("===============  OrderUtil : Inside isTimeBetweenTwoTime :: End  ============= ");

        return false;
    }
    
    public static boolean isTimeBetweenTwoTime(String initialTime, String finalTime, String currentTime,Environment environment) {
        try {
        	LOGGER.info("===============  OrderUtil : Inside isTimeBetweenTwoTime :: Start  ============= ");

            LOGGER.info(" initialTime "+initialTime + "-" +" finalTime "+ finalTime + "-" +" currentTime "+ currentTime);
            String reg = "^([0-1][0-9]|2[0-4]):([0-5][0-9]):([0-5][0-9])$";
            if (initialTime.matches(reg) && finalTime.matches(reg) && currentTime.matches(reg)) {
                boolean valid = false;
                if (finalTime.equals("00:00:00") || finalTime.equals("24:00:00")) {
                    finalTime = "23:59:30";
                }

                // Start Time
                java.util.Date inTime = new SimpleDateFormat("HH:mm:ss").parse(initialTime);
                Calendar calendar1 = Calendar.getInstance();
                calendar1.setTime(inTime);

                // Current Time
                java.util.Date checkTime = new SimpleDateFormat("HH:mm:ss").parse(currentTime);
                Calendar calendar3 = Calendar.getInstance();
                calendar3.setTime(checkTime);
                /*calendar3.add(Calendar.HOUR, hourDifference);
                calendar3.add(Calendar.MINUTE, minutDifference);*/

                // End Time
                java.util.Date finTime = new SimpleDateFormat("HH:mm:ss").parse(finalTime);
                Calendar calendar2 = Calendar.getInstance();
                calendar2.setTime(finTime);

                if (finalTime.compareTo(initialTime) < 0) {
                    calendar2.add(Calendar.DATE, 1);
                    calendar3.add(Calendar.DATE, 1);
                }

                java.util.Date actualTime = calendar3.getTime();
                if ((actualTime.after(calendar1.getTime()) || actualTime.compareTo(calendar1.getTime()) == 0)
                                && actualTime.before(calendar2.getTime())) {
                    valid = true;
                }
                LOGGER.info("===============  OrderUtil : Inside isTimeBetweenTwoTime :: End  ============= ");

                return valid;
            }
        } catch (Exception e) {
            if (e != null) {
                MailSendUtil.sendExceptionByMail(e,environment);
            }
            LOGGER.error("===============  OrderUtil : Inside isTimeBetweenTwoTime :: Exception  ============= " + e);

            LOGGER.error("error: " + e.getMessage());
        }
        LOGGER.info("===============  OrderUtil : Inside isTimeBetweenTwoTime :: End  ============= ");

        return false;
    }

    /**
     * Find operating hours
     * 
     * @param openingClosingTimes
     * @return String
     */
    public static String findOperatingHour(List<OpeningClosingTime> openingClosingTimes) {
    	LOGGER.info("===============  OrderUtil : Inside findOperatingHour :: Start  ============= ");

        String operatingHours = "";
        if (!openingClosingTimes.isEmpty()) {
            for (OpeningClosingTime time : openingClosingTimes) {
                if (time != null) {
                    operatingHours = operatingHours + "&"
                                    + DateUtil.get12Format(time.getStartTime()) + "-"
                                    +  DateUtil.get12Format(time.getEndTime());
                }
            }
            operatingHours = operatingHours.substring(1, operatingHours.length());
            LOGGER.info("===== OrderUtil : Inside findOperatingHour :: operatingHours  == " + operatingHours);

        }
        LOGGER.info("===============  OrderUtil : Inside findOperatingHour :: End  ============= ");

        return operatingHours;
    }

    /**
     * Convert time 24 hour to 12 hour format
     * 
     * @param str
     * @return String
     */
    public static String convert24HoursTo12HourseFormate(String str) {
        SimpleDateFormat displayFormat = new SimpleDateFormat("hh:mm a");
        SimpleDateFormat parseFormat = new SimpleDateFormat("HH:mm:ss");
        Date date = null;
        try {
            date = parseFormat.parse(str);
        } catch (ParseException e) {
            LOGGER.error("error: " + e.getMessage());
        }
        if (date != null) {
            return displayFormat.format(date);
        } else {
            return null;
        }
    }
    
    
    public static String postCouponData(String url, String couponRedeemedJson){
    	
    	LOGGER.info("===OrderUtil :inside postCouponData starts====");
		HttpPost postRequest = new HttpPost(url);
    	LOGGER.info("===OrderUtil :inside postCouponData End====");

		return postOnCoupon(postRequest, couponRedeemedJson);
    	
    }
    
    public static String postOnCoupon(HttpPost postRequest, String couponRedeemedJson)
	{
		StringBuilder responseBuilder = new StringBuilder();
		try {
	    	LOGGER.info("===OrderUtil :inside postOnCoupon starts====");

			HttpClient httpClient = HttpClientBuilder.create().build();

			StringEntity input = new StringEntity(couponRedeemedJson);
			input.setContentType("application/json");
			postRequest.setEntity(input);
			HttpResponse response = httpClient.execute(postRequest);
			BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			String line = "";
			while ((line = rd.readLine()) != null) {
				responseBuilder.append(line);
			}
		
		} catch (MalformedURLException e) {
			LOGGER.error("===============  OrderUtil : Inside postOnCoupon :: Exception  ============= " + e);

			LOGGER.error("error: " + e.getMessage());
		} catch (IOException e) {
			LOGGER.error("===============  OrderUtil : Inside postOnCoupon :: Exception  ============= " + e);

			LOGGER.error("error: " + e.getMessage());
		}
    	LOGGER.info("===OrderUtil :inside postOnCoupon End====");

		return responseBuilder.toString();
	}
    
    public static Boolean checkDeliveryTiming(DeliveryZoneTiming deliveryZoneTiming,int avgTime,String timeZoneCode,Environment environment) {
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
       LOGGER.info("===============  OrderUtil : Inside checkDeliveryTiming :: Start  ============= ");

        sdf.setTimeZone(TimeZone.getTimeZone(timeZoneCode));
        String currentTime = sdf.format(new Date());
        int count = 0;
        
        	if(deliveryZoneTiming.getEndTime().equals("00:00")){
                	if(deliveryZoneTiming.getStartTime().equals("00:00")){
                		try {
                            boolean status = isTimeBetweenTwoTime( deliveryZoneTiming.getStartTime(), deliveryZoneTiming.getEndTime(),
                                            currentTime,0,environment);
                            if (status) {
                                count++;
                            }
                        } catch (Exception e) {
                        	LOGGER.error("===============  OrderUtil : Inside checkDeliveryTiming :: Exception  ============= " + e);

                            LOGGER.error("error: " + e.getMessage());
                        }
                	}else{
                		try {
                            boolean status = isTimeBetweenTwoTime(deliveryZoneTiming.getStartTime(), deliveryZoneTiming.getEndTime(),
                                            currentTime,avgTime,environment);
                            if (status) {
                                count++;
                            }
                        } catch (Exception e) {
                        	LOGGER.error("===============  OrderUtil : Inside checkDeliveryTiming :: Exception  ============= " + e);

                            LOGGER.error("error: " + e.getMessage());
                        }
                	}
        	}else{
        		try {
                    boolean status = isTimeBetweenTwoTime(deliveryZoneTiming.getStartTime(), deliveryZoneTiming.getEndTime(),
                                    currentTime,avgTime,environment);
                    if (status) {
                        count++;
                    }
                } catch (Exception e) {
                	LOGGER.error("===============  OrderUtil : Inside checkDeliveryTiming :: Exception  ============= " + e);

                	LOGGER.error("error: " + e.getMessage());
                }
        	}
            
        if (count > 0) {
        	LOGGER.info("===============  OrderUtil : Inside checkDeliveryTiming :: End  ============= ");

            return true;
        } else {
        	LOGGER.info("===============  OrderUtil : Inside checkDeliveryTiming :: End  ============= ");

            return false;
        }
    }
 /**
     * Call allCategory web service
     * 
     * @return
     */
    public static String orderConfirmationByIdByMerchantUId(Integer orderId,String merchantUId,Boolean enableNotification,Environment environment) {
        HttpGet request = new HttpGet(environment.getProperty("IP_BASE_URL") + "/orderConfirmationByIdByMerchantUId?orderId="+orderId+"&merchantUId="+merchantUId+"&type=Accept"+"&enableNotification="+enableNotification);
        LOGGER.info("OrderUtil :: orderConfirmationByIdByMerchantUId : merchantId "+merchantUId);
        LOGGER.info("OrderUtil :: orderConfirmationByIdByMerchantUId : orderId "+orderId);
        return convertToStringJson(request);
    }
    public static String orderConfirmationByOrderPosId(String orderId,Environment environment) {
        HttpGet request = new HttpGet(environment.getProperty("IP_BASE_URL") + "/orderConfirmation?orderId="+orderId+"&type=Accept");
        LOGGER.info("OrderUtil :: orderConfirmationByIdByMerchantUId : orderId "+orderId);
        return convertToStringJson(request);
    }

    /**
     * Hit URL and get response and parse to string
     * 
     * @param request
     * @return
     */
    public static String convertToStringJson(HttpGet request) {
        HttpClient client = HttpClientBuilder.create().build();
        HttpResponse response = null;
        StringBuilder responseBuilder = new StringBuilder();
        try {
            response = client.execute(request);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            String line = "";
            while ((line = rd.readLine()) != null) {
                responseBuilder.append(line);
            }
        } catch (IOException e) {
            LOGGER.error("error: " + e.getMessage());
        }
        return responseBuilder.toString();
    }

    private static List<ModifierVO> getModifierObjectsV2(JSONArray jsonArray) {
    	
    	LOGGER.info("----------------Start :: OrderUtil : getModifierObjectsV2------------------------");
    	
        List<ModifierVO> modifications = new ArrayList<ModifierVO>();
        for (Object jObj : jsonArray) {
            JSONObject modifierObject = (JSONObject) jObj;
            ModifierVO modifier = new ModifierVO();
            modifier.setId(modifierObject.getString("id"));
            LOGGER.info("OrderUtil :: getModifierObjectsV2 : modifierId "+modifierObject.getString("id"));
            modifier.setName(modifierObject.getString("name"));
            modifier.setPrice(modifierObject.getString("price"));
            modifier.setQuantity(modifierObject.getString("quantity"));
            modifier.setIsCrust(modifierObject.getBoolean("isCrust"));
            modifier.setToppingSide(modifierObject.getString("toppingSide"));
            modifications.add(modifier);
        }
    	LOGGER.info("----------------End :: OrderUtil : getModifierObjectsV2------------------------");
        return modifications;
    }

    
    
}
