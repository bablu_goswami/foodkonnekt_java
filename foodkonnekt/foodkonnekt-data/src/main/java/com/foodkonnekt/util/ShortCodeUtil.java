package com.foodkonnekt.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;

public class ShortCodeUtil {

    private static final String GOOGLE_URL_SHORT_API = "https://www.googleapis.com/urlshortener/v1/url";
    private static final String GOOGLE_API_KEY = "AIzaSyDEi-EkrXJKwzKjVtDHBkQoxhhxCBnUOvQ";

    private static final Logger LOGGER= LoggerFactory.getLogger(ShortCodeUtil.class);
    
    public static void main(String[] args) {
    	
    	String url = getTinyUrl("https://www.foodkonnekt.com/foodkonnekt-web/receipt?orderid=MTU4Nzc%3D&customerid=MTA5OTk%3D");
    	LOGGER.info(" url : "+url);
    }

    public static String generateShortUrl(String longURL) {
        String googleShortURL = ShortCodeUtil.shortenUrl(longURL);
        String googleBackToLongURL = ShortCodeUtil.getActualURLAgainstGoogleShortURL(googleShortURL);

        LOGGER.info("Long URL against Short URL by google API :" + googleBackToLongURL);
        LOGGER.info("Short URL by google API :" + googleShortURL);
       
        return googleShortURL;
    }

    public static String shortenUrl(String longUrl) {

        if (longUrl == null) {
            return longUrl;
        } else if (!longUrl.startsWith("http://") && !longUrl.startsWith("https://")) {
            longUrl = "http://" + longUrl;
        }
        try {
            String json = "{\"longUrl\": \"" + longUrl + "\"}";
            String apiURL = GOOGLE_URL_SHORT_API + "?key=" + GOOGLE_API_KEY;

            HttpPost postRequest = new HttpPost(apiURL);
            postRequest.setHeader("Content-Type", "application/json");
            postRequest.setEntity(new StringEntity(json, "UTF-8"));

            CloseableHttpClient httpClient = HttpClients.createDefault();
            HttpResponse response = httpClient.execute(postRequest);
            String responseText = EntityUtils.toString(response.getEntity());

            Gson gson = new Gson();
            @SuppressWarnings("unchecked")
            HashMap<String, String> res = gson.fromJson(responseText, HashMap.class);

            return res.get("id");

        } catch (MalformedURLException e) {
        	LOGGER.error("===============  ShortCodeUtil : Inside shortenUrl :: Exception  ============= " + e);

            return "error";
        } catch (IOException e) {
        	LOGGER.error("===============  ShortCodeUtil : Inside shortenUrl :: Exception  ============= " + e);

            return "error";
        }
    }

    public static String getActualURLAgainstGoogleShortURL(String shortUrl) {

        if (shortUrl == null) {
            return shortUrl;
        } else if (!shortUrl.startsWith("http://") && !shortUrl.startsWith("https://")) {
            shortUrl = "http://" + shortUrl;
        }
        try {
            String apiURL = GOOGLE_URL_SHORT_API + "?key=" + GOOGLE_API_KEY + "&shortUrl=" + shortUrl;
            HttpGet getRequest = new HttpGet(apiURL);

            CloseableHttpClient httpClient = HttpClients.createDefault();
            HttpResponse response = httpClient.execute(getRequest);
            String responseText = EntityUtils.toString(response.getEntity());

            Gson gson = new Gson();
            @SuppressWarnings("unchecked")
            HashMap<String, String> res = gson.fromJson(responseText, HashMap.class);

            return res.get("longUrl");

        } catch (MalformedURLException e) {
        	LOGGER.error("===============  ShortCodeUtil : Inside getActualURLAgainstGoogleShortURL :: Exception  ============= " + e);

            return "error";
        } catch (IOException e) {
        	LOGGER.error("===============  ShortCodeUtil : Inside getActualURLAgainstGoogleShortURL :: Exception  ============= " + e);

            return "error";
        }
    }
    
    
    public static String getTinyUrl(String longUrl) {
        String resultUrl = "";
        try {
            String output;
            URL url = new URL("https://tinyurl.com/api-create.php?url=" + longUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
            }
            BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
			while ((output = br.readLine()) != null) {
				if (output != null && output.startsWith("https:")) {
					resultUrl = output;
				} else if (output != null && output.startsWith("http:")) {
					resultUrl = output.replace("http", "https");
				}
			}
			LOGGER.info("===================short url==================="+resultUrl);
            LOGGER.info("===================short url==================="+resultUrl);
            conn.disconnect();
        } catch (MalformedURLException e) {
        	LOGGER.error("===============  ShortCodeUtil : Inside getTinyUrl :: Exception  ============= " + e);

            LOGGER.error("error: " + e.getMessage());
        } catch (IOException e) {
        	LOGGER.error("===============  ShortCodeUtil : Inside getTinyUrl :: Exception  ============= " + e);

            LOGGER.error("error: " + e.getMessage());
        }
        return resultUrl;
    }
}
