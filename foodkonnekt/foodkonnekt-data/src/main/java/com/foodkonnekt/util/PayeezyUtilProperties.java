package com.foodkonnekt.util;

import java.util.Properties;

import com.firstData.payeezy.PayeezyClientHelperf;
import com.firstdata.payeezy.PayeezyClientHelper;
import com.foodkonnekt.model.PaymentGateWay;
import org.springframework.core.env.Environment;

public class PayeezyUtilProperties {
    
    public static PayeezyClientHelper getPayeezyProperties(PaymentGateWay paymentGateWay,Environment environment) {
    Properties p = new Properties();
    p.put("url", environment.getProperty("PAYEEZY_URL"));
    p.put("apikey",environment.getProperty("PAYEEZY_APP_ID"));
    
    if(paymentGateWay.getPayeezyMerchnatToken() != null){
    	p.put("token",paymentGateWay.getPayeezyMerchnatToken());
    }
    
    p.put("pzsecret", environment.getProperty("PAYEEZY_APP_SECRET_KEY"));
    PayeezyClientHelper client = new PayeezyClientHelper(p);
    return  client;
    }
    
    
    public static PayeezyClientHelper getPayeezyProperties(String merchantToken,Environment environment) 
    {
        
        PayeezyClientHelper client=null;
        
        if(client==null)
        {
            System.out.println("Client is null creating new");
            Properties p = new Properties();
            p.put("url", environment.getProperty("PAYEEZY_URL"));
            p.put("apikey",environment.getProperty("PAYEEZY_APP_ID"));
            p.put("token",merchantToken);
            p.put("pzsecret", environment.getProperty("PAYEEZY_APP_SECRET_KEY"));
            
            client=   new PayeezyClientHelper(p);
        }
        
        return  client;
    }

    
    
    public static PayeezyClientHelperf getPayeezyProperty(PaymentGateWay paymentGateWay, Environment environment) {
    Properties p = new Properties();
    p.put("url", environment.getProperty("PAYEEZY_URL"));
    p.put("apikey",environment.getProperty("PAYEEZY_APP_ID"));
    p.put("token",paymentGateWay.getPayeezyMerchnatToken());
    p.put("pzsecret", environment.getProperty("PAYEEZY_APP_SECRET_KEY"));
    PayeezyClientHelperf client = new PayeezyClientHelperf(p);
    return  client;
    }
}
