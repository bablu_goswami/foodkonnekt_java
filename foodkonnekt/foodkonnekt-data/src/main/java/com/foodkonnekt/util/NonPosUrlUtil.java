package com.foodkonnekt.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;

import com.foodkonnekt.model.Clover;

public class NonPosUrlUtil {
	private static final Logger LOGGER = LoggerFactory.getLogger(NonPosUrlUtil.class);

	public static String getMerchantDetails(String locationUid,Environment environment) {
		HttpGet request = new HttpGet(environment.getProperty("MkonnektPlatform_BASE_DEV_URL")+"/entity/getLocationDetailsByUid?locationUid=" + locationUid);
		return convertToStringJson(request);
	}
	
	public static String getMerchantDetailsForDev(String locationUid,Environment environment) {
		HttpGet request = new HttpGet(environment.getProperty("MkonnektPlatform_BASE_DEV_URL")+"/entity/getLocationDetailsByUid?locationUid=" + locationUid);
		return convertToStringJson(request);
	}
	
	public static String convertToStringJson(HttpGet request) {
		HttpClient client = HttpClientBuilder.create().build();
		HttpResponse response = null;
		StringBuilder responseBuilder = new StringBuilder();
		try {
			response = client.execute(request);
			BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			String line = "";
			while ((line = rd.readLine()) != null) {
				responseBuilder.append(line);
			}
		} catch (IOException e) {
			LOGGER.error("error: " + e.getMessage());
		}
		return responseBuilder.toString();
	}
	
}
