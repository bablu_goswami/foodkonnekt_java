package com.foodkonnekt.util;

import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.Key;
import java.security.SecureRandom;
import java.util.Random;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import org.kobjects.base64.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SuppressWarnings({"static-access", "deprecation"})
public class EncryptionDecryptionToken {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(EncryptionDecryptionToken.class);
    
    
    private static final String UNICODE_FORMAT = "UTF8";
    static final String GENERATE_STR = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    static SecureRandom alphaNumbericRandom = new SecureRandom();
    
	public static String encryptValue(String valueToEnc,  String secureKey) 
	{
		
        try
        {
            Key key = generateKey(secureKey);
            Cipher c = Cipher.getInstance("AES");
            c.init(Cipher.ENCRYPT_MODE, key);
            byte[] encValue = c.doFinal(valueToEnc.getBytes());
            String encryptedValue = new Base64().encode(encValue);
            String urlEncodeddata= URLEncoder.encode(encryptedValue);
            return urlEncodeddata;
        } catch (Exception e) {
            LOGGER.error("error: " + e.getMessage());
        }
        return valueToEnc;
    }

//	public static String randomString1(int len)
//    {
//        StringBuilder sb = new StringBuilder(len);
//        for (int i = 0; i < len; i++)
//        {
//            sb.append(GENERATE_STR.charAt(alphaNumbericRandom.nextInt(GENERATE_STR.length())));
//        }
//        return sb.toString();
//    }
	
	public static String decryptValue(String encryptedValue, String secureKey) 
    {
        try
        {
            Key key = generateKey(secureKey);
            Cipher c = Cipher.getInstance("AES");
            c.init(Cipher.DECRYPT_MODE, key);
            encryptedValue = URLDecoder.decode(encryptedValue);
            byte[] decordedValue = new Base64().decode(encryptedValue);
            byte[] decValue = c.doFinal(decordedValue);//////////LINE 50
            String decryptedValue = new String(decValue);
            
            return decryptedValue;
        }catch (Exception e){
            LOGGER.error("error: " + e.getMessage());
        }
        
        return "";
    }

    public static String randomString(int len)
    {
        StringBuilder sb = new StringBuilder(len);
        for (int i = 0; i < len; i++)
        {
            sb.append(GENERATE_STR.charAt(alphaNumbericRandom.nextInt(GENERATE_STR.length())));
        }
        return sb.toString();
    }
	/*public static String randomString(int len){
		char[] chars = "abcdefghijklmnopqrstuvwxyzQWERTYUIOPLKJHGFDSAZXCVBNM1234567890".toCharArray();
	    StringBuilder sb = new StringBuilder();
	    Random random = new Random();
	    for (int i = 0; i < 32; i++) {
	        char c = chars[random.nextInt(chars.length)];
	        sb.append(c);
	    }
	    String output = sb.toString();
	    return output;
	}*/
    
    
    

    private static Key generateKey(String secureKey) throws Exception
    {
        byte[] keyAsBytes;
        keyAsBytes = secureKey.getBytes(UNICODE_FORMAT);
        Key key = new SecretKeySpec(keyAsBytes, "AES");
        return key;
    }
    
    public static void main(String[] args) 
    {
    	String ePasswd = "KIvMBg%2BNxa39C1Hvvp%2BuJg%3D%3D";
    	String username="121211212112112112";
		String key = randomString(32);
		System.out.println("key is : "+ key);
		String enc = encryptValue(ePasswd, key);
		System.out.println(enc);
		String dec = decryptValue(enc, key);
		System.out.println(dec);

		
	}


}


