package com.foodkonnekt.util;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import net.authorize.api.contract.v1.CreateTransactionRequest;
import net.authorize.api.contract.v1.CreateTransactionResponse;
import net.authorize.api.contract.v1.CreditCardType;
import net.authorize.api.contract.v1.CustomerAddressType;
import net.authorize.api.contract.v1.CustomerDataType;
import net.authorize.api.contract.v1.CustomerTypeEnum;
import net.authorize.api.contract.v1.MerchantAuthenticationType;
import net.authorize.api.contract.v1.MessageTypeEnum;
import net.authorize.api.contract.v1.PaymentType;
import net.authorize.api.contract.v1.TransactionRequestType;
import net.authorize.api.contract.v1.TransactionResponse;
import net.authorize.api.contract.v1.TransactionTypeEnum;
import net.authorize.api.controller.CreateTransactionController;
import net.authorize.api.controller.base.ApiOperationBase;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;

import com.TPISoft.SmartPayments.Response;
import com.TPISoft.SmartPayments.SmartPaymentsSoapProxy;
import com.firstData.paymentGatway.CardConnectRestClient;
import com.firstData.paymentGatway.CardConnectRestClientExample;
import com.firstdata.payeezy.PayeezyClientHelper;
import com.firstdata.payeezy.models.transaction.PayeezyResponse;
import com.firstdata.payeezy.models.transaction.TransactionRequest;
import com.foodkonnekt.model.PaymentGateWay;
import com.stripe.Stripe;
import com.stripe.model.Charge;
import com.stripe.model.Refund;
import com.stripe.model.Token;
public class PayMentGatwayUtil {
	private static final Logger LOGGER = LoggerFactory.getLogger(PayMentGatwayUtil.class);

    public static void main(String[] args) {
        
        /*String a ="2018";
        LOGGER.info("last character: " +
                a.substring(a.length() - 2)); */
       //authorizeTestCreditCard();
    //   captureCreditCard();
        //40011075497
        //voidTransaction("957GpXaUX","832DbfKv2z6Ew59r","40011103892");
        
     //   createCustomerProfile("957GpXaUX","832DbfKv2z6Ew59r", "test@gmail.com");
        //getCustomerProfileById("957GpXaUX","832DbfKv2z6Ew59r","1503118872");
   //   refundTransaction("957GpXaUX","832DbfKv2z6Ew59r",500.00,"40011076675");
        // TODO Auto-generated method stub

    }

    public static CreateTransactionResponse authorizeTestCreditCard(PaymentGateWay paymentGateWay,Environment environment) {
LOGGER.info("===============  PayMentGatwayUtil : Inside authorizeTestCreditCard :: Start  ============= ");

        CustomerDataType customerDataType = null;
        CustomerAddressType value = null;

        // txnRequest.set
        // Make the API Request
        CreateTransactionRequest apiRequest = new CreateTransactionRequest();
        // apiRequest.setTransactionRequest(txnRequest);

        // Common code to set for all requests
        ApiOperationBase.setEnvironment(ProducerUtil.getAuthorizeEnv(environment.getProperty("AuthorizeEnv")));
        MerchantAuthenticationType merchantAuthenticationType = new MerchantAuthenticationType();
        merchantAuthenticationType.setName(paymentGateWay.getAuthorizeLoginId());
        merchantAuthenticationType.setTransactionKey(paymentGateWay.getAuthorizeTransactionKey());
        ApiOperationBase.setMerchantAuthentication(merchantAuthenticationType);

        // Populate the payment data
        PaymentType paymentType = new PaymentType();
        CreditCardType creditCard = new CreditCardType();
        creditCard.setCardNumber(paymentGateWay.getCcNumber());
        creditCard.setExpirationDate(paymentGateWay.getExpMonthYear());
        paymentType.setCreditCard(creditCard);
        // Create the payment transaction request
        TransactionRequestType txnRequest = new TransactionRequestType();
        txnRequest.setTransactionType(TransactionTypeEnum.AUTH_ONLY_TRANSACTION.value());
        txnRequest.setPayment(paymentType);
        txnRequest.setAmount(new BigDecimal(5.00));

        customerDataType = new CustomerDataType();
        if (paymentGateWay.getCustomerName() != null) {
            value = new CustomerAddressType();
            value.setFirstName(paymentGateWay.getCustomerName());

            if (paymentGateWay.getCustomerAddress() != null) {
                value.setAddress(paymentGateWay.getCustomerAddress());
            }
            if (paymentGateWay.getZipCode() != null) {
                value.setZip(paymentGateWay.getZipCode());
            }
        }
        if (value != null) {
            txnRequest.setBillTo(value);
        }
        if (customerDataType != null) {
            txnRequest.setCustomer(customerDataType);
        }

        
        net.authorize.api.contract.v1.OrderType orderType=new net.authorize.api.contract.v1.OrderType();
        orderType.setInvoiceNumber(IConstant.getTestInvoiceNumber());
        
        txnRequest.setOrder(orderType);
        // Make the API Request
        apiRequest.setTransactionRequest(txnRequest);
        CreateTransactionController controller = new CreateTransactionController(apiRequest);
        controller.execute();
        CreateTransactionResponse response = controller.getApiResponse();
LOGGER.info("===============  PayMentGatwayUtil : Inside authorizeTestCreditCard :: End  ============= ");

        return response;
    }
     

    public static String captureCreditCard(String transId, PaymentGateWay paymentGateWay,Environment environment) {
         //Common code to set for all requests
         LOGGER.info("===============  PayMentGatwayUtil : Inside captureCreditCard :: Start  =============transId "+transId);

         String responseApi = "falied";
            ApiOperationBase.setEnvironment(ProducerUtil.getAuthorizeEnv(environment.getProperty("AuthorizeEnv")));
            MerchantAuthenticationType merchantAuthenticationType  = new MerchantAuthenticationType() ;
            merchantAuthenticationType.setName(paymentGateWay.getAuthorizeLoginId());
            merchantAuthenticationType.setTransactionKey(paymentGateWay.getAuthorizeTransactionKey());
            ApiOperationBase.setMerchantAuthentication(merchantAuthenticationType);
            TransactionRequestType txnRequest = new TransactionRequestType();
            txnRequest.setTransactionType(TransactionTypeEnum.PRIOR_AUTH_CAPTURE_TRANSACTION.value());
            txnRequest.setRefTransId(transId); //id recieved from previous authorizeCreditcard method

            // Make the API Request
            CreateTransactionRequest apiRequest = new CreateTransactionRequest();
            apiRequest.setTransactionRequest(txnRequest);
            CreateTransactionController controller = new CreateTransactionController(apiRequest);
            controller.execute(); 


            CreateTransactionResponse response = controller.getApiResponse();

            if (response!=null) {

                // If API Response is ok, go ahead and check the transaction response
                if (response.getMessages().getResultCode() == MessageTypeEnum.OK) {
LOGGER.info("===============  PayMentGatwayUtil : Inside captureCreditCard :: TransactionResponse : ");
                    TransactionResponse result = response.getTransactionResponse();
                    if (result.getResponseCode().equals("1")) {
                        LOGGER.info(result.toString());
                        LOGGER.info(result.getResponseCode());
                        LOGGER.info("Successful Credit Card Transaction");
                        LOGGER.info(result.getAuthCode());
                        LOGGER.info(result.getTransId()); //40009206593
                   
                        voidTransaction(paymentGateWay, result.getTransId(),environment);
                        responseApi = "Success";
                    }
                    else
                    {
                        LOGGER.info("Failed Transaction"+result.getResponseCode());
                        responseApi =  response.getTransactionResponse().getErrors().getError().get(0).getErrorText();
                        LOGGER.info("Failed Transaction"+responseApi);
                    }
                }
                else
                {
                    LOGGER.info("Failed Transaction:  "+response.getMessages().getResultCode());
                    responseApi =  response.getTransactionResponse().getErrors().getError().get(0).getErrorText();
                    LOGGER.info("Failed Transaction"+responseApi);

                }
            }
            LOGGER.info("===============  PayMentGatwayUtil : Inside captureCreditCard :: End  ============= ");

            return responseApi;
     }
     
     
     
     
     public static String voidTransaction(PaymentGateWay  paymentGateWay,String transactionID,Environment environment) {
         LOGGER.info("===============  PayMentGatwayUtil : Inside voidTransaction :: Start  =============transactionID "+transactionID);

         String responseApi = "falied";
            //Common code to set for all requests
            ApiOperationBase.setEnvironment(ProducerUtil.getAuthorizeEnv(environment.getProperty("AuthorizeEnv")));
            MerchantAuthenticationType merchantAuthenticationType  = new MerchantAuthenticationType() ;
            merchantAuthenticationType.setName(paymentGateWay.getAuthorizeLoginId());
            merchantAuthenticationType.setTransactionKey(paymentGateWay.getAuthorizeTransactionKey());
            ApiOperationBase.setMerchantAuthentication(merchantAuthenticationType);

            // Create the payment transaction request
            TransactionRequestType txnRequest = new TransactionRequestType();
            txnRequest.setTransactionType(TransactionTypeEnum.VOID_TRANSACTION.value());
            txnRequest.setRefTransId(transactionID);

            // Make the API Request
            CreateTransactionRequest apiRequest = new CreateTransactionRequest();
            apiRequest.setTransactionRequest(txnRequest);
            CreateTransactionController controller = new CreateTransactionController(apiRequest);
            controller.execute(); 

            CreateTransactionResponse response = controller.getApiResponse();

            if (response!=null) {
                // If API Response is ok, go ahead and check the transaction response
                if (response.getMessages().getResultCode() == MessageTypeEnum.OK) {
                    TransactionResponse result = response.getTransactionResponse();
                    LOGGER.info("===============  PayMentGatwayUtil : Inside voidTransaction :: getTransactionResponse  : ");

                    if (result.getMessages() != null) {
                        LOGGER.info("Successfully created transaction with Transaction ID: " + result.getTransId());
                        LOGGER.info("Response Code: " + result.getResponseCode());
                        LOGGER.info("Message Code: " + result.getMessages().getMessage().get(0).getCode());
                        LOGGER.info("Description: " + result.getMessages().getMessage().get(0).getDescription());
                        LOGGER.info("Auth Code: " + result.getAuthCode());
                        responseApi = "Success";
                    }
                    else {
                        LOGGER.info("Failed Transaction.");
                        if (response.getTransactionResponse().getErrors() != null) {
                            LOGGER.info("Error Code: " + response.getTransactionResponse().getErrors().getError().get(0).getErrorCode());
                            responseApi =  response.getTransactionResponse().getErrors().getError().get(0).getErrorText();
                            LOGGER.info("Error message: " + response.getTransactionResponse().getErrors().getError().get(0).getErrorText());
                        }
                    }
                }
                else {
                    LOGGER.info("Failed Transaction.");
                    if (response.getTransactionResponse() != null && response.getTransactionResponse().getErrors() != null) {
                        LOGGER.info("Error Code: " + response.getTransactionResponse().getErrors().getError().get(0).getErrorCode());
                        LOGGER.info("Error message: " + response.getTransactionResponse().getErrors().getError().get(0).getErrorText());
                        responseApi =  response.getTransactionResponse().getErrors().getError().get(0).getErrorText();

                    }
                    else {
                        LOGGER.info("Error Code: " + response.getMessages().getMessage().get(0).getCode());
                        LOGGER.info("Error message: " + response.getMessages().getMessage().get(0).getText());
                        responseApi =  response.getTransactionResponse().getErrors().getError().get(0).getErrorText();

                    }
                }
            }
            else {
                LOGGER.info("Null Response.");
            }
            LOGGER.info("===============  PayMentGatwayUtil : Inside voidTransaction :: End  ============= ");

            return responseApi;

        }
     
     
  //   public static String refundTransaction(String apiLoginId, String transactionKey, Double transactionAmount, String transactionID,String creditCardNumber,String expiryDate) {
       public static String refundTransaction(PaymentGateWay paymentGateWay, String transactionID,Environment environment) {
           LOGGER.info("===============  PayMentGatwayUtil : Inside refundTransaction :: Start  =============transactionID "+transactionID);

    	   String expiryDate = Integer.toString(paymentGateWay.getExpYear());
           expiryDate =expiryDate.substring(expiryDate.length() - 2);
           String expireMonth = Integer.toString(paymentGateWay.getExpMonth());
                  if(expireMonth.length()<2){
                      expireMonth = "0"+expireMonth;
                  }
            LOGGER.info("expireMonth-->"+expireMonth);
            String expMonthDate = expireMonth.concat(expiryDate);
            LOGGER.info("expMonthDate-->"+expMonthDate);
         String responseApi = "falied";

            //Common code to set for all requests
            ApiOperationBase.setEnvironment(ProducerUtil.getAuthorizeEnv(environment.getProperty("AuthorizeEnv")));

            MerchantAuthenticationType merchantAuthenticationType  = new MerchantAuthenticationType() ;
            merchantAuthenticationType.setName(paymentGateWay.getAuthorizeLoginId());
            merchantAuthenticationType.setTransactionKey(paymentGateWay.getAuthorizeTransactionKey());
            ApiOperationBase.setMerchantAuthentication(merchantAuthenticationType);

            // Create a payment object, last 4 of the credit card and expiration date are required
            PaymentType paymentType = new PaymentType();
            CreditCardType creditCard = new CreditCardType();
            creditCard.setCardNumber(paymentGateWay.getCcNumber());
            creditCard.setExpirationDate(expMonthDate);
            paymentType.setCreditCard(creditCard);

            // Create the payment transaction request
            TransactionRequestType txnRequest = new TransactionRequestType();
            txnRequest.setTransactionType(TransactionTypeEnum.REFUND_TRANSACTION.value());
            txnRequest.setRefTransId(transactionID);
            txnRequest.setAmount(new BigDecimal(5.0));
            txnRequest.setPayment(paymentType);

            // Make the API Request
            CreateTransactionRequest apiRequest = new CreateTransactionRequest();
            apiRequest.setTransactionRequest(txnRequest);
            CreateTransactionController controller = new CreateTransactionController(apiRequest);
            controller.execute(); 

            CreateTransactionResponse response = controller.getApiResponse();

            if (response!=null) {
                // If API Response is ok, go ahead and check the transaction response
                if (response.getMessages().getResultCode() == MessageTypeEnum.OK) {
                	LOGGER.info(
							"===============  PayMentGatwayUtil : Inside refundTransaction :: getTransactionResponse  : ");

                    TransactionResponse result = response.getTransactionResponse();
                    if (result.getMessages() != null) {
                        LOGGER.info("Successfully created transaction with Transaction ID: " + result.getTransId());
                        LOGGER.info("Response Code: " + result.getResponseCode());
                        LOGGER.info("Message Code: " + result.getMessages().getMessage().get(0).getCode());
                        LOGGER.info("Description: " + result.getMessages().getMessage().get(0).getDescription());
                        LOGGER.info("Auth Code: " + result.getAuthCode());
                        
                        
                        responseApi = "Success";

                    } else {
                        LOGGER.info("Failed Transaction.");
                        if (response.getTransactionResponse().getErrors() != null) {
                            LOGGER.info("Error Code: " + response.getTransactionResponse().getErrors().getError().get(0).getErrorCode());
                            LOGGER.info("Error message: " + response.getTransactionResponse().getErrors().getError().get(0).getErrorText());
                            responseApi =  response.getTransactionResponse().getErrors().getError().get(0).getErrorText();
                        }
                    }
                } else {
                    LOGGER.info("Failed Transaction.");
                    if (response.getTransactionResponse() != null && response.getTransactionResponse().getErrors() != null) {
                        LOGGER.info("Error Code: " + response.getTransactionResponse().getErrors().getError().get(0).getErrorCode());
                        LOGGER.info("Error message: " + response.getTransactionResponse().getErrors().getError().get(0).getErrorText());
                        responseApi =  response.getTransactionResponse().getErrors().getError().get(0).getErrorText();

                    } else {
                        LOGGER.info("Error Code: " + response.getMessages().getMessage().get(0).getCode());
                        LOGGER.info("Error message: " + response.getMessages().getMessage().get(0).getText());
                        responseApi =  response.getTransactionResponse().getErrors().getError().get(0).getErrorText();

                    }
                }
            } else {
                LOGGER.info("Null Response.");
            }
            LOGGER.info("===============  PayMentGatwayUtil : Inside refundTransaction :: End  ============= ");

            return responseApi;

        }
     
     


     
     
     
     
     public static String testAllPaymentGateway(PaymentGateWay paymentGateWay, int paymentGatwayType,Environment environment) {
         LOGGER.info("===============  PayMentGatwayUtil : Inside testAllPaymentGateway :: Start  =============paymentGatwayType "+paymentGatwayType);

    	 String expiryDate = Integer.toString(paymentGateWay.getExpYear());
         expiryDate =expiryDate.substring(expiryDate.length() - 2);
         String expireMonth = Integer.toString(paymentGateWay.getExpMonth());
                if(expireMonth.length()<2){
                    expireMonth = "0"+expireMonth;
                }
          LOGGER.info("expireMonth-->"+expireMonth);
          String expMonthDate = expireMonth.concat(expiryDate);
         
          
          paymentGateWay.setExpMonthYear(expMonthDate);
          
          
         String response="falied"; 
         String responsefinal ="false";

         switch (paymentGatwayType) { 
      
         case IConstant.STRIPE: 
             
             Token token = null;
             Charge charge =null;
             Map<String, Object> tokenParams = new HashMap<String, Object>();

             try 
            {
                LOGGER.info("Stripe is configured, now processing credit card");
                Stripe.apiKey = paymentGateWay.getStripeAPIKey();
                tokenParams = saveCardAndcreateToken(paymentGateWay.getCcNumber(), expireMonth,  expiryDate, paymentGateWay.getCcCode(), paymentGateWay.getCcType(),environment);
                token =  Token.create(tokenParams);
               
                if (token != null) {
                    Map<String, Object> chargeParams = new HashMap<String, Object>();
                    chargeParams.put("amount", 100);
                    chargeParams.put("currency", "usd");
                    chargeParams.put("description", "Charge for foodkonnekt");
                    chargeParams.put("source", token.getId());
                    chargeParams.put("capture", false);
                    charge = Charge.create(chargeParams);
                    LOGGER.info("Charge api is called and receved status as : " + charge.getStatus());
                    if (charge != null && charge.getStatus() != null && charge.getStatus().equals("succeeded")) {
                        charge = charge.capture();
                        Stripe.apiKey = paymentGateWay.getStripeAPIKey();
                        charge = Charge.retrieve(charge.getId());
                        if (charge != null && charge.getStatus() != null && charge.getStatus().equals("succeeded")) {
                            Refund refund = null;
                            Map<String, Object> refundParams = new HashMap<String, Object>();
                            refundParams.put("charge", charge.getId());
                            refund = Refund.create(refundParams);
                            if (refund.getAmount() != null && refund != null) {
                                LOGGER.info("refund.getId()-----" + refund.getId());
                                responsefinal = "Success";

                            }
                        }
                    }
                }
            }
                 catch (Exception exception) {
                	 LOGGER.error("===============  PayMentGatwayUtil : Inside testAllPaymentGateway :: Exception  ============= " + exception);

                     exception.printStackTrace();
                     responsefinal=    exception.getMessage();
                 } 
             break; 
         
         case IConstant.AUTHORIZE_NET: 
             
             
             
             CreateTransactionResponse   createTransactionResponse = authorizeTestCreditCard(paymentGateWay,environment);
             if (createTransactionResponse!=null) {
                 if (createTransactionResponse.getMessages().getResultCode() == MessageTypeEnum.OK) {
                     TransactionResponse result = createTransactionResponse.getTransactionResponse();
                     LOGGER.info(
							"===============  PayMentGatwayUtil : Inside testAllPaymentGateway :: getTransactionResponse  : ");

                     if (result.getResponseCode().equals("1")) {
                         LOGGER.info(result.toString());
                         LOGGER.info(result.getResponseCode());
                         LOGGER.info("Successful  authorizeTestCreditCard() Credit Card Transaction");
                         LOGGER.info(result.getAuthCode());
                         LOGGER.info(result.getTransId()); //40009206593
                         responsefinal =     captureCreditCard(result.getTransId(),paymentGateWay,environment);
                         if(responsefinal.equalsIgnoreCase("Success")) {
                             responsefinal = "Success";
                         }
                     }
                     else
                     {
                         responsefinal =  createTransactionResponse.getTransactionResponse().getErrors().getError().get(0).getErrorText() ;
                         LOGGER.info("Failed Transaction"+result.getResponseCode());
                         LOGGER.info("UnSuccessful  authorizeTestCreditCard() Credit Card Transaction---"+responsefinal);
                     }
                 }
                 else
                 {
                     responsefinal =  createTransactionResponse.getTransactionResponse().getErrors().getError().get(0).getErrorText();
                     LOGGER.info("Failed Transaction:  "+createTransactionResponse.getMessages().getResultCode());
                     LOGGER.info("UnSuccessful  authorizeTestCreditCard() Credit Card Transaction---"+responsefinal);
                 }
                 
                 
             }
             
             CreateTransactionResponse   transactionResponseForVoid = authorizeTestCreditCard(paymentGateWay,environment);
             if (transactionResponseForVoid!=null) {
                 if (transactionResponseForVoid.getMessages().getResultCode() == MessageTypeEnum.OK) {
                     TransactionResponse result = transactionResponseForVoid.getTransactionResponse();
                     LOGGER.info(
							"===============  PayMentGatwayUtil : Inside testAllPaymentGateway :: getTransactionResponse  : ");

                     if (result.getResponseCode().equals("1")) {
                         LOGGER.info(result.toString());
                         LOGGER.info(result.getResponseCode());
                         LOGGER.info("Successful  authorizeTestCreditCard() Credit Card Transaction");
                         LOGGER.info(result.getAuthCode());
                         LOGGER.info(result.getTransId()); //40009206593
                         responsefinal =     voidTransaction(paymentGateWay, result.getTransId(),environment);
                          if(responsefinal.equalsIgnoreCase("Success")) {
                             responsefinal = "Success";
                         }
                     }
                     else
                     {
                         responsefinal =  transactionResponseForVoid.getTransactionResponse().getErrors().getError().get(0).getErrorText();
                         LOGGER.info("Failed Transaction"+result.getResponseCode());
                         LOGGER.info("UnSuccessful  authorizeTestCreditCard() Credit Card Transaction---"+responsefinal);
                     }
                 }
                 else
                 {
                     responsefinal =  transactionResponseForVoid.getTransactionResponse().getErrors().getError().get(0).getErrorText();
                     LOGGER.info("Failed Transaction:  "+createTransactionResponse.getMessages().getResultCode());
                     LOGGER.info("UnSuccessful  authorizeTestCreditCard() Credit Card Transaction---"+responsefinal);
                 }
                 
             }
             
             break; 
      
         case IConstant.CAYAN:
             responsefinal = "Success"; 
             break; 
       
         case IConstant.T_GATE:
             Response responseTgate = null;
             SmartPaymentsSoapProxy soapProxy=new SmartPaymentsSoapProxy(environment.getProperty("ENDPOINT_PROD"));

            try {
                
                String invoiceNumber =        IConstant.getTestInvoiceNumber();
                responseTgate = soapProxy.processCreditCard(paymentGateWay.getiGateUserName(),
                                paymentGateWay.getiGatePassword(), "auth", paymentGateWay.getCcNumber(), expMonthDate,
                                "Payment Card", paymentGateWay.getCustomerName(), "1.0",invoiceNumber
                                , "", "", "", paymentGateWay.getCcCode(), "<Force>T</Force>");

                if (!environment.getProperty("FOODKONNEKT_APP_TYPE").equals("Prod")) {
                    if (responseTgate != null && responseTgate.getMessage() != null
                                    && (responseTgate.getMessage().contains("CVV2 Mismatch")
                                                    || responseTgate.getMessage().contains("Do Not Honor"))) {
                       
                        responseTgate = soapProxy.processCreditCard(paymentGateWay.getiGateUserName(),
                                        paymentGateWay.getiGatePassword(), "auth", paymentGateWay.getCcNumber(),
                                        expMonthDate, "Payment Card Track",
                                        paymentGateWay.getCustomerName(), "1.0", invoiceNumber, "", "", "", "",
                                        "<Force>T</Force>");
                    }
                }

                if (responseTgate != null && responseTgate.getMessage() != null
                                && responseTgate.getMessage().contains("APPROVAL"))
                {
                    LOGGER.info("Auth response : " + responseTgate.getPNRef());
                    Response responseCapture = soapProxy.processCreditCard(paymentGateWay.getiGateUserName(),
                                    paymentGateWay.getiGatePassword(), "Force", "", "", "", "", "", "",
                                    responseTgate.getPNRef(), "", "", "", "");
        
                            if (responseCapture != null && responseCapture.getMessage() != null
                                            && responseCapture.getMessage().contains("APPROVAL")) 
                            {
                                LOGGER.info("Capture response: "  + responseCapture.getPNRef());
        
                                Response voidResponse = soapProxy.processCreditCard(paymentGateWay.getiGateUserName(),paymentGateWay.getiGatePassword(), "void", "", "", "", "", "", "",
                                                responseCapture.getPNRef(), "", "", "", "<Force>C</Force>");
        
                                if (voidResponse != null && voidResponse.getMessage() != null       && voidResponse.getMessage().contains("APPROVAL"))
                                {
                                    LOGGER.info("Void response : "+ voidResponse.getPNRef());
                                    responsefinal = "Success";
                                }
                    
                                else {
                                    responsefinal = voidResponse.getRespMSG();
                                }
                            }
        
                            else {
                                responsefinal = responseCapture.getRespMSG();
                            }
                }
                else {
                    responsefinal =   responseTgate.getRespMSG();
                }

            } catch (RemoteException e1) {
                responsefinal =   e1.getMessage();
                LOGGER.info("Charge Credit card-IGate  : ERROR" + e1);
                MailSendUtil.sendExceptionByMail(e1,environment);
            }
             break; 
       
         case IConstant.FIRST_DATA: 
             org.json.simple.JSONObject request = getFIRST_DATATransactionRequest(paymentGateWay);
             CardConnectRestClient client = new CardConnectRestClient(CardConnectRestClientExample.ENDPOINT,  CardConnectRestClientExample.USERNAME, CardConnectRestClientExample.PASSWORD);
             org.json.simple.JSONObject firstDataAuthorizeapiResponse = client.authorizeTransaction(request);

             if (firstDataAuthorizeapiResponse.containsKey("resptext")) {
                 String apiResponse = (String) firstDataAuthorizeapiResponse.get("resptext");
               
                 if (apiResponse.equals("Approval")) {
                     String authcode = (String) firstDataAuthorizeapiResponse.get("authcode");
                     String retref = (String) firstDataAuthorizeapiResponse.get("retref");
                     String firstDataToken = (String) firstDataAuthorizeapiResponse.get("token");
                     String amount = (String) firstDataAuthorizeapiResponse.get("amount");

                     org.json.simple.JSONObject request1 = new org.json.simple.JSONObject();
                     request1.put("merchid", paymentGateWay.getFirstDataMerchantId());
                     request1.put("amount", amount);
                     request1.put("currency", "USD");
                     request1.put("retref", retref);
                     request1.put("authcode", authcode);
                     // Invoice ID
                     request1.put("invoiceid", "111");
                     SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
                     request1.put("orderdate", dateFormat.format(new java.util.Date()));
                     CardConnectRestClientExample restClient = new CardConnectRestClientExample();
                   
                     org.json.simple.JSONObject response1 = restClient.captureTransaction(retref, CardConnectRestClientExample.USERNAME, CardConnectRestClientExample.PASSWORD,
                                     request1);
                     LOGGER.info("response1 " + response1);
                     
                     
                     if (response1 != null) {
                         
                         retref = (String) response1.get("retref");
                         String setlstat = response1.get("setlstat").toString();
                         LOGGER.info("setlstat -----> " + setlstat);
                         LOGGER.info("Card captured request passed and return status as " + setlstat);
                         if (setlstat == "Queued for Capture" || setlstat.equals("Queued for Capture")) {
                             request.put("merchid",paymentGateWay.getFirstDataMerchantId());
                             request.put("amount", amount);
                             request.put("currency", "USD");
                             request.put("retref", retref);
                      
                             org.json.simple.JSONObject responserefundTransaction = restClient.refundTransaction(request);
                             LOGGER.info("response1--------------------> " + response1);
                             if (responserefundTransaction != null) {
                                 String response2 = responserefundTransaction.get("resptext").toString();
                                 LOGGER.info("resptext--------->" + response2);
                                 if (response2.equals("Approval")) {
                                     
                                     responsefinal = "Success";

                                 } else {
                                 }
                             }

                         } else if (setlstat == "Accepted" || setlstat.equals("Accepted")) {

                         } else {
                             LOGGER.info("Card captured request failed and return status as " + setlstat);
                         }

                     } else {
                         LOGGER.info("Card captured request failed with empty details");
                     }

                 } else {
                     
                     responsefinal =    apiResponse;
                 }
             }

         
             break; 
             
             
         case IConstant.PAYEEZY: 
             String transctiontag = null;
             String transaction_id = null;
             Boolean errorResponse = false;

             TransactionRequest transactionRequest = getPayeezyPrimaryTransaction(paymentGateWay);
             if (paymentGateWay.getPayeezyMerchnatToken() != null && !paymentGateWay.getPayeezyMerchnatToken().isEmpty()) {
                 PayeezyClientHelper client1 = PayeezyUtilProperties.getPayeezyProperties(paymentGateWay,environment);
                 try {
                     if(paymentGateWay.getMerchanttName()!=null) 
                     {
                         transactionRequest.setReferenceNo("Online order for - "+ProducerUtil.removeSpecialCharecter(paymentGateWay.getMerchanttName()));
                     } 
                     
                     PayeezyResponse payeezyResponse = client1.doPrimaryTransaction(transactionRequest);
                     JSONObject jObject = new JSONObject(payeezyResponse.getResponseBody());
                     
                     if (jObject.has("Error")) {
                         errorResponse =true;
                         
                         JSONObject ja_data = jObject.getJSONObject("Error");
                         JSONArray arr = ja_data.getJSONArray("messages");
                         StringBuilder sb = new StringBuilder();
                        
                         for (int i = 0; i <arr.length(); i++) {
                             LOGGER.info("Message code : "+arr.getJSONObject(i).getString("code"));
                             sb.append(arr.getJSONObject(i).getString("code"));
                             sb.append(" "+arr.getJSONObject(i).getString("description"));
                             sb.append("\n");
                         }
                         responsefinal =  sb.toString();
                         LOGGER.info("responsefinal"+responsefinal);
                     }
                     if (jObject.has("transaction_status")) {
                         LOGGER.info("doPrimaryTransaction-----Authorized "+jObject.toString());
                         if (jObject.getString("transaction_status").equals("approved")) {
                             transctiontag = jObject.getString("transaction_tag");
                             transaction_id = jObject.getString("transaction_id");
                             transactionRequest.setTransactionType("CAPTURE");
                             transactionRequest.setTransactionTag(transctiontag);
                             transactionRequest.setAmount("5");
                             
                             if(paymentGateWay.getMerchanttName()!=null) 
                             {
                                 transactionRequest.setReferenceNo("Online order for - "+ProducerUtil.removeSpecialCharecter(paymentGateWay.getMerchanttName()));
                             } 
                             
                             
                             
                             try {
                                 payeezyResponse = client1.doSecondaryTransaction(transaction_id, transactionRequest);
                                 JSONObject payeezyCaputrejObject = new JSONObject(payeezyResponse.getResponseBody());
                                 if (payeezyCaputrejObject.has("transaction_status")) {
                                     LOGGER.info("doSecondaryTransaction-----CAPTURE"+payeezyCaputrejObject);
                                     response = payeezyCaputrejObject.getString("transaction_status");
                                     if (response != null && response.equals("approved")) {
                                         transctiontag = payeezyCaputrejObject.getString("transaction_tag");
                                         transaction_id = payeezyCaputrejObject.getString("transaction_id");
                                         transactionRequest.setTransactionType("REFUND");
                                         transactionRequest.setTransactionTag(transctiontag);
                                         transactionRequest.setAmount("5");
                                         if(paymentGateWay.getMerchanttName()!=null) 
                                         {
                                             transactionRequest.setReferenceNo("Online order for -"+ProducerUtil.removeSpecialCharecter(paymentGateWay.getMerchanttName()));
                                         } 
                                         try {
                                             payeezyResponse = client1.doSecondaryTransaction(transaction_id, transactionRequest);
                                             payeezyCaputrejObject = new JSONObject(payeezyResponse.getResponseBody());
                                             if (payeezyCaputrejObject.has("transaction_status")) {
                                                 LOGGER.info("doSecondaryTransaction-----REFUND"+payeezyCaputrejObject);
                                                 response = payeezyCaputrejObject.getString("transaction_status");
                                                 if (response != null && response.equals("approved")) {
                                                     LOGGER.info("doSecondaryTransaction-- approved---REFUND"+payeezyCaputrejObject);
                                                     transctiontag = payeezyCaputrejObject.getString("transaction_tag");
                                                     transaction_id = payeezyCaputrejObject.getString("transaction_id");
                                                     responsefinal = "Success";
                                                 }
                                             }
                                         } catch (Exception e) {
                                        	 LOGGER.error(
													"===============  PayMentGatwayUtil : Inside testAllPaymentGateway :: Exception  ============= "
															+ e);

                                             LOGGER.error("error: " + e.getMessage());
                                         }

                                     } else {
                                         LOGGER.info("payeezyCaputrejObject-------"   + payeezyResponse.getResponseBody());
                                     }
                                 }

                             } catch (Exception e) {
                            	 LOGGER.error(
										"===============  PayMentGatwayUtil : Inside testAllPaymentGateway :: Exception  ============= "
												+ e);

                                 LOGGER.error("error: " + e.getMessage());
                             }
                         }
                         else {
                             responsefinal =  jObject.getString("transaction_status");
                         }
                     }

                 } catch (Exception e) {
                	 LOGGER.error("===============  PayMentGatwayUtil : Inside testAllPaymentGateway :: Exception  ============= " + e);

                     LOGGER.error("error: " + e.getMessage());
                     responsefinal =   e.getMessage();
                 }
             }
             
             
             if(!errorResponse) {
             
                 LOGGER.info("errorResponse"+errorResponse);
             if (paymentGateWay.getPayeezyMerchnatToken() != null && !paymentGateWay.getPayeezyMerchnatToken().isEmpty()) {
                 PayeezyClientHelper payeezyclient = PayeezyUtilProperties.getPayeezyProperties(paymentGateWay,environment);
                 try {
                     TransactionRequest Payeezyrequest = getPayeezyPrimaryTransaction(paymentGateWay);
                     if(paymentGateWay.getMerchanttName()!=null) 
                     {
                         transactionRequest.setReferenceNo("Online order for -"+ProducerUtil.removeSpecialCharecter(paymentGateWay.getMerchanttName()));
                     } 
                     PayeezyResponse payeezyResponseForVoid = payeezyclient.doPrimaryTransaction(Payeezyrequest);
                     JSONObject jObject = new JSONObject(payeezyResponseForVoid.getResponseBody());
                     if (jObject.has("Error")) {
                         JSONObject ja_data = jObject.getJSONObject("Error");
                         JSONArray arr = ja_data.getJSONArray("messages");
                         for (int i = 0; i < arr.length(); i++) {
                             LOGGER.info(arr.getJSONObject(i).getString("code"));
                             responsefinal = arr.getJSONObject(i).getString("code");
                         }
                     }
                     if (jObject.has("transaction_status")) {
                         LOGGER.info("doPrimaryTransaction-----Authorized"+jObject);
                         if (jObject.getString("transaction_status").equals("approved")) {
                             transctiontag = jObject.getString("transaction_tag");
                             transaction_id = jObject.getString("transaction_id");
                             transactionRequest.setTransactionType("VOID");
                             transactionRequest.setTransactionTag(transctiontag);
                             transactionRequest.setAmount("5");
                             if(paymentGateWay.getMerchanttName()!=null) 
                             {
                                 transactionRequest.setReferenceNo("Online order for -"+ProducerUtil.removeSpecialCharecter(paymentGateWay.getMerchanttName()));
                             } 
                             payeezyResponseForVoid = payeezyclient.doSecondaryTransaction(transaction_id, transactionRequest);
                             jObject = new JSONObject(payeezyResponseForVoid.getResponseBody());
                             if (jObject.has("transaction_status")) {
                                 LOGGER.info("doSecondaryTransaction-----VOID"+jObject);
                                 response = jObject.getString("transaction_status");
                                 if (response != null && response.equals("approved")) {
                                     LOGGER.info("doSecondaryTransaction-- approved---VOID"+jObject);
                                     transctiontag = jObject.getString("transaction_tag");
                                     transaction_id = jObject.getString("transaction_id");
                                     responsefinal = "Success";
                                 }
                             }
                             
                         }
                     }
                     else {
                         JSONObject ja_data = jObject.getJSONObject("Error");
                         JSONArray arr = ja_data.getJSONArray("messages");
                         for (int i = 0; i < arr.length(); i++) {
                          LOGGER.info(arr.getJSONObject(i).getString("code"));
                             responsefinal = arr.getJSONObject(i).getString("code");
                         }
                     }

                 } catch (Exception e) {
                	 LOGGER.error("===============  PayMentGatwayUtil : Inside testAllPaymentGateway :: Exception  ============= " + e);

                     LOGGER.error("error: " + e.getMessage());
                 }
             }
             }
             break;
             
         default: 
             responsefinal = "Invalid gatway select"; 
             break; 
         } 
         LOGGER.info("===============  PayMentGatwayUtil : Inside testAllPaymentGateway :: End  ============= ");

         return responsefinal;

     }
     
     public static TransactionRequest getPayeezyPrimaryTransaction(PaymentGateWay paymentGateWay) {
        LOGGER.info("===============  PayMentGatwayUtil : Inside getPayeezyPrimaryTransaction :: Start  ============= ");

    	 String expiryDate = Integer.toString(paymentGateWay.getExpYear());
         expiryDate =expiryDate.substring(expiryDate.length() - 2);
         String expireMonth = Integer.toString(paymentGateWay.getExpMonth());
                if(expireMonth.length()<2){
                    expireMonth = "0"+expireMonth;
                }
          LOGGER.info("expireMonth-->"+expireMonth);
          String expMonthDate = expireMonth.concat(expiryDate);
          LOGGER.info("expMonthDate-->"+expMonthDate);
          TransactionRequest request = new TransactionRequest();
          request.setAmount("5");
         request.setCurrency("USD");
         request.setPaymentMethod("credit_card");
         request.setTransactionType("AUTHORIZE");
         com.firstdata.payeezy.models.transaction.Card card = new com.firstdata.payeezy.models.transaction.Card();
         card.setCvv(paymentGateWay.getCcCode());
         card.setExpiryDt(expMonthDate);
         card.setName(paymentGateWay.getCustomerName());
         if(paymentGateWay.getCcType().toLowerCase().equalsIgnoreCase("master card")){
             card.setType("mastercard");
         }else{
             card.setType(paymentGateWay.getCcType().toLowerCase());
         }         card.setNumber(paymentGateWay.getCcNumber());
         request.setCard(card);
         com.firstdata.payeezy.models.transaction.Address address = new com.firstdata.payeezy.models.transaction.Address();
         request.setBilling(address);
         address.setState("NY");
         address.setAddressLine1(paymentGateWay.getCustomerAddress());
         address.setZip(paymentGateWay.getZipCode());
         address.setCountry("US");
         
         if(paymentGateWay.getMerchanttName()!=null) 
         {
        	 request.setReferenceNo("Online order for -"+ProducerUtil.removeSpecialCharecter(paymentGateWay.getMerchanttName()));
         } 
         LOGGER.info("===============  PayMentGatwayUtil : Inside getPayeezyPrimaryTransaction :: End  ============= ");

         return request;
     }
     
     
     public static org.json.simple.JSONObject getFIRST_DATATransactionRequest(PaymentGateWay paymentGateWay) {
    	 LOGGER.info("===============  PayMentGatwayUtil : Inside getFIRST_DATATransactionRequest :: Start  ============= ");

         LOGGER.info("\nAuthorization Request");
         org.json.simple.JSONObject request = new org.json.simple.JSONObject();
         request.put("merchid", paymentGateWay.getFirstDataMerchantId());
         request.put("accttype", "VI");
         request.put("account", paymentGateWay.getCcNumber());
         request.put("expiry", paymentGateWay.getExpMonthYear());
         request.put("cvv2", paymentGateWay.getCcCode());
         request.put("amount", "5");
         request.put("currency", "USD");
         request.put("orderid", "12345");
         request.put("name", paymentGateWay.getCustomerName());
         request.put("Street", paymentGateWay.getCustomerAddress());
         request.put("city", "TestCity");
         request.put("region", "TestState");
         request.put("country", "US");
         request.put("tokenize", "Y");
         LOGGER.info("===============  PayMentGatwayUtil : Inside getFIRST_DATATransactionRequest :: End  ============= ");

         return request;
     }
     public static Map<String, Object> saveCardAndcreateToken(String cardNumber,String expireMonth, String expiryDate,String cvc,String cardType,Environment environment){
         Map<String, Object> tokenParams = new HashMap<String, Object>();

         try{
        	 LOGGER.info("===============  PayMentGatwayUtil : Inside saveCardAndcreateToken :: Start  ============= ");

         Map<String, Object> cardParams = new HashMap<String, Object>();
         cardParams.put("number", cardNumber);
         cardParams.put("exp_month", expireMonth);
         cardParams.put("exp_year", expiryDate);
         cardParams.put("cvc", cvc);
         tokenParams.put("card", cardParams);


        
       }
       catch(Exception exception){
    	   LOGGER.error("===============  PayMentGatwayUtil : Inside saveCardAndcreateToken :: Exception  ============= " + exception);

           exception.printStackTrace();
           MailSendUtil.sendExceptionByMail(exception,environment);
       }
         LOGGER.info("===============  PayMentGatwayUtil : Inside saveCardAndcreateToken :: End  ============= ");

         return tokenParams;
     }
}
