package com.foodkonnekt.util;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"message",
"pendingOrderCount",
"futureOrderCount",
"enableNotification",
"autoAccept",
"autoAcceptOrder"
})
public class ResponseDto implements Serializable
{

@JsonProperty("message")
private String message="No Data Found";
@JsonProperty("pendingOrderCount")
private Integer pendingOrderCount=0;
@JsonProperty("futureOrderCount")
private Integer futureOrderCount=0;
@JsonProperty("enableNotification")
private Boolean enableNotification=true;
@JsonProperty("autoAccept")
private Boolean autoAccept=false;
@JsonProperty("autoAcceptOrder")
private Boolean autoAcceptOrder=false;
@JsonIgnore
private Map<String, Object> additionalProperties = new HashMap<String, Object>();
private final static long serialVersionUID = -9059465742959382431L;

@JsonProperty("message")
public String getMessage() {
return message;
}

@JsonProperty("message")
public void setMessage(String message) {
this.message = message;
}

@JsonProperty("pendingOrderCount")
public Integer getPendingOrderCount() {
return pendingOrderCount;
}

@JsonProperty("pendingOrderCount")
public void setPendingOrderCount(Integer pendingOrderCount) {
this.pendingOrderCount = pendingOrderCount;
}

@JsonProperty("futureOrderCount")
public Integer getFutureOrderCount() {
return futureOrderCount;
}

@JsonProperty("futureOrderCount")
public void setFutureOrderCount(Integer futureOrderCount) {
this.futureOrderCount = futureOrderCount;
}

@JsonProperty("enableNotification")
public Boolean getEnableNotification() {
return enableNotification;
}

@JsonProperty("enableNotification")
public void setEnableNotification(Boolean enableNotification) {
this.enableNotification = enableNotification;
}

@JsonProperty("autoAccept")
public Boolean getAutoAccept() {
return autoAccept;
}

@JsonProperty("autoAccept")
public void setAutoAccept(Boolean autoAccept) {
this.autoAccept = autoAccept;
}

@JsonProperty("autoAcceptOrder")
public Boolean getAutoAcceptOrder() {
return autoAcceptOrder;
}

@JsonProperty("autoAcceptOrder")
public void setAutoAcceptOrder(Boolean autoAcceptOrder) {
this.autoAcceptOrder = autoAcceptOrder;
}

@JsonAnyGetter
public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

@JsonAnySetter
public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

}