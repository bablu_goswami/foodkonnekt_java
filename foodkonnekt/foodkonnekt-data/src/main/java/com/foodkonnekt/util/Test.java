package com.foodkonnekt.util;

import java.util.ArrayList;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
 
/* Name of the class has to be "Main" only if the class is public. */
 class Ideone
{
private static ArrayList<Integer> getArrays(int[] input,
			ArrayList<ArrayList<Integer>> array1, ArrayList<Integer> array2) {
		for (int inputChar = 0; inputChar < input.length; inputChar++) {
			if (array2.size() == 0) {
				array2.add(input[inputChar]);
			} else if (array2.get((array2.size()) - 1) < input[inputChar]) {
				array2.add(input[inputChar]);
			} else {
				array1.add(array2);
				array2 = new ArrayList<Integer>();
				array2.add(input[inputChar]);
			}
 
		}
		return array2;
	}
 
	private static int getLargestArray(ArrayList<ArrayList<Integer>> array1, int max) {
		for (ArrayList<Integer> sortedArray : array1) {
			int size = sortedArray.size();
			if (size > max) {
				max = size;
			}
			System.out.println(sortedArray + " = " + sortedArray.size());
		}
		return max;
	}
 
	private static void output(ArrayList<ArrayList<Integer>> array1,
			int max) {
		for (ArrayList<Integer> largestNumber : array1) {
			if (largestNumber.size() == max) {
				System.out
						.println("First Number of Largest Array "+largestNumber.get(0));
			}
		}
	}
 
	public static void main(String[] args) {
		int numberArr[] = { 1, 4, 7, 6, 8, 9, 10, 11, 13, 16, 2, 9, 1, 3, 5, 6, 8 };
 
		ArrayList<ArrayList<Integer>> outputList = new ArrayList<ArrayList<Integer>>();
		ArrayList<Integer> numberList = new ArrayList<Integer>();
 
		numberList = getArrays(numberArr, outputList, numberList);
		outputList.add(numberList);
 
		int max = 0;
		max = getLargestArray(outputList, max);
		output(outputList, max);
 
	}
	}
