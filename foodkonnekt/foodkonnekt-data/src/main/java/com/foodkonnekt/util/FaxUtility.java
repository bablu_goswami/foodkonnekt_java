package com.foodkonnekt.util;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URISyntaxException;
import java.util.Date;
import java.util.List;

import net.interfax.rest.client.InterFAX;
import net.interfax.rest.client.domain.APIResponse;
import net.interfax.rest.client.domain.OutboundFaxStructure;
import net.interfax.rest.client.exception.UnsuccessfulStatusCodeException;
import net.interfax.rest.client.impl.DefaultInterFAXClient;

import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.foodkonnekt.model.Address;
import com.foodkonnekt.model.OnlineOrderNotificationStatus;
import com.foodkonnekt.model.OrderR;
import com.foodkonnekt.model.Vendor;
import com.foodkonnekt.repository.OnlineNotificationStatusRepository;
import com.foodkonnekt.repository.VendorRepository;
import com.itextpdf.text.Document;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.XMLWorkerHelper;


/*import  net.interfax.rest.client.*;
import  net.interfax.rest.client.InterFAX;
import net.interfax.rest.client.domain.APIResponse;
import net.interfax.outbound.Sendfax;
import net.interfax.outbound.SendfaxResponse;
import net.interfax.rest.client.impl.DefaultInterFAXClient;

*/

public class FaxUtility {
	public static int FAX_SEND_COUNT = 0;
    protected static  int ERROR_MAIL_SEND_COUNT = 0;
	//static String USERNAME = "mkonnekt"; 
	//static String PASSWORD = "mK0nn3kt123";
	//static String FAX_NUMBER = "0012142761933"; 
	//static String TEXT_TO_FAX = "test dummy fax number-- arpit";
	
	//c://faxData/
	//file:///C:/faxData/pdf.pdf
	
	//static String FILE_TYPE = "TXT";
		
	private static final Logger LOGGER= LoggerFactory.getLogger(FaxUtility.class);


	public static void main(String[] args) {
		Date d = new Date();
		LOGGER.info("====FaxUtility : Main : Date ====: "+d);
		
		 String inputLinee;
		 String htmlToConvert="";
		 byte[] byteArray = null;
		 
		 
		 
		// FaxUtility.sendPdfFileInFax(FAX_NUMBER, "http://localhost:8080/foodkonnekt-web/orderRec?orderid=NTgw==&customerid=Mzc0", "00000");
		
		 
		 /*		
		try {
			
            
           URL foodkonnektOrderUrl = new URL("https://www.foodkonnekt.com/foodkonnekt-web/orderRec?orderid=NTg2Ng==&customerid=MjkxMw==");
            BufferedReader inn = new BufferedReader(
                        new InputStreamReader(
                        		foodkonnektOrderUrl.openStream()));

            while ((inputLinee = inn.readLine()) != null) {
            	htmlToConvert = htmlToConvert+inputLinee;
                LOGGER.info(inputLinee);
            }
            inn.close();
            writeFile(htmlToConvert,d);
           

          
	    	Document document = new Document();
	    	PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream("c://faxData/pdf.pdf"));
	        document.open();
	        XMLWorkerHelper.getInstance().parseXHtml(writer, document,
	                new FileInputStream("c://faxData/orderHtml.html"));	
	         document.close();
			
			
			 Document document = new Document(PageSize.LETTER);
		      PdfWriter.getInstance(document, new FileOutputStream("c://faxData/pdf.pdf"));
		      document.open();
		     // document.addAuthor("Real Gagnon");
		     // document.addCreator("Real's HowTo");
		     // document.addSubject("Thanks for your support");
		      //document.addCreationDate();
		      //document.addTitle("Please read this");

		      HTMLWorker htmlWorker = new HTMLWorker(document);
		      File file = new File("c://faxData/orderHtml.html");
	             
	             BufferedReader br = new BufferedReader(new FileReader(file));
	            String str="";
	             String st;
	             while ((st = br.readLine()) != null) {
	            	 str = str+st;
	               LOGGER.info(st);
	             }
		      htmlWorker.parse(new StringReader(str));
		      document.close();
		      LOGGER.info("Done");
		      
             
             
             
             
             
             
             
             
       
            
           
            net.interfax.outbound.InterFaxSoapStub theBinding = (net.interfax.outbound.InterFaxSoapStub) new net.interfax.outbound.InterFaxLocator()
					.getInterFaxSoap();
			theBinding.setTimeout(60000);
			byte[] fileData = transformToBytes(FILE_NAME);
			//byte[] fileData = byteArray;
			LOGGER.info("Sending Fax using sendFax().  Document size: " + fileData.length);
			LOGGER.info("Sending Fax using sendCharFax()");
			Sendfax theParams = new Sendfax(USERNAME, PASSWORD, FAX_NUMBER, fileData, "PDF");

			SendfaxResponse theResponse = theBinding.sendfax(theParams);
			String response = new Date() + "<--Sendfax() call returned with code: " + theResponse.getSendfaxResult();
			LOGGER.info(new Date() + "<--Sendfax() call returned with code: " + theResponse.getSendfaxResult());
			writeFile(response.toString(),d);
            LOGGER.info(d);
            
	
}catch(Exception e) {
	LOGGER.error("error: " + e.getMessage());
	writeFile(e.toString(),d);
}

		*/
		

	}

	public static void writeFile(String message, Date d, String orderId,Environment environment) {
		LOGGER.info("===============  FaxUtility : Inside writeFile :: Start  ============= ");

		File file = new File(environment.getProperty("FAX_FILE_PATH")+orderId + ".html");
		
		LOGGER.info("====file ==="+file.getAbsolutePath());
		if (file.getParentFile().mkdir())
			try {
				file.createNewFile();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		FileOutputStream fos = null;
		FileWriter writer = null;
		PrintWriter out = null;
		BufferedWriter bw = null;
		try {

			fos = new FileOutputStream(file);
			writer = new FileWriter(file, true);
			bw = new BufferedWriter(writer);
			out = new PrintWriter(writer);
			bw.write(message);
			bw.newLine();
			bw.flush();
		} catch (IOException e) {
			LOGGER.error("===============  FaxUtility : Inside writeFile :: Exception  ============= " + e);

			LOGGER.error("error: " + e.getMessage());
		}

	}

	private static byte[] transformToBytes(String aFilename) throws Exception {
		if (aFilename == null) {
			throw new NullPointerException("aFilename is null");
		}
		File theFile = new File(aFilename);
		if (!theFile.isFile()) {
			throw new IllegalArgumentException("Path doesn't represent a file: " + aFilename);
		}
		if (!theFile.exists()) {
			throw new IllegalArgumentException("File not found: " + aFilename);
		}
		InputStream theIs = new BufferedInputStream(new FileInputStream(theFile));
		ByteArrayOutputStream theRawData = new ByteArrayOutputStream();
		byte theBuffer[] = new byte[1024];
		int theBytesRead;
		try {
			while ((theBytesRead = theIs.read(theBuffer)) != -1) {
				if (theBytesRead < 1024) {
					byte theSlice[] = new byte[theBytesRead];
					System.arraycopy(theBuffer, 0, theSlice, 0, theBytesRead);
					theRawData.write(theSlice);
				} else {
					theRawData.write(theBuffer);
				}
			}
		} finally {
			theIs.close();
			theRawData.close();
		}
		return theRawData.toByteArray();
	}
	
	  	public  static   String sendPdfFileInFax( String faxNumber,  String htmltopdf, String orderid,Environment environment)
	 {
	  		LOGGER.info("===============  FaxUtility : Inside sendPdfFileInFax :: Start  ============= ");

		 //FAX_NUMBER = faxNumber;
	  	  String response =null;
		 LOGGER.info("FAX_NUMBER--------------------------------------------->inside sendPdfFileInFax"+faxNumber);
		 String inputLinee;
		 String htmlToConvert="";
		 byte[] byteArray = null;
		 Date d = new Date();
         try {
	            /*URL foodkonnektOrderUrl = new URL(url);
	            BufferedReader inn = new BufferedReader(new InputStreamReader(	foodkonnektOrderUrl.openStream()));
	           
	            while ((inputLinee = inn.readLine()) != null) {
	               LOGGER.info(inputLinee);
	            	htmlToConvert = htmlToConvert+inputLinee;
	              //  LOGGER.info(inputLinee);
	            }
	            inn.close();*/
             htmlToConvert =htmltopdf;
	            LOGGER.info("======== htmltopdf ======"+htmlToConvert);
			writeFile(htmlToConvert, d, orderid,environment);
	            Document document = new Document();
	            PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(environment.getProperty("FAX_FILE_PATH")+ orderid+".pdf"));
	            document.open();
	            XMLWorkerHelper.getInstance().parseXHtml(writer, document,   new FileInputStream(environment.getProperty("FAX_FILE_PATH")+orderid + ".html")); 
	             document.close();
	 			byte[] fileData = transformToBytes(environment.getProperty("FAX_FILE_NAME")+orderid+".pdf");
	 			/*LOGGER.info("Sending Fax using sendFax().  Document size: " + fileData.length);
	 			LOGGER.info("Sending Fax using sendCharFax()");
	 			LOGGER.info(response);
	 			*/
	 			
	 			String filepath=(environment.getProperty("FAX_FILE_PATH")+orderid+".pdf");
	 	       // InterFAX interfax = new DefaultInterFAXClient(USERNAME, PASSWORD);
	 	          
               // APIResponse apiResponse = interfax.sendFax(faxNumber, file);
                
                
               // sendFaxFile(faxNumber, filepath,);
                /*
                LOGGER.info("apiResponse_getResponseBody " + apiResponse.getResponseBody());
                LOGGER.info("apiResponse_getStatusCode " + apiResponse.getStatusCode());
                LOGGER.info("apiResponse_getHeaders " + apiResponse.getHeaders());
                *///LOGGER.info("apiResponse_getClass " + apiResponse.getClass());

         }catch(Exception e)
			{
        	 LOGGER.error("===============  FaxUtility : Inside sendPdfFileInFax :: Exception  ============= " + e);

				LOGGER.error("error: " + e.getMessage());
			}
         LOGGER.info("===============  FaxUtility : Inside sendPdfFileInFax :: End  ============= ");

		 return response;
	
	 }

	  	
	  	public 	static void sendFaxFile(final String faxNumber, final String filePath,final VendorRepository vendorRepository,final OnlineNotificationStatusRepository onlineNotificationStatusRepository,final OrderR orderR,int faxCount,Environment environment) throws ClientProtocolException, IOException
      {
LOGGER.info("===============  FaxUtility : Inside sendFaxFile :: Start  ============= ");

		CloseableHttpClient httpclient = HttpClients.createDefault();
		try {
			Date today = (orderR.getMerchant() != null && orderR.getMerchant().getTimeZone() != null
					&& orderR.getMerchant().getTimeZone().getTimeZoneCode() != null)
							? DateUtil.getCurrentDateForTimeZonee(orderR.getMerchant().getTimeZone().getTimeZoneCode())
							: new Date();
			if(faxCount!=3)
			{
			InterFAX interFAX = new DefaultInterFAXClient("mkonnekt", "mK0nn3kt123");
			try {
				String URLPATH = "https://rest.interfax.net/outbound/faxes?faxNumber="
				+ faxNumber;

			LOGGER.info("=== FaxUtility : Inside sendFaxFile :: URLPATH==== " + URLPATH);
			LOGGER.info("======= FaxUtility : Inside sendFaxFile :: filePath =====" + filePath);

		HttpPost httppost = new HttpPost(
				"https://rest.interfax.net/outbound/faxes?faxNumber="
						+ faxNumber);
		httppost.addHeader("Authorization",
				"Basic bWtvbm5la3Q6bUswbm4za3QxMjM=");

		httppost.addHeader("content-type", "application/pdf");
		FileBody bin = new FileBody(new File(filePath));

		HttpEntity reqEntity = MultipartEntityBuilder.create()
				.addPart("bin", bin).build();
		httppost.setEntity(reqEntity);
		System.out
				.println("executing request " + httppost.getRequestLine());
		CloseableHttpResponse response1 = httpclient.execute(httppost);

//				APIResponse apiResponse = interFAX.sendFax(faxNumber, new File(filePath));
				OnlineOrderNotificationStatus onlineNotificationStatus =  new OnlineOrderNotificationStatus();
				LOGGER.info("");
				String url = "";
				String faxId = "";
				for(int i=0; i< response1.getAllHeaders().length; i++ )
                    {
                            if((response1.getAllHeaders()[i]).toString().contains("Location:")) {
                                LOGGER.info("Location in :--------------"+(response1.getAllHeaders()[i]).toString());
                                url =response1.getAllHeaders()[i].toString();
                            }
                    }


                    if(url!=null && url.length()>0) {
                    	faxId =  url.split("faxes/")[1];
                          LOGGER.info("faxId :: "+faxId);
                         }
				
				if(faxId!=null && !faxId.isEmpty())
				{
					String status= ProducerUtil.getFaxStatus(faxId);
					 JsonNode node = new ObjectMapper().readTree(status);
					 String faxStatus=node.findPath("status").toString();
					 if(faxStatus!=null && Integer.parseInt(faxStatus) > 0)
					 {
						 sendFaxFile( faxNumber,  filePath, vendorRepository,onlineNotificationStatusRepository, orderR,faxCount,environment);
					 }else{
				 onlineNotificationStatus.setMerchantId(orderR.getMerchant().getId());
                 onlineNotificationStatus.setOrderId(orderR.getId());
                 onlineNotificationStatus.setFaxUrl(url);
                 onlineNotificationStatus.setFaxId(faxId);
                 onlineNotificationStatus.setUpdatedTime(today);
                 onlineNotificationStatus.setStatus("Pending");
                 onlineNotificationStatusRepository.save(onlineNotificationStatus);
				faxSendStatus(faxId,url,onlineNotificationStatusRepository,interFAX,orderR,onlineNotificationStatus,vendorRepository,1,environment);
					 }
				}else {
					faxCount++;
					sendFaxFile( faxNumber,  filePath, vendorRepository,onlineNotificationStatusRepository, orderR,faxCount,environment);
				}
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				faxCount++;
				sendFaxFile( faxNumber,  filePath, vendorRepository,onlineNotificationStatusRepository, orderR,faxCount,environment); 
				LOGGER.info("Exception :: sendFax :: "+e1);
			}finally{
				interFAX.closeClient();
			}
			
//			String URLPATH = "https://rest.interfax.net/outbound/faxes?faxNumber="
//					+ faxNumber;
//
//			LOGGER.info("URLPATH " + URLPATH);
//			LOGGER.info("filePath " + filePath);
//
//			HttpPost httppost = new HttpPost(
//					"https://rest.interfax.net/outbound/faxes?faxNumber="
//							+ faxNumber);
//			httppost.addHeader("Authorization",
//					"Basic bWtvbm5la3Q6bUswbm4za3QxMjM=");
//
//			httppost.addHeader("content-type", "application/pdf");
//			FileBody bin = new FileBody(new File(filePath));
//
//			HttpEntity reqEntity = MultipartEntityBuilder.create()
//					.addPart("bin", bin).build();
//			httppost.setEntity(reqEntity);
//			System.out
//					.println("executing request " + httppost.getRequestLine());
//			CloseableHttpResponse response1 = httpclient.execute(httppost);
//			StringBuilder responseBuilder=new StringBuilder();
//			BufferedReader rd = new BufferedReader(new InputStreamReader(response1.getEntity().getContent()));
//            String line = "";
//            while ((line = rd.readLine()) != null) {
//                responseBuilder.append(line);
//            }
//			try {
//				if (response1 != null) {
//					
//					System.out
//							.println("----------------------------------------");
//					LOGGER.info(response1.getStatusLine());
//					HttpEntity resEntity = response1.getEntity();
//					 if(FAX_SEND_COUNT ==3 ) {
//
//	                       LOGGER.info("inside  resEntity----" +FAX_SEND_COUNT);
//	                       resEntity =null;
//	                       LOGGER.info("inside  resEntity----" +FAX_SEND_COUNT);
//	                       final Vendor vendor = vendorRepository.findOne(orderR.getMerchant().getOwner().getId());
//	                       final  List<OnlineOrderNotificationStatus>  onlineNotificationStatuslist = onlineNotificationStatusRepository.findByOnlineNotificationStatusListByorderId(orderR.getId());
//	                       LOGGER.info(" vendor :" + vendor.getId());
//	                       LOGGER.info(" onlineNotificationStatuslist :" + onlineNotificationStatuslist.size());
//
//	                       new Thread(){
//	                           public void run(){
//	                                   MailSendUtil.onlineOrderFailedNotification(vendor, onlineNotificationStatuslist,orderR );                                
//	                       if(ERROR_MAIL_SEND_COUNT<1) {
//	                           MailSendUtil.sendMailSystemLog("Foodkonnekt", environment.getProperty("FOODKONNEKT_APP_TYPE"),"FCM Notification Error for MerchantId "+orderR.getMerchant().getId()+" and OrderId "+orderR.getId(),"fax failed");
//	                            ERROR_MAIL_SEND_COUNT++;
//	                       }
//	                           }
//	                       }.start();
//	                       
//	                   
//					 }
//					 
//					   FAX_SEND_COUNT++;
//	                   LOGGER.info("response1" +FAX_SEND_COUNT);
//	                   
//					if (resEntity != null) { 
//	                       LOGGER.info("Response content length: "+ resEntity.getContentLength());
//	                        if(resEntity.getContentLength() > 0)
//	                           {                                    
//	                               LOGGER.info("Response content "+resEntity.getContentLength()+" is Positive");
//	                               new Thread(new Runnable() {
//	                                   
//	                                   public void run() {
//	                                       
//	                                       LOGGER.info("inside thread");
//	                                       try {                                           
//	                                           
//	                                           OnlineOrderNotificationStatus onlineNotificationStatus =  new OnlineOrderNotificationStatus();
//	                                           onlineNotificationStatus.setMerchantId(orderR.getMerchant().getId());
//	                                           onlineNotificationStatus.setOrderId(orderR.getId());
//	                                           onlineNotificationStatus.setUpdatedTime(new Date());
//	                                           onlineNotificationStatusRepository.save(onlineNotificationStatus);  
//	                                           
//	                                           Thread.sleep(10000);
//	                                       
//	                                           sendFaxFile( faxNumber,  filePath, vendorRepository,onlineNotificationStatusRepository, orderR);                                            
//	                                       } catch (Exception e) {
//	                                           LOGGER.error("error: " + e.getMessage());
//	                                       }  
//	                                   }
//	                               }).start();
//	                           }
//	                           else if(resEntity.getContentLength() < 0)
//	                           {
//	                               
//	                               new Thread(new Runnable() {
//	                                   public void run() {
//	                                       LOGGER.info("inside thread");
//	                                     
//	                                       try {
//	                                  //---------------------for testing 
//	                                           
//	                                           OnlineOrderNotificationStatus onlineNotificationStatus =  new OnlineOrderNotificationStatus();
//	                                           onlineNotificationStatus.setMerchantId(orderR.getMerchant().getId());
//	                                           onlineNotificationStatus.setOrderId(orderR.getId());
//	                                           onlineNotificationStatus.setUpdatedTime(new Date());
//	                                           onlineNotificationStatusRepository.save(onlineNotificationStatus);  
//	                                           
//	                                         
//	                                           Thread.sleep(10000);
//	                                  //------------------------
//	                                           sendFaxFile( faxNumber,  filePath, vendorRepository,onlineNotificationStatusRepository, orderR);                                                                  
//	                                        
//	                                       } catch (Exception e) {
//	                                           LOGGER.error("error: " + e.getMessage());
//	                                       }  
//	                                   }
//	                               }).start();
//	                               
//	                        
//	                               LOGGER.info("Response content "+resEntity.getContentLength()+" is Negative");
//	                           }
//	                           else
//	                           {
//
//	                                 String faxLocationUrl = null;
//	                                 String [] faxLocation = null;
//	                                 
//	                                 for(int i=0; i< response1.getAllHeaders().length; i++ )
//	                                   {
//	                                           if((response1.getAllHeaders()[i]).toString().contains("Location:")) {
//	                                               LOGGER.info("Location in :--------------"+(response1.getAllHeaders()[i]).toString());
//	                                               faxLocationUrl =response1.getAllHeaders()[i].toString();
//	                                           }
//	                                   }
//
//
//	                                   if(faxLocationUrl!=null) {
//	                                        faxLocation =  faxLocationUrl.split("faxes/");
//	                                         LOGGER.info(faxLocation[1]);
//	                                        }
//
//	                                   
//	                                   OnlineOrderNotificationStatus onlineNotificationStatus =  new OnlineOrderNotificationStatus();
//	                                   onlineNotificationStatus.setMerchantId(orderR.getMerchant().getId());
//	                                   onlineNotificationStatus.setOrderId(orderR.getId());
//	                                   onlineNotificationStatus.setFaxUrl(faxLocationUrl);
//	                                   onlineNotificationStatus.setFaxId(faxLocation[1]);
//	                                   onlineNotificationStatus.setUpdatedTime(new Date());
//	                                   onlineNotificationStatusRepository.save(onlineNotificationStatus);                               
//	                               LOGGER.info("Response content "+resEntity.getContentLength()+" is neither Positive nor Negative ");
//	                           }
//	                   }
//					EntityUtils.consume(resEntity);	
//				}
//			} finally {
//				response1.close();
//			}
		}else{
				sendFaxFailedmail(vendorRepository, onlineNotificationStatusRepository, orderR,environment);
		}
		} catch(Exception e) {
			LOGGER.error("error: " + e.getMessage());
			LOGGER.info("Exception :: "+e);
			
//			httpclient.close();
		}
	}
	  	public static void faxSendStatus(final String faxId,final String url,final OnlineNotificationStatusRepository onlineNotificationStatusRepository,InterFAX interFAX,final OrderR orderR,OnlineOrderNotificationStatus onlineNotificationStatus,VendorRepository vendorRepository,int count,Environment environment)
	  	{
           try{
        	   Date today = (orderR.getMerchant() != null && orderR.getMerchant().getTimeZone() != null
   					&& orderR.getMerchant().getTimeZone().getTimeZoneCode() != null)
   							? DateUtil.getCurrentDateForTimeZonee(orderR.getMerchant().getTimeZone().getTimeZoneCode())
   							: new Date();
   							
			  if(count!=3)
			  {
			//reload the fax data

				 String status= ProducerUtil.getFaxStatus(faxId);
				 JsonNode node = new ObjectMapper().readTree(status);
				 String faxStatus=node.findPath("status").toString();
				 String uri=node.findPath("uri").toString();
				 String attemptsToPerform=node.findPath("status").toString();
				  String faxLocationUrl = null;
                  String [] faxLocation = null;
                  if(onlineNotificationStatusRepository.findByFaxId(faxId)!=null)
                  {
                	  onlineNotificationStatus=onlineNotificationStatusRepository.findByFaxId(faxId);
                  }
                    if(uri!=null) {
                         faxLocation =  uri.split("faxes/");
                          LOGGER.info(faxLocation[1]);
                         }
			  
			// wait if fax is still sending

				if (Integer.parseInt(faxStatus) < 0) {
				  LOGGER.info("Waiting 10s");
				  if(count==1)
				  {
	                    onlineNotificationStatus.setMerchantId(orderR.getMerchant().getId());
	                    onlineNotificationStatus.setOrderId(orderR.getId());
	                    onlineNotificationStatus.setFaxUrl(url);
	                    onlineNotificationStatus.setFaxId(faxLocation[1]);
	                    onlineNotificationStatus.setUpdatedTime(today);
	                    onlineNotificationStatus.setStatus("Pending");
	                    onlineNotificationStatusRepository.save(onlineNotificationStatus);
				  Thread.sleep(60000*5);
				  faxSendStatus(faxId,url,onlineNotificationStatusRepository,interFAX,orderR,onlineNotificationStatus,vendorRepository,++count, environment);
				  }else if(count==2){
					  sendFaxFailedmail(vendorRepository,onlineNotificationStatusRepository,orderR, environment);
					  
				  }
			// if the status is 0 the fax has been sent

			  } else if (Integer.parseInt(faxStatus) == 0) {
				  LOGGER.info("Sent!");
				  onlineNotificationStatus.setMerchantId(orderR.getMerchant().getId());
                  onlineNotificationStatus.setOrderId(orderR.getId());
                  onlineNotificationStatus.setFaxUrl(url);
                  onlineNotificationStatus.setFaxId(faxLocation[1]);
                  onlineNotificationStatus.setUpdatedTime(today);
                  onlineNotificationStatus.setStatus("Sent");
                  onlineNotificationStatusRepository.save(onlineNotificationStatus);			  
			// if the status is above 0 an error has occured

			  } else if (Integer.parseInt(faxStatus) > 0) {
					sendFaxFailedmail(vendorRepository, onlineNotificationStatusRepository, orderR,environment);
			  onlineNotificationStatus.setMerchantId(orderR.getMerchant().getId());
              onlineNotificationStatus.setOrderId(orderR.getId());
              onlineNotificationStatus.setFaxUrl(url);
              onlineNotificationStatus.setFaxId(faxLocation[1]);
              onlineNotificationStatus.setUpdatedTime(today);
              onlineNotificationStatus.setStatus("Failed");
              onlineNotificationStatusRepository.save(onlineNotificationStatus);
              
			  }
				}
			
}catch(Exception e){
	
	
}
       
}
	  	
	    public  static   String htmltoPdf( OrderR order,  String merchantLogo, String orderDetail)
	     {
	    	LOGGER.info("===============  FaxUtility : Inside htmltoPdf :: Start  ============= ");

	         //FAX_NUMBER = faxNumber;
	          String myvar ="";
//	           myvar = "<%@ taglib uri = \"http://java.sun.com/jsp/jstl/core\" prefix = \"c\" %>"
//	                          + "<%@ taglib prefix=\"fmt\" uri=\"http://java.sun.com/jsp/jstl/fmt\" %>";
	           /*myvar += "<!DOCTYPE html>"+
	                          "<html lang=\"en\">"+
	                          "<head>"+
	                          "<title>FoodKonnekt</title>"+
	                          "<link href=\"resources/img/favicon.ico\" rel=\"icon\" type=\"image/ico\" />";
	        
	               SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy");
	              myvar +=   "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\"/>"
	                          + "<script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\" crossorigin=\"anonymous\"></script>"
	                          + "<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\" crossorigin=\"anonymous\"/>"
	                          + "<link rel=\"stylesheet\" type=\"text/css\" href=\"http://localhost:8080/foodkonnekt-web/resources/orderReceipt/assets/css/styles.css\"/>"
	                          + "<script src=\"resources/js/shop/accordion/jquery-1.11.3.min.js\"></script>" + "" + "</head>"
	                          + "<body>" + "   <div class=\"container\">"
	                          + "      <div style=\"text-align: center; margin: 20px auto\">"
	                          + "         <div class=\"padding-logo\">" + "            <img"
	                          + "               src=\"https://www.foodkonnekt.com/foodkonnekt-web/resources/orderReceipt/assets/images/logo.png\""
	                          + "               style=\"vertical-align: middle;\" alt=\"\" />" + "         </div>"
	                          + "         <div class=\"orange-bar\"></div>" + "      </div>"
	                          + "      <div class=\"row content\">" + "         <div class=\"col-md-12 col-sm-12\">"
	                          + "            <div class=\"col-md-9 text-left\">Order ID: " + order.getId() + "</div>"
	                          + "            <div class=\"col-md-3 text-right tar\">" + "               Date: "
	                          +  simpleDateFormat.format(order.getCreatedOn())
	                          + " " + "            </div>" + "         </div>" + "      </div>"
	                          + "      <!--content ends here-->" + "       <div class=\"orange-line\"></div>" + ""
	                          + "       <div class=\"banner\">" + "           <div class=\"row\">"
	                          + "               <div class=\"col-md-5 col-sm-5 billing_addr\">" + "                   <p>"
	                          + "                       <img src=\"" + merchantLogo + "\" height=\"120px\" width=\"120px\""
	                          + "                           class=\"text-center\" alt=\"\" />" + "                   <h4>"
	                          + "                       <strong> " + order.getMerchant().getName() + "</strong>"
	                          + "                   </h4>";

	          for (Address address : order.getMerchant().getAddresses()) {
	              if(address.getAddress1()!=null &&address.getCity()!=null &&address.getZip()!=null ) {
	              myvar += address.getAddress1() + "<br />" + address.getCity() + ", " + address.getZip(); }
	          }

	          myvar += "                   <br />" + "                   </p>" + "               </div>"
	                          + "               <div class=\"col-md-2 col-sm-2\"></div>"
	                          + "               <div class=\"col-md-5 col-sm-5 billing_addr\">" + "                   <h4>"
	                          + "                       <strong>Billing</strong>" + "                   </h4>" +

	                          "                   <p>" + "                       Mr. " + order.getCustomer().getFirstName()
	                          + "<br />";
	          if(order.getCustomer()!=null &&order.getCustomer().getAddresses()!=null&&!order.getCustomer().getAddresses().isEmpty()&&order.getCustomer().getAddresses().get(0)!=null) {
	          myvar += order.getCustomer().getAddresses().get(0).getAddress1() + "<br />" + order.getCustomer().getEmailId()
	                          + "<br />" + order.getCustomer().getPhoneNumber() + "<br />";
	          }

	          myvar += "                   </p>" +

	                          "               </div>" + "           </div>" + "       </div>" + ""
	                          + "       <h2>order description</h2>" + "      <div class=\"row content1\">"
	                          + "         <div class=\"col-md-12 col-sm-12\">"
	                          + "            <div class=\"col-md-9 text-left\">ORDER TYPE: <font color=\"#f7941d\" style=\"bold\"><b> "
	                          + order.getOrderType() + " </b></font></div>"
	                          + "            <div class=\"col-md-3 text-right\">PAYMENT: <font color=\"#f7941d\" ><b> "
	                          + order.getPaymentMethod() + " </b></font></div>" + "         </div>" + "      </div>"
	                          + "      <!-- content ends here-->" + "      <table class=\"table tablehead1\">"
	                          + "         <thead>" + "            <tr>"
	                          + "               <th class=\"col-md-3 col-sm-3 text-left\">Item</th>"
	                          + "               <th class=\"col-md-3 col-sm-3 text-center\">Price</th>"
	                          + "               <th class=\"col-md-3 col-sm-3 text-center\">Qty</th>"
	                          + "               <th class=\"col-md-3 col-sm-3 text-right\" >Line Total</th>"
	                          + "            </tr>" + "         </thead>" + "         <tbody>   " + orderDetail + "  " + ""
	                          + "                <tr class=\"bottom\">"
	                          + "                    <td class=\"text-left lft\">Sub total</td>"
	                          + "                    <td></td>" + "                    <td></td>"
	                          + "                    <td class=\"text-right rht\">$" + order.getSubTotal() + "</td>"
	                          + "                </tr>" + "" + "                <tr class=\"bottom\">"
	                          + "                    <td class=\"text-left lft\">Tax</td>" + "                    <td></td>"
	                          + "                    <td></td>" + "                    <td class=\"text-right rht\">$"
	                          + order.getTax() + "</td>" + "                </tr>";

	          if (order.getOrderType() == "delivery" && Double.parseDouble(order.getDeliveryFee()) > 0) {
	              myvar += "<tr class=\"bottom\">" + "                        <td class=\"text-left lft\">Delivery Fee</td>"
	                              + "                        <td></td>" + "                        <td></td>"
	                              + "                        <td class=\"text-right rht\">$" + order.getDeliveryFee()
	                              + "</td>" + "                    </tr>";
	          }

	          if (order.getConvenienceFee() != null && !order.getConvenienceFee().isEmpty() && !order.getConvenienceFee().equals(" ") && Double.parseDouble(order.getConvenienceFee())>0) {
	              myvar += "<tr class=\"bottom\">"
	                              + "                    <span style=\"display: none\">" + order.getConvenienceFee() + "</span>"
	                              + "                        <td class=\"text-left lft\">Online Fee</td>"
	                              + "                        <td></td>" + "                        <td></td>"
	                              + "                      <td class=\"text-right rht\">$" + order.getConvenienceFee()
	                              + "</td>" +

	                              "                </tr>";
	          }

	          if (order.getOrderDiscount() > 0 && order.getOrderDiscount() > 0.0) {

	              myvar += "<tr class=\"bottom\">" + "                  "
	                              + "                    <td class=\"text-left lft\">Discount</td>"
	                              + "                    <td></td>" + "                    <td></td>"
	                              + "                    <td class=\"text-right rht\">$" + order.getOrderDiscount() + "</td>"
	                              + "                   " + "                </tr> ";
	          }

	          if (!order.getOrderDiscountsList().isEmpty() && order.getOrderDiscountsList() != null) {

	              myvar += "<tr class=\"bottom\">" + "                  "
	                              + "                    <td class=\"text-left lft\">Discount</td>"
	                              + "                    <td></td>" + "                    <td></td>";
	              for (OrderDiscount discounts : order.getOrderDiscountsList()) {
	                  myvar += "<td class=\"text-right rht\">" + discounts.getCouponCode() + "</td>";
	              }

	              myvar += "                </tr> ";
	          }

	          myvar += "                " + "                <tr class=\"bottom\">"
	                          + "                    <td class=\"text-left lft\">Total</td>" + "                    <td></td>"
	                          + "                    <td></td>"
	                          + "                    <td class=\"text-right rht\">$ "+ order.getOrderPrice() + "</td>"
	                          
	                          + "                </tr>" + "         </tbody>" + "      </table>" + "     "
	                          + "      <div class=\"row\">" + "         <div class=\"copyrights\">"
	                          + "            <p>copyrights</p>" + "         </div>    " + "      </div>" + "   </div>"
	                          + "</body>" +"</html>";*/
	          
	          myvar += "\r\n" + 
	                          "\r\n" + 
	                          "<!DOCTYPE html>\r\n" + 
	                          "<html>\r\n" + 
	                          "<head>\r\n" + 
	                          "<style>\r\n" + 
	                          "table,th,td {\r\n" + 
	                          "    \r\n" + 
	                          "    border-collapse: collapse;\r\n" + 
	                          "}\r\n" + 
	                          "th, td {\r\n" + 
	                          "font-size:10px;\r\n" + 
	                          "    padding: 10px;\r\n" + 
	                          "    text-align: left;\r\n" + 
	                          "    border:0.3px solid #000;\r\n" + 
	                          "}\r\n" + 
	                          "table #01 {\r\n" + 
	                          "    width: 100%;\r\n" + 
	                          "    \r\n" + 
	                          "}\r\n" + 
	                          "</style>\r\n" + 
	                          "</head>\r\n" + 
	                          "<body\r\n" + 
	                          "    style=\"width: 994px; margin: 0px auto; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px;\">\r\n" + 
	                          "    <div style=\"clear: both; padding-bottom: 10px;\">\r\n" + 
	                          "        <div style=\"width: 50%; float: left;\">\r\n" + 
	                          "            <div style=\"margin-top: 20px; font-weight: bold; font-size: 16px;\">\r\n";
	                          
	                          if(order!=null && order.getCustomer()!=null && order.getCustomer().getFirstName()!=null) {
	                                          myvar += ""+order.getCustomer().getFirstName();
	                          }
	                          
	                         myvar += "</div> \r\n" + 
	                          "                \r\n" ;
	                         
	                         if(order.getId()!=null) {
	                          myvar +=    "Order ID: <b>#"+order.getId()+"</b>\r\n";
	                         }
	                          
	                         myvar += "        </div>\r\n" + 
	                          "        <div style=\"width: 70%; float: right; margin-left:30px;\">\r\n" + 
	                          "            <b>FOODKONNEKT</b>\r\n" + 
	                          "        </div>\r\n" + 
	                          "\r\n" + 
	                          "    </div>\r\n" + 
	                          "    <!-- wrapper ends here-->\r\n" + 
	                          "    <div style=\"clear: both;\"></div>\r\n" + 
	                          "    <br />\r\n" + 
	                          "\r\n" + 
	                          "        <div style=\"width: 54%; float: left;\">\r\n" + 
	                          "\r\n" + 
	                          "            <table style=\"width: 94%; float: left; \">\r\n" + 
	                          "                <tr>\r\n" ;
	                         if(order.getOrderType()!=null) {
	                             myvar += "                    <th>"+order.getOrderType()+"</th>\r\n" ;
	                         
	                         myvar += "\r\n" + 
	                          "                </tr>\r\n" + 
	                          "                <tr>\r\n" +
	                          "                    <td>"+order.getOrderType()+" at <b>"+order.getFulfilled_on()+"</b></td>\r\n" + 
	                          "\r\n" + 
	                          "                </tr>\r\n" + 
	                          "\r\n";
	                         }
	                         myvar += "            </table>\r\n" + 
	                          "            \r\n" + 
	                          "            \r\n" + 
	                          "    <table\r\n" + 
	                          "        style=\"width: 94%; float: left; \">\r\n" + 
	                          "        <br/>\r\n" + 
	                          "        <tr style=\"border:1px solid #000\";>\r\n" + 
	                          "            <th style=\"border-bottom:1px solid #000;\">Qty</th>\r\n" + 
	                          "            <th>Description</th>\r\n" + 
	                          "            <th>Price</th>\r\n" + 
	                          "        </tr>\r\n";
	                         
	                         
	                         
	                         
	                         
	                         myvar += orderDetail + 
	                          "        <tr>\r\n" + 
	                          "            <td></td>\r\n" + 
	                          "            <td style=\"text-align:right;\">Subtotal:<br />";
	                         
	                         if(!"Pickup".equalsIgnoreCase(order.getOrderType()) && Double.parseDouble(order.getDeliveryFee()) > 0) {
	                             myvar += "Delivery Fee:<br />\r\n"; 
	                         }
	                          myvar += "                Tax:<br />\r\n" + 
	                          "            Tip:<br />\r\n</td>\r\n" + 
	                          "            <td><b>$"+order.getSubTotal()+"</b><br />";
	                          if(!"Pickup".equalsIgnoreCase(order.getOrderType()) && Double.parseDouble(order.getDeliveryFee()) > 0) {
	                          myvar += "<b>$"+order.getDeliveryFee()+"</b><br />";
	                          }
	                          myvar += "<b>$"+order.getTax()+"</b><br />";
	                          if(order.getTipAmount()!=null && order.getTipAmount() > 0.0){
	                        	  myvar += "<b>$"+order.getTipAmount()+"</b>";
	                          }else{
	                        	  myvar += "<b>---</b>";
	                          }
	                          myvar += "</td>\r\n" ;
	                          
	                          myvar +=		  "        </tr>\r\n" ;
	                         
								
	                          
								myvar += "        <tr>\r\n" + 
	                          "            <td></td>\r\n" + 
	                          "\r\n";
							  
	                          
	                          myvar +="            <td style=\"text-align: right;\">Total:<br />\r\n" + 
	                          "\r\n" + 
	                          "            </td>\r\n" + 
	                          "            <td><b>$"+order.getOrderPrice()+"</b><br /></td>\r\n" + 
	                          "\r\n" + 
	                          "        </tr>\r\n" + 
	                          "\r\n" + 
	                          "\r\n" + 
	                          "    </table>\r\n" + 
	                          "\r\n" + 
	                          "            \r\n" + 
	                          "        </div>\r\n" + 
	                          "\r\n" + 
	                          "    <div style=\"width: 45%; float: left;margin-top:-15px; \">\r\n" + 
	                          "\r\n";
	                         if(order.getPaymentMethod()!=null) {
	                          myvar += "            <table\r\n" + 
	                          "                style=\"width: 150%; float: left; margin-left: 4%; height:500px; border: 1px solid black;\">\r\n" + 
	                          "                <tr>\r\n"+
	                          "                    <td><b>PAYMENT STATUS</b><br /> "+order.getPaymentMethod()+"</td>\r\n" + 
	                          "                </tr>\r\n" + 
	                          "            </table>\r\n";
	                         }
	                         myvar +=  "            \r\n" + 
	                          "            <br/>       <br/><br/>              \r\n" + 
	                          "            \r\n" + 
	                          "            \r\n" ; 
	                          if(!"Pickup".equalsIgnoreCase(order.getOrderType())){
	                        	  myvar +="<table\r\n" + 
		                          "                style=\"width: 150%; float: left; margin-left: 4%; border: 1px solid black;\">\r\n" + 
		                          "                <tr>\r\n" ;
	                        	  myvar +="                    <td><b>Customer Information:</b><br /> ";
	                       int count = 0;
	                       for(Address address : order.getCustomer().getAddresses()){
	                    	 if(count == 0){
	                         if(address.getAddress1()!=null) {
	                             myvar += address.getAddress1()+"<br />\r\n" ;
	                         }
	                         if (address.getAddress2() != null) {
	     						myvar += address.getAddress2() + "<br />\r\n";
	     					}
	                         if(address.getZip() != null) {
	                        	 myvar +="                        "+address.getZip();
	                         }
	                         if(address.getCity()!=null) {
	                        	 myvar +=", "+address.getCity();
	                         }
	                         if(address.getState() != null) {
	                             myvar += ","+address.getState();
	                         }
	                         if(order.getCustomer().getPhoneNumber()!=null){
	                        	 myvar +=",<br />" + 
	                          "                       <br /> "+order.getCustomer().getPhoneNumber();
	                         }
	                         	myvar += "</td>\r\n </tr>\r\n </table>\r\n" ;
	                         }
	                    	 count ++;
	                       }
	                        }
	                         myvar += "" + 
	                          "        </div>\r\n" + 
	                          "                <div style=\"clear: both;\"></div>\r\n" + 
	                          "<br/>\r\n" + 
	                          "\r\n" + 
	                          "<div style=\"text-align: center;\">\r\n" + 
	                          "<br/>\r\n" + 
	                          "<br/>\r\n" + 
	                          "<br/>\r\n" + 
	                          "<br/>\r\n" + 
	                          ".......................................................................End of order.........................................................<br/><br/>\r\n" + 
	                          "\r\n" + 
	                          "\r\n" + 
	                          "    <div style=\"clear:both; padding-bottom:10px;\">\r\n" + 
	                          "        <div style=\"text-align:center; margin-left: 70%; margin-top:10px;\">                                       \r\n" + 
	                          "        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;www.foodkonnekt.com</div>\r\n" + 
	                          "    </div>\r\n" + 
	                          "\r\n" + 
	                          "\r\n" + 
	                          "</div>\r\n" + 
	                          "\r\n" + 
	                          "</body>\r\n" + 
	                          "</html>\r\n" + 
	                          "";
	                         LOGGER.info("===============  FaxUtility : Inside htmltoPdf :: End  ============= returns ");

	         return myvar;
	    
	     }
	    
	    public static void sendFaxFailedmail(VendorRepository vendorRepository,OnlineNotificationStatusRepository onlineNotificationStatusRepository,final OrderR orderR ,final Environment environment)
        {
            LOGGER.info("inside  resEntity----" +FAX_SEND_COUNT);
            LOGGER.info("inside  resEntity----" +FAX_SEND_COUNT);
            final Vendor vendor = vendorRepository.findOne(orderR.getMerchant().getOwner().getId());
            final  List<OnlineOrderNotificationStatus>  onlineNotificationStatuslist = onlineNotificationStatusRepository.findByOnlineNotificationStatusListByorderId(orderR.getId());
            LOGGER.info(" vendor :" + vendor.getId());
            LOGGER.info(" onlineNotificationStatuslist :" + onlineNotificationStatuslist.size());
            new Thread(){
                public void run(){
                        MailSendUtil.onlineOrderFailedNotification(vendor, onlineNotificationStatuslist,orderR,environment );                                
                        MailSendUtil.sendMailSystemLog("Foodkonnekt", environment.getProperty("FOODKONNEKT_APP_TYPE"),"FCM Notification Error for MerchantId "+orderR.getMerchant().getId()+" and OrderId "+orderR.getId(),"fax failed",environment);
                }
            }.start();
            
		
        }
	  
}
