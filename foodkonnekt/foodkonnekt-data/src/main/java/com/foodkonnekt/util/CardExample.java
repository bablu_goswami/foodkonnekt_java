package com.foodkonnekt.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import net.interfax.rest.client.InterFAX;
import net.interfax.rest.client.domain.APIResponse;
import net.interfax.rest.client.domain.OutboundFaxStructure;
import net.interfax.rest.client.exception.UnsuccessfulStatusCodeException;
import net.interfax.rest.client.impl.DefaultInterFAXClient;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.json.JSONArray;
import org.json.JSONObject;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.springframework.util.StringUtils;

import com.csvreader.CsvWriter;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.foodkonnekt.clover.vo.MerchantVo;
import com.foodkonnekt.model.Clover;
import com.foodkonnekt.model.Koupons;
import com.foodkonnekt.model.Merchant;
import com.foodkonnekt.model.OnlineOrderNotificationStatus;
import com.foodkonnekt.model.PaymentMode;
import com.foodkonnekt.model.Vendor;
import com.google.gson.Gson;

class TestCronJOb implements Job{

	public void execute(JobExecutionContext context) throws JobExecutionException {
		// TODO Auto-generated method stub
		System.out.println("job");
	}
	
}
public class CardExample {
	private static final Logger LOGGER = LoggerFactory.getLogger(CardExample.class);

	
	
	static void sendMail(Environment environment){
		try {
			Message message = new MimeMessage(
					SendMailProperty.gmailMailProperty(IConstant.FROM_EMAIL_ID, IConstant.FROM_PASSWORD));
			message.setFrom(new InternetAddress(IConstant.FROM_EMAIL_ID));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse("seshu@mkonnekt.com"));
			message.setSubject("Regards:Forgot password");
			/*String msg = "Dear " + customer.getFirstName() + "<br>";
			msg += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;We have received a request to reset your password .Please find your reset password link below::<br>";
			msg += " <br>";
			msg += " <br>";
			msg += "Click to : " + environment.getProperty("WEB_BASE_URL") + "/changepassword?email=" + customer.getEmailId()
					+ "&merchantId=" + marchantId + "&userId=" + cusID + "&tLog="
					+ EncryptionDecryptionUtil.encryption(timeLog) + "<br>";
			msg += " <br>";
			msg += " <br>";
			msg += " <br>";
			msg += "<b>Regards,</b><br>";
			msg += "FoodKonnekt";*/
			
			String merchantLogo="";
			/*if (customer.getMerchantt().getMerchantLogo() == null) {*/
                merchantLogo = "http://www.dev.foodkonnekt.com/app/foodnew.jpg";
            /*} else {
                merchantLogo = UrlConstant.BASE_PORT + customer.getMerchantt().getMerchantLogo();
            }*/
			
			String msg = "<div style='width: 700px; height: auto; border: solid 1px #ddd; box-shadow: 0px 0px 6px #ddd; background-color: #f2f2f2; text-align: center; margin: 40px 0px 0px 35px;'>"
					+ "<div style='width: 700px; height: 85px; padding: 20px 0px; text-align: center;'>&nbsp;<img src='"
					+ merchantLogo + "' height='80' /></div>"
					+ "<div style='background-color: #fff; padding: 20px 0px 0px 0px; width: 90%; margin: 0 auto 65px auto;'>"
					+ "<p style='font-size: 20px; color: #e6880f; padding-left: 10px' align='left'>Dear Seshu,</p>"
					+ "<p style='font-size: 14px;text-align: left;padding-left:10px;'>We received a password reset request - click on the below link to reset your password.&nbsp;</p>";
			msg = msg+"<br /><br /> <a align='left' style='padding-left:10px ;text-decoration: none; margin: 16px 0px; display: inline-block; background-color: #f8981d; color: #fff; padding: 10px 12px; border-radius: 10px; font-weight: 600; font-size: 15px; ' href='" + environment.getProperty("BASE_URL") + "/changeAdminpassword?email=sumit@mkonnekt.com&merchantId=1 &userId=2&tLog=222222' target='_blank'>Click here</a>"
					+ "<p style='text-align: left;padding-left:10px;'><br /> Note: Link is valid for 30 min only.</p>";

			
					msg = msg +" <p style='text-align: left;padding-left:10px;'><br><br>Regards,<br>Foodkonnekt Team<br><br></p>";
			msg = msg + "</div>";
			msg = msg + "<p>We appreciate your business with us!</p>"
					+ "<div style='height: 70px; text-align: center;'>&nbsp;<img src='http://www.dev.foodkonnekt.com/app/foodnew.jpg' alt='' width='200' height='63' /></div>"
					+ "</div>"
					+ "<p style='color: #676767; font-size: 11px;'>Automated email. Please do not reply to this sender email.</p>";
			
			
			message.setContent(msg, "text/html");
			Transport.send(message);
			System.out.println("sent");
		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
	}
	private static final String DATE_FORMAT = "dd-M-yyyy hh:mm:ss a";
//	public static void main(String[] args) throws IOException, URISyntaxException, UnsuccessfulStatusCodeException {}
	public static void main(String[] args) {

		String orderid="ZmdjAwt=";
		  Integer orderId = 0;
		if (orderid != null) {
        	LOGGER.info(" Start :OrderController :orderRec :orderid ------------------------ "+orderid);
            if (orderid.matches("[0-9]+")) {
            } else {
                if (EncryptionDecryptionUtil.decryption(orderid).matches("[0-9]+"))
                    orderId = Integer.parseInt(EncryptionDecryptionUtil.decryption(orderid));
            }
        }
		
		System.out.println("Out : "+EncryptionDecryptionUtil.decryption("1234"));
		String[] ids = TimeZone.getAvailableIDs();
		for (String id : ids) {
			System.out.println(displayTimeZone(TimeZone.getTimeZone(id)));
		}

		System.out.println("\nTotal TimeZone ID " + ids.length);

	}

	private static String displayTimeZone(TimeZone tz) {

		long hours = TimeUnit.MILLISECONDS.toHours(tz.getRawOffset());
		long minutes = TimeUnit.MILLISECONDS.toMinutes(tz.getRawOffset())
                                  - TimeUnit.HOURS.toMinutes(hours);
		// avoid -4:-30 issue
		minutes = Math.abs(minutes);

		String result = "";
		if (hours > 0) {
			result = String.format("(GMT+%d:%02d) %s", hours, minutes, tz.getID());
		} else {
			result = String.format("(GMT%d:%02d) %s", hours, minutes, tz.getID());
		}

		return result;

	}
	
   /* public static void main(String[] args) throws SchedulerException {
    	
    
    	
    	
    	Calendar cal = Calendar.getInstance();
    	cal.setTime(new Date());

    	// substract 7 days
    	// If we give 7 there it will give 8 days back
    	cal.set(Calendar.DAY_OF_MONTH, cal.get(Calendar.DAY_OF_MONTH)-2);

    	// convert to date
    	Date pastDate = cal.getTime();
    	
    	System.out.println(pastDate.before(new Date()));
    
        Clover clover= new Clover();
        String merchantId="HFX9J65TD529T";//"9GK2J085R8A3A";//
        String accessToken="118ea91d-ccb4-e7ea-8e9f-287f966141bf";//"2b5d6fe3-eec3-0f35-2e6d-4525d9fecb76";//
        clover.setMerchantId(merchantId);
        clover.setAuthToken(accessToken);
        clover.setInstantUrl(IConstant.CLOVER_INSTANCE_URL);
        clover.setUrl(IConstant.CLOVER_URL);
        int count=0;
        boolean isData=false;
        do{
        	int offset=1000*count++;
        String customers=CloverUrlUtil.getAllCloverCustomers(clover,offset);
       
        List<Map<String, Object>> customerList= getCustomerData(customers, pastDate,clover);
        List<Map<String, Object>> customerListCheck = new ArrayList<Map<String,Object>>(); 
       // customerListCheck.addAll(customerList);
        System.out.println();
        if(customerList!=null && !customerList.isEmpty() && customerList.size()>0){
        	
        	

			for (Map<String, Object> customerLists : customerList) {
				if(customerLists.containsKey("firstName")  && customerLists.containsKey("lastName")){
				
				Integer count1 = 1;
				if(customerListCheck.isEmpty() || customerListCheck.size()>0 ){
					customerLists.put("duplicateCount", 1);
					customerListCheck.add(customerLists);
				}else{
					boolean isDuplicate=false;
				for (Map<String, Object> customerListChecks : customerListCheck) {
					if(customerListChecks.containsKey("firstName")  && customerListChecks.containsKey("lastName")){
					
					if (customerListChecks.get("firstName").equals(customerLists.get("firstName")) && customerListChecks.get("lastName").equals(customerLists.get("lastName"))) {
						Integer duplicateCount=(Integer)customerLists.get("duplicateCount");
						duplicateCount=duplicateCount+1;
						customerListChecks.put("duplicateCount",duplicateCount);
						isDuplicate=true;
						break;
					}
					
				}
				}
				if(!isDuplicate){
					customerLists.put("duplicateCount", 1);
					customerListCheck.add(customerLists);
				}
				}
				
			}
				
			}

        writeCSVFile(customerList,"E:/Tacos_CustomerData_3.csv");
        writeCSVFile(customerListCheck,"E:/Tacos_CustomerData_3_duplicate.csv");
        System.out.println(customerList.size());
        System.out.println("done");
        isData=true;
        }else{
        	isData=false;
        }
        }while(isData);
    }*/
    static void writeCSVFile(List<Map<String, Object>> customerData,String outputFile){

		
		// before we open the file check to see if it already exists
		boolean alreadyExists = new File(outputFile).exists();
			
		try {
			// use FileWriter constructor that specifies open for appending
			CsvWriter csvOutput = new CsvWriter(new FileWriter(outputFile, true), ',');

			
			// if the file didn't already exist then we need to write out the header line
			if (!alreadyExists)
			{
				csvOutput.write("email");
				csvOutput.write("phone");
				csvOutput.write("fn");
				csvOutput.write("ln");
				csvOutput.write("customerSince");
				csvOutput.write("event_name");
				csvOutput.write("No Of Orders");
				csvOutput.write("currency");
				csvOutput.write("orderAmount");
				csvOutput.write("createdOn");
				csvOutput.write("orderIds");
				csvOutput.write("duplicateCount");
				
				
				
				csvOutput.endRecord();
			}
			// else assume that the file already has the correct header line
			
			// write out a few records
			for(Map<String, Object> map:customerData){
				if (map.containsKey("emailIds"))
					csvOutput.write((String) map.get("emailIds"));
				else
					csvOutput.write(" ");
				
				if (map.containsKey("phoneNumbers"))
					csvOutput.write((String) map.get("phoneNumbers"));
				else
					csvOutput.write(" ");
				
				if (map.containsKey("firstName"))
					csvOutput.write((String) map.get("firstName"));
				else
					csvOutput.write(" ");
				
				if (map.containsKey("lastName"))
					csvOutput.write((String) map.get("lastName"));
				else
					csvOutput.write(" ");
				if (map.containsKey("customerSince"))
					csvOutput.write((String) map.get("customerSince"));
				else
					csvOutput.write(" ");
				
				
				csvOutput.write("Purchase");
				
				if (map.containsKey("orderCount"))
					csvOutput.write(((Integer) map.get("orderCount")).toString());
				else
					csvOutput.write(" ");
				
				
				csvOutput.write("USD");
				
				if (map.containsKey("orderAmount"))
					csvOutput.write(((String) map.get("orderAmount")).toString());
				else
					csvOutput.write(" ");
				
				if (map.containsKey("createdOn"))
					csvOutput.write(((String) map.get("createdOn")).toString());
				else
					csvOutput.write(" ");
				
				if (map.containsKey("orderIds"))
					csvOutput.write(((String) map.get("orderIds")).toString());
				else
					csvOutput.write(" ");
				
				if (map.containsKey("duplicateCount"))
					csvOutput.write(((Integer) map.get("duplicateCount")).toString());
				else
					csvOutput.write(" ");
				
				
				/*if (map.containsKey("orderData"))
					csvOutput.write(((List) map.get("orderData")).toString());
				else
					csvOutput.write(" ");
				*/
            	
            	csvOutput.endRecord();
			}
			
			csvOutput.close();
		} catch (IOException e) {
			LOGGER.error("error: " + e.getMessage());
		}
    }
    static List<Map<String, Object>>getCustomerData(String customerJson,Date pastDate,Clover clover ){
    	LOGGER.info("===== CardExample : inside getCustomerData : starts ======= Customer : "+customerJson);
    	List<Map<String, Object>> customerList= new ArrayList<Map<String,Object>>();
    	JSONObject jObject = new JSONObject(customerJson);
        if(jObject.has("elements")){
        	
        JSONArray jSONArray = jObject.getJSONArray("elements");
        for (Object jObj : jSONArray) {
        	int orderCount=0;
        	Map<String, Object> customerMap=new HashMap<String, Object>();
            JSONObject customerJsonObj = (JSONObject) jObj;
            if(customerJsonObj.has("customerSince")){
            	
            	if(pastDate.before(new Date(customerJsonObj.getLong("customerSince")))){
            		customerMap.put("customerSince", DateUtil.getMMDDYYYY(new Date(customerJsonObj.getLong("customerSince"))));
            	}else{
            		continue;
            	}
            	
            }
            if(customerJsonObj.has("firstName")){
            	customerMap.put("firstName", customerJsonObj.getString("firstName"));
            }
            if(customerJsonObj.has("lastName")){
            	if(!StringUtils.isEmpty(customerJsonObj.getString("lastName")))
            	customerMap.put("lastName", customerJsonObj.getString("lastName"));
            }
            
           if(customerJsonObj.has("orders")){
        	   JSONObject orderJson = customerJsonObj.getJSONObject("orders");
        	   JSONArray orderJSONArray = orderJson.getJSONArray("elements");
        	   
        	   String orderIds="";
        	   String orderAmount="";
        	   String createdOn="";
        	   List<Map<String, Object>> orderData= new ArrayList<Map<String,Object>>();
               for (Object orderObj : orderJSONArray) {
            	   Map<String, Object> orderMap=new HashMap<String, Object>();
            	   JSONObject order =(JSONObject)orderObj;
            	  
            	   if(order.has("id")){
            		   orderMap.put("id", order.getString("id"));
            		   String orderDetailJson=CloverUrlUtil.getCloverOrder(clover, order.getString("id"));
            		   JSONObject orderDetailObject = new JSONObject(orderDetailJson);
            		   
            		   if(orderDetailObject.has("total"))
            			   orderAmount=orderDetailObject.getLong("total")+" , "+orderAmount;
            		  
            		   //orderMap.put("totalAmount", orderDetailObject.getLong("total"));
            		   
            		   /*JSONObject itemJson = orderDetailObject.getJSONObject("lineItems");
                	   JSONArray itemJSONArray = itemJson.getJSONArray("elements");
                	   String itemNames="";
                	   for (Object itemObj : itemJSONArray) {
                    	   
                    	   JSONObject itemJsonObject =(JSONObject)itemObj;
                    	   
                    	   if(itemJsonObject.has("name")){
                    		   itemNames=itemJsonObject.getString("name")+","+itemNames;
                    	   }
                    	   
                	   }*/
                	   JSONObject paymentsJson = orderDetailObject.getJSONObject("payments");
                	   JSONArray paymentsJSONArray = paymentsJson.getJSONArray("elements");
                	   
                	   for (Object paymentObj : paymentsJSONArray) {
                    	   
                    	   JSONObject paymentJsonObject =(JSONObject)paymentObj;
                    	   
                    	   if(paymentJsonObject.has("id")){
                    		   orderMap.put("PaymentId",paymentJsonObject.getString("id"));
                    	   }
                    	   if(paymentJsonObject.has("label")){
                    		   orderMap.put("label",paymentJsonObject.getString("label"));
                    	   }
                    	   
                    	   
                	   }
                	   
            		   
                	   //orderMap.put("createdTime",DateUtil.getMMDDYYYY(new Date(orderDetailObject.getLong("createdTime"))));
            		   if(orderDetailObject.has("createdTime"))
                	   createdOn=DateUtil.getMMDDYYYY(new Date(orderDetailObject.getLong("createdTime")))+" , "+createdOn;
                	   orderIds=order.getString("id")+" , "+orderIds;
            		   orderCount++;
            	   }
            	   orderData.add(orderMap);
               }
                customerMap.put("orderAmount",orderAmount );
            	customerMap.put("orderCount",orderCount );
            	customerMap.put("createdOn",createdOn );
            	customerMap.put("orderIds",orderIds );
            	
            }
           
           if(customerJsonObj.has("emailAddresses")){
        	   JSONObject emailJson = customerJsonObj.getJSONObject("emailAddresses");
        	   JSONArray emailJSONArray = emailJson.getJSONArray("elements");
        	  // int orderCount=0;
        	   String emailIds="";
               for (Object emailObj : emailJSONArray) {
            	   JSONObject emailJsonObject =(JSONObject)emailObj;
            	  
            	   if(emailJsonObject.has("id")&& emailJsonObject.has("emailAddress")){
            		   emailIds=emailJsonObject.getString("emailAddress")+" , "+emailIds;
            		  
            	   }
               }
            	
            	customerMap.put("emailIds",emailIds );
            }
           
           if(customerJsonObj.has("phoneNumbers")){
        	   JSONObject phoneJson = customerJsonObj.getJSONObject("phoneNumbers");
        	   JSONArray phoneJSONArray = phoneJson.getJSONArray("elements");
        	  // int orderCount=0;
        	   String phoneNumbers="";
               for (Object phoneObj : phoneJSONArray) {
            	   JSONObject phoneJsonObject =(JSONObject)phoneObj;
            	  
            	   if(phoneJsonObject.has("id")&& phoneJsonObject.has("phoneNumber")){
            		   phoneNumbers=phoneJsonObject.getString("phoneNumber")+" , "+phoneNumbers;
            		  
            	   }
               }
            	
            	customerMap.put("phoneNumbers",phoneNumbers );
            }
            
            
            //if(orderCount>1)
            customerList.add(customerMap);
        }
        }
    	LOGGER.info("===== CardExample : inside getCustomerData : End =======");

    	return customerList;
    }
    static List<Map<String, Object>>getOrderData(String orders){
    	LOGGER.info("===== CardExample : inside getOrderData : starts =======orders : "+orders);

    	JSONObject jObject = new JSONObject(orders);
        if(jObject.has("elements")){
        JSONArray jSONArray = jObject.getJSONArray("elements");
        List<PaymentMode> paymentModeList = new ArrayList<PaymentMode>();
        for (Object jObj : jSONArray) {
            JSONObject paymentJson = (JSONObject) jObj;
        
        }
        }
    	LOGGER.info("===== CardExample : inside getOrderData : End =======");

    	return null;
    }
    public static String convertToStringJson(HttpPost postRequest, String customerJson) {
        StringBuilder responseBuilder = new StringBuilder();
        try {
            HttpClient httpClient = HttpClientBuilder.create().build();
            StringEntity input = new StringEntity(customerJson);
            input.setContentType("application/json");
            postRequest.setEntity(input);
            HttpResponse response = httpClient.execute(postRequest);
            System.out.println("Output from Server .... \n");
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            String line = "";
            while ((line = rd.readLine()) != null) {
                responseBuilder.append(line);
            }
            System.out.println(responseBuilder.toString());
        } catch (MalformedURLException e) {
            LOGGER.error("error: " + e.getMessage());
        } catch (IOException e) {
            LOGGER.error("error: " + e.getMessage());
        }
        return responseBuilder.toString();
    }
}
