package com.foodkonnekt.util;

import java.io.IOException;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;

import com.foodkonnekt.model.Address;
import com.foodkonnekt.model.OrderItem;
import com.foodkonnekt.model.OrderItemModifier;
import com.foodkonnekt.model.OrderPizza;
import com.foodkonnekt.model.OrderPizzaCrust;
import com.foodkonnekt.model.OrderPizzaToppings;
import com.foodkonnekt.model.OrderR;
import com.foodkonnekt.repository.AddressRepository;
import com.foodkonnekt.repository.OrderItemModifierRepository;
import com.foodkonnekt.repository.OrderItemRepository;
import com.foodkonnekt.repository.OrderPizzaCrustRepository;
import com.foodkonnekt.repository.OrderPizzaRepository;
import com.foodkonnekt.repository.OrderPizzaToppingsRepository;
import com.itextpdf.kernel.color.Color;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.geom.Rectangle;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.border.Border;
import com.itextpdf.layout.border.SolidBorder;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.element.Text;
import com.itextpdf.layout.property.TextAlignment;
import com.itextpdf.layout.property.UnitValue;

public class ItextFaxUtil 
{
	private static final Logger LOGGER= LoggerFactory.getLogger(ItextFaxUtil.class);

	//public static final String REGULAR = environment.getProperty("REGULAR");
    //public static final String BOLD = environment.getProperty("BOLD");
    
    public static final String NEWLINE = "\n";
    public  static   String sendPdfFax(OrderR order, Table  table, Address address,Environment environment)

    { 
    	LOGGER.info("===============  ItextFaxUtil : Inside sendPdfFax :: Start  ============= ");

    	   DecimalFormat df = new DecimalFormat("#,###,##0.00");
    	      SimpleDateFormat format = new SimpleDateFormat("MM-dd-yyyy hh:mm a");
    	      String fulfilledOn = null;
    	     
    	      if(order.getFulfilled_on() != null){
    	    	  System.out.println("order.getFulfilled_on()"+order.getFulfilled_on());
    	    	  fulfilledOn = format.format(order.getFulfilled_on());
    	      }

        try {
//        	String url = request.getRequestURL().toString();
//        	String baseURL = url.substring(0, url.length() - request.getRequestURI().length()) + request.getContextPath();
//        	String path = baseURL + "/resources/fonts/foobar.ttf";
//        	FontFactory.register(path);
        	
        	
	    String REGULAR = environment.getProperty("REGULAR");
		String BOLD = environment.getProperty("BOLD");
        PdfFont bold = PdfFontFactory.createFont(BOLD, true);
        String dest =  environment.getProperty("FAX_FILE_PATH")+order.getId()+".pdf";
        PdfWriter writer = new PdfWriter(dest);
        PdfDocument pdf = new PdfDocument(writer);
        Document doc = new Document(pdf);
        doc.setFont(PdfFontFactory.createFont(REGULAR, true)).setFontSize(12);
        doc.add( new Paragraph() .setTextAlignment(TextAlignment.RIGHT) .setMultipliedLeading(1) .add(new Text(String.format("%s %s\n","Order Id ",order.getId())).setFont(bold).setFontSize(14))
                        .add( " "));

        String locationAdress ="";
        String lastname ="";
        if(address!=null) {
            locationAdress +=address.getAddress1()+" ";
        
        if(address.getAddress2()!=null) {
            locationAdress +=address.getAddress2()+","+NEWLINE;
        }
        else {
            locationAdress +=","+NEWLINE;
        }
        if(address.getCity()!=null) {
            locationAdress +=address.getCity()+" ";
        }
        if(address.getState()!=null) {
            locationAdress +=address.getState()+" ";
        }
        if(address.getZip()!=null) {
            locationAdress +=address.getZip()+" "+NEWLINE+NEWLINE;
        }
        }
		LOGGER.info("===== ItextFaxUtil : Inside sendPdfFax :: locationAdress  == " + locationAdress);

        if(order.getCustomer().getLastName()!=null) {
        	lastname =order.getCustomer().getLastName();
        }
        doc.add( new Paragraph() .setTextAlignment(TextAlignment.LEFT) .setMultipliedLeading(1) .add(new Text(String.format("%s %s\n","",order.getMerchant().getName())).setFont(bold).setFontSize(14)).setMarginTop(-25).add(locationAdress));
        Table table1 = new Table(new UnitValue[] { new UnitValue(2, 50), new UnitValue(2, 50) }).setWidth(UnitValue.createPercentValue(100));
      
        String futureOrder="";
        if(order.getIsFutureOrder() != null && order.getIsFutureOrder().equals(1)) {
            futureOrder ="Future Order" +" "+fulfilledOn;
            
        }
        LOGGER.info("===== ItextFaxUtil : Inside sendPdfFax :: futureOrder  == " + futureOrder);

        table1.addCell(getOrderInfo("Order Type:  ",toUpperCovert(order.getOrderType())  , "Payment Status : ", toUpperCovert(order.getPaymentMethod()) , futureOrder, "",     " ", bold));
      
        
        
        
  
			if (!"Pickup".equalsIgnoreCase(order.getOrderType())) {
				if (order.getCustomer() != null && order.getAddress() != null) {
					table1.addCell(getCustomerInfoDelivery("Customer Information  \n " + "Name : "  ,  toUpperCovert(order.getCustomer().getFirstName()+" "+lastname), "Phone No:", order.getCustomer().getPhoneNumber(),  "Delivery Address : " , order.getAddress().getAddress1()+(order.getAddress().getAddress2()!=null?(","+order.getAddress().getAddress2()):"")+","+order.getAddress().getCity()+","+order.getAddress().getState()+","+order.getAddress().getZip(),  order.getOrderType()+"Time: ",fulfilledOn, bold).setMarginLeft(70));
				} 
			} else if(order.getOrderType()!=null){
           	   table1.addCell(getCustomerInfopPicKup("Customer Information  \n " + "Name : "  ,  toUpperCovert(order.getCustomer().getFirstName()+" "+lastname), "Phone No:", order.getCustomer().getPhoneNumber(),  order.getOrderType()+"Time: ",fulfilledOn,    "", bold).setMarginLeft(70));
           	}
           
           doc.add(table1);
           
        /*if(!"Pickup".equalsIgnoreCase(order.getOrderType()))
        {
        	
        if(order.getCustomer()!=null &&	order.getCustomer().getAddress1()==null)
        {
        	order.getCustomer().setAddress1("-");
        }
        	table1.addCell(getCustomerInfoDelivery("Customer Information  \n " + "Name : "  ,  toUpperCovert(order.getCustomer().getFirstName()), "Phone No:", order.getCustomer().getPhoneNumber(),  "Delivery Address : " , order.getCustomer().getAddress1(),  order.getOrderType()+"Time: ",fulfilledOn, bold).setMarginLeft(13));
        }
        
        else 
        {
        	table1.addCell(getCustomerInfopPicKup("Customer Information  \n " + "Name : "  ,  toUpperCovert(order.getCustomer().getFirstName()), "Phone No:", order.getCustomer().getPhoneNumber(),  "Time: ",fulfilledOn,    "", bold).setMarginLeft(13));
        }*/
           
           
        
        table.addCell(getPartyAddress2("", "", "", "", "", "", "", bold));
        table.addCell(getPartyAddress2("", "", "", "", "", "", "", bold));
        table.addCell(createCell("Subtotal \t", bold).setTextAlignment(TextAlignment.RIGHT));
        table.addCell(createCell(String.valueOf("$" + df.format(Double.parseDouble(order.getSubTotal())))).setTextAlignment(TextAlignment.CENTER));


        table.addCell(getPartyAddress2("", "", "", "", "", "", "", bold));
        table.addCell(getPartyAddress2("", "", "", "", "", "", "", bold));
        table.addCell(createCell("Online Fee", bold).setTextAlignment(TextAlignment.RIGHT));
        if (order.getConvenienceFee() != null && !order.getConvenienceFee().isEmpty() && !order.getConvenienceFee().equals(" ") && Double.parseDouble(order.getConvenienceFee())>0) {
        	table.addCell(createCell(String.valueOf("$" +df.format(Double.parseDouble(order.getConvenienceFee())))).setTextAlignment(TextAlignment.CENTER));
        }
       else{
            table.addCell(createCell(String.valueOf("-")).setTextAlignment(TextAlignment.CENTER));
        }
        
        
        if(!"Pickup".equalsIgnoreCase(order.getOrderType()) && Double.parseDouble(order.getDeliveryFee()) > 0) {
            table.addCell(getPartyAddress2("", "", "", "", "", "", "", bold));
            table.addCell(getPartyAddress2("", "", "", "", "", "", "", bold));
            table.addCell(createCell(String.valueOf("Delivery Fee \t"), bold).setTextAlignment(TextAlignment.RIGHT));
   	        LOGGER.info(df.format(Double.parseDouble(order.getDeliveryFee())));
            table.addCell(createCell(String.valueOf("$" +df.format(Double.parseDouble(order.getDeliveryFee())))).setTextAlignment(TextAlignment.CENTER));       
            } else {
            if(!"Pickup".equalsIgnoreCase(order.getOrderType())) {
                table.addCell(getPartyAddress2("", "", "", "", "", "", "", bold));
                table.addCell(getPartyAddress2("", "", "", "", "", "", "", bold));
                table.addCell(createCell(String.valueOf("Delivery Fee \t"), bold).setTextAlignment(TextAlignment.RIGHT));
                table.addCell(createCell(String.valueOf("$0.00" )).setTextAlignment(TextAlignment.CENTER));       
                }
        }

        
        
        
        
        table.addCell(getPartyAddress2("", "", "", "", "", "", "", bold));
        table.addCell(getPartyAddress2("", "", "", "", "", "", "", bold));
        table.addCell(createCell("Tax "+" ", bold).setTextAlignment(TextAlignment.RIGHT));
        if( order.getTax()!=null) {
            table.addCell(createCell(String.valueOf("$" + df.format(Double.parseDouble(order.getTax())))).setTextAlignment(TextAlignment.CENTER));
        }
        else {
            table.addCell(createCell(String.valueOf("-")).setTextAlignment(TextAlignment.CENTER));
        }
        

        table.addCell(getPartyAddress2("", "", "", "", "", "", "", bold));
        table.addCell(getPartyAddress2("", "", "", "", "", "", "", bold));
        table.addCell(createCell("Tip"+" ", bold).setTextAlignment(TextAlignment.RIGHT));
        
        if(order.getTipAmount()!=null && order.getTipAmount() > 0.0){
        	
        	
            table.addCell(createCell(String.valueOf("$" + df.format(order.getTipAmount()))).setTextAlignment(TextAlignment.CENTER));
        }else{
            table.addCell(createCell(String.valueOf("-")).setTextAlignment(TextAlignment.CENTER));
        }

      
        table.addCell(getPartyAddress2("", "", "", "", "", "", "", bold));
        table.addCell(getPartyAddress2("", "", "", "", "", "", "", bold));
        table.addCell(createCell(String.valueOf("Total"), bold).setTextAlignment(TextAlignment.RIGHT));
        table.addCell(createCell(String.valueOf("$" +   df.format(order.getOrderPrice())), bold)
                        .setTextAlignment(TextAlignment.CENTER));
        
        doc.add(table);

        
        Table table0 = new Table(new UnitValue[] { new UnitValue(2, 50), new UnitValue(2, 50) }).setWidth(UnitValue.createPercentValue(100));
        table0.addCell(getPartyAddress("  ", "", "", "  ", "", "",     " \n   ", bold));
        doc.add(table0);
        
        
//        String para = "Additional Tip : .................";  
//        Paragraph paragraph = new Paragraph(para).setMarginLeft(380);
//        doc.add(paragraph);
        
        
        String para3 = "Customer Signature : .................";  
        Paragraph paragraph3 = new Paragraph(para3).setMarginLeft(340);
        doc.add(paragraph3);
        
        doc.add( new Paragraph() .setTextAlignment(TextAlignment.LEFT) .setMultipliedLeading(1) .add(new Text(String.format("%s %s\n","Order Note : "," ")).setFont(bold).setFontSize(14)).setMarginTop(-60).setMarginRight(10));

      /*  if(order.getOrderNote()!=null) {
            doc.add( new Paragraph() .setTextAlignment(TextAlignment.LEFT) .setMultipliedLeading(1) .add(new Text(String.format("%s %s\n",order.getOrderNote(),"")).setFontSize(14)).setMarginTop(6).setMarginRight(10));
        }*/
        if(order.getOrderNote()!=null && !order.getOrderNote().equals(" ")) {
            Table table01 = new Table(new UnitValue[] { new UnitValue(2, 50), new UnitValue(2, 50) }).setWidth(UnitValue.createPercentValue(100));
            table01.addCell(getPartyAddress3(order.getOrderNote(), bold).setMarginRight(10));
            doc.add(table01);
        }
        
     
        String para4 = "";  
        Paragraph paragraph4 = new Paragraph(para4).setMarginLeft(340);
        doc.add(paragraph4);
        
        
        doc.add(getPaymentInfo());
        
        String para1 = " www.foodkonnekt.com";    
        Paragraph paragraph2 = new Paragraph(para1).setMarginLeft(180);
        doc.add(paragraph2);

        doc.close();
        LOGGER.info("Table created successfully..");
        //FaxUtility.sendFaxFile(merchantFaxNumber, dest);
        } 
        catch (Exception e) {
        	LOGGER.error("===============  ItextFaxUtil : Inside sendPdfFax :: Exception  ============= " + e);

            LOGGER.error("error: " + e.getMessage());
        }
        LOGGER.info("===============  ItextFaxUtil : Inside sendPdfFax :: End  ============= ");

        return null;
        
   
    }

    public static Cell getPartyAddress3(String who, PdfFont bold) {
    	LOGGER.info("===============  ItextFaxUtil : Inside getPartyAddress3 :: Start  =============who == "+who);

        Paragraph p = new Paragraph().setMultipliedLeading(1.0f).add(new Text(who)).add("\n");
         Cell cell = new Cell().add(p);
LOGGER.info("===============  ItextFaxUtil : Inside getPartyAddress3 :: End  ============= ");

         return cell;
     }
    
    public static Table getToatItemTable(PdfFont bold) {
    	LOGGER.info("===============  ItextFaxUtil : Inside getToatItemTable :: Start  ============= ");

        Table table = new Table(new UnitValue[]{
                new UnitValue(UnitValue.PERCENT, 6.25f),
                new UnitValue(UnitValue.PERCENT, 20.5f),
                new UnitValue(UnitValue.PERCENT, 20.5f),
                new UnitValue(UnitValue.PERCENT, 12.5f)})
                .setWidth(UnitValue.createPercentValue(100))
                .setMarginTop(10).setMarginBottom(10);
        table.addHeaderCell(createCell("Subtotal", bold).setTextAlignment(TextAlignment.CENTER));
        table.addHeaderCell(createCell("Delivery Fee", bold).setTextAlignment(TextAlignment.CENTER));
        table.addHeaderCell(createCell("Tax", bold).setTextAlignment(TextAlignment.CENTER));
        table.addHeaderCell(createCell("Total", bold).setTextAlignment(TextAlignment.CENTER));

            table.addCell(createCell("$5").setTextAlignment(TextAlignment.CENTER));
            table.addCell(createCell( String.valueOf("$20")).setTextAlignment(TextAlignment.CENTER));
            table.addCell(createCell(String.valueOf("$50")) .setTextAlignment(TextAlignment.CENTER));
            table.addCell(createCell(String.valueOf("$"+25))  .setTextAlignment(TextAlignment.CENTER));
         LOGGER.info("===============  ItextFaxUtil : Inside getToatItemTable :: End  ============= ");
   
        return table;
    }
    
    public static Cell createCell(String text, PdfFont font) {
        return new Cell().setPadding(0.8f)
            .add(new Paragraph(text) .setFont(font).setMultipliedLeading(1));
    }

    
    public static Cell createCell(String text) {
        return new Cell().setPadding(0.8f)
            .add(new Paragraph(text)
                .setMultipliedLeading(1));
    }
    
    
    public static Cell getPartyTax(String[] taxId, String[] taxSchema, PdfFont bold) {
    	LOGGER.info("===============  ItextFaxUtil : Inside getPartyTax :: Start  ============= ");

        Paragraph p = new Paragraph().setFontSize(10).setMultipliedLeading(1.0f)
                        .add(new Text("Tax ID(s):").setFont(bold));
        if (taxId.length == 0) {
            p.add("\nNot applicable");
        } else {
            int n = taxId.length;
            for (int i = 0; i < n; i++) {
                p.add("\n").add(String.format("%s: %s", taxSchema[i], taxId[i]));
            }
        }
        LOGGER.info("===============  ItextFaxUtil : Inside getPartyTax :: End  ============= ");

        return new Cell().setBorder(null).add(p);
    }

    //        	table1.addCell(getPartyAddress("Customer Information  \n " + "Name : "+order.getCustomer().getFirstName(), "Phone No : "+order.getCustomer().getPhoneNumber(), "Delivery Address : "+order.getCustomer().getAddress1() , order.getOrderType()+"  Time:  "+order.getFulfilled_on(), "", "",     "", bold));

    public static Cell getPartyAddress(String who, String name, String line1, String line2, String countryID, String postcode, String city, PdfFont bold) {
    	LOGGER.info("===============  ItextFaxUtil : Inside getPartyAddress :: Start  ============= ");

        Paragraph p = new Paragraph().setMultipliedLeading(1.0f).add(new Text(who).setFont(bold)).add("\n").add(name)
                        .add("\n").add(line1).add("\n").add(line2).add("\n")
                        .add(String.format("%s%s %s", countryID, postcode, city));
        Cell cell = new Cell().setBorder(null).add(p);
        LOGGER.info("===============  ItextFaxUtil : Inside getPartyAddress :: End  ============= ");

        return cell;
    }
    
    
    public static Cell getCustomerInfopPicKup(String who, String name, String line1, String line2, String countryID, String postcode, String city, PdfFont bold) {
    	LOGGER.info("===============  ItextFaxUtil : Inside getCustomerInfopPicKup :: Start  ============= ");

        Paragraph p = new Paragraph().setMultipliedLeading(1.0f).add(new Text(who)).add(new Text(name).setFont(bold))
                        .add("\n").add(line1).add("").add(new Text(line2).setFont(bold)).add("")
                        .add("\n").add(countryID).add("").add(new Text(postcode).setFont(bold)).add("\n")
                        .add(String.format("%s", city));
        Cell cell = new Cell().setBorder(null).add(p);
        LOGGER.info("===============  ItextFaxUtil : Inside getCustomerInfopPicKup :: End  ============= ");

        return cell;
    }
    
    public static Cell getCustomerInfoDelivery(String who, String name, String line1, String line2, String countryID, String postcode, String city,String address, PdfFont bold) {
       LOGGER.info("===============  ItextFaxUtil : Inside getCustomerInfoDelivery :: Start  ============= ");

    	Paragraph p = new Paragraph().setMultipliedLeading(1.0f).add(new Text(who)).add(new Text(name).setFont(bold))
                        .add("\n").add(line1).add("").add(new Text(line2).setFont(bold)).add("")
                        .add("\n").add(countryID).add("").add(new Text(postcode).setFont(bold))
                        .add("\n").add(city).add("").add(new Text(address).setFont(bold));
        Cell cell = new Cell().setBorder(null).add(p);
        LOGGER.info("===============  ItextFaxUtil : Inside getCustomerInfoDelivery :: End  ============= ");

        return cell;
    }
    

    public static Cell getOrderInfo(String who, String name, String line1, String line2, String countryID, String postcode, String city, PdfFont bold) {
       LOGGER.info("===============  ItextFaxUtil : Inside getOrderInfo :: Start  ============= ");

    	Paragraph p = new Paragraph().setMultipliedLeading(1.0f).add(new Text(who)).add("").add(new Text(name).setFont(bold))
                        .add("\n").add(new Text(line1)).add("").add( new Text(line2).setFont(bold)).add("\n")
                        .add( new Text(countryID).setFont(bold).setFontSize(15))
                        .add(String.format("%s%s", postcode, city));
        Cell cell = new Cell().setBorder(null).add(p);
        
        LOGGER.info("===============  ItextFaxUtil : Inside getOrderInfo :: End  ============= ");

        return cell;
    }
    
    public static Cell getPartyAddress2(String who, String name, String line1, String line2, String countryID, String postcode, String city, PdfFont bold) {
       LOGGER.info("===============  ItextFaxUtil : Inside getPartyAddress2 :: Start  =============who = "+who+" name "+name+" line1 "+line1+" line2 "+line2+" countryID "+countryID
    		   +" postcode "+postcode+" city "+city);

    	Paragraph p = new Paragraph().setMultipliedLeading(1.0f).add(new Text(who).setFont(bold)).add("\n").add(name)
                        .add("\n").add(line1).add("\n").add(line2).add("\n")
                        .add(String.format("%s-%s %s", countryID, postcode, city));
        Cell cell =  new Cell().setPadding(0.8f)
        .add(new Paragraph(who)
            .setMultipliedLeading(1)).setBorder(null);
        LOGGER.info("===============  ItextFaxUtil : Inside getPartyAddress2 :: End  ============= ");

        return cell;
    }
    
    public static String convertDate(Date d, String newFormat) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat(newFormat);
        return sdf.format(d);
    }
    
    
    public static Cell getPartyAddress1(String who, String name, String line1, String line2, String countryID, String postcode, String city, PdfFont bold) {
      LOGGER.info("===============  ItextFaxUtil : Inside getPartyAddress1 :: Start  =============who = "+who+" name "+name+" line1 "+line1+" line2 "+line2+" countryID "+countryID + 
      		" postcode "+postcode+" city "+city);

    	Paragraph p = new Paragraph().setMultipliedLeading(1.0f).add(new Text(who).setFont(bold)).add("\n").add(name)  .add("\n").add(line1).add("\n").add(line2).add("\n")
                        .add(String.format("%s-%s %s", countryID, postcode, city));
        Cell cell = new Cell().setBorder(null).add(p);
        cell.setBold();
        LOGGER.info("===============  ItextFaxUtil : Inside getPartyAddress1 :: End  ============= ");

        return cell;
    }
    
    public static Paragraph getPaymentInfo() {
    	LOGGER.info("===============  ItextFaxUtil : Inside getPaymentInfo :: Start  ============= ");

        String ref ="";
        Paragraph p = new Paragraph(String.format(".......................................................................End of order.........................................................\n%s",  ref));
       LOGGER.info("===============  ItextFaxUtil : Inside getPaymentInfo :: End  ============= ");

        return p;
    }
    
    public static String toUpperCovert(String value) {
    	LOGGER.info("===============  ItextFaxUtil : Inside toUpperCovert :: Start  =============value "+value);

    char[] array = value.toCharArray();
    array[0] = Character.toUpperCase(array[0]);
    String result = new String(array);
    LOGGER.info("result === "+result);
    LOGGER.info("===============  ItextFaxUtil : Inside toUpperCovert :: End  ============= ");

	return result;
    
    }
    
    public static void getOrderDetails(OrderR order, Integer merchantID,Address address,OrderItemRepository orderItemRepository,
    		OrderItemModifierRepository itemModifierRepository,OrderPizzaRepository orderPizzaRepository,OrderPizzaToppingsRepository orderPizzaToppingsRepository,
    		OrderPizzaCrustRepository orderPizzaCrustRepository,AddressRepository addressRepository,Environment environment) throws IOException {
    	LOGGER.info("===============  ItextFaxUtil : Inside getOrderDetails :: Start  =============merchantID "+merchantID);

    	 Date today = (order.getMerchant() != null && order.getMerchant().getTimeZone() != null
					&& order.getMerchant().getTimeZone().getTimeZoneCode() != null)
							? DateUtil.getCurrentDateForTimeZonee(order.getMerchant().getTimeZone().getTimeZoneCode())
							: new Date();
							
    	String REGULAR = environment.getProperty("REGULAR");
        String BOLD = environment.getProperty("BOLD");
    		System.out.println("inside ItextFaxUtil");
    	
	        PdfFont bold = PdfFontFactory.createFont(REGULAR);       
	        String dest = environment.getProperty("THERMAL_RECEIPT")+order.getId()+".pdf";
	        PdfWriter writer = new PdfWriter(dest);
	        PdfDocument pdf = new PdfDocument(writer);
	        Rectangle r = new Rectangle(0.60f, 0.80f);
	        PageSize ps= new PageSize(203.76f,567.36f);
	        DecimalFormat decFormat = new DecimalFormat("0.00");
	        
	        Document doc = new Document(pdf,ps);
	        
	        doc.setMargins(10f, 10f, 10f, 10f);
	        
	         DateFormat dateFormat = new SimpleDateFormat("MMMM dd, yyyy");
	         DateFormat timeFormat = new SimpleDateFormat("hh:mm:ss a");
	         //get current date time with Date()
	         Date date = today;
	         Date time = today;
	         String Date = dateFormat.format(date);
	         String Time = timeFormat.format(time);
	         
	         DateUtil dt = new DateUtil();
	         
	         Calendar c = Calendar.getInstance();
	         
	         LOGGER.info("getCurrentTimeForTimeZone");
	         LOGGER.info(DateUtil.getCurrentTimeForTimeZone(c.getTimeZone().toString()));
	         
	         LOGGER.info("findCurrentTime");
	         LOGGER.info(DateUtil.findCurrentTime());
	         
	         LOGGER.info("findCurrentDateWithTime");
	         LOGGER.info(DateUtil.findCurrentDateWithTime());
	         
	         LOGGER.info("Date Format");
	         LOGGER.info(Time);
	         
	         doc.setFont(PdfFontFactory.createFont(BOLD)).setFontSize(8);
	         
	         String locationAdress ="";
	         if(address!=null) {
	             locationAdress +=address.getAddress1()+" ";
	         }
	         if(address.getAddress2()!=null) {
	             locationAdress +=address.getAddress2()+","+NEWLINE;
	         }
	         else {
	             locationAdress +=","+NEWLINE;
	         }
	         if(address.getCity()!=null) {
	             locationAdress +=address.getCity()+" ";
	         }
	         if(address.getState()!=null) {
	             locationAdress +=address.getState()+" ";
	         }
	         if(address.getZip()!=null) {
	             locationAdress +=address.getZip();
	         }
	         
	            Table table1 = new Table(new UnitValue[] { new UnitValue(10,50) }).setWidth(UnitValue.createPercentValue(100));
		        
		        table1.addCell(getPartyAddress("","","","","","Receipt","","","", bold).setTextAlignment(TextAlignment.CENTER)).setBold();
		        Border b2=new SolidBorder(Color.WHITE,2f);

		        table1.addCell(createCell("").setTextAlignment(TextAlignment.LEFT).setBorder(b2)).setPaddingLeft(0.0f).setBold();
                table1.addCell(getPartyAddress6("","","","","","","",Date+" "+Time,bold).setTextAlignment(TextAlignment.CENTER).setBorder(b2).setMarginRight(0.0f));
		        
	            doc.add(table1);
		        doc.add(new Paragraph().setTextAlignment(TextAlignment.CENTER).setMultipliedLeading(1).add(new Text(String.format("%s \n %s \n %s\n\n",order.getMerchant().getName(),locationAdress,order.getMerchant().getPhoneNumber())).setFont(bold).setFontSize(8)).setMarginTop(-4).setBold());
		        
		        Border b1=new SolidBorder(Color.WHITE,2f);
		        
		        Table table = new Table(new UnitValue[]
		        {
		                		new UnitValue(UnitValue.PERCENT, 60.5f),
		                        new UnitValue(UnitValue.PERCENT, 15.5f)})
		                        .setWidth(UnitValue.createPercentValue(100))
		                        .setBorder(b1).setMarginLeft(0.0f).setMarginRight(0.0f);
		                
		                List<OrderItem> orderItems = orderItemRepository.findByOrderId(order.getId());

		                for (OrderItem orderItem : orderItems) 
		                {
		                    double modifiersPrice = 0;

		                	if(orderItem.getItem()!=null &&orderItem.getItem().getId()!=null) 
		                	{
		                    if(orderItem.getItem()!=null &&orderItem.getItem().getName()!=null)
		                    {
		                    	table.addCell(getPartyAddress9("",orderItem.getItem().getName()+" X "+orderItem.getQuantity().toString(),bold).setTextAlignment(TextAlignment.LEFT).setBorder(b1).setMarginRight(0.0f));
		                    	table.addCell(getPartyAddress9("$",decFormat.format(Float.parseFloat(String.valueOf((orderItem.getItem().getPrice() + modifiersPrice) * orderItem.getQuantity()))),bold).setTextAlignment(TextAlignment.RIGHT).setBorder(b1).setMarginRight(0.0f));
		                    }
		                  
		                    if ((orderItem.getId() != null)) 
		                    {
		                        List<OrderItemModifier> itemModifiers = itemModifierRepository.findByOrderItemId(orderItem.getId());
		                        String modifiers = "";
		                        
		                        if (!itemModifiers.isEmpty() && itemModifiers != null) 
		                        {
		                            for (OrderItemModifier orderItemModifier : itemModifiers) 
		                            {
		                               modifiers = orderItemModifier.getModifiers().getName()+"\n";
		                               modifiersPrice = Double.valueOf(orderItemModifier.getModifiers().getPrice().toString());
		                               table.addCell(getPartyAddress9("*",modifiers,bold).setTextAlignment(TextAlignment.LEFT).setBorder(b1).setMarginRight(0.0f));
			                           table.addCell(getPartyAddress9("$",decFormat.format(Float.parseFloat(String.valueOf((modifiersPrice) * orderItem.getQuantity()))),bold).setTextAlignment(TextAlignment.RIGHT).setBorder(b1).setMarginRight(0.0f));
		                            }
		                            modifiersPrice = 0;
		                        } 
		                    }
		                    LOGGER.info("orderItem.getItem().getPrice()"+orderItem.getItem().getPrice());
		                    LOGGER.info("orderItem.getQuantity()"+orderItem.getQuantity());
		                    modifiersPrice =0;
		                	}
		               }
		                List<OrderPizza> orderPizzas = orderPizzaRepository.findByOrderId(order.getId());
		                
		                if(!orderPizzas.isEmpty() && orderPizzas != null)
		                {
		                    for (OrderPizza orderPizza : orderPizzas) 
		                    {
		                        if (orderPizza != null && orderPizza.getPizzaTemplate() != null) 
		                        {
		                                    if(orderPizza!=null && orderPizza.getPizzaTemplate()!=null && orderPizza.getPizzaTemplate().getDescription()!= null &&  orderPizza.getPizzaSize() != null && orderPizza.getPizzaSize().getDescription()!=null)
		                                    {
		                                    	        table.addCell(getPartyAddress9("",orderPizza.getPizzaTemplate().getDescription()+"("+orderPizza.getPizzaSize().getDescription()+")"+" X "+orderPizza.getQuantity(),bold).setTextAlignment(TextAlignment.LEFT).setBorder(b1).setMarginRight(0.0f));
		                                    	        table.addCell(getPartyAddress9("$",decFormat.format(Float.parseFloat(String.valueOf((orderPizza.getPrice()) * orderPizza.getQuantity()))),bold).setTextAlignment(TextAlignment.RIGHT));
		                                    }
		                                    String modifiers = "";
		                                    double modifiersPrice = 0;
		                                    List<OrderPizzaToppings> orderPizzaToppings = orderPizzaToppingsRepository.findByOrderPizzaId(orderPizza.getId());
		                                    if(!orderPizzaToppings.isEmpty() && orderPizzaToppings!=null)
		                                    {
		                                        for (OrderPizzaToppings pizzaToppings : orderPizzaToppings)
		                                        {
		                                        	if(pizzaToppings!=null && pizzaToppings.getPizzaTopping()!=null && pizzaToppings.getPizzaTopping().getDescription()!=null)
		                                        	{
		                                                   	modifiers +=  pizzaToppings.getPizzaTopping().getDescription();
		                                                    if(pizzaToppings.getSide1()!=null && pizzaToppings.getSide1())
		                                                    {
		                                                        modifiers += "(First Half)";
		                                                        modifiersPrice += pizzaToppings.getPrice();
		                                                    }
		                                                    else if(pizzaToppings.getSide2()!=null && pizzaToppings.getSide2())
		                                                    {
		                                                    	modifiers += "(Second Half)";
		                                                        modifiersPrice += pizzaToppings.getPrice();
		                                                    }
		                                                    else
		                                                    {
		                                                    	modifiers += "(Full)";
		                                                        modifiersPrice += pizzaToppings.getPrice();
		                                                    }
		                                                   modifiers += "\n";
		                                        	}
		                                        	table.addCell(getPartyAddress9("*",modifiers,bold).setTextAlignment(TextAlignment.LEFT).setBorder(b1).setMarginRight(0.0f));
			                                    	table.addCell(getPartyAddress9("$",decFormat.format(Float.parseFloat(String.valueOf(modifiersPrice * orderPizza.getQuantity()))),bold).setTextAlignment(TextAlignment.RIGHT));
			                                    	modifiers = " ";
			                                    	modifiersPrice = 0.0;
		                                        }
		                                    }
		                                    
		                                    OrderPizzaCrust orderPizzaCrust = orderPizzaCrustRepository.findByOrderPizzaId(orderPizza.getId());
		                                    
		                                    if(orderPizzaCrust != null && orderPizzaCrust.getPizzacrust()!=null)
		                                    {
		                                    	int crustPrice = 0;
		                                        modifiers += orderPizzaCrust.getPizzacrust().getDescription();
		                                        
		                                        crustPrice += orderPizzaCrust.getPrice();
		                                        table.addCell(getPartyAddress9("*",modifiers,bold).setTextAlignment(TextAlignment.LEFT).setBorder(b1).setMarginRight(0.0f));
	                                    	    table.addCell(getPartyAddress9("$",decFormat.format(Float.parseFloat(String.valueOf(orderPizzaCrust.getPrice() * orderPizza.getQuantity()))),bold).setTextAlignment(TextAlignment.RIGHT));
			                                    modifiers="";
		                                    }
		                                    
		                        }
		                    }
		                }
 				        
		                table.addCell(createCell("____________________________").setTextAlignment(TextAlignment.LEFT).setBorder(b1)).setPaddingLeft(0.0f).setBold();
                		table.addCell(createCell("_________").setTextAlignment(TextAlignment.RIGHT).setBorder(b1).setMarginRight(0.0f));
		                
		                table.addCell(createCell("Subtotal :").setTextAlignment(TextAlignment.LEFT).setBorder(b1)).setPaddingLeft(0.0f).setBold();
		                table.addCell(getPartyAddress9("$",order.getSubTotal(),bold).setTextAlignment(TextAlignment.RIGHT).setBorder(b1).setMarginRight(0.0f));
		                
		                Float f = Float.parseFloat(order.getConvenienceFee());
		                LOGGER.info(decFormat.format(f));
		                table.addCell(createCell("Online Fee :").setTextAlignment(TextAlignment.LEFT).setBorder(b1)).setPaddingLeft(0.0f).setBold();
		                table.addCell(getPartyAddress9("$",decFormat.format(f),bold).setTextAlignment(TextAlignment.RIGHT).setBorder(b1).setMarginRight(0.0f));
		                
		                if(order.getOrderType() != null && order.getOrderType().equalsIgnoreCase("Delivery"))
		                {
		                	table.addCell(createCell("Delievery Fee :").setTextAlignment(TextAlignment.LEFT).setBorder(b1)).setPaddingLeft(0.0f).setBold();
			                table.addCell(getPartyAddress9("$",decFormat.format(Float.parseFloat(order.getDeliveryFee() != null ? order.getDeliveryFee() : "$0.00")),bold).setTextAlignment(TextAlignment.RIGHT).setBorder(b1).setMarginRight(0.0f));
		                }
		                
		                table.addCell(createCell("Tax :").setTextAlignment(TextAlignment.LEFT).setBorder(b1)).setPaddingLeft(0.0f).setBold();
		                table.addCell(getPartyAddress9("$",decFormat.format(Float.parseFloat(order.getTax())),bold).setTextAlignment(TextAlignment.RIGHT).setBorder(b1).setMarginRight(0.0f));
		                
		                table.addCell(createCell("Tip :").setTextAlignment(TextAlignment.LEFT).setBorder(b1)).setPaddingLeft(0.0f).setBold();
		                table.addCell(getPartyAddress9("$",order.getTipAmount() != null ? order.getTipAmount().toString() : "_ _ _ _",bold).setTextAlignment(TextAlignment.RIGHT).setBorder(b1).setMarginRight(0.0f));
		                
		                table.addCell(createCell("____________________________").setTextAlignment(TextAlignment.LEFT).setBorder(b1)).setPaddingLeft(0.0f).setBold();
                		table.addCell(createCell("_________").setTextAlignment(TextAlignment.RIGHT).setBorder(b1).setMarginRight(0.0f));
		                
                		table.addCell(createCell("Total :").setTextAlignment(TextAlignment.LEFT).setBorder(b1)).setPaddingLeft(0.0f).setBold();
		                table.addCell(getPartyAddress9("",String.valueOf("$" + Math.round(order.getOrderPrice()*100.0)/100.0),bold).setTextAlignment(TextAlignment.RIGHT).setBorder(b1).setMarginRight(0.0f));
		                
		                table.addCell(createCell("*********************").setTextAlignment(TextAlignment.RIGHT).setBorder(b1)).setPaddingLeft(0.0f).setBold();
                		table.addCell(createCell("").setTextAlignment(TextAlignment.LEFT).setBorder(b1).setMarginRight(0.0f));
		                
		                
		                table.addCell(createCell("Payment Type :").setTextAlignment(TextAlignment.LEFT).setBorder(b1)).setPaddingLeft(0.0f).setBold();
			            table.addCell(getPartyAddress9("",order.getPaymentMethod(),bold).setTextAlignment(TextAlignment.RIGHT).setBorder(b1).setMarginRight(0.0f));

			            Table table3 = new Table(new UnitValue[] { new UnitValue(10,50) }).setWidth(UnitValue.createPercentValue(100));
			         
		              table3.addCell(getPartyAddress9("Special Instruction :",order.getOrderNote().equals(" ") ? "" : order.getOrderNote(),bold).setTextAlignment(TextAlignment.CENTER).setBorder(b1)).setPaddingLeft(0.0f).setBold();
		              
		              table3.addCell(createCell("").setTextAlignment(TextAlignment.LEFT).setBorder(b1)).setPaddingLeft(0.0f).setBold();
		              table3.addCell(getPartyAddress9("","Thank You For Order !!",bold).setTextAlignment(TextAlignment.CENTER).setBorder(b1).setMarginRight(0.0f));
		              doc.add(table);
		              
		              String  cAddress = null;
		              List<Address> customerAddress=null;
		              if(order.getAddressId()!=null)
		            	  customerAddress= addressRepository.findByCustomerIdAndId(order.getCustomer().getId(),order.getAddressId());
		              else
		                  customerAddress = addressRepository.findByCustomerId(order.getCustomer().getId());
		              for (Address addresses : customerAddress) 
		              {
		            	  if(addresses !=  null)
		            	  {
		            		  cAddress = addresses.getAddress1();
		            		  if(addresses.getAddress2() != null)
			            	  {
			            		  cAddress += addresses.getAddress2();
			            		  if(addresses.getAddress3() != null)
				            	  {
				            		  cAddress += addresses.getAddress3();
				            	  }
			            	  }
		            	  }
		              }
		              
		              if(order.getOrderType() != null)
		              {
		            	  if(order.getOrderType().equalsIgnoreCase("delivery"))
		            	  {
		            		  
		            		  doc.add(new Paragraph().setTextAlignment(TextAlignment.CENTER).setMultipliedLeading(1).add(new Text(String.format("%s %s\n","\n\nCustomer Info\n",order.getCustomer().getFirstName()+" "+ (order.getCustomer().getLastName() != null ? order.getCustomer().getLastName() : " "))).setFont(bold).setFontSize(10)).setMarginTop(-7).add(order.getCustomer().getPhoneNumber()+"\n").add(order.getCustomer().getEmailId()+"\n").setBold().add(cAddress != null ? cAddress : " ").setBold());
		            	  }
		            	  else
		            	  {
		            		  doc.add(new Paragraph().setTextAlignment(TextAlignment.CENTER).setMultipliedLeading(1).add(new Text(String.format("%s %s\n","\n\nCustomer Info\n",order.getCustomer().getFirstName()+" "+ (order.getCustomer().getLastName() != null ? order.getCustomer().getLastName() : " "))).setFont(bold).setFontSize(10)).setMarginTop(-7).add(order.getCustomer().getPhoneNumber()+"\n").add(order.getCustomer().getEmailId()).setBold());
		            	  }
		              }
				      doc.add(table3);
				      doc.close();		                
				  	LOGGER.info("Table created successfully..");
LOGGER.info("===============  ItextFaxUtil : Inside getOrderDetails :: End  ============= ");

		}
    
    public static Cell getPartyAddress9(String name,String orderId,PdfFont bold){
    	LOGGER.info("===============  ItextFaxUtil : Inside getPartyAddress9 :: Start  ============= ");

    	Paragraph p = new Paragraph().setMultipliedLeading(1.0f).add(new Text(name).setFont(bold)).add(orderId);
    	Cell cell = new Cell().setBorder(null).add(p).setTextAlignment(TextAlignment.RIGHT).setMarginRight(0.0f);
    	cell.setBold();
    	LOGGER.info("===============  ItextFaxUtil : Inside getPartyAddress9 :: End  ============= ");

    	return cell;
	}

	public static Cell getPartyAddress(String Tab,String who, String name, String line1, String colun,String line2, String countryID, String postcode, String city, PdfFont bold) {
		 LOGGER.info("===============  ItextFaxUtil : Inside getPartyAddress :: Start  =============Tab "+Tab+" who  "+who+" name "+name+" line1 "+line1+" colun "+colun+" line2 "+line2+" countryID "+countryID + 
		      		" postcode "+postcode+" city "+city);

		Paragraph p = new Paragraph().setMultipliedLeading(1.0f).add(new Text(who).setFont(bold)).add(name)
                    .add(line1).add(line2).add(String.format("%s%s %s", countryID, postcode, city));
		Cell cell = new Cell().setBorder(null).add(p).setTextAlignment(TextAlignment.LEFT);
		LOGGER.info("===============  ItextFaxUtil : Inside getPartyAddress :: End  ============= ");

		return cell;
	}

	public static Cell getPartyAddress6(String tab,String name,String tab1,String tab2,String colun,String tab3,String tab4, String date,PdfFont bold) {
		LOGGER.info("===============  ItextFaxUtil : Inside getPartyAddress6 :: Start  ============= Tab "+tab+" tab1  "+tab1+" name "+name+" tab2 "+tab2+" colun "+colun+" tab3 "+tab3+" tab4 "+tab4 + 
	      		" date "+date);

		Paragraph p = new Paragraph().setMultipliedLeading(1.0f).add(new Text(name).setFont(bold)).add(date);
		Cell cell = new Cell().setBorder(null).add(p).setTextAlignment(TextAlignment.RIGHT).setMarginRight(0.0f);
		cell.setBold();
		LOGGER.info("===============  ItextFaxUtil : Inside getPartyAddress6 :: End  ============= ");

		return cell;
	}
    
	 public  static   String sendUberPdfFax(OrderR order, Table  table, Address address,Environment environment)
{ 
	    	LOGGER.info("===============  ItextFaxUtil : Inside sendPdfFax :: Start  ============= ");

	    	   DecimalFormat df = new DecimalFormat("#,###,##0.00");
	    	      SimpleDateFormat format = new SimpleDateFormat("MM-dd-yyyy hh:mm a");
	    	      String fulfilledOn = null;
	    	     
	    	      if(order.getFulfilled_on() != null){
	    	    	  LOGGER.info("order.getFulfilled_on()"+order.getFulfilled_on());
	    	    	  fulfilledOn = format.format(order.getFulfilled_on());
	    	      }

	        try {
	        	String REGULAR = environment.getProperty("REGULAR");
	            String BOLD = environment.getProperty("BOLD");
	        	
	        	Double uberCharges = order.getOrderPrice()-new Double(order.getSubTotal())-new Double(order.getTax());
	        PdfFont bold = PdfFontFactory.createFont(BOLD, true);
	        String dest =  environment.getProperty("FAX_FILE_PATH")+order.getId()+".pdf";
	        PdfWriter writer = new PdfWriter(dest);
	        PdfDocument pdf = new PdfDocument(writer);
	        Document doc = new Document(pdf);
	        doc.setFont(PdfFontFactory.createFont(REGULAR, true)).setFontSize(12);
	        doc.add( new Paragraph() .setTextAlignment(TextAlignment.RIGHT) .setMultipliedLeading(1) .add(new Text(String.format("%s %s\n","Order Id ",order.getId())).setFont(bold).setFontSize(14))
	                        .add( " "));

	        String locationAdress ="";
	        String lastname ="";
	        if(address!=null) {
	            locationAdress +=address.getAddress1()+","+NEWLINE;
	        }
	        if(address.getAddress2()!=null) {
	            locationAdress +=address.getAddress2()+","+NEWLINE;
	        }
	        else {
	            locationAdress +=","+NEWLINE;
	        }
	        if(address.getCity()!=null) {
	            locationAdress +=address.getCity()+" ";
	        }
	        if(address.getState()!=null) {
	            locationAdress +=address.getState()+" ";
	        }
	        if(address.getZip()!=null) {
	            locationAdress +=address.getZip()+" "+NEWLINE+NEWLINE;
	        }
			LOGGER.info("===== ItextFaxUtil : Inside sendPdfFax :: locationAdress  == " + locationAdress);

	        if(order.getCustomer().getLastName()!=null) {
	        	lastname =order.getCustomer().getLastName();
	        }
	        doc.add( new Paragraph() .setTextAlignment(TextAlignment.LEFT) .setMultipliedLeading(1) .add(new Text(String.format("%s %s\n","",order.getMerchant().getName())).setFont(bold).setFontSize(14)).setMarginTop(-25).add(locationAdress));
	        Table table1 = new Table(new UnitValue[] { new UnitValue(2, 50), new UnitValue(2, 50) }).setWidth(UnitValue.createPercentValue(100));
	      
	        String futureOrder="";
	        if(order.getIsFutureOrder() != null && order.getIsFutureOrder().equals(1)) {
	            futureOrder ="Future Order" +" "+fulfilledOn;
	            
	        }
	        LOGGER.info("===== ItextFaxUtil : Inside sendPdfFax :: futureOrder  == " + futureOrder);

	        table1.addCell(getOrderInfo("Order Type:  ",toUpperCovert(order.getOrderType())  , "Payment Status : ", toUpperCovert(order.getPaymentMethod()) , futureOrder, "",     " ", bold));
	      
	        if (!"Pickup".equalsIgnoreCase(order.getOrderType())) {
					if (order.getCustomer() != null && order.getAddress() != null) {
						table1.addCell(getCustomerInfoDelivery("Customer Information  \n " + "Name : "  ,  toUpperCovert(order.getCustomer().getFirstName()+" "+lastname), "Phone No:", order.getCustomer().getPhoneNumber(),  "Delivery Address : " , order.getAddress().getAddress1()+","+order.getAddress().getAddress2()+","+order.getAddress().getCity()+","+order.getAddress().getState()+","+order.getAddress().getZip(),  order.getOrderType()+"Time: ",fulfilledOn, bold).setMarginLeft(70));
					} 
				} else if(order.getOrderType()!=null){
	           	   table1.addCell(getCustomerInfopPicKup("Customer Information  \n " + "Name : "  ,  toUpperCovert(order.getCustomer().getFirstName()+" "+lastname), "Phone No:", order.getCustomer().getPhoneNumber(),  order.getOrderType()+"Time: ",fulfilledOn,    "", bold).setMarginLeft(70));
	           	}
	           
	           doc.add(table1);
	        
	        table.addCell(getPartyAddress2("", "", "", "", "", "", "", bold));
	        table.addCell(getPartyAddress2("", "", "", "", "", "", "", bold));
	        table.addCell(createCell("Subtotal \t", bold).setTextAlignment(TextAlignment.RIGHT));
	        table.addCell(createCell(
                    "$" + df.format(Double.parseDouble(order.getSubTotal()))).setTextAlignment(TextAlignment.CENTER));


	        table.addCell(getPartyAddress2("", "", "", "", "", "", "", bold));
	        table.addCell(getPartyAddress2("", "", "", "", "", "", "", bold));
	        table.addCell(createCell("Uber Charges", bold).setTextAlignment(TextAlignment.RIGHT));
	        if (uberCharges != null && uberCharges>0.0) {
	        	table.addCell(createCell("$" + df.format(uberCharges)).setTextAlignment(TextAlignment.CENTER));
	        }
	       else{
	            table.addCell(createCell("-").setTextAlignment(TextAlignment.CENTER));
	        }
	        
	        table.addCell(getPartyAddress2("", "", "", "", "", "", "", bold));
	        table.addCell(getPartyAddress2("", "", "", "", "", "", "", bold));
	        table.addCell(createCell("Tax "+" ", bold).setTextAlignment(TextAlignment.RIGHT));
	        if( order.getTax()!=null) {
	            table.addCell(createCell(
                        "$" + df.format(Double.parseDouble(order.getTax()))).setTextAlignment(TextAlignment.CENTER));
	        }
	        else {
	            table.addCell(createCell("-").setTextAlignment(TextAlignment.CENTER));
	        }
	        
	        table.addCell(getPartyAddress2("", "", "", "", "", "", "", bold));
	        table.addCell(getPartyAddress2("", "", "", "", "", "", "", bold));
	        table.addCell(createCell("Total", bold).setTextAlignment(TextAlignment.RIGHT));
	        table.addCell(createCell("$" + df.format(order.getOrderPrice()), bold)
	                        .setTextAlignment(TextAlignment.CENTER));
	        
	        doc.add(table);

	        
	        Table table0 = new Table(new UnitValue[] { new UnitValue(2, 50), new UnitValue(2, 50) }).setWidth(UnitValue.createPercentValue(100));
	        table0.addCell(getPartyAddress("  ", "", "", "  ", "", "",     " \n   ", bold));
	        doc.add(table0);
	        
	        String para3 = "Customer Signature : .................";  
	        Paragraph paragraph3 = new Paragraph(para3).setMarginLeft(340);
	        doc.add(paragraph3);
	        
	        doc.add( new Paragraph() .setTextAlignment(TextAlignment.LEFT) .setMultipliedLeading(1) .add(new Text(String.format("%s %s\n","Order Note : "," ")).setFont(bold).setFontSize(14)).setMarginTop(-60).setMarginRight(10));

	        if(order.getOrderNote()!=null && !order.getOrderNote().equals(" ")) {
	            Table table01 = new Table(new UnitValue[] { new UnitValue(2, 50), new UnitValue(2, 50) }).setWidth(UnitValue.createPercentValue(100));
	            table01.addCell(getPartyAddress3(order.getOrderNote(), bold).setMarginRight(10));
	            doc.add(table01);
	        }
	        
	     
	        String para4 = "";  
	        Paragraph paragraph4 = new Paragraph(para4).setMarginLeft(340);
	        doc.add(paragraph4);
	        
	        
	        doc.add(getPaymentInfo());
	        
	        String para1 = " www.foodkonnekt.com";    
	        Paragraph paragraph2 = new Paragraph(para1).setMarginLeft(180);
	        doc.add(paragraph2);

	        doc.close();
	        LOGGER.info("Table created successfully..");
	        //FaxUtility.sendFaxFile(merchantFaxNumber, dest);
	        } 
	        catch (Exception e) {
	        	LOGGER.error("===============  ItextFaxUtil : Inside sendPdfFax :: Exception  ============= " + e);

	            LOGGER.error("error: " + e.getMessage());
	        }
	        LOGGER.info("===============  ItextFaxUtil : Inside sendPdfFax :: End  ============= ");

	        return null;
	        
	   
	    }

	 public static void getUberOrderDetails(OrderR order, Integer merchantID,Address address,OrderItemRepository orderItemRepository,
	    		OrderItemModifierRepository itemModifierRepository,OrderPizzaRepository orderPizzaRepository,OrderPizzaToppingsRepository orderPizzaToppingsRepository,
	    		OrderPizzaCrustRepository orderPizzaCrustRepository,AddressRepository addressRepository,Environment environment) throws IOException {
	    	LOGGER.info("===============  ItextFaxUtil : Inside getOrderDetails :: Start  =============merchantID "+merchantID);

	    	Double uberCharges = order.getOrderPrice()-new Double(order.getSubTotal())-new Double(order.getTax());
		      
	    	Date today = null;
	    	String REGULAR = environment.getProperty("REGULAR");
	        String BOLD = environment.getProperty("BOLD");
	        
			if (order.getMerchant() != null && order.getMerchant().getTimeZone() != null
					&& order.getMerchant().getTimeZone().getTimeZoneCode() != null) {
				today = DateUtil.getCurrentDateForTimeZonee(order.getMerchant().getTimeZone().getTimeZoneCode());
			} else {
				today = new Date();
			}
			
	    		LOGGER.info("inside ItextFaxUtil");
	    	
		        PdfFont bold = PdfFontFactory.createFont(REGULAR);       
		        String dest = environment.getProperty("THERMAL_RECEIPT")+order.getId()+".pdf";
		        PdfWriter writer = new PdfWriter(dest);
		        PdfDocument pdf = new PdfDocument(writer);
		        Rectangle r = new Rectangle(0.60f, 0.80f);
		        PageSize ps= new PageSize(203.76f,567.36f);
		        DecimalFormat decFormat = new DecimalFormat("0.00");
		        
		        Document doc = new Document(pdf,ps);
		        
		        doc.setMargins(10f, 10f, 10f, 10f);
		        
		         DateFormat dateFormat = new SimpleDateFormat("MMMM dd, yyyy");
		         DateFormat timeFormat = new SimpleDateFormat("hh:mm:ss a");
		         //get current date time with Date()
		         Date date = today;
		         Date time = today;
		         String Date = dateFormat.format(date);
		         String Time = timeFormat.format(time);
		         
		         DateUtil dt = new DateUtil();
		         
		         Calendar c = Calendar.getInstance();
		         
		         LOGGER.info("getCurrentTimeForTimeZone");
		         LOGGER.info(DateUtil.getCurrentTimeForTimeZone(c.getTimeZone().toString()));
		         
		         LOGGER.info("findCurrentTime");
		         LOGGER.info(DateUtil.findCurrentTime());
		         
		         LOGGER.info("findCurrentDateWithTime");
		         LOGGER.info(DateUtil.findCurrentDateWithTime());
		         
		         LOGGER.info("Date Format");
		         LOGGER.info(Time);
		         
		         doc.setFont(PdfFontFactory.createFont(BOLD)).setFontSize(8);
		         
		         String locationAdress ="";
		         if(address!=null) {
		             locationAdress +=address.getAddress1()+" ";
		         }
		         if(address.getAddress2()!=null) {
		             locationAdress +=address.getAddress2()+","+NEWLINE;
		         }
		         else {
		             locationAdress +=","+NEWLINE;
		         }
		         if(address.getCity()!=null) {
		             locationAdress +=address.getCity()+" ";
		         }
		         if(address.getState()!=null) {
		             locationAdress +=address.getState()+" ";
		         }
		         if(address.getZip()!=null) {
		             locationAdress +=address.getZip();
		         }
		         
		            Table table1 = new Table(new UnitValue[] { new UnitValue(10,50) }).setWidth(UnitValue.createPercentValue(100));
			        
			        table1.addCell(getPartyAddress("","","","","","Receipt","","","", bold).setTextAlignment(TextAlignment.CENTER)).setBold();
			        Border b2=new SolidBorder(Color.WHITE,2f);

			        table1.addCell(createCell("").setTextAlignment(TextAlignment.LEFT).setBorder(b2)).setPaddingLeft(0.0f).setBold();
	                table1.addCell(getPartyAddress6("","","","","","","",Date+" "+Time,bold).setTextAlignment(TextAlignment.CENTER).setBorder(b2).setMarginRight(0.0f));
			        
		            doc.add(table1);
			        doc.add(new Paragraph().setTextAlignment(TextAlignment.CENTER).setMultipliedLeading(1).add(new Text(String.format("%s \n %s \n %s\n\n",order.getMerchant().getName(),locationAdress,order.getMerchant().getPhoneNumber())).setFont(bold).setFontSize(8)).setMarginTop(-4).setBold());
			        
			        Border b1=new SolidBorder(Color.WHITE,2f);
			        
			        Table table = new Table(new UnitValue[]
			        {
			                		new UnitValue(UnitValue.PERCENT, 60.5f),
			                        new UnitValue(UnitValue.PERCENT, 15.5f)})
			                        .setWidth(UnitValue.createPercentValue(100))
			                        .setBorder(b1).setMarginLeft(0.0f).setMarginRight(0.0f);
			                
			                List<OrderItem> orderItems = orderItemRepository.findByOrderId(order.getId());

			                for (OrderItem orderItem : orderItems) 
			                {
			                    double modifiersPrice = 0;

			                	if(orderItem.getItem()!=null &&orderItem.getItem().getId()!=null) 
			                	{
			                    if(orderItem.getItem()!=null &&orderItem.getItem().getName()!=null)
			                    {
			                    	table.addCell(getPartyAddress9("",orderItem.getItem().getName()+" X "+orderItem.getQuantity().toString(),bold).setTextAlignment(TextAlignment.LEFT).setBorder(b1).setMarginRight(0.0f));
			                    	table.addCell(getPartyAddress9("$",decFormat.format(Float.parseFloat(String.valueOf((orderItem.getItem().getPrice() + modifiersPrice) * orderItem.getQuantity()))),bold).setTextAlignment(TextAlignment.RIGHT).setBorder(b1).setMarginRight(0.0f));
			                    }
			                  
			                    if ((orderItem.getId() != null)) 
			                    {
			                        List<OrderItemModifier> itemModifiers = itemModifierRepository.findByOrderItemId(orderItem.getId());
			                        String modifiers = "";
			                        
			                        if (!itemModifiers.isEmpty() && itemModifiers != null) 
			                        {
			                            for (OrderItemModifier orderItemModifier : itemModifiers) 
			                            {
			                               modifiers = orderItemModifier.getModifiers().getName()+"\n";
			                               modifiersPrice = Double.valueOf(orderItemModifier.getModifiers().getPrice().toString());
			                               table.addCell(getPartyAddress9("*",modifiers,bold).setTextAlignment(TextAlignment.LEFT).setBorder(b1).setMarginRight(0.0f));
				                           table.addCell(getPartyAddress9("$",decFormat.format(Float.parseFloat(String.valueOf((modifiersPrice) * orderItem.getQuantity()))),bold).setTextAlignment(TextAlignment.RIGHT).setBorder(b1).setMarginRight(0.0f));
			                            }
			                            modifiersPrice = 0;
			                        } 
			                    }
			                    LOGGER.info("orderItem.getItem().getPrice()"+orderItem.getItem().getPrice());
			                    LOGGER.info("orderItem.getQuantity()"+orderItem.getQuantity());
			                    modifiersPrice =0;
			                	}
			               }
			                List<OrderPizza> orderPizzas = orderPizzaRepository.findByOrderId(order.getId());
			                
			                if(!orderPizzas.isEmpty() && orderPizzas != null)
			                {
			                    for (OrderPizza orderPizza : orderPizzas) 
			                    {
			                        if (orderPizza != null && orderPizza.getPizzaTemplate() != null) 
			                        {
			                                    if(orderPizza!=null && orderPizza.getPizzaTemplate()!=null && orderPizza.getPizzaTemplate().getDescription()!= null &&  orderPizza.getPizzaSize() != null && orderPizza.getPizzaSize().getDescription()!=null)
			                                    {
			                                    	        table.addCell(getPartyAddress9("",orderPizza.getPizzaTemplate().getDescription()+"("+orderPizza.getPizzaSize().getDescription()+")"+" X "+orderPizza.getQuantity(),bold).setTextAlignment(TextAlignment.LEFT).setBorder(b1).setMarginRight(0.0f));
			                                    	        table.addCell(getPartyAddress9("$",decFormat.format(Float.parseFloat(String.valueOf((orderPizza.getPrice()) * orderPizza.getQuantity()))),bold).setTextAlignment(TextAlignment.RIGHT));
			                                    }
			                                    String modifiers = "";
			                                    double modifiersPrice = 0;
			                                    List<OrderPizzaToppings> orderPizzaToppings = orderPizzaToppingsRepository.findByOrderPizzaId(orderPizza.getId());
			                                    if(!orderPizzaToppings.isEmpty() && orderPizzaToppings!=null)
			                                    {
			                                        for (OrderPizzaToppings pizzaToppings : orderPizzaToppings)
			                                        {
			                                        	if(pizzaToppings!=null && pizzaToppings.getPizzaTopping()!=null && pizzaToppings.getPizzaTopping().getDescription()!=null)
			                                        	{
			                                                   	modifiers +=  pizzaToppings.getPizzaTopping().getDescription();
			                                                    if(pizzaToppings.getSide1()!=null && pizzaToppings.getSide1())
			                                                    {
			                                                        modifiers += "(First Half)";
			                                                        modifiersPrice += pizzaToppings.getPrice();
			                                                    }
			                                                    else if(pizzaToppings.getSide2()!=null && pizzaToppings.getSide2())
			                                                    {
			                                                    	modifiers += "(Second Half)";
			                                                        modifiersPrice += pizzaToppings.getPrice();
			                                                    }
			                                                    else
			                                                    {
			                                                    	modifiers += "(Full)";
			                                                        modifiersPrice += pizzaToppings.getPrice();
			                                                    }
			                                                   modifiers += "\n";
			                                        	}
			                                        	table.addCell(getPartyAddress9("*",modifiers,bold).setTextAlignment(TextAlignment.LEFT).setBorder(b1).setMarginRight(0.0f));
				                                    	table.addCell(getPartyAddress9("$",decFormat.format(Float.parseFloat(String.valueOf(modifiersPrice * orderPizza.getQuantity()))),bold).setTextAlignment(TextAlignment.RIGHT));
				                                    	modifiers = " ";
				                                    	modifiersPrice = 0.0;
			                                        }
			                                    }
			                                    
			                                    OrderPizzaCrust orderPizzaCrust = orderPizzaCrustRepository.findByOrderPizzaId(orderPizza.getId());
			                                    
			                                    if(orderPizzaCrust != null && orderPizzaCrust.getPizzacrust()!=null)
			                                    {
			                                    	int crustPrice = 0;
			                                        modifiers += orderPizzaCrust.getPizzacrust().getDescription();
			                                        
			                                        crustPrice += orderPizzaCrust.getPrice();
			                                        table.addCell(getPartyAddress9("*",modifiers,bold).setTextAlignment(TextAlignment.LEFT).setBorder(b1).setMarginRight(0.0f));
		                                    	    table.addCell(getPartyAddress9("$",decFormat.format(Float.parseFloat(String.valueOf(orderPizzaCrust.getPrice() * orderPizza.getQuantity()))),bold).setTextAlignment(TextAlignment.RIGHT));
				                                    modifiers="";
			                                    }
			                                    
			                        }
			                    }
			                }
	 				        
			                table.addCell(createCell("____________________________").setTextAlignment(TextAlignment.LEFT).setBorder(b1)).setPaddingLeft(0.0f).setBold();
	                		table.addCell(createCell("_________").setTextAlignment(TextAlignment.RIGHT).setBorder(b1).setMarginRight(0.0f));
			                
			                table.addCell(createCell("Subtotal :").setTextAlignment(TextAlignment.LEFT).setBorder(b1)).setPaddingLeft(0.0f).setBold();
			                table.addCell(getPartyAddress9("$",order.getSubTotal(),bold).setTextAlignment(TextAlignment.RIGHT).setBorder(b1).setMarginRight(0.0f));
	
			                table.addCell(createCell("Uber Charges :").setTextAlignment(TextAlignment.LEFT).setBorder(b1)).setPaddingLeft(0.0f).setBold();
				                table.addCell(getPartyAddress9("$",decFormat.format(Float.parseFloat(uberCharges.toString() != null ? uberCharges.toString() : "$0.00")),bold).setTextAlignment(TextAlignment.RIGHT).setBorder(b1).setMarginRight(0.0f));
			                
			                
			                table.addCell(createCell("Tax :").setTextAlignment(TextAlignment.LEFT).setBorder(b1)).setPaddingLeft(0.0f).setBold();
			                table.addCell(getPartyAddress9("$",decFormat.format(Float.parseFloat(order.getTax())),bold).setTextAlignment(TextAlignment.RIGHT).setBorder(b1).setMarginRight(0.0f));
			                
			                 table.addCell(createCell("____________________________").setTextAlignment(TextAlignment.LEFT).setBorder(b1)).setPaddingLeft(0.0f).setBold();
	                		table.addCell(createCell("_________").setTextAlignment(TextAlignment.RIGHT).setBorder(b1).setMarginRight(0.0f));
			                
	                		table.addCell(createCell("Total :").setTextAlignment(TextAlignment.LEFT).setBorder(b1)).setPaddingLeft(0.0f).setBold();
			                table.addCell(getPartyAddress9("",
                                                           "$" + Math.round(order.getOrderPrice() * 100.0) / 100.0, bold).setTextAlignment(TextAlignment.RIGHT).setBorder(b1).setMarginRight(0.0f));
			                
			                table.addCell(createCell("*********************").setTextAlignment(TextAlignment.RIGHT).setBorder(b1)).setPaddingLeft(0.0f).setBold();
	                		table.addCell(createCell("").setTextAlignment(TextAlignment.LEFT).setBorder(b1).setMarginRight(0.0f));
			                
			                
			                table.addCell(createCell("Payment Type :").setTextAlignment(TextAlignment.LEFT).setBorder(b1)).setPaddingLeft(0.0f).setBold();
				            table.addCell(getPartyAddress9("",order.getPaymentMethod(),bold).setTextAlignment(TextAlignment.RIGHT).setBorder(b1).setMarginRight(0.0f));

				            Table table3 = new Table(new UnitValue[] { new UnitValue(10,50) }).setWidth(UnitValue.createPercentValue(100));
				            
				           /* change for Squareup */
				           if(order.getOrderNote()==null)
				            	order.setOrderNote(" ");
			              table3.addCell(getPartyAddress9("Special Instruction :",order.getOrderNote().equals(" ") ? "" : order.getOrderNote(),bold).setTextAlignment(TextAlignment.CENTER).setBorder(b1)).setPaddingLeft(0.0f).setBold();
			              
			              table3.addCell(createCell("").setTextAlignment(TextAlignment.LEFT).setBorder(b1)).setPaddingLeft(0.0f).setBold();
			              table3.addCell(getPartyAddress9("","Thank You For Order !!",bold).setTextAlignment(TextAlignment.CENTER).setBorder(b1).setMarginRight(0.0f));
			              doc.add(table);
			              
			              String  cAddress = null;
			              List<Address> customerAddress = addressRepository.findByCustomerId(order.getCustomer().getId());
			              for (Address addresses : customerAddress) 
			              {
			            	  if(addresses !=  null)
			            	  {
			            		  cAddress = addresses.getAddress1();
			            		  if(addresses.getAddress2() != null)
				            	  {
				            		  cAddress += addresses.getAddress2();
				            		  if(addresses.getAddress3() != null)
					            	  {
					            		  cAddress += addresses.getAddress3();
					            	  }
				            	  }
			            	  }
			              }
			              
			              if(order.getOrderType() != null)
			              {
			            	  if(order.getOrderType().equalsIgnoreCase("delivery"))
			            	  {
			            		  
			            		  doc.add(new Paragraph().setTextAlignment(TextAlignment.CENTER).setMultipliedLeading(1).add(new Text(String.format("%s %s\n","\n\nCustomer Info\n",order.getCustomer().getFirstName()+" "+ (order.getCustomer().getLastName() != null ? order.getCustomer().getLastName() : " "))).setFont(bold).setFontSize(10)).setMarginTop(-7).add(order.getCustomer().getPhoneNumber()+"\n").add(order.getCustomer().getEmailId()+"\n").setBold().add(cAddress != null ? cAddress : " ").setBold());
			            	  }
			            	  else
			            	  {
			            		  doc.add(new Paragraph().setTextAlignment(TextAlignment.CENTER).setMultipliedLeading(1).add(new Text(String.format("%s %s\n","\n\nCustomer Info\n",order.getCustomer().getFirstName()+" "+ (order.getCustomer().getLastName() != null ? order.getCustomer().getLastName() : " "))).setFont(bold).setFontSize(10)).setMarginTop(-7).add(order.getCustomer().getPhoneNumber()+"\n").add(order.getCustomer().getEmailId()).setBold());
			            	  }
			              }
					      doc.add(table3);
					      doc.close();		                
					  	LOGGER.info("Table created successfully..");
	LOGGER.info("===============  ItextFaxUtil : Inside getOrderDetails :: End  ============= ");

			}
	 
public static void main(String[] args) {
	//sendPdfFax(1111, order, table)
}
}
