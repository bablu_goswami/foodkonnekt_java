package com.foodkonnekt.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import java.io.IOException;
import java.io.InputStream;

import com.foodkonnekt.model.ExcelWorkbook;
import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CloudPrintHelper {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(CloudPrintHelper.class);
	
	static String encodeFileToBase64Binary(String fileName)
			throws IOException {

		File file = new File(fileName);
		byte[] bytes = loadFile(file);
		byte[] encoded = Base64.encodeBase64(bytes);
		String encodedString = new String(encoded);

		return encodedString;
	}

	
	private static byte[] loadFile(File file) throws IOException {
	    InputStream is = new FileInputStream(file);

	    long length = file.length();
	    if (length > Integer.MAX_VALUE) {
	        // File is too large
	    }
	    byte[] bytes = new byte[(int)length];
	    
	    int offset = 0;
	    int numRead = 0;
	    while (offset < bytes.length
	           && (numRead=is.read(bytes, offset, bytes.length-offset)) >= 0) {
	        offset += numRead;
	    }

	    if (offset < bytes.length) {
	        throw new IOException("Could not completely read file "+file.getName());
	    }

	    is.close();
	    return bytes;
	}
	
	
	  public static byte[] filetoByteArray(File file) {
		  
		  //init array with file length
		  byte[] bytesArray = new byte[(int) file.length()]; 

		  FileInputStream fis;
		try {
			fis = new FileInputStream(file);
			 fis.read(bytesArray); //read file into bytes[]
			  fis.close();
						
		} catch (FileNotFoundException e) {			
			LOGGER.error("error: " + e.getMessage());
		} catch (IOException e) {
			LOGGER.error("error: " + e.getMessage());
		}
		  
					
		  return bytesArray;
	  }
	 

  
}
