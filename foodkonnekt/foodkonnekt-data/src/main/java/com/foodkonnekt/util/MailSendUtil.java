package com.foodkonnekt.util;

import com.foodkonnekt.model.Address;
import com.foodkonnekt.model.CommonMail;
import com.foodkonnekt.model.Customer;
import com.foodkonnekt.model.CustomerFeedback;
import com.foodkonnekt.model.CustomerFeedbackAnswer;
import com.foodkonnekt.model.Merchant;
import com.foodkonnekt.model.MerchantConfiguration;
import com.foodkonnekt.model.MerchantLogin;
import com.foodkonnekt.model.MerchantSubscription;
import com.foodkonnekt.model.OnlineOrderNotificationStatus;
import com.foodkonnekt.model.OrderR;
import com.foodkonnekt.model.PaymentGateWay;
import com.foodkonnekt.model.PrintJob;
import com.foodkonnekt.model.Subscription;
import com.foodkonnekt.model.Vendor;
import com.foodkonnekt.repository.MerchantConfigurationRepository;
import com.foodkonnekt.repository.OrderRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

@Component
public class MailSendUtil {

	@Autowired
    private Environment environment;
	
	@Autowired
	private OrderRepository orderRepository;
	
	@Autowired
	private MerchantConfigurationRepository merchantConfigurationRepository;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(MailSendUtil.class);
	
	
	public static boolean sendMail(CommonMail commonMail) {
		
		LOGGER.info("----------------Start :: MailSendUtil : sendMail------------------");
		try {
			Message message = new MimeMessage(
					SendMailProperty.gmailMailProperty(IConstant.FROM_EMAIL_ID, IConstant.FROM_PASSWORD));
			message.setFrom(new InternetAddress(IConstant.FROM_EMAIL_ID));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(commonMail.getToEmail()));
			message.setSubject(commonMail.getSubject());
			String msg = "Dear<br>";
			msg += commonMail.getBody();
			msg += " <br>";
			msg += " <br>";
			msg += "<b>Regards,</b><br>";
			msg += "FoodKonnekt";
			message.setContent(msg, "text/html");
			Transport.send(message);
			LOGGER.info("MailSendUtil :: sendMail : mail send successfully");
		} catch (MessagingException e) {
			LOGGER.error("MailSendUtil :: sendMail : Exception "+e);
			throw new RuntimeException(e);
		}
		LOGGER.info("----------------End :: MailSendUtil : sendMail------------------");
		return true;
	}

	public static String forgotPasswordEmailForMultiLocationCustomer(Customer customer,Integer vendorId,Environment environment) {
		LOGGER.info("----------------Start :: MailSendUtil : forgotPasswordEmailForMultiLocationCustomer------------------");
		try {
			String cusID = EncryptionDecryptionUtil.encryptString(String.valueOf(customer.getId()));
			String marchantId = String.valueOf(customer.getMerchantt().getId());
			
			LOGGER.info("MailSendUtil :: forgotPasswordEmailForMultiLocationCustomer : customerId "+EncryptionDecryptionUtil.decryption(cusID));
			LOGGER.info("MailSendUtil :: forgotPasswordEmailForMultiLocationCustomer : merchantId "+marchantId);
			
			String timeLog = String.valueOf(System.currentTimeMillis());
			Message message = new MimeMessage(
					SendMailProperty.gmailMailProperty(IConstant.FROM_EMAIL_ID, IConstant.FROM_PASSWORD));
			message.setFrom(new InternetAddress(IConstant.FROM_EMAIL_ID));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(customer.getEmailId()));
			message.setSubject("Regards:Forgot password");
			/*String msg = "Dear " + customer.getFirstName() + "<br>";
			msg += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;We have received a request to reset your password .Please find your reset password link below::<br>";
			msg += " <br>";
			msg += " <br>";
			msg += "Click to : " + environment.getProperty("WEB_BASE_URL") + "/changepassword?email=" + customer.getEmailId()
					+ "&merchantId=" + marchantId + "&userId=" + cusID + "&tLog="
					+ EncryptionDecryptionUtil.encryption(timeLog) + "<br>";
			msg += " <br>";
			msg += " <br>";
			msg += " <br>";
			msg += "<b>Regards,</b><br>";
			msg += "FoodKonnekt";*/
			
			String merchantLogo="";
			if (customer.getMerchantt().getMerchantLogo() == null) {
                merchantLogo = "http://www.dev.foodkonnekt.com/app/foodnew.jpg";
            } else {
                merchantLogo = environment.getProperty("BASE_PORT") + customer.getMerchantt().getMerchantLogo();
            }
			
			String msg = "<div style='width: 700px; height: auto; border: solid 1px #ddd; box-shadow: 0px 0px 6px #ddd; background-color: #f2f2f2; text-align: center; margin: 40px 0px 0px 35px;'>"
					+ "<div style='width: 700px; height: 85px; padding: 20px 0px; text-align: center;'>&nbsp;<img src='"
					+ merchantLogo + "' height='80' /></div>"
					+ "<div style='background-color: #fff; padding: 20px 0px 0px 0px; width: 90%; margin: 0 auto 65px auto;'>"
					+ "<p style='font-size: 20px; color: #e6880f;'>Dear ";
					if(customer.getFirstName()!= null) {
						msg +=  customer.getFirstName() + " ";
					}
					if(customer.getLastName()!= null) {
						msg += customer.getLastName()  ;
					}
					
					msg += ",</p><p style='font-size: 14px;text-align: left;padding-left:10px;'>We have received a request to reset your password .Please find your reset password link below: &nbsp;</p>";
					msg = msg+"<br /><br /> <a style='text-decoration: none; margin: 16px 0px; display: inline-block; background-color: #f8981d; color: #fff; padding: 10px 12px; border-radius: 10px; font-weight: 600; font-size: 15px;' href='"; 
					
			Integer merchantId = Integer.parseInt(marchantId);
			
					/*if(merchantId.equals(IConstant.TEXAN_REWARDS_MERCHANT_ID)){
						msg = msg+  environment.getProperty("TEXAN_BASE_URL") + "/changepassword?email=" + customer.getEmailId()
								+ "&merchantId=" + marchantId + "&userId=" + cusID + "&tLog="
								+ EncryptionDecryptionUtil.encryption(timeLog);
					}else{
						msg = msg+ environment.getProperty("WEB_BASE_URL") + "/changepassword?email=" + customer.getEmailId()
								+ "&merchantId=" + marchantId + "&userId=" + cusID + "&tLog="
								+ EncryptionDecryptionUtil.encryption(timeLog);
					}*/
			msg = msg+ environment.getProperty("WEB_BASE_URL") + "/changepassword?email=" + customer.getEmailId()
					+ "&merchantId=" + marchantId + "&userId=" + cusID + "&tLog="
					+ EncryptionDecryptionUtil.encryption(timeLog);
			
			msg=msg+ "' target='_blank'>Click here</a><br /> Note: Link will valid till next 30 min only .";

			msg = msg
					+ "<p><Order details: items, extras, price, fees, promo discount, total $><br>For any further questions, ";
			if(customer.getMerchantt().getPhoneNumber()!=null) { 
				msg = msg +"Please call us at " + customer.getMerchantt().getPhoneNumber() + "";}
			else{
				msg = msg +"Please call our store";
			}
					msg = msg +" <br><br>Regards,<br>" + customer.getMerchantt().getName()
					+ "<br><br>";
			msg = msg + "</div>";
			msg = msg + "<p>We appreciate your business with us!</p>";
			/*if( merchantId!=null && merchantId.equals(IConstant.TEXAN_REWARDS_MERCHANT_ID) ){
				msg = msg + "<div style='height: 70px; text-align: center;'>&nbsp;<img src='https://www.foodkonnekt.com/foodkonnekt_merchat_logo/274_PiePizzaria.png' alt='' width='200' height='63' /></div>"
						+ "</div>";
			}else{*/
				msg = msg +	 "<div style='height: 70px; text-align: center;'>&nbsp;<img src='http://www.dev.foodkonnekt.com/app/foodnew.jpg' alt='' width='200' height='63' /></div>"
					+ "</div>";
			//}
				
			msg = msg + "<p style='color: #676767; font-size: 11px;'>Automated email. Please do not reply to this sender email.</p>";
			
			
			message.setContent(msg, "text/html");
			Transport.send(message);
			LOGGER.info("MailSendUtil :: forgotPasswordEmailForMultiLocationCustomer : mail send successfully");
		} catch (MessagingException e) {
			LOGGER.error("MailSendUtil :: forgotPasswordEmailForMultiLocationCustomer : Exception "+e);
			throw new RuntimeException(e);
		}
		LOGGER.info("----------------End :: MailSendUtil : forgotPasswordEmailForMultiLocationCustomer------------------");
		return null;
	}
	public static String forgotPasswordEmail(Customer customer,Environment environment) {
		try {
			LOGGER.info("----------------Start :: MailSendUtil : forgotPasswordEmail------------------");
			String cusID = EncryptionDecryptionUtil.encryptString(String.valueOf(customer.getId()));
			String marchantId = String.valueOf(customer.getMerchantt().getId());
			
			LOGGER.info("MailSendUtil :: forgotPasswordEmail : customerId "+EncryptionDecryptionUtil.decryption(cusID));
			LOGGER.info("MailSendUtil :: forgotPasswordEmail : merchantId "+marchantId);
			
			String timeLog = String.valueOf(System.currentTimeMillis());
			Message message = new MimeMessage(
					SendMailProperty.gmailMailProperty(IConstant.FROM_EMAIL_ID, IConstant.FROM_PASSWORD));
			message.setFrom(new InternetAddress(IConstant.FROM_EMAIL_ID));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(customer.getEmailId()));
			message.setSubject("Regards:Forgot password");
			/*String msg = "Dear " + customer.getFirstName() + "<br>";
			msg += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;We have received a request to reset your password .Please find your reset password link below::<br>";
			msg += " <br>";
			msg += " <br>";
			msg += "Click to : " + environment.getProperty("WEB_BASE_URL") + "/changepassword?email=" + customer.getEmailId()
					+ "&merchantId=" + marchantId + "&userId=" + cusID + "&tLog="
					+ EncryptionDecryptionUtil.encryption(timeLog) + "<br>";
			msg += " <br>";
			msg += " <br>";
			msg += " <br>";
			msg += "<b>Regards,</b><br>";
			msg += "FoodKonnekt";*/
			
			String merchantLogo="";
			if (customer.getMerchantt().getMerchantLogo() == null) {
                merchantLogo = "http://www.dev.foodkonnekt.com/app/foodnew.jpg";
            } else {
                merchantLogo = environment.getProperty("BASE_PORT") + customer.getMerchantt().getMerchantLogo();
            }
			
			String msg = "<div style='width: 700px; height: auto; border: solid 1px #ddd; box-shadow: 0px 0px 6px #ddd; background-color: #f2f2f2; text-align: center; margin: 40px 0px 0px 35px;'>"
					+ "<div style='width: 700px; height: 85px; padding: 20px 0px; text-align: center;'>&nbsp;<img src='"
					+ merchantLogo + "' height='80' /></div>"
					+ "<div style='background-color: #fff; padding: 20px 0px 0px 0px; width: 90%; margin: 0 auto 65px auto;'>"
					+ "<p style='font-size: 20px; color: #e6880f;'>Dear ";
					if(customer.getFirstName()!= null) {
						msg +=  customer.getFirstName() + " ";
					}
					if(customer.getLastName()!= null) {
						msg += customer.getLastName()  ;
					}
					msg = msg+",</p><p style='font-size: 14px;text-align: left;padding-left:10px;'>We have received a request to reset your password .Please find your reset password link below: &nbsp;</p>";
					msg = msg+"<br /><br /> <a style='text-decoration: none; margin: 16px 0px; display: inline-block; background-color: #f8981d; color: #fff; padding: 10px 12px; border-radius: 10px; font-weight: 600; font-size: 15px;' href='";
					
			Integer merchantId = Integer.parseInt(marchantId);
			
			/*if(merchantId.equals(IConstant.TEXAN_REWARDS_MERCHANT_ID)){
						msg = msg+ environment.getProperty("TEXAN_BASE_URL") + "/changepassword?email=" + customer.getEmailId()
								+ "&merchantId=" + marchantId + "&userId=" + cusID + "&tLog="
								+ EncryptionDecryptionUtil.encryption(timeLog);
					}else{
						msg = msg+ environment.getProperty("WEB_BASE_URL") + "/changepassword?email=" + customer.getEmailId()
								+ "&merchantId=" + marchantId + "&userId=" + cusID + "&tLog="
								+ EncryptionDecryptionUtil.encryption(timeLog);
					}*/
				
			msg = msg+ environment.getProperty("WEB_BASE_URL") + "/changepassword?email=" + customer.getEmailId()
					+ "&merchantId=" + marchantId + "&userId=" + cusID + "&tLog="
					+ EncryptionDecryptionUtil.encryption(timeLog);
			
			msg = msg+ "' target='_blank'>Click here</a><br /> Note: Link will valid till next 30 min only .";

			msg = msg
					+ "<p><Order details: items, extras, price, fees, promo discount, total $><br>For any further questions, ";
			if(customer.getMerchantt().getPhoneNumber()!=null) { 
				msg = msg +"Please call us at " + customer.getMerchantt().getPhoneNumber() + "";}
			else{
				msg = msg +"Please call our store";
			}
					msg = msg +" <br><br>Regards,<br>" + customer.getMerchantt().getName()
					+ "<br><br>";
			msg = msg + "</div>";
			msg = msg + "<p>We appreciate your business with us!</p>";
			
			/*if(merchantId.equals(IConstant.TEXAN_REWARDS_MERCHANT_ID)){
				msg = msg + "<div style='height: 70px; text-align: center;'>&nbsp;<img src='https://www.foodkonnekt.com/foodkonnekt_merchat_logo/274_PiePizzaria.png' alt='' width='200' height='63' /></div>"
						+ "</div>";
			}else{*/
				msg = msg + "<div style='height: 70px; text-align: center;'>&nbsp;<img src='http://www.dev.foodkonnekt.com/app/foodnew.jpg' alt='' width='200' height='63' /></div>"
						+ "</div>";
			//}
			
					
			msg = msg + "<p style='color: #676767; font-size: 11px;'>Automated email. Please do not reply to this sender email.</p>";
			
			
			message.setContent(msg, "text/html");
			Transport.send(message);
			LOGGER.info("MailSendUtil :: forgotPasswordEmail : mail send successfully");
		} catch (MessagingException e) {
			LOGGER.error("MailSendUtil :: forgotPasswordEmail : Exception "+e);
			throw new RuntimeException(e);
		}
		LOGGER.info("----------------End :: MailSendUtil : forgotPasswordEmail------------------");
		return null;
	}
	
	public static String forgotAdminPasswordEmail(Customer customer,Environment environment) {
		try {
			LOGGER.info("===============  MailSendUtil : Inside forgotAdminPasswordEmail :: Start  ============= ");

			Date today = (customer.getMerchantt() != null && customer.getMerchantt().getTimeZone() != null
					&& customer.getMerchantt().getTimeZone().getTimeZoneCode() != null)
							? DateUtil.getCurrentDateForTimeZonee(
									customer.getMerchantt().getTimeZone().getTimeZoneCode())
							: new Date();
			String cusID = EncryptionDecryptionUtil.encryptString(String.valueOf(customer.getId()));
			String marchantId = String.valueOf(customer.getMerchantt().getId());
			String timeLog = String.valueOf(System.currentTimeMillis());
			Message message = new MimeMessage(
					SendMailProperty.gmailMailProperty(IConstant.FROM_EMAIL_ID, IConstant.FROM_PASSWORD));
			message.setFrom(new InternetAddress(IConstant.FROM_EMAIL_ID));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(customer.getEmailId()));
			message.setSubject("Regards:Forgot password");
			/*String msg = "Dear " + customer.getFirstName() + "<br>";
			msg += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;We have received a request to reset your password .Please find your reset password link below::<br>";
			msg += " <br>";
			msg += " <br>";
			msg += "Click to : " + environment.getProperty("WEB_BASE_URL") + "/changepassword?email=" + customer.getEmailId()
					+ "&merchantId=" + marchantId + "&userId=" + cusID + "&tLog="
					+ EncryptionDecryptionUtil.encryption(timeLog) + "<br>";
			msg += " <br>";
			msg += " <br>";
			msg += " <br>";
			msg += "<b>Regards,</b><br>";
			msg += "FoodKonnekt";*/
			
			String merchantLogo="";
			/*if (customer.getMerchantt().getMerchantLogo() == null) {*/
                merchantLogo = "http://www.dev.foodkonnekt.com/app/foodnew.jpg";
            /*} else {
                merchantLogo = UrlConstant.BASE_PORT + customer.getMerchantt().getMerchantLogo();
            }*/
			
			String msg = "<div style='width: 700px; height: auto; border: solid 1px #ddd; box-shadow: 0px 0px 6px #ddd; background-color: #f2f2f2; text-align: center; margin: 40px 0px 0px 35px;'>"
					+ "<div style='width: 700px; height: 85px; padding: 20px 0px; text-align: center;'>&nbsp;<img src='"
					+ merchantLogo + "' height='80' /></div>"
					+ "<div style='background-color: #fff; padding: 20px 0px 0px 0px; width: 90%; margin: 0 auto 65px auto;'>"
					+ "<p style='font-size: 20px; color: #e6880f; padding-left: 10px' align='left'>Dear " ;
					if(customer.getFirstName()!= null) {
						msg +=  customer.getFirstName() + " ";
					}
					if(customer.getLastName()!= null) {
						msg += customer.getLastName()  ;
					}
					msg	+= ",</p><p style='font-size: 14px;text-align: left;padding-left:10px;'>We received a password reset request - click on the below link to reset your password.&nbsp;</p>";
					msg = msg+"<br /><br /> <a style='text-decoration: none; margin: 16px 0px; display: inline-block; background-color: #f8981d; color: #fff; padding: 10px 12px; border-radius: 10px; font-weight: 600; font-size: 15px;' href='";
			
			Integer merchantId = Integer.parseInt(marchantId);
			/*if(merchantId.equals(IConstant.TEXAN_REWARDS_MERCHANT_ID)){
				msg = msg+ environment.getProperty("TEXAN_BASE_URL") + "/changepassword?email=" + customer.getEmailId()
						+ "&merchantId=" + marchantId + "&userId=" + cusID + "&tLog="
						+ EncryptionDecryptionUtil.encryption(timeLog);
			}else{
				msg = msg+ environment.getProperty("WEB_BASE_URL") + "/changepassword?email=" + customer.getEmailId()
						+ "&merchantId=" + marchantId + "&userId=" + cusID + "&tLog="
						+ EncryptionDecryptionUtil.encryption(timeLog);
			}*/
			if(customer.getUserPage()!=null && customer.getUserPage().contains("user")) {
				msg = msg+ environment.getProperty("WEB_BASE_URL") + "/changeUserpassword?email=" + customer.getEmailId()
				+ "&merchantId=" + marchantId + "&userId=" + cusID + "&tLog="
				+ EncryptionDecryptionUtil.encryption(timeLog);
			}else {
			msg = msg+ environment.getProperty("WEB_BASE_URL") + "/changepassword?email=" + customer.getEmailId()
					+ "&merchantId=" + marchantId + "&userId=" + cusID + "&tLog="
					+ EncryptionDecryptionUtil.encryption(timeLog);
			}
			
			msg = msg+	"' target='_blank'>Click here</a>"
					+ "<p style='text-align: left;padding-left:10px;'><br /> Note: Link is valid for 30 min only. Sent on :"+ today +" </p>";
			
			msg = msg +"<p style='text-align: left;padding-left:10px;'><br><br>Regards,<br>Foodkonnekt Team<br><br></p>";
			msg = msg + "</div>";
			msg = msg + "<p>We appreciate your business with us!</p>";
			/*if( merchantId!=null && merchantId.equals(IConstant.TEXAN_REWARDS_MERCHANT_ID) ){
				msg = msg + "<div style='height: 70px; text-align: center;'>&nbsp;<img src='https://www.foodkonnekt.com/foodkonnekt_merchat_logo/274_PiePizzaria.png' alt='' width='200' height='63' /></div>"
						+ "</div>";
			}else{*/
				msg = msg + "<div style='height: 70px; text-align: center;'>&nbsp;<img src='http://www.dev.foodkonnekt.com/app/foodnew.jpg' alt='' width='200' height='63' /></div>"
					+ "</div>";
			//}
			msg = msg + "<p style='color: #676767; font-size: 11px;'>Automated email. Please do not reply to this sender email.</p>";
			
			
			message.setContent(msg, "text/html");
			Transport.send(message);
		} catch (MessagingException e) {
			LOGGER.error("===============  MailSendUtil : Inside forgotAdminPasswordEmail :: Exception  ============= " + e);

			throw new RuntimeException(e);
		}
		LOGGER.info("===============  MailSendUtil : Inside forgotAdminPasswordEmail :: End  ============= ");

		return null;
	}

	public static String forgotMerchantPasswordEmail(MerchantLogin merchantLogin,Environment environment) {
		try {
			LOGGER.info("===============  MailSendUtil : Inside forgotMerchantPasswordEmail :: Start  ============= ");

			String marchantId = EncryptionDecryptionUtil.encryptString(String.valueOf(merchantLogin.getId()));
			//String marchantId = String.valueOf(merchantLogin.getMerchant().getId());
			String timeLog = String.valueOf(System.currentTimeMillis());
			Message message = new MimeMessage(
					SendMailProperty.gmailMailProperty(IConstant.FROM_EMAIL_ID, IConstant.FROM_PASSWORD));
			message.setFrom(new InternetAddress(IConstant.FROM_EMAIL_ID));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(merchantLogin.getEmailId()));
			message.setSubject("Regards:Forgot password");
			/*String msg = "Dear " + customer.getFirstName() + "<br>";
			msg += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;We have received a request to reset your password .Please find your reset password link below::<br>";
			msg += " <br>";
			msg += " <br>";
			msg += "Click to : " + environment.getProperty("WEB_BASE_URL") + "/changepassword?email=" + customer.getEmailId()
					+ "&merchantId=" + marchantId + "&userId=" + cusID + "&tLog="
					+ EncryptionDecryptionUtil.encryption(timeLog) + "<br>";
			msg += " <br>";
			msg += " <br>";
			msg += " <br>";
			msg += "<b>Regards,</b><br>";
			msg += "FoodKonnekt";*/
			
			String merchantLogo="";
			//if (merchantLogin.getMerchant().getMerchantLogo() == null) {
                merchantLogo = "http://www.dev.foodkonnekt.com/app/foodnew.jpg";
            /*} else {
                merchantLogo = UrlConstant.BASE_PORT + customer.getMerchantt().getMerchantLogo();
            }*/
			
			String msg = "<div style='width: 700px; height: auto; border: solid 1px #ddd; box-shadow: 0px 0px 6px #ddd; background-color: #f2f2f2; text-align: center; margin: 40px 0px 0px 35px;'>"
					+ "<div style='width: 700px; height: 85px; padding: 20px 0px; text-align: center;'>&nbsp;<img src='"
					+ merchantLogo + "' height='80' /></div>"
					+ "<div style='background-color: #fff; padding: 20px 0px 0px 0px; width: 90%; margin: 0 auto 65px auto;'>"
					+ "<p style='font-size: 20px; color: #e6880f;'>Dear " + merchantLogin.getMerchant().getName() + ",</p>"
					+ "<p style='font-size: 14px;text-align: left;padding-left:10px;'>We have received a request to reset your password .Please find your reset password link below: &nbsp;</p>";
			msg = msg+"<br /><br /> <a style='text-decoration: none; margin: 16px 0px; display: inline-block; background-color: #f8981d; color: #fff; padding: 10px 12px; border-radius: 10px; font-weight: 600; font-size: 15px;' href='"; 
					
					Integer merchantId = Integer.parseInt(marchantId);
			
					/*if(merchantId.equals(IConstant.TEXAN_REWARDS_MERCHANT_ID)){
						msg = msg+ environment.getProperty("TEXAN_BASE_URL") + "/changepassword?email=" + merchantLogin.getEmailId()
								+ "&merchantId=" + marchantId + "&tLog=" 
								+ EncryptionDecryptionUtil.encryption(timeLog);
					}else{
						msg = msg+ environment.getProperty("WEB_BASE_URL") + "/changepassword?email=" + merchantLogin.getEmailId()
								+ "&merchantId=" + marchantId + "&tLog=" 
								+ EncryptionDecryptionUtil.encryption(timeLog);
					}*/
			
			msg = msg+ environment.getProperty("WEB_BASE_URL") + "/changepassword?email=" + merchantLogin.getEmailId()
					+ "&merchantId=" + marchantId + "&tLog=" 
					+ EncryptionDecryptionUtil.encryption(timeLog);
			
					msg= msg+ "' target='_blank'>Click here</a><br /> Note: Link will valid till next 30 min only .";

			msg = msg
					+ "<p><Order details: items, extras, price, fees, promo discount, total $><br>For any further questions, ";
			/*if(customer.getMerchantt().getPhoneNumber()!=null) { 
				msg = msg +"Please call us at " + customer.getMerchantt().getPhoneNumber() + "";}
			else{
				msg = msg +"Please call our store";
			}*/
					msg = msg +" <br><br>Regards, <br> FoodKonnekt <br><br>";
			msg = msg + "</div>";
			msg = msg + "<p>We appreciate your business with us!</p>";
			/*if( merchantId!=null && merchantId.equals(IConstant.TEXAN_REWARDS_MERCHANT_ID) ){
				msg = msg + "<div style='height: 70px; text-align: center;'>&nbsp;<img src='https://www.foodkonnekt.com/foodkonnekt_merchat_logo/274_PiePizzaria.png' alt='' width='200' height='63' /></div>"
						+ "</div>";
			}else{*/
				msg = msg + "<div style='height: 70px; text-align: center;'>&nbsp;<img src='http://www.dev.foodkonnekt.com/app/foodnew.jpg' alt='' width='200' height='63' /></div>"
					+ "</div>";
			//}
			msg = msg + "<p style='color: #676767; font-size: 11px;'>Automated email. Please do not reply to this sender email.</p>";
			
			
			message.setContent(msg, "text/html");
			Transport.send(message);
		} catch (MessagingException e) {
			LOGGER.error("===============  MailSendUtil : Inside forgotMerchantPasswordEmail :: Exception  ============= " + e);

			throw new RuntimeException(e);
		}
		LOGGER.info("===============  MailSendUtil : Inside forgotMerchantPasswordEmail :: End  ============= ");

		return null;
	}
	
	public static String customerRegistartionConfirmation(Customer customer, String logoImage,String merchantName) {
		try {
			LOGGER.info("===============  MailSendUtil : Inside customerRegistartionConfirmation :: Start  =============merchantName "+merchantName);

			Message message = new MimeMessage(
					SendMailProperty.gmailMailProperty(IConstant.FROM_EMAIL_ID, IConstant.FROM_PASSWORD));
			message.setFrom(new InternetAddress(IConstant.FROM_EMAIL_ID));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(customer.getEmailId()));
			message.setSubject("Thank you for registering");
			String msg = "<div style='width: 700px; height: auto; border: solid 1px #ddd; box-shadow: 0px 0px 6px #ddd; background-color: #f2f2f2; text-align: center; margin: 40px 0px 0px 35px;'><div style='width: 700px; height: 85px; padding: 20px 0px; text-align: center;'>&nbsp;<img src='"
					+ logoImage
					+ "' height='80' /></div><div style='background-color: #fff; padding: 20px 0px 0px 0px; width: 90%; margin: 0 auto 65px auto;'><p style='font-size: 20px; color: #e6880f;'>Dear ";
					if(customer.getFirstName()!= null) {
						msg +=  customer.getFirstName() + " ";
					}
					if(customer.getLastName()!= null) {
						msg += customer.getLastName()  ;
					}
					msg+= ",</p><p style='font-size: 14px;'>Thank you for registering. Your account has been created.<br /> Please use the login details below to come back and order with us in future.</p><div style='text-align: left; padding: 5px 20px;'><table><tbody><tr><td width='90'>Name</td><td>:</td><td>";
							if(customer.getFirstName()!= null) {
								msg +=  customer.getFirstName() + " ";
							}
							if(customer.getLastName()!= null) {
								msg += customer.getLastName()  ;
							} 
					 msg +="</td></tr><tr><td>Email</td><td>:</td><td>" + customer.getEmailId()
					+ "</td></tr><tr><td>Phone</td><td>:</td><td>" + customer.getPhoneNumber()
					+ "</td></tr></tbody></table><br /><br /><p>Thank you. We appreciate your business with us!</p>Regards,<br />"+merchantName+"</div>";
			/*if( customer.getMerchantId()!=null &&  customer.getMerchantId().equals(IConstant.TEXAN_REWARDS_MERCHANT_ID) ){
				msg = msg + "<div style='height: 70px; text-align: center;'>&nbsp;<img src='https://www.foodkonnekt.com/foodkonnekt_merchat_logo/274_PiePizzaria.png' alt='' width='200' height='63' /></div>"
						+ "</div>";
			}else{*/
				msg = msg +"<div style='height: 70px; text-align: center;'>&nbsp;<img src='http://www.dev.foodkonnekt.com/app/foodnew.jpg' alt='' width='200' height='63' /></div></div>";
			//}
			
			msg = msg +"<p style='color: #676767; font-size: 11px;'>Automated email. Please do not reply to this sender email.</p></div>'";
				message.setContent(msg, "text/html");
			Transport.send(message);
		} catch (MessagingException e) {
			LOGGER.error("===============  MailSendUtil : Inside customerRegistartionConfirmation :: Exception  ============= " + e);

			throw new RuntimeException(e);
		}
		LOGGER.info("===============  MailSendUtil : Inside customerRegistartionConfirmation :: End  ============= ");

		return null;
	}
	
	public static String productInstallationMail(Merchant merchant,String posName,String status,Environment environment) {
		try {
			LOGGER.info("===============  MailSendUtil : Inside productInstallationMail :: Start  =============posName "+posName+" status "+status);
			if(!environment.getProperty("FOODKONNEKT_APP_TYPE").equals("Local")){
			if(merchant!=null){
				String merchantName="NA";
				String address="NA";
				String subscription="NA";
				String website="NA";
				String phoneNo="NA";
				String emailid="NA";
				if(merchant.getName()!=null && !merchant.getName().isEmpty()){
					merchantName=merchant.getName();
				}
				if(merchant.getPhoneNumber()!=null && !merchant.getPhoneNumber().isEmpty()){
					phoneNo=merchant.getPhoneNumber();
				}
				if(merchant.getOwner()!=null && merchant.getOwner().getEmail()!=null && !merchant.getOwner().getEmail().isEmpty()){
					emailid=merchant.getOwner().getEmail();
				}
				
				if(merchant.getMerchantSubscriptions()!=null &&  merchant.getMerchantSubscriptions().size()> 0){
					for(MerchantSubscription merchantSubscription :merchant.getMerchantSubscriptions()){
						
						Subscription subs = merchantSubscription.getSubscription();
						if(subs!=null){
							if(subs.getSubscriptionPlan()!=null)
								subscription=subs.getSubscriptionPlan();
						}
					}
				}
				
				
				if(merchant.getAddresses()!=null && merchant.getAddresses().size()> 0 ){
					for(Address address2:merchant.getAddresses()){
						if(address2.getAddress1()!=null && !address2.getAddress1().isEmpty())
							address=address2.getAddress1();
							
					   if(address2.getAddress2()!=null && !address2.getAddress2().isEmpty())	{
						   if(address.equals("NA"))
							   address="";
						   
						   address=address+" "+address2.getAddress2();
					   }
					   
						   if(address2.getAddress3()!=null && !address2.getAddress3().isEmpty()){
							   if(address.equals("NA"))
								   address="";
							   address=address+" "+address2.getAddress3();
						   }
						   
                          if(address2.getCity()!=null && !address2.getCity().isEmpty())
							   
							   address=address+" ,"+address2.getCity();
                          
                          if(address2.getState()!=null && !address2.getState().isEmpty())
							   
							   address=address+" ,"+address2.getState();
                          
                          if(address2.getZip()!=null && !address2.getZip().isEmpty())
							   
							   address=address+" ,"+address2.getZip();
                          
                          if(address2.getCountry()!=null && !address2.getCountry().isEmpty())
							   
							   address=address+" "+address2.getCountry();
                          
					}
					
				}
			
			Message message = new MimeMessage(
					SendMailProperty.gmailMailProperty(IConstant.FROM_EMAIL_ID, IConstant.FROM_PASSWORD));
			message.setFrom(new InternetAddress(IConstant.FROM_EMAIL_ID));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse("support@foodkonnekt.com"));
			message.setSubject("FoodKonnekt App "+status+" From "+posName);
			String msg = "<div style='width: 700px; height: auto; border: solid 1px #ddd; box-shadow: 0px 0px 6px #ddd; background-color: #f2f2f2; text-align: center; margin: 40px 0px 0px 35px;'><div style='width: 700px; height: 85px; padding: 20px 0px; text-align: center;'>&nbsp;<img src='http://www.dev.foodkonnekt.com/app/foodnew.jpg' height='80' /></div><div style='background-color: #fff; padding: 20px 0px 0px 0px; width: 90%; margin: 0 auto 65px auto;'><p style='font-size: 20px; color: #e6880f;'>Dear FoodKonnekt,</p><p style='font-size: 14px;'>You got a new installation.Merchant detail is as below :-</p><div style='text-align: left; padding: 5px 20px;'><table><tbody>"
					+ "<tr><td width='90'>Name</td><td>:</td>"
					+"<td>"+ merchantName + "</td>"
					+ "</tr><tr><td>Email</td><td>:</td><td>" + emailid+ "</td></tr>"
					+ "<tr><td>Phone</td><td>:</td><td>" + phoneNo+ "</td></tr>"
					+ "<tr><td>Subscription</td><td>:</td><td>" + subscription+ "</td></tr>"
					+ "<tr><td>Web Site</td><td>:</td><td>" + website+ "</td></tr>"
					+ "<tr><td>Address</td><td>:</td><td>" + address+ "</td></tr>"
					+ "</tbody></table><br /><br /><p>Thank you. We appreciate your business with us!</p>Regards,<br />FoodKonnekt</div>";
			/*if( merchant.getId()!=null &&  merchant.getId().equals(IConstant.TEXAN_REWARDS_MERCHANT_ID) ){
				msg = msg + "<div style='height: 70px; text-align: center;'>&nbsp;<img src='https://www.foodkonnekt.com/foodkonnekt_merchat_logo/274_PiePizzaria.png' alt='' width='200' height='63' /></div>"
						+ "</div>";
			}else{*/
				msg = msg +"<div style='height: 70px; text-align: center;'>&nbsp;<img src='http://www.dev.foodkonnekt.com/app/foodnew.jpg' alt='' width='200' height='63' /></div></div>";
			
			//}
			msg = msg +"<p style='color: #676767; font-size: 11px;'>Automated email. Please do not reply to this sender email.</p></div>'";
				message.setContent(msg, "text/html");
			Transport.send(message);
			}}
		} catch (MessagingException e) {
			LOGGER.error("===============  MailSendUtil : Inside productInstallationMail :: Exception  ============= " + e);

			throw new RuntimeException(e);
		}
		LOGGER.info("===============  MailSendUtil : Inside productInstallationMail :: End  ============= ");

		return null;
	}

	public void placeOrderMail(String name, String paymentYtpe, String orderPosId, String subTotal, String tax,
			String orderDetails, String email, Double orderPrice, String note, String merchantName, String merchantLogo,
			String orderType, String convenienceFeeValue, String avgTime, String deliverFee, Double orderDiscount,Double tipAmount,Integer custId,
			Integer orderId, Integer posTypeId,String orderCoupon, Integer merchantId,Integer isFutureOrder,String fullfilledOn,boolean storeOpenstatus) {
		try {
			
			MerchantConfiguration merchantConfiguration = merchantConfigurationRepository.findByMerchantId(merchantId);
			if(merchantConfiguration != null && merchantConfiguration.getEnableNotification() != null && (merchantConfiguration.getEnableNotification() == true ||(merchantConfiguration.getAutoAcceptOrder()==true && isFutureOrder==1) )){
				LOGGER.info("----------------Start :: MailSendUtil : placeOrderMail------------------");
				LOGGER.info("MailSendUtil :: placeOrderMail : orderId "+orderId);
				String order = (orderType.equalsIgnoreCase("pickUp")) ? "Pick up" : "Delivery";
				if(orderId != null && custId != null) {
				LOGGER.info(" EncryptionDecryptionUtil.encryption(orderId.toString()) : "+EncryptionDecryptionUtil.encryption(orderId.toString()));
				LOGGER.info(" EncryptionDecryptionUtil.encryption(custId.toString()) : "+EncryptionDecryptionUtil.encryption(custId.toString()));
				}
				Message message = new MimeMessage(
						SendMailProperty.gmailMailProperty(IConstant.FROM_EMAIL_ID, IConstant.FROM_PASSWORD));
				message.setFrom(new InternetAddress(IConstant.FROM_EMAIL_ID));
				message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email));
				message.setSubject("Your order has been received");
				String msg = "<div style='width: 700px; height: auto; border: solid 1px #ddd; box-shadow: 0px 0px 6px #ddd; background-color: #f2f2f2; text-align: center; margin: 40px 0px 0px 35px;'>"
						+ "<div style='width: 700px; height: 85px; padding: 20px 0px; text-align: center;'>&nbsp;<img src='"
						+ merchantLogo + "' height='80' /></div>"
						+ "<div style='background-color: #fff; padding: 20px 0px 0px 0px; width: 90%; margin: 0 auto 65px auto;'>"
						+ "<p style='font-size: 20px; color: #e6880f;'>Dear " + name + ",</p>"
						+ "<p style='font-size: 14px;'>Thank you. We have received your order and "; 
				        msg = (storeOpenstatus) ? msg + "are in the process of&nbsp;confirming it.&nbsp;" : msg + "will be confirmed once the restaurant opens.";
						msg = msg + "</p><div style='text-align: left; padding: 20px 20px;'>" + "<table>" + "<tbody>" + "<tr>"
						+ "<td>Order ID</td>" + "<td>:</td>" + "<td>" + orderPosId + "</td>" + "</tr>" + "<tr>"
						+ "<td width='230'>Order type</td>" + "<td>:</td>" + "<td>" + orderType + "</td>" + "</tr>"+ "<tr>"
						+ "<td width='230'>" +order+ " time</td>" + "<td>:</td>" + "<td>" + fullfilledOn + "</td>" + "</tr>" + "<tr>"
						+ "<td>Payment</td>" + "<td>:</td>" + "<td>" + paymentYtpe + "</td>" + "</tr>" + "</tbody>"
						+ "</table>" + "<p><strong>Order Details:</strong><br /></p>" + orderDetails + "</p>"
						+ "<p style='margin: 0;'>-------------------------------------------------------------------------------</p>"
						+ "<table>" + "<tbody>" + "<tr>" + "<td width='230'>Sub-total</td>" + "<td>&nbsp;</td>" + "<td>$"
						/*CHANGE Convenience to Online*/
						/*+ subTotal + "</td>" + "</tr>" + "<tr>" + "<td>Convenience Fee</td>" + "<td>&nbsp;</td>" + "<td>$"*/
						+  String.format("%.2f", Float.parseFloat(subTotal))+ "</td>" + "</tr>" + "<tr>" + "<td>Online Fee</td>" + "<td>&nbsp;</td>" + "<td>$"
						+  String.format("%.2f", Float.parseFloat(convenienceFeeValue))+ "</td>" + "</tr>";
				if (orderType.toLowerCase().equals("delivery") && Integer.parseInt(deliverFee)>0) {
					msg = msg + "<tr>" + "<td>Delivery Fee</td>" + "<td>&nbsp;</td>" + "<td>$" + String.format("%.2f", Float.parseFloat(deliverFee)) + "</td>"
							+ "</tr>";
				}
				
					msg = msg + "<tr>" + "<td>Tax</td>" + "<td>&nbsp;</td>" + "<td>$" +  tax + "</td>"
							+ "</tr>";
				
				if (orderDiscount != null && orderDiscount > 0) {
					msg = msg + "<tr>" + "<td>Discount</td>" + "<td>&nbsp;</td>"+ "<td>$" +  String.format("%.2f", orderDiscount)+ "</td>" + "</tr>"+orderCoupon;
				}else if(orderCoupon!=null && !orderCoupon.isEmpty() && Double.parseDouble(deliverFee)>0){
				    msg = msg + "<tr>" + "<td>Discount</td>" + "<td>&nbsp;</td>"+ "<td>$" + deliverFee + "</td>" + "</tr>"+orderCoupon;
				}
				String orderRecUrl="#";
				if(posTypeId!=null){
					
					if(custId!=null && orderId!=null){
							orderRecUrl=environment.getProperty("WEB_BASE_URL")+"/orderRec?orderid="+EncryptionDecryptionUtil.encryption(orderId.toString())+"==&customerid="
									+EncryptionDecryptionUtil.encryption(custId.toString());
					}
			}
				
				if (tipAmount!=null && tipAmount>0) {
					msg = msg	+ "<tr>" + "<td>Tip</td>" + "<td>&nbsp;</td>" + "<td>$" + String.format("%.2f", tipAmount) + "</td>" + "</tr>";
				}
				
				msg = msg	+  "</tbody>" + "</table>"
						+ "<p style='margin: 0;'>-------------------------------------------------------------------------------</p>"
						+ "<table>" + "<tbody>" + "<tr>" + "<td width='230'>Total</td>" + "<td>&nbsp;</td>" + "<td>$"
						+ String.format("%.2f", orderPrice) + "</td>" + "</tr>" + "</tbody>" + "</table>"
						+ "<br /><br /> <strong style='text-decoration: underline;'>Special Instructions :</strong> " + note
						+ " <br /><br /><br /> <a style='text-decoration: none; margin: 16px 0px; display: inline-block; background-color: #f8981d; color: #fff; padding: 10px 12px; border-radius: 10px; font-weight: 600; font-size: 15px;' href='"+orderRecUrl+"'"
								+ " target='_blank'>View Online Receipt</a>"
						+"<a style='text-decoration: none; margin: 16px 7px; display: inline-block; background-color: #f8981d; color: #fff; padding: 10px 12px; border-radius: 10px; font-weight: 600; font-size: 15px;' href='"+environment.getProperty("WEB_KRITIQ_BASE_URL")+"/customerFeedbackV2?customerId="+EncryptionDecryptionUtil.encryption(custId.toString())+"&orderId="+EncryptionDecryptionUtil.encryption(orderId.toString())+"'"
								+ " target='_blank'>Share Your Feedback</a>"
						+"<br />"
						
						+"<br /><br /> Regards,<br />"
						+ merchantName + "</div>"
						+ "<p>Lookout for an update from us on your order confirmation.<br />We appreciate your business with us!</p>";
	        	
						msg = msg + "<div style='height: 70px; text-align: center;'>&nbsp;<img src='http://www.dev.foodkonnekt.com/app/foodnew.jpg' alt='' width='200' height='63' /></div>"
								+ "</div>";
	        
						msg = msg	+ "<p style='color: #676767; font-size: 11px;'>Automated email. Please do not reply to this sender email.</p>"
						+ "</div>";
						message.setContent(msg, "text/html");
						Transport.send(message);
						LOGGER.error("MailSendUtil :: placeOrderMail : mail send successfully");
				}
			} catch (MessagingException e) {
				
			}
			LOGGER.info("----------------End :: MailSendUtil : placeOrderMail------------------");
		}
	
	public static void failOrderMail(String name, String paymentYtpe, String orderPosId, String subTotal, String tax,
			String orderDetails, String email, Double orderPrice, String note, String merchantName, String merchantLogo,
			String orderType, String convenienceFeeValue, String avgTime, String deliverFee, Double orderDiscount,Double tipAmount,String reason,String menuLink,
			Integer merchantId,Environment environment) {
		try {
			LOGGER.info("----------------Start :: MailSendUtil : failOrderMail------------------");
			LOGGER.info("MailSendUtil :: failOrderMail : orderId "+orderPosId);
			Message message = new MimeMessage(
					SendMailProperty.gmailMailProperty(IConstant.FROM_EMAIL_ID, IConstant.FROM_PASSWORD));
			message.setFrom(new InternetAddress(IConstant.FROM_EMAIL_ID));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email));
			message.addRecipient(Message.RecipientType.CC, new InternetAddress("support@foodkonnekt.com"));
			
			message.setSubject("Order Failed: "+merchantName);
			String msg = "<div style='width: 700px; height: auto; border: solid 1px #ddd; box-shadow: 0px 0px 6px #ddd; background-color: #f2f2f2; text-align: center; margin: 40px 0px 0px 35px;'>"
					+ "<div style='width: 700px; height: 85px; padding: 20px 0px; text-align: center;'>&nbsp;<img src='"
					+ merchantLogo + "' height='80' /></div>"
					+ "<div style='background-color: #fff; padding: 20px 0px 0px 0px; width: 90%; margin: 0 auto 65px auto;'>"
					+ "<p style='font-size: 20px; color: #e6880f;' align='left'>Dear " + name + ",</p>"
					+ "<p style='font-size: 14px;' align='left'>We could not process the order due to card failure and we apologize about this.Please retry your order by clicking this link <a href="+environment.getProperty("WEB_BASE_URL")+"/orderV2?merchantId="+merchantId+">Online Order</a>.</p>"
					+ "<p style='font-size: 14px;' align='left'>  If you have already placed this order again, please ignore this email.&nbsp;</p>"
					+ "<div style='text-align: left; padding: 20px 20px;'>" + "<table>" + "<tbody>" + "<tr>"
					+ "<td width='230'>Order type</td>" + "<td>:</td>" + "<td>" + orderType + "</td>" + "</tr>" + "<tr>"
					+ "<td>Order ID</td>" + "<td>:</td>" + "<td>" + orderPosId + "</td>" + "</tr>" + "<tr>"
					+ "<td>Payment</td>" + "<td>:</td>" + "<td>" + paymentYtpe + "</td>" + "</tr>" + "</tbody>"
					+ "</table>" + "<p><strong>Order Details:</strong><br /></p>" + orderDetails + "</p>"
					+ "<p style='margin: 0;'>-------------------------------------------------------------------------------</p>"
					+ "<table>" + "<tbody>" + "<tr>" + "<td width='230'>Sub-total</td>" + "<td>&nbsp;</td>" + "<td>$"
					/*CHANGE Convenience to Online*/
					/*+ subTotal + "</td>" + "</tr>" + "<tr>" + "<td>Convenience Fee</td>" + "<td>&nbsp;</td>" + "<td>$"*/
					+ subTotal + "</td>" + "</tr>" + "<tr>" + "<td>Online Fee</td>" + "<td>&nbsp;</td>" + "<td>$"
					+ convenienceFeeValue + "</td>" + "</tr>";
			if (orderType.toLowerCase().equals("delivery")) {
				msg = msg + "<tr>" + "<td>Delivery Fee</td>" + "<td>&nbsp;</td>" + "<td>$" + deliverFee + "</td>"
						+ "</tr>";

			}
			
			if (tipAmount!=null && tipAmount>0) {
				msg = msg + "<tr>" + "<td>Tip</td>" + "<td>&nbsp;</td>" + "<td>$" + tipAmount + "</td>"
						+ "</tr>";

			}
			/*msg = msg + "<tr>" + "<td>Discount</td>" + "<td>&nbsp;</td>" + "<td>$" + orderDiscount + "</td>" + "</tr>"*/

        msg = msg		+ "<tr>" + "<td>Tax</td>" + "<td>&nbsp;</td>" + "<td>$" + tax + "</td>" + "</tr>" + "</tbody>"
					+ "</table>"
					+ "<p style='margin: 0;'>-------------------------------------------------------------------------------</p>"
					+ "<table>" + "<tbody>" + "<tr>" + "<td width='230'>Total</td>" + "<td>&nbsp;</td>" + "<td>$"
					+ orderPrice + "</td>" + "</tr>" + "</tbody>" + "</table>"
					+ "<br /><br /> <strong style='text-decoration: underline;'>Special Instructions :</strong> " + note
					+ " <br /><br /><br /> <br /> Regards,<br />"
					+ merchantName + "</div>"
					+ "<p>Lookout for an update from us on your order confirmation.<br />We appreciate your business with us!</p>";
					
  /*if( merchantId!=null && merchantId.equals(IConstant.TEXAN_REWARDS_MERCHANT_ID) ){
		msg = msg + "<div style='height: 70px; text-align: center;'>&nbsp;<img src='https://www.foodkonnekt.com/foodkonnekt_merchat_logo/274_PiePizzaria.png' alt='' width='200' height='63' /></div>"
				+ "</div>";
	}else{*/
		msg = msg + "<div style='height: 70px; text-align: center;'>&nbsp;<img src='http://www.dev.foodkonnekt.com/app/foodnew.jpg' alt='' width='200' height='63' /></div>"
					+ "</div>";
	//}
        msg = msg + "<p style='color: #676767; font-size: 11px;'>Automated email. Please do not reply to this sender email.</p>"
					+ "</div>";
			message.setContent(msg, "text/html");
			Transport.send(message);
			LOGGER.info("MailSendUtil :: failOrderMail : mail send successfully");
		} catch (MessagingException e) {
			LOGGER.error("MailSendUtil :: failOrderMail : Exception "+e);
		}
		LOGGER.info("----------------End :: MailSendUtil : failOrderMail------------------");
	}

	public void sendConfirmMail(String name, String paymentYtpe, String orderPosId, String subTotal, String tax,
			String orderDetails, String email, Double orderPrice, String note, String merchantName, String merchantLogo,
			String orderType, String convenienceFee, String pickUpTimeValue, String deliveryFee,Double orderDiscount,Double tipAmount,
			Integer custId, Integer orderId, Integer posTypeId, String discountCoupon, Integer merchantId, PaymentGateWay  paymentGateWay ) {
		try {
			LOGGER.info("----------------Start :: MailSendUtil : sendConfirmMail------------------");
			
			LOGGER.info("MailSendUtil :: sendConfirmMail : orderPosId "+orderPosId);
			LOGGER.info("MailSendUtil :: sendConfirmMail : orderId "+orderId);
			if(orderId != null && custId != null) {
				LOGGER.info(" EncryptionDecryptionUtil.encryption(orderId.toString()) : "+EncryptionDecryptionUtil.encryption(orderId.toString()));
				LOGGER.info(" EncryptionDecryptionUtil.encryption(custId.toString()) : "+EncryptionDecryptionUtil.encryption(custId.toString()));
				}
			Message message = new MimeMessage(
					SendMailProperty.gmailMailProperty(IConstant.FROM_EMAIL_ID, IConstant.FROM_PASSWORD));
			message.setFrom(new InternetAddress(IConstant.FROM_EMAIL_ID));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email));
			String order = (orderType.equalsIgnoreCase("pickUp")) ? "Pick up" : "Delivery";
			
			MerchantConfiguration merchantConfiguration = merchantConfigurationRepository.findByMerchantId(merchantId);
			OrderR orderR = orderRepository.findOne(orderId);
			if(orderR!=null && orderR.getFulfilled_on()!=null) {
				DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm a");
				pickUpTimeValue = dateFormat.format(orderR.getFulfilled_on());
			}
			if(merchantConfiguration !=null && merchantConfiguration.getEnableNotification() !=null && merchantConfiguration.getEnableNotification() == false){
				message.setSubject(IConstant.subjectForAutoAcceptMail);
			}else{
				message.setSubject(IConstant.subjectForManualAcceptMail);
			}
			
			String msg = "<div style='width: 700px; height: auto; border: solid 1px #ddd; box-shadow: 0px 0px 6px #ddd; background-color: #f2f2f2; text-align: center; margin: 40px 0px 0px 35px;'>"
					+ "<div style='width: 700px; height: 85px; padding: 20px 0px; text-align: center;'>&nbsp;<img src='"
					+ merchantLogo + "' height='80' /></div>"
					+ "<div style='background-color: #fff; padding: 20px 0px 0px 0px; width: 90%; margin: 0 auto 65px auto;'>"
					+ "<p style='font-size: 20px; color: #e6880f;'>Dear " + name + ",</p>"
					+ "<p style='font-size: 14px;'>Thank You. Your order has been confirmed ";
			        if (orderR.getIsFutureOrder() == 0 && merchantConfiguration.getEnableNotification())
				    msg = msg + "and is getting ready! &nbsp;";
			        msg = msg + "</p><div style='text-align: left; padding: 20px 20px;'>" + "<table>" + "<tbody>";
			        if(orderPosId!=null)
					msg =msg + "<tr>"+ "<td>Order ID</td>" + "<td>:</td>" + "<td>" + orderPosId + "</td>" + "</tr>" ;
					
					msg =msg + "<tr><td width='230'>Order type</td>" + "<td>:</td>" + "<td>" + orderType + "</td>" + "</tr>"
							+ "<tr><td width='230'>"+ order +" time</td>" + "<td>:</td>" + "<td>" + pickUpTimeValue + "</td>" + "</tr>"
							+ "<tr> <td>Payment</td>" + "<td>:</td>" + "<td>" + paymentYtpe + "</td>" + "</tr>" + "</tbody>"
					+ "</table>" + "<p><strong>Order Details:</strong><br /></p>" + orderDetails + "</p>"
					+ "<p style='margin: 0;'>-------------------------------------------------------------------------------</p>"
					+ "<table>" + "<tbody>" + "<tr>" + "<td width='230'>Sub-total</td>" + "<td>&nbsp;</td>" + "<td>$"
					/*CHANGE Convenience to Online*/
					/*+ subTotal + "</td>" + "</tr>" + "<tr>" + "<td>Convenience Fee</td>" + "<td>&nbsp;</td>" + "<td>$"*/
					+ subTotal + "</td>" + "</tr>" + "<tr>" + "<td>Online Fee</td>" + "<td>&nbsp;</td>" + "<td>$"
					+ convenienceFee + "</td>" + "</tr>";
			if (orderType.toLowerCase().equals("delivery")) {
				msg = msg + "<tr>" + "<td>Delivery Fee</td>" + "<td>&nbsp;</td>" + "<td>$" + deliveryFee + "</td>"
						+ "</tr>";

			}
			if (tipAmount!=null && tipAmount>0) {
				msg = msg + "<tr>" + "<td>Tip</td>" + "<td>&nbsp;</td>" + "<td>$" + String.format("%.2f",tipAmount) + "</td>"
						+ "</tr>";

			}
			
			String orderRecUrl="#";
			if(posTypeId!=null){
				if(custId!=null && orderId!=null){
					/*if(merchantId.equals(IConstant.TEXAN_REWARDS_MERCHANT_ID)){
						orderRecUrl=environment.getProperty("TEXAN_BASE_URL")+"/orderRec?orderid="+EncryptionDecryptionUtil.encryption(orderId.toString())+"==&customerid="
									+EncryptionDecryptionUtil.encryption(custId.toString());
					}else{*/
						orderRecUrl=environment.getProperty("WEB_BASE_URL")+"/orderRec?orderid="+EncryptionDecryptionUtil.encryption(orderId.toString())+"==&customerid="
								+EncryptionDecryptionUtil.encryption(custId.toString());
					//}
				}
			}
			if(orderDiscount > 0){
			    msg = msg + "<tr>" + "<td>Discount</td>" + "<td>&nbsp;</td>" + "<td>$"+String.format("%.2f",orderDiscount)+"</td>" + "</tr>"+discountCoupon;
			}
			
			msg = msg 	+ "<tr>" + "<td>Tax</td>" + "<td>&nbsp;</td>" + "<td>$" + tax + "</td>" + "</tr>" + "</tbody>"
					+ "</table>"
					+ "<p style='margin: 0;'>-------------------------------------------------------------------------------</p>"
					+ "<table>" + "<tbody>" + "<tr>" + "<td width='230'>Total</td>" + "<td>&nbsp;</td>" + "<td>$"
					+ String.format("%.2f",orderPrice) + "</td>" + "</tr>" + "</tbody>" + "</table>"
					+ "<br /><br /> <strong style='text-decoration: underline;'>Special Instructions :</strong> " + note
					+ " <br /><br /><br /> <a style='text-decoration: none; margin: 16px 0px; display: inline-block; background-color: #f8981d; color: #fff; padding: 10px 12px; border-radius: 10px; font-weight: 600; font-size: 15px;' "
					+ "href='"+orderRecUrl+"' target='_blank'>View Online Receipt</a>"
					+ "<a style='text-decoration: none; margin: 16px 7px; display: inline-block; background-color: #f8981d; color: #fff; padding: 10px 12px; border-radius: 10px; font-weight: 600; font-size: 15px;' href='"+environment.getProperty("WEB_KRITIQ_BASE_URL")+"/customerFeedbackV2?customerId="+EncryptionDecryptionUtil.encryption(custId.toString())+"&orderId="+EncryptionDecryptionUtil.encryption(orderId.toString())+"'"
					+ "target='_blank'>Share Your Feedback</a>";
			
			
			
//				if(orderR != null && orderR.getIsFutureOrder() != null && orderR.getIsFutureOrder() == 0){
//					msg = msg	+ "<br /> Thank you for your order! It'll be ready for "
//						+ orderType + " within the next " + pickUpTimeValue
//						+ " minutes. See you soon.<br />  ";
//				}
					
					if(paymentGateWay!=null && paymentGateWay.getIsMkonnektAccount() != null && paymentGateWay.getIsMkonnektAccount()==1 && !paymentYtpe.equalsIgnoreCase("Cash"))
						msg=msg+"<br /> This charge will appear on your credit card statement as \"mKonnekt LLC\",<br />";
					
						msg=msg + "<br /> Regards,<br />" + merchantName + "</div>";
					
					 if(merchantConfiguration !=null && merchantConfiguration.getEnableNotification() !=null && merchantConfiguration.getEnableNotification() == true){
						 msg = msg + "<p>Lookout for an update from us on your order confirmation.<br />We appreciate your business with us!</p>";
					 }
					msg = msg + "<div style='height: 70px; text-align: center;'>&nbsp;<img src='http://www.dev.foodkonnekt.com/app/foodnew.jpg' alt='' width='200' height='63' /></div>"
					+ "</div>";
			 
			 msg = msg +"<p style='color: #676767; font-size: 11px;'>Automated email. Please do not reply to this sender email.</p>"
					 + "</div>";
			message.setContent(msg, "text/html");
			Transport.send(message);
			LOGGER.info("MailSendUtil :: sendConfirmMail : mail send successfully");
		} catch (MessagingException e) {
			LOGGER.error("MailSendUtil :: sendConfirmMail : Exception "+e);
			throw new RuntimeException(e);
		}
		LOGGER.info("----------------End :: MailSendUtil : sendConfirmMail------------------");
	}

	public static void sendOrderCancellationMail(String name, String orderPosId, String email, String merchantName,
			String merchantLogo, String merchantPhoneNo, String merchantEmail, String reason,Integer merchantId) {
		try {
			LOGGER.info("----------------Start :: MailSendUtil : sendOrderCancellationMail------------------");
			LOGGER.info("MailSendUtil :: sendOrderCancellationMail : merchantId "+merchantId);
			LOGGER.info("MailSendUtil :: sendOrderCancellationMail : orderId "+orderPosId);
			
			if(orderPosId==null){
				orderPosId="";
			}
			Message message = new MimeMessage(
					SendMailProperty.gmailMailProperty(IConstant.FROM_EMAIL_ID, IConstant.FROM_PASSWORD));
			message.setFrom(new InternetAddress(IConstant.FROM_EMAIL_ID));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email));
			message.setSubject("We are sorry! Your recent order has been cancelled");
			String msg = "<div style='width: 700px; height: auto; border: solid 1px #ddd; box-shadow: 0px 0px 6px #ddd; background-color: #f2f2f2; text-align: center; margin: 40px 0px 0px 35px;'>"
					+ "<div style='width: 700px; height: 85px; padding: 20px 0px; text-align: center;'>&nbsp;<img src='"
					+ merchantLogo + "' height='80' /></div>"
					+ "<div style='background-color: #fff; padding: 20px 0px 0px 0px; width: 90%; margin: 0 auto 65px auto;'>"
					+ "<p style='font-size: 20px; color: #e6880f;'>Dear " + name + ",</p>"
					+ "<p style='font-size: 14px;'>We regret to inform you that your recent order " + orderPosId
					+ " , has been declined by " + merchantName + " &nbsp;</p>";
			msg = msg + "<div style='text-align: left; padding: 20px 20px;'>";
			if (reason != null && !reason.equals("")) {

				msg = msg + "<p><strong>The reason for rejecting the order is:</strong><br /></p>";
				msg = msg + "<ul style='font-size:12px'>";
				String reasons[] = reason.split(",");
				for (String r : reasons) {
					if(r!=null && (!r.isEmpty()|| !r.equals("")||!r.equals(" ")) )
					msg = msg + "<li>" + r + "</li>";
				}
				msg = msg + "</ul>";

			}

			msg = msg
					+ "<p><Order details: items, extras, price, fees, promo discount, total $><br>If you paid by credit card, your payment will be refunded to you at the earliest.<br>For any further questions,";
			if(merchantPhoneNo!=null) { 
				msg = msg +"Please call us at" + merchantPhoneNo + "";}
			else{
				msg = msg +"Please call our store";
			}
			
			msg = msg + " <br><br>Regards,<br>" + merchantName
					+ "<br><br>";
			msg = msg + "</div>";
			msg = msg + "<p>We appreciate your business with us!</p>";
					
			/*if(merchantId.equals(IConstant.TEXAN_REWARDS_MERCHANT_ID)){
				msg = msg + "<div style='height: 70px; text-align: center;'>&nbsp;<img src='https://www.foodkonnekt.com/foodkonnekt_merchat_logo/274_PiePizzaria.png' alt='' width='200' height='63' /></div>";
			}else{*/
				msg = msg + "<div style='height: 70px; text-align: center;'>&nbsp;<img src='http://www.dev.foodkonnekt.com/app/foodnew.jpg' alt='' width='200' height='63' /></div>";
			//}
			
			msg = msg +  "</div>"
					+ "<p style='color: #676767; font-size: 11px;'>Automated email. Please do not reply to this sender email.</p>"
					+ "</div>";
			message.setContent(msg, "text/html");
			Transport.send(message);
			LOGGER.info("MailSendUtil :: sendOrderCancellationMail : mail send successfully");
		} catch (MessagingException e) {
			LOGGER.error("MailSendUtil :: sendOrderCancellationMail : Exception "+e);
			throw new RuntimeException(e);
		}
		LOGGER.info("----------------End :: MailSendUtil : sendOrderCancellationMail------------------");
	}

	public static boolean webhookMail(String subject,String json,Environment environment) {
		try {
			LOGGER.info("----------------Start :: MailSendUtil : webhookMail------------------");

			if(!environment.getProperty("FOODKONNEKT_APP_TYPE").equals("Local")){
			Message message = new MimeMessage(SendMailProperty.gmailMailProperty(IConstant.FROM_EMAIL_ID ,IConstant.FROM_PASSWORD));
			message.setFrom(new InternetAddress("orders@foodkonnekt.com"));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(environment.getProperty("MAIL_IDS")));
			message.setSubject(subject+environment.getProperty("FOODKONNEKT_APP_TYPE"));
			
			String msg = "Dear<br>";
			msg += json;
			msg += " <br>";
			msg += " <br>";
			msg += "<b>Regards,</b><br>";
			msg += "FoodKonnekt";
			message.setContent(msg, "text/html");
			Transport.send(message);
			}
		} catch (MessagingException e) {
			LOGGER.error("MailSendUtil :: webhookMail : Exception "+e);

			throw new RuntimeException(e);
		}
		LOGGER.info("----------------End :: MailSendUtil : webhookMail------------------");
		return true;
	}
	
	public static boolean orderFaildMail(String json,Environment environment) {
		try {
			LOGGER.info("----------------Start :: MailSendUtil : orderFailedMail------------------");
			if(!environment.getProperty("FOODKONNEKT_APP_TYPE").equals("Local")){

			LOGGER.info("MailSendUtil :: orderFailedMail : json "+json);	
			Message message = new MimeMessage(SendMailProperty.gmailMailProperty(IConstant.FROM_EMAIL_ID ,IConstant.FROM_PASSWORD));
			message.setFrom(new InternetAddress("orders@foodkonnekt.com"));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(environment.getProperty("MAIL_IDS")));
			
			message.setSubject("Order Failed from "+environment.getProperty("FOODKONNEKT_APP_TYPE"));
			String msg = "Dear<br>";
			msg += json;
			msg += " <br>";
			msg += " <br>";
			msg += "<b>Regards,</b><br>";
			msg += "FoodKonnekt";
			message.setContent(msg, "text/html");
			Transport.send(message);
			LOGGER.info("MailSendUtil :: orderFailedMail : mail send successfully");
			}
		} catch (MessagingException e) {
			LOGGER.error("MailSendUtil :: orderFailedMail : Exception "+e);
			throw new RuntimeException(e);
		}
		LOGGER.info("----------------End :: MailSendUtil : orderFailedMail------------------");
		return true;
	}

	public static boolean sendErrorMailToAdmin(String exception,Environment environment) {
		
		if (!environment.getProperty("FOODKONNEKT_APP_TYPE").equals("Local")) {
			LOGGER.info("----------------Start :: MailSendUtil : sendErrorMailToAdmin------------------");
			try {
				Message message = new MimeMessage(
						SendMailProperty.gmailMailProperty(
								IConstant.FROM_EMAIL_ID,
								IConstant.FROM_PASSWORD));
				message.setFrom(new InternetAddress(IConstant.FROM_EMAIL_ID));

				message.setRecipients(Message.RecipientType.TO,InternetAddress.parse(IConstant.EXCEPTION_EMAIL_ID));
				
				message.setSubject("Exception from "+ environment.getProperty("FOODKONNEKT_APP_TYPE"));
				String msg = "";
				msg += exception;
				msg += " <br>";
				msg += " <br>";
				msg += "<b>Regards,</b><br>";
				msg += "FoodKonnekt";
				message.setContent(msg, "text/html");
				Transport.send(message);
				LOGGER.info("MailSendUtil :: sendErrorMailToAdmin : mail send successfully");
			} catch (MessagingException e) {
				LOGGER.error("MailSendUtil :: sendErrorMailToAdmin : Exception "+e);
				//throw new RuntimeException(e);
			}
		}
		LOGGER.info("----------------End :: MailSendUtil : sendErrorMailToAdmin------------------");
		return true;
	}

	public static void sendExceptionByMail(Exception e,Environment environment) {
		StringWriter errors = new StringWriter();
		e.printStackTrace(new PrintWriter(errors));
		MailSendUtil.sendErrorMailToAdmin(errors.toString(),environment);
	}
	
	public static void sendExceptionByMail(Exception e,Merchant merchant,Environment environment) {
		StringWriter errors = new StringWriter();
		e.printStackTrace(new PrintWriter(errors));
		if(merchant!=null && merchant.getName()!=null)
		MailSendUtil.sendErrorMailToAdmin("MerchantName"+merchant.getName()+","+errors.toString(),environment);
		else
		MailSendUtil.sendErrorMailToAdmin(errors.toString(),environment);
	}

	public static void sendExceptionByMail(Exception e,Merchant merchant,String errorMsg,Environment environment) {
		StringWriter errors = new StringWriter();
		e.printStackTrace(new PrintWriter(errors));
		if(merchant!=null && merchant.getName()!=null)
		MailSendUtil.sendErrorMailToAdmin(errorMsg+"     MerchantName "+merchant.getName()+","+errors.toString(),environment);
		else
		MailSendUtil.sendErrorMailToAdmin(errors.toString(),environment);
	}

	public static String sendUpdatedTime(OrderR orderR, String reason, String time,Environment environment) {
		LOGGER.info("----------------Start :: MailSendUtil : sendUpdateTime------------------");
		Customer customer = orderR.getCustomer();
		try {
			String cusID = EncryptionDecryptionUtil.encryptString(String.valueOf(customer.getId()));
			String marchantId = String.valueOf(customer.getMerchantt().getId());
			String timeLog = String.valueOf(System.currentTimeMillis());

			LOGGER.info("MailSendUtil :: sendUpdatedTime : merchantId "+marchantId);
			LOGGER.info("MailSendUtil :: sendUpdatedTime : customerId "+EncryptionDecryptionUtil.decryption(cusID));
			
			Message message = new MimeMessage(
					SendMailProperty.gmailMailProperty(IConstant.FROM_EMAIL_ID, IConstant.FROM_PASSWORD));
			message.setFrom(new InternetAddress(IConstant.FROM_EMAIL_ID));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(customer.getEmailId()));
			message.setSubject("Order Time Updated");
			String merchantLogo="";
			if (customer.getMerchantt().getMerchantLogo() == null) {
                merchantLogo = "http://www.dev.foodkonnekt.com/app/foodnew.jpg";
            } else {
                merchantLogo = environment.getProperty("BASE_PORT") + customer.getMerchantt().getMerchantLogo();
            }
			/*String msg = "Dear " + customer.getFirstName() + "<br>";
			msg += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Thank you for placing the order with us but due to " + reason
					+ " your order would be ready to be \"picked up\"/delivered at<br>";
			msg += " <br>";
			msg += " <br>";
			msg += "New Time : " + time
					+ "We sincerely apologize about the inconvenience caused. Please call us at \"number\" if you have any questions.\r\n"
					+ "\r\n" + "We look forward to serving you soon" + "<br>";
			msg += " <br>";
			msg += " <br>";
			msg += " <br>";
			msg += "<b>Vendor Name,</b><br>";
			msg += "FoodKonnekt";*/
			String msg = "<div style='width: 700px; height: auto; border: solid 1px #ddd; box-shadow: 0px 0px 6px #ddd; background-color: #f2f2f2; text-align: center; margin: 40px 0px 0px 35px;'>"
					+ "<div style='width: 700px; height: 85px; padding: 20px 0px; text-align: center;'>&nbsp;<img src='"
					+ merchantLogo + "' height='80' /></div>"
					+ "<div style='background-color: #fff; padding: 20px 0px 0px 0px; width: 90%; margin: 0 auto 65px auto;'>"
					+ "<p style='font-size: 20px; color: #e6880f;'>Dear ";
					if(customer.getFirstName()!= null) {
						msg +=  customer.getFirstName() + " ";
					}
					if(customer.getLastName()!= null) {
						msg += customer.getLastName()  ;
					} 
			msg += ",</p><p style='font-size: 14px;text-align: left;padding-left:10px;'>Thank you for placing the order "+ orderR.getId() +" with us. Your order would be ready to be ";
			if (orderR.getOrderType().toLowerCase().equals("delivery")) {
				msg = msg+"delivered";	
			}else{
				msg = msg+"picked up";	
			}
			
			
			msg = msg  +" within "+time+" mins. <br> <br>We sincerely apologize about the inconvenience caused. We look forward to serving you soon. &nbsp;</p>";
			

			msg = msg
					+ "<p><Order details: items, extras, price, fees, promo discount, total $><br>For any further questions, ";
			if(customer.getMerchantt().getPhoneNumber()!=null) { 
				msg = msg +"Please call us at " + customer.getMerchantt().getPhoneNumber() + "";}
			else{
				msg = msg +"Please call our store";
			}
					msg = msg +" <br><br>Regards,<br>" + customer.getMerchantt().getName()
					+ "<br><br>";
			msg = msg + "</div>";
			msg = msg + "<p>We appreciate your business with us!</p>";
			/*if(orderR.getMerchant()!=null &&  orderR.getMerchant().getId().equals(IConstant.TEXAN_REWARDS_MERCHANT_ID)){
				msg = msg + "<div style='height: 70px; text-align: center;'>&nbsp;<img src='https://www.foodkonnekt.com/foodkonnekt_merchat_logo/274_PiePizzaria.png' alt='' width='200' height='63' /></div>"
						+ "</div>";
			}else{*/
				msg = msg + "<div style='height: 70px; text-align: center;'>&nbsp;<img src='http://www.dev.foodkonnekt.com/app/foodnew.jpg' alt='' width='200' height='63' /></div>"
					+ "</div>";
			//}
			msg = msg + "<p style='color: #676767; font-size: 11px;'>Automated email. Please do not reply to this sender email.</p>";
			message.setContent(msg, "text/html");
			Transport.send(message);
			LOGGER.info("MailSendUtil :: sendUpdatedTime : mail send successfully ");
		} catch (MessagingException e) {
			LOGGER.error("MailSendUtil :: sendUpdatedTime : Exception "+e);
			throw new RuntimeException(e);
		}
		LOGGER.info("----------------End :: MailSendUtil : sendUpdateTime------------------");
		return null;
	}

	
	/*public static void sendOrderReceiptAndFeedbackEmail(OrderR order) {
		
	 try {
		String cusID = EncryptionDecryptionUtil.encryption(String.valueOf(order.getCustomer().getId()));
		String orderId = EncryptionDecryptionUtil.encryption(String.valueOf(order.getId()));
		
		
		Message message = new MimeMessage(
				SendMailProperty.mailProperty(IConstant.FROM_EMAIL_ID, IConstant.FROM_PASSWORD));
		message.setFrom(new InternetAddress(IConstant.FROM_EMAIL_ID));
		message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(order.getCustomer().getEmailId()));
		message.setSubject("Regards: Order Receipt and feedback");
		
		String merchantLogo="";
		if (order.getMerchant().getMerchantLogo() == null) {
            merchantLogo = "http://www.dev.foodkonnekt.com/app/foodnew.jpg";
        } else {
            merchantLogo = UrlConstant.BASE_PORT + order.getMerchant().getMerchantLogo();
        }
		
		String msg = "<div style='width: 700px; height: auto; border: solid 1px #ddd; box-shadow: 0px 0px 6px #ddd; background-color: #f2f2f2; text-align: center; margin: 40px 0px 0px 35px;'>"
				+ "<div style='width: 700px; height: 85px; padding: 20px 0px; text-align: center;'>&nbsp;<img src='"
				+ merchantLogo + "' height='80' /></div>";
		
		msg = msg+"<div style='background-color: #fff; padding: 20px 0px 0px 0px; width: 90%; margin: 0 auto 65px auto;'>"
				+ "<p style='font-size: 20px; color: #e6880f;'>Dear " + order.getCustomer().getFirstName() + ",</p>";
		
		msg = msg+"<br /> Thank You for your recent order and we hope that your experience has been pleasant."
						+ " In order for us to improve we are seeking feedback about your online ordering experience as well as about our food. Please find below a "
						+ "link for the survey (it would take around 2 min to complete this survey). As always we look forward to serving you soon. .";
		
			msg = msg+"<br/> <a 'font-size: 14px; none; text-align: left;padding-left:10px;' href ='" + UrlConstant.WEB_BASE_URL + "/feedbackForm?customerId=" + cusID
				+ "&orderId=" + orderId+ "' target='_blank'>Complete the Survey </a> ";
			msg = msg+" &nbsp;";
			msg = msg+" <a 'font-size: 14px; none; text-align: left;padding-left:10px;' href ='" + UrlConstant.WEB_BASE_URL + "/orderRec?orderid=" + orderId
					+ "&customerid=" + cusID+ "' target='_blank'>Order Receipt </a> ";

		msg = msg
				+ "<p><Order details: items, extras, price, fees, promo discount, total $><br>For any further questions, ";
		if(order.getMerchant().getPhoneNumber()!=null) { 
			msg = msg +"Please call us at " + order.getMerchant().getPhoneNumber() + "";}
		else{
			msg = msg +"Please call our store";
		}
				msg = msg +" <br><br>Regards,<br>" + order.getMerchant().getName()
				+ "<br><br>";
		msg = msg + "</div>";
		msg = msg + "<p>We appreciate your business with us!</p>"
				+ "<div style='height: 70px; text-align: center;'>&nbsp;<img src='http://www.dev.foodkonnekt.com/app/foodnew.jpg' alt='' width='200' height='63' /></div>"
				+ "</div>"
				+ "<p style='color: #676767; font-size: 11px;'>Automated email. Please do not reply to this sender email.</p>";
		
		
		message.setContent(msg, "text/html");
		
		Transport.send(message);

		LOGGER.info("mail sent successfully");

	} catch (MessagingException e) {
		throw new RuntimeException(e);
	}

	}
*/	
	/*public static void sendOrderReceiptAndFeedbackEmail(OrderR order, List<OrderR> orderList) {
		
		 try {
			String cusID = EncryptionDecryptionUtil.encryption(String.valueOf(order.getCustomer().getId()));
			String orderId = EncryptionDecryptionUtil.encryption(String.valueOf(order.getId()));
			
			
			Message message = new MimeMessage(
					SendMailProperty.mailProperty(IConstant.FROM_EMAIL_ID, IConstant.FROM_PASSWORD));
			message.setFrom(new InternetAddress(IConstant.FROM_EMAIL_ID));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(order.getCustomer().getEmailId()));
		
		message.setSubject("Regards: Order Receipt and feedback"); 
					
			String merchantLogo="";
			if (order.getMerchant().getMerchantLogo() == null) {
	            merchantLogo = "http://www.dev.foodkonnekt.com/app/foodnew.jpg";
	        } else {
	            merchantLogo = UrlConstant.BASE_PORT + order.getMerchant().getMerchantLogo();
	        }
			
			String msg = "<div style='width: 700px; height: auto; border: solid 1px #ddd; box-shadow: 0px 0px 6px #ddd; background-color: #f2f2f2; text-align: center; margin: 40px 0px 0px 35px;'>"
					+ "<div style='width: 700px; height: 85px; padding: 20px 0px; text-align: center;'>&nbsp;<img src='"
					+ merchantLogo + "' height='80' /></div>";
			
			msg = msg+"<div style='background-color: #fff; padding: 20px 0px 0px 0px; width: 90%; margin: 0 auto 65px auto;'>"
					+ "<p style='font-size: 20px; color: #e6880f;'>Dear " + order.getCustomer().getFirstName() + ",</p>";
			
			msg = msg+"<br /> Thank You for your recent order and we hope that your experience has been pleasant."
							+ " In order for us to improve we are seeking feedback about your online ordering experience as well as about our food. Please find below a "
							+ "link for the survey (it would take around 2 min to complete this survey). As always we look forward to serving you soon. .";
			
				msg = msg+"<br/> <a 'font-size: 14px; none; text-align: left;padding-left:5px;' href ='" + UrlConstant.WEB_BASE_URL + "/feedbackForm?customerId=" + cusID
					+ "&orderId=" + orderId+ "' target='_blank'>Complete the Survey </a> ";
				msg = msg+" &nbsp;";
				
				for(OrderR orderRe :orderList)
				{
					String orderIds = EncryptionDecryptionUtil.encryption(String.valueOf(orderRe.getId()));
				msg = msg+" <a 'font-size: 14px; none; text-align: left;padding-left:10px;' href ='" + UrlConstant.WEB_BASE_URL + "/orderRec?orderid=" + orderIds
						+ "&customerid=" + cusID+ "' target='_blank'>Order Receipt_"+orderRe.getOrderPosId() +"</a> ";
				msg = msg+" &nbsp;";
				}
			msg = msg
					+ "<p><Order details: items, extras, price, fees, promo discount, total $><br>For any further questions, ";
			if(order.getMerchant().getPhoneNumber()!=null) { 
				msg = msg +"Please call us at " + order.getMerchant().getPhoneNumber() + "";}
			else{
				msg = msg +"Please call our store";
			}
					msg = msg +" <br><br>Regards,<br>" + order.getMerchant().getName()
					+ "<br><br>";
			msg = msg + "</div>";
			msg = msg + "<p>We appreciate your business with us!</p>"
					+ "<div style='height: 70px; text-align: center;'>&nbsp;<img src='http://www.dev.foodkonnekt.com/app/foodnew.jpg' alt='' width='200' height='63' /></div>"
					+ "</div>"
					+ "<p style='color: #676767; font-size: 11px;'>Automated email. Please do not reply to this sender email.</p>";
			
			
			message.setContent(msg, "text/html");
			
			Transport.send(message);

			LOGGER.info("mail sent successfully");

		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}

		}
*/
	
	
	//Dummy method for testing
	public static void sendOrderReceiptAndFeedbackEmail(OrderR order, List<OrderR> orderList,Environment environment) {
		LOGGER.info("===============  MailSendUtil : Inside sendOrderReceiptAndFeedbackEmail :: Start  ============= ");
		
		 /*try {
			String cusID = EncryptionDecryptionUtil.encryption(String.valueOf(order.getCustomer().getId()));
			String orderId = EncryptionDecryptionUtil.encryption(String.valueOf(order.getId()));
			
			
			Message message = new MimeMessage(
					SendMailProperty.mailProperty(IConstant.FROM_EMAIL_ID, IConstant.FROM_PASSWORD));
			message.setFrom(new InternetAddress(IConstant.FROM_EMAIL_ID));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(order.getCustomer().getEmailId()));
		*/
		
		
		
//		Properties props = new Properties();
//		props.put("mail.smtp.host", "smtp.gmail.com");
//		props.put("mail.smtp.socketFactory.port", "465");
//		props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
//		props.put("mail.smtp.auth", "true");
//		props.put("mail.smtp.port", "465");

		/*Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication("noreply.co.mail@gmail.com", "qwe@1234");
			}
		});*/
		String cusID = EncryptionDecryptionUtil.encryption(String.valueOf(order.getCustomer().getId()));
		String orderId = EncryptionDecryptionUtil.encryption(String.valueOf(order.getId()));
		LOGGER.info("===============  MailSendUtil : Inside sendOrderReceiptAndFeedbackEmail order.getCustomer().getId() " +order.getCustomer().getId());
		LOGGER.info("===============  MailSendUtil : Inside sendOrderReceiptAndFeedbackEmail order.getId() "+order.getId());
		
		try {
			/*MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress("noreply.co.mail@gmail.com"));*/
			Message message = new MimeMessage(
					SendMailProperty.gmailMailProperty(IConstant.FROM_EMAIL_ID, IConstant.FROM_PASSWORD));
			message.setFrom(new InternetAddress(IConstant.FROM_EMAIL_ID));
			
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(order.getCustomer().getEmailId()));
		
			LOGGER.info("===============  MailSendUtil : Inside sendOrderReceiptAndFeedbackEmail EmailId "+order.getCustomer().getEmailId());
		
		
		
		
		message.setSubject("Regards: Order Receipt and feedback"); 
		
		
		String customerName = "Customer";
		String mailHead ="Thank You for dining with us";
		
		
		if(order.getCustomer()!=null && order.getCustomer().getFirstName()!= null && order.getCustomer().getLastName()!= null){
			customerName = order.getCustomer().getFirstName().concat(" ").concat(order.getCustomer().getLastName());
		}else if(order.getCustomer()!=null && order.getCustomer().getFirstName()!= null){
			customerName = order.getCustomer().getFirstName();
		}
		
					
			String merchantLogo="";
			if (order.getMerchant().getMerchantLogo() == null) {
	            merchantLogo = "https://gallery.mailchimp.com/63d57cb8e02236766b6ec6d06/images/e38be12c-9d09-4248-a809-b45181a5a0f5.jpg";
	        } else {
	            merchantLogo = environment.getProperty("BASE_PORT") + order.getMerchant().getMerchantLogo();
	        }
			
			
			String merchantName = "Merchant";
			if (order.getMerchant()!=null && order.getMerchant().getName() != null) {
				 merchantName = order.getMerchant().getName();
	        }
			
			
			/*String msg = "<div style='width: 700px; height: auto; border: solid 1px #ddd; box-shadow: 0px 0px 6px #ddd; background-color: #f2f2f2; text-align: center; margin: 40px 0px 0px 35px;'>"
					+ "<div style='width: 700px; height: 85px; padding: 20px 0px; text-align: center;'>&nbsp;<img src='"
					+ merchantLogo + "' height='80' /></div>";
			
			msg = msg+"<div style='background-color: #fff; padding: 20px 0px 0px 0px; width: 90%; margin: 0 auto 65px auto;'>"
					+ "<p style='font-size: 20px; color: #e6880f;'>Dear " + order.getCustomer().getFirstName() + ",</p>";
			
			msg = msg+"<br /> <p text-align: left;> Thank You for your recent order and we hope that your experience has been pleasant."
							+ " In order for us to improve we are seeking feedback about your online ordering experience as well as about our food. Please find below a "
							+ "link for the survey (it would take around 2 min to complete this survey). As always we look forward to serving you soon. </p>";
			
				msg = msg+"<br/> <a 'font-size: 14px; none; text-align: left;padding-left:5px;' href ='" + environment.getProperty("WEB_BASE_URL") + "/feedbackForm?customerId=" + cusID
					+ "&orderId=" + orderId+ "' target='_blank'>Complete the Survey </a> ";
				msg = msg+" &nbsp;";
				
				for(OrderR orderRe :orderList)
				{
					String orderIds = EncryptionDecryptionUtil.encryption(String.valueOf(orderRe.getId()));
				msg = msg+" <a 'font-size: 14px; none; text-align: left;padding-left:10px;' href ='" + environment.getProperty("WEB_BASE_URL") + "/orderRec?orderid=" + orderIds
						+ "&customerid=" + cusID+ "' target='_blank'>Order Receipt_"+orderRe.getOrderPosId() +"</a> ";
				msg = msg+" &nbsp;";
				}
			msg = msg
					+ "<p><Order details: items, extras, price, fees, promo discount, total $><br>For any further questions, ";
			if(order.getMerchant().getPhoneNumber()!=null) { 
				msg = msg +"Please call us at " + order.getMerchant().getPhoneNumber() + "";}
			else{
				msg = msg +"Please call our store";
			}
					msg = msg +" <br><br>Regards,<br>" + order.getMerchant().getName()
					+ "<br><br>";
			msg = msg + "</div>";
			msg = msg + "<p>We appreciate your business with us!</p>"
					+ "<div style='height: 70px; text-align: center;'>&nbsp;<img src='http://www.dev.foodkonnekt.com/app/foodnew.jpg' alt='' width='200' height='63' /></div>"
					+ "</div>"
					+ "<p style='color: #676767; font-size: 11px;'>Automated email. Please do not reply to this sender email.</p>";*/
			
			
			String mailBody = "We Thank You for your recent online order at our restaurant at "+merchantName+" .We value your business and your feedback helps us to get better. Please click the link here to begin your feedback process.";
		
			/*mailBody = mailBody+"<br/> <a 'font-size: 14px; none; text-align: left;padding-left:5px;' href ='" + UrlConstant.WEB_KRITIQ_BASE_URL + "/feedbackForm?customerId=" + cusID
					+ "&orderId=" + orderId+ "' target='_blank'>Complete the Survey </a> ";*/
			
			mailBody = mailBody+"<br/> <a 'font-size: 14px; none; text-align: left;padding-left:5px;' href ='" + environment.getProperty("WEB_KRITIQ_BASE_URL") + "/customerFeedbackV2?customerId=" + cusID
					+ "&orderId=" + orderId+ "' target='_blank'>Complete the Survey</a> ";
			
			mailBody = mailBody+" &nbsp;";
			    if(orderList!=null)
				for(OrderR orderRe :orderList)
				{
					String orderIds = EncryptionDecryptionUtil.encryption(String.valueOf(orderRe.getId()));
					mailBody = mailBody+" <a 'font-size: 14px; none; text-align: left;padding-left:10px;' href ='" + environment.getProperty("WEB_BASE_URL") + "/orderRec?orderid=" + orderIds
						+ "&customerid=" + cusID+ "' target='_blank'>Order Receipt_"+orderRe.getOrderPosId() +"</a> ";
					mailBody = mailBody+" &nbsp;";
				}
			
			
			
			
			/*String mailBody = "<br /> <p text-align: left;> Thank You for your recent order and we hope that your experience has been pleasant."
							+ " In order for us to improve we are seeking feedback about your online ordering experience as well as about our food. Please find below a "
							+ "link for the survey (it would take around 2 min to complete this survey). As always we look forward to serving you soon. </p>";
			
			mailBody = mailBody+"<br/> <a 'font-size: 14px; none; text-align: left;padding-left:5px;' href ='" + environment.getProperty("WEB_BASE_URL") + "/feedbackForm?customerId=" + cusID
					+ "&orderId=" + orderId+ "' target='_blank'>Complete the Survey </a> ";
			mailBody = mailBody+" &nbsp;";
				
				for(OrderR orderRe :orderList)
				{
					String orderIds = EncryptionDecryptionUtil.encryption(String.valueOf(orderRe.getId()));
					mailBody = mailBody+" <a 'font-size: 14px; none; text-align: left;padding-left:10px;' href ='" + environment.getProperty("WEB_BASE_URL") + "/orderRec?orderid=" + orderIds
						+ "&customerid=" + cusID+ "' target='_blank'>Order Receipt_"+orderRe.getOrderPosId() +"</a> ";
					mailBody = mailBody+" &nbsp;";
				}
				mailBody = mailBody
					+ "<p><Order details: items, extras, price, fees, promo discount, total $><br>For any further questions, ";
			if(order.getMerchant().getPhoneNumber()!=null) { 
				mailBody = mailBody +"Please call us at " + order.getMerchant().getPhoneNumber() + "";}
			else{
				mailBody = mailBody +"Please call our store";
			}*/
			
			
			
					
			String mailRegards = order.getMerchant().getName();
			
			String msg = kritiqMailFormate(customerName,mailHead,mailBody,merchantLogo,mailRegards);
			
			message.setContent(msg, "text/html");
			
			Transport.send(message);

			LOGGER.info("mail sent successfully");

		} catch (MessagingException e) {
			LOGGER.error("===============  MailSendUtil : Inside save :: Exception  ============= " + e);
			throw new RuntimeException(e);
		}

		}

	
	
public static void thankyouMailForFeedback(CustomerFeedback customerFeedback, Customer customer, Boolean flag,Environment environment) {
		
		try {
			LOGGER.info("===============  MailSendUtil : Inside thankyouMailForFeedback :: Start  ============= ");
			Message message = new MimeMessage(
					SendMailProperty.gmailMailProperty(IConstant.FROM_EMAIL_ID, IConstant.FROM_PASSWORD));
			message.setFrom(new InternetAddress(IConstant.FROM_EMAIL_ID));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(customerFeedback.getCustomer().getEmailId()));
			message.setSubject("Regards: Thankyou for feedback");
			String customerName = "Customer";
			String mailHead ="Thank You for sharing your feedback";
			String mailBody = "We Thank You for your time and for your feedback. One of our staff members will review your feedback and reach out to you in case of any future questions. We look forward to seeing you soon again at our restaurant. ";
			String merchantLogo="";
			if (customer==null || customer.getMerchantt()==null || customer.getMerchantt().getMerchantLogo() == null) {
	            merchantLogo = "https://gallery.mailchimp.com/63d57cb8e02236766b6ec6d06/images/e38be12c-9d09-4248-a809-b45181a5a0f5.jpg";
	        } else {
	            merchantLogo = environment.getProperty("BASE_PORT") + customer.getMerchantt().getMerchantLogo();
	        }
			
			if(customer!=null && customer.getFirstName()!= null && customer.getLastName()!= null){
				customerName = customer.getFirstName().concat(" ").concat(customer.getLastName());
			}else if(customer!=null && customer.getFirstName()!= null){
				customerName = customer.getFirstName();
			}
			
			
			
			
			
			/*String msg = "<%@ page language='java' contentType='text/html; charset=ISO-8859-1'pageEncoding='ISO-8859-1'%>"
					+ "<!DOCTYPE html PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3.org/TR/html4/loose.dtd'>"
					+ "<html><head><meta charset='utf-8'>"
					+ "<meta http-equiv='X-UA-Compatible' content='IE=edge'><meta name='viewport' content='width=device-width, initial-scale=1'>"
					+ "<title>Email Template</title>"
					+ "<link type='text/css' rel='stylesheet' href='http://34.229.94.111:8080/web/resources/mailContent/css/font-awesome.min.css'/>"
     + "<link href='http://34.229.94.111:8080/web/resources/mailContent/css/bootstrap.min.css' rel='stylesheet'>"
		+ "<!-- Bootstrap -->"
	+ "<link href='http://34.229.94.111:8080/web/resources/mailContent/css/style.css' rel='stylesheet'>"
    + "</head>"

    + "<body>"
	   + "<main>"
          + "<div class='email-logo'>"
            + "<img src='img/logo.png' height='70' />"
          + "</div>"
		     + " <div class='email-block'>"
             + " <div class='email-header text-center'>"
             + "     Mkonnekt Social Report"
             + " </div>"
              + "<div class='email-body'>"
               + " <p>Hello John Doe,</p>"
               + " <p>We received a negative review yesterday for 'location name'. Let us know if you would like us to respond in any specific manner."
               + " <br/>"
               + " <p><b>Platform </b> : Facebook</p>"
               + " <p><b>Review Date </b> : 2017-09-01T23:10:27+0000</p>"
               + " <p><b>Customer Name </b> : Stephanie Karl Beaudry</p>"
               + " <p><b>Rating </b> : 1.0</p>"
                + "<p><b>Review Rating </b> : Called four times and got the answering machine each time.</p>"
               + " <br/><br/>"

                + "<p>Best,<br/>"
               + " mKonnekt Team</p>"
               + " <hr/>"
               + " <div class='text-center email-social'> <a href='#'><i class='fa fa-facebook-square'></i></a><a href='#'><i class='fa fa-twitter-square'></i></a><a href='#'><i class='fa fa-rss-square'></i></a></div></div></div></main> </body></html>";
			*/		
			
			
			/*String msg =  "<!DOCTYPE html PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3.org/TR/html4/loose.dtd'>"
					+ "<html>"
					+ "<head>"
					+ "<title>Email Template</title>"
					+ "<style>.email-logo{max-width: 800px; margin: 20px auto; }</style>"
     + "<style> .email-block{max-width: 800px; background: #fff; margin: 20px auto; border-radius: 5px; border: 1px solid #ec902d; }</style>"
		+ "<!-- Bootstrap -->"
	+ "<style> .email-body{background:#fff; color: #8c8c8c; padding: 15px; }</style>"
		+"<style> .email-header{background:#ec902d; color: #fff; font-size: 22px; padding: 15px; text-transform: uppercase; letter-spacing: 5px; } </style>"
    + "</head>"

    + "<body>"
	   + "<main>"
          + "<div class='email-logo'>"
            + "<img src='http://34.229.94.111:8080/web/resources/mailContent/img/logo.png' height='70' />"
          + "</div>"
		     + " <div class='email-block'>"
             + " <div class='email-header text-center'>"
             + "     Mkonnekt Social Report"
             + " </div>"
              + "<div class='email-body'>"
               + " <p>Hello John Doe,</p>"
               + " <p>We received a negative review yesterday for 'location name'. Let us know if you would like us to respond in any specific manner."
               + " <br/>"
               + " <p><b>Platform </b> : Facebook</p>"
               + " <p><b>Review Date </b> : 2017-09-01T23:10:27+0000</p>"
               + " <p><b>Customer Name </b> : Stephanie Karl Beaudry</p>"
               + " <p><b>Rating </b> : 1.0</p>"
                + "<p><b>Review Rating </b> : Called four times and got the answering machine each time.</p>"
               + " <br/><br/>"

                + "<p>Best,<br/>"
               + " mKonnekt Team</p>"
               + " <hr/>"
               + " <div class='text-center email-social'> <a href='#'><i class='fa fa-facebook-square'></i></a><a href='#'><i class='fa fa-twitter-square'></i></a><a href='#'><i class='fa fa-rss-square'></i></a></div></div></div></main> </body></html>";
			*/
			
			/*String msg =  "<!DOCTYPE html PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3.org/TR/html4/loose.dtd'>"
					+ "<html>"
					+ "<head>"
					+ "<title>Email Template</title>"
					+ "<style>.email-logo{max-width: 800px; margin: 20px auto; }</style>"
     + "<style> .email-block{max-width: 800px; background: #fff; margin: 20px auto; border-radius: 5px; border: 1px solid #ec902d; }</style>"
		+ "<!-- Bootstrap -->"
	+ "<style> .email-body{background:#fff; color: #8c8c8c; padding: 15px; }</style>"
		+"<style> .email-header{background:#ec902d; color: #fff; font-size: 22px; padding: 15px; text-transform: uppercase; letter-spacing: 5px; } </style>"
    + "</head>"

    + "<body>"
	   + "<main>"
          + "<div class='email-logo'>"
            + "<img src='" + merchantLogo + "' height='70' />"
          + "</div>"
		     + " <div class='email-block'>"
             + " <div class='email-header text-center'>"
             + " </div>"
              + "<div class='email-body'>"
              + "     Dear "
 			 		+ customerName
               + " <p>We Thank You for your time and for your feedback. We will review your feedback and we will get back to you shortly."
               + " <br/>"
               + " <p><b>Platform </b> : Facebook</p>"
               + " <p><b>Review Date </b> : 2017-09-01T23:10:27+0000</p>"
               + " <p><b>Customer Name </b> : Stephanie Karl Beaudry</p>"
               + " <p><b>Rating </b> : 1.0</p>"
                + "<p><b>Review Rating </b> : Called four times and got the answering machine each time.</p>"
               + " <br/><br/>"

                + "<p>Best,<br/>"
               + " mKonnekt Team</p>"
               + " <hr/>"
               + " <div class='text-center email-social'> <a href='#'><i class='fa fa-facebook-square'></i></a><a href='#'><i class='fa fa-twitter-square'></i></a><a href='#'><i class='fa fa-rss-square'></i></a></div></div></div></main> </body></html>";
			*/
			String mailRegards = "Kritiq" ;
			String msg = kritiqMailFormate(customerName,mailHead,mailBody,merchantLogo,mailRegards);
			
			message.setContent(msg, "text/html");	

			Transport.send(message);
		
		} catch (MessagingException e) {
			LOGGER.error("===============  MailSendUtil : Inside thankyouMailForFeedback :: Exception  ============= " + e);
			throw new RuntimeException(e);
		}

	}
	
public static  String kritiqMailFormate(String customerName,String mailHead,String mailBody,String merchantLogo,String mailRegards){
	LOGGER.info("===============  MailSendUtil : Inside kritiqMailFormate :: Start  =============customerName "+customerName);
	
	String data = "<html xmlns='http://www.w3.org/1999/xhtml' xmlns:v='urn:schemas-microsoft-com:vml' xmlns:o='urn:schemas-microsoft-com:office:office'>"
			+"<head> "
			+"<meta charset='UTF-8'>"
			+"<meta http-equiv='X-UA-Compatible' content='IE=edge'>"
			+"<meta name='viewport' content='width=device-width, initial-scale=1'>"
			+" <style type='text/css'> "
					+" p{"
						+" margin:10px 0;"
						+" padding:0;"
					+" }"
					+" table{"
						+" border-collapse:collapse;"
					+" }"
					+" h1,h2,h3,h4,h5,h6{"
						+" display:block;"
						+" margin:0;"
						+" padding:0;"
					+" }"
					+" img,a img{"
						+" border:0;"
						+" height:auto;"
						+" outline:none;"
						+" text-decoration:none;"
					+" }"
					+" body,#bodyTable,#bodyCell{"
						+" height:100%;"
					+" 	margin:0;"
						+" padding:0;"
					+" 	width:100%;"
				+" 	}"
				+" 	.mcnPreviewText{"
				+" 		display:none !important;"
				+" 	}"
				+" 	#outlook a{"
				+" 		padding:0;"
				+" 	}"
				+" 	img{"
				+"		-ms-interpolation-mode:bicubic;"
				+" 	}"
				+" 	table{"
				+" 		mso-table-lspace:0pt;"
				+" 		mso-table-rspace:0pt;"
				+" 	}"
				+" 	.ReadMsgBody{"
				+" 		width:100%;"
				+" 	}"
				+" 	.ExternalClass{"
				+" 		width:100%;"
				+" 	}"
				+" 	p,a,li,td,blockquote{"
				+" 		mso-line-height-rule:exactly;"
				+" 	}"
				+" 	a[href^=tel],a[href^=sms]{"
				+" 		color:inherit;"
				+" 		cursor:default;"
				+" 		text-decoration:none;"
				+" 	}"
				+" 	p,a,li,td,body,table,blockquote{"
				+" 		-ms-text-size-adjust:100%;"
				+" 		-webkit-text-size-adjust:100%;"
				+" 	}"
				+" 	.ExternalClass,.ExternalClass p,.ExternalClass td,.ExternalClass div,.ExternalClass span,.ExternalClass font{"
				+" 		line-height:100%;"
				+" 	}"
				+" 	a[x-apple-data-detectors]{"
				+" 		color:inherit !important;"
				+" 		text-decoration:none !important;"
				+" 		font-size:inherit !important;"
				+" 		font-family:inherit !important;"
				+" 		font-weight:inherit !important;"
				+" 		line-height:inherit !important;"
				+" 	}"
				+" 	.templateContainer{"
				+" 		max-width:600px !important;"
				+" 	}"
				+" 	a.mcnButton{"
				+" 		display:block;"
				+" 	}"
				+" 	.mcnImage{"
				+" 		vertical-align:bottom;"
				+" 	}"
				+" 	.mcnTextContent{"
				+" 		word-break:break-word;"
				+" 	}"
				+" 	.mcnTextContent img{"
				+" 		height:auto !important;"
				+" 	}"
				+" 	.mcnDividerBlock{"
				+" 		table-layout:fixed !important;"
				+" 	}"
				
				+" 	h1{"
				+" 		color:#222222;"
				+" 		font-family:Helvetica;"
				+" 		font-size:40px;"
				+" 		font-style:normal;"
				+" 		font-weight:bold;"
				+" 		line-height:150%;"
				+" 		letter-spacing:normal;"
				+" 		text-align:center;"
				+" 	}"
				
				+" 	h2{"
				+" 		color:#222222;"
				+" 		font-family:Helvetica;"
				+" 		font-size:34px;"
				+" 		font-style:normal;"
				+" 		font-weight:bold;"
				+" 		line-height:150%;"
				+" 		letter-spacing:normal;"
				+" 		text-align:left;"
				+" 	}"
				
				+" 	h3{"
					+" 	color:#444444;"
					+" 	font-family:Helvetica;"
					+" 	font-size:22px;"
					+" 	font-style:normal;"
					+" 	font-weight:bold;"
					+" 	line-height:150%;"
					+" 	letter-spacing:normal;"
					+" 	text-align:left;"
				+" 	}"
				
				+" 	h4{"
				+" 		color:#999999;"
				+" 		font-family:Georgia;"
				+" 		font-size:20px;"
				+" 		font-style:italic;"
				+" 		font-weight:normal;"
				+" 		line-height:125%;"
				+" 		letter-spacing:normal;"
				+" 		text-align:center;"
				+" 	}"
				
				+" 	#templateHeader{"
				+" 		background-color:#f7f7f7;"
				+" 		background-image:none;"
				+" 		background-repeat:no-repeat;"
				+" 		background-position:center;"
				+" 		background-size:cover;"
				+" 		border-top:0;"
				+" 		border-bottom:0;"
				+" 		padding-top:0px;"
				+" 		padding-bottom:0px;"
				+" 	}"
				+" 	.headerContainer{"
				+" 		background-color:transparent;"
				+" 		background-image:none;"
				+" 		background-repeat:no-repeat;"
				+" 		background-position:center;"
				+" 		background-size:cover;"
				+" 		border-top:0;"
				+" 		border-bottom:0;"
				+" 		padding-top:0;"
				+" 		padding-bottom:0;"
				+" 	}"
				
				+" 	.headerContainer .mcnTextContent,.headerContainer .mcnTextContent p{"
				+" 		color:#808080;"
				+" 		font-family:Helvetica;"
				+" 		font-size:16px;"
				+" 		line-height:150%;"
				+" 		text-align:left;"
				+" 	}"
				
				+" 	.headerContainer .mcnTextContent a,.headerContainer .mcnTextContent p a{"
				+" 		color:#00ADD8;"
				+" 		font-weight:normal;"
				+"		text-decoration:underline;"
				+" 	}"
				
				+" 	#templateBody{"
				+" 		background-color:#FFFFFF;"
				+" 		background-image:none;"
				+" 		background-repeat:no-repeat;"
				+" 		background-position:center;"
				+" 		background-size:cover;"
				+" 		border-top:0;"
				+" 		border-bottom:0;"
				+" 		padding-top:0px;"
				+" 		padding-bottom:0px;"
				+" 	}"
				
				+" 	.bodyContainer{"
				+" 		background-color:transparent;"
				+" 		background-image:none;"
				+" 		background-repeat:no-repeat;"
				+" 		background-position:center;"
				+" 		background-size:cover;"
				+" 		border-top:0;"
				+" 		border-bottom:0;"
				+" 		padding-top:0;"
				+" 		padding-bottom:0;"
				+" 	}"
				
				+" 	.bodyContainer .mcnTextContent,.bodyContainer .mcnTextContent p{"
				+" 		color:#808080;"
				+" 		font-family:Helvetica;"
				+" 		font-size:16px;"
				+" 		line-height:150%;"
				+" 		text-align:left;"
				+" 	}"
				
				+" 	.bodyContainer .mcnTextContent a,.bodyContainer .mcnTextContent p a{"
				+" 		color:#00ADD8;"
				+" 		font-weight:normal;"
				+" 		text-decoration:underline;"
				+" 	}"
				
				+" 	#templateFooter{"
				+" 		background-color:#3f3717;   "
				+" 		background-image:none;   "
				+" 		background-repeat:no-repeat;   "
				+" 		background-position:center;   "
				+" 		background-size:cover;   "
				+" 		border-top:0;   "
				+" 		border-bottom:0;   "
				+" 		padding-top:0px;   "
				+" 		padding-bottom:0px;   "
				+" 	}"
				
				+" 	.footerContainer{"
				+" 		background-color:transparent;   "
				+" 		background-image:none;   "
				+" 		background-repeat:no-repeat;   "
				+" 		background-position:center;   "
				+" 		background-size:cover;   "
				+" 		border-top:0;   "
				+" 		border-bottom:0;   "
				+" 		padding-top:0;   "
				+" 		padding-bottom:0;   "
				+" 	}"
				
				+" 	.footerContainer .mcnTextContent,.footerContainer .mcnTextContent p{"
				+" 		font-family:Helvetica;   "
				+" 		font-size:12px;   "
				+" 		line-height:150%;   "
				+" 		text-align:center;   "
				+" 	}"
				
				+" 	.footerContainer .mcnTextContent a,.footerContainer .mcnTextContent p a{"
				+" 		color:#FFFFFF;   "
				+" 		font-weight:normal;   "
				+" 		text-decoration:underline;   "
				+" 	}"
			+" 	@media only screen and (min-width:768px){"
			+" 		.templateContainer{"
			+" 			width:600px !important;"
			+" 		}"

			+" }	@media only screen and (max-width: 480px){"
			+" 		body,table,td,p,a,li,blockquote{"
			+" 			-webkit-text-size-adjust:none !important;"
			+" 		}"
			
			+" }	@media only screen and (max-width: 480px){" 
			+"  		body{"
			+" 			width:100% !important;   "
			+" 			min-width:100% !important;   "
			+" 		}"

			+" }	@media only screen and (max-width: 480px){"
			+" 		.mcnImage{"
			+" 			width:100% !important;   "
			+" 		}"
			
			+" }	@media only screen and (max-width: 480px){"
			+" 		.mcnCartContainer,.mcnCaptionTopContent,.mcnRecContentContainer,.mcnCaptionBottomContent,.mcnTextContentContainer,.mcnBoxedTextContentContainer,.mcnImageGroupContentContainer,.mcnCaptionLeftTextContentContainer,.mcnCaptionRightTextContentContainer,.mcnCaptionLeftImageContentContainer,.mcnCaptionRightImageContentContainer,.mcnImageCardLeftTextContentContainer,.mcnImageCardRightTextContentContainer{"
			+" 			max-width:100% !important;   "
			+" 			width:100% !important;   "
			+" 		}"

			+" }	@media only screen and (max-width: 480px){"
			+" 		.mcnBoxedTextContentContainer{"
			+" 			min-width:100% !important;"
			+" 		}"

			+" }	@media only screen and (max-width: 480px){"
			+" 		.mcnImageGroupContent{"
			+" 			padding:9px !important;"
			+" 		}"

			+" }	@media only screen and (max-width: 480px){"
			+" 		.mcnCaptionLeftContentOuter .mcnTextContent,.mcnCaptionRightContentOuter .mcnTextContent{"
			+" 			padding-top:9px !important;"
			+" 		}"

			+" }	@media only screen and (max-width: 480px){"
			+" 		.mcnImageCardTopImageContent,.mcnCaptionBlockInner .mcnCaptionTopContent:last-child .mcnTextContent{"
			+" 			padding-top:18px !important;"
			+" 		}"

			+" }	@media only screen and (max-width: 480px){"
			+" 		.mcnImageCardBottomImageContent{"
			+" 			padding-bottom:9px !important;"
			+" 		}"

			+" }	@media only screen and (max-width: 480px){"
			+" 		.mcnImageGroupBlockInner{"
			+" 			padding-top:0 !important;"
			+" 			padding-bottom:0 !important;"
			+" 		}"

			+" }	@media only screen and (max-width: 480px){"
			+" 		.mcnImageGroupBlockOuter{"
			+" 			padding-top:9px !important;"
			+" 			padding-bottom:9px !important;"
			+" 		}"

			+" }	@media only screen and (max-width: 480px){"
			+" 		.mcnTextContent,.mcnBoxedTextContentColumn{"
			+" 			padding-right:18px !important;   "
			+" 			padding-left:18px !important;   "
			+" 		}"

			+" }	@media only screen and (max-width: 480px){"
			+" 		.mcnImageCardLeftImageContent,.mcnImageCardRightImageContent{"
			+" 			padding-right:18px !important;   "
			+" 			padding-bottom:0 !important;   "
			+" 			padding-left:18px !important;   "
			+" 		}"

			+" }	@media only screen and (max-width: 480px){"
			+" 		.mcpreview-image-uploader{"
			+" 			display:none !important;   "
			+" 			width:100% !important;   "
			+" 		}"

			+" }	@media only screen and (max-width: 480px){"
				
			+" 		h1{"
			+" 			font-size:30px !important;"
			+" 			line-height:125% !important;"
			+" 		}"

			+" }	@media only screen and (max-width: 480px){"
				
			+" 		h2{"
			+" 			font-size:26px !important;"
			+" 			line-height:125% !important;"
			+" 		}"

			+" }	@media only screen and (max-width: 480px){"
				
			+" 		h3{"
			+" 			font-size:20px !important;   "
			+" 			line-height:150% !important;   "
			+" 		}"

			+" }	@media only screen and (max-width: 480px){"
				
			+" 		h4{   "
			+" 			font-size:18px !important;   "
			+" 			line-height:150% !important;   "
			+" 		}"

			+" }	@media only screen and (max-width: 480px){   "
				
			+" 		.mcnBoxedTextContentContainer .mcnTextContent,.mcnBoxedTextContentContainer .mcnTextContent p{   "
			+" 			font-size:14px !important;   "
			+" 			line-height:150% !important;   "
			+" 		}"

			+" }	@media only screen and (max-width: 480px){   "
				
			+" 		.headerContainer .mcnTextContent,.headerContainer .mcnTextContent p{   "
			+" 			font-size:16px !important;   "
			+" 			line-height:150% !important;   "
			+" 		}"

			+" }	@media only screen and (max-width: 480px){   "
				
			+" 		.bodyContainer .mcnTextContent,.bodyContainer .mcnTextContent p{   "
			+" 			font-size:16px !important;   "
			+" 			line-height:150% !important;   "
			+" 		}"

			+" }	@media only screen and (max-width: 480px){"
				
			+" 		.footerContainer .mcnTextContent,.footerContainer .mcnTextContent p{"
			+" 			font-size:14px !important;   "
			+" 			line-height:150% !important;   "
			+" 		}"

			+" }</style></head>"
			+"  <body>"
					
					
					
			        +"   <center>"
			            +"   <table align='center' border='0' cellpadding='0' cellspacing='0' height='100%' width='100%' id='bodyTable'>   "
			                +"   <tr>"
			                    +"   <td align='center' valign='top' id='bodyCell'>   "
			                      
			                        +"   <table border='0' cellpadding='0' cellspacing='0' width='100%'>   "
										+"   <tr>   "
											+"   <td align='center' valign='top' id='templateHeader' data-template-container>   "
												
												+"   <table align='center' border='0' cellpadding='0' cellspacing='0' width='100%' class='templateContainer'>   "
													+"   <tr>   "
			                                			+"   <td valign='top' class='headerContainer'><table border='0' cellpadding='0' cellspacing='0' width='100%' class='mcnImageBlock' style='min-width:100%;'>   "
			    +"   <tbody class='mcnImageBlockOuter'>   "
			            +"   <tr>   "
			                +"   <td valign='top' style='padding:9px' class='mcnImageBlockInner'>   "
			                    +"   <table align='left' width='100%' border='0' cellpadding='0' cellspacing='0' class='mcnImageContentContainer' style='min-width:100%;'>   "
			                        +"   <tbody><tr>   "
			                            +"   <td class='mcnImageContent' valign='top' style='padding-right: 9px; padding-left: 9px; padding-top: 0; padding-bottom: 0; text-align:center;'>   "
			                                
			                                    
			                                        +"   <img align='center' alt='' src='"+merchantLogo+"' width='235' style='max-width:235px; padding-bottom: 0; display: inline !important; vertical-align: bottom;' class='mcnImage'>   "
			                                    
			                                
			                            +"   </td>   "
			                        +"   </tr>   "
			                    +"   </tbody>  </table>   "
			                +"   </td>   "
			            +"   </tr>   "
			    +"   </tbody>   "
			+"   </table>   </td>   "
													+"   </tr>   "
												+"   </table>   "
												
											+"   </td>   "
			                            +"   </tr>   "
										+"   <tr>   "
											+"   <td align='center' valign='top' id='templateBody' data-template-container>   "
												
												+"   <table align='center' border='0' cellpadding='0' cellspacing='0' width='100%' class='templateContainer'>   "
													+"   <tr>   "
			                                			+"   <td valign='top' class='bodyContainer'><table border='0' cellpadding='0' cellspacing='0' width='100%' class='mcnTextBlock' style='min-width:100%;'>   "
			    +"   <tbody class='mcnTextBlockOuter'>   "
			        +"   <tr>   "
			            +"   <td valign='top' class='mcnTextBlockInner' style='padding-top:9px;'>   "
			              	
						    
							
			                +"   <table align='left' border='0' cellpadding='0' cellspacing='0' style='max-width:100%; min-width:100%;' width='100%' class='mcnTextContentContainer'>   "
			                    +"   <tbody><tr>   "
			                        
			                        +"   <td valign='top' class='mcnTextContent' style='padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;'>   "
			                        
			                            +"   <h4>" + mailHead +"</h4>   "

			                        +"   </td>   "
			                    +"   </tr>   "
			                +"   </tbody></table>   "
							
			            +"   </td>   "
			        +"   </tr>   "
			    +"   </tbody>   "
			+"   </table><table border='0' cellpadding='0' cellspacing='0' width='100%' class='mcnDividerBlock' style='min-width:100%;'>   "
			    +"   <tbody class='mcnDividerBlockOuter'>   "
			        +"   <tr>   "
			            +"   <td class='mcnDividerBlockInner' style='min-width: 100%; padding: 27px 18px 0px;'>   "
			                +"   <table class='mcnDividerContent' border='0' cellpadding='0' cellspacing='0' width='100%' style='min-width:100%;'>   "
			                    +"   <tbody><tr>   "
			                        +"   <td>   "
			                            +"   <span></span>   "
			                        +"   </td>   "
			                    +"   </tr>   "
			                +"   </tbody></table>   "

			            +"   </td>   "
			        +"   </tr>   "
			    +"   </tbody>   "
			+"   </table><table border='0' cellpadding='0' cellspacing='0' width='100%' class='mcnDividerBlock' style='min-width:100%;'>   "
			    +"   <tbody class='mcnDividerBlockOuter'>   "
			        +"   <tr>   "
			            +"   <td class='mcnDividerBlockInner' style='min-width: 100%; padding: 9px 18px 0px;'>   "
			                +"   <table class='mcnDividerContent' border='0' cellpadding='0' cellspacing='0' width='100%' style='min-width:100%;'>   "
			                    +"   <tbody><tr>   "
			                        +"   <td>   "
			                            +"   <span></span>   "
			                        +"   </td>   "
			                    +"   </tr>   "
			                +"   </tbody></table>   "

			            +"   </td>   "
			        +"   </tr>   "
			    +"   </tbody>   "
			+"   </table><table border='0' cellpadding='0' cellspacing='0' width='100%' class='mcnTextBlock' style='min-width:100%;'>   "
			    +"   <tbody class='mcnTextBlockOuter'>   "
			        +"   <tr>   "
			            +"   <td valign='top' class='mcnTextBlockInner' style='padding-top:9px;'>   "
			            
							+"   <table align='left' border='0' cellspacing='0' cellpadding='0' width='100%' style='width:100%;'>   "
							+"   <tr>   "
							
							+"   <td valign='top' width='600' style='width:600px;'>   "
							+"   <![endif]-->   "
			                +"   <table align='left' border='0' cellpadding='0' cellspacing='0' style='max-width:100%; min-width:100%;' width='100%' class='mcnTextContentContainer'>   "
			                    +"   <tbody><tr>   "
			                        
			                        +"   <td valign='top' class='mcnTextContent' style='padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;'>   "
			                        
			                            +"   <p>Dear " + customerName +",</p>   "

			+"   <p>"+ mailBody +"&nbsp;</p>   "

			+"   <p>Best,</p>   "

			+"   <p>"+mailRegards+" Team</p>   "

			                        +"   </td>   "
			                    +"   </tr>   "
			                +"   </tbody></table>   "
							
			                
							
			            +"   </td>   "
			        +"   </tr>   "
			    +"   </tbody>   "
			+"   </table><table border='0' cellpadding='0' cellspacing='0' width='100%' class='mcnDividerBlock' style='min-width:100%;'>   "
			    +"   <tbody class='mcnDividerBlockOuter'>   "
			        +"   <tr>   "
			            +"   <td class='mcnDividerBlockInner' style='min-width: 100%; padding: 9px 18px;'>   "
			                +"   <table class='mcnDividerContent' border='0' cellpadding='0' cellspacing='0' width='100%' style='min-width: 100%;border-top: 1px solid #E0E0E0;'>   "
			                    +"   <tbody><tr>   "
			                        +"   <td>   "
			                            +"   <span></span>   "
			                        +"   </td>   "
			                    +"   </tr>   "
			                +"   </tbody></table>   "

			            +"   </td>   "
			        +"   </tr>   "
			    +"   </tbody>   "
			+"   </table></td>   "
													+"   </tr>   "
												+"   </table>   "
												
											+"   </td>   "
			                            +"   </tr>   "
			                            +"   <tr>   "
											+"   <td align='center' valign='top' id='templateFooter' data-template-container>   "
												
												+"   <table align='center' border='0' cellpadding='0' cellspacing='0' width='100%' class='templateContainer'>   "
													+"   <tr>   "
			                                			+"   <td valign='top' class='footerContainer'><table border='0' cellpadding='0' cellspacing='0' width='100%' class='mcnFollowBlock' style='min-width:100%;'>   "
			    +"   <tbody class='mcnFollowBlockOuter'>   "
			        +"   <tr>   "
			            +"   <td align='center' valign='top' style='padding:9px' class='mcnFollowBlockInner'>   "
			                +"   <table border='0' cellpadding='0' cellspacing='0' width='100%' class='mcnFollowContentContainer' style='min-width:100%;'>   "
			    +"   <tbody><tr>   "
			        +"   <td align='center' style='padding-left:9px;padding-right:9px;'>   "
			            +"   <table border='0' cellpadding='0' cellspacing='0' width='100%' style='min-width:100%;' class='mcnFollowContent'>   "
			                +"   <tbody><tr>   "
			                    +"   <td align='center' valign='top' style='padding-top:9px; padding-right:9px; padding-left:9px;'>   "
			                        +"   <table align='center' border='0' cellpadding='0' cellspacing='0'>   "
			                            +"   <tbody><tr>   "
			                                +"   <td align='center' valign='top'>   "
			                                  
			                                        
			                                        
			                                            +"   <table align='left' border='0' cellpadding='0' cellspacing='0' style='display:inline;'>   "
			                                                +"   <tbody><tr>   "
			                                                    +"   <td valign='top' style='padding-right:10px; padding-bottom:9px;' class='mcnFollowContentItemContainer'>   "
			                                                        +"   <table border='0' cellpadding='0' cellspacing='0' width='100%' class='mcnFollowContentItem'>   "
			                                                            +"   <tbody><tr>   "
			                                                                +"   <td align='left' valign='middle' style='padding-top:5px; padding-right:10px; padding-bottom:5px; padding-left:9px;'>   "
			                                                                    +"   <table align='left' border='0' cellpadding='0' cellspacing='0' width=''>   "
			                                                                        +"   <tbody><tr>   "
			                                                                            
			                                                                                +"   <td align='center' valign='middle' width='24' class='mcnFollowIconContent'>   "
			                                                                                    +"   <a href='http://www.linkedin.com/company/13358770/' target='_blank'><img src='https://cdn-images.mailchimp.com/icons/social-block-v2/outline-light-linkedin-48.png' style='display:block;' height='24' width='24' class=''></a>   "
			                                                                                +"   </td>   "
			                                                                            
			                                                                            
			                                                                        +"   </tr>   "
			                                                                    +"   </tbody></table>   "
			                                                                +"   </td>   "
			                                                            +"   </tr>   "
			                                                        +"   </tbody></table>   "
			                                                    +"   </td>   "
			                                                +"   </tr>   "
			                                            +"   </tbody></table>   "
			                                        
			                                       
			                                        
			                                        
			                                            +"   <table align='left' border='0' cellpadding='0' cellspacing='0' style='display:inline;'>   "
			                                                +"   <tbody><tr>   "
			                                                    +"   <td valign='top' style='padding-right:10px; padding-bottom:9px;' class='mcnFollowContentItemContainer'>   "
			                                                        +"   <table border='0' cellpadding='0' cellspacing='0' width='100%' class='mcnFollowContentItem'>   "
			                                                            +"   <tbody><tr>   "
			                                                                +"   <td align='left' valign='middle' style='padding-top:5px; padding-right:10px; padding-bottom:5px; padding-left:9px;'>   "
			                                                                    +"   <table align='left' border='0' cellpadding='0' cellspacing='0' width=''>   "
			                                                                        +"   <tbody><tr>   "
			                                                                            
			                                                                                +"   <td align='center' valign='middle' width='24' class='mcnFollowIconContent'>   "
			                                                                                    +"   <a href='http://www.twitter.com/mKonnekt' target='_blank'><img src='https://cdn-images.mailchimp.com/icons/social-block-v2/outline-light-twitter-48.png' style='display:block;' height='24' width='24' class=''></a>   "
			                                                                                +"   </td>   "
			                                                                            
			                                                                            
			                                                                        +"   </tr>   "
			                                                                    +"   </tbody></table>   "
			                                                                +"   </td>   "
			                                                            +"   </tr>   "
			                                                        +"   </tbody></table>   "
			                                                    +"   </td>   "
			                                                +"   </tr>   "
			                                            +"   </tbody></table>   "
			                                        
			                                      
			                                    
			                                        
			                                        
			                                        
			                                            +"   <table align='left' border='0' cellpadding='0' cellspacing='0' style='display:inline;'>   "
			                                                +"   <tbody><tr>   "
			                                                    +"   <td valign='top' style='padding-right:10px; padding-bottom:9px;' class='mcnFollowContentItemContainer'>   "
			                                                        +"   <table border='0' cellpadding='0' cellspacing='0' width='100%' class='mcnFollowContentItem'>   "
			                                                            +"   <tbody><tr>   "
			                                                                +"   <td align='left' valign='middle' style='padding-top:5px; padding-right:10px; padding-bottom:5px; padding-left:9px;'>   "
			                                                                    +"  <table align='left' border='0' cellpadding='0' cellspacing='0' width=''>   "
			                                                                        +"   <tbody><tr>   "
			                                                                            
			                                                                                +"   <td align='center' valign='middle' width='24' class='mcnFollowIconContent'>   "
			                                                                                    +"   <a href='http://www.instagram.com/mKonnekt' target='_blank'><img src='https://cdn-images.mailchimp.com/icons/social-block-v2/outline-light-instagram-48.png' style='display:block;' height='24' width='24' class=''></a>   "
			                                                                                +"   </td>   "
			                                                                            
			                                                                            
			                                                                        +"   </tr>   "
			                                                                    +"   </tbody></table>   "
			                                                                +"   </td>   "
			                                                            +"   </tr>   "
			                                                        +"   </tbody></table>   "
			                                                    +"   </td>   "
			                                                +"   </tr>   "
			                                            +"   </tbody></table>   "
			                                        
			                                       
			                                    
			                                        
			                                        
			                                        
			                                            +"   <table align='left' border='0' cellpadding='0' cellspacing='0' style='display:inline;'>   "
			                                                +"   <tbody><tr>   "
			                                                    +"   <td valign='top' style='padding-right:0; padding-bottom:9px;' class='mcnFollowContentItemContainer'>   "
			                                                        +"   <table border='0' cellpadding='0' cellspacing='0' width='100%' class='mcnFollowContentItem'>   "
			                                                            +"   <tbody><tr>   "
			                                                                +"   <td align='left' valign='middle' style='padding-top:5px; padding-right:10px; padding-bottom:5px; padding-left:9px;'>   "
			                                                                    +"   <table align='left' border='0' cellpadding='0' cellspacing='0' width=''>   "
			                                                                        +"   <tbody><tr>   "
			                                                                            
			                                                                                +"   <td align='center' valign='middle' width='24' class='mcnFollowIconContent'>   "
			                                                                                    +"   <a href='http://www.mkonnekt.com' target='_blank'><img src='https://cdn-images.mailchimp.com/icons/social-block-v2/outline-light-link-48.png' style='display:block;' height='24' width='24' class=''></a>   "
			                                                                                +"   </td>   "
			                                                                            
			                                                                            
			                                                                        +"   </tr>   "
			                                                                    +"   </tbody></table>   "
			                                                                +"   </td>   "
			                                                            +"   </tr>   "
			                                                        +"   </tbody></table>   "
			                                                    +"   </td>   "
			                                                +"   </tr>   "
			                                            +"   </tbody></table>   "
			                                        
			                                        
			                                +"   </td>   "
			                            +"   </tr>   "
			                        +"   </tbody></table>   "
			                    +"   </td>   "
			                +"   </tr>   "
			            +"   </tbody></table>   "
			        +"   </td>   "
			    +"   </tr>   "
			+"   </tbody></table>   "

			            +"   </td>   "
			        +"   </tr>   "
			    +"   </tbody>   "
			+"   </table><table border='0' cellpadding='0' cellspacing='0' width='100%' class='mcnDividerBlock' style='min-width:100%;'>   "
			    +"   <tbody class='mcnDividerBlockOuter'>   "
			        +"   <tr>   "
			            +"   <td class='mcnDividerBlockInner' style='min-width:100%; padding:18px;'>   "
			                +"   <table class='mcnDividerContent' border='0' cellpadding='0' cellspacing='0' width='100%' style='min-width: 100%;border-top: 2px solid #505050;'>   "
			                    +"   <tbody><tr>   "
			                        +"   <td>   "
			                            +"   <span></span>   "
			                        +"   </td>   "
			                    +"   </tr>   "
			                +"   </tbody></table>   "

			            +"   </td>   "
			        +"   </tr>   "
			    +"   </tbody>   "
			+"   </table><table border='0' cellpadding='0' cellspacing='0' width='100%' class='mcnTextBlock' style='min-width:100%;'>   "
			    +"   <tbody class='mcnTextBlockOuter'>   "
			        +"   <tr>   "
			            +"   <td valign='top' class='mcnTextBlockInner' style='padding-top:9px;'>   "
			              	
						    
							
			                +"   <table align='left' border='0' cellpadding='0' cellspacing='0' style='max-width:100%; min-width:100%;' width='100%' class='mcnTextContentContainer'>   "
			                    +"   <tbody><tr>   "
			                        
			                        +"   <td valign='top' class='mcnTextContent' style='padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;'>   "
			                        
			                            +"   <em>Copyright � mKonnekt&nbsp;LLC, All rights reserved.</em>   "
			                        +"   </td>   "
			                    +"   </tr>   "
			                +"   </tbody></table>   "
							
			                
							
			            +"   </td>   "
			        +"   </tr>   "
			    +"   </tbody>   "
			+"   </table></td>   "
													+"   </tr>   "
												+"   </table>   "
												
											+"   </td>   "
			                            +"   </tr>   "
			                        +"   </table>   "
			                        
			                    +"   </td>   "
			                +"   </tr>   "
			            +"   </table>   "
			        +"   </center>   "
			    +"   </body>   "
			+"   </html>   ";

	
	LOGGER.info("===============  MailSendUtil : Inside kritiqMailFormate :: End  ============= ");
	
	return data;
}
	
//	public static void thankyouMailForFeedback(CustomerFeedback customerFeedback, Customer customer, Boolean flag) {
//		
//		try {
//			Message message = new MimeMessage(
//					SendMailProperty.mailProperty(IConstant.FROM_EMAIL_ID, IConstant.FROM_PASSWORD));
//			message.setFrom(new InternetAddress(IConstant.FROM_EMAIL_ID));
//			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(customerFeedback.getCustomer().getEmailId()));
//			message.setSubject("Regards: Thankyou for feedback");
//			String customerName = "Customer";
//			
//			
//			String merchantLogo="";
//			if (customer==null || customer.getMerchantt()==null || customer.getMerchantt().getMerchantLogo() == null) {
//	            merchantLogo = "http://www.dev.foodkonnekt.com/app/foodnew.jpg";
//	        } else {
//	            merchantLogo = UrlConstant.BASE_PORT + customer.getMerchantt().getMerchantLogo();
//	        }
//			
//			if(customer!=null && customer.getFirstName()!= null){
//				customerName = customer.getFirstName();
//			}
//			
//			String msg = "<div style='width: 700px; height: auto; border: solid 1px #ddd; box-shadow: 0px 0px 6px #ddd; background-color: #f2f2f2; text-align: center; margin: 40px 0px 0px 35px;'>"
//					+ "<div style='width: 700px; height: 85px; padding: 20px 0px; text-align: center;'>&nbsp;<img src='"
//					+ merchantLogo + "' height='80' /></div>"
//					+ "<div style='background-color: #fff; padding: 20px 0px 0px 0px; width: 90%; margin: 0 auto 65px auto;'>"
//					+ "<p style='font-size: 20px; color: #e6880f;'>Dear " + customerName  + ",</p>";
//			
//					/*msg = msg+"<br/> <a style='text-decoration: none; margin: 16px 0px; display: inline-block; background-color: #f8981d; color: #fff; padding: 10px 12px; border-radius: 10px; font-weight: 600; font-size: 15px;' "
//							    + "href= http://www.tacosymasdallas.com/ target='_blank'>Please find the link to the certificate</a><br /> this is our way of saying Thanks for providing us your valuable feedback. We will get back to you shortly.";*/
//
//			/*if(flag == true){
//				msg = msg+"<br/> We Thank You for providing your valuable feedback and for your time as well. We sincerely apologize about your experience. We take customer service very seriously and we are sorry about dropping the ball in this case. Can you please send us an email with your contact information so that one of our managers can reach out to you.<br/> We look forward to hearing from you soon.";	
//			}else{*/
//				msg = msg+"<br/><p> We Thank You for your time and for your feedback. We will review your feedback and we will get back to you shortly.</P>";	
//			//}
//					
//					
//					
//					
//					msg = msg +"<br>Best,<br>Kritiq Team<br>";
//			msg = msg + "</div>";
//			msg = msg+ "<div style='height: 70px; text-align: center;'>&nbsp;<img src='http://www.dev.foodkonnekt.com/app/foodnew.jpg' alt='' width='200' height='63' /></div>"
//					+ "</div>"
//					+ "<p style='color: #676767; font-size: 11px;'>Automated email. Please do not reply to this sender email.</p>";
//	
//			message.setContent(msg, "text/html");	
//
//			Transport.send(message);
//		
//		} catch (MessagingException e) {
//			throw new RuntimeException(e);
//		}
//
//	}
		




	
	public static void feedbackResultOfClientToMerchantMail( CustomerFeedback customerFeedback, Customer customer, List<CustomerFeedbackAnswer> custFedbackAnswer , String email, String merchantTypeMerchant,String mailSubject,Environment environment) {
		LOGGER.info("====== MailSendUtil : feedbackResultOfClientToMerchantMail Starts === customer "+customer.getMerchantt().getOwner().getEmail()+" email "+email+" merchantTypeMerchant "+merchantTypeMerchant+" mailSubject "+mailSubject);
		try {
			Message message = new MimeMessage(
					SendMailProperty.gmailMailProperty(IConstant.FROM_EMAIL_ID, IConstant.FROM_PASSWORD));
			message.setFrom(new InternetAddress(IConstant.FROM_EMAIL_ID));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email));
			message.setSubject(mailSubject);
		
			String customerName= "Merchant";
			String merchantName= customer.getMerchantt().getName().substring(0, 1).toUpperCase() + customer.getMerchantt().getName().substring(1);
		if(merchantTypeMerchant!= ""){
			customerName = merchantName;
		}else{
			customerName = merchantTypeMerchant;
		}
		
		
		/*if(customer!=null && customer.getFirstName()!=null){
			customerName=customer.getFirstName();
		}*/
			/*String cusID = EncryptionDecryptionUtil.encryption(String.valueOf(customerFeedback.getCustomer().getId()));
			String orderId = EncryptionDecryptionUtil.encryption(String.valueOf(customerFeedback.getOrderR().getId()));*/
			
		   /*    String merchantLogo = "http://www.dev.foodkonnekt.com/app/foodnew.jpg";
	        
	     	
			String msg = "<div style='width: 700px; height: auto; border: solid 1px #ddd; box-shadow: 0px 0px 6px #ddd; background-color: #f2f2f2; text-align: center; margin: 40px 0px 0px 35px;'>"
					+ "<div style='width: 700px; height: 85px; padding: 20px 0px; text-align: center;'>&nbsp;<img src='"
					+ merchantLogo + "' height='80' /></div>";
					
			msgToDisplay = msgToDisplay+"<p style='font-size: 14px;text-align: left;padding-left:10px;'>Hello "+customer.getMerchantt().getName()+",<p>";
			msgToDisplay = msgToDisplay+"<p style='font-size: 14px;text-align: left;padding-left:10px;'>We just received a feedback provided to us by "+customerName+". We are sending you this email for you to be aware of. Please let us know if you would like to respond in any specific manner.,<p>";
			msgToDisplay = msgToDisplay+"<p style='font-size: 14px;text-align: left;padding-left:10px;'><a 'font-size: 14px; none; text-align: left;padding-left:10px;' href='" + environment.getProperty("WEB_BASE_URL") + "/customerfeedback?customerFeedbackId=" + EncryptionDecryptionUtil.encryption(customerFeedback.getId().toString())+ "' target='_blank'>Customer's Feedback</a><p>";*/
		
		String msgToDisplay = "<table style='width:100%; border: 1px solid black;'><thead style=' border: 1px solid black;'><tr><th>Question</th><th>Customer Rate</th></thead><tbody>";
		for(int j=0;j<custFedbackAnswer.size();j++)
		{
			msgToDisplay = msgToDisplay+"<tr><td style='width: 420px;' align='left'>"+custFedbackAnswer.get(j).getFeedbackQuestion().getQuestion()+"</td><td style='text-align: center;'>"+custFedbackAnswer.get(j).getAnswer()+"</td></tr>";
			
		}
		
		msgToDisplay = msgToDisplay+"</tbody></table>";
		if(customer!=null){
			if(customer.getFirstName()!=null)
		msgToDisplay = msgToDisplay+"<br>Customer Name: "+customer.getFirstName()+" ";
		if(customer.getLastName()!=null)
			msgToDisplay = msgToDisplay+ customer.getLastName();
			if(customer.getPhoneNumber()!=null)
		msgToDisplay = msgToDisplay+"<br>Customer Phone: "+customer.getPhoneNumber();
			if(customer.getEmailId()!=null)
		msgToDisplay = msgToDisplay+"<br>Customer email: "+customer.getEmailId();
		}
		String mailRegards = "Kritiq ";
		String merchantLogo = "https://gallery.mailchimp.com/63d57cb8e02236766b6ec6d06/images/e38be12c-9d09-4248-a809-b45181a5a0f5.jpg";
		//String merchantName= customer.getMerchantt().getName()+",";
		
		String mailHead ="Customer Feedback - Attention Required";
		/*String mailBody = "We have recently had a customer who reviewed us unfavorably on one or more criteria. Please find the link to the customer's detailed feedback. This is for your information so that necessary actions can be executed to alleviate the customer's conerns. <br>"*/
		String mailBody = "We received feedback from a customer for our "+merchantName+". For one or more questions the customer was not satisfied and you can find information regarding customer's feedback below. You can also get the customer's complete feedback "
							+"<a  href='" + environment.getProperty("WEB_BASE_URL") + "/customerfeedback?customerFeedbackId=" + EncryptionDecryptionUtil.encryption(customerFeedback.getId().toString())+ "' target='_blank'>here</a>"
		+ "<style type='text/css'>table, th, td { border: 1px solid black; border-collapse: collapse; } <col width='1030'>"
		+ "</style>"
		/*+ "<table style='width:100%; border: 1px solid black;' >"
			+ "<tbody style=' border: 1px solid black;'>"
				+ "<tr>"
					+ "<th style='width:100px'>Platform</th>"
					+ "<td style='width:100px'>Facebook</td>"
				+ "</tr>"
				+ "<tr>"
					+ "<th>Review Date</th>"
					+ "<td style='width:100px'>"+customerFeedback.getCreateDate()+"/td>"
				+ "</tr>"
				+ "<tr>"
					+ "<th style='width:100px'>Customer Name</th>"
					+ "<th>"+customerName+"</th>"
				+ "</tr>"
				+ "<tr>"
					+ "<th>Rating</th>"
					+ "<td style='width:100px'>"+rating +" star</td>"
				+ "</tr>"
				+ "<tr>"
					+ "<th style='width:100px'>Review</th>"
					+ "<th>"+customerFeedback.getCustomerComments() +"</th>"
				+ "</tr>"
			+ "</tbody>"
		+ "</table> "*/
			+ "<br>"+ msgToDisplay;
			/*msg = msg+"<p style='font-size: 14px;text-align: left;padding-left:10px;'>This is the score for '"+customer.getFirstName()+"' for order ";
			msg = msg+"<a 'font-size: 14px; none; text-align: left;padding-left:10px;' href='" + environment.getProperty("WEB_BASE_URL") + "/orderRec?orderid=" + orderId
				+ "&customerid=" + cusID+ "' target='_blank'>"+customer.getMerchantt().getPosMerchantId()+"</a></p>";*/
			
			
			/*msgToDisplay = msgToDisplay+"<table><thead><tr><th>Question</th><th></th><th>Customer Rate</th></thead><tbody>";
			for(int j=0;j<merchantCount;j++)
			{
				msgToDisplay = msgToDisplay+"<tr><td width='230' align='left'>"+custFedbackAnswer.get(j).getFeedbackQuestion().getQuestion()+"</td><td>:</td><td>"+custFedbackAnswer.get(j).getAnswer()+"</td></tr>";
				
			}
			
			msgToDisplay = msgToDisplay+"</tbody></table>";*/
			
		/*	msg = msg + "</div>";
			msg = msg + "<p>We appreciate your business with us!</p>"
					+ "<div style='height: 70px; text-align: center;'>&nbsp;<img src='http://www.dev.foodkonnekt.com/app/foodnew.jpg' alt='' width='200' height='63' /></div>"
					+ "</div>"
					+ "<p style='color: #676767; font-size: 11px;'>Automated email. Please do not reply to this sender email.</p>";*/
		
		String msg = kritiqMailFormate(customerName,mailHead,mailBody,merchantLogo,mailRegards);
		//String msg = kritiqMailFormate(merchantName,mailHead,mailBody);
			message.setContent(msg, "text/html");	
			Transport.send(message);
		
		} catch (MessagingException e) {
			LOGGER.error("===============  MailSendUtil : Inside feedbackResultOfClientToMerchantMail :: Exception  ============= " + e);
			throw new RuntimeException(e);
		}

	}

	
	/*public static void feedbackResultOfClientToSupportMail( CustomerFeedback customerFeedback, Customer customer,
															int supportCount, List<CustomerFeedbackAnswer> custFedbackAnswer) {
		
		try {
			Message message = new MimeMessage(
					SendMailProperty.mailProperty(IConstant.FROM_EMAIL_ID, IConstant.FROM_PASSWORD));
			message.setFrom(new InternetAddress(IConstant.FROM_EMAIL_ID));
			//receipient will be foodkonnekt support
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse("support@foodkonnekt.com"));
			message.setSubject("Regards: Feedback Result for Online Service");
		
			
			String cusID = EncryptionDecryptionUtil.encryption(String.valueOf(customerFeedback.getCustomer().getId()));
			String orderId = EncryptionDecryptionUtil.encryption(String.valueOf(customerFeedback.getOrderR().getId()));

		
			
			String merchantLogo = "http://www.dev.foodkonnekt.com/app/foodnew.jpg";
			
			
			String merchantName= customer.getMerchantt().getName();
			String mailHead ="Negative Feedback By Customer";
			String mailBody = "<p style='font-size: 14px;text-align: left;padding-left:10px;'>This is the score for '"+customer.getFirstName()+"' for order ";
			mailBody = mailBody+"<a 'font-size: 14px; none; text-align: left;padding-left:10px;' href='" + environment.getProperty("WEB_BASE_URL") + "/orderRec?orderid=" + orderId
					+ "&customerid=" + cusID+ "' target='_blank'>"+customer.getMerchantt().getPosMerchantId()+"</a> </p>";
				
				
			mailBody = mailBody+"<table><thead><tr><th>Question</th><th></th><th>Customer Rate</th></thead><tbody>";
				for(int j=0;j<supportCount;j++)
				{
					mailBody = mailBody+"<tr><td width='230' align='left'>"+custFedbackAnswer.get(j).getFeedbackQuestion().getQuestion()+"</td><td>:</td><td>"+custFedbackAnswer.get(j).getAnswer()+"</td></tr>";
					
				}
				mailBody = mailBody+"</tbody></table>";
				
				mailBody = mailBody+"<br><p style='font-size: 14px;text-align: left;padding-left:10px;'><a 'font-size: 14px; none; text-align: left;padding-left:10px;' href='" + environment.getProperty("WEB_BASE_URL") + "/customerfeedback?customerFeedbackId=" + EncryptionDecryptionUtil.encryption(customerFeedback.getId().toString())+ "' target='_blank'>Customer's Feedback</a><p>";
				
				
				mailBody = mailBody + "</div>";
				mailBody = mailBody + "<p>We appreciate your business with us!</p>"           
						+ "<div style='height: 70px; text-align: center;'>&nbsp;<img src='http://www.dev.foodkonnekt.com/app/foodnew.jpg' alt='' width='200' height='63' /></div>"
						+ "</div>"
						+ "<p style='color: #676767; font-size: 11px;'>Automated email. Please do not reply to this sender email.</p>";
	        
			String msg = "<div style='width: 700px; height: auto; border: solid 1px #ddd; box-shadow: 0px 0px 6px #ddd; background-color: #f2f2f2; text-align: center; margin: 40px 0px 0px 35px;'>"
					+ "<div style='width: 700px; height: 85px; padding: 20px 0px; text-align: center;'>&nbsp;<img src='"
					+ merchantLogo + "' height='80' /></div>";

			msg = msg+"<p style='font-size: 14px;text-align: left;padding-left:10px;'>This is the score for '"+customer.getFirstName()+"' for order ";
			msg = msg+"<a 'font-size: 14px; none; text-align: left;padding-left:10px;' href='" + environment.getProperty("WEB_BASE_URL") + "/orderRec?orderid=" + orderId
				+ "&customerid=" + cusID+ "' target='_blank'>"+customer.getMerchantt().getPosMerchantId()+"</a> </p>";
			
			
			msg = msg+"<table><thead><tr><th>Question</th><th></th><th>Customer Rate</th></thead><tbody>";
			for(int j=0;j<supportCount;j++)
			{
				msg = msg+"<tr><td width='230' align='left'>"+custFedbackAnswer.get(j).getFeedbackQuestion().getQuestion()+"</td><td>:</td><td>"+custFedbackAnswer.get(j).getAnswer()+"</td></tr>";
				
			}
			msg = msg+"</tbody></table>";
			
			msg = msg+"<br><p style='font-size: 14px;text-align: left;padding-left:10px;'><a 'font-size: 14px; none; text-align: left;padding-left:10px;' href='" + environment.getProperty("WEB_BASE_URL") + "/customerfeedback?customerFeedbackId=" + EncryptionDecryptionUtil.encryption(customerFeedback.getId().toString())+ "' target='_blank'>Customer's Feedback</a><p>";
			
			
			msg = msg + "</div>";
			msg = msg + "<p>We appreciate your business with us!</p>"           
					+ "<div style='height: 70px; text-align: center;'>&nbsp;<img src='http://www.dev.foodkonnekt.com/app/foodnew.jpg' alt='' width='200' height='63' /></div>"
					+ "</div>"
					+ "<p style='color: #676767; font-size: 11px;'>Automated email. Please do not reply to this sender email.</p>";
	
			
			
				String msg = kritiqMailFormate(merchantName,mailHead,mailBody);
			
			
			message.setContent(msg, "text/html");	
			Transport.send(message);
		
		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}

	}*/

	public static void birthdayWishMail(String wishDate, Customer customer,Environment environment) {

		LOGGER.info("====== MailSendUtil : birthdayWishMail Starts === customer "+customer.getEmailId());

		
		
		try {
			Message message = new MimeMessage(
					SendMailProperty.gmailMailProperty(IConstant.FROM_EMAIL_ID, IConstant.FROM_PASSWORD));
			message.setFrom(new InternetAddress(IConstant.FROM_EMAIL_ID));
			//receipient will be foodkonnekt support
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse("support@foodkonnekt.com"));
			message.setSubject("Regards: Happy Birthday");

			
			
			String merchantLogo="";
			if (customer.getMerchantt().getMerchantLogo() == null) {
	            merchantLogo = "http://www.dev.foodkonnekt.com/app/foodnew.jpg";
	        } else {
	            merchantLogo = environment.getProperty("BASE_PORT") + customer.getMerchantt().getMerchantLogo();
	        }
			String msg = "<div style='width: 700px; height: auto; border: solid 1px #ddd; box-shadow: 0px 0px 6px #ddd; background-color: #f2f2f2; text-align: center; margin: 40px 0px 0px 35px;'>"
					+ "<div style='width: 700px; height: 85px; padding: 20px 0px; text-align: center;'>&nbsp;<img src='"
					+ merchantLogo + "' height='80' /></div>"
					+ "<div style='background-color: #fff; padding: 20px 0px 0px 0px; width: 90%; margin: 0 auto 65px auto;'>"
					+ "<p style='font-size: 20px; color: #e6880f;'>Dear ";
					if(customer.getFirstName()!= null) {
						msg +=  customer.getFirstName() + " ";
					}
					if(customer.getLastName()!= null) {
						msg += customer.getLastName()  ;
					}
			     
			msg = msg+",</p><br /> <p text-align: left;>On behalf of all our team, we would like to wish you a happy Birthday."
					+ "We wish you a spectacular life and committed behaviour towards your wishes and desires! </p>";
			
			
			msg = msg
					+ "<p><Order details: items, extras, price, fees, promo discount, total $><br>For any further questions, ";
			if(customer.getMerchantt().getPhoneNumber()!=null) { 
				msg = msg +"Please call us at " +customer.getMerchantt().getPhoneNumber() + "";}
			else{
				msg = msg +"Please call our store";
			}
					msg = msg +" <br><br>Regards,<br>" + customer.getMerchantt().getName()
					+ "<br><br>";
			msg = msg + "</div>";
			msg = msg + "<p>We appreciate your business with us!</p>";
			/*if( customer.getMerchantId()!=null &&  customer.getMerchantId().equals(IConstant.TEXAN_REWARDS_MERCHANT_ID) ){
				msg = msg + "<div style='height: 70px; text-align: center;'>&nbsp;<img src='https://www.foodkonnekt.com/foodkonnekt_merchat_logo/274_PiePizzaria.png' alt='' width='200' height='63' /></div>"
						+ "</div>";
			}else{*/
				msg = msg +"<div style='height: 70px; text-align: center;'>&nbsp;<img src='http://www.dev.foodkonnekt.com/app/foodnew.jpg' alt='' width='200' height='63' /></div>"
					+ "</div>";
			//}
			msg = msg +"<p style='color: #676767; font-size: 11px;'>Automated email. Please do not reply to this sender email.</p>";	
			message.setContent(msg, "text/html");	
			Transport.send(message);
		
		} catch (MessagingException e) {
			LOGGER.error("===============  MailSendUtil : Inside birthdayWishMail :: Exception  ============= " + e);
			throw new RuntimeException(e);
		}

		
	}

	public static void anniversaryWishMail(String wishDate, Customer customer,Environment environment) {
		LOGGER.info("====== MailSendUtil : anniversaryWishMail Starts === customer "+customer.getEmailId());

		try {
			Message message = new MimeMessage(
					SendMailProperty.gmailMailProperty(IConstant.FROM_EMAIL_ID, IConstant.FROM_PASSWORD));
			message.setFrom(new InternetAddress(IConstant.FROM_EMAIL_ID));
			//receipient will be foodkonnekt support
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse("support@foodkonnekt.com"));
			message.setSubject("Regards: Happy Anniversary");

		
			String merchantLogo="";
			if (customer.getMerchantt().getMerchantLogo() == null) {
	            merchantLogo = "http://www.dev.foodkonnekt.com/app/foodnew.jpg";
	        } else {
	            merchantLogo = environment.getProperty("BASE_PORT") + customer.getMerchantt().getMerchantLogo();
	        }
			String msg = "<div style='width: 700px; height: auto; border: solid 1px #ddd; box-shadow: 0px 0px 6px #ddd; background-color: #f2f2f2; text-align: center; margin: 40px 0px 0px 35px;'>"
					+ "<div style='width: 700px; height: 85px; padding: 20px 0px; text-align: center;'>&nbsp;<img src='"
					+ merchantLogo + "' height='80' /></div>"
					+ "<div style='background-color: #fff; padding: 20px 0px 0px 0px; width: 90%; margin: 0 auto 65px auto;'>"
					+ "<p style='font-size: 20px; color: #e6880f;'>Dear ";
					if(customer.getFirstName()!= null) {
						msg +=  customer.getFirstName() + " ";
					}
					if(customer.getLastName()!= null) {
						msg += customer.getLastName()  ;
					}
	     
					msg = msg+",</p><br /> <p text-align: left;>On behalf of all our team, we would like to wish you a happy Anniversary."
					+ "We wish you a spectacular life and committed behaviour towards your wishes and desires! </p>";

			
			msg = msg
					+ "<p><Order details: items, extras, price, fees, promo discount, total $><br>For any further questions, ";
			if(customer.getMerchantt().getPhoneNumber()!=null) { 
				msg = msg +"Please call us at " +customer.getMerchantt().getPhoneNumber() + "";}
			else{
				msg = msg +"Please call our store";
			}
					msg = msg +" <br><br>Regards,<br>" + customer.getMerchantt().getName()
					+ "<br><br>";
			msg = msg + "</div>";
			msg = msg + "<p>We appreciate your business with us!</p>";
			/*if( customer.getMerchantId()!=null &&  customer.getMerchantId().equals(IConstant.TEXAN_REWARDS_MERCHANT_ID) ){
				msg = msg + "<div style='height: 70px; text-align: center;'>&nbsp;<img src='https://www.foodkonnekt.com/foodkonnekt_merchat_logo/274_PiePizzaria.png' alt='' width='200' height='63' /></div>"
						+ "</div>";
			}else{*/
				msg = msg + "<div style='height: 70px; text-align: center;'>&nbsp;<img src='http://www.dev.foodkonnekt.com/app/foodnew.jpg' alt='' width='200' height='63' /></div>"
					+ "</div>";
			//}
			msg = msg +"<p style='color: #676767; font-size: 11px;'>Automated email. Please do not reply to this sender email.</p>";	
			message.setContent(msg, "text/html");	
			Transport.send(message);
		
		} catch (MessagingException e) {
			LOGGER.error("===============  MailSendUtil : Inside anniversaryWishMail :: Exception  ============= " + e);
			throw new RuntimeException(e);
		}
	}
	
	public static boolean onlineOrderNotificationReceiptMail(String orderid,
			String customerid, String customerName,
			Double orderPrice, String orderType,
			String paymentType, Date orderDate,
			String orderLink, String customerEmail) {
		try {
			LOGGER.info("----------------Start :: MailSendUtil : onlineOrderNotificationReceiptMail------------------");
			LOGGER.info(" MailSendUtil :: onlineOrderNotificationReceiptMail : customerId "+customerid);
			LOGGER.info(" MailSendUtil :: onlineOrderNotificationReceiptMail : orderId "+orderid);
			LOGGER.info(" === customerName : "+customerName+" orderPrice : "+orderPrice +" orderType : "+orderType+" paymentType : "+paymentType+" orderDate : "+orderDate
					+" orderLink : "+orderLink+" customerEmail : "+customerEmail);
				Message message = new MimeMessage(
						SendMailProperty.gmailMailProperty(IConstant.FROM_EMAIL_ID ,IConstant.FROM_PASSWORD));
				message.setFrom(new InternetAddress("orders@foodkonnekt.com"));
				message.setRecipients(Message.RecipientType.TO, InternetAddress
						.parse(customerEmail));
				message.setSubject("Online Order Notification");
				
				StringBuilder htmlBuilder = new StringBuilder();
		    	htmlBuilder.append("<html>");

				htmlBuilder.append("<div style='width: 670px; height: auto; border: solid 1px #ddd; box-shadow: 0px 0px 6px #ddd; background-color: #f2f2f2; text-align: center; margin: 40px 0px 0px 35px; margin-left: 25%;'>");
				
				htmlBuilder.append("<div style='height: 670px; height: 85px;  text-align: center;'>&nbsp;<img src='https://www.foodkonnekt.com/foodkonnekt_merchat_logos/269_1207.jpg' height='80' /></div>");
				htmlBuilder.append("<div style='background-color: #fff; padding: 20px 0px 0px 0px; width: 90%; margin: 0 auto 65px auto;'>");
				htmlBuilder.append("<head><title>Hello World</title></head>");
				htmlBuilder.append("<div style='text-align: left; padding: 20px 20px;'>");
				htmlBuilder.append("<table><tbody><tr>");
				htmlBuilder.append("<td width='230'>Customer Name</td>");
				htmlBuilder.append("<td>:</td><td>"+customerName+" </td></tr>");
				htmlBuilder.append("<tr>");
				htmlBuilder.append("<td width='230'>Order type</td>");
				htmlBuilder.append("<td>:</td><td>"+orderType+" </td></tr><tr><td>Order ID</td><td>:</td><td>"+orderid+" </td></tr><tr>");
				htmlBuilder.append("<td>Payment</td><td>:</td><td> "+paymentType+" </td></tr>");
				htmlBuilder.append("<td>Order Receipt</td><td>:</td><td> "+orderLink+" </td></tr>");
				htmlBuilder.append("</tbody></table>");
				htmlBuilder.append("<p><strong>Order Details:</strong><br /></p>");
				
				htmlBuilder.append("<table><tbody>");
					
				htmlBuilder.append("<tr style='font-weight:600;text-transform:capitalize;font-family:arial'><td width='230'><b>item name</b></td>");
				htmlBuilder.append("<td width='100px;' style='text-align:center'><b>3</b></td><td width='100px;' style='text-align:center'><b>6.01</b></td></tr>");
				
					
				htmlBuilder.append("<tr style='text-transform:capitalize;font-family:arial;margin-top:5px;font-size:12px'><td width='200px;'>modifiersName</td><td width='100px;' style='text-align:center'>modifiersItemCount </td>"
						+ "<td width='100px;' style='text-align:center'> modifiersPrice</td></tr>");
				htmlBuilder.append("<tr style='font-weight:600;text-transform:capitalize;font-family:arial'><td width='200px;'><b>Online Fee</b></td><td width='100px;' style='text-align:center'><b>1</b></td><td width='100px;' style='text-align:center'><b>$0.75</b></td></tr>");
				
				htmlBuilder.append("</tbody></table>");
				
				
			htmlBuilder.append("<p style='margin: 0;'>-------------------------------------------------------------------------------</p>");
			
			htmlBuilder.append("<table><tbody><tr><td width='230'>Sub-total</td><td width='100px'; style=\"text-align:center\"></td><td id=\"subtotal\">");
			htmlBuilder.append("</td></tr><tr><td>Convenience Fee</td><td>&nbsp;</td><td id=\"convFee\">");	
			htmlBuilder.append("</td></tr>");
			htmlBuilder.append("<tr><td>Discount</td><td>&nbsp;</td><td id=\"discount\"></td></tr>");
			htmlBuilder.append("<tr><td>Tax</td><td>&nbsp;</td><td id=\"tax\"></td></tr>");
			htmlBuilder.append("</tbody></table>");
			htmlBuilder.append("<p style='margin: 0;'>-------------------------------------------------------------------------------</p>");
			
			htmlBuilder.append("<table><tbody><tr><td width='230'>Total</td><td width=\"100px;\" style=\"text-align:center\">&nbsp;</td><td>"+orderPrice+"</td></tr></tbody></table>");
			
			
			htmlBuilder.append("<br /><br /> <strong style='text-decoration: underline;'>Special Instructions :</strong>  note\r\n<br /><br /><br /> ");
			//

			htmlBuilder.append("<div style='height: 70px; text-align: center;'>&nbsp;<img src='http://www.dev.foodkonnekt.com/app/foodnew.jpg' alt='' width='200' height='63' /></div></div></div></div>");

			
				
				
				htmlBuilder.append("</html>");
				
				
				String msg = "Customer Id"+customerid;
				msg += "Cusromer Name"+customerName;
				msg += " Order Id"+orderid;
				msg += " Order Type"+orderType;
				msg += " Order Price"+String.format("%.2f",(orderPrice));
				msg += "Order Date"+ orderDate;
				msg += "Payment Type"+ orderDate;
				msg += "Order link"+ orderLink;
				
				String msgs = new String(htmlBuilder);
				
				message.setContent(msgs, "text/html");
				Transport.send(message);
				LOGGER.info(" MailSendUtil :: onlineOrderNotificationReceiptMail : mail send successfully");
		} catch (MessagingException e) {
			LOGGER.error(" MailSendUtil :: onlineOrderNotificationReceiptMail : Exception "+e);
			throw new RuntimeException(e);
		}
		LOGGER.info("----------------End :: MailSendUtil : onlineOrderNotificationReceiptMail------------------");
		return true;
	}
	
	
	public static boolean onlineOrderNotification(String orderid,
			String customerid, String customerName,
			Double orderPrice, String orderType,String paymentType
			, Date orderDate,
			String orderLink, String customerEmail,String merchantLogo,String subTotal,String convenienceFeeValue,
			String deliverFee,String tax,Double tipAmount,String note,String merchantName,String orderDetails,String avgTime,
			Double orderDiscount,String orderCoupon, Integer merchantId ,String orderPosId,Environment environment){
		try {
			LOGGER.info("----------------Start :: MailSendUtil : onlineOrderNotification------------------");
			LOGGER.info(" MailSendUtil :: onlineOrderNotification : orderId "+EncryptionDecryptionUtil.decryption(orderid));
			LOGGER.info(" MailSendUtil :: onlineOrderNotification : customerId "+EncryptionDecryptionUtil.decryption(customerid));
			
			Message message = new MimeMessage(
					SendMailProperty.gmailMailProperty(IConstant.FROM_EMAIL_ID, IConstant.FROM_PASSWORD));
			message.setFrom(new InternetAddress(IConstant.FROM_EMAIL_ID));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(customerEmail));
			message.setSubject("Online Order Notification");
			Multipart multipart = new MimeMultipart();
			
			  BodyPart messageBodyPart = new MimeBodyPart();
			  
			  BodyPart attachementBodyPart = new MimeBodyPart();
			  BodyPart thermalReceiptAttachment = new MimeBodyPart();
			  
			String msg = "<div style='width: 700px; height: auto; border: solid 1px #ddd; box-shadow: 0px 0px 6px #ddd; background-color: #f2f2f2; text-align: center; margin: 40px 0px 0px 35px;'>"
					+ "<div style='width: 700px; height: 85px; padding: 20px 0px; text-align: center;'>&nbsp;<img src='"
					+ merchantLogo + "' height='80' /></div>"
					+ "<div style='background-color: #fff; padding: 20px 0px 0px 0px; width: 90%; margin: 0 auto 65px auto;'>"
					+ "<p style='font-size: 20px; color: #e6880f;'>Dear " + merchantName + ",</p>"
					+ "<p style='font-size: 14px;'>You got an Online Order.&nbsp;</p>"
					+ "<div style='text-align: left; padding: 20px 20px;'>" + "<table>" + "<tbody>" + "<tr>"
					+ "<td width='230'>Order type</td>" + "<td>:</td>" + "<td>" + orderType + "</td>" + "</tr>" + "<tr>"
					+ "<td width='230'>Customer Name:</td>" + "<td>:</td>" + "<td>" + customerName + "</td>" + "</tr>" + "<tr>"
					+ "<td>Order ID</td>" + "<td>:</td>" + "<td>" + EncryptionDecryptionUtil.decryption(orderid) + "</td>" + "</tr>" + "<tr>"
					+ "<td>Payment</td>" + "<td>:</td>" + "<td>" + paymentType + "</td>" + "</tr>"+ "<tr>"
					+ "<td>Order PosId</td>" + "<td>:</td>" + "<td>" + orderPosId + "</td>" + "</tr>"
					
					+ "</tbody>"
					+ "</table>" + "<p><strong>Order Details:</strong><br /></p>" + orderDetails + "</p>"
					+ "<p style='margin: 0;'>-------------------------------------------------------------------------------</p>"
					+ "<table>" + "<tbody>" + "<tr>" + "<td width='230'>Sub-total</td>" + "<td>&nbsp;</td>" + "<td>$"
					
					/*CHANGE Convenience to Online*/
					/*+ subTotal + "</td>" + "</tr>" + "<tr>" + "<td>Convenience Fee</td>" + "<td>&nbsp;</td>" + "<td>$"*/
					+ String.format("%.2f",Float.parseFloat(subTotal)) + "</td>" + "</tr>";
					if(convenienceFeeValue!= null && !convenienceFeeValue.isEmpty() &&  convenienceFeeValue!= "" && Double.parseDouble(convenienceFeeValue) > 0.0){
						msg += "<tr><td>Online Fee</td>" + "<td>&nbsp;</td>"
						+ "<td>$" +String.format("%.2f",Float.parseFloat(convenienceFeeValue)) + "</td>" + "</tr>";
					}
			if (deliverFee!=null && orderType.toLowerCase().equals("delivery") && Double.parseDouble(deliverFee) > 0.0) {
				msg = msg + "<tr>" + "<td>Delivery Fee</td>" + "<td>&nbsp;</td>" + "<td>$" + String.format("%.2f",Float.parseFloat(deliverFee)) + "</td>"
						+ "</tr>";

			}
			msg = msg +"<tr>" + "<td>Tax</td>" + "<td>&nbsp;</td>" + "<td>$" + String.format("%.2f",Float.parseFloat(tax)) + "</td>" + "</tr>" ;
			
			if (orderDiscount != null && orderDiscount > 0) {
				msg = msg + "<tr>" + "<td>Discount</td>" + "<td>&nbsp;</td>"
						+ "<td>$" + String.format("%.2f",orderDiscount) + "</td>" + "</tr>"+orderCoupon;
			}else if(orderCoupon!=null && !orderCoupon.isEmpty() && Double.parseDouble(deliverFee)>0){
			    msg = msg + "<tr>" + "<td>Discount</td>" + "<td>&nbsp;</td>"
		                        + "<td>$" + String.format("%.2f",Float.parseFloat(deliverFee)) + "</td>" + "</tr>"+orderCoupon;
			}
			
			if (tipAmount!=null && tipAmount>0) {
				msg = msg + "<tr>" + "<td>Tip</td>" + "<td>&nbsp;</td>" + "<td>$" + String.format("%.2f",tipAmount )+ "</td>"
						+ "</tr>";
			}
			
        msg = msg	+  "</tbody>" + "</table>"
					+ "<p style='margin: 0;'>-------------------------------------------------------------------------------</p>"
					+ "<table>" + "<tbody>" + "<tr>" + "<td width='230'>Total</td>" + "<td>&nbsp;</td>" + "<td>$"
					+ String.format("%.2f",orderPrice) + "</td>" + "</tr>" + "</tbody>" + "</table>"
					+ "<br /><br /> <strong style='text-decoration: underline;'>Special Instructions :</strong> " + note
					+ " <br /><br /><br /> <a style='text-decoration: none; margin: 16px 0px; display: inline-block; background-color: #f8981d; color: #fff; padding: 10px 12px; border-radius: 10px; font-weight: 600; font-size: 15px;' href='"+orderLink+"'"
							+ " target='_blank'>View Online Receipt</a><br /> " ;
        	/*if(merchantId!=null &&  merchantId.equals(IConstant.TEXAN_REWARDS_MERCHANT_ID) ){
			msg = msg + "<div style='height: 70px; text-align: center;'>&nbsp;<img src='https://www.foodkonnekt.com/foodkonnekt_merchat_logo/274_PiePizzaria.png' alt='' width='200' height='63' /></div>"
					+ "</div>";
        	}else{*/
			msg = msg +"<div style='height: 70px; text-align: center;'>&nbsp;<img src='http://www.dev.foodkonnekt.com/app/foodnew.jpg' alt='' width='200' height='63' /></div></div>";
        	//}
        	msg = msg		+ "<p style='color: #676767; font-size: 11px;'>Automated email. Please do not reply to this sender email.</p>"
					+ "</div>";
        	
        	
        	 messageBodyPart.setContent(msg, "text/html");
 	          multipart.addBodyPart(messageBodyPart);
 	        
 	      
 	        DataSource source = new FileDataSource(environment.getProperty("FAX_FILE_PATH")+Integer.parseInt(EncryptionDecryptionUtil.decryption(orderid))+".pdf");

 	        attachementBodyPart.setDataHandler(new DataHandler(source));

 	        attachementBodyPart.setFileName("Order_"+Integer.parseInt(EncryptionDecryptionUtil.decryption(orderid))+".pdf");

 	        multipart.addBodyPart(attachementBodyPart);
 	        DataSource source1 = new FileDataSource(environment.getProperty("THERMAL_RECEIPT")+Integer.parseInt(EncryptionDecryptionUtil.decryption(orderid))+".pdf");
	        thermalReceiptAttachment.setDataHandler(new DataHandler(source1));
	        thermalReceiptAttachment.setFileName("Thermal Receipt_"+Integer.parseInt(EncryptionDecryptionUtil.decryption(orderid))+".pdf");
	        multipart.addBodyPart(thermalReceiptAttachment);
	       
 	        message.setContent(msg, "text/html");
 	        message.setContent(multipart);
			Transport.send(message);
			LOGGER.info(" MailSendUtil :: onlineOrderNotification : mail send successfully");
			IConstant.mailStatus="sent";
		} catch (MessagingException e) {
			LOGGER.error(" MailSendUtil :: onlineOrderNotification : Exception "+e);
		}	
		LOGGER.info("----------------End :: MailSendUtil : onlineOrderNotification------------------");
		return false;
		
	}
	
	
	
	
	
	
	
	/*public static boolean sendKritiqUrlMailToCustomer(String toMail,String url) {
		try {
			Message message = new MimeMessage(
					SendMailProperty.mailProperty(IConstant.FROM_EMAIL_ID, IConstant.FROM_PASSWORD));
			message.setFrom(new InternetAddress(IConstant.FROM_EMAIL_ID));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toMail));
			message.setSubject("Kritiq Url");
			String msg = url;
			msg += " <br>";
			msg += " <br>";
			msg += "<b>Regards,</b><br>";
			msg += "FoodKonnekt";
			message.setContent(msg, "text/html");
			Transport.send(message);
		} catch (Exception e) {
			LOGGER.error("error: " + e.getMessage());
		}
		return true;
	}*/

	
	public static boolean sendKritiqUrlMailToCustomer(String toMail, String url, String merchantName,
			String merchantAddress, String merchantLogo) {
			try {
				LOGGER.info("===============  MailSendUtil : Inside sendKritiqUrlMailToCustomer :: Start  =============toMail "+toMail+" url "+url+" merchantName "+merchantName+" merchantAddress "+
			merchantAddress+" merchantLogo "+merchantLogo);

			Message message = new MimeMessage(
			SendMailProperty.gmailMailProperty(IConstant.FROM_EMAIL_ID, IConstant.FROM_PASSWORD));
			message.setFrom(new InternetAddress(IConstant.FROM_EMAIL_ID));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toMail));
			message.setSubject("Kritiq Url");

			/*
			 * String msg = url;
			 * 
			 * msg += " <br>"; msg += " <br>"; msg += "<b>Regards,</b><br>"; msg
			 * += "FssoodKonnekt"; message.setContent(msg, "text/html");
			 */

			StringBuilder msg = new StringBuilder();
			msg.append("<!doctype html>");
			msg.append(
			"<html xmlns=\"http://www.w3.org/1999/xhtml\" xmlns:v=\"urn:schemas-microsoft-com:vml\" xmlns:o=\"urn:schemas-microsoft-com:office:office\">");
			msg.append("	<head>");
			msg.append("	");
			msg.append("	<meta charset=\"UTF-8\">");
			msg.append("        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">");
			msg.append("        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">");
			msg.append("	<title>*|MC:SUBJECT|*</title>");
			msg.append("        ");
			msg.append("    <style type=\"text/css\">");
			msg.append("	p{");
			msg.append("	margin:10px 0;");
			msg.append("	padding:0;");
			msg.append("	}");
			msg.append("	table{");
			msg.append("	border-collapse:collapse;");
			msg.append("	}");
			msg.append("	h1,h2,h3,h4,h5,h6{");
			msg.append("	display:block;");
			msg.append("	margin:0;");
			msg.append("	padding:0;");
			msg.append("	}");
			msg.append("	img,a img{");
			msg.append("	border:0;");
			msg.append("	height:auto;");
			msg.append("	outline:none;");
			msg.append("	text-decoration:none;");
			msg.append("	}");
			msg.append("	body,#bodyTable,#bodyCell{");
			msg.append("	height:100%;");
			msg.append("	margin:0;");
			msg.append("	padding:0;");
			msg.append("	width:100%;");
			msg.append("	}");
			msg.append("	.mcnPreviewText{");
			msg.append("	display:none !important;");
			msg.append("	}");
			msg.append("	#outlook a{");
			msg.append("	padding:0;");
			msg.append("	}");
			msg.append("	img{");
			msg.append("	-ms-interpolation-mode:bicubic;");
			msg.append("	}");
			msg.append("	table{");
			msg.append("	mso-table-lspace:0pt;");
			msg.append("	mso-table-rspace:0pt;");
			msg.append("	}");
			msg.append("	.ReadMsgBody{");
			msg.append("	width:100%;");
			msg.append("	}");
			msg.append("	.ExternalClass{");
			msg.append("	width:100%;");
			msg.append("	}");
			msg.append("	p,a,li,td,blockquote{");
			msg.append("	mso-line-height-rule:exactly;");
			msg.append("	}");
			msg.append("	a[href^=tel],a[href^=sms]{");
			msg.append("	color:inherit;");
			msg.append("	cursor:default;");
			msg.append("	text-decoration:none;");
			msg.append("	}");
			msg.append("	p,a,li,td,body,table,blockquote{");
			msg.append("	-ms-text-size-adjust:100%;");
			msg.append("	-webkit-text-size-adjust:100%;");
			msg.append("	}");
			msg.append(
			"	.ExternalClass,.ExternalClass p,.ExternalClass td,.ExternalClass div,.ExternalClass span,.ExternalClass font{");
			msg.append("	line-height:100%;");
			msg.append("	}");
			msg.append("	a[x-apple-data-detectors]{");
			msg.append("	color:inherit !important;");
			msg.append("	text-decoration:none !important;");
			msg.append("	font-size:inherit !important;");
			msg.append("	font-family:inherit !important;");
			msg.append("	font-weight:inherit !important;");
			msg.append("	line-height:inherit !important;");
			msg.append("	}");
			msg.append("	#bodyCell{");
			msg.append("	padding:10px;");
			msg.append("	}");
			msg.append("	.templateContainer{");
			msg.append("	max-width:600px !important;");
			msg.append("	}");
			msg.append("	a.mcnButton{");
			msg.append("	display:block;");
			msg.append("	}");
			msg.append("	.mcnImage,.mcnRetinaImage{");
			msg.append("	vertical-align:bottom;");
			msg.append("	}");
			msg.append("	.mcnTextContent{");
			msg.append("	word-break:break-word;");
			msg.append("	}");
			msg.append("	.mcnTextContent img{");
			msg.append("	height:auto !important;");
			msg.append("	}");
			msg.append("	.mcnDividerBlock{");
			msg.append("	table-layout:fixed !important;");
			msg.append("	}");
			msg.append("	/*");
			msg.append("	@tab Page");
			msg.append("	@section Background Style");
			msg.append(
			"	@tip Set the background color and top border for your email. You may want to choose colors that match your company's branding.");
			msg.append("	*/");
			msg.append("	body,#bodyTable{");
			msg.append("	/*@editable*/background-color:#FAFAFA;");
			msg.append("	}");
			msg.append("	/*");
			msg.append("	@tab Page");
			msg.append("	@section Background Style");
			msg.append(
			"	@tip Set the background color and top border for your email. You may want to choose colors that match your company's branding.");
			msg.append("	*/");
			msg.append("	#bodyCell{");
			msg.append("	/*@editable*/border-top:0;");
			msg.append("	}");
			msg.append("	/*");
			msg.append("	@tab Page");
			msg.append("	@section Email Border");
			msg.append("	@tip Set the border for your email.");
			msg.append("	*/");
			msg.append("	.templateContainer{");
			msg.append("	/*@editable*/border:0;");
			msg.append("	}");
			msg.append("	/*");
			msg.append("	@tab Page");
			msg.append("	@section Heading 1");
			msg.append(
			"	@tip Set the styling for all first-level headings in your emails. These should be the largest of your headings.");
			msg.append("	@style heading 1");
			msg.append("	*/");
			msg.append("	h1{");
			msg.append("	/*@editable*/color:#202020;");
			msg.append("	/*@editable*/font-family:Helvetica;");
			msg.append("	/*@editable*/font-size:26px;");
			msg.append("	/*@editable*/font-style:normal;");
			msg.append("	/*@editable*/font-weight:bold;");
			msg.append("	/*@editable*/line-height:125%;");
			msg.append("	/*@editable*/letter-spacing:normal;");
			msg.append("	/*@editable*/text-align:left;");
			msg.append("	}");
			msg.append("	/*");
			msg.append("	@tab Page");
			msg.append("	@section Heading 2");
			msg.append("	@tip Set the styling for all second-level headings in your emails.");
			msg.append("	@style heading 2");
			msg.append("	*/");
			msg.append("	h2{");
			msg.append("	/*@editable*/color:#202020;");
			msg.append("	/*@editable*/font-family:Helvetica;");
			msg.append("	/*@editable*/font-size:22px;");
			msg.append("	/*@editable*/font-style:normal;");
			msg.append("	/*@editable*/font-weight:bold;");
			msg.append("	/*@editable*/line-height:125%;");
			msg.append("	/*@editable*/letter-spacing:normal;");
			msg.append("	/*@editable*/text-align:left;");
			msg.append("	}");
			msg.append("	/*");
			msg.append("	@tab Page");
			msg.append("	@section Heading 3");
			msg.append("	@tip Set the styling for all third-level headings in your emails.");
			msg.append("	@style heading 3");
			msg.append("	*/");
			msg.append("	h3{");
			msg.append("	/*@editable*/color:#202020;");
			msg.append("	/*@editable*/font-family:Helvetica;");
			msg.append("	/*@editable*/font-size:20px;");
			msg.append("	/*@editable*/font-style:normal;");
			msg.append("	/*@editable*/font-weight:bold;");
			msg.append("	/*@editable*/line-height:125%;");
			msg.append("	/*@editable*/letter-spacing:normal;");
			msg.append("	/*@editable*/text-align:left;");
			msg.append("	}");
			msg.append("	/*");
			msg.append("	@tab Page");
			msg.append("	@section Heading 4");
			msg.append(
			"	@tip Set the styling for all fourth-level headings in your emails. These should be the smallest of your headings.");
			msg.append("	@style heading 4");
			msg.append("	*/");
			msg.append("	h4{");
			msg.append("	/*@editable*/color:#202020;");
			msg.append("	/*@editable*/font-family:Helvetica;");
			msg.append("	/*@editable*/font-size:18px;");
			msg.append("	/*@editable*/font-style:normal;");
			msg.append("	/*@editable*/font-weight:bold;");
			msg.append("	/*@editable*/line-height:125%;");
			msg.append("	/*@editable*/letter-spacing:normal;");
			msg.append("	/*@editable*/text-align:left;");
			msg.append("	}");
			msg.append("	/*");
			msg.append("	@tab Preheader");
			msg.append("	@section Preheader Style");
			msg.append("	@tip Set the background color and borders for your email's preheader area.");
			msg.append("	*/");
			msg.append("	#templatePreheader{");
			msg.append("	/*@editable*/background-color:#FAFAFA;");
			msg.append("	/*@editable*/background-image:none;");
			msg.append("	/*@editable*/background-repeat:no-repeat;");
			msg.append("	/*@editable*/background-position:center;");
			msg.append("	/*@editable*/background-size:cover;");
			msg.append("	/*@editable*/border-top:0;");
			msg.append("	/*@editable*/border-bottom:0;");
			msg.append("	/*@editable*/padding-top:9px;");
			msg.append("	/*@editable*/padding-bottom:9px;");
			msg.append("	}");
			msg.append("	/*");
			msg.append("	@tab Preheader");
			msg.append("	@section Preheader Text");
			msg.append(
			"	@tip Set the styling for your email's preheader text. Choose a size and color that is easy to read.");
			msg.append("	*/");
			msg.append("	#templatePreheader .mcnTextContent,#templatePreheader .mcnTextContent p{");
			msg.append("	/*@editable*/color:#656565;");
			msg.append("	/*@editable*/font-family:Helvetica;");
			msg.append("	/*@editable*/font-size:12px;");
			msg.append("	/*@editable*/line-height:150%;");
			msg.append("	/*@editable*/text-align:left;");
			msg.append("	}");
			msg.append("	/*");
			msg.append("	@tab Preheader");
			msg.append("	@section Preheader Link");
			msg.append(
			"	@tip Set the styling for your email's preheader links. Choose a color that helps them stand out from your text.");
			msg.append("	*/");
			msg.append("	#templatePreheader .mcnTextContent a,#templatePreheader .mcnTextContent p a{");
			msg.append("	/*@editable*/color:#656565;");
			msg.append("	/*@editable*/font-weight:normal;");
			msg.append("	/*@editable*/text-decoration:underline;");
			msg.append("	}");
			msg.append("	/*");
			msg.append("	@tab Header");
			msg.append("	@section Header Style");
			msg.append("	@tip Set the background color and borders for your email's header area.");
			msg.append("	*/");
			msg.append("	#templateHeader{");
			msg.append("	/*@editable*/background-color:#FFFFFF;");
			msg.append("	/*@editable*/background-image:none;");
			msg.append("	/*@editable*/background-repeat:no-repeat;");
			msg.append("	/*@editable*/background-position:center;");
			msg.append("	/*@editable*/background-size:cover;");
			msg.append("	/*@editable*/border-top:0;");
			msg.append("	/*@editable*/border-bottom:0;");
			msg.append("	/*@editable*/padding-top:9px;");
			msg.append("	/*@editable*/padding-bottom:0;");
			msg.append("	}");
			msg.append("	/*");
			msg.append("	@tab Header");
			msg.append("	@section Header Text");
			msg.append(
			"	@tip Set the styling for your email's header text. Choose a size and color that is easy to read.");
			msg.append("	*/");
			msg.append("	#templateHeader .mcnTextContent,#templateHeader .mcnTextContent p{");
			msg.append("	/*@editable*/color:#202020;");
			msg.append("	/*@editable*/font-family:Helvetica;");
			msg.append("	/*@editable*/font-size:16px;");
			msg.append("	/*@editable*/line-height:150%;");
			msg.append("	/*@editable*/text-align:left;");
			msg.append("	}");
			msg.append("	/*");
			msg.append("	@tab Header");
			msg.append("	@section Header Link");
			msg.append(
			"	@tip Set the styling for your email's header links. Choose a color that helps them stand out from your text.");
			msg.append("	*/");
			msg.append("	#templateHeader .mcnTextContent a,#templateHeader .mcnTextContent p a{");
			msg.append("	/*@editable*/color:#2BAADF;");
			msg.append("	/*@editable*/font-weight:normal;");
			msg.append("	/*@editable*/text-decoration:underline;");
			msg.append("	}");
			msg.append("	/*");
			msg.append("	@tab Body");
			msg.append("	@section Body Style");
			msg.append("	@tip Set the background color and borders for your email's body area.");
			msg.append("	*/");
			msg.append("	#templateBody{");
			msg.append("	/*@editable*/background-color:#FFFFFF;");
			msg.append("	/*@editable*/background-image:none;");
			msg.append("	/*@editable*/background-repeat:no-repeat;");
			msg.append("	/*@editable*/background-position:center;");
			msg.append("	/*@editable*/background-size:cover;");
			msg.append("	/*@editable*/border-top:0;");
			msg.append("	/*@editable*/border-bottom:2px solid #EAEAEA;");
			msg.append("	/*@editable*/padding-top:0;");
			msg.append("	/*@editable*/padding-bottom:9px;");
			msg.append("	}");
			msg.append("	/*");
			msg.append("	@tab Body");
			msg.append("	@section Body Text");
			msg.append(
			"	@tip Set the styling for your email's body text. Choose a size and color that is easy to read.");
			msg.append("	*/");
			msg.append("	#templateBody .mcnTextContent,#templateBody .mcnTextContent p{");
			msg.append("	/*@editable*/color:#202020;");
			msg.append("	/*@editable*/font-family:Helvetica;");
			msg.append("	/*@editable*/font-size:16px;");
			msg.append("	/*@editable*/line-height:150%;");
			msg.append("	/*@editable*/text-align:left;");
			msg.append("	}");
			msg.append("	/*");
			msg.append("	@tab Body");
			msg.append("	@section Body Link");
			msg.append(
			"	@tip Set the styling for your email's body links. Choose a color that helps them stand out from your text.");
			msg.append("	*/");
			msg.append("	#templateBody .mcnTextContent a,#templateBody .mcnTextContent p a{");
			msg.append("	/*@editable*/color:#2BAADF;");
			msg.append("	/*@editable*/font-weight:normal;");
			msg.append("	/*@editable*/text-decoration:underline;");
			msg.append("	}");
			msg.append("	/*");
			msg.append("	@tab Footer");
			msg.append("	@section Footer Style");
			msg.append("	@tip Set the background color and borders for your email's footer area.");
			msg.append("	*/");
			msg.append("	#templateFooter{");
			msg.append("	/*@editable*/background-color:#FAFAFA;");
			msg.append("	/*@editable*/background-image:none;");
			msg.append("	/*@editable*/background-repeat:no-repeat;");
			msg.append("	/*@editable*/background-position:center;");
			msg.append("	/*@editable*/background-size:cover;");
			msg.append("	/*@editable*/border-top:0;");
			msg.append("	/*@editable*/border-bottom:0;");
			msg.append("	/*@editable*/padding-top:9px;");
			msg.append("	/*@editable*/padding-bottom:9px;");
			msg.append("	}");
			msg.append("	/*");
			msg.append("	@tab Footer");
			msg.append("	@section Footer Text");
			msg.append(
			"	@tip Set the styling for your email's footer text. Choose a size and color that is easy to read.");
			msg.append("	*/");
			msg.append("	#templateFooter .mcnTextContent,#templateFooter .mcnTextContent p{");
			msg.append("	/*@editable*/color:#656565;");
			msg.append("	/*@editable*/font-family:Helvetica;");
			msg.append("	/*@editable*/font-size:12px;");
			msg.append("	/*@editable*/line-height:150%;");
			msg.append("	/*@editable*/text-align:center;");
			msg.append("	}");
			msg.append("	/*");
			msg.append("	@tab Footer");
			msg.append("	@section Footer Link");
			msg.append(
			"	@tip Set the styling for your email's footer links. Choose a color that helps them stand out from your text.");
			msg.append("	*/");
			msg.append("	#templateFooter .mcnTextContent a,#templateFooter .mcnTextContent p a{");
			msg.append("	/*@editable*/color:#656565;");
			msg.append("	/*@editable*/font-weight:normal;");
			msg.append("	/*@editable*/text-decoration:underline;");
			msg.append("	}");
			msg.append("	@media only screen and (min-width:768px){");
			msg.append("	.templateContainer{");
			msg.append("	width:600px !important;");
			msg.append("	}");
			msg.append("");
			msg.append("}	@media only screen and (max-width: 480px){");
			msg.append("	body,table,td,p,a,li,blockquote{");
			msg.append("	-webkit-text-size-adjust:none !important;");
			msg.append("	}");
			msg.append("");
			msg.append("}	@media only screen and (max-width: 480px){");
			msg.append("	body{");
			msg.append("	width:100% !important;");
			msg.append("	min-width:100% !important;");
			msg.append("	}");
			msg.append("");
			msg.append("}	@media only screen and (max-width: 480px){");
			msg.append("	#bodyCell{");
			msg.append("	padding-top:10px !important;");
			msg.append("	}");
			msg.append("");
			msg.append("}	@media only screen and (max-width: 480px){");
			msg.append("	.mcnRetinaImage{");
			msg.append("	max-width:100% !important;");
			msg.append("	}");
			msg.append("");
			msg.append("}	@media only screen and (max-width: 480px){");
			msg.append("	.mcnImage{");
			msg.append("	width:100% !important;");
			msg.append("	}");
			msg.append("");
			msg.append("}	@media only screen and (max-width: 480px){");
			msg.append(
			"	.mcnCartContainer,.mcnCaptionTopContent,.mcnRecContentContainer,.mcnCaptionBottomContent,.mcnTextContentContainer,.mcnBoxedTextContentContainer,.mcnImageGroupContentContainer,.mcnCaptionLeftTextContentContainer,.mcnCaptionRightTextContentContainer,.mcnCaptionLeftImageContentContainer,.mcnCaptionRightImageContentContainer,.mcnImageCardLeftTextContentContainer,.mcnImageCardRightTextContentContainer,.mcnImageCardLeftImageContentContainer,.mcnImageCardRightImageContentContainer{");
			msg.append("	max-width:100% !important;");
			msg.append("	width:100% !important;");
			msg.append("	}");
			msg.append("");
			msg.append("}	@media only screen and (max-width: 480px){");
			msg.append("	.mcnBoxedTextContentContainer{");
			msg.append("	min-width:100% !important;");
			msg.append("	}");
			msg.append("");
			msg.append("}	@media only screen and (max-width: 480px){");
			msg.append("	.mcnImageGroupContent{");
			msg.append("	padding:9px !important;");
			msg.append("	}");
			msg.append("");
			msg.append("}	@media only screen and (max-width: 480px){");
			msg.append(
			"	.mcnCaptionLeftContentOuter .mcnTextContent,.mcnCaptionRightContentOuter .mcnTextContent{");
			msg.append("	padding-top:9px !important;");
			msg.append("	}");
			msg.append("");
			msg.append("}	@media only screen and (max-width: 480px){");
			msg.append(
			"	.mcnImageCardTopImageContent,.mcnCaptionBottomContent:last-child .mcnCaptionBottomImageContent,.mcnCaptionBlockInner .mcnCaptionTopContent:last-child .mcnTextContent{");
			msg.append("	padding-top:18px !important;");
			msg.append("	}");
			msg.append("");
			msg.append("}	@media only screen and (max-width: 480px){");
			msg.append("	.mcnImageCardBottomImageContent{");
			msg.append("	padding-bottom:9px !important;");
			msg.append("	}");
			msg.append("");
			msg.append("}	@media only screen and (max-width: 480px){");
			msg.append("	.mcnImageGroupBlockInner{");
			msg.append("	padding-top:0 !important;");
			msg.append("	padding-bottom:0 !important;");
			msg.append("	}");
			msg.append("");
			msg.append("}	@media only screen and (max-width: 480px){");
			msg.append("	.mcnImageGroupBlockOuter{");
			msg.append("	padding-top:9px !important;");
			msg.append("	padding-bottom:9px !important;");
			msg.append("	}");
			msg.append("");
			msg.append("}	@media only screen and (max-width: 480px){");
			msg.append("	.mcnTextContent,.mcnBoxedTextContentColumn{");
			msg.append("	padding-right:18px !important;");
			msg.append("	padding-left:18px !important;");
			msg.append("	}");
			msg.append("");
			msg.append("}	@media only screen and (max-width: 480px){");
			msg.append("	.mcnImageCardLeftImageContent,.mcnImageCardRightImageContent{");
			msg.append("	padding-right:18px !important;");
			msg.append("	padding-bottom:0 !important;");
			msg.append("	padding-left:18px !important;");
			msg.append("	}");
			msg.append("");
			msg.append("}	@media only screen and (max-width: 480px){");
			msg.append("	.mcpreview-image-uploader{");
			msg.append("	display:none !important;");
			msg.append("	width:100% !important;");
			msg.append("	}");
			msg.append("");
			msg.append("}	@media only screen and (max-width: 480px){");
			msg.append("	/*");
			msg.append("	@tab Mobile Styles");
			msg.append("	@section Heading 1");
			msg.append(
			"	@tip Make the first-level headings larger in size for better readability on small screens.");
			msg.append("	*/");
			msg.append("	h1{");
			msg.append("	/*@editable*/font-size:22px !important;");
			msg.append("	/*@editable*/line-height:125% !important;");
			msg.append("	}");
			msg.append("");
			msg.append("}	@media only screen and (max-width: 480px){");
			msg.append("	/*");
			msg.append("	@tab Mobile Styles");
			msg.append("	@section Heading 2");
			msg.append(
			"	@tip Make the second-level headings larger in size for better readability on small screens.");
			msg.append("	*/");
			msg.append("	h2{");
			msg.append("	/*@editable*/font-size:20px !important;");
			msg.append("	/*@editable*/line-height:125% !important;");
			msg.append("	}");
			msg.append("");
			msg.append("}	@media only screen and (max-width: 480px){");
			msg.append("	/*");
			msg.append("	@tab Mobile Styles");
			msg.append("	@section Heading 3");
			msg.append(
			"	@tip Make the third-level headings larger in size for better readability on small screens.");
			msg.append("	*/");
			msg.append("	h3{");
			msg.append("	/*@editable*/font-size:18px !important;");
			msg.append("	/*@editable*/line-height:125% !important;");
			msg.append("	}");
			msg.append("");
			msg.append("}	@media only screen and (max-width: 480px){");
			msg.append("	/*");
			msg.append("	@tab Mobile Styles");
			msg.append("	@section Heading 4");
			msg.append(
			"	@tip Make the fourth-level headings larger in size for better readability on small screens.");
			msg.append("	*/");
			msg.append("	h4{");
			msg.append("	/*@editable*/font-size:16px !important;");
			msg.append("	/*@editable*/line-height:150% !important;");
			msg.append("	}");
			msg.append("");
			msg.append("}	@media only screen and (max-width: 480px){");
			msg.append("	/*");
			msg.append("	@tab Mobile Styles");
			msg.append("	@section Boxed Text");
			msg.append(
			"	@tip Make the boxed text larger in size for better readability on small screens. We recommend a font size of at least 16px.");
			msg.append("	*/");
			msg.append(
			"	.mcnBoxedTextContentContainer .mcnTextContent,.mcnBoxedTextContentContainer .mcnTextContent p{");
			msg.append("	/*@editable*/font-size:14px !important;");
			msg.append("	/*@editable*/line-height:150% !important;");
			msg.append("	}");
			msg.append("");
			msg.append("}	@media only screen and (max-width: 480px){");
			msg.append("	/*");
			msg.append("	@tab Mobile Styles");
			msg.append("	@section Preheader Visibility");
			msg.append(
			"	@tip Set the visibility of the email's preheader on small screens. You can hide it to save space.");
			msg.append("	*/");
			msg.append("	#templatePreheader{");
			msg.append("	/*@editable*/display:block !important;");
			msg.append("	}");
			msg.append("");
			msg.append("}	@media only screen and (max-width: 480px){");
			msg.append("	/*");
			msg.append("	@tab Mobile Styles");
			msg.append("	@section Preheader Text");
			msg.append("	@tip Make the preheader text larger in size for better readability on small screens.");
			msg.append("	*/");
			msg.append("	#templatePreheader .mcnTextContent,#templatePreheader .mcnTextContent p{");
			msg.append("	/*@editable*/font-size:14px !important;");
			msg.append("	/*@editable*/line-height:150% !important;");
			msg.append("	}");
			msg.append("");
			msg.append("}	@media only screen and (max-width: 480px){");
			msg.append("	/*");
			msg.append("	@tab Mobile Styles");
			msg.append("	@section Header Text");
			msg.append("	@tip Make the header text larger in size for better readability on small screens.");
			msg.append("	*/");
			msg.append("	#templateHeader .mcnTextContent,#templateHeader .mcnTextContent p{");
			msg.append("	/*@editable*/font-size:16px !important;");
			msg.append("	/*@editable*/line-height:150% !important;");
			msg.append("	}");
			msg.append("");
			msg.append("}	@media only screen and (max-width: 480px){");
			msg.append("	/*");
			msg.append("	@tab Mobile Styles");
			msg.append("	@section Body Text");
			msg.append(
			"	@tip Make the body text larger in size for better readability on small screens. We recommend a font size of at least 16px.");
			msg.append("	*/");
			msg.append("	#templateBody .mcnTextContent,#templateBody .mcnTextContent p{");
			msg.append("	/*@editable*/font-size:16px !important;");
			msg.append("	/*@editable*/line-height:150% !important;");
			msg.append("	}");
			msg.append("");
			msg.append("}	@media only screen and (max-width: 480px){");
			msg.append("	/*");
			msg.append("	@tab Mobile Styles");
			msg.append("	@section Footer Text");
			msg.append("	@tip Make the footer content text larger in size for better readability on small screens.");
			msg.append("	*/");
			msg.append("	#templateFooter .mcnTextContent,#templateFooter .mcnTextContent p{");
			msg.append("	/*@editable*/font-size:14px !important;");
			msg.append("	/*@editable*/line-height:150% !important;");
			msg.append("	}");
			msg.append("");
			msg.append("}</style></head>");
			msg.append("    <body>");
			msg.append("	<!--*|IF:MC_PREVIEW_TEXT|*-->");
			msg.append(
			"	<!--[if !gte mso 9]><!----><span class=\"mcnPreviewText\" style=\"display:none; font-size:0px; line-height:0px; max-height:0px; max-width:0px; opacity:0; overflow:hidden; visibility:hidden; mso-hide:all;\">*|MC_PREVIEW_TEXT|*</span><!--<![endif]-->");
			msg.append("	<!--*|END:IF|*-->");
			msg.append("        <center>");
			msg.append(
			"            <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" height=\"100%\" width=\"100%\" id=\"bodyTable\">");
			msg.append("                <tr>");
			msg.append("                    <td align=\"center\" valign=\"top\" id=\"bodyCell\">");
			msg.append("                        <!-- BEGIN TEMPLATE // -->");
			msg.append("	<!--[if (gte mso 9)|(IE)]>");
			msg.append(
			"	<table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"600\" style=\"width:600px;\">");
			msg.append("	<tr>");
			msg.append(
			"	<td align=\"center\" valign=\"top\" width=\"600\" style=\"width:600px;\">");
			msg.append("	<![endif]-->");
			msg.append(
			"                        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" class=\"templateContainer\">");
			msg.append("                            <tr>");
			msg.append(
			"                                <td valign=\"top\" id=\"templatePreheader\"><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" class=\"mcnTextBlock\" style=\"min-width:100%;\">");
			msg.append("    <tbody class=\"mcnTextBlockOuter\">");
			msg.append("        <tr>");
			msg.append("            <td valign=\"top\" class=\"mcnTextBlockInner\" style=\"padding-top:9px;\">");
			msg.append("              	<!--[if mso]>");
			msg.append(
			"	<table align=\"left\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\" style=\"width:100%;\">");
			msg.append("	<tr>");
			msg.append("	<![endif]-->");
			msg.append("	    ");
			msg.append("	<!--[if mso]>");
			msg.append("	<td valign=\"top\" width=\"600\" style=\"width:600px;\">");
			msg.append("	<![endif]-->");
			msg.append(
			"                <table align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"max-width:100%; min-width:100%;\" width=\"100%\" class=\"mcnTextContentContainer\">");
			msg.append("                    <tbody><tr>");
			msg.append("                        ");
			msg.append(
			"                        <td valign=\"top\" class=\"mcnTextContent\" style=\"padding: 0px 18px 9px; text-align: center;\">");
			msg.append("                        ");
			msg.append(
			"                            <a href=\"*|ARCHIVE|*\" target=\"_blank\">View this email in your browser</a>");
			msg.append("                        </td>");
			msg.append("                    </tr>");
			msg.append("                </tbody></table>");
			msg.append("	<!--[if mso]>");
			msg.append("	</td>");
			msg.append("	<![endif]-->");
			msg.append("                ");
			msg.append("	<!--[if mso]>");
			msg.append("	</tr>");
			msg.append("	</table>");
			msg.append("	<![endif]-->");
			msg.append("            </td>");
			msg.append("        </tr>");
			msg.append("    </tbody>");
			msg.append("</table></td>");
			msg.append("                            </tr>");
			msg.append("                            <tr>");
			msg.append(
			"                                <td valign=\"top\" id=\"templateHeader\"><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" class=\"mcnImageBlock\" style=\"min-width:100%;\">");
			msg.append("    <tbody class=\"mcnImageBlockOuter\">");
			msg.append("            <tr>");
			msg.append("                <td valign=\"top\" style=\"padding:9px\" class=\"mcnImageBlockInner\">");
			msg.append(
			"                    <table align=\"left\" width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnImageContentContainer\" style=\"min-width:100%;\">");
			msg.append("                        <tbody><tr>");
			msg.append(
			"                            <td class=\"mcnImageContent\" valign=\"top\" style=\"padding-right: 9px; padding-left: 9px; padding-top: 0; padding-bottom: 0; text-align:center;\">");
			msg.append("                                ");
			msg.append("                                    ");
			msg.append("                                        <img align=\"center\" alt=\"\" src=\"" + merchantLogo
			+ "\" width=\"564\" style=\"max-width:200px; padding-bottom: 0; display: inline !important; vertical-align: bottom;\" class=\"mcnImage\">");
			msg.append("                                    ");
			msg.append("                                ");
			msg.append("                            </td>");
			msg.append("                        </tr>");
			msg.append("                    </tbody></table>");
			msg.append("                </td>");
			msg.append("            </tr>");
			msg.append("    </tbody>");
			msg.append("</table></td>");
			msg.append("                            </tr>");
			msg.append("                            <tr>");
			msg.append(
			"                                <td valign=\"top\" id=\"templateBody\"><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" class=\"mcnTextBlock\" style=\"min-width:100%;\">");
			msg.append("    <tbody class=\"mcnTextBlockOuter\">");
			msg.append("        <tr>");
			msg.append("            <td valign=\"top\" class=\"mcnTextBlockInner\" style=\"padding-top:9px;\">");
			msg.append("              	<!--[if mso]>");
			msg.append(
			"	<table align=\"left\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\" style=\"width:100%;\">");
			msg.append("	<tr>");
			msg.append("	<![endif]-->");
			msg.append("	    ");
			msg.append("	<!--[if mso]>");
			msg.append("	<td valign=\"top\" width=\"600\" style=\"width:600px;\">");
			msg.append("	<![endif]-->");
			msg.append(
			"                <table align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"max-width:100%; min-width:100%;\" width=\"100%\" class=\"mcnTextContentContainer\">");
			msg.append("                    <tbody><tr>");
			msg.append("                        ");
			msg.append(
			"                        <td valign=\"top\" class=\"mcnTextContent\" style=\"padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;\">");
			msg.append("                        ");
			/*
			 * msg.
			 * append("                            <h1><strong style=\"font-size:20px\">Your Feedback Matters: "
			 * + "<a href=\""); msg.append(url) msg.append(
			 * "\" target=\"_blank\">Kritiq Us</a></strong></h1>");
			 */
			msg.append(
			"<h1><strong style=\"font-size:20px\">Your Feedback Matters: <a href=\"https://kritiq.us/\" target=\"_blank\">Kritiq Us</a></strong></h1>");
			msg.append("<br>");
			msg.append("Dear Customer,<br>");
			msg.append("                      <br>");
			msg.append("Thank you for your recent visit. <br>");
			msg.append("<br>");
			msg.append(
			"Your feedback and suggestions are valuable to us. Help us to improve by filling out the 2-minute <a href=\""
			+ url + "\" target=\"_blank\">feedback form</a>.<br>");
			msg.append("<br>");
			// msg.append("Best,<br>"); merchantName, String merchantAddress
			/*
			 * msg.append("merchantName - Royse City");
			 */ msg.append(merchantName);
			msg.append(" -" + merchantAddress);
			msg.append("                        </td>");
			msg.append("                    </tr>");
			msg.append("                </tbody></table>");
			msg.append("	<!--[if mso]>");
			msg.append("	</td>");
			msg.append("	<![endif]-->");
			msg.append("                ");
			msg.append("	<!--[if mso]>");
			msg.append("	</tr>");
			msg.append("	</table>");
			msg.append("	<![endif]-->");
			msg.append("            </td>");
			msg.append("        </tr>");
			msg.append("    </tbody>");
			msg.append("</table></td>");
			msg.append("                            </tr>");
			msg.append("                            <tr>");
			msg.append(
			"                                <td valign=\"top\" id=\"templateFooter\"><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" class=\"mcnFollowBlock\" style=\"min-width:100%;\">");
			msg.append("    <tbody class=\"mcnFollowBlockOuter\">");
			msg.append("        <tr>");
			msg.append(
			"            <td align=\"center\" valign=\"top\" style=\"padding:9px\" class=\"mcnFollowBlockInner\">");
			msg.append(
			"                <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" class=\"mcnFollowContentContainer\" style=\"min-width:100%;\">");
			msg.append("    <tbody><tr>");
			msg.append("        <td align=\"center\" style=\"padding-left:9px;padding-right:9px;\">");
			msg.append(
			"            <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"min-width:100%;\" class=\"mcnFollowContent\">");
			msg.append("                <tbody><tr>");
			msg.append(
			"                    <td align=\"center\" valign=\"top\" style=\"padding-top:9px; padding-right:9px; padding-left:9px;\">");
			msg.append(
			"                        <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">");
			msg.append("                            <tbody><tr>");
			msg.append("                                <td align=\"center\" valign=\"top\">");
			msg.append("                                    <!--[if mso]>");
			msg.append(
			"                                    <table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">");
			msg.append("                                    <tr>");
			msg.append("                                    <![endif]-->");
			msg.append("                                    ");
			msg.append("                                        <!--[if mso]>");
			msg.append("                                        <td align=\"center\" valign=\"top\">");
			msg.append("                                        <![endif]-->");
			msg.append("                                        ");
			msg.append("                                        ");
			msg.append(
			"                                            <table align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"display:inline;\">");
			msg.append("                                                <tbody><tr>");
			msg.append(
			"                                                    <td valign=\"top\" style=\"padding-right:10px; padding-bottom:9px;\" class=\"mcnFollowContentItemContainer\">");
			msg.append(
			"                                                        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" class=\"mcnFollowContentItem\">");
			msg.append("                                                            <tbody><tr>");
			msg.append(
			"                                                                <td align=\"left\" valign=\"middle\" style=\"padding-top:5px; padding-right:10px; padding-bottom:5px; padding-left:9px;\">");
			msg.append(
			"                                                                    <table align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"\">");
			msg.append("                                                                        <tbody><tr>");
			msg.append("                                                                            ");
			msg.append(
			"                                                                                <td align=\"center\" valign=\"middle\" width=\"24\" class=\"mcnFollowIconContent\">");
			msg.append(
			"                                                                                    <a href=\"http://www.twitter.com/\" target=\"_blank\"><img src=\"https://cdn-images.mailchimp.com/icons/social-block-v2/color-twitter-48.png\" style=\"display:block;\" height=\"24\" width=\"24\" class=\"\"></a>");
			msg.append("                                                                                </td>");
			msg.append("                                                                            ");
			msg.append("                                                                            ");
			msg.append("                                                                        </tr>");
			msg.append("                                                                    </tbody></table>");
			msg.append("                                                                </td>");
			msg.append("                                                            </tr>");
			msg.append("                                                        </tbody></table>");
			msg.append("                                                    </td>");
			msg.append("                                                </tr>");
			msg.append("                                            </tbody></table>");
			msg.append("                                        ");
			msg.append("                                        <!--[if mso]>");
			msg.append("                                        </td>");
			msg.append("                                        <![endif]-->");
			msg.append("                                    ");
			msg.append("                                        <!--[if mso]>");
			msg.append("                                        <td align=\"center\" valign=\"top\">");
			msg.append("                                        <![endif]-->");
			msg.append("                                        ");
			msg.append("                                        ");
			msg.append(
			"                                            <table align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"display:inline;\">");
			msg.append("                                                <tbody><tr>");
			msg.append(
			"                                                    <td valign=\"top\" style=\"padding-right:10px; padding-bottom:9px;\" class=\"mcnFollowContentItemContainer\">");
			msg.append(
			"                                                        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" class=\"mcnFollowContentItem\">");
			msg.append("                                                            <tbody><tr>");
			msg.append(
			"                                                                <td align=\"left\" valign=\"middle\" style=\"padding-top:5px; padding-right:10px; padding-bottom:5px; padding-left:9px;\">");
			msg.append(
			"                                                                    <table align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"\">");
			msg.append("                                                                        <tbody><tr>");
			msg.append("                                                                            ");
			msg.append(
			"                                                                                <td align=\"center\" valign=\"middle\" width=\"24\" class=\"mcnFollowIconContent\">");
			msg.append(
			"                                                                                    <a href=\"http://www.facebook.com\" target=\"_blank\"><img src=\"https://cdn-images.mailchimp.com/icons/social-block-v2/color-facebook-48.png\" style=\"display:block;\" height=\"24\" width=\"24\" class=\"\"></a>");
			msg.append("                                                                                </td>");
			msg.append("                                                                            ");
			msg.append("                                                                            ");
			msg.append("                                                                        </tr>");
			msg.append("                                                                    </tbody></table>");
			msg.append("                                                                </td>");
			msg.append("                                                            </tr>");
			msg.append("                                                        </tbody></table>");
			msg.append("                                                    </td>");
			msg.append("                                                </tr>");
			msg.append("                                            </tbody></table>");
			msg.append("                                        ");
			msg.append("                                        <!--[if mso]>");
			msg.append("                                        </td>");
			msg.append("                                        <![endif]-->");
			msg.append("                                    ");
			msg.append("                                        <!--[if mso]>");
			msg.append("                                        <td align=\"center\" valign=\"top\">");
			msg.append("                                        <![endif]-->");
			msg.append("                                        ");
			msg.append("                                        ");
			msg.append(
			"                                            <table align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"display:inline;\">");
			msg.append("                                                <tbody><tr>");
			msg.append(
			"                                                    <td valign=\"top\" style=\"padding-right:0; padding-bottom:9px;\" class=\"mcnFollowContentItemContainer\">");
			msg.append(
			"                                                        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" class=\"mcnFollowContentItem\">");
			msg.append("                                                            <tbody><tr>");
			msg.append(
			"                                                                <td align=\"left\" valign=\"middle\" style=\"padding-top:5px; padding-right:10px; padding-bottom:5px; padding-left:9px;\">");
			msg.append(
			"                                                                    <table align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"\">");
			msg.append("                                                                        <tbody><tr>");
			msg.append("                                                                            ");
			msg.append(
			"                                                                                <td align=\"center\" valign=\"middle\" width=\"24\" class=\"mcnFollowIconContent\">");
			msg.append(
			"                                                                                    <a href=\"http://mailchimp.com\" target=\"_blank\"><img src=\"https://cdn-images.mailchimp.com/icons/social-block-v2/color-link-48.png\" style=\"display:block;\" height=\"24\" width=\"24\" class=\"\"></a>");
			msg.append("                                                                                </td>");
			msg.append("                                                                            ");
			msg.append("                                                                            ");
			msg.append("                                                                        </tr>");
			msg.append("                                                                    </tbody></table>");
			msg.append("                                                                </td>");
			msg.append("                                                            </tr>");
			msg.append("                                                        </tbody></table>");
			msg.append("                                                    </td>");
			msg.append("                                                </tr>");
			msg.append("                                            </tbody></table>");
			msg.append("                                        ");
			msg.append("                                        <!--[if mso]>");
			msg.append("                                        </td>");
			msg.append("                                        <![endif]-->");
			msg.append("                                    ");
			msg.append("                                    <!--[if mso]>");
			msg.append("                                    </tr>");
			msg.append("                                    </table>");
			msg.append("                                    <![endif]-->");
			msg.append("                                </td>");
			msg.append("                            </tr>");
			msg.append("                        </tbody></table>");
			msg.append("                    </td>");
			msg.append("                </tr>");
			msg.append("            </tbody></table>");
			msg.append("        </td>");
			msg.append("    </tr>");
			msg.append("</tbody></table>");
			msg.append("");
			msg.append("            </td>");
			msg.append("        </tr>");
			msg.append("    </tbody>");
			msg.append(
			"</table><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" class=\"mcnDividerBlock\" style=\"min-width:100%;\">");
			msg.append("    <tbody class=\"mcnDividerBlockOuter\">");
			msg.append("        <tr>");
			msg.append(
			"            <td class=\"mcnDividerBlockInner\" style=\"min-width: 100%; padding: 10px 18px 25px;\">");
			msg.append(
			"                <table class=\"mcnDividerContent\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"min-width: 100%;border-top: 2px solid #EEEEEE;\">");
			msg.append("                    <tbody><tr>");
			msg.append("                        <td>");
			msg.append("                            <span></span>");
			msg.append("                        </td>");
			msg.append("                    </tr>");
			msg.append("                </tbody></table>");
			msg.append("<!--            ");
			msg.append("                <td class=\"mcnDividerBlockInner\" style=\"padding: 18px;\">");
			msg.append(
			"                <hr class=\"mcnDividerContent\" style=\"border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;\" />");
			msg.append("-->");
			msg.append("            </td>");
			msg.append("        </tr>");
			msg.append("    </tbody>");
			msg.append(
			"</table><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" class=\"mcnTextBlock\" style=\"min-width:100%;\">");
			msg.append("    <tbody class=\"mcnTextBlockOuter\">");
			msg.append("        <tr>");
			msg.append("            <td valign=\"top\" class=\"mcnTextBlockInner\" style=\"padding-top:9px;\">");
			msg.append("              	<!--[if mso]>");
			msg.append(
			"	<table align=\"left\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\" style=\"width:100%;\">");
			msg.append("	<tr>");
			msg.append("	<![endif]-->");
			msg.append("	    ");
			msg.append("	<!--[if mso]>");
			msg.append("	<td valign=\"top\" width=\"600\" style=\"width:600px;\">");
			msg.append("	<![endif]-->");
			msg.append(
			"                <table align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"max-width:100%; min-width:100%;\" width=\"100%\" class=\"mcnTextContentContainer\">");
			msg.append("                    <tbody><tr>");
			msg.append("                        ");
			msg.append(
			"                        <td valign=\"top\" class=\"mcnTextContent\" style=\"padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;\">");
			msg.append("                        ");
			msg.append("   ");
			msg.append("");
			msg.append("                        </td>");
			msg.append("                    </tr>");
			msg.append("                </tbody></table>");
			msg.append("	<!--[if mso]>");
			msg.append("	</td>");
			msg.append("	<![endif]-->");
			msg.append("                ");
			msg.append("	<!--[if mso]>");
			msg.append("	</tr>");
			msg.append("	</table>");
			msg.append("	<![endif]-->");
			msg.append("            </td>");
			msg.append("        </tr>");
			msg.append("    </tbody>");
			msg.append("</table></td>");
			msg.append("                            </tr>");
			msg.append("                        </table>");
			msg.append("	<!--[if (gte mso 9)|(IE)]>");
			msg.append("	</td>");
			msg.append("	</tr>");
			msg.append("	</table>");
			msg.append("	<![endif]-->");
			msg.append("                        <!-- // END TEMPLATE -->");
			msg.append("                    </td>");
			msg.append("                </tr>");
			msg.append("            </table>");
			msg.append("        </center>");
			msg.append("    </body>");
			msg.append("</html>");

			message.setContent(msg.toString(), "text/html");
			Transport.send(message);
			} catch (Exception e) {
				LOGGER.error("===============  MailSendUtil : Inside sendKritiqUrlMailToCustomer :: Exception  ============= " + e);

			LOGGER.error("error: " + e.getMessage());
			}
			LOGGER.info("===============  MailSendUtil : Inside sendKritiqUrlMailToCustomer :: End  ============= ");

			return true;
			}
	
/*-----------------------------------------------------------------------------------	*/
//MailSendUtil of design V2
	
	
	
	public static String forgotPasswordEmailForMultiLocationCustomerV2(Customer customer,Integer vendorId,Environment environment) {
		try {
			LOGGER.info("===============  MailSendUtil : Inside forgotPasswordEmailForMultiLocationCustomerV2 :: Start  =============vendorId "+vendorId);

			String cusID = EncryptionDecryptionUtil.encryptString(String.valueOf(customer.getId()));
			String marchantId = String.valueOf(customer.getMerchantt().getId());
			String timeLog = String.valueOf(System.currentTimeMillis());
			Message message = new MimeMessage(
					SendMailProperty.gmailMailProperty(IConstant.FROM_EMAIL_ID, IConstant.FROM_PASSWORD));
			message.setFrom(new InternetAddress(IConstant.FROM_EMAIL_ID));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(customer.getEmailId()));
			message.setSubject("Regards:Forgot password");
			/*String msg = "Dear " + customer.getFirstName() + "<br>";
			msg += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;We have received a request to reset your password .Please find your reset password link below::<br>";
			msg += " <br>";
			msg += " <br>";
			msg += "Click to : " + environment.getProperty("WEB_BASE_URL") + "/changepassword?email=" + customer.getEmailId()
					+ "&merchantId=" + marchantId + "&userId=" + cusID + "&tLog="
					+ EncryptionDecryptionUtil.encryption(timeLog) + "<br>";
			msg += " <br>";
			msg += " <br>";
			msg += " <br>";
			msg += "<b>Regards,</b><br>";
			msg += "FoodKonnekt";*/
			
			String merchantLogo="";
			if (customer.getMerchantt().getMerchantLogo() == null) {
                merchantLogo = "http://www.dev.foodkonnekt.com/app/foodnew.jpg";
            } else {
                merchantLogo = environment.getProperty("BASE_PORT") + customer.getMerchantt().getMerchantLogo();
            }
			
			String msg = "<div style='width: 700px; height: auto; border: solid 1px #ddd; box-shadow: 0px 0px 6px #ddd; background-color: #f2f2f2; text-align: center; margin: 40px 0px 0px 35px;'>"
					+ "<div style='width: 700px; height: 85px; padding: 20px 0px; text-align: center;'>&nbsp;<img src='"
					+ merchantLogo + "' height='80' /></div>"
					+ "<div style='background-color: #fff; padding: 20px 0px 0px 0px; width: 90%; margin: 0 auto 65px auto;'>"
					+ "<p style='font-size: 20px; color: #e6880f;'>Dear ";
					if(customer.getFirstName()!= null) {
						msg +=  customer.getFirstName() + " ";
					}
					if(customer.getLastName()!= null) {
						msg += customer.getLastName()  ;
					}
					msg += ",</p><p style='font-size: 14px;text-align: left;padding-left:10px;'>We have received a request to reset your password .Please find your reset password link below: &nbsp;</p>";
			msg = msg+"<br /><br /> <a style='text-decoration: none; margin: 16px 0px; display: inline-block; background-color: #f8981d; color: #fff; padding: 10px 12px; border-radius: 10px; font-weight: 600; font-size: 15px;' href='";
				
			Integer merchantId = Integer.parseInt(marchantId);
			
			/*if(merchantId.equals(IConstant.TEXAN_REWARDS_MERCHANT_ID)){
				msg = msg+ environment.getProperty("TEXAN_BASE_URL") + "/changepassword?email=" + customer.getEmailId()
						+ "&merchantId=" + marchantId + "&userId=" + cusID + "&tLog="
						+ EncryptionDecryptionUtil.encryption(timeLog);
			}else{
				msg = msg+ environment.getProperty("WEB_BASE_URL") + "/changepassword?email=" + customer.getEmailId()
						+ "&merchantId=" + marchantId + "&userId=" + cusID + "&tLog="
						+ EncryptionDecryptionUtil.encryption(timeLog);
			}*/
			
			msg = msg+ environment.getProperty("WEB_BASE_URL") + "/changepassword?email=" + customer.getEmailId()
					+ "&merchantId=" + marchantId + "&userId=" + cusID + "&tLog="
					+ EncryptionDecryptionUtil.encryption(timeLog);
					
			msg=msg+"' target='_blank'>Click here</a><br /> Note: Link will valid till next 30 min only .";

			msg = msg
					+ "<p><Order details: items, extras, price, fees, promo discount, total $><br>For any further questions, ";
			if(customer.getMerchantt().getPhoneNumber()!=null) { 
				msg = msg +"Please call us at " + customer.getMerchantt().getPhoneNumber() + "";}
			else{
				msg = msg +"Please call our store";
			}
					msg = msg +" <br><br>Regards,<br>" + customer.getMerchantt().getName()
					+ "<br><br>";
			msg = msg + "</div>";
			msg = msg + "<p>We appreciate your business with us!</p>";
			/*if( customer.getMerchantId()!=null &&  customer.getMerchantId().equals(IConstant.TEXAN_REWARDS_MERCHANT_ID) ){
				msg = msg + "<div style='height: 70px; text-align: center;'>&nbsp;<img src='https://www.foodkonnekt.com/foodkonnekt_merchat_logo/274_PiePizzaria.png' alt='' width='200' height='63' /></div>"
						+ "</div>";
			}else{*/
				msg = msg + "<div style='height: 70px; text-align: center;'>&nbsp;<img src='http://www.dev.foodkonnekt.com/app/foodnew.jpg' alt='' width='200' height='63' /></div>"
					+ "</div>";
			//}
			msg = msg + "<p style='color: #676767; font-size: 11px;'>Automated email. Please do not reply to this sender email.</p>";
			
			
			message.setContent(msg, "text/html");
			Transport.send(message);
		} catch (MessagingException e) {
			LOGGER.error("===============  MailSendUtil : Inside forgotPasswordEmailForMultiLocationCustomerV2 :: Exception  ============= " + e);
			throw new RuntimeException(e);
		}
		LOGGER.info("===============  MailSendUtil : Inside forgotPasswordEmailForMultiLocationCustomerV2 :: End  ============= ");

		return null;
	}
	
	
	public static String forgotPasswordEmailV2(Customer customer,Environment environment) {
		try {
			LOGGER.info("===============  MailSendUtil : Inside forgotPasswordEmailV2 :: Start  ============= ");

			String cusID = EncryptionDecryptionUtil.encryptString(String.valueOf(customer.getId()));
			String marchantId = String.valueOf(customer.getMerchantt().getId());
			String timeLog = String.valueOf(System.currentTimeMillis());
			Message message = new MimeMessage(
					SendMailProperty.gmailMailProperty(IConstant.FROM_EMAIL_ID, IConstant.FROM_PASSWORD));
			message.setFrom(new InternetAddress(IConstant.FROM_EMAIL_ID));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(customer.getEmailId()));
			message.setSubject("Regards:Forgot password");
			/*String msg = "Dear " + customer.getFirstName() + "<br>";
			msg += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;We have received a request to reset your password .Please find your reset password link below::<br>";
			msg += " <br>";
			msg += " <br>";
			msg += "Click to : " + environment.getProperty("WEB_BASE_URL") + "/changepassword?email=" + customer.getEmailId()
					+ "&merchantId=" + marchantId + "&userId=" + cusID + "&tLog="
					+ EncryptionDecryptionUtil.encryption(timeLog) + "<br>";
			msg += " <br>";
			msg += " <br>";
			msg += " <br>";
			msg += "<b>Regards,</b><br>";
			msg += "FoodKonnekt";*/
			
			String merchantLogo="";
			if (customer.getMerchantt().getMerchantLogo() == null) {
                merchantLogo = "http://www.dev.foodkonnekt.com/app/foodnew.jpg";
            } else {
                merchantLogo = environment.getProperty("BASE_PORT") + customer.getMerchantt().getMerchantLogo();
            }
			
			String msg = "<div style='width: 700px; height: auto; border: solid 1px #ddd; box-shadow: 0px 0px 6px #ddd; background-color: #f2f2f2; text-align: center; margin: 40px 0px 0px 35px;'>"
					+ "<div style='width: 700px; height: 85px; padding: 20px 0px; text-align: center;'>&nbsp;<img src='"
					+ merchantLogo + "' height='80' /></div>"
					+ "<div style='background-color: #fff; padding: 20px 0px 0px 0px; width: 90%; margin: 0 auto 65px auto;'>"
					+ "<p style='font-size: 20px; color: #e6880f;'>Dear ";
					if(customer.getFirstName()!= null) {
						msg +=  customer.getFirstName() + " ";
					}
					if(customer.getLastName()!= null) {
						msg += customer.getLastName()  ;
					}
			msg += ",</p><p style='font-size: 14px;text-align: left;padding-left:10px;'>We have received a request to reset your password .Please find your reset password link below: &nbsp;</p>";
			msg = msg+"<br /><br /> <a style='text-decoration: none; margin: 16px 0px; display: inline-block; background-color: #f8981d; color: #fff; padding: 10px 12px; border-radius: 10px; font-weight: 600; font-size: 15px;' href='";
				
			Integer merchantId = Integer.parseInt(marchantId);
			/*if(merchantId.equals(IConstant.TEXAN_REWARDS_MERCHANT_ID)){
						msg = msg+ environment.getProperty("TEXAN_BASE_URL") + "/changepasswordV2?email=" + customer.getEmailId()
								+ "&merchantId=" + marchantId + "&userId=" + cusID + "&tLog="
								+ EncryptionDecryptionUtil.encryption(timeLog);
					}else{
						msg = msg+ environment.getProperty("WEB_BASE_URL") + "/changepasswordV2?email=" + customer.getEmailId()
								+ "&merchantId=" + marchantId + "&userId=" + cusID + "&tLog="
								+ EncryptionDecryptionUtil.encryption(timeLog);
					}*/
			
			msg = msg+ environment.getProperty("WEB_BASE_URL") + "/changepasswordV2?email=" + customer.getEmailId()
					+ "&merchantId=" + marchantId + "&userId=" + cusID + "&tLog="
					+ EncryptionDecryptionUtil.encryption(timeLog);
			
			msg=  msg+"' target='_blank'>Click here</a><br /> Note: Link will valid till next 30 min only .";

			msg = msg
					+ "<p><Order details: items, extras, price, fees, promo discount, total $><br>For any further questions, ";
			if(customer.getMerchantt().getPhoneNumber()!=null) { 
				msg = msg +"Please call us at " + customer.getMerchantt().getPhoneNumber() + "";}
			else{
				msg = msg +"Please call our store";
			}
					msg = msg +" <br><br>Regards,<br>" + customer.getMerchantt().getName()
					+ "<br><br>";
			msg = msg + "</div>";
			msg = msg + "<p>We appreciate your business with us!</p>";
			/*if(merchantId.equals(IConstant.TEXAN_REWARDS_MERCHANT_ID) ){
				msg = msg + "<div style='height: 70px; text-align: center;'>&nbsp;<img src='https://www.foodkonnekt.com/foodkonnekt_merchat_logo/274_PiePizzaria.png' alt='' width='200' height='63' /></div>"
						+ "</div>";
			}else{*/
				msg = msg + "<div style='height: 70px; text-align: center;'>&nbsp;<img src='http://www.dev.foodkonnekt.com/app/foodnew.jpg' alt='' width='200' height='63' /></div>"
					+ "</div>";
			//}
			msg = msg + "<p style='color: #676767; font-size: 11px;'>Automated email. Please do not reply to this sender email.</p>";
			
			
			message.setContent(msg, "text/html");
			Transport.send(message);
		} catch (MessagingException e) {
			LOGGER.error("===============  MailSendUtil : Inside forgotPasswordEmailV2 :: Exception  ============= " + e);

			throw new RuntimeException(e);
		}
		LOGGER.info("===============  MailSendUtil : Inside forgotPasswordEmailV2 :: End  ============= ");

		return null;
	}
	
	public static void sendMonthlyReportMail(String file,Merchant merchant,Environment environment){
		try {
			LOGGER.info("----------------Start :: MailSendUtil : sendMonthlyReportMail------------------");
	        String merchantLogo = null;
	        String emailId= null;
	        String vendorEmail= null;
	        String merchantName= null;
	    	LOGGER.info("MailSendUtil :: sendMonthlyReportMail : merchantId "+merchant.getId());
	        if(merchant !=null && merchant.getMerchantLogo()!=null){
	        	merchantLogo= merchant.getMerchantLogo();
	        }else{
	        	merchantLogo = "http://www.dev.foodkonnekt.com/app/foodnew.jpg";
	        }
	        
	        if(merchant !=null && merchant.getEmailId() !=null){
	        	emailId = merchant.getEmailId();
	        }
	        
	        if(merchant !=null && merchant.getOwner()!=null && merchant.getOwner().getEmail()!=null){
	        	vendorEmail = merchant.getOwner().getEmail();
	        }
	        
	        if(merchant!=null && merchant.getName() !=null){
	        	merchantName = merchant.getName();
	        }
	        
	        
	        MimeMessage message = new MimeMessage(
					SendMailProperty.gmailMailProperty(IConstant.FROM_EMAIL_ID, IConstant.FROM_PASSWORD));
			message.setFrom(new InternetAddress(IConstant.FROM_EMAIL_ID));
			
			emailId=emailId+","+vendorEmail;
			String to[] = emailId.split(","); 
	        InternetAddress[] address = new InternetAddress[to.length];
	        
	        for(int i =0; i< to.length; i++)
	        {
	            address[i] = new InternetAddress(to[i]);
	        }
	        message.setRecipients(Message.RecipientType.TO, address);
			
	        message.setSubject("Order Report : ENV : "+environment.getProperty("FOODKONNEKT_APP_TYPE"));
	        
			  Multipart multipart = new MimeMultipart();
			
			  BodyPart messageBodyPart = new MimeBodyPart();
			  
			  BodyPart attachementBodyPart = new MimeBodyPart();
			
			  String msg = "<div style='width: 700px; height: auto; border: solid 1px #ddd; box-shadow: 0px 0px 6px #ddd; background-color: #f2f2f2; text-align: center; margin: 40px 0px 0px 35px;'>"
					+ "<div style='width: 700px; height: 85px; padding: 20px 0px; text-align: center;'>&nbsp;<img src='"
					+ merchantLogo + "' height='80' /></div>"
					+ "<div style='background-color: #fff; padding: 20px 0px 0px 0px; width: 90%; margin: 0 auto 65px auto;'>"
					+ "<p style='font-size: 20px; color: #e6880f;'>Hello,<br />"+merchantName+"</p>"
					+ "<p style='font-size: 14px;'>Please find attached the meal request report for today.&nbsp;</p>"
					+ "<br /> Best,<br />Kidz Caters"
					+ "</div>"
					+ "<p>Lookout for an update from us on your order confirmation.<br />We appreciate your business with us!</p>";
			  /*if( merchant.getId()!=null &&  merchant.getId().equals(IConstant.TEXAN_REWARDS_MERCHANT_ID) ){
					msg = msg + "<div style='height: 70px; text-align: center;'>&nbsp;<img src='https://www.foodkonnekt.com/foodkonnekt_merchat_logo/274_PiePizzaria.png' alt='' width='200' height='63' /></div>"
							+ "</div>";
				}else{*/
					msg = msg + "<div style='height: 70px; text-align: center;'>&nbsp;<img src='http://www.dev.foodkonnekt.com/app/foodnew.jpg' alt='' width='200' height='63' /></div>";
				//}
			  msg = msg + "<p style='color: #676767; font-size: 11px;'>Automated email. Please do not reply to this sender email.</p>"
					+ "</div>";
	        
	       
	        messageBodyPart.setContent(msg, "text/html");
	        multipart.addBodyPart(messageBodyPart);
	        
	        
	        DataSource source = new FileDataSource(file);

	        attachementBodyPart.setDataHandler(new DataHandler(source));

	        attachementBodyPart.setFileName(file);

	        multipart.addBodyPart(attachementBodyPart);
	        
	        message.setContent(multipart);
			Transport.send(message);
			LOGGER.info("MailSendUtil :: sendMonthlyReportMail : mail send successfully");
		} catch (MessagingException e) {
			LOGGER.error("MailSendUtil :: sendMonthlyReportMail : Exception "+e);
			throw new RuntimeException(e);
		}
		LOGGER.info("----------------End :: MailSendUtil : sendMonthlyReportMail------------------");
	}
	public static void sendWeeklyTotalOrders(List<Merchant> merchantDataListObj, String startDate, String endDate,Environment environment) {
		int count = 0;
		try {
			LOGGER.info("===============  MailSendUtil : Inside sendWeeklyTotalOrders :: Start  =============startDate "+startDate+" enddate "+endDate);

			if(merchantDataListObj!=null && !merchantDataListObj.isEmpty()){
				Message message = new MimeMessage(
						SendMailProperty.gmailMailProperty(IConstant.FROM_EMAIL_ID,
								IConstant.FROM_PASSWORD));
				message.setFrom(new InternetAddress(IConstant.FROM_EMAIL_ID));
				// message.setRecipients(Message.RecipientType.TO,
				// InternetAddress.parse(merchantDataList.getEmailId()));
				
				LOGGER.info("In MailSendUtil------>Merchant Size--->"+merchantDataListObj.size());
				
				String myvar =null;
				for (Merchant merchantDataList : merchantDataListObj) {
					try{
					if(merchantDataList.getId()!=null){
						if(merchantDataList.getOwner()!=null && merchantDataList.getOwner().getEmail()!=null){
							message.setRecipients(Message.RecipientType.TO,InternetAddress.parse(merchantDataList.getOwner().getEmail()));
							//message.setRecipients(Message.RecipientType.TO,InternetAddress.parse("sumitgangrade1006@gmail.com"));
							LOGGER.info("Merchant Name----------------->"+merchantDataList.getName() + " Vendor Mail " +
									"Id------------------->"+merchantDataList.getOwner().getEmail());
							
							myvar = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">"
									+ "<html xmlns=\"http://www.w3.org/1999/xhtml\">"
									+ "<head>"
									+ "    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />"
									+ "    <meta name=\"viewport\" content=\"width=device-width\"/>"
									+ ""
									+ "    <style>"
									+ "	table{border-collapse:collapse; }"
									+ "	</style>"
									+ "    "
									+ "</head>"
									+ "<body style=\"background:#f2f2f2;font-size: 100%; font-family:Gotham, 'Helvetica Neue', Helvetica, Arial, sans-serif; line-height: 1.65;\"> <br><br>"
									+ "<table style=\"background: #fff; width:600px; margin:20px auto;\">"
									+ "    <tr>"
									+ "        <td class=\"container\">"
									+ ""
									+ "            <!-- Message start -->"
									+ "            <table>"
									+ "                <tr>"
									+ "                    <td align=\"center\" class=\"masthead\">"
									+ ""
									+ "                       <p class=\"logo\">"
									+ "											<img height:\"auto\" alt=\"\" src=\"https://www.foodkonnekt.com/wp-content/themes/softdesigners/images/logo%20(1).png\"  style=\"display:block; \">"
									+ "												</p>"
									+ ""
									+ "                    </td>"
									+ "                </tr>"
									+ "                <tr>"
									+ "                    <td style=\"padding:15px;\">"
									+ ""
									+ "                         <p style=\"font-size:14px; line-height:22px;  color:#333333; margin:0 0 5px;\">Dear " +merchantDataList.getName()+",<br><br />"
									+ "																Please find the weekly ("
									+ startDate
									+ " to "
									+ endDate
									+ ") online ordering report for "
									+ merchantDataList.getName()
									+ ". For detailed information about ordering details please log into the admin panel."
									+ "																</p><br>"
									+ "   "
									+ "<table style=\"border: 1px solid black;width:100%;\">"
									+ "<th style=\"background:#f8981d;color:#fff;text-align:left; padding:10px;\">		 ORDER REPORT</th><th style=\"background:#f8981d;color:#f8981d;\">		 ORDER REPORT</th>"
									+ ""
									+ "            <tr>"
									+ "                <td  style=\"border: 1px solid black ;padding:10px;font-size:15px;\">"
									+ "                    Number of Orders"
									+ "                </td>"
									+ "                <td  style=\"border: 1px solid black ;padding:10px;font-size:15px;\">"
									+ String.valueOf(merchantDataList.getTotalOrders())
									+ "                </td>"
									+ "                "
									+ "            <tr style=\"border: 1px solid black;\">"
									+ "                <td style=\"border: 1px solid black;padding:10px;font-size:15px;\">"
									+ "                    Total Order Value"
									+ "                </td>"
									+ "                <td style=\"border: 1px solid black;padding:10px;font-size:15px;\">"
									+ "$"+String.valueOf(merchantDataList.getTotalOrdersPrice())
									+ "                </td>"
									+ "                "
									+ "            </tr>"
									+ "            <tr style=\"border: 1px solid black;\">"
									+ "                <td style=\"border: 1px solid black;padding:10px;font-size:15px;\">"
									+ "                    Delivery Orders"
									+ "                </td>"
									+ "                <td style=\"border: 1px solid black;padding:10px;font-size:15px;\">"
									+ String.valueOf(merchantDataList.getDelivery())
									+ "                </td>"
									+ "                "
									+ "            "
									+ "            </tr>"
									+ "			<tr style=\"border: 1px solid black;\">"
									+ "                <td style=\"border: 1px solid black;padding:10px;font-size:15px;\">"
									+ "                    Pickup  Orders"
									+ "                </td>"
									+ "                <td style=\"border: 1px solid black;padding:10px;font-size:15px;\">"
									+ String.valueOf(merchantDataList.getPickup())
									+ "                </td>"
									+ "                "
									+ "            "
									+ "            </tr>"
									+ "        </table>"
									+ "                                <br />                            "
									+ "								<a href="
									+ environment.getProperty("BASE_URL")
									+ "/installFoodkonnekt?location_uid="
									+ merchantDataList.getMerchantUid()
									+ " style=\"color:#fff; text-decoration:none;background:#f8981d;border-radius:10px; border:none !important;padding:8px;font-size:16px;margin-top:20px;\">View Dashboard</a> <br />             "
									+ "                                <!-- end wrapper-->"
									+ "								<p style=\"font-size:14px; line-height:22px; font-weight:bold; color:#333333; margin:20px 0 5px;\">Regards,<br />"
									+ "								Foodkonnekt</p><br />						"
									+ "                                "
									+ "                            </td>"
									+ "							"
									+ "                        </tr>"
									+ "						"
									+ "                    </tbody>"
									+ "                </table>"
									+ ""
									+ "                       "
									+ ""
									+ "                    </td>"
									+ "                </tr>"
									+ "            </table>"
									+ ""
									+ "        </td>"
									+ "    </tr>"
									+ "    <tr>"
									+ "      "
									+ "    </tr>"
									+ "</table>"
									+ "<p style=\"font-size:9px; color:#5f5f5f;text-align:center;margin-top:-10px;\">Automated email. Please do not reply to this sender email.</p>"
									+ "" + "" + "</body>" + "</html>";

							count++;

//							if (count == 5) {
//								break; // A unlabeled break is enough. You don't need a
//										// labeled break here.
//							}
						}else message.setRecipients(Message.RecipientType.TO,InternetAddress.parse("sumit.a.gangrade@gmail.com"));
						LOGGER.info("Weekly Orders Mail sent");
						message.setSubject("Regards:Weekly Report Order : ENV : "+environment.getProperty("FOODKONNEKT_APP_TYPE"));
						message.setContent(myvar, "text/html");
						Transport.send(message);
						LOGGER.info("Mail Sent Successfully for " + merchantDataList.getName());
					}
				}catch(Exception e)
					{
					LOGGER.info("MailSendUtil : Inside sendWeeklyTotalOrders :: Exception"+e.getMessage());
					}
			}
			}
			
		} catch (MessagingException e) {
			LOGGER.error("===============  MailSendUtil : Inside sendWeeklyTotalOrders :: Exception  ============= " + e);
			throw new RuntimeException(e);
		}
	}
	
	
	public static boolean sendPaymentGatwayMailToAdmin(String exception, String action, String gatewaytype , OrderR orderR,Environment environment) {
		try {
			LOGGER.info("----------------Start :: MailSendUtil : sendPaymentGatwayMailToAdmin------------------");
			Message message = new MimeMessage(
					SendMailProperty.gmailMailProperty(IConstant.FROM_EMAIL_ID,
							IConstant.FROM_PASSWORD));
			LOGGER.info("MailSendUtil :: sendPaymentGatwayMailToAdmin : orderId "+orderR.getId());
			message.setFrom(new InternetAddress("orders@foodkonnekt.com"));
			/*message.setRecipients(
					Message.RecipientType.TO,
					InternetAddress
							.parse("sumit.a.gangrade@gmail.com,sumit.gangrade1234@gmail.com,sumit@mkonnekt.com,advertise60@gmail.com,chandana@mkonnekt.com,seshu@mkonnekt.com"));*/
							
			message.setRecipients(Message.RecipientType.TO,InternetAddress.parse(environment.getProperty("MAIL_IDS")));				
			
			//message.setSubject("Exception from Foodkonnekt payment gatway " + UrlConstant.FOODKONNEKT_APP_TYPE + type);
			message.setSubject("Payment failed "+ orderR.getMerchant().getName() + " " + gatewaytype + " order Id " + orderR.getId()+ " : ENV : "+environment.getProperty("FOODKONNEKT_APP_TYPE"));
			String msg = "";
			msg += action;
			msg += " <br>";
			if(orderR.getCustomer() != null && orderR.getCustomer().getFirstName() != null && orderR.getCustomer().getLastName() != null){
				msg += "Customer Name : "+orderR.getCustomer().getFirstName()+" "+orderR.getCustomer().getLastName();
				msg += " <br>";
			}else if(orderR.getCustomer() != null && orderR.getCustomer().getFirstName() != null){
				msg += "Customer Name : "+orderR.getCustomer().getFirstName();
				msg += " <br>";
			}
			if(orderR.getMerchant()!= null && orderR.getMerchant().getName()!= null){
				msg += "Merchant Name : "+orderR.getMerchant().getName();
				msg += " <br>";
			}
			msg += "Order Amount : "+orderR.getOrderPrice();
			msg += " <br>";
			msg += "Reason : "+exception;
			msg += " <br>";
			msg += exception;
			msg += " <br>";
			msg += " <br>";
			msg += "<b>Regards,</b><br>";
			msg += "FoodKonnekt";
			message.setContent(msg, "text/html");
			Transport.send(message);
			LOGGER.info("MailSendUtil :: sendPaymentGatwayMailToAdmin : mail send successfully ");
		} catch (MessagingException e) {
			LOGGER.error("MailSendUtil :: sendPaymentGatwayMailToAdmin : Exception "+e);
			throw new RuntimeException(e);
		}
		// }
		LOGGER.info("----------------End :: MailSendUtil : sendPaymentGatwayMailToAdmin------------------");
		return true;
	}
	   /* public static void sendPaymentExceptionByMail(Exception e,String action,String gatewaytype,OrderR orderR ) {
	        StringWriter errors = new StringWriter();

	    	if(gatewaytype.equalsIgnoreCase("Stripe")&&e!=null){
		        MailSendUtil.sendPaymentGatwayMailToAdmin(e.getMessage().toString(), action, gatewaytype , orderR);
	    	}
	    	else{
		        e.printStackTrace(new PrintWriter(errors));
		        MailSendUtil.sendPaymentGatwayMailToAdmin(errors.toString(), action, gatewaytype , orderR);

	    	}
	    }
	    
	    
	    public static boolean sendMailSystemLog(String appName, String source, String event, String log) {
			try {
				Message message = new MimeMessage(
						SendMailProperty.gmailMailProperty(IConstant.FROM_EMAIL_ID, IConstant.FROM_PASSWORD));
				message.setFrom(new InternetAddress(IConstant.FROM_EMAIL_ID));
				message.setRecipients(Message.RecipientType.TO, InternetAddress.parse("sumit@mkonnekt.com,chandana@mkonnekt.com,sumit.a.gangrade@gmail.com,sumit.gangrade1234@gmail.com,jain.abhi6190@gmail.com"));
				message.setSubject("Error From "+appName+" & "+source);
				String msg = " ";
				msg += " <br>";
				msg += " <br>";
				msg += event + "----->" + log;
				message.setContent(msg, "text/html");
				Transport.send(message);
			} catch (MessagingException e) {
				throw new RuntimeException(e);
			}
			return true;
		}*/
	
	
	public static boolean sendPaymentGatwayMailToAdmin(String exception,
			String action, String gatewaytype, OrderR orderR, String reason,Environment environment) {
		try {
			LOGGER.info("----------------Start :: MailSendUtil : sendPaymentGatwayMailToAdmin------------------");
			Message message = new MimeMessage(
					SendMailProperty.gmailMailProperty(IConstant.FROM_EMAIL_ID,
							IConstant.FROM_PASSWORD));
			message.setFrom(new InternetAddress("orders@foodkonnekt.com"));
			
			message.setRecipients(Message.RecipientType.TO,InternetAddress.parse(environment.getProperty("MAIL_IDS")));
			
			message.setSubject("Paymentfailed:-  PG " + gatewaytype
					+ "   OID :" + orderR.getId() + "  ENV :"
					+ environment.getProperty("FOODKONNEKT_APP_TYPE"));
			String msg = "";
			msg += action;
			msg += " <br>";
			if (orderR.getCustomer() != null && orderR.getCustomer().getFirstName() != null) {
				msg += "Customer Name : " + orderR.getCustomer().getFirstName();
				msg += " <br>";
			}
			msg += "Order Amount : " + orderR.getOrderPrice();
			msg += " <br>";
			msg += "Reason : " + reason;
			msg += " <br>";
			msg += exception;
			msg += " <br>";
			msg += " <br>";
			msg += "<b>Regards,</b><br>";
			msg += "FoodKonnekt";
			message.setContent(msg, "text/html");
			Transport.send(message);
			LOGGER.info("MailSendUtil :: sendPaymentGatwayMailToAdmin : mail send successfully ");
		} catch (MessagingException e) {
			LOGGER.error("MailSendUtil :: sendPaymentGatwayMailToAdmin : Exception "+e);
			throw new RuntimeException(e);
		}
		LOGGER.info("----------------End :: MailSendUtil : sendPaymentGatwayMailToAdmin------------------");
		return true;
	}

	public static void sendPaymentExceptionByMail(Exception e, String action,String gatewaytype, OrderR orderR, String reason,Environment environment) {
		LOGGER.info("----------------Start :: MailSendUtil : sendPaymentExceptionByMail------------------");
		StringWriter errors = new StringWriter();
		if (gatewaytype.equalsIgnoreCase("Stripe") && e != null) {
			MailSendUtil.sendPaymentGatwayMailToAdmin(e.getMessage().toString(), action, gatewaytype, orderR, reason,environment);
		} else {
			e.printStackTrace(new PrintWriter(errors));
			MailSendUtil.sendPaymentGatwayMailToAdmin(errors.toString(),action, gatewaytype, orderR, reason,environment);

		}
		LOGGER.info("----------------End :: MailSendUtil : sendPaymentExceptionByMail------------------");
	}
	
	 public static boolean sendMailSystemLog(String appName, String source, String event, String log,Environment environment) {
			try {
				LOGGER.info("----------------Start :: MailSendUtil : sendMailSystemLog------------------");
				Message message = new MimeMessage(
						SendMailProperty.gmailMailProperty(IConstant.FROM_EMAIL_ID, IConstant.FROM_PASSWORD));
				message.setFrom(new InternetAddress(IConstant.FROM_EMAIL_ID));
				//message.setRecipients(Message.RecipientType.TO, InternetAddress.parse("sumit@mkonnekt.com,chandana@mkonnekt.com,sumit.a.gangrade@gmail.com,sumit.gangrade1234@gmail.com,seshu@mkonnekt.com"));
				message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(environment.getProperty("MAIL_IDS")));
				message.setSubject("Error From "+appName+" & "+source + " : ENV : "+environment.getProperty("FOODKONNEKT_APP_TYPE"));
				String msg = " ";
				msg += " <br>";
				msg += " <br>";
				msg += event + "----->" + log;
				message.setContent(msg, "text/html");
				Transport.send(message);
				LOGGER.info("MailSendUtil :: sendMailSystemLog : mail send successfully");
			} catch (MessagingException e) {
				LOGGER.error("MailSendUtil :: sendMailSystemLog : Exception "+e);
				throw new RuntimeException(e);
			}
			LOGGER.info("----------------End :: MailSendUtil : sendMailSystemLog------------------");
			return true;
		}

	 public static boolean futureOnlineOrderNotificationReceiptMail(
				List<OrderR> orderR, String merchentEmail,String merchantName,Environment environment) {
			try {
				LOGGER.info("===============  MailSendUtil : Inside futureOnlineOrderNotificationReceiptMail :: Start  =============merchentEmail "+merchentEmail);

				String orderIdd = null;
				String customerrId = null;
				String orderLink = null;
				Message message = new MimeMessage(
						SendMailProperty.gmailMailProperty(IConstant.FROM_EMAIL_ID,
								IConstant.FROM_PASSWORD));
				message.setFrom(new InternetAddress(IConstant.FROM_EMAIL_ID));
				message.setRecipients(Message.RecipientType.TO,
						InternetAddress.parse(merchentEmail));
				message.setSubject("Future Online Order Notification : ENV : "+environment.getProperty("FOODKONNEKT_APP_TYPE"));
				String msg = "";
				msg =msg+"<head><style>table {  font-family: arial, sans-serif;  border-collapse: collapse;  width: 60%;}td, th {  border: 1px solid #dddddd;  text-align: left;  padding: 8px;} th { background-color:#FF9800;}"
                         +"tr:nth-child(even) {  background-color: #dddddd;}</style></head><body><h2>Hi Team,</h2>"
						+ "<br /><br /> <strong style='text-decoration: underline;'>Special Instructions :</strong> "
						+ "Please find the list of future orders for "+merchantName+" below:";
				msg=msg+"<table><tr><th>Order Date</th><th>Fulfillment date</th><th>Customer Name</th><th>Order Type</th><th>Amount</th><th>View</th></tr>";
				for (OrderR orderR2 : orderR) {

                   LOGGER.info("inside futureOnlineOrderNotificationReceiptMail orderR2.getCustomer().getId() : "+ orderR2.getCustomer().getId());
                   LOGGER.info("inside futureOnlineOrderNotificationReceiptMail Encrypted orderId : "+ EncryptionDecryptionUtil.encryption(Integer
							.toString(orderR2.getId())));
                   LOGGER.info("inside futureOnlineOrderNotificationReceiptMail Encrypted customerId : "+ EncryptionDecryptionUtil.encryption(Integer
							.toString(orderR2.getCustomer().getId())));
					orderIdd = EncryptionDecryptionUtil.encryption(Integer
							.toString(orderR2.getId()));

					customerrId = EncryptionDecryptionUtil.encryption(Integer
							.toString(orderR2.getCustomer().getId()));

					orderLink = environment.getProperty("WEB_BASE_URL") + "/receipt?orderid="+ orderIdd + "&customerid=" + customerrId;
					msg=msg+"<tr><td>"+orderR2.getCreatedOn()+"</td><td>"+orderR2.getFulfilled_on()+"</td><td>"+orderR2.getCustomer().getFirstName()+"</td><td>"+orderR2.getOrderType()+"</td><td>"+orderR2.getOrderPrice()+"</td><td>";
				  msg = msg + "<a href='"+ orderLink + "'"+ " target='_blank'>View Online Receipt "+ orderR2.getId() + "</a></td></tr>";
				}
				
				msg = msg + "</table><div style='height: 70px; text-align: center;'>&nbsp;<img src='http://www.dev.foodkonnekt.com/app/foodnew.jpg' alt='' width='200' height='63' /></div></div>";
				
				msg = msg + "<p style='color: #676767; font-size: 11px;'>Automated email. Please do not reply to this sender email.</p>"
						  + "</div></body>";
				message.setContent(msg, "text/html");
				Transport.send(message);
			} catch (MessagingException e) {
				LOGGER.error("===============  MailSendUtil : Inside futureOnlineOrderNotificationReceiptMail :: Exception  ============= " + e);

			}
			LOGGER.info("===============  MailSendUtil : Inside futureOnlineOrderNotificationReceiptMail :: End  ============= ");

			return false;
		}
	 
	 public static boolean sendAppStatusNotificationFailedMail(Merchant merchant ,String merchantEmail,Environment environment) {
  	   
	    	String merchantName = merchant.getName();
	    	   String msg="";
			try {
				LOGGER.info("===============  MailSendUtil : Inside sendAppStatusNotificationFailedMail :: Start  =============merchantName "+merchantName);

				if(!environment.getProperty("FOODKONNEKT_APP_TYPE").equals("Local")){
				Message message = new MimeMessage(
						SendMailProperty.gmailMailProperty(IConstant.FROM_EMAIL_ID, IConstant.FROM_PASSWORD));
				message.setFrom(new InternetAddress(IConstant.FROM_EMAIL_ID));
				if(merchantEmail!=null && !merchantEmail.isEmpty())
				{
					message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(environment.getProperty("MAIL_IDS")+","+merchantEmail));
					
				}else	message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(environment.getProperty("MAIL_IDS")));
				
				message.setSubject("App status update failed :"+merchantName+" ENV : "+environment.getProperty("FOODKONNEKT_APP_TYPE"));
			    
				msg = msg + "<div> Hello " +   merchantName + ",</div> " ;
				
             msg =   msg  + " <p> Your server is unable to communicate with the in-store app in order to process online orders."
             		+ " Some common reasons for this failure could be  </p> ";
             
             msg = msg + "<div> 1) There is no internet connection </div>";
             msg = msg + "<div> 2) The tablet/computer is powered off </div>";
             
             msg =   msg  + " <p> Please check and make sure that the tablet/computer is powered on and is connected to the internet."
             		+ " If this problem persists, we will not be able to process any online orders. For any questions please call 619-566-6358  </p> ";
             
             msg =  msg +  " <div> Best </div> " +
                    " <div> FoodKonnekt Team </div> ";
				
				message.setContent(msg, "text/html");
				Transport.send(message);
				}
			} catch (MessagingException e) {
				LOGGER.error("===============  MailSendUtil : Inside sendAppStatusNotificationFailedMail :: Exception  ============= " + e);

				throw new RuntimeException(e);
			}
			LOGGER.info("===============  MailSendUtil : Inside sendAppStatusNotificationFailedMail :: End  ============= ");

			return true;
		}
	 
	 public static void sendPaymentExceptionByMail(Exception e,String action,String gatewaytype,OrderR orderR,Environment environment) {
	        StringWriter errors = new StringWriter();
	        e.printStackTrace(new PrintWriter(errors));
	        MailSendUtil.sendPaymentGatwayMailToAdmin(errors.toString(), action, gatewaytype , orderR,environment);
	    }
	 
	 public static void cloudPrinterMail(String merchentMailId,String mailMessage,String merchantName,Environment environment){
			try {
				LOGGER.info("===============  MailSendUtil : Inside cloudPrinterMail :: Start  =============merchentMailId "+merchentMailId+" mailMessage "+mailMessage+" merchantName "+merchantName);

				Message message = new MimeMessage(SendMailProperty.gmailMailProperty(IConstant.FROM_EMAIL_ID ,IConstant.FROM_PASSWORD));
				message.setFrom(new InternetAddress("orders@foodkonnekt.com"));
				message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(merchentMailId));
				message.setSubject("Regarding cloud print "+environment.getProperty("FOODKONNEKT_APP_TYPE"));
				String msg = "Dear "+merchantName+",<br>";
				msg += mailMessage;
				msg += " <br>";
				msg += " <br>";
				msg += "<b>Regards,</b><br>";
				msg += "FoodKonnekt";
				message.setContent(msg, "text/html");
				Transport.send(message);
				
			} catch (MessagingException e) {
				LOGGER.error("===============  MailSendUtil : Inside cloudPrinterMail :: Exception  ============= " + e);

				throw new RuntimeException(e);
			}
	    }
	 
	 public static boolean onlineOrderCancel(OrderR orderR, boolean Status,Environment environment) {
			try {
				LOGGER.info("===============  MailSendUtil : Inside onlineOrderCancel :: Start  =============Status "+Status);
				if(orderR != null && orderR.getCustomer() != null) {
					LOGGER.info(" EncryptionDecryptionUtil.encryption(orderR.getId().toString()) : "+EncryptionDecryptionUtil.encryption(orderR.getId().toString()));
					LOGGER.info(" EncryptionDecryptionUtil.encryption(orderR.getCustomer().getId().toString()) : "+EncryptionDecryptionUtil.encryption(orderR.getCustomer().getId().toString()));
					}
				Message message = new MimeMessage(
						SendMailProperty.gmailMailProperty(IConstant.FROM_EMAIL_ID,
								IConstant.FROM_PASSWORD));
				message.setFrom(new InternetAddress(IConstant.FROM_EMAIL_ID));
				message.setRecipients(Message.RecipientType.TO,
						InternetAddress.parse(orderR.getCustomer().getEmailId()));
				message.setSubject("Order Cancel Notification");
				String merchantLogo="";
				if (orderR.getCustomer().getMerchantt().getMerchantLogo() == null) {
	                merchantLogo = "http://www.dev.foodkonnekt.com/app/foodnew.jpg";
	            } else {
	                merchantLogo = environment.getProperty("BASE_PORT") +orderR.getCustomer().getMerchantt().getMerchantLogo();
	            }
				String msg = "";
				if (Status == true) {
					
							
					msg = "<div style='width: 700px; height: auto; border: solid 1px #ddd; box-shadow: 0px 0px 6px #ddd; background-color: #f2f2f2; text-align: center; margin: 40px 0px 0px 35px;'>"
								+ "<div style='width: 700px; height: 85px; padding: 20px 0px; text-align: center;'>&nbsp;<img src='"
								+ merchantLogo + "' height='80' /></div>"
								+ "<div style='background-color: #fff; padding: 20px 0px 0px 0px; width: 90%; margin: 0 auto 65px auto;'>"
								+ "<p style='font-size: 20px; color: #e6880f;'>Dear ";
					if(orderR.getCustomer().getFirstName()!=null && orderR.getCustomer().getLastName()!=null)
						msg+=orderR.getCustomer().getFirstName().concat(" ").concat(orderR.getCustomer().getLastName());
					else if(orderR.getCustomer().getFirstName()!=null)
						msg+=orderR.getCustomer().getFirstName();
					
					msg+= ",</p>"
								+ "<p style='font-size: 14px;text-align: left;padding-left:10px;'> Your order has been cancelled and the order amount "+  orderR.getOrderPrice() +" has been refunded. You should be seeing these charges credited to your account within 24 to 48 hours. Please contact us if you have any more questions.</p><br>";

							
							
				
				}

				if (Status == false) {
					
					msg = "<div style='width: 700px; height: auto; border: solid 1px #ddd; box-shadow: 0px 0px 6px #ddd; background-color: #f2f2f2; text-align: center; margin: 40px 0px 0px 35px;'>"
							+ "<div style='width: 700px; height: 85px; padding: 20px 0px; text-align: center;'>&nbsp;<img src='"
							+ merchantLogo + "' height='80' /></div>"
							+ "<div style='background-color: #fff; padding: 20px 0px 0px 0px; width: 90%; margin: 0 auto 65px auto;'>"
							+ "<p style='font-size: 20px; color: #e6880f;'>Dear ";
					if(orderR.getCustomer().getFirstName()!=null && orderR.getCustomer().getLastName()!=null)
						msg+=orderR.getCustomer().getFirstName().concat(" ").concat(orderR.getCustomer().getLastName());
					else if(orderR.getCustomer().getFirstName()!=null)
						msg+=orderR.getCustomer().getFirstName();
					msg+=",</p>"
							+ "<p style='font-size: 14px;text-align: left;padding-left:10px;'>Your order has been cancelled.You should be seeing these charges credited to your account within 24 to 48 hours. Please contact us if you have any more questions.</p><br>";

					}
				
				
				
				String orderLink = environment.getProperty("WEB_BASE_URL") + "/orderRec?orderid=" + EncryptionDecryptionUtil.encryption(orderR.getId().toString()) + "&customerid=" + 
							EncryptionDecryptionUtil.encryption(orderR.getCustomer().getId().toString());
				
				msg = msg +"<a href = "+ orderLink +">Click here for Receipt.</a>";
				
				msg = msg
						+ "<p><Order details: items, extras, price, fees, promo discount, total $><br>For any further questions, ";
				if(orderR.getCustomer().getMerchantt().getPhoneNumber()!=null) { 
					msg = msg +"Please call us at " +orderR.getCustomer().getMerchantt().getPhoneNumber() + "";}
				else{
					msg = msg +"Please call our store";
				}
						msg = msg +" <br><br>Best,<br>" +"Client Location Team"
						+ "<br><br>";
				msg = msg + "</div>";
				msg = msg + "<p>We appreciate your business with us!</p>";
				
			
					msg = msg + "<div style='height: 70px; text-align: center;'>&nbsp;<img src='http://www.dev.foodkonnekt.com/app/foodnew.jpg' alt='' width='200' height='63' /></div>"
						+ "</div>";
				
				msg = msg + "<p style='color: #676767; font-size: 11px;'>Automated email. Please do not reply to this sender email.</p>";
				
				
				message.setContent(msg, "text/html");
				Transport.send(message);
			} catch (MessagingException e) {
				LOGGER.error("===============  MailSendUtil : Inside onlineOrderCancel :: Exception  ============= " + e);

				LOGGER.error("error: " + e.getMessage());
			}
			LOGGER.info("===============  MailSendUtil : Inside onlineOrderCancel :: End  ============= ");

			return false;
		}

		
	 public static String customerLoginDetail(String firstname, String lastName, String email, String password) {
			try {
				LOGGER.info("===============  MailSendUtil : Inside customerLoginDetail :: Start  =============firstname "+firstname+" lastName "+lastName+" email "+email+" password "+password);

				Message message = new MimeMessage(
						SendMailProperty.gmailMailProperty(IConstant.FROM_EMAIL_ID, IConstant.FROM_PASSWORD));
				message.setFrom(new InternetAddress(IConstant.FROM_EMAIL_ID));
				message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email));
				message.setSubject("Thank you for Installation");
				String msg = "<div style='width: 700px; height: auto; border: solid 1px #ddd; box-shadow: 0px 0px 6px #ddd; background-color: #f2f2f2; text-align: center; margin: 40px 0px 0px 35px;'><div style='width: 700px; height: 85px; padding: 20px 0px; text-align: center;'>&nbsp;<img src='' height='80' />"
						
						+ "</div><div style='background-color: #fff; padding: 20px 0px 0px 0px; width: 90%; margin: 0 auto 65px auto;'><p style='font-size: 20px; color: #e6880f;'>Dear "
						+ firstname +" " +lastName
						+ ",</p><p style='font-size: 14px;'>Thank you for Installation. Your account has been created.<br /> Please use the login details below to come back and order with us in future.</p><div style='text-align: left; padding: 5px 20px;'><table><tbody><tr><td width='90'>Name</td><td>:</td><td>"
						+ firstname +" " + lastName + "</td></tr><tr><td>Email</td><td>:</td><td>" + email
						+ "</td></tr><tr><td>Password</td><td>:</td><td>" + password
						+ "</td></tr></tbody></table><br /><br /><p>Thank you. We appreciate your business with us!</p></div>";
				 msg = msg +"</div>'";
					message.setContent(msg, "text/html");
				Transport.send(message);
			} catch (MessagingException e) {
				LOGGER.error("===============  MailSendUtil : Inside customerLoginDetail :: Exception  ============= " + e);

				throw new RuntimeException(e);
			}
			LOGGER.info("===============  MailSendUtil : Inside customerLoginDetail :: End  ============= ");

			return null;
		}
	 public  String orderConfirmetionMailWirhOrderPosId(OrderR orderR) {
			try {
				LOGGER.info("===============  MailSendUtil : Inside orderConfirmetionMailWirhOrderPosId :: Start  ============= ");

				Message message = new MimeMessage(
						SendMailProperty.gmailMailProperty(IConstant.FROM_EMAIL_ID, IConstant.FROM_PASSWORD));
				message.setFrom(new InternetAddress(IConstant.FROM_EMAIL_ID));
				message.setRecipients(Message.RecipientType.TO, InternetAddress.parse("sumit.a.gangrade@gmail.com,seshu@mkonnekt.com"));
				message.setSubject("Order Confirmation Mail");
				
				String msg = "<div style='width: 700px; height: auto; border: solid 1px #ddd; box-shadow: 0px 0px 6px #ddd; background-color: #f2f2f2; text-align: center; margin: 40px 0px 0px 35px;'><div style='width: 700px; height: 85px; padding: 20px 0px; text-align: center;'>&nbsp;<img src='' height='80' />"
						
						+ "</div><div style='background-color: #fff; padding: 20px 0px 0px 0px; width: 90%; margin: 0 auto 65px auto;'><p style='font-size: 20px; color: #e6880f;'>Dear "
						+ "Sheshu"
						+ ",</p><p style='font-size: 14px;'>Order has been went throuth by app.<br /> Please check</p><div style='text-align: left; padding: 5px 20px;'><table><tbody><tr><td width='90'>Name</td><td>:</td><td>"
						+ "" +" " ;
				if(orderR.getCustomer().getFirstName()!=null) {
					msg +=  orderR.getCustomer().getFirstName()+ " " ;
				}
				if(orderR.getCustomer().getFirstName()!=null) {
					msg += orderR.getCustomer().getFirstName() ;
				}
				
				msg+="</td></tr>"
						+"<tr><td>Merchant Name </td><td>: </td><td>" + orderR.getMerchant().getName()+"</td></tr>"
						+"<tr><td>OrderId </td><td>: </td><td>" + orderR.getId()+"</td></tr>"
						+"<tr><td>order PosId </td><td>: </td><td>" + orderR.getOrderPosId()
						+ "</td></tr></tbody></table><br /><br /><p>Thank you. We appreciate your business with us!</p></div>";
				        msg = msg +"</div>'";
					message.setContent(msg, "text/html");
				Transport.send(message);
			} catch (MessagingException e) {
				LOGGER.error("===============  MailSendUtil : Inside orderConfirmetionMailWirhOrderPosId :: Exception  ============= " + e);

				throw new RuntimeException(e);
			}
			LOGGER.info("===============  MailSendUtil : Inside orderConfirmetionMailWirhOrderPosId :: End  ============= ");

			return null;
		}
	 public  String orderPenddingNotificationMail(OrderR orderR,String merchantEmail) {
			try {
				LOGGER.info("===============  MailSendUtil : Inside orderPenddingNotificationMail :: Start  ============= ");
				String merchantName="Dear Support";
				if(orderR!=null && orderR.getMerchant().getName()!=null && !orderR.getMerchant().getName().isEmpty())
				{
					merchantName=orderR.getMerchant().getName();
				}
				Message message = new MimeMessage(
						SendMailProperty.gmailMailProperty(IConstant.FROM_EMAIL_ID, IConstant.FROM_PASSWORD));
				message.setFrom(new InternetAddress(IConstant.FROM_EMAIL_ID));
				if(merchantEmail!=null && !merchantEmail.isEmpty())
				message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(environment.getProperty("MAIL_IDS")+","+merchantEmail));
				else
				message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(environment.getProperty("MAIL_IDS")));
				message.setSubject("Order Pending Mail");
				String msg = "<div style='width: 700px; height: auto; border: solid 1px #ddd; box-shadow: 0px 0px 6px #ddd; background-color: #f2f2f2; text-align: center; margin: 40px 0px 0px 35px;'><div style='width: 700px; height: 85px; padding: 20px 0px; text-align: center;'>&nbsp;<img src='' height='80' />"
						
						+ "</div><div style='background-color: #fff; padding: 20px 0px 0px 0px; width: 90%; margin: 0 auto 65px auto;'><p style='font-size: 20px; color: #e6880f;'>"+merchantName
						+ ",</p><p style='font-size: 14px;'>The order ID - "+ orderR.getId()+" "+orderR.getOrderType() +" for client "+orderR.getMerchant().getName()+" is not processed. Please check and contact the client regarding this..<br /></p><div style='text-align: left; padding: 5px 20px;'><br /><br /><p><br>Best,<br>FoodKonnekt Support Team!</p></div>";
				        msg = msg +"</div>'";
					message.setContent(msg, "text/html");
				Transport.send(message);
			} catch (MessagingException e) {
				LOGGER.error("===============  MailSendUtil : Inside orderPenddingNotificationMail :: Exception  ============= " + e);
				
				MailSendUtil.sendExceptionByMail(e,environment);
			}
			LOGGER.info("===============  MailSendUtil : Inside orderPenddingNotificationMail :: End  ============= ");
			return null;
		}
	 public static boolean onlineOrderFailedNotification(Vendor vendor, List<OnlineOrderNotificationStatus> onlineNotificationStatuslist,
             OrderR orderR,Environment environment) {
   try {
    //   vendor.setEmail(vendor.getEmail());
       Message message = new MimeMessage(
               SendMailProperty.gmailMailProperty(IConstant.FROM_EMAIL_ID, IConstant.FROM_PASSWORD));
       message.setFrom(new InternetAddress(IConstant.FROM_EMAIL_ID));
       message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(vendor.getEmail()));
       message.setSubject("Online Order Notification");
       Multipart multipart = new MimeMultipart();
       
         BodyPart messageBodyPart = new MimeBodyPart();
         
         BodyPart attachementBodyPart = new MimeBodyPart();
           

           String msg="";
       
           int orderId = orderR.getId();
                           
           SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy hh:mm a");
       
           msg = msg + 
                           "<div> Hello " +   orderR.getMerchant().getName() + "</div> <br/> " +
                            "<p> We are unable to fax the order - " + orderId  +" due to connectivity issues. </p> " +
                             " <p> Attempt Time <p> " ;
                            
                              for(OnlineOrderNotificationStatus onlineNotificationStatus : onlineNotificationStatuslist){  
                                  
                                  msg = msg + " <div> "  +
                              sdf.format(onlineNotificationStatus.getUpdatedTime()) + " </div> " ;
                               }

                            
                        msg =   msg  + " <p> Please find the attached order for your reference.  </p> " +
                               " <div> Best Regards,</div> " +
                               " <div> FoodKonnekt Team </div><br><div><p> Automated email. Please do not reply to this sender mail.  </p> </div> ";
                        
       
       
       
        messageBodyPart.setContent(msg, "text/html");
         multipart.addBodyPart(messageBodyPart);
       
       
       DataSource source = new FileDataSource(environment.getProperty("FAX_FILE_PATH")+orderR.getId()+".pdf");

       attachementBodyPart.setDataHandler(new DataHandler(source));

       attachementBodyPart.setFileName("Fax"+orderR.getId()+".pdf");

       multipart.addBodyPart(attachementBodyPart);
       
       message.setContent(msg, "text/html");
       message.setContent(multipart);
       
       
       
       Transport.send(message);
   } catch (MessagingException e) {

   }       
   return false;
   }
	 public static String sendAutoStartmail(String locationName,String merchantPosId,Environment environment) {
			try {
				LOGGER.info("sendAutoStartmail :locationName: "+locationName);
				LOGGER.info("sendAutoStartmail :merchantPosId: "+merchantPosId);
				Message message = new MimeMessage(
						SendMailProperty.gmailMailProperty(IConstant.FROM_EMAIL_ID, IConstant.FROM_PASSWORD));
				message.setFrom(new InternetAddress(IConstant.FROM_EMAIL_ID));
				message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(environment.getProperty("MAIL_IDS")));
				message.setSubject("Auto Start App");
				String msg = "<div style='width: 700px; height: auto; border: solid 1px #ddd; box-shadow: 0px 0px 6px #ddd; background-color: #f2f2f2; text-align: center; margin: 40px 0px 0px 35px;'><div style='width: 700px; height: 85px; padding: 20px 0px; text-align: center;'>&nbsp;<img src='' height='80' />"
						
						+ "</div><div style='background-color: #fff; padding: 20px 0px 0px 0px; width: 90%; margin: 0 auto 65px auto;'><p style='font-size: 20px; color: #e6880f;'>Dear "+locationName;
						
						msg+= ",</p><p style='font-size: 14px;'> App is Auto Started At time : "+new Date()+" merchantId :: "+merchantPosId+" </p><div style='text-align: left; padding: 5px 20px;'><br /><br /><p></p></div>";
				 msg = msg +"</div>'";
					message.setContent(msg, "text/html");
				Transport.send(message);
				return "true";
			} catch (MessagingException e) {
				LOGGER.info("Exception :"+e);
				sendExceptionByMail(e,environment);
			}
			return "false";
		}
	 
	 public static boolean sendFoodtronixErrorMailToAdmin(String exception,String merhchantName,Environment environment) {
			
			if (!environment.getProperty("FOODKONNEKT_APP_TYPE").equals("Local")) {
				LOGGER.info("----------------Start :: MailSendUtil : sendErrorMailToAdmin------------------");
				try {
					Message message = new MimeMessage(
							SendMailProperty.mailProperty(
									IConstant.EXCEPTION_EMAIL_ID,
									IConstant.EXCEPTION_PASSWORD));
					message.setFrom(new InternetAddress("bugs@mkonnekt.com"));

					message.setRecipients(Message.RecipientType.TO,InternetAddress.parse("sumit.a.gangrade@gmail.com,jisha.mkonnekt@gmail.com"));
					
					message.setSubject("Foodtronix Exception from"+ environment.getProperty("FOODKONNEKT_APP_TYPE"));
					String msg = "Merhchant Name :: "+merhchantName+" <br> ";
					msg += exception;
					msg += " <br> ";
					msg += " <br>";
					msg += "<b>Regards,</b><br>";
					msg += "FoodKonnekt";
					message.setContent(msg, "text/html");
					Transport.send(message);
					LOGGER.info("MailSendUtil :: sendErrorMailToAdmin : mail send successfully");
				} catch (MessagingException e) {
					LOGGER.error("MailSendUtil :: sendErrorMailToAdmin : Exception "+e);
					throw new RuntimeException(e);
				}
			}
			LOGGER.info("----------------End :: MailSendUtil : sendErrorMailToAdmin------------------");
			return true;
		}
	 public static boolean sendFaxThroughMail(String orderid,String customerEmail,Environment environment){
		 LOGGER.info("----------------Start :: MailSendUtil : onlineOrderNotification------------------");
			LOGGER.info(" MailSendUtil :: sendFaxThroughMail : orderId "+EncryptionDecryptionUtil.decryption(orderid));
			LOGGER.info(" MailSendUtil :: sendFaxThroughMail : customerEmail "+customerEmail);
			boolean response =true;
           try {
			Message message = new MimeMessage(
					SendMailProperty.gmailMailProperty(IConstant.FROM_EMAIL_ID, IConstant.FROM_PASSWORD));
			message.setFrom(new InternetAddress(IConstant.FROM_EMAIL_ID));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(customerEmail));
			message.setSubject("Online Order Notification");
			Multipart multipart = new MimeMultipart();
			
			  BodyPart messageBodyPart = new MimeBodyPart();
			  
			  BodyPart attachementBodyPart = new MimeBodyPart();
		 
		 DataSource source = new FileDataSource(environment.getProperty("FAX_FILE_PATH")+Integer.parseInt(EncryptionDecryptionUtil.decryption(orderid))+".pdf");

	        attachementBodyPart.setDataHandler(new DataHandler(source));

	        attachementBodyPart.setFileName("Order_"+Integer.parseInt(EncryptionDecryptionUtil.decryption(orderid))+".pdf");

	        multipart.addBodyPart(attachementBodyPart);
	        
	       message.setContent(multipart);
			Transport.send(message);
			LOGGER.info(" MailSendUtil :: sendFaxThroughMail : fax sent successfully");
      }catch (Exception e) {
	LOGGER.info("Exception in sendFaxThroughMail : Exception : "+e);
	response=false;
       }
			return response;
	 }
	 
	 public static boolean onlineUberOrderNotification(String orderid,
				String customerid, String customerName,
				Double orderPrice, String orderType,String paymentType
				,Date orderDate,String orderLink, String customerEmail,String merchantLogo,String subTotal,Double uberCharges
				,String tax,String note,String merchantName,String orderDetails,String avgTime,
				Double orderDiscount,String orderCoupon, Integer merchantId ,String orderPosId,Environment environment){
			try {
				LOGGER.info("----------------Start :: MailSendUtil : onlineOrderNotification------------------");
				LOGGER.info(" MailSendUtil :: onlineOrderNotification : orderId "+EncryptionDecryptionUtil.decryption(orderid));
				LOGGER.info(" MailSendUtil :: onlineOrderNotification : customerId "+EncryptionDecryptionUtil.decryption(customerid));
				
				Message message = new MimeMessage(
						SendMailProperty.gmailMailProperty(IConstant.FROM_EMAIL_ID, IConstant.FROM_PASSWORD));
				message.setFrom(new InternetAddress(IConstant.FROM_EMAIL_ID));
				message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(customerEmail));
				message.setSubject("Online Order Notification");
				Multipart multipart = new MimeMultipart();
				
				  BodyPart messageBodyPart = new MimeBodyPart();
				  
				  BodyPart attachementBodyPart = new MimeBodyPart();
				  BodyPart thermalReceiptAttachment = new MimeBodyPart();
				  
				String msg = "<div style='width: 700px; height: auto; border: solid 1px #ddd; box-shadow: 0px 0px 6px #ddd; background-color: #f2f2f2; text-align: center; margin: 40px 0px 0px 35px;'>"
						+ "<div style='width: 700px; height: 85px; padding: 20px 0px; text-align: center;'>&nbsp;<img src='"
						+ merchantLogo + "' height='80' /></div>"
						+ "<div style='background-color: #fff; padding: 20px 0px 0px 0px; width: 90%; margin: 0 auto 65px auto;'>"
						+ "<p style='font-size: 20px; color: #e6880f;'>Dear " + merchantName + ",</p>"
						+ "<p style='font-size: 14px;'>You got an Online Order.&nbsp;</p>"
						+ "<div style='text-align: left; padding: 20px 20px;'>" + "<table>" + "<tbody>" + "<tr>"
						+ "<td width='230'>Order type</td>" + "<td>:</td>" + "<td>" + orderType + "</td>" + "</tr>" + "<tr>"
						+ "<td width='230'>Customer Name:</td>" + "<td>:</td>" + "<td>" + customerName + "</td>" + "</tr>" + "<tr>"
						+ "<td>Order ID</td>" + "<td>:</td>" + "<td>" + EncryptionDecryptionUtil.decryption(orderid) + "</td>" + "</tr>" + "<tr>"
						+ "<td>Payment</td>" + "<td>:</td>" + "<td>" + paymentType + "</td>" + "</tr>"+ "<tr>"
						+ "<td>Order PosId</td>" + "<td>:</td>" + "<td>" + orderPosId + "</td>" + "</tr>"
						
						+ "</tbody>"
						+ "</table>" + "<p><strong>Order Details:</strong><br /></p>" + orderDetails + "</p>"
						+ "<p style='margin: 0;'>-------------------------------------------------------------------------------</p>"
						+ "<table>" + "<tbody>" + "<tr>" + "<td width='230'>Sub-total</td>" + "<td>&nbsp;</td>" + "<td>$"
						
						/*CHANGE Convenience to Online*/
						/*+ subTotal + "</td>" + "</tr>" + "<tr>" + "<td>Convenience Fee</td>" + "<td>&nbsp;</td>" + "<td>$"*/
						+ String.format("%.2f",Float.parseFloat(subTotal)) + "</td>" + "</tr>";
						if(uberCharges!= null && uberCharges > 0.0){
							msg += "<tr><td>Uber Charges</td>" + "<td>&nbsp;</td>"
							+ "<td>$" +String.format("%.2f",Float.parseFloat(uberCharges.toString())) + "</td>" + "</tr>";
						}
				
				msg = msg +"<tr>" + "<td>Tax</td>" + "<td>&nbsp;</td>" + "<td>$" + String.format("%.2f",Float.parseFloat(tax)) + "</td>" + "</tr>" ;
				
				
				
	        msg = msg	+  "</tbody>" + "</table>"
						+ "<p style='margin: 0;'>-------------------------------------------------------------------------------</p>"
						+ "<table>" + "<tbody>" + "<tr>" + "<td width='230'>Total</td>" + "<td>&nbsp;</td>" + "<td>$"
						+ String.format("%.2f",orderPrice) + "</td>" + "</tr>" + "</tbody>" + "</table>"
						+ "<br /><br /> <strong style='text-decoration: underline;'>Special Instructions :</strong> " + note
						+ " <br /><br /><br /> <a style='text-decoration: none; margin: 16px 0px; display: inline-block; background-color: #f8981d; color: #fff; padding: 10px 12px; border-radius: 10px; font-weight: 600; font-size: 15px;' href='"+orderLink+"'"
								+ " target='_blank'>View Online Receipt</a><br /> " ;
	        	/*if(merchantId!=null &&  merchantId.equals(IConstant.TEXAN_REWARDS_MERCHANT_ID) ){
				msg = msg + "<div style='height: 70px; text-align: center;'>&nbsp;<img src='https://www.foodkonnekt.com/foodkonnekt_merchat_logo/274_PiePizzaria.png' alt='' width='200' height='63' /></div>"
						+ "</div>";
	        	}else{*/
				msg = msg +"<div style='height: 70px; text-align: center;'>&nbsp;<img src='http://www.dev.foodkonnekt.com/app/foodnew.jpg' alt='' width='200' height='63' /></div></div>";
	        	//}
	        	msg = msg		+ "<p style='color: #676767; font-size: 11px;'>Automated email. Please do not reply to this sender email.</p>"
						+ "</div>";
	        	
	        	
	        	 messageBodyPart.setContent(msg, "text/html");
	 	          multipart.addBodyPart(messageBodyPart);
	 	        
	 	        
	 	        DataSource source = new FileDataSource(environment.getProperty("FAX_FILE_PATH")+Integer.parseInt(EncryptionDecryptionUtil.decryption(orderid))+".pdf");

	 	        attachementBodyPart.setDataHandler(new DataHandler(source));

	 	        attachementBodyPart.setFileName("Order_"+Integer.parseInt(EncryptionDecryptionUtil.decryption(orderid))+".pdf");

	 	        multipart.addBodyPart(attachementBodyPart);
	 	        
	 	        DataSource source1 = new FileDataSource(environment.getProperty("THERMAL_RECEIPT")+Integer.parseInt(EncryptionDecryptionUtil.decryption(orderid))+".pdf");
		        thermalReceiptAttachment.setDataHandler(new DataHandler(source1));
		        thermalReceiptAttachment.setFileName("Thermal Receipt_"+Integer.parseInt(EncryptionDecryptionUtil.decryption(orderid))+".pdf");
		        multipart.addBodyPart(thermalReceiptAttachment);
		       
	 	        message.setContent(msg, "text/html");
	 	        message.setContent(multipart);
				Transport.send(message);
				LOGGER.info(" MailSendUtil :: onlineOrderNotification : mail send successfully");
				IConstant.mailStatus="sent";
			} catch (MessagingException e) {
				LOGGER.error(" MailSendUtil :: onlineOrderNotification : Exception "+e);
			}	
			LOGGER.info("----------------End :: MailSendUtil : onlineOrderNotification------------------");
			return false;
			
		}
	 
	 public static boolean sendMonthlyFundRaiserReport(Merchant merchant,String month) {
			try {
	
					LOGGER.info("merchantId : "+merchant.getId());
					Message message = new MimeMessage(
							SendMailProperty.gmailMailProperty(IConstant.FROM_EMAIL_ID,
									IConstant.FROM_PASSWORD));
					message.setFrom(new InternetAddress(IConstant.FROM_EMAIL_ID));
					message.setRecipients(Message.RecipientType.TO,InternetAddress.parse(merchant.getToMail()));
				
					//message.setRecipients(Message.RecipientType.TO,InternetAddress.parse("bablugoswamidevloper@gmail.com"));
					message.setSubject("Fundraiser Efforts For "+month);
					String msg = "";
					msg = msg + "<div> Hello " + merchant.getPartnerName() + ",</div> " ;
					
		             msg =   msg  + " <p>Please find below the report for the month of "+month+". Let us know if you have any questions. </p> ";
		             
					msg =msg+"<head><style>table {  font-family: arial, sans-serif;  border-collapse: collapse;  width: 60%;}td, th {  border: 1px solid #dddddd;  text-align: center;  padding: 8px;} th { background-color:#FF9800;}"
	                      +"tr:nth-child(even) {  background-color: #dddddd;}</style></head><body>"
							+ "<br />";
					msg=msg+"<table><tr><th>Order Date</th><th>Order Id</th><th>Order Amount($)</th><th>Donated Amount($)</th></tr>";
					if(merchant.getOrdersList().size()>0) {
					 for(OrderR orderR : merchant.getOrdersList()) {
				    LOGGER.info("inside sendMonthlyFundRaiserReport orderR : "+ orderR.getOrderId());
					
					msg=msg+"<tr><td>"+orderR.getOrderDate()+"</td><td>"+orderR.getOrderId()+"</td><td>"+orderR.getOrderAmount()+"</td><td>"+orderR.getDonatedAmount()+"</td></tr>";
					
					//msg = msg + "</table><div style='height: 70px; text-align: center;'>&nbsp;<img src='http://www.dev.foodkonnekt.com/app/foodnew.jpg' alt='' width='200' height='63' /></div></div>";
					}
					msg = msg + "</table></div>";
					
					 msg =  msg +  " <div><br><br> Best </div> " +
			                    " <div>"+ merchant.getName() +" Team</div> ";
					
					msg = msg + "<p style='color: #676767; font-size: 11px;'>Automated email. Please do not reply to this sender email.</p>"
							  + "</div></body>";
					message.setContent(msg, "text/html");
					
					Transport.send(message);
					}
				
			} catch (MessagingException e) {
				LOGGER.error("===============  MailSendUtil : Inside sendMonthlyFundRaiserReport :: Exception  ============= " + e);
				return false;
			}
			LOGGER.info("===============  MailSendUtil : Inside sendMonthlyFundRaiserReport :: End  ============= ");

			return false;
		}
	 
	 public static String sendPrintFailMail(PrintJob printJob,Environment environment) {
			try {
				LOGGER.info("sendAutoStartmail :locationName: "+printJob.getMerchant().getName());
				LOGGER.info("sendAutoStartmail :merchantPosId: "+printJob.getMerchant().getPosMerchantId());
				Message message = new MimeMessage(
						SendMailProperty.gmailMailProperty(IConstant.FROM_EMAIL_ID, IConstant.FROM_PASSWORD));
				message.setFrom(new InternetAddress(IConstant.FROM_EMAIL_ID));
				message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(environment.getProperty("MAIL_IDS")));
				message.setSubject("Auto Start App");
				String msg = "<div style='width: 700px; height: auto; border: solid 1px #ddd; box-shadow: 0px 0px 6px #ddd; background-color: #f2f2f2; text-align: center; margin: 40px 0px 0px 35px;'><div style='width: 700px; height: 85px; padding: 20px 0px; text-align: center;'>&nbsp;<img src='' height='80' />"
						
						+ "</div><div style='background-color: #fff; padding: 20px 0px 0px 0px; width: 90%; margin: 0 auto 65px auto;'><p style='font-size: 20px; color: #e6880f;'>Dear "+printJob.getMerchant().getName();
						
						msg+= ",</p><p style='font-size: 14px;'> Order is not printed : "+new Date()+" merchantId :: "+printJob.getMerchant().getPosMerchantId()+" OrderId :: "+printJob.getOrderId()+" </p><div style='text-align: left; padding: 5px 20px;'><br /><br /><p></p></div>";
				 msg = msg +"</div>'";
					message.setContent(msg, "text/html");
				Transport.send(message);
				return "true";
			} catch (MessagingException e) {
				LOGGER.info("Exception :"+e);
				sendExceptionByMail(e,environment);
			}
			return "false";
		}
}
