package com.foodkonnekt.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.commons.httpclient.util.URIUtil;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;

import com.firstdata.payeezy.models.transaction.PayeezyResponse;
import com.foodkonnekt.model.Address;
import com.foodkonnekt.model.ApplicationDto;
import com.foodkonnekt.model.Customer;
import com.foodkonnekt.model.CustomerEntityDTO;
import com.foodkonnekt.model.Merchant;
import com.foodkonnekt.model.PaymentGateWay;
import com.google.gson.Gson;
import com.google.gson.JsonArray;

public class ProducerUtil {

    /**
     * customer signin
     * 
     * @param customerJson
     * @return
     */
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ProducerUtil.class);
	
	public static String signIn(String customerJson,Environment environment) {
    	LOGGER.info("===============  ProducerUtil : Inside signIn :: Start  ============= ");

        HttpPost postRequest = new HttpPost(environment.getProperty("IP_BASE_URL") + "/LoginByCustomer");
        LOGGER.info("===============  ProducerUtil : Inside signIn :: End  ============= ");

        return convertToStringJson(postRequest, customerJson);
    }
    
   
    
    public static String createOrganizationOnTrumpia(String orgName,String orgDescription,Environment environment) throws UnsupportedEncodingException {
        LOGGER.info("===============  ProducerUtil : Inside createOrganizationOnTrumpia :: Start  ============= ");

    	HttpGet request = new HttpGet(environment.getProperty("CommunicationPlatform_BASE_URL_SERVER") + "/createFKOrg?orgName="+URLEncoder.encode(orgName, "UTF-8")+"&orgDescription="+URLEncoder.encode(orgDescription, "UTF-8"));
       LOGGER.info("===============  ProducerUtil : Inside createOrganizationOnTrumpia :: End  ============= ");

    	return convertToStringJson(request);
    }
    
    public static String customerDataThroughUid(String customerUid,Environment environment) throws UnsupportedEncodingException {
       LOGGER.info("===============  ProducerUtil : Inside customerDataThroughUid :: Start  =============customerUid "+customerUid);

    	HttpGet request = new HttpGet(environment.getProperty("MkonnektPlatform_BASE_URL") + "entity/findByCustomerByCustomerUid?customerUid="+ customerUid);
       LOGGER.info("===============  ProducerUtil : Inside customerDataThroughUid :: End  ============= ");

    	return convertToStringJson(request);
    }
    
    public static String generateLogs(String appName,String event,String log,Environment environment) throws UnsupportedEncodingException {
    	 log =URLEncoder.encode(log, "UTF-8");
    	 appName=URLEncoder.encode(appName, "UTF-8");
    	 event=URLEncoder.encode(event, "UTF-8");
        HttpGet request = new HttpGet(environment.getProperty("BASE_URL")+"/logs?appName="+appName+"&event="+event+"&log="+log);
        return convertToStringJson(request);
    }
     public static String futureAmountRefund(Integer customerUid,Environment environment) throws UnsupportedEncodingException {
       LOGGER.info("===============  ProducerUtil : Inside futureAmountRefund :: Start  =============customerUid "+customerUid);

    	 HttpGet request = new HttpGet(environment.getProperty("WEB_BASE_URL") + "/chargeRefundByOrderId?orderId="+ customerUid);
       LOGGER.info("===============  ProducerUtil : Inside futureAmountRefund :: End  ============= ");

    	 return convertToStringJson(request);
    }
    
    /**
     * Hit specific url
     * 
     * @param postRequest
     * @param customerJson
     * @return json response
     */
    public static String convertToStringJson(HttpPost postRequest, String customerJson) {
        StringBuilder responseBuilder = new StringBuilder();
        try {
        	LOGGER.info("responseBuilder postRequest:: "+postRequest);
        	LOGGER.info("responseBuilder customerJson:: "+customerJson);
            HttpClient httpClient = HttpClientBuilder.create().build();
            StringEntity input = new StringEntity(customerJson);
            input.setContentType("application/json");
            postRequest.setEntity(input);
            LOGGER.info("convertToStringJson : "+postRequest.toString());
            HttpResponse response = httpClient.execute(postRequest);
            LOGGER.info("Output from Server .... \n");
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            String line = "";
            while ((line = rd.readLine()) != null) {
                responseBuilder.append(line);
            }
            // LOGGER.info(responseBuilder.toString());
        } catch (MalformedURLException e) {
            LOGGER.error("error: " + e.getMessage());
        } catch (IOException e) {
            LOGGER.error("error: " + e.getMessage());
        }
        LOGGER.info("responseBuilder :: "+responseBuilder);
        return responseBuilder.toString();
    }

    /**
     * Set customer from json
     * 
     * @param jCustomer
     * @return
     */
    public static Customer getCustomer(JSONObject jCustomer) {
    	LOGGER.info("===============  ProducerUtil : Inside getCustomer :: Start  ============= ");

        Customer customer = new Customer();
        Merchant merchant = new Merchant();
        customer.setId(jCustomer.getInt("id"));
        customer.setFirstName(jCustomer.getString("firstName"));
        if (!jCustomer.isNull("lastName")) {
            customer.setLastName(jCustomer.getString("lastName"));
        }
        
      
LOGGER.info("jCustomer.isNull(\"customerPosId\") === "+jCustomer.isNull("customerPosId"));
        if(jCustomer.toString().contains("customerPosId")){
        	if(!jCustomer.isNull("customerPosId")){
        	if(!jCustomer.get("customerPosId").toString().isEmpty() )
        customer.setCustomerPosId(jCustomer.getString("customerPosId"));
        	}
        }
        
        if (jCustomer.has("password")) {
            customer.setPassword(jCustomer.getString("password"));
        }
        if (jCustomer.has("emailId"))
        customer.setEmailId(jCustomer.getString("emailId"));
        
        if(jCustomer.has("phoneNumber") && !jCustomer.isNull("phoneNumber"))
        customer.setPhoneNumber(jCustomer.getString("phoneNumber"));
        
        merchant.setId(jCustomer.getJSONObject("merchantt").getInt("id"));
        if (!jCustomer.isNull("image")) {
            customer.setImage(jCustomer.getString("image"));
        }
        customer.setMerchantt(merchant);
        if (!jCustomer.isNull("birthDate")) {
            customer.setBirthDate(jCustomer.getString("birthDate"));
        }
        if (!jCustomer.isNull("anniversaryDate")) {
            customer.setAnniversaryDate(jCustomer.getString("anniversaryDate"));
        }
        if (!jCustomer.isNull("addresses")) {
            customer.setAddresses(getAddress(jCustomer.getJSONArray("addresses")));
        }
        LOGGER.info("===============  ProducerUtil : Inside getCustomer :: End  ============= ");

        return customer;
    }

    /**
     * Set address object
     * 
     * @param jCustomer
     * @return
     */
    public static List<Address> getAddress(JSONArray jCustomerArray) {
    	LOGGER.info("===============  ProducerUtil : Inside getAddress :: Start  ============= ");

        List<Address> addresses = new ArrayList<Address>();
        for (Object jObj : jCustomerArray) {
            Address address = new Address();
            JSONObject jCustomer = (JSONObject) jObj;
            address.setId(jCustomer.getInt("id"));
            if (!jCustomer.isNull("address1")) {
                address.setAddress1(jCustomer.getString("address1"));
            }else{
            	address.setAddress1("");
            }
            
            if (!jCustomer.isNull("address2") && !jCustomer.get("address2").equals("")) {
                address.setAddress2(jCustomer.getString("address2"));
            }else{
                address.setAddress2("");
            }

            if (!jCustomer.isNull("city")) {
            	address.setCity(jCustomer.getString("city"));
            }else{
            	address.setCity("");
            }
            
            if (!jCustomer.isNull("state")) {
            	address.setState(jCustomer.getString("state"));
            }else{
            	address.setState("");
            }
            
            if (!jCustomer.isNull("zip")) {
            	address.setZip(jCustomer.getString("zip"));
            }else{
            	address.setZip("");
            }
            
            if (!jCustomer.isNull("country") && !jCustomer.get("country").equals("")) {
            	address.setCountry(jCustomer.getString("country"));
            }else{
            	address.setCountry("");
            }
            
            if(!jCustomer.isNull("addressPosId"))
            address.setAddressPosId(jCustomer.getString("addressPosId"));

            if (!jCustomer.isNull("deliveryFee")) {
                address.setDeliveryFee(jCustomer.getDouble("deliveryFee"));
            }
            
            if (!jCustomer.isNull("deliveryPosId")) {
                address.setDeliveryPosId(jCustomer.getString("deliveryPosId"));
            }
            addresses.add(address);
        }
        LOGGER.info("===============  ProducerUtil : Inside getAddress :: End  ============= ");

        return addresses;
    }

    /**
     * Get customer details by customerId
     * 
     * @param customerId
     * @return
     */
    public static String getCustomerProfile(Integer customerId,Environment environment) {
    	LOGGER.info("===============  ProducerUtil : Inside getCustomerProfile :: Start  =============customerId "+customerId);

        HttpGet request = new HttpGet(environment.getProperty("BASE_URL") + "/customerProfile?customerId=" + customerId);
       LOGGER.info("===============  ProducerUtil : Inside getCustomerProfile :: End  ============= ");

        return convertToStringJson(request);
    }
    
    public static String getFaceBookTabs(String pageId) {
        HttpGet request = new HttpGet("https://graph.facebook.com/v2.10/"+pageId+"/tabs/app_608947722608987?access_token=608947722608987%7C0xPRJ3XXSshXoHiIR7czih9nMnc");
        return convertToStringJson(request);
    }

    public static String convertToStringJson(HttpGet request) {
        HttpClient client = HttpClientBuilder.create().build();
        HttpResponse response = null;
        StringBuilder responseBuilder = new StringBuilder();
        try {
            response = client.execute(request);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            String line = "";
            while ((line = rd.readLine()) != null) {
                responseBuilder.append(line);
               // LOGGER.info("secont    "+responseBuilder);
            }
        } catch (IOException e) {
            LOGGER.error("error: " + e.getMessage());
        }
        LOGGER.info("third   "+responseBuilder);
        return responseBuilder.toString();
    }

    public static Customer getAddress(JSONArray customerJson, Customer customer) {
    	LOGGER.info("===============  ProducerUtil : Inside getAddress :: Start  ============= ");

        List<Address> addresses = new ArrayList<Address>();
        for (Object jObj : customerJson) {
            Address address = new Address();
            JSONObject jCustomer = (JSONObject) jObj;
            address.setId(jCustomer.getInt("id"));
            address.setAddress1(jCustomer.getString("address1"));
            address.setAddress2(jCustomer.getString("address2"));
            // address.setAddress3(jCustomer.getString("address3"));
            address.setCity(jCustomer.getString("city"));
            address.setState(jCustomer.getString("state"));
            address.setZip(jCustomer.getString("zip"));
            address.setCustomer(customer);
            addresses.add(address);
        }
        customer.setAddresses(addresses);
    	LOGGER.info("===============  ProducerUtil : Inside getAddress :: End  ============= ");

        return customer;
    }

    /**
     * Call update profile
     * 
     * @param customerJson
     * @return
     */
    public static String updateCustomer(String customerJson,Environment environment) {
    	LOGGER.info("===============  ProducerUtil : Inside updateCustomer :: Start  ============= ");

        HttpPost postRequest = new HttpPost(environment.getProperty("IP_BASE_URL") + "/updateProfile");
        LOGGER.info("===============  ProducerUtil : Inside updateCustomer :: End  ============= ");

        return convertToStringJson(postRequest, customerJson);
    }

    /**
     * Convert address to customer
     * 
     * @param address
     * @return
     */
    public static Customer setCustomer(Address address) {
    	LOGGER.info("===============  ProducerUtil : Inside setCustomer :: Start  ============= ");

        Customer customer = new Customer();
        Merchant vendor = new Merchant();
        if (address.getCustomer().getId() != null) {
            customer.setId(address.getCustomer().getId());
        }
        customer.setFirstName(address.getCustomer().getFirstName());
        customer.setEmailId(address.getCustomer().getEmailId());
        customer.setPhoneNumber(address.getCustomer().getPhoneNumber());
        customer.setPassword(address.getCustomer().getPassword());
        customer.setCheckId(address.getCustomer().getCheckId());
        vendor.setId(address.getCustomer().getMerchantt().getId());
        customer.setMerchantt(vendor);
        customer.setAddresses(setAddress(address));
        LOGGER.info("===============  ProducerUtil : Inside setCustomer :: End  ============= ");

        return customer;
    }

    public static Customer setCusotmerInfo(Customer customer, Integer merchantId) {
    	
    	LOGGER.info("===============  ProducerUtil : Inside setCusotmerInfo :: Start  ============= ");
        Customer finalCustomer = new Customer();
        Merchant merchant = new Merchant();
        if (customer.getId() != null) {
            finalCustomer.setId(customer.getId());
        }
        if(customer.getLastName()!=null)
            finalCustomer.setLastName(customer.getLastName());
        finalCustomer.setFirstName(customer.getFirstName());
        finalCustomer.setEmailId(customer.getEmailId());
        finalCustomer.setPhoneNumber(customer.getPhoneNumber());
        finalCustomer.setPassword(customer.getPassword());
        finalCustomer.setCheckId(customer.getCheckId());
        merchant.setId(merchantId);
        merchant.setIsFBAppPublished(null);
        finalCustomer.setMerchantt(merchant);
        finalCustomer.setVendor(customer.getVendor());
        finalCustomer.setFreekwentBox(customer.getFreekwentBox());
        finalCustomer.setLoyalityProgram(customer.getLoyalityProgram());
        List<Address> addresses = new ArrayList<Address>();
        Address address = new Address();
        if(customer.getAddress1()!=null){
        	 address.setAddress1(customer.getAddress1());
        }else{
        	 address.setAddress1("");
        }
       
        if(customer.getAddress2()!=null && customer.getAddress2()!=""){
        	 address.setAddress2(customer.getAddress2());
        }else{
        	 address.setAddress2("");
        }
        
        if(customer.getCity()!=null){
        	address.setCity(customer.getCity());
        }else{
        	address.setCity("");
        }
        
        if(customer.getState()!=null){
        	address.setState(customer.getState());
        }else{
        	address.setState("");
        }
        
        if(customer.getZip()!=null){
        	address.setZip(customer.getZip());
        }else{
        	address.setZip("");
        }
        
        if(customer.getCountry()!=null && customer.getCountry()!=""){
        	address.setCountry(customer.getCountry());
        }else{
        	address.setCountry("");
        }
        
        addresses.add(address);
        finalCustomer.setAddresses(addresses);
        LOGGER.info("===============  ProducerUtil : Inside setCusotmerInfo :: End  ============= ");
        return finalCustomer;
    }

    /**
     * Set addresses
     * 
     * @param addressObj
     * @return
     */
    private static List<Address> setAddress(Address addressObj) {
    	LOGGER.info("===============  ProducerUtil : Inside setAddress :: Start  ============= ");

        List<Address> addresses = new ArrayList<Address>();
        Address address = new Address();
        address.setAddress1(addressObj.getAddress1());
        address.setAddress2(addressObj.getAddress2());
        address.setCity(addressObj.getCity());
        address.setState(addressObj.getState());
        address.setZip(addressObj.getZip());
        addresses.add(address);
        LOGGER.info("===============  ProducerUtil : Inside setAddress :: End  ============= ");

        return addresses;
    }

    public static com.foodkonnekt.model.Customer getsignUpCustomer(JSONObject jCustomer) {
    	LOGGER.info("===============  ProducerUtil : Inside getsignUpCustomer :: Start  ============= ");

        Customer customer = new Customer();
        Merchant merchant = new Merchant();
        customer.setId(jCustomer.getInt("id"));
        customer.setFirstName(jCustomer.getString("firstName"));
        if (jCustomer.has("lastName") && !jCustomer.isNull("lastName")) {
        	customer.setLastName(jCustomer.getString("lastName"));
		}
        customer.setEmailId(jCustomer.getString("emailId"));
        customer.setPhoneNumber(jCustomer.getString("phoneNumber"));
        customer.setPassword(jCustomer.getString("password"));
        if(jCustomer.has("customerPosId")&& !jCustomer.isNull("customerPosId")){
        customer.setCustomerPosId(jCustomer.getString("customerPosId"));
        customer.setEmailPosId(jCustomer.getString("emailPosId"));
        customer.setPhonePosId(jCustomer.getString("phonePosId"));
        }
        merchant.setId(jCustomer.getJSONObject("merchantt").getInt("id"));
        customer.setMerchantt(merchant);
        if (!jCustomer.isNull("addresses")) {
            customer.setAddresses(getAddress(jCustomer.getJSONArray("addresses")));
        }
        if (!jCustomer.isNull("image")) {
            customer.setImage(jCustomer.getString("image"));
        }
        LOGGER.info("===============  ProducerUtil : Inside getsignUpCustomer :: End  ============= ");

        return customer;
    }

    /**
     * Request for forgot password
     * 
     * @param customerJson
     * @return
     */
    public static String forgotPassword(String customerJson,Environment environment) {
    	LOGGER.info("===============  ProducerUtil : Inside forgotPassword :: Start  ============= ");

        HttpPost postRequest = new HttpPost(environment.getProperty("IP_BASE_URL") + "/forgotPasswordByEmail");
       LOGGER.info("===============  ProducerUtil : Inside forgotPassword :: End  ============= ");

        return convertToStringJson(postRequest, customerJson);
    }

    public static Customer setAddresses(Customer customer) {
    	LOGGER.info("===============  ProducerUtil : Inside setAddresses :: Start  ============= ");

        List<Address> list = new ArrayList<Address>();
        Address address = null;
        if(customer != null)
        address = customer.getAddresses().get(0);
        
		String[] city = null;
		String[] add1 = null;
		String[] add2 = null;
		String[] state = null;
		String[] zip = null;
		String[] country = null;

		int add2Length = 0;
		int add1Length = 0;

		if (address != null && address.getCity() != null && address.getCity() != "") {
			city = address.getCity().split(",");
		}
		if (address != null && address.getAddress1() != null && address.getAddress1() != "") {
			add1 = address.getAddress1().split(",");
		}

		if (address != null && address.getAddress2() != null && address.getAddress2() != "") {
			add2 = address.getAddress2().split(",");
		}

		if (add2 != null) {
			add2Length = add2.length;
		}
		if (add1 != null) {
			add1Length = add1.length;
		}
		
		if (address != null && address.getCountry() != null && address.getCountry() !="") {
			country = address.getCountry().split(",");
		}

		if (address != null && address.getState() != null && address.getState() !="") {
			state = address.getState().split(",");
		}
		if (address != null && address.getZip() != null && address.getZip() != "") {
			zip = address.getZip().split(",");
		}
		if (city != null) {
			for (int i = 0; i < city.length; i++) {
				Address address2 = new Address();
				if (i == 0) {
					address2.setId(address.getId());
					address2.setAddressPosId(address.getAddressPosId());
				}
				if (add1Length > i) {
					address2.setAddress1(add1[i]);
				}
				if (add2Length > i) {
					address2.setAddress2(add2[i]);
				}
				address2.setCity(city[i]);				
			    if(state!= null){
				 address2.setState(state[i]);
				}
				if(zip!=null) {
				 address2.setZip(zip[i]);
				}
				if(country!= null){
					address2.setCountry(country[i]);
				}
				list.add(address2);
			}

			customer.setAddresses(list);
		}
		LOGGER.info("===============  ProducerUtil : Inside setAddresses :: End  ============= ");

        return customer;
    }

    /**
     * Validate address
     * 
     * @param address
     * @return
     * @throws UnsupportedEncodingException
     */
    public static String validateAddress(Address address) throws UnsupportedEncodingException {
        String userId = "327MKONN4730";
        String encodeUrl = "<AddressValidateRequest USERID='" + userId + "'><Address><Address1>"
                        + address.getAddress1() + "</Address1> <Address2>"
                        + "</Address2> <City>" + address.getCity() + "</City><State>" + address.getState()
                        + "</State><Zip5>" + address.getZip()
                        + "</Zip5> <Zip4></Zip4></Address></AddressValidateRequest>";
        HttpGet request = new HttpGet("http://production.shippingapis.com/ShippingAPI.dll?API=Verify&XML="
                        + URLEncoder.encode(encodeUrl, "UTF-8"));
        return convertToStringJson(request);
    }

    /**
     * Check duplicate emailId
     * 
     * @param customerJson
     * @return
     */
    public static String duplicateEmail(String customerJson, Merchant merchant,Integer vendorId,Environment environment) {
       LOGGER.info("===============  ProducerUtil : Inside duplicateEmail :: Start  ============= ");

    	HttpPost postRequest = new HttpPost(environment.getProperty("IP_BASE_URL") + "/checkEmailId?merchantId=" + merchant.getId()+"&vendorId=" + vendorId);
       LOGGER.info("===============  ProducerUtil : Inside duplicateEmail :: End  ============= ");

    	return convertToStringJson(postRequest, customerJson);
    }

    /**
     * Check delivery zone
     * 
     * @param addressJson
     * @return
     */
    public static String checkDeliveryZone(String addressJson,Environment environment) {
    	LOGGER.info("===============  ProduceUtil : Inside checkDeliveryZone :: Start  ============= ");
        HttpPost postRequest = new HttpPost(environment.getProperty("IP_BASE_URL") + "/checkDeliveryZone");
        LOGGER.info("===============  ProduceUtil : Inside checkDeliveryZone :: End  ============= ");
        return convertToStringJson(postRequest, addressJson);
    }
    
    public static String createUpdateCloverAddress(String addressJson,Environment environment) {
    	LOGGER.info("===============  ProducerUtil : Inside createUpdateCloverAddress :: Start  ============= ");

        HttpPost postRequest = new HttpPost(environment.getProperty("IP_BASE_URL") + "/createUpdateCloverAddress");
       LOGGER.info("===============  ProducerUtil : Inside createUpdateCloverAddress :: End  ============= ");

        return convertToStringJson(postRequest, addressJson);
    }

    /**
     * POST request for place order
     * 
     * @param finalOrderJson
     * @param merchant
     * @return
     */
    public static String placeOrder(String finalOrderJson, Merchant merchant,Environment environment) {
    	 LOGGER.info("----------------Start :: ProducerUtil : placeOrder------------------------");
    	 LOGGER.info("ProducerUtil :: placeOrder : merchantId "+merchant.getId());
        HttpPost postRequest = new HttpPost(environment.getProperty("IP_BASE_URL") + "/postOrderOnClover?merchant_id="
                        + merchant.getPosMerchantId() + "&access_token=" + merchant.getAccessToken());
         LOGGER.info("----------------End :: ProducerUtil : placeOrder------------------------");
        return convertToStringJson(postRequest, finalOrderJson);
    }

    /**
     * Payment API for clover order
     * 
     * @param paymentJson
     * @param merchant
     * @return
     */
    public static String orderPayment(String paymentJson, Merchant merchant,Environment environment) {
    	LOGGER.info("===============  ProducerUtil : Inside orderPayment :: Start  ============= ");

        HttpPost postRequest = new HttpPost(environment.getProperty("IP_BASE_URL") + "/cloverOrderPayment?merchant_id="
                        + merchant.getPosMerchantId() + "&access_token=" + merchant.getAccessToken());
      LOGGER.info("===============  ProducerUtil : Inside orderPayment :: End  ============= ");

        return convertToStringJson(postRequest, paymentJson);
    }

    public static String callMeteredAPI(Integer merchantId,Environment environment) {
        HttpGet request = new HttpGet(environment.getProperty("BASE_URL") + "/addPerOrderCharge?merchantId=" + merchantId);
        return convertToStringJson(request);
    }

    public static String validateAddress1(Address address) throws UnsupportedEncodingException {
       LOGGER.info("===============  ProducerUtil : Inside validateAddress1 :: Start  ============= ");

    	String userId = "327MKONN4730";
        String encodeUrl = "<AddressValidateRequest USERID='" + userId + "'><Address><Address1>"
                        + address.getAddress1() + " " + address.getAddress2() + "</Address1> <Address2>"
                        + "</Address2> <City>" + address.getCity() + "</City><State>" + address.getState()
                        + "</State><Zip5>" + address.getZip()
                        + "</Zip5> <Zip4></Zip4></Address></AddressValidateRequest>";
        LOGGER.info(" === Address as encoded url ==== : "+encodeUrl);
        HttpGet request = new HttpGet("http://production.shippingapis.com/ShippingAPI.dll?API=Verify&XML="
                        + URLEncoder.encode(encodeUrl, "UTF-8"));
       LOGGER.info("===============  ProducerUtil : Inside validateAddress1 :: End  ============= ");

        return convertToStringJson(request);
    }
    
    public static String createCustomerMkonnektPlatform(String CustomerEntityDTOJson,Environment environment) {
      LOGGER.info("===============  ProducerUtil : Inside createCustomerMkonnektPlatform :: Start  ============= ");

    	HttpPost postRequest = new HttpPost(environment.getProperty("MkonnektPlatform_BASE_URL")+"entity/createCustomer");
        LOGGER.info("===============  ProducerUtil : Inside createCustomerMkonnektPlatform :: End  ============= ");

    	return convertToStringJson(postRequest, CustomerEntityDTOJson);
    }
    
    public static String getCustomerEntityDTO(Customer customer){
    	LOGGER.info("===============  ProducerUtil : Inside getCustomerEntityDTO :: Start  ============= ");

    	//CustomerEntityDTO customerEntityDTO = null;
    	String customerJson = null;
    	if(customer!= null){
    		CustomerEntityDTO customerEntityDTO = new CustomerEntityDTO();
    		if(customer.getFirstName()!= null){
    			customerEntityDTO.setFirstName(customer.getFirstName());
    		}
    		if(customer.getLastName()!= null){
    			customerEntityDTO.setLastName(customer.getLastName());
    		}
    		if(customer.getEmailId()!= null){
    			customerEntityDTO.setEmailId(customer.getEmailId());
    		}
    		if(customer.getPhoneNumber()!= null){
    			customerEntityDTO.setPhoneNo(customer.getPhoneNumber());
    			customerEntityDTO.setMobileNumber(customer.getPhoneNumber());
    		}
    		if(customer.getBirthDate()!= null){
    			customerEntityDTO.setBirthDate(customer.getBirthDate());
    		}
    		if(customer.getAnniversaryDate()!= null){
    			customerEntityDTO.setAnniversaryDate(customer.getAnniversaryDate());
    		}
    		if(customer.getMerchantt()!=null && customer.getMerchantt().getMerchantUid()!=null)
    			customerEntityDTO.setLocationUId(customer.getMerchantt().getMerchantUid());
    		customerEntityDTO.setActive(null);
    		customerEntityDTO.setApplicationId(1);
    		Gson gson = new Gson();
            customerJson = gson.toJson(customerEntityDTO);
    	}
    	LOGGER.info("===============  ProducerUtil : Inside getCustomerEntityDTO :: End  ============= ");

    	return customerJson;
    }
    
	public static String sendFax(String orderid ,String customerid,Environment environment) throws UnsupportedEncodingException {
        LOGGER.info("===============  ProducerUtil : Inside sendFax :: Start  ============= ");

		HttpGet request = new HttpGet(environment.getProperty("IP_WEB_BASE_URL") +"/sendFax?orderid="+orderid+"&customerid="+customerid);
       LOGGER.info("===============  ProducerUtil : Inside sendFax :: End  ============= ");

		return convertToStringJson(request);
    }
	
	public static String dummyMethod(Environment environment) throws Exception {
        HttpGet request = new HttpGet(environment.getProperty("BASE_URL") +"/dummyView");
        return convertToStringJson(request);
    }
	
	public static String payeezyErrorResponse( PayeezyResponse    payeezyResponseCaputre )  {
	      JSONObject jObject = new JSONObject(payeezyResponseCaputre.getResponseBody());
          StringBuilder sb = new StringBuilder();
          if( jObject.has("Error")){
           JSONObject ja_data = jObject.getJSONObject("Error");
           JSONArray arr = ja_data.getJSONArray("messages");
           LOGGER.info("arr.length() === "+arr.length());
           for (int i = 0; i <arr.length(); i++) {
               LOGGER.info(arr.getJSONObject(i).getString("code"));
               sb.append(arr.getJSONObject(i).getString("code"));
               sb.append(" "+arr.getJSONObject(i).getString("description"));
               sb.append("\n");
           }
          }
		return sb.toString();
	}
	
	public static String get3DToken(PaymentGateWay paymentGateWay, PaymentGateWay details,Environment environment) {

        try {
        	LOGGER.info("===============  ProducerUtil : Inside get3DToken :: Start  ============= ");

        if(paymentGateWay.getCcType().toLowerCase().equalsIgnoreCase("master card")){
            paymentGateWay.setCcType("mastercard");
        }
        if(paymentGateWay.getCcType().toLowerCase().equalsIgnoreCase("american express")) {
            paymentGateWay.setCcType(URLEncoder.encode(paymentGateWay.getCcType(), "UTF-8"));
        }
        }
        catch (Exception e) {
            LOGGER.error("===============  ProducerUtil : Inside get3DToken :: Exception  ============= " + e);

        }
        String url = environment.getProperty("PAYEEZY_URL")+"/v1/securitytokens?apikey=" + environment.getProperty("PAYEEZY_APP_ID")
        + "&js_security_key=" + EncryptionDecryptionUtil.decryption(details.getPayeezyMerchnatTokenJs())
        + "&callback=Payeezy.callback&auth=false"
        + "&ta_token=" + EncryptionDecryptionUtil.decryption(details.getPayeezy3DTaToken())
        + "&type=FDToken&credit_card.type=" + paymentGateWay.getCcType()
        + "&credit_card.cardholder_name=" + paymentGateWay.getCustomerName()
        + "&credit_card.card_number=" + paymentGateWay.getCcNumber()
        + "&credit_card.exp_date=" + paymentGateWay.getExpMonthYear()
        + "&credit_card.cvv=" +paymentGateWay.getCcCode() + "&currency=USD";
               
        LOGGER.info("Calling API for payeezy is : "+ url);
        HttpGet request = new HttpGet(url);
    LOGGER.info("===============  ProducerUtil : Inside get3DToken :: End  ============= ");

        return convertToStringJson(request);
        
    }
	
	public static String removeSpecialCharecter(String merchnatName) {
		String alphaAndDigits = merchnatName.replaceAll("[^a-zA-Z0-9]+","");
        return alphaAndDigits;
    }
	 public static String saveMerchantNotificationAppStatus(Integer merchantId , String appId,Environment environment){
    	LOGGER.info("===============  ProducerUtil : Inside saveMerchantNotificationAppStatus :: Start  =============merchantId "+merchantId+" appId "+appId);

		 HttpGet getRequest = new HttpGet(environment.getProperty("BASE_URL") + "/createNotificationStatusForMerchant?merchantId="+merchantId+"&appId="+Integer.parseInt(appId));
    	 return convertToStringJson(getRequest);
    }
	 public static String acceptAndDeclineOrder(Integer orderid,String type,Environment environment) throws UnsupportedEncodingException {
		LOGGER.info("===============  ProducerUtil : Inside acceptAndDeclineOrder :: Start  =============orderid "+orderid+" type "+type);

		 String reason="merchant not accepted your order";
	        HttpGet request = new HttpGet(environment.getProperty("BASE_URL")+"/orderConfirmationById?orderId="+orderid+"&type="+type+"&reason="+URLEncoder.encode(reason));
	       System.out.print("first    "+request);
	       LOGGER.info("===============  ProducerUtil : Inside acceptAndDeclineOrder :: End  ============= ");

	       return convertToStringJson(request);
	        
	    }
	 
	    public static Customer customerPojo(Customer customer ,Customer customerDTO){
	    	//CustomerEntityDTO customerEntityDTO = null;
	    	String customerJson = null;
	    	if(customer!= null){
	    		CustomerEntityDTO customerEntityDTO = new CustomerEntityDTO();
	    		if(customer.getId()!= null){
	    			customerDTO.setId(customer.getId());
	    		}
	    		if(customer.getFirstName()!= null){
	    			customerDTO.setFirstName(customer.getFirstName());
	    		}
	    		if(customer.getLastName()!= null){
	    			customerDTO.setLastName(customer.getLastName());
	    		}
	    		if(customer.getEmailId()!= null){
	    			customerDTO.setEmailId(customer.getEmailId());
	    		}
	    		if(customer.getPhoneNumber()!= null){
	    			customerDTO.setPhoneNumber(customer.getPhoneNumber());
	    		}
	    	
	    	}
	    	
	    	return customerDTO;
	    }
	    public static void acceptAndCancelOrder(int orderId, String type, String reason, int time,Environment environment){
	    	LOGGER.info("===============  ProducerUtil : Inside acceptAndCancelOrder :: Start  ============= ");

	    	HttpGet getRequest = new HttpGet(environment.getProperty("BASE_URL") + "/orderConfirmationById?orderId="+orderId+"&type="+type+"&reason="+URLEncoder.encode(reason)+"&changedOrderAvgTime="+time);
	    	  convertToStringJson(getRequest);
	    	  LOGGER.info("===============  ProducerUtil : Inside acceptAndCancelOrder :: End  ============= ");

		}
	    
	    public static String createKritiqCustomerMkonnektPlatform(String CustomerEntityDTOJson,String locationUid,Environment environment) {
	    	try{
	    		LOGGER.info("===============  ProducerUtil : Inside createKritiqCustomerMkonnektPlatform :: Start  =============CustomerEntityDTOJson "+CustomerEntityDTOJson+" locationUid "+locationUid);

	        HttpPost postRequest = new HttpPost(environment.getProperty("MkonnektPlatform_BASE_URL")+"entity/saveStandAloneCustomer/"+locationUid);
	        String response= convertToStringJson(postRequest, CustomerEntityDTOJson);
	        try{
				if (isJSONValid(response)) {
	        JSONObject jsonResult=new  JSONObject(response);
	        if(jsonResult.has("customer"))
            {
           	JSONObject customer = jsonResult.getJSONObject("customer");
               locationUid = customer.get("customerUid").toString();
            }
            return locationUid;
						} else {
							LOGGER.error("Inside createKritiqCustomerMkonnektPlatform :: response from mkonnekt Post Request : "
									+ response);
		               }
	        }catch(Exception e)
	        {
	        	System.out.println("createKritiqCustomerMkonnektPlatform : Exception :: "+e);
	        	LOGGER.error("===============  ProducerUtil : Inside createKritiqCustomerMkonnektPlatform :: Exception  ============= " + e);
	        }
	          }catch(Exception e)
	    	    {
	    	MailSendUtil.sendExceptionByMail(e,environment);
	    	LOGGER.error("error: " + e.getMessage());
	    	LOGGER.error("===============  ProducerUtil : Inside createKritiqCustomerMkonnektPlatform :: Exception  ============= " + e);
	    	  }
	    	return null;
	    	}
	    public static StringBuffer ticketStreamWithDate(String locationUid,String authorization,String toDayDate) throws Exception{
	    	StringBuffer response=null;
	    	try{
	    		LOGGER.info("===============  ProducerUtil : Inside ticketStreamWithDate :: Start  ============= ");

	    	URL url = new URL("https://thrive-control-center.grs-tcc.com/api/ticket-stream/orders/"+locationUid+"/"+toDayDate+"T00:00:00Z/"+toDayDate+"T23:59:59Z");
	        HttpURLConnection conn = (HttpURLConnection) url.openConnection();	
	        conn.addRequestProperty("Authorization","Bearer "+authorization);
	        conn.setRequestProperty("Content-Type","application/json");
	        conn.setRequestMethod("GET");
	        BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
	        String output;
	         response = new StringBuffer();
	        while ((output = in.readLine()) != null) {
	            response.append(output);
	        }
	        in.close();
	         LOGGER.info("Response:-" + response.toString());
	    	}catch(Exception e){
	    		LOGGER.error("===============  ProducerUtil : Inside ticketStreamWithDate :: Exception  ============= " + e);

	    		System.out.println(e);
	    	}
	    	LOGGER.info("===============  ProducerUtil : Inside ticketStreamWithDate :: End  ============= ");

	    	return response;
		}

	    public static String createCustomerIntoCentralDb(Customer CustomerEntityDTOJson,Environment environment) {
	    	String response =null;
	    	try{
	    		LOGGER.info("===============  ProducerUtil : Inside createCustomerIntoCentralDb :: Start  ============= ");
	    		LOGGER.info("createCustomerIntoCentralDb ::-" + environment.getProperty("BASE_URL")+"/centralDbIntegration");
	        HttpPost postRequest = new HttpPost(environment.getProperty("BASE_URL")+"/centralDbIntegration");
	       return(convertToStringJson(postRequest, new Gson().toJson(CustomerEntityDTOJson)));
	        
	         }catch (Exception e) {
	        	 LOGGER.error("===============  ProducerUtil : Inside createCustomerIntoCentralDb :: Exception  ============= " + e);

				LOGGER.error("error: " + e.getMessage());
			}
	    	LOGGER.info("===============  ProducerUtil : Inside createCustomerIntoCentralDb :: End  ============= ");

	        return response;
	    }
	    public static String getCustomerUid(String CustomerEntityDTOJson,Environment environment) {
	    	LOGGER.info("===============  ProducerUtil : Inside getCustomerUid :: Start  ============= ");

	    	String response =null;
	    	try{
	        HttpPost postRequest = new HttpPost(environment.getProperty("MkonnektPlatform_BASE_URL")+"entity/saveCustomerForAllPlatform");
	        JSONObject jsonResult=new  JSONObject(convertToStringJson(postRequest, CustomerEntityDTOJson));
	        LOGGER.info("json data : "+jsonResult);
	        if(jsonResult.has("Data")){
	        	JSONObject customer = jsonResult.getJSONObject("Data");
	        	response = customer.get("customerUId").toString();
	        }
	    }catch (Exception e) {
	    	LOGGER.error("===============  ProducerUtil : Inside getCustomerUid :: Exception  ============= " + e);

				LOGGER.error("error: " + e.getMessage());
			}
	    	LOGGER.info("===============  ProducerUtil : Inside getCustomerUid :: End  ============= ");

	        return response;
	    }
	    
	    public static String createCustomerIntoFreekwent(String CustomerEntityDTOJson,Environment environment) {
	    	String response =null;
	    	try{
	    		LOGGER.info("===============  ProducerUtil : Inside createCustomerIntoFreekwent :: Start  ============= ");
	    		LOGGER.info("createCustomerIntoFreekwent : "+environment.getProperty("FREEKWENTBASEURL")+"registerCustomer");
	        HttpPost postRequest = new HttpPost(environment.getProperty("FREEKWENTBASEURL")+"registerCustomer");
	        return convertToStringJson(postRequest, CustomerEntityDTOJson);
	    }catch (Exception e) {
	    	LOGGER.error("===============  ProducerUtil : Inside createCustomerIntoFreekwent :: Exception  ============= " + e);

				LOGGER.error("error: " + e.getMessage());
			}
	    	LOGGER.info("===============  ProducerUtil : Inside createCustomerIntoFreekwent :: End  ============= ");

	        return response;
	    }
	    public static String callFreekWentApi(String customerOrderDetailJson,Environment environment) {
	    	LOGGER.info("===============  ProducerUtil : Inside callFreekWentApi :: Start  ============= ");
	    	LOGGER.info("===============  ProducerUtil : Inside callFreekWentApi :: URL :: "+environment.getProperty("FREEKWENTBASEURL") + "/addCustomerOrderDetail");
	        HttpPost postRequest = new HttpPost(environment.getProperty("FREEKWENTBASEURL") + "/addCustomerOrderDetail");
           LOGGER.info("===============  ProducerUtil : Inside callFreekWentApi :: End  ============= ");
	        return convertToStringJson(postRequest, customerOrderDetailJson);
	    }
	    public static Boolean isFreekwentInstallOrNot(String locationId, Integer productId,Environment environment)
				throws UnsupportedEncodingException {
			try{
			HttpGet request = new HttpGet(environment.getProperty("MkonnektPlatform_BASE_DEV_URL")
					+ "entity/getInstallStatusForAnyProduct?locationId=" + locationId + "&productId=" + productId);
			String responce = convertToStringJson(request);
			LOGGER.info("ProducerUtil : Inside isFreekwentInstallOrNot responce: "+responce);
			return true;
			}catch (Exception e) {
				LOGGER.info("ProducerUtil : Inside isFreekwentInstallOrNot Exception: "+e);
			}
			return false;
		}
	    public static StringBuffer googleApi(String request) throws Exception{
	    	StringBuffer response=null;
	    	try{
	    	URL url = new URL(request);
	        HttpURLConnection conn = (HttpURLConnection) url.openConnection();	
	        conn.setRequestProperty("Content-Type","application/json");
	        conn.setRequestMethod("GET");
	        BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
	        String output;
	         response = new StringBuffer();
	        while ((output = in.readLine()) != null) {
	            response.append(output);
	        }
	        in.close();
	         LOGGER.info("Response:-" + response.toString());
	    	}catch(Exception e){
	    		LOGGER.info("Exception ==== "+e);
	    	}
	    	return response;
		}
	    public static String getFaxStatus(String faxId) {
	        HttpGet request = new HttpGet("https://rest.interfax.net/outbound/faxes/"+faxId);
	   
	        HttpClient client = HttpClientBuilder.create().build();
	        HttpResponse response = null;
	        request.setHeader("Authorization", "Basic bWtvbm5la3Q6bUswbm4za3QxMjM=");
	        StringBuilder responseBuilder = new StringBuilder();
	        try {
	            response = client.execute(request);
	            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
	            String line = "";
	            while ((line = rd.readLine()) != null) {
	                responseBuilder.append(line);
	                LOGGER.info("secont    "+responseBuilder);
	            }
	        } catch (IOException e) {
	            LOGGER.error("error: " + e.getMessage());
	        }
	        LOGGER.info("third   "+responseBuilder);
	        return responseBuilder.toString();
	    }

	public static String convertToStringJson(HttpPost postRequest) {
		StringBuilder responseBuilder = new StringBuilder();
		try {
			HttpClient httpClient = HttpClientBuilder.create().build();

			LOGGER.info("convertToStringJson : " + postRequest.toString());
			HttpResponse response = httpClient.execute(postRequest);
			LOGGER.info(response.toString());
			BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			String line = "";
			while ((line = rd.readLine()) != null) {
				responseBuilder.append(line);
			}

		} catch (Exception e) {
			LOGGER.error("error: " + e.getMessage());
		}
		return responseBuilder.toString();
	}

	public static String getUberAccessToken(String scope) {

		LOGGER.info("===============  ProducerUtil : Inside getUberAccessToken :: Start  ============= ");

		String access_token = null;
		ArrayList<NameValuePair> postParameters;
		try {
			HttpPost postRequest = new HttpPost("https://login.uber.com/oauth/v2/token");
			postParameters = new ArrayList<NameValuePair>();
			postParameters.add(new BasicNameValuePair("client_id", IConstant.CLIENT_ID));
			postParameters.add(new BasicNameValuePair("client_secret", IConstant.CLIENT_SECRET));
			postParameters.add(new BasicNameValuePair("grant_type", IConstant.GRANT_TYPE));
			postParameters.add(new BasicNameValuePair("scope", scope));

			postRequest.setEntity(new UrlEncodedFormEntity(postParameters, "UTF-8"));

			String json = convertToStringJson(postRequest);
			JSONObject jsonObject = new JSONObject(json);
			access_token = jsonObject.getString("access_token");
		} catch (Exception e) {
			LOGGER.info("===============  Exception Inside getUberAccessToken ::exception : " + e);
		}
		LOGGER.info("===============  ProducerUtil : Inside getUberAccessToken :: End  ============= ");

		return access_token;
	}

	public static String uploadUberMenu(String menuJson, String storeId) {
		LOGGER.info("===============  ProducerUtil : Inside uploadUberMenu :: Start  ============= ");
		LOGGER.info("menuJson ===== " + menuJson);
		String response = null;
		try {
			String access_token = getUberAccessToken(IConstant.STORE_MENU_SCOPE);
			HttpPut putRequest = new HttpPut("https://api.uber.com/v2/eats/stores/" + storeId + "/menus");
			putRequest.addHeader("Authorization", "Bearer " + access_token);
			putRequest.addHeader("Content-Type", "application/json");
			LOGGER.info("===============  ProducerUtil : Inside uploadUberMenu :: End  ============= ");

			response = convertToStringJson(putRequest, menuJson);
			LOGGER.info("api response code== " + response);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			LOGGER.error("error: " + e.getMessage());
		}
		return response;
	}

	public static String validateStore(String storeId) {
		LOGGER.info("===============  ProducerUtil : Inside validateStore :: Start  ============= ");

		String response = null;
		String storeStatus = null;
		String access_token = getUberAccessToken(IConstant.STORE_MENU_SCOPE);

		LOGGER.info(" access_token === " + access_token);
		HttpGet getRequest = new HttpGet("https://api.uber.com/v1/eats/stores/" + storeId);
		getRequest.addHeader("Authorization", "Bearer " + access_token);
		getRequest.addHeader("Content-Type", "application/json");
		LOGGER.info("===============  ProducerUtil : Inside validateStore :: End  ============= ");

		String storeJson = convertToStringJson(getRequest);
		JSONObject jsonObject = new JSONObject(storeJson);
		if (jsonObject.has("status")) {
			storeStatus = jsonObject.getString("status");
			if (storeStatus.equals("active")) {
				response = "success";
			} else
				response = "Store is not active";
		} else {
			response = "Invalid Store Id";
		}
		return response;
	}

	public static String getUberOrderDetails(String getUberOrderUrl) {
		LOGGER.info("===============  ProducerUtil : Inside getUberOrderDetails :: Start  ============= ");
		String orderDetails = null;
		try {
			String access_token = getUberAccessToken(IConstant.READ_ORDER_SCOPE);
			LOGGER.info("access_token : " + access_token);
			HttpGet getRequest = new HttpGet(getUberOrderUrl);
			// HttpPost postRequest = new HttpPost("https://api.uber.com/v2/eats/stores/" +
			// IConstant.STORE_ID + "/menus");
			getRequest.addHeader("Authorization", "Bearer " + access_token);
			getRequest.addHeader("Content-Type", "application/json");
			LOGGER.info("===============  ProducerUtil : Inside getUberOrderDetails :: End  ============= ");

			LOGGER.info("getUberOrderUrl ===== " + getUberOrderUrl);
			orderDetails = convertToStringJson(getRequest);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			LOGGER.error("error: " + e.getMessage());
		}
		return orderDetails;
	}

	public static String convertToStringJson(HttpPut putRequest, String customerJson) {
		StringBuilder responseBuilder = new StringBuilder();
		HttpResponse response = null;
		try {
			LOGGER.info("responseBuilder putRequest:: " + putRequest);
			LOGGER.info("responseBuilder customerJson:: " + customerJson);
			HttpClient httpClient = HttpClientBuilder.create().build();
			StringEntity input = new StringEntity(customerJson);
			input.setContentType("application/json");
			putRequest.setEntity(input);
			LOGGER.info("convertToStringJson : " + putRequest.toString());
			response = httpClient.execute(putRequest);
			LOGGER.info("Output from Server .... \n");
			LOGGER.info(""+response);

		} catch (MalformedURLException e) {
			LOGGER.error("error: " + e.getMessage());
		} catch (IOException e) {
			LOGGER.error("error: " + e.getMessage());
		}
		LOGGER.info("responseBuilder :: " + responseBuilder);
		return response.getStatusLine().toString();
	}

	public static String AcceptDenyOrder_convertToStringJson(HttpPost postRequest, String reasonJson) {
		StringBuilder responseBuilder = new StringBuilder();
		HttpResponse response = null;
		try {
			LOGGER.info("responseBuilder postRequest:: " + postRequest);
			LOGGER.info("responseBuilder customerJson:: " + reasonJson);
			HttpClient httpClient = HttpClientBuilder.create().build();
			StringEntity input = new StringEntity(reasonJson);
			input.setContentType("application/json");
			postRequest.setEntity(input);
			LOGGER.info("convertToStringJson : " + postRequest.toString());
			response = httpClient.execute(postRequest);
			LOGGER.info("Output from Server .... \n" + response.getStatusLine());

		} catch (MalformedURLException e) {
			LOGGER.error("error: " + e.getMessage());
		} catch (IOException e) {
			LOGGER.error("error: " + e.getMessage());
		}
		LOGGER.info("responseBuilder :: " + responseBuilder);
		return response.getStatusLine().toString();
	}

	public static String acceptUberOrder(String orderId) {
		LOGGER.info("===============  ProducerUtil : Inside acceptUberOrder :: Start  ============= ");
		LOGGER.info("orderId ===== " + orderId);
		String response = null;
		try {
			String access_token = getUberAccessToken(IConstant.ACCEPT_DENY_ORDER_SCOPE);
			HttpPost postRequest = new HttpPost("https://api.uber.com/v1/eats/orders/" + orderId + "/accept_pos_order");
			postRequest.addHeader("Authorization", "Bearer " + access_token);
			postRequest.addHeader("Content-Type", "application/json");
			LOGGER.info("===============  ProducerUtil : Inside acceptUberOrder :: End  ============= ");

			String reason = "{\"reason\": \"accepted\"}";
			response = AcceptDenyOrder_convertToStringJson(postRequest, reason);
			LOGGER.info("api response == " + response);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			LOGGER.error("error: " + e.getMessage());
		}
		return response;
	}

	public static String denyUberOrder(String orderId) {
		LOGGER.info("===============  ProducerUtil : Inside denyUberOrder :: Start  ============= ");
		LOGGER.info("orderId ===== " + orderId);
		String response = null;
		try {
			String access_token = getUberAccessToken(IConstant.ACCEPT_DENY_ORDER_SCOPE);
			HttpPost postRequest = new HttpPost("https://api.uber.com/v1/eats/orders/" + orderId + "/deny_pos_order");
			postRequest.addHeader("Authorization", "Bearer " + access_token);
			postRequest.addHeader("Content-Type", "application/json");
			LOGGER.info("===============  ProducerUtil : Inside denyUberOrder :: End  ============= ");

			String reason = "{\"reason\": {\"explanation\": \"Order has been denied By Store.\"}}";
			response = AcceptDenyOrder_convertToStringJson(postRequest, reason);
			LOGGER.info("api response == " + response);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			LOGGER.error("error: " + e.getMessage());
		}
		return response;
	}

	public static String cancelUberOrder(String orderId) {
		LOGGER.info("===============  ProducerUtil : Inside cancelUberOrder :: Start  ============= ");
		LOGGER.info("orderId ===== " + orderId);
		String response = null;
		try {
			String access_token = getUberAccessToken(IConstant.CANCEL_ORDER_SCOPE);
			HttpPost postRequest = new HttpPost("https://api.uber.com/v1/eats/orders/" + orderId + "/cancel");
			postRequest.addHeader("Authorization", "Bearer " + access_token);
			postRequest.addHeader("Content-Type", "application/json");
			LOGGER.info("===============  ProducerUtil : Inside cancelUberOrder :: End  ============= ");

			String reason = "{\"reason\": {\"explanation\": \"RESTAURANT_TOO_BUSY.\"}}";
			response = AcceptDenyOrder_convertToStringJson(postRequest, reason);
			LOGGER.info("api response == " + response);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			LOGGER.error("error: " + e.getMessage());
		}
		return response;
	}

	public static String getStore(String storeId) {
		LOGGER.info("===============  ProducerUtil : Inside getStore :: Start  ============= ");

		String access_token = getUberAccessToken(IConstant.STORE_MENU_SCOPE);

		LOGGER.info(" access_token === " + access_token);
		HttpGet getRequest = new HttpGet("https://api.uber.com/v1/eats/stores/" + storeId);
		getRequest.addHeader("Authorization", "Bearer " + access_token);
		getRequest.addHeader("Content-Type", "application/json");
		LOGGER.info("===============  ProducerUtil : Inside getStore :: End  ============= ");

		String storeJson = convertToStringJson(getRequest);

		return storeJson;
	}
	    
	    public static boolean isValid(String email) {
			String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\." + "[a-zA-Z0-9_+&*-]+)*@" + "(?:[a-zA-Z0-9-]+\\.)+[a-z"
					+ "A-Z]{2,7}$";

			Pattern pat = Pattern.compile(emailRegex);
			if (email == null)
				return false;
			return pat.matcher(email).matches();
		}
	    
	    public static boolean isJSONValid(String test) {
			try {
				new JSONObject(test);
			} catch (Exception ex) {
				// e.g. in case JSONArray is valid as well...
				try {
					new JSONArray(test);
				} catch (JSONException ex1) {
					return false;
				}
			}
			return true;
		}
	public static net.authorize.Environment getAuthorizeEnv(String environment){
		
		net.authorize.Environment AuthorizeEnv = (environment != null && environment.equalsIgnoreCase("Prod"))
				? net.authorize.Environment.PRODUCTION
				: net.authorize.Environment.SANDBOX;
	    
	    return AuthorizeEnv;
		
	}
	
	public static String getAllApplicationstatus(String locationUid,Environment environment) {

		String reviewResponse = null;
		try {
		
			String url = environment.getProperty("MkonnektPlatform_BASE_DEV_URL")
					+ "entity/getAllApplicationInstallStatus?locationUId=" + locationUid;

			HttpClient client = HttpClientBuilder.create().build();
			HttpGet httpRequest = new HttpGet(url);
			
			reviewResponse = convertToStringJson(httpRequest);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return reviewResponse;
	}
	
//	public static ApplicationDto getFoodTronixAppStatus(Integer merchantId, String application,Environment environment) {
//		ApplicationDto foodTronixStatus = new ApplicationDto();
//		foodTronixStatus.setApplication(application);
//		foodTronixStatus.setAppStatus("<div><a style='color: blue;'>" + 
//		                   ProducerUtil.isFoodTronixRunning(merchantId, environment) + "</a></div>");
//
//		return foodTronixStatus;
//	}
	 
	 public static String isFoodTronixRunning(Integer merchantId,Environment environment) {
		LOGGER.info("==== ProducerUtil : Inside getFoodTronixAppStatus :: Start ===== merchantId "+ merchantId);
		HttpGet request = new HttpGet(environment.getProperty("BASE_URL") + "/foodkonnektAppStatus?merchantId=" + merchantId);
		LOGGER.info("===============  ProducerUtil : Inside getFoodTronixAppStatus :: End  ============= ");
        String response = convertToStringJson(request);
        
        if(!response.equalsIgnoreCase("true"))
        	response = "false";
        
        response = response.substring(0, 1).toUpperCase() + response.substring(1);
        
		return response;
	}
}
