package com.foodkonnekt.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.impl.client.HttpClients;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestFactory;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.UrlEncodedContent;
import com.google.api.client.util.ByteStreams;

public class CloudPrintUtil {

	private static final Logger LOGGER= LoggerFactory.getLogger(CloudPrintUtil.class);
	
	 private static final String CLOUD_PRINT_URL = "https://www.google.com/cloudprint";
	 
	 
	
//////////////////////////////////////////////////==========GET PRINTERS LIST============//////////////////////////////////////////////////////////
	  
	public static Map<String, String> getPrintersList(String refreshToken,Environment environment) {
		LOGGER.info("----------------Start :: CloudPrintUtil : getPrintersList------------------");
		
		String accessToken = GoogleOAuthToken.generateAccessToken(refreshToken,environment);
		LOGGER.info("CloudPrintUtil : getPrintersList : accessToken "+accessToken);
		
		Map<String, String> printers = new HashMap<String, String>();
		LOGGER.info("CloudPrintUtil : getPrintersList : CLOUD_PRINT_URL "+CLOUD_PRINT_URL);
		
		HttpUriRequest request = RequestBuilder.get().setUri(CLOUD_PRINT_URL + "/search")
				.setHeader("Authorization", "Bearer " + accessToken).build();
		HttpClient client = HttpClients.custom().build();
		try {
			client.execute(request);
			HttpResponse httpResponse = client.execute(request);
			BufferedReader rd = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent()));
			StringBuffer result = new StringBuffer();
			String line = "";
			while ((line = rd.readLine()) != null) {
				result.append(line);
			}
			String reviewResponse = result.toString();
			LOGGER.info("CloudPrintUtil : getPrintersList : reviewResponse "+reviewResponse);
			JSONParser parse = new JSONParser();
			try {
				JSONObject jobj = (JSONObject) parse.parse(reviewResponse);
				JSONArray printers_arr = (JSONArray) jobj.get("printers");
				for (int i = 0; i < printers_arr.size(); i++) {
					JSONObject printerObj_1 = (JSONObject) printers_arr.get(i);
					if (printerObj_1.get("id") != null) {
						String printer_id = (String) printerObj_1.get("id");
						String printer_name = (String) printerObj_1.get("displayName");
						printers.put(printer_id, printer_name);
					}
				}
				printers.remove("__google__docs");
			} catch (ParseException e) {
				if(e != null){
					LOGGER.error("error: " + e.getMessage());
					MailSendUtil.sendExceptionByMail(e,environment);
					LOGGER.error("CloudPrintUtil : getPrintersList : Exception "+e);
				}
			}
		} catch (ClientProtocolException e) {
			if(e != null){
				LOGGER.error("error: " + e.getMessage());
				MailSendUtil.sendExceptionByMail(e,environment);
				LOGGER.error("CloudPrintUtil : getPrintersList : Exception "+e);
			}
			
		} catch (IOException e) {
			if(e != null){
				LOGGER.error("error: " + e.getMessage());
				MailSendUtil.sendExceptionByMail(e,environment);
				LOGGER.error("CloudPrintUtil : getPrintersList : Exception "+e);
			}
		}
		
		LOGGER.info("----------------End :: CloudPrintUtil : getPrintersList------------------");
		return printers;
	}
	
//////////////////////////////////////////=======SUBMIT PRINT JOBS==========//////////////////////////////////////////////////////////////
	

	public static void submitPrintJobs(String file, String printerId, String refreshToken,Environment environment) {
		LOGGER.info("----------------------CloudPrintUtil :: submitPrintJobs : Start------------------------------------");
		LOGGER.info("CloudPrintUtil :: submitPrintJobs : file "+file);
		File f = new File(file);
		if(f.exists()){
			LOGGER.info("CloudPrintUtil :: submitPrintJobs : File Found");
		}
		else{
			LOGGER.info("CloudPrintUtil :: submitPrintJobs : No File Found");
			return;
		}
		
		String accessToken = GoogleOAuthToken.generateAccessToken(refreshToken,environment);
		LOGGER.info("CloudPrintUtil :: submitPrintJobs : accessToken "+accessToken);
		try {

			String output = CloudPrintHelper.encodeFileToBase64Binary(file);

			HttpTransport httpTransport = GoogleNetHttpTransport.newTrustedTransport();

			Map<String, Object> params = new LinkedHashMap<String, Object>();
			params.put("printerid", printerId);
			params.put("title", "Test Pdf");
			params.put("ticket", "{\"version\":\"1.0\", \"print\":{}}");
			params.put("content", output);
			params.put("contentTransferEncoding", "base64");
			params.put("contentType", "application/pdf");

			HttpRequestFactory requestFactory = httpTransport.createRequestFactory();
			HttpRequest httpRequest = requestFactory.buildPostRequest(
					new GenericUrl("https://www.google.com/cloudprint/submit"), new UrlEncodedContent(params));

			httpRequest.getHeaders().set("Authorization", Arrays.asList(("Bearer " + accessToken).split(",")));

			com.google.api.client.http.HttpResponse httpResponse = httpRequest.execute();
			
			try {
				LOGGER.info("CloudPrintUtil :: submitPrintJobs : httpResponse "+httpResponse.parseAsString());
			} finally {
				httpResponse.ignore();
			}

		} catch (GeneralSecurityException e) {
			LOGGER.error("CloudPrintUtil :: submitPrintJobs : Exception "+e);
			LOGGER.error("error: " + e.getMessage());
		} catch (IOException e) {
			LOGGER.error("CloudPrintUtil :: submitPrintJobs : Exception "+e);
			LOGGER.error("error: " + e.getMessage());
		}
		LOGGER.info("----------------------CloudPrintUtil :: submitPrintJobs : End------------------------------------");

	}


//////////////////////////////////////////======DOWNLOAD FILES ON THE QUEUE=======////////////////////////////////////////////////////////////		

	/**
	 * For download file from google cloud print when job arrive<br/>
	 *
	 * @param fileUrl    from job.getFileUrl() when job arrive(job notify) or other
	 * @param outputFile must is pdf file (.pdf only)
	 */

	public static void downloadFile(String fileUrl, File outputFile, String refreshToken,Environment environment) {

		String accessToken = GoogleOAuthToken.generateAccessToken(refreshToken,environment);
		OutputStream outputStream = null;
		try {
			outputStream = new FileOutputStream(outputFile);
			downloadFile(fileUrl, outputStream, accessToken);
		} catch (Exception ex) {

		} finally {
			if (outputStream != null) {
				try {
					outputStream.close();
					LOGGER.info(" downloaded successfully!");
				} catch (IOException ex) {
					LOGGER.error("CloudPrintUtil :: downloadFile : Exception "+ex);

				}
			}
		}
	}

	public static void downloadFile(String fileUrl, OutputStream outputStream, String accessToken) {

		HttpURLConnection connection = null;
		InputStream inputStream = null;

		try {
			URL url = new URL(fileUrl);
			connection = (HttpURLConnection) url.openConnection();
//connection.addRequestProperty("X-CloudPrint-Proxy", authen.getSource());
			connection.addRequestProperty("Content-Length", fileUrl.getBytes().length + "");
			connection.addRequestProperty("Authorization", "Bearer " + accessToken);
			inputStream = connection.getInputStream();

			ByteStreams.copy(inputStream, outputStream);
		} catch (Exception ex) {
			LOGGER.error("CloudPrintUtil :: downloadFile : Exception "+ex);

		} finally {
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException ex) {
					LOGGER.error("CloudPrintUtil :: downloadFile : Exception "+ex);

				}
			}

			if (connection != null) {
				connection.disconnect();
			}
		}
	}
	
	
//////////////////////////////////////////=======PRINTER INFORMATION ==========//////////////////////////////////////////////

	public static String getPrinterInformation(String printerId, String refreshToken,Environment environment) {
		LOGGER.info("----------------------CloudPrintUtil :: getPrinterInformation : Start------------------------------------");
		String accessToken = GoogleOAuthToken.generateAccessToken(refreshToken,environment);
		LOGGER.info("CloudPrintUtil :: getPrinterInformation : accessToken "+accessToken);
		String reviewResponse = "";

		String service_url = CLOUD_PRINT_URL + "/printer?printerid=" + printerId + "&printer_connection_status=true";

		HttpUriRequest request = RequestBuilder.get().setUri(service_url)
				.setHeader("Authorization", "Bearer " + accessToken).build();

		HttpClient client = HttpClients.custom().build();

		try {
			client.execute(request);
			HttpResponse httpResponse = client.execute(request);
			BufferedReader rd = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent()));
			StringBuffer result = new StringBuffer();
			String line = "";
			while ((line = rd.readLine()) != null) {
				result.append(line);
			}
			reviewResponse = result.toString();
			LOGGER.info("CloudPrintUtil :: getPrinterInformation : reviewResponse "+reviewResponse);
			
		} catch (ClientProtocolException e) {
			LOGGER.error("CloudPrintUtil :: getPrinterInformation : Exception "+e);
			LOGGER.error("error: " + e.getMessage());
		} catch (IOException e) {
			LOGGER.error("CloudPrintUtil :: getPrinterInformation : Exception "+e);
			LOGGER.error("error: " + e.getMessage());
		}
		LOGGER.info("----------------------CloudPrintUtil :: getPrinterInformation : End------------------------------------");
		return reviewResponse;

	}

//////////////////////////////////////////=======CHECK PRINTER ID VALID OR NOT ==========//////////////////////////////////////////////

	public static boolean checkPrinterId(String printerId, String refreshToken,Environment environment) {

		boolean ifValid = true;

		String response = getPrinterInformation(printerId, refreshToken,environment);

		JsonFactory factory = new JsonFactory();
		ObjectMapper mapper = new ObjectMapper(factory);
		JsonNode rootNode;

		try {
			boolean flag = false;

			rootNode = mapper.readTree(response);
			Iterator<Map.Entry<String, JsonNode>> fieldsIterator = rootNode.fields();
			while (fieldsIterator.hasNext()) {

				Map.Entry<String, JsonNode> field = fieldsIterator.next();
				if (field.getKey().equals("success")) {
					String value = field.getValue().asText();
					if (value.equals("false")) {
						flag = true;
					}
//System.out.println("Key: " + field.getKey() + "\tValue:" + value);
				}

				if (flag == true) {
					if (field.getKey().equals("errorCode")) {
						String value = field.getValue().asText();
						if (value.equals("111")) {
							ifValid = false;
						}
// System.out.println("Key: " + field.getKey() + "\tValue: " + value);
					}

				}
			}
		} catch (JsonProcessingException e) {
			LOGGER.error("error: " + e.getMessage());
		} catch (IOException e) {
			LOGGER.error("error: " + e.getMessage());
		}

		return ifValid;
	}

//////////////////////////////////////////=======CHECK IF PRINTER ONLINE  ==========//////////////////////////////////////////////

	public static boolean checkIfPrinterOnline(String printerId, String refreshToken,Environment environment) {
		LOGGER.info("----------------------CloudPrintUtil :: checkIfPrinterOnline : Start----------------------- printerId : "+printerId+" refreshToken : "+refreshToken);
		String parseStatus = "";
		boolean status = false;
		String reviewResponse = getPrinterInformation(printerId, refreshToken,environment);

		JSONParser parse = new JSONParser();
		JSONObject jobj;
		try {
			jobj = (JSONObject) parse.parse(reviewResponse);
			JSONArray printers_arr = (JSONArray) jobj.get("printers");
			if(printers_arr != null){
				for (int i = 0; i < printers_arr.size(); i++) {
					JSONObject printerObj_1 = (JSONObject) printers_arr.get(i);
					parseStatus = (String) printerObj_1.get("connectionStatus");
					LOGGER.info("CloudPrintUtil :: checkIfPrinterOnline : connectionStatus "+parseStatus);
					if (parseStatus.equals("ONLINE")) {
						status = true;
					}
				}
			}
		} catch (ParseException e) {
			LOGGER.error("CloudPrintUtil : checkIfPrinterOnline : Exception "+e);
			LOGGER.error("error: " + e.getMessage());
		}
		LOGGER.info("CloudPrintUtil :: checkIfPrinterOnline : status "+status);
		LOGGER.info("----------------------CloudPrintUtil :: checkIfPrinterOnline : End------------------------------------");
		return status;

	}

///////////////////////////////////////////========GET JOB STATUS======///////////////////////////////////////////////////

	public static void getJobStatus(String printerId, String jobId, String refreshToken,Environment environment) {
		
		String accessToken = GoogleOAuthToken.generateAccessToken(refreshToken,environment);

		String reviewResponse = "";

		String service_url = CLOUD_PRINT_URL + "/job?output=json&printerid=" + printerId + "&jobid=" + jobId;

		HttpUriRequest request = RequestBuilder.get().setUri(service_url)
				.setHeader("Authorization", "Bearer " + accessToken).build();

		HttpClient client = HttpClients.custom().build();

		try {

			client.execute(request);
			HttpResponse httpResponse = client.execute(request);
			BufferedReader rd = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent()));
			StringBuffer result = new StringBuffer();
			String line = "";
			while ((line = rd.readLine()) != null) {
				result.append(line);
			}
			reviewResponse = result.toString();
         //System.out.println(reviewResponse);

			JSONParser parse = new JSONParser();

			JSONObject jobj;
			try {
				jobj = (JSONObject) parse.parse(reviewResponse);
				JSONObject printers_arr = (JSONObject) jobj.get("job");

				String parseStatus = (String) printers_arr.get("status");
				System.out.println("Job Id : " + jobId);
				System.out.println("Status : " + parseStatus);

			} catch (ParseException e) {
				LOGGER.error("CloudPrintUtil : getJobStatus : Exception "+e);

				LOGGER.error("error: " + e.getMessage());
			}

		} catch (ClientProtocolException e) {
			LOGGER.error("CloudPrintUtil : getJobStatus : Exception "+e);

			LOGGER.error("error: " + e.getMessage());
		} catch (IOException e) {
			LOGGER.error("CloudPrintUtil : getJobStatus : Exception "+e);

			LOGGER.error("error: " + e.getMessage());
		}

	}
	
	
///////////////////////////////////////////========FETCH JOBS ON THE QUEUE======///////////////////////////////////////////////////
    
	public static List<String> fetchPrintJobs(String printerId, String refreshToken,Environment environment) {

		String accessToken = GoogleOAuthToken.generateAccessToken(refreshToken,environment);

		List<String> fileUrls = new ArrayList<String>();
		String fileUrl1 = "";

        //this url will get the list of jobs on the queue
		String API_SERVICE_URL = CLOUD_PRINT_URL + "/fetch?output=json&printerid=" + printerId;

		HttpUriRequest request = RequestBuilder.get().setUri(API_SERVICE_URL)
				.setHeader("Authorization", "Bearer " + accessToken).build();

		HttpClient client = HttpClients.custom().build();

		try {
			client.execute(request);
			HttpResponse httpResponse = client.execute(request);
			BufferedReader rd = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent()));
			StringBuffer result = new StringBuffer();
			String line = "";
			while ((line = rd.readLine()) != null) {
				result.append(line);
			}
			String reviewResponse = result.toString();

           // System.out.println("Printers --> " + reviewResponse);
			JSONParser parse = new JSONParser();
			try {
				JSONObject jobj = (JSONObject) parse.parse(reviewResponse);

				JSONArray printers_arr = (JSONArray) jobj.get("jobs");
				if (printers_arr == null) {
					return fileUrls;
				}

				else {
					JSONObject printerObj_2 = (JSONObject) printers_arr.get(0);

					fileUrl1 = (String) printerObj_2.get("fileUrl");
					System.out.println(" file url 0 : " + fileUrl1);

					for (int i = 0; i < printers_arr.size(); i++) {
						JSONObject printerObj_1 = (JSONObject) printers_arr.get(i);
						String fileUrl = (String) printerObj_1.get("fileUrl");
						fileUrls.add(fileUrl);
            // System.out.println(" file url : " + fileUrl);
					}

				}
			} catch (ParseException e) {
				LOGGER.error("CloudPrintUtil : fetchPrintJobs : Exception "+e);

				LOGGER.error("error: " + e.getMessage());
			}

		} catch (ClientProtocolException e) {
			LOGGER.error("CloudPrintUtil : fetchPrintJobs : Exception "+e);

			LOGGER.error("error: " + e.getMessage());
		} catch (IOException e) {
			LOGGER.error("CloudPrintUtil : fetchPrintJobs : Exception "+e);

			LOGGER.error("error: " + e.getMessage());
		}

		return fileUrls;
	}

	public static String printORderReceipt(Integer customerId, Integer orderId){
		return null ;
	}
	
	
}
