

	package com.foodkonnekt.util;

	import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.itextpdf.io.font.FontConstants;
	import com.itextpdf.io.image.ImageData;
import com.itextpdf.io.image.ImageDataFactory;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Image;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.element.Text;
import com.itextpdf.layout.property.TextAlignment;
import com.itextpdf.layout.property.UnitValue;
	
	

public class Pdf {

	    /** The pattern of the destination paths. */
	  //  public static final String DEST = "results/zugferd/pdf/basic%05d.pdf";

	    /** The path to the color profile. */

	    /** The path to a regular font. */
	    public static final String REGULAR = "src/main/resources/fonts/OpenSans-Regular.ttf";

	    /** The path to a bold font. */
	    public static final String BOLD = "src/main/resources/fonts/OpenSans-Bold.ttf";

	    /** A <code>String</code> with a newline character. */
	    public static final String NEWLINE = "\n";

	    public static void main(String[] args) throws Exception { // Creating a PdfWriter
	        PdfFont bold = PdfFontFactory.createFont(FontConstants.HELVETICA_BOLDOBLIQUE);       
	        String dest = "C:/itextExamples/addingParagraph.pdf";
	        PdfWriter writer = new PdfWriter(dest);
	        PdfDocument pdf = new PdfDocument(writer);
	        Document doc = new Document(pdf);
	        
	            
//	        String imFile = "resources/images/dog.bmp";       
//	        ImageData data = ImageDataFactory.create(imFile);             
//	        Image image = new Image(data);                
//	        image.setFixedPosition(35, 700);                  
	  //      doc.setFont(PdfFontFactory.createFont(REGULAR, true))    .setFontSize(12);
	        
	        
	        doc.setFont(PdfFontFactory.createFont(FontConstants. COURIER_OBLIQUE))    .setFontSize(12);

	        Date invoiceDate =  new Date();

	        
	        
	        doc.add( new Paragraph() .setTextAlignment(TextAlignment.RIGHT) .setMultipliedLeading(1) .add(new Text(String.format("%s %s\n","Order Id "," 1001 ")).setFont(bold).setFontSize(14))
	                        /*.add(convertDate( invoiceDate, "MMM dd, yyyy")*/);
	       

	        
	        
	        Table table1 = new Table(new UnitValue[] { new UnitValue(2, 50), new UnitValue(2, 50) }).setWidth(UnitValue.createPercentValue(100));
	        table1.addCell(getPartyAddress("Order Type: Pickup ", "PaymentT Status :Cash ", "", "", "", "",     " ", bold));
	        table1.addCell(
	        		getPartyAddress("Customer Information  \n " + "Name : Arpit ", "Phone No.: 898998999 ", "Delivery Address : Ground Fianae, TX " , " Pickup Time:  2018-10-26 17:01:54", "", "",     "", bold).setMarginLeft(13));
	        doc.add(table1);
	      
	        
	        
	        
	        doc.add(getLineItemTable( bold));
	        
	        

	        Table table0 = new Table(new UnitValue[] { new UnitValue(2, 50), new UnitValue(2, 50) }).setWidth(UnitValue.createPercentValue(100));
	        table0.addCell(getPartyAddress("  ", "", "", "  ", "", "",     " \n   ", bold));
	        doc.add(table0);
	        
	        
	        String para = "Addtional Tip: 10";  
	        Paragraph paragraph = new Paragraph(para).setMarginLeft(380);
	        doc.add(paragraph);
	        
	        
	        String para3 = "Customer Singnature: .................";  
	        Paragraph paragraph3 = new Paragraph(para3).setMarginLeft(340);
	        doc.add(paragraph3);
	        
	        doc.add(getPaymentInfo());
	        
	        String para1 = "    www.foodkonnekt.com";  
	                        Paragraph paragraph2 = new Paragraph(para1).setMarginLeft(180);
	                        doc.add(paragraph2);

	        doc.close();
	        System.out.println("Table created successfully..");
	    }


	    
	    
	    public static Table getLineItemTable(PdfFont bold) {
	        Table table = new Table(new UnitValue[]{
	                new UnitValue(UnitValue.PERCENT, 6.25f),
	                new UnitValue(UnitValue.PERCENT, 20.5f),
	                new UnitValue(UnitValue.PERCENT, 20.5f),
	                new UnitValue(UnitValue.PERCENT, 12.5f)})
	                .setWidth(UnitValue.createPercentValue(100))
	                .setMarginTop(10).setMarginBottom(10);
	        table.addHeaderCell(createCell("Qty", bold).setTextAlignment(TextAlignment.CENTER));
	        table.addHeaderCell(createCell("Item", bold).setTextAlignment(TextAlignment.CENTER));
	        table.addHeaderCell(createCell("Options", bold).setTextAlignment(TextAlignment.CENTER));
	        table.addHeaderCell(createCell("Price", bold).setTextAlignment(TextAlignment.CENTER));

	            table.addCell(createCell("5").setTextAlignment(TextAlignment.CENTER));
	            table.addCell(createCell( String.valueOf("Grey Goose")).setTextAlignment(TextAlignment.CENTER));
	            table.addCell(createCell(("add chees $2 "+NEWLINE+  "add chees $2" +NEWLINE+"add chees $2 ")) .setTextAlignment(TextAlignment.CENTER));
	            table.addCell(createCell(String.valueOf("$"+25))  .setTextAlignment(TextAlignment.CENTER));
	            
	            table.addCell(createCell("4").setTextAlignment(TextAlignment.CENTER));
	            table.addCell(createCell( String.valueOf("Ciroc")).setTextAlignment(TextAlignment.CENTER));
	            table.addCell(createCell(String.valueOf("-")) .setTextAlignment(TextAlignment.CENTER));
	            table.addCell(createCell(String.valueOf("$"+28))  .setTextAlignment(TextAlignment.CENTER));
	            
	            table.addCell(createCell("3").setTextAlignment(TextAlignment.CENTER));
	            table.addCell(createCell( String.valueOf("pizza")).setTextAlignment(TextAlignment.CENTER));
	            table.addCell(createCell(String.valueOf("-")) .setTextAlignment(TextAlignment.CENTER));
	            table.addCell(createCell(String.valueOf(20))  .setTextAlignment(TextAlignment.CENTER));
	            
	            
	            table.addCell(getPartyAddress2( "" , "", "", "", "", "",   ""  , bold));
	            table.addCell(getPartyAddress2( "" , "", "", "", "", "",   ""  , bold));
	            table.addCell(createCell("Subtotal", bold).setTextAlignment(TextAlignment.RIGHT));
	            table.addCell(createCell(String.valueOf("$"+25))  .setTextAlignment(TextAlignment.CENTER));
	            
	            table.addCell(getPartyAddress2( "" , "", "", "", "", "",   ""  , bold));
	            table.addCell(getPartyAddress2( "" , "", "", "", "", "",   ""  , bold));
	            table.addCell(createCell(String.valueOf("Delivery Fee"),bold) .setTextAlignment(TextAlignment.RIGHT));
	            table.addCell(createCell(String.valueOf("$"+25))  .setTextAlignment(TextAlignment.CENTER));
	            
	            table.addCell(getPartyAddress2( "" , "", "", "", "", "",   ""  , bold));
	            table.addCell(getPartyAddress2( "" , "", "", "", "", "",   ""  , bold));
	            table.addCell(createCell("Tax", bold).setTextAlignment(TextAlignment.RIGHT));
	            table.addCell(createCell(String.valueOf("$"+25))  .setTextAlignment(TextAlignment.CENTER));
	            
	            table.addCell(getPartyAddress2( "" , "", "", "", "", "",   ""  , bold));
	            table.addCell(getPartyAddress2( "" , "", "", "", "", "",   ""  , bold));
	            table.addCell(createCell("Tip", bold).setTextAlignment(TextAlignment.RIGHT));
	            table.addCell(createCell(String.valueOf("$"+25))  .setTextAlignment(TextAlignment.CENTER));
	            
	            table.addCell(getPartyAddress2( "" , "", "", "", "", "",   ""  , bold));
	            table.addCell(getPartyAddress2( "" , "", "", "", "", "",   ""  , bold));
	            table.addCell(createCell(String.valueOf("Total"),bold) .setTextAlignment(TextAlignment.RIGHT));
	            table.addCell(createCell(String.valueOf("$"+25),bold)  .setTextAlignment(TextAlignment.CENTER));
	            
	        
	        return table;
	    }
	    
	    public static Table getToatItemTable(PdfFont bold) {
	        Table table = new Table(new UnitValue[]{
	                new UnitValue(UnitValue.PERCENT, 6.25f),
	                new UnitValue(UnitValue.PERCENT, 20.5f),
	                new UnitValue(UnitValue.PERCENT, 20.5f),
	                new UnitValue(UnitValue.PERCENT, 12.5f)})
	                .setWidth(UnitValue.createPercentValue(100))
	                .setMarginTop(10).setMarginBottom(10);
	        table.addHeaderCell(createCell("Subtotal", bold).setTextAlignment(TextAlignment.CENTER));
	        table.addHeaderCell(createCell("Delivery Fee", bold).setTextAlignment(TextAlignment.CENTER));
	        table.addHeaderCell(createCell("Tax", bold).setTextAlignment(TextAlignment.CENTER));
	        table.addHeaderCell(createCell("Total", bold).setTextAlignment(TextAlignment.CENTER));

	            table.addCell(createCell("$5").setTextAlignment(TextAlignment.CENTER));
	            table.addCell(createCell( String.valueOf("$20")).setTextAlignment(TextAlignment.CENTER));
	            table.addCell(createCell(String.valueOf("$50")) .setTextAlignment(TextAlignment.CENTER));
	            table.addCell(createCell(String.valueOf("$"+25))  .setTextAlignment(TextAlignment.CENTER));
	            
	        return table;
	    }
	    
	    public static Cell createCell(String text, PdfFont font) {
	        return new Cell().setPadding(0.8f)
	            .add(new Paragraph(text) .setFont(font).setMultipliedLeading(1));
	    }

	    
	    public static Cell createCell(String text) {
	        return new Cell().setPadding(0.8f)
	            .add(new Paragraph(text)
	                .setMultipliedLeading(1));
	    }
	    
	    
	    public static Cell getPartyTax(String[] taxId, String[] taxSchema, PdfFont bold) {
	        Paragraph p = new Paragraph().setFontSize(10).setMultipliedLeading(1.0f)
	                        .add(new Text("Tax ID(s):").setFont(bold));
	        if (taxId.length == 0) {
	            p.add("\nNot applicable");
	        } else {
	            int n = taxId.length;
	            for (int i = 0; i < n; i++) {
	                p.add("\n").add(String.format("%s: %s", taxSchema[i], taxId[i]));
	            }
	        }
	        return new Cell().setBorder(null).add(p);
	    }

	    public static Cell getPartyAddress(String who, String name, String line1, String line2, String countryID, String postcode, String city, PdfFont bold) {
	        Paragraph p = new Paragraph().setMultipliedLeading(1.0f).add(new Text(who).setFont(bold)).add("\n").add(name)
	                        .add("\n").add(line1).add("\n").add(line2).add("\n")
	                        .add(String.format("%s%s %s", countryID, postcode, city));
	        Cell cell = new Cell().setBorder(null).add(p);
	        
	        
	        return cell;
	    }
	    
	    public static Cell getPartyAddress2(String who, String name, String line1, String line2, String countryID, String postcode, String city, PdfFont bold) {
	        Paragraph p = new Paragraph().setMultipliedLeading(1.0f).add(new Text(who).setFont(bold)).add("\n").add(name)
	                        .add("\n").add(line1).add("\n").add(line2).add("\n")
	                        .add(String.format("%s-%s %s", countryID, postcode, city));
	        Cell cell =  new Cell().setPadding(0.8f)
	        .add(new Paragraph(who)
	            .setMultipliedLeading(1)).setBorder(null);
	        return cell;
	    }
	    
	    public static String convertDate(Date d, String newFormat) throws ParseException {
	        SimpleDateFormat sdf = new SimpleDateFormat(newFormat);
	        return sdf.format(d);
	    }
	    
	    
	    public static Cell getPartyAddress1(String who, String name, String line1, String line2, String countryID, String postcode, String city, PdfFont bold) {
	        Paragraph p = new Paragraph().setMultipliedLeading(1.0f).add(new Text(who).setFont(bold)).add("\n").add(name)  .add("\n").add(line1).add("\n").add(line2).add("\n")
	                        .add(String.format("%s-%s %s", countryID, postcode, city));
	        Cell cell = new Cell().setBorder(null).add(p);
	        cell.setBold();
	        return cell;
	    }
	    
	    public static Paragraph getPaymentInfo() {
	        String ref ="";
	        Paragraph p = new Paragraph(String.format(".......................................................................End of order.........................................................\n%s",  ref));
	        return p;
	    }
	    
	}