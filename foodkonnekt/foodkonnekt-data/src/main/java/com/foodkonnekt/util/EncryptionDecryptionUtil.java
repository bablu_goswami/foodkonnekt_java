package com.foodkonnekt.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EncryptionDecryptionUtil {

	private static final Logger LOGGER= LoggerFactory.getLogger(EncryptionDecryptionUtil.class);

    /**
     * Encrypt original string
     * 
     * @param original
     * @return Encrypt string
     */
    public static String encryption(String original) {
        byte[] encoded = Base64.encodeBase64(original.getBytes());
        return new String(encoded);
    }

    /**
     * Decrypt string to original
     * 
     * @param decrypt
     * @return original string
     */
    public static String decryption(String decrypt) {
        byte[] decoded = Base64.decodeBase64(decrypt);
        return new String(decoded);
    }
    
    public static String encryptString(String variable){
    	MessageDigest md;
    	StringBuffer sb = new StringBuffer();
		try {
			md = MessageDigest.getInstance("SHA-256");
			md.update(variable.getBytes());

	        byte byteData[] = md.digest();

	        //convert the byte to hex format method 1
	        
	        for (int i = 0; i < byteData.length; i++) {
	         sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
	        }
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			LOGGER.error("error: " + e.getMessage());
		}
        
         return sb.toString();
        
    }
    
    public static boolean checkForEncode(String string) {
	    String pattern = "^([A-Za-z0-9+/]{4})*([A-Za-z0-9+/]{4}|[A-Za-z0-9+/]{3}=|[A-Za-z0-9+/]{2}==)$";
	    Pattern r = Pattern.compile(pattern);
	    Matcher m = r.matcher(string);
	    if (m.find()) {
	    	LOGGER.info("valid for encryption/decription ");
	        return true;
	    } else {
	        LOGGER.info("invalid");
	        return false;
	    }
	}
   /* public static void main(String[] args)throws Exception
    {
    	String password = "12";
    	
    	System.out.println("Hex format : " + encryptString(password));

    }*/
}
