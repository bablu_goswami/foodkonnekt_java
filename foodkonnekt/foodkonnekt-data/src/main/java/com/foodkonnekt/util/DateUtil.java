package com.foodkonnekt.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TimeZone;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;

import com.foodkonnekt.model.Address;
import com.foodkonnekt.model.OrderR;

public class DateUtil {
	
	private static final Logger LOGGER= LoggerFactory.getLogger(DateUtil.class);


    /**
     * Convert string to date
     * 
     * @param stringDate
     * @return Date
     */
	
	
	 public static void main(String[] args) {
	    	try {
	    		
	    		getCurrentDateForTimeZonee("America/Chicago");
	    		Address address=new Address();
	    		address.setAddress1("1009 macarthur blvd");
	    		address.setAddress2("#21");
	    		address.setCity("Irving");
	    		address.setCountry("United States");
	    		address.setState("TX");
	    		address.setZip("75063");
	    		//LOGGER.info("TimeZone Code :: "+getTimeZone(address));
	    		//checkCurrentTimeIsFiveMinutGreterthan("02:23","","America/Chicago");
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
	    	
	    	//checkCurrentAndFullFillTime("2019-08-16 17:34:45","America/Chicago");
	        LOGGER.info("find10FutureDates=== "+find10FutureDates());

	        List<java.sql.Time> intervals = new ArrayList<java.sql.Time>(25);
	        java.sql.Time startTime = new java.sql.Time(00, 30, 0);
	        java.sql.Time endTime = new java.sql.Time(20, 0, 0);

	        intervals.add(startTime);

	        Calendar cal = Calendar.getInstance();
	        cal.setTime(startTime);
	        while (cal.getTime().before(endTime)) {
	            cal.add(Calendar.MINUTE, 30);
	            intervals.add(new java.sql.Time(cal.getTimeInMillis()));
	        }

	        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
	        for (java.sql.Time time : intervals) {
	            LOGGER.info(sdf.format(time));
	        }
	        
	        DateTimeFormatter formatter = DateTimeFormat.forPattern("dd-MM-yyyy HH:mm:ss a");
	        DateTime dt = formatter.parseDateTime("11-2-2017 01:10:00 AM");
	        
	        Date dateInUS = dt.toDate();
	        LOGGER.info("dateinus === "+dateInUS);

	        final long ONE_MINUTE_IN_MILLIS = 60000;// millisecs

	        Calendar date = Calendar.getInstance();
	        long t = date.getTimeInMillis();
	        Date afterAddingTenMins = new Date(t + (30 * ONE_MINUTE_IN_MILLIS));
	        LOGGER.info(date.getTime() + "---" + afterAddingTenMins);

	        SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm:ss");
	        SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm a");
	        Date date2;
	        try {
	            date2 = parseFormat.parse("10:30 PM");
	            String futureTime = displayFormat.format(date2);
	            LOGGER.info(parseFormat.format(date2) + " = " + futureTime.split(":")[2].replace("00", "10"));
	        } catch (ParseException e) {
	            LOGGER.error("error: " + e.getMessage());
	        }
	    }
	 
	public static String findCurrentDateWithYYYYMMDD() {
        Date myDate = new Date();
        return new SimpleDateFormat("yyyy-MM-dd").format(myDate);
    }
	
    public static String convert12hoursTo24HourseFormate(String time) {
        SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm");
        SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm a");
        Date date = null;
        try {
            if (time != null)
                date = parseFormat.parse(time);
        } catch (ParseException e) {
            LOGGER.error("error: " + e.getMessage());
        }
        if (date != null)
            return displayFormat.format(date);
        else {
            return null;
        }
    }

    public static Date convertStringToDate(String stringDate) {
        Date date = null;
        if (stringDate != null) {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            try {
                if (stringDate != null && !stringDate.isEmpty())
                    date = formatter.parse(stringDate);
            } catch (ParseException e) {
                LOGGER.error("error: " + e.getMessage());
            }
        }
        return date;
    }

    /**
     * Find current date
     * 
     * @return Date
     */
    public static Date currentDate() {
    	LOGGER.info("===============  DateUtil : Inside currentDate :: Start  ============= ");

        Calendar now = Calendar.getInstance();
        String start_dt = (now.get(Calendar.YEAR)) + "-" + (now.get(Calendar.MONTH) + 1) + "-"
                        + (now.get(Calendar.DATE));
    	LOGGER.info("===============  DateUtil : Inside currentDate :: End  ============= ");

        return convertStringToDate(start_dt);
    }

    /**
     * Find date after 30days
     * 
     * @return Date
     */
    public static Date incrementedDate() {
        Calendar now = Calendar.getInstance();
        now.add(Calendar.DATE, 30);
        String incrementedDate = (now.get(Calendar.YEAR)) + "-" + (now.get(Calendar.MONTH) + 1) + "-"
                        + (now.get(Calendar.DATE));
        return convertStringToDate(incrementedDate);
    }

    public static long findDifferenceBetweenTwoDates(Date currentDate, Date endDate) {
    	
        long diff = currentDate.getTime() - endDate.getTime();
        long diffSeconds = diff / 1000 % 60;
        long diffMinutes = diff / (60 * 1000) % 60;
        long diffHours = diff / (60 * 60 * 1000) % 24;
        long diffDays = diff / (24 * 60 * 60 * 1000);

        LOGGER.info(diffDays + " days, ");
        LOGGER.info(diffHours + " hours, ");
        LOGGER.info(diffMinutes + " minutes, ");
        LOGGER.info(diffSeconds + " seconds.");
        
    	LOGGER.info("===============  DateUtil : Inside findDifferenceBetweenTwoDates :: End  ============= ");

        return diffDays;
    }

    /**
     * Find current time
     * 
     * @return String
     */
    public static String findCurrentTime() {
    	LOGGER.info("===============  DateUtil : Inside findCurrentTime :: Start  ============= ");

        Date date = new Date();
        DateFormat df = new SimpleDateFormat("HH:mm");
        
        String currentTime = df.format(date);
        df.setTimeZone(TimeZone.getTimeZone("UTC"));
        
    	LOGGER.info("===============  DateUtil : Inside findCurrentTime :: End  ============= ");

        return currentTime;
    }
    
    

    /**
     * Find current day
     * 
     * @return String
     */
    
    public static String getCurrentDayForTimeZone(String timeZoneCode){
    	LOGGER.info("===============  DateUtil : Inside getCurrentDayForTimeZone :: Start  ============= timeZoneCode "+timeZoneCode);

    	Date date = new Date();
    	DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

    	// Use Madrid's time zone to format the date in
    	df.setTimeZone(TimeZone.getTimeZone(timeZoneCode));

    	String toDay=DateUtil.findDayNameFromDate( df.format(date));
    	LOGGER.info("===============  DateUtil : Inside getCurrentDayForTimeZone :: End  ============= ");

    	return toDay;
    }
    
    public static String getCurrentDay(){
    	Date date = new Date();
    	DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

    	// Use Madrid's time zone to format the date in
    	//df.setTimeZone(TimeZone.getTimeZone(timeZoneCode));

    	String toDay=DateUtil.findDayNameFromDate( df.format(date));
    	return toDay;
    }
    public static String getCurrentTimeForTimeZone(String timeZoneCode){
    	LOGGER.info("===============  DateUtil : Inside getCurrentTimeForTimeZone :: Start  =============timeZoneCode "+timeZoneCode);

    SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
    
    sdf.setTimeZone(TimeZone.getTimeZone(timeZoneCode));
    String currentTime = sdf.format(new Date());
    return currentTime;
    }
    public String findTodayDayName() {
        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_WEEK);
        String currentDay = null;
        switch (day) {
        case Calendar.SUNDAY:
            currentDay = "sunday";
            break;
        case Calendar.MONDAY:
            currentDay = "monday";
            break;
        case Calendar.TUESDAY:
            currentDay = "tuesday";
            break;
        case Calendar.WEDNESDAY:
            currentDay = "wednesday";
            break;
        case Calendar.THURSDAY:
            currentDay = "thursday";
            break;
        case Calendar.FRIDAY:
            currentDay = "friday";
            break;
        case Calendar.SATURDAY:
            currentDay = "saturday";
            break;
        }
    	LOGGER.info("===============  DateUtil : Inside getCurrentTimeForTimeZone :: End  =============");

        return currentDay;
    }

    public boolean checkCurrentTimeExistBetweenTwoTimeOrNot(String startTime, String endTime) {
    	LOGGER.info("===============  DateUtil : Inside checkCurrentTimeExistBetweenTwoTimeOrNot :: Start  =============startTime "+startTime+" endTime : "+endTime);

        boolean status = true;
        try {
            String string1 = startTime;
            Date time1 = new SimpleDateFormat("HH:mm").parse(string1);
            Calendar calendar1 = Calendar.getInstance();
            calendar1.setTime(time1);

            String string2 = endTime;
            Date time2 = new SimpleDateFormat("HH:mm").parse(string2);
            Calendar calendar2 = Calendar.getInstance();
            calendar2.setTime(time2);
            calendar2.add(Calendar.DATE, 1);

            String someRandomTime = findCurrentTime();
            Date d = new SimpleDateFormat("HH:mm").parse(someRandomTime);
            Calendar calendar3 = Calendar.getInstance();
            calendar3.setTime(d);
            calendar3.add(Calendar.DATE, 1);

            Date x = calendar3.getTime();
            if (x.after(calendar1.getTime()) && x.before(calendar2.getTime())) {
                // checkes whether the current time is between 14:49:00 and 20:11:13.
                status = false;
            }
        } catch (ParseException e) {
        	LOGGER.info("===============  DateUtil : Inside checkCurrentTimeExistBetweenTwoTimeOrNot :: Exception  ============="+e);

            LOGGER.error("error: " + e.getMessage());
        }
    	LOGGER.info("===============  DateUtil : Inside checkCurrentTimeExistBetweenTwoTimeOrNot :: End  =============");

        return status;
    }

    public static String findCurrentDate() {
        Date myDate = new Date();
        return new SimpleDateFormat("MM-dd-yyyy").format(myDate);
    }
    
    public static String findCurrentDateWithTime() {
        Date myDate = new Date();
        return new SimpleDateFormat("MM-dd-yyyy hh:mm:ss").format(myDate);
    }
    
    
    
    
    public static String getYYYYMMDDHHMMSS(Date date) {

        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);
    }

    public static String getYYYYMMDD(Date date) {

        return new SimpleDateFormat("yyyy-MM-dd").format(date);
    }
    
    public static String getMMDDYYYY(Date date) {

        return new SimpleDateFormat("MM-dd-yyyy").format(date);
    }

    public static String getDDMMYYYY(Date date) {

        return new SimpleDateFormat("dd-MM-yyyy").format(date);
    }

    /**
     * All time list
     * 
     * @return List<String>
     */
    
    public static List<String> findAllWeekDays(){
    	 List<String> weekDays = new ArrayList<String>();
    	 weekDays.add("Sunday");
    	 weekDays.add("Monday");
    	 weekDays.add("Tuesday");
    	 weekDays.add("Wednesday");
    	 weekDays.add("Thursday");
    	 weekDays.add("Friday");
    	 weekDays.add("Saturday");
    	 return weekDays;
    }
    public static List<String> findAllTime() {
        List<String> times = new ArrayList<String>();
        times.add("12:00 AM");
        times.add("12:30 AM");
        times.add("01:00 AM");
        times.add("01:30 AM");
        times.add("02:00 AM");
        times.add("02:30 AM");
        times.add("03:00 AM");
        times.add("03:30 AM");
        times.add("04:00 AM");
        times.add("04:30 AM");
        times.add("05:00 AM");
        times.add("05:30 AM");
        times.add("06:00 AM");
        times.add("06:30 AM");
        times.add("07:00 AM");
        times.add("07:30 AM");
        times.add("08:00 AM");
        times.add("08:30 AM");
        times.add("09:00 AM");
        times.add("09:30 AM");
        times.add("10:00 AM");
        times.add("10:30 AM");
        times.add("11:00 AM");
        times.add("11:30 AM");
        times.add("12:00 PM");
        times.add("12:30 PM");
        times.add("01:00 PM");
        times.add("01:30 PM");
        times.add("02:00 PM");
        times.add("02:30 PM");
        times.add("03:00 PM");
        times.add("03:30 PM");
        times.add("04:00 PM");
        times.add("04:30 PM");
        times.add("05:00 PM");
        times.add("05:30 PM");
        times.add("06:00 PM");
        times.add("06:30 PM");
        times.add("07:00 PM");
        times.add("07:30 PM");
        times.add("08:00 PM");
        times.add("08:30 PM");
        times.add("09:00 PM");
        times.add("09:30 PM");
        times.add("10:00 PM");
        times.add("10:30 PM");
        times.add("11:00 PM");
        times.add("11:30 PM");
        times.add("12:00 AM");
        return times;
    }
    
    public static List<String> findAll24Time() {
        List<String> times = new ArrayList<String>();
        times.add("00:00");
        times.add("12:30");
        times.add("01:00");
        times.add("01:30");
        times.add("02:00");
        times.add("02:30");
        times.add("03:00");
        times.add("03:30");
        times.add("04:00");
        times.add("04:30");
        times.add("05:00");
        times.add("05:30");
        times.add("06:00");
        times.add("06:30");
        times.add("07:00");
        times.add("07:30");
        times.add("08:00");
        times.add("08:30");
        times.add("09:00");
        times.add("09:30");
        times.add("10:00");
        times.add("10:30");
        times.add("11:00");
        times.add("11:30");
        times.add("12:00");
        times.add("12:30");
        times.add("13:00");
        times.add("13:30");
        times.add("14:00");
        times.add("14:30");
        times.add("15:00");
        times.add("15:30");
        times.add("16:00");
        times.add("16:30");
        times.add("17:00");
        times.add("17:30");
        times.add("18:00");
        times.add("18:30");
        times.add("19:00");
        times.add("19:30");
        times.add("20:00");
        times.add("20:30");
        times.add("21:00");
        times.add("21:30");
        times.add("22:00");
        times.add("22:30");
        times.add("23:00");
        times.add("23:30");
        times.add("24:00");
        return times;
    }

    public static String getTimeFormat(String time) {

        return null;
    }

    public static String getTwentyFourFormat(String time) {
        SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm");
        SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm a");
        String finalTime = null;
        Date date;
        try {
            date = parseFormat.parse(time);
            finalTime = displayFormat.format(date);
        } catch (ParseException e) {
            LOGGER.error("error: " + e.getMessage());
        }
        return finalTime;
    }

    public static String get12Format(String time) {
        SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm");
        SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm a");
        String finalTime = null;
        Date date;
        try {
            date = displayFormat.parse(time);
            finalTime = parseFormat.format(date);
        } catch (ParseException e) {
            LOGGER.error("error: " + e.getMessage());
        }
        return finalTime;
    }

    /**
     * Find future 10 days dates
     * 
     * @return List<String>
     */
    public static List<String> find10FutureDates() {
        List<String> futureDates = new ArrayList<String>();
        // get a calendar instance, which defaults to "now"
        Calendar calendar = Calendar.getInstance();
        // get a date to represent "today"
        Date today = calendar.getTime();
        LOGGER.info("DateUtil : inside find10FutureDates starts === today:    " + today);

        futureDates.add((calendar.get(Calendar.DATE)) + "-" + (calendar.get(Calendar.MONTH) + 1) + "-"
                        + (calendar.get(Calendar.YEAR)));
        for (int i = 1; i <= 9; i++) {
            // add one day to the date/calendar
            calendar.add(Calendar.DAY_OF_YEAR, 1);
            // now get "tomorrow"
            // Date tomorrow = calendar.getTime();
            // LOGGER.info("tomorrow: " + tomorrow);
            String start_dt = (calendar.get(Calendar.DATE)) + "-" + (calendar.get(Calendar.MONTH) + 1) + "-"
                            + (calendar.get(Calendar.YEAR));
            futureDates.add(start_dt);
        }
        return futureDates;
    }

   

    /**
     * Find Day from date
     * 
     * @param futureDate
     * @return String
     */
    public static String findDayNameFromDate(String futureDate) {
        LOGGER.info("DateUtil : In findDayNameFromDate starts : futureDate : "+futureDate);

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");//new SimpleDateFormat("dd-MM-yyyy");
        Date date = null;
        try {
            date = format.parse(futureDate);
           // LOGGER.info("date util date- "+date);
        } catch (ParseException e) {
            LOGGER.info("DateUtil : In findDayNameFromDate : Exception : "+e);

            LOGGER.error("error: " + e.getMessage());
        }
        LOGGER.info("DateUtil : In findDayNameFromDate End ====");
        return (new SimpleDateFormat("EEEE").format(date)).toLowerCase();
    }

    public static Date futureDateAndTime(String futureDate, String futureTime) {
        LOGGER.info("DateUtil : In futureDateAndTime : starts futureDate : "+futureDate+" futureTime : "+futureTime);

        SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm:ss");
        SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm a");
        Date date2;
        Date futureDateTime = null;
        try {
            date2 = parseFormat.parse(futureTime);
            String updateTime = displayFormat.format(date2);
            String timeArray[] = updateTime.split(":");
            String futureUpdatedFinalTime = timeArray[0] + ":" + timeArray[1] + ":" + timeArray[2].replace("00", "10");
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            futureDateTime = formatter.parse(futureDate + " " + futureUpdatedFinalTime);
           
           // futureDateTime = dt.toDate();
            LOGGER.info("futureDateTime----" + futureDateTime);
        } catch (ParseException e) {
            LOGGER.info("DateUtil : In futureDateAndTime Exception ===="+e);

            LOGGER.error("error: " + e.getMessage());
        }
        LOGGER.info("DateUtil : In futureDateAndTime End ====");

        return futureDateTime;
    }
    
    
    public static String getTimeZone(Address address,Environment environment) {
        LOGGER.info("DateUtil : In getTimeZone Starts ====");

        HttpGet latLongRequest;
        HttpGet timeZoneRequest;
        Double lat = null;
        Double lng = null;
        String timeZoneCode = null;
        Boolean validJson = true;

        if (address != null && address.getZip() != null) {
            try {
                latLongRequest = new HttpGet("https://maps.googleapis.com/maps/api/geocode/json?address="
                                + address.getZip() + "&key=" + IConstant.GOOGLE_API_KEY_FOR_ZIP);
                String latLongResponse = convertToStringJson(latLongRequest,environment);
                
               StringBuffer latLongResponse1=ProducerUtil.googleApi("https://maps.googleapis.com/maps/api/geocode/json?address="
                                + address.getZip() + "&key=" + IConstant.GOOGLE_API_KEY_FOR_ZIP);
                JSONObject latLongjObject = null;
                try {
                    latLongjObject = new JSONObject(latLongResponse1.toString());
                    if (latLongjObject.has("status")
                                    && latLongjObject.getString("status").equalsIgnoreCase("OVER_QUERY_LIMIT")) {
                        validJson = false;
                    }
                } catch (Exception e) {
                    LOGGER.info("DateUtil : In getTimeZone Exception ===="+e);

                    validJson = false;
                }

                if (validJson) {
                    if (latLongjObject.has("status") && latLongjObject.getString("status").equals("OK")) {
                        JSONArray results = latLongjObject.getJSONArray("results");
                        for (Object jObj : results) {
                            JSONObject jsonObj = (JSONObject) jObj;
                            JSONObject geometry = jsonObj.getJSONObject("geometry");

                            JSONObject location = geometry.getJSONObject("location");
                            lat = location.getDouble("lat");
                            lng = location.getDouble("lng");
                        }
                    }
                } else {
                    if (address != null && address.getMerchant() != null && address.getMerchant().getId() != null) {
                        /*MailSendUtil.webhookMail("GoogleApi Response For merchant " + address.getMerchant().getId(),
                                        latLongjObject.toString());*/
                    } else {
                       // MailSendUtil.webhookMail("GoogleApi Response", latLongjObject.toString());
                    }
                }

                if (lat != null && lng != null) {
                    timeZoneRequest = new HttpGet("https://maps.googleapis.com/maps/api/timezone/json?location=" + lat
                                    + "," + lng + "&timestamp=1458000000&key=" + IConstant.GOOGLE_API_KEY);
                    String timeZoneresponse = convertToStringJson(timeZoneRequest,environment);
                    JSONObject timeZoneObject = new JSONObject(timeZoneresponse);
                    if (timeZoneObject.getString("status").equals("OK")) {
                        timeZoneCode = timeZoneObject.getString("timeZoneId");
                    }
                }

            } catch (Exception e) {
                if (e != null) {
                    MailSendUtil.sendExceptionByMail(e,environment);
                }
                LOGGER.info("DateUtil : In getTimeZone Exception ===="+e);

                LOGGER.error("error: " + e.getMessage());
            }
        }

        if (timeZoneCode != null) {
            return timeZoneCode;
        } else {
            String timeZone = "America/Chicago";
            if (address.getMerchant() != null && address.getMerchant().getTimeZone() != null
                            && address.getMerchant().getTimeZone().getTimeZoneCode() != null) {
                timeZone = address.getMerchant().getTimeZone().getTimeZoneCode();
            }
            LOGGER.info("DateUtil : In getTimeZone timeZone ===="+timeZone);

            LOGGER.info("DateUtil : In getTimeZone Ends ====");

            return timeZone;
        }
    }


    
    public static String convertToStringJson(HttpGet request,Environment environment) {
        HttpClient client = HttpClientBuilder.create().build();
        HttpResponse response = null;
        StringBuilder responseBuilder = new StringBuilder();
        try {
            response = client.execute(request);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            String line = "";
            while ((line = rd.readLine()) != null) {
                responseBuilder.append(line);
            }
        } catch (Exception e) {
            if (e != null) {
                MailSendUtil.sendExceptionByMail(e,environment);
            }
            LOGGER.error("error: " + e.getMessage());
        }
        return responseBuilder.toString();
    }
    
    public static Boolean getZoneDateTime(String timeZoneCode, String time) throws ParseException{
    	LOGGER.info("======= DateUtil : getZoneDateTime starts : time==="+time);
   	 Calendar TimeZoneTime = new GregorianCalendar(TimeZone.getTimeZone(timeZoneCode));
   	 LOGGER.info("TIme According to timeZoneCode : "+TimeZoneTime.getTime());
		 
		  String timeArray[]=time.split(":");
		  TimeZoneTime.set(Calendar.HOUR, Integer.parseInt(timeArray[0])); 
		  TimeZoneTime.set(Calendar.MINUTE, Integer.parseInt(timeArray[1]));
	     LOGGER.info(" converted time  : "+TimeZoneTime.getTime());
	      Calendar ustTimeZone = new GregorianCalendar(TimeZone.getTimeZone("UTC"));
	        if(TimeZoneTime.getTime().after(ustTimeZone.getTime())){
	        	LOGGER.info("======= DateUtil : getZoneDateTime End ");
	 			return true;
	 		}else{
	        	LOGGER.info("======= DateUtil : getZoneDateTime End ");

	 			return false;
	 		}
    	}
    
    public static String convertTimeToUTC(String time,String timeZone) throws ParseException{
  	  	  Calendar TimeZoneTime = new GregorianCalendar(TimeZone.getTimeZone(timeZone));
		  LOGGER.info("DateUtil : In convertTimeToUTC Starts ===="+TimeZoneTime.getTime());
		  String timeArray[]=time.split(":");
		  TimeZoneTime.set(Calendar.HOUR, Integer.parseInt(timeArray[0])); 
		  TimeZoneTime.set(Calendar.MINUTE, Integer.parseInt(timeArray[1]));
		  TimeZoneTime.set(Calendar.SECOND, 0);
		  
		  SimpleDateFormat sdf4 = new SimpleDateFormat("HH:mm");
		  String str=sdf4.format(TimeZoneTime.getTime());
		  LOGGER.info( "schedular fire on time : "+str);
		  LOGGER.info("DateUtil : In convertTimeToUTC End ====");
		return str;
    }
    
    public boolean checkCurrentTimeExistBetweenTwoTime(String startTime, String endTime,String timeZoneCode) {
        boolean status = false;
        try {
        	LOGGER.info("===============  DateUtil : Inside checkCurrentTimeExistBetweenTwoTime :: Start  =============startTime "+startTime+" endTime : "+endTime);
          if(startTime!=null && endTime !=null && timeZoneCode!=null) {
            String string1 = startTime;
            Date time1 = new SimpleDateFormat("HH:mm").parse(string1);
            Calendar calendar1 = Calendar.getInstance();
            calendar1.setTime(time1);
            calendar1.add(Calendar.DATE, 1);

            String string2 = endTime;
            Date time2 = new SimpleDateFormat("HH:mm").parse(string2);
            Calendar calendar2 = Calendar.getInstance();
            if(string2.equals("00:00"))
            {
            	calendar2.add(Calendar.MINUTE, 23);
            	calendar2.add(Calendar.MINUTE, 59);
            }else
            calendar2.setTime(time2);
            calendar2.add(Calendar.DATE, 1);

            String someRandomTime = getCurrentTimeForTimeZone(timeZoneCode);
            Date d = new SimpleDateFormat("HH:mm").parse(someRandomTime);
            Calendar calendar3 = Calendar.getInstance();
            calendar3.setTime(d);
            calendar3.add(Calendar.DATE, 1);

            Date x = calendar3.getTime();
            if (x.after(calendar1.getTime()) && x.before(calendar2.getTime())) {
                // checkes whether the current time is between 14:49:00 and 20:11:13.
                status = true;
            }
          }
        } catch (ParseException e) {
            LOGGER.info("===============  DateUtil : Inside checkCurrentTimeExistBetweenTwoTime :: Exception  ============"+e);

            LOGGER.error("error: " + e.getMessage());
        }
        LOGGER.info("===============  DateUtil : Inside checkCurrentTimeExistBetweenTwoTime :: Ends  ============");
        return status;
    }
    
    public static String findCurrentTime(String timeZoneCode) {
    	Date date = new Date();
        DateFormat df = new SimpleDateFormat("HH:mm");
       
        String currentTime = df.format(date);
        df.setTimeZone(TimeZone.getTimeZone(timeZoneCode));
        
        return df.format(date);
    }
    
    public boolean checkCurrentTimeExistBetweenTwoTimeForFutureDate(String startTime, String endTime,String timeZoneCode) {
        boolean status = false;
        try {
        	LOGGER.info("===============  DateUtil : Inside checkCurrentTimeExistBetweenTwoTimeForFutureDate :: Start  =============startTime "+startTime+" endTime : "+endTime);

            String string1 = startTime;
            Date time1 = new SimpleDateFormat("HH:mm").parse(string1);
            Calendar calendar1 = Calendar.getInstance();
            calendar1.setTime(time1);
            calendar1.add(Calendar.DATE, 1);

            String string2 = endTime;
            Date time2 = new SimpleDateFormat("HH:mm").parse(string2);
            Calendar calendar2 = Calendar.getInstance();
            if(string2.equals("00:00"))
            {
            	calendar2.add(Calendar.MINUTE, 23);
            	calendar2.add(Calendar.MINUTE, 59);
            }else
            calendar2.setTime(time2);
            calendar2.add(Calendar.DATE, 1);
            String someRandomTime = getCurrentTimeForTimeZone(timeZoneCode);
            Date d = new SimpleDateFormat("HH:mm").parse(someRandomTime);
            Calendar calendar3 = Calendar.getInstance();
            calendar3.setTime(d);
            calendar3.add(Calendar.DATE, 1);

            Date x = calendar3.getTime();
            if (x.before(calendar2.getTime())) {
                // checkes whether the current time is between 14:49:00 and 20:11:13.
                status = true;
            }
        } catch (ParseException e) {
        	LOGGER.info("===============  DateUtil : Inside checkCurrentTimeExistBetweenTwoTimeForFutureDate :: Exception  ========="+e);

            LOGGER.error("error: " + e.getMessage());
        }
    	LOGGER.info("===============  DateUtil : Inside checkCurrentTimeExistBetweenTwoTimeForFutureDate :: End  =========");

        return status;
    }
    
    public static boolean findMinuteDifferenceBetweenTwoDates(Date currentDate, Date endDate) {
    	LOGGER.info("===============  DateUtil : Inside findMinuteDifferenceBetweenTwoDates :: Start  =============currentDate "+currentDate+" endDate : "+endDate);

    	boolean flag=false;
        long diff = currentDate.getTime() - endDate.getTime();
        long diffSeconds = diff / 1000 % 60;
        long diffMinutes = diff / (60 * 1000) % 60;
        long diffHours = diff / (60 * 60 * 1000) % 24;
        long diffDays = diff / (24 * 60 * 60 * 1000);
        
        if(diffDays==0 && diffHours==0 && diffMinutes<10)
        {
        	flag=true;
        }
        LOGGER.info(diffDays + " days, ");
        LOGGER.info(diffHours + " hours, ");
        LOGGER.info(diffMinutes + " minutes, ");
        LOGGER.info(diffSeconds + " seconds.");
        
        LOGGER.info("===============  DateUtil : Inside findMinuteDifferenceBetweenTwoDates :: End =====");
        return flag;
    }
    
	public static Boolean getZoneDateTime(String timeZoneCode,
			String startTime, String endTime) throws ParseException {
    	LOGGER.info("===============  DateUtil : Inside getZoneDateTime :: Start  =============startTime "+startTime+" endTime : "+endTime);


		SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss");
		sdf.setTimeZone(TimeZone.getTimeZone(timeZoneCode));
		Calendar TimeZoneTime = new GregorianCalendar();
		LOGGER.info("Time According to timeZoneCode : "
				+ TimeZoneTime.getTime());

		LOGGER.info(" converted time  : "
				+ sdf.format(TimeZoneTime.getTime()));

		String currentTimeArray[] = sdf.format(TimeZoneTime.getTime()).split(
				":");
		Calendar currentTime = new GregorianCalendar();
		currentTime.set(Calendar.HOUR, Integer.parseInt(currentTimeArray[0]));
		currentTime.set(Calendar.MINUTE, Integer.parseInt(currentTimeArray[1]));
		LOGGER.info("currentTime : " + currentTime.getTime());

		String opniningtimeArray[] = startTime.split(":");
		Calendar openingTime = new GregorianCalendar(TimeZone.getTimeZone(timeZoneCode));
		openingTime.set(Calendar.HOUR, Integer.parseInt(opniningtimeArray[0]));
		openingTime.set(Calendar.MINUTE, Integer.parseInt(opniningtimeArray[1]));
		LOGGER.info("openingTime : " + openingTime.getTime());

		String endtimeArray[] = endTime.split(":");
		Calendar endingTime = new GregorianCalendar(TimeZone.getTimeZone(timeZoneCode));
		endingTime.set(Calendar.HOUR, Integer.parseInt(endtimeArray[0]));
		endingTime.set(Calendar.MINUTE, Integer.parseInt(endtimeArray[1]));
		
		LOGGER.info("endingTime : " + endingTime.getTime());

		if (currentTime.getTime().after(openingTime.getTime()) && currentTime.getTime().before(endingTime.getTime())) {
	    	LOGGER.info("===============  DateUtil : Inside getZoneDateTime :: End  =============");
			return true;
		} else {
	    	LOGGER.info("===============  DateUtil : Inside getZoneDateTime :: End  =============");

			return false;
		}
	}

	public static boolean checkCurrentAndFullFillTime(String startTime,String timeZoneCode) {
		boolean status = false;
		try {
			LOGGER.info("===============  DateUtil : Inside checkCurrentAndFullFillTime :: Start  =============startTime "+startTime);
			String string1 = startTime;
			string1 = string1.substring(10, 16);
			String dateTimeArray[]=startTime.split(" ");
			Date fullfieldDate = new SimpleDateFormat("yyyy-MM-dd")
             .parse(dateTimeArray[0]);
            Date currentDate = new SimpleDateFormat("yyyy-MM-dd")
            .parse(DateUtil.getYYYYMMDD(new Date()));
			if(fullfieldDate.equals(currentDate)){
			Date time1 = new SimpleDateFormat("HH:mm").parse(string1);
			Calendar calendar1 = Calendar.getInstance();
			calendar1.setTime(time1);
			calendar1.add(Calendar.DATE, 1);
			Date y = calendar1.getTime();
			String someRandomTime = getCurrentTimeForTimeZone(timeZoneCode);
			Date d = new SimpleDateFormat("HH:mm").parse(someRandomTime);
			Calendar calendar3 = Calendar.getInstance();
			calendar3.setTime(d);
			calendar3.add(Calendar.DATE, 1);
			Date x = calendar3.getTime();
			if (y.after(calendar3.getTime())) {
				status = true;
			}
			}else if(fullfieldDate.after(currentDate)) status=true;
			else status=false;
			
		} catch (ParseException e) {
			LOGGER.info("===============  DateUtil : Inside checkCurrentAndFullFillTime :: Exception  =========="+e);

			LOGGER.error("error: " + e.getMessage());
		}
		LOGGER.info("===============  DateUtil : Inside checkCurrentAndFullFillTime :: End  ==========");
		return status;
	}
	public static boolean checkDeliveryTimeExistBetweenDeliveryTime(
			String selectDeliveryTime, String deliveryStartTime,
			String deliveryEndTime) {
		LOGGER.info("===============  DateUtil : Inside checkDeliveryTimeExistBetweenDeliveryTime :: Start  =============selectDeliveryTime "+selectDeliveryTime
				+" deliveryStartTime "+deliveryStartTime+" deliveryEndTime "+deliveryEndTime);

		boolean status = false;
		try {
			String string1 = deliveryStartTime;
			Date time1 = new SimpleDateFormat("HH:mm").parse(string1);
			Calendar calendar1 = Calendar.getInstance();
			calendar1.setTime(time1);
			calendar1.add(Calendar.DATE, 1);

			String string2 = deliveryEndTime;
			Date time2 = new SimpleDateFormat("HH:mm").parse(string2);
			Calendar calendar2 = Calendar.getInstance();
			calendar2.setTime(time2);
			calendar2.add(Calendar.DATE, 1);

			String string3 = selectDeliveryTime;
			Date time3 = new SimpleDateFormat("HH:mm").parse(string3);
			Calendar calendar3 = Calendar.getInstance();
			calendar3.setTime(time3);
			calendar3.add(Calendar.DATE, 1);
			Date x = calendar3.getTime();

			if (x.after(calendar1.getTime()) && x.before(calendar2.getTime())) {
				status = true;
			}
		} catch(ParseException e) {
			LOGGER.info("===============  DateUtil : Inside checkDeliveryTimeExistBetweenDeliveryTime :: Exception==========="+e);

			LOGGER.error("error: " + e.getMessage());
		}
		LOGGER.info("===============  DateUtil : Inside checkDeliveryTimeExistBetweenDeliveryTime :: End===========");

		return status;
	}

	 public static boolean checkCurrentTimeIsFiveMinutGreterthan(Date date,String timeZoneCode) {
	        boolean status = false;
	        try {
				LOGGER.info("===============  DateUtil : Inside checkCurrentTimeIsFiveMinutGreterthan :: Start  =============timeZoneCode "+timeZoneCode+" date : "+date);

	            String string1 = date.getHours()+":"+date.getMinutes();
	            Date time1 = new SimpleDateFormat("HH:mm").parse(string1);
	            Calendar calendar1 = Calendar.getInstance();
	            calendar1.setTime(time1);
	            calendar1.add(Calendar.DATE, 1);

	            String someRandomTime = getCurrentTimeForTimeZone(timeZoneCode);
	            Date d = new SimpleDateFormat("HH:mm").parse(someRandomTime);
	            Calendar calendar3 = Calendar.getInstance();
	            calendar3.setTime(d);
	            calendar3.add(Calendar.DATE, 1);
	            
	            long diff = calendar3.getTime().getTime() - calendar1.getTime().getTime();
	            long diffSeconds = diff / 1000 % 60;
	            long diffMinutes = diff / (60 * 1000) % 60;
	            long diffHours = diff / (60 * 60 * 1000) % 24;
	            long diffDays = diff / (24 * 60 * 60 * 1000);

	            LOGGER.info(diffMinutes + " minutes, ");
	            if (diffMinutes>5 ||diffHours>0 ||diffDays>0){
	                // checkes whether the current time is between 14:49:00 and 20:11:13.
	                status = true;
	            }
	        } catch (ParseException e) {
		        LOGGER.info("===============  DateUtil : Inside checkCurrentTimeIsFiveMinutGreterthan :: Exception  ========="+e);

	            LOGGER.error("error: " + e.getMessage());
	        }
	        LOGGER.info("===============  DateUtil : Inside checkCurrentTimeIsFiveMinutGreterthan :: End  =========");
	        return status;
	    }
	 public static String getYesterday() {
	        Calendar now = Calendar.getInstance();
	        now.add(Calendar.DATE, -1);
	        String start_dt = (now.get(Calendar.YEAR)) + "-" + (now.get(Calendar.MONTH) + 1) + "-"
	                        + (now.get(Calendar.DATE));
	        return (start_dt);
	    }
	 
	 public static String getCurrentDateYYYYMMDDForTimeZone(String timeZoneCode){
		    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		    
		    sdf.setTimeZone(TimeZone.getTimeZone(timeZoneCode));
		    String currentDate = sdf.format(new Date());
		    return currentDate;
		    }
	 
	 public static String getBefore7Daydate() {
	        Calendar now = Calendar.getInstance();
	        now.add(Calendar.DATE, -7);
	        String start_dt = (now.get(Calendar.YEAR)) + "-" + (now.get(Calendar.MONTH) + 1) + "-"
	                        + (now.get(Calendar.DATE));
	        return (start_dt);
	    }
	 
	 public static String findOneDate(String oldDate) {
		 SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Calendar c = Calendar.getInstance();
			try{
			   c.setTime(sdf.parse(oldDate));
			}catch(ParseException e){
				LOGGER.error("error: " + e.getMessage());
			 }
			
			c.add(Calendar.DAY_OF_MONTH, 1); 
			String newDate = sdf.format(c.getTime()); 
			return newDate;
		 }
     public static Date getCurrentDateForTimeZonee(String timeZoneCode){
         SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
         sdf.setTimeZone(TimeZone.getTimeZone(timeZoneCode));
         String currentTime = sdf.format(new Date());
         try {
              Date d = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(currentTime);
                 Calendar calendar3 = Calendar.getInstance();
                 calendar3.setTime(d);
                 System.out.println("getCurrentDateForTimeZonee ::"+calendar3.getTime());
             return calendar3.getTime();
         } catch (ParseException e) {
             LOGGER.error("error: " + e.getMessage());
         }
         return new Date();
         }
     
     public static Date addOrderAvgTimeIntoCurrentTime(String timeZoneCode,Date date,Integer orderAvg){
         SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
         sdf.setTimeZone(TimeZone.getTimeZone(timeZoneCode));
         String currentTime = sdf.format(new Date());
         try {
              Date d = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(currentTime);
                 Calendar calendar3 = Calendar.getInstance();
                 calendar3.setTime(d);
                 calendar3.add(Calendar.MINUTE, orderAvg);
                 System.out.println("getCurrentDateForTimeZonee ::"+calendar3.getTime());
             return calendar3.getTime();
         } catch (ParseException e) {
             LOGGER.error("error: " + e.getMessage());
         }
         return new Date();
         }
		 
	  public static String findCurrentDateWithTimeforTimeZone(String time_zone) {
		Date myDate = getCurrentDateForTimeZonee(time_zone);
		return new SimpleDateFormat("MM-dd-yyyy hh:mm:ss").format(myDate);
	  }
     
     public static String getCurrentDayForTimeZonee(String timeZoneCode){
    	 String[] days = new String[] { "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" };
         SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
         sdf.setTimeZone(TimeZone.getTimeZone(timeZoneCode));
         String currentTime = sdf.format(new Date());
         try {
              Date d = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(currentTime);
                 Calendar calendar3 = Calendar.getInstance();
                 calendar3.setTime(d);
                 LOGGER.info("getCurrentDateForTimeZonee ::"+calendar3.getTime());
             return days[calendar3.get(Calendar.DAY_OF_WEEK)-1];
         } catch (ParseException e) {
             LOGGER.error("error: " + e.getMessage());
         }
         return "";
         }
	 
	  //testing purpose for making business hour as 5 minute time interval
	   public static List<String> findBusinessHours() {
      List<String> times = new ArrayList<String>();
      java.sql.Time startTime = new java.sql.Time(0, 0, 0);

		@SuppressWarnings("deprecation")
		java.sql.Time endTime = new java.sql.Time(24, 0, 0);

		Calendar cal = Calendar.getInstance();
		cal.setTime(startTime);
		Calendar endCal = Calendar.getInstance();
		endCal.setTime(endTime);

		List<java.sql.Time> intervals = new ArrayList<java.sql.Time>();
		while (cal.getTime().before(endCal.getTime())) {
			intervals.add(new java.sql.Time(cal.getTimeInMillis()));
			cal.add(Calendar.MINUTE, 5);
			}
		
		intervals.add(new java.sql.Time(endCal.getTimeInMillis()));

		for (java.sql.Time time : intervals) {
			times.add(DateUtil.get12Format(time.toString()));
		}

		return times;
 }
	   
	   public static String getCurrentDayForTimeZoneAsString(String timeZoneCode){
	     	LOGGER.info("===============  DateUtil : Inside getCurrentDayForTimeZone :: Start  ============= timeZoneCode "+timeZoneCode);

	     	Date date = getCurrentDateForTimeZonee(timeZoneCode);
//	     	// Use Madrid's time zone to format the date in
//	     	df.setTimeZone(TimeZone.getTimeZone(timeZoneCode));

	     	String toDay=DateUtil.findDayNameFromDate(getYYYYMMDD(date));
	     	LOGGER.info("===============  DateUtil : Inside getCurrentDayForTimeZone :: End  ============= ");

	     	return toDay;
	     }
	   
	   public static Date currentDateAsYYYYMMdd(TimeZone timezone) {
	     	LOGGER.info("===============  DateUtil : Inside currentDate :: Start  ============= ");

	         Calendar now = Calendar.getInstance();
	         now.setTimeZone(timezone);
	         String start_dt = (now.get(Calendar.YEAR)) + "-" + (now.get(Calendar.MONTH) + 1) + "-"
	                         + (now.get(Calendar.DATE));
	     	LOGGER.info("===============  DateUtil : Inside currentDate :: End  ============= ");

	         return convertStringToDate(start_dt);
	     }
	   
	   public static String getCurrentTimeForTimeZoneForReciept(String timeZoneCode){
	        LOGGER.info("===============  DateUtil : Inside getCurrentTimeForTimeZone :: Start  =============timeZoneCode "+timeZoneCode);
	        
	        SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy hh:mm a");
	        
	        sdf.setTimeZone(TimeZone.getTimeZone(timeZoneCode));
	        String currentTime = sdf.format(new Date());
	        return currentTime;
	    }
		
		public static boolean checkDateIsAfterTodayDate(String startTime,String endTime,String timeZoneCode) {
	        boolean status = false;
	        try {
	        	LOGGER.info("===============  DateUtil : Inside checkCurrentTimeExistBetweenTwoTime :: Start  ============= endTime : "+endTime +"timeZoneCode : "+timeZoneCode);

	            String string1 = startTime;
	            Date time1 = new SimpleDateFormat("yyyy-MM-dd").parse(startTime);
	            Calendar calendar1 = Calendar.getInstance();
	            calendar1.setTime(time1);
	            Date fromdate = calendar1.getTime();
	            calendar1.add(Calendar.DATE, -1);
//	            LOGGER.info("DateUtil : Inside checkCurrentTimeExistBetweenTwoTime calendar1 time : "+calendar1.getTime());
	            //String string2 = endTime;
	            Date time2 = new SimpleDateFormat("yyyy-MM-dd").parse(endTime);
	            Calendar calendar2 = Calendar.getInstance();
	          
	            calendar2.setTime(time2);
	            calendar2.add(Calendar.DATE, 1);
	            LOGGER.info("DateUtil : Inside checkCurrentTimeExistBetweenTwoTime calendar2 time : "+calendar2.getTime());
	            String someRandomTime = getCurrentDateYYYYMMDDForTimeZone(timeZoneCode);
	            LOGGER.info("DateUtil : Inside checkCurrentTimeExistBetweenTwoTime someRandomTime time : "+someRandomTime);
	            Date d = new SimpleDateFormat("yyyy-MM-dd").parse(someRandomTime);
	            Calendar calendar3 = Calendar.getInstance();
	            calendar3.setTime(d);
	            LOGGER.info("DateUtil : Inside checkCurrentTimeExistBetweenTwoTime calendar3 time : "+calendar3.getTime());
	            Date x = calendar3.getTime();
	            String xs ="";
	            if (x.before(calendar2.getTime()) && fromdate.before(calendar2.getTime())) {
	                status = true;
	            }
	        } catch (ParseException e) {
	            LOGGER.info("===============  DateUtil : Inside checkCurrentTimeExistBetweenTwoTime :: Exception  ============"+e);

	            LOGGER.error("error: " + e.getMessage());
	        }
	        LOGGER.info("DateUtil : Inside checkCurrentTimeExistBetweenTwoTime status time : "+status);
	        LOGGER.info("===============  DateUtil : Inside checkCurrentTimeExistBetweenTwoTime :: Ends  ============");
	        return status;
	    }
	   
	   public static String dateFormatddMMMYYY(Date date) throws ParseException {
	        String date1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);
	        System.out.println(date1);
	        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	        // Format to match actual String to parse
	        Date fechaNueva = format.parse(date1);
	        SimpleDateFormat newFormat = new SimpleDateFormat("dd MMM YYY");
	        String formateDate = newFormat.format(fechaNueva);
	        System.out.println(formateDate);
	        return formateDate;
	    }
	   
	   public static String findMonthNameFromDate(String today) {
	        LOGGER.info("DateUtil : In findDayNameFromDate starts : futureDate : "+today);

	        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");//new SimpleDateFormat("dd-MM-yyyy");
	        Date date = null;
	        try {
	            date = format.parse(today);
	           // LOGGER.info("date util date- "+date);
	        } catch (ParseException e) {
	            LOGGER.info("DateUtil : In findDayNameFromDate : Exception : "+e);

	            LOGGER.error("error: " + e.getMessage());
	        }
	        LOGGER.info("DateUtil : In findDayNameFromDate End ====");
	        return (new SimpleDateFormat("LLLL").format(date)).toLowerCase();
	    }

}
