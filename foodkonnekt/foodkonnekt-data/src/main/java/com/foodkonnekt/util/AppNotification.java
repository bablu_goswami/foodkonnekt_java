package com.foodkonnekt.util;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;


public class AppNotification {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(AppNotification.class);
	
	public final static String AUTH_KEY_FCM = "AIzaSyA1Q5xK03bGWZuOPI1cq_SHxuohAw4I6JA";
	public final static String API_URL_FCM = "https://fcm.googleapis.com/fcm/send";

    public static String sendPushNotification(String deviceToken, String orderJson, String merchantPosId,Environment environment) throws IOException {
    	LOGGER.info("---------------Start :: AppNotification : sendPushNotification-------------------------");
    	LOGGER.info("AppNotification :: sendPushNotification : merchantPosId "+merchantPosId);
    	LOGGER.info("AppNotification :: sendPushNotification : deviceToken "+deviceToken);
        String result = "FAILURE";
        try {
        URL url = new URL(API_URL_FCM);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();

        conn.setUseCaches(false);
        conn.setDoInput(true);
        conn.setDoOutput(true);

        conn.setRequestMethod("POST");
        conn.setRequestProperty("Authorization", "key="+AUTH_KEY_FCM);
        conn.setRequestProperty("Content-Type", "application/json");

        JSONObject json = new JSONObject();

        if(deviceToken!=null){
            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
            wr.write(orderJson);
            wr.flush();

            BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

            String output;
            LOGGER.info("AppNotification :: sendPushNotification :  Output from Server .... \n");
            while ((output = br.readLine()) != null) {
                LOGGER.info(""+output);
                
               
                
//                LOGGER.info(orderJsonJObject.getString("data"));
                
                JSONObject jObject = new JSONObject(output);
                
                Integer failure = 0;
                if (jObject != null && jObject.has("failure")) {
                	 LOGGER.info("jObject === "+jObject.getInt("failure"));
                	 failure = jObject.getInt("failure");
                }

                JSONObject orderJsonjObject = new JSONObject(orderJson);
                Integer orderId = null;
                Integer merchantId = null;
                if(orderJsonjObject != null && orderJsonjObject.has("data")){
                	LOGGER.info("orderJsonjObject== "+orderJsonjObject.get("data"));
                    
                    JSONObject merchantJson = new JSONObject(orderJsonjObject.get("data").toString());
                    if(merchantJson != null && merchantJson.has("payload")){
                    	LOGGER.info("merchantId== "+ merchantJson.get("merchantId"));
                        LOGGER.info("payload== "+ merchantJson.get("payload"));
                        merchantId = merchantJson.getInt("merchantId");
                        
                        JSONObject payloadJson = new JSONObject(merchantJson.get("payload").toString());
                        if(payloadJson != null && payloadJson.has("orderR")){
                        	LOGGER.info("orderPayload== " +payloadJson.get("orderR"));
                            
                            JSONObject orderJsonObj = new JSONObject(payloadJson.get("orderR").toString());
                            if(orderJsonObj != null && orderJsonObj.has("orderPosId")){
                            	LOGGER.info("orderId== "+ orderJsonObj.get("orderPosId"));
                            	orderId =  orderJsonObj.getInt("orderPosId");
                            }
                        }
                    }
                }
                if(failure == 1 ){
                	if(deviceToken != null && !deviceToken.equals("")){
                    	MailSendUtil.sendMailSystemLog("Foodkonnekt","Prod Server","FCM Notification Error for MerchantId "+merchantId+" and OrderId "+orderId,output,environment);
                    }else{
                    	 LOGGER.info("AppNotification :: DeviceId is not found : merchantPosId "+merchantPosId);
                    }
                }else{
                	LOGGER.info("AppNotification :: GCM Notification is sent successfully : merchantPosId "+merchantPosId);
                }
            }
            result = "SUCCESS";
            
        }
        } catch (Exception e) {
            LOGGER.error("error: " + e.getMessage());
            MailSendUtil.sendExceptionByMail(e,environment);
            LOGGER.info("AppNotification :: sendPushNotification : Exception "+e);
            result = "FAILURE";
        }
       
        LOGGER.info("---------------End :: AppNotification : sendPushNotification-------------------------");
        return result;

    }

    public static void main(String[] args) {
    	String deviceId="ecVe5yrOtiU:APA91bFTdLvBOIeY0yf6nyq8I5x8atkcv4Pgcxt0I5PvXu6KhcwD0SikfefsxyhfL2qqij5dN4R9D9CNYhn-h0r4e_xYrN-B4vH-JupS5lL810D3aSFgTkwWPuNe0HaygtoSFRnXDh0S";
       
        String json="{\"data\": {\"merchantId\":\"134\",\"locationUid\":\"c01cd814-5bd2-4888-9167-8c9270298e48\",\"deviceId\":\"danSxrl4Vwg:APA91bGZhFvtyxNKMTW4O5spjEUhMszsA4SVkeszotG3ZEMnjDfpC1Q5-H6GUf2tbSqxCd5MKSef16KXbla0I-DrKosPTAYoscLquKn1H46rpHysfGvPJUutjvP1t6jVuMMJ_-IzVj2h\",\"payload\":{\"orderR\":{\"id\":851,\"orderName\":\"\",\"orderNote\":\"\",\"orderPrice\":18.48,\"orderDiscount\":0.0,\"tipAmount\":0.0,\"orderAvgTime\":\"45\",\"isDefaults\":0,\"createdOn\":\"Oct 6, 2018 6:02:25 AM\",\"fulfilled_on\":\"Oct 6, 2018 6:47:25 AM\",\"orderPosId\":\"851\",\"orderItems\":[{\"id\":304,\"quantity\":2,\"item\":{\"id\":1920,\"isHidden\":false,\"posItemId\":\"RVR7XZCD5GX3W\",\"name\":\"testitem3\",\"taxAble\":false,\"auxTaxAble\":false,\"price\":6.01,\"priceType\":\"FIXED\",\"isRevenue\":true,\"isDefaultTaxRates\":true,\"modifiedTime\":\"Apr 3, 2017 12:00:00 AM\",\"allowItemTimings\":0,\"itemStatus\":0},\"orderItemModifiers\":[{\"id\":118,\"quantity\":2,\"modifiers\":{\"id\":3929,\"name\":\"Meat- Potato\",\"price\":0.0,\"posModifierId\":\"KSTKFX95JHPWR\"}},{\"id\":119,\"quantity\":2,\"modifiers\":{\"id\":3930,\"name\":\"Meat- Migas\",\"price\":0.0,\"posModifierId\":\"C8ZTMVYN4Y3HJ\"}}]},{\"id\":305,\"quantity\":1,\"item\":{\"id\":2015,\"isHidden\":true,\"posItemId\":\"SFXKGAKCSGMNG\",\"name\":\"Convenience Fee\",\"taxAble\":false,\"auxTaxAble\":false,\"price\":2.5,\"priceType\":\"FIXED\",\"isRevenue\":true,\"isDefaultTaxRates\":true,\"modifiedTime\":\"Oct 12, 2017 12:00:00 AM\",\"allowItemTimings\":0,\"itemStatus\":1},\"orderItemModifiers\":[]}],\"subTotal\":\"12.02\",\"tax\":\"1.20\",\"posPaymentId\":\"30NDHTPYWYDV6\",\"paymentMethod\":\"Credit Card\",\"orderType\":\"Pickup\",\"convenienceFee\":\"2.5\",\"deliveryFee\":\"0\",\"isFutureOrder\":0}}} ,\"registration_ids\":[\"duDRPg4_rUE:APA91bFSyoyZbih24sYxrBYxfAbcGwnkCBnr2tJf7iGnr4_UVoErf5kALnYADU9cFr7WrpqzjKGOdWtLjT9ptcu6ibQ1_m0xjsnOwTqBaZoAYkTsUgbqK1WGLQHwwy5PrsE_xGjtKLoc\"]}";
        try {
        	//sendPushNotification(deviceId,json,"301");
		} catch (Exception e) {
			LOGGER.info("AppNotification :: main : Exception "+e);
			LOGGER.error("error: " + e.getMessage());
		}
    }
}
