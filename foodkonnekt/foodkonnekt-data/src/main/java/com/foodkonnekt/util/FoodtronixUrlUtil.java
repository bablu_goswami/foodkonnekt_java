package com.foodkonnekt.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.foodkonnekt.model.FoodTronix;

public class FoodtronixUrlUtil {

    /**
     * Find foodtronix category
     * 
     * @param foodtronix
     * @return
     */
	private static final Logger LOGGER= LoggerFactory.getLogger(FoodtronixUrlUtil.class);

	public static String getRestaurantDetails(FoodTronix foodTronix) {
		LOGGER.info("===============  FoodtronixUrlUtil : Inside getRestaurantDetails :: Start  ============= ");

        HttpGet request = new HttpGet(foodTronix.getUrl()+foodTronix.getCompanyId() + foodTronix.getInstantUrl() + foodTronix.getStoreId()+"/store");
        request.addHeader("x-access-token", foodTronix.getAuthToken());
        LOGGER.info("===============  FoodtronixUrlUtil : Inside getRestaurantDetails :: End  ============= ");

        return convertToStringJson(request);
    }
    
	public static String getItemProperty(FoodTronix foodTronix) {
		LOGGER.info("===============  FoodtronixUrlUtil : Inside getItemProperty :: Start  ============= ");

        HttpGet request = new HttpGet(foodTronix.getUrl()+foodTronix.getCompanyId() + foodTronix.getInstantUrl() + foodTronix.getStoreId()+"/ItemProperty");
        request.addHeader("x-access-token", foodTronix.getAuthToken());
		LOGGER.info("===============  FoodtronixUrlUtil : Inside getItemProperty :: End  ============= ");

        return convertToStringJson(request);
    }
	
	public static String getItemPropertyGroup(FoodTronix foodTronix) {
		LOGGER.info("===============  FoodtronixUrlUtil : Inside getItemPropertyGroup :: Start  ============= ");

        HttpGet request = new HttpGet(foodTronix.getUrl()+foodTronix.getCompanyId() + foodTronix.getInstantUrl() + foodTronix.getStoreId()+"/itempropertygroup");
        request.addHeader("x-access-token", foodTronix.getAuthToken());
		LOGGER.info("===============  FoodtronixUrlUtil : Inside getItemPropertyGroup :: End  ============= ");

        return convertToStringJson(request);
    }
	
	public static String getDishCategory(FoodTronix foodTronix) {
		LOGGER.info("===============  FoodtronixUrlUtil : Inside getDishCategory :: Start  ============= ");

        HttpGet request = new HttpGet(foodTronix.getUrl()+foodTronix.getCompanyId() + foodTronix.getInstantUrl() + foodTronix.getStoreId()+"/dishcategory");
        request.addHeader("x-access-token", foodTronix.getAuthToken());
		LOGGER.info("===============  FoodtronixUrlUtil : Inside getDishCategory :: End  ============= ");

        return convertToStringJson(request);
    }
	
	public static String getPizzaTemplate(FoodTronix foodTronix) {
		LOGGER.info("===============  FoodtronixUrlUtil : Inside getPizzaTemplate :: Starts  ============= ");

        HttpGet request = new HttpGet(foodTronix.getUrl()+foodTronix.getCompanyId() + foodTronix.getInstantUrl() + foodTronix.getStoreId()+"/pizzatemplate");
        request.addHeader("x-access-token", foodTronix.getAuthToken());
		LOGGER.info("===============  FoodtronixUrlUtil : Inside getPizzaTemplate :: End  ============= ");

        return convertToStringJson(request);
    }
	
	public static String getPizzaTemplateSize(FoodTronix foodTronix) {
		LOGGER.info("===============  FoodtronixUrlUtil : Inside getPizzaTemplateSize :: Starts  ============= ");

        HttpGet request = new HttpGet(foodTronix.getUrl()+foodTronix.getCompanyId() + foodTronix.getInstantUrl() + foodTronix.getStoreId()+"/pizzatemplatesize");
        request.addHeader("x-access-token", foodTronix.getAuthToken());
		LOGGER.info("===============  FoodtronixUrlUtil : Inside getPizzaTemplateSize :: End  ============= ");

        return convertToStringJson(request);
    }
	
	public static String getPizzaTopping(FoodTronix foodTronix) {
		LOGGER.info("===============  FoodtronixUrlUtil : Inside getPizzaTopping :: Starts  ============= ");

        HttpGet request = new HttpGet(foodTronix.getUrl()+foodTronix.getCompanyId() + foodTronix.getInstantUrl() + foodTronix.getStoreId()+"/pizzatoppings");
        request.addHeader("x-access-token", foodTronix.getAuthToken());
		LOGGER.info("===============  FoodtronixUrlUtil : Inside getPizzaTopping :: End  ============= ");

        return convertToStringJson(request);
    }
	
	public static String getPizzaToppingSize(FoodTronix foodTronix) {
		LOGGER.info("===============  FoodtronixUrlUtil : Inside getPizzaToppingSize :: Starts  ============= ");

        HttpGet request = new HttpGet(foodTronix.getUrl()+foodTronix.getCompanyId() + foodTronix.getInstantUrl() + foodTronix.getStoreId()+"/pizzasizetopping");
        request.addHeader("x-access-token", foodTronix.getAuthToken());
		LOGGER.info("===============  FoodtronixUrlUtil : Inside getPizzaToppingSize :: End  ============= ");

        return convertToStringJson(request);
    }
	
	public static String getPizzaSize(FoodTronix foodTronix) {
		LOGGER.info("===============  FoodtronixUrlUtil : Inside getPizzaSize :: Starts  ============= ");

        HttpGet request = new HttpGet(foodTronix.getUrl()+foodTronix.getCompanyId() + foodTronix.getInstantUrl() + foodTronix.getStoreId()+"/pizzasizes");
        request.addHeader("x-access-token", foodTronix.getAuthToken());
		LOGGER.info("===============  FoodtronixUrlUtil : Inside getPizzaSize :: End  ============= ");

        return convertToStringJson(request);
    }
	
	
	
	
	
	
	
	
	
	
	
    
    public static String convertToStringJson(HttpGet request) {
    	
        HttpClient client = HttpClientBuilder.create().build();
        HttpResponse response = null;
        StringBuilder responseBuilder = new StringBuilder();
        try {
            response = client.execute(request);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            String line = "";
            while ((line = rd.readLine()) != null) {
                responseBuilder.append(line);
            }
        } catch (IOException e) {
            LOGGER.error("error: " + e.getMessage());
        }
        return responseBuilder.toString();
    }

}
