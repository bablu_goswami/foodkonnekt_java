package com.foodkonnekt.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.foodkonnekt.model.Customer;
import com.foodkonnekt.model.OrderR;

public class CommonUtil {
	private static final Logger LOGGER= LoggerFactory.getLogger(CommonUtil.class);

	public static String getStartTime(String startTime, String validity) {
		String finalTime = null;
		if (startTime != null) {
			String[] startTimeArray = startTime.split(",");
			if (startTimeArray.length > 0) {
				SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm");
				SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm a");
				String time;
				try {
					if (validity.equals("0")) {
						time = startTimeArray[0] + ":" + startTimeArray[1];
						Date date = displayFormat.parse(time);
						finalTime = parseFormat.format(date);
					}
					if (validity.equals("1")) {
						time = startTimeArray[2] + ":" + startTimeArray[3];
						Date date = displayFormat.parse(time);
						finalTime = parseFormat.format(date);
					}
					if (validity.equals("2")) {
						time = startTimeArray[4] + ":" + startTimeArray[5];
						Date date = displayFormat.parse(time);
						finalTime = parseFormat.format(date);
					}
				} catch (ParseException e) {
					LOGGER.error("error: " + e.getMessage());
				}
			}
		}
		return finalTime;
	}

	public static List<Customer> filterCustomers(List<Customer> customers) {
		LOGGER.info("===== CommonUtil : filterCustomers starts =====");
		for (Customer customer : customers) {
			customer.setMerchantt(null);
			customer.setVendor(null);
		}
		LOGGER.info("===== CommonUtil : filterCustomers End =====");

		return customers;
	}
	public static OrderR filterOrderRForAppNotification(OrderR orderR) {
		if(orderR!=null)
		{
			orderR.setAddress(null);
			orderR.setMerchant(null);
			orderR.setOrderDiscountsList(null);
			orderR.setOrderItems(null);
			orderR.setOrderPizza(null);
			orderR.setOrders(null);
		}
		return orderR;
	}
	
	public static String getIPAddressFromRequest(HttpServletRequest request) {
		LOGGER.info("===== CommonUtil : getIPAddressFromRequest starts =====");
		String remoteAddr = "";
		if (request != null) {			
			remoteAddr = request.getHeader(IConstant.REQUEST_HEADER);
			if (remoteAddr == null || "".equals(remoteAddr)) {
				remoteAddr = request.getRemoteAddr();
			}
		}
		LOGGER.info("===== CommonUtil : getIPAddressFromRequest ends:remoteAddr "+remoteAddr);
		return remoteAddr;
	}
	
	
	public static String getGeoLocationByIPaddress(String ipAddress) {
		LOGGER.info("===== CommonUtil : getGeoLocationByIPaddress starts: ipAddress =====" + ipAddress);
		String geolocation = "";
		if (ipAddress != null) {
			// TODO ipAddress format check
			try {
				URL geoLocationURL = new URL(IConstant.GEO_LOCATION_URL + ipAddress);
				try {
					if (geoLocationURL != null) {
						BufferedReader br = new BufferedReader(new InputStreamReader(geoLocationURL.openStream()));
						if (br != null) {
							// First 17 digits are 'IP address' and 'Status'.
							// TODO hardcoding removal
							String glocation = br.readLine();
							if (glocation != null) {
								geolocation = glocation.substring(17);
							}
						}
					}
				} catch (IOException e) {
					LOGGER.info("CommonUtil : Inside getGeoLocationByIPaddress : IOException :" + e);
				}
			} catch (MalformedURLException e) {
				LOGGER.info("CommonUtil : Inside getGeoLocationByIPaddress : MalformedURLException :" + e);
			}
		}
		LOGGER.info("===== CommonUtil : getGeoLocationByIPaddress ends: geolocation =====" + geolocation);
		return geolocation;
	}
}
