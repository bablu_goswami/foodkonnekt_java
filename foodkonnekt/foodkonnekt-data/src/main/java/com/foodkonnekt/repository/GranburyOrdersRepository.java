package com.foodkonnekt.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.foodkonnekt.model.GranburyOrders;

public interface GranburyOrdersRepository extends JpaRepository<GranburyOrders, Integer>{

	
	 public GranburyOrders findByOrderIdAndMerchantId(Integer orderId,Integer merchantId);
}
