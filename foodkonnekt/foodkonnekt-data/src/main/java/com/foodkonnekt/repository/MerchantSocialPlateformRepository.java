package com.foodkonnekt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.foodkonnekt.model.Merchant;
import com.foodkonnekt.model.MerchantSocialPlatform;

@Repository

public interface MerchantSocialPlateformRepository extends JpaRepository<MerchantSocialPlatform, Integer> {
	
	@Query(value="SELECT ga_script FROM merchant_social_plateform WHERE merchant_id=:id",nativeQuery = true)
     public String findGascriptByMerchant(@Param("id") final Integer id);
	
	public List<MerchantSocialPlatform> findByMerchantId(Integer merchantId);
	
}
