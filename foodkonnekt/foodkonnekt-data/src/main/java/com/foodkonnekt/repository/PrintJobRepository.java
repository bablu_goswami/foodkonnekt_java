package com.foodkonnekt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.foodkonnekt.model.PrintJob;

public interface PrintJobRepository extends JpaRepository<PrintJob, Integer> {
	
	PrintJob findByOrderId(String orderId);
	
	List<PrintJob> findByMerchantId (Integer merchantId);
	
	

}
