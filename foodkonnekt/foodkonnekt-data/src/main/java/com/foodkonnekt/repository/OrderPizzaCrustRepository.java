package com.foodkonnekt.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.foodkonnekt.model.OrderPizzaCrust;

public interface OrderPizzaCrustRepository extends JpaRepository<OrderPizzaCrust, Integer>{
    OrderPizzaCrust findByOrderPizzaId(Integer orderId);
}
