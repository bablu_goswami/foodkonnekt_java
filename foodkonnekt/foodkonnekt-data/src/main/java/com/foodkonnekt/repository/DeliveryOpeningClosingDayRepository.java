package com.foodkonnekt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.foodkonnekt.model.DeliveryOpeningClosingDay;

public interface DeliveryOpeningClosingDayRepository extends JpaRepository<DeliveryOpeningClosingDay, Integer> {
	
	List<DeliveryOpeningClosingDay> findByMerchantId(Integer merchantId);
	
	public DeliveryOpeningClosingDay findByDayAndMerchantId(String day, Integer merchantId);

}
