package com.foodkonnekt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.foodkonnekt.model.DesktopVesion;



public interface DesktopversionRepository extends JpaRepository<DesktopVesion, Integer>  {

	@Query(value="select version from desktop_app_version where is_active=1",nativeQuery=true)
	    public Integer findVersionByIsActive();
	@Query(value="select * FROM desktop_app_version ORDER BY is_active desc",nativeQuery=true)
	public List<DesktopVesion> findByDesnding();
}
