package com.foodkonnekt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.foodkonnekt.model.PizzaToppingSize;

public interface PizzaToppingSizeRepository extends JpaRepository<PizzaToppingSize, Integer> {

	List<PizzaToppingSize> findByPizzaToppingId(Integer id);

	List<PizzaToppingSize> findByPizzaSizeId(int pizzaSizeId);

	List<PizzaToppingSize> findByPizzaSizeIdAndPizzaToppingMerchantId(int pizzaSizeId, Integer merchantId);

	List<PizzaToppingSize> findByPizzaToppingMerchantId(Integer merchantId);
	
	List<PizzaToppingSize> findByPizzaSizeIdAndPizzaToppingId(int pizzaSizeId,Integer id);
	
	List<PizzaToppingSize> findByPizzaSizeDescriptionAndPizzaToppingId(String description,Integer toppingId);

	PizzaToppingSize findByPizzaToppingIdAndPizzaSizeId(Integer toppingId,Integer pizzaSizeId);
	
	List<PizzaToppingSize> findByPizzaSizeIdAndPizzaToppingIdAndActive(int pizzaSizeId,Integer id,Integer active);
	
	@Query(value = "SELECT * FROM pizzatoppingsize WHERE pizzatopping_id in (:pizzatoppingId) and active=:active and pizzasize_id=:pizzaSizeId", nativeQuery = true) 
	List<PizzaToppingSize> findByPizzaSizeIdAndPizzaToppingIdAndActive(@Param("pizzaSizeId")Integer pizzaSizeId,@Param("pizzatoppingId")List<Integer> pizzatoppingId,@Param("active")Integer active);

	List<PizzaToppingSize> findByPizzaSizeIdAndActive(int pizzaSizeId,Integer active);
	
	List<PizzaToppingSize> findByPizzaToppingIdAndPizzaSizeActive(Integer id,Integer active);
	
	List<PizzaToppingSize> findByPizzaSizeIdAndPizzaToppingActive(int pizzaSizeId, int active);
	
//	List<PizzaToppingSize> findByActiveAndPizzaToppingMerchantIdAndPizzaToppingActive(Integer toppingSizeActive,
//			Integer merchantId , Integer toppingActive);
	
	@Query(value = "SELECT * FROM pizzatoppingsize WHERE pizzasize_id=:sizeId" ,nativeQuery = true)
	PizzaToppingSize findPizzaByPizzaSizeId(@Param("sizeId") Integer sizeId);
	
	List<PizzaToppingSize> findByPizzaSizeIdAndActiveAndPizzaToppingActive(int pizzaSizeId, int active ,int toppingActive);

}
