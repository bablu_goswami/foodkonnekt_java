package com.foodkonnekt.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.foodkonnekt.model.NotificationApp;

public interface NotificationAppRepository extends JpaRepository<NotificationApp, Integer> {
	
	public NotificationApp findById(Integer id);

}
