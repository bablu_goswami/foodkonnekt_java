package com.foodkonnekt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.foodkonnekt.model.ItemTiming;

public interface ItemTimingRepository extends JpaRepository<ItemTiming, Integer> {
    
	  List<ItemTiming> findByItemId(Integer id);
	  
	  @Query(value="select * from item_timing ct where ct.day=:day and ct.item_id=:itemId and ct.start_time<=:startTime and ct.end_time>=:endTime", nativeQuery = true)
	  ItemTiming findByItemIdAndDayAndStartTimeAndEndTime(@Param("day")String day,@Param("itemId")Integer itemId,@Param("startTime")String startTime,@Param("endTime")String endTime);
		
     
     ItemTiming  findByItemIdAndDay(Integer id,String day);
     
     @Modifying
     @Transactional
     @Query("delete from ItemTiming it where it.item.id=?1")
     Integer deleteByItemId(Integer id);
}
