package com.foodkonnekt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.foodkonnekt.model.ItemModifiers;

public interface ItemModifiersRepository extends JpaRepository<ItemModifiers, Integer> {
	
	/**
     * Find modifierGroup by item id from database
     * 
     * @param itemId
     * @return List<ItemModifierGroup>
     */
    public List<ItemModifiers> findByItemId(Integer itemId);

    
    
    /**
     * find by ModifierGroupId and itemId
     * 
     * @param modifierGroupId
     * @param itemId
     * @return ItemModifierGroup instance
     */
    public ItemModifiers findByModifiersIdAndItemId(Integer modifiersId, Integer itemId);
    
    public ItemModifiers findByModifiersIdAndItemIdAndModifierStatus(Integer modifiersId, Integer itemId,Integer modifierStatus);

    /**
     * Find by modifierGroupId
     * 
     * @param modifierGroupId
     * @return List<ItemModifierGroup>
     */
    public List<ItemModifiers> findByModifiersId(Integer modifiersId);
    
    public List<ItemModifiers> findByModifierGroupIdAndItemId(Integer modifierGroupId,Integer itemId);

    public List<ItemModifiers> findByModifierGroupIdAndItemIdAndModifiersStatus(Integer modifierGroupId,Integer itemId,Integer modifierStatu);

    @Query(value="SELECT * from item_modifiers_map im join modifiergroup_modifiers mm on (im.modifier_group_id=mm.modifiergroup_id and im.modifiers_id=mm.modifiers_id)" + 
    		" join modifiers m on im.modifiers_id=m.id where im.modifier_group_id=:modifierGrpId and im.item_id=:itemId "
    		+ "and im.modifier_status=:modifierStatus and mm.status=1 and m.status=1",nativeQuery = true)
    public List<ItemModifiers> findByModifierGroupIdAndItemIdAndModifiersStatusAndGrpMapping(@Param("modifierGrpId") Integer modifierGroupId,
    		@Param("itemId") Integer itemId,@Param("modifierStatus") Integer modifierStatus);
    
    @Modifying
    @Transactional
    @Query(value=" DELETE FROM item_modifiers_map WHERE modifiers_id=:modifiersId",nativeQuery = true )
    void deleteDuplicateItemModifiersMapping(@Param("modifiersId") Integer modifiersId);

}
