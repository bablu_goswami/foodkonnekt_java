package com.foodkonnekt.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.foodkonnekt.model.OrderTracker;

public interface OrderTrackerRepository extends JpaRepository<OrderTracker, Integer>{

}
