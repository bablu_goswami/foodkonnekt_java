package com.foodkonnekt.repository;

import java.util.Date;
import java.util.List;
import java.util.List;import org.hibernate.type.TrueFalseType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.foodkonnekt.model.Customer;

public interface CustomerrRepository extends JpaRepository<Customer, Integer> {

	/**
	 * Find by email , password and vendorId
	 * 
	 * @param emailId
	 * @param password
	 * @param vendorId
	 * @return
	 */
	@Query("SELECT c FROM Customer c WHERE c.emailId=:emailId and c.password=:password and c.merchantt.id=:merchantId")
	List<Customer> findByEmailIdAndPasswordAndMerchanttId(@Param("emailId") final String emailId,
			@Param("password") final String password, @Param("merchantId") final Integer merchantId);

	@Query("SELECT c FROM Customer c WHERE c.emailId=:emailId and c.password=:password and c.vendor.id=:vendorId")
	List<Customer> findByEmailIdAndPasswordAndVendorId(@Param("emailId") final String emailId,
			@Param("password") final String password, @Param("vendorId") final Integer vendorId);

	@Query("SELECT c FROM Customer c WHERE c.emailId=:emailId and c.password=:password")
	List<Customer> findByEmailIdAndPassword(@Param("emailId") final String emailId,
			@Param("password") final String password);

	/**
	 * Find by email Id
	 * 
	 * @param emailId
	 * @return
	 */
	List<Customer> findByEmailId(String emailId);

	@Query("SELECT c FROM Customer c WHERE c.merchantt.id=:merchantId and c.emailId=:emailId")
	List<Customer> findByEmailIdAndMerchantId(@Param("emailId") final String emailId,
			@Param("merchantId") final Integer merchantId);

	@Query("SELECT c FROM Customer c WHERE c.vendor.id=:vendorId and c.emailId=:emailId")
	List<Customer> findByEmailIdAndVendorId(@Param("emailId") final String emailId,
			@Param("vendorId") final Integer vendorId);

	@Query("SELECT c FROM Customer c WHERE c.id=:id and c.emailId=:emailId")
	List<Customer> findByEmailIdAndCustomerId(@Param("emailId") final String emailId, @Param("id") final Integer id);

	Customer findByPassword(String password);

	/**
	 * Find by vendorId
	 * 
	 * @param vendorId
	 * @return List<Customer>
	 */
	@Query("SELECT c FROM Customer c WHERE c.merchantt.id=:merchantId")
	List<Customer> findByMerchantId(@Param("merchantId") final Integer merchantId);

	@Query("SELECT count(c) FROM Customer c WHERE c.merchantt.id=:merchantId")
	int countByVendorId(@Param("merchantId") final Integer merchantId);

	@Query("SELECT c FROM Customer c WHERE c.merchantt.id=:merchantId AND ((c.customerType <> 'admin' AND c.customerType <> 'location') OR c.customerType is null) ")
	Page<Customer> findByMerchantId(@Param("merchantId") final Integer merchantId, Pageable pageable);

	@Query("SELECT c FROM Customer c WHERE c.merchantt.id=:merchantId and c.customerType<>:customerType")
	Page<Customer> findByMerchantIdAndCustomerType(@Param("merchantId") final Integer merchantId,
			@Param("customerType") final String customerType, Pageable pageable);

	@Query("SELECT c FROM Customer c WHERE c.merchantt.id=:merchantId and c.firstName like %:searchTxt% ")
	List<Customer> findByFirstNameLikeIgnoreCaseAndMerchantId(@Param("searchTxt") final String searchTxt,
			@Param("merchantId") final Integer merchantId);

	List<Customer> findByBirthDate(String wishDate);

	List<Customer> findByAnniversaryDate(String wishDate);

	/**
	 * Get Customers by merchantUId
	 * 
	 * @param merchantUId
	 * @return List<Customer>
	 */
	@Query("SELECT c FROM Customer c WHERE c.merchantt.merchantUid=:merchantUId")
	public List<Customer> findByMerchantMerchantUid(@Param("merchantUId") final String merchantUId);

	Customer findByCustomerUid(String searchCustomerByUUId);

	List<Customer> findByPhoneNumber(String phonenumber);
	
	@Query("SELECT c FROM Customer c WHERE c.createdDate BETWEEN :startDate AND :endDate")
	List<Customer> findByCreatedDate(@Param("startDate") String startDate, 
			@Param("endDate") String endDate);
	
	List<Customer> findByPhoneNumberAndMerchanttId(String phonenumber,Integer merchantId);
	

	@Query(value = "select * from customer c join customer_feedback cf on c.id=cf.customer_id join"
			+ " merchant m on cf.merchant_id=m.id where m.merchant_uid =:merchantUid GROUP BY c.id",
			nativeQuery = true)
	List<Customer> findAllkritiqCustomerByMerchantUid(@Param("merchantUid") String merchantUid);
	
	@Query(value = "select * from customer c join customer_feedback cf on c.id=cf.customer_id join merchant m on cf.merchant_id=m.id "
			+ "where m.merchant_uid =:merchantUid and cf.create_date BETWEEN :startDate AND :endDate GROUP BY c.id "
			,nativeQuery = true)
	List<Customer> findAllkritiqCustomerByMerchantUidAndDate(@Param("merchantUid")String merchantUid,@Param("startDate")Date startDate, 
			@Param("endDate")Date endDate);
	
	@Query(value = "select * from customer c join customer_feedback cf on c.id=cf.customer_id join"
			+ " merchant m on cf.merchant_id=m.id where m.merchant_uid =:merchantUid GROUP BY c.id HAVING COUNT(cf.customer_id) > 3",
			nativeQuery = true)
	List<Customer> findActivekritiqCustomerByMerchantUid(@Param("merchantUid") String merchantUid);
	
	@Query(value = "select * from customer c join customer_feedback cf on c.id=cf.customer_id join merchant m on cf.merchant_id=m.id "
			+ "where m.merchant_uid =:merchantUid and cf.create_date BETWEEN :startDate AND :endDate "
			+ "GROUP BY c.id HAVING COUNT(cf.customer_id) > 3",nativeQuery = true)
	List<Customer> findActivekritiqCustomerByMerchantUidAndDate(@Param("merchantUid")String merchantUid,@Param("startDate")Date startDate, 
			@Param("endDate")Date endDate);
	
	@Query(value ="SELECT c FROM Customer c join order_r r on r.customer_id=c.id WHERE c.merchant_id=:merchantId AND r.createdOn BETWEEN :startDate AND :endDate group by c.id having count(*)>3", nativeQuery=true)
	List<Customer> findActiveFoodkonnektCustomerByMerchantIdAndDate(@Param("merchantId") Integer merchantId,@Param("startDate") Date startDate, 
			@Param("endDate") Date endDate);
	
	@Query(value ="select * from customer c  where (c.merchant_id=:merchantId and c.customer_type='location')"
			+ " or (c.vendor_id=:vendorId and c.customer_type='admin')",nativeQuery = true)
	public List<Customer> findByMerchantIdAndVendorIdAndCustomerType(@Param("merchantId") Integer merchantId ,
			@Param("vendorId") Integer vendorId);
	
	@Query(value ="select * from customer c where c.email_id=:emailId and c.password is not null",nativeQuery = true)
	public List<Customer> findByEmailAndPasswordNotNull(@Param("emailId") String emailId);
	
	@Query("SELECT c FROM Customer c WHERE c.merchantt.id=:merchantId and c.customerType=:customerType")
    Customer findByCustomertypeAndMerchantId(@Param("customerType") final String customerType,
                                             @Param("merchantId") final Integer merchantId);

	@Query(value ="select * from customer c where c.email_id=:emailId and c.password is not null and "
			+ "(c.customer_type='admin' OR c.customer_type='location')",nativeQuery = true)
    List<Customer> findByEmailAndPasswordNotNullAndCustomerType(@Param("emailId") String emailId);
	@Query(value ="select * from customer c where c.email_id=:emailId and c.password is not null and "
			+ "c.customer_type=''",nativeQuery = true)
	List<Customer> findDeletedCustomerByEmailAndPasswordNotNull(@Param("emailId") String emailId);
}
