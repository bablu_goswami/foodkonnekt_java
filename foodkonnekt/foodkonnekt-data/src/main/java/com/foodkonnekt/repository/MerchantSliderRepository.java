package com.foodkonnekt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.foodkonnekt.model.MerchantSliders;

public interface MerchantSliderRepository extends JpaRepository<MerchantSliders, Integer>{
	
	List<MerchantSliders> findByMerchantId(Integer merchantId);
	
	@Query(value="SELECT count(slider_image) from merchant_sliders where merchant_id= :merchantId",nativeQuery=true)
	Integer findSliderImageCountByMerchantId(@Param ("merchantId") Integer merchantId);

	MerchantSliders findById(Integer sliderId);
}
