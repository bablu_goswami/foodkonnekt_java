package com.foodkonnekt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.foodkonnekt.model.PizzaTemplateCategory;

public interface PizzaTemplateCategoryRepository extends JpaRepository<PizzaTemplateCategory, Integer> {

	List<PizzaTemplateCategory> findByCategoryIdAndPizzaTemplateId(Integer categoryId,Integer pizzaTemplateId);
	List<PizzaTemplateCategory> findByCategoryId(Integer categoryId);
	Long countByCategoryId(Integer categoryId);
	
	PizzaTemplateCategory findByPizzaTemplateId(Integer templateId);
	
	PizzaTemplateCategory findByPizzaTemplateIdAndCategoryId(Integer pizzaTemplateId,Integer categoryId);
	
	@Query("select m from PizzaTemplateCategory m where m.category.id=:categoryId and  m.pizzaTemplate.id in(:pizzaTemplateId)")
	List<PizzaTemplateCategory> findByCategoryIdAndPizzaTemplateIds(@Param("categoryId") Integer categoryId,@Param("pizzaTemplateId")List<Integer> pizzaTemplateId);
	
	List<PizzaTemplateCategory> findByCategoryIdAndPizzaTemplateActive(Integer categoryId,Integer active);
	
	@Query(value="SELECT * FROM pizza_template_category WHERE pizza_template_category.pizza_template_id IN (:template_id)", nativeQuery = true)
    List<PizzaTemplateCategory> findByPizzaTemplateId(@Param(value="template_id") List<Integer> template_id);
	
	@Query(value="SELECT * FROM pizza_template_category WHERE pizza_template_category.pizza_template_id=:template_id" ,nativeQuery = true)
	List<PizzaTemplateCategory> findListByPizzaTemplateId(@Param(value="template_id") Integer templateId);
	
	List<PizzaTemplateCategory> findByPizzaTemplateIdAndCategoryItemStatus(Integer templateId , Integer itemStatus);
	
	@Query(value="select * from pizza_template_category pc join pizzatemplate p on pc.pizza_template_id=p.id "
			+ "join pizza_template_taxes t on p.id=t.template_id where pc.category_id=:categoryId and "
			+ "p.active=:pizzaActive and t.is_active=:taxActive",nativeQuery = true)
    List<PizzaTemplateCategory> findByCategoryIdAndPizzaTemplateActiveAndTax(@Param("categoryId") Integer categoryId
    		        ,@Param("pizzaActive")  Integer pizzaActive,@Param("taxActive")  Integer taxActive);
	
}
