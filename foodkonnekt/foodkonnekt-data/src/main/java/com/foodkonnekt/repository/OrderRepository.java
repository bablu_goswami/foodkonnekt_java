package com.foodkonnekt.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.PageableDefaults;

import com.foodkonnekt.model.OrderR;

public interface OrderRepository extends JpaRepository<OrderR, Integer> {

    /**
     * Find by customerId and merchantId
     * 
     * @param customerId
     * @param merchantId
     * @return List<OrderR> orderRs
     */
    public List<OrderR> findByCustomerIdAndMerchantId(Integer customerId, Integer merchantId);
    
    public List<OrderR> findByCustomerIdAndMerchantIdAndId(Integer customerId, Integer merchantId, Integer id);

    /**
     * Find by customerId and orderPosId
     * 
     * @param customerId
     * @param orderPosId
     * @return {@link OrderR}
     */
    public OrderR findByCustomerIdAndOrderPosId(Integer customerId, String orderPosId);

    /**
     * Find by orderPosId
     * 
     * @param orderId
     * @return OrderR instance
     */
    public OrderR findByOrderPosId(String orderId);
    
    public OrderR findByOrderPosIdAndMerchantId(String orderId, Integer merchantId);

    /**
     * Find by merchantId
     * 
     * @param merchantId
     * @param pageable 
     * @param enddate 
     * @param startdate 
     * @return List<OrderR>
     */
    public List<OrderR> findByMerchantId(int merchantId);

    @Query("SELECT c FROM OrderR c WHERE c.createdOn BETWEEN :startDate AND :endDate and c.merchant.id=:merchantId")
    public List<OrderR> findByStartDateAndEndDateAndMerchantId(@Param("startDate") final Date startDate,
                    @Param("endDate") final Date endDate, @Param("merchantId") final Integer merchantId);

    @Query("SELECT c FROM OrderR c WHERE c.createdOn = :startDate and c.merchant.id=:merchantId")
    public List<OrderR> findByStartDateAndMerchantId(@Param("startDate") final Date startDate,
                    @Param("merchantId") final Integer merchantId);

    @Query("SELECT c FROM OrderR c WHERE c.fulfilled_on > :currentDate and c.merchant.id=:merchantId")
    public List<OrderR> findByFulFilledDateAndMerchantId(@Param("currentDate") final Date currentDate,
                    @Param("merchantId") final Integer merchantId);

   /* @Query("SELECT c FROM OrderR c WHERE c.createdOn = :startDate and c.merchant.activeCustomerFeedback=:activeCustomerFeedback")
    public List<OrderR> findByStartDateAndMerchantActiveCustomerFeedback(@Param("startDate") final Date startDate,
                    @Param("activeCustomerFeedback") final Integer activeCustomerFeedback);*/

    /*@Query("SELECT c FROM OrderR c WHERE c.createdOn  like ':startDate%' and c.merchant.activeCustomerFeedback=:activeCustomerFeedback")
    public List<OrderR> findByCreatedOnAndMerchantActiveCustomerFeedback(@Param("startDate") final Date startDate,
                    @Param("activeCustomerFeedback") final Integer activeCustomerFeedback);*/
    
   /* @Query("SELECT c FROM OrderR c WHERE c.createdOn  LIKE '%:startDate%' ")
    public List<OrderR> findByCreatedOn(@Param("startDate") final Date startDate);*/
    
   /* @Query("SELECT c FROM OrderR c WHERE c.createdOn  BETWEEN :startDate AND :endDate ")
    public List<OrderR> findByStartDateAndEndDate(@Param("startDate") final Date startDate, @Param("endDate") final Date endDate);*/
    
   /* @Query("SELECT c FROM OrderR c WHERE c.createdOn BETWEEN :startDate AND :endDate and c.merchant.activeCustomerFeedback=:activeCustomerFeedback")
    public List<OrderR> findByStartDateAndEndDateAndMerchantActiveCustomerFeedback(@Param("startDate") final Date startDate,
            @Param("endDate") final Date endDate,
            @Param("activeCustomerFeedback") final Integer activeCustomerFeedback);*/
    @Query("SELECT c FROM OrderR c WHERE c.createdOn BETWEEN :startDate AND :endDate and c.merchant.activeCustomerFeedback=:activeCustomerFeedback GROUP BY customer")
    public List<OrderR> findByStartDateAndEndDateAndMerchantActiveCustomerFeedback(@Param("startDate") final Date startDate,
            @Param("endDate") final Date endDate,
            @Param("activeCustomerFeedback") final Integer activeCustomerFeedback);
   
    @Query("SELECT c FROM OrderR c WHERE c.createdOn BETWEEN :startDate AND :endDate and c.customer.id=:id")
    public List<OrderR> findByStartDateAndEndDateAndCustomerId(@Param("startDate") final Date startDate,
    															@Param("endDate") final Date endDate,
    															 @Param("id") final Integer id);	
    
    
    @Query("SELECT c FROM OrderR c WHERE c.createdOn BETWEEN :startDate AND :endDate and c.customer.id=:id")
    public List<OrderR> findByStartDateAndEndDateAndCustomerId(@Param("startDate") final String startDate,
    															@Param("endDate") final String endDate,
    															 @Param("id") final Integer id);	
    
    
    /**
     * Find by orderType
     * 
     * @param orderType
     * @return List<OrderR>
     */
    public List<OrderR> findByOrderType(String orderType);

    /**
     * Find by OrderStatus
     * 
     * @param status
     * @return List<OrderR>
     */
    public List<OrderR> findByIsDefaults(int status);

    /**
     * Find by orderStatus and OrderType
     * 
     * @param status
     * @param orderType
     * @return List<OrderR>
     */
    public List<OrderR> findByIsDefaultsAndOrderType(int status, String orderType);

    /**
     * Find by customerId
     * 
     * @param customerId
     * @return List<OrderR>
     */
    public List<OrderR> findByCustomerId(int customerId);

    /**
     * Find by merchantId and order type
     * 
     * @param merchantId
     * @param orderType
     * @return Integer
     */
    public Integer countUsersByMerchantIdAndOrderType(Integer merchantId, String orderType);
    
    
    /**
     * Find by customerId and merchantId
     * 
     * @param customerId
     * @param orderId
     * @return List<OrderR> orderRs
     */
    public OrderR findByCustomerIdAndId(Integer customerId, Integer orderId);

    

    @Query(value = "SELECT sum(orderPrice) FROM order_r WHERE merchant_id = :merchantId", nativeQuery = true)
    public double findSum(@Param("merchantId") Integer merchantId);

    public int countUsersByMerchantId(Integer merchantId);

    @Query(value = "SELECT item_id , COUNT(item_id) as value_occurrence from order_r INNER JOIN order_item on order_r.id = order_item.order_id WHERE order_r.merchant_id = :merchantId GROUP BY item_id ORDER BY value_occurrence desc LIMIT 1", nativeQuery = true)
    public List<Object> findByMerId(@Param("merchantId") Integer merchantId);

	//public Page<OrderR> findByMerchantId(Integer merchantId, Pageable pageable);
    @Query("SELECT c FROM OrderR c WHERE c.createdOn BETWEEN :startDate AND :endDate and c.merchant.id=:merchantId")
    public List<OrderR> findByMerchantsId(@Param("startDate") Date startDate1,
                    @Param("endDate") Date endDate1, @Param("merchantId") final Integer merchantId);
    
   
    @Query("SELECT c FROM OrderR c WHERE c.createdOn BETWEEN :startDate AND :endDate and c.customer.id=:customerId")
    public List<OrderR> findByCustomerId(@Param("startDate") Date startDate1,
                    @Param("endDate") Date endDate1, @Param("customerId") final Integer customerId);
  
    
    @Query(value = "SELECT c FROM OrderR c WHERE c.createdOn BETWEEN :startDate AND :endDate and c.isDefaults <> 3 and order_type <> 'Granbury' and merchant_id = :merchantId and c.customer.id is not null ORDER BY c.createdOn desc")
    public Page<OrderR> findByMerchantId(@Param("merchantId") Integer merchantId,@PageableDefault(size = 3)  Pageable pageable,@Param("startDate") Date startDate1,@Param("endDate") Date endDate1);
    
    @Query(value = "SELECT c FROM OrderR c WHERE c.createdOn BETWEEN :startDate AND :endDate and merchant_id = :merchantId  ORDER BY c.createdOn desc")
    public List<OrderR> findByMerchantIdAndDateRange(@Param("merchantId") Integer merchantId,@Param("startDate") Date startDate,@Param("endDate") Date endDate);
    
   /*
    @Query(value = "SELECT c FROM OrderR c WHERE c.createdOn BETWEEN :startDate AND :endDate and merchant_id = :merchantId  ORDER BY c.createdOn desc")
    public Page<OrderR> findByMerchantIdAndDate(@Param("merchantId") Integer merchantId,@PageableDefault(size = 3)  Pageable pageable,@Param("startDate") Date startDate,@Param("endDate") Date endDate);
   
    */
    @Query("SELECT c FROM OrderR c WHERE  c.fulfilled_on BETWEEN :afterAddingTenMins AND :betWeenWithTime  and c.isFutureOrder=:isFutureOrder and c.merchant.id=:merchantId")
    public List<OrderR> findByFulFilledDateAndIsFutureOrderAndMerchantId(
                    @Param("afterAddingTenMins") final Date afterAddingTenMins,
                    @Param("isFutureOrder") final Integer isFutureOrder, @Param("merchantId") final Integer merchantId,
                    @Param("betWeenWithTime") final Date betWeenWithTime);
    
    @Query("SELECT c FROM OrderR c WHERE  c.fulfilled_on BETWEEN :afterAddingTenMins AND :betWeenWithTime  and c.isFutureOrder=:isFutureOrder and c.merchant.id=:merchantId and c.isDefaults= :isDefault")
    public List<OrderR> findByFulFilledDateAndIsFutureOrderAndMerchantIdAndIsDefaults(
                    @Param("afterAddingTenMins") final Date afterAddingTenMins,
                    @Param("isFutureOrder") final Integer isFutureOrder, @Param("merchantId") final Integer merchantId,
                    @Param("betWeenWithTime") final Date betWeenWithTime, @Param("isDefault") Integer isDefaults);

    @Query("SELECT o FROM OrderR o WHERE o.merchant.id=:merchantId and o.customer.firstName like %:searchTxt%")
    public List<OrderR> findByMerchantIdAndCustomerName(@Param("merchantId") final Integer merchantId,
                    @Param("searchTxt") final String searchTxt);
    
    @Query("SELECT o FROM OrderR o WHERE o.merchant.id=:merchantId and o.customer.firstName like %:searchTxt%")
    public Page<OrderR> findByMerchantIdAndCustomerName(@Param("merchantId") final Integer merchantId,
                    @Param("searchTxt") final String searchTxt, Pageable pageable);
    
    @Query("SELECT o FROM OrderR o WHERE o.merchant.id=:merchantId and o.isDefaults <> 3 and o.customer.firstName like %:searchTxt% and o.createdOn BETWEEN :startDate AND :endDate")
    public Page<OrderR> findByMerchantIdAndCustomerNameAndDate(@Param("merchantId") final Integer merchantId,
                    @Param("searchTxt") final String searchTxt, Pageable pageable , @Param("startDate") Date startDate,@Param("endDate") Date endDate);
    
    @Query("SELECT o FROM OrderR o WHERE o.merchant.id=:merchantId and o.customer.firstName like %:searchTxt% and o.createdOn BETWEEN :startDate AND :endDate")
    public List<OrderR> findByMerchantIdAndCustomerNameAndDateRange(@Param("merchantId") final Integer merchantId,
    		@Param("searchTxt") final String searchTxt ,@Param("startDate") Date startDate,@Param("endDate") Date endDate);
    
    /*@Query(value = "SELECT c FROM OrderR c WHERE c.createdOn BETWEEN :startDate AND :endDate and merchant_id = :merchantId  ORDER BY c.createdOn desc")
    public Page<OrderR> findByMerchantId(@Param("merchantId") Integer merchantId,@PageableDefault(size = 3)  Pageable pageable,@Param("startDate") Date startDate1,@Param("endDate") Date endDate1);*/
    
    @Query("SELECT c FROM OrderR c WHERE  c.fulfilled_on > :date  and c.isFutureOrder=:isFutureOrder and c.merchant.id=:merchantId")
    public List<OrderR> findByFulFilledOnAndIsFutureOrderAndMerchantId(
                    @Param("date") final Date date,
                    @Param("isFutureOrder") final Integer isFutureOrder, @Param("merchantId") final Integer merchantId);

    @Query("SELECT c FROM OrderR c WHERE  c.fulfilled_on > :date  and c.isFutureOrder=:isFutureOrder and c.merchant.id=:merchantId and c.isDefaults= :isDefault")
    public List<OrderR> findByFulFilledOnAndIsFutureOrderAndMerchantIdAndIsDefaults(
                    @Param("date") final Date date,
                    @Param("isFutureOrder") final Integer isFutureOrder, @Param("merchantId") final Integer merchantId, @Param("isDefault") Integer isDefaults);


    @Query("SELECT count(c) FROM OrderR c WHERE c.createdOn BETWEEN :startDate AND :endDate and c.merchant.id=:merchantId and c.paymentMethod=:paymentMethod")
	public Integer getCashOrderCount(@Param("startDate") Date startDate1,
            @Param("endDate") Date endDate1, @Param("merchantId") final Integer merchantId, @Param("paymentMethod") final String paymentMethod);
 
    
    @Query("SELECT count(c) FROM OrderR c WHERE c.createdOn BETWEEN :startDate AND :endDate and c.merchant.id=:merchantId and c.orderType=:orderType")
	public Integer getPickupCount(@Param("startDate") Date startDate1,
            @Param("endDate") Date endDate1, @Param("merchantId") final Integer merchantId, @Param("orderType") final String orderType);

	public OrderR findByOrderPosIdAndCustomerId(String orderPosId, Integer id);
	
	@Query("SELECT c FROM OrderR c WHERE c.createdOn BETWEEN :startDate AND :endDate and c.merchant.id= :merchantId and c.orderPrice > :orderPrice")
	public List<OrderR> findByMerchantIdDateRangeAndSpent(@Param("startDate")Date startDate, @Param("endDate")Date endDate,@Param("merchantId")Integer merchantId,@Param("orderPrice")Double orderPrice);

	/*@Query(value = "SELECT *  FROM order_r WHERE merchant_id= :merchantId AND  customer_id NOT IN (:customer)", nativeQuery = true)
	public List<OrderR> findCustomerNonTranscation(@Param("merchantId")Integer merchantId , @Param("customer") String customer);
	
	@Query(value = "SELECT *  FROM order_r WHERE merchant_id= :merchantId AND  customer_id  IN (:customer)", nativeQuery = true)
	public List<OrderR> findCustomerTranscation(@Param("merchantId")Integer merchantId , @Param("customer") String customer);*/
	
	/*@Query(value = "SELECT *  FROM order_r WHERE merchant_id= :merchantId AND customer_id IN (:customer) AND payment_method IS NOT NULL;", nativeQuery = true)
	public List<OrderR> findCustomerNonTranscation(@Param("merchantId")Integer merchantId , @Param("customer") String customer);
	
	@Query(value = "SELECT *  FROM order_r WHERE merchant_id= :merchantId AND customer_id  IN (:customer) AND payment_method IS NULL OR payment_method='';", nativeQuery = true)
	public List<OrderR> findCustomerTranscation(@Param("merchantId")Integer merchantId , @Param("customer") String customer);*/
	
/*	
	@Query(value = "SELECT *  FROM order_r WHERE merchant_id= :merchantId AND customer_id IN (:customer) AND payment_method IS NOT NULL;", nativeQuery = true)
	public List<OrderR> findCustomerNonTranscation(@Param("merchantId")Integer merchantId , @Param("customer") String customer);
	*/
/*	@Query(value = "SELECT *  FROM order_r WHERE merchant_id= :merchantId AND customer_id  IN (:customer) AND payment_method IS NULL OR payment_method='';", nativeQuery = true)
	public List<OrderR> findCustomerTranscation(@Param("merchantId")Integer merchantId , @Param("customer") String customer);

*/
	@Query(value = "SELECT o  FROM OrderR o WHERE o.merchant.id= :merchantId AND  o.customer.id IN (:customer) AND o.paymentMethod IS NULL OR o.paymentMethod=''")
	public List<OrderR> findCustomerTranscation(@Param("merchantId")Integer merchantId , @Param("customer") List<Integer> customer);
	
	@Query(value = "SELECT o  FROM OrderR o WHERE o.merchant.id= :merchantId AND  o.customer.id IN (:customer) AND o.paymentMethod IS NOT NULL")
	public List<OrderR> findCustomerNonTranscation(@Param("merchantId")Integer merchantId , @Param("customer") List<Integer> customer);

	public OrderR findByMerchantIdAndId(Integer merchantId, Integer orderId);
	
	
	
	@Query(value = "SELECT o  FROM OrderR o WHERE o.merchant.id= :merchantId AND  o.customer.id=:customerId  and o.orderPrice >=10")
    public List<OrderR> findByCustomerIdAndMerchantIdWithOrderGraterThan(@Param("merchantId")Integer merchantId , @Param("customerId") Integer customerId);
	
	
	@Query(value= "select * from order_r where isDefaults=4 and is_future_order=1 and fulfilled_on > NOW();", nativeQuery = true)
	public List<OrderR> findFutureOrderFromCurrentDate(); 
	
	@Query(value = "SELECT c FROM OrderR c WHERE c.createdOn BETWEEN :startDate AND :endDate and merchant_id = :merchantId  ORDER BY c.createdOn desc")
    public List<OrderR> findByMerchantIdAndfromDateAndtoDate(@Param("merchantId") Integer merchantId,@Param("startDate") Date startDate,@Param("endDate") Date endDate);


	/*@Query(value= "SELECT * from order_r o LEFT JOIN order_item oi on o.id= oi.order_id LEFT JOIN item i on i.id = oi.item_id LEFT JOIN order_item_modifier oim on oim.order_item_id = oi.id LEFT JOIN modifiers m on m.id= oim.modifier_id where i.`name`=(:todayDayDate)",nativeQuery = true)	
	public List<OrderR> findByTodayDateAndDay(@Param("todayDayDate")String todayDayDate);*/
	
	@Query(value= "SELECT * from order_r  where createdOn BETWEEN :startDate AND :endDate AND merchant_id= :merchantId",nativeQuery = true)
	public List<OrderR> findByMonthStartDateAndMonthEndDateAndMerchantId(@Param("startDate") String startDate, @Param("endDate") String endDate, @Param("merchantId") Integer merchantId);

	
	@Query(value="SELECT o.* from order_r o RIGHT JOIN order_item oi on oi.order_id= o.id left join item i on i.id=oi.item_id and i.`name`= :todayDayDate GROUP BY o.id",nativeQuery= true)
	public List<OrderR> findByTodayDateAndDay(@Param("todayDayDate")String todayDayDate);
	
	
    @Query(value= "SELECT * FROM order_r c WHERE date(c.createdOn)>=:startDate and date(c.createdOn)<=:endDate AND c.merchant_id=:merchantId order by c.createdOn asc",nativeQuery = true)
    public List<OrderR> findAllOrdersFromStartDateAndEndDate(@Param("startDate") Date startDate, @Param("endDate") Date endDate, @Param("merchantId") Integer merchantId);
   
	//@Query(value = "Select avg(o.orderPrice) from OrderR o where o.merchant.id = :merchantId and o.isDefaults = :isDefault and o.createdOn between :fromDate and :toDate ")
	@Query(value = "Select AVG(orderPrice) from order_r where isDefaults=1 and merchant_id = :merchantId and createdOn between :fromDate and :toDate", nativeQuery=true)
	Double avgOrderPriceByMerchantIdAndCreatedDateBetweenAndIsDefault(@Param("merchantId")Integer merchantId, @Param("fromDate")String fromDate, @Param("toDate")String toDate);

	@Query(value = "SELECT sum(c.orderPrice) FROM OrderR c WHERE c.createdOn BETWEEN :startDate AND :endDate and merchant_id = :merchantId  ORDER BY c.createdOn desc")
    public String findSumOfOrderPrice(@Param("merchantId") Integer merchantId,@Param("startDate") Date startDate,@Param("endDate") Date endDate);

	@Query(value = "SELECT count(*) FROM OrderR c WHERE c.createdOn BETWEEN :startDate AND :endDate and merchant_id = :merchantId  ORDER BY c.createdOn desc")
    public String findCountOFOrder(@Param("merchantId") Integer merchantId,@Param("startDate") Date startDate,@Param("endDate") Date endDate);
	
	@Query(value = "SELECT avg(c.orderPrice) FROM OrderR c WHERE c.createdOn BETWEEN :startDate AND :endDate and merchant_id = :merchantId  ORDER BY c.createdOn desc")
    public String findAvgOfOrderPrice (@Param("merchantId") Integer merchantId,@Param("startDate") Date startDate,@Param("endDate") Date endDate);
	
	@Query(value ="SELECT i.name FROM order_r c JOIN order_item o on o.order_id=c.id join item i on i.id=o.item_id where c.merchant_id=:merchantId AND c.createdOn BETWEEN :startDate AND :endDate  GROUP BY c.id limit 5", nativeQuery=true)
	public List<String>getOrdername(@Param("merchantId") Integer merchantId,@Param("startDate") Date startDate,@Param("endDate") Date endDate);
	
	@Query(value ="SELECT COUNT(*) FROM order_r c JOIN order_item o on o.order_id=c.id join item i on i.id=o.item_id where c.merchant_id=:merchantId AND c.createdOn BETWEEN :startDate AND :endDate GROUP BY c.id limit 5", nativeQuery=true)
	public List<Integer>getOrderCount(@Param("merchantId") Integer merchantId,@Param("startDate") Date startDate,@Param("endDate") Date endDate);
	
	@Query(value ="SELECT  SUM(orderPrice) FROM order_r c JOIN order_item o on o.order_id=c.id join item i on i.id=o.item_id where c.merchant_id=:merchantId AND c.createdOn BETWEEN :startDate AND :endDate GROUP BY c.id limit 5", nativeQuery=true)
	public List<Integer>getSumOfOrder(@Param("merchantId") Integer merchantId,@Param("startDate") Date startDate,@Param("endDate") Date endDate);

	@Query(value ="SELECT date(c.createdOn) FROM order_r c JOIN order_item o on o.order_id=c.id join item i on i.id=o.item_id where c.merchant_id=:merchantId AND c.createdOn BETWEEN :startDate AND :endDate GROUP BY c.id limit 5", nativeQuery=true)
	public List<String>getTopMenuDateReport(@Param("merchantId") Integer merchantId,@Param("startDate") Date startDate,@Param("endDate") Date endDate);
	
	@Query(value ="SELECT EXTRACT(HOUR from c.createdOn) FROM order_r c  JOIN order_item o on o.order_id=c.id join item i on i.id=o.item_id where c.merchant_id=:merchantId AND c.createdOn BETWEEN :startDate AND :endDate  GROUP BY c.id limit 5", nativeQuery=true)
	public List<Integer>getTopMenuHourReport(@Param("merchantId") Integer merchantId,@Param("startDate") Date startDate,@Param("endDate") Date endDate);
	
	@Query(value ="SELECT  COUNT(*) FROM order_r c  JOIN order_item o on o.order_id=c.id join item i on i.id=o.item_id where c.merchant_id=:merchantId AND c.createdOn BETWEEN :startDate AND :endDate  GROUP BY c.id limit 5", nativeQuery=true)
	public List<Integer>getTopMenuCountReport(@Param("merchantId") Integer merchantId,@Param("startDate") Date startDate,@Param("endDate") Date endDate);
	
	@Query(value ="SELECT  i.`name` FROM order_r c  JOIN order_item o on o.order_id=c.id join item i on i.id=o.item_id  where c.merchant_id=:merchantId AND c.createdOn BETWEEN :startDate AND :endDate  GROUP BY c.id limit 5", nativeQuery=true)
	public List<String>getTopMenuItemReport(@Param("merchantId") Integer merchantId,@Param("startDate") Date startDate,@Param("endDate") Date endDate);

	 @Query(value = "select SUM(orderPrice) from order_r where id in (:orderList)", nativeQuery=true)
	 Integer getOrderSumByCouponId(@Param("orderList") List<Integer> orderList);
	 
	 
	 
	 @Query("SELECT c FROM OrderR c WHERE c.createdOn BETWEEN :startDate AND :endDate and c.merchant.id=:merchantId and c.isDefaults= :isDefault")
	    public List<OrderR> findOrderhistoryByMerchantsIdsAndIsDefault(@Param("startDate") Date startDate1,  @Param("endDate") Date endDate1, @Param("merchantId") final Integer merchantId,@Param("isDefault") Integer isDefaults);
	    
	 @Query("SELECT count(c) FROM OrderR c WHERE c.createdOn BETWEEN :startDate AND :endDate and c.merchant.id=:merchantId and c.isDefaults=:isDefault and c.orderType=:orderType")
	 public Integer getPickupCountByIsDefault(@Param("startDate") Date startDate1,
	            @Param("endDate") Date endDate1, @Param("merchantId") final Integer merchantId, @Param("isDefault") Integer isDefault, @Param("orderType") final String orderType);
	    
	    
	 @Query(value = "Select * from order_r where isDefaults in(1,0)  and merchant_id = :merchantId", nativeQuery=true)
	 public List<OrderR> findOrderInfoByMerchantId(@Param("merchantId")Integer merchantId);

	 @Query(value = "Select count(*) from order_r where isDefaults in(1,0) and order_type=:orderType  and merchant_id = :merchantId", nativeQuery=true)
	  public Integer countOrderByMerchantIdAndOrderTypeAndIsDefaults(@Param("merchantId")Integer merchantId, @Param("orderType") String orderType);

	  
	  @Query(value= "select * from order_r where cast(createdOn as Date) = :currentDate and is_future_order = 1",nativeQuery = true)
		 public List<OrderR> findOrdersTodayDate(@Param("currentDate") String currentDate);

	  @Query(value= "select merchant_id from order_r where cast(fulfilled_on as Date)>=:currentDate and is_future_order = 1 GROUP BY merchant_id",nativeQuery = true)
		 public List<Integer> findMerchantsTodayDate(@Param("currentDate") String currentDate);
	 
	 @Query(value= "select * from order_r where fulfilled_on> :currentDate AND cast(fulfilled_on as Date)>=:currentDate and is_future_order = 1 and merchant_id=:merchantId and isDefaults in (1,0)",nativeQuery = true)
	 public List<OrderR> findOrdersTodayDateAndMerchent(@Param("currentDate") String currentDate,@Param("merchantId") Integer merchantId);
	 
	 public OrderR findByIdAndMerchantId(Integer id, Integer merchantId);

	 @Query(value= "select * from order_r where cast(createdOn as Date) = :createdDate and isDefaults = 0 and is_future_order = 1",nativeQuery = true)
	 public List<OrderR> findOrdersByCreatedDate(@Param("createdDate") String createdDate);
	 
	 @Query(value= "select * from order_r where cast(fulfilled_on as Date) = :fulfilledOn and isDefaults = 0",nativeQuery = true)
	 public List<OrderR> findOrdersByFullFilleddate(@Param("fulfilledOn") String fulfilledOn);
	 
	 @Query(value= "select * from order_r where cast(createdOn as Date) = :currentDate and isDefaults = 0",nativeQuery = true)
	 public List<OrderR> findOrdersTodayDateAndIsDefault(@Param("currentDate") String currentDate);
	 
	 @Query("select r from OrderR r where r.merchant.owner.pos.posId not IN(3) and r.createdOn BETWEEN :startDate AND :endDate AND r.id = r.orderPosId")
	 public List<OrderR> findOrdersTodayDateAndIsDefault(@Param("startDate") Date startDate,@Param("endDate") Date endDate);

	 OrderR findByUberorderId(String uberOrderId);
	 
	 @Query(value= "select * from order_r where cast(fulfilled_on as Date)>=:fulfilledOn and merchant_id=:merchantId order by createdOn asc",nativeQuery = true)
     List<OrderR> findOrdersByfulfilledDate(@Param("fulfilledOn") String fulfilledOn, @Param("merchantId") Integer merchantId);
	 
	 @Query(value= "select * from order_r o join merchant m on o.merchant_id=m.id where cast(fulfilled_on as Date)>=:fulfilledOn and m.pos_merchant_id=:merchantPosId and o.isDefaults=:isDefault",nativeQuery = true)
	 List<OrderR> findbyMerchantPosIdAndIsdefault(@Param("fulfilledOn") String fulfilledOn, @Param("merchantPosId") String merchantPosId,@Param("isDefault") Integer isDefault);
	
//	 @Query(value= "select * from order_r where isDefaults=:isDefault and merchant_id=:merchantId and createdOn "
//	 		+ "BETWEEN :startDate AND :endDate and virtual_fund_Id is NOT null",nativeQuery = true)
//	 List<OrderR> findOrdersWithFundToSchools(@Param("merchantId") Integer merchantId,@Param("startDate") Date startDate,
//			                        @Param("endDate") Date endDate,@Param("isDefault") Integer isDefault);
	 
	 @Query(value= "select * from order_r where isDefaults=:isDefault and merchant_id=:merchantId and virtual_fund_Id=:virtualFundId "
	 		+ "and fulfilled_on BETWEEN :startDate AND :endDate",nativeQuery = true)
		 List<OrderR> findOrdersWithFundToSchoolsByPartnerId(@Param("merchantId") Integer merchantId,@Param("virtualFundId") Integer virtualFundId,
				 @Param("startDate") Date startDate,@Param("endDate") Date endDate,@Param("isDefault") Integer isDefault);
}
