package com.foodkonnekt.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.foodkonnekt.model.PizzaTemplate;
import com.foodkonnekt.model.PizzaTopping;

public interface PizzaToppingRepository extends JpaRepository<PizzaTopping, Integer> {
	
	public PizzaTopping findByMerchantIdAndPosPizzaToppingId(Integer merchantId, String posPizzaToppingId);

	public Page<PizzaTopping> findByMerchantIdAndActive(Integer merchantId, Pageable pageable, int booleanTrue);

	public List<PizzaTopping> findByMerchantId(Integer merchantId);
	
	
	public List<PizzaTopping> findByDescriptionAndMerchantId(String description ,Integer merchantId);

	@Query("SELECT z FROM PizzaTopping z WHERE z.merchant.id=:merchantId and z.description like %:searchTxt% ")
	public List<PizzaTopping> findByMerchantIdAndDescription(@Param("merchantId") final Integer merchantId,
            @Param("searchTxt") final String searchTxt);
	
	public PizzaTopping findByMerchantIdAndId(Integer merchantId, Integer id);

	public PizzaTopping findByIdAndActive(Integer id,Integer active);
	
	@Query(value= "select p.* from pizzatoppingsize ps join pizzatopping p on ps.pizzatopping_id= p.id where ps.pizzasize_id =:sizeId",nativeQuery = true)
	List<PizzaTopping> findPizzaToppingByPizzaSizeId(@Param(value="sizeId") Integer sizeId);
	
	@Query(value= "SELECT p.* from pizzatopping p where p.merchant_id=:merchantId and p.id NOT IN(select ptt.pizza_topping_id from pizzatemplatetopping ptt where ptt.pizza_template_id=:templateId)",nativeQuery = true)
	List<PizzaTopping> findPizzaTopping(@Param("merchantId")  Integer merchantId,@Param(value="templateId") Integer templateId);
	
	@Query(value="select * from pizzatopping p left join pizzatoppingsize ps on (p.id=ps.pizzatopping_id and ps.pizzasize_id=:sizeId) " + 
			"where ps.pizzatopping_id is NULL and p.merchant_id=:merchantId" , nativeQuery = true)
    List<PizzaTopping> findUnMappedToppingBySizeId(@Param(value="sizeId") Integer sizeId, @Param("merchantId") Integer merchantId);
	
	@Query(value="select * from pizzatopping p left join pizzatoppingsize ps on (p.id=ps.pizzatopping_id and ps.pizzasize_id=:sizeId) " + 
			"where ps.pizzatopping_id is NULL and p.merchant_id=:merchantId and p.active=:active" , nativeQuery = true)
    List<PizzaTopping> findUnMappedToppingBySizeIdAndToppingActive(@Param(value="sizeId") Integer sizeId
    		             , @Param("merchantId") Integer merchantId, @Param("active") Integer active);
	
	@Query(value= "SELECT p.* from pizzatopping p where p.merchant_id=:merchantId and p.active=:active and p.id NOT IN(select ptt.pizza_topping_id from pizzatemplatetopping ptt where ptt.pizza_template_id=:templateId)",nativeQuery = true)
	List<PizzaTopping> findPizzaToppingByActive(@Param("merchantId")  Integer merchantId,@Param(value="templateId") Integer templateId
			, @Param("active")  Integer active);
	
	 
}
