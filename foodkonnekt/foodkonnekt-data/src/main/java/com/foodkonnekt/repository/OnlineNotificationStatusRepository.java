package com.foodkonnekt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.foodkonnekt.model.OnlineOrderNotificationStatus;

@Repository
public interface OnlineNotificationStatusRepository extends JpaRepository<OnlineOrderNotificationStatus, Integer>{
	
	

    @Query(value="SELECT * FROM online_order_notification_status m WHERE m.order_Id=:orderId",nativeQuery = true)
    List<OnlineOrderNotificationStatus>  findByOnlineNotificationStatusListByorderId(@Param("orderId") final Integer orderId);
    
    @Query(value="SELECT * FROM online_order_notification_status m WHERE m.fax_id=:faxId",nativeQuery = true)
    OnlineOrderNotificationStatus  findByFaxId(@Param("faxId") final String faxId);
}