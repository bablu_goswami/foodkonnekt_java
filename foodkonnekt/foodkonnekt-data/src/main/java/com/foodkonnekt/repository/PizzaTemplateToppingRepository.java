package com.foodkonnekt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.foodkonnekt.model.PizzaTemplateTopping;

public interface PizzaTemplateToppingRepository extends JpaRepository<PizzaTemplateTopping, Integer> {

	PizzaTemplateTopping findById(Integer id);
	
	List<PizzaTemplateTopping> findByPizzaTemplateId(Integer id);

	List<PizzaTemplateTopping> findByPizzaToppingIdAndPizzaTemplateId(Integer toppingId,Integer templateId);
	
	PizzaTemplateTopping findByPizzaTemplateIdAndPizzaToppingId(Integer templateId,Integer toppingId);
	
	List<PizzaTemplateTopping> findByPizzaToppingId(Integer id);
	
	@Query("select p from PizzaTemplateTopping p where pizzaTemplate.id=:templateId")
	List<PizzaTemplateTopping> findByPizzaTemplateIds(@Param("templateId")Integer templateId);
	
	@Query(value="select ptm.* from  pizzatemplatetopping ptm LEFT JOIN  pizzatopping p on ptm.pizza_topping_id = p.id LEFT JOIN pizzatoppingsize pts on pts.pizzatopping_id = ptm.pizza_topping_id where p.merchant_id=:merchantId and ptm.pizza_template_id =:templateId and pts.pizzasize_id =:sizeId",nativeQuery =true)
	List<PizzaTemplateTopping> findBySizeIdAndTemplateId(@Param("templateId")Integer templateId , @Param("sizeId")Integer sizeId , @Param("merchantId") Integer merchantId);
	
	PizzaTemplateTopping findByPizzaTemplateIdAndPizzaToppingIdAndActive(Integer templateId,Integer toppingId,Boolean active);
    @Query(value = "SELECT * FROM pizzatemplatetopping WHERE pizza_template_id=:templateId and pizza_topping_id in (:toppingId)", nativeQuery = true)
	List<PizzaTemplateTopping> findByPizzaTemplateIdAndPizzaToppingId(@Param("templateId")Integer templateId,@Param("toppingId")List<Integer> toppingid);

	List<PizzaTemplateTopping> findByPizzaTemplateIdAndPizzaToppingActive(Integer templateId,Integer active);
}
