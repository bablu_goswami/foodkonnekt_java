package com.foodkonnekt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.foodkonnekt.model.CardInfo;

public interface CardInfoRepository extends JpaRepository<CardInfo, Integer>{

	CardInfo findByLast4AndFirst6AndCustomerIdAndStatus(String string, String string2,Integer customerId,boolean status);

	List<CardInfo> findByCustomerIdAndStatus(Integer customerId,boolean status);
	
	List<CardInfo> findByCustomerIdAndMerchantIdAndStatus(Integer customerId,Integer merchantId,boolean status);

	CardInfo findByIdAndLast4AndStatus(Integer vaultedId, String vaultedLastValue,boolean status);

	CardInfo findByCustomerIdAndIdAndStatus(Integer customerId, Integer cardId,boolean status);

}
