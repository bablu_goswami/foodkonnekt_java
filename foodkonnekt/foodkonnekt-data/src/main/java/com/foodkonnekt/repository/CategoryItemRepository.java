package com.foodkonnekt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.foodkonnekt.model.CategoryItem;
import com.foodkonnekt.model.Item;

public interface CategoryItemRepository extends JpaRepository<CategoryItem, Integer> {

    /**
     * Find by itemId and categoryId
     * 
     * @param itemId
     * @param categoryId
     * @return CategoryItem instance
     */
    CategoryItem findByItemIdAndCategoryId(Integer itemId, Integer categoryId);

    /**
     * find by ItemId
     * 
     * @param id
     * @return CategoryItem instance
     */
    List<CategoryItem> findByItemId(Integer id);
    
    @Query("select count(c) from CategoryItem c where c.category.id = ?1 and c.item.itemStatus!= ?2")
    Long categoryItemCountByCategoryIdAndItemStatus(Integer categoryId,Integer itemStatus);

    /**
     * Find by categoryId
     * 
     * @param categoryId
     * @return List<CategoryItem>
     */
    List<CategoryItem> findByCategoryId(Integer categoryId);
    
    List<CategoryItem> findByCategoryIdAndSortOrderAndIdNot(Integer categoryId,Integer sortOrder,Integer id);
    
    List<CategoryItem> findByCategoryIdOrderBySortOrderAsc(int categoryId);
  //  List<CategoryItem> findByItemIdOrderBySortOrder(Integer id);

	List<CategoryItem> findByCategoryIdAndSortOrderNotOrderBySortOrderAsc(Integer id, int booleanFalse);


	/* @Query(value = "SELECT COUNT(c) from CategoryItem c WHERE c.category.id = :categoryId GROUP BY sortOrder ")
	 Long categoryItemCount(Integer categoryId);*/
	 
	  @Query(value = "SELECT COUNT(c) from CategoryItem c WHERE c.category.id = :categoryId and c.sortOrder <> 0 GROUP BY sortOrder")
	  List<Long> categoryItemCount(@Param("categoryId") Integer categoryId);
	  
	  @Query(value="SELECT COUNT(*) from item_category c join item_taxes t on c.item_id=t.item_id "
	  		+ "WHERE category_id =:categoryId ",nativeQuery=true)
	  Integer findItemCountByCategoryId(@Param("categoryId") Integer categoryId);
	  
	  @Query(value="select i.id from item i where merchant_id = :merchantId and id not IN (select item_id from item_category where category_id= :categoryId)" ,nativeQuery = true)
	  List<Integer> getItemByMerchantId(@Param("categoryId") Integer categoryId , @Param("merchantId") Integer merchantId);
	  
	  @Query(value="SELECT COUNT(item_id) from item_category c join item_taxes t on c.item_id=t.item_id "
		  		+ "WHERE c.category_id =:categoryId and c.active=:active",nativeQuery=true)
	  Integer findItemCountByCategoryIdAndActive(@Param("categoryId") Integer categoryId ,@Param("active") Integer active);
	  
	  List<CategoryItem> findByItemIdAndActiveAndCategoryItemStatus(Integer id ,Integer active,Integer categoryActive);
	  
	  List<CategoryItem> findByCategoryIdAndActiveAndItemItemStatus(Integer categoryId,Integer active,Integer itemActive);
	  
	  @Query(value = "select * from item_category ic join item i on i.id=ic.item_id join item_taxes it on " + 
		  		"i.id=it.item_id where ic.category_id=:categoryId and ic.active=1 and i.item_status=0 and it.active=1 GROUP BY ic.id",nativeQuery = true)
	  List<CategoryItem> findByCategoryIdAndActiveAnditemStatustaxActive(@Param("categoryId") Integer categoryId);

}
