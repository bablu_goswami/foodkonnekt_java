package com.foodkonnekt.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.foodkonnekt.model.CustomerFeedback;
import com.foodkonnekt.model.CustomerFeedbackAnswer;

public interface CustomerFeedbackAnswerRepository extends JpaRepository<CustomerFeedbackAnswer, Integer> {

	
	@Query("SELECT c   FROM  CustomerFeedbackAnswer c WHERE c.customerFeedback.id=:customerFeedbackId ")
	List<CustomerFeedbackAnswer> findCustomerFeedbackInfo(@Param("customerFeedbackId")Integer customerFeedbackId);

	@Query("SELECT c.answer  FROM  CustomerFeedbackAnswer c WHERE c.customerFeedback.id=:customerFeedbackId ")
	List<Integer> findByCustomerFeedbackId(Integer id);

	//List<Integer> findByCustomerFeedbackId(Integer id);
	@Query(value = "select sum((cfa.customer_service+cfa.food_quality+cfa.order_experience))/(count(*)*3)  as " + 
	 		"AVG_RATING from customer_feedback_answer cfa left join customer_feedback cf on cf.id=cfa.customer_feedback_id " + 
	 		"where  cfa.id =:id",nativeQuery = true)
	Double findIndividualCustomersAverageRating(@Param("id") Integer customerFeedbackAnswerId);
		
	@Query(value="select c.*,cf.* from customer_feedback_answer c JOIN customer_feedback cf on c.customer_feedback_id=cf.id where c.customer_feedback_id IN (:customerFeedbackIds)",nativeQuery = true)
	 List<CustomerFeedbackAnswer> findByCustomerFeedbackIds(@Param("customerFeedbackIds") List<Integer> customerFeedbackIds);
}
