package com.foodkonnekt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.foodkonnekt.model.DeliveryOpeningClosingTime;

public interface DeliveryOpeningClosingTimeRepository extends JpaRepository<DeliveryOpeningClosingTime, Integer>{

	 List<DeliveryOpeningClosingTime> findByDeliveryOpeningClosingDayId(Integer openingClosingDayId);
}
