package com.foodkonnekt.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.foodkonnekt.model.OrderPrintStatus;

public interface OrderPrintStatusRepository extends JpaRepository<OrderPrintStatus, Integer>{

	OrderPrintStatus findByOrderId(Integer orderPosId);
}