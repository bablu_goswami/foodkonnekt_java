package com.foodkonnekt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.foodkonnekt.model.OrderDiscount;

public interface OrderDiscountRepository extends JpaRepository<OrderDiscount, Integer> {

	List<OrderDiscount> findByCustomerIdAndCouponCode(Integer customerId,String couponCode);
	
	List<OrderDiscount> findByOrderId(Integer orderId);
	
	@Query(value="SELECT od.coupon_code from order_discount od RIGHT JOIN order_r o on o.id=od.order_id where od.coupon_code is not null and o.merchant_id= :merchantId and o.createdOn BETWEEN :fromDate and :toDate GROUP BY od.coupon_code ORDER BY SUM(o.orderPrice) DESC", nativeQuery=true)
	List<String> findOfferNameWithHighestRevenue(@Param("merchantId") Integer merchantId,@Param("fromDate") String fromDate,@Param("toDate") String toDate);
	
	@Query(value="SELECT SUM(o.orderPrice) from order_discount od RIGHT JOIN order_r o on o.id=od.order_id where od.coupon_code is not null and o.merchant_id= :merchantId and o.createdOn BETWEEN :fromDate and :toDate GROUP BY od.coupon_code ORDER BY SUM(o.orderPrice) DESC", nativeQuery=true)
	List<Double> findOfferValueWithHighestRevenue(@Param("merchantId") Integer merchantId,@Param("fromDate") String fromDate,@Param("toDate") String toDate);
}	
