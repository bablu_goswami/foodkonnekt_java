package com.foodkonnekt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.foodkonnekt.model.PizzaCrust;
import com.foodkonnekt.model.PizzaTemplate;
import com.foodkonnekt.model.PizzaTemplateCrust;

public interface PizzaTemplateCrustRepository extends JpaRepository<PizzaTemplateCrust, Integer>{
    List<PizzaTemplateCrust> findByPizzaTemplateId(Integer id);
    
    PizzaTemplateCrust findByPizzaTemplateIdAndPizzaCrustId(Integer templateId, Integer crustId);
    
    @Query(value="SELECT * FROM pizzatemplatecrust WHERE pizzatemplatecrust.pizzacrust_id IN :crust_id", nativeQuery = true)
    List<PizzaTemplateCrust> findByPizzaCrustId(@Param(value="crust_id") List<PizzaCrust> crust_id);

    List<PizzaTemplateCrust> findByPizzaCrustId(Integer id);
    
    List<PizzaTemplateCrust> findByPizzaCrustIdAndPizzaTemplateActive(Integer id,Integer active);
    
    List<PizzaTemplateCrust> findByPizzaTemplateIdAndActive(Integer id ,Integer active);
    
    @Query(value="SELECT * FROM pizzatemplatecrust WHERE active=:active and pizzatemplatecrust.pizzacrust_id IN :crust_id", nativeQuery = true)
    List<PizzaTemplateCrust> findByPizzaCrustIdAndActive(@Param(value="active") Integer active
    		, @Param(value="crust_id") List<PizzaCrust> crust_id);
    

}
