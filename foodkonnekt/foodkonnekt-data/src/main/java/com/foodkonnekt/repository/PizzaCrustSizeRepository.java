package com.foodkonnekt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.foodkonnekt.model.PizzaCrustSizes;
import com.foodkonnekt.model.PizzaToppingSize;

public interface PizzaCrustSizeRepository extends JpaRepository<PizzaCrustSizes, Integer>{
	
    List<PizzaCrustSizes> findByPizzaSizeIdAndPizzaCrustIdAndActive(int pizzaSizeId,Integer id,Integer active);
    PizzaCrustSizes findByPizzaCrustIdAndPizzaSizeId(Integer id,Integer pizzaSizeId);
    List<PizzaCrustSizes> findByPizzaCrustId(Integer pizzaCrustId);
    
    List<PizzaCrustSizes> findByPizzaSizeId(int pizzaSizeId);
    
    List<PizzaCrustSizes> findByPizzaCrustIdAndPizzaSizeActive(Integer pizzaCrustId,Integer active);
    
    List<PizzaCrustSizes> findByPizzaSizeIdAndPizzaCrustActive(int pizzaSizeId,int active);
    
    @Query(value = "select * from pizza_crust_sizes ps join pizzatemplatecrust pc on ps.pizzacrust_id=pc.pizzacrust_id join "
    		+ "pizzacrust p on p.id=pc.pizzacrust_id where pc.pizzatemplate_id=:templateId and ps.pizzasize_id=:pizzaSizeId and p.active=1 and ps.active=1 and pc.active=1",nativeQuery = true)
    List<PizzaCrustSizes> findByPizzaCrustIdAndActiveAndPizzaSizeActive(@Param("pizzaSizeId") Integer pizzaSizeId,@Param("templateId") Integer templateId);
}
