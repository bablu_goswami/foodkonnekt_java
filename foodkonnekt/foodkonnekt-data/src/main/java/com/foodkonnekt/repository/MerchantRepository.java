package com.foodkonnekt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.foodkonnekt.model.Merchant;

public interface MerchantRepository extends JpaRepository<Merchant, Integer> {

    public Merchant findByPosMerchantId(String posMerchantId);
    
    public Merchant findByStoreId(String storeId);
    
    public List<Merchant>  findByFBTabIds(String pageId);
    
    
    
    public Merchant findById(Integer id);
    
    public List<Merchant> findByOwnerIdAndIsInstallNot(Integer ownerId,Integer isInstall);
    public List<Merchant> findByOwnerId(Integer id);
    
    public List<Merchant> findByActiveCustomerFeedback(Integer activeCustomerFeedback);
    
    public List<Merchant> findByOwnerIdAndActiveCustomerFeedbackAndIsInstallNot(Integer ownerId,Integer activeCustomerFeedback,Integer isInstall);
    

    /**
     * Find vendor id by merchantId
     * 
     * @param merchantId
     * @return vendorId
     */
    @Query("SELECT m.owner.id FROM Merchant m WHERE m.id=:merchantId")
    public Integer findOwnerByMerchantId(@Param("merchantId") final Integer merchantId);

    /**
     * Find by owner id
     * 
     * @param vendorId
     * @return
     */
    @Query("SELECT m FROM Merchant m WHERE m.owner.id=:vendorId")
    public Merchant findOwnerId(@Param("vendorId") final Integer vendorId);

	public Merchant findByMerchantUid(String merchantUid);

	public Merchant findByMerchantUidAndPosMerchantId(String merchantUid,String posMerchantId);
	
	 @Query(value="select * from merchant where is_install=1",nativeQuery = true)
	 public List<Merchant>  findMerchantByIsInstall();
	
	 @Query(value="select * from merchant m join uber_store u on m.id=u.merchant_id where u.store_id=:storeId",nativeQuery = true)
     Merchant findByUberStoreId(@Param("storeId") String storeId);
	 
	 @Query(value="select * from merchant m join virtual_fund v on m.id=v.merchant_id where m.is_install=1 and v.merchant_id is NOT null ",nativeQuery = true)
	 public List<Merchant>  findMerchantHavingFundRaiserPartner();
	 
}
