package com.foodkonnekt.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.foodkonnekt.model.Category;

public interface CategoryRepository extends JpaRepository<Category, Integer> {

    List<Category> findByMerchantId(Integer merchantId);
    
    List<Category> findByMerchantIdOrderBySortOrderAsc(Integer merchantId);
    
    /*@Query(value="select c.* from category c left join item_category ic on c.id=ic.category_id left join item_taxes it on it.item_id=ic.item_id left join item i on i.id=it.item_id where c.merchant_id=:merchantId and c.item_status=0 and  i.item_status=0 GROUP BY c.id ORDER BY c.sortOrder ASC", nativeQuery = true)
    	List<Category> findByMerchantIdOrderBySortOrderAscByQuery(@Param("merchantId") Integer merchantId);*/
    
    
    @Query(value="select * from category c  left join item_category ic on c.id=ic.category_id left join item_taxes it on it.item_id=ic.item_id left join item_timing itm on itm.item_id=it.item_id left join category_timing ct on ct.category_id=c.id "
    		+ "   left join item i on i.id=it.item_id where c.merchant_id=:merchantId and c.item_status=0 and  i.item_status=0 and "
    		+ "   (( c.allow_category_timing=0) or (c.allow_category_timing=1 and ct.isHoliday=0 and ct.`day`=:day and  ct.start_time<=:startTime and  ct.end_time>=:endTime))"
    		+ "   and ((i.allow_item_timing=0) or (i.allow_item_timing=1 and itm.isHoliday=0 and itm.`day`=:day and itm.start_time<=:startTime and itm.end_time>=:endTime)) "
    		+ "   GROUP BY c.id ORDER BY c.sortOrder ASC", nativeQuery = true)
	List<Category> findByMerchantIdOrderBySortOrderAscByQuery(@Param("merchantId") Integer merchantId,@Param("day")String day,@Param("startTime")String startTime,@Param("endTime")String endTime);
    
    @Query(value="select * from category c where c.merchant_id=:merchantId and c.isPizza=1 GROUP BY c.id ORDER BY c.sortOrder ASC", nativeQuery = true)
	List<Category> findByMerchantIdAndIsPizza(@Param("merchantId") Integer merchantId);
    
    /*List<Category> findByMerchantIdOrderBySortOrderAscByQuery(Integer merchantId);*/
    
 /*   @Query("select c from Category c where c.merchant.id = ?1 Order By c.sortOrder ASC LIMIT ?2,5")
    List<Category> findByMerchantIdOrderBySortOrderAscWithLimit(Integer merchantId,Integer page);*/
    
    List<Category> findByMerchantIdAndSortOrderAndIdNot(Integer merchantId,Integer sortOrder ,Integer categoryId);
    
    List<Category> findByMerchantIdAndSortOrderNotOrderBySortOrderAsc(Integer merchantId,Integer sortOrder);
    
    

    public Category findByMerchantIdAndPosCategoryId(Integer merchantId, String posCategoryId);

    @Query("select count(c) from Category c where c.merchant.id = ?1")
    Long categoryCountByMerchantId(Integer merchantId);

    @Query("SELECT c FROM Category c WHERE c.merchant.id=:merchantId")
    Page<Category> findByMerchantId(@Param("merchantId") Integer merchantId, Pageable pageable);
    
    @Query("SELECT c FROM Category c WHERE c.merchant.id=:merchantId")
    Page<Category> findByMerchantIdAndI(@Param("merchantId") Integer merchantId, Pageable pageable);
    
    Page<Category> findByMerchantIdAndItemStatusNot(Integer merchantId, Pageable pageable,Integer itemStatus);
    
    @Query("SELECT c FROM Category c WHERE c.merchant.id=:merchantId")
    Page<Category> findByMerchantIdAndItemStatusOrItemStatus(Integer merchantId, Pageable pageable,Integer itemStatus1,Integer itemStatus2);
    
    @Query("SELECT c FROM Category c WHERE c.merchant.id=:merchantId and c.itemStatus <>:itemStatus order by c.itemStatus,c.sortOrder,c.id")
    Page<Category> findByMerchantIdAndItemStatus(@Param("merchantId")Integer merchantId, Pageable pageable,@Param("itemStatus")Integer itemStatus);
    

    @Query("SELECT c FROM Category c WHERE c.merchant.id=:merchantId and c.name like %:searchTxt% ")
    List<Category> findByMerchantIdAndCategoryName(@Param("merchantId") final Integer merchantId,
                    @Param("searchTxt") final String searchTxt);
    
    List<Category> findByMerchantIdAndName(Integer merchantId,String name);


	List<Category> findByMerchantOwnerVendorUid(String vendorUId);

	List<Category> findByMerchantMerchantUidAndIsPizza(String merchantUId,Integer isPizza);

	Page<Category> findByMerchantIdOrderBySortOrderAsc(Integer merchantId,
			Pageable topTen);
	
	 @Query(value="select * from category c left join category_timing ct on ct.category_id=c.id "
	    		+ "   where c.item_status=0 and c.merchant_id=:merchantId and c.isPizza=1 and "
	    		+ "   (( c.allow_category_timing=0) or (c.allow_category_timing=1 and ct.isHoliday=0 and ct.`day`=:day and  ct.start_time<=:startTime and  ct.end_time>=:endTime))"
	    		+ "   GROUP BY c.id ORDER BY c.sortOrder ASC", nativeQuery = true)
		List<Category> findByMerchantIdAndIsPizza(@Param("merchantId") Integer merchantId,@Param("day")String day,@Param("startTime")String startTime,@Param("endTime")String endTime);


	 @Query(value="select * from category c  left join item_category ic on c.id=ic.category_id left join item_taxes it on it.item_id=ic.item_id left join item_timing itm on itm.item_id=it.item_id left join category_timing ct on ct.category_id=c.id "
	    		+ "   left join item i on i.id=it.item_id where c.merchant_id=:merchantId and c.item_status=0 and  i.item_status=0 and (c.isPizza=0 or c.isPizza IS NULL) and "
	    		+ "   (( c.allow_category_timing=0) or (c.allow_category_timing=1 and ct.isHoliday=0 and ct.`day`=:day and  ct.start_time<=:startTime and  ct.end_time>=:endTime))"
	    		+ "   and ((i.allow_item_timing=0) or (i.allow_item_timing=1 and itm.isHoliday=0 and itm.`day`=:day and itm.start_time<=:startTime and itm.end_time>=:endTime)) "
	    		+ "   GROUP BY c.id ORDER BY c.sortOrder ASC", nativeQuery = true)
		List<Category> findByMerchantIdOrderBySortOrderAscByQueryV2(@Param("merchantId") Integer merchantId,@Param("day")String day,@Param("startTime")String startTime,@Param("endTime")String endTime);

	   @Query(value="select * from category c where c.merchant_id=:merchantId and c.isPizza=0", nativeQuery = true)
	 	List<Category> findByMerchantIdAndisPizza(@Param("merchantId") Integer merchantId);

	   
	   List<Category> findByIsPizzaAndMerchantId(Integer isPizza , Integer merchantId);
	   
	   List<Category> findByMerchantIdAndIsPizzaAndItemStatus(Integer merchantId ,Integer isPizza,Integer status);
	   
	   List<Category> findByMerchantIdAndItemStatus(Integer merchantId,Integer status);
}
