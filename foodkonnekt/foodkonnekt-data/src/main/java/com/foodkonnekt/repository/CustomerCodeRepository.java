package com.foodkonnekt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.foodkonnekt.model.CustomerCode;
import com.foodkonnekt.model.CustomerFeedback;

public interface CustomerCodeRepository extends JpaRepository<CustomerCode, Integer> {

	List<CustomerCode> findByCustomerId(Integer customerId);
	
	List<CustomerCode> findByFundCodeAndCustomerId(String code,Integer customerId);
	
	@Modifying
    @Transactional
    @Query("delete from CustomerCode c where c.fundCode=:fundCode and c.customer.id=:customerId")
    Integer deleteCodeByCustomerId(@Param("fundCode") String fundCode, @Param("customerId") Integer customerId);
	
	 @Query(value="select * from customer_code_mapping c join virtual_fund v on c.fund_code=v.code where c.customer_id=:customerId "
	 		+ "and v.merchant_id=:merchantId and v.status=:status and :today BETWEEN v.startDate AND v.endDate",nativeQuery = true)
	 List<CustomerCode> findByCustomerIdAndMerchantIdAndDate(@Param("customerId") Integer customerId,
			 @Param("merchantId") Integer merchantId,@Param("today") String today,@Param("status") boolean status);
}
