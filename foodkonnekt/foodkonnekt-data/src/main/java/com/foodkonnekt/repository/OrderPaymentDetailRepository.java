package com.foodkonnekt.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.foodkonnekt.model.OrderPaymentDetail;

public interface OrderPaymentDetailRepository extends JpaRepository<OrderPaymentDetail, Integer> {

	OrderPaymentDetail findByOrderId(Integer orderId);

}
