package com.foodkonnekt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.foodkonnekt.model.MerchantSubscriptionLevel;

public interface MerchantSubscriptionLevelRepository extends JpaRepository<MerchantSubscriptionLevel, String> {
	
	public MerchantSubscriptionLevel findByMerchantIdAndActive(Integer merchantId,Integer active);
	
	public List<MerchantSubscriptionLevel> findByMerchantId(Integer merchantId);

}
