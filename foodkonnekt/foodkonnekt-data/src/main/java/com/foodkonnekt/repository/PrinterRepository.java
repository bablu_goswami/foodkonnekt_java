package com.foodkonnekt.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.foodkonnekt.model.Printer;


public interface PrinterRepository  extends JpaRepository<Printer, Integer>{
   
	 public Printer findById(Integer id);
	 
	 public Printer findByMerchantId(Integer merchantId);
	  
	 @Query(value="select p.refresh_token from merchant_printer p where p.merchant_id= ?1", nativeQuery = true)
	 public String findRefreshTokenByMerchantId(Integer merchantId);
	 
}
