package com.foodkonnekt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.foodkonnekt.model.NotificationMethod;

public interface NotificationMethodRepository extends JpaRepository< NotificationMethod, Integer> {
	
	List<NotificationMethod> findByMerchantId(Integer merchantId);
	
	NotificationMethod findById(Integer id);
	
	NotificationMethod findByMerchantIdAndContact(Integer merchantId,String contact);
	
	/*@Query("delete from NotificationMethod n where n.id=?1")
	NotificationMethod deleteNotificationMethod(Integer id);*/
	
	@Query("select n from NotificationMethod n where n.merchant.id=:merchantId AND n.active=true")
	List<NotificationMethod> findByMerchantIdAndIsActive(@Param("merchantId")Integer merchantId);
	
    @Query("select n from NotificationMethod n where n.merchant.id=:merchantId AND n.notificationMethodType.id=:methodTypeId")
	List<NotificationMethod> findByMerchantIdAndMethodTypeId(@Param("merchantId")Integer merchantId,@Param("methodTypeId")Integer methodTypeId);
    
    @Query("select n from NotificationMethod n where n.merchant.id=:merchantId AND n.notificationMethodType.id=:methodTypeId "
    		                  + "AND n.active=:active")
   	List<NotificationMethod> findByMerchantIdAndMethodTypeIdAndActive(@Param("merchantId")Integer merchantId,
   			          @Param("methodTypeId")Integer methodTypeId,@Param("active")boolean active);
}
