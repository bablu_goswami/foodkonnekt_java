package com.foodkonnekt.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.foodkonnekt.model.GranburySolution;

public interface GranburySolutionRepository  extends JpaRepository<GranburySolution, Integer>{
	


}
