package com.foodkonnekt.repository;

import java.math.BigInteger;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.foodkonnekt.model.PosIntegration;

public interface PosIntegrationRepository extends JpaRepository<PosIntegration, Integer>{
	
	PosIntegration findByMerchantId(int merchantId);
	
	 @Query(value="select count(*) from pos_integration p  where p.app_version=:appVersion", nativeQuery = true)
	// List<Integer> findappVersion();
     public BigInteger findappVersion(@Param("appVersion") String appVersion);

	 @Query(value="SELECT d.app_version FROM pos_integration d",nativeQuery=true)
	    public List<String> findByAppdatedversion();

	




	
	
}
