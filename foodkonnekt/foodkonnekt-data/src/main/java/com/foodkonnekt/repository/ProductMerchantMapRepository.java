package com.foodkonnekt.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.foodkonnekt.model.ProductMerchantMap;

public interface ProductMerchantMapRepository extends JpaRepository<ProductMerchantMap, Integer>{
	
		ProductMerchantMap findByMerchantIdAndProductId(Integer id,Integer productId);
}
