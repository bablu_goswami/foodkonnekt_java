package com.foodkonnekt.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.foodkonnekt.model.Modifiers;

public interface ModifiersRepository extends JpaRepository<Modifiers, Integer> {

    /**
     * Find modifiers by modifierGroupId
     * 
     * @param modifierGroupId
     * @return List<Modifiers>
     */
    public List<Modifiers> findByModifierGroupId(Integer modifierGroupId);

    /**
     * Find by PosModifierIdAndMerchantId
     * 
     * @param posModifierId
     * @param merchantId
     * @return Modifiers
     */
    public Modifiers findByPosModifierIdAndMerchantId(String posModifierId, Integer merchantId);
    
    public Modifiers findByIdAndMerchantId(Integer id, Integer merchantId);

    /**
     * Find by PosModifierId
     * 
     * @param id
     * @return Modifiers
     */
    public Modifiers findByPosModifierId(String posModifierId);

    /**
     * Find by merchantId
     * 
     * @param merchantId
     * @return List<Modifiers>
     */
    public List<Modifiers> findByMerchantId(int merchantId);

    /**
     * Find modifier count by merchantId
     * 
     * @param merchantId
     * @return Long
     */
    @Query("select count(m) from Modifiers m where m.merchant.id = ?1")
    public Long modifierCountByMerchantId(Integer merchantId);
     
    
     public Page<Modifiers> findByMerchantId(Integer merchantId, Pageable pageable);
     
     
     

    @Query("SELECT m FROM Modifiers m WHERE m.merchant.id=:merchantId and m.name like %:searchTxt% ")
    public List<Modifiers> findByMerchantIdAndModifierName(@Param("merchantId") final Integer merchantId,
                    @Param("searchTxt") final String searchTxt);
    
    public List<Modifiers> findByMerchantIdAndName(Integer merchantId,String name);
	 
@Query(value="select * from  modifiers m left join modifiergroup_modifiers mgm on (m.id=mgm.modifiers_id and "
		+ "mgm.modifiergroup_id=:modifierGroupId ) where mgm.modifiers_id is NULL and m.merchant_id=:merchantId",nativeQuery = true)
	 public List<Modifiers> findByMerchantIdAndModifierGroupId(@Param("merchantId") final Integer merchantId ,
			 @Param("modifierGroupId") final Integer modifierGroupId);


@Query(value="select * from modifiers where id IN(SELECT modifiers_id FROM modifiergroup_modifiers WHERE modifiergroup_id =:modifierGroupId and status=1) and status=1",nativeQuery = true) 
public List<Modifiers> findAllBymodifiergroupid(@Param("modifierGroupId") final Integer modifierGroupId);

 @Query(value="select * from  modifiers m left join modifiergroup_modifiers mgm on (m.id=mgm.modifiers_id and "
	 +"mgm.modifiergroup_id=:modifierGroupId ) where mgm.modifiers_id is NULL and m.merchant_id=:merchantId and m.status=:status",nativeQuery = true)
	 public List<Modifiers> findByMerchantIdAndModifierGroupIdAndStatus(@Param("merchantId") final Integer merchantId ,
			 @Param("modifierGroupId") final Integer modifierGroupId,@Param("status") final Integer status);
 
 public Page<Modifiers> findByMerchantIdAndNameContaining(Integer merchantId,String name, Pageable pageable);
 
 @Query(value="SELECT * FROM modifiers where merchant_id=:merchantId GROUP BY pos_modifier_id HAVING COUNT(*) > 1 "
	 		+ "and pos_modifier_id!='' and pos_modifier_id is not NULL",nativeQuery = true)
	 List<Modifiers> findDuplicatePosModifiers(@Param("merchantId") final Integer merchantId);
	 
	 @Modifying
	 @Transactional
	 @Query(value=" DELETE FROM modifiers WHERE id=:modifiersId",nativeQuery = true )
	 void deleteDuplicatePosModifierId(@Param("modifiersId") Integer modifiersId);
	 
	 @Query("SELECT m FROM Modifiers m WHERE m.merchant.id=:merchantId and m.posModifierId=:posModifierId")
	 public List<Modifiers> findListByPosModifierIdAndMerchantId(@Param("posModifierId") String posModifierId,@Param("merchantId") Integer merchantId);
}
