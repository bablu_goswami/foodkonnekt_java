package com.foodkonnekt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.foodkonnekt.model.PizzaSize;

public interface PizzaSizeRepository extends JpaRepository<PizzaSize, Integer> {

	PizzaSize findByPosPizzaSizeIdAndMerchantId(String posPizzaSizeId,Integer merchantId);

	List<PizzaSize> findByMerchantId(Integer id);
	public List<PizzaSize> findByMerchantIdAndDescription(Integer merchantId, String name);
	
	PizzaSize findById(Integer id);
	
	public List<PizzaSize> findByMerchantIdAndDescriptionIgnoreCase(Integer merchantId, String name);
	
	@Query(value="SELECT * FROM pizzasize WHERE id NOT IN (:pizzasizeid) and merchant_id=:merchantId",nativeQuery=true)
	List<PizzaSize> findByMerchantIdAndId(@Param("merchantId")Integer merchantId , @Param("pizzasizeid") List<Integer> pizzasizeid);
	
	@Query(value="SELECT * FROM pizzasize WHERE id IN (:pizzasizeid) and merchant_id=:merchantId",nativeQuery=true)
	List<PizzaSize> findByIdAndMerchantId (@Param("pizzasizeid") List<Integer> pizzasizeid , @Param("merchantId")Integer merchantId );
	
	List<PizzaSize> findByMerchantIdAndActive(Integer id,Integer active);
	
}
