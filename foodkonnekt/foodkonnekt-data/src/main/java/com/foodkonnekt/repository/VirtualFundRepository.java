package com.foodkonnekt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.foodkonnekt.model.OrderR;
import com.foodkonnekt.model.VirtualFund;

public interface VirtualFundRepository extends JpaRepository<VirtualFund, Integer> {

	List<VirtualFund> findByMerchantId(Integer merchantId);
	
	List<VirtualFund> findByPartnerEmailIdAndMerchantId(String emailId,Integer merchantId);
	
	//VirtualFund findByMerchantIdAndCodeAndStatus(Integer merchantId,String code,boolean status);
	
	@Query(value = "select * from virtual_fund v where v.merchant_id=:merchantId and v.status=:status "
			+ "and :date BETWEEN v.startDate AND v.endDate and v.code LIKE :code%", nativeQuery = true)
	List<VirtualFund> findByMerchantIdAndCodeAndStatus(@Param("merchantId") Integer merchantId,
			       @Param("status")boolean status,@Param("code") String code,@Param("date")String date);
	
	VirtualFund findByCode(String code);
	
	VirtualFund findByCodeAndMerchantId(String code,Integer merchantId);
	
//	@Query(value = "select * from virtual_fund where partner_emailId=:emailId and merchant_id=:merchantId and id!=:id" ,nativeQuery = true)
//	List<VirtualFund> findByPartnerEmailIdAndMerchantIdAndIdNot(@Param("emailId")String emailId, 
//			                                    @Param("merchantId")Integer merchantId, @Param("id") Integer id);
	
	List<VirtualFund> findByPartnerEmailIdAndMerchantIdAndIdNot(String emailId,Integer merchantId,Integer id);
	
	VirtualFund findByCodeAndIdNot(String code,Integer id);
	
	@Query(value = "select count(*) from virtual_fund v where v.merchant_id=:merchantId and v.status=:status "
			+ "and :date BETWEEN v.startDate AND v.endDate", nativeQuery = true)
	Integer findByMerchantIdAndStatusAndDate(@Param("merchantId") Integer merchantId,
			       @Param("status")boolean status,@Param("date")String date);
}