package com.foodkonnekt.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.foodkonnekt.model.OrderItem;

public interface OrderItemRepository extends JpaRepository<OrderItem, Integer> {

    /**
     * Find by order id
     * 
     * @param orderId
     * @return
     */
    List<OrderItem> findByOrderId(Integer orderId);
    
    List<OrderItem> findByOrderIdAndItemItemStatus(Integer orderId,Integer itemStatus);

    /**
     * Find by ItemId
     * 
     * @param id
     * @return List<OrderItem>
     */
    List<OrderItem> findByItemId(Integer itemId);

    @Query("SELECT DISTINCT orderItem.item.id FROM OrderItem orderItem where orderItem.order.id=:orderId")
    List<Integer> findDistinctItemIdByOrderId(@Param("orderId") Integer orderId);
    
    @Query(value="select i.`name` from order_item oi LEFT JOIN item i on i.id=oi.item_id left JOIN order_r o on o.id=oi.order_id and o.merchant_id=:merchantId and o.createdOn between :fromDate and :toDate GROUP BY oi.item_id ORDER BY count(oi.item_id) DESC LIMIT 1", nativeQuery=true)
    String findMostPopularOrderItem(@Param("merchantId") Integer merchantId,@Param("fromDate") String fromDate,@Param("toDate") String toDate);
    
    @Query(value= "select it.`name`  from order_item i LEFT join order_r o on  o.id = i.order_id JOIN item it on i.id = it.id where o.createdOn BETWEEN :startDate AND :endDate AND o.merchant_id=:merchant_id GROUP BY o.id order by count(o.id) desc LIMIT 3;",nativeQuery = true)
	List<String> getTop3ItemReport(@Param("merchant_id")Integer merchant_id,  @Param("startDate")Date startDate,  @Param("endDate")Date endDate);
}
