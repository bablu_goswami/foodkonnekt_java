package com.foodkonnekt.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.foodkonnekt.model.PizzaTemplate;
import com.foodkonnekt.model.PizzaTemplateSize;
import com.foodkonnekt.model.PizzaTopping;

public interface PizzaTemplateRepository extends JpaRepository<PizzaTemplate, Integer>  {
	
	public PizzaTemplate findByMerchantIdAndPosPizzaTemplateId(Integer merchantId, String posPizzaToppingId);
	
	Page<PizzaTemplate> findByMerchantIdAndActive(Integer merchantId, Pageable pageable, int active);

	List<PizzaTemplate> findByMerchantId(Integer merchantId);
	
	public List<PizzaTemplate> findByDescriptionAndMerchantId(String description ,Integer merchantId);
	@Query("SELECT z FROM PizzaTemplate z WHERE z.merchant.id=:merchantId and z.description like %:searchTxt%")
		//@Query("SELECT z FROM PizzaTemplate z WHERE z.merchant.id=:merchantId and z.description=:searchTxt")
		List<PizzaTemplate> findByMerchantIdAndDescriptionByQuery(@Param("merchantId") final Integer merchantId,
	            @Param("searchTxt") final String searchTxt);
		
	public List<PizzaTemplate> findByCategoryIdAndMerchantId(Integer categoryId, Integer merchantId);
	
	public PizzaTemplate findByIdAndMerchantId(int templateId, int merchantId);
	
	List<PizzaTemplate> findByMerchantIdAndActive(Integer merchantId,Integer active);
	
	PizzaTemplate findByMerchantIdAndDescription(Integer merchantId,String description);
	
	public PizzaTemplate findById(int templateId);
	
	@Query(value="select * from pizzatemplate where id IN(select pizzatemplate_id from pizzatemplatecrust where pizzacrust_id=:crustId)" , nativeQuery = true)
    List<PizzaTemplate> findMappedpizzaByCrustId(@Param(value="crustId") Integer crustId);
	
	@Query(value="select * from pizzatemplate p left join pizzatemplatecrust pc on (p.id=pc.pizzatemplate_id and pc.pizzacrust_id=:crustId) " + 
			"where pc.pizzatemplate_id is NULL and p.merchant_id=:merchantId" , nativeQuery = true)
    List<PizzaTemplate> findUnMappedpizzaByCrustId(@Param(value="crustId") Integer crustId, @Param("merchantId") Integer merchantId);

	@Query(value="select * from pizzatemplate p left join pizzatemplatesize ps on (p.id=ps.pizzatemplate_id and ps.pizzasize_id=:sizeId) " + 
			"where ps.pizzatemplate_id is NULL and p.merchant_id=:merchantId" , nativeQuery = true)
    List<PizzaTemplate> findUnMappedpizzaBySizeId(@Param(value="sizeId") Integer sizeId, @Param("merchantId") Integer merchantId);

	@Query(value="select * from pizzatemplate p left join pizzatemplatecrust pc on (p.id=pc.pizzatemplate_id and pc.pizzacrust_id=:crustId) " + 
			"where pc.pizzatemplate_id is NULL and p.merchant_id=:merchantId and p.active=:active" , nativeQuery = true)
    List<PizzaTemplate> findUnMappedpizzaByCrustIdAndPizzaActive(@Param(value="crustId") Integer crustId
    		           , @Param("merchantId") Integer merchantId , @Param("active") Integer active);

	@Query(value="select * from pizzatemplate p left join pizzatemplatesize ps on (p.id=ps.pizzatemplate_id and ps.pizzasize_id=:sizeId) " + 
			"where ps.pizzatemplate_id is NULL and p.merchant_id=:merchantId and p.active=:active" , nativeQuery = true)
    List<PizzaTemplate> findUnMappedpizzaBySizeIdAndPizzaActive(@Param(value="sizeId") Integer sizeId
    		                                         , @Param("merchantId") Integer merchantId, @Param("active") Integer active);
	
	@Query(value="select * from pizzatemplate p left join pizza_template_category pc on (p.id=pc.pizza_template_id and pc.category_id=:categoryId) " + 
			"where pc.pizza_template_id is NULL and p.merchant_id=:merchantId and p.active=:active" , nativeQuery = true)
    List<PizzaTemplate> findUnMappedpizzaByCategoryIdAndPizzaActive(@Param(value="categoryId") Integer categoryId
    		                                         , @Param("merchantId") Integer merchantId, @Param("active") Integer active);
	
	@Query(value = "select * from pizzatemplate p left join pizza_template_category pc on (p.id=pc.pizza_template_id) "
			+ "where pc.category_id=:categoryId and p.active=:active",nativeQuery = true)
	List<PizzaTemplate> findByCategoryIdAndPizzaActive(@Param(value="categoryId") Integer categoryId
                                            , @Param("active") Integer active);
	
	Page<PizzaTemplate> findByMerchantId(Integer merchantId, Pageable pageable);
	
	@Query(value="select p.* from pizzatemplate p WHERE p.merchant_id=:merchantId and p.id not in (select pt.template_id from pizza_template_taxes pt join pizzatemplate p on  p.id=pt.template_id where p.merchant_id=:merchantId)" , nativeQuery = true)
    List<PizzaTemplate> findUnmapedTemplate(@Param(value="merchantId") Integer merchantId);
}
