package com.foodkonnekt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.foodkonnekt.model.ConvenienceFee;

public interface ConvenienceFeeRepository extends JpaRepository<ConvenienceFee, Integer> {
	
	public List<ConvenienceFee> findByMerchantId(Integer merchantId);

}
