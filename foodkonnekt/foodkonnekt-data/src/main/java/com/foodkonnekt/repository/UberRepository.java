package com.foodkonnekt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.foodkonnekt.model.UberStore;

public interface UberRepository extends JpaRepository<UberStore, Integer>{

	List<UberStore> findByMerchantId(Integer merchantId);
	
	UberStore findByStoreId(String storeId);
	
	@Query(value="select * from uber_store where store_id=:storeId and merchant_id!=:merchantId",nativeQuery = true)
    UberStore findByStoreIdAndMerchantNot(@Param("storeId") String storeId, @Param("merchantId") Integer merchantId);
}
