package com.foodkonnekt.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.foodkonnekt.model.OrderNotification;

public interface OrderNotificationRepository extends JpaRepository<OrderNotification, Integer>{
	
	public OrderNotification findByMerchantId(Integer merchantId);

}
