package com.foodkonnekt.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.foodkonnekt.model.Item;
import com.foodkonnekt.model.ModifierModifierGroupDto;
import com.foodkonnekt.model.PizzaTemplate;
import com.foodkonnekt.model.PizzaTemplateSize;

public interface PizzaTemplateSizeRepository extends JpaRepository<PizzaTemplateSize, Integer>  {

	List<PizzaTemplateSize> findByPizzaTemplateId(Integer id);

	List<PizzaTemplateSize> findByPizzaSizeId(int pizzaSizeId);

	List<PizzaTemplateSize> findByPizzaTemplateMerchantIdAndPizzaTemplateDescription(Integer merchantId,
			String searchTxt);

	List<PizzaTemplateSize> findByPizzaSizeIdAndPizzaTemplateMerchantId(int pizzaSizeId, Integer merchantId);

	List<PizzaTemplateSize> findByPizzaTemplateMerchantId(Integer merchantId);
	
	PizzaTemplateSize findByPizzaSizeIdAndPizzaTemplateId(Integer pizzaSizeId, Integer pizzaTemplateId);
	
	List<PizzaTemplateSize> findByPizzaSizeDescriptionAndPizzaTemplateId(String description, Integer id);
	
	List<PizzaTemplateSize> findByPizzaTemplateIdAndActive(Integer templateId , Integer active);
	
	@Query(value = "SELECT * FROM pizzatemplatesize WHERE pizzatemplate_id in (:pizzaTemplateIds) and active=:active", nativeQuery = true)
	List<PizzaTemplateSize> findByPizzaTemplateIdAndActive(@Param("pizzaTemplateIds")List<Integer> pizzaTemplateIds, @Param("active")Integer active);
	
	@Query("select p from PizzaTemplateSize p where p.active=:active and p.pizzaTemplate.id in (:pizzaTemplateIds)")
	List<PizzaTemplateSize> findByPizzaTemplateIdsAndActive(@Param("pizzaTemplateIds")List<Integer> pizzaTemplateIds,  @Param("active")Integer active);

	List<PizzaTemplateSize> findByPizzaTemplateIdAndPizzaSizeActive(Integer id,Integer active);
	
	@Query(value="select * from pizzatemplatesize p join pizzasize ps on p.pizzasize_id=ps.id where p.active=:pizzaactive "
			+ "and ps.active=:sizeactive and pizzatemplate_id in (:pizzaTemplateIds)",nativeQuery = true)
	List<PizzaTemplateSize> findByPizzaTemplateIdsAndSizeActive(@Param("pizzaTemplateIds")List<Integer> pizzaTemplateIds,  
			@Param("pizzaactive")Integer pizzaactive,  @Param("sizeactive")Integer sizeactive);

	List<PizzaTemplateSize> findByPizzaSizeIdAndPizzaTemplateActive(int pizzaSizeId, Integer active);

	Page<PizzaTemplateSize> findByPizzaTemplateMerchantId(Integer merchantId, Pageable pageable);
 
	List<PizzaTemplateSize> findByPizzaTemplateIdAndActiveAndPizzaSizeActive(Integer id,Integer active,Integer sizeactive);
}
