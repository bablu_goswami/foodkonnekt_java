package com.foodkonnekt.repository;



import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.foodkonnekt.model.CustomerFeedback;
import com.foodkonnekt.model.CustomerFeedbackAnswer;

public interface CustomerFeedbackRepository extends JpaRepository<CustomerFeedback, Integer> {


	@Query("SELECT c FROM CustomerFeedback c WHERE c.customer.id=:customerId AND c.orderR.id=:orderid")
	List<CustomerFeedback> findByCustomerIdAndOrderId(@Param("customerId")Integer customerId, @Param("orderid")Integer orderid);
	
	
	@Query("SELECT c FROM CustomerFeedback c WHERE c.orderR.id=:orderid")
	List<CustomerFeedback> findByOrderId( @Param("orderid")Integer orderid);
	
	@Query("SELECT c FROM CustomerFeedback c WHERE c.orderR.id=:orderid AND c.merchant.id=:merchantId")
	List<CustomerFeedback> findByOrderIdAndMerchantId( @Param("orderid")Integer orderid,@Param("merchantId")Integer merchantId);
	
	CustomerFeedback findById(Integer id);

	//@Query("SELECT c FROM CustomerFeedback c WHERE c.orderR.id=:orderid")
	List<CustomerFeedback> findByCustomerId(Integer id);
	
	@Query("SELECT c FROM CustomerFeedback c WHERE c.createDate BETWEEN :startDateRange AND :endDateRange and c.merchant.id=:merchantId")
	List<CustomerFeedback> findByMerchantId(@Param("merchantId")Integer merchantId, @Param("startDateRange")Date startDateRange, @Param("endDateRange")Date endDateRange);


	@Query("SELECT c FROM CustomerFeedback c WHERE c.createDate BETWEEN :startDateRange AND :endDateRange and c.customer.id=:customerId")
	CustomerFeedback findByCustomerIdDateRange(@Param("customerId")Integer customerId, @Param("startDateRange")Date startDateRange, @Param("endDateRange")Date endDateRange);


	@Query("SELECT c FROM CustomerFeedback c WHERE c.customer.id=:customerId AND c.createDate BETWEEN :startDate AND :endDate ")
    List<CustomerFeedback> findByCustomerIdAndCreateDate(@Param("customerId")Integer customerId,
                    @Param("startDate")Date startDate,
                    @Param("endDate")Date endDate);
	
	@Query(value = "select SUM(mka.customer_count) as Mail_Sent from merchant_kritiq_analytics mka WHERE mka.merchant_id= :merchantId and mka.send_date BETWEEN :startDateRange and :endDateRange", nativeQuery = true)
	 Integer findMailSentCount(@Param("merchantId") Integer merchantId, @Param("startDateRange")Date startDateRange, @Param("endDateRange")Date endDateRange);
	 
	 @Query(value = "select count(*)from customer_feedback cf where cf.merchant_id = :merchantId and cf.create_date BETWEEN :startDateRange and :endDateRange ", nativeQuery = true)
	 Integer findResponseCount(@Param("merchantId") Integer merchantId ,@Param("startDateRange")Date startDateRange, @Param("endDateRange")Date endDateRange);
	 
	 @Query(value = "select cast((sum((cfa.customer_service+cfa.food_quality+cfa.order_experience))/(count(*)*3)) as  decimal(16,1)) as AVG_RATING from customer_feedback_answer cfa left join customer_feedback cf on cf.id=cfa.customer_feedback_id where  cf.merchant_id = :merchantId and cf.create_date BETWEEN :startDateRange and :endDateRange", nativeQuery = true)
	 Double findAverageRating(@Param("merchantId") Integer merchantId,@Param("startDateRange")Date startDateRange, @Param("endDateRange")Date endDateRange);
	
	 @Query(value = "select * from customer_feedback c join merchant m on c.merchant_id=m.id where m.merchant_uid=:merchantUid",nativeQuery = true)
	 List<CustomerFeedback> findCustomerFeedback(@Param("merchantUid")String merchantUid);

	 @Query(value = "select cast((sum((cfa.customer_service+cfa.food_quality+cfa.order_experience))/(count(*)*3)) as  decimal(16,1)) as AVG_RATING from customer_feedback_answer cfa left join customer_feedback cf on cf.id=cfa.customer_feedback_id where  cfa.customer_feedback_id = :customerFeedbackId and cf.create_date BETWEEN :startDateRange and :endDateRange", nativeQuery = true)
	 Double findCustomersAverageRating(@Param("customerFeedbackId") Integer customerFeedbackId,@Param("startDateRange")Date startDateRange, @Param("endDateRange")Date endDateRange);
	
	 @Query("SELECT c.id FROM CustomerFeedback c WHERE c.createDate BETWEEN :startDateRange AND :endDateRange and c.merchant.id=:merchantId")
	 List<Integer> findFeedbackIdsByMerchantIdAndDate(@Param("merchantId")Integer merchantId, @Param("startDateRange")Date startDateRange, @Param("endDateRange")Date endDateRange);

	 @Query("SELECT c.id FROM CustomerFeedback c WHERE c.merchant.id=:merchantId")
	 List<Integer> findFeedbackIdsByMerchantId(@Param("merchantId")Integer merchantId);

	 @Query(value="select * from customer_feedback c where c.id IN (:customerFeedbackIds)",nativeQuery = true)
	 List<CustomerFeedback> findByCustomerFeedbackIds(@Param("customerFeedbackIds")List<Integer> customerFeedbackIds);
	 
	 @Query(value = "select * from  customer_feedback cf left join  customer_feedback_answer cfa on cf.id=cfa.customer_feedback_id where "
				+ "(cfa.customer_service<3 OR cfa.food_quality<3 or cfa.order_experience<3) and cf.create_date BETWEEN :startDateRange and :endDateRange", nativeQuery = true)
	 List<CustomerFeedback> findRatingBelowThree(@Param("startDateRange")Date startDateRange, @Param("endDateRange")Date endDateRange);
		

}
