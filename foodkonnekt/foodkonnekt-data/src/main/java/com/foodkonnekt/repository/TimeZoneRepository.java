package com.foodkonnekt.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.foodkonnekt.model.TimeZone;

public interface TimeZoneRepository extends JpaRepository<TimeZone, Integer>  {

	TimeZone findByTimeZoneCode(String timeZoneCode);
	
	@Query(value="select t.time_zone_code from time_zone t join merchant m on t.id=m.time_zone_id where m.id=:merchentId", nativeQuery=true)
	public String findTimeZoneCodeByMerchentId(@Param("merchentId") Integer merchentId);
}
