package com.foodkonnekt.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.foodkonnekt.model.NotificationMethodType;

public interface NotificationMethodTypeRepository extends JpaRepository<NotificationMethodType, Integer>{

}
