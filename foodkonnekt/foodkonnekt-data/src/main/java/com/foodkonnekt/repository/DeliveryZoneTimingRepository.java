package com.foodkonnekt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.foodkonnekt.model.DeliveryZoneTiming;

public interface DeliveryZoneTimingRepository extends JpaRepository<DeliveryZoneTiming, Integer> {

	// DeliveryZoneTiming save(DeliveryZoneTiming deliveryZoneTiming);

	List<DeliveryZoneTiming> findByMerchantId(int id);

	@Modifying
	@Transactional
	@Query("delete from DeliveryZoneTiming dt where dt.merchant.id=?1")
	Integer deleteByMerchantId(Integer id);
	
	DeliveryZoneTiming findByDayAndMerchantIdAndIsHoliday(String day,Integer merchantId,boolean isHoliday);
	
	
	
	
}
