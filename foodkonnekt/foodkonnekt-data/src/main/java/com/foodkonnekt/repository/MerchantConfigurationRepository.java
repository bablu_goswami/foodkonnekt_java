package com.foodkonnekt.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.foodkonnekt.model.MerchantConfiguration;

@Repository

public interface MerchantConfigurationRepository  extends JpaRepository<MerchantConfiguration ,Integer>{ 
	
	MerchantConfiguration 	findByMerchantId( Integer merchantId);

}
