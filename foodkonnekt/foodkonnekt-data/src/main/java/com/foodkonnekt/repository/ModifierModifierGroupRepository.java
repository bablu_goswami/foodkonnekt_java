package com.foodkonnekt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.foodkonnekt.model.ModifierModifierGroupDto;
import com.foodkonnekt.model.Modifiers;

public interface ModifierModifierGroupRepository extends JpaRepository<ModifierModifierGroupDto, Integer> {

    /**
     * Find modifiers based on modifierGroupId
     * 
     * @param modifierGroupId
     * @return
     */
    @Query("SELECT md.modifiers FROM ModifierModifierGroupDto md WHERE md.modifierGroup.id=:modifierGroupId")
    List<Modifiers> findByModifierGroupId(@Param("modifierGroupId") final Integer modifierGroupId);

    /**
     * find by modifierId
     * 
     * @param modifierId
     * @return List<ModifierModifierGroupDto>
     */
    List<ModifierModifierGroupDto> findByModifiersId(Integer modifierId);
    
    ModifierModifierGroupDto findByModifierGroupIdAndModifiersId(Integer modifierGroupId, Integer modifierId);
    
    @Query(value="select * from modifiergroup_modifiers where modifiergroup_id=:modifierGroupId",nativeQuery = true )
    List<ModifierModifierGroupDto> findlistByModifierGroupId(@Param("modifierGroupId") final Integer modifierGroupId);

    @Query(value="select * from modifiergroup_modifiers mgm  left join modifiers m on (m.id=mgm.modifiers_id and "
    		+ "mgm.modifiergroup_id=:modifierGroupId ) where mgm.modifiers_id is NULL and m.merchant_id=:merchantId",nativeQuery = true)
    	 public List<ModifierModifierGroupDto> findByMerchantIdAndModifierGroupId(@Param("merchantId") final Integer merchantId ,
    			 @Param("modifierGroupId") final Integer modifierGroupId);
    @Query("SELECT md.modifiers FROM ModifierModifierGroupDto md WHERE md.modifierGroup.id=:modifierGroupId and md.modifiers.status=1")
    List<Modifiers> findByModifierGroupIdAndModifierStatus(@Param("modifierGroupId") final Integer modifierGroupId);

    @Query("SELECT md.modifiers FROM ModifierModifierGroupDto md WHERE md.modifierGroup.id=:modifierGroupId and md.status=:status")
	List<Modifiers> findByModifierGroupIdAndStatus(@Param("modifierGroupId") final Integer modifierGroupId,
			@Param("status") final int status);
    
    List<ModifierModifierGroupDto> findByModifierGroupIdAndModifiersStatus(Integer modifierGroupId,Integer status);

    List<ModifierModifierGroupDto> findByModifiersIdAndModifierGroupActive(Integer modifierId,Integer status);
    
    @Modifying
    @Transactional
    @Query(value=" DELETE FROM modifiergroup_modifiers WHERE modifiers_id=:modifiersId",nativeQuery = true )
    void deleteDuplicateModifierGroupModifiersMapping(@Param("modifiersId") Integer modifiersId);
}
