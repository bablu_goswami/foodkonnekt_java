package com.foodkonnekt.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.foodkonnekt.model.EncryptedKey;

public interface MerchantEncryptedKeyRepository extends JpaRepository<EncryptedKey, Integer>{

	EncryptedKey findByMerchantId(Integer id);

}
