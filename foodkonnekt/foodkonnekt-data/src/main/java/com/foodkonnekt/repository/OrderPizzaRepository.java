package com.foodkonnekt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.foodkonnekt.model.OrderPizza;

/**
 * The Interface OrderPizzaRepository.
 */
public interface OrderPizzaRepository extends JpaRepository<OrderPizza, Integer>{
	List<OrderPizza> findByOrderId(Integer id);
}
