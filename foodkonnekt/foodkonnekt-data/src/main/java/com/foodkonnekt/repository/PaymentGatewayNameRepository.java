package com.foodkonnekt.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.foodkonnekt.model.PaymentGatewayName;

public interface PaymentGatewayNameRepository extends JpaRepository<PaymentGatewayName,Integer>{

}
