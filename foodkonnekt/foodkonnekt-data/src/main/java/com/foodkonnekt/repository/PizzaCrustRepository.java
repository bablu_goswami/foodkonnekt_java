package com.foodkonnekt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.foodkonnekt.model.PizzaCrust;
import com.foodkonnekt.model.PizzaTopping;

public interface PizzaCrustRepository extends
		JpaRepository<PizzaCrust, Integer> {

	List<PizzaCrust> findByMerchantIdAndDescription(Integer merchantId,
			String descpition);
	
	PizzaCrust findByMerchantIdAndId(Integer merchantId,
	                Integer id);
	
	List<PizzaCrust> findByMerchantId(Integer merchantId);
	
	@Query(value= "select pc.* from pizzatemplatecrust pt left join pizzacrust pc on pt.pizzacrust_id = pc.id where pt.pizzatemplate_id =:templateId",nativeQuery = true)
	List<PizzaCrust> findPzzaCrustByTemplateId(@Param("templateId") Integer templateId);
	
	public List<PizzaCrust> findByDescriptionAndMerchantId(String description, Integer merchantId);

	@Query(value="select * from pizzacrust p left join pizza_crust_sizes ps on (p.id=ps.pizzacrust_id and ps.pizzasize_id=:sizeId) " + 
			"where ps.pizzacrust_id is NULL and p.merchant_id=:merchantId" , nativeQuery = true)
    List<PizzaCrust> findUnMappedCrustBySizeId(@Param(value="sizeId") Integer sizeId, @Param("merchantId") Integer merchantId);
	
	@Query(value="select * from pizzacrust p left join pizza_crust_sizes ps on (p.id=ps.pizzacrust_id and ps.pizzasize_id=:sizeId) " + 
			"where ps.pizzacrust_id is NULL and p.merchant_id=:merchantId and p.active=:active" , nativeQuery = true)
    List<PizzaCrust> findUnMappedCrustBySizeIdAndCrustActive(@Param(value="sizeId") Integer sizeId
    		              , @Param("merchantId") Integer merchantId, @Param("active") Integer active);
	
	List<PizzaCrust> findByMerchantIdAndActive(Integer merchantId ,Integer active);
}
