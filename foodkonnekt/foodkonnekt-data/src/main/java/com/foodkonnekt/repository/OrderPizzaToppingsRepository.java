package com.foodkonnekt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.foodkonnekt.model.OrderPizzaToppings;

public interface OrderPizzaToppingsRepository extends JpaRepository<OrderPizzaToppings, Integer>{
	List<OrderPizzaToppings> findByOrderPizzaId(Integer id);
}
