package com.foodkonnekt.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.foodkonnekt.model.PaymentGateWay;

public interface PaymentGateWayRepository extends JpaRepository<PaymentGateWay,Integer> {


	 @Query("SELECT m FROM PaymentGateWay m WHERE m.merchantId=:merchantId and m.isDeleted=:isDeleted")
	 PaymentGateWay findByMerchantIdAndIsDeleted(@Param("merchantId") final Integer merchantId,
			 @Param("isDeleted") final Boolean isDeleted);

	 PaymentGateWay findByMerchantIdAndIsDeletedAndIsActive(Integer merchantId, Boolean isDeleted, Boolean isActive);

}
