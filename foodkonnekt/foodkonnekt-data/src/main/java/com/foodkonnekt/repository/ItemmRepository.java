package com.foodkonnekt.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.foodkonnekt.model.Item;

public interface ItemmRepository extends JpaRepository<Item, Integer> {

    /**
     * find by clover ItemId
     * 
     * @param posItemId
     * @return Item instance
     */
    Item findByPosItemId(String posItemId);

    /**
     * Find by merchatId
     * 
     * @param merchantId
     * @return List<Item>
     */
    List<Item> findByMerchantId(Integer merchantId);

    /**
     * find by ItemId and clover merchantId
     * 
     * @param posItemId
     * @param merchantId
     * @return Item instance
     */
    Item findByPosItemIdAndMerchantId(String posItemId, Integer merchantId);
    
    Item findByIdAndMerchantId(Integer id, Integer merchantId);

    /**
     * find by categoryId
     * 
     * @param categoryId
     * @return List<Item>
     */
    List<Item> findByCategoriesId(int categoryId);
    
   

    /**OrderBySortOrderAsc
     * Find item count by merchantId
     * 
     * @param merchantId
     * @return Long
     */
    @Query("select count(item) from Item item where item.merchant.id = ?1")
    Long itemCountByMerchantId(Integer merchantId);
    
    @Query(value="select i.* from item i left join item_taxes it on it.item_id=i.id "
    		+ "left join  item_category ic on ic.item_id=it.item_id "
    		+ "left join item_timing itm on itm.item_id=ic.item_id "
    		+ "where ic.category_id=:categoryId and i.item_status=0 and "
    		+ "((i.allow_item_timing=0) or (i.allow_item_timing=1 and itm.isHoliday=0 and itm.`day`=:day and itm.start_time<=:startTime and itm.end_time>=:endTime))  "
    		+ "GROUP BY i.id ORDER BY ic.sortOrder ASC",nativeQuery=true)
    List<Item> findItemByCategoryId(@Param("categoryId")Integer categoryId,@Param("day")String day,@Param("startTime")String startTime,@Param("endTime")String endTime);
    
    
    
    @Query(value="select i.* from item i left join item_taxes it on it.item_id=i.id "
    		+ "left join  item_category ic on ic.item_id=it.item_id "
    		+ "left join item_timing itm on itm.item_id=ic.item_id "
    		+ "where ic.category_id=:categoryId and i.item_status=0 and "
    		+ "((i.allow_item_timing=0) or (i.allow_item_timing=1 and itm.isHoliday=0 and itm.`day`=:day and itm.start_time<=:startTime and itm.end_time>=:endTime))  "
    		+ "GROUP BY i.id ORDER BY ic.sortOrder ASC LIMIT 10 OFFSET :offSet ",nativeQuery=true)
    List<Item> findItemByCategoryIdV2(@Param("categoryId")Integer categoryId,@Param("day")String day,@Param("startTime")String startTime,@Param("endTime")String endTime,
    		@Param("offSet")Integer offSet);

    
    @Query(value="select i.* from item i "
    		+ "left join item_taxes it on it.item_id=i.id "
    		+ "left join item_timing itm on itm.item_id=it.item_id "
    		+ "where i.id=:itemId  and i.item_status=0 and "
    		+ "((i.allow_item_timing=0) or (i.allow_item_timing=1 and itm.isHoliday=0 "
    		+ "and itm.`day`=:day and itm.start_time<=:startTime and itm.end_time>=:endTime)) GROUP BY i.id",nativeQuery=true)
    Item findItemByItemId(@Param("itemId")Integer itemId,@Param("day")String day,@Param("startTime")String startTime,@Param("endTime")String endTime);

    /**
     * Find Item Name by itemId
     * 
     * @param itemId
     * @return String
     */
    @Query("select item.name from Item item where item.id = ?1")
    String findItemNameByItemId(Integer itemId);

    Page<Item> findByMerchantId(Integer merchantId, Pageable pageable);
    
    Page<Item> findByMerchantIdAndItemStatusNot(Integer merchantId, Pageable pageable,Integer itemStatus);

    @Query("SELECT item FROM Item item WHERE item.merchant.id=:merchantId and item.name like %:searchTxt% ")
    List<Item> findByMerchantIdAndItemName(@Param("merchantId") final Integer merchantId,
                    @Param("searchTxt") final String searchTxt);
    
    List<Item> findByMerchantIdAndName(Integer merchantId,String name);


	List<Item> findByMerchantMerchantUid(String merchantUId);

	List<Item> findByMerchantOwnerVendorUid(String vendorUId);
	
	List<Item> findByName(String name);
	
	 @Query(value="select i.* from item i left join item_taxes it on it.item_id=i.id "
	    		+ "left join  item_category ic on ic.item_id=it.item_id "
	    		+ "left join item_timing itm on itm.item_id=ic.item_id "
	    		+ "where ic.category_id=:categoryId and ic.active=1 and i.item_status=0 and "
	    		+ "((i.allow_item_timing=0) or (i.allow_item_timing=1 and itm.isHoliday=0 and itm.`day`=:day and itm.start_time<=:startTime and itm.end_time>=:endTime))  "
	    		+ "GROUP BY i.id ORDER BY ic.sortOrder ASC",nativeQuery=true)
	    List<Item> findItemByCategoryIdAndActive(@Param("categoryId")Integer categoryId,@Param("day")String day,@Param("startTime")String startTime,@Param("endTime")String endTime);
	    
	    
	    
	    @Query(value="select i.* from item i left join item_taxes it on it.item_id=i.id "
	    		+ "left join  item_category ic on ic.item_id=it.item_id "
	    		+ "left join item_timing itm on itm.item_id=ic.item_id "
	    		+ "where ic.category_id=:categoryId and ic.active=1 and i.item_status=0 and "
	    		+ "((i.allow_item_timing=0) or (i.allow_item_timing=1 and itm.isHoliday=0 and itm.`day`=:day and itm.start_time<=:startTime and itm.end_time>=:endTime))  "
	    		+ "GROUP BY i.id ORDER BY ic.sortOrder ASC LIMIT 10 OFFSET :offSet ",nativeQuery=true)
	    List<Item> findItemByCategoryIdV2AndActive(@Param("categoryId")Integer categoryId,@Param("day")String day,@Param("startTime")String startTime,@Param("endTime")String endTime,
	    		@Param("offSet")Integer offSet);
	
}
