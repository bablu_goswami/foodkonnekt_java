package com.foodkonnekt.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;

import com.foodkonnekt.model.NotificationTracker;
import com.foodkonnekt.model.OrderR;
import com.foodkonnekt.model.OrderTracker;


public interface NotificationTrackerRepository extends JpaRepository<NotificationTracker, Integer>{
	
	//@Query(value = "SELECT c FROM OrderR c WHERE c.createdOn BETWEEN :startDate AND :endDate and merchant_id = :merchantId  ORDER BY c.createdOn desc")
	
	@Query(value = "SELECT * FROM notification_tracker WHERE order_id =:orderId and merchant_id =:merchantId", nativeQuery = true)
    public List<NotificationTracker> findByOrderIdAndMerchantId(@Param("orderId") Integer orderId, @Param("merchantId") Integer merchantId);
    
	

}
