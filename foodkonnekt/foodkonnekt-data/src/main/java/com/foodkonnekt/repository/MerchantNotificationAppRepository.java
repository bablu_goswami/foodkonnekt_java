package com.foodkonnekt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.foodkonnekt.model.MerchantNotificationApp;

@Repository
public interface MerchantNotificationAppRepository extends JpaRepository<MerchantNotificationApp, Integer>{

	public List<MerchantNotificationApp> findAll();
	
	@Query("select m from MerchantNotificationApp m where merchant.id=:merchantId and appId.id=:appId")
	public MerchantNotificationApp FindByMerchantIdAndNotificationAppId(@Param("merchantId") Integer merchantId, @Param("appId") Integer appId);
}
