package com.foodkonnekt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.foodkonnekt.model.PizzaTemplateTax;

public interface PizzaTemplateTaxRepository extends JpaRepository<PizzaTemplateTax, Integer> {

	List<PizzaTemplateTax> findByPizzaTemplateIdAndPizzaTemplateMerchantIdAndIsActive(Integer templateId, Integer merchantId,Integer isActive);
	
	List<PizzaTemplateTax> findByPizzaTemplateId(Integer templateId);
	
	PizzaTemplateTax findByPizzaTemplateIdAndTaxRatesId(Integer templateId, Integer taxRateId);
	List<PizzaTemplateTax> findByPizzaTemplateIdAndPizzaTemplateMerchantId(Integer templateId, Integer merchantId);
	
	List<PizzaTemplateTax> findByPizzaTemplateIdAndIsActive(Integer templateId, Integer active);
}
