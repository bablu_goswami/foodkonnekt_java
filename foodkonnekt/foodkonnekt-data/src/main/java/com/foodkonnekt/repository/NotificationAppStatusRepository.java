package com.foodkonnekt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.foodkonnekt.model.NotificationAppStatus;

@Repository
public interface NotificationAppStatusRepository extends JpaRepository<NotificationAppStatus, Integer> {

	public List<NotificationAppStatus> findByMerchantid(Integer merchantId);

	@Query(value = "SELECT COUNT(*) FROM notification_app_status GROUP BY merchant_id;", nativeQuery = true)
	public List<Integer> merchantcount();

	public List<NotificationAppStatus> findByAppId(Integer appId);

	@Query(value = "SELECT DISTINCT merchant_id FROM notification_app_status;", nativeQuery = true)
	public List<Integer> distinctMerchants();

	@Query(value = "SELECT * FROM notification_app_status WHERE id IN (SELECT MAX(id) FROM notification_app_status GROUP BY merchant_id);", nativeQuery = true)
	public List<NotificationAppStatus> lastOccuranceOfMerchantId();

	@Query(value = "SELECT * FROM notification_app_status WHERE is_running = '1';", nativeQuery = true)
	public List<NotificationAppStatus> findByIsRunning1();

	@Query(value = "SELECT * FROM notification_app_status WHERE id IN (SELECT MAX(id) FROM notification_app_status where merchant_id=:merchantId);", nativeQuery = true)
	public NotificationAppStatus lastOccuranceOfMerchantId(@Param("merchantId") Integer merchantId);

}
