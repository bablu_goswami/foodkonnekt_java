package com.foodkonnekt.model;

import java.util.List;

import javax.persistence.Transient;

public class UpdatedAndCurrentVersion {

	private String merchantName;
	private Integer appVersion;
	private String currentVersion;

	public String getMerchantName() {
		return merchantName;
	}

	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}

	public Integer getAppVersion() {
		return appVersion;
	}

	public void setAppVersion(Integer appVersion) {
		this.appVersion = appVersion;
	}

	public String getCurrentVersion() {
		return currentVersion;
	}

	public void setCurrentVersion(String currentVersion) {
		this.currentVersion = currentVersion;
	}



	

}
