package com.foodkonnekt.model;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "merchant_configuration")
public class MerchantConfiguration {
	
	 @Id
	    @GeneratedValue(strategy = IDENTITY)
	    @Column(name = "id", unique = true, nullable = false)
	    private Integer id;
	 
		@Column(name = "enable_notification")
	    private Boolean enableNotification ;
	 
	
	 @OneToOne(fetch=FetchType.EAGER)
	 @JoinColumn(name="merchantId", referencedColumnName="id")
	 private Merchant merchant;
	 
	
	 public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public Boolean getEnableNotification() {
		return enableNotification;
	}


	public void setEnableNotification(Boolean enableNotification) {
		this.enableNotification = enableNotification;
	}


	public Merchant getMerchant() {
		return merchant;
	}


	public void setMerchant(Merchant merchant) {
		this.merchant = merchant;
	}

	@Column(name = "future_order_notification")
    private Boolean autoAccept ;

	
	@Column(name= "auto_accept")
	private Boolean autoAcceptOrder;


	public Boolean getAutoAccept() {
		return autoAccept;
	}


	public void setAutoAccept(Boolean autoAccept) {
		this.autoAccept = autoAccept;
	}


	public Boolean getAutoAcceptOrder() {
		return autoAcceptOrder;
	}


	public void setAutoAcceptOrder(Boolean autoAcceptOrder) {
		this.autoAcceptOrder = autoAcceptOrder;
	}


}
