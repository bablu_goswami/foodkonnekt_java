package com.foodkonnekt.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Table(name = "merchant_kritiq_analytics")
@Entity
public class KritiqAnalytics {

	 @Id
	 @GeneratedValue(strategy = IDENTITY)
	 @Column(name = "id", unique = true, nullable = false)
	    private Integer id;
	 
	 @Column(name="send_date")
	 	private Date mailSendDate;
	 
	 @Column(name="customer_count")
	 	private Integer customerCount;
	 
	 @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
	 @JoinColumn(name = "merchant_id", referencedColumnName = "id")
	    private Merchant merchant;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getMailSendDate() {
		return mailSendDate;
	}

	public void setMailSendDate(Date mailSendDate) {
		this.mailSendDate = mailSendDate;
	}

	public Integer getCustomerCount() {
		return customerCount;
	}

	public void setCustomerCount(Integer customerCount) {
		this.customerCount = customerCount;
	}

	public Merchant getMerchant() {
		return merchant;
	}

	public void setMerchant(Merchant merchant) {
		this.merchant = merchant;
	}
	
}
