package com.foodkonnekt.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "merchant_printer")
public class Printer {
 
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id;
	
	@Column(name = "refresh_token")
	private String printerRefreshToken;
	
	@Column(name = "printer_id")
	private String printerId;
	
	@Column(name = "printer_name")
	private String printerName;

	@Column(name = "merchant_id")
	private Integer merchantId;
	
	@Column(name = "is_active")
	private Integer isActive;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getPrinterRefreshToken() {
		return printerRefreshToken;
	}

	public void setPrinterRefreshToken(String printerRefreshToken) {
		this.printerRefreshToken = printerRefreshToken;
	}

	public String getPrinterId() {
		return printerId;
	}

	public void setPrinterId(String printerId) {
		this.printerId = printerId;
	}

	public String getPrinterName() {
		return printerName;
	}

	public void setPrinterName(String printerName) {
		this.printerName = printerName;
	}


	public Integer getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(Integer merchantId) {
		this.merchantId = merchantId;
	}


	public Integer getIsActive() {
		return isActive;
	}

	public void setIsActive(Integer isActive) {
		this.isActive = isActive;
	}




	@Transient
	private Map<String,String> printers;


	public Map<String, String> getPrinters() {
		return printers;
	}

	public void setPrinters(Map<String, String> printers) {
		this.printers = printers;
	}
		
	
}
