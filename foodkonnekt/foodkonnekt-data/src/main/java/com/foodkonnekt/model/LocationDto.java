package com.foodkonnekt.model;




public class LocationDto {

	
    private Integer locationId;

    private String locationName;

    private String locationLogo;

    private String address1;

    private String address2;

    private String city;

    private String country;

    private String latitude;

    private String longitude;

    private String state;

    private String phone;

    private String fax;

    private String zip;
    
    private String locationUid;
    
    private boolean isActive;

    
    private Long faceBookPageId;
    
    private String fbAccessToken;
    
    
    private String refreshFbAccessToken;
    
    
    
    public Long getFaceBookPageId() {
		return faceBookPageId;
	}

	public void setFaceBookPageId(Long faceBookPageId) {
		this.faceBookPageId = faceBookPageId;
	}

	public String getFbAccessToken() {
		return fbAccessToken;
	}

	public void setFbAccessToken(String fbAccessToken) {
		this.fbAccessToken = fbAccessToken;
	}

	public String getRefreshFbAccessToken() {
		return refreshFbAccessToken;
	}

	public void setRefreshFbAccessToken(String refreshFbAccessToken) {
		this.refreshFbAccessToken = refreshFbAccessToken;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

    public String getLocationUid() {
		return locationUid;
	}

	public void setLocationUid(String locationUid) {
		this.locationUid = locationUid;
	}

	


    public Integer getLocationId() {
        return locationId;
    }

    public void setLocationId(Integer locationId) {
        this.locationId = locationId;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

     

    public String getLocationLogo() {
        return locationLogo;
    }

    public void setLocationLogo(String locationLogo) {
        this.locationLogo = locationLogo;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

 
	
	
	
	
}
