package com.foodkonnekt.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "notification_app_status")
public class NotificationAppStatus {
	
		@Id
	    @GeneratedValue(strategy = IDENTITY)
	    @Column(name = "id", unique = true, nullable = false)
	    private Integer id;
	 
	 	@Column(name = "merchant_id")
	    private Integer merchantid;
	    
	 	@Column(name = "app_id")	 
	    private Integer appId;
	    
	    @Column(name = "updated_date")
	    private Date updatedDate;
	    
	    @Column(name = "is_running")
	    private Integer isRunning;

	    @Column(name = "notified_date")
	    private Date notifieDate;
	    
	    @Column(name = "is_active")
	    private Integer isActive;
	    
		public Integer getIsActive() {
			return isActive;
		}

		public void setIsActive(Integer isActive) {
			this.isActive = isActive;
		}

		public Date getNotifieDate() {
			return notifieDate;
		}

		public void setNotifieDate(Date notifieDate) {
			this.notifieDate = notifieDate;
		}

		public Integer getId() {
			return id;
		}

		public void setId(Integer id) {
			this.id = id;
		}

		public Integer getMerchantid() {
			return merchantid;
		}

		public void setMerchantid(Integer merchantid) {
			this.merchantid = merchantid;
		}

		public Integer getAppId() {
			return appId;
		}

		public void setAppId(Integer appId) {
			this.appId = appId;
		}

		public Date getUpdatedDate() {
			return updatedDate;
		}

		public void setUpdatedDate(Date updatedDate) {
			this.updatedDate = updatedDate;
		}

		public Integer getIsRunning() {
			return isRunning;
		}

		public void setIsRunning(Integer isRunning) {
			this.isRunning = isRunning;
		}

		@Column(name = "is_notified")
	    private Integer isNotified;

		public Integer getIsNotified() {
			return isNotified;
		}

		public void setIsNotified(Integer isNotified) {
			this.isNotified = isNotified;
		}

	    

}

