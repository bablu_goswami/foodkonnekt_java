package com.foodkonnekt.model;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "pizzatoppingsize")
public class PizzaToppingSize {
	
	@Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;
 
    private double price;
    
    private int active;
    
    private String posPizzaToppingSizeId;
    
    public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getActive() {
		return active;
	}

	public void setActive(int active) {
		this.active = active;
	}

	public String getPosPizzaToppingSizeId() {
		return posPizzaToppingSizeId;
	}

	public void setPosPizzaToppingSizeId(String posPizzaToppingSizeId) {
		this.posPizzaToppingSizeId = posPizzaToppingSizeId;
	}

	public PizzaSize getPizzaSize() {
		return pizzaSize;
	}

	public void setPizzaSize(PizzaSize pizzaSize) {
		this.pizzaSize = pizzaSize;
	}

	public PizzaTopping getPizzaTopping() {
		return pizzaTopping;
	}

	public void setPizzaTopping(PizzaTopping pizzaTopping) {
		this.pizzaTopping = pizzaTopping;
	}

	@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "pizzasize_id", referencedColumnName = "id")
    private PizzaSize pizzaSize;
    
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "pizzatopping_id", referencedColumnName = "id")
    private PizzaTopping pizzaTopping;

    @Transient
	private String status;
	@Transient
    private String action;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	
	public String getTopping() {
		return topping;
	}

	public void setTopping(String topping) {
		this.topping = topping;
	}

	@Transient
	private String name;
    
	@Transient
	private String size;
	
	@Transient
	private String topping;
}
