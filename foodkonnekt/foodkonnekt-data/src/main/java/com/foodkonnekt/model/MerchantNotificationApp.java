package com.foodkonnekt.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "merchant_notification_app")
public class MerchantNotificationApp {
	
	 	@Id
	    @GeneratedValue(strategy = IDENTITY)
	    @Column(name = "id", unique = true, nullable = false)
	    private Integer id;
	 
	 	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
	 	@JoinColumn(name = "merchant_id", referencedColumnName = "id")
	    private Merchant merchant;
	    
	 	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
	 	@JoinColumn(name = "app_id", referencedColumnName = "id")	 
	    private NotificationApp appId;
	    
	    @Column(name = "created_date")
	    private Date createdDate;

		public Integer getId() {
			return id;
		}

		public void setId(Integer id) {
			this.id = id;
		}

		public Merchant getMerchant() {
			return merchant;
		}

		public void setMerchant(Merchant merchant) {
			this.merchant = merchant;
		}

		public NotificationApp getAppId() {
			return appId;
		}

		public void setAppId(NotificationApp appId) {
			this.appId = appId;
		}

		public Date getCreatedDate() {
			return createdDate;
		}

		public void setCreatedDate(Date createdDate) {
			this.createdDate = createdDate;
		}

	    
}
