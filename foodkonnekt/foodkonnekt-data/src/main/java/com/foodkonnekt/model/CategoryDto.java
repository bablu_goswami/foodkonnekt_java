package com.foodkonnekt.model;

import java.util.List;

public class CategoryDto implements Comparable<CategoryDto>{

    private Integer id;
    private String categoryName;
	private List<ItemDto> itemDtos;
	private String categoryImage;
	private Integer isPizza;
	private Integer sortOrder;
	    
	public Integer getIsPizza() {
		return isPizza;
	}

	public void setIsPizza(Integer isPizza) {
		this.isPizza = isPizza;
	}
	public int getCategoryStatus() {
		return categoryStatus;
	}

	public void setCategoryStatus(int categoryStatus) {
		this.categoryStatus = categoryStatus;
	}

	private int categoryStatus;
    
	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public List<ItemDto> getItemDtos() {
        return itemDtos;
    }

    public void setItemDtos(List<ItemDto> itemDtos) {
        this.itemDtos = itemDtos;
    }

	public String getCategoryImage() {
		return categoryImage;
	}

	public void setCategoryImage(String categoryImage) {
		this.categoryImage = categoryImage;
	}

	public Integer getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(Integer sortOrder) {
		this.sortOrder = sortOrder;
	}

	public int compareTo(CategoryDto o) {
		if(o!=null && o.getSortOrder()!=null && this.sortOrder!=null){
			return (this.sortOrder - o.sortOrder);
		}
		return -1;
	}
}
