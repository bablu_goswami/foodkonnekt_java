package com.foodkonnekt.model;



import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Entity;

import com.foodkonnekt.util.EncryptionDecryptionToken;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ExcelWorkbook {
	private static final Logger LOGGER = LoggerFactory.getLogger(ExcelWorkbook.class);

	private String fileName;
	private Collection<ExcelWorksheet> sheets = new ArrayList<ExcelWorksheet>();
	
	public void addExcelWorksheet(ExcelWorksheet sheet) {
		sheets.add(sheet);
	}
	
	public String toJson(boolean pretty) {
		try {
			if(pretty) {
				return new ObjectMapper().writer().withDefaultPrettyPrinter().writeValueAsString(this);	
			} else {
				return new ObjectMapper().writer().withPrettyPrinter(null).writeValueAsString(this);	
			}
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			LOGGER.error("error: " + e.getMessage());
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			LOGGER.error("error: " + e.getMessage());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			LOGGER.error("error: " + e.getMessage());
		}
		return null;
	}
	
	
	// GET/SET
	
	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public Collection<ExcelWorksheet> getSheets() {
		return sheets;
	}

	public void setSheets(Collection<ExcelWorksheet> sheets) {
		this.sheets = sheets;
	}

}
