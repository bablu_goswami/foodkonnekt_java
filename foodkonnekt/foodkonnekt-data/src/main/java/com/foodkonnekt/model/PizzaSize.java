package com.foodkonnekt.model;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.envers.Audited;

@Entity
@Table(name = "pizzasize")
@XmlRootElement(name = "PizzaSize")
@Audited
public class PizzaSize {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	private String description;

	private int active;

	private String posPizzaSizeId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "merchant_id", referencedColumnName = "id")
	private Merchant merchant;

	public Merchant getMerchant() {
		return merchant;
	}

	public void setMerchant(Merchant merchant) {
		this.merchant = merchant;
	}

	public Set<PizzaTemplate> getPizzaTemplates() {
		return pizzaTemplates;
	}

	public void setPizzaTemplates(Set<PizzaTemplate> pizzaTemplates) {
		this.pizzaTemplates = pizzaTemplates;
	}

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "pizzasize_pizzatemplate", joinColumns = {
			@JoinColumn(name = "pizza_size_id") }, inverseJoinColumns = { @JoinColumn(name = "pizzatemplate_id") })
	private Set<PizzaTemplate> pizzaTemplates = new HashSet<PizzaTemplate>();

	@LazyCollection(LazyCollectionOption.FALSE)
	@ManyToMany(mappedBy = "pizzaSizes", fetch = FetchType.LAZY)
	private Set<Category> categories = new HashSet<Category>();

	@OneToMany(mappedBy = "pizzaSize", fetch = FetchType.LAZY)
	private List<PizzaToppingSize> pizzaToppingSizes;

	@OneToMany(mappedBy = "pizzaSize", fetch = FetchType.LAZY)
	private List<PizzaCrustSizes> pizzaCrustSizes;

	public List<PizzaToppingSize> getPizzaToppingSizes() {
		return pizzaToppingSizes;
	}

	public void setPizzaToppingSizes(List<PizzaToppingSize> pizzaToppingSizes) {
		this.pizzaToppingSizes = pizzaToppingSizes;
	}

	public int getActive() {
		return active;
	}

	public Set<Category> getCategories() {
		return categories;
	}

	public void setCategories(Set<Category> categories) {
		this.categories = categories;
	}

	public String getPosPizzaSizeId() {
		return posPizzaSizeId;
	}

	public void setPosPizzaSizeId(String posPizzaSizeId) {
		this.posPizzaSizeId = posPizzaSizeId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int isActive() {
		return active;
	}

	public void setActive(int active) {
		this.active = active;
	}

	public List<PizzaCrustSizes> getPizzaCrustSizes() {
		return pizzaCrustSizes;
	}

	public void setPizzaCrustSizes(List<PizzaCrustSizes> pizzaCrustSizes) {
		this.pizzaCrustSizes = pizzaCrustSizes;
	}

	@Transient
	private Double price;

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	@Transient
	private List<Integer> tempId;

	@Transient
	private List<Integer> unmaptempId;

	public List<Integer> getTempId() {
		return tempId;
	}

	public void setTempId(List<Integer> tempId) {
		this.tempId = tempId;
	}

	public List<Integer> getUnmaptempId() {
		return unmaptempId;
	}

	public void setUnmaptempId(List<Integer> unmaptempId) {
		this.unmaptempId = unmaptempId;
	}

    @Transient
    private List<Integer> mappizzatopping;
    
    @Transient
    private List<Integer> unmappizzatopping;
    
    @Transient
    private List<Integer> mappizzaCrust;
    
    @Transient
    private List<Integer> unmappizzaCrust;
    

	public void setMappizzatopping(List<Integer> mappizzatopping) {
		this.mappizzatopping = mappizzatopping;
	}

	public List<Integer> getUnmappizzatopping() {
		return unmappizzatopping;
	}

	
	public List<Integer> getMappizzatopping() {
		return mappizzatopping;
	}

	public void setUnmappizzatopping(List<Integer> unmappizzatopping) {
		this.unmappizzatopping = unmappizzatopping;
	}

	public List<Integer> getMappizzaCrust() {
		return mappizzaCrust;
	}

	public void setMappizzaCrust(List<Integer> mappizzaCrust) {
		this.mappizzaCrust = mappizzaCrust;
	}

	public List<Integer> getUnmappizzaCrust() {
		return unmappizzaCrust;
	}

	public void setUnmappizzaCrust(List<Integer> unmappizzaCrust) {
		this.unmappizzaCrust = unmappizzaCrust;
	}
	
	@Transient
	private String topping;
	
	public String getTopping() {
		return topping;
	}

	public void setTopping(String topping) {
		this.topping = topping;
	}

	 @Transient
		private String status;
		@Transient
	    private String action;

		public String getStatus() {
			return status;
		}

		public void setStatus(String status) {
			this.status = status;
		}

		public String getAction() {
			return action;
		}

		public void setAction(String action) {
			this.action = action;
		}
}
