package com.foodkonnekt.model;

import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "desktop_app_version")
public class DesktopVesion {
	@Id
	@Column(name = "version")
	private Integer version;

	@Column(name = "release_date")
	private String release_date;

	@Column(name = "is_active")
	private Integer isActive;

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	public String getRelease_date() {
		return release_date;
	}

	public void setRelease_date(String release_date) {
		this.release_date = release_date;
	}

	@Transient
	private BigInteger countVerion;

	public BigInteger getCountVerion() {
		return countVerion;
	}

	public void setCountVerion(BigInteger countVerion) {
		this.countVerion = countVerion;
	}

	public Integer getIsActive() {
		return isActive;
	}

	public void setIsActive(Integer isActive) {
		this.isActive = isActive;
	}

}
