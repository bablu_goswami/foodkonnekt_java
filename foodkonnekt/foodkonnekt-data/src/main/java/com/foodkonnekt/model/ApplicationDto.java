package com.foodkonnekt.model;

public class ApplicationDto {

//	private Integer applicationId;
	
	private String application;
	
	private String appStatus;
	
	
	public String getAppStatus() {
		return appStatus;
	}

	public void setAppStatus(String appStatus) {
		this.appStatus = appStatus;
	}

	public String getApplication() {
		return application;
	}

	public void setApplication(String application) {
		this.application = application;
	}
	
}
