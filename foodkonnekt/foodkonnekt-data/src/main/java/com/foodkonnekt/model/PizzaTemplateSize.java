package com.foodkonnekt.model;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "pizzatemplatesize")
public class PizzaTemplateSize {
	
	@Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;
 
    private String description;
    
    private String posPizzaTemplateSizeId;
    
    public String getPosPizzaTemplateSizeId() {
		return posPizzaTemplateSizeId;
	}

	public void setPosPizzaTemplateSizeId(String posPizzaTemplateSizeId) {
		this.posPizzaTemplateSizeId = posPizzaTemplateSizeId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getActive() {
		return active;
	}

	public void setActive(int active) {
		this.active = active;
	}

	public PizzaSize getPizzaSize() {
		return pizzaSize;
	}

	public void setPizzaSize(PizzaSize pizzaSize) {
		this.pizzaSize = pizzaSize;
	}

	public PizzaTemplate getPizzaTemplate() {
		return pizzaTemplate;
	}

	public void setPizzaTemplate(PizzaTemplate pizzaTemplate) {
		this.pizzaTemplate = pizzaTemplate;
	}

	private double price;
    
    private int active;
    
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "pizzasize_id", referencedColumnName = "id")
    private PizzaSize pizzaSize;
    
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "pizzatemplate_id", referencedColumnName = "id")
    private PizzaTemplate pizzaTemplate;

    @Transient
	private String status;
	@Transient
    private String action;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}
    
    public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public String getTopping() {
		return topping;
	}

	public void setTopping(String topping) {
		this.topping = topping;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	
	public String getTemplate() {
		return template;
	}

	public void setTemplate(String template) {
		this.template = template;
	}

	@Transient
	private String name;
	
	@Transient
	private String size;
	
	@Transient
	private String topping;
	
	@Transient
	private String categoryName;
	
	@Transient
	private String template;
	
	

}
