package com.foodkonnekt.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;



@Entity
@Table(name = "order_payment_detail")
public class OrderPaymentDetail {

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
	
	 @Column(name = "token")
	 private String transactionId;
	 
	
	 @Column(name = "customer_id")
	 private Integer customerId;
	
	 @Column(name = "order_id")
	 private Integer orderId;
	 	
	 @Column(name = "merchant_id")
	 private Integer merchantId;
	 
	 
	 @Column(name = "firstDataRetRef")
	 private String firstDataRetRef;

	 @Column(name = "auth_code")
	 private String authCode;
	 
	 
	 public String getAuthCode() {
		return authCode;
	}

	public void setAuthCode(String authCode) {
		this.authCode = authCode;
	}
	 
	 public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}



	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public Integer getOrderId() {
		return orderId;
	}

	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}

	public Integer getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(Integer merchantId) {
		this.merchantId = merchantId;
	}

	public Integer getPaymentGatewayId() {
		return paymentGatewayId;
	}

	public void setPaymentGatewayId(Integer paymentGatewayId) {
		this.paymentGatewayId = paymentGatewayId;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public CardInfo getCardInfoId() {
		return cardInfoId;
	}

	public void setCardInfoId(CardInfo cardInfoId) {
		this.cardInfoId = cardInfoId;
	}

	@Column(name = "payment_gateway_id")
	    private Integer paymentGatewayId;
	
	 @OneToOne(fetch = FetchType.EAGER,cascade=CascadeType.MERGE)
	 @JoinColumn(name = "card_info_id", referencedColumnName = "id")
    private CardInfo cardInfoId;
	 
	 
	 
	 @Column(name = "pre_auth_token")
	    private String preAuthToken;
		
		
		public String getPreAuthToken() {
		return preAuthToken;
	}

	public void setPreAuthToken(String preAuthToken) {
		this.preAuthToken = preAuthToken;
	}

	public String getPostAuthToken() {
		return postAuthToken;
	}

	public void setPostAuthToken(String postAuthToken) {
		this.postAuthToken = postAuthToken;
	}

	public String getVoidToken() {
		return voidToken;
	}

	public void setVoidToken(String voidToken) {
		this.voidToken = voidToken;
	}

		public String getFirstDataRetRef() {
		return firstDataRetRef;
	}

	public void setFirstDataRetRef(String firstDataRetRef) {
		this.firstDataRetRef = firstDataRetRef;
	}

		@Column(name = "post_auth_token")
	    private String postAuthToken;
		
		
		@Column(name = "void_token")
	    private String voidToken;
	
		@Column(name = "is_refunded")
	    private Boolean isRefunded;

		
		public Date getRefundedDate() {
			return refundedDate;
		}

		public void setRefundedDate(Date refundedDate) {
			this.refundedDate = refundedDate;
		}

		public String getRefundedAuthCode() {
			return refundedAuthCode;
		}

		public void setRefundedAuthCode(String refundedAuthCode) {
			this.refundedAuthCode = refundedAuthCode;
		}

		@Column(name = "refunded_date")
	    private  Date refundedDate;
		
		@Column(name = "refunded_authCode")
	    private String refundedAuthCode;
		
	      @Column(name = "post_payezzey_transctionId")
	        private String postPayezzeyTransctionId;
		

		public String getPostPayezzeyTransctionId() {
            return postPayezzeyTransctionId;
        }

        public void setPostPayezzeyTransctionId(String postPayezzeyTransctionId) {
            this.postPayezzeyTransctionId = postPayezzeyTransctionId;
        }

        public Boolean getIsRefunded() {
			return isRefunded;
		}

		public void setIsRefunded(Boolean isRefunded) {
			this.isRefunded = isRefunded;
		}
		
	
}
