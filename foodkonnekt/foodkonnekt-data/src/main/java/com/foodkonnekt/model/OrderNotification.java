package com.foodkonnekt.model;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "order_notification")
public class OrderNotification {

	    @Id
	    @GeneratedValue(strategy = GenerationType.AUTO)
	    private Integer id;

	    public Integer getId() {
			return id;
		}

		public void setId(Integer id) {
			this.id = id;
		}

		public Merchant getMerchant() {
			return merchant;
		}

		public void setMerchant(Merchant merchant) {
			this.merchant = merchant;
		}

		public String getEmail() {
			return email;
		}

		public void setEmail(String email) {
			this.email = email;
		}

		public Integer getPendingStatus() {
			return pendingStatus;
		}

		public void setPendingStatus(Integer pendingStatus) {
			this.pendingStatus = pendingStatus;
		}

		public Integer getAppStatus() {
			return appStatus;
		}

		public void setAppStatus(Integer appStatus) {
			this.appStatus = appStatus;
		}

		public Integer getStatus() {
			return status;
		}

		public void setStatus(Integer status) {
			this.status = status;
		}

		@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
	    @JoinColumn(name = "merchant_id", referencedColumnName = "id")
	    private Merchant merchant;
	    
	    @Column(name = "email")
	    private String email;
	    
	    @Column(name = "pending_status")
	    private Integer pendingStatus;
	    
	    @Column(name = "app_status")
	    private Integer appStatus;
	    
	    @Column(name = "status")
	    private Integer status;
	    
	    @Basic
	    private Date createdOn;
	    
	    public Date getCreatedOn() {
			return createdOn;
		}

		public void setCreatedOn(Date createdOn) {
			this.createdOn = createdOn;
		}

		public Date getUpdatedOn() {
			return updatedOn;
		}

		public void setUpdatedOn(Date updatedOn) {
			this.updatedOn = updatedOn;
		}

		@Basic
	    private Date updatedOn;
		
		@Basic
	    private String mobile;

		public String getMobile() {
			return mobile;
		}

		public void setMobile(String mobile) {
			this.mobile = mobile;
		}
	    
	 
}
