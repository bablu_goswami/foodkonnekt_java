package com.foodkonnekt.model;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "pos_integration")
public class PosIntegration {

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id;

	@Column(name = "merchant_id")
	private Integer merchantId;

	@Column(name = "scheduler_status")
	private Integer schedulerStatus;
	
	
	@Column(name = "app_version")
	private String appVersion;
	 	
	
	@Column(name = "notification_status")
	private Boolean notificationStatus;

	@Column(name = "order_cancel_time")
	private Integer orderCancelTime;
	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAppVersion() {
		return appVersion;
	}

	public void setAppVersion(String appVersion) {
		this.appVersion = appVersion;
	}



	public Integer getOrderCancelTime() {
		return orderCancelTime;
	}

	public void setOrderCancelTime(Integer orderCancelTime) {
		this.orderCancelTime = orderCancelTime;
	}

	public Integer getMerchantId() {
		return merchantId;
	}

	public Boolean getNotificationStatus() {
		return notificationStatus;
	}

	public void setNotificationStatus(Boolean notificationStatus) {
		this.notificationStatus = notificationStatus;
	}

	public void setMerchantId(Integer merchantId) {
		this.merchantId = merchantId;
	}

	public Integer getSchedulerStatus() {
		return schedulerStatus;
	}

	public void setSchedulerStatus(Integer schedulerStatus) {
		this.schedulerStatus = schedulerStatus;
	}

}
