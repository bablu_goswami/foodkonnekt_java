package com.foodkonnekt.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Granbury_locationUid")
public class GranburySolution {

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLocatinUid() {
		return locatinUid;
	}

	public void setLocatinUid(String locatinUid) {
		this.locatinUid = locatinUid;
	}

	@Column(name = "locatin_Uid")
    private String locatinUid;
	
	@Column(name = "autorization_Token")
    private String autorizationToken;

	public String getAutorizationToken() {
		return autorizationToken;
	}

	public void setAutorizationToken(String autorizationToken) {
		this.autorizationToken = autorizationToken;
	}
	
	@Column(name = "merchant_id")
    private Integer merchantId;

	public Integer getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(Integer merchantId) {
		this.merchantId = merchantId;
	}
}
