package com.foodkonnekt.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "pizzatopping")
public class PizzaTopping {

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true)
	private Integer id;

	private String description;

	private int active;

	private String posPizzaToppingId;

	@OneToMany(mappedBy = "pizzaTopping", fetch = FetchType.EAGER)
	private List<PizzaToppingSize> pizzaToppingSizes;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "merchant_id", referencedColumnName = "id")
	private Merchant merchant;

	@Transient
	private List<Integer> sizeIds;
	
	@Transient
	private List<Double> sizePrice;
	
	public List<Integer> getSizeIds() {
		return sizeIds;
	}

	public void setSizeIds(List<Integer> sizeIds) {
		this.sizeIds = sizeIds;
	}

	public List<Double> getSizePrice() {
		return sizePrice;
	}

	public void setSizePrice(List<Double> sizePrice) {
		this.sizePrice = sizePrice;
	}
	public Merchant getMerchant() {
		return merchant;
	}

	public void setMerchant(Merchant merchant) {
		this.merchant = merchant;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getActive() {
		return active;
	}

	public void setActive(int active) {
		this.active = active;
	}

	public String getPosPizzaToppingId() {
		return posPizzaToppingId;
	}

	public void setPosPizzaToppingId(String posPizzaToppingId) {
		this.posPizzaToppingId = posPizzaToppingId;
	}

	public List<PizzaToppingSize> getPizzaToppingSizes() {
		return pizzaToppingSizes;
	}

	public void setPizzaToppingSizes(List<PizzaToppingSize> pizzaToppingSizes) {
		this.pizzaToppingSizes = pizzaToppingSizes;
	}

	@Transient
	private String status;
	@Transient
	private String action;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	@Transient
	private List<PizzaTemplateTopping> pizzaTemplateToppings;

	public List<PizzaTemplateTopping> getPizzaTemplateToppings() {
		return pizzaTemplateToppings;
	}

	public void setPizzaTemplateToppings(List<PizzaTemplateTopping> pizzaTemplateToppings) {
		this.pizzaTemplateToppings = pizzaTemplateToppings;
	}
	
	public PizzaTemplateTopping getPizzaTemplateTopping() {
		return pizzaTemplateTopping;
	}

	public void setPizzaTemplateTopping(PizzaTemplateTopping pizzaTemplateTopping) {
		this.pizzaTemplateTopping = pizzaTemplateTopping;
	}

	@Transient
	private PizzaTemplateTopping pizzaTemplateTopping;
	
	@Transient
	private List<Integer> templateId;
	
	@Transient
	private List<Integer> toppingId;
	
	public List<Integer> getTemplateId() {
		return templateId;
	}

	public void setTemplateId(List<Integer> templateId) {
		this.templateId = templateId;
	}

	public List<Integer> getToppingId() {
		return toppingId;
	}

	public void setToppingId(List<Integer> toppingId) {
		this.toppingId = toppingId;
	}

	public List<Integer> getSizeId() {
		return sizeId;
	}

	public void setSizeId(List<Integer> sizeId) {
		this.sizeId = sizeId;
	}

	@Transient
	private List<Integer> sizeId;
	@Transient
	private List<Double> price;

	public List<Double> getPrice() {
		return price;
	}

	public void setPrice(List<Double> price) {
		this.price = price;
	}
	
	@Transient
	private String size;

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}
	
	@Transient
    private double sizeprice;

	public double getSizeprice() {
		return sizeprice;
	}

	public void setSizeprice(double sizeprice) {
		this.sizeprice = sizeprice;
	}
	
	
	
}
