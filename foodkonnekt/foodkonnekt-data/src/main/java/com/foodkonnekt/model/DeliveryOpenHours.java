package com.foodkonnekt.model;






public class DeliveryOpenHours  {

	
	private String sTimeToSave;

    
    private String eTimeToSave;
    
	
    private String selectedDay;
    
    private Integer allowDeliveryTimings;

	public Integer getAllowDeliveryTimings() {
		return allowDeliveryTimings;
	}

	public void setAllowDeliveryTimings(Integer allowDeliveryTimings) {
		this.allowDeliveryTimings = allowDeliveryTimings;
	}

	public String getsTimeToSave() {
		return sTimeToSave;
	}

	public void setsTimeToSave(String sTimeToSave) {
		this.sTimeToSave = sTimeToSave;
	}

	public String geteTimeToSave() {
		return eTimeToSave;
	}

	public void seteTimeToSave(String eTimeToSave) {
		this.eTimeToSave = eTimeToSave;
	}

	public String getSelectedDay() {
		return selectedDay;
	}

	public void setSelectedDay(String selectedDay) {
		this.selectedDay = selectedDay;
	}

}

