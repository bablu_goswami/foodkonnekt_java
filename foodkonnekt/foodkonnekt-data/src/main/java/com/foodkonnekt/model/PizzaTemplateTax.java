package com.foodkonnekt.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "pizza_template_taxes")
public class PizzaTemplateTax {

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "taxrate_id", referencedColumnName = "id")
	private TaxRates taxRates;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "template_id", referencedColumnName = "id")
	private PizzaTemplate pizzaTemplate;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public TaxRates getTaxRates() {
		return taxRates;
	}

	public void setTaxRates(TaxRates taxRates) {
		this.taxRates = taxRates;
	}

	public PizzaTemplate getPizzaTemplate() {
		return pizzaTemplate;
	}

	public void setPizzaTemplate(PizzaTemplate pizzaTemplate) {
		this.pizzaTemplate = pizzaTemplate;
	}

	public Integer getIsActive() {
		return isActive;
	}

	public void setIsActive(Integer isActive) {
		this.isActive = isActive;
	}

	public List<Integer> getTemplateId() {
		return templateId;
	}

	public void setTemplateId(List<Integer> templateId) {
		this.templateId = templateId;
	}

	public List<Integer> getTaxId() {
		return taxId;
	}

	public void setTaxId(List<Integer> taxId) {
		this.taxId = taxId;
	}

	@Column(name = "is_active")
	private Integer isActive;
	
	
	@Transient
	private List<Integer> templateId;
	
	@Transient
	private List<Integer> taxId;
	
}
