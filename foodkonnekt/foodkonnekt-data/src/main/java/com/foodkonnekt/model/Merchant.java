package com.foodkonnekt.model;

import static javax.persistence.GenerationType.IDENTITY;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "merchant")
public class Merchant {

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id;

	@Column(name = "pos_merchant_id")
	private String posMerchantId;

	@Column(name = "name")
	private String name;

	@Column(name = "employeePosId")
	private String employeePosId;

	@Column(name = "modile_device_id")
	private String modileDeviceId;
	
	@Column(name = "isFBAppPublished")
	private Boolean isFBAppPublished = false;
	
	@Column(name = "modie_imei_code")
	private String modieImeiCode;

	public String getModileDeviceId() {
		return modileDeviceId;
	}

	public void setModileDeviceId(String modileDeviceId) {
		this.modileDeviceId = modileDeviceId;
	}

	public String getModieImeiCode() {
		return modieImeiCode;
	}

	public void setModieImeiCode(String modieImeiCode) {
		this.modieImeiCode = modieImeiCode;
	}
	
	@Column(name = "allowauxrax")
	private Integer allowAuxTax;
	
	
	

	public Boolean getIsFBAppPublished() {
		return isFBAppPublished;
	}

	public void setIsFBAppPublished(Boolean isFBAppPublished) {
		this.isFBAppPublished = isFBAppPublished;
	}

	@Column(name = "fBTabIds")
	private String fBTabIds;

	@Column(name = "allowReOrderFunctionality")
	private Boolean allowReOrder;

	public Boolean getAllowReOrder() {
		return allowReOrder;
	}

	public void setAllowReOrder(Boolean allowReOrder) {
		this.allowReOrder = allowReOrder;
	}

	public String getfBTabIds() {
		return fBTabIds;
	}

	public void setfBTabIds(String fBTabIds) {
		this.fBTabIds = fBTabIds;
	}

	public String getEmployeePosId() {
		return employeePosId;
	}

	public void setEmployeePosId(String employeePosId) {
		this.employeePosId = employeePosId;
	}

	@Column(name = "allow_future_order")
	private Integer allowFutureOrder;

	@Column(name = "active_customer_feedback")
	private Integer activeCustomerFeedback = 0;

	public Integer getActiveCustomerFeedback() {
		return activeCustomerFeedback;
	}

	public void setActiveCustomerFeedback(Integer activeCustomerFeedback) {
		this.activeCustomerFeedback = activeCustomerFeedback;
	}

	@Column(name = "future_days_ahead")
	private Integer futureDaysAhead = 10;

	public Integer getFutureDaysAhead() {
		return futureDaysAhead;
	}

	public void setFutureDaysAhead(Integer futureDaysAhead) {
		this.futureDaysAhead = futureDaysAhead;
	}

	public Integer getAllowFutureOrder() {
		return allowFutureOrder;
	}

	public void setAllowFutureOrder(Integer allowFutureOrder) {
		this.allowFutureOrder = allowFutureOrder;
	}

	@Column(name = "created_date")
	private String createdDate;

	@Column(name = "updated_date")
	private String updatedDate;

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(String updatedDate) {
		this.updatedDate = updatedDate;
	}

	@Column(name = "phone_number")
	private String phoneNumber;

	@Transient
	private String address;
	
	@Transient
	private String action;

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	@Transient
	private Integer totalOrders;

	@Transient
	private Integer cashOrder;

	@Transient
	private Integer creditOrder;

	@Transient
	private Integer pickup;

	@Transient
	private Integer delivery;

	public Integer getCreditOrder() {
		return creditOrder;
	}

	public void setCreditOrder(Integer creditOrder) {
		this.creditOrder = creditOrder;
	}

	public Integer getPickup() {
		return pickup;
	}

	public void setPickup(Integer pickup) {
		this.pickup = pickup;
	}

	public Integer getDelivery() {
		return delivery;
	}

	public void setDelivery(Integer delivery) {
		this.delivery = delivery;
	}

	public Integer getCashOrder() {
		return cashOrder;
	}

	public void setCashOrder(Integer cashOrder) {
		this.cashOrder = cashOrder;
	}

	@Transient
	private Double totalOrdersPrice;

	public Double getTotalOrdersPrice() {
		return totalOrdersPrice;
	}

	public void setTotalOrdersPrice(Double totalOrdersPrice) {
		this.totalOrdersPrice = totalOrdersPrice;
	}

	@Transient
	private String status;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getTotalOrders() {
		return totalOrders;
	}

	public void setTotalOrders(Integer totalOrders) {
		this.totalOrders = totalOrders;
	}

	@Column(name = "storeId")
	private String storeId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "subscription_id", referencedColumnName = "id")
	private Subscription subscription;

	@OneToMany(mappedBy = "merchant", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
	private List<Item> items;

	@OneToMany(mappedBy = "merchant", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
	private List<Address> addresses;

	@OneToMany(mappedBy = "merchant", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
	private List<PaymentMode> paymentModes;

	@OneToMany(mappedBy = "merchant", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
	private List<OpeningClosingDay> openingClosingDays;

	@OneToMany(mappedBy = "merchant", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
	private List<Vouchar> vouchars;

	@OneToMany(mappedBy = "merchant", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
	private List<OrderType> orderTypes;

	@OneToMany(mappedBy = "merchant", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
	private List<MerchantSubscription> merchantSubscriptions;

	@OneToMany(mappedBy = "merchant", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
	private List<Modifiers> modifiers;

	@OneToMany(mappedBy = "merchant", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
	private List<ModifierGroup> modifierGroups;

	@OneToMany(mappedBy = "merchant", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
	private List<OrderR> orderRs;

	@OneToMany(mappedBy = "merchant", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
	private List<TaxRates> taxRates;

	@OneToOne(mappedBy = "merchant", cascade = CascadeType.ALL)
	private MerchantLogin merchantLogin;

	@OneToOne(mappedBy = "merchant", cascade = CascadeType.ALL)
	private SocialMediaLinks socialMediaLinks;

	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
	@JoinColumn(name = "time_zone_id", referencedColumnName = "id")
	private TimeZone timeZone;

	public TimeZone getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(TimeZone timeZone) {
		this.timeZone = timeZone;
	}

	public SocialMediaLinks getSocialMediaLinks() {
		return socialMediaLinks;
	}

	public void setSocialMediaLinks(SocialMediaLinks socialMediaLinks) {
		this.socialMediaLinks = socialMediaLinks;
	}

	public MerchantLogin getMerchantLogin() {
		return merchantLogin;
	}

	public void setMerchantLogin(MerchantLogin merchantLogin) {
		this.merchantLogin = merchantLogin;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "vendor_id", referencedColumnName = "id")
	private Vendor owner;

	@Column(name = "merchant_logo")
	private String merchantLogo;

	@Column(name = "accessToken")
	private String accessToken;

	@Column(name = "is_install")
	private Integer isInstall;

	@Column(name = "merchant_uid")
	private String merchantUid;

	@Column(name = "allow_multiple_koupon")
	private Integer allowMultipleKoupon;

	@Column(name = "allowMultiPay")
	private Boolean allowMultiPay;

	@Column(name = "website")
	private String website;

	@Column(name = "birthday_koupon_url")
	private String birthdayKouponUrl;
	
	@Column(name ="mailchimp_api_key")
	private String mailchimpApiKey;
	
	@Column(name="allow_Delivery_Timing")
	private Integer allowDeliveryTiming;

	public String getBirthdayKouponUrl() {
		return birthdayKouponUrl;
	}

	public void setBirthdayKouponUrl(String birthdayKouponUrl) {
		this.birthdayKouponUrl = birthdayKouponUrl;
	}

	public Boolean getAllowMultiPay() {
		return allowMultiPay;
	}

	public void setAllowMultiPay(Boolean allowMultiPay) {
		this.allowMultiPay = allowMultiPay;
	}

	public Integer getAllowMultipleKoupon() {
		return allowMultipleKoupon;
	}

	public void setAllowMultipleKoupon(Integer allowMultipleKoupon) {
		this.allowMultipleKoupon = allowMultipleKoupon;
	}

	public String getMerchantUid() {
		return merchantUid;
	}

	public void setMerchantUid(String merchantUid) {
		this.merchantUid = merchantUid;
	}

	public Subscription getSubscription() {
		return subscription;
	}

	public void setSubscription(Subscription subscription) {
		this.subscription = subscription;
	}

	public List<Item> getItems() {
		return items;
	}

	public void setItems(List<Item> items) {
		this.items = items;
	}

	public List<PaymentMode> getPaymentModes() {
		return paymentModes;
	}

	public void setPaymentModes(List<PaymentMode> paymentModes) {
		this.paymentModes = paymentModes;
	}

	public List<ModifierGroup> getModifierGroups() {
		return modifierGroups;
	}

	public void setModifierGroups(List<ModifierGroup> modifierGroups) {
		this.modifierGroups = modifierGroups;
	}

	public List<OrderR> getOrderRs() {
		return orderRs;
	}

	public void setOrderRs(List<OrderR> orderRs) {
		this.orderRs = orderRs;
	}

	public List<TaxRates> getTaxRates() {
		return taxRates;
	}

	public void setTaxRates(List<TaxRates> taxRates) {
		this.taxRates = taxRates;
	}

	public List<Modifiers> getModifiers() {
		return modifiers;
	}

	public void setModifiers(List<Modifiers> modifiers) {
		this.modifiers = modifiers;
	}

	public List<Vouchar> getVouchars() {
		return vouchars;
	}

	public void setVouchars(List<Vouchar> vouchars) {
		this.vouchars = vouchars;
	}

	public List<OrderType> getOrderTypes() {
		return orderTypes;
	}

	public void setOrderTypes(List<OrderType> orderTypes) {
		this.orderTypes = orderTypes;
	}

	public List<MerchantSubscription> getMerchantSubscriptions() {
		return merchantSubscriptions;
	}

	public void setMerchantSubscriptions(List<MerchantSubscription> merchantSubscriptions) {
		this.merchantSubscriptions = merchantSubscriptions;
	}

	public List<OpeningClosingDay> getOpeningClosingDays() {
		return openingClosingDays;
	}

	public void setOpeningClosingDays(List<OpeningClosingDay> openingClosingDays) {
		this.openingClosingDays = openingClosingDays;
	}

	public List<Address> getAddresses() {
		return addresses;
	}

	public void setAddresses(List<Address> addresses) {
		this.addresses = addresses;
	}

	public String getStoreId() {
		return storeId;
	}

	public void setStoreId(String storeId) {
		this.storeId = storeId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getPosMerchantId() {
		return posMerchantId;
	}

	public void setPosMerchantId(String posMerchantId) {
		this.posMerchantId = posMerchantId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public Vendor getOwner() {
		return owner;
	}

	public void setOwner(Vendor owner) {
		this.owner = owner;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getMerchantLogo() {
		return merchantLogo;
	}

	public void setMerchantLogo(String merchantLogo) {
		this.merchantLogo = merchantLogo;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Integer getIsInstall() {
		return isInstall;
	}

	public void setIsInstall(Integer isInstall) {
		this.isInstall = isInstall;
	}

	public Integer getAllowAuxTax() {
		return allowAuxTax;
	}

	public void setAllowAuxTax(Integer allowAuxTax) {
		this.allowAuxTax = allowAuxTax;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getMailchimpApiKey() {
		return mailchimpApiKey;
	}

	public void setMailchimpApiKey(String mailchimpApiKey) {
		this.mailchimpApiKey = mailchimpApiKey;
	}

	public Integer getAllowDeliveryTiming() {
		return allowDeliveryTiming;
	}

	public void setAllowDeliveryTiming(Integer allowDeliveryTiming) {
		this.allowDeliveryTiming = allowDeliveryTiming;
	}
	
	public String getCouponCode() {
		return couponCode;
	}

	public void setCouponCode(String couponCode) {
		this.couponCode = couponCode;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public Integer getDeliverystatus() {
		return deliverystatus;
	}

	public void setDeliverystatus(Integer deliverystatus) {
		this.deliverystatus = deliverystatus;
	}

	@Column(name="coupon_code")
	private String couponCode;
	
	@Column(name="email_id")
	private String emailId;

	@Transient
	private Integer deliverystatus;
	
	@Column(name = "tip_for_pickup")
	private  Integer tipsForPickup;
	
	@Column(name = "tip_for_dilevery")
	private  Integer tipsForDilevery;
	
	@Column(name = "active_tip_for_pickup")
	private  boolean activeTipsForPickup;
	
	@Column(name = "active_tip_for_dilevery")
	private  boolean activeTipForDilevery;
	
	public Integer getTipsForPickup() {
		return tipsForPickup;
	}

	public void setTipsForPickup(Integer tipsForPickup) {
		this.tipsForPickup = tipsForPickup;
	}

	public Integer getTipsForDilevery() {
		return tipsForDilevery;
	}

	public void setTipsForDilevery(Integer tipsForDilevery) {
		this.tipsForDilevery = tipsForDilevery;
	}

	public boolean isActiveTipsForPickup() {
		return activeTipsForPickup;
	}

	public void setActiveTipsForPickup(boolean activeTipsForPickup) {
		this.activeTipsForPickup = activeTipsForPickup;
	}

	public boolean isActiveTipForDilevery() {
		return activeTipForDilevery;
	}

	public void setActiveTipForDilevery(boolean activeTipForDilevery) {
		this.activeTipForDilevery = activeTipForDilevery;
	}

    @Transient
    public List<OrderR> ordersList;

    @Transient
	public String toMail;
    
    @Transient
	public String partnerName;
    
	public List<OrderR> getOrdersList() {
		return ordersList;
	}

	public void setOrdersList(List<OrderR> ordersList) {
		this.ordersList = ordersList;
	}

	public String getToMail() {
		return toMail;
	}

	public void setToMail(String toMail) {
		this.toMail = toMail;
	}

	public String getPartnerName() {
		return partnerName;
	}

	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}
    
}
