package com.foodkonnekt.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "pizzatemplatecrust")
public class PizzaTemplateCrust {

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id")
	private Integer id;

	@Column
	private double price;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "pizzatemplate_id", referencedColumnName = "id")
	private PizzaTemplate pizzaTemplate;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "pizzacrust_id", referencedColumnName = "id")
	private PizzaCrust pizzaCrust;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public PizzaTemplate getPizzaTemplate() {
		return pizzaTemplate;
	}

	public void setPizzaTemplate(PizzaTemplate pizzaTemplate) {
		this.pizzaTemplate = pizzaTemplate;
	}

	public PizzaCrust getPizzaCrust() {
		return pizzaCrust;
	}

	public void setPizzaCrust(PizzaCrust pizzaCrust) {
		this.pizzaCrust = pizzaCrust;
	}

	@Transient
	private String pizza;
	
	@Transient
	private String crust;

public String getPizza() {
		return pizza;
	}

	public void setPizza(String pizza) {
		this.pizza = pizza;
	}

	public String getCrust() {
		return crust;
	}

	public void setCrust(String crust) {
		this.crust = crust;
	}
	
	@Transient
	private String status;

	@Transient
	private String action;

    public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}
	
	@Transient
	private List<Integer> crustId;
	
	@Transient
	private List<Integer> templateId;

	public List<Integer> getCrustId() {
		return crustId;
	}

	public void setCrustId(List<Integer> crustId) {
		this.crustId = crustId;
	}

	public List<Integer> getTemplateId() {
		return templateId;
	}

	public void setTemplateId(List<Integer> templateId) {
		this.templateId = templateId;
	}

	@Column(name="active")
    private int active;

	public int getActive() {
		return active;
	}

	public void setActive(int active) {
		this.active = active;
	}
	
	

	
	
}
