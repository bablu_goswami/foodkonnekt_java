package com.foodkonnekt.model;

public class LatLongHolder
{
	private Double lat;
	private Double lng;
	
	public Double getLng() {
		return lng;
	}
	public void setLng(Double lng) {
		this.lng = lng;
	}
	
	@Override
	public String toString() {
		return "LatLongHolder [lat=" + lat + ", lng=" + lng +"]";
	}
	public Double getLat() {
		return lat;
	}
	public void setLat(Double lat) {
		this.lat = lat;
	}
	
	

}
