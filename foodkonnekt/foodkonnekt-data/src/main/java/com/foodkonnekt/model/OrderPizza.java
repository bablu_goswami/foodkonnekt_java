package com.foodkonnekt.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "orderpizza")
public class OrderPizza {

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id;

	@Column(name = "quantity")
	private Integer quantity;

	@Column(name = "createdOn")
	private Date createdOn;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "order_id", referencedColumnName = "id")
	private OrderR order;

	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
	@JoinColumn(name = "pizza_template_id", referencedColumnName = "id")
	private PizzaTemplate pizzaTemplate;

	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
	@JoinColumn(name = "pizza_size_id", referencedColumnName = "id")
	private PizzaSize pizzaSize;

	@OneToMany(mappedBy = "orderPizza", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<OrderPizzaToppings> orderPizzaToppings;

	@Column(name = "aux_tax")
	private String auxTax;

	@Column(name = "sales_tax")
	private String salesTax;
	
	@Column(name = "price")
	private Double price;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public OrderR getOrder() {
		return order;
	}

	public void setOrder(OrderR order) {
		this.order = order;
	}

	public PizzaTemplate getPizzaTemplate() {
		return pizzaTemplate;
	}

	public void setPizzaTemplate(PizzaTemplate pizzaTemplate) {
		this.pizzaTemplate = pizzaTemplate;
	}

	public PizzaSize getPizzaSize() {
		return pizzaSize;
	}

	public void setPizzaSize(PizzaSize pizzaSize) {
		this.pizzaSize = pizzaSize;
	}

	public String getAuxTax() {
		return auxTax;
	}

	public void setAuxTax(String auxTax) {
		this.auxTax = auxTax;
	}

	public String getSalesTax() {
		return salesTax;
	}

	public void setSalesTax(String salesTax) {
		this.salesTax = salesTax;
	}

	public List<OrderPizzaToppings> getOrderPizzaToppings() {
		return orderPizzaToppings;
	}

	public void setOrderPizzaToppings(List<OrderPizzaToppings> orderPizzaToppings) {
		this.orderPizzaToppings = orderPizzaToppings;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}
}
