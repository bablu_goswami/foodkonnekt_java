package com.foodkonnekt.model;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "order_pizza_crust")
public class OrderPizzaCrust {  @Id
  
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "quantity")
    private Integer quantity;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "order_pizza_id", referencedColumnName = "id")
    private OrderPizza orderPizza;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "crust_id", referencedColumnName = "id")
    private PizzaCrust pizzacrust;
    
    @Column(name= "price")
    private Double price;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public PizzaCrust getPizzacrust() {
        return pizzacrust;
    }

    public void setPizzacrust(PizzaCrust pizzacrust) {
        this.pizzacrust = pizzacrust;
    }

    public OrderPizza getOrderPizza() {
        return orderPizza;
    }

    public void setOrderPizza(OrderPizza orderPizza) {
        this.orderPizza = orderPizza;
    }
}
