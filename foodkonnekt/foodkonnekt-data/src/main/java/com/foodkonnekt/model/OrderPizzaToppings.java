package com.foodkonnekt.model;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "orderpizzatoppings")
public class OrderPizzaToppings {
	@Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "quantity")
    private Integer quantity;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "order_pizza_id", referencedColumnName = "id")
    private OrderPizza orderPizza;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "pizza_topping_id", referencedColumnName = "id")
	private PizzaTopping pizzaTopping;
    
    @Column(name= "side1")
    private Boolean side1;
    
    @Column(name= "side2")
    private Boolean side2;
    
    @Column(name= "whole")
    private Boolean whole;
    
    @Column(name= "price")
    private Double price;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public OrderPizza getOrderPizza() {
		return orderPizza;
	}

	public void setOrderPizza(OrderPizza orderPizza) {
		this.orderPizza = orderPizza;
	}

	public PizzaTopping getPizzaTopping() {
		return pizzaTopping;
	}

	public void setPizzaTopping(PizzaTopping pizzaTopping) {
		this.pizzaTopping = pizzaTopping;
	}

	public Boolean getSide1() {
		return side1;
	}

	public void setSide1(Boolean side1) {
		this.side1 = side1;
	}

	public Boolean getSide2() {
		return side2;
	}

	public void setSide2(Boolean side2) {
		this.side2 = side2;
	}

	public Boolean getWhole() {
		return whole;
	}

	public void setWhole(Boolean whole) {
		this.whole = whole;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}
}
