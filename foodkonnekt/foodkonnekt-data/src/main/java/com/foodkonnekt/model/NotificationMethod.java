package com.foodkonnekt.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "notification_method")
public class NotificationMethod implements Serializable {

	private static final long serialVersionUID = 1L;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public NotificationMethodType getNotificationMethodType() {
		return notificationMethodType;
	}

	public void setNotificationMethodType(NotificationMethodType notificationMethodType) {
		this.notificationMethodType = notificationMethodType;
	}

	public Merchant getMerchant() {
		return merchant;
	}

	public void setMerchant(Merchant merchant) {
		this.merchant = merchant;
	}

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
	@JoinColumn(name = "notification_method_type_id", referencedColumnName = "id")
	private NotificationMethodType notificationMethodType;

	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
	@JoinColumn(name = "merchant_id", referencedColumnName = "id")
	private Merchant merchant;

	@Column(name = "contact_name")
	private String contactName;

	@Column(name = "sms_requestId")
	private String smsRequestId;

	public String getSmsRequestId() {
		return smsRequestId;
	}

	public void setSmsRequestId(String smsRequestId) {
		this.smsRequestId = smsRequestId;
	}

	@Column(name = "contact")
	private String contact;

	@Column(name = "created_date")
	private Date createdDate;

	@Column(name = "updated_date")
	private Date updatedDate;

	@Column(name = "is_active")
	private boolean active;

	@Column(name = "is_accepted")
	private int isOrderAccepted;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getIsOrderAccepted() {
		return isOrderAccepted;
	}

	public void setIsOrderAccepted(int isOrderAccepted) {
		this.isOrderAccepted = isOrderAccepted;
	}

	@Transient
	private String status;

}
