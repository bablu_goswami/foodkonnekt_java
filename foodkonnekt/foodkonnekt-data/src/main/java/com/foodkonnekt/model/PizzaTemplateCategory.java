package com.foodkonnekt.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "pizza_template_category")
public class PizzaTemplateCategory {

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "category_id", referencedColumnName = "id")
	private Category category;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "pizza_template_id", referencedColumnName = "id")
	private PizzaTemplate pizzaTemplate;

	@Column(name = "serviceCategoryId")
	private Integer serviceCategoryId;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getServiceCategoryId() {
		return serviceCategoryId;
	}

	public void setServiceCategoryId(Integer serviceCategoryId) {
		this.serviceCategoryId = serviceCategoryId;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public PizzaTemplate getPizzaTemplate() {
		return pizzaTemplate;
	}

	public void setPizzaTemplate(PizzaTemplate pizzaTemplate) {
		this.pizzaTemplate = pizzaTemplate;
	}
	@Transient
    private List<Integer> pId;
    
    public List<Integer> getpId() {
		return pId;
	}

	public void setpId(List<Integer> pId) {
		this.pId = pId;
	}

	public List<Integer> getcId() {
		return cId;
	}

	public void setcId(List<Integer> cId) {
		this.cId = cId;
	}

	@Transient
    private List<Integer> cId;

	@Transient
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
}
