package com.foodkonnekt.model;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "pizzatemplatetopping")
public class PizzaTemplateTopping {

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id")
	private Integer id;

	@Column
	private Integer posPizzaTemplateToppingId;

	/*
	 * @Column(name = "pizza_template_id") private Integer pizzaTemplateId;
	 * 
	 * @Column(name = "pizza_topping_id") private Integer pizzaToppingId;
	 */

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "pizza_template_id", referencedColumnName = "id")
	private PizzaTemplate pizzaTemplate;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "pizza_topping_id", referencedColumnName = "id")
	private PizzaTopping pizzaTopping;

	public PizzaTemplate getPizzaTemplate() {
		return pizzaTemplate;
	}

	public void setPizzaTemplate(PizzaTemplate pizzaTemplate) {
		this.pizzaTemplate = pizzaTemplate;
	}

	public PizzaTopping getPizzaTopping() {
		return pizzaTopping;
	}

	public void setPizzaTopping(PizzaTopping pizzaTopping) {
		this.pizzaTopping = pizzaTopping;
	}

	@Column
	private double price = 0;

	@Column
	private boolean active;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getPosPizzaTemplateToppingId() {
		return posPizzaTemplateToppingId;
	}

	public void setPosPizzaTemplateToppingId(Integer posPizzaTemplateToppingId) {
		this.posPizzaTemplateToppingId = posPizzaTemplateToppingId;
	}

	/*
	 * public Integer getPizzaTemplateId() { return pizzaTemplateId; }
	 * 
	 * public void setPizzaTemplateId(Integer pizzaTemplateId) {
	 * this.pizzaTemplateId = pizzaTemplateId; }
	 * 
	 * public Integer getPizzaToppingId() { return pizzaToppingId; }
	 * 
	 * public void setPizzaToppingId(Integer pizzaToppingId) {
	 * this.pizzaToppingId = pizzaToppingId; }
	 */

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public boolean getActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	@Column(name = "is_included")
	private Boolean isIncluded = false;

	@Column(name = "is_replacable")
	private Boolean isReplacable = false;

	public boolean isIncluded() {
		return isIncluded;
	}

	public void setIncluded(boolean isIncluded) {
		this.isIncluded = isIncluded;
	}

	public boolean isReplacable() {
		return isReplacable;
	}

	public void setReplacable(boolean isReplacable) {
		this.isReplacable = isReplacable;
	}
}
