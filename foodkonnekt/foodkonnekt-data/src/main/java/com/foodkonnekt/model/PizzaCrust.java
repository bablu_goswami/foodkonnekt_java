package com.foodkonnekt.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "pizzacrust")
public class PizzaCrust {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id")
    private Integer id;

    private String description;

    private int active;

    private String posPizzaCrustId;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "merchant_id", referencedColumnName = "id")
    private Merchant merchant;

    @OneToMany(mappedBy = "pizzaCrust", fetch = FetchType.LAZY)
    private List<PizzaCrustSizes> pizzaCrustSizes;
    
    @Transient
    private Double price;
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }

    public String getPosPizzaCrustId() {
        return posPizzaCrustId;
    }

    public void setPosPizzaCrustId(String posPizzaCrustId) {
        this.posPizzaCrustId = posPizzaCrustId;
    }

    public Merchant getMerchant() {
        return merchant;
    }

    public void setMerchant(Merchant merchant) {
        this.merchant = merchant;
    }

    public List<PizzaCrustSizes> getPizzaCrustSizes() {
        return pizzaCrustSizes;
    }

    public void setPizzaCrustSizes(List<PizzaCrustSizes> pizzaCrustSizes) {
        this.pizzaCrustSizes = pizzaCrustSizes;
    }
    
        public List<PizzaTemplateCrust> getPizzaTemplateCrusts() {
        return pizzaTemplateCrusts;
    }

    public void setPizzaTemplateCrusts(List<PizzaTemplateCrust> pizzaTemplateCrusts) {
        this.pizzaTemplateCrusts = pizzaTemplateCrusts;
    }

    @Transient
    private List<PizzaTemplateCrust> pizzaTemplateCrusts;
    
    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public List<Integer> getTempId() {
		return tempId;
	}

	public void setTempId(List<Integer> tempId) {
		this.tempId = tempId;
	}

	@Transient
    private List<Integer> tempId;
	
	@Transient
	private String status;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	@Transient
	private List<Double> sizePrice;
	
	
    
    public List<Double> getSizePrice() {
		return sizePrice;
	}

	public void setSizePrice(List<Double> sizePrice) {
		this.sizePrice = sizePrice;
	}
	@Transient
	private List<Integer> sizeId;

	public List<Integer> getSizeId() {
		return sizeId;
	}

	public void setSizeId(List<Integer> sizeId) {
		this.sizeId = sizeId;
	}
	
	@Transient
	private List<Double> pricelist;

	public List<Double> getPricelist() {
		return pricelist;
	}

	public void setPricelist(List<Double> pricelist) {
		this.pricelist = pricelist;
	}
	
	@Transient
    private List<Integer> mappizzaId;
    
    @Transient
    private List<Integer> unmappizzaId;

    
    
	public List<Integer> getMappizzaId() {
		return mappizzaId;
	}

	public void setMappizzaId(List<Integer> mappizzaId) {
		this.mappizzaId = mappizzaId;
	}

	public List<Integer> getUnmappizzaId() {
		return unmappizzaId;
	}

	public void setUnmappizzaId(List<Integer> unmappizzaId) {
		this.unmappizzaId = unmappizzaId;
	}

	@Transient
	private String pizza;

public String getPizza() {
		return pizza;
	}

	public void setPizza(String pizza) {
		this.pizza = pizza;
	}
	
	@Transient
    private String action;
	
	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}
}
