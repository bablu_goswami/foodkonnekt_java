package com.foodkonnekt.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "item_category")
public class CategoryItem {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "category_id", referencedColumnName = "id")
    private Category category;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "item_id", referencedColumnName = "id")
    private Item item;
    
    @Basic
    @Column(length = 100)
    private Integer sortOrder=0;

    public Integer getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(Integer sortOrder) {
		this.sortOrder = sortOrder;
	}

	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }
    
    @Transient
    private List<Integer> iId;
    
    @Transient
    private List<Integer> cId;

	public List<Integer> getiId() {
		return iId;
	}

	public void setiId(List<Integer> iId) {
		this.iId = iId;
	}

	public List<Integer> getcId() {
		return cId;
	}

	public void setcId(List<Integer> cId) {
		this.cId = cId;
	}
	
	 @Column
	 private Integer active;

		public Integer getActive() {
			return active;
		}

		public void setActive(Integer active) {
			this.active = active;
		}
	

}
