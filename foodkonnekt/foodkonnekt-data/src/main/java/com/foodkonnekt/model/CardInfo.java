package com.foodkonnekt.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "cardInfo")
public class CardInfo {
	
	 @Id
	    @GeneratedValue(strategy = IDENTITY)
	    @Column(name = "id", unique = true, nullable = false)
	    private Integer id;
	
	 @Column(name = "lastfournum")
	    private String last4;
	 
	 @Column(name = "firstsixnum")
	    private String first6;
	 
	 @Column(name = "expirationdate")
	    private String expirationDate;
	 
	 @Column(name = "token")
	    private String token;
	 
	 @Column(name = "card_type")
	    private String cardType;
	 
	 @Column(name = "create_date")
	 private Date createDate;
	 
	 @Column(name = "deleted_date")
	 private Date deletedDate;
	
	 @Column(name = "update_date")
	 private Date updateDate;
	 
	 @Column(name = "is_active")
	 private boolean status;
	 
	 public Date getDeletedDate() {
		return deletedDate;
	}

	public void setDeletedDate(Date deletedDate) {
		this.deletedDate = deletedDate;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}















	@ManyToOne(fetch=FetchType.EAGER)
	 @JoinColumn(name="customer_id", referencedColumnName="id")
	 private Customer customer;
	
	@ManyToOne(fetch=FetchType.EAGER)
	 @JoinColumn(name="merchant_id", referencedColumnName="id")
	 private Merchant merchant;

	public Merchant getMerchant() {
		return merchant;
	}

	public void setMerchant(Merchant merchant) {
		this.merchant = merchant;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLast4() {
		return last4;
	}

	public void setLast4(String last4) {
		this.last4 = last4;
	}

	public String getFirst6() {
		return first6;
	}

	public void setFirst6(String first6) {
		this.first6 = first6;
	}

	public String getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(String expirationDate) {
		this.expirationDate = expirationDate;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	 
	
	public String getPreAuthToken() {
		return preAuthToken;
	}

	public void setPreAuthToken(String preAuthToken) {
		this.preAuthToken = preAuthToken;
	}


	public String getPostAuthToken() {
		return postAuthToken;
	}

	public void setPostAuthToken(String postAuthToken) {
		this.postAuthToken = postAuthToken;
	}


/*	public String getVoidToken() {
		return voidToken;
	}

	public void setVoidToken(String voidToken) {
		this.voidToken = voidToken;
	}
*/
	
	

	@Column(name = "pre_auth_token")
    private String preAuthToken;
	
	
	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}















	@Column(name = "post_auth_token")
    private String postAuthToken;
	
	
	@Column(name = "zip_Code")
    private String zipCode;

	
	 @Column(name = "first_data_token")
	    private String firstDataToken;
	 
	 @Column(name = "first_data_retref")
	    private String firstDataRetref;
	
	 @Column(name = "first_data_authcode")
	    private String firstDataAuthcode;

	public String getFirstDataToken() {
		return firstDataToken;
	}

	public void setFirstDataToken(String firstDataToken) {
		this.firstDataToken = firstDataToken;
	}

	public String getFirstDataRetref() {
		return firstDataRetref;
	}

	public void setFirstDataRetref(String firstDataRetref) {
		this.firstDataRetref = firstDataRetref;
	}

	public String getFirstDataAuthcode() {
		return firstDataAuthcode;
	}

	public void setFirstDataAuthcode(String firstDataAuthcode) {
		this.firstDataAuthcode = firstDataAuthcode;
	}
	
	@Column(name = "void_token")
    private String voidToken;
}
