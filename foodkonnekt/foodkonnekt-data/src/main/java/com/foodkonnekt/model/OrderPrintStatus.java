package com.foodkonnekt.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "order_print_status")
public class OrderPrintStatus {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	
	@Column(name = "orderId")
	private Integer orderId;
	
	@Column(name = "order_item_status")
	private boolean orderItemStatus;
	
	@Column(name = "payment_status")
	private boolean paymentStatus;
	
	@Column(name = "print_status")
	private boolean printStatus;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getOrderId() {
		return orderId;
	}

	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}

	public boolean isOrderItemStatus() {
		return orderItemStatus;
	}

	public void setOrderItemStatus(boolean orderItemStatus) {
		this.orderItemStatus = orderItemStatus;
	}

	public boolean isPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(boolean paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public boolean isPrintStatus() {
		return printStatus;
	}

	public void setPrintStatus(boolean printStatus) {
		this.printStatus = printStatus;
	}
	
}
