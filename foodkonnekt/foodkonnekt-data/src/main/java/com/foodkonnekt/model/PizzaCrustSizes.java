package com.foodkonnekt.model;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "pizza_crust_sizes")
public class PizzaCrustSizes {

    
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;
 
    private double price;
    
    private int active;
    
    private String posPizzaCrustSizeId;
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }

    public PizzaSize getPizzaSize() {
        return pizzaSize;
    }

    public void setPizzaSize(PizzaSize pizzaSize) {
        this.pizzaSize = pizzaSize;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "pizzasize_id", referencedColumnName = "id")
    private PizzaSize pizzaSize;
    
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "pizzacrust_id", referencedColumnName = "id")
    private PizzaCrust pizzaCrust;

    @Transient
    private String status;
    @Transient
    private String action;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    @Transient
    private String name;
    
    @Transient
    private String size;

    public String getPosPizzaCrustSizeId() {
        return posPizzaCrustSizeId;
    }

    public void setPosPizzaCrustSizeId(String posPizzaCrustSizeId) {
        this.posPizzaCrustSizeId = posPizzaCrustSizeId;
    }

    public PizzaCrust getPizzaCrust() {
        return pizzaCrust;
    }

    public void setPizzaCrust(PizzaCrust pizzaCrust) {
        this.pizzaCrust = pizzaCrust;
    }

}
