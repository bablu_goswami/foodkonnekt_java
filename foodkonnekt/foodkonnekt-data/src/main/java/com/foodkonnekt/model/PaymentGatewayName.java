package com.foodkonnekt.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "payment_gateway_name")
public class PaymentGatewayName {

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getPaymentGatewayName() {
		return paymentGatewayName;
	}

	public void setPaymentGatewayName(String paymentGatewayName) {
		this.paymentGatewayName = paymentGatewayName;
	}

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
	
	 @Column(name = "payment_gateway_name")
	    private String paymentGatewayName;
}
