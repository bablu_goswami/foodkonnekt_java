package com.foodkonnekt.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "payment_gateway")
public class PaymentGateWay {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	@Column(name = "stripe_API_Key")
	private String stripeAPIKey;

	@Column(name = "authorize_login_id")
	private String authorizeLoginId;

	@Column(name = "cayan_merchant_name")
	private String cayanMerchantName;

	@Column(name = "cayan_merchantsite_id")
	private String cayanMerchantSiteId;

	@Column(name = "cayan_merchant_key")
	private String cayanMerchantKey;

	@Column(name = "authorize_transaction_key")
	private String authorizeTransactionKey;
	
	
	@Column(name = "igate_user_name")
	private String iGateUserName;
	
	@Column(name = "igate_password")
	private String iGatePassword;
	

	@Column(name = "merchant_id")
	private Integer merchantId;

	@Column(name = "create_date")
	private Date createdDate;

	@Column(name = "update_date")
	private Date updatedDate;

	@Column(name = "is_deleted")
	private boolean isDeleted;

	@Column(name = "gateway_type")
	private String gateWayType;

	@Column(name = "is_active")
	private boolean isActive;
	
	@Column(name = "firstData_user_name")
	private String firstDataUserName;
	
	@Column(name = "firstData_password")
	private String firstDataPassword;
	
	@Column(name = "firstData_merchant_id")
	private String firstDataMerchantId;

    @Column(name = "payeezy_app_Id")
    private String payeezyAppId;

    @Column(name = "payeezy_app_secretKey")
    private String payeezyAppSecretKey;

    @Column(name = "payeezy_merchnat_token")
    private String payeezyMerchnatToken;
       
    @Column(name = "payeezy_merchnat_token_js")
    private String payeezyMerchnatTokenJs;
    
    @Column(name = "payeezy3DS_apiKey")
    private String payeezy3DSApiKey;

    @Column(name = "payeezy3DS_apiIdentifier")
    private String payeezy3DSApiIdentifier;
       
    @Column(name = "payeezy3DS_orgUnitId")
    private String payeezy3DSOrgUnitId;
    
    @Column(name = "payeezy3D_taToken")
    private String payeezy3DTaToken;
    
    @Column(name = "is_mkonnekt_account")
    private Integer isMkonnektAccount;
    
    
    public String getPayeezy3DSApiKey() {
        return payeezy3DSApiKey;
    }

    public void setPayeezy3DSApiKey(String payeezy3dsApiKey) {
        payeezy3DSApiKey = payeezy3dsApiKey;
    }

    public String getPayeezy3DSApiIdentifier() {
        return payeezy3DSApiIdentifier;
    }

    public void setPayeezy3DSApiIdentifier(String payeezy3dsApiIdentifier) {
        payeezy3DSApiIdentifier = payeezy3dsApiIdentifier;
    }

    public String getPayeezy3DSOrgUnitId() {
        return payeezy3DSOrgUnitId;
    }

    public void setPayeezy3DSOrgUnitId(String payeezy3dsOrgUnitId) {
        payeezy3DSOrgUnitId = payeezy3dsOrgUnitId;
    }
	
	public String getPayeezyMerchnatTokenJs() {
        return payeezyMerchnatTokenJs;
    }

    public void setPayeezyMerchnatTokenJs(String payeezyMerchnatTokenJs) {
        this.payeezyMerchnatTokenJs = payeezyMerchnatTokenJs;
    }

    public String getPayeezyAppId() {
        return payeezyAppId;
    }

    public void setPayeezyAppId(String payeezyAppId) {
        this.payeezyAppId = payeezyAppId;
    }

    public String getPayeezyAppSecretKey() {
        return payeezyAppSecretKey;
    }

    public void setPayeezyAppSecretKey(String payeezyAppSecretKey) {
        this.payeezyAppSecretKey = payeezyAppSecretKey;
    }

    public String getPayeezyMerchnatToken() {
        return payeezyMerchnatToken;
    }

    public void setPayeezyMerchnatToken(String payeezyMerchnatToken) {
        this.payeezyMerchnatToken = payeezyMerchnatToken;
    }

    public String getFirstDataMerchantId() {
		return firstDataMerchantId;
	}

	public void setFirstDataMerchantId(String firstDataMerchantId) {
		this.firstDataMerchantId = firstDataMerchantId;
	}

	public String getFirstDataUserName() {
		return firstDataUserName;
	}

	public void setFirstDataUserName(String firstDataUserName) {
		this.firstDataUserName = firstDataUserName;
	}

	public String getFirstDataPassword() {
		return firstDataPassword;
	}

	public void setFirstDataPassword(String firstDataPassword) {
		this.firstDataPassword = firstDataPassword;
	}

	public String getAuthorizeLoginId() {
		return authorizeLoginId;
	}

	public void setAuthorizeLoginId(String authorizeLoginId) {
		this.authorizeLoginId = authorizeLoginId;
	}

	public String getAuthorizeTransactionKey() {
		return authorizeTransactionKey;
	}

	public void setAuthorizeTransactionKey(String authorizeTransactionKey) {
		this.authorizeTransactionKey = authorizeTransactionKey;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getStripeAPIKey() {
		return stripeAPIKey;
	}

	public void setStripeAPIKey(String stripeAPIKey) {
		this.stripeAPIKey = stripeAPIKey;
	}

	public Integer getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(Integer merchantId) {
		this.merchantId = merchantId;
	}

	public String getGateWayType() {
		return gateWayType;
	}

	public void setGateWayType(String gateWayType) {
		this.gateWayType = gateWayType;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public boolean getIsDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public boolean getIsActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	public String getCayanMerchantName() {
		return cayanMerchantName;
	}

	public void setCayanMerchantName(String cayanMerchantName) {
		this.cayanMerchantName = cayanMerchantName;
	}

	public String getCayanMerchantSiteId() {
		return cayanMerchantSiteId;
	}

	public void setCayanMerchantSiteId(String cayanMerchantSiteId) {
		this.cayanMerchantSiteId = cayanMerchantSiteId;
	}

	public String getCayanMerchantKey() {
		return cayanMerchantKey;
	}

	public void setCayanMerchantKey(String cayanMerchantKey) {
		this.cayanMerchantKey = cayanMerchantKey;
	}

	public String getiGatePassword() {
		return iGatePassword;
	}

	public void setiGatePassword(String iGatePassword) {
		this.iGatePassword = iGatePassword;
	}

	public String getiGateUserName() {
		return iGateUserName;
	}

	public void setiGateUserName(String iGateUserName) {
		this.iGateUserName = iGateUserName;
	}
	
    @Transient
    private String ccType;
    
    @Transient
    private String ccNumber;
    
    @Transient
    private String ccExpiration;
    
    @Transient
    private String ccCode;
    
    @Transient
    private String total;
    
    @Transient
    private String zipCode;
    
    @Transient
    private String customerAddress;
    
    @Transient
    private String merchanttName;


    public String getMerchanttName() {
        return merchanttName;
    }

    public void setMerchanttName(String merchanttName) {
        this.merchanttName = merchanttName;
    }

    @Transient
    private String customerName;
    
    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    @Transient
    private Integer expMonth;
    @Transient
    private Integer expYear;
    
    
    @Transient
    private String expMonthYear;

    
    @Transient
    private String state;
    
    
    @Transient
    private String token;
    
    public String getCavvPayeezytoken() {
        return cavvPayeezytoken;
    }

    public void setCavvPayeezytoken(String cavvPayeezytoken) {
        this.cavvPayeezytoken = cavvPayeezytoken;
    }

    @Transient
    private String cavvPayeezytoken;
    

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Transient
    private String name;

    public String getExpMonthYear() {
        return expMonthYear;
    }

    public void setExpMonthYear(String expMonthYear) {
        this.expMonthYear = expMonthYear;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public Integer getExpMonth() {
        return expMonth;
    }

    public void setExpMonth(Integer expMonth) {
        this.expMonth = expMonth;
    }

    public Integer getExpYear() {
        return expYear;
    }

    public void setExpYear(Integer expYear) {
        this.expYear = expYear;
    }

    public String getCcType() {
        return ccType;
    }

    public void setCcType(String ccType) {
        this.ccType = ccType;
    }

    public String getCcNumber() {
        return ccNumber;
    }

    public void setCcNumber(String ccNumber) {
        this.ccNumber = ccNumber;
    }

    public String getCcExpiration() {
        return ccExpiration;
    }

    public void setCcExpiration(String ccExpiration) {
        this.ccExpiration = ccExpiration;
    }

    public String getCcCode() {
        return ccCode;
    }

    public void setCcCode(String ccCode) {
        this.ccCode = ccCode;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }


    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }


public String getName() {
    return name;
}

public void setName(String name) {
    this.name = name;
}

public Integer getIsMkonnektAccount() {
	return isMkonnektAccount;
}

public void setIsMkonnektAccount(Integer isMkonnektAccount) {
	this.isMkonnektAccount = isMkonnektAccount;
}

	
	public String getPayeezy3DTaToken() {
	return payeezy3DTaToken;
}

public void setPayeezy3DTaToken(String payeezy3dTaToken) {
	payeezy3DTaToken = payeezy3dTaToken;
}
	
}
