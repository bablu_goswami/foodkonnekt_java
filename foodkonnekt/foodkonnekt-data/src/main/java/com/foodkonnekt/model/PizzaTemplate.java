package com.foodkonnekt.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@Entity
@Table(name = "pizzatemplate")
public class PizzaTemplate {

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id")
	private Integer id;

	private String description;

	private int active;

	private String posPizzaTemplateId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "merchant_id", referencedColumnName = "id")
	private Merchant merchant;

	@Column(name = "taxable")
	private Boolean taxable;

	@Column(name = "aux_taxable")
	private Boolean auxTaxable;

	public Merchant getMerchant() {
		return merchant;
	}

	public void setMerchant(Merchant merchant) {
		this.merchant = merchant;
	}

	public int getActive() {
		return active;
	}

	public String getPosPizzaTemplateId() {
		return posPizzaTemplateId;
	}

	public void setPosPizzaTemplateId(String posPizzaTemplateId) {
		this.posPizzaTemplateId = posPizzaTemplateId;
	}

	public Set<PizzaSize> getPizzaSizes() {
		return pizzaSizes;
	}

	public void setPizzaSizes(Set<PizzaSize> pizzaSizes) {
		this.pizzaSizes = pizzaSizes;
	}

	@LazyCollection(LazyCollectionOption.FALSE)
	@ManyToMany(fetch = FetchType.LAZY, mappedBy = "pizzaTemplates")
	private Set<PizzaSize> pizzaSizes = new HashSet<PizzaSize>();

	@LazyCollection(LazyCollectionOption.FALSE)
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "category_id", referencedColumnName = "id")
	private Category category;

	@Transient
	private List<PizzaTemplateSize> pizzaTemplateSizes;

	public List<PizzaTemplateSize> getPizzaTemplateSizes() {
		return pizzaTemplateSizes;
	}

	public void setPizzaTemplateSizes(List<PizzaTemplateSize> pizzaTemplateSizes) {
		this.pizzaTemplateSizes = pizzaTemplateSizes;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int isActive() {
		return active;
	}

	public void setActive(int active) {
		this.active = active;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	@Transient
	private String status;
	@Transient
	private String action;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public List<PizzaTopping> getPizzaToppings() {
		return pizzaToppings;
	}

	public void setPizzaToppings(List<PizzaTopping> pizzaToppings) {
		this.pizzaToppings = pizzaToppings;
	}

	public Double getMinimumTemplateSizePrice() {
		return minimumTemplateSizePrice;
	}

	public void setMinimumTemplateSizePrice(Double minimumTemplateSizePrice) {
		this.minimumTemplateSizePrice = minimumTemplateSizePrice;
	}

	public Integer getMinimumTemplateSizeId() {
		return minimumTemplateSizeId;
	}

	public void setMinimumTemplateSizeId(Integer minimumTemplateSizeId) {
		this.minimumTemplateSizeId = minimumTemplateSizeId;
	}

	public String getMinimumPizzaSizeName() {
		return minimumPizzaSizeName;
	}

	public void setMinimumPizzaSizeName(String minimumPizzaSizeName) {
		this.minimumPizzaSizeName = minimumPizzaSizeName;
	}

	public Boolean getTaxable() {
		return taxable;
	}

	public void setTaxable(Boolean taxable) {
		this.taxable = taxable;
	}

	public Boolean getAuxTaxable() {
		return auxTaxable;
	}

	public void setAuxTaxable(Boolean auxTaxable) {
		this.auxTaxable = auxTaxable;
	}

	public Boolean getIsDefaultTaxRates() {
		return isDefaultTaxRates;
	}

	public void setIsDefaultTaxRates(Boolean isDefaultTaxRates) {
		this.isDefaultTaxRates = isDefaultTaxRates;
	}

	public Set<TaxRates> getTaxes() {
		return taxes;
	}

	public void setTaxes(Set<TaxRates> taxes) {
		this.taxes = taxes;
	}

	public List<PizzaCrust> getPizzaCrust() {
		return pizzaCrust;
	}

	public void setPizzaCrust(List<PizzaCrust> pizzaCrust) {
		this.pizzaCrust = pizzaCrust;
	}

	public List<PizzaTemplateCategory> getPizzaTemplateCategories() {
		return pizzaTemplateCategories;
	}

	public void setPizzaTemplateCategories(
			List<PizzaTemplateCategory> pizzaTemplateCategories) {
		this.pizzaTemplateCategories = pizzaTemplateCategories;
	}

	public String getPizzaToppingsIds() {
		return pizzaToppingsIds;
	}

	public void setPizzaToppingsIds(String pizzaToppingsIds) {
		this.pizzaToppingsIds = pizzaToppingsIds;
	}

	public List<PizzaTemplateTopping> getPizzaTemplateToppings() {
		return pizzaTemplateToppings;
	}

	public void setPizzaTemplateToppings(
			List<PizzaTemplateTopping> pizzaTemplateToppings) {
		this.pizzaTemplateToppings = pizzaTemplateToppings;
	}

	public String getPizzaSizesIds() {
		return pizzaSizesIds;
	}

	public void setPizzaSizesIds(String pizzaSizesIds) {
		this.pizzaSizesIds = pizzaSizesIds;
	}

	public Integer getAllowToppingLimit() {
		return allowToppingLimit;
	}

	public void setAllowToppingLimit(Integer allowToppingLimit) {
		this.allowToppingLimit = allowToppingLimit;
	}

	@Transient
	private List<PizzaTopping> pizzaToppings;

	@Transient
	private String pizzaToppingsIds;

	@Transient
	private Double minimumTemplateSizePrice;

	@Transient
	private Integer minimumTemplateSizeId;

	@Transient
	private String minimumPizzaSizeName;

	@Basic
	private Boolean isDefaultTaxRates;

	@ManyToMany(cascade = { CascadeType.REMOVE }, fetch = FetchType.EAGER)
	@JoinTable(name = "pizza_template_taxes", joinColumns = { @JoinColumn(name = "template_id") }, inverseJoinColumns = { @JoinColumn(name = "taxrate_id") })
	private Set<TaxRates> taxes = new HashSet<TaxRates>();

	@Transient
	private List<PizzaCrust> pizzaCrust;

	@Transient
	private List<PizzaTemplateCategory> pizzaTemplateCategories;

	@Transient
	private List<PizzaTemplateTopping> pizzaTemplateToppings;

	@Transient
	private String pizzaSizesIds;

	@Column(name = "allow_topping_limit")
	private Integer allowToppingLimit;
	
	public String getTemplateToppingIds() {
		return templateToppingIds;
	}

	public void setTemplateToppingIds(String templateToppingIds) {
		this.templateToppingIds = templateToppingIds;
	}

	@Column(name = "is_max_limit")
	private Integer isMaxLimit = 0;
	
	@Column(name = "is_min_limit")
	private Integer isMinLimit = 0;
	
	public Integer getIsMinLimit() {
		return isMinLimit;
	}

	public void setIsMinLimit(Integer isMinLimit) {
		this.isMinLimit = isMinLimit;
	}

	public void setIsMaxLimit(Integer isMaxLimit) {
		this.isMaxLimit = isMaxLimit;
	}
	
	public Integer getIsMaxLimit() {
		return isMaxLimit;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	@Transient
	private String templateToppingIds;
	
	@Transient
	private String categoryName;
	
	@Transient
	private List<Integer> templateId;
	
	@Transient
	private List<Integer> toppingId;
	
	@Transient
	private List<Integer> sizeId;
	
	public List<Integer> getTemplateId() {
		return templateId;
	}

	public void setTemplateId(List<Integer> templateId) {
		this.templateId = templateId;
	}

	public List<Integer> getToppingId() {
		return toppingId;
	}

	public void setToppingId(List<Integer> toppingId) {
		this.toppingId = toppingId;
	}

	public List<Integer> getSizeId() {
		return sizeId;
	}

	public void setSizeId(List<Integer> sizeId) {
		this.sizeId = sizeId;
	}

	public List<Integer> getCrustId() {
		return crustId;
	}

	public void setCrustId(List<Integer> crustId) {
		this.crustId = crustId;
	}

	

	public List<Integer> getCatId() {
		return catId;
	}

	public void setCatId(List<Integer> catId) {
		this.catId = catId;
	}

	

	public List<Double> getSizePrice() {
		return sizePrice;
	}

	public void setSizePrice(List<Double> sizePrice) {
		this.sizePrice = sizePrice;
	}

	public List<PizzaTemplateTax> getPizzaTemplateTaxs() {
		return pizzaTemplateTaxs;
	}

	public void setPizzaTemplateTaxs(List<PizzaTemplateTax> pizzaTemplateTaxs) {
		this.pizzaTemplateTaxs = pizzaTemplateTaxs;
	}

	public List<Integer> getTaxId() {
		return taxId;
	}

	public void setTaxId(List<Integer> taxId) {
		this.taxId = taxId;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public List<Integer> getPizzaTemplateToppingid() {
		return pizzaTemplateToppingid;
	}

	public void setPizzaTemplateToppingid(List<Integer> pizzaTemplateToppingid) {
		this.pizzaTemplateToppingid = pizzaTemplateToppingid;
	}

	@Transient
	private List<Integer> crustId;
	
	@Transient
	private List<Integer> catId;
	
	@Transient
	private List<Double> sizePrice;
	
	@Transient
	private List<PizzaTemplateTax> pizzaTemplateTaxs;
	
	@Transient
	private List<Integer> taxId;
	
	@Transient
	private String isActive;
	
	@Transient
	private List<Integer> pizzaTemplateToppingid;
	
	@Column(name = "description2")
	private String description2;

	public String getDescription2() {
		return description2;
	}

	public void setDescription2(String description2) {
		this.description2 = description2;
	}
	
	@Transient
	private Integer mapcruststatus;

	public Integer getMapcruststatus() {
		return mapcruststatus;
	}

	public void setMapcruststatus(Integer mapcruststatus) {
		this.mapcruststatus = mapcruststatus;
	}
	
	@Transient
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
 