package com.foodkonnekt.clover.vo;

public class FBUser {
	
	private String country;
	private String locale;
	private FBAge age;
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getLocale() {
		return locale;
	}
	public void setLocale(String locale) {
		this.locale = locale;
	}
	public FBAge getAge() {
		return age;
	}
	public void setAge(FBAge age) {
		this.age = age;
	}
	
}
