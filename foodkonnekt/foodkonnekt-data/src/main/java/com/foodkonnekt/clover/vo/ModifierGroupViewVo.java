package com.foodkonnekt.clover.vo;

public class ModifierGroupViewVo {

    private Integer id;
    private String modifierGroupName;
    private String modifiers;
    private String action;
    private String status;

    public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getModifierGroupName() {
        return modifierGroupName;
    }

    public void setModifierGroupName(String modifierGroupName) {
        this.modifierGroupName = modifierGroupName;
    }

    public String getModifiers() {
        return modifiers;
    }

    public void setModifiers(String modifiers) {
        this.modifiers = modifiers;
    }

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
