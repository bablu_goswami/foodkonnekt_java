package com.foodkonnekt.clover.vo;

import java.util.List;

public class Item implements Cloneable{
	private String id;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	
	private String itemName;
	private String itemid;
	private String amount;
	private String price;
	private List<ModifierVO> modifier;
	private String itemUid;
	private String itemPosId;
	private String modifierPrice;
	private Boolean extraCharge;
	private String pizzaSizeId;
	private String pizzaSizeName;
	private String pizzaSizePrice;
	private Boolean isPizza;

	public String getItemid() {
		return itemid;
	}

	public void setItemid(String itemid) {
		this.itemid = itemid;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public List<ModifierVO> getModifier() {
		return modifier;
	}

	public void setModifier(List<ModifierVO> modifier) {
		this.modifier = modifier;
	}

	public String getItemUid() {
		return itemUid;
	}

	public void setItemUid(String itemUid) {
		this.itemUid = itemUid;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getItemPosId() {
		return itemPosId;
	}

	public void setItemPosId(String itemPosId) {
		this.itemPosId = itemPosId;
	}

	public String getModifierPrice() {
		return modifierPrice;
	}

	public void setModifierPrice(String modifierPrice) {
		this.modifierPrice = modifierPrice;
	}

	public Boolean getExtraCharge() {
		return extraCharge;
	}

	public void setExtraCharge(Boolean extraCharge) {
		this.extraCharge = extraCharge;
	}

	public String getPizzaSizeId() {
		return pizzaSizeId;
	}

	public void setPizzaSizeId(String pizzaSizeId) {
		this.pizzaSizeId = pizzaSizeId;
	}

	public String getPizzaSizeName() {
		return pizzaSizeName;
	}

	public void setPizzaSizeName(String pizzaSizeName) {
		this.pizzaSizeName = pizzaSizeName;
	}

	public Boolean getIsPizza() {
		return isPizza;
	}

	public void setIsPizza(Boolean isPizza) {
		this.isPizza = isPizza;
	}

	public String getPizzaSizePrice() {
		return pizzaSizePrice;
	}

	public void setPizzaSizePrice(String pizzaSizePrice) {
		this.pizzaSizePrice = pizzaSizePrice;
	}

    @Override
    public Object clone() throws CloneNotSupportedException {
        // TODO Auto-generated method stub
        return super.clone();
    }
	
	
	

}
