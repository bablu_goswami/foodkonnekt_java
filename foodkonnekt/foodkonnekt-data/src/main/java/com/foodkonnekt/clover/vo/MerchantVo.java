package com.foodkonnekt.clover.vo;

import java.util.List;

public class MerchantVo {

	private String merchantUid;

	public String getMerchantUid() {
		return merchantUid;
	}

	public void setMerchantUid(String merchantUid) {
		this.merchantUid = merchantUid;
	}

}
