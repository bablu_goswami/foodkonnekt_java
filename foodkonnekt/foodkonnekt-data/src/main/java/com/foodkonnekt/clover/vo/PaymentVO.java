package com.foodkonnekt.clover.vo;

public class PaymentVO {

    private String paymentMethod;
    private String ccType;
    private String ccNumber;
    private String ccExpiration;
    private String ccCode;
    private String instruction;
    private String orderPosId;
    private String total;
    private String zipCode;

    private double tip = 0.0;

    public double getTip() {
        return tip;
    }

    public void setTip(double tip) {
        this.tip = tip;
    }

    private String subTotal;
    private String tax;
    private Integer expMonth;
    private Integer expYear;
    private String futureOrderType;
    private String futureDate;
    private String futureTime;
    private String listOfALLDiscounts;
    
    //vaultedInfo
    private String token;
    private String expirationDate;
    private String first6;
    private String last4;
    private Boolean isVaulted;
    private String auxTax;
    
    
    private String payeezyToken;
    private String cavvPayeezyToken;
    
    
    
    
    public String getPayeezyToken() {
        return payeezyToken;
    }

    public void setPayeezyToken(String payeezyToken) {
        this.payeezyToken = payeezyToken;
    }

    public String getCavvPayeezyToken() {
        return cavvPayeezyToken;
    }

    public void setCavvPayeezyToken(String cavvPayeezyToken) {
        this.cavvPayeezyToken = cavvPayeezyToken;
    }

    public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(String expirationDate) {
		this.expirationDate = expirationDate;
	}

	public String getFirst6() {
		return first6;
	}

	public void setFirst6(String first6) {
		this.first6 = first6;
	}

	public String getLast4() {
		return last4;
	}

	public void setLast4(String last4) {
		this.last4 = last4;
	}

	public Boolean getIsVaulted() {
		return isVaulted;
	}

	public void setIsVaulted(Boolean isVaulted) {
		this.isVaulted = isVaulted;
	}

	private Boolean saveCard;
    private String _saveCard;
    private String vaultedInfo;

    public String get_saveCard() {
		return _saveCard;
	}

	public void set_saveCard(String _saveCard) {
		this._saveCard = _saveCard;
	}

	public Boolean getSaveCard() {
		return saveCard;
	}

	public void setSaveCard(Boolean saveCard) {
		this.saveCard = saveCard;
	}

	public String getListOfALLDiscounts() {
		return listOfALLDiscounts;
	}

	public void setListOfALLDiscounts(String listOfALLDiscounts) {
		this.listOfALLDiscounts = listOfALLDiscounts;
	}

	public Integer getExpMonth() {
        return expMonth;
    }

    public void setExpMonth(Integer expMonth) {
        this.expMonth = expMonth;
    }

    public Integer getExpYear() {
        return expYear;
    }

    public void setExpYear(Integer expYear) {
        this.expYear = expYear;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getCcType() {
        return ccType;
    }

    public void setCcType(String ccType) {
        this.ccType = ccType;
    }

    public String getCcNumber() {
        return ccNumber;
    }

    public void setCcNumber(String ccNumber) {
        this.ccNumber = ccNumber;
    }

    public String getCcExpiration() {
        return ccExpiration;
    }

    public void setCcExpiration(String ccExpiration) {
        this.ccExpiration = ccExpiration;
    }

    public String getCcCode() {
        return ccCode;
    }

    public void setCcCode(String ccCode) {
        this.ccCode = ccCode;
    }

    public String getInstruction() {
        return instruction;
    }

    public void setInstruction(String instruction) {
        this.instruction = instruction;
    }

    public String getOrderPosId() {
        return orderPosId;
    }

    public void setOrderPosId(String orderPosId) {
        this.orderPosId = orderPosId;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(String subTotal) {
        this.subTotal = subTotal;
    }

    public String getTax() {
        return tax;
    }

    public void setTax(String tax) {
        this.tax = tax;
    }

    public String getFutureOrderType() {
        return futureOrderType;
    }

    public void setFutureOrderType(String futureOrderType) {
        this.futureOrderType = futureOrderType;
    }

    public String getFutureDate() {
        return futureDate;
    }

    public void setFutureDate(String futureDate) {
        this.futureDate = futureDate;
    }

    public String getFutureTime() {
        return futureTime;
    }

    public void setFutureTime(String futureTime) {
        this.futureTime = futureTime;
    }

	public String getVaultedInfo() {
		return vaultedInfo;
	}

	public void setVaultedInfo(String vaultedInfo) {
		this.vaultedInfo = vaultedInfo;
	}

    public String getAuxTax() {
        return auxTax;
    }

    public void setAuxTax(String auxTax) {
        this.auxTax = auxTax;
    }

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	
}
