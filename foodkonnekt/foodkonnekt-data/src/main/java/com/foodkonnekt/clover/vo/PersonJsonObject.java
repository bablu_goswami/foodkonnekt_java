package com.foodkonnekt.clover.vo;

import java.util.List;

import com.foodkonnekt.model.PizzaTemplateSize;
import com.foodkonnekt.model.PizzaToppingSize;

public class PersonJsonObject {

    int iTotalRecords;

    int iTotalDisplayRecords;

    String sEcho;

    String sColumns;

    List<InventoryItemVo> aaData;
    
    List<PizzaTemplateSize> pizzaData;

    public int getiTotalRecords() {
        return iTotalRecords;
    }

    public void setiTotalRecords(int iTotalRecords) {
        this.iTotalRecords = iTotalRecords;
    }

    public int getiTotalDisplayRecords() {
        return iTotalDisplayRecords;
    }

    public void setiTotalDisplayRecords(int iTotalDisplayRecords) {
        this.iTotalDisplayRecords = iTotalDisplayRecords;
    }

    public String getsEcho() {
        return sEcho;
    }

    public void setsEcho(String sEcho) {
        this.sEcho = sEcho;
    }

    public String getsColumns() {
        return sColumns;
    }

    public void setsColumns(String sColumns) {
        this.sColumns = sColumns;
    }

    public List<InventoryItemVo> getAaData() {
        return aaData;
    }

    public void setAaData(List<InventoryItemVo> aaData) {
        this.aaData = aaData;
    }

	public List<PizzaToppingSize> getToping() {
		return toping;
	}

	public void setToping(List<PizzaToppingSize> toping) {
		this.toping = toping;
	}


	private List<PizzaToppingSize> toping;

	public List<PizzaTemplateSize> getPizzaData() {
		return pizzaData;
	}

	public void setPizzaData(List<PizzaTemplateSize> pizzaData) {
		this.pizzaData = pizzaData;
	}
	
	
}
