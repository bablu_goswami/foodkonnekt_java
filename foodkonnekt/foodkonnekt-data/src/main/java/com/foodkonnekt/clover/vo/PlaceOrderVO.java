package com.foodkonnekt.clover.vo;

import java.util.List;

import com.foodkonnekt.model.ItemDto;

public class PlaceOrderVO {

	// private String orderJson;
	private String orderTotal;
	private String instruction;
	private Double discount;
	private PaymentVO paymentVO;

	public PaymentVO getPaymentVO() {
		return paymentVO;
	}

	public void setPaymentVO(PaymentVO paymentVO) {
		this.paymentVO = paymentVO;
	}

	private List<Item> orderJson;;

	public List<Item> getOrderJson() {
		return orderJson;
	}

	public void setOrderJson(List<Item> orderJson) {
		this.orderJson = orderJson;
	}

	public String getOrderTotal() {
		return orderTotal;
	}

	public void setOrderTotal(String orderTotal) {
		this.orderTotal = orderTotal;
	}

	public String getInstruction() {
		return instruction;
	}

	public void setInstruction(String instruction) {
		this.instruction = instruction;
	}

	public Double getDiscount() {
		return discount;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}

	public String getDiscountType() {
		return discountType;
	}

	public void setDiscountType(String discountType) {
		this.discountType = discountType;
	}

	public String getItemPosIds() {
		return itemPosIds;
	}

	public void setItemPosIds(String itemPosIds) {
		this.itemPosIds = itemPosIds;
	}

	public String getInventoryLevel() {
		return inventoryLevel;
	}

	public void setInventoryLevel(String inventoryLevel) {
		this.inventoryLevel = inventoryLevel;
	}

	public String getVoucherCode() {
		return voucherCode;
	}

	public void setVoucherCode(String voucherCode) {
		this.voucherCode = voucherCode;
	}

	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

	public String getConvenienceFee() {
		return convenienceFee;
	}

	public void setConvenienceFee(String convenienceFee) {
		this.convenienceFee = convenienceFee;
	}

	public String getDeliveryItemPrice() {
		return deliveryItemPrice;
	}

	public void setDeliveryItemPrice(String deliveryItemPrice) {
		this.deliveryItemPrice = deliveryItemPrice;
	}

	public String getAvgDeliveryTime() {
		return avgDeliveryTime;
	}

	public void setAvgDeliveryTime(String avgDeliveryTime) {
		this.avgDeliveryTime = avgDeliveryTime;
	}

	public List<ItemDto> getItemsForDiscount() {
		return itemsForDiscount;
	}

	public void setItemsForDiscount(List<ItemDto> itemsForDiscount) {
		this.itemsForDiscount = itemsForDiscount;
	}

	public List<CouponVO> getListOfALLDiscounts() {
		return listOfALLDiscounts;
	}

	public void setListOfALLDiscounts(List<CouponVO> listOfALLDiscounts) {
		this.listOfALLDiscounts = listOfALLDiscounts;
	}

	public Boolean getIsDeliveryCoupon() {
		return isDeliveryCoupon;
	}

	public void setIsDeliveryCoupon(Boolean isDeliveryCoupon) {
		this.isDeliveryCoupon = isDeliveryCoupon;
	}

	private String discountType;
	private String itemPosIds;
	private String inventoryLevel;
	private String voucherCode;
	private String orderType;
	private String convenienceFee;
	private String deliveryItemPrice;
	private String avgDeliveryTime;
	private List<ItemDto> itemsForDiscount;
	private List<CouponVO> listOfALLDiscounts;
	private Boolean isDeliveryCoupon;
	private Integer addressId;

	public Integer getAddressId() {
		return addressId;
	}

	public void setAddressId(Integer addressId) {
		this.addressId = addressId;
	}

	public Integer getZoneId() {
		return zoneId;
	}

	public void setZoneId(Integer zoneId) {
		this.zoneId = zoneId;
	}

	private Integer zoneId;
}
