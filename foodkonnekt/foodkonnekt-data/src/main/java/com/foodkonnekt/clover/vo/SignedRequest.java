package com.foodkonnekt.clover.vo;

public class SignedRequest {
	
	private String algorithm;
	private long issued_at;
	private FBPage page;
	private FBUser user;
	public FBUser getUser() {
		return user;
	}
	public void setUser(FBUser user) {
		this.user = user;
	}
	public String getAlgorithm() {
		return algorithm;
	}
	public void setAlgorithm(String algorithm) {
		this.algorithm = algorithm;
	}
	public long getIssued_at() {
		return issued_at;
	}
	public void setIssued_at(long issued_at) {
		this.issued_at = issued_at;
	}
	public FBPage getPage() {
		return page;
	}
	public void setPage(FBPage page) {
		this.page = page;
	}

}
