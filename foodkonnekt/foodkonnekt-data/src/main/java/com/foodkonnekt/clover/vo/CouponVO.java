package com.foodkonnekt.clover.vo;

public class CouponVO {
	
	private Double discount;
    private String discountType;
    private String couponUID;
    private String inventoryLevel;
    private Boolean isUsabilitySingle;
    private String voucherCode;
	public Double getDiscount() {
		return discount;
	}
	public void setDiscount(Double discount) {
		this.discount = discount;
	}
	public String getDiscountType() {
		return discountType;
	}
	public void setDiscountType(String discountType) {
		this.discountType = discountType;
	}
	public String getCouponUID() {
		return couponUID;
	}
	public void setCouponUID(String couponUID) {
		this.couponUID = couponUID;
	}
	public String getInventoryLevel() {
		return inventoryLevel;
	}
	public void setInventoryLevel(String inventoryLevel) {
		this.inventoryLevel = inventoryLevel;
	}
	public Boolean getIsUsabilitySingle() {
		return isUsabilitySingle;
	}
	public void setIsUsabilitySingle(Boolean isUsabilitySingle) {
		this.isUsabilitySingle = isUsabilitySingle;
	}
	public String getVoucherCode() {
		return voucherCode;
	}
	public void setVoucherCode(String voucherCode) {
		this.voucherCode = voucherCode;
	}

}
