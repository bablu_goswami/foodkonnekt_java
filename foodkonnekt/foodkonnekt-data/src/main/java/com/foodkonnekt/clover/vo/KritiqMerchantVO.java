package com.foodkonnekt.clover.vo;

public class KritiqMerchantVO {
	
	private Integer noOfMailSent;
	
	private Integer noOfResponse;
	
	private Double avgRate;
	
	private Integer merchantId;
	
	private String merchantName;

	public String getMerchantName() {
		return merchantName;
	}

	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}

	public Integer getNoOfMailSent() {
		return noOfMailSent;
	}

	public void setNoOfMailSent(Integer noOfMailSent) {
		this.noOfMailSent = noOfMailSent;
	}

	public Integer getNoOfResponse() {
		return noOfResponse;
	}

	public void setNoOfResponse(Integer noOfResponse) {
		this.noOfResponse = noOfResponse;
	}

	public Double getAvgRate() {
		return avgRate;
	}

	public void setAvgRate(Double avgRate) {
		this.avgRate = avgRate;
	}

	public Integer getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(Integer merchantId) {
		this.merchantId = merchantId;
	}
	

}
