package com.foodkonnekt.clover.vo;

import java.util.Date;
import java.util.List;

import com.foodkonnekt.model.NotificationTracker;
import com.foodkonnekt.model.OrderDiscount;
import com.foodkonnekt.model.OrderItem;
import com.foodkonnekt.model.OrderItemModifier;
import com.foodkonnekt.model.OrderPizza;

public class AllOrderVo {

	private String DT_RowId;
    public String getDT_RowId() {
		return DT_RowId;
	}

	public void setDT_RowId(String dT_RowId) {
		DT_RowId = dT_RowId;
	}
	
	private List<OrderItemViewVO> orderItemViewVOs;

	public List<OrderItemViewVO> getOrderItemViewVOs() {
		return orderItemViewVOs;
	}

	public void setOrderItemViewVOs(List<OrderItemViewVO> orderItemViewVOs) {
		this.orderItemViewVOs = orderItemViewVOs;
	}

	private Integer id;
	private String orderAdrress;
	private String state;
	private String city;
	private String zip;
    public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	private String firstName;
    private Date createdOn;
    private Double orderPrice;
    private Double tipAmount;
    private Double discount=0.0;
    private String emailId;
    public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	private String phoneNo;
    public Double getDiscount() {
		return discount;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}

	public Double getTipAmount() {
		return tipAmount;
	}

	public void setTipAmount(Double tipAmount) {
		this.tipAmount = tipAmount;
	}

	private String orderType;
    private String orderName;
    private String status;
    private String view;
    private String edit;
    private String orderItemName;
    private double orderItemPrice;
    public String getOrderItemName() {
		return orderItemName;
	}

    private List<OrderItem> orderItems;
    
    private List<OrderItemModifier> orderItemModifiers;
    
	public List<OrderItem> getOrderItems() {
		return orderItems;
	}

	public void setOrderItems(List<OrderItem> orderItems) {
		this.orderItems = orderItems;
	}

	public void setOrderItemName(String orderItemName) {
		this.orderItemName = orderItemName;
	}

	public double getOrderItemPrice() {
		return orderItemPrice;
	}

	public void setOrderItemPrice(double orderItemPrice) {
		this.orderItemPrice = orderItemPrice;
	}

	public int getQty() {
		return qty;
	}

	public void setQty(int qty) {
		this.qty = qty;
	}

	private int qty;
    
    

	private String convenienceFee;
    private String deliveryFee;
    private String tax;
    private String subTotal;
    
    private String customerEmail;
    private String customerPhone;

    public String getConvenienceFee() {
		return convenienceFee;
	}

	public void setConvenienceFee(String convenienceFee) {
		this.convenienceFee = convenienceFee;
	}

	public String getDeliveryFee() {
		return deliveryFee;
	}

	public void setDeliveryFee(String deliveryFee) {
		this.deliveryFee = deliveryFee;
	}

	public String getTax() {
		return tax;
	}

	public void setTax(String tax) {
		this.tax = tax;
	}

	public String getSubTotal() {
		return subTotal;
	}

	public void setSubTotal(String subTotal) {
		this.subTotal = subTotal;
	}

	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Double getOrderPrice() {
        return orderPrice;
    }

    public void setOrderPrice(Double orderPrice) {
        this.orderPrice = orderPrice;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getView() {
        return view;
    }

    public void setView(String view) {
        this.view = view;
    }
    public String getEdit() {
		return edit;
	}

	public void setEdit(String edit) {
		this.edit = edit;
	}

	public String getCustomerEmail() {
		return customerEmail;
	}

	public void setCustomerEmail(String customerEmail) {
		this.customerEmail = customerEmail;
	}

	public String getCustomerPhone() {
		return customerPhone;
	}

	public void setCustomerPhone(String customerPhone) {
		this.customerPhone = customerPhone;
	}
	
	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

    public List<OrderDiscount> getOrderDiscountsList() {
        return orderDiscountsList;
    }

    public void setOrderDiscountsList(List<OrderDiscount> orderDiscountsList) {
        this.orderDiscountsList = orderDiscountsList;
    }

    public List<OrderItemModifier> getOrderItemModifiers() {
		return orderItemModifiers;
	}

	public void setOrderItemModifiers(List<OrderItemModifier> orderItemModifiers) {
		this.orderItemModifiers = orderItemModifiers;
	}

	public String getOrderAdrress() {
		return orderAdrress;
	}

	public void setOrderAdrress(String orderAdrress) {
		this.orderAdrress = orderAdrress;
	}

	public List<OrderPizza> getOrderPizza() {
		return orderPizza;
	}

	public void setOrderPizza(List<OrderPizza> orderPizza) {
		this.orderPizza = orderPizza;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	private String paymentMethod;
	
	private List<OrderDiscount> orderDiscountsList;
	
	private List<OrderPizza> orderPizza;
	
	private String action;
	
	private String orderPosId;
	
	 private Date  fulfilledOn;
	
	public String getOrderPosId() {
		return orderPosId;
	}

	public void setOrderPosId(String orderPosId) {
		this.orderPosId = orderPosId;
	}
	private List<NotificationTracker> notificationTracker;
	public List<NotificationTracker> getNotificationTracker() {
		return notificationTracker;
	}

	public void setNotificationTracker(List<NotificationTracker> notificationTracker) {
		this.notificationTracker = notificationTracker;
	}

	public Date getFulfilledOn() {
		return fulfilledOn;
	}

	public void setFulfilledOn(Date fulfilledOn) {
		this.fulfilledOn = fulfilledOn;
	}
}
