package com.foodkonnekt.clover.vo;

public class NotificationVO {
	
	private String merchantId;
	private String locationUid;
	private String deviceId;
	private Payload payload;
	public String getMerchantId() {
		return merchantId;
	}
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}
	public String getLocationUid() {
		return locationUid;
	}
	public void setLocationUid(String locationUid) {
		this.locationUid = locationUid;
	}
	public String getDeviceId() {
		return deviceId;
	}
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	public Payload getPayload() {
		return payload;
	}
	public void setPayload(Payload payload) {
		this.payload = payload;
	}

}
