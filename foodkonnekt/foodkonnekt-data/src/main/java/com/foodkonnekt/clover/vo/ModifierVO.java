package com.foodkonnekt.clover.vo;

public class ModifierVO {
	
	private String id;
	private String name;
	private String price;
	private String quantity;
	private String toppingSide;
	private String posId; 
	private Boolean isCrust; 
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	public String getToppingSide() {
		return toppingSide;
	}
	public void setToppingSide(String toppingSide) {
		this.toppingSide = toppingSide;
	}
    public String getPosId() {
        return posId;
    }
    public void setPosId(String posId) {
        this.posId = posId;
    }
    public Boolean getIsCrust() {
        return isCrust;
    }
    public void setIsCrust(Boolean isCrust) {
        this.isCrust = isCrust;
    }

}
