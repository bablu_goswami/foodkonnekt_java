package com.foodkonnekt.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

@Configuration
@ComponentScan({"com.foodkonnekt.configuration"})
@PropertySource({"classpath:hibernate.properties", "classpath:application-${env}.properties"})
public class IConstantProperties {
    @Autowired
    private Environment environment;
    public String getProperty(String key) {
        return environment.getProperty(key);
    }
}
