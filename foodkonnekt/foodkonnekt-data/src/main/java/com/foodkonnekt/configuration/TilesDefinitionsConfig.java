package com.foodkonnekt.configuration;

import static com.foodkonnekt.util.TilesIConstant.*;
import static com.foodkonnekt.util.TilesIConstant.MERCHANT_SOCIAL_PLATFORM;

import java.util.HashMap;
import java.util.Map;

import org.apache.tiles.Attribute;
import org.apache.tiles.Definition;
import org.apache.tiles.definition.DefinitionsFactory;

import com.foodkonnekt.util.TilesIConstant;

public final class TilesDefinitionsConfig implements DefinitionsFactory {
    private static final Map<String, Definition> tilesDefinitions = new HashMap<String, Definition>();
    private static final Attribute BASE_TEMPLATE = new Attribute(DEFAULT_LAYOUT);

    public Definition getDefinition(String name, org.apache.tiles.request.Request tilesContext) {
        return tilesDefinitions.get(name);
    }

    /**
     * @param name
     *            <code>Name of the view</code>
     * 
     * @param title
     *            <code>Page title</code>
     * @param body
     *            <code>Body JSP file path</code>
     * 
     *            <code>Adds default layout definitions</code>
     */
    private static void addDefaultLayoutDef(String name, String body, String header, String footer) {
        Map<String, Attribute> attributes = new HashMap<String, Attribute>();

        attributes.put("title", new Attribute(TITLE));
        attributes.put("header", new Attribute(header));
        attributes.put("body", new Attribute(body));
        attributes.put("footer", new Attribute(footer));
        tilesDefinitions.put(name, new Definition(name, BASE_TEMPLATE, attributes));
    }

    public static void addDefinitions() {
        addDefaultLayoutDef("home", HOME_PAGE, FRONT_HEADER, FRONT_FOOTER);
        addDefaultLayoutDef("forgotPassword", FORGOT_PAGE, FRONT_HEADER,
                        FRONT_FOOTER);
        addDefaultLayoutDef("sessionTimeOut", WELCOME_PAGE, HEADER,
                        FOOTER);
        addDefaultLayoutDef("categoryEmpty", CATEGORY_EMPTY, HEADER,
                        FOOTER);
        addDefaultLayoutDef("order", ORDER_PAGE, "", "");
        addDefaultLayoutDef("foodTronixOrder", FOODTRONIX_ORDER_PAGE, "", "");
        addDefaultLayoutDef("myOrder", MY_ORDER_PAGE, HEADER, FOOTER);
        addDefaultLayoutDef("profile", PROFILE_PAGE, HEADER, FOOTER);
        addDefaultLayoutDef("exception", EXCEPTION_PAGE, FRONT_HEADER,
                        FRONT_FOOTER);

        addDefaultLayoutDef("PaymentForm", PAYMENT_FORM, "", "");
        addDefaultLayoutDef("OrderReceipt", ORDER_RECEIPT, "", "");
        addDefaultLayoutDef("feedbackForm", FEEDBACK, "", "");
        addDefaultLayoutDef("feedbackFormWalkInCustomer", FEEDBACK_WALKIN_CUSTOMER, "", "");
        addDefaultLayoutDef("feedbackFormWalkInCustomer5", FEEDBACK_WALKIN_CUSTOMER5, "", "");
        addDefaultLayoutDef("feedbackFormWalkInCustomer6", FEEDBACK_WALKIN_CUSTOMER6, "", "");
        addDefaultLayoutDef("feedbackFormWalkInCustomer7", FEEDBACK_WALKIN_CUSTOMER7, "", "");
        addDefaultLayoutDef("feedbackForm1", FEEDBACK1, "", "");

        addDefaultLayoutDef("feedbackResponse", FEEDBACK_RESPONSE, "", "");

        addDefaultLayoutDef("feedbackshare", FEEDBACK_SHARE, "", "");
        addDefaultLayoutDef("feedbackOrderId", FEEDBACK_ORDER_ID, "", "");
        addDefaultLayoutDef("displayCustomerFeedback", DISPLAY_CUSTOMER_FEEDBACK, "", "");
        addDefaultLayoutDef("feedbackOrderId5", FEEDBACK_ORDER_ID5, "", "");
        addDefaultLayoutDef("feedbackOrderId6", FEEDBACK_ORDER_ID6, "", "");
        addDefaultLayoutDef("feedbackOrderId7", FEEDBACK_ORDER_ID7, "", "");

        addDefaultLayoutDef("createVouchar", CREATE_VOUCHAR, "", "");
        addDefaultLayoutDef("vouchars", VOUCHARS, "", "");
        addDefaultLayoutDef("adminHome", ADMIN_HOME, "", "");
        addDefaultLayoutDef("inventory", INVENTORY, "", "");
        addDefaultLayoutDef("addLineItem", ADD_LINE_ITEM, "", "");
         addDefaultLayoutDef("editModifier", EDIT_MODIFIER, "", "");
        addDefaultLayoutDef("editModifierGroup", EDIT_MODIFIER_GROUP, "", "");
        addDefaultLayoutDef("editCategory", EDIT_CATEGORY, "", "");
        addDefaultLayoutDef("adminLogin", ADMIN_LOGIN, "", "");
        addDefaultLayoutDef("getAllMerchants", GET_ALL_MERCHANT, "", "");
        addDefaultLayoutDef("getAllMerchantsByVendor", GET_ALL_MERCHANT_BY_VENDOR, "", "");
        addDefaultLayoutDef("kritiqMerchantDetail", KRITIQ_MERCHANT_DETAIL, "", "");
        addDefaultLayoutDef("kritiqCustomerDetail", KRITIQ_CUSTOMER_DETAIL, "", "");

        addDefaultLayoutDef("uploadInventory", UPLOAD_INVENTORY, "", "");
        addDefaultLayoutDef("categoryItems", CATEGORY_ITEMS, "", "");

        addDefaultLayoutDef("addCategory", ADD_CATEGORY, "", "");
        addDefaultLayoutDef("allOrders", ALL_ORDERS, "", "");
        addDefaultLayoutDef("customerOrders", CUSTOMER_ORDERS, "", "");
        addDefaultLayoutDef("merchants", MERCHANT, "", "");

        addDefaultLayoutDef("modifierGroups", MODIFIERS_GROUPS, "", "");
        addDefaultLayoutDef("modifiers", MODIFIERS, "", "");
        addDefaultLayoutDef("category", CATEGORY, "", "");
        addDefaultLayoutDef("logOut", LOG_OUT, "", "");

        // Configuration screens
        addDefaultLayoutDef("welcome", WELCOME, "", "");
        addDefaultLayoutDef("uploadLogo", UPLOAD_LOGO, "", "");
        addDefaultLayoutDef("setDeliveryZone", SET_DELIVERY_ZONE, "", "");
        addDefaultLayoutDef("setPickupTime", SET_PICKUP_TIME, "", "");
        addDefaultLayoutDef("setConvenienceFee", SET_CONVENIENCE_FEE, "", "");
        addDefaultLayoutDef("adminPanel", SET_ADMIN_PANEL, "", "");
        addDefaultLayoutDef("onLineOrderLink", ONLINE_ORDER_LINK, "", "");

        addDefaultLayoutDef("signup", MERCHANT_SIGN_UP, "", "");
        addDefaultLayoutDef("login", LOGIN, "", "");
        addDefaultLayoutDef("forgotpassword", FORGOT_PASSWORD, "", "");
        addDefaultLayoutDef("changepassword", CHANGE_PASSWORD, "", "");

        addDefaultLayoutDef("adminLogo", ADMIN_LOGO, "", "");
        addDefaultLayoutDef("addDeliveryZone", ADMIN_DELIVERY_ZONE, "", "");
        addDefaultLayoutDef("pickUpTime", ADMIN_PICKTME, "", "");
        addDefaultLayoutDef("convenienceFee", ADMIN_CONFENCE_FEE, "", "");
        addDefaultLayoutDef("customers", CUSTOMER, "", "");
        addDefaultLayoutDef("updateDeliveryZone", UPDATE_DELIVERY_ZONE, "", "");
        addDefaultLayoutDef("deliveryZones", DELIVERY_ZONE, "", "");
        addDefaultLayoutDef("updateCategory", UPDATE_CATEGORY, "", "");
        addDefaultLayoutDef("updateVouchar", UPDATE_VOUCHAR, "", "");
        addDefaultLayoutDef("setGuestPassword", GUEST_PASSWORD, FRONT_HEADER,
                        FRONT_FOOTER);
        addDefaultLayoutDef("resetGuestPassword", RESET_PASSWORD, FRONT_HEADER,
                        FRONT_FOOTER);

        addDefaultLayoutDef("resetAdminPassword", RESET_ADMIN_PASSWORD, "", "");
        addDefaultLayoutDef("setupPaymentGateway", SET_PAYMENT_GATEWAY, "", "");
        addDefaultLayoutDef("setPaymentGateway", SETPAYMENT_GATEWAY, "", "");
        addDefaultLayoutDef("pizzaTamplate", PIZZATAMPLATE, "", "");
        addDefaultLayoutDef("pizzaTopping", PIZZATOPPINGS, "", "");
        addDefaultLayoutDef("editPizzaTemplate", EDITPIZZATEMPLATE, "", "");
        
        addDefaultLayoutDef("multiLocationKritiq", MULTILOCATIONKRITIQ, "", "");
        addDefaultLayoutDef("deliveryZonesWithMap", DELIVERYZONEWITHMAP, "", "");
        addDefaultLayoutDef("notificationMethodLink", NOTIFICATION_METHOD_LINK, "", "");
        
         addDefaultLayoutDef("receipt", RECEIPT, "", "");
         addDefaultLayoutDef("menu", MENUV2, HEADERV2, "");
         addDefaultLayoutDef("headerV2", HEADERV2, "", "");
         addDefaultLayoutDef("infoV2", INFOV2, "", "");
         addDefaultLayoutDef("cartV2", CARTV2,"","");
         addDefaultLayoutDef("deliveryTimings", DELIVERY_TIMINGS, "", "");
         addDefaultLayoutDef("userProfileV2", USERPROFILEV2, "", "");
         addDefaultLayoutDef("myOrderV2", MYORDERV2, "", "");
         addDefaultLayoutDef("loginV2", LOGINV2, "", "");
         addDefaultLayoutDef("forgotPasswordV2", FORGOTPASSWORDV2, "", "");
         addDefaultLayoutDef("changePasswordV2", CHANGEPASSWORDV2, "", "");
         addDefaultLayoutDef("setGuestPasswordV2", SETGUESTPASSWORDV2, "", "");
         addDefaultLayoutDef("footerV2", FOOTERV2, "", "");
         
         addDefaultLayoutDef("merchantTaxes", GETALLTAXES, "", "");
         addDefaultLayoutDef("editTaxRates", EDITTAXRATES, "", "");
         
         addDefaultLayoutDef("merchantSliders", MERCHANTSLIDERS, "", "");
         addDefaultLayoutDef("freekwentClover", FREEKWENT_CLOVER, "", "");
         
         addDefaultLayoutDef("itemCategory", INVENTORY_CATEGORY, "", "");
         addDefaultLayoutDef("itemModifierGroup", INVENTORY_MODIFIERGROUP, "", "");
         addDefaultLayoutDef("masterFormOfitemModifierGroup", MASTER_FORM_OF_ITEM_MODIFIRE_GROUP, "", "");
         addDefaultLayoutDef("masterFormOfItemTexes", MASTER_FORM_ITEM_TEXES, "", "");
         
         addDefaultLayoutDef("customerFeedbackNew", CUSTOMER_FEEDBACK_FORM_NEW, "", "");
         
         addDefaultLayoutDef("createItem", CREATE_ITEM, "", "");
         addDefaultLayoutDef("createModifierGroup", CREATE_MODIFIER_GROUPS, "", "");
         addDefaultLayoutDef("createCategory", CREATE_CATEGORY, "", "");
         addDefaultLayoutDef("createModifier", CREATE_MODIFIER, "", "");
         addDefaultLayoutDef("createPizzaTemplate", CREATE_PIZZA_TEMPLATE, "", "");
         addDefaultLayoutDef("pizzaTemplateMpping", PIZZA_TEMPLATE_MAPPING, "", "");
         addDefaultLayoutDef("createPizzaTopping", CREATE_PIZZA_TOPPING, "", "");
         addDefaultLayoutDef("pizzaCategoryMap", PIZZA_CATEGORY_MAPPING, "", "");
         addDefaultLayoutDef("editPizzaTopping", EDIT_PIZZA_TOPPING, "", "");
         addDefaultLayoutDef("pizzaCrust", PIZZA_CRUST, "", "");
         addDefaultLayoutDef("templateTaxMap", TEMPLATE_TAX_MAP, "", "");
         addDefaultLayoutDef("desktopVerson", DOWNLOAD_UPDATED_VERSION, "", "");
         addDefaultLayoutDef("currentAndUpdatedVerson", DOWNLOAD_CURRENT_AND_UPDATED_VERSION, "", "");
         addDefaultLayoutDef("addPrinter", ADD_PRINTER, "", "");
         addDefaultLayoutDef("addCloudPrinter", ADD_CLOUD_PRINTER, "", "");
         addDefaultLayoutDef("offlineCustomer", OFFLINECUSTOMERS, "", "");
         addDefaultLayoutDef("createPizzaSize", CREATE_SIZE, "", "");
         
         addDefaultLayoutDef("createNewTax", CREATE_NEW_TAX, "", "");
         
         addDefaultLayoutDef("pizzaCategory", PIZZA_CATEGORY, "", "");
         addDefaultLayoutDef("orderNotification", SET_ORDER_NOTIFICSTION, "", "");
         
         addDefaultLayoutDef("editPizzaCrust", EDIT_PIZZA_CRUST, "", "");
         
         addDefaultLayoutDef("createPizzaCrust", CREATE_PIZZA_CRUST, "", "");
     
         addDefaultLayoutDef("templateCrustMap", TEMPLATE_CRUST_MAP, "", "");

         addDefaultLayoutDef("itemTaxMap", ITEM_TAX_MAP, "", "");
         
         addDefaultLayoutDef("pizzaSize", PIZZA_SIZE, "", "");
         
         addDefaultLayoutDef("editPizzaSize", EDIT_PIZZA_SIZE, "", "");
        
         addDefaultLayoutDef("user", USER, "", "");
         
         addDefaultLayoutDef("editUser", EDIT_USER, "", "");
         
         addDefaultLayoutDef("addUser", ADD_USER, "", "");
         
         addDefaultLayoutDef("uberStore", UBER_STORE, "", "");
         
         addDefaultLayoutDef("uberReceipt", UBER_RECEIPT, "", "");
         
         addDefaultLayoutDef("environment", ENVIRONMENT, "", "");
        
        addDefaultLayoutDef("virtualFund", VIRTUALFUND, "", "");
        
        addDefaultLayoutDef("editVirtualFund", EDIT_VIRTUALFUND, "", "");
        
        addDefaultLayoutDef("addVirtualFund", ADD_VIRTUALFUND, "", "");
        addDefaultLayoutDef("merchantSocialPlatform", MERCHANT_SOCIAL_PLATFORM, "", "");
        
        addDefaultLayoutDef("appStatus", APP_STATUS, "", "");
    }
}
