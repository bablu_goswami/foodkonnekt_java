/**
 * SmartPaymentsSoap.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.TPISoft.SmartPayments;

public interface SmartPaymentsSoap extends java.rmi.Remote {

    /**

     */
    public com.TPISoft.SmartPayments.Response processCreditCard(java.lang.String userName, java.lang.String password, java.lang.String transType, java.lang.String cardNum, java.lang.String expDate, java.lang.String magData, java.lang.String nameOnCard, java.lang.String amount, java.lang.String invNum, java.lang.String PNRef, java.lang.String zip, java.lang.String street, java.lang.String CVNum, java.lang.String extData) throws java.rmi.RemoteException;

    /**

     */
    public com.TPISoft.SmartPayments.Response processDebitCard(java.lang.String userName, java.lang.String password, java.lang.String transType, java.lang.String cardNum, java.lang.String expDate, java.lang.String magData, java.lang.String nameOnCard, java.lang.String amount, java.lang.String invNum, java.lang.String PNRef, java.lang.String pin, java.lang.String registerNum, java.lang.String sureChargeAmt, java.lang.String cashBackAmt, java.lang.String extData) throws java.rmi.RemoteException;

    /**

     */
    public com.TPISoft.SmartPayments.Response processEBTCard(java.lang.String userName, java.lang.String password, java.lang.String transType, java.lang.String cardNum, java.lang.String expDate, java.lang.String magData, java.lang.String nameOnCard, java.lang.String amount, java.lang.String invNum, java.lang.String PNRef, java.lang.String pin, java.lang.String registerNum, java.lang.String sureChargeAmt, java.lang.String cashBackAmt, java.lang.String extData) throws java.rmi.RemoteException;

    /**

     */
    public com.TPISoft.SmartPayments.Response processLoyaltyCard(java.lang.String userName, java.lang.String password, java.lang.String transType, java.lang.String cardNum, java.lang.String expDate, java.lang.String magData, java.lang.String amount, java.lang.String invNum, java.lang.String PNRef, java.lang.String extData) throws java.rmi.RemoteException;

    /**

     */
    public com.TPISoft.SmartPayments.Response processGiftCard(java.lang.String userName, java.lang.String password, java.lang.String transType, java.lang.String cardNum, java.lang.String expDate, java.lang.String magData, java.lang.String amount, java.lang.String invNum, java.lang.String PNRef, java.lang.String extData) throws java.rmi.RemoteException;

    /**

     */
    public com.TPISoft.SmartPayments.Response processCheck(java.lang.String userName, java.lang.String password, java.lang.String transType, java.lang.String checkNum, java.lang.String transitNum, java.lang.String accountNum, java.lang.String amount, java.lang.String MICR, java.lang.String nameOnCheck, java.lang.String DL, java.lang.String SS, java.lang.String DOB, java.lang.String stateCode, java.lang.String checkType, java.lang.String extData) throws java.rmi.RemoteException;

    /**

     */
    public com.TPISoft.SmartPayments.Response checkThirdPartySupport(java.lang.String userName, java.lang.String password, java.lang.String thirdPartyName, java.lang.String transType, java.lang.String extData) throws java.rmi.RemoteException;

    /**

     */
    public com.TPISoft.SmartPayments.Response getInfo(java.lang.String userName, java.lang.String password, java.lang.String transType, java.lang.String extData) throws java.rmi.RemoteException;

    /**

     */
    public com.TPISoft.SmartPayments.Response processSignature(java.lang.String userName, java.lang.String password, java.lang.String signatureType, java.lang.String signatureData, java.lang.String PNRef, java.lang.String result, java.lang.String authCode, java.lang.String extData) throws java.rmi.RemoteException;
}
