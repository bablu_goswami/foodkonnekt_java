/**
 * SmartPaymentsLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.TPISoft.SmartPayments;

public class SmartPaymentsLocator extends org.apache.axis.client.Service implements com.TPISoft.SmartPayments.SmartPayments {

/**
 * SmartPayments - ePayment Web Service
 */

    public SmartPaymentsLocator() {
    }


    public SmartPaymentsLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public SmartPaymentsLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for SmartPaymentsSoap
    private java.lang.String SmartPaymentsSoap_address = "https://gatewaystage.itstgate.com/SmartPayments/transact.asmx";

    public java.lang.String getSmartPaymentsSoapAddress() {
        return SmartPaymentsSoap_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String SmartPaymentsSoapWSDDServiceName = "SmartPaymentsSoap";

    public java.lang.String getSmartPaymentsSoapWSDDServiceName() {
        return SmartPaymentsSoapWSDDServiceName;
    }

    public void setSmartPaymentsSoapWSDDServiceName(java.lang.String name) {
        SmartPaymentsSoapWSDDServiceName = name;
    }

    public com.TPISoft.SmartPayments.SmartPaymentsSoap getSmartPaymentsSoap() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(SmartPaymentsSoap_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getSmartPaymentsSoap(endpoint);
    }

    public com.TPISoft.SmartPayments.SmartPaymentsSoap getSmartPaymentsSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.TPISoft.SmartPayments.SmartPaymentsSoapStub _stub = new com.TPISoft.SmartPayments.SmartPaymentsSoapStub(portAddress, this);
            _stub.setPortName(getSmartPaymentsSoapWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setSmartPaymentsSoapEndpointAddress(java.lang.String address) {
        SmartPaymentsSoap_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.TPISoft.SmartPayments.SmartPaymentsSoap.class.isAssignableFrom(serviceEndpointInterface)) {
                com.TPISoft.SmartPayments.SmartPaymentsSoapStub _stub = new com.TPISoft.SmartPayments.SmartPaymentsSoapStub(new java.net.URL(SmartPaymentsSoap_address), this);
                _stub.setPortName(getSmartPaymentsSoapWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("SmartPaymentsSoap".equals(inputPortName)) {
            return getSmartPaymentsSoap();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://TPISoft.com/SmartPayments/", "SmartPayments");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://TPISoft.com/SmartPayments/", "SmartPaymentsSoap"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("SmartPaymentsSoap".equals(portName)) {
            setSmartPaymentsSoapEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
