package com.TPISoft.SmartPayments;

public class BridgePay {

	private String UserName;
	private String Password;
	private String TransType;
	private String CardNum;
	private String ExpDate;
	private String MagData;
	private String NameOnCard;

	private String Amount;
	private String InvNum;
	private String PNRef;
	private String Zip;
	private String Street;
	private String CVNum;
	private String ExtData;

	public String getUserName() {
		return UserName;
	}

	public void setUserName(String userName) {
		UserName = userName;
	}

	public String getPassword() {
		return Password;
	}

	public void setPassword(String password) {
		Password = password;
	}

	public String getTransType() {
		return TransType;
	}

	public void setTransType(String transType) {
		TransType = transType;
	}

	public String getCardNum() {
		return CardNum;
	}

	public void setCardNum(String cardNum) {
		CardNum = cardNum;
	}

	public String getExpDate() {
		return ExpDate;
	}

	public void setExpDate(String expDate) {
		ExpDate = expDate;
	}

	public String getMagData() {
		return MagData;
	}

	public void setMagData(String magData) {
		MagData = magData;
	}

	public String getNameOnCard() {
		return NameOnCard;
	}

	public void setNameOnCard(String nameOnCard) {
		NameOnCard = nameOnCard;
	}

	public String getAmount() {
		return Amount;
	}

	public void setAmount(String amount) {
		Amount = amount;
	}

	public String getInvNum() {
		return InvNum;
	}

	public void setInvNum(String invNum) {
		InvNum = invNum;
	}

	public String getPNRef() {
		return PNRef;
	}

	public void setPNRef(String pNRef) {
		PNRef = pNRef;
	}

	public String getZip() {
		return Zip;
	}

	public void setZip(String zip) {
		Zip = zip;
	}

	public String getStreet() {
		return Street;
	}

	public void setStreet(String street) {
		Street = street;
	}

	public String getCVNum() {
		return CVNum;
	}

	public void setCVNum(String cVNum) {
		CVNum = cVNum;
	}

	public String getExtData() {
		return ExtData;
	}

	public void setExtData(String extData) {
		ExtData = extData;
	}

}
