/**
 * SmartPayments.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.TPISoft.SmartPayments;

public interface SmartPayments extends javax.xml.rpc.Service {

/**
 * SmartPayments - ePayment Web Service
 */
    public java.lang.String getSmartPaymentsSoapAddress();

    public com.TPISoft.SmartPayments.SmartPaymentsSoap getSmartPaymentsSoap() throws javax.xml.rpc.ServiceException;

    public com.TPISoft.SmartPayments.SmartPaymentsSoap getSmartPaymentsSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
