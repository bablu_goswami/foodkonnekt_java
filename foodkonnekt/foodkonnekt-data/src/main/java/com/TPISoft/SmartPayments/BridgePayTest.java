package com.TPISoft.SmartPayments;

import java.rmi.RemoteException;

import com.foodkonnekt.util.IConstant;
import org.springframework.core.env.Environment;

public class BridgePayTest {

	public static void main(String[] args) {
	

			
//		chargeCard(environment);
//	//	captureCard("1791299");
//	voidCard("1791301",environment);
			
			/*System.setProperty("org.apache.commons.logging.LogFactory", 
					"org.apache.commons.logging.impl.LogFactoryImpl");
					System.setProperty("org.apache.commons.logging.Log", 
					"org.apache.commons.logging.impl.Log4JLogger");*/	
	}
	
	public static void chargeCard(Environment environment){
		try{
		SmartPaymentsSoapProxy soapProxy=new SmartPaymentsSoapProxy(environment.getProperty("ENDPOINT_PROD"));
		
		Response response=  soapProxy.processCreditCard("mKon5277", "M2581fk4", "auth",
				"371449635392376", "1018", "Payment Card Track II Mag-Stripe Data", 
				"test", "101", "2BGJ7", "", "", "", "1234", "<Force>C</Force>");
		//4242424242424242 , 4005550000000019 showing cvv2 mismatch
		//4111111111111111  5499740000000057     6011000991001201 371449635392376 
		//36999999999999 working with cvv(for auth and void only)
		//4005550000000019  (work if cvv blank)
		//5439750001500347  
//		/5499740000000057    6011000991001201
		//374255312721002 4055016727870315     5454545454545454     371449635398431
	System.out.println(response.getExtData());
	System.out.println("Auth code-"+response.getAuthCode());
	System.out.println("PNRef-"+response.getPNRef());
	System.out.println("msg-"+response.getMessage());
	System.out.println("response message-"+response.getRespMSG());
	System.out.println("result-"+response.getResult());
	
		
		}catch(Exception e){
			
		}
		
	}
	
	public static void captureCard(String id,Environment environment){
		try{
			SmartPaymentsSoapProxy soapProxy=new SmartPaymentsSoapProxy(environment.getProperty("ENDPOINT_PROD"));
			
			/*Response response=  soapProxy.processCreditCard("mKon5277", "M2581fk4", "Capture",
					"36999999999999", "1218", "Payment Card Track II Mag-Stripe Data", 
					"test", "10", "2BGJ7", id, "", "", "238", "<Force>C</Force>");*/
			Response response=  soapProxy.processCreditCard("mKon5277", "M2581fk4", "Force",
					"", "", "Payment Card Track II Mag-Stripe Data", 
					"", "", "", id, "", "", "", "<Force>C</Force>");
			
			//4005550000000019 (work if cvv blank)
			//5439750001500347  5439750001500347 1786930
			//36999999999999 working with cvv
			//374255312721002 4055016727870315     5454545454545454     371449635398431
		System.out.println(response.getExtData());
		System.out.println(response.getAuthCode());
		System.out.println(response.getPNRef());
		System.out.println(response.getMessage());
		System.out.println(response.getRespMSG());
			
			
			}catch(Exception e){
				
			}
	}
	
	public static Response voidCard(String id,Environment environment){
		
		Response response =  new Response();
		try{
			SmartPaymentsSoapProxy soapProxy=new SmartPaymentsSoapProxy(environment.getProperty("ENDPOINT_PROD"));
			
			/*Response response=  soapProxy.processCreditCard("mKon5277", "M2581fk4", "void",
					"36999999999999", "1218", "Payment Card Track II Mag-Stripe Data", 
					"test", "10", "2BGJ7", id, "", "", "238", "<Force>C</Force>");*/
			 response=  soapProxy.processCreditCard("mKon5277", "M2581fk4", "void",
					"", "", "Payment Card Track II Mag-Stripe Data", 
					"", "", "", id, "", "", "", "<Force>C</Force>");
			
			//4005550000000019 (work if cvv blank)
			//5439750001500347  5439750001500347 1786930
			//36999999999999 working with cvv
			//374255312721002 4055016727870315     5454545454545454     371449635398431
		System.out.println("response.getExtData()"+response.getExtData());
		System.out.println(response.getAuthCode());
		System.out.println(response.getPNRef());
		System.out.println(response.getMessage());
		System.out.println(response.getRespMSG());
			
			
			}catch(Exception e){
				
			}
		return response;
	}

}
