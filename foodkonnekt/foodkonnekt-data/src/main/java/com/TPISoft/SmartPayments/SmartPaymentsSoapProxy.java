package com.TPISoft.SmartPayments;

public class SmartPaymentsSoapProxy implements com.TPISoft.SmartPayments.SmartPaymentsSoap {
  private String _endpoint = null;
  private com.TPISoft.SmartPayments.SmartPaymentsSoap smartPaymentsSoap = null;
  
  public SmartPaymentsSoapProxy() {
    _initSmartPaymentsSoapProxy();
  }
  
  public SmartPaymentsSoapProxy(String endpoint) {
    _endpoint = endpoint;
    _initSmartPaymentsSoapProxy();
  }
  
  private void _initSmartPaymentsSoapProxy() {
    try {
      smartPaymentsSoap = (new com.TPISoft.SmartPayments.SmartPaymentsLocator()).getSmartPaymentsSoap();
      if (smartPaymentsSoap != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)smartPaymentsSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)smartPaymentsSoap)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (smartPaymentsSoap != null)
      ((javax.xml.rpc.Stub)smartPaymentsSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.TPISoft.SmartPayments.SmartPaymentsSoap getSmartPaymentsSoap() {
    if (smartPaymentsSoap == null)
      _initSmartPaymentsSoapProxy();
    return smartPaymentsSoap;
  }
  
  public com.TPISoft.SmartPayments.Response processCreditCard(java.lang.String userName, java.lang.String password, java.lang.String transType, java.lang.String cardNum, java.lang.String expDate, java.lang.String magData, java.lang.String nameOnCard, java.lang.String amount, java.lang.String invNum, java.lang.String PNRef, java.lang.String zip, java.lang.String street, java.lang.String CVNum, java.lang.String extData) throws java.rmi.RemoteException{
    if (smartPaymentsSoap == null)
      _initSmartPaymentsSoapProxy();
    return smartPaymentsSoap.processCreditCard(userName, password, transType, cardNum, expDate, magData, nameOnCard, amount, invNum, PNRef, zip, street, CVNum, extData);
  }
  
  public com.TPISoft.SmartPayments.Response processDebitCard(java.lang.String userName, java.lang.String password, java.lang.String transType, java.lang.String cardNum, java.lang.String expDate, java.lang.String magData, java.lang.String nameOnCard, java.lang.String amount, java.lang.String invNum, java.lang.String PNRef, java.lang.String pin, java.lang.String registerNum, java.lang.String sureChargeAmt, java.lang.String cashBackAmt, java.lang.String extData) throws java.rmi.RemoteException{
    if (smartPaymentsSoap == null)
      _initSmartPaymentsSoapProxy();
    return smartPaymentsSoap.processDebitCard(userName, password, transType, cardNum, expDate, magData, nameOnCard, amount, invNum, PNRef, pin, registerNum, sureChargeAmt, cashBackAmt, extData);
  }
  
  public com.TPISoft.SmartPayments.Response processEBTCard(java.lang.String userName, java.lang.String password, java.lang.String transType, java.lang.String cardNum, java.lang.String expDate, java.lang.String magData, java.lang.String nameOnCard, java.lang.String amount, java.lang.String invNum, java.lang.String PNRef, java.lang.String pin, java.lang.String registerNum, java.lang.String sureChargeAmt, java.lang.String cashBackAmt, java.lang.String extData) throws java.rmi.RemoteException{
    if (smartPaymentsSoap == null)
      _initSmartPaymentsSoapProxy();
    return smartPaymentsSoap.processEBTCard(userName, password, transType, cardNum, expDate, magData, nameOnCard, amount, invNum, PNRef, pin, registerNum, sureChargeAmt, cashBackAmt, extData);
  }
  
  public com.TPISoft.SmartPayments.Response processLoyaltyCard(java.lang.String userName, java.lang.String password, java.lang.String transType, java.lang.String cardNum, java.lang.String expDate, java.lang.String magData, java.lang.String amount, java.lang.String invNum, java.lang.String PNRef, java.lang.String extData) throws java.rmi.RemoteException{
    if (smartPaymentsSoap == null)
      _initSmartPaymentsSoapProxy();
    return smartPaymentsSoap.processLoyaltyCard(userName, password, transType, cardNum, expDate, magData, amount, invNum, PNRef, extData);
  }
  
  public com.TPISoft.SmartPayments.Response processGiftCard(java.lang.String userName, java.lang.String password, java.lang.String transType, java.lang.String cardNum, java.lang.String expDate, java.lang.String magData, java.lang.String amount, java.lang.String invNum, java.lang.String PNRef, java.lang.String extData) throws java.rmi.RemoteException{
    if (smartPaymentsSoap == null)
      _initSmartPaymentsSoapProxy();
    return smartPaymentsSoap.processGiftCard(userName, password, transType, cardNum, expDate, magData, amount, invNum, PNRef, extData);
  }
  
  public com.TPISoft.SmartPayments.Response processCheck(java.lang.String userName, java.lang.String password, java.lang.String transType, java.lang.String checkNum, java.lang.String transitNum, java.lang.String accountNum, java.lang.String amount, java.lang.String MICR, java.lang.String nameOnCheck, java.lang.String DL, java.lang.String SS, java.lang.String DOB, java.lang.String stateCode, java.lang.String checkType, java.lang.String extData) throws java.rmi.RemoteException{
    if (smartPaymentsSoap == null)
      _initSmartPaymentsSoapProxy();
    return smartPaymentsSoap.processCheck(userName, password, transType, checkNum, transitNum, accountNum, amount, MICR, nameOnCheck, DL, SS, DOB, stateCode, checkType, extData);
  }
  
  public com.TPISoft.SmartPayments.Response checkThirdPartySupport(java.lang.String userName, java.lang.String password, java.lang.String thirdPartyName, java.lang.String transType, java.lang.String extData) throws java.rmi.RemoteException{
    if (smartPaymentsSoap == null)
      _initSmartPaymentsSoapProxy();
    return smartPaymentsSoap.checkThirdPartySupport(userName, password, thirdPartyName, transType, extData);
  }
  
  public com.TPISoft.SmartPayments.Response getInfo(java.lang.String userName, java.lang.String password, java.lang.String transType, java.lang.String extData) throws java.rmi.RemoteException{
    if (smartPaymentsSoap == null)
      _initSmartPaymentsSoapProxy();
    return smartPaymentsSoap.getInfo(userName, password, transType, extData);
  }
  
  public com.TPISoft.SmartPayments.Response processSignature(java.lang.String userName, java.lang.String password, java.lang.String signatureType, java.lang.String signatureData, java.lang.String PNRef, java.lang.String result, java.lang.String authCode, java.lang.String extData) throws java.rmi.RemoteException{
    if (smartPaymentsSoap == null)
      _initSmartPaymentsSoapProxy();
    return smartPaymentsSoap.processSignature(userName, password, signatureType, signatureData, PNRef, result, authCode, extData);
  }
  
  
}