package com.firstData.paymentGatway;/*
Copyright 2014, CardConnect (http://www.cardconnect.com)

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.
*/

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.foodkonnekt.util.IConstant;

/**
 * Client Example showing various service request calls to CardConnect using REST
 */
@SuppressWarnings("unchecked")
public class CardConnectRestClientExample 
{
	// https://fts.cardconnect.com:6443/cardconnect
	public static final String ENDPOINT = IConstant.firstDataEndPoint;
	public static final String USERNAME = "testing";
	public static final String PASSWORD = "testing123";
	private static final Logger LOGGER = LoggerFactory.getLogger(CardConnectRestClientExample.class);

	public static void main (String args[])
	{ 
		java.util.Date  date = new java.util.Date();
		SimpleDateFormat  dateFormat =  new  SimpleDateFormat("yyyyMMdd");
		LOGGER.info(dateFormat.format(new Date()));
		
		
	//	refundTransaction("197580137254");
		
		// Send an Auth Transaction request
		//String retref = authTransaction();
		
		//LOGGER.info(retref);
		
		//("173769204557");
		
		//voidTransaction("173769204557");
		//496160873888
		
	//LOGGER.info( 	validateLoginCredentails("1","496160873888","testing","testing123"));
	
/*	LOGGER.info(acesss);
*/		
		//LOGGER.info("Auth completed-------------> : "+retref );
		
		//captureTransaction(retref);
		
		//LOGGER.info("retref "+retref);
		// Void transaction
/*		voidTransaction(retref);
		
		// Send an Auth Transaction w/ user fields
		retref = authTransactionWithUserFields();
		// Inquire transaction
		inquireTransaction(retref);
		
		// Send an Auth w/ Capture
		retref = authTransactionWithCapture();
		// Void 
		voidTransaction(retref);
		
		// Send normal Auth
		retref = authTransaction();
		// Explicit capture
		captureTransaction(retref);
		
		// Settlement Status
		settlementStatusTransaction();
		
		// Deposit Status
		depositTransaction();
		
		// Auth with Profile
		String profileid = authTransactionWithProfile();
		
		// Get profile
		getProfile(profileid);
		
		// Delete profile
		deleteProfile(profileid);
		
		// Create profile
		addProfile();
*/	}
	
	
	/**
	 * Authorize Transaction REST Example
	 * @return
	 */
	public static String authTransaction() {
		LOGGER.info("\nAuthorization Request");
		
		// Create Authorization Transaction request
		JSONObject request = new JSONObject();
		// Merchant ID
		request.put("merchid", "496160873888");
		// Card Type
		request.put("accttype", "VI");
		// Card Number
		request.put("account", "5454545454545454");
		// Card Expiry
		request.put("expiry", "0918");
		// Card CCV2
		request.put("cvv2", "776");
		// Transaction amount
		request.put("amount", "100");
		// Transaction currency
		request.put("currency", "USD");
		// Order ID
		request.put("orderid", "12345");
		// Cardholder Name
		request.put("name", "Test User");
		// Cardholder Address
		request.put("Street", "123 Test St");
		// Cardholder City
		request.put("city", "TestCity");
		// Cardholder State
		request.put("region", "TestState");
		// Cardholder Country
		request.put("country", "US");
		// Cardholder Zip-Code
		request.put("postal", "11111");
		// Return a token for this card number
		request.put("tokenize", "Y");
		
		// Create the REST client
		CardConnectRestClient client = new CardConnectRestClient(ENDPOINT, USERNAME, PASSWORD);
		
		//client.
		// Send an AuthTransaction request
		JSONObject response = client.authorizeTransaction(request);
		
		
		
		// Handle response
		Set<String> keys = response.keySet();
		for (String key : keys) 
			LOGGER.info("gygh"+key + ": " + response.get(key));
		
		return (String)response.get("retref");
	}
	
	
	/**
	 * Authorize Transaction with User Fields REST Example
	 * @returnso
	 */
	public static String authTransactionWithUserFields() {
		LOGGER.info("\nAuthorization With User Fields Request");
		
		// Create Authorization Transaction request
		JSONObject request = new JSONObject();
		// Merchant ID
		request.put("merchid", "496400000840");
		// Card Type
		request.put("accttype", "VI");
		// Card Number
		request.put("account", "4444333322221111");
		// Card Expiry
		request.put("expiry", "0914");
		// Card CCV2
		request.put("cvv2", "776");
		// Transaction amount
		request.put("amount", "100");
		// Transaction currency
		request.put("currency", "USD");
		// Order ID
		request.put("orderid", "12345");
		// Cardholder Name
		request.put("name", "Test User");
		// Cardholder Address
		request.put("Street", "123 Test St");
		// Cardholder City
		request.put("city", "TestCity");
		// Cardholder State
		request.put("region", "TestState");
		// Cardholder Country
		request.put("country", "US");
		// Cardholder Zip-Code
		request.put("postal", "11111");
		// Return a token for this card number
		request.put("tokenize", "Y");
		
		// Create user fields
		JSONArray fields = new JSONArray();
		JSONObject field = new JSONObject();
		field.put("Field1", "Value1");
		fields.add(field);
		request.put("userfields", fields);
		
		// Create the REST client
		CardConnectRestClient client = new CardConnectRestClient(ENDPOINT, USERNAME, PASSWORD);
		
		// Send an AuthTransaction request
		JSONObject response = client.authorizeTransaction(request);
		
		// Handle response
		Set<String> keys = response.keySet();
		for (String key : keys) 
			LOGGER.info(key + ": " + response.get(key));
		
		return (String)response.get("retref");
	}
	
	
	/**
	 * Authorize Transaction With Capture REST Example
	 * @return
	 */
	public static String authTransactionWithCapture() {
		LOGGER.info("\nAuthorization With Capture Request");
		
		// Create Authorization Transaction request
		JSONObject request = new JSONObject();
		// Merchant ID
		request.put("merchid", "496400000840");
		// Card Type
		request.put("accttype", "VI");
		// Card Number
		request.put("account", "4444333322221111");
		// Card Expiry
		request.put("expiry", "0914");
		// Card CCV2
		request.put("cvv2", "776");
		// Transaction amount
		request.put("amount", "100");
		// Transaction currency
		request.put("currency", "USD");
		// Order ID
		request.put("orderid", "12345");
		// Cardholder Name
		request.put("name", "Test User");
		// Cardholder Address
		request.put("Street", "123 Test St");
		// Cardholder City
		request.put("city", "TestCity");
		// Cardholder State
		request.put("region", "TestState");
		// Cardholder Country
		request.put("country", "US");
		// Cardholder Zip-Code
		request.put("postal", "11111");
		// Return a token for this card number
		request.put("tokenize", "Y");
		// Capture auth
		request.put("capture", "Y");
		
		// Create the REST client
		CardConnectRestClient client = new CardConnectRestClient(ENDPOINT, USERNAME, PASSWORD);
		
		// Send an AuthTransaction request
		JSONObject response = client.authorizeTransaction(request);
		
		// Handle response
		Set<String> keys = response.keySet();
		for (String key : keys) 
			LOGGER.info(key + ": " + response.get(key));
		
		return (String)response.get("retref");
	}
	
	
	/**
	 * Authorize Transaction with Profile REST Example
	 * @return
	 */
	public static String authTransactionWithProfile() {
		LOGGER.info("\nAuthorization With Profile Request");
		
		JSONObject request = new JSONObject();
		request.put("merchid", "496400000840");
		request.put("accttype", "VI");
		request.put("account", "4444333322221111");
		request.put("expiry", "0914");
		request.put("cvv2", "776");
		request.put("amount", "100");
		request.put("currency", "USD");
		request.put("orderid", "12345");
		request.put("name", "Test User");
		request.put("Street", "123 Test St");
		request.put("city", "TestCity");
		request.put("region", "TestState");
		request.put("country", "US");
		request.put("postal", "11111");
		request.put("tokenize", "Y");
		request.put("profile", "Y");
		
		CardConnectRestClient client = new CardConnectRestClient(ENDPOINT, USERNAME, PASSWORD);
		
		JSONObject response = client.authorizeTransaction(request);
		
		Set<String> keys = response.keySet();
		for (String key : keys) 
			LOGGER.info(key + ": " + response.get(key));
		
		return (String)response.get("profileid");
	}
	
	
	/**
	 * Capture Transaction REST Example
	 * @param retref
	 */
	public static JSONObject captureTransaction(String retref,  String username, String password, JSONObject request) 
	{
		LOGGER.info("\nCapture Transaction Request");
		CardConnectRestClient client = new CardConnectRestClient(ENDPOINT, username, password);
		JSONObject response = client.captureTransaction(request);
		return response;	
	}
	
	
	/**
	 * Void Transaction REST Example
	 * @param retref
	 */
	public static JSONObject voidTransaction(String username, String password, JSONObject request )
	{
		LOGGER.info("\nVoid Transaction Request");
		CardConnectRestClient client = new CardConnectRestClient(ENDPOINT, username, password);
		JSONObject response = client.voidTransaction(request);
		
		return response;
		
	}
	
	
	/**
	 * Refund Transaction REST Example
	 * @param retref
	 */
	public static JSONObject refundTransaction(org.json.simple.JSONObject request) {
		LOGGER.info("\nRefund Transaction Request");
/*		
		// Create Update Transaction request
		JSONObject request = new JSONObject();
		// Merchant ID
		request.put("merchid", "496400000840");
		// Transaction amount
		request.put("amount","-100");
		// Transaction currency
		request.put("currency", "USD");
		// Return Reference code from authorization request
		request.put("retref", retref);
		*/
		// Create the CardConnect REST client
		CardConnectRestClient client = new CardConnectRestClient(ENDPOINT, USERNAME, PASSWORD);
		
		// Send an refundTransaction request
		JSONObject response = client.refundTransaction(request);
		
		
		
		
		// Handle response
		Set<String> keys = response.keySet();
		for (String key : keys) 
			LOGGER.info(key + ": " + response.get(key));
		
		
		return response;
	}
	
	
	/**
	 * Inquire Transaction REST Example
	 * @param retref
	 */
	public static void inquireTransaction(String retref) {
		LOGGER.info("\nInquire Transaction Request");
		String merchid = "496400000840";
		
		// Create the CardConnect REST client
		CardConnectRestClient client = new CardConnectRestClient(ENDPOINT, USERNAME, PASSWORD);
		
		// Send an inquire Transaction request
		JSONObject response = client.inquireTransaction(merchid, retref);
		
		// Handle response
		if (response != null) {
			Set<String> keys = response.keySet();
			for (String key : keys) 
				LOGGER.info(key + ": " + response.get(key));
		}
	}
	
	
	/**
	 * Settlement Status REST Example
	 */
	public static void settlementStatusTransaction() {
		LOGGER.info("\nSettlement Status Transaction Request");
		// Merchant ID
		String merchid = "496400000840";
		String date = "0404";
		
		// Create the CardConnect REST client
		CardConnectRestClient client = new CardConnectRestClient(ENDPOINT, USERNAME, PASSWORD);
		
		JSONArray responses = client.settlementStatus(merchid, date);
		//JSONArray responses = client.settlementStatus(null, null);
		
		// Handle response
		if (responses != null) {
			for (int i=0; i<responses.size(); i++) {
				JSONObject response = (JSONObject)responses.get(i);
				Set<String> keys = response.keySet();
				for (String key : keys) {
					if ("txns".equals(key)) {
						LOGGER.info("transactions: ");
						JSONArray txns = (JSONArray) response.get(key);
						for (int j=0; j<txns.size(); j++) {
							LOGGER.info("  ===");
							JSONObject txn = (JSONObject)txns.get(j);
							Set<String> txnkeys = txn.keySet();
							for (String txnkey : txnkeys) 
								LOGGER.info("  " + txnkey + ": " + txn.get(txnkey));
						}
					} else {
						LOGGER.info(key + ": " + response.get(key));
					}
				}
			}
		}
	}
	
	
	/** 
	 * Deposit Transaction REST Example
	 */
	public static void depositTransaction() {
		LOGGER.info("\nDeposit Transaction Request");
		// Merchant ID
		String merchid = "496400000840";
		// Date
		String date = "20140131";
		
		// Create the CardConnect REST client
		CardConnectRestClient client = new CardConnectRestClient(ENDPOINT, USERNAME, PASSWORD);
		
		JSONObject response = client.depositStatus(merchid, date);
		
		// Handle response
		if (response != null) {
			Set<String> keys = response.keySet();
			for (String key : keys) {
				if ("txns".equals(key)) {
					LOGGER.info("transactions: ");
					JSONArray txns = (JSONArray) response.get(key);
					for (int i=0; i<txns.size(); i++) {
						LOGGER.info("  ===");
						JSONObject txn = (JSONObject)txns.get(i);
						Set<String> txnkeys = txn.keySet();
						for (String txnkey : txnkeys) 
							LOGGER.info("  " + txnkey + ": " + txn.get(txnkey));
					}
				} else {
					LOGGER.info(key + ": " + response.get(key));
				}
			}
		}
	}
	
	
	/**
	 * Get Profile REST Example
	 * @param profileid
	 */
	public static boolean validateLoginCredentails(String profileid, String merchid, String username , String password) {
		LOGGER.info("\nGet Profile Request");
		
			String accountid = "1";
			LOGGER.info("username "+username);
			LOGGER.info("password "+password);
			CardConnectRestClient client = new CardConnectRestClient(ENDPOINT, username, password);
		
			JSONArray response;
			try 
			{
				response = client.profileGet(profileid, accountid, merchid);
				if(response==null)
				{
					return false;
				}
				else 
				{
					return true;
				}
			} catch (Exception e) 
			{
				LOGGER.error("error: " + e.getMessage());
				return false;
			}
		
		
	}
	
	
	/**
	 * Delete Profile REST Example
	 * @param profileid
	 */
	private static void deleteProfile(String profileid) {
		LOGGER.info("\nDelete Profile Request");
		// Merchant ID
		String merchid = "496400000840";
		String accountid = "";
		
		// Create the CardConnect REST client
		CardConnectRestClient client = new CardConnectRestClient(ENDPOINT, USERNAME, PASSWORD);
		
		// Delete profile using Profile Service
		JSONObject response = client.profileDelete(profileid, accountid, merchid);
		
		// Handle response
		if (response != null) {
			Set<String> keys = response.keySet();
			for (String key : keys) 
				LOGGER.info(key + ": " + response.get(key));
		}
	}
	
	
	/**
	 * Add Profile REST Example
	 */
	private static void addProfile() {
		LOGGER.info("\nAdd Profile Request");
		
		// Create Profile Request
		JSONObject request = new JSONObject();
		// Merchant ID
		request.put("merchid", "496400000840");
		// Default account
		request.put("defaultacct", "Y");
		// Card Number
		request.put("account", "4444333322221111");
		// Card Expiry
		request.put("expiry", "0914");
		// Cardholder Name
		request.put("name", "Test User");
		// Cardholder Address
		request.put("address", "123 Test St");
		// Cardholder City
		request.put("city", "TestCity");
		// Cardholder State
		request.put("region", "TestState");
		// Cardholder Country
		request.put("country", "US");
		// Cardholder Zip-Code
		request.put("postal", "11111");
		
		// Create the CardConnect REST client
		CardConnectRestClient client = new CardConnectRestClient(ENDPOINT, USERNAME, PASSWORD);
		
		// Create profile using Profile Service
		JSONObject response = client.profileCreate(request);
		
		// Handle response
		Set<String> keys = response.keySet();
		for (String key : keys) 
			LOGGER.info(key + ": " + response.get(key));
	}
}
