ALTER TABLE `pizzatemplatetopping`
ADD COLUMN `is_included`  tinyint(4) NULL AFTER `active`,
ADD COLUMN `is_replacable`  tinyint(4) NULL AFTER `is_included`;

