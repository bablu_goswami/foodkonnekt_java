ALTER TABLE `cardinfo`
ADD COLUMN `deleted_date`  varchar(60) NULL AFTER `customer_id`,
ADD COLUMN `is_active`  tinyint(2) NULL DEFAULT 0 AFTER `deleted_date`;

