ALTER TABLE `customer_feedback_answer`
ADD COLUMN `food_quality`  double(10,2) NULL AFTER `customer_feedback_id`,
ADD COLUMN `customer_service`  double(10,2) NULL AFTER `food_quality`,
ADD COLUMN `order_experience`  double(10,2) NULL AFTER `customer_service`;

