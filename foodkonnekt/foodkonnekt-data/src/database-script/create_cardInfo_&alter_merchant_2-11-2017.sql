CREATE TABLE `cardInfo` (
`id`  int(11) NOT NULL ,
`lastfournum`  integer(6) NULL ,
`firstsixnum`  integer(8) NULL ,
`expirationdate`  integer(8) NULL ,
`token`  varchar(60) NULL ,
`card_type`  varchar(60) NULL ,
`create_date`  datetime NULL ,
`update_date`  datetime NULL ,
`customer_id`  int(11) NULL ,
PRIMARY KEY (`id`),
FOREIGN KEY (`customer_id`) REFERENCES `customer` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
)
;

ALTER TABLE `cardInfo`
MODIFY COLUMN `lastfournum`  varchar(10) NULL DEFAULT NULL AFTER `id`,
MODIFY COLUMN `firstsixnum`  varchar(10) NULL DEFAULT NULL AFTER `lastfournum`,
MODIFY COLUMN `expirationdate`  varchar(10) NULL DEFAULT NULL AFTER `firstsixnum`;

ALTER TABLE `cardInfo`
MODIFY COLUMN `id`  int(11) NOT NULL AUTO_INCREMENT FIRST ;



ALTER TABLE `merchant`
ADD COLUMN `allowMultiPay`  tinyint(2) NULL DEFAULT 0 AFTER `fBTabIds`;