CREATE TABLE `merchant_kritiq_analytics` (
`id`  int(11) NOT NULL AUTO_INCREMENT ,
`send_date`  date NULL ,
`merchant_id`  int(11) NULL ,
`customer_count`  int(10) NULL ,
PRIMARY KEY (`id`),
FOREIGN KEY (`merchant_id`) REFERENCES `merchant` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
)
;