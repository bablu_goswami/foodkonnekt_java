ALTER TABLE `payment_gateway`
ADD COLUMN `payeezy_app_Id`  varchar(50) NULL AFTER `firstData_merchant_id`,
ADD COLUMN `payeezy_app_secretKey`  varchar(100) NULL AFTER `payeezy_app_Id`,
ADD COLUMN `payeezy_merchnat_token`  varchar(100) NULL AFTER `payeezy_app_secretKey`;

ALTER TABLE `order_payment_detail`
ADD COLUMN `post_payezzey_transctionId`  varchar(100) NULL AFTER `refunded_authCode`;