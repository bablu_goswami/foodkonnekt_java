ALTER TABLE `merchant`
ADD COLUMN `isFBAppPublished`  tinyint(2) NULL DEFAULT 0 AFTER `updated_date`,
ADD COLUMN `fBTabIds`  varchar(200) NULL AFTER `isFBAppPublished`;

