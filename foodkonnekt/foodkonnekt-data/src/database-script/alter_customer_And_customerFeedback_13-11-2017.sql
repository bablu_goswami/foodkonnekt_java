ALTER TABLE `customer`
ADD COLUMN `customerUid`  varchar(120) NULL AFTER `vendor_id`;

ALTER TABLE `customer_feedback`
ADD COLUMN `merchant_id`  int(11) NULL AFTER `create_date`;

ALTER TABLE `customer_feedback` ADD FOREIGN KEY (`merchant_id`) REFERENCES `merchant` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;