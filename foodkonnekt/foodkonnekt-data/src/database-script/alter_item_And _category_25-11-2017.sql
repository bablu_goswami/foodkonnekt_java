ALTER TABLE `item`
ADD COLUMN `itemImage`  longtext NULL AFTER `allow_item_timing`;

ALTER TABLE `category`
ADD COLUMN `categoryImage`  longtext NULL AFTER `allow_category_timing`;