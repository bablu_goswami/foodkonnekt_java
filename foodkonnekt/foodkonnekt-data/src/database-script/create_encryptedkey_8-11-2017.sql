CREATE TABLE `encryptedkey` (
`id`  int(11) NOT NULL AUTO_INCREMENT ,
`key`  varchar(60) NULL ,
`cardInfoId`  int(11) NULL ,
PRIMARY KEY (`id`),
FOREIGN KEY (`cardInfoId`) REFERENCES `cardinfo` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
)
;

ALTER TABLE `encryptedkey`
CHANGE COLUMN `cardInfoId` `merchantId`  int(11) NULL DEFAULT NULL AFTER `key`;

ALTER TABLE `encryptedkey` DROP FOREIGN KEY `encryptedkey_ibfk_1`;

ALTER TABLE `encryptedkey` ADD CONSTRAINT `encryptedkey_ibfk_1` FOREIGN KEY (`merchantId`) REFERENCES `merchant` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE `encryptedkey`
CHANGE COLUMN `key` `encykey`  varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `id`;