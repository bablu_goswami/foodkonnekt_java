ALTER TABLE `merchant`
ADD COLUMN `active_tip_for_dilevery`  tinyint(1) NULL AFTER `email_id`,
ADD COLUMN `tip_for_dilevery`  int(10) NULL AFTER `active_tip_for_dilevery`,
ADD COLUMN `active_tip_for_pickup`  tinyint(1) NULL AFTER `tip_for_dilevery`,
ADD COLUMN `tip_for_pickup`  int(10) NULL AFTER `active_tip_for_pickup`;

ALTER TABLE `merchant`
MODIFY COLUMN `active_tip_for_dilevery`  tinyint(1) NULL DEFAULT 0 AFTER `email_id`,
MODIFY COLUMN `tip_for_dilevery`  int(10) NULL DEFAULT 0 AFTER `active_tip_for_dilevery`,
MODIFY COLUMN `active_tip_for_pickup`  tinyint(1) NULL DEFAULT 0 AFTER `tip_for_dilevery`,
MODIFY COLUMN `tip_for_pickup`  int(10) NULL DEFAULT 0 AFTER `active_tip_for_pickup`;

update merchant set active_tip_for_dilevery=0 ,tip_for_dilevery=0,active_tip_for_pickup=0,tip_for_pickup=0;




