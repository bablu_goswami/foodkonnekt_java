package com.foodkonnekt.service;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

import com.foodkonnekt.model.Category;
import com.foodkonnekt.model.Customer;
import com.foodkonnekt.model.Item;
import com.foodkonnekt.model.Merchant;
import com.foodkonnekt.model.ModifierGroup;
import com.foodkonnekt.model.OrderR;
import com.foodkonnekt.model.PizzaCrust;
import com.foodkonnekt.model.PizzaSize;
import com.foodkonnekt.model.PizzaTemplate;
import com.foodkonnekt.model.PizzaTopping;
import com.foodkonnekt.model.TaxRates;
import com.foodkonnekt.serviceImpl.ExcelToJsonConverterConfig;

public interface ImportExcelService {
	
	public String saveInventoryByExcel(ExcelToJsonConverterConfig config,Merchant merchant,HttpSession session)throws InvalidFormatException, IOException;

    public String saveInvetoryDataOfExcelSheet(ArrayList<TaxRates> taxRates, ArrayList<ModifierGroup> modifierGroups,
                    ArrayList<Item> items, List<Category> categories, List<PizzaSize> pizzaSizes,List<PizzaTemplate> pizaaTemplates,List<PizzaCrust>pizzaCrusts,
                    List<PizzaTopping> pizzaToppings);
    public String saveOfflineCustomerByExcel(ExcelToJsonConverterConfig config,Merchant merchant,HttpSession session)throws InvalidFormatException, IOException, ParseException;

    public void kritiqFeedbackByGranbury(Customer customer,Merchant merchant,OrderR orderR);
}

