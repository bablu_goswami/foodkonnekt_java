package com.foodkonnekt.service;

import java.util.List;

import com.foodkonnekt.model.PrintJob;

public interface PrintJobService {
	
	public PrintJob saveOrUpdate(PrintJob printJob);
	
	public PrintJob findByOrderId(String orderId);
	
	public List<PrintJob> findByMerchantId(Integer merchantId);

}
