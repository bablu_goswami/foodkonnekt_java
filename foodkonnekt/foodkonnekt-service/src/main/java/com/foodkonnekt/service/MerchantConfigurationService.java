package com.foodkonnekt.service;

import com.foodkonnekt.model.MerchantConfiguration;

public interface MerchantConfigurationService {


	

	MerchantConfiguration saveMerchantConfiguration(Integer merchantId, Boolean enableNotification,Boolean autoAccept,Boolean autoAcceptOrder);
	
	MerchantConfiguration findMerchantConfigurationBymerchantId(Integer merchantId);
	

	
	

}
