package com.foodkonnekt.service;

import java.io.IOException;
import java.util.List;

import com.foodkonnekt.model.Address;
import com.foodkonnekt.model.CardInfo;
import com.foodkonnekt.model.ConvenienceFee;
import com.foodkonnekt.model.LocationDto;
import com.foodkonnekt.model.Merchant;
import com.foodkonnekt.model.MerchantLogin;
import com.foodkonnekt.model.OpeningClosingDay;
import com.foodkonnekt.model.OpeningClosingTime;
import com.foodkonnekt.model.OrderPaymentDetail;
import com.foodkonnekt.model.PaymentGateWay;
import com.foodkonnekt.model.PaymentGatewayName;
import com.foodkonnekt.model.PaymentMode;
import com.foodkonnekt.model.PickUpTime;
import com.foodkonnekt.model.SocialMediaLinks;
import com.foodkonnekt.model.TaxRates;
import com.foodkonnekt.model.Vendor;
import com.foodkonnekt.model.VirtualFund;

public interface MerchantService {

    /**
     * Find by merchantPosId
     * 
     * @param merchantId
     * @return Merchant instance
     * 
     * 
     */
	
	
	
	public List<TaxRates> findAllTaxRatesByMerchantId(Integer merchantId);
	public SocialMediaLinks saveSocialMediaLinks(SocialMediaLinks socialMediaLinks);
	
	public SocialMediaLinks getSocialMediaLinksByMerchantId(Integer merchantId);
    public Merchant getMerchantByMerchantPsoId(String posMerchantId);
    
    public List<Merchant> findAllMerchants();
    
    public List<Merchant> findAllMerchants(String startDate, String endDate,String view);
    
    public List<Merchant> findAllMerchantsByVendorId(Integer vendorId );

    /**
     * Find address by merchantId
     * 
     * @param merchantId
     * @return
     */
    public List<Address> findAddressByMerchantId(Integer merchantId);

    /**
     * Add merchant logo
     * 
     * @param merchant
     * @return
     */
    public Merchant addMerchantLogo(Merchant merchant);
    
    public MerchantLogin  findByEmailAndPassword(String emailId, String password);
    
    public boolean  findByEmailId(String emailId);
    public MerchantLogin findByMerchantEmailId(String emailId);

    /**
     * Find merchant logo by merchant Id
     * 
     * @param merchantId
     * @return
     */
    public Merchant findByMerchantId(Integer merchantId);

    /**
     * Find opening closing hour by merchantId
     * 
     * @param merchantId
     * @return List<OpeningClosingTime>
     */
    public List<OpeningClosingTime> findOpeningClosingHourByMerchantId(Integer merchantId);

    /**
     * Find by merchantId
     * 
     * @param merchantId
     * @return vendorId
     */
    public Integer findVendorIdByMerchantId(Integer merchantId);

    /**
     * Find by merchantId
     * 
     * @param merchantId
     * @return Merchant instance
     */
    public Merchant findById(int merchantId);
    
    public List<Merchant> findByFBTabIds(String pageId);

    /**
     * Find by merchant posId
     * 
     * @param merchantId
     * @return Merchant instance
     */
    public Merchant findbyPosId(String merchantId);
    
    public Merchant findbyStoreId(String storeId);

    /**
     * Find by merchantId
     * 
     * @param intMerchantId
     * @return List<OpeningClosingDay>
     */
    public List<OpeningClosingDay> findOpeningClosingDayByMerchantId(Integer merchantId);

    /**
     * Find by merchantId
     * 
     * @param merchantId
     * @return List<Merchant>
     */
    public List<Merchant> findMerchantById(Integer merchantId);

    /**
     * Update merchant logo
     * 
     * @param merchant
     */
    public Merchant save(Merchant merchant);
    
    public MerchantLogin saveMerchantLogin(MerchantLogin merchantLogin);

    /**
     * Save pickup time
     * 
     * @param pickUpTime
     */
    public void savePickupTime(PickUpTime pickUpTime);
    
    public PickUpTime findPickupTime(Integer merchantId);

    /**
     * Save convenience fee
     * 
     * @param convenienceFee
     */
    public void saveConvenienceFee(ConvenienceFee convenienceFee, Merchant merchant);
    
    public String  getMerchantSubscription(Integer merchantId, String status);

	public String generateLinkActiveCustomerFeedback(Integer id);
	
	public String addDefaultTaxByMerchantId(Integer merchantId);
	
	public Vendor findVendorById(Integer intVendorId);
	
	public void updateMerchantUid(Merchant result, String locationUid);
	
	public Merchant updateMerchantDeviceDetail(Merchant result, String modileDeviceId,String modieImeiCode);
	public List<Merchant> findAllMerchantsByVendorIdAndDateRange(Integer id, String startDate, String endDate);
	public Merchant findByMerchantUid(String merchantUid);
	public Merchant findByMerchantUidAndMerchantPosId(String merchantUid,String posMerchantId);
	public String createMerchantViaClientModuleLocation(LocationDto location);
	
	public Boolean checkAllowMultiPay(Merchant merchantCheck, String posMerchantId, String accessToken, Boolean allowMultiPay) throws IOException;
	
	public Merchant saveMerchantLogin(String merchantDetails,String merchantUid,String merchantId);
	
	public void savePaymentGateWay(PaymentGateWay paymentGateWay);
	public List<PaymentGatewayName> findAllPaymentGatewayName();
	public PaymentGateWay findbyMerchantId(Integer id,Boolean isDeleted);
	public void saveOrderPaymentDetail(OrderPaymentDetail orderPaymentDetail);
	public void savePaymentMode(PaymentMode mode);
	public PaymentMode findByMerchantIdAndLabel(Integer id, String label);
	public CardInfo saveCustomerCards(CardInfo cardInfo);
	public PaymentGateWay findPaymentGatewayDetails(Integer id, Boolean isDeleted, Boolean isActive);
	public OrderPaymentDetail findPaymentDetailsByOrderId(Integer orderId);
	public CardInfo findDetailsByCardId(Integer cardInfoId);
	
	public Boolean updateMerchantDetail(Merchant merchant);
	
	public List<Merchant> getWeeklyReportOfMerchant(String startDate,String endDate);
	
	public Merchant getWeeklyReportOfMerchant(String startDate,String endDate, int merchantId);
	
	public Merchant findByPosMerchantId(String merchantPosId);
	
	public String updateVirtualFund(VirtualFund virtualFund ,Merchant merchant);
	
	public String showVirtualFundByMerchantId(Merchant merchant);
	
	public String saveVirtualFund(VirtualFund virtualFund, Merchant merchant);
	
	public List<Merchant> getMonthlyFundRaiserReport(String startDate, String endDate);
	Merchant merhcnatUninstallProcess(Merchant merchant);

	String showAllApplicationstatus(Merchant merchant);
}
