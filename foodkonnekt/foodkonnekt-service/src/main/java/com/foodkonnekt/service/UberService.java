package com.foodkonnekt.service;

import java.util.List;

import com.foodkonnekt.model.Merchant;
import com.foodkonnekt.model.UberStore;

public interface UberService {

	List<UberStore> findStorebyMerchnatId(Merchant merchant);
	
	String saveUberStore(UberStore uberStore, Merchant merchant);
	
	String updateStoreDetails(UberStore uberStore);
	
	String uploadMenuOnUber(Integer merchantId, String storeId);

	String getOrderDetails(String orderNotification);

	String updateOrderDetails(String orderDetails);
	
	String updateUberInventory(String uberStoreId, Merchant merchant);
	
	//public String updateUberBusinessHours(String uberStoreId,Merchant merchant);
	
}
