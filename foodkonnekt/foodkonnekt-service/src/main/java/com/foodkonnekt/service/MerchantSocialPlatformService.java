package com.foodkonnekt.service;

import com.foodkonnekt.model.MerchantSocialPlatform;


public interface MerchantSocialPlatformService {
	
	public MerchantSocialPlatform findByMerchantId(Integer merchantId);
	
	void saveAndFlush(MerchantSocialPlatform merchantSocialPlatform);
	
	
}
