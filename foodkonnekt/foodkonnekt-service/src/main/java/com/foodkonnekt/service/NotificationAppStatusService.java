package com.foodkonnekt.service;

import java.util.List;

import com.foodkonnekt.model.NotificationApp;
import com.foodkonnekt.model.NotificationAppStatus;

public interface NotificationAppStatusService {

	public List<NotificationAppStatus> findByMerchantid(Integer merchantId);

	public NotificationAppStatus findByMerchantIdandAppId(Integer id, NotificationApp appId);

}