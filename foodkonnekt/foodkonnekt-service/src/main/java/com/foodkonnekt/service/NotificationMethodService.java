package com.foodkonnekt.service;

import java.util.List;

import com.foodkonnekt.model.NotificationMethod;
import com.foodkonnekt.model.NotificationMethodType;

public interface NotificationMethodService {

	List<NotificationMethodType> getNotificationMethodTypes();
	
	List<NotificationMethod> getNotificationMethods(Integer merchantId);
	
	NotificationMethod saveNotificationMethod(NotificationMethod notificationMethod);
	
	NotificationMethod updateNotificationById(Integer id,Boolean status);
	
	NotificationMethod findByMerchantIdAndContact(Integer merchantId, String contact);
	
	void deleteNotificationMethod(Integer id);
	
	NotificationMethod findById(Integer id);
	
	List<NotificationMethod> notifications(Integer merchantId);
	
	List<NotificationMethod> findByMerchantIdAndMethodTypeId(Integer merchantId,Integer methodTypeId);
}
