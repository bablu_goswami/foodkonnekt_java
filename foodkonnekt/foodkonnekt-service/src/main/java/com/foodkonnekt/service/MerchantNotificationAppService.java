package com.foodkonnekt.service;

import java.util.List;

import com.foodkonnekt.model.MerchantNotificationApp;

public interface MerchantNotificationAppService {

	public List<MerchantNotificationApp> findAll();

	public MerchantNotificationApp FindBymerchantIdAndAppId(Integer merchantId,Integer appd);
	
	public MerchantNotificationApp save(MerchantNotificationApp merchantNotificationApp);
}
