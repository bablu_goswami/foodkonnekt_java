package com.foodkonnekt.service;

import java.util.List;

import com.foodkonnekt.model.Merchant;
import com.foodkonnekt.model.ModifierGroup;
import com.foodkonnekt.model.PizzaCrust;
import com.foodkonnekt.model.PizzaCrustSizes;
import com.foodkonnekt.model.PizzaSize;
import com.foodkonnekt.model.PizzaTemplate;
import com.foodkonnekt.model.PizzaTemplateSize;
import com.foodkonnekt.model.PizzaTemplateTopping;
import com.foodkonnekt.model.PizzaTopping;
import com.foodkonnekt.model.PizzaToppingSize;
import com.foodkonnekt.model.TaxRates;


public interface PizzaService {

	List<PizzaSize> findAllSizes(Integer id);

	String findPizzaTamplateByMerchantId(Integer id, Integer pageDisplayLength, Integer pageNumber,
			String searchParameter);

	String findPizzaToppingByMerchantId(Integer id, Integer pageDisplayLength, Integer pageNumber,
			String searchParameter);

	String filterTamplateBypizzaSizeId(Integer merchantId, int pizzaSizeId);

	String filterToppingBypizzaSizeId(Integer merchantId, int pizzaSizeId);

	String searchTemplateByTxt(Integer id, String searchTxt);

	String searchToppingsByTxt(Integer id, String searchTxt);

	void updateTamplateStatusById(PizzaTemplate template);

	void updateToppingStatusById(PizzaTopping topping);

	PizzaTemplate findByTemplateId(int templateId);

	PizzaTemplate updatePizzaTemplateById(PizzaTemplate pizzaTemplate);

	
	List<PizzaTemplate>  findPizzaTemplateByMerchantId(Integer merchantId);
	
	
	List<PizzaTemplateSize>  findPizzaTemplateSizeByTemplateId(Integer templateId);
	List<PizzaTemplateSize>  findPizzaTemplateSizeByTemplateId(List<Integer> templateId);

	List<PizzaTopping> findByToppingByMerchantId(Integer merchantId);


	List<PizzaToppingSize>  findByToppingIdAndSizeId(List<Integer> id, Integer sizeId);

	PizzaTemplate findByTemplateIdAndMerchantId(int templateId, int merchantId, int sizeId);
	
	List<PizzaTemplate> findByMerchantId(int merchantId);

    List<PizzaCrustSizes> findByCrustIdAndSizeId(Integer id, Integer sizeId);

    PizzaCrustSizes findByCrustId(Integer id, Integer sizeId);
    
    List<PizzaToppingSize> findByPizzaSizeId(Integer sizeId);
    
    List<PizzaTemplateTopping> findAllPizzaToppings(Integer templateId, Integer sizeId , Integer merchantId);
    
    List<PizzaTopping> findPizzaToppingMerchantId(Integer merchantId);
    
    List<PizzaSize> findPizzaSizeMerchantId(Integer merchantId);
    
    List<PizzaCrust> findPizzaCrustMerchantId(Integer merchantId);
    
   /* public String findPizzaByMerchantById(Integer merchantId, Integer pageDisplayLength, Integer pageNumber,
            String searchParameter);
*/
  PizzaTopping findByToppingId(int toppingId);
    
    PizzaTopping updatePizzaToppingById(PizzaTopping pizzaTopping);

List<PizzaTemplateSize> findByPizzaTemplateSizeById(Integer templateId);

void savePizzaCrust(PizzaCrust pizzaCrust, Merchant merchant);

public String savePizzaSize(PizzaSize pizzaSize,Merchant merchant);

public String saveNewTax(TaxRates taxRates,Merchant merchant);

public String filterPizzaTemplateIdAndPizzaCrustId(int merchant_id);

public PizzaCrust findByCrustId(Integer crustId);

PizzaCrust updatePizzaCrustById(PizzaCrust pizzaCrust,Integer merchant_id);

public void updateCrustStatusById(PizzaCrust crust);

public void updatePizzaCrustMapping(PizzaCrust pizzaCrust,Integer merchantId);

String showPizzaSizeData(Integer merchantId);

public void updateSizeStatusById(PizzaSize pizzaSize);

public PizzaSize findSizeByPizzaSizeId(int sizeId);

public void updatePizzaSizeValue(PizzaSize pizzaSize,Integer merchantId);
}
