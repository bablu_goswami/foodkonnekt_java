package com.foodkonnekt.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.json.JSONObject;

import com.foodkonnekt.clover.vo.AllOrderVo;
import com.foodkonnekt.model.Address;
import com.foodkonnekt.model.CardInfo;
import com.foodkonnekt.model.CategoryDto;
import com.foodkonnekt.model.Customer;
import com.foodkonnekt.model.ItemDto;
import com.foodkonnekt.model.Merchant;
import com.foodkonnekt.model.ModifierGroupDto;
import com.foodkonnekt.model.OrderR;
import com.foodkonnekt.model.OrderType;
import com.foodkonnekt.model.PaymentGateWay;

public interface OrderService {

    /**
     * Find by customerId and merchantId
     * 
     * @param id
     * @param parseInt
     * @return
     */
	
	public String getFutureOrders(String merchantId);
	 public String searchOrderByText(Integer merchantId, String searchTxt,Integer pageDisplayLength, Integer pageNumber);

    public OrderType findByMerchantIdAndLabel(Integer merchantId, String orderType);

    public List<OrderR> getAllOrder(Integer customerId, Integer merchantId);

    /**
     * Find line item categories wise
     * 
     * @param categories
     * @return
     */
    /*public List<CategoryDto> findMenuItems(List<CategoryDto> categories);*/

    public List<ItemDto> findCategoryItems(Integer categoryId,Merchant merchant,Integer offSet);
    
    public List<CategoryDto> findCategoriesByMerchantId(Merchant merchant);
    public List<CategoryDto> findCategoriesByMerchantIdWithLimit(Integer merchantId,Integer page);

    public List<ModifierGroupDto> getModifierGroup(Integer itemId, Integer allowModifierLimit,Merchant merchant);

	/**
	 * Save place order details into database
	 * 
	 * @param jObject
	 * @param finalJson
	 * @param customer
	 * @param merchant
	 * @param discount
	 * @param convenienceFee
	 * @param deliveryItemPrice
	 * @param tax
	 * @param auxTax
	 * @param futureTime
	 * @param futurdate
	 * @param fututeOrderType
	 * @return
	 */
	public String saveOrder(JSONObject jObject, String finalJson, Customer customer, Merchant merchant, Double discount,
			String convenienceFee, String deliveryItemPrice, String avgDeliveryTime, String orderType, String auxTax,
			String tax);

	/**
	 * Update order status
	 * 
	 * @param orderPosId
	 * @param result
	 * @param customerId
	 * @param paymentType
	 * @param tax
	 * @param name
	 * @param email
	 * @param merchantLogo
	 * @param merchantName
	 * @param orderType
	 * @param futureTime
	 * @param futureDate
	 * @param futureOrderType
	 * @param ccType
	 * @param saveCard
	 * @param auxTax
	 * @param string 
	 */
	public boolean updateOrderStatus(String orderPosId, String result, Integer customerId, String paymentType,
			String subTotal, String tax, String name, String email, String merchantName, String merchantLogo,
			String orderType, double tipAmount, String orderPrice, String futureOrderType, String futureDate,
			String futureTime, Merchant merchant, String listOfALLDiscounts, String ccType, Boolean saveCard,
			String auxTax);

	/**
	 * Set order status(Accept/decline)
	 * 
	 * @param orderId
	 * @param type
	 */
	public boolean setOrderStatus(String orderId, String type, String reason);

	//public boolean setOrderStatus(Integer orderId, String type, String reason);

	public String setOrderStatus(Integer orderId, String type, String reason, String changedOrderAvgTime);

	/**
	 * Find all orders by merchantId
	 * 
	 * @param merchantId
	 * @return List<OrderR>
	 */
	public List<OrderR> findAllOrdersByMerchantId(int merchantId);

	/**
	 * Find by orderType
	 * 
	 * @param orderType
	 * @return List<OrderR>
	 */
	public List<OrderR> findOrdersByOrderType(String orderType);

	/**
	 * Find by OrderStatus
	 * 
	 * @param orderStatus
	 * @return List<OrderR>
	 */
	public List<OrderR> findOrdersByStatus(String orderStatus);

	/**
	 * Find by orderStatus and OrderType
	 * 
	 * @param orderStatus
	 * @param orderType
	 * @return List<OrderR>
	 */
	public List<OrderR> findOrdersByStatusAndOrderType(String orderStatus, String orderType);

	/**
	 * Find by OrderStatus,OrderType and by Date
	 * 
	 * @param orderStatus
	 * @param orderType
	 * @param startDate
	 * @param endDate
	 * @return List<OrderR>
	 */
	public List<OrderR> findOrdersByStatusAndOrderTypeAndDateRange(String orderStatus, String orderType,
			String startDate, String endDate);

	/**
	 * Find by start and end date
	 * 
	 * @param startDate
	 * @param endDate
	 * @return List<OrderR>
	 */
	public List<OrderR> findByOrderDate(String startDate, String endDate);

	/**
	 * Find by orderType and orderDate
	 * 
	 * @param orderType
	 * @param startDate
	 * @param endDate
	 * @return List<OrderR>
	 */
	public List<OrderR> findByOrderTypeAndOrderDate(String orderType, String startDate, String endDate);

	/**
	 * Find by orderStatusAndOrderDate
	 * 
	 * @param orderStatus
	 * @param startDate
	 * @param endDate
	 * @return List<OrderR>
	 */
	public List<OrderR> findByOrderStatusAndOrderDate(String orderStatus, String startDate, String endDate);

	/**
	 * Find customer orders
	 * 
	 * @param customerId
	 * @return List<OrderR>
	 */
	public List<OrderR> findOrderByCustomerId(int customerId);
	
	public List<OrderR> findOrderByCustomerIdd(int customerId);


	/**
	 * Find number of delivery order
	 * 
	 * @param merchantId
	 * 
	 * @return Integer
	 */
	public Integer findNoOfDeliveryOrder(Integer merchantId);

	/**
	 * Find number of pick up order
	 * 
	 * @return Integer
	 */
	public Integer findNoOfPickUpOrder(Integer merchantId);

	/**
	 * Find average order value
	 * 
	 * @param merchantId
	 * @return double
	 */
	public double findAverageOrderValue(Integer merchantId);

	/**
	 * Find total Order value
	 * 
	 * @param merchantId
	 * @return double
	 */
	public int findtotalOrderValue(Integer merchantId);

	/**
	 * Find order frequency
	 * 
	 * @param merchantId
	 * @param vendorId
	 * @return double
	 */
	public double findOrderFrequency(Integer merchantId, Integer vendorId);

	/**
	 * find customer order average
	 * 
	 * @param merchantId
	 * @param vendorId
	 * @return double
	 */
	public double findCustomerOrderAverage(Integer merchantId, Integer vendorId);

	/**
	 * Find total number of customer
	 * 
	 * @param merchantId
	 * @param vendorId
	 * @return Integer
	 */
	public Integer findTotalCustomer(Integer merchantId, Integer vendorId);

	/**
	 * Find trending item
	 * 
	 * @param merchantId
	 * @return String
	 */
	public String findTrendingItem(Integer merchantId);

	/**
	 * Find average number of item per order
	 * 
	 * @param merchantId
	 * @param vendorId
	 * @return double
	 */
	public double findAverageNumberOfItemPerOrder(Integer merchantId, Integer vendorId);

	/**
	 * Find payment mode
	 * 
	 * @param id
	 * @return List<String>
	 */
	public List<String> findPaymentMode(Integer id);

	public void deleteAnOrder(Integer customerId, String orderPOSId);

	public Double findConvenienceFeeAfterTax(String convenienceFee, Integer merchantId);

	public List<Address> findAddessByCustomerId(Integer customerId);

	public String findMerchantTaxs(Integer merchantId);

	public String findConvenienceFeeAfterMultiTax(String convenienceFee, Integer merchantId);

	public List<OrderR> findAllOrdersByFulfilledDate(java.util.Date currentDate, Merchant merchant);

	public OrderR findOrderByOrderID(String id);

	public OrderR findOrderByID(Integer id);

	public OrderR findOrderDetailById(Integer id);

	public void saveOrder(OrderR orderR);

	public void sendMailUser(OrderR orderR, String reason, String time);

	/*
	 * public String findAllOrderFromDataTable(Integer merchantId, Integer
	 * pageDisplayLength, Integer pageNumber, String searchParameter);
	 */

	public String findAllOrderFromDataTable(Integer merchantId, Integer pageDisplayLength, Integer pageNumber,
			String searchParameter, String startDate, String endDate);

	public String findOrderDetailsById(Integer orderId);

	public String getAllFutureOrders(String merchantId);

	public String getAllFutureOrdersByLocationUid(String locationUid);

	public String searchOrderByTextAndDate(Integer id, String searchTxt, Integer pageDisplayLength, Integer pageNumber,
			String startDate, String endDate);

	public List<AllOrderVo> generateExcelForOrder(Integer id, String searchTxt, String startDate, String endDate);

	public CardInfo getVaultedDetail(Integer vaultedId, String string);

	public Boolean deleteCardDetail(Integer customerId, Integer cardId);

	public String getEncyKey(String token, Integer merchantId);

	public List<OrderR> getRepeateOrder(Integer customerId, Merchant merchant, Integer repeateOrderId);

	public List<OrderR> getAllRepeateOrder(Integer customerId, Merchant merchant);

	/* public String searchOrderByText(Integer merchantId, String searchTxt); */

	public List<Double> getFoodTronixTax(Integer merchantId, String orderType);

	public OrderR findOrderByOrderPosIdAndCustomerId(String orderPosId, Integer id);

	public boolean updateDeliveryTime(String orderId, String deliveryTime) throws Exception;

	public List<OrderR> getOrderByCustomerId(Integer id, java.util.Date startDate, java.util.Date endDate);

	public List<OrderR> findCustomerNonTranscation(List<Integer> customerIdList, Integer id);

/*	public List<OrderR> findCustomerTranscation(String customerId, Integer id);
*/
	public List<OrderR> findOrderDetailByCustomerId(int customerId);

	public List<OrderR> getOrderByCustomerid(Integer id, String startDate, String endDate);

	public List<OrderR> getOrderByCustomerIdd(Integer id, String startDate, String endDate);

	public List<OrderR> findCustomerTranscation(List<Integer> customerIdList, Integer id);
	public String getNotificationJSON(int orderId, String deviceId, String locationUid, String merchantPosId);
	public String setOrderStatus(Integer orderId, String type, String reason, String changedOrderAvgTime, Integer merchantId);
	
	List<CategoryDto> getPizzaCategory(Merchant merchant);

	public String savePizzaOrder(JSONObject jObject, String finalJson,
			Customer customer, Merchant merchant, Double discount,
			String convenienceFee, String deliveryItemPrice,
			String avgDeliveryTime, String orderType, String auxTax, String tax,Integer addressId, Integer zoneId,String fundCode);

	List<CategoryDto> findCategoriesByMerchantIdV2(Merchant merchant);
	
	public String chargeRefundByOrderId(Integer orderId);
	
	public List<OrderR> findFutureOrderByCurrentDate();
	
	public Integer findItemCountByCategoryId(Integer categoryId, Merchant merchant);
	
	public List<OrderR> findByMerchantIdAndStartDateAndEndDate(Integer merchantId,Date fromDate, Date toDate);
	
	public void deleteOrder(String orderPOSId);
	
	OrderR findById(Integer id);
	
	Address findByAddressId(Integer addressId);
	public Boolean findByMerchantIdandOId(Merchant merchant, OrderR order);
	
	String mostPopularMenuItemForFoodkonnekt(String merchantUid,String fromDate, String toDate) throws Exception;
	
	Map<String, Object> getSalesReportOfFoodkonnekt(String merchantUid,String fromDate, String toDate);
	
	public Map<String, Object> getOrderNameAndCount(String merchantUid,String fromDate, String toDate);
	
	Map<String, Object> getTopMenuReport(String locationUid, String fromDate,String toDate);
	
	public Double avgOrderValueByMerchantUId(String merchantUid, String fromDate,String toDate) throws Exception;
	
	public Map<String, Object> getTop3ItemReport(String locationUId, String fromDate, String toDate);
	
	public List<Integer> findMerchantsTodayDate(String currentDate);

     public OrderR findByOrderIdAndMerchantId(Integer id, Integer merchantId);

	public String getOrderDetailsByOrderId(Integer orderId , String merchantUid);
	
	public PaymentGateWay getPaymentGatewayDetailsByMerchantId(Integer merchantId);
	
	List<Customer> getActiveFoodkonnektCustomerByLocationUidAnddate(String locationUid, String startDate,
			String endDate);
	
	public void sendNotification(String notificationJson, String merchantId, String accessToken);
	
	public String getOrdersByPosIdAndStatus(String merchantPosId,Integer isDefault,String timeZoneCode);
	
	void updatePrintJobStatus(Integer orderId, boolean printStatus);

}
