package com.foodkonnekt.service;

import java.util.Map;

public interface OrderDiscountService {
	
	public Map<String,Object> getOfferWithHighestRevenue(String merchantUId, String fromDate, String toDate);
}
