package com.foodkonnekt.service;

import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.foodkonnekt.clover.vo.KritiqCustomerVO;
import com.foodkonnekt.clover.vo.KritiqMerchantVO;
import com.foodkonnekt.model.Customer;
import com.foodkonnekt.model.CustomerFeedback;
import com.foodkonnekt.model.CustomerFeedbackAnswer;
import com.foodkonnekt.model.FeedbackQuestionCategory;
import com.foodkonnekt.model.Merchant;
import com.foodkonnekt.model.OrderR;
import com.foodkonnekt.model.Vendor;
import com.itextpdf.layout.element.Table;

public interface KritiqService {
	
	public String checkMerchantExpiration(Merchant merchant);
	
	public boolean verifyOrderFromClover( String orderId, String merchantId);

	public OrderR getAllOrderInfo(Integer customerID, Integer orderId);

	//public List<FeedbackQuestionCategory> getFeedbackQuestionCategorys(String orderType);

	public OrderR validateCustomerAndOrder(String customerId, String orderId);

	public String getOrderDetail(Integer orderId);
	
	public void saveCustomerFeedback(CustomerFeedback customerFeedback);

	public List<CustomerFeedback> findByCustomerIdAndOrderId(String customerId, String orderid);
	public List<CustomerFeedback> findByOrderPosID(String posID);
	
	public List<CustomerFeedback> findByOrderPosIDAndMerchantId(String posID,Integer merchantId);

	public OrderR findByOrderId(String orderid);

	public List<OrderR> multipleOrdersListOfCustomer(Date startDate, Date endDate, int custId);

	public List<FeedbackQuestionCategory> getFeedbackQuestionCategorys(String orderType, boolean pickup_flag,boolean delivery_flag, boolean dineIn_flag);

	public List<FeedbackQuestionCategory> getFeedbackQuestionCategorys();

//	public List<FeedbackQuestionCategory> getFeedbackQuestionCategorysByPosId(Integer id);
	
	public Customer saveWalkInCustomerDetail(Customer customer);

	public OrderR saveOrderDetails(OrderR orderR);

	public Vendor validateVendor(String vendorId);

	public List<Merchant> getMerchantsByVendorId(Integer id);

	public Merchant getMerchantDetailsByPosMerchantId(String merchantPosId);

	public Merchant validateMerchant(String merchantId);

	public Merchant getMerchantDetailsByMerchantId(Integer id);

	public OrderR getOrderDetailsByOrderPosId(String o);

	public List<CustomerFeedback> findByOrderId(Integer id);
	
	public CustomerFeedback getCustomerFeedback(Integer id);

	public Vendor getVendorDetailsByVendorId(Integer id);

	public List<CustomerFeedbackAnswer> getCustomerFeedbackResult(Integer custFeedbackId);

	public FeedbackQuestionCategory getFeedbackQuestionCategory(Integer id);

	public Map<String, Integer> getPositiveAndNegetiveFeedbackCount(String merchantUid);

	public Map<String, Integer> getPositiveAndNegetiveFeedbackCountByDateRange(String merchantUid, String startDate,
			String endDate);
   
	public List<KritiqMerchantVO> getKritiqMerchantDetail(String startDate,String endDate,Boolean isFoodkonnektAdmin,Integer vendorId);

	public List<KritiqCustomerVO> getKritiqCustomerDetail(Integer merchantId,String startDate,String endDate);

	public OrderR getOrderDetailsByOrderPosIdAndMerchantId(String orderId, Integer merchantId);

	public Table getOrderDetailForFax(Integer orderId);
	
	public void saveCustomerFeedbackNew(CustomerFeedback customerFeedback);

	public  List<Customer> getAllkritiqCustomerByMerchantUid(String merchantUid);

	public List<Customer> getAllkritiqCustomerByMerchantUidAndDateRange(String merchantUid, String startDate,
			String endDate);

	public List<Customer> getActivekritiqCustomerByMerchantUid(String merchantUid);

	public List<Customer> getActivekritiqCustomerByMerchantUidAndDateRange(String merchantUid, String startDate,
			String endDate);

    List<Map<String, Object>> getKritiqReviewData(List<Integer> customerFeedbackIds);
    
    List<String> getKritiqIdsReviewDataByMerchantId(Integer merchantId,String startDate,String endDate);
    
    KritiqCustomerVO getKritiqReviewDataByFeedbackId(Integer customerFeedBackId) throws ParseException;
    
    void saveResponseFrom4Q(Map<String, Object> feedBackResponse);
    
    void updateResponseInCustomerFeedBack(String feedBackId, String response);
    
    List<Map<String, Object>> getKrtiqLowRating(String startDate, String endDate);
}
