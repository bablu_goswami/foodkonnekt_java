package com.foodkonnekt.service;

import com.foodkonnekt.model.OrderNotification;

public interface OrderNotificationService {

	
	public OrderNotification findMerchantId(Integer merchantId);
	
	public OrderNotification save(OrderNotification orderNotification);
}
