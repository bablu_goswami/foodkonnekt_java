package com.foodkonnekt.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.foodkonnekt.model.Merchant;
import com.foodkonnekt.model.MerchantSliders;


public interface MerchantSliderService {

	public void saveMerchantSliderValue(Merchant merchant, MultipartFile file);

	List<MerchantSliders> findByMerchantId(Integer merchantId);

	Integer findSliderImageCountByMerchantId(Integer merchantId);

	MerchantSliders findById(Integer sliderId);
	
	public void deleteItemImage(MerchantSliders merchantSliders);

}
