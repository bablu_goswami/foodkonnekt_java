package com.foodkonnekt.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.foodkonnekt.model.Address;
import com.foodkonnekt.model.CardInfo;
import com.foodkonnekt.model.Customer;
import com.foodkonnekt.model.CustomerFeedback;
import com.foodkonnekt.model.Item;
import com.foodkonnekt.model.Merchant;
import com.foodkonnekt.model.OrderItem;
import com.foodkonnekt.model.Vendor;

public interface CustomerService {

	/**
	 * Find by email , password and vendorId
	 * 
	 * @param emailId
	 * @param password
	 * @param vendorId
	 * @return Customer instance
	 */
	Customer findByEmailAndPasswordAndVendorId(String emailId, String password, Integer vendorId, Integer merchantId);

	Customer findByEmailAndPasswordAndMerchantId(String emailId, String password, Integer merchantId);

	Customer findByEmailAndPassword(String emailId, String password);

	Customer findByEmailAndPasswordForAdmin(String emailId, String password);

	/**
	 * Get address by customerId
	 * 
	 * @param id
	 * @return Address instance
	 */
	List<Address> getAddressByCustomerId(Integer id);

	boolean findByEmailIdAndMerchantId(String emailId, Integer merchantId);

	boolean findByEmailIdAndVendorId(String emailId, Integer vendorId);

	/**
	 * Get customer by customerId
	 * 
	 * @param customerId
	 * @return
	 */
	Customer getCustomerProfile(Integer customerId);

	List<Map<String, Object>> checkDuplicatCouponAndRecalculate(Customer customer,
			List<Map<String, Object>> listOfALLDiscounts);

	/**
	 * Update customer profile
	 * 
	 * @param customer
	 * @return
	 */
	Customer updateCustomerProfile(Customer customer);

	/**
	 * Find by email Id
	 * 
	 * @param emailId
	 * @return
	 */
	boolean findByEmail(String emailId);

	boolean findAdminByEmail(String emailId);

	/**
	 * Find by emailId
	 * 
	 * @param emailId
	 * @return
	 */
	boolean findByEmailId(String emailId);

	/**
	 * Find by vendorId
	 * 
	 * @param vendorId
	 * @return List<Customer>
	 */
	List<Customer> findByVendorId(Integer vendorId);

	/**
	 * Set guest customer password
	 * 
	 * @param sessionCustomer
	 * @return Customer
	 */
	Customer setGuestCustomerPassword(Customer sessionCustomer);

	/**
	 * Find by emailId and merchantId
	 * 
	 * @param emailId
	 * @param id
	 * @return boolean
	 */
	boolean findByEmailAndMerchantId(String emailId, Integer id);
	
	boolean findByEmailAndMerchantIdAndVendorId(String emailId, Integer id,Integer vendorId);

	Customer findByEmailAndCustomerId(String emailId, Integer id);

	Customer findCustomerByEmailAndMerchantId(String emailId, Integer id);

	Customer findCustomerByEmailAndMerchantIdForAdmin(String emailId, Integer id, String userId);

	List<Customer> findByEmailIDAndMerchantId(String emailId, Integer merchantId);

	List<Customer> findByEmailIDAndVendorId(String emailId, Integer vendorId);

	String findCustomerInventory(Integer merchantId, Integer pageDisplayLength, Integer pageNumber,
			String searchParameter);

	String searchCustomerByTxt(Integer merchantId, String searchTxt);

	List<Address> findAllCustomerAddress(Integer customerId, Integer merchantId);

	Customer findByCustomerId(Integer id);

	List<CardInfo> getCustomerCardInfo(Integer id);
	
	List<CardInfo> getCustomerCardInfo(Integer id,Integer merchantId);

	List<Customer> findByMerchantId(Integer id);

	/**
	 * Get Customer by merchantUId
	 * 
	 * @param merchantUId
	 * @return List<Customer>
	 */
	List<Customer> findByMerchantUId(String merchantUId);
	public List<Customer> findByMerchantIdAndDateRangeAndSpent(Integer merchantId, Date startDate, Date endDate, Double orderPrice);

	Customer searchCustomerByUUId(String searchCustomerByUUId);

	List<OrderItem> findItemByOrderId(Integer id);

	Item findItemByItemId(Integer id);

	List<Customer> findByPhoneNumber(String phonenumber);
	
	List<Customer> findByDateAndCustomerId(String startDate, String endDate);

	List<CustomerFeedback> getAllFeedbackOfCustomer(String customerUid);
	
	
	//Service  for design V2
		boolean findByEmailAndMerchantIdAndVendorIdV2(String emailId, Integer id,Integer vendorId);
		
		boolean findByEmailAndMerchantIdV2(String emailId, Integer id);
		
	public void save(Customer customer);
	
     public String showUserByMerchantId(Merchant merchant);
     
     public void updateCustomerOnAdmin(Customer customer ,Merchant merchant);
     
     String saveCustomerOnAdmin(Customer customer, Merchant merchant);
     
     boolean findUserByEmail(String emailId);
}
