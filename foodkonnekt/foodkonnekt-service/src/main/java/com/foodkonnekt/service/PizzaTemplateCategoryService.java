package com.foodkonnekt.service;

import java.util.List;

import com.foodkonnekt.model.PizzaTemplateCategory;

public interface PizzaTemplateCategoryService {

	List<PizzaTemplateCategory> findByCategoryId(Integer categoryId);
	
	
}
