package com.foodkonnekt.serviceImpl;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.foodkonnekt.clover.vo.InventoryItemVo;
import com.foodkonnekt.clover.vo.PersonJsonObject;
import com.foodkonnekt.model.CategoryItem;
import com.foodkonnekt.model.Item;
import com.foodkonnekt.model.ItemModifierGroup;
import com.foodkonnekt.model.ItemModifiers;
import com.foodkonnekt.model.ItemTax;
import com.foodkonnekt.model.ItemTiming;
import com.foodkonnekt.model.Merchant;
import com.foodkonnekt.model.ModifierGroup;
import com.foodkonnekt.model.Modifiers;
import com.foodkonnekt.model.PosIntegration;
import com.foodkonnekt.repository.CategoryItemRepository;
import com.foodkonnekt.repository.ItemModifierGroupRepository;
import com.foodkonnekt.repository.ItemModifiersRepository;
import com.foodkonnekt.repository.ItemTaxRepository;
import com.foodkonnekt.repository.ItemTimingRepository;
import com.foodkonnekt.repository.ItemmRepository;
import com.foodkonnekt.repository.ModifierModifierGroupRepository;
import com.foodkonnekt.repository.ModifiersRepository;
import com.foodkonnekt.repository.PosIntegrationRepository;
import com.foodkonnekt.service.ItemService;
import com.foodkonnekt.util.DateUtil;
import com.foodkonnekt.util.IConstant;
import com.foodkonnekt.util.JsonUtil;
import com.foodkonnekt.util.MailSendUtil;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Service
public class ItemServiceImpl implements ItemService {

	private static final Logger LOGGER= LoggerFactory.getLogger(ItemServiceImpl.class);
	
	@Autowired
    private Environment environment;
	
	@Autowired
	private ItemmRepository itemmRepository;

	@Autowired
	private ItemTimingRepository itemTimingRepository;

	@Autowired
	private ItemModifierGroupRepository itemModifierGroupRepository;

	@Autowired
	private ItemModifiersRepository itemModifiersRepository;

	@Autowired
	private CategoryItemRepository categoryItemRepository;

	@Autowired
	private ModifierModifierGroupRepository modifierModifierGroupRepository;

	@Autowired
	private ModifiersRepository modifiersRepository;

	@Autowired
	private PosIntegrationRepository posIntegrationRepository;
	
	@Autowired
	private ItemTaxRepository itemTaxRepository;

	/**
	 * Save items
	 */
	public void saveItem(String itemmJson, Merchant merchant) {
		LOGGER.info("----------------Start :: ItemServiceImpl : saveItem()------------------------");

		List<Item> items = JsonUtil.setItems(itemmJson, merchant);
		LOGGER.info("----------------End :: ItemServiceImpl : saveItem() : saved items------------------------");

		itemmRepository.save(items);
	}

	/**
	 * Find by merchantId from database
	 */
	public List<Item> findByMerchantId(Integer merchantId) {
		LOGGER.info("----------------Start :: ItemServiceImpl : findByMerchantId()------------------------");
		LOGGER.info(": ItemServiceImpl : findByMerchantId() : merchantId :"+merchantId);
		LOGGER.info("----------------End :: ItemServiceImpl : findByMerchantId()------------------------");

		return itemmRepository.findByMerchantId(merchantId);
	}

	/**
	 * Find modifierGroup by item id from database
	 */
	public List<ItemModifierGroup> findByItemId(Integer itemId) {
		LOGGER.info("----------------Start :: ItemServiceImpl : findByItemId()------------------------");
		LOGGER.info(": ItemServiceImpl : findByItemId():itemId:"+itemId);
		LOGGER.info("----------------End :: ItemServiceImpl : findByItemId()------------------------");

		return itemModifierGroupRepository.findByItemId(itemId);
	}

	/**
	 * Find by merchantId
	 */
	public List<Item> findLineItemByMerchantId(int merchantId) {
		LOGGER.info("----------------Start :: ItemServiceImpl : findLineItemByMerchantId()------------------------");
		LOGGER.info(": ItemServiceImpl : findLineItemByMerchantId():merchantId:"+merchantId);

		List<Item> items = itemmRepository.findByMerchantId(merchantId);
		try {
			for (Item item : items) {
				String categoryName = "";
				String modifierGroupName = "";
				LOGGER.info(": ItemServiceImpl : findLineItemByMerchantId():itemId:"+item.getId());

				List<CategoryItem> categoryItems = categoryItemRepository.findByItemId(item.getId());
				for (CategoryItem categoryItem : categoryItems) {
					categoryName = categoryName + "," + categoryItem.getCategory().getName();
				}
				categoryName = categoryName.trim();
				if (!categoryName.isEmpty())
					item.setCategoriesName(categoryName.substring(1, categoryName.length()));
				List<ItemModifierGroup> itemModifierGroups = itemModifierGroupRepository.findByItemId(item.getId());
				for (ItemModifierGroup itemModifierGroup : itemModifierGroups) {
					modifierGroupName = modifierGroupName + "," + itemModifierGroup.getModifierGroup().getName();
				}
				modifierGroupName = modifierGroupName.trim();
				if (!modifierGroupName.isEmpty())
					item.setModifierGroups(modifierGroupName.substring(1, modifierGroupName.length() - 1));

				item.setCategories(null);
				item.setItemModifierGroups(null);
				item.setTaxes(null);
				item.setOrderItems(null);
				item.setMerchant(null);
			}
		} catch (Exception e) {
			if (e != null) {
				LOGGER.error(" End : ItemServiceImpl : findLineItemByMerchantId():Exception:"+e);

				MailSendUtil.sendExceptionByMail(e,environment);
			}
			LOGGER.error("error: " + e.getMessage());
			LOGGER.error(" End : ItemServiceImpl : findLineItemByMerchantId():Exception:"+e);

		}
		LOGGER.info("-------------- End :: ItemServiceImpl : findLineItemByMerchantId():------------:");

		return items;
	}

	/**
	 * Find by merchantId
	 */
	public Long itemCountByMerchantId(Integer merchantId) {
		LOGGER.info("-------------- Start :: ItemServiceImpl : itemCountByMerchantId():------------:");
		LOGGER.info(": ItemServiceImpl : itemCountByMerchantId():merchantId:"+merchantId);
		LOGGER.info("-------------- End :: ItemServiceImpl : itemCountByMerchantId():------------:");

		return itemmRepository.itemCountByMerchantId(merchantId);
	}

	public Item findItemByItemId(int itemId) {
		LOGGER.info("----------------Start :: ItemServiceImpl : findItemByItemId()------------------------");
		Item item = itemmRepository.findOne(itemId);
		try {
			LOGGER.info(": ItemServiceImpl : findItemByItemId():itemId:"+itemId);

			List<ItemTiming> itemTimings = itemTimingRepository.findByItemId(itemId);
			LOGGER.info(": ItemServiceImpl : findItemByItemId(): ItemTimingListSize:"+itemTimings.size());

			if (itemTimings != null && itemTimings.size() > 0) {
				item.setItemTimings(itemTimings);
				String days = "";
				String startTime = "00:00 AM";
				String endTime = "00:00 AM";
				for (ItemTiming itemTiming : itemTimings) {
					days = days + "," + itemTiming.getDay();
					if (itemTiming.getStartTime() != null && itemTiming.getEndTime() != null
							&& !itemTiming.isHoliday()) {
						startTime = itemTiming.getStartTime();
						endTime = itemTiming.getEndTime();
					}
				}
				LOGGER.info(": ItemServiceImpl : findItemByItemId(): startTime:"+startTime+":endTime:"+endTime);

				if (days != null && !days.isEmpty() && !days.equals(""))
					days.replaceFirst(",", "");

				item.setStartTime(DateUtil.get12Format(startTime));
				item.setEndTime(DateUtil.get12Format(endTime));

				item.setDays(days);
			} else {
				itemTimings = new ArrayList<ItemTiming>();
				String weekDays[] = { "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" };
				for (String day : weekDays) {
					ItemTiming itemTiming = new ItemTiming();
					itemTiming.setDay(day);
					itemTiming.setStartTime("00:00 AM");
					itemTiming.setEndTime("00:00 AM");
					itemTiming.setHoliday(true);
					itemTimings.add(itemTiming);
				}
				item.setItemTimings(itemTimings);
			}
			List<String> categories = new ArrayList<String>();
			List<String> extras = new ArrayList<String>();
			List<CategoryItem> categoryItems = categoryItemRepository.findByItemId(item.getId());

			System.out.println(
					"count output is:-----" + itemModifierGroupRepository.categoryItemModifierCount(item.getId()));
			LOGGER.info(": ItemServiceImpl : findItemByItemId():count output is:-----" + itemModifierGroupRepository.categoryItemModifierCount(item.getId()));

			for (CategoryItem categoryItem : categoryItems) {
				categories.add(categoryItem.getCategory().getName());
			}
			item.setCategoryList(categories);
			List<ItemModifierGroup> itemModifierGroups = itemModifierGroupRepository
					.findByModifierGroupActiveAndItemIdOrderBySortOrderAsc(1,item.getId());

			boolean flag = false;
			List<Long> count = itemModifierGroupRepository.categoryItemModifierCount(item.getId());
			for (Long counts : count) {
				if (counts > 1) {
					flag = true;
					break;
				}
			}
			int groupStatus = 1;
			if (flag) {
				for (ItemModifierGroup itemModifierGroup : itemModifierGroups) {
					itemModifierGroup.setSortOrder(groupStatus++);
				}
				itemModifierGroupRepository.save(itemModifierGroups);
				LOGGER.info(": ItemServiceImpl : findItemByItemId(): saved itemModifierGroups :");

			}

			for (ItemModifierGroup itemModifierGroup : itemModifierGroups) {

				ModifierGroup group = itemModifierGroup.getModifierGroup();
				if (itemModifierGroup.getModifiersLimit() == null) {
					itemModifierGroup.setModifiersLimit(1);
				}
				LOGGER.info(": ItemServiceImpl : findItemByItemId():  ModifierGroupId:"+group.getId());

				List<Modifiers> modifiers = modifierModifierGroupRepository.findByModifierGroupIdAndModifierStatus(group.getId());
				Set modifiersSet = new HashSet(modifiers);
				group.setModifiers(modifiersSet);

			}
			item.setItemModifierGroups(itemModifierGroups);

			for (ItemModifierGroup itemModifierGroup : itemModifierGroups) {
				extras.add(itemModifierGroup.getModifierGroup().getName());
			}
			item.setExtras(extras);
			item.setCategories(null);
			item.setTaxes(null);
			item.setOrderItems(null);
			item.setMerchant(null);
		} catch (Exception e) {
			if (e != null) {
				LOGGER.error("ItemServiceImpl :: findItemByItemId : Exception "+e);
				MailSendUtil.sendExceptionByMail(e,environment);
			}
			LOGGER.error("error: " + e.getMessage());
		}
		LOGGER.info("----------------End :: ItemServiceImpl : findItemByItemId------------------------");
		return item;
	}

	/**
	 * Update line item
	 */
	public void updateLineItemValue(Item item, MultipartFile file) {
		LOGGER.info("----------------Start :: ItemServiceImpl : updateLineItemValue():------------------------");

		try {
			String weekDays[] = { "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" };
			List<ItemTiming> itemTimings = itemTimingRepository.findByItemId(item.getId());
			LOGGER.info("ItemServiceImpl : updateLineItemValue(): ItemTimingListSize:"+itemTimings.size());

			if (itemTimings != null && itemTimings.size() > 0) {
				LOGGER.info("ItemServiceImpl : updateLineItemValue(): itemId:"+item.getId());

				itemTimingRepository.deleteByItemId(item.getId());
				LOGGER.info("ItemServiceImpl : updateLineItemValue(): itemTimings deleted:");

			}
			//
			Item resultItem = itemmRepository.findOne(item.getId());
			LOGGER.info("ItemServiceImpl : updateLineItemValue(): FileSize:"+file.getSize());

			if (!(file.getSize() == 0)) {
				// images
				Long size = file.getSize();
				if (size >= 2097152) {
					// model.addAttribute("filesize", "Maximum Allowed File Size
					// is 2 MB");
					// return "uploadLogo";
				}

				String orgName = file.getOriginalFilename();

				String exts[] = orgName.split("\\.");

				String ext = exts[1];
				String logoName = resultItem.getId() + "_" + resultItem.getName().replaceAll("[^a-zA-Z0-9]", "") + "."
						+ ext;
				// String logoName = item.getId() + "_" + "." + ext;
				String filePath = environment.getProperty("ADMIN_SERVER_LOGO_PATH") + logoName;
				File dest = new File(filePath);
				try {
					file.transferTo(dest);
				} catch (IllegalStateException e) {
					LOGGER.error("error: " + e.getMessage());
					LOGGER.error("ItemServiceImpl : updateLineItemValue(): IllegalStateException:"+e);

					// return "File uploaded failed:" + orgName;
				} catch (IOException e) {
					LOGGER.error("error: " + e.getMessage());
					LOGGER.error("ItemServiceImpl : updateLineItemValue(): IOException :"+e);

					// return "File uploaded failed:" + orgName;
				}
				System.out.println("File uploaded:" + orgName);
				LOGGER.info("ItemServiceImpl : updateLineItemValue():File uploaded:" + orgName);

				resultItem.setItemImage(environment.getProperty("ADMIN_LOGO_PATH_TO_SHOW") + logoName);
			}

			String modifierLimit = item.getModifierLimits();
			String isMinLimit = item.getIsMinLimit();
			String days = item.getDays();
			String startTime = item.getStartTime();
			String endTime = item.getEndTime();
			try {
				startTime = DateUtil.convert12hoursTo24HourseFormate(startTime);
				endTime = DateUtil.convert12hoursTo24HourseFormate(endTime);
			} catch (Exception e) {
				System.out.println(e);
				LOGGER.error("ItemServiceImpl : updateLineItemValue():Exception :" +e);

			}
			itemTimings = new ArrayList<ItemTiming>();
			if ((days != null && !days.isEmpty() && !days.equals(""))
					&& (item.getAllowItemTimings() != null && item.getAllowItemTimings() != 0)) {
				String timingDays[] = days.split(",");
				for (String day : weekDays) {
					if (!days.contains(day)) {
						ItemTiming itemTiming = new ItemTiming();
						itemTiming.setDay(day);
						itemTiming.setStartTime("00:00:00");
						itemTiming.setEndTime("24:00:00");
						itemTiming.setItem(resultItem);
						itemTiming.setHoliday(true);
						itemTimings.add(itemTiming);
					} else {
						ItemTiming itemTiming = new ItemTiming();
						itemTiming.setDay(day);
						itemTiming.setStartTime(startTime);
						if (endTime.equals("00:00")) {
							endTime = "24:00";
						}
						itemTiming.setEndTime(endTime);
						itemTiming.setItem(resultItem);
						itemTiming.setHoliday(false);
						itemTimings.add(itemTiming);
					}
				}

			} else {

				for (String day : weekDays) {
					ItemTiming itemTiming = new ItemTiming();
					itemTiming.setDay(day);
					itemTiming.setStartTime("00:00:00");
					itemTiming.setEndTime("24:00:00");
					itemTiming.setItem(resultItem);
					itemTiming.setHoliday(false);
					itemTimings.add(itemTiming);
				}
			}

			resultItem.setItemTimings(itemTimings);
			String itemModifierGroupsId = item.getItemModifierGroupsIds();
			String itemModifierGroupsStatusIds = item.getItemModifierGroupsStatusIds();
			String itemModifiersStatusIds = item.getItemModifiersStatusIds();
			Integer allowModifierGroupOrder = item.getAllowModifierGroupOrder();
			System.out.println("allowModifierGroupOrder--" + allowModifierGroupOrder);
			LOGGER.info("ItemServiceImpl : updateLineItemValue():allowModifierGroupOrder--" + allowModifierGroupOrder);
			LOGGER.info("ItemServiceImpl : updateLineItemValue():itemId:"+item.getId());

			List<ItemModifierGroup> itemModifierGroups = itemModifierGroupRepository.findByItemId(item.getId());
			List<ItemModifiers> itemModifiers = itemModifiersRepository.findByItemId(item.getId());
			if (itemModifierGroupsStatusIds != null && !itemModifierGroupsStatusIds.isEmpty()
					&& !itemModifierGroupsStatusIds.equals("null")
					&& !itemModifierGroupsStatusIds.equals("undefined")) {

				for (ItemModifierGroup itemModifierGroup : itemModifierGroups) {
					if (itemModifierGroupsStatusIds.contains(itemModifierGroup.getId().toString())) {
						itemModifierGroup.setModifierGroupStatus(IConstant.BOOLEAN_TRUE);
					} else {
						itemModifierGroup.setModifierGroupStatus(IConstant.BOOLEAN_FALSE);
					}
				}
				itemModifierGroupRepository.save(itemModifierGroups);
				LOGGER.info("ItemServiceImpl : updateLineItemValue(): saved itemModifierGroups:");

			} 
			
			else if (itemModifierGroups != null && itemModifierGroups.size() > 0) {
				LOGGER.info("ItemServiceImpl : updateLineItemValue(): itemModifierGroupsSize:"+itemModifierGroups.size());

				for (ItemModifierGroup itemModifierGroup : itemModifierGroups) {

					itemModifierGroup.setModifierGroupStatus(IConstant.BOOLEAN_FALSE);

				}
				itemModifierGroupRepository.save(itemModifierGroups);
				LOGGER.info("ItemServiceImpl : updateLineItemValue(): saved itemModifierGroups:");

			}

			if (itemModifiersStatusIds != null && !itemModifiersStatusIds.isEmpty()
					&& !itemModifiersStatusIds.equals("null") && !itemModifiersStatusIds.equals("undefined")) {

				for (ItemModifiers itemModifier : itemModifiers) {
					if (itemModifiersStatusIds.contains(itemModifier.getId().toString())) {
						itemModifier.setModifierStatus(IConstant.BOOLEAN_TRUE);
					} else {
						itemModifier.setModifierStatus(IConstant.BOOLEAN_FALSE);
					}
				}
				itemModifiersRepository.save(itemModifiers);
				LOGGER.info("ItemServiceImpl : updateLineItemValue(): saved itemModifiers:");

			} else if (itemModifiers != null && itemModifiers.size() > 0) {
				LOGGER.info("ItemServiceImpl : updateLineItemValue():itemModifiersSize:"+itemModifiers.size());

				for (ItemModifiers itemModifier : itemModifiers) {

					itemModifier.setModifierStatus(IConstant.BOOLEAN_FALSE);

				}
				itemModifiersRepository.save(itemModifiers);
				LOGGER.info("ItemServiceImpl : updateLineItemValue(): saved itemModifiers:");

			}

			if (modifierLimit != null || itemModifierGroupsId != null || isMinLimit!=null) {
				String[] modifierLimits = modifierLimit.split(",");
				String[] itemModifierGroupsIds = itemModifierGroupsId.split(",");
				String[] isMinLimitsList = isMinLimit.split(",");
				int index = 0;
				for (String id : itemModifierGroupsIds) {
					Integer itmModiGrpid = Integer.parseInt(id);
					LOGGER.info("ItemServiceImpl : updateLineItemValue(): itemModifierGroupiD:"+itmModiGrpid);

					ItemModifierGroup itemModifierGroup = itemModifierGroupRepository.findOne(itmModiGrpid);
					List<Modifiers> modifiers = null;
					
					if (itemModifierGroup != null) {
						modifiers = modifierModifierGroupRepository
								.findByModifierGroupId(itemModifierGroup.getModifierGroup().getId());
					
					if (modifierLimits.length > index && modifierLimits[index] != null
							&& !modifierLimits[index].isEmpty() && !modifierLimits[index].equals("")) {
						Integer modifierLmt = Integer.parseInt(modifierLimits[index]);
						Integer minModLimit=0;
						if(isMinLimitsList.length > index && isMinLimitsList.length > 0 
								&& !isMinLimitsList[index].isEmpty() && !isMinLimitsList[index].equals("")){
							minModLimit = Integer.parseInt(isMinLimitsList[index]);
						}
						if (item.getAllowModifierLimit() == 0) {
							itemModifierGroup.setModifiersLimit(1);
							itemModifierGroup.setIsMaxLimit(IConstant.BOOLEAN_FALSE);
						} else {
							if (modifiers != null && !modifiers.isEmpty() && modifierLmt > modifiers.size()) {
								itemModifierGroup.setIsMaxLimit(IConstant.BOOLEAN_TRUE);
								
							} else {
								itemModifierGroup.setIsMaxLimit(IConstant.BOOLEAN_FALSE);
							}
							itemModifierGroup.setModifiersLimit(modifierLmt);
							itemModifierGroup.setIsMinLimit(minModLimit);
						}
						itemModifierGroupRepository.save(itemModifierGroup);
						LOGGER.info("ItemServiceImpl : updateLineItemValue(): saved itemModifierGroup:");

						index++;
					} else {

						if (modifiers != null && !modifiers.isEmpty()) {
							itemModifierGroup.setModifiersLimit(modifiers.size());
							itemModifierGroup.setIsMaxLimit(IConstant.BOOLEAN_TRUE);
							itemModifierGroupRepository.save(itemModifierGroup);
							LOGGER.info("ItemServiceImpl : updateLineItemValue(): saved itemModifierGroup:");

						}
						index++;
					}
				}

				}
			}

//			if (item.getItemImage() != null && item.getItemImage() != "" && !item.getItemImage().isEmpty()) {
//				resultItem.setItemImage(item.getItemImage());
//			}

			resultItem.setDescription(item.getDescription());
			resultItem.setAllowModifierLimit(item.getAllowModifierLimit());
			resultItem.setAllowItemTimings(item.getAllowItemTimings());
			resultItem.setItemStatus(item.getItemStatus());
			resultItem.setModifiersLimit(item.getModifiersLimit());
			resultItem.setAllowModifierGroupOrder(allowModifierGroupOrder);
			Merchant merchant = resultItem.getMerchant();
			if (merchant != null && merchant.getOwner() != null && merchant.getOwner().getPos() != null
					&& merchant.getOwner().getPos().getPosId() != null
					&& merchant.getOwner().getPos().getPosId() != 1) {
				if (item.getPrice() != null)
					resultItem.setPrice(item.getPrice());
				if (item.getName() != null)
				{
					resultItem.setName(item.getName());
					LOGGER.info("ItemServiceImpl : updateLineItemValue(): Description:"+resultItem.getName());
				}
			}
			LOGGER.info("ItemServiceImpl : updateLineItemValue(): Description:"+resultItem.getDescription());
			
			itemmRepository.save(resultItem);
			LOGGER.info("ItemServiceImpl : updateLineItemValue(): saved items:");
			
			List<ItemTax> itemTax= itemTaxRepository.findByItemId(item.getId());
			if(itemTax!=null &&  !itemTax.isEmpty())
			{
				
				for (ItemTax itemTax2 : itemTax) {
					if(item.getTexesId()!=null && !item.getTexesId().isEmpty())
					{
					for(Integer ids:item.getTexesId())
					{
						if(itemTax2.getTaxRates().getId().equals(ids))
						{
							itemTax2.setActive(1);
							break;
						}
						else itemTax2.setActive(0);
					}
				
					itemTaxRepository.save(itemTax2);
				} else {
					 itemTax2.setActive(0);
					 itemTaxRepository.save(itemTax2);
				}
				}
				
			}
			
			List<CategoryItem> categoryItem=categoryItemRepository.findByItemId(item.getId());
			System.err.println("");
				if(categoryItem!=null &&  !categoryItem.isEmpty())
				{
					for (CategoryItem categoryItem2 : categoryItem) {
						if(item.getCategoryItemIds()!=null && !item.getCategoryItemIds().isEmpty())
						{
						for(Integer ids:item.getCategoryItemIds())
						{
							if(categoryItem2.getCategory().getId().equals(ids))
							{
								categoryItem2.setActive(1);
								break;
							}
							else categoryItem2.setActive(0);
						}
						categoryItemRepository.save(categoryItem2);
					}else { categoryItem2.setActive(0);
					categoryItemRepository.save(categoryItem2);
					}
					}
					
				}


		} catch (Exception e) {
			if (e != null) {
				LOGGER.error("ItemServiceImpl : updateLineItemValue(): Exception:"+e);

				MailSendUtil.sendExceptionByMail(e,environment);
			}
			LOGGER.error("error: " + e.getMessage());
			LOGGER.error("ItemServiceImpl : updateLineItemValue(): Exception:"+e);

		}
	}

	/**
	 * Find by categoryId
	 */
	public List<Item> findByCategoryId(Integer categoryId) {
		LOGGER.info("-------------Start :: ItemServiceImpl : findByCategoryId():---------------");
		LOGGER.info("ItemServiceImpl : findByCategoryId(): categoryId:"+categoryId);

		List<Item> items = new ArrayList<Item>();
		try {
			List<CategoryItem> categoryItems = categoryItemRepository.findByCategoryIdAndActiveAndItemItemStatus(categoryId,1,0);
			for (CategoryItem categoryItem : categoryItems) {
				categoryItem.getItem().setCategories(null);
				categoryItem.getItem().setItemModifierGroups(null);
				categoryItem.getItem().setTaxes(null);
				categoryItem.getItem().setOrderItems(null);
				categoryItem.getItem().setMerchant(null);
				categoryItem.getItem().setItemTimings(null);
				items.add(categoryItem.getItem());
			}
		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
				LOGGER.info("ItemServiceImpl : findByCategoryId(): Exception:"+e);

			}
			LOGGER.error("error: " + e.getMessage());
			LOGGER.info("ItemServiceImpl : findByCategoryId(): Exception:"+e);

		}
		LOGGER.info("-------------End :: ItemServiceImpl : findByCategoryId():---------------");

		return items;
	}

	/**
	 * Find Items by categoryId
	 */
	public List<Item> findItemByCategoryId(Integer categoryId) {
		LOGGER.info("-------------Start :: ItemServiceImpl : findItemByCategoryId():---------------");
		LOGGER.info(" ItemServiceImpl : findItemByCategoryId(): categoryId :"+categoryId);

		List<Item> items = new ArrayList<Item>();
		try {
			List<CategoryItem> categoryItemsList = categoryItemRepository.findByCategoryId(categoryId);
			for (CategoryItem categoryItem : categoryItemsList) {
				if (categoryItem.getItem() != null && categoryItem.getItem().getItemStatus() != null
						&& categoryItem.getItem().getItemStatus() != IConstant.SOFT_DELETE) {
					String categoryName = "";
					String modifierGroupName = "";
					LOGGER.info(" ItemServiceImpl : findItemByCategoryId(): itemId :"+categoryItem.getItem().getId());

					List<CategoryItem> categoryItems = categoryItemRepository
							.findByItemId(categoryItem.getItem().getId());
					for (CategoryItem cateItem : categoryItems) {
						categoryName = categoryName + "," + cateItem.getCategory().getName();
					}
					categoryName = categoryName.trim();
					if (!categoryName.isEmpty())
						categoryItem.getItem().setCategoriesName(categoryName.substring(1, categoryName.length()));
					List<ItemModifierGroup> itemModifierGroups = itemModifierGroupRepository
							.findByItemId(categoryItem.getItem().getId());
					for (ItemModifierGroup itemModifierGroup : itemModifierGroups) {
						modifierGroupName = modifierGroupName + "," + itemModifierGroup.getModifierGroup().getName();
					}
					modifierGroupName = modifierGroupName.trim();
					if (!modifierGroupName.isEmpty())
						categoryItem.getItem()
								.setModifierGroups(modifierGroupName.substring(1, modifierGroupName.length() - 1));

					categoryItem.getItem().setCategories(null);
					categoryItem.getItem().setItemModifierGroups(null);
					categoryItem.getItem().setTaxes(null);
					categoryItem.getItem().setOrderItems(null);
					categoryItem.getItem().setMerchant(null);
					int selectedMenuOrder = 0;
					Integer sortOrder = categoryItem.getSortOrder();
					if (sortOrder != null && !sortOrder.equals("")) {
						try {
							selectedMenuOrder = sortOrder;
						} catch (Exception e) {
							LOGGER.error(" ItemServiceImpl : findItemByCategoryId(): Exception :"+e);

						}
					}
					String menuOrder = "";
					LOGGER.info(" ItemServiceImpl : findItemByCategoryId(): itemStatus :"+categoryItem.getItem().getItemStatus());

					if (categoryItem.getItem().getItemStatus() != null && categoryItem.getItem().getItemStatus() == 0) {
						menuOrder = "<select  class='category_order' id='menuOrdr_" + categoryItem.getId()
								+ "' style='width: 45%' catid=" + categoryItem.getId() + ">";

					} else {
						menuOrder = "<select class='category_order' id='menuOrdr_" + categoryItem.getId()
								+ "' style='width: 45%' catid=" + categoryItem.getId() + " disabled>";
						menuOrder = menuOrder + "<option id='0' value='0' selected>0</option>";
					}
					LOGGER.info(" ItemServiceImpl : findItemByCategoryId(): categoryItemsListSize :"+categoryItemsList.size());

					for (int i = 1; i <= categoryItemsList.size(); i++) {
						if (selectedMenuOrder == i) {
							menuOrder = menuOrder + "<option id='" + i + "' value='" + i + "' selected>" + i
									+ "</option>";
						} else {
							menuOrder = menuOrder + "<option id='" + i + "' value='" + i + "'>" + i + "</option>";
						}

					}
					menuOrder = menuOrder + "</select>";
					categoryItem.getItem().setMenuOrder(menuOrder);
					items.add(categoryItem.getItem());
				}
			}
		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
				LOGGER.error(" ItemServiceImpl : findItemByCategoryId(): Exception :"+e);

			}
			LOGGER.error("error: " + e.getMessage());
		}
		LOGGER.info("-------------End :: ItemServiceImpl : findItemByCategoryId():--------------");

		return items;
	}

	public String findinventoryLineItemByMerchantId(int merchantId, Integer pageDisplayLength, Integer pageNumber,
			String searchParameter) {
		LOGGER.info("-------------Start :: ItemServiceImpl : findinventoryLineItemByMerchantId():--------------");
		LOGGER.info(" ItemServiceImpl : findinventoryLineItemByMerchantId(): merchantId:"+merchantId+":pageDisplayLength:"+pageDisplayLength+":pageNumber:"+pageNumber+":searchParameter:"+searchParameter);

		Pageable pageable = new PageRequest(pageNumber - 1, pageDisplayLength, Sort.Direction.ASC, "id");
		// Page<Item> page = itemmRepository.findByMerchantId(merchantId,
		// pageable);
		Page<Item> page = itemmRepository.findByMerchantIdAndItemStatusNot(merchantId, pageable, IConstant.SOFT_DELETE);

		List<InventoryItemVo> inventoryItemVos = new ArrayList<InventoryItemVo>();
		try {
			for (Item item : page.getContent()) {
				InventoryItemVo inventoryItemVo = new InventoryItemVo();
				String categoryName = "";
				String modifierGroupName = "";
				LOGGER.info("ItemServiceImpl : findinventoryLineItemByMerchantId():itemId:"+item.getId());

				List<CategoryItem> categoryItems = categoryItemRepository.findByItemIdAndActiveAndCategoryItemStatus(item.getId() ,1 ,0);
				for (CategoryItem categoryItem : categoryItems) {
					categoryName = categoryName + "," + categoryItem.getCategory().getName();
				}
				categoryName = categoryName.trim();
				if (!categoryName.isEmpty()) {
					inventoryItemVo.setCategoriesName(categoryName.substring(1, categoryName.length()));
				} else {
					inventoryItemVo.setCategoriesName("");
				}
				List<ItemModifierGroup> itemModifierGroups = itemModifierGroupRepository.findByItemId(item.getId());
				if (itemModifierGroups != null) {
					for (ItemModifierGroup itemModifierGroup : itemModifierGroups) {
						if (itemModifierGroup.getModifierGroupStatus() != null
								&& itemModifierGroup.getModifierGroupStatus() == IConstant.BOOLEAN_TRUE)
							modifierGroupName = modifierGroupName + ","
									+ itemModifierGroup.getModifierGroup().getName();
					}
				}
				modifierGroupName = modifierGroupName.trim();
				if (modifierGroupName != null && !modifierGroupName.isEmpty()) {
					inventoryItemVo.setModifierGroups(modifierGroupName.substring(1, modifierGroupName.length()));
				} else {
					inventoryItemVo.setModifierGroups("");
				}
				inventoryItemVo.setId(item.getId());
				inventoryItemVo.setName(item.getName());
				if (item.getItemStatus() != null && item.getItemStatus() != IConstant.SOFT_DELETE) {
					if (item.getItemStatus() != null && item.getItemStatus() == 0) {
						// inventoryItemVo.setStatus("Active");
						inventoryItemVo.setStatus("<div><a href=javascript:void(0) class='nav-toggle' itmId="
								+ item.getId() + " style='color: blue;'>Active</a></div>");

					} else {
						// inventoryItemVo.setStatus("InActive");
						inventoryItemVo.setStatus("<div><a href=javascript:void(0) class='nav-toggle' itmId="
								+ item.getId() + " style='color: blue;'>InActive</a></div>");
					}
					inventoryItemVo.setPrice(item.getPrice());
					inventoryItemVo.setAction("<a href=addLineItem?itemId=" + item.getId()
							+ " class='edit'><i class='fa fa-pencil' aria-hidden='true'></i></a>");
					inventoryItemVos.add(inventoryItemVo);
				}
			}
		} catch (Exception e) {
			if (e != null) {
				LOGGER.error("ItemServiceImpl : findinventoryLineItemByMerchantId():Exception:"+e);

				MailSendUtil.sendExceptionByMail(e,environment);
			}
			LOGGER.error("error: " + e.getMessage());
			LOGGER.error("ItemServiceImpl : findinventoryLineItemByMerchantId():Exception:"+e);

		}
		inventoryItemVos = getListBasedOnSearchParameter(searchParameter, inventoryItemVos);

		PersonJsonObject personJsonObject = new PersonJsonObject();
		// Set Total display record
		// personJsonObject.setiTotalDisplayRecords(pageDisplayLength *
		// page.getTotalPages());
		personJsonObject.setiTotalDisplayRecords((int) page.getTotalElements());
		// Set Total record
		personJsonObject.setiTotalRecords((int) page.getTotalElements());
		personJsonObject.setAaData(inventoryItemVos);

		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String json2 = gson.toJson(personJsonObject);
		LOGGER.info("---------------End :: ItemServiceImpl : findinventoryLineItemByMerchantId():--------------");

		return json2;
	}

	private List<InventoryItemVo> getListBasedOnSearchParameter(String searchParameter,
			List<InventoryItemVo> personsList) {
		LOGGER.info("---------------Start :: ItemServiceImpl : getListBasedOnSearchParameter():--------------");
		LOGGER.info(": ItemServiceImpl : getListBasedOnSearchParameter(): searchParameter:"+searchParameter);


		if (null != searchParameter && !searchParameter.equals("")) {
			List<InventoryItemVo> personsListForSearch = new ArrayList<InventoryItemVo>();
			searchParameter = searchParameter.toUpperCase();
			for (InventoryItemVo person : personsList) {
				if (person.getName().toUpperCase().indexOf(searchParameter) != -1
						|| person.getCategoriesName().toUpperCase().indexOf(searchParameter) != -1
						|| person.getModifierGroups().toUpperCase().indexOf(searchParameter) != -1) {
					personsListForSearch.add(person);
				}
			}
			personsList = personsListForSearch;
			personsListForSearch = null;
		}
		LOGGER.info("---------------End :: ItemServiceImpl : getListBasedOnSearchParameter():--------------");

		return personsList;
	}

	public String filterInventoryByCategoryId(Integer merchantId, int categoryId) {
		LOGGER.info("---------------Start :: ItemServiceImpl : filterInventoryByCategoryId():--------------");
		LOGGER.info(": ItemServiceImpl : filterInventoryByCategoryId(): merchantId:"+merchantId+":categoryId:"+categoryId);

		if (categoryId == 0) {
			List<Item> page = itemmRepository.findByMerchantId(merchantId);
			List<InventoryItemVo> inventoryItemVos = new ArrayList<InventoryItemVo>();
			try {
				for (Item item : page) {
					InventoryItemVo inventoryItemVo = new InventoryItemVo();
					String categoryName = "";
					String modifierGroupName = "";
					LOGGER.info(": ItemServiceImpl : filterInventoryByCategoryId(): itemId:"+item.getId());

					List<CategoryItem> categoryItems = categoryItemRepository.findByItemId(item.getId());

					for (CategoryItem categoryItem : categoryItems) {
						LOGGER.info(": ItemServiceImpl : filterInventoryByCategoryId(): itemStatus:"+categoryItem.getCategory().getItemStatus());

						if (categoryItem.getCategory().getItemStatus() != IConstant.SOFT_DELETE)
							categoryName = categoryName + "," + categoryItem.getCategory().getName();
					}
					categoryName = categoryName.trim();
					if (!categoryName.isEmpty()) {
						inventoryItemVo.setCategoriesName(categoryName.substring(1, categoryName.length()));
					} else {
						inventoryItemVo.setCategoriesName("");
					}
					List<ItemModifierGroup> itemModifierGroups = itemModifierGroupRepository.findByItemId(item.getId());
					for (ItemModifierGroup itemModifierGroup : itemModifierGroups) {
						if (itemModifierGroup.getModifierGroup().getShowByDefault() != IConstant.SOFT_DELETE)
							modifierGroupName = modifierGroupName + ","
									+ itemModifierGroup.getModifierGroup().getName();
					}
					modifierGroupName = modifierGroupName.trim();
					if (!modifierGroupName.isEmpty()) {
						inventoryItemVo
								.setModifierGroups(modifierGroupName.substring(1, modifierGroupName.length() - 1));
					} else {
						inventoryItemVo.setModifierGroups("");
					}
					inventoryItemVo.setId(item.getId());
					inventoryItemVo.setName(item.getName());
					if (item.getItemStatus() != null && item.getItemStatus() != IConstant.SOFT_DELETE) {
						if (item.getItemStatus() != null && item.getItemStatus() == 0) {
							// inventoryItemVo.setStatus("Active");
							inventoryItemVo.setStatus("<div><a href=javascript:void(0) class='nav-toggle' itmId="
									+ item.getId() + " style='color: blue;'>Active</a></div>");

						} else {
							// inventoryItemVo.setStatus("InActive");
							inventoryItemVo.setStatus("<div><a href=javascript:void(0) class='nav-toggle' itmId="
									+ item.getId() + " style='color: blue;'>InActive</a></div>");
						}
						inventoryItemVo.setPrice(item.getPrice());
						inventoryItemVo.setAction("<a href=addLineItem?itemId=" + item.getId()
								+ " class='edit'><i class='fa fa-pencil' aria-hidden='true'></i></a>");
						inventoryItemVos.add(inventoryItemVo);
					}
				}
			} catch (Exception e) {
				if (e != null) {
					LOGGER.error("End :: ItemServiceImpl : filterInventoryByCategoryId():Exception :"+e);

					MailSendUtil.sendExceptionByMail(e,environment);
				}
				LOGGER.error("error: " + e.getMessage());
				LOGGER.error("End :: ItemServiceImpl : filterInventoryByCategoryId():Exception :"+e);

			}
			Gson gson = new GsonBuilder().setPrettyPrinting().create();
			LOGGER.info("End :: ItemServiceImpl : filterInventoryByCategoryId():");

			return gson.toJson(inventoryItemVos);
		} else {
			System.out.println("filterInventoryByCategoryId");
			LOGGER.info(": ItemServiceImpl : filterInventoryByCategoryId(): filterInventoryByCategoryId :");

			List<CategoryItem> categoryItems = categoryItemRepository.findByCategoryIdOrderBySortOrderAsc(categoryId);
			System.out.println(categoryItems.size());
			LOGGER.info(": ItemServiceImpl : filterInventoryByCategoryId(): categoryItemsSize() :"+categoryItems.size());

			System.out.println("count output is:-----" + categoryItemRepository.categoryItemCount(categoryId));
			LOGGER.info(": ItemServiceImpl : filterInventoryByCategoryId():count output is:-----" + categoryItemRepository.categoryItemCount(categoryId));

			boolean flag = false;
			LOGGER.info(": ItemServiceImpl : filterInventoryByCategoryId(): categoryId:"+categoryId);

			List<Long> count = categoryItemRepository.categoryItemCount(categoryId);
			for (Long counts : count) {
				if (counts > 1) {
					flag = true;
					break;
				}
			}
			int sortOrdr = 1;
			if (flag) {
				for (CategoryItem categoryItem : categoryItems) {
					categoryItem.setSortOrder(sortOrdr++);
				}
				categoryItemRepository.save(categoryItems);
				LOGGER.info(": ItemServiceImpl : filterInventoryByCategoryId(): categoryItems saved:");

			}

			List<InventoryItemVo> inventoryItemVos = new ArrayList<InventoryItemVo>();
			for (CategoryItem categoryItem : categoryItems) {
				InventoryItemVo inventoryItemVo = new InventoryItemVo();
				String modifierGroupName = "";
				inventoryItemVo.setCategoriesName(categoryItem.getCategory().getName());
				List<ItemModifierGroup> itemModifierGroups = itemModifierGroupRepository
						.findByItemId(categoryItem.getItem().getId());
				for (ItemModifierGroup itemModifierGroup : itemModifierGroups) {
					modifierGroupName = modifierGroupName + "," + itemModifierGroup.getModifierGroup().getName();
				}
				modifierGroupName = modifierGroupName.trim();
				if (!modifierGroupName.isEmpty()) {
					inventoryItemVo.setModifierGroups(modifierGroupName.substring(1, modifierGroupName.length() - 1));
				} else {
					inventoryItemVo.setModifierGroups("");
				}
				inventoryItemVo.setId(categoryItem.getItem().getId());
				inventoryItemVo.setName(categoryItem.getItem().getName());
				if (categoryItem.getItem().getItemStatus() != null
						&& categoryItem.getItem().getItemStatus() != IConstant.SOFT_DELETE) {
					if (categoryItem.getItem().getItemStatus() != null && categoryItem.getItem().getItemStatus() == 0) {
						// inventoryItemVo.setStatus("Active");
						inventoryItemVo.setStatus("<div><a href=javascript:void(0) class='nav-toggle' itmId="
								+ categoryItem.getItem().getId() + "  categoryItemId=" + categoryItem.getId()
								+ " categoryId=" + categoryItem.getCategory().getId()
								+ "    style='color: blue;'>Active</a></div>");

					} else {
						// inventoryItemVo.setStatus("InActive");
						inventoryItemVo.setStatus("<div><a href=javascript:void(0) class='nav-toggle' itmId="
								+ categoryItem.getItem().getId() + "  categoryItemId=" + categoryItem.getId()
								+ "      categoryId=" + categoryItem.getCategory().getId()
								+ "  style='color: blue;'>InActive</a></div>");
					}
					inventoryItemVo.setPrice(categoryItem.getItem().getPrice());
					inventoryItemVo.setAction("<a href=addLineItem?itemId=" + categoryItem.getItem().getId()
							+ " class='edit'><i class='fa fa-pencil' aria-hidden='true'></i></a>");

					int selectedMenuOrder = 0;
					Integer sortOrder = categoryItem.getSortOrder();
					if (sortOrder != null && !sortOrder.equals("")) {
						try {
							selectedMenuOrder = sortOrder;
						} catch (Exception e) {
							LOGGER.error(": End :: ItemServiceImpl : filterInventoryByCategoryId(): Exception:"+e);


						}
					}
					String menuOrder = "";
					if (categoryItem.getItem().getItemStatus() != null && categoryItem.getItem().getItemStatus() == 0) {
						menuOrder = "<select  class='category_order' id='menuOrdr_" + categoryItem.getId()
								+ "' style='width: 75px;' catid=" + categoryItem.getId() + ">";

					} else {
						menuOrder = "<select class='category_order' id='menuOrdr_" + categoryItem.getId()
								+ "' style='width: 75px;' catid=" + categoryItem.getId() + " disabled>";
						menuOrder = menuOrder + "    <option id='0' value='0' selected>0</option>";
					}
					for (int i = 1; i <= categoryItems.size(); i++) {
						if (selectedMenuOrder == i) {
							menuOrder = menuOrder + "<option id='" + i + "' value='" + i + "' selected>" + i
									+ "</option>";
						} else {
							menuOrder = menuOrder + "<option id='" + i + "' value='" + i + "'>" + i + "</option>";
						}

					}
					menuOrder = menuOrder + "</select>";
					inventoryItemVo.setMenuOrder(menuOrder);
					inventoryItemVos.add(inventoryItemVo);
				}

			}
			Gson gson = new GsonBuilder().setPrettyPrinting().create();
			LOGGER.info("------------------End :: ItemServiceImpl : filterInventoryByCategoryId()--------------------");

			return gson.toJson(inventoryItemVos);
		}
	}

	public String searchItemByTxt(Integer merchantId, String searchTxt) {
		LOGGER.info("------------------Start :: ItemServiceImpl : searchItemByTxt()--------------------");
		LOGGER.info(": ItemServiceImpl : searchItemByTxt() :merchantId:"+merchantId+":searchTxt:"+searchTxt);

		List<Item> items = itemmRepository.findByMerchantIdAndItemName(merchantId, searchTxt);
		List<InventoryItemVo> inventoryItemVos = new ArrayList<InventoryItemVo>();
		try {
			for (Item item : items) {
				InventoryItemVo inventoryItemVo = new InventoryItemVo();
				String categoryName = "";
				String modifierGroupName = "";
				LOGGER.info(": ItemServiceImpl : searchItemByTxt() :itemId:"+item.getId());

				List<CategoryItem> categoryItems = categoryItemRepository.findByItemId(item.getId());
				for (CategoryItem categoryItem : categoryItems) {
					categoryName = categoryName + "," + categoryItem.getCategory().getName();
				}
				categoryName = categoryName.trim();
				if (!categoryName.isEmpty()) {
					inventoryItemVo.setCategoriesName(categoryName.substring(1, categoryName.length()));
				} else {
					inventoryItemVo.setCategoriesName("");
				}
				List<ItemModifierGroup> itemModifierGroups = itemModifierGroupRepository.findByItemId(item.getId());
				for (ItemModifierGroup itemModifierGroup : itemModifierGroups) {
					modifierGroupName = modifierGroupName + "," + itemModifierGroup.getModifierGroup().getName();
				}
				modifierGroupName = modifierGroupName.trim();
				if (!modifierGroupName.isEmpty()) {
					inventoryItemVo.setModifierGroups(modifierGroupName.substring(1, modifierGroupName.length() - 1));
				} else {
					inventoryItemVo.setModifierGroups("");
				}
				inventoryItemVo.setId(item.getId());
				inventoryItemVo.setName(item.getName());
				/*
				 * if ( item.getItemStatus()!=null && item.getItemStatus() == 0)
				 * { inventoryItemVo.setStatus("Active"); } else {
				 * inventoryItemVo.setStatus("InActive"); }
				 */
				LOGGER.info(": ItemServiceImpl : searchItemByTxt() :itemStatus:"+item.getItemStatus());

				if (item.getItemStatus() != null && item.getItemStatus() != IConstant.SOFT_DELETE) {
					if (item.getItemStatus() != null && item.getItemStatus() == 0) {
						// inventoryItemVo.setStatus("Active");
						inventoryItemVo.setStatus("<div><a href=javascript:void(0) class='nav-toggle' itmId="
								+ item.getId() + " style='color: blue;'>Active</a></div>");

					} else {
						// inventoryItemVo.setStatus("InActive");
						inventoryItemVo.setStatus("<div><a href=javascript:void(0) class='nav-toggle' itmId="
								+ item.getId() + " style='color: blue;'>InActive</a></div>");
					}
					inventoryItemVo.setPrice(item.getPrice());
					inventoryItemVo.setAction("<a href=addLineItem?itemId=" + item.getId()
							+ " class='edit'><i class='fa fa-pencil' aria-hidden='true'></i></a>");
					inventoryItemVos.add(inventoryItemVo);
				}
			}
		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
				LOGGER.error(": ItemServiceImpl : searchItemByTxt() :Exception:"+e);

			}
			LOGGER.error("error: " + e.getMessage());
		}
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		LOGGER.info("------------------End :: ItemServiceImpl : searchItemByTxt() :---------------------");

		return gson.toJson(inventoryItemVos);
	}

	/*
	 * public void updateItemStatusById(Item item) {
	 * 
	 * 
	 * Item result = itemmRepository.findOne(item.getId());
	 * 
	 * System.out.println(result.getId()+" "+result.getItemStatus());
	 * result.setItemStatus(item.getItemStatus()); itemmRepository.save(result);
	 * 
	 * }
	 */
	// change method return type and add parameter categoryItemId
	public String updateItemStatusById(Item item, Integer categoryItemId) {
		LOGGER.info("------------------Start :: ItemServiceImpl : updateItemStatusById() : :---------------------");
	LOGGER.info("ItemServiceImpl : updateItemStatusById() :"+categoryItemId);

		/*
		 * Item result = itemmRepository.findOne(item.getId());
		 * result.setItemStatus(item.getItemStatus());
		 * itemmRepository.save(result);
		 */

		// changes made by manish gupta for update items states and sorting
		// order on 05/05/17
		Map<String, Object> map = new HashMap();
		try {
			LOGGER.info("ItemServiceImpl : updateItemStatusById() : itemId:"+item.getId());

			Item result = itemmRepository.findOne(item.getId());
			CategoryItem categoryItemResult = categoryItemRepository.findOne(categoryItemId);
			int mostAvailableOrder = 0;
			if (item.getItemStatus() == IConstant.BOOLEAN_FALSE) {
				System.out.println("inside if item.getItemStatus()-" + item.getItemStatus());
				LOGGER.info("ItemServiceImpl : updateItemStatusById() :inside if item.getItemStatus()-" + item.getItemStatus());

				Pageable pageable = new PageRequest(1 - 1, 10, Sort.Direction.DESC, "id");
				Page<Item> allItems = itemmRepository.findByMerchantIdAndItemStatusNot(result.getMerchant().getId(),
						pageable, IConstant.SOFT_DELETE);
				int totalItems = (int) allItems.getTotalElements();
				System.out.println("getTotalItems:-" + totalItems + " result.getMerchant().getId()-"
						+ result.getMerchant().getId() + " categoryItemId-" + categoryItemId);
				LOGGER.info("ItemServiceImpl : updateItemStatusById() :getTotalItems:-" + totalItems + " result.getMerchant().getId()-"
						+ result.getMerchant().getId() + " categoryItemId-" + categoryItemId);

				List<CategoryItem> itemss = categoryItemRepository.findByCategoryIdAndSortOrderNotOrderBySortOrderAsc(
						categoryItemResult.getCategory().getId(), IConstant.BOOLEAN_FALSE);
				int itemListSize = itemss.size();
				System.out.println("itemListSize-" + itemListSize);
				LOGGER.info("ItemServiceImpl : updateItemStatusById() :itemListSize-" + itemListSize);

				if (itemss != null && itemss.size() > 0) {
					LOGGER.info("ItemServiceImpl : updateItemStatusById() : itemSize:"+itemss.size());

					for (int i = 1; i <= totalItems; i++) {
						if (i <= itemListSize) {
							CategoryItem categoryItem2 = itemss.get(i - 1);
							System.out.println("categoryItem2.getSortOrder()-" + categoryItem2.getSortOrder());
							LOGGER.info("ItemServiceImpl : updateItemStatusById() :categoryItem2.getSortOrder()-" + categoryItem2.getSortOrder());

							if (categoryItem2 != null) {
								if (categoryItem2.getSortOrder() != i) {
									mostAvailableOrder = i;
									System.out.println("i-" + i + "mostAvailableOrder-" + mostAvailableOrder);
									LOGGER.info("ItemServiceImpl : updateItemStatusById() :i-" + i + "mostAvailableOrder-" + mostAvailableOrder);

									break;
								}
							}
						} else {
							mostAvailableOrder = i;
							System.out.println("mostAvailableOrder-" + mostAvailableOrder);
							LOGGER.info("ItemServiceImpl : updateItemStatusById() :mostAvailableOrder-" + mostAvailableOrder);

							break;
						}
					}
				} else {
					mostAvailableOrder = 1;
				}
				categoryItemResult.setSortOrder(mostAvailableOrder);

			} else {
				categoryItemResult.setSortOrder(IConstant.BOOLEAN_FALSE);
				mostAvailableOrder = IConstant.BOOLEAN_FALSE;
			}

			System.out.println(result.getId() + " " + result.getItemStatus());
			LOGGER.info("ItemServiceImpl : updateItemStatusById() :" +result.getId() + " " + result.getItemStatus());

			result.setItemStatus(item.getItemStatus());
			itemmRepository.save(result);
			categoryItemRepository.save(categoryItemResult);
			LOGGER.info("ItemServiceImpl : updateItemStatusById() : save categoryItemResult");

			map.put("result", "success");
			map.put("categoryItemResult", "success");
			map.put("menuOrder", mostAvailableOrder);
		} catch (Exception e) {
			map.put("result", "failed");
			LOGGER.error("End :: ItemServiceImpl : updateItemStatusById() :Exception:"+e);

		}
		LOGGER.info("-----------------End :: ItemServiceImpl : updateItemStatusById() :-----------------------");

		return new Gson().toJson(map);
	}

	public boolean updateItemSortOrderById(CategoryItem categoryItem, String action) {
		LOGGER.info("-----------------Start :: ItemServiceImpl : updateItemSortOrderById() :-----------------------");
		LOGGER.info(":ItemServiceImpl : updateItemSortOrderById() :categoryItemId:"+categoryItem.getId());

		CategoryItem result = categoryItemRepository.findOne(categoryItem.getId());
		System.out.println(
				"result--" + result.getCategory().getId() + " " + result.getSortOrder() + " " + result.getId());
		LOGGER.info(":ItemServiceImpl : updateItemSortOrderById() :result--" + result.getCategory().getId() + " " + result.getSortOrder() + " " + result.getId());

		if (result != null) {
			int oldOrder = 0;
			if (result.getSortOrder() != null) {
				oldOrder = result.getSortOrder();
			}
			List<CategoryItem> categoryItems = categoryItemRepository.findByCategoryIdAndSortOrderAndIdNot(
					result.getCategory().getId(), categoryItem.getSortOrder(), result.getId());
			// if(action!=null&&!action.isEmpty()){
			// if(action.equals("check")){
			if (categoryItems != null && categoryItems.size() > 0) {
				result.setSortOrder(categoryItem.getSortOrder());
				categoryItemRepository.save(result);
				LOGGER.info(":ItemServiceImpl : updateItemSortOrderById() :saved CategoryItem:");

				for (CategoryItem categoryItem2 : categoryItems) {
					categoryItem2.setSortOrder(oldOrder);
					categoryItemRepository.save(categoryItem2);
					LOGGER.info(":ItemServiceImpl : updateItemSortOrderById() :saved categoryItem:");

				}
				LOGGER.info("-------------------End : :ItemServiceImpl : updateItemSortOrderById() : return true:--------------------------");

				return true;
			} else {
				result.setSortOrder(categoryItem.getSortOrder());
				categoryItemRepository.save(result);
				LOGGER.info("-------------------End : :ItemServiceImpl : updateItemSortOrderById() : return true :--------------------------");

				return true;
			} // }else{
				// oldOrder=result.getSortOrder();

			// return true;
			// }
		} else {
			LOGGER.info("-------------------End : :ItemServiceImpl : updateItemSortOrderById() : return true --------------------------");

			return true;
		}

	}

	public boolean updateItemModifierGroupSortOrderById(ItemModifierGroup itemModifierGroup, String action,
			Integer itemModifierGroupOrder, Integer itemId) {
		LOGGER.info("-------------------Start : :ItemServiceImpl : updateItemModifierGroupSortOrderById() :--------------------------");
		LOGGER.info(" :ItemServiceImpl : updateItemModifierGroupSortOrderById() :action:"+action+":itemModifierGroupOrder:"+itemModifierGroupOrder+":itemId:"+itemId);

		ItemModifierGroup result = itemModifierGroupRepository.findOne(itemModifierGroup.getId());

		System.out.println(" itemId--" + itemId);
		
		Item item = itemmRepository.findOne(itemId);

		// itemmRepository.save(itemId);
		if (result != null) {
			int oldOrder = 0;
			if (item != null) {
				System.out.println("inside item");
				LOGGER.info(" :ItemServiceImpl : updateItemModifierGroupSortOrderById() :inside item");

				item.setAllowModifierGroupOrder(itemModifierGroupOrder);
				itemmRepository.save(item);
				LOGGER.info(" :ItemServiceImpl : updateItemModifierGroupSortOrderById() :saved item");

			}

			if (result.getSortOrder() != null) {
				oldOrder = result.getSortOrder();
			}
			List<ItemModifierGroup> itemModifierGroups = itemModifierGroupRepository.findByItemIdAndSortOrderAndIdNot(
					result.getItem().getId(), itemModifierGroup.getSortOrder(), result.getId());
			if (action != null && !action.isEmpty()) {
				if (action.equals("check")) {
					if (itemModifierGroups != null && itemModifierGroups.size() > 0) {
						result.setSortOrder(itemModifierGroup.getSortOrder());
						itemModifierGroupRepository.save(result);
						LOGGER.info(" :ItemServiceImpl : updateItemModifierGroupSortOrderById() :saved ItemModifierGroup");

						for (ItemModifierGroup itemModifierGroup2 : itemModifierGroups) {
							itemModifierGroup2.setSortOrder(oldOrder);
							itemModifierGroupRepository.save(itemModifierGroup2);
							LOGGER.info(" :ItemServiceImpl : updateItemModifierGroupSortOrderById() :saved ItemModifierGroup");

						}
						LOGGER.info("-------------------End : :ItemServiceImpl : updateItemModifierGroupSortOrderById() : return true--------------------------");

						return true;
						// return false;
					} else {
						result.setSortOrder(itemModifierGroup.getSortOrder());
						itemModifierGroupRepository.save(result);
						LOGGER.info(" :ItemServiceImpl : updateItemModifierGroupSortOrderById() :saved ItemModifierGroup");

						LOGGER.info("-------------------End : :ItemServiceImpl : updateItemModifierGroupSortOrderById() : return true--------------------------");

						return true;
					}
				} else {
					// oldOrder=result.getSortOrder();
					result.setSortOrder(itemModifierGroup.getSortOrder());
					itemModifierGroupRepository.save(result);
					for (ItemModifierGroup itemModifierGroup2 : itemModifierGroups) {
						itemModifierGroup2.setSortOrder(oldOrder);
						itemModifierGroupRepository.save(itemModifierGroup2);
						LOGGER.info(" :ItemServiceImpl : updateItemModifierGroupSortOrderById() :saved ItemModifierGroup");

					}
					LOGGER.info("-------------------End : :ItemServiceImpl : updateItemModifierGroupSortOrderById() : return true--------------------------");

					return true;
				}
			} else {
				LOGGER.info("-------------------End : :ItemServiceImpl : updateItemModifierGroupSortOrderById() : return true--------------------------");

				return true;
			}
		} else {
			LOGGER.info("-------------------End : :ItemServiceImpl : updateItemModifierGroupSortOrderById() : return true--------------------------");

			return true;
		}
	}

	public void updateInventoryItemStatusById(Item item) {
		LOGGER.info("-------------------Start : :ItemServiceImpl : updateInventoryItemStatusById() :--------------------------");
		LOGGER.info(":ItemServiceImpl : updateInventoryItemStatusById() : itemId :"+item.getId());

		Item result = itemmRepository.findOne(item.getId());
		result.setItemStatus(item.getItemStatus());
		itemmRepository.save(result);
		LOGGER.info("--------------: End :: ItemServiceImpl : updateInventoryItemStatusById() : saved item :--------------");

	}

	/*
	 * public List<Item> getAllItemsByMerchantUId(String merchantUId) { try {
	 * return itemmRepository.findByMerchantAccessToken(merchantUId); } catch
	 * (Exception exception) { exception.printStackTrace(); return new
	 * ArrayList<Item>(); } }
	 */
	public List<Item> getAllItemsByMerchantUId(String merchantUId) {
		LOGGER.info("-------------------Start : :ItemServiceImpl : getAllItemsByMerchantUId() :--------------------------");
		LOGGER.info(" :ItemServiceImpl : getAllItemsByMerchantUId() :merchantUId:"+merchantUId);

		List<Item> items = itemmRepository.findByMerchantMerchantUid(merchantUId);

		List<Item> finalItems = new ArrayList<Item>();
		for (Item item : items) {
			if (item.getItemStatus() != null && item.getItemStatus() != IConstant.SOFT_DELETE) {
				Item item2 = new Item();
				item2.setId(item.getId());
				item2.setName(item.getName());
				item2.setPrice(item.getPrice());
				item2.setTaxes(null);
				item2.setCategories(null);
				// item2.setPriceType(item.getPriceType());
				// item2.setModifiedTime(item.getModifiedTime());
				// item2.setPosItemId(item.getPosItemId());
				// item2.setMerchant(item.getMerchant());
				// item2.setIsDefaultTaxRates(item.getIsDefaultTaxRates());
				// item2.setUnitName(item.getUnitName());
				// item2.setItemStatus(item.getItemStatus());
				// item2.setModifierLimits(item.getModifierLimits());
				// item2.setAllowModifierLimit(item.getAllowModifierLimit());
				// item2.setAllowModifierGroupOrder(item.getAllowModifierGroupOrder());
				// item2.setDescription(item.getDescription());

				finalItems.add(item2);
			}
		}
		LOGGER.info("----------------End :: ItemServiceImpl : getAllItemsByMerchantUId():-------------");

		return finalItems;
	}

	public List<Item> getAllItemsByVenderUId(String vendorUId) {
		LOGGER.info("----------------Start :: ItemServiceImpl : getAllItemsByVenderUId():-------------");
		LOGGER.info(" ItemServiceImpl : getAllItemsByVenderUId():vendorUId:"+vendorUId);

		List<Item> items = itemmRepository.findByMerchantOwnerVendorUid(vendorUId);
		List<Item> finalItems = new ArrayList<Item>();
		for (Item item : items) {
			if (item.getItemStatus() != null && item.getItemStatus() != IConstant.SOFT_DELETE) {
				Item item2 = new Item();
				item2.setId(item.getId());
				item2.setName(item.getName());
				item2.setPrice(item.getPrice());
				item2.setTaxes(null);
				item2.setCategories(null);
				/*
				 * item2.setPriceType(item.getPriceType());
				 * item2.setModifiedTime(item.getModifiedTime());
				 * item2.setPosItemId(item.getPosItemId());
				 * //item2.setMerchant(item.getMerchant());
				 * item2.setIsDefaultTaxRates(item.getIsDefaultTaxRates());
				 * item2.setUnitName(item.getUnitName());
				 * item2.setItemStatus(item.getItemStatus());
				 * item2.setModifierLimits(item.getModifierLimits());
				 * item2.setAllowModifierLimit(item.getAllowModifierLimit());
				 * item2.setAllowModifierGroupOrder(item.
				 * getAllowModifierGroupOrder());
				 * item2.setDescription(item.getDescription());
				 */

				finalItems.add(item2);
			}
		}
		LOGGER.info("-------------End :: ItemServiceImpl : getAllItemsByVenderUId():--------------------");

		return finalItems;
	}

	public PosIntegration findBySchedularStatusByMerchantId(Integer merchantId) {
		LOGGER.info("-------------Start :: ItemServiceImpl : findBySchedularStatusByMerchantId():--------------------");

		PosIntegration integration;
		int count = 0;
		while (true) {
			count++;
			LOGGER.info(": ItemServiceImpl : findBySchedularStatusByMerchantId(): merchantId :"+merchantId);

			integration = posIntegrationRepository.findByMerchantId(merchantId);

			System.out.println(integration.getSchedulerStatus());
			LOGGER.info(": ItemServiceImpl : findBySchedularStatusByMerchantId() :"+integration.getSchedulerStatus());

			if (integration.getSchedulerStatus() > 0 || count == 500) {
				integration.setSchedulerStatus(1);
				break;
			}
			try {
				Thread.sleep(3000);
			} catch (Exception e) {
				LOGGER.error("error: " + e.getMessage());
				LOGGER.info(":End :: ItemServiceImpl : findBySchedularStatusByMerchantId() :"+e);

			}
		}
		LOGGER.info("---------------------:End :: ItemServiceImpl : findBySchedularStatusByMerchantId():--------------");

		return integration;
	}
	
	public Item findItemByItemPosId(String itemPosId, Integer merchantId){
		LOGGER.info("---------------------:Start :: ItemServiceImpl : findItemByItemPosId():--------------");
		LOGGER.info(": ItemServiceImpl : findItemByItemPosId():itemPosId:"+itemPosId+":merchantId:"+merchantId);
		LOGGER.info("---------------------:End :: ItemServiceImpl : findItemByItemPosId():--------------");

		return itemmRepository.findByPosItemIdAndMerchantId(itemPosId, merchantId);
	}

	public Item createItem(Item item, Merchant merchant) {
		LOGGER.info("---------------------:Start :: ItemServiceImpl : createItem():--------------");

		item.setMerchant(merchant);
		item.setPriceType("FIXED");
		item.setIsDefaultTaxRates(true);
		item.setIsHidden(false);
		LOGGER.info("---------------------:End :: ItemServiceImpl : createItem(): item saved:--------------");

		return itemmRepository.save(item);
		
	}
}
