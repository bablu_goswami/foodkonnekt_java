package com.foodkonnekt.serviceImpl;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.foodkonnekt.model.Merchant;
import com.foodkonnekt.model.NotificationMethod;
import com.foodkonnekt.model.NotificationMethodType;
import com.foodkonnekt.repository.NotificationMethodRepository;
import com.foodkonnekt.repository.NotificationMethodTypeRepository;
import com.foodkonnekt.service.NotificationMethodService;
import com.foodkonnekt.util.DateUtil;
import com.foodkonnekt.util.IConstant;
import com.foodkonnekt.util.ProducerUtil;

@Service
public class NotificationMethodServiceImpl implements NotificationMethodService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(NotificationMethodServiceImpl.class);
	
	@Autowired
    private Environment environment;
	
	 @Autowired
	  private NotificationMethodTypeRepository notificationMethodTypeRepository;
	 
	 @Autowired
	  private NotificationMethodRepository notificationMethodRepository;
	 
	public List<NotificationMethodType> getNotificationMethodTypes() {
		return notificationMethodTypeRepository.findAll();
	}

	
	 String getSMSRequestId(Merchant merchant){
		 
		 String response=null;
		try {
			Pattern pt = Pattern.compile("[^a-zA-Z0-9]");
			 String merchantName=merchant.getName();
	        Matcher match= pt.matcher(merchantName);
	        while(match.find())
	        {
	            String s= match.group();
	            merchantName=merchantName.replaceAll("\\"+s, "");
	        }
			response = ProducerUtil.createOrganizationOnTrumpia(merchantName,"make SMS",environment);
			System.out.println("Trumpia response=========="+response);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			LOGGER.error("error: " + e.getMessage());
		}
		return response;
	}
	public NotificationMethod saveNotificationMethod(NotificationMethod notificationMethod) {
		LOGGER.info("NotificationMethodServiceImpl : Inside saveNotificationMethod : Start");
		String smsRequestId=null;
		if(notificationMethod!=null){
			Date today = (notificationMethod.getMerchant() != null
					&& notificationMethod.getMerchant().getTimeZone() != null
					&& notificationMethod.getMerchant().getTimeZone().getTimeZoneCode() != null)
							? DateUtil.getCurrentDateForTimeZonee(
									notificationMethod.getMerchant().getTimeZone().getTimeZoneCode())
							: new Date();
		notificationMethod.setCreatedDate(today);
		notificationMethod.setUpdatedDate(today);
		
		LOGGER.info("NotificationMethodServiceImpl : Inside saveNotificationMethod : NotificationMethodType :: "+notificationMethod.getNotificationMethodType());
		
		if(notificationMethod.getNotificationMethodType()!=null && notificationMethod.getNotificationMethodType().getId()==IConstant.SMS ){
			
				List<NotificationMethod> list = notificationMethodRepository
						.findByMerchantId(notificationMethod.getMerchant().getId());
				LOGGER.info("NotificationMethodServiceImpl : Inside saveNotificationMethod : MerchantId :: "+notificationMethod.getMerchant().getId());
				for (NotificationMethod method : list) {
					if (method.getSmsRequestId() != null) {
						smsRequestId = method.getSmsRequestId();
						break;
					}
				}
				if (smsRequestId != null) {
					notificationMethod.setSmsRequestId(smsRequestId);
				} else {
					String requestId = null;
					String statusCode = null;
					String response = getSMSRequestId(notificationMethod.getMerchant());
					
					if(response != null){
						JSONObject jsonObject=new JSONObject(response);
						   if(jsonObject.has("statusCode")){
							   statusCode =jsonObject.getString("statusCode");
							   if(statusCode.equalsIgnoreCase("Organisation added successfully")){
								   if(jsonObject.has("requestId")){
									   requestId =jsonObject.getString("requestId");
									   notificationMethod.setSmsRequestId(requestId);
								   }
								   LOGGER.info("NotificationMethodServiceImpl : Inside saveNotificationMethod : statusCode "+statusCode);
							   }else{
								   LOGGER.info("NotificationMethodServiceImpl : Inside saveNotificationMethod : statusCode "+statusCode);
							   }
						    }
						}
					}
				}
		LOGGER.info("NotificationMethodServiceImpl : Inside saveNotificationMethod : End");
		return notificationMethodRepository.save(notificationMethod);
		}else{
			LOGGER.info("NotificationMethodServiceImpl : Inside saveNotificationMethod : End");
			return null;
		}
	}

	public List<NotificationMethod> getNotificationMethods(Integer merchantId) {
		// TODO Auto-generated method stub
		return notificationMethodRepository.findByMerchantId(merchantId);
	}


	public NotificationMethod updateNotificationById(Integer id, Boolean status) {
		LOGGER.info("NotificationMethodServiceImpl : Inside updateNotificationById : Start");
		NotificationMethod method = notificationMethodRepository.findById(id);
		if (method != null) {
			method.setActive(status);
		}
		LOGGER.info("NotificationMethodServiceImpl : Inside updateNotificationById : End");
		return notificationMethodRepository.save(method);
	}

	public NotificationMethod findByMerchantIdAndContact(Integer merchantId, String contact) {
		LOGGER.info("NotificationMethodServiceImpl : Inside findByMerchantIdAndContact : Start : MerchantId :: "+merchantId);
		LOGGER.info("NotificationMethodServiceImpl : Inside findByMerchantIdAndContact : End : MerchantId :: "+merchantId);
		return notificationMethodRepository.findByMerchantIdAndContact(merchantId, contact);
	}

	public void deleteNotificationMethod(Integer id) {
		LOGGER.info("NotificationMethodServiceImpl : Inside deleteNotificationMethod : Start : NotificationId :: "+id);
		NotificationMethod method = notificationMethodRepository.findById(id);
		if (method != null) {
			notificationMethodRepository.delete(method);
			System.out.println("deleted");
		}
		LOGGER.info("NotificationMethodServiceImpl : Inside deleteNotificationMethod : End : NotificationId :: "+id);
	}

	public NotificationMethod findById(Integer id) {
		return notificationMethodRepository.findById(id);
	}

	public List<NotificationMethod> notifications(Integer merchantId) {
		return notificationMethodRepository.findByMerchantIdAndIsActive(merchantId);
	}


	public List<NotificationMethod> findByMerchantIdAndMethodTypeId(Integer merchantId,Integer methodTypeId) {
		return notificationMethodRepository.findByMerchantIdAndMethodTypeId(merchantId,methodTypeId);
	}
}
