package com.foodkonnekt.serviceImpl;

import com.TPISoft.SmartPayments.BridgePayTest;
import com.TPISoft.SmartPayments.Response;
import com.TPISoft.SmartPayments.SmartPaymentsSoapProxy;
import com.firstData.paymentGatway.CardConnectRestClientExample;
import com.firstdata.payeezy.JSONHelper;
import com.firstdata.payeezy.PayeezyClientHelper;
import com.firstdata.payeezy.models.transaction.PayeezyResponse;
import com.firstdata.payeezy.models.transaction.TransactionRequest;
import com.foodkonnekt.clover.vo.AllOrderVo;
import com.foodkonnekt.clover.vo.CloverOrderVO;
import com.foodkonnekt.clover.vo.ItemVO;
import com.foodkonnekt.clover.vo.Modifications;
import com.foodkonnekt.clover.vo.ModifierVO;
import com.foodkonnekt.clover.vo.NotificationVO;
import com.foodkonnekt.clover.vo.OrderItemModifierViewVO;
import com.foodkonnekt.clover.vo.OrderItemVO;
import com.foodkonnekt.clover.vo.OrderItemViewVO;
import com.foodkonnekt.clover.vo.OrderJson;
import com.foodkonnekt.clover.vo.Payload;
import com.foodkonnekt.model.Address;
import com.foodkonnekt.model.CardInfo;
import com.foodkonnekt.model.Category;
import com.foodkonnekt.model.CategoryDto;
import com.foodkonnekt.model.CategoryTiming;
import com.foodkonnekt.model.Clover;
import com.foodkonnekt.model.ConvenienceFee;
import com.foodkonnekt.model.CouponRedeemedDto;
import com.foodkonnekt.model.Customer;
import com.foodkonnekt.model.CustomerCode;
import com.foodkonnekt.model.EncryptedKey;
import com.foodkonnekt.model.Item;
import com.foodkonnekt.model.ItemDto;
import com.foodkonnekt.model.ItemModifierGroup;
import com.foodkonnekt.model.ItemModifiers;
import com.foodkonnekt.model.ItemTax;
import com.foodkonnekt.model.Koupons;
import com.foodkonnekt.model.Merchant;
import com.foodkonnekt.model.MerchantConfiguration;
import com.foodkonnekt.model.MerchantOrders;
import com.foodkonnekt.model.ModifierDto;
import com.foodkonnekt.model.ModifierGroupDto;
import com.foodkonnekt.model.Modifiers;
import com.foodkonnekt.model.NotificationMethod;
import com.foodkonnekt.model.NotificationTracker;
import com.foodkonnekt.model.OpeningClosingDay;
import com.foodkonnekt.model.OpeningClosingTime;
import com.foodkonnekt.model.OrderDiscount;
import com.foodkonnekt.model.OrderItem;
import com.foodkonnekt.model.OrderItemModifier;
import com.foodkonnekt.model.OrderPaymentDetail;
import com.foodkonnekt.model.OrderPizza;
import com.foodkonnekt.model.OrderPizzaCrust;
import com.foodkonnekt.model.OrderPizzaToppings;
import com.foodkonnekt.model.OrderPrintStatus;
import com.foodkonnekt.model.OrderR;
import com.foodkonnekt.model.OrderType;
import com.foodkonnekt.model.PaymentGateWay;
import com.foodkonnekt.model.PaymentMode;
import com.foodkonnekt.model.PickUpTime;
import com.foodkonnekt.model.PizzaCrust;
import com.foodkonnekt.model.PizzaSize;
import com.foodkonnekt.model.PizzaTemplate;
import com.foodkonnekt.model.PizzaTemplateSize;
import com.foodkonnekt.model.Printer;
import com.foodkonnekt.model.TaxRates;
import com.foodkonnekt.model.Vendor;
import com.foodkonnekt.model.VirtualFund;
import com.foodkonnekt.model.Zone;
import com.foodkonnekt.repository.AddressRepository;
import com.foodkonnekt.repository.CardInfoRepository;
import com.foodkonnekt.repository.CategoryItemRepository;
import com.foodkonnekt.repository.CategoryRepository;
import com.foodkonnekt.repository.CategoryTimingRepository;
import com.foodkonnekt.repository.ConvenienceFeeRepository;
import com.foodkonnekt.repository.CustomerCodeRepository;
import com.foodkonnekt.repository.CustomerrRepository;
import com.foodkonnekt.repository.ItemModifierGroupRepository;
import com.foodkonnekt.repository.ItemModifiersRepository;
import com.foodkonnekt.repository.ItemTaxRepository;
import com.foodkonnekt.repository.ItemTimingRepository;
import com.foodkonnekt.repository.ItemmRepository;
import com.foodkonnekt.repository.MerchantEncryptedKeyRepository;
import com.foodkonnekt.repository.MerchantOrdersRepository;
import com.foodkonnekt.repository.MerchantRepository;
import com.foodkonnekt.repository.MerchantSubscriptionRepository;
import com.foodkonnekt.repository.ModifierModifierGroupRepository;
import com.foodkonnekt.repository.ModifiersRepository;
import com.foodkonnekt.repository.NotificationMethodRepository;
import com.foodkonnekt.repository.NotificationTrackerRepository;
import com.foodkonnekt.repository.OnlineNotificationStatusRepository;
import com.foodkonnekt.repository.OpeningClosingDayRepository;
import com.foodkonnekt.repository.OpeningClosingTimeRepository;
import com.foodkonnekt.repository.OrderDiscountRepository;
import com.foodkonnekt.repository.OrderItemModifierRepository;
import com.foodkonnekt.repository.OrderItemRepository;
import com.foodkonnekt.repository.OrderPaymentDetailRepository;
import com.foodkonnekt.repository.OrderPizzaCrustRepository;
import com.foodkonnekt.repository.OrderPizzaRepository;
import com.foodkonnekt.repository.OrderPizzaToppingsRepository;
import com.foodkonnekt.repository.OrderPrintStatusRepository;
import com.foodkonnekt.repository.OrderRepository;
import com.foodkonnekt.repository.OrderTypeRepository;
import com.foodkonnekt.repository.PaymentGateWayRepository;
import com.foodkonnekt.repository.PaymentModeRepository;
import com.foodkonnekt.repository.PickUpTimeRepository;
import com.foodkonnekt.repository.PizzaCrustRepository;
import com.foodkonnekt.repository.PizzaSizeRepository;
import com.foodkonnekt.repository.PizzaTemplateRepository;
import com.foodkonnekt.repository.PizzaTemplateSizeRepository;
import com.foodkonnekt.repository.PizzaToppingRepository;
import com.foodkonnekt.repository.PrinterRepository;
import com.foodkonnekt.repository.TaxRateRepository;
import com.foodkonnekt.repository.VendorRepository;
import com.foodkonnekt.repository.VirtualFundRepository;
import com.foodkonnekt.repository.ZoneRepository;
import com.foodkonnekt.service.MerchantConfigurationService;
import com.foodkonnekt.service.MerchantService;
import com.foodkonnekt.service.OrderService;
import com.foodkonnekt.util.AppNotification;
import com.foodkonnekt.util.AuthorizeTest;
import com.foodkonnekt.util.CloudPrintUtil;
import com.foodkonnekt.util.CloverUrlUtil;
import com.foodkonnekt.util.CommonUtil;
import com.foodkonnekt.util.DateUtil;
import com.foodkonnekt.util.EncryptionDecryptionToken;
import com.foodkonnekt.util.EncryptionDecryptionUtil;
import com.foodkonnekt.util.FaxUtility;
import com.foodkonnekt.util.IConstant;
import com.foodkonnekt.util.MailSendUtil;
import com.foodkonnekt.util.OrderUtil;
import com.foodkonnekt.util.PayeezyUtilProperties;
import com.foodkonnekt.util.ProducerUtil;
import com.foodkonnekt.util.ShortCodeUtil;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.merchantwarehouse.schemas.merchantware.v45.CaptureRequest;
import com.merchantwarehouse.schemas.merchantware.v45.CreditSoapProxy;
import com.merchantwarehouse.schemas.merchantware.v45.MerchantCredentials;
import com.merchantwarehouse.schemas.merchantware.v45.TransactionResponse45;
import com.merchantwarehouse.schemas.merchantware.v45.VoidRequest;
import com.stripe.Stripe;
import com.stripe.model.Charge;
import com.stripe.model.Refund;
import net.authorize.api.contract.v1.CreateTransactionRequest;
import net.authorize.api.contract.v1.CreateTransactionResponse;
import net.authorize.api.contract.v1.MerchantAuthenticationType;
import net.authorize.api.contract.v1.MessageTypeEnum;
import net.authorize.api.contract.v1.TransactionRequestType;
import net.authorize.api.contract.v1.TransactionResponse;
import net.authorize.api.contract.v1.TransactionTypeEnum;
import net.authorize.api.controller.CreateTransactionController;
import net.authorize.api.controller.base.ApiOperationBase;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.rmi.RemoteException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

@Service
public class OrderServiceImpl extends Thread implements OrderService, Runnable {

	private static final Logger LOGGER = LoggerFactory.getLogger(OrderServiceImpl.class);

	@Autowired
    private Environment environment;
	
	@Autowired
    private VirtualFundRepository virtualFundRepository;
	
	@Autowired
    private CustomerCodeRepository customerCodeRepository;	
	
	@Autowired
	private OrderRepository orderRepo;

	@Autowired
	private NotificationTrackerRepository notificatonRepo;

	@Autowired
	private CategoryRepository categoryRepository;

	@Autowired
	private OrderItemRepository orderItemRepository;

	@Autowired
	private MerchantRepository merchantRepository;

	@Autowired
	private OrderRepository orderRepository;

	@Autowired
	private OrderItemModifierRepository orderItemModifierRepository;

	@Autowired
	private ItemmRepository itemmRepository;

	@Autowired
	private CategoryItemRepository categoryItemRepository;

	@Autowired
	private ItemTimingRepository itemTimingRepository;

	@Autowired
	private CategoryTimingRepository categoryTimingRepository;

	@Autowired
	private OrderDiscountRepository orderDiscountRepository;

	@Autowired
	private ItemModifierGroupRepository itemModifierGroupRepository;

	@Autowired
	private ModifiersRepository modifiersRepository;

	@Autowired
	private ModifierModifierGroupRepository modifierModifierGroupRepository;

	@Autowired
	private ItemModifiersRepository itemModifiersRepository;

	@Autowired
	private ItemTaxRepository itemTaxRepository;

	@Autowired
	private OrderItemModifierRepository itemModifierRepository;

	@Autowired
	private MerchantSubscriptionRepository merchantSubscriptionRepository;

	@Autowired
	private MerchantOrdersRepository merchantOrdersRepository;

	@Autowired
	private CustomerrRepository customerrRepository;

	@Autowired
	private PaymentModeRepository paymentModeRepository;

	@Autowired
	private OrderTypeRepository orderTypeRepository;

	@Autowired
	private TaxRateRepository taxRateRepository;

	@Autowired
	private ConvenienceFeeRepository convenienceFeeRepository;

	@Autowired
	private PickUpTimeRepository pickUpTimeRepository;

	@Autowired
	private AddressRepository addressRepository;

	@Autowired
	private ZoneRepository zoneRepository;

	@Autowired
	private VendorRepository vendorRepository;

	@Autowired
	private CardInfoRepository cardInfoRepository;

	@Autowired
	private MerchantEncryptedKeyRepository merchantEncryptedKeyRepository;

	@Autowired
	private PaymentGateWayRepository paymentGateWayRepository;

	@Autowired
	private OrderPaymentDetailRepository paymentDetailRepository;

	@Autowired
	private NotificationMethodRepository methodRepository;

	@Autowired
	private PizzaTemplateRepository pizzaTemplateRepository;

	@Autowired
	private PizzaToppingRepository pizzaToppingRepository;

	@Autowired
	private PizzaTemplateSizeRepository pizzaTemplateSizeRepository;

	@Autowired
	private OrderPizzaRepository orderPizzaRepository;

	@Autowired
	private OrderPizzaToppingsRepository orderPizzaToppingsRepository;

	@Autowired
	private MerchantConfigurationService merchantConfigurationService;

	@Autowired
	private PizzaCrustRepository pizzaCrustRepository;

	@Autowired
	private OrderPizzaCrustRepository orderPizzaCrustRepository;

	@Autowired
	private PizzaSizeRepository pizzaSizeRepository;

	@Autowired
	private MerchantService merchantService;

	@Autowired
	private OpeningClosingDayRepository openingCosingDayRepository;

	@Autowired
	private OpeningClosingTimeRepository openingClosingTimeRepository;

	@Autowired
	private PrinterRepository printerRepository;

	@Autowired
	private MailSendUtil mailSendUtil;

	@Autowired
	private OnlineNotificationStatusRepository onlineNotificationStatusRepository;
	
	@Autowired
    private OrderPrintStatusRepository  orderPrintStatusRepo;

	public static Integer custId;
	public static Integer orderId;
	public static Integer posTypeId = IConstant.POS_FOODTRONIX;;
	public static String name1;
	public static String paymentYtpe1;
	public static String orderPosId1;
	public static String subTotal1;
	public static String tax1;
	public static String orderDetails1;
	public static String email1;
	public static Double orderPrice1;
	public static String note1;
	public static String merchantName1;
	public static String merchantLogo1;
	public static String orderType1;
	public static String convenienceFeeValue1 = "0";
	public static String avgTime = "30";
	public static String deliverFee = "0";
	public static Double orderDiscount = 0.0;
	public static Double tipAmount = 0.0;
	public static String listOfCoupons = "";
	public static Integer merchantId;
	public static Integer isfutureorder=0;
	public static String fullfilledOn;
	public static boolean storeOpenstatus = false;
	
	public List<OrderR> getAllCategory(Clover clover) {
		return orderRepo.findAll();
	}

	/**
	 * Find by customerId and merchantId
	 */
	public List<OrderR> getAllOrder(Integer customerId, Integer merchantId) {
		LOGGER.info("---------------------Start :: OrderServiceImpl : getAllOrder---------------------");
		List<OrderR> orderRs = orderRepo.findByCustomerIdAndMerchantId(customerId, merchantId);
		LOGGER.info("OrderServiceImpl : getAllOrder :customerId : " + customerId);
		LOGGER.info("OrderServiceImpl : getAllOrder :merchantId : " + merchantId);
		Collections.sort(orderRs, new Comparator<OrderR>() {
			public int compare(OrderR m1, OrderR m2) {
				LOGGER.info("---------------------End :: OrderServiceImpl : getAllOrder---------------------");
				if(m2.getCreatedOn()!=null && m1.getCreatedOn()!=null)
				return m2.getCreatedOn().compareTo(m1.getCreatedOn());
				else return 0;
			}
		});
		LOGGER.info("---------------------End :: OrderServiceImpl : getAllOrder---------------------");
		return findOrderDetails(orderRs);
	}

	public List<CategoryDto> findCategoriesByMerchantId(Merchant merchant) {
		LOGGER.info("---------------------Start :: OrderServiceImpl : findCategoriesByMerchantId---------------------");
		String timeZoneCode = "America/Chicago";
		if (merchant != null && merchant.getId() != null) {
			if (merchant.getTimeZone() != null && merchant.getTimeZone().getTimeZoneCode() != null) {

				timeZoneCode = merchant.getTimeZone().getTimeZoneCode();
				LOGGER.info("OrderServiceImpl : getAllOrder :merchantId : " + merchant.getId());
				LOGGER.info("OrderServiceImpl : getAllOrder :timeZoneCode : " + timeZoneCode);
			}
			String currentDay = DateUtil.getCurrentDayForTimeZone(timeZoneCode);
			String currentTime = DateUtil.getCurrentTimeForTimeZone(timeZoneCode);
			LOGGER.info("OrderServiceImpl : getAllOrder :currentDay : " + currentDay);
			LOGGER.info("OrderServiceImpl : getAllOrder :currentTime : " + currentTime);
			List<Category> categories = categoryRepository.findByMerchantIdOrderBySortOrderAscByQuery(merchant.getId(),
					currentDay, currentTime, currentTime);
			List<CategoryDto> finalCategories = new ArrayList<CategoryDto>();
			for (Category category : categories) {
				boolean categoryTimingStatus = true;
				/*
				 * if(category.getAllowCategoryTimings()==IConstant. BOOLEAN_TRUE) {
				 * CategoryTiming categoryTiming=categoryTimingRepository.
				 * findByCategoryIdAndDayAndStartTimeAndEndTime(currentDay,
				 * category.getId(),currentTime,currentTime);
				 * 
				 * if(categoryTiming!=null ){ if(categoryTiming.isHoliday()){
				 * categoryTimingStatus=false; }else{ categoryTimingStatus=true; } }else{
				 * categoryTimingStatus=false; }}
				 */
				if (categoryTimingStatus && category.getItemStatus() != null
						&& category.getItemStatus() != IConstant.SOFT_DELETE) {
					/*
					 * List<Item> items = itemmRepository.findByCategoriesId(category.getId());
					 * List<ItemDto> itemDtos=findCategoryItems(category.getId(), merchant);; if (
					 * itemDtos!=null && itemDtos.size() >0) {
					 */
					CategoryDto categoryDto = new CategoryDto();
					categoryDto.setId(category.getId());
					categoryDto.setCategoryName(category.getName());
					categoryDto.setCategoryStatus(category.getItemStatus());
					categoryDto.setCategoryImage(category.getCategoryImage());
					finalCategories.add(categoryDto);
					/* } */
				}
			}
			LOGGER.info("---------------------End :: OrderServiceImpl : getAllOrder---------------------");
			return finalCategories;
		} else {
			LOGGER.info("---------------------End :: OrderServiceImpl : getAllOrder :No category---------------------");
			return null;
		}

	}

	public List<CategoryDto> findCategoriesByMerchantIdWithLimit(Integer merchantId, Integer page) {
		LOGGER.info(
				"---------------------Start :: OrderServiceImpl : findCategoriesByMerchantIdWithLimit---------------------");
		LOGGER.info(" === merchantId : " + merchantId + " page : " + page);
		Pageable topTen = new PageRequest(0, 5);

		Merchant merchant = merchantRepository.findById(merchantId);
		String timeZoneCode = "America/Chicago";
		if (merchant != null && merchant.getTimeZone() != null && merchant.getTimeZone().getTimeZoneCode() != null) {
			/*
			 * hourDifference=merchant.getTimeZone().getHourDifference();
			 * if(merchant.getTimeZone().getMinutDifference()!=null)
			 * minutDifference=merchant.getTimeZone().getMinutDifference();
			 */
			timeZoneCode = merchant.getTimeZone().getTimeZoneCode();
			LOGGER.info("OrderServiceImpl : findCategoriesByMerchantIdWithLimit :timeZoneCode : " + timeZoneCode);
		}
		String currentDay = DateUtil.getCurrentDayForTimeZone(timeZoneCode);
		String currentTime = DateUtil.getCurrentTimeForTimeZone(timeZoneCode);
		LOGGER.info("OrderServiceImpl : findCategoriesByMerchantIdWithLimit :currentDay : " + currentDay);
		LOGGER.info("OrderServiceImpl : findCategoriesByMerchantIdWithLimit :currentTime : " + currentTime);
		if (merchant != null) {
			// List<Category> categories =
			// categoryRepository.findByMerchantIdOrderBySortOrderAsc(merchantId);
			Page<Category> categories = categoryRepository.findByMerchantIdOrderBySortOrderAsc(merchantId, topTen);
			List<CategoryDto> finalCategories = new ArrayList<CategoryDto>();
			for (Category category : categories) {
				CategoryTiming categoryTiming = categoryTimingRepository.findByCategoryIdAndDay(category.getId(),
						currentDay);
				LOGGER.info("OrderServiceImpl : findCategoriesByMerchantIdWithLimit :categoryId : " + category.getId());
				boolean categoryTimingStatus = false;
				if (categoryTiming != null) {
					if (categoryTiming.isHoliday()) {
						categoryTimingStatus = false;
						LOGGER.info(
								"OrderServiceImpl : findCategoriesByMerchantIdWithLimit :categoryTimingStatusHoliday : "
										+ categoryTimingStatus);
					} else {
						String startTime = DateUtil.convert12hoursTo24HourseFormate(categoryTiming.getStartTime());
						String endTime = DateUtil.convert12hoursTo24HourseFormate(categoryTiming.getEndTime());
						categoryTimingStatus = OrderUtil.isTimeBetweenTwoTime(startTime + ":00", endTime + ":00",
								currentTime,environment);
						LOGGER.info("OrderServiceImpl : findCategoriesByMerchantIdWithLimit :startTime : " + startTime);
						LOGGER.info("OrderServiceImpl : findCategoriesByMerchantIdWithLimit :endTime : " + endTime);
						LOGGER.info("OrderServiceImpl : findCategoriesByMerchantIdWithLimit :categoryTimingStatus : "
								+ categoryTimingStatus);
					}
				} else {
					categoryTimingStatus = true;
					LOGGER.info(
							"OrderServiceImpl : findCategoriesByMerchantIdWithLimit :categoryTimingStatusBydefalut : "
									+ categoryTimingStatus);
				}
				if (categoryTimingStatus && category.getItemStatus() != null
						&& category.getItemStatus() != IConstant.SOFT_DELETE) {
					// List<Item> items =
					// itemmRepository.findByCategoriesId(category.getId());

					Integer startIndex = 0;
					// Integer lastIndex=0;
					List<ItemDto> itemDtos = findCategoryItems(category.getId(), merchant, startIndex);

					if (itemDtos != null && itemDtos.size() > 0) {
						CategoryDto categoryDto = new CategoryDto();
						categoryDto.setId(category.getId());
						categoryDto.setCategoryName(category.getName());
						categoryDto.setCategoryStatus(category.getItemStatus());
						finalCategories.add(categoryDto);
					}
				}
			}
			LOGGER.info(
					"-----------------End :: OrderServiceImpl : findCategoriesByMerchantIdWithLimit------------------");
			return finalCategories;
		} else {
			LOGGER.info(
					"-----------------End :: OrderServiceImpl : findCategoriesByMerchantIdWithLimit : No category------------------");
			return null;
		}

	}

	/**
	 * Find Items based in cetegoryId
	 */

	/*
	 * public List<CategoryDto> findMenuItems(List<CategoryDto> categories) {
	 * 
	 * ArrayList<CategoryDto> newlist = new ArrayList<CategoryDto>(); for
	 * (CategoryDto categoryDto : categories) { List<Item> items =
	 * itemmRepository.findByCategoriesId(categoryDto.getId()); List<ItemDto>
	 * itemDtos = new ArrayList<ItemDto>(); if (items.size() != 0 &&
	 * categoryDto.getCategoryStatus() == 0) { newlist.add(categoryDto); for (Item
	 * item : items) { if (item.getItemStatus() != null && item.getItemStatus() ==
	 * 0) { ItemDto itemDto = new ItemDto(); itemDto.setId(item.getId());
	 * itemDto.setItemName(item.getName());
	 * itemDto.setItemTax(getItemTaxPrice(item.getId()));
	 * itemDto.setPrice(item.getPrice()); itemDto.setItemPosId(item.getPosItemId());
	 * itemDto.setAllowModifierLimit(item.getAllowModifierLimit());
	 * List<ModifierGroupDto> groupDtos = getModifierGroup(item.getId(),
	 * item.getAllowModifierLimit()); itemDto.setModifierGroupDtos(groupDtos);
	 * itemDtos.add(itemDto); } } categoryDto.setItemDtos(itemDtos); } }
	 * 
	 * // categories.remove(newlist); return newlist; }
	 */

	/**
	 * Calculate tax based on item
	 * 
	 * @param itemId
	 * @return
	 */
	private double getItemTaxPrice(Integer itemId) {
		LOGGER.info("===============  OrderServiceImpl : Inside getItemTaxPrice :: Start  ============= ");
		LOGGER.info(" === itemId : " + itemId);
		double tax = 0;
		try {
			if (itemId != null) {
				List<ItemTax> itemTaxs = itemTaxRepository.findByItemId(itemId);
				if (!itemTaxs.isEmpty()) {
					for (ItemTax itemTax : itemTaxs) {
						tax = tax + itemTax.getTaxRates().getRate();
					}
				}
			}
			LOGGER.info("===============  OrderServiceImpl : Inside getItemTaxPrice :: End  ============= ");

			return tax;
		} catch (Exception e) {
			LOGGER.error("===============  OrderServiceImpl : Inside getItemTaxPrice :: Exception  ============= " + e);

			MailSendUtil.sendExceptionByMail(e,environment);
			return tax;
		}
	}

	/**
	 * Find modifier group based on itemId
	 * 
	 * @param itemId
	 * @return
	 */
	public List<ModifierGroupDto> getModifierGroup(Integer itemId, Integer allowModifierLimit, Merchant merchant) {
		LOGGER.info("===============  OrderServiceImpl : Inside getModifierGroup :: Start  ============= ");
		LOGGER.info(" === itemId : " + itemId + "  allowModifierLimit : " + allowModifierLimit + " merchant : "
				+ merchant.getId());

		List<ModifierGroupDto> groupDtos = new ArrayList<ModifierGroupDto>();
		List<ItemModifierGroup> groups = itemModifierGroupRepository.findByModifierGroupActiveAndItemIdOrderBySortOrderAsc(1,itemId);
		for (ItemModifierGroup group : groups) {
			if (group.getModifierGroup() != null && group.getModifierGroup().getActive()!=null
					&& group.getModifierGroup().getShowByDefault() != IConstant.SOFT_DELETE && group.getModifierGroup().getActive()==1) {
				if (group.getModifierGroupStatus() != null
						&& group.getModifierGroupStatus() == IConstant.BOOLEAN_TRUE) {
					ModifierGroupDto groupDto = new ModifierGroupDto();
					groupDto.setId(group.getModifierGroup().getId());

					if (allowModifierLimit != null && allowModifierLimit == IConstant.BOOLEAN_TRUE)
						groupDto.setModifiersLimit(group.getModifiersLimit());
					groupDto.setIsMaxLimit(group.getIsMaxLimit());
					groupDto.setModifierGroupName(group.getModifierGroup().getName());
					groupDto.setIsMinLimit(group.getIsMinLimit());

					List<ModifierDto> modifierDtos = getModifierByModifierGroup(group.getModifierGroup().getId(),
							itemId, merchant);
					groupDto.setModifierDtos(modifierDtos);
					groupDtos.add(groupDto);
				}
			}
		}
		LOGGER.info("===============  OrderServiceImpl : Inside getModifierGroup :: End  ============= ");

		return groupDtos;
	}

	/**
	 * Get modifiers based on modifierGroup id
	 * 
	 * @param modifierGroupId
	 * @return
	 */
	private List<ModifierDto> getModifierByModifierGroup(Integer modifierGroupId, Integer itemId, Merchant merchant) {
		LOGGER.info("===============  OrderServiceImpl : Inside getModifierByModifierGroup :: Start  ============= ");
		LOGGER.info(" === itemId : " + itemId + "  modifierGroupId : " + modifierGroupId + " merchant : "
				+ merchant.getId());

		List<ModifierDto> modifierDtos = new ArrayList<ModifierDto>();

		// List<Modifiers> modifiers =
		// modifierModifierGroupRepository.findByModifierGroupId(modifierGroupId);
		List<ItemModifiers> itemModifiers = itemModifiersRepository.findByModifierGroupIdAndItemIdAndModifiersStatusAndGrpMapping(modifierGroupId,
				itemId,1);
		for (ItemModifiers itemModifier : itemModifiers) {
			if (itemModifier.getModifierStatus() == IConstant.BOOLEAN_TRUE) {
				ModifierDto modifierDto = new ModifierDto();
				Modifiers mod = itemModifier.getModifiers();
				modifierDto.setId(mod.getId());
				modifierDto.setModifierName(mod.getName());
				modifierDto.setPrice(mod.getPrice());

				modifierDto.setModifierPosId(mod.getPosModifierId());
				if (merchant != null && merchant.getOwner() != null && merchant.getOwner().getPos() != null
						&& merchant.getOwner().getPos().getPosId() != null
						&& (merchant.getOwner().getPos().getPosId() == IConstant.NON_POS
								|| merchant.getOwner().getPos().getPosId() == IConstant.FOCUS)) {
					modifierDto.setModifierPosId(Integer.toString(mod.getId()));
				}
				modifierDtos.add(modifierDto);
			}
		}
		LOGGER.info("===============  OrderServiceImpl : Inside getModifierByModifierGroup :: End  ============= ");

		return modifierDtos;
	}

	/**
	 * Save place order information into database
	 */
	public String saveOrder(JSONObject jObject, String finalJson, Customer customer, Merchant merchant, Double discount,
			String convenienceFee, String deliveryItemPrice, String avgDeliveryTime, String orderType, String auxTax,
			String tax) {
		LOGGER.info("---------------------Start :: OrderServiceImpl : saveOrder---------------------");
		LOGGER.info(" finalJson : " + finalJson + " discount : " + discount
				+ " convenienceFee : " + convenienceFee + " deliveryItemPrice : " + deliveryItemPrice
				+ " avgDeliveryTime : " + avgDeliveryTime + " orderType : " + orderType);
		OrderR orderR = null;
		double auxTaxValue = 0;
		double taxValue = 0;
		double totalTax = 0;
		Gson gson = new Gson();
		String timeZoneCode = merchant.getTimeZone().getTimeZoneCode();
		CloverOrderVO cloverOrderVO = gson.fromJson(finalJson, CloverOrderVO.class);

		if (merchant.getOwner() != null && merchant.getOwner().getPos() != null
				&& merchant.getOwner().getPos().getPosId() != null && merchant.getOwner().getPos().getPosId() == 1) {
			LOGGER.info("OrderServiceImpl : inside saveOrder :merchant PosId()=1 ");
			orderR = OrderUtil.getObjectFromJson(jObject, customer, merchant);
			/*
			 * if (orderR!=null && discount != 0) { CloverUrlUtil.applyCoupon(discount,
			 * merchant, orderR.getOrderPosId()); }
			 */
		} else {
			orderR = new OrderR();
			orderR.setCustomer(customer);
			orderR.setMerchant(merchant);

			

			String instruction = cloverOrderVO.getOrderVO().getNote();
			orderR.setOrderNote(instruction.split(":")[1]);
			// orderR.setIsDefaults(3);
			orderR.setIsDefaults(0);

			if (auxTax != null) {
				auxTaxValue = Double.valueOf(auxTax);
				LOGGER.info("OrderServiceImpl : inside saveOrder : auxTaxValue " + auxTaxValue);
			}
			if (tax != null) {
				taxValue = Double.valueOf(tax);
				LOGGER.info("OrderServiceImpl : inside saveOrder : taxValue " + taxValue);
			}
			totalTax = auxTaxValue + taxValue;
			LOGGER.info("OrderServiceImpl : inside saveOrder : totalTax " + totalTax);
			orderR.setSalesTax(tax);
			orderR.setAuxTax(auxTax);
			orderR.setTax(String.valueOf(totalTax));

			orderR.setOrderPrice(Double.parseDouble(cloverOrderVO.getOrderVO().getTotal()) / 100);
			orderRepo.save(orderR);

			LOGGER.info("OrderServiceImpl : inside saveOrder : orderId " + orderR.getId());

			orderR.setOrderPosId(Integer.toString(orderR.getId()));
		}
		if (null != orderR) {
			// orderR.setIsDefaults(0);
//			Integer hourDifference = 0;
//			Integer minutDifference = 0;
//			if (merchant != null && merchant.getTimeZone() != null
//					&& merchant.getTimeZone().getHourDifference() != null) {
//				hourDifference = merchant.getTimeZone().getHourDifference();
//				LOGGER.info("OrderServiceImpl : inside saveOrder : hourDifference " + hourDifference);
//				if (merchant.getTimeZone().getMinutDifference() != null)
//					minutDifference = merchant.getTimeZone().getMinutDifference();
//				LOGGER.info("OrderServiceImpl : inside saveOrder : minutDifference " + minutDifference);
//			}

			Date currentDate =  (merchant != null && merchant.getTimeZone() != null
					&& merchant.getTimeZone().getTimeZoneCode() != null)
					? DateUtil.getCurrentDateForTimeZonee(merchant.getTimeZone().getTimeZoneCode())
					: new Date();
			Calendar cal = Calendar.getInstance();
			// remove next line if you're always using the current time.
			cal.setTime(currentDate);
//			cal.add(Calendar.HOUR, hourDifference);
//			cal.add(Calendar.MINUTE, minutDifference);
			currentDate = cal.getTime();
			LOGGER.info("OrderServiceImpl : inside saveOrder : Order Created date " + currentDate);
			orderR.setCreatedOn(currentDate);
			// Gson gson = new Gson();
			// CloverOrderVO cloverOrderVO = gson.fromJson(finalJson,
			// CloverOrderVO.class);
			taxValue = 0;
			auxTaxValue = 0;
			totalTax = 0;
			if (auxTax != null) {
				auxTaxValue = Double.valueOf(auxTax);
			}
			if (tax != null) {
				taxValue = Double.valueOf(tax);
			}
			totalTax = auxTaxValue + taxValue;
			orderR.setSalesTax(tax);
			orderR.setAuxTax(auxTax);
			orderR.setTax(String.valueOf(totalTax));
			orderR.setConvenienceFee(convenienceFee);
			orderR.setDeliveryFee(deliveryItemPrice);
			orderR.setOrderDiscount(discount);
			orderR.setOrderAvgTime(avgDeliveryTime);
			orderR.setOrderType(orderType);
			orderRepo.save(orderR);
			LOGGER.info("OrderServiceImpl : inside saveOrder : orderIDD " + orderR.getId());
			DecimalFormat df2 = new DecimalFormat("###.##");
			DecimalFormat df3 = new DecimalFormat("###.###");
			DecimalFormat df4 = new DecimalFormat("###.####");
			Map<String, Double> taxMap = new HashMap<String, Double>();
			for (OrderItemVO item : cloverOrderVO.getOrderItemVOs()) {

				Item item2 = null;
				if (merchant != null && merchant.getOwner() != null && merchant.getOwner().getPos() != null
						&& merchant.getOwner().getPos().getPosId() != null
						&& (merchant.getOwner().getPos().getPosId() == IConstant.NON_POS
								|| merchant.getOwner().getPos().getPosId() == IConstant.FOCUS)) {
					LOGGER.info("OrderServiceImpl : inside saveOrder : merchant getPosId() " + IConstant.FOCUS);
					Integer itemId = 0;
					try {
						if (item.getItem().getId() != null && !item.getItem().getId().isEmpty()) {
							itemId = Integer.parseInt(item.getItem().getId());
							LOGGER.info("OrderServiceImpl : inside saveOrder : itemId " + itemId);
						}
					} catch (Exception e) {
						LOGGER.info("OrderServiceImpl : inside saveOrder : Exception " + e);
						MailSendUtil.sendExceptionByMail(e,environment);
					}
					item2 = itemmRepository.findByIdAndMerchantId(itemId, merchant.getId());

					if (item2 != null && item2.getName() != null) {
						LOGGER.info("OrderServiceImpl : inside saveOrder : itemName " + item2.getName());
					}

				} else {
					item2 = itemmRepository.findByPosItemIdAndMerchantId(item.getItem().getId(), merchant.getId());

				}
				if (item2 != null) {
					OrderItem orderItem = new OrderItem();
					orderItem.setItem(item2);
					orderItem.setOrder(orderR);
					orderItem.setQuantity(Integer.parseInt(item.getUnitQty()));

					List<OrderItemModifier> itemModifiers = new ArrayList<OrderItemModifier>();
					double itemModiferPrice = 0.0;
					for (Modifications modifications : item.getModifications()) {
						OrderItemModifier itemModifier = new OrderItemModifier();

						if (merchant != null && merchant.getOwner() != null && merchant.getOwner().getPos() != null
								&& merchant.getOwner().getPos().getPosId() != null
								&& (merchant.getOwner().getPos().getPosId() == IConstant.NON_POS
										|| merchant.getOwner().getPos().getPosId() == IConstant.FOCUS)) {
							Integer modifierId = 0;
							try {
								modifierId = Integer.parseInt(modifications.getModifier().getId());
								LOGGER.info("OrderServiceImpl : inside saveOrder : modifierId " + modifierId);
							} catch (Exception e) {

							}
							Modifiers modifier = modifiersRepository.findByIdAndMerchantId(modifierId,
									merchant.getId());
							if (modifier != null) {
								itemModiferPrice = itemModiferPrice + modifier.getPrice();
								itemModifier.setModifiers(modifier);
								LOGGER.info("OrderServiceImpl : inside saveOrder : modifierName " + modifier.getName());
							}
							
						} else {
							Modifiers modifier = modifiersRepository.findByPosModifierIdAndMerchantId(
									modifications.getModifier().getId(), merchant.getId());
							if (modifier != null) {
								itemModiferPrice = itemModiferPrice + modifier.getPrice();
								itemModifier.setModifiers(modifier);
							}
						}

						itemModifier.setOrderItem(orderItem);
						itemModifier.setQuantity(Integer.parseInt(item.getUnitQty()));
						itemModifiers.add(itemModifier);
					}

					if (item2 != null && item2.getAuxTaxAble() != null && item2.getAuxTaxAble()) {
						OrderType orderTypes = findByMerchantIdAndLabel(merchant.getId(), orderType);
						if (orderTypes != null && orderTypes.getAuxTax() != null && orderTypes.getAuxTax() != 0) {
							double itemPrice = ((item2.getPrice() + itemModiferPrice))
									* Integer.parseInt(item.getUnitQty());
							double totalItemAuxTax = ((item2.getPrice() + itemModiferPrice) / 100)
									* orderTypes.getAuxTax();
							orderItem.setAuxTax(String.valueOf(df3.format(totalItemAuxTax)));
							if (taxMap.containsKey("auxTaxValue")) {
								Double total = taxMap.get("auxTaxValue") + (itemPrice);
								LOGGER.info("OrderServiceImpl : inside saveOrder : auxTaxValue+itemPrice " + total);
								taxMap.put("auxTaxValue", total);
							} else {
								LOGGER.info("OrderServiceImpl : inside saveOrder : itemPrice " + itemPrice);
								taxMap.put("auxTaxValue", itemPrice);
							}
						}
					}

					if (item2 != null && item2.getTaxAble()) {
						OrderType orderTypes = findByMerchantIdAndLabel(merchant.getId(), orderType);
						if (orderTypes != null && orderTypes.getSalesTax() != null && orderTypes.getSalesTax() != 0) {
							double itemPrice = ((item2.getPrice() + itemModiferPrice))
									* Integer.parseInt(item.getUnitQty());
							double totalItemTax = ((item2.getPrice() + itemModiferPrice) / 100)
									* orderTypes.getSalesTax();
							orderItem.setSalesTax(String.valueOf(df4.format(totalItemTax)));
							if (taxMap.containsKey("saleTaxValue")) {
								Double total = taxMap.get("saleTaxValue") + itemPrice;
								taxMap.put("saleTaxValue", total);
								LOGGER.info(
										"OrderServiceImpl : inside saveOrder : item2 saleTaxValue+itemPrice " + total);
							} else {
								taxMap.put("saleTaxValue", itemPrice);
								LOGGER.info("OrderServiceImpl : inside saveOrder :item2 itemPrice " + itemPrice);
							}
						}
					}

					double orderItemPrice = item2.getPrice() + itemModiferPrice;
					orderItem.setOrderItemPrice(orderItemPrice);
					orderItemRepository.save(orderItem);
					itemModifierRepository.save(itemModifiers);

					LOGGER.info("OrderServiceImpl : inside saveOrder : orderItemId " + orderItem.getId());

				}
			}

			if (taxMap.containsKey("saleTaxValue")) {
				orderR.setTaxableTotal(taxMap.get("saleTaxValue").toString());
			}
			if (taxMap.containsKey("auxTaxValue")) {
				orderR.setAuxTaxableTotal(taxMap.get("auxTaxValue").toString());
			}
			orderRepo.save(orderR);

			if (merchant.getOwner() != null && merchant.getOwner().getPos() != null
					&& merchant.getOwner().getPos().getPosId() != null
					&& merchant.getOwner().getPos().getPosId() == IConstant.POS_CLOVER) {

				if (orderR.getOrderPosId() != null) {

					Integer merchantSubscriptionId = merchantSubscriptionRepository.findByMerchantId(merchant.getId());
					MerchantOrders merchantOrders = merchantOrdersRepository
							.findByMerchantSubscriptionId(merchantSubscriptionId);
					// call metered API for increase order count
					if (merchantOrders != null && merchantOrders.getMerchantSubscription() != null
							&& merchantOrders.getMerchantSubscription().getSubscription() != null) {
						long difference = DateUtil.findDifferenceBetweenTwoDates(merchantOrders.getEndDate(),
								DateUtil.getCurrentDateForTimeZonee(timeZoneCode));

						if ((difference > 0) && (merchantOrders.getOrderCount() <= merchantOrders
								.getMerchantSubscription().getSubscription().getOrderLimit())) {
							LOGGER.info("-------Metered if block----");
							LOGGER.info("OrderServiceImpl : inside saveOrder : Metered if block ");
						} else {
							LOGGER.info("-------Metered else block----");
							LOGGER.info("OrderServiceImpl : inside saveOrder :Metered else block ");
							merchant.setSubscription(merchantOrders.getMerchantSubscription().getSubscription());
							String response = CloverUrlUtil.addMteredPrice(merchant,environment);
							LOGGER.info("-------Metered API response-----" + response);
							LOGGER.info("OrderServiceImpl : inside saveOrder :Metered API response " + response);
						}
						merchantOrders.setOrderCount(merchantOrders.getOrderCount() + 1);
						merchantOrdersRepository.save(merchantOrders);
					} else {
						MailSendUtil.sendErrorMailToAdmin(
								"Merchant '" + merchant.getName() + "'doesn't have any Subscription",environment);
						LOGGER.error("Merchant '" + merchant.getName() + "'doesn't have any Subscription");

					}
				}
			}
			LOGGER.info("----------------End :: OrderServiceImpl : saveOrder orderId{}----------------"
					+ orderR.getOrderPosId());
			return orderR.getOrderPosId();
		} else {
			LOGGER.info("----------------End :: OrderServiceImpl : saveOrder returns null ------------ ");
			return null;
		}
	}

	/**
	 * Update order status after payment
	 */
	@SuppressWarnings("deprecation")
	public boolean updateOrderStatus(String orderPosId, String result, Integer customerId, String paymentYtpe,
			String subTotal, String tax, String name, String email, String merchantName, String merchantLogo,
			String orderType, double tip, String orderPrice, String futureOrderType, String futureDate,
			String futureTime, Merchant merchant, String listOfALLDiscounts, String ccType, Boolean saveCard,
			String auxTax) {

		LOGGER.info("----------------Start :: OrderServiceImpl : updateOrderStatust------------------");
		LOGGER.info(" ===result " + result + " paymentype : " + paymentYtpe + " subTotal : " + subTotal + " name : "
				+ name + "  email : " + email + " merchantName : " + merchantName + " tax : " + tax + " orderType : "
				+ orderType + " tip : " + tip + " orderPrice : " + orderPrice + "  futureOrderType : " + futureOrderType
				+ " futureDate : " + futureDate + " futureTime : " + futureTime + " listOfALLDiscounts : "
				+ listOfALLDiscounts + " ccType : " + ccType + " saveCard : " + saveCard + " auxTax : " + auxTax
				+ " orderPosId : " + orderPosId + " customerId : " + customerId);
		try {
			IConstant.flag = false;
			OrderR orderR = null;
			Vendor vendor = null;
			String orderDetails = "";
			double conveniencePercent = 0;
			Double conveniencePercentAmount = 0.0;
			Map<String, String> notification = new HashMap<String, String>();
			String orderStatus = "orderNow";
			Map<String, Map<String, String>> notf = new HashMap<String, Map<String, String>>();
			List<Map<String, String>> productList = new ArrayList<Map<String, String>>();
			DateUtil date=new DateUtil();

			Map<String, String> order = new HashMap<String, String>();
			if (customerId != null && orderPosId != null) {
				orderR = orderRepo.findByCustomerIdAndOrderPosId(customerId, orderPosId);

				LOGGER.info("OrderServiceImpl :: updateOrderStatus : customerId " + customerId);
				LOGGER.info("OrderServiceImpl :: updateOrderStatus : orderPosId " + orderPosId);

				if (orderR != null) {
					orderR.setTipAmount(tip);

					orderR.setIsDefaults(0);
					orderR.setSubTotal(subTotal);
					orderR.setTax(tax);
					orderR.setAuxTax(auxTax);
					orderR.setPaymentMethod(paymentYtpe);
					orderR.setOrderType(orderType);
					merchant = orderR.getMerchant();

					vendor = vendorRepository.findOne(merchant.getOwner().getId());

					LOGGER.info("OrderServiceImpl :: updateOrderStatus : vendorId " + vendor.getId());

					ConvenienceFee convenienceFee = null;
					PickUpTime pickUpTime = null;
					Zone zone = null;
					//String convenienceFeeValue = "0";
					String pickUpTimeValue = "0";
					if (null != merchant) {
						List<ConvenienceFee> convenienceFees = convenienceFeeRepository
								.findByMerchantId(merchant.getId());
						if (convenienceFees != null && convenienceFees.size() > 0) {
							int index = convenienceFees.size() - 1;
							convenienceFee = convenienceFees.get(index);
						}
						if (null != convenienceFee) {
							double ConvenienceFeePercent =  (convenienceFee.getConvenienceFeePercent()!=null) ? convenienceFee.getConvenienceFeePercent() : 0;
					         conveniencePercent = (new Double(orderR.getSubTotal())*(ConvenienceFeePercent/100.0));
					         conveniencePercentAmount = new Double(convenienceFee.getConvenienceFee()) + conveniencePercent;
					         conveniencePercentAmount = Math.round(conveniencePercentAmount * 100.0) / 100.0;
						}
// Note : The following code has been written in savePizzaOrder method to get the required details of order if any exception arises in the further flow of code 
//						if ("pickup".equals(orderType.toLowerCase())) {
//							pickUpTime = pickUpTimeRepository.findByMerchantId(merchant.getId());
//							if (pickUpTime != null) {
//								try {
//									Integer.parseInt(pickUpTime.getPickUpTime());
//									avgTime = pickUpTime.getPickUpTime();
//								} catch (NumberFormatException e) {
//									avgTime = "45";
//									LOGGER.error("OrderServiceImpl :: updateOrderStatus : Exception " + e);
//								}
//
//							}
//						} else {
//							if (orderR.getZoneId() != null) {
//								zone = zoneRepository.findById(orderR.getZoneId());
//								LOGGER.info("OrderServiceImpl :: updateOrderStatus : zoneId " + orderR.getZoneId());
//							}
//							try {
//								if (zone != null && zone.getAvgDeliveryTime() != null) {
//									Integer.parseInt(zone.getAvgDeliveryTime());
//									avgTime = zone.getAvgDeliveryTime();
//								}
//							} catch (NumberFormatException e) {
//								avgTime = "45";
//								LOGGER.error("OrderServiceImpl :: updateOrderStatus : Exception " + e);
//							}
//
//						}
					}

					notification.put("appEvent", "notification");

					order.put("total", Double.toString(orderR.getOrderPrice()));
					order.put("tax", tax);

					List<Map<String, String>> extraList = null;
					Map<String, String> poduct = null;
					Map<String, String> exctra = null;

					List<OrderItem> orderItems = orderItemRepository.findByOrderId(orderR.getId());
					for (OrderItem orderItem : orderItems) {
						poduct = new HashMap<String, String>();
						if (null != orderItem) {
							if (null != orderItem.getItem()) {

								String items = "<tr style='font-weight:600;text-transform:capitalize;font-family:arial'><td width='200px;'>"
										+ orderItem.getItem().getName()
										+ "</td><td width='100px;' style='text-align:center'>" + orderItem.getQuantity()
										+ "</td><td width='100px;' style='text-align:center'>" + "$"
										+ String.format("%.2f", orderItem.getItem().getPrice()) + "</td></tr>";
								orderDetails = orderDetails + "" + "<b>" + items + "</b>";

								poduct.put("product_id", orderItem.getItem().getPosItemId());
								poduct.put("name", orderItem.getItem().getName());
								poduct.put("price", Double.toString(orderItem.getItem().getPrice()));
								poduct.put("qty", Integer.toString(orderItem.getQuantity()));

								extraList = new ArrayList<Map<String, String>>();
								List<OrderItemModifier> itemModifiers = itemModifierRepository
										.findByOrderItemId(orderItem.getId());
								String itemModifierName = "";
								if (itemModifiers.size() > 0) {
									for (OrderItemModifier itemModifier : itemModifiers) {
										exctra = new HashMap<String, String>();

										if (itemModifier != null && itemModifier.getModifiers() != null
												&& itemModifier.getModifiers().getName() != null) {
											itemModifierName = itemModifier.getModifiers().getName();
										}

										String modifiers = "<tr style='text-transform:capitalize;font-family:arial;margin-top:5px;font-size:12px'><td width='200px;'>"
												+ itemModifierName
												+ "</td><td width='100px;' style='text-align:center'>"
												+ itemModifier.getQuantity()
												+ " </td><td width='100px;' style='text-align:center'> " + "$"
												+ String.format("%.2f", itemModifier.getModifiers().getPrice())
												+ "</td></tr>";
										orderDetails = orderDetails + "" + modifiers;
										exctra.put("id", itemModifier.getModifiers().getPosModifierId());
										exctra.put("price", Double.toString(itemModifier.getModifiers().getPrice()));
										exctra.put("name", itemModifier.getModifiers().getName());
										exctra.put("qty", Integer.toString(orderItem.getQuantity()));
										extraList.add(exctra);
									}
								}

								Gson gson = new Gson();
								if (extraList.size() > 0) {
									String extraJson = gson.toJson(extraList);
									poduct.put("extras", extraJson);
									productList.add(poduct);
								} else {
									productList.add(poduct);
								}
							}
						}
					}

					/*
					 * -------------------------------------------------------------For Pizza
					 * View-------------------------------------------------------------------------
					 * ---------
					 */
					LOGGER.info("OrderServiceImpl :: updateOrderStatus : orderR.getId : " + orderR.getId());

					List<OrderPizza> orderPizzas = orderPizzaRepository.findByOrderId(orderR.getId());
					if (!orderPizzas.isEmpty() && orderPizzas != null) {
						for (OrderPizza orderPizza : orderPizzas) {
							poduct = new HashMap<String, String>();
							if (orderPizza != null && orderPizza.getPizzaTemplate() != null) {

								Item item = new Item();
								if (orderPizza != null && orderPizza.getPizzaTemplate() != null
										&& orderPizza.getPizzaTemplate().getDescription() != null
										&& orderPizza.getPizzaSize() != null
										&& orderPizza.getPizzaSize().getDescription() != null) {
									item.setName(orderPizza.getPizzaTemplate().getDescription() + "("
											+ orderPizza.getPizzaSize().getDescription() + ")");

									String pizza = "<tr style='font-weight:600;text-transform:capitalize;font-family:arial'><td width='200px;'>"
											+ orderPizza.getPizzaTemplate().getDescription() + "("
											+ orderPizza.getPizzaSize().getDescription() + ")"
											+ "</td><td width='100px;' style='text-align:center'>"
											+ orderPizza.getQuantity()
											+ "</td><td width='100px;' style='text-align:center'>" + "$"
											+ String.format("%.2f", orderPizza.getPrice()) + "</td></tr>";
									orderDetails = orderDetails + "" + "<b>" + pizza + "</b>";

									poduct.put("product_id", orderPizza.getPizzaTemplate().getPosPizzaTemplateId());
									poduct.put("name", orderPizza.getPizzaTemplate().getDescription() + "("
											+ orderPizza.getPizzaSize().getDescription() + ")");
									poduct.put("price", Double.toString(orderPizza.getPrice()));
									poduct.put("qty", Integer.toString(orderPizza.getQuantity()));

								}
								item.setPrice(orderPizza.getPrice());
								List<OrderPizzaToppings> orderPizzaToppings = orderPizzaToppingsRepository
										.findByOrderPizzaId(orderPizza.getId());
								extraList = new ArrayList<Map<String, String>>();
								if (!orderPizzaToppings.isEmpty() && orderPizzaToppings != null) {
									for (OrderPizzaToppings pizzaToppings : orderPizzaToppings) {
										exctra = new HashMap<String, String>();
										Modifiers itemModifier = new Modifiers();
										if (pizzaToppings != null && pizzaToppings.getPizzaTopping() != null
												&& pizzaToppings.getPizzaTopping().getDescription() != null) {
											itemModifier.setName(pizzaToppings.getPizzaTopping().getDescription());
											itemModifier.setPrice(pizzaToppings.getPrice());

											String modifiers = "<tr style='text-transform:capitalize;font-family:arial;margin-top:5px;font-size:12px'><td width='200px;'>"
													+ pizzaToppings.getPizzaTopping().getDescription();

											if (pizzaToppings.getSide1() != null && pizzaToppings.getSide1()) {
												modifiers += " (First Half)";
											} else if (pizzaToppings.getSide1() != null && pizzaToppings.getSide2()) {
												modifiers += " (Second Half)";
											} else {
												modifiers += " (Full)";
											}
											modifiers += "</td><td width='100px;' style='text-align:center'>"
													+ orderPizza.getQuantity()
													+ " </td><td width='100px;' style='text-align:center'> " + "$"
													+ String.format("%.2f", pizzaToppings.getPrice()) + "</td></tr>";

											orderDetails = orderDetails + "" + modifiers;

											exctra.put("id", pizzaToppings.getPizzaTopping().getPosPizzaToppingId());
											exctra.put("price", Double.toString(pizzaToppings.getPrice()));
											exctra.put("name", pizzaToppings.getPizzaTopping().getDescription());
											exctra.put("qty", orderPizza.getQuantity().toString());
											extraList.add(exctra);
										}
									}
								}

								OrderPizzaCrust orderPizzaCrust = orderPizzaCrustRepository
										.findByOrderPizzaId(orderPizza.getId());
								if (orderPizzaCrust != null && orderPizzaCrust.getPizzacrust() != null) {
									exctra = new HashMap<String, String>();
									String crust = "<tr style='text-transform:capitalize;font-family:arial;margin-top:5px;font-size:12px'><td width='200px;'>"
											+ orderPizzaCrust.getPizzacrust().getDescription();

									crust += "</td><td width='100px;' style='text-align:center'>"
											+ orderPizza.getQuantity()
											+ " </td><td width='100px;' style='text-align:center'> " + "$"
											+ String.format("%.2f", orderPizzaCrust.getPrice()) + "</td></tr>";
									orderDetails = orderDetails + "" + crust;

									exctra.put("id", orderPizzaCrust.getPizzacrust().getId().toString());
									exctra.put("price", Double.toString(orderPizzaCrust.getPrice()));
									exctra.put("name", orderPizzaCrust.getPizzacrust().getDescription());
									exctra.put("qty", orderPizza.getQuantity().toString());
								}
							}
							Gson gson = new Gson();
							if (extraList.size() > 0) {
								String extraJson = gson.toJson(extraList);
								poduct.put("extras", extraJson);
								productList.add(poduct);
							} else {
								productList.add(poduct);
							}
						}
					}

					/*
					 * -----------------------------------------------------------------------------
					 * -------------------------
					 * ---------------------------------------------------------
					 */

					/*
					 * Gson gson = new Gson(); String extraJson = gson.toJson(extraList);
					 * poduct.put("extras", extraJson); productList.add(poduct);
					 */

					orderDetails = "<table width='300px;'><tbody>" + orderDetails + "</table></tbody>";
					if (orderPrice != null && !orderPrice.isEmpty()) {
						double orderTotalPrice = Math.round(Double.valueOf(orderPrice) * 100.0) / 100.0;

						orderTotalPrice = orderTotalPrice + tip;
						orderR.setOrderPrice(orderTotalPrice);
					}
					double auxTaxValue = 0;
					double taxValue = 0;
					double totalTax = 0;
					if (auxTax != null) {
						auxTaxValue = Double.valueOf(auxTax);
					}
					if (tax != null) {
						taxValue = Double.valueOf(tax);
					}
					totalTax = auxTaxValue + taxValue;
					LOGGER.error("OrderServiceImpl :: updateOrderStatus : totalTax : " + auxTaxValue + taxValue);
					orderR.setSalesTax(tax);
					orderR.setAuxTax(auxTax);
					orderR.setTax(String.valueOf(totalTax));

					orderRepo.save(orderR);

					LOGGER.error("OrderServiceImpl :: updateOrderStatus : orderId " + orderR.getId());

					custId = customerId;
					orderId = orderR.getId();
					convenienceFeeValue1 = conveniencePercentAmount.toString();
					name1 = name;
					paymentYtpe1 = paymentYtpe;
					orderPosId1 = orderPosId;
					subTotal1 = subTotal;
					tax1 = tax;
					orderDetails1 = orderDetails;
					email1 = email;
					orderPrice1 = orderR.getOrderPrice();
					note1 = orderR.getOrderNote();
					merchantName1 = merchantName;
					merchantLogo1 = merchantLogo;
					orderType1 = orderType;
					tipAmount = orderR.getTipAmount();
					merchantId = merchant.getId();
					isfutureorder = orderR.getIsFutureOrder();
					if(isfutureorder==null)
						isfutureorder=0;

					if (orderR.getDeliveryFee() != null && !orderR.getDeliveryFee().isEmpty())
						deliverFee = orderR.getDeliveryFee();
					if (orderR.getOrderDiscount() != null)
						orderDiscount = orderR.getOrderDiscount();
					
					DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm a");  
					if(orderR.getFulfilled_on() != null) {
					   fullfilledOn = dateFormat.format(orderR.getFulfilled_on()); 
					}
				}
			} else {
				return false;
			}
			String timeZoneCode = (merchant != null && merchant.getTimeZone() != null
					&& merchant.getTimeZone().getTimeZoneCode() != null) ? merchant.getTimeZone().getTimeZoneCode()
							: "America/Chicago";
					String toDay = DateUtil.getCurrentDayForTimeZonee(timeZoneCode);
					String openingTime = null;
					String closinggTime = null;
					LOGGER.error("OrderServiceImpl :: updateOrderStatus :  toDay : " + toDay);
					OpeningClosingDay openingClosingDay = openingCosingDayRepository.findByDayAndMerchantId(toDay,
							merchant.getId());
					List<OpeningClosingTime> openingClosingTimes = openingClosingTimeRepository
							.findByOpeningClosingDayId(openingClosingDay.getId());
					if (openingClosingTimes != null && !openingClosingTimes.isEmpty()) {

						openingTime = openingClosingTimes.get(0).getStartTime();
						closinggTime = openingClosingTimes.get(0).getEndTime();
						LOGGER.error("OrderServiceImpl :: updateOrderStatus :  openingTime : " + openingTime + " closinggTime "
								+ closinggTime);
					}
			if(openingTime!=null && closinggTime!=null && timeZoneCode!=null)
			storeOpenstatus = date.checkCurrentTimeExistBetweenTwoTime(openingTime, closinggTime,timeZoneCode);
			else storeOpenstatus=false;
			LOGGER.error("OrderServiceImpl :: updateOrderStatus : timeZoneCode " + timeZoneCode);
					
			if (result.contains("result") && !result.contains("DECLINED")) {
				JSONObject jObject = new JSONObject(result);
				JSONArray jsonitemForDiscountArray = null;
				if (listOfALLDiscounts != null && !listOfALLDiscounts.equals("")) {
					jsonitemForDiscountArray = new JSONArray(listOfALLDiscounts);
				}

				if (jObject.toString().contains("result")) {
					final String orderAvgTime = avgTime;
					new Thread(new Runnable() {

						public void run() {
							try {
								mailSendUtil.placeOrderMail(name1, paymentYtpe1, orderPosId1, subTotal1, tax1,
										orderDetails1, email1, orderPrice1, note1, merchantName1, merchantLogo1,
										orderType1, convenienceFeeValue1, orderAvgTime, deliverFee, orderDiscount,
										tipAmount, custId, orderId, posTypeId, listOfCoupons, merchantId,
										isfutureorder,fullfilledOn,storeOpenstatus);
								LOGGER.info(" Order mail thread over");
							} catch (Exception e) {
								LOGGER.error(
										"===============  OrderServiceImpl : Inside updateOrderStatus :: Exception  ============= "
												+ e);

								System.out.println(" Invalid Emailid");
							}

						}
					}).start();
					String status = jObject.getString("result");
					String paymentPOSID = null;
					if (merchant.getOwner() != null && merchant.getOwner().getPos() != null
							&& merchant.getOwner().getPos().getPosId() != null
							&& merchant.getOwner().getPos().getPosId() == IConstant.POS_CLOVER) {

						if (status.equals("APPROVED")) {
							if (jObject.toString().contains("paymentId")) {
								paymentPOSID = jObject.getString("paymentId");
							}
						} else {
							if (jObject.toString().contains("id")) {
								paymentPOSID = jObject.getString("id");
							}
						}

					}
					LOGGER.error("OrderServiceImpl :: updateOrderStatus : payment json " + jObject);
					LOGGER.info("payment json-----" + jObject);
					if (orderR != null) {
						if ("APPROVED".equals(status) || "SUCCESS".equals(status) && orderR != null) {
							if (orderR != null && paymentPOSID != null) {
								orderR.setPosPaymentId(paymentPOSID);
							}

							if (orderR != null && avgTime != null && !avgTime.isEmpty()) {
								orderR.setOrderAvgTime(avgTime);
							}
//							Integer hourDifference = 0;
//							Integer minutDifference = 0;
//							if (merchant != null && merchant.getTimeZone() != null
//									&& merchant.getTimeZone().getHourDifference() != null) {
//								hourDifference = merchant.getTimeZone().getHourDifference();
//								if (merchant.getTimeZone().getMinutDifference() != null)
//									minutDifference = merchant.getTimeZone().getMinutDifference();
//							}
							
							TimeZone.setDefault(TimeZone.getTimeZone(timeZoneCode));

							Date currentDate =DateUtil.getCurrentDateForTimeZonee(timeZoneCode);
							Date fullFiledfDate=DateUtil.getCurrentDateForTimeZonee(timeZoneCode);
							if(currentDate==null)
								currentDate=new Date();

							if (orderR != null) {
								orderR.setCreatedOn(currentDate);
								LOGGER.error(
										"OrderServiceImpl :: updateOrderStatus : OrderCreated Date " + currentDate);
							}

							
							if (("on".equals(futureOrderType)) && (!"select".equals(futureDate))) {

								orderR.setIsFutureOrder(IConstant.BOOLEAN_TRUE);
								LOGGER.error(
										"OrderServiceImpl :: updateOrderStatus : IsFutureOrder Date " + IConstant.BOOLEAN_TRUE);

								Date futureDateTime = DateUtil.futureDateAndTime(futureDate, futureTime);
								if (orderR != null) {
									orderR.setFulfilled_on(futureDateTime);
									LOGGER.error("OrderServiceImpl :: upOrderStatus : Fulfilled_on  " + futureTime);
								}
							} else {
								orderR.setIsFutureOrder(IConstant.BOOLEAN_FALSE);
								LOGGER.error("OrderServiceImpl :: upOrderStatus : IsFutureOrder  " + IConstant.BOOLEAN_FALSE);

								Date fulfilled_on = DateUtil.getCurrentDateForTimeZonee(timeZoneCode);
								fulfilled_on=DateUtil.addOrderAvgTimeIntoCurrentTime(timeZoneCode,fullFiledfDate,Integer.valueOf(orderR.getOrderAvgTime()));
								orderR.setFulfilled_on(fulfilled_on);
								LOGGER.error("OrderServiceImpl :: updateOrderStatus : Fulfilled_on Date " + fulfilled_on);

							}
							if (orderR != null) {
								LOGGER.error("OrderServiceImpl :: updateOrderStatus : IsComplete : " + 1);
								orderR.setIsComplete(1);
							}
							if (orderR != null) {
								final OrderR savedOrder = orderRepo.save(orderR);

								LOGGER.info("OrderServiceImpl :: updateOrderStatus : orderId " + orderR.getId());

								if (!environment.getProperty("FOODKONNEKT_APP_TYPE").equals("Prod")) {
									final Merchant merchantForThread = merchant;

									new Thread(new Runnable() {

										public void run() {
											if (merchantForThread != null
													&& merchantForThread.getActiveCustomerFeedback() != null
													&& merchantForThread.getActiveCustomerFeedback() == 1) {

												DateFormat dateFormat = new SimpleDateFormat(IConstant.YYYYMMDD);
												DateFormat dateFormat1 = new SimpleDateFormat(IConstant.YYYYMMDDHHMMSS);
												java.util.Date date = new java.util.Date();

												String date1 = dateFormat.format(date);
												LOGGER.error(
														"OrderServiceImpl :: updateOrderStatus : date1 : " + date1);
												LOGGER.info("date1----" + date1);
												String s1 = date1 + IConstant.STARTDATE;
												String s2 = date1 + IConstant.ENDDATE;

												java.util.Date startDate;
												try {
													startDate = dateFormat1.parse(s1);
													Date endDate = dateFormat1.parse(s2);
													Integer customerId = savedOrder.getCustomer().getId();
													List<OrderR> orderList = orderRepo
															.findByStartDateAndEndDateAndCustomerId(startDate, endDate,
																	customerId);
													LOGGER.error(
															"OrderServiceImpl :: updateOrderStatus : order list --> : "
																	+ orderList.size());
													LOGGER.info("order list -->" + orderList.size());
//													MailSendUtil.sendOrderReceiptAndFeedbackEmail(savedOrder,
//															orderList,environment);
												} catch (Exception e) {
													MailSendUtil.sendExceptionByMail(e,environment);
												}
											} else {
												if (merchantForThread != null)
													MailSendUtil.webhookMail("Kritiq Mail faied",
															merchantForThread.getActiveCustomerFeedback() + " ",environment);
												else
													MailSendUtil.webhookMail("Kritiq Mail faied",
															"merchant is null for thread ",environment);
											}

										}
									}).start();

								}
							}

							if (jsonitemForDiscountArray != null) {
								List<Koupons> koupons = new ArrayList<Koupons>();
								Date date1 = orderR.getCreatedOn();
								for (int i = 0; i < jsonitemForDiscountArray.length(); i++) {
									Koupons koupon = new Koupons();
									OrderDiscount orderDiscount1 = new OrderDiscount();
									JSONObject object = jsonitemForDiscountArray.getJSONObject(i);
									orderDiscount1.setOrder(orderR);
									orderDiscount1.setCouponDate(date1.toString());
									orderDiscount1.setCouponCode(object.getString("couponUID"));
									orderDiscount1.setDiscount(object.getDouble("discount"));
									orderDiscount1.setInvenotryType(object.getString("inventoryLevel"));
									if (orderR.getCustomer() != null)
										orderDiscount1.setCustomer(orderR.getCustomer());
									orderDiscountRepository.save(orderDiscount1);
									koupon.setKouponCode(object.getString("couponUID"));
									koupon.setDiscount(object.getDouble("discount"));
									koupons.add(koupon);
								}
								// Koupons koupons = new Koupons();

								CouponRedeemedDto couponRedeemedDto = new CouponRedeemedDto();
								couponRedeemedDto.setAppliedDate(date1.toString());
								couponRedeemedDto.setKoupons(koupons);
								couponRedeemedDto.setMerchantUId(merchant.getMerchantUid());
								couponRedeemedDto.setOrderId(orderR.getId().toString());
								couponRedeemedDto.setVendorUId(vendor.getVendorUid());
								couponRedeemedDto.setCustomerContactNo(orderR.getCustomer().getPhoneNumber());
								try {
									couponRedeemedDto.setAmountSpent(Double.parseDouble(orderR.getSubTotal()));
									if (orderR.getCustomer().getCustomerUid() != null)
										couponRedeemedDto.setCustomerUId(orderR.getCustomer().getCustomerUid());
								} catch (Exception e) {
									LOGGER.error("error: " + e.getMessage());
									LOGGER.info("OrderServiceImpl :: updateOrderStatus : Exception : " + e);
								}
								String url = environment.getProperty("KOUPONS_BASE_URL") + "couponRedeemed";
								Gson gson = new Gson();
								String couponRedeemedJson = gson.toJson(couponRedeemedDto);
								LOGGER.error("OrderServiceImpl :: updateOrderStatus : couponRedeemedJson --> : "
										+ couponRedeemedJson);
								LOGGER.info("couponRedeemedJson::::-" + couponRedeemedJson);
								String response = OrderUtil.postCouponData(url, couponRedeemedJson);
								LOGGER.info("couponRedeemedJson RESPONSE::::-" + response);
								LOGGER.error(
										"OrderServiceImpl :: updateOrderStatus : couponRedeemedJson RESPONSE::::- --> : "
												+ response);
							}

							Gson gson = new Gson();
							String productJson = gson.toJson(productList);
							LOGGER.error("OrderServiceImpl :: updateOrderStatus :  notificationJson : " + productJson);
							LOGGER.info("notificationJson=" + productJson);

							order.put("productItems", productJson);
							// order.put("productItems", productList.toString());
							String orderJson = gson.toJson(order);
							notification.put("payload", orderPosId + "@#You got a new order(" + orderPosId + ") at "
									+ orderR.getCreatedOn() + "@# $" + orderR.getOrderPrice() + "@# " + orderType + "@#"
									+ orderJson + "@#" + orderR.getMerchant().getPosMerchantId() + "@#"
									+ orderR.getOrderNote() + " @#" + paymentPOSID + "@#"
									+ ((orderR.getIsFutureOrder() != null && orderR.getIsFutureOrder() == 1) ? true
											: false)
									+ "@#" + orderR.getFulfilled_on() + " @#" + orderR.getPaymentMethod());

							// +"@#"+tip add this to notification payload if we add
							// tip amount to show on popup on
							// notification app side
							notf.put("notification", notification);
							String notificationJson = gson.toJson(notf);
							notificationJson = notificationJson.trim().replaceAll("\\\\+\"", "'").replace("'[", "[")
									.replace("]'", "]");
							// notificationJson=notificationJson.trim().replace("'[",
							// "[").replace("]'", "]");
							// notificationJson=notificationJson.trim().replace("]'",
							// "]");
							LOGGER.info(notificationJson);

							LOGGER.info("OrderServiceImpl :: updateOrderStatus : nofiticationJson " + notificationJson);

							/*
							 * if (("on".equals(futureOrderType) || futureOrderType.equals("") ||
							 * futureOrderType.isEmpty()) && ("select".equals(futureDate))) {
							 */
							if (merchant != null && merchant.getOwner() != null && merchant.getOwner().getPos() != null
									&& merchant.getOwner().getPos().getPosId() != null
									&& merchant.getOwner().getPos().getPosId() == IConstant.POS_CLOVER) {
								posTypeId = IConstant.POS_CLOVER;
								if(orderR.getIsFutureOrder()!=null && orderR.getIsFutureOrder()==1)
								{
									if(storeOpenstatus)
								    sendNotification(notificationJson, orderR.getMerchant().getPosMerchantId(),
										orderR.getMerchant().getAccessToken());
								}else sendNotification(notificationJson, orderR.getMerchant().getPosMerchantId(),
										orderR.getMerchant().getAccessToken());
							} else {
								try {
									String orderNotificationJson = getNotificationJSONForAPK(orderR.getId(),
											merchant.getModileDeviceId(), merchant.getMerchantUid(),
											merchant.getPosMerchantId());
									String finalJson = "";
									posTypeId = IConstant.POS_FOODTRONIX;
									LOGGER.info("OrderServiceImpl :: updateOrderStatus : orderNotificationJson " + orderNotificationJson);
									finalJson = finalJson + "{\"data\": " + orderNotificationJson
											+ " ,\"registration_ids\":[\"" + merchant.getModileDeviceId() + "\"],"
											+ "\"android\": {\"priority\": \"high\"}}";
									LOGGER.info("OrderServiceImpl :: updateOrderStatus : finalJson " + finalJson);
									if (merchant != null && merchant.getOwner() != null && merchant.getOwner().getPos() != null
											&& merchant.getOwner().getPos().getPosId() != null
											&& merchant.getOwner().getPos().getPosId() == IConstant.NON_POS){
									if(orderR.getIsFutureOrder()!=null && orderR.getIsFutureOrder()==1)
									{
										if(storeOpenstatus)
									AppNotification.sendPushNotification(merchant.getModileDeviceId(), finalJson,
											merchant.getPosMerchantId(),environment);
									}
									else {
										AppNotification.sendPushNotification(merchant.getModileDeviceId(), finalJson,
												merchant.getPosMerchantId(),environment);
									}
									}
								} catch (IOException e) {
									MailSendUtil.sendExceptionByMail(e,environment);
									LOGGER.error("error: " + e.getMessage());
									LOGGER.error("OrderServiceImpl :: updateOrderStatus : Exception " + e);
								}
							}
							// }
							// here

							if (saveCard != null && saveCard == true) {
								if (jObject.toString().contains("vaultedCard")) {
									// JSONObject vaultedCard = new
									// JSONObject("vaultedCard");
									JSONObject vaultedCard = jObject.getJSONObject("vaultedCard");
									CardInfo cardInfo = null;
									if (vaultedCard.toString().contains("first6")
											&& vaultedCard.toString().contains("last4")
											&& vaultedCard.toString().contains("token")
											&& vaultedCard.toString().contains("expirationDate")
											&& orderR.getCustomer() != null && orderR.getCustomer().getId() != null) {
										cardInfo = cardInfoRepository.findByLast4AndFirst6AndCustomerIdAndStatus(
												vaultedCard.getString("last4"), vaultedCard.getString("first6"),
												orderR.getCustomer().getId(), true);

										// CardInfo CardInfo = new CardInfo();
										Date today = (merchant != null && merchant.getTimeZone() != null
												&& merchant.getTimeZone().getTimeZoneCode() != null)
												? DateUtil.getCurrentDateForTimeZonee(merchant.getTimeZone().getTimeZoneCode())
												: new Date();
										if (cardInfo != null) {
											cardInfo.setUpdateDate(today);

										} else {
											cardInfo = new CardInfo();
											cardInfo.setStatus(true);
											cardInfo.setCreateDate(today);
											cardInfo.setCustomer(orderR.getCustomer());
											cardInfo.setMerchant(merchant);
										}
										if (ccType.equals("American Express")) {
											cardInfo.setCardType("AX");
										} else if (ccType.equals("Master Card")) {
											cardInfo.setCardType("MC");
										} else {
											cardInfo.setCardType("VISA");
										}
										cardInfo.setExpirationDate(vaultedCard.getString("expirationDate"));
										cardInfo.setFirst6(vaultedCard.getString("first6"));
										cardInfo.setLast4(vaultedCard.getString("last4"));

										EncryptedKey encryptedKey = merchantEncryptedKeyRepository
												.findByMerchantId(merchant.getId());
										if (encryptedKey != null) {
											String encyToken = EncryptionDecryptionToken.encryptValue(
													vaultedCard.getString("token"), encryptedKey.getKey());
											if (encyToken != null && encyToken != "" && !encyToken.isEmpty()) {
												cardInfo.setToken(encyToken);
											}
										}

										cardInfoRepository.save(cardInfo);

									}

								}
							}
						}
					}
				}
			} else {
				try {
					if (result != null && !result.equals("")) {
						if (orderR != null) {
							orderR.setIsDefaults(3);
							orderRepo.save(orderR);
						}
						String merchId = merchant.getId().toString();
						String base64encode = EncryptionDecryptionUtil.encryption(merchId);
						String locationName = merchant.getName().replaceAll("[^a-zA-Z0-9]", "");
						final String FINALORDERLINK = environment.getProperty("WEB_BASE_URL") + "/" + locationName + "/clover/"
								+ base64encode;
						final Integer MERCHANTID = merchant.getId();
						new Thread() {
							public void run() {
								MailSendUtil.failOrderMail(name1, paymentYtpe1, orderPosId1, subTotal1, tax1,
										orderDetails1, email1, orderPrice1, note1, merchantName1, merchantLogo1,
										orderType1, convenienceFeeValue1, avgTime, deliverFee, orderDiscount, tipAmount,
										"we could not process payment due to card failure.", FINALORDERLINK,
										MERCHANTID,environment);
							}
						}.start();

						LOGGER.info("OrderServiceImpl :: updateOrderStatus : order mail thread over");
					}
				} catch (Exception e) {
					MailSendUtil.sendExceptionByMail(e,environment);
					LOGGER.error("OrderServiceImpl :: updateOrderStatus : Exception " + e);
					LOGGER.info(" Invalid Emailid");
				}
			}
			// Notification Method Starts
			String methodType = null;
			int orderAccepted = 0;
			String orderLink = null;
			String requestId = null;
			String contact = null;
			String orderIdd = null;
			String customerrId = null;

			if (orderR != null && orderR.getId() != null) {
				orderIdd = EncryptionDecryptionUtil.encryption(Integer.toString(orderR.getId()));
			}

			if (orderR != null && orderR.getCustomer() != null && orderR.getCustomer().getId() != null) {
				customerrId = EncryptionDecryptionUtil.encryption(Integer.toString(orderR.getCustomer().getId()));
			}

			Boolean enableNotification = false;
			Boolean autoAcceptFutureOrder = false;
			Boolean autoAccpetOrder = false;
			String notificationType = "";
			MerchantConfiguration merchantConfiguration = merchantConfigurationService
					.findMerchantConfigurationBymerchantId(merchant.getId());
			if (merchantConfiguration != null) {
				if (merchantConfiguration.getEnableNotification() != null) {

					if (merchantConfiguration.getEnableNotification() == true) {
						notificationType = "Enable Notification";
						enableNotification = merchantConfiguration.getEnableNotification();
						LOGGER.info(
								"OrderServiceImpl :: updateOrderStatus : enableNotification  :: " + enableNotification);
					}
				}

				if (merchantConfiguration.getAutoAcceptOrder() != null) {

					if (merchantConfiguration.getAutoAcceptOrder() == true) {
						notificationType = "Auto Accept";
						autoAccpetOrder = merchantConfiguration.getAutoAcceptOrder();
						LOGGER.info("OrderServiceImpl :: updateOrderStatus : autoAccpetOrder  :: " + autoAccpetOrder);
					}
				}
				if (merchantConfiguration.getAutoAccept() != null) {

					if (merchantConfiguration.getAutoAccept() == true) {
						notificationType = "Auto Accept Future Order";
						autoAcceptFutureOrder = merchantConfiguration.getAutoAccept();
						LOGGER.info("OrderServiceImpl :: updateOrderStatus : autoAcceptFutureOrder  :: "
								+ autoAcceptFutureOrder);
					}
				}

			}

			String reviewResponse = null;
			String url = environment.getProperty("WEB_BASE_URL") + "/thermalPrinterReceiptforOrder?orderid=" + orderR.getId()
					+ "&merchantId=" + merchant.getId() + "&customerId=" + orderR.getCustomer().getId();
			LOGGER.error("OrderServiceImpl :: updateOrderStatus :  Thermal receipt URL=== : " + url);
			LOGGER.info("Thermal receipt URL===" + url);
			try {
				HttpClient client = HttpClientBuilder.create().build();
				HttpGet httpRequest = new HttpGet(url);
				HttpResponse httpResponse = client.execute(httpRequest);
				BufferedReader rd = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent()));
				StringBuffer result1 = new StringBuffer();
				String line = "";
				while ((line = rd.readLine()) != null) {
					result1.append(line);
				}
				reviewResponse = result1.toString();
			} catch (Exception e) {
				MailSendUtil.sendExceptionByMail(e,environment);
				LOGGER.error("OrderServiceImpl :: updateOrderStatus :  Exception : " + e);
				LOGGER.error("error: " + e.getMessage());
			}

			// send order receipt in printer starts
			Printer printer = printerRepository.findByMerchantId(merchant.getId());
			if (printer != null && merchant != null) {
				LOGGER.info("OrderServiceImpl :: updateOrderStatus : printOrderReceipt call start");
				printOrderReceipt(printer, orderIdd, customerrId, merchant,environment);
				LOGGER.info("OrderServiceImpl :: updateOrderStatus : printOrderReceipt call end");
			}
			// send order receipt in printer ends

			// payment processing according to notification type
			if (enableNotification == false) {
				if (orderR.getPaymentMethod().equals("Cash")
						|| (orderR.getPaymentMethod().equals("Credit Card") && !result.contains("DECLINED"))) {
					if (autoAccpetOrder == true && autoAcceptFutureOrder == false
							&& (((futureOrderType.equals("on")) && (!futureDate.equals("select"))))) {
						orderStatus = "success";
						LOGGER.info("OrderServiceImpl :: updateOrderStatus : autoAccpetOrder  :: " + autoAccpetOrder);
					} else if (autoAccpetOrder == true && autoAcceptFutureOrder == true
							&& (((futureOrderType.equals("on")) && (!futureDate.equals("select"))))
							&& merchant.getOwner() != null && merchant.getOwner().getPos() != null
							&& merchant.getOwner().getPos().getPosId() != null
							&& merchant.getOwner().getPos().getPosId() != IConstant.FOCUS
							&& merchant.getOwner().getPos().getPosId() != IConstant.POS_FOODTRONIX) {
						LOGGER.info("OrderServiceImpl :: updateOrderStatus : autoAcceptFutureOrder  :: "
								+ autoAcceptFutureOrder);
						if (merchant.getOwner().getPos().getPosId() != IConstant.POS_CLOVER)
						{
							if(orderR.getIsFutureOrder()!=null && orderR.getIsFutureOrder()==1)
							{
								if(storeOpenstatus)
								{
							orderStatus = OrderUtil.orderConfirmationByIdByMerchantUId(orderR.getId(),
									merchant.getMerchantUid(), enableNotification,environment);
							LOGGER.info(
									"OrderServiceImpl :: updateOrderStatus : autoAcceptFutureOrder  ::order conform metod called ");
								}
							}else orderStatus = OrderUtil.orderConfirmationByIdByMerchantUId(orderR.getId(),
									merchant.getMerchantUid(), enableNotification,environment);
						}
						else
							if(orderR.getIsFutureOrder()!=null && orderR.getIsFutureOrder()==1)
							{
								if(storeOpenstatus || merchant.getId()==624 || merchant.getId()==190)
								{
							orderStatus = OrderUtil.orderConfirmationByOrderPosId(orderR.getOrderPosId(),environment);
							LOGGER.info(
									"OrderServiceImpl :: updateOrderStatus : autoAcceptFutureOrder  ::order conform metod called ");
								}
							}else orderStatus = OrderUtil.orderConfirmationByOrderPosId(orderR.getOrderPosId(),environment);
					} else if (merchant.getOwner().getPos() != null && merchant.getOwner().getPos().getPosId() != null
							&& merchant.getOwner().getPos().getPosId() != IConstant.FOCUS
							&& merchant.getOwner().getPos().getPosId() != IConstant.POS_FOODTRONIX) {
						if (merchant.getOwner().getPos().getPosId() != IConstant.POS_CLOVER)
						{
							LOGGER.info("OrderServiceImpl :: updateOrderStatus : orderConfirmationByIdByMerchantUId else part ");
							orderStatus = OrderUtil.orderConfirmationByIdByMerchantUId(orderR.getId(),
									merchant.getMerchantUid(), enableNotification,environment);
						}
						else{
							orderStatus = OrderUtil.orderConfirmationByOrderPosId(orderR.getOrderPosId(),environment);
						LOGGER.info("OrderServiceImpl :: updateOrderStatus : autoAccpetOrder but order now case  :: "
								+ autoAcceptFutureOrder);
						LOGGER.info("OrderServiceImpl :: updateOrderStatus : orderConfirmationByIdByMerchantUId else part ");
						}
					}
				}
			} else if (enableNotification == true) {
				orderStatus = "success";
				LOGGER.info("OrderServiceImpl :: updateOrderStatus : enableNotification  :: " + enableNotification);
			}

			List<NotificationMethod> methods = methodRepository.findByMerchantIdAndIsActive(merchant.getId());
			if (methods != null && methods.size() > 0) {
				for (NotificationMethod method : methods) {
					NotificationTracker notificationTracker = new NotificationTracker();
					notificationTracker.setMerchantId(merchant.getId());
					notificationTracker.setOrderId(orderR.getId());
					methodType = method.getNotificationMethodType().getMethodType();
					orderAccepted = method.getIsOrderAccepted();
					requestId = method.getSmsRequestId();
					contact = method.getContact();
					notificationTracker.setContactId(contact);
					notificationTracker.setEnableNotification(notificationType);
					if (merchant.getMerchantLogo() != null) {
						merchantLogo = environment.getProperty("BASE_PORT") + merchant.getMerchantLogo();
					} else {
						merchantLogo = "http://www.dev.foodkonnekt.com/app/foodnew.jpg";
					}

					if (orderR != null) {
						if (methodType.equals("Mail") && enableNotification != null) {
							notificationTracker.setNotificationType("Mail");
							if (!IConstant.flag) {
								ProducerUtil.sendFax(orderIdd, customerrId,environment);
							}

							final String FINALORDERIDD = orderIdd;
							final String FINALCUSTOMERID = customerrId;

							final Integer MERCHANTID = merchant.getId();

							double ordrPrice = 0;
							String customerName = null;
							if (orderR.getOrderPrice() != null) {
								ordrPrice = orderR.getOrderPrice();
							}
							final Double FINALORDERPRICE = ordrPrice;
							if (orderR.getCustomer().getFirstName() != null
									&& orderR.getCustomer().getLastName() != null) {
								customerName = orderR.getCustomer().getFirstName().concat(" ")
										.concat(orderR.getCustomer().getLastName());
							} else if (orderR.getCustomer().getFirstName() != null) {
								customerName = orderR.getCustomer().getFirstName();
							}
							final String FINALCUSTOMERFIRSTNAME = customerName;
							final String FINALORDERTYPE = orderR.getOrderType();
							final String FINALPAYMENTMETHOD = orderR.getPaymentMethod();

							final String FINALCONTACT = contact;
							final Date FINALCREATEDDATE = orderR.getCreatedOn();
							final String FINALMERCHANTLOGO = merchantLogo;
							final String FINALSUBTOTAL = orderR.getSubTotal();
							final String FINALCONVENIENCEFEE = orderR.getConvenienceFee();
							final String FINALDELIVERYFEE = orderR.getDeliveryFee();
							final String FINALTAX = orderR.getTax();
							final Double FINALTIPAMOUNT = orderR.getTipAmount();
							final String ORDERNOTE = orderR.getOrderNote();
							final String MERCHANTNAME = merchant.getName();
							final String ORDERDETAILS = orderDetails;
							final String ORDERAVGTIME = orderR.getOrderAvgTime();
							final Double ORDERDISCOUNT = orderR.getOrderDiscount();
							String discountCoupon = "";
							if (orderR.getOrderDiscount() != null) {
								List<OrderDiscount> orderDiscounts = orderDiscountRepository
										.findByOrderId(orderR.getId());
								if (!orderDiscounts.isEmpty() && orderDiscounts != null) {
									discountCoupon = discountCoupon + "<tr>" + "<td>Discount Coupon</td>"
											+ "<td>&nbsp;</td>" + "<td>" + orderDiscounts.get(0).getCouponCode()
											+ "</td>" + "</tr>";
									if (orderDiscounts.size() > 1) {
										for (int i = 1; i < orderDiscounts.size(); i++) {
											discountCoupon = discountCoupon + "<tr>" + "<td></td>" + "<td>&nbsp;</td>"
													+ "<td>" + orderDiscounts.get(i).getCouponCode() + "</td>"
													+ "</tr>";
										}
									}
								}
							}
							listOfCoupons = discountCoupon;
							final String ORDERCOUPON = discountCoupon;
							String orderLinkUrl = null;
							LOGGER.info(
									"OrderServiceImpl :: updateOrderStatus :  method type====Mail== : " + methodType);
							LOGGER.info("method type====Mail==" + methodType);
							if (enableNotification == false) {
								LOGGER.info(
										"OrderServiceImpl :: updateOrderStatus :  when enable notification is false : ");
								LOGGER.info("when enable notification is false");
								if (orderR.getPaymentMethod().equals("Cash")
										|| (orderR.getPaymentMethod().equals("Credit Card")
												&& !result.contains("DECLINED"))) {
									// when merchant is not focus and foodtronix then this order confirmation will
									// run because we will auto accept this order from focus/foodtronix

									// when auto accept is on but auto accept future order is off
									if (autoAccpetOrder == true && autoAcceptFutureOrder == false
											&& (((futureOrderType.equals("on")) && (!futureDate.equals("select"))))) {

										LOGGER.info(
												"OrderServiceImpl :: updateOrderStatus :  when autoAccept is true : ");

										if (storeOpenstatus || merchant.getId()==624 || merchant.getId()==190) {
											LOGGER.info(
													"OrderServiceImpl :: updateOrderStatus :   when autoAcceptFutureOrder is false : opening closing hours is matched");
											LOGGER.info("opening closing hours is matched");
											if (merchant != null && merchant.getOwner() != null
													&& merchant.getOwner().getPos() != null
													&& merchant.getOwner().getPos().getPosId() != null) {
												orderLinkUrl = environment.getProperty("WEB_BASE_URL") + "/receipt?orderid=" + orderIdd
														+ "&customerid=" + customerrId;
											}
											LOGGER.info("OrderServiceImpl :: updateOrderStatus : orderLinkUrl : "
													+ orderLinkUrl);
											final String FINALORDERLINK = orderLinkUrl;
											final NotificationTracker notificationTracker2 = notificationTracker;
											new Thread() {
												public void run() {
													MailSendUtil.onlineOrderNotification(FINALORDERIDD, FINALCUSTOMERID,
															FINALCUSTOMERFIRSTNAME, FINALORDERPRICE, FINALORDERTYPE,
															FINALPAYMENTMETHOD, FINALCREATEDDATE, FINALORDERLINK,
															FINALCONTACT, FINALMERCHANTLOGO, FINALSUBTOTAL,
															FINALCONVENIENCEFEE, FINALDELIVERYFEE, FINALTAX,
															FINALTIPAMOUNT, ORDERNOTE, MERCHANTNAME, ORDERDETAILS,
															ORDERAVGTIME, ORDERDISCOUNT, ORDERCOUPON, MERCHANTID,
															orderPosId1,environment);
													if (notificationTracker2.getId() != null)
														notificationTracker2.setId(null);
													notificationTracker2.setStatus(IConstant.mailStatus);
													notificatonRepo.save(notificationTracker2);
													IConstant.mailStatus = "Not sent";
												}
											}.start();
											IConstant.flagofMail = true;
										} else {
											notificationTracker.setStatus("Not sent");
											notificatonRepo.save(notificationTracker);
										}

										// when auto accept order is on and auto accept future order is on
									} else if (autoAccpetOrder == true && autoAcceptFutureOrder == true
											&& (((futureOrderType.equals("on")) && (!futureDate.equals("select"))))) {

										LOGGER.info("autoAccpetOrder is true & autoAcceptFutureOrder is true");
										LOGGER.info(
												"OrderServiceImpl :: updateOrderStatus :   autoAccpetOrder is true & autoAcceptFutureOrder is true : ");
										if (merchant != null && merchant.getOwner() != null
												&& merchant.getOwner().getPos() != null
												&& merchant.getOwner().getPos().getPosId() != null) {
											// orderStatus= OrderUtil.orderConfirmationByIdByMerchantUId(orderR.getId(),
											// merchant.getMerchantUid(), enableNotification);

											orderLinkUrl = environment.getProperty("WEB_BASE_URL") + "/receipt?orderid=" + orderIdd
													+ "&customerid=" + customerrId;
										}
										
										if (storeOpenstatus || merchant.getId()==624 || merchant.getId()==190) {
											LOGGER.info(
													"OrderServiceImpl :: updateOrderStatus :   autoAccpetOrder is true & autoAcceptFutureOrder is true : opening closing hours is matched");
											LOGGER.info("when time matches");
											LOGGER.info("OrderServiceImpl :: updateOrderStatus : orderLinkUrl : "
													+ orderLinkUrl);
											final String FINALORDERLINK = orderLinkUrl;
											final NotificationTracker notificationTracker2 = notificationTracker;
											new Thread() {
												public void run() {
													MailSendUtil.onlineOrderNotification(FINALORDERIDD, FINALCUSTOMERID,
															FINALCUSTOMERFIRSTNAME, FINALORDERPRICE, FINALORDERTYPE,
															FINALPAYMENTMETHOD, FINALCREATEDDATE, FINALORDERLINK,
															FINALCONTACT, FINALMERCHANTLOGO, FINALSUBTOTAL,
															FINALCONVENIENCEFEE, FINALDELIVERYFEE, FINALTAX,
															FINALTIPAMOUNT, ORDERNOTE, MERCHANTNAME, ORDERDETAILS,
															ORDERAVGTIME, ORDERDISCOUNT, ORDERCOUPON, MERCHANTID,
															orderPosId1,environment);
													if (notificationTracker2.getId() != null)
														notificationTracker2.setId(null);
													notificationTracker2.setStatus(IConstant.mailStatus);
													notificatonRepo.save(notificationTracker2);
													IConstant.mailStatus = "No";
												}
											}.start();
											IConstant.flagofMail = true;
										} else {
											notificationTracker.setStatus("Not sent");
											notificatonRepo.save(notificationTracker);
										}

										// case only for order now
									} else {
										LOGGER.info(
												"OrderServiceImpl :: updateOrderStatus : When Enablenotification is false  and order now case");
										LOGGER.info("order now case");
										if (merchant != null && merchant.getOwner() != null
												&& merchant.getOwner().getPos() != null
												&& merchant.getOwner().getPos().getPosId() != null) {
											// orderStatus=OrderUtil.orderConfirmationByIdByMerchantUId(orderR.getId(),
											// merchant.getMerchantUid(), enableNotification);
											orderLinkUrl = environment.getProperty("WEB_BASE_URL") + "/receipt?orderid=" + orderIdd
													+ "&customerid=" + customerrId;
										}
										LOGGER.info("OrderServiceImpl :: updateOrderStatus : orderLinkUrl : "
												+ orderLinkUrl);

										final String FINALORDERLINK = orderLinkUrl;
										if (!((futureOrderType.equals("on")) && (!futureDate.equals("select")))) {
											final NotificationTracker notificationTracker2 = notificationTracker;
											new Thread() {
												public void run() {
													MailSendUtil.onlineOrderNotification(FINALORDERIDD, FINALCUSTOMERID,
															FINALCUSTOMERFIRSTNAME, FINALORDERPRICE, FINALORDERTYPE,
															FINALPAYMENTMETHOD, FINALCREATEDDATE, FINALORDERLINK,
															FINALCONTACT, FINALMERCHANTLOGO, FINALSUBTOTAL,
															FINALCONVENIENCEFEE, FINALDELIVERYFEE, FINALTAX,
															FINALTIPAMOUNT, ORDERNOTE, MERCHANTNAME, ORDERDETAILS,
															ORDERAVGTIME, ORDERDISCOUNT, ORDERCOUPON, MERCHANTID,
															orderPosId1,environment);
													if (notificationTracker2.getId() != null)
														notificationTracker2.setId(null);
													notificationTracker2.setStatus(IConstant.mailStatus);
													notificatonRepo.save(notificationTracker2);
													IConstant.mailStatus = "No";
												}
											}.start();
											IConstant.flagofMail = true;
										}

									}
								}
							} else if (enableNotification == true) {
								if (orderR.getPaymentMethod().equals("Cash")
										|| (orderR.getPaymentMethod().equals("Credit Card")
												&& !result.contains("DECLINED"))) {
									LOGGER.info("OrderServiceImpl :: updateOrderStatus : enable notification is true");
									LOGGER.info("enable notification is true");

									// if order type is future
									if ((futureOrderType.equals("on")) && (!futureDate.equals("select"))) {
										LOGGER.info("order is future order");
										
										if (storeOpenstatus || merchant.getId()==624 || merchant.getId()==190) {
											LOGGER.info(
													"OrderServiceImpl :: updateOrderStatus : enable notification is true :time between opening closing hours");
											LOGGER.info("time matches");
											orderLinkUrl = environment.getProperty("WEB_BASE_URL") + "/receipt?orderid=" + orderIdd
													+ "&customerid=" + customerrId;
											LOGGER.info("OrderServiceImpl :: updateOrderStatus : orderLinkUrl : "
													+ orderLinkUrl);
											final String FINALORDERLINK = orderLinkUrl;
											final NotificationTracker notificationTracker2 = notificationTracker;
											new Thread() {
												public void run() {
													MailSendUtil.onlineOrderNotification(FINALORDERIDD, FINALCUSTOMERID,
															FINALCUSTOMERFIRSTNAME, FINALORDERPRICE, FINALORDERTYPE,
															FINALPAYMENTMETHOD, FINALCREATEDDATE, FINALORDERLINK,
															FINALCONTACT, FINALMERCHANTLOGO, FINALSUBTOTAL,
															FINALCONVENIENCEFEE, FINALDELIVERYFEE, FINALTAX,
															FINALTIPAMOUNT, ORDERNOTE, MERCHANTNAME, ORDERDETAILS,
															ORDERAVGTIME, ORDERDISCOUNT, ORDERCOUPON, MERCHANTID,
															orderPosId1,environment);
													if (notificationTracker2.getId() != null)
														notificationTracker2.setId(null);
													notificationTracker2.setStatus(IConstant.mailStatus);
													notificatonRepo.save(notificationTracker2);
													IConstant.mailStatus = "No";
												}
											}.start();
											IConstant.flagofMail = true;
										} else {
											notificationTracker.setStatus("Not sent");
											notificatonRepo.save(notificationTracker);
										}

										// if order type in not future
									} else {
										LOGGER.info(
												"OrderServiceImpl :: updateOrderStatus : enable notification is true:order now  ");
										LOGGER.info("order now");
										orderLinkUrl = environment.getProperty("WEB_BASE_URL") + "/receipt?orderid=" + orderIdd
												+ "&customerid=" + customerrId;
										LOGGER.info(
												"OrderServiceImpl :: updateOrderStatus : enable notification is true: orderLinkUrl  "
														+ orderLinkUrl);
										final String FINALORDERLINK = orderLinkUrl;
										final NotificationTracker notificationTracker2 = notificationTracker;
										new Thread() {
											public void run() {
												MailSendUtil.onlineOrderNotification(FINALORDERIDD, FINALCUSTOMERID,
														FINALCUSTOMERFIRSTNAME, FINALORDERPRICE, FINALORDERTYPE,
														FINALPAYMENTMETHOD, FINALCREATEDDATE, FINALORDERLINK,
														FINALCONTACT, FINALMERCHANTLOGO, FINALSUBTOTAL,
														FINALCONVENIENCEFEE, FINALDELIVERYFEE, FINALTAX, FINALTIPAMOUNT,
														ORDERNOTE, MERCHANTNAME, ORDERDETAILS, ORDERAVGTIME,
														ORDERDISCOUNT, ORDERCOUPON, MERCHANTID, orderPosId1,environment);
												if (notificationTracker2.getId() != null)
													notificationTracker2.setId(null);
												notificationTracker2.setStatus(IConstant.mailStatus);
												notificatonRepo.save(notificationTracker2);
												IConstant.mailStatus = "No";
											}
										}.start();
										IConstant.flagofMail = true;
									}
								}
							}
						} else if (methodType.equals("SMS") && enableNotification != null) {
							notificationTracker.setNotificationType("SMS");
							LOGGER.info("Customer name=" + orderR.getCustomer().getFirstName());
							LOGGER.info("Order Price=" + orderR.getOrderPrice());
							LOGGER.info("Order Type=" + orderR.getOrderType());
							LOGGER.info("Payment type=" + orderR.getPaymentMethod());
							LOGGER.info("Order Date=" + orderR.getCreatedOn());
							LOGGER.info("Order Link=" + orderLink);
							LOGGER.info(
									"OrderServiceImpl :: updateOrderStatus : enable notification is true: Customer name="
											+ orderR.getCustomer().getFirstName());
							LOGGER.info(
									"OrderServiceImpl :: updateOrderStatus : enable notification is true: Order Price="
											+ orderR.getOrderPrice());
							LOGGER.info(
									"OrderServiceImpl :: updateOrderStatus : enable notification is true: Order Type="
											+ orderR.getOrderType());
							LOGGER.info(
									"OrderServiceImpl :: updateOrderStatus : enable notification is true: Payment type="
											+ orderR.getPaymentMethod());
							LOGGER.info(
									"OrderServiceImpl :: updateOrderStatus : enable notification is true: Order Date="
											+ orderR.getCreatedOn());
							LOGGER.info(
									"OrderServiceImpl :: updateOrderStatus : enable notification is true: Order Link="
											+ orderLink);
							final String FINALCONTACT = contact;
							final String FINALREQUESTID = requestId;
							if (enableNotification == false) {

								if (orderR.getPaymentMethod().equals("Cash")
										|| (orderR.getPaymentMethod().equals("Credit Card")
												&& !result.contains("DECLINED"))) {
									LOGGER.info(
											"OrderServiceImpl :: updateOrderStatus : method type===sms==" + methodType);
									LOGGER.info("method type===sms==" + methodType);

									final String FINALSMSORDERLINK = environment.getProperty("WEB_BASE_URL") + "/receipt?orderid="
											+ EncryptionDecryptionUtil.encryption(orderR.getId().toString())
											+ "&customerid=" + EncryptionDecryptionUtil
													.encryption(orderR.getCustomer().getId().toString());
									LOGGER.info(FINALSMSORDERLINK);
									LOGGER.info("OrderServiceImpl :: updateOrderStatus : FINALSMSORDERLINK "
											+ FINALSMSORDERLINK);

									if (autoAccpetOrder == true && autoAcceptFutureOrder == false
											&& (((futureOrderType.equals("on")) && (!futureDate.equals("select"))))) {
										LOGGER.info(
												"OrderServiceImpl :: updateOrderStatus : when autoAccpetOrder is true");
										LOGGER.info(
												"OrderServiceImpl :: updateOrderStatus :when autoAccpetOrder is true");

										if (storeOpenstatus || merchant.getId()==624 || merchant.getId()==190) {

											LOGGER.info(
													"OrderServiceImpl :: updateOrderStatus : when autoAccpetOrder is false : current time is between opening closing hours");
											LOGGER.info("opening closing hours is matched");
											final NotificationTracker notificationTracker2 = notificationTracker;
											new Thread() {
												public void run() {
													String googleShortURL = null;
													// googleShortURL=ShortCodeUtil.generateShortUrl(FINALSMSORDERLINK);
													googleShortURL = ShortCodeUtil.getTinyUrl(FINALSMSORDERLINK);
													HttpPost postRequest = new HttpPost(
															environment.getProperty("CommunicationPlatform_BASE_URL_SERVER")
																	+ "/sendFoodKonnketSMSForOrder");
													String request = "{\"mobileNumber\":\"" + FINALCONTACT
															+ "\",\"messsage\":\"You got a new online order please view this link "
															+ googleShortURL + "\",\"orgId\":\"" + FINALREQUESTID
															+ "\"} ";
													String response = convertToStringJson(postRequest, request,environment);
													LOGGER.info("response for " + FINALCONTACT + " SMS=== " + response);
													LOGGER.info(
															"OrderServiceImpl :: updateOrderStatus : response for SMS=== "
																	+ response);
													if (response.contains("SMS sent successfully")) {
														IConstant.smsStatus = "sent";
													} else
														IConstant.smsStatus = "not sent";
													notificationTracker2.setStatus(IConstant.smsStatus);
													notificatonRepo.save(notificationTracker2);
												}
											}.start();
											notificationTracker.setId(null);
										} else {
											notificationTracker.setStatus("Not sent");
											notificatonRepo.save(notificationTracker);
										}
									} else if (autoAccpetOrder == true && autoAcceptFutureOrder == true
											&& (((futureOrderType.equals("on")) && (!futureDate.equals("select"))))) {
										LOGGER.info(
												"OrderServiceImpl :: updateOrderStatus : autoAccpetOrder is true & autoAcceptFutureOrder is true");
										LOGGER.info("autoAccpetOrder is true & autoAcceptFutureOrder is true");
										
										if (storeOpenstatus || merchant.getId()==624 || merchant.getId()==190) {
											LOGGER.info(
													"OrderServiceImpl :: updateOrderStatus : autoAccpetOrder is true & autoAcceptFutureOrder is true : current time is between opening closing hours");
											LOGGER.info("when time matches");
											final NotificationTracker notificationTracker2 = notificationTracker;
											new Thread() {
												public void run() {
													String googleShortURL = null;
													// googleShortURL=ShortCodeUtil.generateShortUrl(FINALSMSORDERLINK);
													googleShortURL = ShortCodeUtil.getTinyUrl(FINALSMSORDERLINK);
													HttpPost postRequest = new HttpPost(
															environment.getProperty("CommunicationPlatform_BASE_URL_SERVER")
																	+ "/sendFoodKonnketSMSForOrder");
													String request = "{\"mobileNumber\":\"" + FINALCONTACT
															+ "\",\"messsage\":\"You got a new online order please view this link "
															+ googleShortURL + "\",\"orgId\":\"" + FINALREQUESTID
															+ "\"} ";
													String response = convertToStringJson(postRequest, request,environment);
													LOGGER.info("response for " + FINALCONTACT + " SMS=== " + response);
													if (response.contains("SMS sent successfully")) {
														IConstant.smsStatus = "sent";
													} else
														IConstant.smsStatus = "not sent";
													notificationTracker2.setStatus(IConstant.smsStatus);
													notificatonRepo.save(notificationTracker2);
													LOGGER.info(
															"OrderServiceImpl :: updateOrderStatus : response for SMS=== "
																	+ response);
												}
											}.start();
										} else {
											notificationTracker.setStatus("Not sent");
											notificatonRepo.save(notificationTracker);
										}
									} else {

										LOGGER.info("order now case");
										final NotificationTracker notificationTracker2 = notificationTracker;
										new Thread() {
											public void run() {
												String googleShortURL = null;
												// googleShortURL=ShortCodeUtil.generateShortUrl(FINALSMSORDERLINK);
												googleShortURL = ShortCodeUtil.getTinyUrl(FINALSMSORDERLINK);
												HttpPost postRequest = new HttpPost(
														environment.getProperty("CommunicationPlatform_BASE_URL_SERVER")
																+ "/sendFoodKonnketSMSForOrder");
												String request = "{\"mobileNumber\":\"" + FINALCONTACT
														+ "\",\"messsage\":\"You got a new online order please view this link "
														+ googleShortURL + "\",\"orgId\":\"" + FINALREQUESTID + "\"} ";
												String response = convertToStringJson(postRequest, request,environment);
												LOGGER.info("response for " + FINALCONTACT + " SMS=== " + response);
												if (response.contains("SMS sent successfully")) {
													IConstant.smsStatus = "sent";
												} else
													IConstant.smsStatus = "not sent";
												notificationTracker2.setStatus(IConstant.smsStatus);
												notificatonRepo.save(notificationTracker2);
											}
										}.start();
									}
								}
							} else if (enableNotification == true) {
								if (orderR.getPaymentMethod().equals("Cash")
										|| (orderR.getPaymentMethod().equals("Credit Card")
												&& !result.contains("DECLINED"))) {
									LOGGER.info("method type===sms==" + methodType);

									final String FINALSMSORDERLINK = environment.getProperty("WEB_BASE_URL") + "/receipt?orderid="
											+ EncryptionDecryptionUtil.encryption(orderR.getId().toString())
											+ "&customerid=" + EncryptionDecryptionUtil
													.encryption(orderR.getCustomer().getId().toString());
									LOGGER.info(FINALSMSORDERLINK);

									if ((futureOrderType.equals("on")) && (!futureDate.equals("select"))) {
										LOGGER.info("order is future order");
										
										if (storeOpenstatus || merchant.getId()==624 || merchant.getId()==190) {
											LOGGER.info("time matches");
											final NotificationTracker notificationTracker2 = notificationTracker;
											new Thread() {
												public void run() {
													String googleShortURL = null;
													// googleShortURL=ShortCodeUtil.generateShortUrl(FINALSMSORDERLINK);
													googleShortURL = ShortCodeUtil.getTinyUrl(FINALSMSORDERLINK);
													HttpPost postRequest = new HttpPost(
															environment.getProperty("CommunicationPlatform_BASE_URL_SERVER")
																	+ "/sendFoodKonnketSMSForOrder");
													String request = "{\"mobileNumber\":\"" + FINALCONTACT
															+ "\",\"messsage\":\"You got a new online order please view this link "
															+ googleShortURL + "\",\"orgId\":\"" + FINALREQUESTID
															+ "\"} ";
													String response = convertToStringJson(postRequest, request,environment);
													LOGGER.info("response for SMS=== " + response);
													if (response.contains("SMS sent successfully")) {
														IConstant.smsStatus = "sent";
													} else
														IConstant.smsStatus = "not sent";
													notificationTracker2.setStatus(IConstant.smsStatus);
													notificatonRepo.save(notificationTracker2);
												}
											}.start();

										} else {
											notificationTracker.setStatus("Not sent");
											notificatonRepo.save(notificationTracker);
										}
									} else {
										LOGGER.info("order now case");
										final NotificationTracker notificationTracker2 = notificationTracker;
										new Thread() {
											public void run() {
												String googleShortURL = null;
												// googleShortURL=ShortCodeUtil.generateShortUrl(FINALSMSORDERLINK);
												googleShortURL = ShortCodeUtil.getTinyUrl(FINALSMSORDERLINK);
												HttpPost postRequest = new HttpPost(
														environment.getProperty("CommunicationPlatform_BASE_URL_SERVER")
																+ "/sendFoodKonnketSMSForOrder");
												String request = "{\"mobileNumber\":\"" + FINALCONTACT
														+ "\",\"messsage\":\"You got a new online order please view this link "
														+ googleShortURL + "\",\"orgId\":\"" + FINALREQUESTID + "\"} ";
												String response = convertToStringJson(postRequest, request,environment);
												LOGGER.info("response for SMS=== " + response);
												if (response.contains("SMS sent successfully")) {
													IConstant.smsStatus = "sent";
												} else
													IConstant.smsStatus = "not sent";
												notificationTracker2.setStatus(IConstant.smsStatus);
												notificatonRepo.save(notificationTracker2);
											}
										}.start();
									}
								}
							}
						} else if (methodType.equals("FAX") && enableNotification != null) {

							LOGGER.info("OrderServiceImpl :: updateOrderStatus : inside methodType " + methodType);

							contact = method.getContact();
							LOGGER.info(orderIdd);
							LOGGER.info(customerrId);
							final String FINALCONTACT = contact;
							final String FINALCUSTOMERID = customerrId;
							final String FINALORDERIDD = orderIdd;
							final OrderR FINALORDERR = orderR;
							if (orderR.getPaymentMethod().equals("Cash")
									|| (orderR.getPaymentMethod().equals("Credit Card")
											&& !result.contains("DECLINED"))) {
								if (orderIdd != null && customerrId != null && contact != null) {
									new Thread() {
										public void run() {
											String response = null;
											try {
												if (IConstant.flag == false) {
													response = ProducerUtil.sendFax(FINALORDERIDD, FINALCUSTOMERID,environment);
												}
												try {
													if (FINALCONTACT.contains("@")) {
														if (ProducerUtil.isValid(FINALCONTACT)) {
															LOGGER.info(FINALCONTACT + " is valid");
															boolean faxResponse = MailSendUtil.sendFaxThroughMail(FINALORDERIDD,
		                                                                                                          FINALCONTACT,environment);
														} else {
															LOGGER.info(FINALCONTACT + " is invalid");
														}
													} else {
													FaxUtility.sendFaxFile(FINALCONTACT,
															environment.getProperty("FAX_FILE_PATH") + Integer.parseInt(
																	EncryptionDecryptionUtil.decryption(FINALORDERIDD))
																	+ ".pdf",
															vendorRepository, onlineNotificationStatusRepository,
															FINALORDERR, 1,environment);
													}
												} catch (NumberFormatException e) {
													LOGGER.error("error: " + e.getMessage());
													LOGGER.error(
															"OrderServiceImpl :: updateOrderStatus : Exception " + e);
												} catch (ClientProtocolException e) {
													LOGGER.error("error: " + e.getMessage());
													LOGGER.error(
															"OrderServiceImpl :: updateOrderStatus : Exception " + e);
												} catch (IOException e) {
													LOGGER.error("error: " + e.getMessage());
													LOGGER.error(
															"OrderServiceImpl :: updateOrderStatus : Exception " + e);
												}
											} catch (UnsupportedEncodingException e) {
												LOGGER.error("error: " + e.getMessage());
												LOGGER.error("OrderServiceImpl :: updateOrderStatus : Exception " + e);
											}
											LOGGER.info(
													"OrderServiceImpl :: updateOrderStatus : fax response " + response);
										}
									}.start();
								}
							}
						}

						else if (methodType.equals("APP")) {
							LOGGER.info("method type====APP==" + methodType);
							LOGGER.error("OrderServiceImpl :: updateOrderStatus : method type====APP== " + methodType);
						}
					}
				}
			} else {
				NotificationTracker notificationTracker = new NotificationTracker();
				notificationTracker.setContactId("No");
				notificationTracker.setEnableNotification(notificationType);
				notificationTracker.setMerchantId(merchant.getId());
				notificationTracker.setNotificationType("No");
				notificationTracker.setOrderId(orderR.getId());
				notificationTracker.setStatus("Ok");
				notificatonRepo.save(notificationTracker);

			}
			// Notification method ends
			LOGGER.info("----------------End :: OrderServiceImpl : updateOrderStatus------------------");
			return true;
		} catch (Exception exception) {
			exception.printStackTrace();
			LOGGER.error("OrderServiceImpl :: updateOrderStatus : Exception " + exception);
			MailSendUtil.sendExceptionByMail(exception,environment);
			return false;
		}
	}

	public void sendNotification(String notificationJson, String merchantId, String accessToken) {
		LOGGER.info("----------------Start :: OrderServiceImpl : sendNotification------------------");
		LOGGER.info(" OrderServiceImpl :sendNotification: notificationJson------------------" + notificationJson);
		LOGGER.info(" OrderServiceImpl :sendNotification: merchantId------------------" + merchantId);
		LOGGER.info(" OrderServiceImpl :sendNotification: accessToken------------------" + accessToken);
		HttpPost postRequest = new HttpPost(environment.getProperty("CLOVER_V2_URL") + merchantId + "/apps/" + environment.getProperty("CLOVER_APP_ID")
				+ "/notify?access_token=" + accessToken);
		notificationJson = convertToStringJson(postRequest, notificationJson,environment);
		LOGGER.info("notificationJson=====" + notificationJson);
		LOGGER.info(" OrderServiceImpl :sendNotification: response------------------" + notificationJson);
		LOGGER.info("----------------End :: OrderServiceImpl : sendNotification------------------");

	}

	public static String convertToStringJson(HttpPost postRequest, String customerJson,Environment environment) {
		StringBuilder responseBuilder = new StringBuilder();
		try {
			HttpClient httpClient = HttpClientBuilder.create().build();
			StringEntity input = new StringEntity(customerJson);
			input.setContentType("application/json");
			postRequest.setEntity(input);
			HttpResponse response = httpClient.execute(postRequest);
			LOGGER.info("Output from Server .... \n");
			BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			String line = "";
			while ((line = rd.readLine()) != null) {
				responseBuilder.append(line);
			}
		} catch (MalformedURLException e) {
			MailSendUtil.sendExceptionByMail(e,environment);
			LOGGER.error("error: " + e.getMessage());
		} catch (IOException e) {
			MailSendUtil.sendExceptionByMail(e,environment);
			LOGGER.error("error: " + e.getMessage());
		}
		return responseBuilder.toString();
	}

	public String getFutureOrders(String merchantId) {
		LOGGER.info(" ===merchantId : " + merchantId);
		LOGGER.info("----------------Start :: OrderServiceImpl : getFutureOrders------------------");
		Merchant merchant = merchantRepository.findByPosMerchantId(merchantId);
		if (merchant != null) {
			String avgDeliveryTime = null;
			String avgPickUpTime = null;
			PickUpTime pickUpTime = pickUpTimeRepository.findByMerchantId(merchant.getId());
			if (pickUpTime != null && pickUpTime.getPickUpTime() != null) {
				avgPickUpTime = pickUpTime.getPickUpTime();
				LOGGER.info("OrderServiceImpl : getFutureOrders : avgPickUpTime : " + avgPickUpTime);
			}

			List<Zone> zones = zoneRepository.findByMerchantIdAndZoneStatus(merchant.getId(), 0);
			if (zones != null) {
				if (!zones.isEmpty() && zones.size() > 0) {

					avgDeliveryTime = zones.get(0).getAvgDeliveryTime();
					LOGGER.info("OrderServiceImpl : getFutureOrders : avgDeliveryTime : " + avgDeliveryTime);
				}
			}
			Calendar deliveryAvgTimeCalendar = Calendar.getInstance();
			Calendar pickUpAvgTimeCalendar = Calendar.getInstance();
			int time = 45;
			try {
				if (avgDeliveryTime != null) {
					java.util.Date deliveryAvgTime = new SimpleDateFormat("HH:mm:ss")
							.parse("00:" + avgDeliveryTime + ":00");
					deliveryAvgTimeCalendar.setTime(deliveryAvgTime);
				}
				if (avgPickUpTime != null) {
					java.util.Date pickUpAvgTime = new SimpleDateFormat("HH:mm:ss")
							.parse("00:" + avgPickUpTime + ":00");
					pickUpAvgTimeCalendar.setTime(pickUpAvgTime);
				}
				if (avgDeliveryTime != null && avgPickUpTime != null) {
					if (pickUpAvgTimeCalendar.getTimeInMillis() < deliveryAvgTimeCalendar.getTimeInMillis()) {
						time = pickUpAvgTimeCalendar.getTime().getMinutes();
					} else {
						time = deliveryAvgTimeCalendar.getTime().getMinutes();
					}

				} else if (avgDeliveryTime != null) {
					time = deliveryAvgTimeCalendar.getTime().getMinutes();
				} else if (avgPickUpTime != null) {
					time = pickUpAvgTimeCalendar.getTime().getMinutes();
				}
			} catch (Exception e) {
				MailSendUtil.sendExceptionByMail(e,environment);
			}

			LOGGER.info("OrderServiceImpl : getFutureOrders : time : " + time);
			int timeInterval = time + 15;
			final long ONE_MINUTE_IN_MILLIS = 60000;
			Calendar date = Calendar.getInstance();
			long t = date.getTimeInMillis();
			Date afterAddingTenMins = new Date(t + (time * ONE_MINUTE_IN_MILLIS));
			afterAddingTenMins.setSeconds(0);
			Date betWeenWithTime = new Date(t + (timeInterval * ONE_MINUTE_IN_MILLIS));
			betWeenWithTime.setSeconds(0);
			LOGGER.info("-------" + afterAddingTenMins + "------" + betWeenWithTime);
			LOGGER.info("OrderServiceImpl : getFutureOrders : afterAddingTenMins : " + afterAddingTenMins);
			LOGGER.info("OrderServiceImpl : getFutureOrders : betWeenWithTime : " + betWeenWithTime);
			List<OrderR> orderRs = orderRepo.findByFulFilledDateAndIsFutureOrderAndMerchantIdAndIsDefaults(
					afterAddingTenMins, IConstant.BOOLEAN_TRUE, merchant.getId(), betWeenWithTime,
					IConstant.Order_Status_Pending);
			List<String> notifications = new ArrayList<String>();
			for (OrderR orderR : orderRs) {
				// String orderDetails = "";
				Map<String, Map<String, String>> notf = new HashMap<String, Map<String, String>>();
				Map<String, String> notification = new HashMap<String, String>();
				notification.put("appEvent", "notification");
				Map<String, String> order = new HashMap<String, String>();
				order.put("total", Double.toString(orderR.getOrderPrice()));
				order.put("tax", orderR.getTax());
				List<Map<String, String>> productList = new ArrayList<Map<String, String>>();
				List<OrderItem> orderItems = orderItemRepository.findByOrderId(orderR.getId());
				for (OrderItem orderItem : orderItems) {
					if (orderItem != null) {
						if (orderItem.getItem() != null) {

							List<Map<String, String>> extraList = new ArrayList<Map<String, String>>();
							Map<String, String> poduct = new HashMap<String, String>();
							poduct.put("product_id", orderItem.getItem().getPosItemId());
							poduct.put("name", orderItem.getItem().getName());
							poduct.put("price", Double.toString(orderItem.getItem().getPrice()));
							poduct.put("qty", Integer.toString(orderItem.getQuantity()));
							List<OrderItemModifier> itemModifiers = itemModifierRepository
									.findByOrderItemId(orderItem.getId());
							for (OrderItemModifier itemModifier : itemModifiers) {

								Map<String, String> exctra = new HashMap<String, String>();
								exctra.put("id", itemModifier.getModifiers().getPosModifierId());
								exctra.put("price", Double.toString(itemModifier.getModifiers().getPrice()));
								exctra.put("name", itemModifier.getModifiers().getName());
								exctra.put("qty", Integer.toString(orderItem.getQuantity()));
								extraList.add(exctra);
							}
							Gson gson = new Gson();
							String extraJson = gson.toJson(extraList);
							poduct.put("extras", extraJson);
							productList.add(poduct);
						}
					}
				}
				Gson gson = new Gson();
				String productJson = gson.toJson(productList);
				LOGGER.info("OrderServiceImpl : getFutureOrders : productJson : " + productJson);
				order.put("productItems", productJson);
				// order.put("productItems", productList.toString());

				Date fulFilledTime = orderR.getFulfilled_on();
				Calendar calobjs = Calendar.getInstance();
				calobjs.setTime(fulFilledTime);
				calobjs.add(Calendar.MINUTE, -Integer.parseInt(orderR.getOrderAvgTime()));
				Date orderFulFilledTime = calobjs.getTime();
				LOGGER.info("orderFulFilledTime-" + orderFulFilledTime);
				LOGGER.info("OrderServiceImpl : getFutureOrders : orderFulFilledTime : " + orderFulFilledTime);
				long timeInMillis = orderFulFilledTime.getTime();
				// DateFormat dateFormat = new
				// SimpleDateFormat(IConstant.YYYYMMDDHHMMSS);
				// String orderFulTime =
				// dateFormat.format(orderFulFilledTime).toString();
				String orderJson = gson.toJson(order);
				notification.put("payload", orderR.getOrderPosId() + "@#You got a new order(" + orderR.getOrderPosId()
						+ ") at " + orderR.getCreatedOn() + "@# $" + orderR.getOrderPrice() + "@# "
						+ orderR.getOrderType() + "@#" + orderJson + "@#" + orderR.getMerchant().getPosMerchantId()
						+ "@#" + orderR.getOrderNote() + " @#" + orderR.getPosPaymentId() + " @#" + timeInMillis);

				notf.put("notification", notification);
				String notificationJson = gson.toJson(notf);
				notificationJson = notificationJson.trim().replaceAll("\\\\+\"", "'").replace("'[", "[").replace("]'",
						"]");
				notifications.add(notificationJson);
				LOGGER.info("OrderServiceImpl : getFutureOrders : notificationJson : " + notificationJson);
			}
			if (notifications.size() > 0) {
				LOGGER.info("--------------End ::OrderServiceImpl : getFutureOrders : notifications.size() : "
						+ notifications.size());
				return notifications.toString();
			} else {
				LOGGER.info("--------------End ::OrderServiceImpl : getFutureOrders : return : null");
				return null;
			}

		} else {
			LOGGER.info("--------------End ::OrderServiceImpl : getFutureOrders : return : null");
			return null;
		}
	}
	/*
	 * public boolean setOrderStatus(Integer orderId, String type, String reason) {
	 * if (null == orderId) { return false; } OrderR orderR =
	 * orderRepo.findOne(orderId); if (orderR == null) { return false; }
	 * 
	 * Merchant merchant = orderR.getMerchant(); String merchantLogo = "";
	 * ConvenienceFee convenienceFee = null; PickUpTime pickUpTime = null; String
	 * convenienceFeeValue = "0"; String orderAvgTime = "0"; if (merchant != null) {
	 * List<ConvenienceFee>
	 * convenienceFees=convenienceFeeRepository.findByMerchantId(merchant.getId());
	 * if(convenienceFees!=null && convenienceFees.size()>0){ int
	 * index=convenienceFees.size()-1; convenienceFee=convenienceFees.get(index);
	 * 
	 * } pickUpTime = pickUpTimeRepository .findByMerchantId(merchant.getId());
	 * LOGGER.info(pickUpTime); if (orderR != null) { orderAvgTime =
	 * orderR.getOrderAvgTime(); } if (convenienceFee != null) { convenienceFeeValue
	 * = convenienceFee.getConvenienceFee(); } if (merchant.getMerchantLogo() ==
	 * null) { merchantLogo = "http://www.dev.foodkonnekt.com/app/foodnew.jpg"; }
	 * else { merchantLogo = environment.getProperty("BASE_PORT") + merchant.getMerchantLogo(); } }
	 * 
	 * if (type.equals("Accept")) { orderR.setIsDefaults(1); orderRepo.save(orderR);
	 * String orderDetails = ""; List<OrderItem> orderItems = orderItemRepository
	 * .findByOrderId(orderR.getId()); for (OrderItem orderItem : orderItems) { if
	 * (orderItem.getItem() != null) { String items =
	 * "<tr style='font-weight:600;text-transform:capitalize;font-family:arial'><td width='200px;'>"
	 * + orderItem.getItem().getName() +
	 * "</td><td width='100px;' style='text-align:center'>" +
	 * orderItem.getQuantity() +
	 * "</td><td width='100px;' style='text-align:center'>" + "$" +
	 * orderItem.getItem().getPrice() + "</td></tr>"; orderDetails = orderDetails +
	 * "" + "<b>" + items + "</b>"; } List<OrderItemModifier> itemModifiers =
	 * itemModifierRepository .findByOrderItemId(orderItem.getId()); for
	 * (OrderItemModifier itemModifier : itemModifiers) { String modifiers =
	 * "<tr style='text-transform:capitalize;font-family:arial;margin-top:5px;font-size:12px'><td width='200px;'>"
	 * + itemModifier.getModifiers().getName() +
	 * "</td><td width='100px;' style='text-align:center'>" +
	 * itemModifier.getQuantity() +
	 * " </td><td width='100px;' style='text-align:center'> " + "$" +
	 * itemModifier.getModifiers().getPrice() + "</td></tr>"; orderDetails =
	 * orderDetails + "" + modifiers; } } orderDetails =
	 * "<table width='300px;'><tbody>" + orderDetails + "</table></tbody>";
	 * LOGGER.info(orderDetails); String deliveryFee = "0"; if
	 * (orderR.getDeliveryFee() != null && !orderR.getDeliveryFee().isEmpty())
	 * deliveryFee = orderR.getDeliveryFee();
	 * 
	 * Double orderDiscount = 0.0;
	 * 
	 * if (orderR.getOrderDiscount() != null) orderDiscount = orderDiscount;
	 * 
	 * MailSendUtil.sendConfirmMail(orderR.getCustomer().getFirstName(),
	 * orderR.getPaymentMethod(), null, orderR.getSubTotal(), orderR.getTax(),
	 * orderDetails, orderR.getCustomer() .getEmailId(), orderR.getOrderPrice(),
	 * orderR .getOrderNote(), orderR.getMerchant().getName(), merchantLogo,
	 * orderR.getOrderType(), convenienceFeeValue, orderAvgTime, deliveryFee,
	 * orderDiscount, orderR .getTipAmount()); } else { orderR.setIsDefaults(2);
	 * orderRepo.save(orderR);
	 * MailSendUtil.sendOrderCancellationMail(orderR.getCustomer() .getFirstName(),
	 * null, orderR.getCustomer().getEmailId(), orderR.getMerchant() .getName(),
	 * merchantLogo, orderR.getMerchant() .getPhoneNumber(),
	 * orderR.getMerchant().getOwner( ).getEmail()
	 * 
	 * "praveen.raghuvanshii@gmail.com", reason); } return true; }
	 */

	public OrderR findOrderDetailById(Integer id) {
		LOGGER.info("----------------Start :: OrderServiceImpl : findOrderDetailById------------------");
		OrderR orderR = orderRepository.findOne(id);
		LOGGER.info("OrderServiceImpl : findOrderDetailById :orderRId : " + id);
		List<OrderItem> items = new ArrayList<OrderItem>();

		List<OrderItem> orderItems = orderItemRepository.findByOrderId(id);
		if (orderItems != null && orderItems.size() > 0) {
			LOGGER.info("OrderServiceImpl : findOrderDetailById :orderItems.size : " + orderItems.size());
			for (OrderItem orderItem : orderItems) {

				if (orderItem.getItem() != null) {
					orderItem.setOrder(null);
					Item item = orderItem.getItem();
					item.setMerchant(null);
					item.setModifierGroups(null);
					item.setItemTimings(null);
					item.setCategories(null);
					item.setTaxes(null);
					item.setCategoryList(null);
					item.setExtras(null);
					item.setOrderItems(null);
					item.setItemModifierGroups(null);
					List<OrderItemModifier> orderItemModifiers = orderItemModifierRepository
							.findByOrderItemId(orderItem.getId());
					for (OrderItemModifier itemModifier : orderItemModifiers) {
						itemModifier.setOrderItem(null);
						if (itemModifier.getModifiers() != null) {
							itemModifier.getModifiers().setMerchant(null);
							itemModifier.getModifiers().setModifierGroup(null);

						}

					}
					orderItem.setOrderItemModifiers(orderItemModifiers);
					items.add(orderItem);
				}

			}
			orderR.setOrderItems(items);
		}

		List<OrderPizza> orderPizzas = orderPizzaRepository.findByOrderId(id);
		if (orderPizzas != null && !orderPizzas.isEmpty() && orderPizzas.size() > 0) {
			LOGGER.info("OrderServiceImpl : findOrderDetailById :orderPizzas.size : " + orderPizzas.size());
			for (OrderPizza orderPizza : orderPizzas) {
				
				Item item = new Item();
				OrderItem orderItem = new OrderItem();

				if (orderPizza != null) {
					LOGGER.info(" === orderPizza : " + orderPizza.getId());
					orderPizza.setOrder(null);
					PizzaTemplate pizzaTemplate = orderPizza.getPizzaTemplate();
					if(pizzaTemplate != null) {
					pizzaTemplate.setMerchant(null);
					pizzaTemplate.setCategory(null);
					pizzaTemplate.setPizzaCrust(null);
					pizzaTemplate.setPizzaSizes(null);
					pizzaTemplate.setPizzaTemplateCategories(null);
					pizzaTemplate.setPizzaTemplateToppings(null);
					pizzaTemplate.setTaxes(null);
					}
					item.setMerchant(null);
					item.setModifierGroups(null);
					item.setItemTimings(null);
					item.setCategories(null);
					item.setTaxes(null);
					item.setCategoryList(null);
					item.setExtras(null);
					item.setOrderItems(null);
					item.setItemModifierGroups(null);
					if(pizzaTemplate != null) {
					item.setId(pizzaTemplate.getId());
					item.setIsDefaultTaxRates(pizzaTemplate.getIsDefaultTaxRates());
					}
					item.setPrice(orderPizza.getPrice());
					item.setAllowModifierLimit(0);
					item.setAllowModifierGroupOrder(0);
					item.setItemStatus(0);
					orderItem.setQuantity(orderPizza.getQuantity());

					PizzaSize pizzaSize = pizzaSizeRepository.findById(orderPizza.getPizzaSize().getId());
					if (pizzaSize != null) {
						pizzaSize.setCategories(null);
						pizzaSize.setMerchant(null);
						pizzaSize.setPizzaCrustSizes(null);
						pizzaSize.setPizzaTemplates(null);
						pizzaSize.setPizzaToppingSizes(null);
						if(pizzaTemplate != null)
						item.setName(pizzaTemplate.getDescription() + "(" + pizzaSize.getDescription() + ")");
					}

					orderItem.setItem(item);
					List<OrderPizzaToppings> orderPizzaToppings = orderPizzaToppingsRepository
							.findByOrderPizzaId(orderPizza.getId());
					List<OrderItemModifier> itemModifiersList = new ArrayList<OrderItemModifier>();
					for (OrderPizzaToppings orderPizzaTopping : orderPizzaToppings) {
						orderPizzaTopping.setOrderPizza(null);
						if (orderPizzaTopping.getPizzaTopping() != null) {
							orderPizzaTopping.getPizzaTopping().setMerchant(null);
							orderPizzaTopping.getPizzaTopping().setPizzaTemplateToppings(null);
							orderPizzaTopping.getPizzaTopping().setPizzaToppingSizes(null);
							orderPizzaTopping.getPizzaTopping().setPizzaTemplateTopping(null);

							OrderItemModifier orderItemModifier = new OrderItemModifier();

							Modifiers modifiers = new Modifiers();

							if (orderPizzaTopping.getSide1()) {
								modifiers.setName(
										orderPizzaTopping.getPizzaTopping().getDescription() + " (First Half)");
							} else if (orderPizzaTopping.getSide2()) {
								modifiers.setName(
										orderPizzaTopping.getPizzaTopping().getDescription() + " (Second Half)");
							} else {
								modifiers.setName(orderPizzaTopping.getPizzaTopping().getDescription() + " (Full)");
							}

							modifiers.setId(orderPizzaTopping.getPizzaTopping().getId());
							modifiers.setPrice(orderPizzaTopping.getPrice());
							modifiers.setModifierGroup(null);
							modifiers.setMerchant(null);

							orderItemModifier.setId(orderPizzaTopping.getId());
							orderItemModifier.setQuantity(orderPizzaTopping.getQuantity());
							orderItemModifier.setModifiers(modifiers);
							itemModifiersList.add(orderItemModifier);
						}
					}

					OrderPizzaCrust orderPizzaCrusts = orderPizzaCrustRepository.findByOrderPizzaId(orderPizza.getId());
					if (orderPizzaCrusts != null) {
						OrderItemModifier orderItemModifiers = new OrderItemModifier();

						Modifiers modifier = new Modifiers();
						modifier.setName(orderPizzaCrusts.getPizzacrust().getDescription());
						modifier.setId(orderPizzaCrusts.getPizzacrust().getId());
						modifier.setPrice(orderPizzaCrusts.getPrice());
						modifier.setModifierGroup(null);
						modifier.setMerchant(null);

						orderItemModifiers.setId(orderPizzaCrusts.getId());
						orderItemModifiers.setQuantity(orderPizzaCrusts.getQuantity());
						orderItemModifiers.setModifiers(modifier);
						itemModifiersList.add(orderItemModifiers);
					}
					orderItem.setOrderItemModifiers(itemModifiersList);
					items.add(orderItem);
				}
			}
			orderR.setOrderItems(items);
			orderR.setMerchant(null);
		}
		LOGGER.info("OrderServiceImpl : findOrderDetailById :End : ");
		return orderR;
	}

	public boolean setOrderStatus(String orderId, String type, String reason) {
		LOGGER.info("--------------Start ::OrderServiceImpl : setOrderStatus ");
		LOGGER.info(" ===orderId : " + orderId + " type : " + type + " reason : " + reason);
		if (null == orderId) {
			return false;
		}
		double conveniencePercent =0;
		OrderR orderR = orderRepo.findByOrderPosId(orderId);
		String customerName = "";
		if (orderR.getCustomer().getFirstName() != null && orderR.getCustomer().getLastName() != null) {
			customerName = orderR.getCustomer().getFirstName().concat(" ").concat(orderR.getCustomer().getLastName());
		} else if (orderR.getCustomer().getFirstName() != null) {
			customerName = orderR.getCustomer().getFirstName();
		}
		PaymentGateWay paymentGateWay = null;
		if (orderR != null && orderR.getMerchant() != null && orderR.getMerchant().getId() != null) {
			paymentGateWay = paymentGateWayRepository
					.findByMerchantIdAndIsDeletedAndIsActive(orderR.getMerchant().getId(), false, true);
			if (orderR.getMerchant().getOwner() != null && orderR.getMerchant().getOwner().getPos() != null
					&& orderR.getMerchant().getOwner().getPos().getPosId() != null) {
				posTypeId = orderR.getMerchant().getOwner().getPos().getPosId();
				LOGGER.info("OrderServiceImpl : setOrderStatus : posTypeId : " + posTypeId);
			}
		}

		if (orderR == null) {
			LOGGER.info("OrderServiceImpl : setOrderStatus : Order Not Found : ");
			return false;
		}

		Merchant merchant = orderR.getMerchant();

		String merchantLogo = "";
		ConvenienceFee convenienceFee = null;
		PickUpTime pickUpTime = null;
		String convenienceFeeValue = "0";
		String orderAvgTime = "0";
		if (merchant != null) {

			List<ConvenienceFee> convenienceFees = convenienceFeeRepository.findByMerchantId(merchant.getId());
			if (convenienceFees != null && convenienceFees.size() > 0) {
				int index = convenienceFees.size() - 1;
				convenienceFee = convenienceFees.get(index);

			}
			pickUpTime = pickUpTimeRepository.findByMerchantId(merchant.getId());
			LOGGER.info("pickUpTime ==== " + pickUpTime);
			if (orderR != null) {
				orderAvgTime = orderR.getOrderAvgTime();
			}
			if (convenienceFee != null) {
				double ConvenienceFeePercent =  (convenienceFee.getConvenienceFeePercent()!=null) ? convenienceFee.getConvenienceFeePercent() : 0;
		        conveniencePercent = (new Double(orderR.getSubTotal())*(ConvenienceFeePercent/100.0));
				Double finalConvenienceFee = new Double(convenienceFee.getConvenienceFee())+conveniencePercent;
				finalConvenienceFee = Math.round(finalConvenienceFee * 100.0) / 100.0;
				convenienceFeeValue = finalConvenienceFee.toString();
			}
			if (merchant.getMerchantLogo() == null) {
				merchantLogo = "http://www.dev.foodkonnekt.com/app/foodnew.jpg";
			} else {
				merchantLogo = environment.getProperty("BASE_PORT") + merchant.getMerchantLogo();
			}
		}

		if (type.equalsIgnoreCase("Accept")) {
			LOGGER.info("OrderServiceImpl : setOrderStatus : type Accept:");
			orderR.setIsDefaults(1);
			orderRepo.save(orderR);
			String orderDetails = "";

			List<Map<String, String>> extraList = null;
			Map<String, String> poduct = null;
			Map<String, String> exctra = null;
			List<Map<String, String>> productList = new ArrayList<Map<String, String>>();

			LOGGER.info(" === orderR : " + orderR.getId());
			List<OrderItem> orderItems = orderItemRepository.findByOrderId(orderR.getId());
			for (OrderItem orderItem : orderItems) {
				if (orderItem.getItem() != null) {
					String items = "<tr style='font-weight:600;text-transform:capitalize;font-family:arial'><td width='200px;'>"
							+ orderItem.getItem().getName() + "</td><td width='100px;' style='text-align:center'>"
							+ orderItem.getQuantity() + "</td><td width='100px;' style='text-align:center'>" + "$"
							+ String.format("%.2f",orderItem.getItem().getPrice()) + "</td></tr>";
					orderDetails = orderDetails + "" + "<b>" + items + "</b>";
				}
				List<OrderItemModifier> itemModifiers = itemModifierRepository.findByOrderItemId(orderItem.getId());
				for (OrderItemModifier itemModifier : itemModifiers) {
					String modifiers = "<tr style='text-transform:capitalize;font-family:arial;margin-top:5px;font-size:12px'><td width='200px;'>"
							+ itemModifier.getModifiers().getName()
							+ "</td><td width='100px;' style='text-align:center'>" + itemModifier.getQuantity()
							+ " </td><td width='100px;' style='text-align:center'> " + "$"
							+ String.format("%.2f",itemModifier.getModifiers().getPrice()) + "</td></tr>";
					orderDetails = orderDetails + "" + modifiers;
				}
			}

			/*
			 * -------------------------------------------------------------For Pizza
			 * View-------------------------------------------------------------------------
			 * ---------
			 */
			List<OrderPizza> orderPizzas = orderPizzaRepository.findByOrderId(orderR.getId());
			if (!orderPizzas.isEmpty() && orderPizzas != null) {
				for (OrderPizza orderPizza : orderPizzas) {
					poduct = new HashMap<String, String>();
					if (orderPizza != null && orderPizza.getPizzaTemplate() != null) {

						Item item = new Item();
						if (orderPizza != null && orderPizza.getPizzaTemplate() != null
								&& orderPizza.getPizzaTemplate().getDescription() != null
								&& orderPizza.getPizzaSize() != null
								&& orderPizza.getPizzaSize().getDescription() != null) {
							item.setName(orderPizza.getPizzaTemplate().getDescription() + "("
									+ orderPizza.getPizzaSize().getDescription() + ")");

							String pizza = "<tr style='font-weight:600;text-transform:capitalize;font-family:arial'><td width='200px;'>"
									+ orderPizza.getPizzaTemplate().getDescription() + "("
									+ orderPizza.getPizzaSize().getDescription() + ")"
									+ "</td><td width='100px;' style='text-align:center'>" + orderPizza.getQuantity()
									+ "</td><td width='100px;' style='text-align:center'>" + "$"
									+ String.format("%.2f", String.format("%.2f",orderPizza.getPrice())) + "</td></tr>";
							orderDetails = orderDetails + "" + "<b>" + pizza + "</b>";

							poduct.put("product_id", orderPizza.getPizzaTemplate().getPosPizzaTemplateId());
							poduct.put("name", orderPizza.getPizzaTemplate().getDescription() + "("
									+ orderPizza.getPizzaSize().getDescription() + ")");
							poduct.put("price", Double.toString(orderPizza.getPrice()));
							poduct.put("qty", Integer.toString(orderPizza.getQuantity()));

						}
						item.setPrice(orderPizza.getPrice());
						List<OrderPizzaToppings> orderPizzaToppings = orderPizzaToppingsRepository
								.findByOrderPizzaId(orderPizza.getId());
						extraList = new ArrayList<Map<String, String>>();
						if (!orderPizzaToppings.isEmpty() && orderPizzaToppings != null) {
							for (OrderPizzaToppings pizzaToppings : orderPizzaToppings) {
								exctra = new HashMap<String, String>();
								Modifiers itemModifier = new Modifiers();
								if (pizzaToppings != null && pizzaToppings.getPizzaTopping() != null
										&& pizzaToppings.getPizzaTopping().getDescription() != null) {
									itemModifier.setName(pizzaToppings.getPizzaTopping().getDescription());
									itemModifier.setPrice(pizzaToppings.getPrice());

									String modifiers = "<tr style='text-transform:capitalize;font-family:arial;margin-top:5px;font-size:12px'><td width='200px;'>"
											+ pizzaToppings.getPizzaTopping().getDescription();

									if (pizzaToppings.getSide1() != null && pizzaToppings.getSide1()) {
										modifiers += " (First Half)";
									} else if (pizzaToppings.getSide1() != null && pizzaToppings.getSide2()) {
										modifiers += " (Second Half)";
									} else {
										modifiers += " (Full)";
									}
									modifiers += "</td><td width='100px;' style='text-align:center'>"
											+ orderPizza.getQuantity()
											+ " </td><td width='100px;' style='text-align:center'> " + "$"
											+ String.format("%.2f", pizzaToppings.getPrice()) + "</td></tr>";

									orderDetails = orderDetails + "" + modifiers;

									exctra.put("id", pizzaToppings.getPizzaTopping().getPosPizzaToppingId());
									exctra.put("price", Double.toString(pizzaToppings.getPrice()));
									exctra.put("name", pizzaToppings.getPizzaTopping().getDescription());
									exctra.put("qty", orderPizza.getQuantity().toString());
									extraList.add(exctra);
								}
							}
						}

						OrderPizzaCrust orderPizzaCrust = orderPizzaCrustRepository
								.findByOrderPizzaId(orderPizza.getId());
						if (orderPizzaCrust != null && orderPizzaCrust.getPizzacrust() != null) {
							exctra = new HashMap<String, String>();
							String crust = "<tr style='text-transform:capitalize;font-family:arial;margin-top:5px;font-size:12px'><td width='200px;'>"
									+ orderPizzaCrust.getPizzacrust().getDescription();

							crust += "</td><td width='100px;' style='text-align:center'>" + orderPizza.getQuantity()
									+ " </td><td width='100px;' style='text-align:center'> " + "$"
									+ String.format("%.2f", orderPizzaCrust.getPrice()) + "</td></tr>";
							orderDetails = orderDetails + "" + crust;

							exctra.put("id", orderPizzaCrust.getPizzacrust().getId().toString());
							exctra.put("price", Double.toString(orderPizzaCrust.getPrice()));
							exctra.put("name", orderPizzaCrust.getPizzacrust().getDescription());
							exctra.put("qty", orderPizza.getQuantity().toString());
						}
					}
					Gson gson = new Gson();
					if (extraList.size() > 0) {
						String extraJson = gson.toJson(extraList);
						poduct.put("extras", extraJson);
						productList.add(poduct);
					} else {
						productList.add(poduct);
					}
				}
			}

			/*
			 * -----------------------------------------------------------------------------
			 * -------------------------
			 * ---------------------------------------------------------
			 */

			orderDetails = "<table width='300px;'><tbody>" + orderDetails + "</table></tbody>";
			LOGGER.info("OrderServiceImpl : setOrderStatus : orderDetails : " + orderDetails);
			LOGGER.info(orderDetails);
			String deliveryFee = "0";
			if (orderR.getDeliveryFee() != null && !orderR.getDeliveryFee().isEmpty())
				deliveryFee = orderR.getDeliveryFee();

			Double orderDiscount = 0.0;
			String discountCoupon = "";
			if (orderR.getOrderDiscount() != null) {
				orderDiscount = orderR.getOrderDiscount();

				List<OrderDiscount> orderDiscounts = orderDiscountRepository.findByOrderId(orderR.getId());
				if (orderDiscounts != null && !orderDiscounts.isEmpty()) {
					discountCoupon = discountCoupon + "<tr>" + "<td>Discount Coupon</td>" + "<td>&nbsp;</td>" + "<td>"
							+ orderDiscounts.get(0).getCouponCode() + "</td>" + "</tr>";

					if (orderDiscounts.size() > 1) {
						for (int i = 1; i < orderDiscounts.size(); i++) {
							discountCoupon = discountCoupon + "<tr>" + "<td></td>" + "<td>&nbsp;</td>" + "<td>"
									+ orderDiscounts.get(i).getCouponCode() + "</td>" + "</tr>";
						}
					}
				}
			}
			
			mailSendUtil.sendConfirmMail(customerName, orderR.getPaymentMethod(), orderId, orderR.getSubTotal(),
					orderR.getTax(), orderDetails, orderR.getCustomer().getEmailId(), orderR.getOrderPrice(),
					orderR.getOrderNote(), orderR.getMerchant().getName(), merchantLogo, orderR.getOrderType(),
					convenienceFeeValue, orderAvgTime, deliveryFee, orderDiscount, orderR.getTipAmount(),
					orderR.getCustomer().getId(), orderR.getId(), posTypeId, discountCoupon, merchant.getId(),
					paymentGateWay);
			LOGGER.info("OrderServiceImpl : setOrderStatus : sendConfirmMail : " + orderId);
			storeOrderIntoFreekwent(orderR, merchant.getMerchantUid());
		} else {
			orderR.setIsDefaults(2);
			orderRepo.save(orderR);

			MailSendUtil.sendOrderCancellationMail(customerName, orderId, orderR.getCustomer().getEmailId(),
					orderR.getMerchant().getName(), merchantLogo, orderR.getMerchant().getPhoneNumber(),
					orderR.getMerchant().getOwner().getEmail(), reason, merchant.getId());
			LOGGER.info("OrderServiceImpl : setOrderStatus : sendOrderCancellationMail : " + orderId);
		}
		LOGGER.info("----------End :: OrderServiceImpl : setOrderStatus : return : true");
		return true;
	}

	/**
	 * find all orders by merchantId
	 */
	public List<OrderR> findAllOrdersByMerchantId(int merchantId) {
		LOGGER.info("OrderServiceImpl : findAllOrdersByMerchantId :");
		Merchant merchant = merchantRepository.findById(merchantId);
		Date date = (merchant != null && merchant.getTimeZone() != null
				&& merchant.getTimeZone().getTimeZoneCode() != null)
						? DateUtil.getCurrentDateForTimeZonee(merchant.getTimeZone().getTimeZoneCode())
						: new Date();
		List<OrderR> orderRs = orderRepo.findByMerchantId(merchantId);
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal = new GregorianCalendar();
		cal.add(Calendar.DAY_OF_MONTH, -12);
		Date today90 = cal.getTime();
		LOGGER.info("OrderServiceImpl :findAllOrdersByMerchantId: today90 :" + today90);
		String startDate = dateFormat.format(date);
		String endDate = dateFormat.format(today90);
		LOGGER.info(startDate + endDate);
		LOGGER.info(
				" OrderServiceImpl :findAllOrdersByMerchantId: startDate + endDate : " + startDate + " : " + endDate);
		Collections.sort(orderRs, new Comparator<OrderR>() {
			public int compare(OrderR m1, OrderR m2) {
				return m2.getCreatedOn().compareTo(m1.getCreatedOn());
			}
		});
		LOGGER.info("----------End :: OrderServiceImpl :findAllOrdersByMerchantId ");
		return findOrderDetails(orderRs);

	}

	@Transactional
	private List<OrderR> findOrderDetails(List<OrderR> orderRs) {
		LOGGER.info("===============  OrderServiceImpl : Inside findOrderDetails :: Start  ============= ");

		for (OrderR orderR : orderRs) {
			Merchant merchant = new Merchant();
			merchant.setId(orderR.getMerchant().getId());
			merchant.setName(orderR.getMerchant().getName());
			merchant.setAllowReOrder(orderR.getMerchant().getAllowReOrder());
			orderR.setMerchant(merchant);
			if (orderR.getAddressId() != null) {
				LOGGER.info(" === orderR.getAddressId() : " + orderR.getAddressId());
				Address address = addressRepository.findOne(orderR.getAddressId());
				if (address != null) {
					orderR.setAddress(address);
				}
			}
			// orderR
			// orderR.getMerchant().setAddresses(null);
			// orderR.setCustomer(null);
			List<OrderItemViewVO> orderItemViewVOs = new ArrayList<OrderItemViewVO>();
			LOGGER.info(" === orderR.getId() : " + orderR.getId());
			List<OrderItem> orderItems = orderItemRepository.findByOrderId(orderR.getId());

			for (OrderItem orderItem : orderItems) {
				if (orderItem != null) {
					if (orderItem.getItem() != null) {
						if (!orderItem.getItem().getName().equals("Convenience Fee")
								&& !orderItem.getItem().getName().equals("Delivery Fee")
								&& !orderItem.getItem().getName().equals("Online Fee")) {
							List<OrderItemModifierViewVO> itemModifierViewVOs = new ArrayList<OrderItemModifierViewVO>();
							OrderItemViewVO orderItemViewVO = new OrderItemViewVO();
							orderItemViewVO.setQuantity(orderItem.getQuantity());
							orderItem.getItem().setCategories(null);
							orderItem.getItem().setItemModifierGroups(null);
							orderItem.getItem().setTaxes(null);
							orderItem.getItem().setItemTimings(null);
							orderItem.getItem().setOrderItems(null);
							orderItem.getItem().setMerchant(null);
							orderItemViewVO.setItem(orderItem.getItem());
							LOGGER.info(" === orderItem.getId() : " + orderItem.getId());
							List<OrderItemModifier> itemModifiers = itemModifierRepository
									.findByOrderItemId(orderItem.getId());
							for (OrderItemModifier itemModifier : itemModifiers) {
								OrderItemModifierViewVO itemModifierViewVO = new OrderItemModifierViewVO();
								itemModifierViewVO.setQuantity(itemModifier.getQuantity());

								if (itemModifier.getModifiers() != null)
									itemModifier.getModifiers().setModifierGroup(null);
								itemModifier.getModifiers().setMerchant(null);
								itemModifierViewVO.setModifiers(itemModifier.getModifiers());
								itemModifierViewVOs.add(itemModifierViewVO);
							}
							orderItemViewVO.setItemModifierViewVOs(itemModifierViewVOs);

							orderItemViewVOs.add(orderItemViewVO);
						}
					}
				}
			}

			List<OrderPizza> orderPizzas = orderPizzaRepository.findByOrderId(orderR.getId());
			if (!orderPizzas.isEmpty() && orderPizzas != null) {
				for (OrderPizza orderPizza : orderPizzas) {

					if (orderPizza != null && orderPizza.getPizzaTemplate() != null) {
						orderPizza.setOrderPizzaToppings(null);

						List<OrderItemModifierViewVO> itemModifierViewVOs = new ArrayList<OrderItemModifierViewVO>();
						OrderItemViewVO orderItemViewVO = new OrderItemViewVO();
						orderItemViewVO.setQuantity(orderPizza.getQuantity());

						Item item = new Item();
						if (orderPizza != null && orderPizza.getPizzaTemplate() != null
								&& orderPizza.getPizzaTemplate().getDescription() != null
								&& orderPizza.getPizzaSize() != null
								&& orderPizza.getPizzaSize().getDescription() != null) {
							item.setName(orderPizza.getPizzaTemplate().getDescription() + "("
									+ orderPizza.getPizzaSize().getDescription() + ")");
						}
						// item.setName(orderPizza.getPizzaTemplate().getDescription() + "("
						// +orderPizza.getPizzaSize().getDescription() + ")");
						item.setPrice(orderPizza.getPrice());
						orderItemViewVO.setItem(item);
						LOGGER.info(" === orderPizza.getId() : " + orderPizza.getId());

						List<OrderPizzaToppings> orderPizzaToppings = orderPizzaToppingsRepository
								.findByOrderPizzaId(orderPizza.getId());
						if (!orderPizzaToppings.isEmpty() && orderPizzaToppings != null) {
							for (OrderPizzaToppings pizzaToppings : orderPizzaToppings) {
								OrderItemModifierViewVO itemModifierViewVO = new OrderItemModifierViewVO();
								itemModifierViewVO.setQuantity(pizzaToppings.getQuantity());

								Modifiers itemModifier = new Modifiers();
								if (pizzaToppings != null && pizzaToppings.getPizzaTopping() != null
										&& pizzaToppings.getPizzaTopping().getDescription() != null) {
									StringBuilder toppingName = new StringBuilder(
											pizzaToppings.getPizzaTopping().getDescription());
									if (pizzaToppings.getSide1() != null && pizzaToppings.getSide1()) {
										toppingName.append(" (First Half)");
									} else if (pizzaToppings.getSide2() != null && pizzaToppings.getSide2()) {
										toppingName.append(" (Second Half)");
									} else {
										toppingName.append(" (Full)");
									}
									itemModifier.setName(toppingName.toString());
									itemModifier.setPrice(pizzaToppings.getPrice());
								}
								itemModifierViewVO.setModifiers(itemModifier);
								itemModifierViewVOs.add(itemModifierViewVO);
							}
						}
						OrderPizzaCrust orderPizzaCrust = orderPizzaCrustRepository
								.findByOrderPizzaId(orderPizza.getId());
						if (orderPizzaCrust != null && orderPizzaCrust.getPizzacrust() != null) {

							OrderItemModifierViewVO itemModifierViewVO = new OrderItemModifierViewVO();
							itemModifierViewVO.setQuantity(orderPizzaCrust.getQuantity());
							Modifiers itemModifier = new Modifiers();

							itemModifier.setId(orderPizzaCrust.getPizzacrust().getId());
							itemModifier.setName(orderPizzaCrust.getPizzacrust().getDescription());
							itemModifier.setPrice(orderPizzaCrust.getPrice());
							itemModifier.setPosModifierId(orderPizzaCrust.getPizzacrust().getPosPizzaCrustId());

							itemModifierViewVO.setModifiers(itemModifier);
							itemModifierViewVOs.add(itemModifierViewVO);
						}

						orderItemViewVO.setItemModifierViewVOs(itemModifierViewVOs);
						orderItemViewVOs.add(orderItemViewVO);
					}

				}
			}

			List<OrderDiscount> orderDiscounts = orderDiscountRepository.findByOrderId(orderR.getId());
			for (OrderDiscount orderDiscount : orderDiscounts) {
				orderDiscount.setOrder(null);
				orderDiscount.setCustomer(null);
			}
			orderR.setOrderDiscountsList(orderDiscounts);
			orderR.setOrderItemViewVOs(orderItemViewVOs);
		}
		LOGGER.info("===============  OrderServiceImpl : Inside findOrderDetails :: End  ============= ");

		return orderRs;
	}

	/**
	 * Find by orderType
	 */
	public List<OrderR> findOrdersByOrderType(String orderType) {
		LOGGER.info("===============  OrderServiceImpl : Inside findOrdersByOrderType :: Start  ============= ");
		LOGGER.info(" ===orderType : " + orderType);
		LOGGER.info("===============  OrderServiceImpl : Inside findOrdersByOrderType :: End  ============= ");

		return orderRepo.findByOrderType(orderType);
	}

	/**
	 * Find by orderStatus
	 */
	public List<OrderR> findOrdersByStatus(String orderStatus) {
		LOGGER.info("===============  OrderServiceImpl : Inside findOrdersByStatus :: Start  ============= ");

		LOGGER.info(" ===orderStatus : " + orderStatus);

		int status = 0;
		if (orderStatus.equals("Pending")) {
			status = 0;
		} else {
			status = 1;
		}
		LOGGER.info(" === status : " + status);
		LOGGER.info("===============  OrderServiceImpl : Inside findOrdersByStatus :: End  ============= ");

		return orderRepo.findByIsDefaults(status);
	}

	/**
	 * Find by orderStatus and OrderType
	 */
	public List<OrderR> findOrdersByStatusAndOrderType(String orderStatus, String orderType) {
		LOGGER.info(
				"===============  OrderServiceImpl : Inside findOrdersByStatusAndOrderType :: Start  ============= ");
		LOGGER.info(" ===orderStatus : " + orderStatus + " orderType :" + orderType);

		int status = 0;
		if (orderStatus.equals("Pending")) {
			status = 0;
		} else {
			status = 1;
		}
		LOGGER.info(" === status : " + status);

		LOGGER.info("===============  OrderServiceImpl : Inside findOrdersByStatusAndOrderType :: End  ============= ");

		return orderRepo.findByIsDefaultsAndOrderType(status, orderType);
	}

	public List<OrderR> findOrdersByStatusAndOrderTypeAndDateRange(String orderStatus, String orderType,
			String startDate, String endDate) {
		LOGGER.info(
				"===============  OrderServiceImpl : Inside findOrdersByStatusAndOrderTypeAndDateRange :: Start  ============= ");
		LOGGER.info(" ===orderStatus : " + orderStatus + " orderType :" + orderType + " startDate : " + startDate
				+ " enddate : " + endDate);

		List<OrderR> orderRs = new ArrayList<OrderR>();
		int status = 0;
		if (orderStatus.equals("Pending")) {
			status = 0;
		} else {
			status = 1;
		}
		LOGGER.info(" === status : " + status);

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		List<OrderR> orders = orderRepo.findByIsDefaultsAndOrderType(status, orderType);
		try {
			for (OrderR orderR : orders) {
				Date sDate = sdf.parse(startDate);
				Date eDate = sdf.parse(endDate);
				if ((orderR.getCreatedOn().compareTo(sDate) > 0) && (eDate.compareTo(orderR.getCreatedOn()) > 0)) {
					orderRs.add(orderR);
				}
			}
		} catch (ParseException e) {
			LOGGER.error(
					"===============  OrderServiceImpl : Inside findOrdersByStatusAndOrderTypeAndDateRange :: Exception  ============= "
							+ e);

			MailSendUtil.sendExceptionByMail(e,environment);
			LOGGER.error("error: " + e.getMessage());
		}
		LOGGER.info(
				"===============  OrderServiceImpl : Inside findOrdersByStatusAndOrderTypeAndDateRange :: End  ============= ");

		return orderRs;
	}

	/**
	 * Find orders between two date
	 */
	public List<OrderR> findByOrderDate(String startDate, String endDate) {
		LOGGER.info("===============  OrderServiceImpl : Inside findByOrderDate :: Start  ============= ");
		LOGGER.info(" startDate : " + startDate + " enddate : " + endDate);
		List<OrderR> orderRs = new ArrayList<OrderR>();
		List<OrderR> orders = orderRepo.findAll();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		try {
			for (OrderR orderR : orders) {
				Date sDate = sdf.parse(startDate);
				Date eDate = sdf.parse(endDate);
				if ((orderR.getCreatedOn().compareTo(sDate) > 0) && (eDate.compareTo(orderR.getCreatedOn()) > 0)) {
					orderRs.add(orderR);
				}
			}
		} catch (ParseException e) {
			LOGGER.error("===============  OrderServiceImpl : Inside findByOrderDate :: Exception  ============= " + e);

			MailSendUtil.sendExceptionByMail(e,environment);
			LOGGER.error("error: " + e.getMessage());
		}
		LOGGER.info("===============  OrderServiceImpl : Inside findByOrderDate :: End  ============= ");

		return orderRs;
	}

	/**
	 * Find by orderType and orderDate
	 */
	public List<OrderR> findByOrderTypeAndOrderDate(String orderType, String startDate, String endDate) {
		LOGGER.info("===============  OrderServiceImpl : Inside findByOrderTypeAndOrderDate :: Start  ============= ");

		LOGGER.info(" orderType :" + orderType + " startDate : " + startDate + " enddate : " + endDate);
		List<OrderR> list = orderRepo.findByOrderType(orderType);
		List<OrderR> orderRs = new ArrayList<OrderR>();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		try {
			for (OrderR orderR : list) {
				Date sDate = sdf.parse(startDate);
				Date eDate = sdf.parse(endDate);
				if ((orderR.getCreatedOn().compareTo(sDate) > 0) && (eDate.compareTo(orderR.getCreatedOn()) > 0)) {
					orderRs.add(orderR);
				}
			}
		} catch (ParseException e) {
			LOGGER.error(
					"===============  OrderServiceImpl : Inside findByOrderTypeAndOrderDate :: Exception  ============= "
							+ e);

			MailSendUtil.sendExceptionByMail(e,environment);
			LOGGER.error("error: " + e.getMessage());
		}
		LOGGER.info("===============  OrderServiceImpl : Inside findByOrderTypeAndOrderDate :: End  ============= ");

		return orderRs;
	}

	/**
	 * Find by orderStatus and oderDate
	 */
	public List<OrderR> findByOrderStatusAndOrderDate(String orderStatus, String startDate, String endDate) {
		LOGGER.info(
				"===============  OrderServiceImpl : Inside findByOrderStatusAndOrderDate :: Start  ============= ");

		int status = 0;
		LOGGER.info(" orderStatus :" + orderStatus + " startDate : " + startDate + " enddate : " + endDate);

		if (orderStatus.equals("Pending")) {
			status = 0;
		} else {
			status = 1;
		}
		LOGGER.info(" === status : " + status);

		List<OrderR> orderRs = new ArrayList<OrderR>();
		List<OrderR> list = orderRepo.findByIsDefaults(status);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		try {
			for (OrderR orderR : list) {
				Date sDate = sdf.parse(startDate);
				Date eDate = sdf.parse(endDate);
				if ((orderR.getCreatedOn().compareTo(sDate) > 0) && (eDate.compareTo(orderR.getCreatedOn()) > 0)) {
					orderRs.add(orderR);
				}
			}
		} catch (ParseException e) {
			LOGGER.error(
					"===============  OrderServiceImpl : Inside findByOrderStatusAndOrderDate :: Exception  ============= "
							+ e);

			MailSendUtil.sendExceptionByMail(e,environment);
			LOGGER.error("error: " + e.getMessage());
		}
		LOGGER.info("===============  OrderServiceImpl : Inside findByOrderStatusAndOrderDate :: End  ============= ");

		return orderRs;
	}

	/**
	 * Find by customerId
	 */
	public List<OrderR> findOrderByCustomerId(int customerId) {
		LOGGER.info("===============  OrderServiceImpl : Inside findOrderByCustomerId :: Start  ============= ");
		LOGGER.info(" === customerId " + customerId);
		List<OrderR> orderRs = orderRepo.findByCustomerId(customerId);
		LOGGER.info("===============  OrderServiceImpl : Inside findOrderByCustomerId :: End  ============= ");

		return findOrderDetails(orderRs);
	}

	public List<OrderR> findOrderByCustomerIdd(int customerId) {
		LOGGER.info("===============  OrderServiceImpl : Inside findOrderByCustomerIdd :: Start  ============= ");

		LOGGER.info(" === customerId " + customerId);

		List<OrderR> orderRs = orderRepo.findByCustomerId(customerId);
		LOGGER.info("===============  OrderServiceImpl : Inside findOrderByCustomerIdd :: End  ============= ");

		return orderRs;
	}

	public Integer findNoOfDeliveryOrder(Integer merchantId) {
		LOGGER.info("===============  OrderServiceImpl : Inside findNoOfDeliveryOrder :: Start  ============= ");
		LOGGER.info(" === merchant : " + merchantId);
		LOGGER.info("===============  OrderServiceImpl : Inside findNoOfDeliveryOrder :: End  ============= ");

		return orderRepo.countUsersByMerchantIdAndOrderType(merchantId, "Delivery");
	}

	public Integer findNoOfPickUpOrder(Integer merchantId) {
		LOGGER.info("===============  OrderServiceImpl : Inside findNoOfPickUpOrder :: Start  ============= ");
		LOGGER.info(" === merchant : " + merchantId);

		Integer noOfPickUpOrder = orderRepo.countUsersByMerchantIdAndOrderType(merchantId, "Pickup");
		if (noOfPickUpOrder == null) {
			noOfPickUpOrder = 0;
		}
		LOGGER.info(" === noOfPickUpOrder : " + noOfPickUpOrder);
		LOGGER.info("===============  OrderServiceImpl : Inside findNoOfPickUpOrder :: End  ============= ");

		return noOfPickUpOrder;
	}

	public double findAverageOrderValue(Integer merchantId) {
		LOGGER.info("===============  OrderServiceImpl : Inside findAverageOrderValue :: Start  ============= ");
		LOGGER.info(" === merchant : " + merchantId);

		List<OrderR> orderRs = orderRepo.findByMerchantId(merchantId);
		double roundOff = 0;
		if (orderRs != null) {
			if (!orderRs.isEmpty()) {
				double totalValue = orderRepo.findSum(merchantId);
				int orderCount = orderRepo.countUsersByMerchantId(merchantId);
				double oValue = totalValue / orderCount;
				roundOff = Math.round(oValue * 100.0) / 100.0;
			}
		}
		LOGGER.info(" === roundOff : " + roundOff);
		LOGGER.info("===============  OrderServiceImpl : Inside findAverageOrderValue :: End  ============= ");

		return roundOff;
	}

	public int findtotalOrderValue(Integer merchantId) {
		LOGGER.info("===============  OrderServiceImpl : Inside findtotalOrderValue :: Start  ============= ");

		LOGGER.info(" === merchant : " + merchantId);

		List<OrderR> orderRs = orderRepo.findByMerchantId(merchantId);
		if (orderRs != null) {
			LOGGER.info(" ==== inside if (orderRs != null) ===== ");
			if (!orderRs.isEmpty()) {
				/*
				 * double totalValue = orderRepo.findSum(merchantId); roundOff =
				 * Math.round(totalValue * 100.0) / 100.0;
				 */
				LOGGER.info("===============  OrderServiceImpl : Inside findtotalOrderValue :: End  ============= ");

				return orderRs.size();
			}
		}
		LOGGER.info("===============  OrderServiceImpl : Inside findtotalOrderValue :: End  ============= ");

		return IConstant.IS_NOT_APPROVED;
	}

	public double findOrderFrequency(Integer merchantId, Integer vendorId) {
		LOGGER.info("===============  OrderServiceImpl : Inside findOrderFrequency :: Start  ============= ");

		LOGGER.info(" === merchant : " + merchantId + " vendorId : " + vendorId);

		List<OrderR> orderRs = orderRepo.findByMerchantId(merchantId);
		double roundOff = 0;
		if (orderRs != null) {
			LOGGER.info(" ==== inside if (orderRs != null) ===== ");

			if (!orderRs.isEmpty()) {
				int orderCount = orderRepo.countUsersByMerchantId(merchantId);
				int totalCustomer = customerrRepository.countByVendorId(merchantId);
				double oValue = 0;
				if (totalCustomer > 0)
					oValue = orderCount / totalCustomer;

				roundOff = Math.round(oValue * 100.0) / 100.0;
			}
		}
		LOGGER.info(" === roundOff : " + roundOff);
		LOGGER.info("===============  OrderServiceImpl : Inside findOrderFrequency :: End  ============= ");

		return roundOff;
	}

	public double findCustomerOrderAverage(Integer merchantId, Integer vendorId) {
		LOGGER.info("===============  OrderServiceImpl : Inside findCustomerOrderAverage :: Start  ============= ");
		LOGGER.info(" === merchant : " + merchantId + " vendorId : " + vendorId);

		List<OrderR> orderRs = orderRepo.findByMerchantId(merchantId);
		double roundOff = 0;
		if (orderRs != null) {
			LOGGER.info(" ==== inside if (orderRs != null) ===== ");

			if (!orderRs.isEmpty()) {
				double totalValue = orderRepo.findSum(merchantId);
				int totalCustomer = customerrRepository.countByVendorId(merchantId);
				double oValue = totalValue / totalCustomer;
				roundOff = Math.round(oValue * 100.0) / 100.0;
			}
		}
		LOGGER.info(" === roundOff : " + roundOff);

		LOGGER.info("===============  OrderServiceImpl : Inside findCustomerOrderAverage :: End  ============= ");

		return Math.round(roundOff);
	}

	public Integer findTotalCustomer(Integer merchantId, Integer vendorId) {
		LOGGER.info("===============  OrderServiceImpl : Inside findTotalCustomer :: Start  ============= ");

		LOGGER.info(" === merchant : " + merchantId + " vendorId : " + vendorId);

		LOGGER.info("===============  OrderServiceImpl : Inside findTotalCustomer :: End  ============= ");

		return customerrRepository.countByVendorId(merchantId);
	}

	/**
	 * Find trending item name
	 */
	public String findTrendingItem(Integer merchantId) {
		LOGGER.info("===============  OrderServiceImpl : Inside findTrendingItem :: Start  ============= ");

		LOGGER.info(" === merchant : " + merchantId);
		String itemName = null;
		List<Object> orderRs = orderRepo.findByMerId(merchantId);
		if (orderRs != null) {
			LOGGER.info(" ==== inside if (orderRs != null) ===== ");

			if (!orderRs.isEmpty()) {
				Object[] obj = (Object[]) orderRs.get(0);
				Integer itemId = (Integer) obj[0];
				itemName = itemmRepository.findItemNameByItemId(itemId);
			}
		}
		LOGGER.info(" ===itemName : " + itemName);
		LOGGER.info("===============  OrderServiceImpl : Inside findTrendingItem :: End  ============= ");

		return itemName;
	}

	public double findAverageNumberOfItemPerOrder(Integer merchantId, Integer vendorId) {
		LOGGER.info(
				"===============  OrderServiceImpl : Inside findAverageNumberOfItemPerOrder :: Start  ============= ");
		LOGGER.info(" === merchant : " + merchantId + " vendorId : " + vendorId);

		int numberOfItem = 0;
		double averageNumberOfItemPerOrder = 0;
		List<OrderR> orderRs = orderRepo.findByMerchantId(merchantId);
		if (orderRs != null) {
			LOGGER.info(" ==== inside if (orderRs != null) ===== ");

			if (!orderRs.isEmpty()) {
				for (OrderR orderR : orderRs) {
					List<Integer> orderItems = orderItemRepository.findDistinctItemIdByOrderId(orderR.getId());
					if (orderItems != null) {
						if (!orderItems.isEmpty()) {
							numberOfItem = numberOfItem + orderItems.size();
						}
					}
				}
				System.out.println(numberOfItem);
				double result = numberOfItem / (orderRs.size());
				averageNumberOfItemPerOrder = Math.round(result);
			}
		}
		LOGGER.info(" === averageNumberOfItemPerOrder : " + averageNumberOfItemPerOrder);
		LOGGER.info(
				"===============  OrderServiceImpl : Inside findAverageNumberOfItemPerOrder :: End  ============= ");

		return averageNumberOfItemPerOrder;
	}

	public List<String> findPaymentMode(Integer merchantId) {
		LOGGER.info("----------------Start :: OrderServiceImpl : findPaymentMode------------------------");
		LOGGER.info(" === merchant : " + merchantId);
		List<PaymentMode> modes = paymentModeRepository.findByMerchantIdAndAllowPaymentMode(merchantId, 1);
		List<String> paymentModes = new ArrayList<String>();
		if (modes != null) {
			if (!modes.isEmpty()) {
				for (PaymentMode mode : modes) {
					if ((mode.getLabelKey() != null && mode.getLabelKey().contains("cash"))
							|| (mode.getLabel() != null && mode.getLabel().equals("Cash"))) {
						paymentModes.add(mode.getLabel());
						LOGGER.info("OrderServiceImpl : findPaymentMode :paymentModes : " + mode.getLabel());
					}
					if ((mode.getLabelKey() != null && mode.getLabelKey().contains("credit_card"))
							|| (mode.getLabel() != null && mode.getLabel().equals("Credit Card"))) {
						paymentModes.add(mode.getLabel());
						LOGGER.info("OrderServiceImpl : findPaymentMode :paymentModes : " + mode.getLabel());
					}
				}
			}
		}

		LOGGER.info("----------------End :: OrderServiceImpl : findPaymentMode------------------------");
		return paymentModes;
	}

	public OrderType findByMerchantIdAndLabel(Integer merchantId, String orderType) {
		LOGGER.info("----------------Start :: OrderServiceImpl : findByMerchantIdAndLabel------------------------");
		OrderType orderTypes = null;

		LOGGER.info("OrderServiceImpl :: findByMerchantIdAndLabel : merchantId " + merchantId + " orderType : "
				+ orderType);

		if (orderType.toLowerCase().equals("pickup")) {
			orderTypes = orderTypeRepository.findByMerchantIdAndLabel(merchantId, "Foodkonnekt Online Pickup");
		} else if (orderType.toLowerCase().equals("delivery")) {
			orderTypes = orderTypeRepository.findByMerchantIdAndLabel(merchantId, "Foodkonnekt Online Delivery");
		}
		LOGGER.info("----------------End :: OrderServiceImpl : findByMerchantIdAndLabel------------------------");
		return orderTypes;
	}

	public void deleteAnOrder(Integer customerId, String orderPOSId) {
		LOGGER.info("===============  OrderServiceImpl : Inside deleteAnOrder :: Start  ============= ");
		LOGGER.info(" === customerId : " + customerId + " orderPOSId : " + orderPOSId);
		OrderR orderR = orderRepository.findByCustomerIdAndOrderPosId(customerId, orderPOSId);
		if (orderR != null) {
			LOGGER.info(" === orderR.getId() :  " + orderR.getId());
			List<OrderItem> orderItems = orderItemRepository.findByOrderId(orderR.getId());
			if (null != orderItems && !orderItems.isEmpty()) {
				for (OrderItem orderItem : orderItems) {
					LOGGER.info(" ===orderItemModifier to delete : " + orderItem.getId());
					List<OrderItemModifier> orderItemModifiers = orderItemModifierRepository
							.findByOrderItemId(orderItem.getId());
					orderItemModifierRepository.delete(orderItemModifiers);
					orderItem.setOrder(null);
					orderItem.setItem(null);
				}
			}

			List<OrderPizza> orderPizzas = orderPizzaRepository.findByOrderId(orderR.getId());
			if (null != orderPizzas && !orderPizzas.isEmpty()) {
				for (OrderPizza orderPizza : orderPizzas) {
					LOGGER.info(" ===orderPizzaCrusts to delete : " + orderPizza.getId());

					OrderPizzaCrust orderPizzaCrusts = orderPizzaCrustRepository.findByOrderPizzaId(orderPizza.getId());
					if (orderPizzaCrusts != null) {
						orderPizzaCrustRepository.delete(orderPizzaCrusts);
					}
					LOGGER.info(" ===orderPizzaToppings to delete : " + orderPizza.getId());

					List<OrderPizzaToppings> orderPizzaToppings = orderPizzaToppingsRepository
							.findByOrderPizzaId(orderPizza.getId());
					if (!orderPizzaToppings.isEmpty() && orderPizzaToppings.size() > 0) {
						orderPizzaToppingsRepository.delete(orderPizzaToppings);
					}
					orderPizza.setOrder(null);
					orderPizza.setPizzaTemplate(null);
				}
			}
			orderR.setMerchant(null);
			orderR.setCustomer(null);
			orderItemRepository.delete(orderItems);
			orderPizzaRepository.delete(orderPizzas);
			orderRepository.delete(orderR);
		}
	}

	public void run() {
		try {
			Thread.sleep(4000);
		} catch (InterruptedException exception) {
			MailSendUtil.sendExceptionByMail(exception,environment);
			exception.printStackTrace();
		}

		try {
			mailSendUtil.placeOrderMail(name1, paymentYtpe1, orderPosId1, subTotal1, tax1, orderDetails1, email1,
					orderPrice1, note1, merchantName1, merchantLogo1, orderType1, convenienceFeeValue1, avgTime,
					deliverFee, orderDiscount, tipAmount, custId, orderId, posTypeId, listOfCoupons, merchantId,
					isfutureorder,fullfilledOn,storeOpenstatus);
			LOGGER.info(" Order mail thread over");
		} catch (Exception e) {
			MailSendUtil.sendExceptionByMail(e,environment);
			LOGGER.info(" Invalid Emailid");
		}
	}

	/**
	 * Find calculated convenience fee with tax
	 */
	public Double findConvenienceFeeAfterTax(String convenienceFee, Integer merchantId) {
		LOGGER.info("----------------Start :: OrderServiceImpl : findConvenienceFeeAfterTax------------------------");
		LOGGER.info(" ===convenienceFee : " + convenienceFee + " merchantId : " + merchantId);
		Double totalConvenienceFeeTax = 0.0;
		try {
			List<TaxRates> taxRates = taxRateRepository.findByMerchantIdAndIsDefault(merchantId,
					IConstant.BOOLEAN_TRUE);
			for (TaxRates rates : taxRates) {
				Double conveniceFeeTax = (Double.valueOf(convenienceFee) * rates.getRate()) / 100;
				totalConvenienceFeeTax = totalConvenienceFeeTax + Math.round(conveniceFeeTax * 100.0) / 100.0;
			}
		} catch (Exception e) {
			if (e != null) {
				LOGGER.error("OrderServiceImpl :: findConvenienceFeeAfterTax : Exception " + e);

				MailSendUtil.sendExceptionByMail(e,environment);
			}
			LOGGER.error("error: " + e.getMessage());
		}
		LOGGER.info("----------------End :: OrderServiceImpl : findConvenienceFeeAfterTax: totalConvenienceFeeTax : "
				+ totalConvenienceFeeTax);
		return totalConvenienceFeeTax;
	}

	public String findConvenienceFeeAfterMultiTax(String convenienceFee, Integer merchantId) {
		String totalConvenienceFeeTax = "";
		try {
			LOGGER.info(
					"===============  OrderServiceImpl : Inside findConvenienceFeeAfterMultiTax :: Start  ============= ");
			LOGGER.info(" ===convenienceFee : " + convenienceFee + " merchantId : " + merchantId);

			List<TaxRates> taxRates = taxRateRepository.findByMerchantIdAndIsDefault(merchantId,
					IConstant.BOOLEAN_TRUE);
			for (TaxRates rates : taxRates) {
				totalConvenienceFeeTax = totalConvenienceFeeTax + "," + rates.getName();
			}
		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
			}
			LOGGER.error(
					"===============  OrderServiceImpl : Inside findConvenienceFeeAfterMultiTax :: Exception  ============= "
							+ e);

			LOGGER.error("error: " + e.getMessage());
		}
		LOGGER.info(" ===totalConvenienceFeeTax : " + totalConvenienceFeeTax);
		LOGGER.info(
				"===============  OrderServiceImpl : Inside findConvenienceFeeAfterMultiTax :: End  ============= ");

		return totalConvenienceFeeTax;
	}

	public List<ItemDto> findCategoryItems(Integer categoryId, Merchant merchant, Integer offSet) {
		LOGGER.info("===============  OrderServiceImpl : Inside findCategoryItems :: Start  ============= ");
		LOGGER.info(" ===categoryId : " + categoryId + " merchant : " + merchant.getId() + " offSet : " + offSet);
		String timeZoneCode = "America/Chicago";
		if (merchant != null && merchant.getTimeZone() != null && merchant.getTimeZone().getTimeZoneCode() != null) {

			timeZoneCode = merchant.getTimeZone().getTimeZoneCode();
		}
		String currentDay = DateUtil.getCurrentDayForTimeZone(timeZoneCode);
		String currentTime = DateUtil.getCurrentTimeForTimeZone(timeZoneCode);

		LOGGER.info(" === currentDay : " + currentDay + " currentTime : " + currentTime);
		// List<CategoryItem> categoryItems
		// =categoryItemRepository.findByCategoryIdOrderBySortOrderAsc(categoryId);

		List<Item> items = null;
		if (offSet == null) {
			items = itemmRepository.findItemByCategoryIdAndActive(categoryId, currentDay, currentTime, currentTime);
		} else {
			items = itemmRepository.findItemByCategoryIdV2AndActive(categoryId, currentDay, currentTime, currentTime, offSet);
		}
		List<ItemDto> itemDtos = new ArrayList<ItemDto>();
		try {
			if (items.size() != 0) {
				for (Item item : items) {

					// Item item=categoryItem.getItem();
					if (item != null) {
						boolean itemTimingStatus = true;
						/*
						 * if(item.getAllowItemTimings()==IConstant. BOOLEAN_TRUE) { ItemTiming
						 * itemTiming=itemTimingRepository. findByItemIdAndDayAndStartTimeAndEndTime
						 * (currentDay,item. getId(),currentTime,currentTime);
						 * 
						 * if(itemTiming!=null ){ if(itemTiming.isHoliday()){ itemTimingStatus=false;
						 * }else{ String startTime=DateUtil .convert12hoursTo24HourseFormate(itemTiming
						 * .getStartTime()); String endTime=DateUtil.convert12hoursTo24HourseFormate
						 * (itemTiming.getEndTime()); itemTimingStatus=OrderUtil.
						 * isTimeBetweenTwoTime(startTime + ":00", endTime + ":00",currentTime);
						 * itemTimingStatus=true; } }else{ itemTimingStatus=false; }}
						 */
						if (itemTimingStatus) {
							String taxName = null;
							Double tax = 0.0;
							LOGGER.info(" ===item : " + item.getId());
							Map<String, Object> itemNameAndPrice = getItemTaxNameAndPrice(item.getId());
							if (itemNameAndPrice.get("taxNames") != null && itemNameAndPrice.get("tax") != null) {
								taxName = (String) itemNameAndPrice.get("taxNames");
								tax = (Double) itemNameAndPrice.get("tax");
							}
							if (item.getItemStatus() != null && item.getItemStatus() == 0 && taxName != null
									&& !taxName.isEmpty()) {
								ItemDto itemDto = new ItemDto();
								itemDto.setId(item.getId());
								itemDto.setItemName(item.getName());
								itemDto.setItemTax(getItemTaxPrice(item.getId()));
								itemDto.setPrice(item.getPrice());
								itemDto.setItemPosId(item.getPosItemId());
								itemDto.setItemImage(item.getItemImage());
								if (item != null && item.getTaxAble() != null) {
									itemDto.setTaxAble(1);
								} else {
									itemDto.setTaxAble(0);
								}
								if (item.getAuxTaxAble() != null && item.getAuxTaxAble()) {
									if (merchant.getAllowAuxTax() == 1) {
										itemDto.setAuxTaxAble(1);
									} else {
										itemDto.setAuxTaxAble(0);
									}
								} else {
									itemDto.setAuxTaxAble(0);
								}
								if (merchant != null && merchant.getOwner() != null
										&& merchant.getOwner().getPos() != null
										&& merchant.getOwner().getPos().getPosId() != null
										&& merchant.getOwner().getPos().getPosId() == IConstant.NON_POS) {
									itemDto.setItemPosId(Integer.toString(item.getId()));
								}
								itemDto.setAllowModifierLimit(item.getAllowModifierLimit());
								itemDto.setItemTaxName(taxName);
								if (item.getDescription() != null) {
									itemDto.setDescription(item.getDescription());
								} else {
									itemDto.setDescription("");
								}
								if (merchant.getOwner() != null && merchant.getOwner().getPos() != null
										&& merchant.getOwner().getPos().getPosId() != null
										&& merchant.getOwner().getPos().getPosId() == 1) {
									if (itemDto != null && itemDto.getItemPosId() != null
											&& !itemDto.getItemPosId().isEmpty())
										itemDtos.add(itemDto);
								} else
									itemDtos.add(itemDto);
							}
						}
					}
				}
			}
		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
			}
			LOGGER.error(
					"===============  OrderServiceImpl : Inside findCategoryItems :: Exception  ============= " + e);

			LOGGER.error("error: " + e.getMessage());
		}
		LOGGER.info("===============  OrderServiceImpl : Inside findCategoryItems :: End  ============= ");

		return itemDtos;
	}

	private String getItemTaxName(Integer itemId) {
		LOGGER.info("===============  OrderServiceImpl : Inside getItemTaxName :: Start  ============= ");
		LOGGER.info(" === itemId : " + itemId);
		String taxNames = null;
		try {
			if (itemId != null) {
				List<ItemTax> itemTaxs = itemTaxRepository.findByItemId(itemId);
				if (itemTaxs != null && !itemTaxs.isEmpty()) {
					taxNames = "";
					for (ItemTax itemTax : itemTaxs) {
						taxNames = taxNames + "," + itemTax.getTaxRates().getName();
					}
					taxNames = taxNames.trim();
				}
			}
			LOGGER.info(" === taxNames : " + taxNames);
			LOGGER.info("===============  OrderServiceImpl : Inside getItemTaxName :: End  ============= ");

			return taxNames;
		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
			}
			LOGGER.error("===============  OrderServiceImpl : Inside getItemTaxName :: Exception  ============= " + e);

			LOGGER.info(" === taxNames : " + taxNames);

			return taxNames;
		}
	}

	private Map<String, Object> getItemTaxNameAndPrice(Integer itemId) {
		LOGGER.info("===============  OrderServiceImpl : Inside getItemTaxNameAndPrice :: Start  ============= ");
		LOGGER.info(" === itemId : " + itemId);

		Map<String, Object> itemNameAndPrice = new HashMap<String, Object>();
		String taxNames = null;
		double tax = 0;

		try {
			if (itemId != null) {
				List<ItemTax> itemTaxs = itemTaxRepository.findByItemIdAndActive(itemId,1);
				if (itemTaxs != null && !itemTaxs.isEmpty()) {
					taxNames = "";
					for (ItemTax itemTax : itemTaxs) {
						taxNames = taxNames + "," + itemTax.getTaxRates().getName().trim();
						tax = tax + itemTax.getTaxRates().getRate();
					}
					taxNames = taxNames.trim();
					itemNameAndPrice.put("taxNames", taxNames);
					itemNameAndPrice.put("tax", tax);
				}
			}
			LOGGER.info("===============  OrderServiceImpl : Inside getItemTaxNameAndPrice :: End  ============= ");

			return itemNameAndPrice;
		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
			}
			LOGGER.error(
					"===============  OrderServiceImpl : Inside getItemTaxNameAndPrice :: Exception  ============= "
							+ e);

			return itemNameAndPrice;
		}
	}

	public List<ModifierGroupDto> findItemModifierGroups(Integer itemId, Integer allowModifierLimit) {
		// TODO Auto-generated method stub
		LOGGER.info("===============  OrderServiceImpl : Inside findItemModifierGroups :: Start  ============= ");

		LOGGER.info(" === itemId : " + itemId + " allowModifierLimit : " + allowModifierLimit);
		LOGGER.info("===============  OrderServiceImpl : Inside findItemModifierGroups :: End  ============= ");

		return null;
	}

	public List<Address> findAddessByCustomerId(Integer customerId) {
		LOGGER.info("===============  OrderServiceImpl : Inside findAddessByCustomerId :: Start  ============= ");
		LOGGER.info(" === customerId : " + customerId);
		Customer customer = customerrRepository.findOne(customerId);
		List<Address> finalAddresses = new ArrayList<Address>();
		try {
			List<Address> addresses = addressRepository.findByCustomerId(customerId);
			for (Address address : addresses) {
				Address address2 = new Address();
				address2.setId(address.getId());
				address2.setAddress1(address.getAddress1());
				address2.setAddress2(address.getAddress2());
				address2.setAddress3(address.getAddress3());
				address2.setCity(address.getCity());
				address2.setState(address.getState());
				address2.setZip(address.getZip());
				LOGGER.info(" === customer.getMerchantt().getId() : " + customer.getMerchantt().getId());
				List<Zone> zones = zoneRepository.findByMerchantId(customer.getMerchantt().getId());
				double distance;
				final double MILES_PER_KILOMETER = 0.621;
				for (Zone zone : zones) {
					CustomerServiceImpl customerServiceImpl = new CustomerServiceImpl();
					distance = customerServiceImpl.checkDelivery(address, zone.getAddress());
					double miles = distance * MILES_PER_KILOMETER;
					LOGGER.info(" === distance : " + distance + " miles : " + miles);
					LOGGER.info(" === zone.getZoneDistance()  : " + zone.getZoneDistance());
					if (zone.getZoneDistance() != null) {
						if (miles <= zone.getZoneDistance()) {
							address2.setDeliveryFee(zone.getDeliveryFee());
							address2.setDeliveryPosId(zone.getDeliveryLineItemPosId());
						}
					}
				}
				finalAddresses.add(address2);
			}
		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
			}
			LOGGER.error(
					"===============  OrderServiceImpl : Inside findAddessByCustomerId :: Exception  ============= "
							+ e);

			LOGGER.error("error: " + e.getMessage());
		}
		LOGGER.info("===============  OrderServiceImpl : Inside findAddessByCustomerId :: End  ============= ");

		return finalAddresses;
	}

	public String findMerchantTaxs(Integer merchantId) {
		LOGGER.info("----------------Start :: OrderServiceImpl : findMerchantTaxs------------------------");

		String merchantTaxs = "";
		try {
			LOGGER.info("OrderServiceImpl :: findMerchantTaxs : merchantId " + merchantId);
			List<TaxRates> taxRates = taxRateRepository.findByMerchantId(merchantId);
			for (TaxRates rates : taxRates) {
				rates.setMerchant(null);
				rates.setItems(null);
				if (rates.getName() != null)
					rates.setName(rates.getName().trim());
			}
			Gson gson = new Gson();
			merchantTaxs = gson.toJson(taxRates);
		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
				LOGGER.error("OrderServiceImpl :: findMerchantTaxs : Exception " + e);
			}
			LOGGER.error("error: " + e.getMessage());
		}
		LOGGER.info("----------------End :: OrderServiceImpl : findMerchantTaxs------------------------");
		return merchantTaxs;
	}

	public List<OrderR> findAllOrdersByFulfilledDate(Date currentDate, Merchant merchant) {
		LOGGER.info("===============  OrderServiceImpl : Inside findAllOrdersByFulfilledDate :: Start  ============= ");
		LOGGER.info(" === currentDate : " + currentDate + " merchantId : " + merchant.getId());
		String timeZoneCode = (merchant.getTimeZone() != null && merchant.getTimeZone().getTimeZoneCode() != null)
				? merchant.getTimeZone().getTimeZoneCode()
				: "CST";
		List<OrderR> orderRs = null;
		try {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sdf.setTimeZone(TimeZone.getTimeZone(timeZoneCode));
        String currentTime = sdf.format(currentDate);
        Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(currentTime);
       
		
                SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
		String fulfilledDate = sdf2.format(date);
		LOGGER.info("===============  fullfilledDate : "+fulfilledDate);
		orderRs = orderRepo.findOrdersByfulfilledDate(fulfilledDate, merchant.getId());
		}catch (Exception e) {
			LOGGER.info("Exception in findAllOrdersByFulfilledDate : e : "+e);
		}
		LOGGER.info("===============  OrderServiceImpl : Inside findAllOrdersByFulfilledDate :: End  ============= ");

		return orderRs;
	}

	public OrderR findOrderByOrderID(String id) {
		LOGGER.info("===============  OrderServiceImpl : Inside findOrderByOrderID :: Start  ============= ");

		LOGGER.info(" === id : " + id);
		LOGGER.info("===============  OrderServiceImpl : Inside findOrderByOrderID :: End  ============= ");

		return orderRepository.findByOrderPosId(String.valueOf(id));
	}

	public OrderR findOrderByID(Integer id) {
		LOGGER.info("===============  OrderServiceImpl : Inside findOrderByID :: Start  ============= ");

		LOGGER.info(" === id : " + id);
		LOGGER.info("===============  OrderServiceImpl : Inside findOrderByID :: End  ============= ");

		return orderRepository.findOne(id);
	}

	public String getNotificationJSON(int orderId, String deviceId, String locationUid, String merchantPosId) {
		LOGGER.info("===============  OrderServiceImpl : Inside getNotificationJSON :: Start  ============= ");
		LOGGER.info(" === orderId : " + orderId + " deviceId : " + deviceId + " locationUid : " + locationUid
				+ " merchantPosId : " + merchantPosId);
		NotificationVO response = new NotificationVO();
		OrderR orderR = null;
		try {
			orderR = findOrderDetailById(orderId);
			if(orderR != null) {
				LOGGER.info("===============  inside if(orderR != null)");
			Customer customer = orderR.getCustomer();
			// customer.setAddress1(null);
			// customer.setId(null);
			// customer.setAddresses(null);
			if(customer != null) {
			LOGGER.info(" === customer : " + customer.getId());
			List<Address> addresses=new ArrayList<Address>();
	           Address addres =null;
	           if(orderR.getAddressId()!=null)
	        	   addres = addressRepository.findOne(orderR.getAddressId());
	            if(addres==null)
	            {
	              addresses = addressRepository.findByCustomerId(customer.getId());
	            }else addresses.add(addres);
	            for (Address address : addresses) {
	                Address address2 = new Address();
	                address2.setId(address.getId());
	                address2.setAddress1(address.getAddress1());
	                address2.setAddress2(address.getAddress2());
	                address2.setAddress3(address.getAddress3());
	                address2.setCity(address.getCity());
	                address2.setState(address.getState());
	                address2.setZip(address.getZip());
	                address.setCustomer(null);
	                address.setMerchant(null);
	                address.setIsDeliveryKoupon(null);
	                address.setZones(null);
	            }
	            customer.setAddresses(addresses);


			customer.setMerchantt(null);

			customer.setVendor(null);
			customer.setCreatedDate(null);
			customer.setUpdatedDate(null);
			customer.setPassword(null);
			customer.setAnniversaryDate(null);
			customer.setBirthDate(null);
		}
			orderR.setCustomer(customer);
			orderR.setMerchant(null);
			orderR.setVirtualFund(null);
			response.setDeviceId(deviceId);
			response.setLocationUid(locationUid);
			response.setMerchantId(merchantPosId);
			Payload payload = new Payload();
			if(orderR.isCommunicated()==null)
			orderR.setCommunicated(false);
			payload.setOrderR(orderR);
			response.setPayload(payload);
			Gson gson = new Gson();
			String extraJson = gson.toJson(response);
			return extraJson;
			}else
				return "";
			} catch (Exception exception) {
			LOGGER.error("===============  OrderServiceImpl : Inside getNotificationJSON :: Exception  ============= "
					+ exception);
            if (exception != null) {
				MailSendUtil.sendExceptionByMail(exception,environment);
			}
			
			return null;
		}
	}

	/*
	 * public OrderR findOrderDetailById(Integer id) { OrderR orderR =
	 * orderRepository.findOne(id); List<OrderItem> items = new
	 * ArrayList<OrderItem>(); List<OrderItem> orderItems =
	 * orderItemRepository.findByOrderId(id); if (orderItems != null &&
	 * orderItems.size() > 0) { for (OrderItem orderItem : orderItems) {
	 * 
	 * if (orderItem.getItem() != null) { orderItem.setOrder(null); Item item =
	 * orderItem.getItem(); item.setMerchant(null); item.setModifierGroups(null);
	 * item.setItemTimings(null); item.setCategories(null); item.setTaxes(null);
	 * item.setCategoryList(null); item.setExtras(null); item.setOrderItems(null);
	 * item.setItemModifierGroups(null); List<OrderItemModifier> orderItemModifiers
	 * = orderItemModifierRepository .findByOrderItemId(orderItem.getId()); for
	 * (OrderItemModifier itemModifier : orderItemModifiers) {
	 * itemModifier.setOrderItem(null); if (itemModifier.getModifiers() != null) {
	 * itemModifier.getModifiers().setMerchant(null);
	 * itemModifier.getModifiers().setModifierGroup(null);
	 * 
	 * }
	 * 
	 * } orderItem.setOrderItemModifiers(orderItemModifiers); items.add(orderItem);
	 * }
	 * 
	 * } orderR.setOrderItems(items); }
	 * 
	 * return orderR; }
	 */

	public void saveOrder(OrderR order) {
		LOGGER.info("===============  OrderServiceImpl : Inside saveOrder :: Start  ============= ");
		LOGGER.info(" === order : " + order.getId());
		orderRepo.save(order);

		LOGGER.info("===============  OrderServiceImpl : Inside saveOrder :: End  ============= ");

	}

	public void sendMailUser(OrderR orderR, String reason, String time) {
		LOGGER.info("===============  OrderServiceImpl : Inside sendMailUser :: Start  ============= ");
		LOGGER.info(" === orderR : " + orderR.getId() + " reason : " + reason + " time : " + time);
		MailSendUtil.sendUpdatedTime(orderR, reason, time,environment);
		LOGGER.info("===============  OrderServiceImpl : Inside sendMailUser :: End  ============= ");

	}

	/*
	 * public String findAllOrderFromDataTable(Integer merchantId, Integer
	 * pageDisplayLength, Integer pageNumber, String searchParameter) { Pageable
	 * pageable = new PageRequest(pageNumber - 1, pageDisplayLength,
	 * Sort.Direction.DESC, "id"); Page<OrderR> orderRs =
	 * orderRepo.findByMerchantId(merchantId, pageable); List<AllOrderVo>
	 * allOrderVos = new ArrayList<AllOrderVo>(); for (OrderR orderR :
	 * orderRs.getContent()) { AllOrderVo allOrderVo = new AllOrderVo();
	 * allOrderVo.setDT_RowId("row_" + orderR.getId());
	 * allOrderVo.setId(orderR.getId());
	 * allOrderVo.setFirstName(orderR.getCustomer().getFirstName());
	 * allOrderVo.setCreatedOn(orderR.getCreatedOn()); Double roundOff =
	 * Math.round(orderR.getOrderPrice()*100)/100.0;
	 * allOrderVo.setOrderPrice(roundOff);
	 * allOrderVo.setConvenienceFee(orderR.getConvenienceFee());
	 * allOrderVo.setDeliveryFee(orderR.getDeliveryFee());
	 * allOrderVo.setTax(orderR.getTax()); if(orderR.getOrderDiscount()!=null &&
	 * orderR.getOrderDiscount()>0)
	 * allOrderVo.setDiscount(orderR.getOrderDiscount());
	 * allOrderVo.setSubTotal(orderR.getSubTotal());
	 * allOrderVo.setTipAmount(orderR.getTipAmount()); if
	 * (orderR.getOrderNote()==null || orderR.getOrderNote().isEmpty()) {
	 * allOrderVo.setOrderName(""); } else {
	 * allOrderVo.setOrderName(orderR.getOrderNote()); }
	 * allOrderVo.setOrderType(orderR.getOrderType()); if (orderR.getIsDefaults() ==
	 * 1) { allOrderVo.setStatus("Confirmed"); } else { if (orderR.getIsDefaults()
	 * == 2) { allOrderVo.setStatus("Cancelled"); } else {
	 * allOrderVo.setStatus("Pending"); } }
	 * 
	 * // order items List<OrderItemViewVO> orderItemViewVOs = new
	 * ArrayList<OrderItemViewVO>(); List<OrderItem> orderItems =
	 * orderItemRepository.findByOrderId(orderR.getId()); for (OrderItem orderItem :
	 * orderItems) { if (orderItem != null) { if (orderItem.getItem() != null) { if
	 * (!orderItem.getItem().getName().equals( "Convenience Fee") &&
	 * !orderItem.getItem().getName().equals("Delivery Fee")) {
	 * List<OrderItemModifierViewVO> itemModifierViewVOs = new
	 * ArrayList<OrderItemModifierViewVO>(); OrderItemViewVO orderItemViewVO = new
	 * OrderItemViewVO(); orderItemViewVO.setQuantity(orderItem.getQuantity());
	 * ItemVO itemVO = new ItemVO();
	 * itemVO.setPrice(orderItem.getItem().getPrice());
	 * itemVO.setName(orderItem.getItem().getName());
	 * orderItemViewVO.setItemVO(itemVO); orderItem.setItem(null);
	 * 
	 * List<OrderItemModifier> itemModifiers =
	 * itemModifierRepository.findByOrderItemId(orderItem .getId()); for
	 * (OrderItemModifier itemModifier : itemModifiers) { if
	 * (itemModifier.getModifiers() != null) { OrderItemModifierViewVO
	 * itemModifierViewVO = new OrderItemModifierViewVO();
	 * itemModifierViewVO.setQuantity(itemModifier.getQuantity());
	 * 
	 * itemModifier.getModifiers().setModifierGroup(null);
	 * itemModifier.getModifiers().setMerchant(null);
	 * 
	 * itemModifierViewVO.setModifiers(itemModifier.getModifiers());
	 * itemModifierViewVOs.add(itemModifierViewVO); } }
	 * orderItemViewVO.setItemModifierViewVOs(itemModifierViewVOs);
	 * orderItemViewVOs.add(orderItemViewVO); } } } }
	 * allOrderVo.setOrderItemViewVOs(orderItemViewVOs); // order items end
	 * 
	 * allOrderVo.setView(
	 * "<p><button class='md-trigger custom-sd-button-dialog' id='viewOrderDetailPopup' data-modal= modal-15-"
	 * + orderR.getId() + " orderAttr=" + orderR.getId() + ">View</button></p>");
	 * 
	 * allOrderVos.add(allOrderVo); } OrderJson orderJosn = new OrderJson();
	 * orderJosn.setiTotalDisplayRecords((int)orderRs.getTotalElements());
	 * orderJosn.setiTotalRecords((int)orderRs.getTotalElements());
	 * orderJosn.setAaData(allOrderVos); Gson gson = new
	 * GsonBuilder().setPrettyPrinting().create(); return gson.toJson(orderJosn); }
	 */

	public String findAllOrderFromDataTable(Integer merchantId, Integer pageDisplayLength, Integer pageNumber,
			String searchParameter, String startDate, String endDate) {
		LOGGER.info("===============  OrderServiceImpl : Inside findAllOrderFromDataTable :: Start  ============= ");
		LOGGER.info(" merchantId : " + merchantId + " pageDisplayLength : " + pageDisplayLength + " pageNumber : "
				+ pageNumber + " searchParameter : " + searchParameter + " startDate : " + startDate + " enddate : "
				+ endDate);

		java.util.Date startDateRange = null;
		java.util.Date endDateRange = null;
		PaymentGateWay paymentGateWay = null;

		paymentGateWay = paymentGateWayRepository.findByMerchantIdAndIsDeletedAndIsActive(merchantId, false, true);
		try {
			if (startDate != null && endDate != null) {

				startDateRange = new SimpleDateFormat(IConstant.YYYYMMDD).parse(startDate);
				// endDateRange=new
				// SimpleDateFormat(IConstant.YYYYMMDD).parse(endDate);

				SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
				Calendar cal = Calendar.getInstance();
				cal.setTime(dateFormat.parse(endDate));
				cal.add(Calendar.DATE, 1);
				// endDate = dateFormat.format(cal.getTime());
				endDateRange = new SimpleDateFormat(IConstant.YYYYMMDD).parse(dateFormat.format(cal.getTime()));
			}
		} catch (ParseException e) {
			LOGGER.error(
					"===============  OrderServiceImpl : Inside findAllOrderFromDataTable :: Exception  ============= "
							+ e);

			// TODO Auto-generated catch block
			MailSendUtil.sendExceptionByMail(e,environment);
			LOGGER.error("error: " + e.getMessage());
		}
		Pageable pageable = new PageRequest(pageNumber - 1, pageDisplayLength, Sort.Direction.DESC, "id");
		Page<OrderR> orderRs = orderRepo.findByMerchantId(merchantId, pageable, startDateRange, endDateRange);
		List<AllOrderVo> allOrderVos = new ArrayList<AllOrderVo>();
		for (OrderR orderR : orderRs.getContent()) {
			LOGGER.info(" === orderR : " + orderR.getId());
			List<NotificationTracker> notificationTracker = notificatonRepo.findByOrderIdAndMerchantId(orderR.getId(),
					merchantId);
			if (orderR.getIsDefaults() != 3) {
				LOGGER.info("orderR.getSubTotal() ==  " + orderR.getSubTotal());
				AllOrderVo allOrderVo = new AllOrderVo();
				if (orderR.getSubTotal() != null && orderR.getIsDefaults() != null && orderR.getIsDefaults() != 3) {
					if (notificationTracker != null && !notificationTracker.isEmpty())
						allOrderVo.setNotificationTracker(notificationTracker);
					else
						allOrderVo.setNotificationTracker(null);

					allOrderVo.setDT_RowId("row_" + orderR.getId());
					allOrderVo.setId(orderR.getId());
					/*
					 * if (orderR.getCustomer() != null && orderR.getCustomer().getFirstName() !=
					 * null) { allOrderVo.setFirstName(orderR.getCustomer().getFirstName()); }else{
					 * allOrderVo.setFirstName(""); }
					 */

					if (orderR.getCustomer() != null && orderR.getCustomer().getFirstName() == null) {
						allOrderVo.setFirstName("");
						allOrderVo.setEmailId("");
						allOrderVo.setPhoneNo("");
					} else {
						if (orderR.getCustomer().getFirstName() != null && orderR.getCustomer().getLastName() != null) {
							allOrderVo.setFirstName(orderR.getCustomer().getFirstName().concat(" ")
									.concat(orderR.getCustomer().getLastName()));
						} else if (orderR.getCustomer().getFirstName() != null) {
							allOrderVo.setFirstName(orderR.getCustomer().getFirstName());
						}
						allOrderVo.setEmailId(orderR.getCustomer().getEmailId());
						allOrderVo.setPhoneNo(orderR.getCustomer().getPhoneNumber());

					}

					allOrderVo.setCreatedOn(orderR.getCreatedOn());
					if (orderR.getFulfilled_on() != null)
						allOrderVo.setFulfilledOn(orderR.getFulfilled_on());

					if (orderR.getOrderPrice() != null) {
						Double roundOff = Math.round(orderR.getOrderPrice() * 100) / 100.0;
						allOrderVo.setOrderPrice(roundOff);
					}
					if (orderR.getAddressId() != null) {
						LOGGER.info(" === orderR addressId : " + orderR.getAddressId());
						Address address = addressRepository.findOne(orderR.getAddressId());
						if (address != null) {
							allOrderVo.setOrderAdrress(address.getAddress1() +","+(address.getAddress2()!=null?address.getAddress2():""));
							allOrderVo.setState(address.getState());
							allOrderVo.setCity(address.getCity());
							allOrderVo.setZip(address.getZip());
						}
						allOrderVo.setEmailId(orderR.getCustomer().getEmailId());
						allOrderVo.setPhoneNo(orderR.getCustomer().getPhoneNumber());

					}

					allOrderVo.setConvenienceFee(orderR.getConvenienceFee());
					allOrderVo.setDeliveryFee(orderR.getDeliveryFee());
					allOrderVo.setDeliveryFee(orderR.getDeliveryFee());
					allOrderVo.setTax(orderR.getTax());

					if (orderR.getOrderDiscount() != null && orderR.getOrderDiscount() > 0)
						allOrderVo.setDiscount(orderR.getOrderDiscount());
					allOrderVo.setSubTotal(orderR.getSubTotal());
					allOrderVo.setTipAmount(orderR.getTipAmount());

					if (orderR.getOrderNote() == null || orderR.getOrderNote().isEmpty()) {
						allOrderVo.setOrderName("");
					} else {
						allOrderVo.setOrderName(orderR.getOrderNote());
					}
					allOrderVo.setOrderType(orderR.getOrderType());
					// order items
					List<OrderItemViewVO> orderItemViewVOs = new ArrayList<OrderItemViewVO>();
					List<OrderItem> orderItems = orderItemRepository.findByOrderId(orderR.getId());
					for (OrderItem orderItem : orderItems) {
						if (orderItem != null) {
							if (orderItem.getItem() != null) {
								if (!orderItem.getItem().getName().equals("Online Fee")
										&& !orderItem.getItem().getName().equals("Convenience Fee")
										&& !orderItem.getItem().getName().equals("Delivery Fee")) {
									List<OrderItemModifierViewVO> itemModifierViewVOs = new ArrayList<OrderItemModifierViewVO>();
									OrderItemViewVO orderItemViewVO = new OrderItemViewVO();
									orderItemViewVO.setQuantity(orderItem.getQuantity());
									ItemVO itemVO = new ItemVO();
									itemVO.setPrice(orderItem.getItem().getPrice());
									itemVO.setName(orderItem.getItem().getName());
									orderItemViewVO.setItemVO(itemVO);
									orderItem.setItem(null);
									LOGGER.info(" === orderItem.getId() : " + orderItem.getId());

									List<OrderItemModifier> itemModifiers = itemModifierRepository
											.findByOrderItemId(orderItem.getId());
									for (OrderItemModifier itemModifier : itemModifiers) {
										if (itemModifier.getModifiers() != null) {
											OrderItemModifierViewVO itemModifierViewVO = new OrderItemModifierViewVO();
											itemModifierViewVO.setQuantity(itemModifier.getQuantity());

											itemModifier.getModifiers().setModifierGroup(null);
											itemModifier.getModifiers().setMerchant(null);

											itemModifierViewVO.setModifiers(itemModifier.getModifiers());
											itemModifierViewVOs.add(itemModifierViewVO);
										}
									}
									orderItemViewVO.setItemModifierViewVOs(itemModifierViewVOs);
									orderItemViewVOs.add(orderItemViewVO);
								}
							}
						}
					}
					/*
					 * ------------------------------------------------------Pizza
					 * Stuff--------------------------------------------------------
					 */
					List<OrderPizza> orderPizzas = orderPizzaRepository.findByOrderId(orderR.getId());
					if (!orderPizzas.isEmpty() && orderPizzas != null) {
						for (OrderPizza orderPizza : orderPizzas) {

							if (orderPizza != null && orderPizza.getPizzaTemplate() != null) {
								orderPizza.setOrderPizzaToppings(null);

								List<OrderItemModifierViewVO> itemModifierViewVOs = new ArrayList<OrderItemModifierViewVO>();
								OrderItemViewVO orderItemViewVO = new OrderItemViewVO();
								orderItemViewVO.setQuantity(orderPizza.getQuantity());

								if (orderPizza.getPizzaTemplate() != null
										&& orderPizza.getPizzaTemplate().getDescription() != null
										&& !orderPizza.getPizzaTemplate().getDescription().equals("Convenience Fee")
										&& !orderPizza.getPizzaTemplate().getDescription().equals("Delivery Fee")) {

									orderItemViewVO.setQuantity(orderPizza.getQuantity());
									ItemVO itemVO = new ItemVO();
									itemVO.setPrice(orderPizza.getPrice());
									itemVO.setName(orderPizza.getPizzaTemplate().getDescription() + "("
											+ orderPizza.getPizzaSize().getDescription() + ")");
									orderItemViewVO.setItemVO(itemVO);
									LOGGER.info(" === orderPizza.getId() : " + orderPizza.getId());

									List<OrderPizzaToppings> orderPizzaToppings = orderPizzaToppingsRepository
											.findByOrderPizzaId(orderPizza.getId());
									for (OrderPizzaToppings itemModifier : orderPizzaToppings) {
										if (itemModifier.getPizzaTopping() != null) {
											OrderItemModifierViewVO itemModifierViewVO = new OrderItemModifierViewVO();
											itemModifierViewVO.setQuantity(itemModifier.getQuantity());

											Modifiers modifiers = new Modifiers();
											modifiers.setId(itemModifier.getId());
											StringBuilder toppingName = new StringBuilder(
													itemModifier.getPizzaTopping().getDescription());

											if (itemModifier.getSide1() != null && itemModifier.getSide1()) {
												toppingName.append(" (First Half)");
											} else if (itemModifier.getSide1() != null && itemModifier.getSide2()) {
												toppingName.append(" (Second Half)");
											} else {
												toppingName.append(" (Full)");
											}
											modifiers.setName(toppingName.toString());
											modifiers.setPosModifierId(
													itemModifier.getPizzaTopping().getPosPizzaToppingId());
											modifiers.setPrice(itemModifier.getPrice());
											itemModifierViewVO.setModifiers(modifiers);
											itemModifierViewVOs.add(itemModifierViewVO);
										}
									}

									OrderPizzaCrust orderPizzaCrust = orderPizzaCrustRepository
											.findByOrderPizzaId(orderPizza.getId());
									if (orderPizzaCrust != null && orderPizzaCrust.getPizzacrust() != null) {

										OrderItemModifierViewVO itemModifierViewVO = new OrderItemModifierViewVO();
										itemModifierViewVO.setQuantity(orderPizzaCrust.getQuantity());
										Modifiers itemModifier = new Modifiers();

										itemModifier.setId(orderPizzaCrust.getPizzacrust().getId());
										itemModifier.setName(orderPizzaCrust.getPizzacrust().getDescription());
										itemModifier.setPrice(orderPizzaCrust.getPrice());
										itemModifier
												.setPosModifierId(orderPizzaCrust.getPizzacrust().getPosPizzaCrustId());

										itemModifierViewVO.setModifiers(itemModifier);
										itemModifierViewVOs.add(itemModifierViewVO);
									}

									orderItemViewVO.setItemModifierViewVOs(itemModifierViewVOs);
									orderItemViewVOs.add(orderItemViewVO);
								}
							}

						}
					}
					/*
					 * -----------------------------------------------------------------------------
					 * ------------------------- -----------------------------
					 */
					allOrderVo.setOrderItemViewVOs(orderItemViewVOs);
					// order items end

					if (orderR.getPaymentMethod() != null && orderR.getPaymentMethod() != "") {
						allOrderVo.setPaymentMethod(orderR.getPaymentMethod());
					}

					allOrderVo.setView(
							"<p><button class='md-trigger custom-sd-button-dialog' id='viewOrderDetailPopup' data-modal= modal-15-"
									+ orderR.getId() + " orderAttr=" + orderR.getId() + ">View</button></p>");

					List<OrderDiscount> orderDiscounts = orderDiscountRepository.findByOrderId(orderR.getId());
					for (OrderDiscount orderDiscount : orderDiscounts) {
						orderDiscount.setOrder(null);
						orderDiscount.setCustomer(null);
					}

					if (orderR.getOrderPosId() != null) {
						allOrderVo.setOrderPosId(orderR.getOrderPosId());
					}

					String timeZoneCode = "America/Chicago";
					if (orderR.getMerchant() != null && orderR.getMerchant().getTimeZone() != null
							&& orderR.getMerchant().getTimeZone().getTimeZoneCode() != null) {
						timeZoneCode = orderR.getMerchant().getTimeZone().getTimeZoneCode();
					}
					boolean status;
					if (orderR.getFulfilled_on() != null) {
						status = DateUtil.checkCurrentAndFullFillTime(orderR.getFulfilled_on().toString(),
								timeZoneCode);
					} else
						status = false;

					LOGGER.info(" === status : " + status);
					LOGGER.info(" === orderR.getIsDefaults() : " + orderR.getIsDefaults());
					if (orderR.getIsDefaults() == 0 && status == true) {
						allOrderVo.setAction("<select onchange = 'updateOrderStatus(" + orderR.getId()
								+ ",\""+orderR.getOrderPosId()+ "\", this.value)'>" + "<option value='select_" + orderR.getOrderType() + "_"
								+ orderR.getOrderAvgTime() + "'>Select</option>" + "<option value='accept_"
								+ orderR.getOrderType() + "_" + orderR.getOrderAvgTime() + "'>Accept</option>"
								+ "<option value ='decline_" + orderR.getOrderType() + "_" + orderR.getOrderAvgTime()
								+ "'>Decline</option>" + "</select>");
					} else if (orderR.getIsDefaults() == 1 && status == true
							&& orderR.getPaymentMethod().equals("Cash")) {
						allOrderVo.setAction("<select onchange = 'updateOrderStatus(" + orderR.getId()
						+ ",\""+orderR.getOrderPosId()+ "\", this.value)'>" + "<option value='select_" + orderR.getOrderType() + "_"
								+ orderR.getOrderAvgTime() + "'>Select</option>" + "<option value='updateOrderTime_"
								+ orderR.getOrderType() + "_" + orderR.getOrderAvgTime() + "'>Update Time</option>"
//   									+ "<option value ='decline_"
//   									+ orderR.getOrderType() + "_"
//   									+ orderR.getOrderAvgTime()
//   									+ "'>Decline</option>"
								+ "<option value ='cancel_" + orderR.getOrderType() + "_" + orderR.getOrderAvgTime()
								+ "'>Cancel</option>" + "</select>");
					}

					else if (orderR.getIsDefaults() == 1 && status == true
							&& orderR.getPaymentMethod().equals("Credit Card") && paymentGateWay != null
							&& paymentGateWay.getGateWayType().equals("6")) {
						allOrderVo.setAction("<select onchange = 'updateOrderStatus(" + orderR.getId()
						+ ",\""+orderR.getOrderPosId()+ "\", this.value)'>" + "<option value='select_" + orderR.getOrderType() + "_"
								+ orderR.getOrderAvgTime() + "'>Select</option>" + "<option value='updateOrderTime_"
								+ orderR.getOrderType() + "_" + orderR.getOrderAvgTime() + "'>Update Time</option>"
//   									+ "<option value ='decline_"
//   									+ orderR.getOrderType() + "_"
//   									+ orderR.getOrderAvgTime()
//   									+ "'>Decline</option>"
								+ "<option value ='cancel_" + orderR.getOrderType() + "_" + orderR.getOrderAvgTime()
								+ "'>Cancel</option>" + "</select>");
					} else if (orderR.getIsDefaults() == 1 && status == true) {
						allOrderVo.setAction("<select  onchange = 'updateOrderStatus(" + orderR.getId()
						+ ",\""+orderR.getOrderPosId()+ "\", this.value)'>" + "<option value='select_" + orderR.getOrderType() + "_"
								+ orderR.getOrderAvgTime() + "'>Select</option>" + "<option value='updateOrderTime_"
								+ orderR.getOrderType() + "_" + orderR.getOrderAvgTime() + "'>Update Time</option>"
								+ "</select>");

					}

					else if ((orderR.getIsDefaults() == 2) || (orderR.getIsDefaults() == 1 && status == false)) {
						allOrderVo.setAction("N/A");
					} else if (orderR.getIsDefaults() == 0 && status == false) {
						allOrderVo.setAction("<select onchange = 'updateOrderStatus(" + orderR.getId()
						+ ",\""+orderR.getOrderPosId()+ "\", this.value)'>" + "<option value='select_" + orderR.getOrderType() + "_"
								+ orderR.getOrderAvgTime() + "'>Select</option>" + "<option value ='decline_"
								+ orderR.getOrderType() + "_" + orderR.getOrderAvgTime() + "'>Decline</option>");

					} else {
						allOrderVo.setAction("N/A");
					}

					allOrderVo.setOrderDiscountsList(orderDiscounts);
					allOrderVos.add(allOrderVo);
				}
				if (orderR.getIsDefaults() != null) {
					if (orderR.getIsDefaults() == 1) {
						allOrderVo.setStatus("Confirmed");
						if (orderR.getOrderType().equals("delivery") || orderR.getOrderType().equals("Delivery")) {
							allOrderVo.setEdit(
									"<p><button class='md-trigger custom-sd-button-dialog' id='editOrderDetailPopup' onclick= 'editPopup("
											+ orderR.getOrderPosId() + ",\"" + orderR.getOrderType() + "\","
											+ orderR.getIsDefaults() + ",\"" + orderR.getFulfilled_on()
											+ "\")'>Edit</button></p>");
						}
					} else if (orderR.getIsDefaults() == 4) {
						allOrderVo.setStatus("Agreed");
						if (orderR.getOrderType().equals("delivery") || orderR.getOrderType().equals("Delivery")) {
							allOrderVo.setEdit(
									"<p><button class='md-trigger custom-sd-button-dialog' id='editOrderDetailPopup' onclick= 'editPopup("
											+ orderR.getOrderPosId() + ",\"" + orderR.getOrderType() + "\","
											+ orderR.getIsDefaults() + ",\"" + orderR.getFulfilled_on()
											+ "\")'>Edit</button></p>");
						}
					} else {
						if (orderR.getIsDefaults() == 2) {
							allOrderVo.setStatus("Cancelled");
						} else if (orderR.getIsDefaults() == 5) {
							allOrderVo.setStatus("Canceled/refunded");
						} else if (orderR.getIsDefaults() == 6) {
							allOrderVo.setStatus("Canceled");
						} else {
							allOrderVo.setStatus("Pending");
							allOrderVo.setEdit(
									"<p><button class='md-trigger custom-sd-button-dialog' id='editOrderDetailPopup' onclick= 'editPopup("
											+ orderR.getOrderPosId() + ",\"" + orderR.getOrderType() + "\","
											+ orderR.getIsDefaults() + ",\"" + orderR.getFulfilled_on()
											+ "\")'>Edit</button></p>");

							if (orderR.getOrderType().equals("Pickup")) {
								allOrderVo.setEdit(
										"<p><button class='md-trigger custom-sd-button-dialog' id='editOrderDetailPopup' onclick= 'editPopup("
												+ orderR.getOrderPosId() + ",\"" + orderR.getOrderType() + "\","
												+ orderR.getIsDefaults() + ",\"" + orderR.getFulfilled_on()
												+ "\")'>Cancel Order</button></p>");
							}

							// allOrderVo.setEdit("<button id='confirmModal_ex2'
							// style='display: none' class='btn btn-primary'
							// data-confirmmodal-bind='#confirm_content'
							// data-topoffset='0'
							// data-top='10%''>Example</button>");
						}
					}
				}
			}
		}
		OrderJson orderJosn = new OrderJson();
		orderJosn.setiTotalDisplayRecords((int) orderRs.getTotalElements());
		orderJosn.setiTotalRecords((int) orderRs.getTotalElements());
		orderJosn.setAaData(allOrderVos);
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		LOGGER.info("===============  OrderServiceImpl : Inside findAllOrderFromDataTable :: End  ============= ");

		return gson.toJson(orderJosn);
	}

	public String findOrderDetailsById(Integer orderId) {
		LOGGER.info("===============  OrderServiceImpl : Inside findOrderDetailsById :: Start  ============= ");
		LOGGER.info(" ===orderId : " + orderId);
		OrderR orderRs = orderRepo.findOne(orderId);
		LOGGER.info("===============  OrderServiceImpl : Inside findOrderDetailsById :: End  ============= ");

		return findOrderDetail(orderRs);
	}

	private String findOrderDetail(OrderR orderRs) {
		LOGGER.info("===============  OrderServiceImpl : Inside findOrderDetail :: Start  ============= ");
		LOGGER.info(" === orderRs.getId() : " + orderRs.getId());

		OrderR orderR = new OrderR();
		orderR.setId(orderRs.getId());
		orderR.setConvenienceFee(orderRs.getConvenienceFee());
		orderR.setCreatedOn(orderRs.getCreatedOn());
		orderR.setDeliveryFee(orderRs.getDeliveryFee());
		orderR.setIsDefaults(orderRs.getIsDefaults());
		orderR.setOrderName(orderRs.getOrderName());
		orderR.setOrderNote(orderRs.getOrderNote());
		orderR.setOrderPrice(orderRs.getOrderPrice());
		orderR.setSubTotal(orderRs.getSubTotal());
		orderR.setOrderPrice(orderRs.getOrderPrice());
		orderR.setTax(orderRs.getTax());
		List<OrderItemViewVO> orderItemViewVOs = new ArrayList<OrderItemViewVO>();
		LOGGER.info(" === orderR.getId() : " + orderR.getId());

		List<OrderItem> orderItems = orderItemRepository.findByOrderId(orderR.getId());
		for (OrderItem orderItem : orderItems) {
			if (orderItem != null) {
				if (orderItem.getItem() != null) {
					if (!orderItem.getItem().getName().equals("Convenience Fee")
							&& !orderItem.getItem().getName().equals("Delivery Fee")) {
						List<OrderItemModifierViewVO> itemModifierViewVOs = new ArrayList<OrderItemModifierViewVO>();
						OrderItemViewVO orderItemViewVO = new OrderItemViewVO();
						orderItemViewVO.setQuantity(orderItem.getQuantity());
						orderItem.getItem().setCategories(null);
						orderItem.getItem().setItemModifierGroups(null);
						orderItem.getItem().setTaxes(null);
						orderItem.getItem().setOrderItems(null);
						orderItemViewVO.setItem(orderItem.getItem());
						LOGGER.info(" === orderItem.getId() : " + orderItem.getId());

						List<OrderItemModifier> itemModifiers = itemModifierRepository
								.findByOrderItemId(orderItem.getId());
						for (OrderItemModifier itemModifier : itemModifiers) {
							OrderItemModifierViewVO itemModifierViewVO = new OrderItemModifierViewVO();
							itemModifierViewVO.setQuantity(itemModifier.getQuantity());

							if (itemModifier.getModifiers() != null)
								itemModifier.getModifiers().setModifierGroup(null);

							itemModifierViewVO.setModifiers(itemModifier.getModifiers());
							itemModifierViewVOs.add(itemModifierViewVO);
						}
						orderItemViewVO.setItemModifierViewVOs(itemModifierViewVOs);
						orderItemViewVOs.add(orderItemViewVO);
					}
				}
			}
		}
		orderR.setOrderItemViewVOs(orderItemViewVOs);
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		LOGGER.info("===============  OrderServiceImpl : Inside findOrderDetail :: End  ============= ");

		return gson.toJson(orderR);
	}

	public String searchOrderByText(Integer merchantId, String searchTxt, Integer pageDisplayLength,
			Integer pageNumber) {
		LOGGER.info("===============  OrderServiceImpl : Inside searchOrderByText :: Start  ============= ");

		LOGGER.info(" merchantId : " + merchantId + " pageDisplayLength : " + pageDisplayLength + " pageNumber : "
				+ pageNumber + " searchTxt : " + searchTxt);

		Pageable pageable = new PageRequest(pageNumber - 1, pageDisplayLength, Sort.Direction.DESC, "id");
		Page<OrderR> orderRs = orderRepo.findByMerchantIdAndCustomerName(merchantId, searchTxt, pageable);
		List<AllOrderVo> allOrderVos = new ArrayList<AllOrderVo>();
		for (OrderR orderR : orderRs) {

			if (orderR.getIsDefaults() != 3) {
				AllOrderVo allOrderVo = new AllOrderVo();
				allOrderVo.setDT_RowId("row_" + orderR.getId());
				allOrderVo.setId(orderR.getId());
				if (orderR.getCustomer().getFirstName() != null && orderR.getCustomer().getLastName() != null) {
					allOrderVo.setFirstName(
							orderR.getCustomer().getFirstName().concat(" ").concat(orderR.getCustomer().getLastName()));
				} else if (orderR.getCustomer().getFirstName() != null) {
					allOrderVo.setFirstName(orderR.getCustomer().getFirstName());
				}
				allOrderVo.setCreatedOn(orderR.getCreatedOn());
				allOrderVo.setOrderPrice(orderR.getOrderPrice());
				allOrderVo.setConvenienceFee(orderR.getConvenienceFee());
				allOrderVo.setDeliveryFee(orderR.getDeliveryFee());
				allOrderVo.setTax(orderR.getTax());
				allOrderVo.setSubTotal(orderR.getSubTotal());
				allOrderVo.setTipAmount(orderR.getTipAmount());
				if (orderR.getOrderNote().isEmpty()) {
					allOrderVo.setOrderName("");
				} else {
					allOrderVo.setOrderName(orderR.getOrderNote());
				}
				allOrderVo.setOrderType(orderR.getOrderType());
				if (orderR.getIsDefaults() == 1) {
					allOrderVo.setStatus("Confirmed");
				} else {
					if (orderR.getIsDefaults() == 2) {
						allOrderVo.setStatus("Cancelled");
					} else {
						allOrderVo.setStatus("Pending");
					}
				}

				// order items
				LOGGER.info(" === orderR.getId() : " + orderR.getId());

				List<OrderItemViewVO> orderItemViewVOs = new ArrayList<OrderItemViewVO>();
				List<OrderItem> orderItems = orderItemRepository.findByOrderId(orderR.getId());
				for (OrderItem orderItem : orderItems) {
					if (orderItem != null) {
						if (orderItem.getItem() != null) {
							if (!orderItem.getItem().getName().equals("Convenience Fee")
									&& !orderItem.getItem().getName().equals("Delivery Fee")) {
								List<OrderItemModifierViewVO> itemModifierViewVOs = new ArrayList<OrderItemModifierViewVO>();
								OrderItemViewVO orderItemViewVO = new OrderItemViewVO();
								orderItemViewVO.setQuantity(orderItem.getQuantity());
								ItemVO itemVO = new ItemVO();
								itemVO.setPrice(orderItem.getItem().getPrice());
								itemVO.setName(orderItem.getItem().getName());
								orderItemViewVO.setItemVO(itemVO);
								orderItem.setItem(null);
								LOGGER.info(" === orderItem.getId() : " + orderItem.getId());

								List<OrderItemModifier> itemModifiers = itemModifierRepository
										.findByOrderItemId(orderItem.getId());
								for (OrderItemModifier itemModifier : itemModifiers) {
									if (itemModifier.getModifiers() != null) {
										OrderItemModifierViewVO itemModifierViewVO = new OrderItemModifierViewVO();
										itemModifierViewVO.setQuantity(itemModifier.getQuantity());

										itemModifier.getModifiers().setModifierGroup(null);
										itemModifier.getModifiers().setMerchant(null);

										itemModifierViewVO.setModifiers(itemModifier.getModifiers());
										itemModifierViewVOs.add(itemModifierViewVO);
									}
								}
								orderItemViewVO.setItemModifierViewVOs(itemModifierViewVOs);
								orderItemViewVOs.add(orderItemViewVO);
							}
						}
					}
				}
				allOrderVo.setOrderItemViewVOs(orderItemViewVOs);
				// order items end

				allOrderVo.setView(
						"<p><button class='md-trigger custom-sd-button-dialog' id='viewOrderDetailPopup' data-modal= modal-15-"
								+ orderR.getId() + " orderAttr=" + orderR.getId() + ">View</button></p>");

				allOrderVos.add(allOrderVo);
			}
		}
		OrderJson orderJosn = new OrderJson();
		orderJosn.setiTotalDisplayRecords((int) orderRs.getTotalElements());
		orderJosn.setiTotalRecords((int) orderRs.getTotalElements());
		orderJosn.setAaData(allOrderVos);
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		LOGGER.info("===============  OrderServiceImpl : Inside searchOrderByText :: End  ============= ");

		return gson.toJson(orderJosn);
		/*
		 * Gson gson = new GsonBuilder().setPrettyPrinting().create(); return
		 * gson.toJson(allOrderVos);
		 */
	}

	/*
	 * public String searchOrderByText(Integer merchantId, String searchTxt) {
	 * 
	 * 
	 * List<OrderR> orderRs = orderRepo.findByMerchantIdAndCustomerName(merchantId,
	 * searchTxt); List<AllOrderVo> allOrderVos = new ArrayList<AllOrderVo>(); for
	 * (OrderR orderR : orderRs) { AllOrderVo allOrderVo = new AllOrderVo();
	 * allOrderVo.setDT_RowId("row_" + orderR.getId());
	 * allOrderVo.setId(orderR.getId());
	 * allOrderVo.setFirstName(orderR.getCustomer().getFirstName());
	 * allOrderVo.setCreatedOn(orderR.getCreatedOn());
	 * allOrderVo.setOrderPrice(orderR.getOrderPrice());
	 * allOrderVo.setConvenienceFee(orderR.getConvenienceFee());
	 * allOrderVo.setDeliveryFee(orderR.getDeliveryFee());
	 * allOrderVo.setTax(orderR.getTax());
	 * allOrderVo.setSubTotal(orderR.getSubTotal());
	 * allOrderVo.setTipAmount(orderR.getTipAmount()); if
	 * (orderR.getOrderNote().isEmpty()) { allOrderVo.setOrderName(""); } else {
	 * allOrderVo.setOrderName(orderR.getOrderNote()); }
	 * allOrderVo.setOrderType(orderR.getOrderType()); if (orderR.getIsDefaults() ==
	 * 1) { allOrderVo.setStatus("Confirmed"); } else { if (orderR.getIsDefaults()
	 * == 2) { allOrderVo.setStatus("Cancelled"); } else {
	 * allOrderVo.setStatus("Pending"); } }
	 * 
	 * // order items List<OrderItemViewVO> orderItemViewVOs = new
	 * ArrayList<OrderItemViewVO>(); List<OrderItem> orderItems =
	 * orderItemRepository.findByOrderId(orderR.getId()); for (OrderItem orderItem :
	 * orderItems) { if (orderItem != null) { if (orderItem.getItem() != null) { if
	 * (!orderItem.getItem().getName().equals( "Convenience Fee") &&
	 * !orderItem.getItem().getName().equals("Delivery Fee")) {
	 * List<OrderItemModifierViewVO> itemModifierViewVOs = new
	 * ArrayList<OrderItemModifierViewVO>(); OrderItemViewVO orderItemViewVO = new
	 * OrderItemViewVO(); orderItemViewVO.setQuantity(orderItem.getQuantity());
	 * ItemVO itemVO = new ItemVO();
	 * itemVO.setPrice(orderItem.getItem().getPrice());
	 * itemVO.setName(orderItem.getItem().getName());
	 * orderItemViewVO.setItemVO(itemVO); orderItem.setItem(null);
	 * 
	 * List<OrderItemModifier> itemModifiers =
	 * itemModifierRepository.findByOrderItemId(orderItem .getId()); for
	 * (OrderItemModifier itemModifier : itemModifiers) { if
	 * (itemModifier.getModifiers() != null) { OrderItemModifierViewVO
	 * itemModifierViewVO = new OrderItemModifierViewVO();
	 * itemModifierViewVO.setQuantity(itemModifier.getQuantity());
	 * 
	 * itemModifier.getModifiers().setModifierGroup(null);
	 * itemModifier.getModifiers().setMerchant(null);
	 * 
	 * itemModifierViewVO.setModifiers(itemModifier.getModifiers());
	 * itemModifierViewVOs.add(itemModifierViewVO); } }
	 * orderItemViewVO.setItemModifierViewVOs(itemModifierViewVOs);
	 * orderItemViewVOs.add(orderItemViewVO); } } } }
	 * allOrderVo.setOrderItemViewVOs(orderItemViewVOs); // order items end
	 * 
	 * allOrderVo.setView(
	 * "<p><button class='md-trigger custom-sd-button-dialog' id='viewOrderDetailPopup' data-modal= modal-15-"
	 * + orderR.getId() + " orderAttr=" + orderR.getId() + ">View</button></p>");
	 * 
	 * allOrderVos.add(allOrderVo); } Gson gson = new
	 * GsonBuilder().setPrettyPrinting().create(); return gson.toJson(allOrderVos);
	 * }
	 */

	public String getAllFutureOrders(String merchantId) {
		LOGGER.info("===============  OrderServiceImpl : Inside getAllFutureOrders :: Start  ============= ");
		LOGGER.info(" === merchantId : " + merchantId);
		Merchant merchant = merchantRepository.findByPosMerchantId(merchantId);
		if (merchant != null) {
//			Integer hourDifference = 0;
//			Integer minutDifference = 0;
//			if (merchant != null && merchant.getTimeZone() != null
//					&& merchant.getTimeZone().getHourDifference() != null) {
//				hourDifference = merchant.getTimeZone().getHourDifference();
//			}
//			if (merchant.getTimeZone().getMinutDifference() != null) {
//				minutDifference = merchant.getTimeZone().getMinutDifference();
//			}
			// DateFormat dateFormat = new
			// SimpleDateFormat(IConstant.YYYYMMDDHHMMSS);
//			LOGGER.info(" === hourDifference : " + hourDifference);
//			LOGGER.info(" === minutDifference : " + minutDifference);
//			Calendar calobj = Calendar.getInstance();
//			calobj.add(Calendar.HOUR, hourDifference);
//			calobj.add(Calendar.MINUTE, minutDifference);
//			LOGGER.info("calobj-" + calobj.getTime());
//
//			Date date = calobj.getTime();

			Date date =  (merchant != null && merchant.getTimeZone() != null
					&& merchant.getTimeZone().getTimeZoneCode() != null)
					? DateUtil.getCurrentDateForTimeZonee(merchant.getTimeZone().getTimeZoneCode())
					: new Date();
			List<OrderR> orderRs = orderRepo.findByFulFilledOnAndIsFutureOrderAndMerchantIdAndIsDefaults(date,
					IConstant.BOOLEAN_TRUE, merchant.getId(), IConstant.Order_Status_Pending);
			List<String> notifications = new ArrayList<String>();

			for (OrderR orderR : orderRs) {
				// String orderDetails = "";
				Map<String, Map<String, String>> notf = new HashMap<String, Map<String, String>>();
				Map<String, String> notification = new HashMap<String, String>();
				notification.put("appEvent", "notification");
				Map<String, String> order = new HashMap<String, String>();
				order.put("total", Double.toString(orderR.getOrderPrice()));
				order.put("tax", orderR.getTax());
				List<Map<String, String>> productList = new ArrayList<Map<String, String>>();
				LOGGER.info(" === orderR.getId() : " + orderR.getId());

				List<OrderItem> orderItems = orderItemRepository.findByOrderId(orderR.getId());

				for (OrderItem orderItem : orderItems) {
					if (orderItem != null) {
						if (orderItem.getItem() != null) {

							List<Map<String, String>> extraList = new ArrayList<Map<String, String>>();
							Map<String, String> poduct = new HashMap<String, String>();
							poduct.put("product_id", orderItem.getItem().getPosItemId());
							poduct.put("name", orderItem.getItem().getName());
							poduct.put("price", Double.toString(orderItem.getItem().getPrice()));
							poduct.put("qty", Integer.toString(orderItem.getQuantity()));
							LOGGER.info(" === orderItem.getId() : " + orderItem.getId());

							List<OrderItemModifier> itemModifiers = itemModifierRepository
									.findByOrderItemId(orderItem.getId());
							for (OrderItemModifier itemModifier : itemModifiers) {

								Map<String, String> exctra = new HashMap<String, String>();
								exctra.put("id", itemModifier.getModifiers().getPosModifierId());
								exctra.put("price", Double.toString(itemModifier.getModifiers().getPrice()));
								exctra.put("name", itemModifier.getModifiers().getName());
								exctra.put("qty", Integer.toString(orderItem.getQuantity()));
								extraList.add(exctra);
							}
							Gson gson = new Gson();
							String extraJson = gson.toJson(extraList);
							poduct.put("extras", extraJson);
							productList.add(poduct);
						}
					}
				}
				Gson gson = new Gson();
				String productJson = gson.toJson(productList);
				order.put("productItems", productJson);
				// order.put("productItems", productList.toString());
				String orderJson = gson.toJson(order);
				LOGGER.info("== orderJson :  " + orderJson);
				Date fulFilledTime = orderR.getFulfilled_on();
				Calendar calobjs = Calendar.getInstance();
				calobjs.setTime(fulFilledTime);
				calobjs.add(Calendar.MINUTE, -Integer.parseInt(orderR.getOrderAvgTime()));
				Date orderFulFilledTime = calobjs.getTime();

				long timeInMillis = orderFulFilledTime.getTime();
				// String orderFulTime =
				// dateFormat.format(orderFulFilledTime).toString();
				notification.put("payload", orderR.getOrderPosId() + "@#You got a new order(" + orderR.getOrderPosId()
						+ ") at " + orderR.getCreatedOn() + "@# $" + orderR.getOrderPrice() + "@# "
						+ orderR.getOrderType() + "@#" + orderJson + "@#" + orderR.getMerchant().getPosMerchantId()
						+ "@#" + orderR.getOrderNote() + " @#" + orderR.getPosPaymentId() + " @#" + timeInMillis);

				notf.put("notification", notification);
				String notificationJson = gson.toJson(notf);
				notificationJson = notificationJson.trim().replaceAll("\\\\+\"", "'").replace("'[", "[").replace("]'",
						"]");
				notifications.add(notificationJson);

			}
			if (notifications.size() > 0) {
				LOGGER.info("===============  OrderServiceImpl : Inside getAllFutureOrders :: End  ============= ");

				return notifications.toString();
			} else {
				LOGGER.info("===============  OrderServiceImpl : Inside getAllFutureOrders :: End  ============= ");

				return null;
			}
		} else {
			LOGGER.info("===============  OrderServiceImpl : Inside getAllFutureOrders :: End  ============= ");

			return null;
		}

	}

	public String searchOrderByTextAndDate(Integer merchantId, String searchTxt, Integer pageDisplayLength,
			Integer pageNumber, String startDate, String endDate) {
		LOGGER.info("===============  OrderServiceImpl : Inside searchOrderByTextAndDate :: Start  ============= ");

		LOGGER.info(" merchantId : " + merchantId + " pageDisplayLength : " + pageDisplayLength + " pageNumber : "
				+ pageNumber + " searchTxt : " + searchTxt + " startDate : " + startDate + " enddate : " + endDate);

		java.util.Date startDateRange = null;
		java.util.Date endDateRange = null;
		try {
			if (startDate != null && endDate != null) {

				startDateRange = new SimpleDateFormat(IConstant.YYYYMMDD).parse(startDate);
				// endDateRange=new
				// SimpleDateFormat(IConstant.YYYYMMDD).parse(endDate);

				SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
				Calendar cal = Calendar.getInstance();
				cal.setTime(dateFormat.parse(endDate));
				cal.add(Calendar.DATE, 1);
				// endDate = dateFormat.format(cal.getTime());
				endDateRange = new SimpleDateFormat(IConstant.YYYYMMDD).parse(dateFormat.format(cal.getTime()));

			}
		} catch (ParseException e) {
			LOGGER.error(
					"===============  OrderServiceImpl : Inside searchOrderByTextAndDate :: Exception  ============= "
							+ e);

			// TODO Auto-generated catch block
			MailSendUtil.sendExceptionByMail(e,environment);
			LOGGER.error("error: " + e.getMessage());
		}

		Pageable pageable = new PageRequest(pageNumber - 1, pageDisplayLength, Sort.Direction.DESC, "id");
		Page<OrderR> orderRs = orderRepo.findByMerchantIdAndCustomerNameAndDate(merchantId, searchTxt, pageable,
				startDateRange, endDateRange);
		List<AllOrderVo> allOrderVos = new ArrayList<AllOrderVo>();
		for (OrderR orderR : orderRs) {
			if (orderR.getIsDefaults() != 3) {
				AllOrderVo allOrderVo = new AllOrderVo();
				allOrderVo.setDT_RowId("row_" + orderR.getId());
				allOrderVo.setId(orderR.getId());
				if (orderR.getCustomer().getFirstName() != null && orderR.getCustomer().getLastName() != null) {
					allOrderVo.setFirstName(
							orderR.getCustomer().getFirstName().concat(" ").concat(orderR.getCustomer().getLastName()));
				} else if (orderR.getCustomer().getFirstName() != null) {
					allOrderVo.setFirstName(orderR.getCustomer().getFirstName());
				}
				allOrderVo.setCreatedOn(orderR.getCreatedOn());
				allOrderVo.setOrderPrice(orderR.getOrderPrice());
				allOrderVo.setConvenienceFee(orderR.getConvenienceFee());
				allOrderVo.setDeliveryFee(orderR.getDeliveryFee());
				allOrderVo.setTax(orderR.getTax());
				allOrderVo.setSubTotal(orderR.getSubTotal());
				allOrderVo.setTipAmount(orderR.getTipAmount());
				if (orderR.getOrderNote().isEmpty()) {
					allOrderVo.setOrderName("");
				} else {
					allOrderVo.setOrderName(orderR.getOrderNote());
				}
				allOrderVo.setOrderType(orderR.getOrderType());
				/*
				 * if (orderR.getIsDefaults() == 1) { allOrderVo.setStatus("Confirmed"); } else
				 * { if (orderR.getIsDefaults() == 2) { allOrderVo.setStatus("Cancelled"); }
				 * else { allOrderVo.setStatus("Pending"); } }
				 */

				allOrderVo.setEmailId(orderR.getCustomer().getEmailId());
				allOrderVo.setPhoneNo(orderR.getCustomer().getPhoneNumber());
				
				if (orderR.getAddressId() != null) {
					LOGGER.info(" === orderR addressId : " + orderR.getAddressId());
					Address address = addressRepository.findOne(orderR.getAddressId());
					if (address != null) {
						allOrderVo.setOrderAdrress(address.getAddress1() +","+(address.getAddress2()!=null?address.getAddress2():""));
						allOrderVo.setState(address.getState());
						allOrderVo.setCity(address.getCity());
						allOrderVo.setZip(address.getZip());
					}

				}
				if (orderR.getFulfilled_on() != null)
					allOrderVo.setFulfilledOn(orderR.getFulfilled_on());
				
				if (orderR.getIsDefaults() != null) {
					if (orderR.getIsDefaults() == 1) {
						allOrderVo.setStatus("Confirmed");
					} else {
						if (orderR.getIsDefaults() == 2) {
							allOrderVo.setStatus("Cancelled");
						} else {
							allOrderVo.setStatus("Pending");
						}
					}
				}

				// order items
				List<OrderItemViewVO> orderItemViewVOs = new ArrayList<OrderItemViewVO>();
				LOGGER.info(" === orderR.getId() : " + orderR.getId());

				List<OrderItem> orderItems = orderItemRepository.findByOrderId(orderR.getId());
				for (OrderItem orderItem : orderItems) {
					if (orderItem != null) {
						if (orderItem.getItem() != null) {
							if (!orderItem.getItem().getName().equals("Convenience Fee")
									&& !orderItem.getItem().getName().equals("Delivery Fee")) {
								List<OrderItemModifierViewVO> itemModifierViewVOs = new ArrayList<OrderItemModifierViewVO>();
								OrderItemViewVO orderItemViewVO = new OrderItemViewVO();
								orderItemViewVO.setQuantity(orderItem.getQuantity());
								ItemVO itemVO = new ItemVO();
								itemVO.setPrice(orderItem.getItem().getPrice());
								itemVO.setName(orderItem.getItem().getName());
								orderItemViewVO.setItemVO(itemVO);
								orderItem.setItem(null);
								LOGGER.info(" === orderItem.getId() : " + orderItem.getId());

								List<OrderItemModifier> itemModifiers = itemModifierRepository
										.findByOrderItemId(orderItem.getId());
								for (OrderItemModifier itemModifier : itemModifiers) {
									if (itemModifier.getModifiers() != null) {
										OrderItemModifierViewVO itemModifierViewVO = new OrderItemModifierViewVO();
										itemModifierViewVO.setQuantity(itemModifier.getQuantity());

										itemModifier.getModifiers().setModifierGroup(null);
										itemModifier.getModifiers().setMerchant(null);

										itemModifierViewVO.setModifiers(itemModifier.getModifiers());
										itemModifierViewVOs.add(itemModifierViewVO);
									}
								}
								orderItemViewVO.setItemModifierViewVOs(itemModifierViewVOs);
								orderItemViewVOs.add(orderItemViewVO);
							}
						}
					}
				}
				allOrderVo.setOrderItemViewVOs(orderItemViewVOs);
				// order items end

				if (orderR.getPaymentMethod() != null) {
					allOrderVo.setPaymentMethod(orderR.getPaymentMethod());
				}

				allOrderVo.setView(
						"<p><button class='md-trigger custom-sd-button-dialog' id='viewOrderDetailPopup' data-modal= modal-15-"
								+ orderR.getId() + " orderAttr=" + orderR.getId() + ">View</button></p>");

				List<OrderDiscount> orderDiscounts = orderDiscountRepository.findByOrderId(orderR.getId());
				for (OrderDiscount orderDiscount : orderDiscounts) {
					orderDiscount.setOrder(null);
					orderDiscount.setCustomer(null);
				}
				
				if (orderR.getIsDefaults() == 0) {
					allOrderVo.setAction("<select onchange = 'updateOrderStatus(" + orderR.getId() + ", this.value)'>"
							+ "<option value='select_" + orderR.getOrderType() + "_" + orderR.getOrderAvgTime()
							+ "'>Select</option>" + "<option value='accept_" + orderR.getOrderType() + "_"
							+ orderR.getOrderAvgTime() + "'>Accept</option>" + "<option value ='decline_"
							+ orderR.getOrderType() + "_" + orderR.getOrderAvgTime() + "'>Decline</option>"
							+ "</select>");
				} else if (orderR.getIsDefaults() == 1) {
					allOrderVo.setAction("<select  onchange = 'updateOrderStatus(" + orderR.getId() + ", this.value)'>"
							+ "<option value='select_" + orderR.getOrderType() + "_" + orderR.getOrderAvgTime()
							+ "'>Select</option>" + "<option value='updateOrderTime_" + orderR.getOrderType() + "_"
							+ orderR.getOrderAvgTime() + "'>Update Time</option>" + "<option value ='decline_"
							+ orderR.getOrderType() + "_" + orderR.getOrderAvgTime() + "'>Decline</option>"
							+ "</select>");
				} else if (orderR.getIsDefaults() == 2) {
					allOrderVo.setAction("N/A");
				}

				allOrderVo.setOrderDiscountsList(orderDiscounts);
				allOrderVos.add(allOrderVo);
			}
		}
		OrderJson orderJosn = new OrderJson();
		orderJosn.setiTotalDisplayRecords((int) orderRs.getTotalElements());
		orderJosn.setiTotalRecords((int) orderRs.getTotalElements());
		orderJosn.setAaData(allOrderVos);
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		LOGGER.info("===============  OrderServiceImpl : Inside searchOrderByTextAndDate :: End  ============= ");

		return gson.toJson(orderJosn);
		/*
		 * Gson gson = new GsonBuilder().setPrettyPrinting().create(); return
		 * gson.toJson(allOrderVos);
		 */
	}

	public List<AllOrderVo> generateExcelForOrder(Integer merchantId, String searchTxt, String startDate,
			String endDate) {
		LOGGER.info("===============  OrderServiceImpl : Inside generateExcelForOrder :: Start  ============= ");

		LOGGER.info(" merchantId : " + merchantId + " searchTxt : " + searchTxt + " startDate : " + startDate
				+ " enddate : " + endDate);

		java.util.Date startDateRange = null;
		java.util.Date endDateRange = null;
		try {
			if (startDate != null && endDate != null) {
				startDateRange = new SimpleDateFormat(IConstant.YYYYMMDD).parse(startDate);
				// endDateRange=new
				// SimpleDateFormat(IConstant.YYYYMMDD).parse(endDate);

				SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
				Calendar cal = Calendar.getInstance();
				cal.setTime(dateFormat.parse(endDate));
				cal.add(Calendar.DATE, 1);
				endDateRange = new SimpleDateFormat(IConstant.YYYYMMDD).parse(dateFormat.format(cal.getTime()));

			}
		} catch (ParseException e) {
			LOGGER.error(
					"===============  OrderServiceImpl : Inside generateExcelForOrders :: Exception  ============= "
							+ e);

			// TODO Auto-generated catch block
			MailSendUtil.sendExceptionByMail(e,environment);
			LOGGER.error("error: " + e.getMessage());
		}
		List<OrderR> orderRs = new ArrayList<OrderR>();
		if (searchTxt != null) {
			orderRs = orderRepo.findByMerchantIdAndCustomerNameAndDateRange(merchantId, searchTxt, startDateRange,
					endDateRange);
		} else {
			orderRs = orderRepo.findByMerchantIdAndDateRange(merchantId, startDateRange, endDateRange);
		}

		List<AllOrderVo> allOrderVos = new ArrayList<AllOrderVo>();
		for (OrderR orderR : orderRs) {
			if(orderR!=null)
			{
			AllOrderVo allOrderVo = new AllOrderVo();

			if (orderR != null && orderR.getCustomer() != null)
				if (orderR != null && orderR.getCustomer() != null)
					if (orderR.getCustomer().getFirstName() != null && orderR.getCustomer().getLastName() != null) {
						allOrderVo.setFirstName(orderR.getCustomer().getFirstName().concat(" ")
								.concat(orderR.getCustomer().getLastName()));
					} else if (orderR.getCustomer().getFirstName() != null) {
						allOrderVo.setFirstName(orderR.getCustomer().getFirstName());
					}

			allOrderVo.setCustomerEmail(orderR.getCustomer().getEmailId());
			allOrderVo.setCustomerPhone(orderR.getCustomer().getPhoneNumber());
			allOrderVo.setCreatedOn(orderR.getCreatedOn());
			allOrderVo.setOrderPrice(orderR.getOrderPrice());
			allOrderVo.setConvenienceFee(orderR.getConvenienceFee());
			allOrderVo.setDeliveryFee(orderR.getDeliveryFee());
			allOrderVo.setTax(orderR.getTax());
			allOrderVo.setSubTotal(orderR.getSubTotal());
			allOrderVo.setTipAmount(orderR.getTipAmount());
			/*
			 * if (orderR.getOrderNote().isEmpty()) { allOrderVo.setOrderName(""); } else {
			 * allOrderVo.setOrderName(orderR.getOrderNote()); }
			 */
			if (orderR.getOrderNote() != null) {
				allOrderVo.setOrderName(orderR.getOrderNote());
			}

			if (orderR.getPaymentMethod() != null) {
				allOrderVo.setPaymentMethod(orderR.getPaymentMethod());
			}

			allOrderVo.setOrderType(orderR.getOrderType());
			if (orderR.getIsDefaults() != null) {
				if (orderR.getIsDefaults() == 1) {
					allOrderVo.setStatus("Confirmed");
				} else if (orderR.getIsDefaults() == 3) {
					allOrderVo.setStatus("failed");
				} else {
					if (orderR.getIsDefaults() == 2) {
						allOrderVo.setStatus("Cancelled");
					} else {
						allOrderVo.setStatus("Pending");
					}
				}
			}

			// order items
			LOGGER.info(" === orderR.getId() : " + orderR.getId());

			List<OrderItem> orderItems = orderItemRepository.findByOrderId(orderR.getId());
			// order items end

			// order items modifiers
			List<OrderItemModifier> orderItemModifiers = null;
			if (orderItems != null && orderItems.size() > 0) {
				for (OrderItem orderItem : orderItems) {
					LOGGER.info(" === orderItem.getId() : " + orderItem.getId());

					orderItemModifiers = orderItemModifierRepository.findByOrderItemId(orderItem.getId());
					orderItem.setOrderItemModifiers(orderItemModifiers);
				}
			}
			// order item modifiers end
			allOrderVo.setOrderItems(orderItems);

			List<OrderPizza> orderPizzas = orderPizzaRepository.findByOrderId(orderR.getId());

			List<OrderPizzaToppings> orderPizzaToppings = null;
			if (orderPizzas != null && orderPizzas.size() > 0) {
				for (OrderPizza orderPizza : orderPizzas) {
					LOGGER.info(" === orderPizza.getId() : " + orderPizza.getId());

					orderPizzaToppings = orderPizzaToppingsRepository.findByOrderPizzaId(orderPizza.getId());
					orderPizza.setOrderPizzaToppings(orderPizzaToppings);
				}
			}

			allOrderVo.setOrderPizza(orderPizzas);

			allOrderVo.setView(
					"<p><button class='md-trigger custom-sd-button-dialog' id='viewOrderDetailPopup' data-modal= modal-15-"
							+ orderR.getId() + " orderAttr=" + orderR.getId() + ">View</button></p>");

			allOrderVos.add(allOrderVo);
		}}
		LOGGER.info("===============  OrderServiceImpl : Inside generateExcelForOrder :: End  ============= ");

		return allOrderVos;
	}

	public CardInfo getVaultedDetail(Integer vaultedId, String vaultedLastValue) {
		LOGGER.info("===============  OrderServiceImpl : Inside getVaultedDetail :: Start  ============= ");
		LOGGER.info(" ===vaultedId : " + vaultedId + " vaultedLastValue : " + vaultedLastValue);
		CardInfo cardInfo = new CardInfo();
		if (vaultedId != null && vaultedLastValue != null) {
			cardInfo = cardInfoRepository.findByIdAndLast4AndStatus(vaultedId, vaultedLastValue, true);
		} else {
			cardInfo = null;
		}
		LOGGER.info("===============  OrderServiceImpl : Inside getVaultedDetail :: End  ============= ");

		return cardInfo;
	}

	public Boolean deleteCardDetail(Integer customerId, Integer cardId) {
		LOGGER.info("===============  OrderServiceImpl : Inside deleteCardDetail :: Start  ============= ");
		LOGGER.info(" === customerId : " + customerId + " cardId : " + cardId);
		Boolean status = false;
		Customer customer = customerrRepository.findOne(customerId);
		Date date = (customer.getMerchantt()!=null && customer.getMerchantt().getTimeZone() !=null 
				&& customer.getMerchantt().getTimeZone().getTimeZoneCode()!=null) ?
					DateUtil.getCurrentDateForTimeZonee(customer.getMerchantt().getTimeZone().getTimeZoneCode()) : new Date();
		if (customerId != null && cardId != null) {
			CardInfo cardInfo = cardInfoRepository.findByCustomerIdAndIdAndStatus(customerId, cardId, true);
			if (cardInfo != null) {
				cardInfo.setStatus(false);
				cardInfo.setDeletedDate(date);
				cardInfo.setToken(null);
				cardInfoRepository.save(cardInfo);

				status = true;
			}

		}
		LOGGER.info(" === status : " + status);
		LOGGER.info("===============  OrderServiceImpl : Inside deleteCardDetail :: End  ============= ");

		return status;
	}

	public String getEncyKey(String token, Integer merchantId) {
		LOGGER.info("===============  OrderServiceImpl : Inside getEncyKey :: Start  ============= ");
		LOGGER.info(" ===token : " + token + " merchantId : " + merchantId);
		String decryptString = "";
		EncryptedKey encryptedKey = merchantEncryptedKeyRepository.findByMerchantId(merchantId);
		if (encryptedKey != null) {
			decryptString = EncryptionDecryptionToken.decryptValue(token, encryptedKey.getKey());
		}
		LOGGER.info(" === decryptString : " + decryptString);
		LOGGER.info("===============  OrderServiceImpl : Inside getEncyKey :: End  ============= ");

		return decryptString;
	}

	public List<OrderR> getRepeateOrder(Integer customerId, Merchant merchant, Integer repeateOrderId) {
		LOGGER.info("===============  OrderServiceImpl : Inside getRepeateOrder :: Start  ============= ");
		LOGGER.info(" === customerId : " + customerId + " merchant : " + merchant.getId() + " repeateOrderId : "
				+ repeateOrderId);
		List<OrderR> orderRs = orderRepo.findByCustomerIdAndMerchantIdAndId(customerId, merchant.getId(),
				repeateOrderId);
		LOGGER.info("===============  OrderServiceImpl : Inside getRepeateOrder :: End  ============= ");

		return findRepeateOrderDetails(orderRs, merchant);
	}

	public List<OrderR> getAllRepeateOrder(Integer customerId, Merchant merchant) {
		LOGGER.info("===============  OrderServiceImpl : Inside getAllRepeateOrder :: Start  ============= ");
		LOGGER.info(" === customerId : " + customerId + " merchant : " + merchant.getId());
		List<OrderR> orderRs = orderRepo.findByCustomerIdAndMerchantId(customerId, merchant.getId());
		Collections.sort(orderRs, new Comparator<OrderR>() {
			public int compare(OrderR m1, OrderR m2) {
				return m2.getCreatedOn().compareTo(m1.getCreatedOn());
			}
		});
		LOGGER.info("===============  OrderServiceImpl : Inside getAllRepeateOrder :: End  ============= ");

		return findRepeateOrderDetails(orderRs, merchant);
	}

	private List<OrderR> findRepeateOrderDetails(List<OrderR> orderRs, Merchant merchant) {
		LOGGER.info("===============  OrderServiceImpl : Inside findRepeateOrderDetails :: Start  ============= ");

		for (OrderR orderR : orderRs) {
			List<OrderItemViewVO> orderItemViewVOs = new ArrayList<OrderItemViewVO>();
			String timeZoneCode = "America/Chicago";
			if (merchant != null && merchant.getTimeZone() != null
					&& merchant.getTimeZone().getTimeZoneCode() != null) {

				timeZoneCode = merchant.getTimeZone().getTimeZoneCode();
			}
			String currentDay = DateUtil.getCurrentDayForTimeZone(timeZoneCode);
			String currentTime = DateUtil.getCurrentTimeForTimeZone(timeZoneCode);
			LOGGER.info(" === orderR.getId() : " + orderR.getId());

			List<OrderItem> orderItems = orderItemRepository.findByOrderIdAndItemItemStatus(orderR.getId(), 0);
			for (OrderItem orderItem : orderItems) {
				if (orderItem != null) {
					if (orderItem.getItem() != null) {
						if (!orderItem.getItem().getName().equals("Convenience Fee")
								&& !orderItem.getItem().getName().equals("Delivery Fee")) {
							Item item = null;
							if (orderItem.getItem().getId() != null) {
								LOGGER.info(" === orderItem.getItem().getId() : " + orderItem.getItem().getId());

								item = itemmRepository.findItemByItemId(orderItem.getItem().getId(), currentDay,
										currentTime, currentTime);
							}
							if (item != null) {
								orderItem.getItem().setItemTimings(null);
								List<OrderItemModifierViewVO> itemModifierViewVOs = new ArrayList<OrderItemModifierViewVO>();
								OrderItemViewVO orderItemViewVO = new OrderItemViewVO();
								orderItemViewVO.setQuantity(orderItem.getQuantity());
								orderItem.getItem().setCategories(null);
								orderItem.getItem().setItemModifierGroups(null);

								// boolean itemTimingStatus=true;

								String taxName = null;
								Double tax = 0.0;
								Map<String, Object> itemNameAndPrice = getItemTaxNameAndPrice(
										orderItem.getItem().getId());
								if (itemNameAndPrice.get("taxNames") != null && itemNameAndPrice.get("tax") != null) {
									taxName = (String) itemNameAndPrice.get("taxNames");
									tax = (Double) itemNameAndPrice.get("tax");
								}

								orderItem.getItem().setTaxNames(taxName);
								orderItem.getItem().setTaxes(null);
								orderItem.getItem().setOrderItems(null);
								orderItem.getItem().setMerchant(null);
								orderItemViewVO.setItem(orderItem.getItem());
								LOGGER.info(" === orderItem.getId() : " + orderItem.getId());

								List<OrderItemModifier> itemModifiers = itemModifierRepository
										.findByOrderItemId(orderItem.getId());
								for (OrderItemModifier itemModifier : itemModifiers) {
									ItemModifiers modifiers = itemModifiersRepository
											.findByModifiersIdAndItemIdAndModifierStatus(
													itemModifier.getModifiers().getId(), orderItem.getItem().getId(),
													IConstant.BOOLEAN_TRUE);
									if (modifiers != null) {
										OrderItemModifierViewVO itemModifierViewVO = new OrderItemModifierViewVO();
										itemModifierViewVO.setQuantity(itemModifier.getQuantity());

										if (itemModifier.getModifiers() != null)
											itemModifier.getModifiers().setModifierGroup(null);
										itemModifier.getModifiers().setMerchant(null);
										itemModifierViewVO.setModifiers(itemModifier.getModifiers());
										itemModifierViewVOs.add(itemModifierViewVO);
									}
								}
								orderItemViewVO.setItemModifierViewVOs(itemModifierViewVOs);

								orderItemViewVOs.add(orderItemViewVO);
							}
						}
					}
				}
			}
			orderR.setOrderItemViewVOs(orderItemViewVOs);
		}
		LOGGER.info("===============  OrderServiceImpl : Inside findRepeateOrderDetails :: End  ============= ");

		return orderRs;
	}

	public List<Double> getFoodTronixTax(Integer merchantId, String orderType) {
		LOGGER.info("===============  OrderServiceImpl : Inside getFoodTronixTax :: Start  ============= ");
		LOGGER.info(" ===merchantId " + merchantId + " orderType : " + orderType);
		List<Double> list = new ArrayList<Double>();
		OrderType type = orderTypeRepository.findByMerchantIdAndLabel(merchantId, orderType);
		if (type != null) {
			list.add(type.getSalesTax());
			list.add(type.getAuxTax());
		}
		LOGGER.info("===============  OrderServiceImpl : Inside getFoodTronixTax :: End  ============= ");

		return list;
	}

	public OrderR findOrderByOrderPosIdAndCustomerId(String orderPosId, Integer id) {
		LOGGER.info(
				"===============  OrderServiceImpl : Inside findOrderByOrderPosIdAndCustomerId :: Start  ============= ");
		LOGGER.info(" ===orderPosId : " + orderPosId + " id : " + id);
		LOGGER.info(
				"===============  OrderServiceImpl : Inside findOrderByOrderPosIdAndCustomerId :: End  ============= ");

		return orderRepository.findByOrderPosIdAndCustomerId(orderPosId, id);
	}

	public boolean updateDeliveryTime(String orderId, String deliveryTime) throws Exception {
		OrderR order = orderRepository.findByOrderPosId(orderId);
		try {
			LOGGER.info("===============  OrderServiceImpl : Inside updateDeliveryTime :: Start  ============= ");
			LOGGER.info(" ===orderId : " + orderId + " deliveryTime : " + deliveryTime);
			if (deliveryTime != null) {
				DateFormat inputFormat = new SimpleDateFormat("E MMM dd yyyy HH:mm:ss", Locale.ENGLISH);
				String[] dateArray = deliveryTime.split(" ");
				String newInput = dateArray[0] + " " + dateArray[1] + " " + dateArray[2] + " " + dateArray[3] + " "
						+ dateArray[4];
				Date date = inputFormat.parse(newInput);
				System.out.println(date);

				DateFormat outputFormat = new SimpleDateFormat("MM-dd-yyyy hh:mm:ss", Locale.ENGLISH);

				String parseDate = outputFormat.format(date);
				Date d = outputFormat.parse(parseDate);
				System.out.println(d);

				order.setFulfilled_on(d);
			}
		} catch (ParseException e) {
			LOGGER.error(
					"===============  OrderServiceImpl : Inside updateDeliveryTime :: Exception  ============= " + e);

			MailSendUtil.sendExceptionByMail(e,environment);
			LOGGER.error("error: " + e.getMessage());
		}
		OrderR response = orderRepository.save(order);
		if (response != null) {
			LOGGER.info("===============  OrderServiceImpl : Inside updateDeliveryTime :: End  ============= ");

			return true;
		}
		LOGGER.info("===============  OrderServiceImpl : Inside updateDeliveryTime :: End  ============= ");

		return false;
	}

	public String setOrderStatus(Integer orderId, String type, String reason, String changedOrderAvgTime,
			Integer merchantId) {
		LOGGER.info("===============  OrderServiceImpl : Inside setOrderStatus :: Start  ============= ");
		LOGGER.info(" ===orderId : " + orderId + " type : " + type + " reason : " + reason + " changedOrderAvgTime : "
				+ changedOrderAvgTime + " merchantId : " + merchantId);
		String responseText = null;
		double conveniencePercent =0;
		PaymentGateWay paymentGateWay = null;
		if (merchantId != null) {
			paymentGateWay = paymentGateWayRepository.findByMerchantIdAndIsDeletedAndIsActive(merchantId, false, true);
		}

		if (null == orderId) {
			LOGGER.info("===============  OrderServiceImpl : Inside setOrderStatus :: End  ============= ");

			return "order doesn't exist";
		}
		OrderR orderR = orderRepo.findByMerchantIdAndId(merchantId, orderId);
		if (orderR == null) {
			LOGGER.info("===============  OrderServiceImpl : Inside setOrderStatus :: End  ============= ");

			return "order doesn't exist";
		} else {
			if (orderR.getMerchant() != null && orderR.getMerchant().getOwner() != null
					&& orderR.getMerchant().getOwner().getPos() != null
					&& orderR.getMerchant().getOwner().getPos().getPosId() != null) {
				posTypeId = orderR.getMerchant().getOwner().getPos().getPosId();
			}
			if (orderR.getIsDefaults() != null && orderR.getIsDefaults() == 1) {
				responseText = "order has been approved already";
			} else if (orderR.getIsDefaults() != null && orderR.getIsDefaults() == 2) {
				responseText = "order has been declined already";
			}
			if (orderR.getIsDefaults() != null && orderR.getIsDefaults() == 0) {
				Merchant merchant = orderR.getMerchant();
				String merchantLogo = "";
				ConvenienceFee convenienceFee = null;
				PickUpTime pickUpTime = null;
				String convenienceFeeValue = "0";
				String orderAvgTime = "0";
				String customerName = "";
				if (orderR.getCustomer().getFirstName() != null && orderR.getCustomer().getLastName() != null) {
					customerName = orderR.getCustomer().getFirstName().concat(" ")
							.concat(orderR.getCustomer().getLastName());
				} else if (orderR.getCustomer().getFirstName() != null) {
					customerName = orderR.getCustomer().getFirstName();
				}

				if (merchant != null) {
					LOGGER.info(" === merchant : " + merchant.getId());
					List<ConvenienceFee> convenienceFees = convenienceFeeRepository.findByMerchantId(merchant.getId());
					if (convenienceFees != null && convenienceFees.size() > 0) {
						int index = convenienceFees.size() - 1;
						convenienceFee = convenienceFees.get(index);

					}
					pickUpTime = pickUpTimeRepository.findByMerchantId(merchant.getId());

					if (orderR != null) {
						orderAvgTime = orderR.getOrderAvgTime();
					}
					if (convenienceFee != null) {
						double ConvenienceFeePercent =  (convenienceFee.getConvenienceFeePercent()!=null) ? convenienceFee.getConvenienceFeePercent() : 0;
				        conveniencePercent = (new Double(orderR.getSubTotal())*(ConvenienceFeePercent/100.0));
						Double finalConvenienceFee = new Double(convenienceFee.getConvenienceFee())+conveniencePercent;
						finalConvenienceFee = Math.round(finalConvenienceFee * 100.0) / 100.0;
						convenienceFeeValue = finalConvenienceFee.toString();
					}
					if (merchant.getMerchantLogo() == null) {
						merchantLogo = "http://www.dev.foodkonnekt.com/app/foodnew.jpg";
					} else {
						merchantLogo = environment.getProperty("BASE_PORT") + merchant.getMerchantLogo();
					}
				}

				if (type.equalsIgnoreCase("Accept")) {
					if (orderR.getPaymentMethod() != null && orderR.getPaymentMethod().equals("Credit Card")) {
						PaymentGateWay gateway = paymentGateWayRepository.findByMerchantIdAndIsDeleted(merchant.getId(),
								false);
						if (gateway != null && gateway.getGateWayType().equals("3")) {
							LOGGER.info(" === orderId : " + orderId);
							OrderPaymentDetail paymentDetail = paymentDetailRepository.findByOrderId(orderId);
							// CardInfo cardInfo = null;
							if (paymentDetail != null)
								try {
									CreditSoapProxy creditSoapProxy = new CreditSoapProxy();
									MerchantCredentials credentials = new MerchantCredentials();
									credentials.setMerchantKey(
											EncryptionDecryptionUtil.decryption(gateway.getCayanMerchantKey()));
									credentials.setMerchantName(
											EncryptionDecryptionUtil.decryption(gateway.getCayanMerchantName()));
									credentials.setMerchantSiteId(
											EncryptionDecryptionUtil.decryption(gateway.getCayanMerchantSiteId()));

									CaptureRequest captureRequest = new CaptureRequest();
									captureRequest.setAmount(String.valueOf(orderR.getOrderPrice()));
									captureRequest.setToken(
											EncryptionDecryptionUtil.decryption(paymentDetail.getPreAuthToken()));

									TransactionResponse45 responseCapture = creditSoapProxy.capture(credentials,
											captureRequest);
									if (responseCapture.getApprovalStatus().equals("APPROVED")) {
										paymentDetail.setPostAuthToken(
												EncryptionDecryptionUtil.encryption(responseCapture.getToken()));
										paymentDetailRepository.save(paymentDetail);
									} else {
										LOGGER.info(
												"===============  OrderServiceImpl : Inside setOrderStatus :: End  ============= ");

										// something went wrong
										return "false";
									}
								} catch (RemoteException e) {
									LOGGER.error(
											"===============  OrderServiceImpl : Inside setOrderStatus :: Exception  ============= "
													+ e);
									// TODO Auto-generated catch block
									LOGGER.error("error: " + e.getMessage());
								}
						}
						if (gateway != null && gateway.getGateWayType().equals("4")) {
							OrderPaymentDetail paymentDetail = paymentDetailRepository.findByOrderId(orderId);
							if (paymentDetail != null) {
								try {
									SmartPaymentsSoapProxy soapProxy = new SmartPaymentsSoapProxy(
											environment.getProperty("ENDPOINT_PROD"));
									Response responseCapture = soapProxy.processCreditCard(
											EncryptionDecryptionUtil.decryption(gateway.getiGateUserName()),
											EncryptionDecryptionUtil.decryption(gateway.getiGatePassword()), "Force",
											"", "", "", "", "", "",
											EncryptionDecryptionUtil.decryption(paymentDetail.getPreAuthToken()), "",
											"", "", "");

									LOGGER.info("Tgate capture response : : -----> " + responseCapture);
									if (responseCapture != null && responseCapture.getMessage() != null
											&& responseCapture.getMessage().contains("APPROVAL")) {
										/*
										 * paymentDetail.setPostAuthToken(getEncyKey(responseCapture.getPNRef(),
										 * merchant.getId()));
										 */
										paymentDetail.setPostAuthToken(
												EncryptionDecryptionUtil.encryption(responseCapture.getPNRef()));
										paymentDetailRepository.save(paymentDetail);

									}

									if (responseCapture != null && responseCapture.getMessage() != null
											&& responseCapture.getMessage().contains("APPROVED")) {
										/*
										 * paymentDetail.setPostAuthToken(getEncyKey(responseCapture.getPNRef(),
										 * merchant.getId()));
										 */
										paymentDetail.setPostAuthToken(
												EncryptionDecryptionUtil.encryption(responseCapture.getPNRef()));
										paymentDetailRepository.save(paymentDetail);

									}

								} catch (Exception e) {
									LOGGER.error(
											"===============  OrderServiceImpl : Inside setOrderStatus :: Exception  ============= "
													+ e);
									LOGGER.error("error: " + e.getMessage());
								}
							}
						}

						if (gateway != null && gateway.getGateWayType().equals("2")) {
							OrderPaymentDetail paymentDetail = paymentDetailRepository.findByOrderId(orderId);
							if (paymentDetail != null) {
								try {
									// Common code to set for all requests
									ApiOperationBase.setEnvironment(ProducerUtil.getAuthorizeEnv(environment.getProperty("AuthorizeEnv")));

									MerchantAuthenticationType merchantAuthenticationType = new MerchantAuthenticationType();
									merchantAuthenticationType.setName(
											EncryptionDecryptionUtil.decryption(gateway.getAuthorizeLoginId()));
									merchantAuthenticationType.setTransactionKey(
											EncryptionDecryptionUtil.decryption(gateway.getAuthorizeTransactionKey()));
									ApiOperationBase.setMerchantAuthentication(merchantAuthenticationType);

									TransactionRequestType txnRequest = new TransactionRequestType();
									txnRequest.setTransactionType(
											TransactionTypeEnum.PRIOR_AUTH_CAPTURE_TRANSACTION.value());
									txnRequest.setRefTransId(
											EncryptionDecryptionUtil.decryption(paymentDetail.getPreAuthToken())); // id
																													// recieved
																													// from
																													// previous
																													// authorizeCreditcard
																													// method

									// Make the API Request
									CreateTransactionRequest apiRequest = new CreateTransactionRequest();
									apiRequest.setTransactionRequest(txnRequest);
									CreateTransactionController controller = new CreateTransactionController(
											apiRequest);
									controller.execute();
									CreateTransactionResponse responseCapture = controller.getApiResponse();

									if (responseCapture != null) {

										// If API Response is ok, go ahead and check the transaction response
										if (responseCapture.getMessages().getResultCode() == MessageTypeEnum.OK) {
											TransactionResponse result = responseCapture.getTransactionResponse();
											/*
											 * paymentDetail.setPostAuthToken(getEncyKey(result.getTransId(),
											 * merchant.getId()));
											 */
											paymentDetail.setPostAuthToken(
													EncryptionDecryptionUtil.encryption(result.getTransId()));
											// cardInfo.setPostAuthToken(EncryptionDecryptionUtil.encryption(responseCapture.getToken()));
											paymentDetailRepository.save(paymentDetail);
										}
									}

								} catch (Exception e) {
									LOGGER.error(
											"===============  OrderServiceImpl : Inside setOrderStatus :: Exception  ============= "
													+ e);
									LOGGER.error("error: " + e.getMessage());
								}
							}
						}
						if (gateway != null && gateway.getGateWayType().equals("1")) {
							OrderPaymentDetail paymentDetail = paymentDetailRepository.findByOrderId(orderId);
							if (paymentDetail != null) {
								try {
									Charge charge = null;
									Stripe.apiKey = EncryptionDecryptionUtil.decryption(gateway.getStripeAPIKey());
									charge = Charge.retrieve(
											EncryptionDecryptionUtil.decryption(paymentDetail.getPreAuthToken()));
									    //FW-1274
								    if(charge!=null && !charge.getCaptured()) {							
									    charge.capture();
							        }
									if (charge != null && charge.getStatus() != null
											&& charge.getStatus().equals("succeeded")) {
										paymentDetail
												.setPostAuthToken(EncryptionDecryptionUtil.encryption(charge.getId()));
										paymentDetailRepository.save(paymentDetail);
									}
								} catch (Exception e) {
									LOGGER.error(
											"===============  OrderServiceImpl : Inside setOrderStatus :: Exception  ============= "
													+ e);
									LOGGER.error("error: " + e.getMessage());
								}
							}
						}

						if (gateway != null && gateway.getGateWayType().equals("6")) {
							OrderPaymentDetail paymentDetail = paymentDetailRepository.findByOrderId(orderId);
							if (paymentDetail != null)
								try {
									String merchantToken = EncryptionDecryptionUtil
											.decryption(gateway.getPayeezyMerchnatToken());
									PayeezyClientHelper client = PayeezyUtilProperties
											.getPayeezyProperties(merchantToken,environment);
									String transactionId = EncryptionDecryptionUtil
											.decryption(paymentDetail.getTransactionId());
									TransactionRequest trans = new TransactionRequest();
									String transactiontag = EncryptionDecryptionUtil
											.decryption(paymentDetail.getPreAuthToken());
									LOGGER.info(
											"transactionId---" + transactionId + "transactiontag---" + transactiontag);
									Double totalInDouble = orderR.getOrderPrice();
									int totalInInt = (int) Math.round(totalInDouble * 100);

									LOGGER.info("totalprice" + totalInInt);
									trans.setPaymentMethod("credit_card");
									trans.setCurrency("USD");
									trans.setTransactionType("CAPTURE");
									trans.setTransactionTag(transactiontag);
									trans.setAmount(String.valueOf(totalInInt));
									if (merchant.getName() != null) {
										trans.setReferenceNo("Online order for -  " + merchant.getName());
									}
									try {
										JSONHelper jsonHelper = new JSONHelper();
										String payload = jsonHelper.getJSONObject(trans);
										LOGGER.info("OrderServiceImpl  : Payeezy payload for CAPTURE transaction :  "
												+ payload);

										PayeezyResponse payeezyResponseCaputre = client
												.doSecondaryTransaction(transactionId, trans);
										JSONObject payeezyCaputrejObject = new JSONObject(
												payeezyResponseCaputre.getResponseBody());
										LOGGER.info("payeezyCaputrejObject-------"
												+ payeezyResponseCaputre.getResponseBody());
										if (payeezyCaputrejObject.has("transaction_status")) {
											String response = payeezyCaputrejObject.getString("transaction_status");
											LOGGER.info(" === response : " + response);
											if (response != null && response.equals("approved")) {
												String transctiontag = payeezyCaputrejObject
														.getString("transaction_tag");
												String transaction_id = payeezyCaputrejObject
														.getString("transaction_id");
												paymentDetail.setPostPayezzeyTransctionId(
														EncryptionDecryptionUtil.encryption(transaction_id));
												paymentDetail.setPostAuthToken(
														EncryptionDecryptionUtil.encryption(transctiontag));
												paymentDetailRepository.save(paymentDetail);
											} else {
												LOGGER.info("payeezyCaputrejObject-------"
														+ payeezyResponseCaputre.getResponseBody());
												LOGGER.error(
														"OrderServiceImpl : payezzy not Caputre  Failed    orderID : "
																+ orderId + " "
																+ payeezyResponseCaputre.getResponseBody());
												MailSendUtil.sendPaymentGatwayMailToAdmin(
														payeezyResponseCaputre.getResponseBody(), "postAuth", "payeezy",
														orderR,
														ProducerUtil.payeezyErrorResponse(payeezyResponseCaputre),environment);
											}
										}

									} catch (Exception e) {
										LOGGER.error(
												"===============  OrderServiceImpl : Inside setOrderStatus :: Exception  ============= "
														+ e);
										LOGGER.error("error: " + e.getMessage());
									}

								} catch (Exception e) {
									LOGGER.error(
											"===============  OrderServiceImpl : Inside setOrderStatus :: Exception  ============= "
													+ e);
									LOGGER.error("error: " + e.getMessage());
								}
						}
					}
					orderR.setIsDefaults(1);
					if (changedOrderAvgTime != null && changedOrderAvgTime != "" && !changedOrderAvgTime.isEmpty()) {

						orderR.setOrderAvgTime(changedOrderAvgTime);
						orderAvgTime = changedOrderAvgTime;

					}
					orderRepo.save(orderR);
					String orderDetails = "";

					List<Map<String, String>> extraList = null;
					Map<String, String> poduct = null;
					Map<String, String> exctra = null;
					List<Map<String, String>> productList = new ArrayList<Map<String, String>>();
					LOGGER.info(" === orderR : " + orderR.getId());
					List<OrderItem> orderItems = orderItemRepository.findByOrderId(orderR.getId());
					for (OrderItem orderItem : orderItems) {
						if (orderItem.getItem() != null) {
							String items = "<tr style='font-weight:600;text-transform:capitalize;font-family:arial'><td width='200px;'>"
									+ orderItem.getItem().getName()
									+ "</td><td width='100px;' style='text-align:center'>" + orderItem.getQuantity()
									+ "</td><td width='100px;' style='text-align:center'>" + "$"
									+ String.format("%.2f",orderItem.getItem().getPrice()) + "</td></tr>";
							orderDetails = orderDetails + "" + "<b>" + items + "</b>";
						}
						List<OrderItemModifier> itemModifiers = itemModifierRepository
								.findByOrderItemId(orderItem.getId());
						for (OrderItemModifier itemModifier : itemModifiers) {
							String modifiers = "<tr style='text-transform:capitalize;font-family:arial;margin-top:5px;font-size:12px'><td width='200px;'>"
									+ itemModifier.getModifiers().getName()
									+ "</td><td width='100px;' style='text-align:center'>" + itemModifier.getQuantity()
									+ " </td><td width='100px;' style='text-align:center'> " + "$"
									+ String.format("%.2f",itemModifier.getModifiers().getPrice()) + "</td></tr>";
							orderDetails = orderDetails + "" + modifiers;
						}
					}

					/*
					 * -------------------------------------------------------------For Pizza
					 * View-------------------------------------------------------------------------
					 * ---------
					 */
					List<OrderPizza> orderPizzas = orderPizzaRepository.findByOrderId(orderR.getId());
					if (!orderPizzas.isEmpty() && orderPizzas != null) {
						for (OrderPizza orderPizza : orderPizzas) {
							poduct = new HashMap<String, String>();
							if (orderPizza != null && orderPizza.getPizzaTemplate() != null) {

								Item item = new Item();
								if (orderPizza != null && orderPizza.getPizzaTemplate() != null
										&& orderPizza.getPizzaTemplate().getDescription() != null
										&& orderPizza.getPizzaSize() != null
										&& orderPizza.getPizzaSize().getDescription() != null) {
									item.setName(orderPizza.getPizzaTemplate().getDescription() + "("
											+ orderPizza.getPizzaSize().getDescription() + ")");

									String pizza = "<tr style='font-weight:600;text-transform:capitalize;font-family:arial'><td width='200px;'>"
											+ orderPizza.getPizzaTemplate().getDescription() + "("
											+ orderPizza.getPizzaSize().getDescription() + ")"
											+ "</td><td width='100px;' style='text-align:center'>"
											+ orderPizza.getQuantity()
											+ "</td><td width='100px;' style='text-align:center'>" + "$"
											+ String.format("%.2f", orderPizza.getPrice()) + "</td></tr>";
									orderDetails = orderDetails + "" + "<b>" + pizza + "</b>";

									poduct.put("product_id", orderPizza.getPizzaTemplate().getPosPizzaTemplateId());
									poduct.put("name", orderPizza.getPizzaTemplate().getDescription() + "("
											+ orderPizza.getPizzaSize().getDescription() + ")");
									poduct.put("price", Double.toString(orderPizza.getPrice()));
									poduct.put("qty", Integer.toString(orderPizza.getQuantity()));

								}
								item.setPrice(orderPizza.getPrice());
								LOGGER.info(" === orderPizza.getId() : " + orderPizza.getId());

								List<OrderPizzaToppings> orderPizzaToppings = orderPizzaToppingsRepository
										.findByOrderPizzaId(orderPizza.getId());
								extraList = new ArrayList<Map<String, String>>();
								if (!orderPizzaToppings.isEmpty() && orderPizzaToppings != null) {
									for (OrderPizzaToppings pizzaToppings : orderPizzaToppings) {
										exctra = new HashMap<String, String>();
										Modifiers itemModifier = new Modifiers();
										if (pizzaToppings != null && pizzaToppings.getPizzaTopping() != null
												&& pizzaToppings.getPizzaTopping().getDescription() != null) {
											itemModifier.setName(pizzaToppings.getPizzaTopping().getDescription());
											itemModifier.setPrice(pizzaToppings.getPrice());

											String modifiers = "<tr style='text-transform:capitalize;font-family:arial;margin-top:5px;font-size:12px'><td width='200px;'>"
													+ pizzaToppings.getPizzaTopping().getDescription();

											if (pizzaToppings.getSide1() != null && pizzaToppings.getSide1()) {
												modifiers += " (First Half)";
											} else if (pizzaToppings.getSide1() != null && pizzaToppings.getSide2()) {
												modifiers += " (Second Half)";
											} else {
												modifiers += " (Full)";
											}
											modifiers += "</td><td width='100px;' style='text-align:center'>"
													+ orderPizza.getQuantity()
													+ " </td><td width='100px;' style='text-align:center'> " + "$"
													+ String.format("%.2f", pizzaToppings.getPrice()) + "</td></tr>";

											orderDetails = orderDetails + "" + modifiers;

											exctra.put("id", pizzaToppings.getPizzaTopping().getPosPizzaToppingId());
											exctra.put("price", Double.toString(pizzaToppings.getPrice()));
											exctra.put("name", pizzaToppings.getPizzaTopping().getDescription());
											exctra.put("qty", orderPizza.getQuantity().toString());
											extraList.add(exctra);
										}
									}
								}

								OrderPizzaCrust orderPizzaCrust = orderPizzaCrustRepository
										.findByOrderPizzaId(orderPizza.getId());
								if (orderPizzaCrust != null && orderPizzaCrust.getPizzacrust() != null) {
									exctra = new HashMap<String, String>();
									String crust = "<tr style='text-transform:capitalize;font-family:arial;margin-top:5px;font-size:12px'><td width='200px;'>"
											+ orderPizzaCrust.getPizzacrust().getDescription();

									crust += "</td><td width='100px;' style='text-align:center'>"
											+ orderPizza.getQuantity()
											+ " </td><td width='100px;' style='text-align:center'> " + "$"
											+ String.format("%.2f", orderPizzaCrust.getPrice()) + "</td></tr>";
									orderDetails = orderDetails + "" + crust;

									exctra.put("id", orderPizzaCrust.getPizzacrust().getId().toString());
									exctra.put("price", Double.toString(orderPizzaCrust.getPrice()));
									exctra.put("name", orderPizzaCrust.getPizzacrust().getDescription());
									exctra.put("qty", orderPizza.getQuantity().toString());
								}
							}
							Gson gson = new Gson();
							if (extraList.size() > 0) {
								String extraJson = gson.toJson(extraList);
								poduct.put("extras", extraJson);
								productList.add(poduct);
							} else {
								productList.add(poduct);
							}
						}
					}

					/*
					 * -----------------------------------------------------------------------------
					 * -------------------------
					 * ---------------------------------------------------------
					 */

					orderDetails = "<table width='300px;'><tbody>" + orderDetails + "</table></tbody>";
					System.out.println(orderDetails);
					String deliveryFee = "0";
					if (orderR.getDeliveryFee() != null && !orderR.getDeliveryFee().isEmpty())
						deliveryFee = orderR.getDeliveryFee();
					Double orderDiscount = 0.0;
					String discountCoupon = "";
					if (orderR.getOrderDiscount() != null) {
						orderDiscount = orderR.getOrderDiscount();

						List<OrderDiscount> orderDiscounts = orderDiscountRepository.findByOrderId(orderR.getId());
						if (orderDiscounts != null && !orderDiscounts.isEmpty()) {
							discountCoupon = discountCoupon + "<tr>" + "<td>Discount Coupon</td>" + "<td>&nbsp;</td>"
									+ "<td>" + orderDiscounts.get(0).getCouponCode() + "</td>" + "</tr>";

							if (orderDiscounts.size() > 1) {
								for (int i = 1; i < orderDiscounts.size(); i++) {
									discountCoupon = discountCoupon + "<tr>" + "<td></td>" + "<td>&nbsp;</td>" + "<td>"
											+ orderDiscounts.get(i).getCouponCode() + "</td>" + "</tr>";
								}
							}
						}
					}
					mailSendUtil.sendConfirmMail(customerName, orderR.getPaymentMethod(), orderR.getOrderPosId(),
							orderR.getSubTotal(), orderR.getTax(), orderDetails, orderR.getCustomer().getEmailId(),
							orderR.getOrderPrice(), orderR.getOrderNote(), orderR.getMerchant().getName(), merchantLogo,
							orderR.getOrderType(), convenienceFeeValue, orderAvgTime, deliveryFee, orderDiscount,
							orderR.getTipAmount(), orderR.getCustomer().getId(), orderR.getId(), posTypeId,
							discountCoupon, merchant.getId(), paymentGateWay);
					responseText = "true";
					storeOrderIntoFreekwent(orderR, merchant.getMerchantUid());
				} else {
					if (orderR.getPaymentMethod() != null && orderR.getPaymentMethod().equals("Credit Card")) {
						PaymentGateWay gateway = paymentGateWayRepository.findByMerchantIdAndIsDeleted(merchant.getId(),
								false);
						if (gateway != null && gateway.getGateWayType().equals("3")) {
							OrderPaymentDetail paymentDetail = paymentDetailRepository.findByOrderId(orderId);
							// CardInfo cardInfo = null;
							if (paymentDetail != null) {
								// cardInfo = cardInfoRepository.findOne(paymentDetail.getCardInfoId().getId());
								// if(cardInfo!=null){
								try {
									CreditSoapProxy creditSoapProxy = new CreditSoapProxy();
									MerchantCredentials credentials = new MerchantCredentials();
									credentials.setMerchantKey(
											EncryptionDecryptionUtil.decryption(gateway.getCayanMerchantKey()));
									credentials.setMerchantName(
											EncryptionDecryptionUtil.decryption(gateway.getCayanMerchantName()));
									credentials.setMerchantSiteId(
											EncryptionDecryptionUtil.decryption(gateway.getCayanMerchantSiteId()));

									VoidRequest voidRequest = new VoidRequest();
									voidRequest.setToken(
											EncryptionDecryptionUtil.decryption(paymentDetail.getPreAuthToken()));

									TransactionResponse45 voidResponse = creditSoapProxy._void(credentials,
											voidRequest);
									if (voidResponse.getApprovalStatus().equals("APPROVED")) {
										// paymentDetail.setVoidToken(getEncyKey(paymentDetail.getPreAuthToken(),merchant.getId()));
										paymentDetail.setVoidToken(
												EncryptionDecryptionUtil.encryption(voidResponse.getToken()));
										// cardInfo.setVoidToken(EncryptionDecryptionUtil.encryption(voidResponse.getToken()));
										// cardInfoRepository.save(cardInfo);
										paymentDetailRepository.save(paymentDetail);
									}

								} catch (Exception e) {
									LOGGER.error(
											"===============  OrderServiceImpl : Inside setOrderStatus :: Exception  ============= "
													+ e);
									LOGGER.error("error: " + e.getMessage());
								}
							}
							// }
						}
						if (gateway != null && gateway.getGateWayType().equals("4")) {
							OrderPaymentDetail paymentDetail = paymentDetailRepository.findByOrderId(orderId);
							if (paymentDetail != null) {
								try {
									SmartPaymentsSoapProxy soapProxy = new SmartPaymentsSoapProxy(
											environment.getProperty("ENDPOINT_PROD"));
									Response voidResponse = soapProxy.processCreditCard(
											EncryptionDecryptionUtil.decryption(gateway.getiGateUserName()),
											EncryptionDecryptionUtil.decryption(gateway.getiGatePassword()), "void", "",
											"", "", "", "", "",
											EncryptionDecryptionUtil.decryption(paymentDetail.getPreAuthToken()), "",
											"", "", "");

									LOGGER.info("TGATE void response : " + voidResponse);

									if (voidResponse != null && voidResponse.getMessage() != null
											&& voidResponse.getMessage().contains("APPROVAL")) {
										// paymentDetail.setVoidToken(getEncyKey(voidResponse.getPNRef(),
										// merchant.getId()));
										paymentDetail.setVoidToken(
												EncryptionDecryptionUtil.encryption(voidResponse.getPNRef()));
										paymentDetailRepository.save(paymentDetail);
									}

									else if (voidResponse != null && voidResponse.getMessage() != null
											&& voidResponse.getMessage().contains("APPROVED")) {
										// paymentDetail.setVoidToken(getEncyKey(voidResponse.getPNRef(),
										// merchant.getId()));
										paymentDetail.setVoidToken(
												EncryptionDecryptionUtil.encryption(voidResponse.getPNRef()));
										paymentDetailRepository.save(paymentDetail);
									}

								} catch (Exception e) {
									LOGGER.error(
											"===============  OrderServiceImpl : Inside setOrderStatus :: Exception  ============= "
													+ e);
									LOGGER.error("error: " + e.getMessage());
								}
							}
						}
						if (gateway != null && gateway.getGateWayType().equals("2")) {
							OrderPaymentDetail paymentDetail = paymentDetailRepository.findByOrderId(orderId);
							if (paymentDetail != null) {
								try {
									// Common code to set for all requests
									ApiOperationBase.setEnvironment(ProducerUtil.getAuthorizeEnv(environment.getProperty("AuthorizeEnv")));
									MerchantAuthenticationType merchantAuthenticationType = new MerchantAuthenticationType();
									merchantAuthenticationType.setName(
											EncryptionDecryptionUtil.decryption(gateway.getAuthorizeLoginId()));
									merchantAuthenticationType.setTransactionKey(
											EncryptionDecryptionUtil.decryption(gateway.getAuthorizeTransactionKey()));
									ApiOperationBase.setMerchantAuthentication(merchantAuthenticationType);
									// Create the payment transaction request
									TransactionRequestType txnRequest = new TransactionRequestType();
									txnRequest.setTransactionType(TransactionTypeEnum.VOID_TRANSACTION.value());
									txnRequest.setRefTransId(paymentDetail.getPreAuthToken());
									txnRequest.setRefTransId(
											EncryptionDecryptionUtil.decryption(paymentDetail.getPreAuthToken()));

									// Make the API Request
									CreateTransactionRequest apiRequest = new CreateTransactionRequest();
									apiRequest.setTransactionRequest(txnRequest);
									CreateTransactionController controller = new CreateTransactionController(
											apiRequest);
									controller.execute();
									CreateTransactionResponse voidResponse = controller.getApiResponse();
									if (voidResponse != null) {
										// If API Response is ok, go ahead and check the transaction response
										if (voidResponse.getMessages().getResultCode() == MessageTypeEnum.OK) {
											TransactionResponse result = voidResponse.getTransactionResponse();
											if (result.getMessages() != null) {
												paymentDetail.setVoidToken(
														EncryptionDecryptionUtil.encryption(result.getTransId()));
												paymentDetailRepository.save(paymentDetail);
											}
										}
									}

								} catch (Exception e) {
									LOGGER.error(
											"===============  OrderServiceImpl : Inside setOrderStatus :: Exception  ============= "
													+ e);
									LOGGER.error("error: " + e.getMessage());
								}
							}
						}
						if (gateway != null && gateway.getGateWayType().equals("1")) {
							OrderPaymentDetail paymentDetail = paymentDetailRepository.findByOrderId(orderId);
							if (paymentDetail != null) {
								try {
									Charge charge = null;
									Stripe.apiKey = EncryptionDecryptionUtil.decryption(gateway.getStripeAPIKey());
									charge = Charge.retrieve(
											EncryptionDecryptionUtil.decryption(paymentDetail.getPreAuthToken()));
									if(charge!=null && !charge.getCaptured()) {
										charge.capture();
									}
									if (charge != null && charge.getStatus() != null
											&& charge.getStatus().equals("succeeded")) {
										// paymentDetail.setPostAuthToken(EncryptionDecryptionUtil.encryption(charge.getId()));
										// paymentDetailRepository.save(paymentDetail);

										Refund refund = null;
										Map<String, Object> refundParams = new HashMap<String, Object>();
										refundParams.put("charge", charge.getId());
										refund = Refund.create(refundParams);
										if (refund.getAmount() != null && refund != null) {
											paymentDetail
													.setVoidToken(EncryptionDecryptionUtil.encryption(refund.getId()));
											// cardInfo.setPostAuthToken(EncryptionDecryptionUtil.encryption(responseCapture.getToken()));
											paymentDetailRepository.save(paymentDetail);
										}
									}
								} catch (Exception e) {
									LOGGER.error(
											"===============  OrderServiceImpl : Inside setOrderStatus :: Exception  ============= "
													+ e);
									LOGGER.error("error: " + e.getMessage());
								}

							}
						}

						if (gateway != null && gateway.getGateWayType().equals("5")) {
							OrderPaymentDetail paymentDetail = paymentDetailRepository.findByOrderId(orderId);
							if (paymentDetail != null) {
								try {
									String firstDataMerchantId = EncryptionDecryptionUtil
											.decryption(gateway.getFirstDataMerchantId());
									String firstDataUserName = EncryptionDecryptionUtil
											.decryption(gateway.getFirstDataUserName());
									String firstDataPassword = EncryptionDecryptionUtil
											.decryption(gateway.getFirstDataPassword());
									CardConnectRestClientExample restExample = new CardConnectRestClientExample();
									// Create Update Transaction request
									org.json.simple.JSONObject request = new org.json.simple.JSONObject();
									request.put("merchid", firstDataMerchantId);
									request.put("amount", "0");
									request.put("currency", "USD");
									request.put("retref", paymentDetail.getFirstDataRetRef());
									org.json.simple.JSONObject response = CardConnectRestClientExample.voidTransaction(firstDataUserName,
											firstDataPassword, request);
								} catch (Exception e) {
									LOGGER.error(
											"===============  OrderServiceImpl : Inside setOrderStatus :: Exception  ============= "
													+ e);
									LOGGER.error("error: " + e.getMessage());
								}

							}
						}

						if (gateway != null && gateway.getGateWayType().equals("6")) {
							OrderPaymentDetail paymentDetail = paymentDetailRepository.findByOrderId(orderId);
							if (paymentDetail != null)
								try {
									String merchantToken = EncryptionDecryptionUtil
											.decryption(gateway.getPayeezyMerchnatToken());
									PayeezyClientHelper client = PayeezyUtilProperties
											.getPayeezyProperties(merchantToken,environment);
									String transactionId = EncryptionDecryptionUtil
											.decryption(paymentDetail.getTransactionId());
									TransactionRequest trans = new TransactionRequest();
									String transactiontag = EncryptionDecryptionUtil
											.decryption(paymentDetail.getPreAuthToken());
									LOGGER.info(
											"transactionId---" + transactionId + "transactiontag---" + transactiontag);
									Double totalInDouble = orderR.getOrderPrice();
									int totalInInt = (int) Math.round(totalInDouble * 100);

									System.out.println("totalInInt" + totalInInt);
									trans.setPaymentMethod("credit_card");
									trans.setCurrency("USD");
									trans.setTransactionType("VOID");
									trans.setTransactionTag(transactiontag);
									trans.setAmount(String.valueOf(totalInInt));
									if (merchant.getName() != null) {
										trans.setReferenceNo("Online order for -  " + merchant.getName());
									}
									try {
										JSONHelper jsonHelper = new JSONHelper();
										String payload = jsonHelper.getJSONObject(trans);
										LOGGER.info("OrderServiceImpl  : Payeezy payload for VOID transaction :  "
												+ payload);
										PayeezyResponse payeezyResponseCaputre = client
												.doSecondaryTransaction(transactionId, trans);
										JSONObject payeezyCaputrejObject = new JSONObject(
												payeezyResponseCaputre.getResponseBody());
										LOGGER.info("payeezy-VOID--jObject-------"
												+ payeezyResponseCaputre.getResponseBody());
										if (payeezyCaputrejObject.has("transaction_status")) {
											String response = payeezyCaputrejObject.getString("transaction_status");

											if (response != null && response.equals("approved")) {
												String transctiontag = payeezyCaputrejObject
														.getString("transaction_tag");
												String transaction_id = payeezyCaputrejObject
														.getString("transaction_id");
												paymentDetail.setPostPayezzeyTransctionId(
														EncryptionDecryptionUtil.encryption(transaction_id));
												paymentDetail.setPostAuthToken(
														EncryptionDecryptionUtil.encryption(transctiontag));
												paymentDetailRepository.save(paymentDetail);
											} else {
												LOGGER.info("payeezy void Object Failed  orderID : " + orderId + "----"
														+ payeezyResponseCaputre.getResponseBody());
												LOGGER.error("OrderServiceImpl : payezzy not void  Failed    orderID : "
														+ orderId + " " + payeezyResponseCaputre.getResponseBody());
												MailSendUtil.sendPaymentGatwayMailToAdmin(
														payeezyResponseCaputre.getResponseBody(), "postAuth", "payeezy",
														orderR,
														ProducerUtil.payeezyErrorResponse(payeezyResponseCaputre),environment);

											}
										}
									} catch (Exception e) {
										LOGGER.error(
												"===============  OrderServiceImpl : Inside setOrderStatus :: Exception  ============= "
														+ e);

										LOGGER.error("error: " + e.getMessage());
									}

								} catch (Exception e) {
									LOGGER.error(
											"===============  OrderServiceImpl : Inside setOrderStatus :: Exception  ============= "
													+ e);
									LOGGER.error("error: " + e.getMessage());
								}
						}

					}

					orderR.setIsDefaults(2);
					orderRepo.save(orderR);
					if (orderR.getCustomer() != null) {
						MailSendUtil.sendOrderCancellationMail(customerName, null, orderR.getCustomer().getEmailId(),
								orderR.getMerchant().getName(), merchantLogo,
								orderR.getMerchant().getPhoneNumber(), /*
																		 * orderR.getMerchant().getOwner( ).getEmail()
																		 */
								"praveen.raghuvanshii@gmail.com", reason, orderR.getMerchant().getId());
					}
					responseText = "true";
				}

			}

		}
		LOGGER.info("===============  OrderServiceImpl : Inside setOrderStatus :: End  ============= ");

		return responseText;

	}

	public String getAllFutureOrdersByLocationUid(String locationUid) {
		LOGGER.info(
				"===============  OrderServiceImpl : Inside getAllFutureOrdersByLocationUid :: Start  ============= ");
		LOGGER.info(" === locationUid : " + locationUid);
		Merchant merchant = merchantRepository.findByMerchantUid(locationUid);
		String futureOrders = "";
		if (merchant != null) {
//			Integer hourDifference = 0;
//			Integer minutDifference = 0;
//			if (merchant != null && merchant.getTimeZone() != null
//					&& merchant.getTimeZone().getHourDifference() != null) {
//				hourDifference = merchant.getTimeZone().getHourDifference();
//			}
//			if (merchant.getTimeZone().getMinutDifference() != null) {
//				minutDifference = merchant.getTimeZone().getMinutDifference();
//			}
			// DateFormat dateFormat = new
			// SimpleDateFormat(IConstant.YYYYMMDDHHMMSS);
//			Calendar calobj = Calendar.getInstance();
//			calobj.add(Calendar.HOUR, hourDifference);
//			calobj.add(Calendar.MINUTE, minutDifference);
//			LOGGER.info("calobj-" + calobj.getTime());
//            Date date = calobj.getTime();
			
            Date date =  (merchant != null && merchant.getTimeZone() != null
					&& merchant.getTimeZone().getTimeZoneCode() != null)
					? DateUtil.getCurrentDateForTimeZonee(merchant.getTimeZone().getTimeZoneCode())
					: new Date();
			LOGGER.info(" === date : " + date);
			List<OrderR> orderRs = orderRepo.findByFulFilledOnAndIsFutureOrderAndMerchantIdAndIsDefaults(date,
					IConstant.BOOLEAN_TRUE, merchant.getId(), IConstant.Order_Status_Pending);
			List<String> notifications = new ArrayList<String>();

			for (OrderR orderR : orderRs) {
				// String orderDetails = "";
				/*
				 * Map<String, Map<String, String>> notf = new HashMap<String, Map<String,
				 * String>>(); Map<String, String> notification = new HashMap<String, String>();
				 * notification.put("appEvent", "notification"); Map<String, String> order = new
				 * HashMap<String, String>(); order.put("total",
				 * Double.toString(orderR.getOrderPrice())); order.put("tax", orderR.getTax());
				 * List<Map<String, String>> productList = new ArrayList<Map<String, String>>();
				 * List<OrderItem> orderItems =
				 * orderItemRepository.findByOrderId(orderR.getId());
				 * 
				 * for (OrderItem orderItem : orderItems) { if (orderItem != null) { if
				 * (orderItem.getItem() != null) {
				 * 
				 * List<Map<String, String>> extraList = new ArrayList<Map<String, String>>();
				 * Map<String, String> poduct = new HashMap<String, String>();
				 * poduct.put("product_id", orderItem.getItem().getPosItemId());
				 * poduct.put("name", orderItem.getItem().getName()); poduct.put("price",
				 * Double.toString(orderItem.getItem().getPrice())); poduct.put("qty",
				 * Integer.toString(orderItem.getQuantity())); List<OrderItemModifier>
				 * itemModifiers = itemModifierRepository .findByOrderItemId(orderItem.getId());
				 * for (OrderItemModifier itemModifier : itemModifiers) {
				 * 
				 * Map<String, String> exctra = new HashMap<String, String>(); exctra.put("id",
				 * itemModifier.getModifiers().getPosModifierId()); exctra.put("price",
				 * Double.toString(itemModifier.getModifiers().getPrice())); exctra.put("name",
				 * itemModifier.getModifiers().getName()); exctra.put("qty",
				 * Integer.toString(orderItem.getQuantity())); extraList.add(exctra); } Gson
				 * gson = new Gson(); String extraJson = gson.toJson(extraList);
				 * poduct.put("extras", extraJson); productList.add(poduct); } } } Gson gson =
				 * new Gson(); String productJson = gson.toJson(productList);
				 * order.put("productItems", productJson); // order.put("productItems",
				 * productList.toString()); String orderJson = gson.toJson(order);
				 * LOGGER.info(orderJson); Date fulFilledTime = orderR.getFulfilled_on();
				 * Calendar calobjs = Calendar.getInstance(); calobjs.setTime(fulFilledTime);
				 * calobjs.add(Calendar.MINUTE, -Integer.parseInt(orderR.getOrderAvgTime()));
				 * Date orderFulFilledTime = calobjs.getTime();
				 * 
				 * long timeInMillis = orderFulFilledTime.getTime(); // String orderFulTime = //
				 * dateFormat.format(orderFulFilledTime).toString(); notification.put("payload",
				 * orderR.getOrderPosId() + "@#You got a new order(" + orderR.getOrderPosId() +
				 * ") at " + orderR.getCreatedOn() + "@# $" + orderR.getOrderPrice() + "@# " +
				 * orderR.getOrderType() + "@#" + orderJson + "@#" +
				 * orderR.getMerchant().getPosMerchantId() + "@#" + orderR.getOrderNote() +
				 * " @#" + orderR.getPosPaymentId() + " @#" + timeInMillis);
				 * 
				 * notf.put("notification", notification); String notificationJson =
				 * gson.toJson(notf); notificationJson =
				 * notificationJson.trim().replaceAll("\\\\+\"", "'").replace("'[",
				 * "[").replace("]'", "]");
				 */
				String orderNotificationJson = getNotificationJSON(orderR.getId(), merchant.getModileDeviceId(),
						merchant.getMerchantUid(), merchant.getPosMerchantId());
				notifications.add(orderNotificationJson);
				LOGGER.info(" ===orderNotificationJson : " + orderNotificationJson);
				// notifications.add(notificationJson);

			}
			if (notifications.size() > 0) {
				futureOrders = "";
				futureOrders = futureOrders + "{\"code\":200 ,";
				futureOrders = futureOrders + "\"orders\":" + notifications.toString() + "}";
				LOGGER.info(
						"===============  OrderServiceImpl : Inside getAllFutureOrdersByLocationUid :: End  ============= ");

				return futureOrders;
			} else {
				futureOrders = "";
				futureOrders = futureOrders + "{\"code\":0 ,";
				futureOrders = futureOrders + "\"orders\":[]}";
				LOGGER.info(
						"===============  OrderServiceImpl : Inside getAllFutureOrdersByLocationUid :: End  ============= ");

				return futureOrders;
			}
		}
		LOGGER.info(" === futureOrders : " + futureOrders);
		LOGGER.info(
				"===============  OrderServiceImpl : Inside getAllFutureOrdersByLocationUid :: End  ============= ");

		return futureOrders;

	}

	public List<OrderR> getOrderByCustomerId(Integer id, Date startDate, Date endDate) {
		LOGGER.info("===============  OrderServiceImpl : Inside getOrderByCustomerId :: Start  ============= ");
		LOGGER.info(" === id : " + id + " startDate : " + startDate + " endDate : " + endDate);
		List<OrderR> orderRs = new ArrayList<OrderR>();

		if (id != null && startDate != null && endDate != null) {

			orderRs = orderRepo.findByStartDateAndEndDateAndCustomerId(startDate, endDate, id);

		}
		LOGGER.info("===============  OrderServiceImpl : Inside getOrderByCustomerId :: End  ============= ");

		return orderRs;
	}

	public List<OrderR> findCustomerNonTranscation(List<Integer> customerIdList, Integer id) {
		LOGGER.info("===============  OrderServiceImpl : Inside findCustomerNonTranscation :: Start  ============= ");
		LOGGER.info(" === id : " + id);
		for (Integer list : customerIdList) {
			LOGGER.info(" === customer_Id " + list);
		}
		List<OrderR> orderRs = new ArrayList<OrderR>();
		orderRs = orderRepo.findCustomerTranscation(id, customerIdList);
		LOGGER.info("===============  OrderServiceImpl : Inside findCustomerNonTranscation :: End  ============= ");

		return orderRs;
	}

	public List<OrderR> findOrderDetailByCustomerId(int customerId) {
		LOGGER.info("===============  OrderServiceImpl : Inside findOrderDetailByCustomerId :: Start  ============= ");
		LOGGER.info(" === customerId : " + customerId);
		List<OrderR> orderRs = orderRepo.findByCustomerId(customerId);
		LOGGER.info("===============  OrderServiceImpl : Inside findOrderDetailByCustomerId :: End  ============= ");

		return orderRs;
	}

	public List<OrderR> getOrderByCustomerid(Integer id, String startDate, String endDate) {
		LOGGER.info("===============  OrderServiceImpl : Inside getOrderByCustomerid :: Start  ============= ");
		LOGGER.info(" === id : " + id + " startDate : " + startDate + " endDate : " + endDate);

		List<OrderR> list = orderRepository.findByStartDateAndEndDateAndCustomerId(startDate, endDate, id);
		LOGGER.info("===============  OrderServiceImpl : Inside getOrderByCustomerid :: End  ============= ");

		return list;
	}

	public List<OrderR> findCustomerTranscation(List<Integer> customerIdList, Integer id) {
		LOGGER.info("===============  OrderServiceImpl : Inside findCustomerTranscation :: Start  ============= ");

		LOGGER.info(" === id : " + id);
		for (Integer list : customerIdList) {
			LOGGER.info(" === customer_Id " + list);
		}
		List<OrderR> orderRs = new ArrayList<OrderR>();

		orderRs = orderRepo.findCustomerNonTranscation(id, customerIdList);
		LOGGER.info("===============  OrderServiceImpl : Inside findCustomerTranscation :: End  ============= ");

		return orderRs;
	}

	public List<OrderR> getOrderByCustomerIdd(Integer id, String startDate, String endDate) {
		LOGGER.info("===============  OrderServiceImpl : Inside getOrderByCustomerIdd :: Start  ============= ");
		LOGGER.info(" === id : " + id + " startDate : " + startDate + " endDate : " + endDate);

		List<OrderR> orderRs = new ArrayList<OrderR>();
		try {
			Date newstartDate = new SimpleDateFormat("MM-dd-yyyy").parse(startDate);
			Date newendDate = new SimpleDateFormat("MM-dd-yyyy").parse(endDate);
			if (id != null && startDate != null && endDate != null) {
				orderRs = orderRepo.findByStartDateAndEndDateAndCustomerId(newstartDate, newendDate, id);
			}
		} catch (ParseException e) {
			LOGGER.error("===============  OrderServiceImpl : Inside getOrderByCustomerIdd :: Exception  ============= "
					+ e);

			// TODO Auto-generated catch block
			LOGGER.error("error: " + e.getMessage());

		}
		LOGGER.info("===============  OrderServiceImpl : Inside getOrderByCustomerIdd :: End  ============= ");

		return orderRs;
	}

	public List<CategoryDto> getPizzaCategory(Integer merchantId) {
		LOGGER.info("===============  OrderServiceImpl : Inside getPizzaCategory :: Start  ============= ");
		LOGGER.info(" === merchantId : " + merchantId);
		List<Category> categoryList = categoryRepository.findByMerchantIdAndIsPizza(merchantId);
		List<CategoryDto> finalCategories = new ArrayList<CategoryDto>();
		if (categoryList != null && !categoryList.isEmpty()) {
			for (Category category : categoryList) {
				CategoryDto categoryDto = new CategoryDto();
				categoryDto.setId(category.getId());
				categoryDto.setCategoryName(category.getName());
				categoryDto.setCategoryStatus(category.getItemStatus());
				categoryDto.setCategoryImage(category.getCategoryImage());
				categoryDto.setIsPizza(category.getIsPizza());
				categoryDto.setSortOrder(category.getSortOrder());
				finalCategories.add(categoryDto);
			}
		}
		LOGGER.info("===============  OrderServiceImpl : Inside getPizzaCategory :: End  ============= ");

		return finalCategories;
	}

	public String chargeRefundByOrderId(Integer orderId) {
		LOGGER.info("===============  OrderServiceImpl : Inside chargeRefundByOrderId :: Start  ============= ");
		LOGGER.info(" === orderId : " + orderId);
		String responseText = "failed";
		OrderR orderR = orderRepository.findOne(orderId);
		Boolean isDeleted = false;
		Date date = (orderR.getMerchant() != null && orderR.getMerchant().getTimeZone() != null
				&& orderR.getMerchant().getTimeZone().getTimeZoneCode() != null)
						? DateUtil.getCurrentDateForTimeZonee(orderR.getMerchant().getTimeZone().getTimeZoneCode())
						: new Date();
		if (orderR.getPaymentMethod().equals("Credit Card")) {
			LOGGER.info(" === orderR.getMerchant().getId() : " + +orderR.getMerchant().getId() + " isDeleted : "
					+ isDeleted);

			PaymentGateWay gateway = paymentGateWayRepository.findByMerchantIdAndIsDeleted(orderR.getMerchant().getId(),
					isDeleted);
			if (orderR.getId() != null && orderR.getMerchant().getId() != null) {
				OrderPaymentDetail orderPaymentDetail = paymentDetailRepository.findByOrderId(orderId);
				LOGGER.info("===============  OrderServiceImpl : Inside chargeRefundByOrderId : " + orderPaymentDetail);
				if (orderPaymentDetail != null && orderPaymentDetail.getPaymentGatewayId() == 6) {

					if (orderPaymentDetail.getPaymentGatewayId() == 5) {
						LOGGER.info(
								"===============  OrderServiceImpl : Inside chargeRefundByOrderId : orderR.getOrderPrice() :"
										+ orderR.getOrderPrice());
						String merchantId = EncryptionDecryptionUtil.decryption(gateway.getFirstDataMerchantId());
						String firstDataRetRef = EncryptionDecryptionUtil
								.decryption(orderPaymentDetail.getFirstDataRetRef());
						LOGGER.info(
								"===============  OrderServiceImpl : Inside chargeRefundByOrderId : orderPaymentDetail.getFirstDataRetRef():"
										+ orderPaymentDetail.getFirstDataRetRef());
						CardConnectRestClientExample restClient = new CardConnectRestClientExample();
						org.json.simple.JSONObject request = new org.json.simple.JSONObject();
						request.put("merchid", merchantId);
						request.put("amount", orderR.getOrderPrice());
						request.put("currency", "USD");
						request.put("retref", firstDataRetRef);
						org.json.simple.JSONObject response1 = restClient.refundTransaction(request);
						LOGGER.info("===============  OrderServiceImpl : Inside chargeRefundByOrderId : response1 :"
								+ response1.toString());
						if (response1 != null) {
							String response = response1.get("resptext").toString();
							LOGGER.info("===============  OrderServiceImpl : Inside chargeRefundByOrderId : resptext :"
									+ response);

							if (response.equals("Approval")) {
								String newRetref = response1.get("retref").toString();
								LOGGER.info(
										"===============  OrderServiceImpl : Inside chargeRefundByOrderId : newRetref :"
												+ newRetref);
								orderPaymentDetail.setRefundedAuthCode(EncryptionDecryptionUtil.encryption(newRetref));
								// orderPaymentDetail.setRefundedAuthCode(newRetref);
								orderPaymentDetail.setRefundedDate(date);
								orderPaymentDetail.setIsRefunded(true);
								paymentDetailRepository.save(orderPaymentDetail);
								orderR.setIsDefaults(2);
								orderRepo.save(orderR);
								// responseText = "Approval";
								responseText = "The Order has been cancelled";

							} else {
								responseText = response;
								orderPaymentDetail.setIsRefunded(false);

							}

						}

					} else if (orderPaymentDetail.getPaymentGatewayId() == 4) {
						String transactionID = EncryptionDecryptionUtil
								.decryption(orderPaymentDetail.getTransactionId());
						String getPreAuthToken = EncryptionDecryptionUtil
								.decryption(orderPaymentDetail.getPreAuthToken());
						Response response = BridgePayTest.voidCard(getPreAuthToken,environment);
						LOGGER.info("===============  OrderServiceImpl : Inside chargeRefundByOrderId : Response :"
								+ response);
						if (response != null && response.getMessage() != null
								&& response.getMessage().contains("APPROVAL")) {
							orderR.setIsDefaults(2);
							orderRepo.save(orderR);
							orderPaymentDetail
									.setRefundedAuthCode(EncryptionDecryptionUtil.encryption(response.getAuthCode()));
							orderPaymentDetail.setRefundedDate(date);
							orderPaymentDetail.setIsRefunded(true);
							paymentDetailRepository.save(orderPaymentDetail);

							LOGGER.info("===============  OrderServiceImpl : Inside chargeRefundByOrderId : APPROVAL ");
							LOGGER.info(
									"===============  OrderServiceImpl : Inside chargeRefundByOrderId : response.getExtData() :"
											+ response.getExtData());
							LOGGER.info(
									"===============  OrderServiceImpl : Inside chargeRefundByOrderId : response.getAuthCode() :"
											+ response.getAuthCode());
							LOGGER.info(
									"===============  OrderServiceImpl : Inside chargeRefundByOrderId : response.getPNRef() :"
											+ response.getPNRef());
							LOGGER.info(
									"===============  OrderServiceImpl : Inside chargeRefundByOrderId : response.getMessage() :"
											+ response.getMessage());
							LOGGER.info(
									"===============  OrderServiceImpl : Inside chargeRefundByOrderId : response.getRespMSG() :"
											+ response.getRespMSG());
							responseText = "The Order has been cancelled";
						}

						else if (response!=null){
							
							LOGGER.info(
									"===============  OrderServiceImpl : Inside chargeRefundByOrderId : response.getExtData() :"
											+ response.getExtData());
							LOGGER.info(
									"===============  OrderServiceImpl : Inside chargeRefundByOrderId : response.getAuthCode() :"
											+ response.getAuthCode());
							LOGGER.info(
									"===============  OrderServiceImpl : Inside chargeRefundByOrderId : response.getPNRef() :"
											+ response.getPNRef());
							LOGGER.info(
									"===============  OrderServiceImpl : Inside chargeRefundByOrderId : response.getMessage() :"
											+ response.getMessage());
							LOGGER.info(
									"===============  OrderServiceImpl : Inside chargeRefundByOrderId : response.getRespMSG() :"
											+ response.getRespMSG());

							orderPaymentDetail.setIsRefunded(false);

							responseText = response.getMessage();

						}else responseText=null;

					}

					else if (orderPaymentDetail.getPaymentGatewayId() == 2) {
						orderPaymentDetail.getPreAuthToken();
						String transactionID = EncryptionDecryptionUtil
								.decryption(orderPaymentDetail.getPreAuthToken());

						String authorizeLoginId = EncryptionDecryptionUtil.decryption(gateway.getAuthorizeLoginId());
						String authorizeTransactionKey = EncryptionDecryptionUtil
								.decryption(gateway.getAuthorizeTransactionKey());

						String expirationDate = EncryptionDecryptionUtil
								.decryption(orderPaymentDetail.getCardInfoId().getExpirationDate());

						String last4CreditCardNumber = EncryptionDecryptionUtil
								.decryption(orderPaymentDetail.getCardInfoId().getLast4());

						LOGGER.info(
								"===============  OrderServiceImpl : Inside chargeRefundByOrderId : orderPaymentDetail.getPreAuthToken() :"
										+ transactionID + "getPreAuthToken" + authorizeLoginId
										+ "authorizeTransactionKey" + authorizeTransactionKey);
						// CreateTransactionResponse response
						CreateTransactionResponse response = null;
						response = (CreateTransactionResponse) AuthorizeTest.refundTransaction(authorizeLoginId,
								authorizeTransactionKey, orderR.getOrderPrice(), orderPaymentDetail.getPreAuthToken(),
								last4CreditCardNumber, expirationDate,environment);

						LOGGER.info("===============  OrderServiceImpl : Inside chargeRefundByOrderId : Response :"
								+ response);
						if (response != null) {
							// If API Response is ok, go ahead and check the transaction response
							if (response.getMessages().getResultCode() == MessageTypeEnum.OK) {
								TransactionResponse result = response.getTransactionResponse();
								if (result.getMessages() != null) {
									// responseText = "Successfully created transaction with Transaction ID: "+
									// result.getTransId();
									// result.getTransId();
									LOGGER.info(
											"===============  OrderServiceImpl : Inside chargeRefundByOrderId : Auth Code :"
													+ result.getAuthCode());
									orderR.setIsDefaults(2);
									orderRepo.save(orderR);
									orderPaymentDetail.setRefundedAuthCode(
											EncryptionDecryptionUtil.encryption(result.getAuthCode()));
									orderPaymentDetail.setRefundedDate(date);
									orderPaymentDetail.setIsRefunded(true);
									paymentDetailRepository.save(orderPaymentDetail);
									responseText = "The Order has been cancelled";

								} else {
									LOGGER.info(
											"===============  OrderServiceImpl : Inside chargeRefundByOrderId : Failed Transaction.");
									if (response.getTransactionResponse().getErrors() != null) {
										orderPaymentDetail.setIsRefunded(false);
										responseText = "Failed Transaction: " + response.getTransactionResponse()
												.getErrors().getError().get(0).getErrorText();
									}
								}
							} else {
								orderPaymentDetail.setIsRefunded(false);
								LOGGER.info(
										"===============  OrderServiceImpl : Inside chargeRefundByOrderId : Failed Transaction.");
								LOGGER.info("Failed Transaction.");
								if (response.getTransactionResponse() != null
										&& response.getTransactionResponse().getErrors() != null) {
									responseText = "Failed Transaction: " + response.getTransactionResponse()
											.getErrors().getError().get(0).getErrorText();
								} else {
									responseText = "Failed Transaction: "
											+ response.getMessages().getMessage().get(0).getText();

								}
							}
						} else {
							responseText = "Null Response";
						}

					} else if (orderPaymentDetail.getPaymentGatewayId() == 1) {
						if (orderPaymentDetail != null) {
							try {
								Charge charge = null;
								Stripe.apiKey = EncryptionDecryptionUtil.decryption(gateway.getStripeAPIKey());
								charge = Charge.retrieve(
										EncryptionDecryptionUtil.decryption(orderPaymentDetail.getPreAuthToken()));
								if(charge!=null && !charge.getCaptured()) {	
								   charge.capture();
								}  
								if (charge != null && charge.getStatus() != null
										&& charge.getStatus().equals("succeeded")) {
									// paymentDetail.setPostAuthToken(EncryptionDecryptionUtil.encryption(charge.getId()));
									// paymentDetailRepository.save(paymentDetail);

									Refund refund = null;
									Map<String, Object> refundParams = new HashMap<String, Object>();
									refundParams.put("charge", charge.getId());
									refund = Refund.create(refundParams);
									if (refund.getAmount() != null && refund != null) {
										orderPaymentDetail
												.setVoidToken(EncryptionDecryptionUtil.encryption(refund.getId()));
										// cardInfo.setPostAuthToken(EncryptionDecryptionUtil.encryption(responseCapture.getToken()));
										orderPaymentDetail.setRefundedAuthCode(
												EncryptionDecryptionUtil.encryption(refund.getId()));

										orderR.setIsDefaults(2);
										orderRepo.save(orderR);
										orderPaymentDetail.setRefundedDate(date);
										orderPaymentDetail.setIsRefunded(true);
										paymentDetailRepository.save(orderPaymentDetail);
										responseText = "The Order has been cancelled";

									}
								}
							} catch (Exception e) {
								LOGGER.error("error: " + e.getMessage());
								LOGGER.info(
										"===============  OrderServiceImpl : Inside chargeRefundByOrderId : Exception:   ============= "
												+ e);

							}
						}

					} else if (orderPaymentDetail.getPaymentGatewayId() == 6) {
						OrderPaymentDetail paymentDetail = paymentDetailRepository.findByOrderId(orderId);
						if (paymentDetail != null)
							try {

								Double totalInDouble = orderR.getOrderPrice();

								int totalInInt = (int) Math.round(totalInDouble * 100);
								LOGGER.info(
										"===============  OrderServiceImpl : Inside chargeRefundByOrderId : totalInInt : "
												+ orderR.getOrderPrice().toString());
								String merchantToken = EncryptionDecryptionUtil
										.decryption(gateway.getPayeezyMerchnatToken());
								PayeezyClientHelper client = PayeezyUtilProperties.getPayeezyProperties(merchantToken,environment);
								String transactionId = EncryptionDecryptionUtil
										.decryption(paymentDetail.getPostPayezzeyTransctionId());
								TransactionRequest trans = new TransactionRequest();
								trans.setPaymentMethod("credit_card");
								trans.setCurrency("USD");
								trans.setTransactionType("REFUND");
								trans.setTransactionTag(
										EncryptionDecryptionUtil.decryption(paymentDetail.getPostAuthToken()));
								trans.setAmount(String.valueOf(totalInInt));
								{
									trans.setReferenceNo("Online order for -  "
											+ ProducerUtil.removeSpecialCharecter(orderR.getMerchant().getName()));
								}
								try {
									PayeezyResponse payeezyResponseCaputre = client
											.doSecondaryTransaction(transactionId, trans);
									JSONObject payeezyCaputrejObject = new JSONObject(
											payeezyResponseCaputre.getResponseBody());

									LOGGER.info(
											"===============  OrderServiceImpl : Inside chargeRefundByOrderId : payeezy  refund   jObject : "
													+ payeezyResponseCaputre.getResponseBody());
									if (payeezyCaputrejObject.has("transaction_status")) {
										String response = payeezyCaputrejObject.getString("transaction_status");

										if (response != null && response.equals("approved")) {
											String transctiontag = payeezyCaputrejObject.getString("transaction_tag");
											String transaction_id = payeezyCaputrejObject.getString("transaction_id");
											paymentDetail.setPostPayezzeyTransctionId(
													EncryptionDecryptionUtil.encryption(transaction_id));
											paymentDetail.setPostAuthToken(
													EncryptionDecryptionUtil.encryption(transctiontag));
											paymentDetailRepository.save(paymentDetail);
											orderR.setIsDefaults(5);
											orderRepo.save(orderR);
											responseText = "The Order has been cancelled";
											if (orderR.getCustomer() != null) {
												boolean Status = true;

												MailSendUtil.onlineOrderCancel(orderR, Status,environment);
											}

										} else {
											LOGGER.info(
													"===============  OrderServiceImpl : Inside chargeRefundByOrderId : payeezy Caputre  Object : "
															+ payeezyResponseCaputre.getResponseBody());
											responseText = "falied";

										}
									}
								} catch (Exception e) {
									responseText = "failed";
									LOGGER.error("error: " + e.getMessage());
									LOGGER.info(
											"===============  OrderServiceImpl : Inside chargeRefundByOrderId : Exception:   ============= "
													+ e);
								}
							} catch (Exception e) {
								LOGGER.error("error: " + e.getMessage());
								LOGGER.info(
										"===============  OrderServiceImpl : Inside chargeRefundByOrderId : Exception:   ============= "
												+ e);
							}

					} else {

					}

				} else
					responseText = "Refund is not applicable for this order";

			}
		}
		if (orderR.getPaymentMethod().equals("Cash")) {
			if (orderR.getCustomer().getCustomerType() != null
					&& orderR.getCustomer().getCustomerType().equalsIgnoreCase("Uber-Eats")) {
				String responseCode = null;
				responseCode = ProducerUtil.cancelUberOrder(orderR.getUberorderId());
				LOGGER.info("OrderServiceImpl :: setOrderStatus :responseCode : " + responseCode);
				if (responseCode != null && responseCode.contains("204")) {
					responseText = "The Order has been cancelled";
					orderR.setIsDefaults(2);
				} else {
					responseText = "failed to cancel";
					orderR.setIsDefaults(3);
				}
				orderRepo.save(orderR);
				LOGGER.info("OrderServiceImpl :: setOrderStatus {NON-POS} : responseText " + responseText);

			} else {
				orderR.setIsDefaults(6);
				orderRepo.save(orderR);

			if (orderR.getCustomer() != null) {
				boolean Status = false;

				MailSendUtil.onlineOrderCancel(orderR, Status,environment);
			}

			responseText = "The Order has been cancelled";
			LOGGER.info(" === responseText : " + responseText);
			LOGGER.info("===============  OrderServiceImpl : Inside chargeRefundByOrderId : End:   ============= ");
			}
			return responseText;
		}
		LOGGER.info(" === responseText : " + responseText);

		LOGGER.info("===============  OrderServiceImpl : Inside chargeRefundByOrderId : End:   ============= ");

		return responseText;

	}

	public List<OrderR> findFutureOrderByCurrentDate() {
		LOGGER.info("===============  OrderServiceImpl : Inside findFutureOrderByCurrentDate :: Start  ============= ");

		List<OrderR> futureOrders = orderRepo.findFutureOrderFromCurrentDate();
		LOGGER.info("===============  OrderServiceImpl : Inside findFutureOrderByCurrentDate :: End  ============= ");

		return futureOrders;
	}

	public String setOrderStatus(Integer orderId, String type, String reason, String changedOrderAvgTime) {
		LOGGER.info("----------------Start :: OrderServiceImpl : setOrderStatus {NON-POS}------------------------");
		LOGGER.info(" === type : " + type + " reason : " + reason + " changedOrderAvgTime : " + changedOrderAvgTime
				+ " merchantId : " + merchantId);

		try {
			LOGGER.info("OrderServiceImpl :: setOrderStatus {NON-POS} : orderId " + orderId);
			String responseText = null;
			String responseCode = null;
			double conveniencePercent =0;
			if (null == orderId) {
				LOGGER.info("OrderServiceImpl :: setOrderStatus {NON-POS} : responseText " + responseText);
				LOGGER.info(
						"----------------End :: OrderServiceImpl : setOrderStatus {NON-POS}------------------------");
				return responseText;
			}
			OrderR orderR = orderRepo.findOne(orderId);

			if (orderR == null) {
				LOGGER.info("OrderServiceImpl :: setOrderStatus {NON-POS} : responseText " + responseText);
				LOGGER.info(
						"----------------End :: OrderServiceImpl : setOrderStatus {NON-POS}------------------------");
				return responseText;
			}
			
			PaymentGateWay paymentGateWay = null;
			if (orderR.getMerchant() != null && orderR.getMerchant().getId() != null) {
				paymentGateWay = paymentGateWayRepository
						.findByMerchantIdAndIsDeletedAndIsActive(orderR.getMerchant().getId(), false, true);
			}
			 

			if (orderR.getIsDefaults() == 1 && type != null && !type.equalsIgnoreCase("updateOrderTime")) {
				responseText = "order has been Accepted already ";
				LOGGER.info("OrderServiceImpl :: setOrderStatus {NON-POS} : responseText " + responseText);
				LOGGER.info(
						"----------------End :: OrderServiceImpl : setOrderStatus {NON-POS}------------------------");
				return responseText;
			} else if (orderR.getIsDefaults() == 2) {
				responseText = "order has been Decline already ";
				LOGGER.info("OrderServiceImpl :: setOrderStatus {NON-POS} : responseText " + responseText);
				LOGGER.info(
						"----------------End :: OrderServiceImpl : setOrderStatus {NON-POS}------------------------");
				return responseText;

			} else if (orderR.getIsDefaults() == 3) {
				responseText = "order has been failed ";
				LOGGER.info("OrderServiceImpl :: setOrderStatus {NON-POS} : responseText " + responseText);
				LOGGER.info(
						"----------------End :: OrderServiceImpl : setOrderStatus {NON-POS}------------------------");
				return responseText;

			} else if (orderR.getCustomer()!=null && orderR.getCustomer().getCustomerType() != null
					&& orderR.getCustomer().getCustomerType().equalsIgnoreCase("Uber-Eats")) {
				if (type!=null && type.equalsIgnoreCase("accept")) {
					responseCode = ProducerUtil.acceptUberOrder(orderR.getUberorderId());
					if (responseCode != null && responseCode.contains("204")) {
						responseText = "Order accepted successfully";
						orderR.setIsDefaults(1);
					} else {
						responseText = "failed to accept Order";
						orderR.setIsDefaults(3);
					}
					orderRepo.save(orderR);
					LOGGER.info("OrderServiceImpl :: setOrderStatus {NON-POS} : responseText " + responseText);
				}
				
				else if (type!=null && type.equalsIgnoreCase("decline")) {
					responseCode = ProducerUtil.denyUberOrder(orderR.getUberorderId());

					if (responseCode != null && responseCode.contains("204")) {
						responseText = "Order declined successfully";
						orderR.setIsDefaults(2);
					} else {
						responseText = "failed to deny Order";
						orderR.setIsDefaults(3);
					}
					orderRepo.save(orderR);
					LOGGER.info("OrderServiceImpl :: setOrderStatus {NON-POS} : responseText " + responseText);
				}
				else if (type!=null && type.equalsIgnoreCase("updateOrderTime")) {
					responseText = "can't update uber order time";
				}
				return responseText;
			} else {
				Merchant merchant = orderR.getMerchant();
				String merchantLogo = "";
				ConvenienceFee convenienceFee = null;
				PickUpTime pickUpTime = null;
				String convenienceFeeValue = "0";
				String orderAvgTime = "0";
				Boolean autoAccpetOrder = false;
				MerchantConfiguration merchantConfiguration = merchantConfigurationService
						.findMerchantConfigurationBymerchantId(merchant.getId());
				if (merchantConfiguration != null) {
					if (merchantConfiguration.getAutoAcceptOrder() != null) {
						if (merchantConfiguration.getAutoAcceptOrder() == true) {
							autoAccpetOrder = merchantConfiguration.getAutoAcceptOrder();
						}
					}
				}
				LOGGER.info("OrderServiceImpl :: setOrderStatus : autoAccpetOrder  :: " + autoAccpetOrder);
				if (merchant != null) {
					List<ConvenienceFee> convenienceFees = convenienceFeeRepository.findByMerchantId(merchant.getId());
					if (convenienceFees != null && convenienceFees.size() > 0) {
						int index = convenienceFees.size() - 1;
						convenienceFee = convenienceFees.get(index);

					}
					pickUpTime = pickUpTimeRepository.findByMerchantId(merchant.getId());

					if (orderR != null) {
						orderAvgTime = orderR.getOrderAvgTime();
					}
					if (convenienceFee != null) {
						double ConvenienceFeePercent =  (convenienceFee.getConvenienceFeePercent()!=null) ? convenienceFee.getConvenienceFeePercent() : 0;
				        conveniencePercent = (new Double(orderR.getSubTotal())*(ConvenienceFeePercent/100.0));
						Double finalConvenienceFee = new Double(convenienceFee.getConvenienceFee())+conveniencePercent;
						finalConvenienceFee = Math.round(finalConvenienceFee * 100.0) / 100.0;
						convenienceFeeValue = finalConvenienceFee.toString();
					}
					if (merchant.getMerchantLogo() == null) {
						merchantLogo = "http://www.dev.foodkonnekt.com/app/foodnew.jpg";
					} else {
						merchantLogo = environment.getProperty("BASE_PORT") + merchant.getMerchantLogo();
					}
				}
				String customerName = "";

				if (orderR.getCustomer()!=null && orderR.getCustomer().getFirstName() != null && orderR.getCustomer().getLastName() != null) {
					customerName = orderR.getCustomer().getFirstName().concat(" ")
							.concat(orderR.getCustomer().getLastName());
				} else if (orderR.getCustomer()!=null && orderR.getCustomer().getFirstName() != null) {
					customerName = orderR.getCustomer().getFirstName();
				}
				if (type!=null && type.equalsIgnoreCase("Accept")) {
					// if ((orderR.getIsFutureOrder()==0 && type.equalsIgnoreCase("Accept")) ||
					// (orderR.getIsFutureOrder()==1 && type.equalsIgnoreCase("Agree"))) {

					LOGGER.info("OrderServiceImpl :: setOrderStatus {NON-POS} : Inside accept ");

					if (orderR.getPaymentMethod() != null && orderR.getPaymentMethod().equals("Credit Card")) {
						LOGGER.info("OrderServiceImpl :: setOrderStatus {NON-POS} : inside Credit Card ");
						PaymentGateWay gateway = paymentGateWayRepository.findByMerchantIdAndIsDeleted(merchant.getId(),
								false);
						if (gateway != null && gateway.getGateWayType().equals("3")) {
							OrderPaymentDetail paymentDetail = paymentDetailRepository.findByOrderId(orderId);
							// CardInfo cardInfo = null;
							if (paymentDetail != null)
								try {
									CreditSoapProxy creditSoapProxy = new CreditSoapProxy();
									MerchantCredentials credentials = new MerchantCredentials();
									credentials.setMerchantKey(
											EncryptionDecryptionUtil.decryption(gateway.getCayanMerchantKey()));
									credentials.setMerchantName(
											EncryptionDecryptionUtil.decryption(gateway.getCayanMerchantName()));
									credentials.setMerchantSiteId(
											EncryptionDecryptionUtil.decryption(gateway.getCayanMerchantSiteId()));

									CaptureRequest captureRequest = new CaptureRequest();
									captureRequest.setAmount(String.valueOf(orderR.getOrderPrice()));
									captureRequest.setToken(
											EncryptionDecryptionUtil.decryption(paymentDetail.getPreAuthToken()));

									TransactionResponse45 responseCapture = creditSoapProxy.capture(credentials,
											captureRequest);
									if (responseCapture.getApprovalStatus().equals("APPROVED")) {
										paymentDetail.setPostAuthToken(
												EncryptionDecryptionUtil.encryption(responseCapture.getToken()));
										paymentDetailRepository.save(paymentDetail);
										orderR.setIsDefaults(1);
									} else {
										// something went wrong
										return responseText;
									}
								} catch (RemoteException e) {
									// TODO Auto-generated catch block
									LOGGER.error("error: " + e.getMessage());
									LOGGER.info("response Cyan not capture Failed---------" + e);
									
								}
						}
						if (gateway != null && gateway.getGateWayType().equals("4")) {
							OrderPaymentDetail paymentDetail = paymentDetailRepository.findByOrderId(orderId);
							if (paymentDetail != null) {
								try {
									LOGGER.info("response Igate  capture  Request---------");
									SmartPaymentsSoapProxy soapProxy = new SmartPaymentsSoapProxy(
											environment.getProperty("ENDPOINT_PROD"));
									Response responseCapture = soapProxy.processCreditCard(
											EncryptionDecryptionUtil.decryption(gateway.getiGateUserName()),
											EncryptionDecryptionUtil.decryption(gateway.getiGatePassword()), "Force",
											"", "", "", "", "", "",
											EncryptionDecryptionUtil.decryption(paymentDetail.getPreAuthToken()), "",
											"", "", "");

									LOGGER.info(" TGATE RESPONSE for capture-------->  " + responseCapture);
									if (responseCapture != null && responseCapture.getMessage() != null
											&& responseCapture.getMessage().contains("APPROVAL")) {
										/*
										 * paymentDetail.setPostAuthToken(getEncyKey(responseCapture.getPNRef(),
										 * merchant.getId()));
										 */
										paymentDetail.setPostAuthToken(
												EncryptionDecryptionUtil.encryption(responseCapture.getPNRef()));
										paymentDetailRepository.save(paymentDetail);
										orderR.setIsDefaults(1);
										LOGGER.info("response Igate  capture success---------" + responseCapture);
									} else if (responseCapture != null && responseCapture.getMessage() != null
											&& responseCapture.getMessage().contains("APPROVED")) {
										paymentDetail.setPostAuthToken(
												EncryptionDecryptionUtil.encryption(responseCapture.getPNRef()));
										paymentDetailRepository.save(paymentDetail);
										orderR.setIsDefaults(1);

									} else {
										LOGGER.info("response Igate  capture Failed---- " + responseCapture.getPNRef()
												+ "-----" + responseCapture);
										MailSendUtil.sendPaymentGatwayMailToAdmin(responseCapture.getRespMSG(),
												"postAuthCAPTURE", "Igate", orderR, responseCapture.getRespMSG(),environment);
										
									}

								} catch (Exception e) {
									LOGGER.error("error: " + e.getMessage());
									MailSendUtil.sendPaymentExceptionByMail(e, "postAuthCAPTURE", "Igate", orderR,
											e.getMessage(),environment);
									LOGGER.error("response Igate  capture Failed---------" + e
											+ "with orderId----------" + orderR.getId());
								}
							}
						}

						if (gateway != null && gateway.getGateWayType().equals("2")) {
							OrderPaymentDetail paymentDetail = paymentDetailRepository.findByOrderId(orderId);
							if (paymentDetail != null) {
								try {

									LOGGER.info("response AUTHORIZE  capture  Request---------");

									// Common code to set for all requests
									ApiOperationBase.setEnvironment(ProducerUtil.getAuthorizeEnv(environment.getProperty("AuthorizeEnv")));

									MerchantAuthenticationType merchantAuthenticationType = new MerchantAuthenticationType();
									merchantAuthenticationType.setName(
											EncryptionDecryptionUtil.decryption(gateway.getAuthorizeLoginId()));
									merchantAuthenticationType.setTransactionKey(
											EncryptionDecryptionUtil.decryption(gateway.getAuthorizeTransactionKey()));
									ApiOperationBase.setMerchantAuthentication(merchantAuthenticationType);

									TransactionRequestType txnRequest = new TransactionRequestType();
									txnRequest.setTransactionType(
											TransactionTypeEnum.PRIOR_AUTH_CAPTURE_TRANSACTION.value());
									txnRequest.setRefTransId(
											EncryptionDecryptionUtil.decryption(paymentDetail.getPreAuthToken())); // id
																													// recieved
																													// from
																													// previous
																													// authorizeCreditcard
																													// method

									// Make the API Request
									CreateTransactionRequest apiRequest = new CreateTransactionRequest();
									apiRequest.setTransactionRequest(txnRequest);
									CreateTransactionController controller = new CreateTransactionController(
											apiRequest);
									controller.execute();
									CreateTransactionResponse responseCapture = controller.getApiResponse();

									if (responseCapture != null) {

										// If API Response is ok, go ahead and check the transaction response
										if (responseCapture.getMessages().getResultCode() == MessageTypeEnum.OK) {
											LOGGER.info("response AUTHORIZE  capture Success---------");
											TransactionResponse result = responseCapture.getTransactionResponse();
											/*
											 * paymentDetail.setPostAuthToken(getEncyKey(result.getTransId(),
											 * merchant.getId()));
											 */
											paymentDetail.setPostAuthToken(
													EncryptionDecryptionUtil.encryption(result.getTransId()));
											// cardInfo.setPostAuthToken(EncryptionDecryptionUtil.encryption(responseCapture.getToken()));
											paymentDetailRepository.save(paymentDetail);
											orderR.setIsDefaults(1);
										} else {
											LOGGER.info("response AUTHORIZE  capture Failed---------"
													+ responseCapture.getTransactionResponse().getErrors().getError()
															.get(0).getErrorText());
											MailSendUtil.sendPaymentGatwayMailToAdmin(
													responseCapture.getTransactionResponse().getErrors().getError()
															.get(0).getErrorText(),
													"postAuthCAPTURE", "AUTHORIZE._NET", orderR,environment);
										}
									}

								} catch (Exception e) {
									LOGGER.error("error: " + e.getMessage());
									LOGGER.error("response AUTHORIZE  capture Failed---------" + e
											+ " orderId-----------" + orderR.getId());
									MailSendUtil.sendPaymentExceptionByMail(e, "postAuthCAPTURE", "AUTHORIZE._NET",
											orderR, e.getMessage(),environment);
								}
							}
						}
						if (gateway != null && gateway.getGateWayType().equals("1")) {
							OrderPaymentDetail paymentDetail = paymentDetailRepository.findByOrderId(orderId);
							if (paymentDetail != null) {
								LOGGER.info("response Stripe  capture  Request---------");

								try {
									Charge charge = null;
									Stripe.apiKey = EncryptionDecryptionUtil.decryption(gateway.getStripeAPIKey());
									charge = Charge.retrieve(
											EncryptionDecryptionUtil.decryption(paymentDetail.getPreAuthToken()));

									// charge.capture();
									if (charge != null && charge.getStatus() != null
											&& charge.getStatus().equals("succeeded")) {
										LOGGER.info("response Stripe  capture Success---------");

										paymentDetail
												.setPostAuthToken(EncryptionDecryptionUtil.encryption(charge.getId()));
										paymentDetailRepository.save(paymentDetail);
										orderR.setIsDefaults(1);
									} else {

										LOGGER.info("response Stripe   capture  Failed---------" + charge);
										if (charge != null && charge.getFailureMessage() != null) {
											LOGGER.info("response Stripe  Stripe Failed---------"
													+ charge.getFailureMessage());
											MailSendUtil.sendPaymentGatwayMailToAdmin(charge.getFailureMessage(),
													"postAuthCAPTURE", "Stripe", orderR, charge.getFailureMessage(),environment);
										}
									}
								} catch (Exception e) {
									LOGGER.error("error: " + e.getMessage());
									MailSendUtil.sendPaymentExceptionByMail(e, "postAuthCAPTURE", "Stripe", orderR,
											e.getMessage(),environment);
									LOGGER.error("response Stripe capture Failed---------" + e + " orderId-----------"
											+ orderR.getId());

								}
							}
						}

						if (gateway != null && gateway.getGateWayType().equals("6")) {
							OrderPaymentDetail paymentDetail = paymentDetailRepository.findByOrderId(orderId);
							if (paymentDetail != null)
								try {
									LOGGER.info("response payeezy  capture  Request---------");

									Double totalInDouble = orderR.getOrderPrice();

									int totalInInt = (int) Math.round(totalInDouble * 100);

									LOGGER.info("totalInInt" + orderR.getOrderPrice().toString());
									String merchantToken = EncryptionDecryptionUtil
											.decryption(gateway.getPayeezyMerchnatToken());
									PayeezyClientHelper client = PayeezyUtilProperties
											.getPayeezyProperties(merchantToken,environment);
									String transactionId = EncryptionDecryptionUtil
											.decryption(paymentDetail.getTransactionId());
									TransactionRequest trans = new TransactionRequest();
									trans.setPaymentMethod("credit_card");
									trans.setCurrency("USD");
									trans.setTransactionType("CAPTURE");
									trans.setTransactionTag(
											EncryptionDecryptionUtil.decryption(paymentDetail.getPreAuthToken()));
									trans.setAmount(String.valueOf(totalInInt));
									if (merchant.getName() != null) {
										trans.setReferenceNo("Online order for -  "
												+ ProducerUtil.removeSpecialCharecter(merchant.getName()));
									}
									try {
										JSONHelper jsonHelper = new JSONHelper();
										String payload = jsonHelper.getJSONObject(trans);
										LOGGER.info("OrderServiceImpl  : Payeezy payload for CAPTURE transaction :  "
												+ payload);
										PayeezyResponse payeezyResponseCaputre = client
												.doSecondaryTransaction(transactionId, trans);
										JSONObject payeezyCaputrejObject = new JSONObject(
												payeezyResponseCaputre.getResponseBody());
										LOGGER.info("payeezy  Caputre   jObject-------"
												+ payeezyResponseCaputre.getResponseBody());
										if (payeezyCaputrejObject.has("transaction_status")) {
											String response = payeezyCaputrejObject.getString("transaction_status");

											if (response != null && response.equals("approved")) {
												String transctiontag = payeezyCaputrejObject
														.getString("transaction_tag");
												String transaction_id = payeezyCaputrejObject
														.getString("transaction_id");
												paymentDetail.setPostPayezzeyTransctionId(
														EncryptionDecryptionUtil.encryption(transaction_id));
												paymentDetail.setPostAuthToken(
														EncryptionDecryptionUtil.encryption(transctiontag));
												paymentDetailRepository.save(paymentDetail);
												orderR.setIsDefaults(1);
											} else {
												if (payeezyResponseCaputre != null
														&& payeezyResponseCaputre.getResponseBody() != null) {
													LOGGER.info("payeezy CAPTURE Object Failed  orderID : " + orderId
															+ "----" + payeezyResponseCaputre.getResponseBody());
													LOGGER.error(
															"OrderServiceImpl : payezzy  CAPTURE  Failed    orderID : "
																	+ orderId + " "
																	+ payeezyResponseCaputre.getResponseBody());
													MailSendUtil.sendPaymentGatwayMailToAdmin(
															payeezyResponseCaputre.getResponseBody(), "postAuth",
															"payeezy", orderR,
															ProducerUtil.payeezyErrorResponse(payeezyResponseCaputre),environment);
												} else
													LOGGER.error(
															"OrderServiceImpl : payezzy  CAPTURE  Failed    orderID :response dd=idnt get ");

											}
										} else {
											if (payeezyResponseCaputre != null
													&& payeezyResponseCaputre.getResponseBody() != null) {
												LOGGER.info("payeezy CAPTURE Object Failed  orderID : " + orderId
														+ "----" + payeezyResponseCaputre.getResponseBody());
												LOGGER.error("OrderServiceImpl : payezzy  CAPTURE  Failed    orderID : "
														+ orderId + " " + payeezyResponseCaputre.getResponseBody());
												MailSendUtil.sendPaymentGatwayMailToAdmin(
														payeezyResponseCaputre.getResponseBody(), "postAuth", "payeezy",
														orderR,
														ProducerUtil.payeezyErrorResponse(payeezyResponseCaputre),environment);
											} else
												LOGGER.error(
														"OrderServiceImpl : payezzy  CAPTURE  Failed    orderID :response dd=idnt get ");
										}
									} catch (Exception e) {
										LOGGER.error("error: " + e.getMessage());
										LOGGER.error("response payeezy capture Failed---------" + e
												+ " orderId-----------" + orderR.getId());
										MailSendUtil.sendPaymentExceptionByMail(e, "postAuthCAPTURE", "payeezy", orderR,
												e.getMessage(),environment);

									}

								} catch (Exception e) {
									LOGGER.error("error: " + e.getMessage());
									MailSendUtil.sendPaymentExceptionByMail(e, "postAuthCAPTURE", "payeezy", orderR,
											e.getMessage(),environment);
									LOGGER.error("response payeezy capture Failed---------" + e + " orderId-----------"
											+ orderR.getId());

								}
						}

					}else if (orderR.getPaymentMethod() != null && orderR.getPaymentMethod().equalsIgnoreCase(IConstant.CASH_PAYMENT))
						       orderR.setIsDefaults(1);
					orderRepo.save(orderR);
					String orderDetails = "";

					List<Map<String, String>> extraList = null;
					Map<String, String> poduct = null;
					Map<String, String> exctra = null;
					List<Map<String, String>> productList = new ArrayList<Map<String, String>>();

					List<OrderItem> orderItems = orderItemRepository.findByOrderId(orderR.getId());
					if (orderItems != null && orderItems.size() > 0)
						for (OrderItem orderItem : orderItems) {
							if (orderItem.getItem() != null) {
								String items = "<tr style='font-weight:600;text-transform:capitalize;font-family:arial'><td width='200px;'>"
										+ orderItem.getItem().getName()
										+ "</td><td width='100px;' style='text-align:center'>" + orderItem.getQuantity()
										+ "</td><td width='100px;' style='text-align:center'>" + "$"
										+ String.format("%.2f",orderItem.getItem().getPrice()) + "</td></tr>";
								orderDetails = orderDetails + "" + "<b>" + items + "</b>";
							}
							List<OrderItemModifier> itemModifiers = itemModifierRepository
									.findByOrderItemId(orderItem.getId());
							if(itemModifiers!=null && itemModifiers.size()>0)
							for (OrderItemModifier itemModifier : itemModifiers) {
								if (itemModifier != null && itemModifier.getModifiers() != null) {
									String modifiers = "<tr style='text-transform:capitalize;font-family:arial;margin-top:5px;font-size:12px'><td width='200px;'>"
											+ itemModifier.getModifiers().getName()
											+ "</td><td width='100px;' style='text-align:center'>"
											+ itemModifier.getQuantity()
											+ " </td><td width='100px;' style='text-align:center'> " + "$"
											+ String.format("%.2f",itemModifier.getModifiers().getPrice()) + "</td></tr>";
									orderDetails = orderDetails + "" + modifiers;
								}
							}
						}

					/*
					 * -------------------------------------------------------------For Pizza
					 * View-------------------------------------------------------------------------
					 * ---------
					 */
					List<OrderPizza> orderPizzas = orderPizzaRepository.findByOrderId(orderR.getId());
					if (orderPizzas != null && !orderPizzas.isEmpty()) {
						for (OrderPizza orderPizza : orderPizzas) {
							poduct = new HashMap<String, String>();
							if (orderPizza != null && orderPizza.getPizzaTemplate() != null) {

								Item item = new Item();
								if (orderPizza != null && orderPizza.getPizzaTemplate() != null
										&& orderPizza.getPizzaTemplate().getDescription() != null
										&& orderPizza.getPizzaSize() != null
										&& orderPizza.getPizzaSize().getDescription() != null) {
									item.setName(orderPizza.getPizzaTemplate().getDescription() + "("
											+ orderPizza.getPizzaSize().getDescription() + ")");

									String pizza = "<tr style='font-weight:600;text-transform:capitalize;font-family:arial'><td width='200px;'>"
											+ orderPizza.getPizzaTemplate().getDescription() + "("
											+ orderPizza.getPizzaSize().getDescription() + ")"
											+ "</td><td width='100px;' style='text-align:center'>"
											+ orderPizza.getQuantity()
											+ "</td><td width='100px;' style='text-align:center'>" + "$"
											+ String.format("%.2f", orderPizza.getPrice()) + "</td></tr>";
									orderDetails = orderDetails + "" + "<b>" + pizza + "</b>";

									poduct.put("product_id", orderPizza.getPizzaTemplate().getPosPizzaTemplateId());
									poduct.put("name", orderPizza.getPizzaTemplate().getDescription() + "("
											+ orderPizza.getPizzaSize().getDescription() + ")");
									poduct.put("price", Double.toString(orderPizza.getPrice()));
									poduct.put("qty", Integer.toString(orderPizza.getQuantity()));

								}
								item.setPrice(orderPizza.getPrice());
								List<OrderPizzaToppings> orderPizzaToppings = orderPizzaToppingsRepository
										.findByOrderPizzaId(orderPizza.getId());
								extraList = new ArrayList<Map<String, String>>();
								if (orderPizzaToppings != null && !orderPizzaToppings.isEmpty()) {
									for (OrderPizzaToppings pizzaToppings : orderPizzaToppings) {
										exctra = new HashMap<String, String>();
										Modifiers itemModifier = new Modifiers();
										if (pizzaToppings != null && pizzaToppings.getPizzaTopping() != null
												&& pizzaToppings.getPizzaTopping().getDescription() != null) {
											itemModifier.setName(pizzaToppings.getPizzaTopping().getDescription());
											itemModifier.setPrice(pizzaToppings.getPrice());

											String modifiers = "<tr style='text-transform:capitalize;font-family:arial;margin-top:5px;font-size:12px'><td width='200px;'>"
													+ pizzaToppings.getPizzaTopping().getDescription();

											if (pizzaToppings.getSide1() != null && pizzaToppings.getSide1()) {
												modifiers += " (First Half)";
											} else if (pizzaToppings.getSide1() != null && pizzaToppings.getSide2()) {
												modifiers += " (Second Half)";
											} else {
												modifiers += " (Full)";
											}
											modifiers += "</td><td width='100px;' style='text-align:center'>"
													+ orderPizza.getQuantity()
													+ " </td><td width='100px;' style='text-align:center'> " + "$"
													+ String.format("%.2f", pizzaToppings.getPrice()) + "</td></tr>";

											orderDetails = orderDetails + "" + modifiers;

											exctra.put("id", pizzaToppings.getPizzaTopping().getPosPizzaToppingId());
											exctra.put("price", Double.toString(pizzaToppings.getPrice()));
											exctra.put("name", pizzaToppings.getPizzaTopping().getDescription());
											exctra.put("qty", orderPizza.getQuantity().toString());
											extraList.add(exctra);
										}
									}
								}

								OrderPizzaCrust orderPizzaCrust = orderPizzaCrustRepository
										.findByOrderPizzaId(orderPizza.getId());
								if (orderPizzaCrust != null && orderPizzaCrust.getPizzacrust() != null) {
									exctra = new HashMap<String, String>();
									String crust = "<tr style='text-transform:capitalize;font-family:arial;margin-top:5px;font-size:12px'><td width='200px;'>"
											+ orderPizzaCrust.getPizzacrust().getDescription();

									crust += "</td><td width='100px;' style='text-align:center'>"
											+ orderPizza.getQuantity()
											+ " </td><td width='100px;' style='text-align:center'> " + "$"
											+ String.format("%.2f", orderPizzaCrust.getPrice()) + "</td></tr>";
									orderDetails = orderDetails + "" + crust;

									exctra.put("id", orderPizzaCrust.getPizzacrust().getId().toString());
									exctra.put("price", Double.toString(orderPizzaCrust.getPrice()));
									exctra.put("name", orderPizzaCrust.getPizzacrust().getDescription());
									exctra.put("qty", orderPizza.getQuantity().toString());
								}
							}
							Gson gson = new Gson();
							if (extraList.size() > 0) {
								String extraJson = gson.toJson(extraList);
								poduct.put("extras", extraJson);
								productList.add(poduct);
							} else {
								productList.add(poduct);
							}
						}
					}

					/*
					 * -----------------------------------------------------------------------------
					 * -------------------------
					 * ---------------------------------------------------------
					 */

					orderDetails = "<table width='300px;'><tbody>" + orderDetails + "</table></tbody>";
					LOGGER.info(orderDetails);
					String deliveryFee = "0";
					if (orderR.getDeliveryFee() != null && !orderR.getDeliveryFee().isEmpty())
						deliveryFee = orderR.getDeliveryFee();
					Double orderDiscount = 0.0;
					String discountCoupon = "";
					if (orderR.getOrderDiscount() != null) {
						orderDiscount = orderR.getOrderDiscount();

						List<OrderDiscount> orderDiscounts = orderDiscountRepository.findByOrderId(orderR.getId());
						if (orderDiscounts != null && !orderDiscounts.isEmpty()) {
							discountCoupon = discountCoupon + "<tr>" + "<td>Discount Coupon</td>" + "<td>&nbsp;</td>"
									+ "<td>" + orderDiscounts.get(0).getCouponCode() + "</td>" + "</tr>";

							if (orderDiscounts.size() > 1) {
								for (int i = 1; i < orderDiscounts.size(); i++) {
									discountCoupon = discountCoupon + "<tr>" + "<td></td>" + "<td>&nbsp;</td>" + "<td>"
											+ orderDiscounts.get(i).getCouponCode() + "</td>" + "</tr>";
								}
							}
						}
					}

					String dummyReason = "Due to time changed by Merchant";
					String avgOrderTime = (orderR.getOrderAvgTime() != null) ? orderR.getOrderAvgTime() : "";
					if (type.equalsIgnoreCase("Accept") && changedOrderAvgTime != null
							&& !changedOrderAvgTime.equals("")
							&& !avgOrderTime.equals(changedOrderAvgTime) && !autoAccpetOrder) {
						orderR.setOrderAvgTime(changedOrderAvgTime);
						orderAvgTime = changedOrderAvgTime;
						// orderRepo.save(orderR);
						//MailSendUtil.sendUpdatedTime(orderR, dummyReason, changedOrderAvgTime);

						Date fullfilled_date = orderR.getFulfilled_on();
						SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-dd-MM HH:mm:ss");
						String myTime = simpleDateFormat.format(fullfilled_date);
						String split[] = myTime.split(" ");
						//String time = split[1];
						String timeZone = (orderR.getMerchant() != null && orderR.getMerchant().getTimeZone() != null
								&& orderR.getMerchant().getTimeZone().getTimeZoneCode() != null)
										? orderR.getMerchant().getTimeZone().getTimeZoneCode()
										: "America/Chicago";
						String time = DateUtil.getCurrentTimeForTimeZone(timeZone);

						SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
						Date d = null;
						try {
							d = df.parse(time);
						} catch (ParseException e) {
							LOGGER.error("error: " + e.getMessage());
							d=new Date();
						}

						Calendar cal = Calendar.getInstance();
						cal.setTime(d);
						cal.add(Calendar.MINUTE, Integer.parseInt(orderAvgTime));
						String newTime = df.format(cal.getTime());
						LOGGER.info(newTime);

						String finalTime = split[0] + " " + newTime;
						LOGGER.info(finalTime);

						DateFormat dateFormat = new SimpleDateFormat("yyyy-dd-MM HH:mm:ss");
						Date finalDate = null;
						try {
							finalDate = dateFormat.parse(finalTime);
						} catch (ParseException e) {
							// TODO Auto-generated catch block
							LOGGER.error("error: " + e.getMessage());
						}
						if (finalDate != null)
						{
							orderR.setFulfilled_on(finalDate);
							LOGGER.info("OrderServiceImpl :: savePizzaOrder : finalDate " + finalDate);
						}

					}

					orderR = orderRepo.save(orderR);
					
					mailSendUtil.sendConfirmMail(customerName, orderR.getPaymentMethod(), orderR.getOrderPosId(),
							orderR.getSubTotal(), orderR.getTax(), orderDetails, orderR.getCustomer().getEmailId(),
							orderR.getOrderPrice(), orderR.getOrderNote(), orderR.getMerchant().getName(), merchantLogo,
							orderR.getOrderType(), convenienceFeeValue, orderAvgTime, deliveryFee, orderDiscount,
							orderR.getTipAmount(), orderR.getCustomer().getId(), orderR.getId(), posTypeId,
							discountCoupon, merchant.getId(), paymentGateWay);
					
//        if(orderR!=null && orderR.getMerchant()!=null && orderR.getMerchant().getOwner()!=null && orderR.getMerchant().getOwner().getPos()!=null && orderR.getMerchant().getOwner().getPos().getPosId()!=3)
//        mailSendUtil.orderConfirmetionMailWirhOrderPosId(orderR);
					responseText = "Order accepted successfully";
					LOGGER.info("OrderServiceImpl :: setOrderStatus {NON-POS} : responseText " + responseText);
					storeOrderIntoFreekwent(orderR, merchant.getMerchantUid());
					return responseText;

				} else if (type.equalsIgnoreCase("updateOrderTime")) {
					String dummyReason = "Due to time changed by Merchant";

					LOGGER.info("OrderServiceImpl : Inside setOrderStatus of NON-POS :: inside changeOrder Avg Time");

					if (changedOrderAvgTime != null && changedOrderAvgTime != "") {
						MailSendUtil.sendUpdatedTime(orderR, dummyReason, changedOrderAvgTime,environment);
						orderR.setOrderAvgTime(changedOrderAvgTime);
					} else {
						changedOrderAvgTime = orderR.getOrderAvgTime();
						MailSendUtil.sendUpdatedTime(orderR, dummyReason, orderR.getOrderAvgTime(),environment);
					}
					LOGGER.info("OrderServiceImpl : Inside setOrderStatus of NON-POS :: order updated mail send");

					String timeZoneCode = "America/Chicago";
					if (merchant != null && merchant.getTimeZone() != null
							&& merchant.getTimeZone().getTimeZoneCode() != null) {
						timeZoneCode = merchant.getTimeZone().getTimeZoneCode();
					}

					LOGGER.info("OrderServiceImpl : Inside setOrderStatus of NON-POS : current timeZoneCode "
							+ timeZoneCode);

					final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
					SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT);
					formatter.setTimeZone(TimeZone.getTimeZone(timeZoneCode));

					Calendar currentTime = Calendar.getInstance();

					LOGGER.info("currentTime from timeZone " + currentTime);

					String timeStr = formatter.format(currentTime.getTime());
					LOGGER.info("current time " + timeStr);

					String split[] = timeStr.split(" ");
					String time = split[1];
					LOGGER.info("current time of timeZone " + time);

					SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
					Date d = DateUtil.addOrderAvgTimeIntoCurrentTime(timeZoneCode,currentTime.getTime(),Integer.parseInt(changedOrderAvgTime));
//					
//
//					Calendar cal = Calendar.getInstance();
//					cal.setTime(d);
//					cal.add(Calendar.MINUTE, Integer.parseInt(changedOrderAvgTime));
//					String newTime = df.format(cal.getTime());
//					LOGGER.info("newTime after addition of avg time " + newTime);
//
//					String finalTime = split[0] + " " + newTime;
//					LOGGER.info("finalTime after addition " + finalTime);
//
//					DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//					Date finalDate = null;
//					try {
//						finalDate = dateFormat.parse(finalTime);
//					} catch (ParseException e) {
//						LOGGER.error("error: " + e.getMessage());
//					}
					LOGGER.info("final fullfilledTime " + d);
					orderR.setFulfilled_on(d);
					orderRepo.save(orderR);

					responseText = "Order time updated successfully";
					LOGGER.info("OrderServiceImpl :: setOrderStatus {NON-POS} : responseText " + responseText);
					return responseText;

				} else {
					if (orderR.getPaymentMethod() != null && orderR.getPaymentMethod().equals("Credit Card")) {
						PaymentGateWay gateway = paymentGateWayRepository.findByMerchantIdAndIsDeleted(merchant.getId(),
								false);
						if (gateway != null && gateway.getGateWayType().equals("3")) {
							OrderPaymentDetail paymentDetail = paymentDetailRepository.findByOrderId(orderId);
							// CardInfo cardInfo = null;
							if (paymentDetail != null) {
								// cardInfo = cardInfoRepository.findOne(paymentDetail.getCardInfoId().getId());
								// if(cardInfo!=null){
								try {
									CreditSoapProxy creditSoapProxy = new CreditSoapProxy();
									MerchantCredentials credentials = new MerchantCredentials();
									credentials.setMerchantKey(
											EncryptionDecryptionUtil.decryption(gateway.getCayanMerchantKey()));
									credentials.setMerchantName(
											EncryptionDecryptionUtil.decryption(gateway.getCayanMerchantName()));
									credentials.setMerchantSiteId(
											EncryptionDecryptionUtil.decryption(gateway.getCayanMerchantSiteId()));

									VoidRequest voidRequest = new VoidRequest();
									voidRequest.setToken(
											EncryptionDecryptionUtil.decryption(paymentDetail.getPreAuthToken()));

									TransactionResponse45 voidResponse = creditSoapProxy._void(credentials,
											voidRequest);
									if (voidResponse.getApprovalStatus().equals("APPROVED")) {
										// paymentDetail.setVoidToken(getEncyKey(paymentDetail.getPreAuthToken(),merchant.getId()));
										paymentDetail.setVoidToken(
												EncryptionDecryptionUtil.encryption(voidResponse.getToken()));
										// cardInfo.setVoidToken(EncryptionDecryptionUtil.encryption(voidResponse.getToken()));
										// cardInfoRepository.save(cardInfo);
										paymentDetailRepository.save(paymentDetail);
									}

								} catch (Exception e) {
									LOGGER.error("error: " + e.getMessage());
									MailSendUtil.sendPaymentExceptionByMail(e, "postAuthVoid", "Cayan", orderR,
											e.getMessage(),environment);
									LOGGER.error("response Cayan  VOID Failed---------" + e + " orderId-----------"
											+ orderR.getId());
								}
							}
							// }
						}
						if (gateway != null && gateway.getGateWayType().equals("4")) {
							OrderPaymentDetail paymentDetail = paymentDetailRepository.findByOrderId(orderId);
							if (paymentDetail != null) {
								try {
									LOGGER.info("response I-Gate  Void  Request---------");
									SmartPaymentsSoapProxy soapProxy = new SmartPaymentsSoapProxy(
											environment.getProperty("ENDPOINT_PROD"));
									Response voidResponse = soapProxy.processCreditCard(
											EncryptionDecryptionUtil.decryption(gateway.getiGateUserName()),
											EncryptionDecryptionUtil.decryption(gateway.getiGatePassword()), "void", "",
											"", "", "", "", "",
											EncryptionDecryptionUtil.decryption(paymentDetail.getPreAuthToken()), "",
											"", "", "");

									LOGGER.info("Tgate void response : " + voidResponse);

									if (voidResponse != null && voidResponse.getMessage() != null
											&& voidResponse.getMessage().contains("APPROVAL")) {
										// paymentDetail.setVoidToken(getEncyKey(voidResponse.getPNRef(),
										// merchant.getId()));
										paymentDetail.setVoidToken(
												EncryptionDecryptionUtil.encryption(voidResponse.getPNRef()));
										paymentDetailRepository.save(paymentDetail);
									}

									else if (voidResponse != null && voidResponse.getMessage() != null
											&& voidResponse.getMessage().contains("APPROVED")) {
										paymentDetail.setPostAuthToken(
												EncryptionDecryptionUtil.encryption(voidResponse.getPNRef()));
										paymentDetailRepository.save(paymentDetail);

									} else {
										MailSendUtil.sendPaymentGatwayMailToAdmin(voidResponse.getRespMSG(),
												"postAuthVoid", "I-Gate", orderR,environment);
										LOGGER.info("response Igate  VOID Failed---------" + voidResponse.getRespMSG());

									}

								} catch (Exception e) {
									LOGGER.error("error: " + e.getMessage());
									MailSendUtil.sendPaymentExceptionByMail(e, "postAuthVoid", "I-Gate", orderR,
											e.getMessage(),environment);
									LOGGER.error("response I-Gate  VOID Failed---------" + e + " orderId-----------"
											+ orderR.getId());
								}
							}
						}
						if (gateway != null && gateway.getGateWayType().equals("2")) {
							OrderPaymentDetail paymentDetail = paymentDetailRepository.findByOrderId(orderId);
							if (paymentDetail != null) {
								try {
									LOGGER.info("response AUTHORIZE  Void  Request---------");
									// Common code to set for all requests
									ApiOperationBase.setEnvironment(ProducerUtil.getAuthorizeEnv(environment.getProperty("AuthorizeEnv")));
									MerchantAuthenticationType merchantAuthenticationType = new MerchantAuthenticationType();
									merchantAuthenticationType.setName(
											EncryptionDecryptionUtil.decryption(gateway.getAuthorizeLoginId()));
									merchantAuthenticationType.setTransactionKey(
											EncryptionDecryptionUtil.decryption(gateway.getAuthorizeTransactionKey()));
									ApiOperationBase.setMerchantAuthentication(merchantAuthenticationType);
									// Create the payment transaction request
									TransactionRequestType txnRequest = new TransactionRequestType();
									txnRequest.setTransactionType(TransactionTypeEnum.VOID_TRANSACTION.value());
									txnRequest.setRefTransId(paymentDetail.getPreAuthToken());
									txnRequest.setRefTransId(
											EncryptionDecryptionUtil.decryption(paymentDetail.getPreAuthToken()));

									// Make the API Request
									CreateTransactionRequest apiRequest = new CreateTransactionRequest();
									apiRequest.setTransactionRequest(txnRequest);
									CreateTransactionController controller = new CreateTransactionController(
											apiRequest);
									controller.execute();
									CreateTransactionResponse voidResponse = controller.getApiResponse();
									if (voidResponse != null) {
										// If API Response is ok, go ahead and check the transaction response
										if (voidResponse.getMessages().getResultCode() == MessageTypeEnum.OK) {
											TransactionResponse result = voidResponse.getTransactionResponse();
											if (result.getMessages() != null) {
												paymentDetail.setVoidToken(
														EncryptionDecryptionUtil.encryption(result.getTransId()));
												paymentDetailRepository.save(paymentDetail);
											}
										} else {
											LOGGER.info("response AUTHORIZE  VOID Failed---------"
													+ voidResponse.getTransactionResponse().getErrors().getError()
															.get(0).getErrorText());
											MailSendUtil
													.sendPaymentGatwayMailToAdmin(
															voidResponse.getTransactionResponse().getErrors().getError()
																	.get(0).getErrorText(),
															"postAuthVoid", "AUTHORIZE._NET", orderR,environment);
										}
									}

								} catch (Exception e) {
									LOGGER.error("error: " + e.getMessage());
									MailSendUtil.sendPaymentExceptionByMail(e, "postAuthVoid", "AUTHORIZE.NET", orderR,
											e.getMessage(),environment);
									LOGGER.error("response AUTHORIZE  VOID Failed---------" + e + " orderId-----------"
											+ orderR.getId());

								}
							}
						}
						if (gateway != null && gateway.getGateWayType().equals("1")) {
							LOGGER.info("Stripe gateway is enabled");
							OrderPaymentDetail paymentDetail = paymentDetailRepository.findByOrderId(orderId);
							if (paymentDetail != null) {
								try {
									LOGGER.info("response Stripe  Void  Request---------");
									Charge charge = null;
									Stripe.apiKey = EncryptionDecryptionUtil.decryption(gateway.getStripeAPIKey());
									String stripeChargeKey = EncryptionDecryptionUtil
											.decryption(paymentDetail.getPreAuthToken());
									charge = Charge.retrieve(stripeChargeKey);
									LOGGER.info(" retrived charge object for key is  " + stripeChargeKey + "  Object "
											+ charge);
									if (charge != null && charge.getStatus() != null
											&& charge.getStatus().equals("succeeded")) {
										LOGGER.info(
												"Charge objest is not null, and order is declined so refunding the charge");
										Refund refund = null;
										Map<String, Object> refundParams = new HashMap<String, Object>();
										refundParams.put("charge", charge.getId());
										refund = Refund.create(refundParams);
										if (refund.getAmount() != null && refund != null) {
											paymentDetail
													.setVoidToken(EncryptionDecryptionUtil.encryption(refund.getId()));
											paymentDetailRepository.save(paymentDetail);
											LOGGER.info(
													"Stripe charge is refunded to respective customer account and receive the followig response :  "
															+ refund);
										}
									} else {
										if (charge != null && charge.getFailureMessage() != null) {
											MailSendUtil.sendPaymentGatwayMailToAdmin(charge.getFailureMessage(),
													"postAuthVoid", "Stripe", orderR,environment);
											LOGGER.info("response Stripe  VOID Failed---------"
													+ charge.getFailureMessage());

										}
									}
								} catch (Exception e) {
									LOGGER.error("error: " + e.getMessage());
									MailSendUtil.sendPaymentExceptionByMail(e, "postAuthVoid", "Stripe", orderR,
											e.getMessage(),environment);
									LOGGER.error("response Stripe  VOID Failed---------" + e + " orderId-----------"
											+ orderR.getId());

								}

							}
						}

						if (gateway != null && gateway.getGateWayType().equals("5")) {
							OrderPaymentDetail paymentDetail = paymentDetailRepository.findByOrderId(orderId);
							if (paymentDetail != null) {
								try {
									LOGGER.info("response FirstData  Void  Request---------");

									String firstDataMerchantId = EncryptionDecryptionUtil
											.decryption(gateway.getFirstDataMerchantId());
									String firstDataUserName = EncryptionDecryptionUtil
											.decryption(gateway.getFirstDataUserName());
									String firstDataPassword = EncryptionDecryptionUtil
											.decryption(gateway.getFirstDataPassword());
									String firstretref = EncryptionDecryptionUtil
											.decryption(paymentDetail.getFirstDataRetRef());
									CardConnectRestClientExample restExample = new CardConnectRestClientExample();
									// Create Update Transaction request
									org.json.simple.JSONObject request = new org.json.simple.JSONObject();
									request.put("merchid", firstDataMerchantId);
									request.put("amount", "0");
									request.put("currency", "USD");
									request.put("retref", firstretref);
									org.json.simple.JSONObject response = restExample.voidTransaction(firstDataUserName,
											firstDataPassword, request);
									LOGGER.info("firstData----------------------------------------------" + response);

								} catch (Exception e) {
									LOGGER.error("error: " + e.getMessage());
									MailSendUtil.sendPaymentExceptionByMail(e, "postAuthVoid", "FirstData", orderR,
											e.getMessage(),environment);
									LOGGER.error("response FirstData  VOID Failed---------" + e + " orderId-----------"
											+ orderR.getId());

								}

							}
						}

						if (gateway != null && gateway.getGateWayType().equals("6")) {
							OrderPaymentDetail paymentDetail = paymentDetailRepository.findByOrderId(orderId);
							if (paymentDetail != null)
								try {
									LOGGER.info("response payeezy  VOID  Request---------");
									String merchantToken = EncryptionDecryptionUtil
											.decryption(gateway.getPayeezyMerchnatToken());
									PayeezyClientHelper client = PayeezyUtilProperties
											.getPayeezyProperties(merchantToken,environment);
									String transactionId = EncryptionDecryptionUtil
											.decryption(paymentDetail.getTransactionId());
									TransactionRequest trans = new TransactionRequest();
									String transactiontag = EncryptionDecryptionUtil
											.decryption(paymentDetail.getPreAuthToken());
									LOGGER.info(
											"transactionId---" + transactionId + "transactiontag---" + transactiontag);
									Double totalInDouble = orderR.getOrderPrice();
									int totalInInt = (int) Math.round(totalInDouble * 100);

									LOGGER.info("totalInInt" + totalInInt);
									trans.setPaymentMethod("credit_card");
									trans.setCurrency("USD");
									trans.setTransactionType("VOID");
									trans.setTransactionTag(transactiontag);
									trans.setAmount(String.valueOf(totalInInt));
									trans.setAmount(String.valueOf(totalInInt));
									if (merchant.getName() != null) {
										trans.setReferenceNo("Online order for -  "
												+ ProducerUtil.removeSpecialCharecter(merchant.getName()));
									}
									try {
										JSONHelper jsonHelper = new JSONHelper();
										String payload = jsonHelper.getJSONObject(trans);
										LOGGER.info("OrderServiceImpl  : Payeezy payload for VOID transaction :  "
												+ payload);
										PayeezyResponse payeezyResponseCaputre = client
												.doSecondaryTransaction(transactionId, trans);
										JSONObject payeezyCaputrejObject = new JSONObject(
												payeezyResponseCaputre.getResponseBody());
										LOGGER.info("payeezy VOID jObject-------"
												+ payeezyResponseCaputre.getResponseBody());
										LOGGER.info("response payeezy  VOID jObject---------"
												+ payeezyResponseCaputre.getResponseBody());
										if (payeezyCaputrejObject.has("transaction_status")) {
											String response = payeezyCaputrejObject.getString("transaction_status");
											if (response != null && response.equals("approved")) {
												String transctiontag = payeezyCaputrejObject
														.getString("transaction_tag");
												String transaction_id = payeezyCaputrejObject
														.getString("transaction_id");
												paymentDetail.setPostPayezzeyTransctionId(
														EncryptionDecryptionUtil.encryption(transaction_id));
												paymentDetail.setPostAuthToken(
														EncryptionDecryptionUtil.encryption(transctiontag));
												paymentDetailRepository.save(paymentDetail);
											} else {
												LOGGER.info("payeezy void Object Failed  orderID : " + orderId + "----"
														+ payeezyResponseCaputre.getResponseBody());
												LOGGER.error("OrderServiceImpl : payezzy not void  Failed    orderID : "
														+ orderId + " " + payeezyResponseCaputre.getResponseBody());
												MailSendUtil.sendPaymentGatwayMailToAdmin(
														payeezyResponseCaputre.getResponseBody(), "postAuth", "payeezy",
														orderR,
														ProducerUtil.payeezyErrorResponse(payeezyResponseCaputre),environment);

											}
										} else {
											LOGGER.info("payeezy void Object Failed  orderID : " + orderId + "----"
													+ payeezyResponseCaputre.getResponseBody());
											LOGGER.error("OrderServiceImpl : payezzy not void  Failed    orderID : "
													+ orderId + " " + payeezyResponseCaputre.getResponseBody());
											MailSendUtil.sendPaymentGatwayMailToAdmin(
													payeezyResponseCaputre.getResponseBody(), "postAuth", "payeezy",
													orderR, ProducerUtil.payeezyErrorResponse(payeezyResponseCaputre),environment);

										}
									} catch (Exception e) {
										LOGGER.error("error: " + e.getMessage());
										MailSendUtil.sendPaymentExceptionByMail(e, "postAuthVoid", "payeezy", orderR,
												e.getMessage(),environment);
										LOGGER.error("response payezzy  VOID Failed---------" + e
												+ " orderId-----------" + orderR.getId());
									}
								} catch (Exception e) {
									LOGGER.error("error: " + e.getMessage());
									LOGGER.error("response payezzy  VOID Failed---------" + e + " orderId-----------"
											+ orderR.getId());
								}
						}
					}
					orderR.setIsDefaults(2);
					orderRepo.save(orderR);
					if (orderR.getCustomer() != null) {

						if (merchant.getOwner() != null && merchant.getOwner().getPos() != null
								&& merchant.getOwner().getPos().getPosId() == IConstant.NON_POS) {
							orderId = orderR.getId();
						}

						MailSendUtil.sendOrderCancellationMail(customerName, orderId.toString(),
								orderR.getCustomer().getEmailId(), orderR.getMerchant().getName(), merchantLogo,
								orderR.getMerchant().getPhoneNumber(), /*
																		 * orderR.getMerchant().getOwner( ).getEmail()
																		 */
								"praveen.raghuvanshii@gmail.com", reason, orderR.getMerchant().getId());

					}

					responseText = "Order declined successfully";
					LOGGER.info("OrderServiceImpl :: setOrderStatus {NON-POS} : responseText " + responseText);
					LOGGER.info(
							"----------------End :: OrderServiceImpl : setOrderStatus {NON-POS}------------------------");
					return responseText;
				}

			}
			/*
			 * return "falied";
			 */
		} catch (Exception e) {
			LOGGER.info("-Exception :: OrderServiceImpl : setOrderStatus {NON-POS} :: " + e);
			MailSendUtil.sendExceptionByMail(e,environment);

			return "Exception";
		}
	}

	public List<CategoryDto> getPizzaCategory(Merchant merchant) {
		/*
		 * List<Category> categoryList = categoryRepository
		 * .findByMerchantIdAndIsPizza(merchant.getId()); List<CategoryDto>
		 * finalCategories = new ArrayList<CategoryDto>(); if (categoryList != null &&
		 * !categoryList.isEmpty()) { for (Category category : categoryList) {
		 * CategoryDto categoryDto = new CategoryDto();
		 * categoryDto.setId(category.getId());
		 * categoryDto.setCategoryName(category.getName());
		 * categoryDto.setCategoryStatus(category.getItemStatus());
		 * categoryDto.setCategoryImage(category.getCategoryImage());
		 * categoryDto.setIsPizza(category.getIsPizza());
		 * finalCategories.add(categoryDto); } } return finalCategories;
		 */
		LOGGER.info("===============  OrderServiceImpl : Inside getPizzaCategory :: Start  ============= ");
		LOGGER.info(" === merchant.getId() : " + merchant.getId());

		String timeZoneCode = "America/Chicago";
		if (merchant != null && merchant.getId() != null) {
			if (merchant.getTimeZone() != null && merchant.getTimeZone().getTimeZoneCode() != null) {

				timeZoneCode = merchant.getTimeZone().getTimeZoneCode();
			}
			String currentDay = DateUtil.getCurrentDayForTimeZone(timeZoneCode);
			String currentTime = DateUtil.getCurrentTimeForTimeZone(timeZoneCode);
			LOGGER.info(" === currentTime : " + currentDay + " currentTime : " + currentTime);
			List<Category> categories = categoryRepository.findByMerchantIdAndIsPizza(merchant.getId(), currentDay,
					currentTime, currentTime);
			List<CategoryDto> finalCategories = new ArrayList<CategoryDto>();
			for (Category category : categories) {
				boolean categoryTimingStatus = true;

				if (categoryTimingStatus && category.getItemStatus() != null
						&& category.getItemStatus() != IConstant.SOFT_DELETE) {

					CategoryDto categoryDto = new CategoryDto();
					categoryDto.setId(category.getId());
					categoryDto.setCategoryName(category.getName());
					categoryDto.setCategoryStatus(category.getItemStatus());
					categoryDto.setCategoryImage(category.getCategoryImage());
					categoryDto.setIsPizza(category.getIsPizza());
					categoryDto.setSortOrder(category.getSortOrder());
					finalCategories.add(categoryDto);
				}
			}
			LOGGER.info("===============  OrderServiceImpl : Inside getPizzaCategory :: End  ============= ");

			return finalCategories;
		} else {
			LOGGER.info("===============  OrderServiceImpl : Inside getPizzaCategory :: End  ============= ");

			return null;
		}

	}

	public String savePizzaOrder(JSONObject jObject, String finalJson, Customer customer, Merchant merchant,
			Double discount, String convenienceFee, String deliveryItemPrice, String avgDeliveryTime, String orderType,
			String auxTax, String tax, Integer addressId, Integer zoneId,String fundCode) {

		LOGGER.info("----------------Start :: OrderServiceImpl : savePizzaOrder------------------------");
		LOGGER.info(" finalJson : " + finalJson + " discount : " + discount + " convenienceFee : " + convenienceFee
				+ " deliveryItemPrice : " + deliveryItemPrice + " avgDeliveryTime : " + avgDeliveryTime
				+ " orderType : " + orderType + " auxTax : " + auxTax + " tax : " + tax );
		
		String result = null;

		if (jObject != null && customer != null && merchant != null) {
			LOGGER.info(" ===jObject : " + jObject.toString() + " customer : " + customer.getId() + " merchant : "+ merchant.getId());
		}
		OrderR orderR = null;
		double auxTaxValue = 0;
		double taxValue = 0;
		double totalTax = 0;
		String timeZoneCode="America/Chicago";
		Gson gson = new Gson();
		CloverOrderVO cloverOrderVO = gson.fromJson(finalJson, CloverOrderVO.class);

		String subTotal = (cloverOrderVO.getPaymentVO().getSubTotal() != null)
				? cloverOrderVO.getPaymentVO().getSubTotal()
				: "0.0";
		String paymentMethod = (cloverOrderVO.getPaymentVO().getPaymentMethod() != null)
				? cloverOrderVO.getPaymentVO().getPaymentMethod()
				: "Cash";
		double tip = (new Double(cloverOrderVO.getPaymentVO().getTip()) != null) ? cloverOrderVO.getPaymentVO().getTip()
				: 0.0;
		String futureDate = (cloverOrderVO.getPaymentVO().getFutureDate() != null)
				? cloverOrderVO.getPaymentVO().getFutureDate()
				: "";
		String futureTime = (cloverOrderVO.getPaymentVO().getFutureTime() != null)
				? cloverOrderVO.getPaymentVO().getFutureTime()
				: "";
		String futureOrderType = (cloverOrderVO.getPaymentVO().getFutureOrderType() != null)
				? cloverOrderVO.getPaymentVO().getFutureOrderType()
				: "";

		LOGGER.info("== cloverOrderVO.getOrderVO().getTotal() : " + cloverOrderVO.getOrderVO().getTotal());
		
		if (Double.parseDouble(cloverOrderVO.getOrderVO().getTotal()) > 0.0) {
		LOGGER.info("OrderServiceImpl :: savePizzaOrder : addressId " + addressId);
		LOGGER.info("OrderServiceImpl :: savePizzaOrder : zoneId " + zoneId);

		if (merchant.getOwner() != null && merchant.getOwner().getPos() != null
				&& merchant.getOwner().getPos().getPosId() != null && merchant.getOwner().getPos().getPosId() == 1) {
			orderR = OrderUtil.getObjectFromJson(jObject, customer, merchant);
			orderR.setAddressId(addressId);
			orderR.setZoneId(zoneId);

		} else {
			orderR = new OrderR();
			orderR.setCustomer(customer);
			orderR.setMerchant(merchant);
			orderR.setAddressId(addressId);
			orderR.setZoneId(zoneId);

			String instruction = cloverOrderVO.getOrderVO().getNote();
			orderR.setOrderNote(instruction.split(":")[1]);
			// orderR.setIsDefaults(3);
			orderR.setIsDefaults(0);

			if (auxTax != null) {
				auxTaxValue = Double.valueOf(auxTax);
			}
			if (tax != null) {
				taxValue = Double.valueOf(tax);
			}
			totalTax = auxTaxValue + taxValue;
			orderR.setSalesTax(tax);
			orderR.setAuxTax(auxTax);
			orderR.setTax(String.valueOf(totalTax));

			orderR.setOrderPrice(Double.parseDouble(cloverOrderVO.getOrderVO().getTotal()) / 100);
			if(fundCode!=null && !fundCode.isEmpty()) {
			VirtualFund virtualFund = virtualFundRepository.findByCodeAndMerchantId(fundCode, merchant.getId());
			orderR.setVirtualFund(virtualFund);
			}
			orderRepo.save(orderR);
			orderR.setOrderPosId(Integer.toString(orderR.getId()));
				
				LOGGER.info("fundCode : "+fundCode);
				if(fundCode !=null && !fundCode.isEmpty()) {
					List<CustomerCode> dbCustomerCode = customerCodeRepository.findByFundCodeAndCustomerId(fundCode,orderR.getCustomer().getId());
					if(dbCustomerCode.size()==0) {
						CustomerCode customerCode = new CustomerCode();
						customerCode.setCustomer(orderR.getCustomer());
						customerCode.setFundCode(fundCode);
						customerCodeRepository.save(customerCode);
						LOGGER.info("CustomerCode saved succesfully : ");
					}
				}

			LOGGER.info("OrderServiceImpl :: savePizzaOrder : orderId " + orderR.getId());
		}
		if (null != orderR) {
			// orderR.setIsDefaults(0);
			Integer hourDifference = 0;
			Integer minutDifference = 0;
			if (merchant != null && merchant.getTimeZone() != null
					&& merchant.getTimeZone().getTimeZoneCode() != null) {

//				hourDifference = merchant.getTimeZone().getHourDifference();
//				if (merchant.getTimeZone().getMinutDifference() != null)
//					minutDifference = merchant.getTimeZone().getMinutDifference();
				
					timeZoneCode=merchant.getTimeZone().getTimeZoneCode();
					LOGGER.info("OrderServiceImpl :: savePizzaOrder : merchantTimeZone " + merchant.getTimeZone().getTimeZoneName());
			}
			LOGGER.info("OrderServiceImpl :: savePizzaOrder : timeZoneCode " + timeZoneCode);
			Date currentDate =DateUtil.getCurrentDateForTimeZonee(timeZoneCode);
			Date fullFiledfDate =DateUtil.getCurrentDateForTimeZonee(timeZoneCode);
			if(currentDate==null)
				currentDate=new Date();
			if(fullFiledfDate==null)
			{
				fullFiledfDate=new Date();
			}
			LOGGER.info("OrderServiceImpl :: savePizzaOrder : fullFiledfDate " + fullFiledfDate);
			fullFiledfDate=DateUtil.addOrderAvgTimeIntoCurrentTime(timeZoneCode,fullFiledfDate,30);
			LOGGER.info("OrderServiceImpl :: savePizzaOrder : fullFiledfDate " + fullFiledfDate);
//			Date currentDate = new Date();
//			Calendar cal = Calendar.getInstance();
//			// remove next line if you're always using the current time.
//			cal.setTime(currentDate);
//			cal.add(Calendar.HOUR, hourDifference);
//			cal.add(Calendar.MINUTE, minutDifference);
//			currentDate = cal.getTime();
			orderR.setCreatedOn(currentDate);
			orderR.setFulfilled_on(fullFiledfDate);
			// Gson gson = new Gson();
			// CloverOrderVO cloverOrderVO = gson.fromJson(finalJson,
			// CloverOrderVO.class);
			taxValue = 0;
			auxTaxValue = 0;
			totalTax = 0;
			if (auxTax != null) {
				auxTaxValue = Double.valueOf(auxTax);
			}
			if (tax != null) {
				taxValue = Double.valueOf(tax);
			}
			totalTax = auxTaxValue + taxValue;
			orderR.setSalesTax(tax);
			orderR.setAuxTax(auxTax);
			orderR.setTax(String.valueOf(totalTax));

			orderR.setConvenienceFee(convenienceFee);
			orderR.setDeliveryFee(deliveryItemPrice);
			orderR.setOrderDiscount(discount);
			
			orderR.setOrderType(orderType);
			orderR.setSubTotal(subTotal);
			orderR.setPaymentMethod(paymentMethod);
			orderR.setTipAmount(tip);
			avgTime=getAvgTimeForOrder(merchant, orderR);
			orderR.setOrderAvgTime(avgTime);
			if (("on".equals(futureOrderType)) && (!"select".equals(futureDate))) {

				orderR.setIsFutureOrder(IConstant.BOOLEAN_TRUE);
				LOGGER.error(
						"OrderServiceImpl :: updateOrderStatus : IsFutureOrder Date " + IConstant.BOOLEAN_TRUE);

				Date futureDateTime = DateUtil.futureDateAndTime(futureDate, futureTime);
				if (orderR != null) {
					orderR.setFulfilled_on(futureDateTime);
					LOGGER.error("OrderServiceImpl :: upOrderStatus : Fulfilled_on  " + futureTime);
				}
			} else {
				orderR.setIsFutureOrder(IConstant.BOOLEAN_FALSE);
				LOGGER.error("OrderServiceImpl :: upOrderStatus : IsFutureOrder  " + IConstant.BOOLEAN_FALSE);

				Date fulfilled_on = DateUtil.getCurrentDateForTimeZonee(timeZoneCode);
				fulfilled_on=DateUtil.addOrderAvgTimeIntoCurrentTime(timeZoneCode,fullFiledfDate,Integer.valueOf(avgTime));
				orderR.setFulfilled_on(fulfilled_on);
				LOGGER.error("OrderServiceImpl :: updateOrderStatus : Fulfilled_on Date " + fulfilled_on);

			}

			if (orderR.getOrderAvgTime() == null || orderR.getOrderAvgTime().isEmpty())
				orderR.setOrderAvgTime(getAvgTimeForOrder(merchant, orderR));
            orderRepo.save(orderR);

			LOGGER.info("OrderServiceImpl :: savePizzaOrder : orderId " + orderR.getId());

			DecimalFormat df2 = new DecimalFormat("###.##");
			DecimalFormat df3 = new DecimalFormat("###.###");
			DecimalFormat df4 = new DecimalFormat("###.####");

			Map<String, Double> taxMap = new HashMap<String, Double>();
			Double taxAbleTotal = 0.0;
			Double auxTaxAbleTotal = 0.0;
			for (OrderItemVO item : cloverOrderVO.getOrderItemVOs()) {
				if ((item.getItem().getIsPizza() == null || !item.getItem().getIsPizza())) {
					OrderItem orderItem = new OrderItem();
					Item item2 = null;
					if (merchant != null && merchant.getOwner() != null && merchant.getOwner().getPos() != null
							&& merchant.getOwner().getPos().getPosId() != null
							&& (merchant.getOwner().getPos().getPosId() == IConstant.NON_POS
									|| merchant.getOwner().getPos().getPosId() == IConstant.FOCUS)) {
						Integer itemId = 0;
						try {
							if (item.getItem().getExtraCharge() != null && !item.getItem().getExtraCharge()) {
								itemId = Integer.parseInt(item.getItem().getId());
								item2 = itemmRepository.findByIdAndMerchantId(itemId, merchant.getId());
							}
						} catch (Exception e) {
							LOGGER.error("OrderServiceImpl :: savePizzaOrder : Exception " + e.getMessage());
							MailSendUtil.sendExceptionByMail(e,environment);
						}
					} else {
						LOGGER.info(" ===item.getItem().getItemPosId() : " + item.getItem().getItemPosId());
						if (item.getItem().getExtraCharge() != null && !item.getItem().getExtraCharge()) {
							item2 = itemmRepository.findByPosItemIdAndMerchantId(item.getItem().getItemPosId(),
									merchant.getId());
						}
					}
					if (item2 != null && item2.getAuxTaxAble() != null && item2.getAuxTaxAble()) {
						OrderType orderTypes = findByMerchantIdAndLabel(merchant.getId(), orderType);
						if (orderTypes != null && orderTypes.getAuxTax() != null && orderTypes.getAuxTax() != 0) {
							double totalItemAuxTax = (item2.getPrice() / 100) * orderTypes.getAuxTax();
							orderItem.setAuxTax(String.valueOf(df3.format(totalItemAuxTax)));
							if (taxMap.containsKey("auxTaxValue")) {
								Double total = taxMap.get("auxTaxValue")
										+ (item2.getPrice() * Integer.parseInt(item.getUnitQty()));
								taxMap.put("auxTaxValue", total);
							} else {
								taxMap.put("auxTaxValue", item2.getPrice() * Integer.parseInt(item.getUnitQty()));
							}
						}
					}

					if (item2 != null && item2.getTaxAble() != null && item2.getTaxAble()) {
						OrderType orderTypes = findByMerchantIdAndLabel(merchant.getId(), orderType);
						if (orderTypes != null && orderTypes.getSalesTax() != null && orderTypes.getSalesTax() != 0) {
							double totalItemTax = (item2.getPrice() / 100) * orderTypes.getSalesTax();
							orderItem.setSalesTax(String.valueOf(df4.format(totalItemTax)));
							if (taxMap.containsKey("saleTaxValue")) {
								Double total = taxMap.get("saleTaxValue")
										+ (item2.getPrice() * Integer.parseInt(item.getUnitQty()));
								taxMap.put("saleTaxValue", total);
							} else {
								taxMap.put("saleTaxValue", item2.getPrice() * Integer.parseInt(item.getUnitQty()));
							}
						}
					}

					orderItem.setItem(item2);
					orderItem.setOrder(orderR);
					orderItem.setQuantity(Integer.parseInt(item.getUnitQty()));
					// orderItemRepository.save(orderItem);
					Double itemPrice = 0.0;
					Double taxablleItemPrice = 0.0;
					if (orderItem.getItem() != null && orderItem.getItem().getPrice() != null) {
						if (merchant != null && merchant.getOwner() != null && merchant.getOwner().getPos() != null
								&& merchant.getOwner().getPos().getPosId() != null
								&& (merchant.getOwner().getPos().getPosId() == IConstant.NON_POS
										|| merchant.getOwner().getPos().getPosId() == IConstant.FOCUS)) {
							itemPrice = orderItem.getItem().getPrice();
						} else {
							itemPrice = orderItem.getItem().getPrice() * Integer.parseInt(item.getUnitQty());
							taxablleItemPrice = taxablleItemPrice + orderItem.getItem().getPrice();
						}
					}

					List<OrderItemModifier> itemModifiers = new ArrayList<OrderItemModifier>();
					Double modifierPrice = 0.0;
					Double taxableModifierPrice = 0.0;
					for (Modifications modifications : item.getModifications()) {
						LOGGER.info(" ===modifications.getModifier().getId() : " + modifications.getModifier().getId());

						OrderItemModifier itemModifier = new OrderItemModifier();
						if (merchant != null && modifications.getModifier() != null) {
							if (merchant != null && merchant.getOwner() != null && merchant.getOwner().getPos() != null
									&& merchant.getOwner().getPos().getPosId() != null
									&& (merchant.getOwner().getPos().getPosId() == IConstant.NON_POS
											|| merchant.getOwner().getPos().getPosId() == IConstant.FOCUS)) {
								itemModifier.setModifiers(modifiersRepository.findByIdAndMerchantId(
										Integer.parseInt(modifications.getModifier().getId()), merchant.getId()));

								if (itemModifier.getModifiers() != null
										&& itemModifier.getModifiers().getPrice() != null) {
									itemPrice += modifierPrice + itemModifier.getModifiers().getPrice();
									taxableModifierPrice = taxableModifierPrice
											+ itemModifier.getModifiers().getPrice();
								}

							} else {

								itemModifier.setModifiers(modifiersRepository.findByPosModifierIdAndMerchantId(
										modifications.getModifier().getId(), merchant.getId()));

								if (itemModifier.getModifiers() != null
										&& itemModifier.getModifiers().getPrice() != null) {
									modifierPrice = modifierPrice + (itemModifier.getModifiers().getPrice()
											* Integer.parseInt(item.getUnitQty()));
									taxableModifierPrice = taxableModifierPrice
											+ itemModifier.getModifiers().getPrice();
								}

								/*
								 * if (item2 != null && item2.getAuxTaxAble()) { OrderType orderTypes =
								 * findByMerchantIdAndLabel(merchant.getId(), orderType); if (orderTypes != null
								 * && orderTypes.getAuxTax() != null && orderTypes.getAuxTax() != 0) { Double
								 * totalModifierAuxTax = (itemPrice / 100) * orderTypes.getAuxTax();
								 * //totalItemAuxTax += totalModifierAuxTax;
								 * orderItem.setAuxTax(String.valueOf(df3.format(totalModifierAuxTax))); if
								 * (taxMap.containsKey("auxTaxValue")) { Double total =
								 * taxMap.get("auxTaxValue") + modifierPrice; //Double total =
								 * taxMap.get("auxTaxValue"); taxMap.put("auxTaxValue", total); } else {
								 * taxMap.put("auxTaxValue", itemPrice); } } }
								 */

								/*
								 * if (item2 != null && item2.getTaxAble()) { OrderType orderTypes =
								 * findByMerchantIdAndLabel(merchant.getId(), orderType); if (orderTypes != null
								 * && orderTypes.getSalesTax() != null && orderTypes.getSalesTax() != 0) {
								 * double totalModifierTax = (itemPrice / 100) * orderTypes.getSalesTax();
								 * orderItem.setSalesTax(String.valueOf(df4.format(totalModifierTax))); if
								 * (taxMap.containsKey("saleTaxValue")) { Double total =
								 * taxMap.get("saleTaxValue") + modifierPrice; // Double total =
								 * taxMap.get("saleTaxValue"); taxMap.put("saleTaxValue", total); } else {
								 * taxMap.put("saleTaxValue", itemPrice); } } }
								 */
							}
						}
						itemModifier.setOrderItem(orderItem);
						itemModifier.setQuantity(Integer.parseInt(item.getUnitQty()));
						itemModifiers.add(itemModifier);
					}

					// start
					if (item2 != null && item2.getAuxTaxAble() != null && item2.getAuxTaxAble()) {
						OrderType orderTypes = findByMerchantIdAndLabel(merchant.getId(), orderType);
						if (orderTypes != null && orderTypes.getAuxTax() != null && orderTypes.getAuxTax() != 0) {
							Double totalModifierAuxTax = ((taxablleItemPrice + taxableModifierPrice) / 100)
									* orderTypes.getAuxTax();
							orderItem.setAuxTax(String.valueOf(df3.format(totalModifierAuxTax)));

							if (taxMap.containsKey("auxTaxValue")) {
								Double total = taxMap.get("auxTaxValue") + modifierPrice;
								// Double total = taxMap.get("auxTaxValue");
								taxMap.put("auxTaxValue", total);
							} else {
								taxMap.put("auxTaxValue", itemPrice);
							}
						}
					}

					if (item2 != null && item2.getTaxAble() != null && item2.getTaxAble()) {
						OrderType orderTypes = findByMerchantIdAndLabel(merchant.getId(), orderType);
						if (orderTypes != null && orderTypes.getSalesTax() != null && orderTypes.getSalesTax() != 0) {
							double totalModifierTax = ((taxablleItemPrice + taxableModifierPrice) / 100)
									* orderTypes.getSalesTax();
							orderItem.setSalesTax(String.valueOf(df4.format(totalModifierTax)));

							if (taxMap.containsKey("saleTaxValue")) {
								Double total = taxMap.get("saleTaxValue") + modifierPrice;
//                            	Double total = taxMap.get("saleTaxValue");
								taxMap.put("saleTaxValue", total);
							} else {
								taxMap.put("saleTaxValue", itemPrice);
							}
						}
					}
					// end
					orderItem.setOrderItemPrice(taxablleItemPrice);
					orderItemRepository.save(orderItem);
					itemModifierRepository.save(itemModifiers);
				} else {
					OrderPizza orderPizza = new OrderPizza();
					PizzaTemplate pizza = null;
					LOGGER.info(" === item.getItem().getId() : " + item.getItem().getId());
					pizza = pizzaTemplateRepository.findByIdAndMerchantId(Integer.parseInt(item.getItem().getId()),
							merchant.getId());
					PizzaTemplateSize pizzaTemplateSize = pizzaTemplateSizeRepository
							.findByPizzaSizeIdAndPizzaTemplateId(Integer.parseInt(item.getItem().getPizzaSizeId()),
									Integer.parseInt(item.getItem().getId()));
					if (pizza != null && pizza.getAuxTaxable() != null && pizza.getAuxTaxable()) {

						OrderType orderTypes = findByMerchantIdAndLabel(merchant.getId(), orderType);
						if (orderTypes != null && orderTypes.getAuxTax() != null && orderTypes.getAuxTax() != 0) {
							double totalItemAuxTax = (pizzaTemplateSize.getPrice() / 100) * orderTypes.getAuxTax();
							orderPizza.setAuxTax(String.valueOf(df3.format(totalItemAuxTax)));
							if (taxMap.containsKey("auxTaxValue")) {
								Double total = taxMap.get("auxTaxValue") + pizzaTemplateSize.getPrice();
								taxMap.put("auxTaxValue", total);
							} else {
								taxMap.put("auxTaxValue", pizzaTemplateSize.getPrice());
							}
						}
					}

					if (pizza != null && pizza.getTaxable() != null && pizza.getTaxable()) {
						OrderType orderTypes = findByMerchantIdAndLabel(merchant.getId(), orderType);
						if (orderTypes != null && orderTypes.getSalesTax() != null && orderTypes.getSalesTax() != 0) {
							double totalItemTax = (pizzaTemplateSize.getPrice() / 100) * orderTypes.getSalesTax();
							orderPizza.setSalesTax(String.valueOf(df4.format(totalItemTax)));
							if (taxMap.containsKey("saleTaxValue")) {
								Double total = taxMap.get("saleTaxValue") + pizzaTemplateSize.getPrice();
								taxMap.put("saleTaxValue", total);
							} else {
								taxMap.put("saleTaxValue", pizzaTemplateSize.getPrice());
							}
						}
					}

					orderPizza.setPizzaTemplate(pizza);
					if (pizzaTemplateSize != null && pizzaTemplateSize.getPizzaSize() != null) {
						orderPizza.setPizzaSize(pizzaTemplateSize.getPizzaSize());
					}
					orderPizza.setPrice(pizzaTemplateSize.getPrice());
					orderPizza.setOrder(orderR);
					orderPizza.setCreatedOn(currentDate);
					orderPizza.setQuantity(Integer.parseInt(item.getUnitQty()));
					orderPizza = orderPizzaRepository.save(orderPizza);

					if (item.getItem().getModifier() != null && !item.getItem().getModifier().isEmpty()) {
						List<com.foodkonnekt.model.OrderPizzaToppings> orderPizzaToppingList = new ArrayList<com.foodkonnekt.model.OrderPizzaToppings>();
						for (ModifierVO modifications : item.getItem().getModifier()) {
							com.foodkonnekt.model.OrderPizzaToppings orderPizzaTopping = new com.foodkonnekt.model.OrderPizzaToppings();
							LOGGER.info(" === modifications.getId() : " + modifications.getId());
							if (modifications.getIsCrust() != null && modifications.getIsCrust()) {
								PizzaCrust pizzaCrust = pizzaCrustRepository.findByMerchantIdAndId(merchant.getId(),
										Integer.parseInt(modifications.getId()));
								OrderPizzaCrust orderPizzaCrust = new OrderPizzaCrust();
								orderPizzaCrust.setOrderPizza(orderPizza);
								orderPizzaCrust.setPizzacrust(pizzaCrust);
								orderPizzaCrust.setPrice(Double.parseDouble(modifications.getPrice()));
								orderPizzaCrust.setQuantity(Integer.parseInt(modifications.getQuantity()));

								orderPizzaCrustRepository.save(orderPizzaCrust);
							} else {
								orderPizzaTopping.setPizzaTopping(pizzaToppingRepository.findByMerchantIdAndId(
										merchant.getId(), Integer.parseInt(modifications.getId())));

								orderPizzaTopping.setOrderPizza(orderPizza);
								orderPizzaTopping.setQuantity(Integer.parseInt(item.getUnitQty()));

								if (modifications.getToppingSide() != null) {
									if (modifications.getToppingSide().equals("1")) {
										orderPizzaTopping.setSide1(true);
										orderPizzaTopping.setSide2(false);
										orderPizzaTopping.setWhole(false);
									} else if (modifications.getToppingSide().equals("2")) {
										orderPizzaTopping.setSide1(false);
										orderPizzaTopping.setSide2(true);
										orderPizzaTopping.setWhole(false);
									} else {
										orderPizzaTopping.setSide1(false);
										orderPizzaTopping.setSide2(false);
										orderPizzaTopping.setWhole(true);
									}
								}
								orderPizzaTopping.setPrice(Double.parseDouble(modifications.getPrice()));
								orderPizzaToppingList.add(orderPizzaTopping);
							}
							orderPizzaToppingsRepository.save(orderPizzaToppingList);

						}

					}
				}

				if (taxMap.containsKey("saleTaxValue")) {
					orderR.setTaxableTotal(taxMap.get("saleTaxValue").toString());
				}
				if (taxMap.containsKey("auxTaxValue")) {
					orderR.setAuxTaxableTotal(taxMap.get("auxTaxValue").toString());
				}
				orderRepo.save(orderR);

				LOGGER.info("OrderServiceImpl :: savePizzaOrder : orderId " + orderR.getId());

				if (merchant.getOwner() != null && merchant.getOwner().getPos() != null
						&& merchant.getOwner().getPos().getPosId() != null
						&& merchant.getOwner().getPos().getPosId() == IConstant.POS_CLOVER) {

					if (orderR.getOrderPosId() != null) {

						Integer merchantSubscriptionId = merchantSubscriptionRepository
								.findByMerchantId(merchant.getId());
						LOGGER.info(" === merchantSubscriptionId : " + merchantSubscriptionId);
						MerchantOrders merchantOrders = merchantOrdersRepository
								.findByMerchantSubscriptionId(merchantSubscriptionId);
						// call metered API for increase order count
						if (merchantOrders != null && merchantOrders.getMerchantSubscription() != null
								&& merchantOrders.getMerchantSubscription().getSubscription() != null) {
							long difference = DateUtil.findDifferenceBetweenTwoDates(merchantOrders.getEndDate(),
									DateUtil.currentDate());

							if ((difference > 0) && (merchantOrders.getOrderCount() <= merchantOrders
									.getMerchantSubscription().getSubscription().getOrderLimit())) {
								LOGGER.info("-------Metered if block----");
							} else {
								LOGGER.info("-------Metered else block----");
								merchant.setSubscription(merchantOrders.getMerchantSubscription().getSubscription());
//								String response = CloverUrlUtil.addMteredPrice(merchant,environment);
//								LOGGER.info("-------Metered API response-----" + response);
							}
							merchantOrders.setOrderCount(merchantOrders.getOrderCount() + 1);
							merchantOrdersRepository.save(merchantOrders);
						} else {
							LOGGER.error("OrderServiceImpl :: savePizzaOrder : Merchant " + merchant.getName()
									+ "'doesn't have any Subscription");
							MailSendUtil.sendErrorMailToAdmin(
									"Merchant '" + merchant.getName() + "'doesn't have any Subscription",environment);
						}
					}
				}
			}
		}
			LOGGER.info("OrderServiceImpl :: savePizzaOrder : merchantId " + merchant.getId());
			LOGGER.info("----------------End :: OrderServiceImpl : savePizzaOrder------------------------");
			
			if (orderR.getOrderPrice() > 0.0) {
				result = orderR.getOrderPosId();
				// return result;
			} else {
				orderR.setIsDefaults(3);
				orderRepo.save(orderR);
			}
		}

		return result;
	}

	public List<CategoryDto> findCategoriesByMerchantIdV2(Merchant merchant) {
		LOGGER.info("===============  OrderServiceImpl : Inside findCategoriesByMerchantIdV2 :: Start  ============= ");
		LOGGER.info(" === merchant.getId() : " + merchant.getId());

		String timeZoneCode = "America/Chicago";
		if (merchant != null && merchant.getId() != null) {
			if (merchant.getTimeZone() != null && merchant.getTimeZone().getTimeZoneCode() != null) {

				timeZoneCode = merchant.getTimeZone().getTimeZoneCode();
			}
			String currentDay = DateUtil.getCurrentDayForTimeZone(timeZoneCode);
			String currentTime = DateUtil.getCurrentTimeForTimeZone(timeZoneCode);
			LOGGER.info(" === currentTime : " + currentDay + " currentTime : " + currentTime);

			List<Category> categories = categoryRepository.findByMerchantIdOrderBySortOrderAscByQueryV2(
					merchant.getId(), currentDay, currentTime, currentTime);
			List<CategoryDto> finalCategories = new ArrayList<CategoryDto>();
			for (Category category : categories) {
				boolean categoryTimingStatus = true;
				/*
				 * if(category.getAllowCategoryTimings()==IConstant. BOOLEAN_TRUE) {
				 * CategoryTiming categoryTiming=categoryTimingRepository.
				 * findByCategoryIdAndDayAndStartTimeAndEndTime(currentDay,
				 * category.getId(),currentTime,currentTime);
				 * 
				 * if(categoryTiming!=null ){ if(categoryTiming.isHoliday()){
				 * categoryTimingStatus=false; }else{ categoryTimingStatus=true; } }else{
				 * categoryTimingStatus=false; }}
				 */
				if (categoryTimingStatus && category.getItemStatus() != null
						&& category.getItemStatus() != IConstant.SOFT_DELETE) {
					/*
					 * List<Item> items = itemmRepository.findByCategoriesId(category.getId());
					 * List<ItemDto> itemDtos=findCategoryItems(category.getId(), merchant);; if (
					 * itemDtos!=null && itemDtos.size() >0) {
					 */
					CategoryDto categoryDto = new CategoryDto();
					categoryDto.setId(category.getId());
					categoryDto.setCategoryName(category.getName());
					categoryDto.setCategoryStatus(category.getItemStatus());
					categoryDto.setCategoryImage(category.getCategoryImage());
					categoryDto.setSortOrder(category.getSortOrder());
					finalCategories.add(categoryDto);
					/* } */
				}
			}
			LOGGER.info(
					"===============  OrderServiceImpl : Inside findCategoriesByMerchantIdV2 :: End  ============= ");

			return finalCategories;
		} else {
			LOGGER.info(
					"===============  OrderServiceImpl : Inside findCategoriesByMerchantIdV2 :: End  ============= ");

			return null;
		}

	}

	public Integer findItemCountByCategoryId(Integer categoryId, Merchant merchant) {
		LOGGER.info("===============  OrderServiceImpl : Inside findItemCountByCategoryId :: Start  ============= ");
		LOGGER.info(" ===categoryId : " + categoryId + " merchant : " + merchant.getId());
		String timeZoneCode = "America/Chicago";
		if (merchant != null && merchant.getTimeZone() != null && merchant.getTimeZone().getTimeZoneCode() != null) {

			timeZoneCode = merchant.getTimeZone().getTimeZoneCode();
		}
		String currentDay = DateUtil.getCurrentDayForTimeZone(timeZoneCode);
		String currentTime = DateUtil.getCurrentTimeForTimeZone(timeZoneCode);
		LOGGER.info("===============  OrderServiceImpl : Inside findItemCountByCategoryId :: End  ============= ");

		return categoryItemRepository.findItemCountByCategoryId(categoryId);
	}

	/*
	 * public Boolean findByMerchantIdandOId(Merchant merchant, OrderR order) {
	 * Boolean status = false;
	 * 
	 * OrderR fOrder = orderRepository.findByMerchantIdAndId(merchant.getId(),
	 * order.getId()); String reason = ""; String date = "";
	 * 
	 * if (fOrder != null) { fOrder.setOrderAvgTime(order.getOrderAvgTime());
	 * fOrder.setOrderType(order.getOrderType()); date = order.getOrderAvgTime();
	 * orderRepository.save(fOrder);
	 * 
	 * MailSendUtil.sendUpdatedTime(fOrder, reason, date); status = true; } return
	 * status; }
	 */

	public Boolean findByMerchantIdandOId(Merchant merchant, OrderR order) {
		Boolean status = false;
		LOGGER.info("---------------------OrderServiceImpl : Inside findByMerchantIdandOId---------------------");
		LOGGER.info(" === order.getId() : " + order.getId() + " merchant : " + merchant.getId());

		OrderR fOrder = orderRepository.findByMerchantIdAndId(merchant.getId(), order.getId());
		String reason = "Due to time changed by Merchant";

		String date = "";

		if (fOrder != null) {
			if (order.getOrderAvgTime() != null && order.getOrderAvgTime() != "" && fOrder.getOrderAvgTime() != null
					&& !fOrder.getOrderAvgTime().equals(order.getOrderAvgTime())) {
				long min = 0;
				String arrtimeZoneTime = null;
				String timeZoneCode = "America/Chicago";

				if (merchant.getTimeZone() != null && merchant.getTimeZone().getTimeZoneCode() != null) {
					timeZoneCode = merchant.getTimeZone().getTimeZoneCode();

					Date currentDate = new Date();
					DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					df.setTimeZone(TimeZone.getTimeZone(timeZoneCode));
					String timeZoneDate = df.format(currentDate);

					String timeZoneDateArray[] = timeZoneDate.split(" ");
					arrtimeZoneTime = timeZoneDateArray[1];

					Date createdOn = fOrder.getCreatedOn();

					long diffMs = currentDate.getTime() - createdOn.getTime();
					long diffSec = diffMs / 1000;
					min = diffSec / 60;
					System.out.println(min);

				}

				LOGGER.info("OrderServiceImpl : Inside findByMerchantIdandOId :: inside second check");

				date = order.getOrderAvgTime();
				MailSendUtil.sendUpdatedTime(fOrder, reason, date,environment);

				LOGGER.info("OrderServiceImpl : Inside findByMerchantIdandOId :: inside mail send");

				Date fullfilled_date = fOrder.getFulfilled_on();
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-dd-MM HH:mm:ss");
				String myTime = simpleDateFormat.format(fullfilled_date);
				String split[] = myTime.split(" ");
				String time = split[1];

				SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
				Date d = null;
				try {
					d = df.parse(time);
				} catch (ParseException e) {
					LOGGER.error(
							"===============  OrderServiceImpl : Inside findByMerchantIdandOId :: Exception  ============= "
									+ e);

					LOGGER.error("error: " + e.getMessage());
				}

				String finalTime = null;
				String newTime = null;
				Calendar cal = Calendar.getInstance();

				if (min > 1) {
					LOGGER.info("current time by timeZone===" + arrtimeZoneTime);
					try {
						d = df.parse(arrtimeZoneTime);
					} catch (ParseException e) {
						LOGGER.error(
								"===============  OrderServiceImpl : Inside findByMerchantIdandOId :: Exception  ============= "
										+ e);

						LOGGER.error("error: " + e.getMessage());
					}
					cal.setTime(d);
					cal.add(Calendar.MINUTE, Integer.parseInt(order.getOrderAvgTime()));
					newTime = df.format(cal.getTime());

				} else {
					cal.setTime(d);
					cal.add(Calendar.MINUTE, Integer.parseInt(order.getOrderAvgTime()));
					newTime = df.format(cal.getTime());
					LOGGER.info(newTime);
				}

				LOGGER.info("finaltime after minuted adding====" + newTime);
				finalTime = split[0] + " " + newTime;

				LOGGER.info("final fullfilled date is=========" + finalTime);

				DateFormat dateFormat = new SimpleDateFormat("yyyy-dd-MM HH:mm:ss");
				Date finalDate = null;
				try {
					finalDate = dateFormat.parse(finalTime);
				} catch (ParseException e) {
					LOGGER.error("error: " + e.getMessage());
					LOGGER.error("OrderServiceImpl : Inside findByMerchantIdandOId :: Exception " + e);
				}

				fOrder.setOrderAvgTime(order.getOrderAvgTime());
				fOrder.setOrderType(order.getOrderType());
				fOrder.setFulfilled_on(finalDate);

				orderRepo.save(fOrder);
			}
			status = true;
		}
		LOGGER.info(" ===status : " + status);
		LOGGER.info("-----------------OrderServiceImpl : Inside findByMerchantIdandOId :: END---------------------");
		return status;
	}

	public OrderR findById(Integer id) {
		LOGGER.info("===============  OrderServiceImpl : Inside findById :: Start  ============= ");
		LOGGER.info(" === id : " + id);
		OrderR orderR = orderRepository.findOne(id);
		LOGGER.info("===============  OrderServiceImpl : Inside findById :: End  ============= ");

		return orderR;
	}

	public void deleteOrder(String orderPosId) {
		LOGGER.info("===============  OrderServiceImpl : Inside deleteOrder :: Start  ============= ");
		LOGGER.info(" === orderPosId : " + orderPosId);
		OrderR orderR = orderRepository.findByOrderPosId(orderPosId);
		if (orderR != null) {
			List<OrderItem> orderItems = orderItemRepository.findByOrderId(orderR.getId());
			if (null != orderItems && !orderItems.isEmpty()) {
				for (OrderItem orderItem : orderItems) {
					LOGGER.info(" === orderItem.getId() : " + orderItem.getId());

					List<OrderItemModifier> orderItemModifiers = orderItemModifierRepository
							.findByOrderItemId(orderItem.getId());
					orderItemModifierRepository.delete(orderItemModifiers);
					orderItem.setOrder(null);
					orderItem.setItem(null);
				}
			}

			List<OrderPizza> orderPizzas = orderPizzaRepository.findByOrderId(orderR.getId());
			if (null != orderPizzas && !orderPizzas.isEmpty()) {
				for (OrderPizza orderPizza : orderPizzas) {
					LOGGER.info(" === orderPizza.getId() : " + orderPizza.getId());

					OrderPizzaCrust orderPizzaCrusts = orderPizzaCrustRepository.findByOrderPizzaId(orderPizza.getId());
					if (orderPizzaCrusts != null) {
						orderPizzaCrustRepository.delete(orderPizzaCrusts);
					}

					List<OrderPizzaToppings> orderPizzaToppings = orderPizzaToppingsRepository
							.findByOrderPizzaId(orderPizza.getId());
					if (!orderPizzaToppings.isEmpty() && orderPizzaToppings.size() > 0) {
						orderPizzaToppingsRepository.delete(orderPizzaToppings);
					}
					orderPizza.setOrder(null);
					orderPizza.setPizzaTemplate(null);
				}
			}
			orderR.setMerchant(null);
			orderR.setCustomer(null);
			orderItemRepository.delete(orderItems);
			orderPizzaRepository.delete(orderPizzas);
			orderRepository.delete(orderR);
			LOGGER.info("===============  OrderServiceImpl : Inside deleteOrder :: End  ============= ");

		}
	}

	public Address findByAddressId(Integer addressId) {
		LOGGER.info("===============  OrderServiceImpl : Inside findByAddressId :: Start  ============= ");
		LOGGER.info(" === addressId : " + addressId);
		LOGGER.info("===============  OrderServiceImpl : Inside findByAddressId :: End  ============= ");

		return addressRepository.findOne(addressId);
	}

	public String mostPopularMenuItemForFoodkonnekt(String merchantUid, String fromDate, String toDate)
			throws ParseException {
		LOGGER.info(
				"===============  OrderServiceImpl : Inside mostPopularMenuItemForFoodkonnekt :: Start  ============= ");
		LOGGER.info(" ===merchantUid : " + merchantUid + " fromDate : " + fromDate + " toDate : " + toDate);
		Merchant merchant = merchantService.findByMerchantUid(merchantUid);
		String popularItemName = "";
		if (merchant != null) {
			popularItemName = orderItemRepository.findMostPopularOrderItem(merchant.getId(), fromDate, toDate);
		}
		LOGGER.info(" === popularItemName : " + popularItemName);
		LOGGER.info(
				"===============  OrderServiceImpl : Inside mostPopularMenuItemForFoodkonnekt :: End  ============= ");

		return popularItemName;
	}

	public Map<String, Object> getSalesReportOfFoodkonnekt(String merchantUid, String fromDate, String toDate) {
		LOGGER.info("===============  OrderServiceImpl : Inside getSalesReportOfFoodkonnekt :: Start  ============= ");

		LOGGER.info(" ===merchantUid : " + merchantUid + " fromDate : " + fromDate + " toDate : " + toDate);

		Map<String, Object> responseMap = new HashMap<String, Object>();
		try {
			Merchant merchant = merchantService.findByMerchantUid(merchantUid);

			if (merchant != null && merchant.getId() != null) {
				fromDate = fromDate.replace("/", "-");
				toDate = toDate.replace("/", "-");
				SimpleDateFormat simpleDateFormate = new SimpleDateFormat("yyyy-MM-dd");
				Date startDate = simpleDateFormate.parse(fromDate);
				Date endDate = simpleDateFormate.parse(toDate);
				String orderCount = "0";
				String ordersSum = "0";
				String orderAvgPrice = "0";
				orderCount = orderRepository.findCountOFOrder(merchant.getId(), startDate, endDate);
				ordersSum = orderRepository.findSumOfOrderPrice(merchant.getId(), startDate, endDate);
				orderAvgPrice = orderRepository.findAvgOfOrderPrice(merchant.getId(), startDate, endDate);
				responseMap.put("totalOrderCountOfFk", orderCount != "" && orderCount != null ? orderCount : 0);
				responseMap.put("orderTotalSumOfFk", ordersSum != "" && ordersSum != null ? ordersSum : 0);
				responseMap.put("averageOrderValue", orderAvgPrice != "" && orderAvgPrice != null ? orderAvgPrice : 0);
			} else {
				responseMap.put("status", "failed");
			}
		} catch (Exception e) {
			LOGGER.error(
					"===============  OrderServiceImpl : Inside getSalesReportOfFoodkonnekt :: Exception  ============= "
							+ e);

			LOGGER.error("error: " + e.getMessage());
		}
		LOGGER.info("===============  OrderServiceImpl : Inside getSalesReportOfFoodkonnekt :: End  ============= ");

		return responseMap;
	}

	public Map<String, Object> getOrderNameAndCount(String merchantUid, String fromDate, String toDate) {
		LOGGER.info("===============  OrderServiceImpl : Inside getOrderNameAndCount :: Start  ============= ");

		LOGGER.info(" ===merchantUid : " + merchantUid + " fromDate : " + fromDate + " toDate : " + toDate);

		Map<String, Object> response = new HashMap<String, Object>();
		List<String> orderName = new ArrayList<String>();
		List<Integer> orderCount = new ArrayList<Integer>();
		List<Integer> orderSum = new ArrayList<Integer>();
		try {
			fromDate = fromDate.replace("/", "-");
			toDate = toDate.replace("/", "-");
			SimpleDateFormat simpleDateFormate = new SimpleDateFormat("yyyy-MM-dd");
			Date startDate = simpleDateFormate.parse(fromDate);
			Date endDate = simpleDateFormate.parse(toDate);
			Merchant merchant = merchantService.findByMerchantUid(merchantUid);
			if (merchant != null) {
				orderName = orderRepository.getOrdername(merchant.getId(), startDate, endDate);
				orderCount = orderRepository.getOrderCount(merchant.getId(), startDate, endDate);
				orderSum = orderRepository.getSumOfOrder(merchant.getId(), startDate, endDate);

				if (!orderName.isEmpty() && orderName != null) {
					response.put("itemName", orderName);
				}
				if (orderCount.isEmpty() && orderCount != null && orderName.size() == orderCount.size()) {
					response.put("orderCount", orderCount);
				}

				if (!orderName.isEmpty() && orderName != null && !orderSum.isEmpty() && orderSum != null
						&& orderName.size() == orderSum.size()) {
					response.put("orderSum", orderSum);
				}
			} else {
				response.put("status", "failed");
			}
		} catch (Exception e) {
			MailSendUtil.sendExceptionByMail(e,environment);
			LOGGER.error(
					"===============  OrderServiceImpl : Inside getOrderNameAndCount :: Exception  ============= " + e);

			LOGGER.error("error: " + e.getMessage());
		}
		LOGGER.info("===============  OrderServiceImpl : Inside getOrderNameAndCount :: End  ============= ");

		return response;
	}

	public Map<String, Object> getTopMenuReport(String locationUid, String fromDate, String toDate) {
		LOGGER.info("===============  OrderServiceImpl : Inside getTopMenuReport :: Start  ============= ");

		LOGGER.info(" ===locationUid : " + locationUid + " fromDate : " + fromDate + " toDate : " + toDate);

		Map<String, Object> response = new HashMap<String, Object>();
		List<String> orderDate = new ArrayList<String>();
		List<String> itemName = new ArrayList<String>();
		List<Integer> orderCount = new ArrayList<Integer>();
		List<Integer> orderHour = new ArrayList<Integer>();
		try {
			fromDate = fromDate.replace("/", "-");
			toDate = toDate.replace("/", "-");
			SimpleDateFormat simpleDateFormate = new SimpleDateFormat("yyyy-MM-dd");
			Date startDate = simpleDateFormate.parse(fromDate);
			Date endDate = simpleDateFormate.parse(toDate);
			Merchant merchant = merchantService.findByMerchantUid(locationUid);
			if (merchant != null) {
				orderDate = orderRepository.getTopMenuDateReport(merchant.getId(), startDate, endDate);
				orderCount = orderRepository.getTopMenuCountReport(merchant.getId(), startDate, endDate);
				orderHour = orderRepository.getTopMenuHourReport(merchant.getId(), startDate, endDate);
				itemName = orderRepository.getTopMenuItemReport(merchant.getId(), startDate, endDate);
				if (!orderDate.isEmpty() && orderDate != null && orderDate.size() == orderCount.size()) {
					response.put("orderDate", orderDate);
				}
				if (!orderCount.isEmpty() && orderCount != null && orderDate.size() == orderCount.size()) {
					response.put("orderCount", orderCount);
				}
				if (!orderDate.isEmpty() && orderDate != null && !orderHour.isEmpty() && orderHour != null
						&& orderDate.size() == orderHour.size()) {
					response.put("orderHour", orderHour);
				}

				if (!itemName.isEmpty() && itemName != null && !orderHour.isEmpty() && orderHour != null
						&& itemName.size() == orderHour.size()) {
					response.put("itemName", itemName);
				}

			} else {
				response.put("status", "failed");
			}
		} catch (Exception e) {
			LOGGER.error("error: " + e.getMessage());
			LOGGER.error(
					"===============  OrderServiceImpl : Inside getTopMenuReport :: Exception  ============= " + e);

			MailSendUtil.sendExceptionByMail(e,environment);
		}
		LOGGER.info("===============  OrderServiceImpl : Inside getTopMenuReport :: End  ============= ");

		return response;
	}

	public Double avgOrderValueByMerchantUId(String merchantUid, String fromDate, String toDate) throws ParseException {
		LOGGER.info("===============  OrderServiceImpl : Inside avgOrderValueByMerchantUId :: Start  ============= ");
		LOGGER.info(" ===merchantUid : " + merchantUid + " fromDate : " + fromDate + " toDate : " + toDate);

		Merchant merchant = merchantService.findByMerchantUid(merchantUid);
		Double avgOrderValue = 0d;
		if (merchant != null) {
			avgOrderValue = orderRepository.avgOrderPriceByMerchantIdAndCreatedDateBetweenAndIsDefault(merchant.getId(),
					fromDate, toDate);
		}
		LOGGER.info(" === avgOrderValue : " + avgOrderValue);
		LOGGER.info("===============  OrderServiceImpl : Inside avgOrderValueByMerchantUId :: End  ============= ");

		return avgOrderValue != null ? avgOrderValue : 0d;
	}

	public Map<String, Object> getTop3ItemReport(String locationUId, String fromDate, String toDate) {
		LOGGER.info("===============  OrderServiceImpl : Inside getTop3ItemReport :: Start  ============= ");
		LOGGER.info(" ===locationUid : " + locationUId + " fromDate : " + fromDate + " toDate : " + toDate);

		Map<String, Object> response = new HashMap<String, Object>();
		try {
			fromDate = fromDate.replace("/", "-");
			toDate = toDate.replace("/", "-");
			SimpleDateFormat simpleDateFormate = new SimpleDateFormat("yyyy-MM-dd");
			Date startDate = simpleDateFormate.parse(fromDate);
			Date endDate = simpleDateFormate.parse(toDate);
			Merchant merchant = merchantRepository.findByMerchantUid(locationUId);
			if (merchant != null) {
				List<String> items = orderItemRepository.getTop3ItemReport(merchant.getId(), startDate, endDate);

				response.put("top_3_orderitems", items);
			} else {
				response.put("status", "failed");
			}
		} catch (Exception e) {
			LOGGER.error(
					"===============  OrderServiceImpl : Inside getTop3ItemReport :: Exception  ============= " + e);

			LOGGER.error("error: " + e.getMessage());
		}
		LOGGER.info("===============  OrderServiceImpl : Inside getTop3ItemReport :: End  ============= ");

		return response;
	}

	public List<OrderR> findByMerchantIdAndStartDateAndEndDate(Integer merchantId, Date fromDate, Date toDate) {
		LOGGER.info(
				"===============  OrderServiceImpl : Inside findByMerchantIdAndStartDateAndEndDate :: Start  ============= ");
		LOGGER.info(" ===merchantId : " + merchantId + " fromDate : " + fromDate + " toDate : " + toDate);

		List<OrderR> orderRs = orderRepository.findAllOrdersFromStartDateAndEndDate(fromDate, toDate, merchantId);
		if (!orderRs.isEmpty() && orderRs.size() > 0) {
			for (OrderR orderR : orderRs) {
				orderR.setCustomer(null);
				orderR.setMerchant(null);
			}
		}
		LOGGER.info(
				"===============  OrderServiceImpl : Inside findByMerchantIdAndStartDateAndEndDate :: End  ============= ");

		return orderRs;
	}

	/*
	 * public String getNotificationJSONForPushNotification(int orderId, String
	 * deviceId, String locationUid, String merchantPosId) { NotificationVO response
	 * = new NotificationVO(); OrderR orderR = null; try { orderR =
	 * orderRepository.findOne(orderId); Customer customer = orderR.getCustomer();
	 * 
	 * List<Address> addresses =
	 * addressRepository.findByCustomerId(customer.getId()); for (Address address :
	 * addresses) { Address address2 = new Address();
	 * address2.setId(address.getId()); address2.setAddress1(address.getAddress1());
	 * address2.setAddress2(address.getAddress2());
	 * address2.setAddress3(address.getAddress3());
	 * address2.setCity(address.getCity()); address2.setState(address.getState());
	 * address2.setZip(address.getZip()); address.setCustomer(null);
	 * address.setMerchant(null); address.setIsDeliveryKoupon(null);
	 * address.setZones(null); } customer.setAddresses(addresses);
	 * 
	 * customer.setMerchantt(null);
	 * 
	 * customer.setVendor(null); customer.setCreatedDate(null);
	 * customer.setUpdatedDate(null); customer.setPassword(null);
	 * customer.setAnniversaryDate(null); customer.setBirthDate(null);
	 * 
	 * orderR.setCustomer(customer); orderR.setMerchant(null);
	 * response.setDeviceId(deviceId); response.setLocationUid(locationUid);
	 * response.setMerchantId(merchantPosId); Payload payload = new Payload();
	 * payload.setOrderR(orderR); response.setPayload(payload); Gson gson = new
	 * Gson(); String extraJson = gson.toJson(response); return extraJson; } catch
	 * (Exception exception) { if (exception != null) {
	 * MailSendUtil.sendExceptionByMail(exception,environment); } exception.printStackTrace();
	 * return null; } }
	 */

	public List<Integer> findMerchantsTodayDate(String currentDate) {
		LOGGER.info("===============  OrderServiceImpl : Inside findMerchantsTodayDate :: Start  ============= ");
		LOGGER.info(" ===currentDate : " + currentDate);
		LOGGER.info("===============  OrderServiceImpl : Inside findMerchantsTodayDate :: End  ============= ");

		return orderRepository.findMerchantsTodayDate(currentDate);
	}

	public OrderR findByOrderIdAndMerchantId(Integer id, Integer merchantId) {
		LOGGER.info("===============  OrderServiceImpl : Inside findByOrderIdAndMerchantId :: Start  ============= ");
		LOGGER.info(" === id : " + id + " merchantId : " + merchantId);
		OrderR orderR = orderRepository.findByIdAndMerchantId(id, merchantId);
		LOGGER.info("===============  OrderServiceImpl : Inside findByOrderIdAndMerchantId :: End  ============= ");

		return orderR;
	}

	public String getOrderDetailsByOrderId(Integer orderId, String merchantUid) {
		LOGGER.info("===============  OrderServiceImpl : Inside getOrderDetailsByOrderId :: Start  ============= ");
		LOGGER.info(" ===orderId : " + orderId + " merchantUid : " + merchantUid);
		String response = null;
		if (orderId != null && merchantUid != null) {
			Merchant merchant = merchantRepository.findByMerchantUid(merchantUid);
			if (merchant != null && merchant.getModileDeviceId() != null && merchant.getPosMerchantId() != null) {
				response = getNotificationJSON(orderId, merchant.getModileDeviceId(), merchantUid,
						merchant.getPosMerchantId());
			}
		}
		LOGGER.info("===============  OrderServiceImpl : Inside getOrderDetailsByOrderId :: End  ============= ");

		return response;
	}

	public static void printOrderReceipt(Printer printer, String orderIdd, String customerrId, Merchant merchant,Environment environment)
			throws Exception {
		LOGGER.info(
				"-----------------------OrderServiceImpl :: inside printOrderReceipt : Start-----------------------------------------");
		String mailMessage = null;
		if (printer != null && printer.getPrinterId() != null && printer.getPrinterRefreshToken() != null) {
			LOGGER.info("OrderServiceImpl : printOrderReceipt :: printerId " + printer.getPrinterId());
			LOGGER.info("OrderServiceImpl : printOrderReceipt :: refreshToken " + printer.getPrinterRefreshToken());
			LOGGER.info(" ===orderId :  " + orderIdd + " customerId : " + customerrId);
			ProducerUtil.sendFax(orderIdd, customerrId,environment);
			Boolean isOnline = CloudPrintUtil.checkIfPrinterOnline(printer.getPrinterId(),
					printer.getPrinterRefreshToken(),environment);
			LOGGER.info("OrderServiceImpl : printOrderReceipt :: printerOnlineOrNot " + isOnline);
			if (isOnline == true) {
				CloudPrintUtil.submitPrintJobs(
						environment.getProperty("THERMAL_RECEIPT") + EncryptionDecryptionUtil.decryption(orderIdd) + ".pdf",
						printer.getPrinterId(), printer.getPrinterRefreshToken(),environment);
			} else {
				if (merchant.getEmailId() != null && !merchant.getEmailId().isEmpty()) {
					LOGGER.info("OrderServiceImpl : printOrderReceipt :: Your printer is Offline.");
					mailMessage = "Your printer is Offline.";
					MailSendUtil.cloudPrinterMail(merchant.getEmailId(), mailMessage, merchant.getName(),environment);
					LOGGER.info(
							"OrderServiceImpl : printOrderReceipt :: Mail Send successfully " + merchant.getEmailId());
				}
			}
		} else {
			if (merchant.getEmailId() != null && !merchant.getEmailId().isEmpty()) {
				LOGGER.info("OrderServiceImpl : printOrderReceipt :: PrinterId is not found.");
				mailMessage = "PrinterId is not found.";
				MailSendUtil.cloudPrinterMail(merchant.getEmailId(), mailMessage, merchant.getName(),environment);
				LOGGER.info("OrderServiceImpl : printOrderReceipt :: Mail Send successfully " + merchant.getEmailId());
			}
		}
		LOGGER.info(
				"-----------------------OrderServiceImpl :: inside printOrderReceipt : End-------------------------------------------");
	}

	public PaymentGateWay getPaymentGatewayDetailsByMerchantId(Integer merchantId) {
		LOGGER.info(
				"===============  OrderServiceImpl : Inside getPaymentGatewayDetailsByMerchantId :: Start  ============= ");
		LOGGER.info(" ===merchantId : " + merchantId);
		PaymentGateWay paymentGateWay = new PaymentGateWay();
		PaymentGateWay dbPaymentGateWay = paymentGateWayRepository.findByMerchantIdAndIsDeletedAndIsActive(merchantId,
				false, true);
		LOGGER.info(" === dbPaymentGateWay : " + dbPaymentGateWay.getId());
		if (dbPaymentGateWay != null) {
			if (dbPaymentGateWay.getPayeezyMerchnatToken() != null) {
				paymentGateWay.setPayeezyMerchnatToken(
						EncryptionDecryptionUtil.decryption(dbPaymentGateWay.getPayeezyMerchnatToken()));
			}
			if (dbPaymentGateWay.getPayeezyMerchnatTokenJs() != null) {
				paymentGateWay.setPayeezyMerchnatTokenJs(
						EncryptionDecryptionUtil.decryption(dbPaymentGateWay.getPayeezyMerchnatTokenJs()));
			}
			if (dbPaymentGateWay.getPayeezy3DSApiKey() != null) {
				paymentGateWay.setPayeezy3DSApiKey(
						EncryptionDecryptionUtil.decryption(dbPaymentGateWay.getPayeezy3DSApiKey()));
			}
			if (dbPaymentGateWay.getPayeezy3DSApiIdentifier() != null) {
				paymentGateWay.setPayeezy3DSApiIdentifier(
						EncryptionDecryptionUtil.decryption(dbPaymentGateWay.getPayeezy3DSApiIdentifier()));
			}
			if (dbPaymentGateWay.getPayeezy3DSOrgUnitId() != null) {
				paymentGateWay.setPayeezy3DSOrgUnitId(
						EncryptionDecryptionUtil.decryption(dbPaymentGateWay.getPayeezy3DSOrgUnitId()));
			}
			if (dbPaymentGateWay.getPayeezy3DTaToken() != null) {
				paymentGateWay.setPayeezy3DTaToken(
						EncryptionDecryptionUtil.decryption(dbPaymentGateWay.getPayeezy3DTaToken()));
			}
		}
		LOGGER.info(
				"===============  OrderServiceImpl : Inside getPaymentGatewayDetailsByMerchantId :: End  ============= ");

		return paymentGateWay;
	}

	public List<Customer> getActiveFoodkonnektCustomerByLocationUidAnddate(String locationUid, String startDate,
			String endDate) {
		// Map<Integer, Object> response = new HashMap<Integer, Object>();
		List<String> orderDate = new ArrayList<String>();
		List<String> itemName = new ArrayList<String>();
		List<Integer> orderCount = new ArrayList<Integer>();
		List<Integer> orderHour = new ArrayList<Integer>();
		List<Customer> customers = new ArrayList<Customer>();
		List<Customer> result = new ArrayList<Customer>();

		int count = 0;
		try {
			if (startDate == null) {
				startDate = DateUtil.getBefore7Daydate();
			} else if (endDate == null) {
				endDate = DateUtil.findCurrentDateWithYYYYMMDD();
			} else if (startDate.equals(endDate)) {
				endDate = DateUtil.findOneDate(endDate);
			}
			startDate = startDate.replace("/", "-");
			endDate = endDate.replace("/", "-");
			SimpleDateFormat simpleDateFormate = new SimpleDateFormat("yyyy-MM-dd");
			Date fromDate = simpleDateFormate.parse(startDate);
			Date toDate = simpleDateFormate.parse(endDate);
			Merchant merchant = merchantService.findByMerchantUid(locationUid);
			if (merchant != null) {

				customers = customerrRepository.findActiveFoodkonnektCustomerByMerchantIdAndDate(merchant.getId(),
						fromDate, toDate);
				orderDate = orderRepository.getTopMenuDateReport(merchant.getId(), fromDate, toDate);
				orderCount = orderRepository.getTopMenuCountReport(merchant.getId(), fromDate, toDate);
				orderHour = orderRepository.getTopMenuHourReport(merchant.getId(), fromDate, toDate);
				itemName = orderRepository.getTopMenuItemReport(merchant.getId(), fromDate, toDate);

				if (!itemName.isEmpty() && itemName != null && !orderHour.isEmpty() && orderHour != null
						&& itemName.size() == orderHour.size()) {
					for (Customer customer : customers) {
						customer.setMerchantt(null);
						customer.setListOfALLDiscounts(null);
						customer.setVendor(null);
						customer.setAddresses(null);
						count++;
						result.add(customer);
					}
				}

			} else {
				// response.put(count, "failed");
			}
		} catch (Exception e) {
			LOGGER.error("error: " + e.getMessage());
			MailSendUtil.sendExceptionByMail(e,environment);
		}

		return result;
	}
	public String getNotificationJSONForAPK(int orderId, String deviceId, String locationUid, String merchantPosId){
		NotificationVO response = new NotificationVO();
		Gson gson = new Gson();
		LOGGER.info("OrderServiceImpl : getNotificationJSONForAPK :orderRId : " + orderId);
		LOGGER.info("OrderServiceImpl : getNotificationJSONForAPK :deviceId : " + deviceId);
		LOGGER.info("OrderServiceImpl : getNotificationJSONForAPK :locationUid : " + locationUid);
		LOGGER.info("OrderServiceImpl : getNotificationJSONForAPK :merchantPosId : " + merchantPosId);
		try{
		LOGGER.info("----------------Start :: OrderServiceImpl : getNotificationJSONForAPK------------------");
		OrderR orderR =  CommonUtil.filterOrderRForAppNotification(orderRepository.findOne(orderId));
		LOGGER.info("OrderServiceImpl : getNotificationJSONForAPK :orderRId : " + orderId);
		List<OrderItem> items = new ArrayList<OrderItem>();
		List<OrderItem> orderItems = orderItemRepository.findByOrderId(orderId);
		Customer customer = new Customer();
		if(orderR.getCustomer()!=null){
			customer.setFirstName(orderR.getCustomer().getFirstName());
			customer.setPhoneNumber(orderR.getCustomer().getPhoneNumber());
			customer.setEmailId(orderR.getCustomer().getEmailId());
			customer.setId(orderR.getCustomer().getId());
			List<Address> addresses = new ArrayList<Address>();
			Address addres = null;
			if (orderR.getAddressId() != null)
				addres = addressRepository.findOne(orderR.getAddressId());
			if (addres != null) {
				addresses.add(addres);
			} 
				
			Address address2 = new Address();
			if(addresses!=null && addresses.size()>0 && orderR.getOrderType()!=null && !orderR.getOrderType().equalsIgnoreCase("pickUp"))
			{
			for (Address address : addresses) {
				address2.setId(address.getId());
				address2.setAddress1(address.getAddress1());
				address2.setAddress2(address.getAddress2());
				address2.setAddress3(address.getAddress3());
				address2.setCity(address.getCity());
				address2.setState(address.getState());
				address2.setZip(address.getZip());
				break;
			}
			}else {
				address2.setAddress1("");
				address2.setAddress2("");
				address2.setAddress3("");
				address2.setCity("");
				address2.setState("");
				address2.setZip("");
			}
			addresses.clear();
			addresses.add(address2);
			customer.setAddresses(addresses);
			orderR.setCustomer(customer);
		}
		if (orderItems != null && orderItems.size() > 0) {
			LOGGER.info("OrderServiceImpl : getNotificationJSONForAPK :orderItems.size : " + orderItems.size());
			for (OrderItem orderItem : orderItems) {

				if (orderItem.getItem() != null) {
					OrderItem orderItem2=new OrderItem();
					orderItem.setOrder(null);
					Item item = new Item();
					item.setId(orderItem.getItem().getId());
					item.setName(orderItem.getItem().getName());
					item.setPrice(orderItem.getItem().getPrice());
					orderItem2.setItem(item);
					List<OrderItemModifier> orderItemModifiers = orderItemModifierRepository
							.findByOrderItemId(orderItem.getId());
					for (OrderItemModifier itemModifier : orderItemModifiers) {
						itemModifier.setOrderItem(null);
						if (itemModifier.getModifiers() != null) {
							Modifiers modifiers=new Modifiers();
							modifiers.setId(itemModifier.getModifiers().getId());
							modifiers.setName(itemModifier.getModifiers().getName());
							modifiers.setPrice(itemModifier.getModifiers().getPrice());
							itemModifier.setModifiers(modifiers);
						}

					}
					
					orderItem2.setId(orderItem.getId());
					orderItem2.setQuantity(orderItem.getQuantity());
					
					orderItem2.setOrderItemModifiers(orderItemModifiers);
					items.add(orderItem2);
				}

			}
			orderR.setOrderItems(items);
		}

		List<OrderPizza> orderPizzas = orderPizzaRepository.findByOrderId(orderId);
		if (!orderPizzas.isEmpty() && orderPizzas.size() > 0) {
			LOGGER.info("OrderServiceImpl : getNotificationJSONForAPK :orderPizzas.size : " + orderPizzas.size());
			for (OrderPizza orderPizza : orderPizzas) {
				OrderPizza orderPizza2=new OrderPizza();
				Item item = new Item();
				OrderItem orderItem = new OrderItem();

				if (orderPizza != null) {
					LOGGER.info(" === orderPizza : " + orderPizza.getId());
					orderPizza.setOrder(null);
					PizzaTemplate pizzaTemplate = new PizzaTemplate();
					if (orderPizza.getPizzaTemplate()!=null) {
						pizzaTemplate.setId(orderPizza.getPizzaTemplate().getId());
						pizzaTemplate.setName(orderPizza.getPizzaTemplate().getName());
						pizzaTemplate.setDescription(orderPizza.getPizzaTemplate().getDescription());
						orderPizza2.setPizzaTemplate(pizzaTemplate);
					}
					
					if (pizzaTemplate != null) {
						item.setId(pizzaTemplate.getId());
					}
					item.setPrice(orderPizza.getPrice());
					orderItem.setQuantity(orderPizza.getQuantity());

					PizzaSize pizzaSize = pizzaSizeRepository.findById(orderPizza.getPizzaSize().getId());
					if (pizzaSize != null) {
						pizzaSize.setCategories(null);
						pizzaSize.setMerchant(null);
						pizzaSize.setPizzaCrustSizes(null);
						pizzaSize.setPizzaTemplates(null);
						pizzaSize.setPizzaToppingSizes(null);
						if (pizzaTemplate != null)
							item.setName(pizzaTemplate.getDescription() + "(" + pizzaSize.getDescription() + ")");
					}

					orderItem.setItem(item);
					List<OrderPizzaToppings> orderPizzaToppings = orderPizzaToppingsRepository
							.findByOrderPizzaId(orderPizza.getId());
					List<OrderItemModifier> itemModifiersList = new ArrayList<OrderItemModifier>();
					for (OrderPizzaToppings orderPizzaTopping : orderPizzaToppings) {
						orderPizzaTopping.setOrderPizza(null);
						if (orderPizzaTopping.getPizzaTopping() != null) {
							orderPizzaTopping.getPizzaTopping().setMerchant(null);
							orderPizzaTopping.getPizzaTopping().setPizzaTemplateToppings(null);
							orderPizzaTopping.getPizzaTopping().setPizzaToppingSizes(null);
							orderPizzaTopping.getPizzaTopping().setPizzaTemplateTopping(null);

							OrderItemModifier orderItemModifier = new OrderItemModifier();

							Modifiers modifiers = new Modifiers();

							if (orderPizzaTopping.getSide1()) {
								modifiers.setName(
										orderPizzaTopping.getPizzaTopping().getDescription() + " (First Half)");
							} else if (orderPizzaTopping.getSide2()) {
								modifiers.setName(
										orderPizzaTopping.getPizzaTopping().getDescription() + " (Second Half)");
							} else {
								modifiers.setName(orderPizzaTopping.getPizzaTopping().getDescription() + " (Full)");
							}

							modifiers.setId(orderPizzaTopping.getPizzaTopping().getId());
							modifiers.setPrice(orderPizzaTopping.getPrice());
							modifiers.setModifierGroup(null);
							modifiers.setMerchant(null);

							orderItemModifier.setId(orderPizzaTopping.getId());
							orderItemModifier.setQuantity(orderPizzaTopping.getQuantity());
							orderItemModifier.setModifiers(modifiers);
							itemModifiersList.add(orderItemModifier);
						}
					}

					OrderPizzaCrust orderPizzaCrusts = orderPizzaCrustRepository.findByOrderPizzaId(orderPizza.getId());
					if (orderPizzaCrusts != null) {
						OrderItemModifier orderItemModifiers = new OrderItemModifier();

						Modifiers modifier = new Modifiers();
						modifier.setName(orderPizzaCrusts.getPizzacrust().getDescription());
						modifier.setId(orderPizzaCrusts.getPizzacrust().getId());
						modifier.setPrice(orderPizzaCrusts.getPrice());
						modifier.setModifierGroup(null);
						modifier.setMerchant(null);

						orderItemModifiers.setId(orderPizzaCrusts.getId());
						orderItemModifiers.setQuantity(orderPizzaCrusts.getQuantity());
						orderItemModifiers.setModifiers(modifier);
						itemModifiersList.add(orderItemModifiers);
					}
					orderItem.setOrderItemModifiers(itemModifiersList);
					items.add(orderItem);
				}
			}
			orderR.setOrderItems(items);
			orderR.setMerchant(null);
			orderR.setVirtualFund(null);
		}
		response.setDeviceId(deviceId);
		response.setLocationUid(locationUid);
		response.setMerchantId(merchantPosId);
		orderR.setMerchant(null);
		orderR.setVirtualFund(null);
		Payload payload = new Payload();
		payload.setOrderR(orderR);
		response.setPayload(payload);
		LOGGER.info("OrderServiceImpl : getNotificationJSONForAPK :response : " + response);
		String extraJson = gson.toJson(response);
		LOGGER.info("OrderServiceImpl : getNotificationJSONForAPK :extraJson : " + extraJson);
		return extraJson;
	}catch (Exception e) {
		LOGGER.info("OrderServiceImpl : getNotificationJSONForAPK :Exception : " + e);
	}
		return gson.toJson(response);
	}
	
	public String getAvgTimeForOrder(Merchant merchant, OrderR orderR) {
		String avgTime = "45";
		PickUpTime pickUpTime = null;
		Zone zone = null;

		if (merchant != null) {
			try {
				if ("pickup".equals(orderR.getOrderType().toLowerCase())) {
					pickUpTime = pickUpTimeRepository.findByMerchantId(merchant.getId());
					Integer.parseInt(pickUpTime.getPickUpTime());
					avgTime = (pickUpTime != null) ? pickUpTime.getPickUpTime() : "30";
				} else if (orderR.getZoneId() != null) {

					zone = zoneRepository.findById(orderR.getZoneId());
					Integer.parseInt(zone.getAvgDeliveryTime());
					avgTime = (zone != null && zone.getAvgDeliveryTime() != null) ? zone.getAvgDeliveryTime() : "30";
					LOGGER.info("OrderServiceImpl :: updateOrderStatus : zoneId " + orderR.getZoneId());
				}
			} catch (NumberFormatException e) {
				avgTime = "45";
				LOGGER.error("OrderServiceImpl :: savePizzaOrder : Exception " + e);
			}
		}
		return avgTime;
	}
	public String getOrdersByPosIdAndStatus(String merchantPosId, Integer isDefault,String timeZoneCode) {

		List<Map<String, Map<String, String>>> orderList = new ArrayList<Map<String,Map<String,String>>>();
		LOGGER.info("===============  orderServiceImpl : Inside getOrdersByPosIdAndStatus started :: ");

		Gson gson = new Gson();
		String notificationJson = "";
		try {
			if (merchantPosId != null) {

				Date date=DateUtil.getCurrentDateForTimeZonee(timeZoneCode);
				
				List<OrderR> dbOrderList = orderRepository.findbyMerchantPosIdAndIsdefault(DateUtil.getYYYYMMDD(date),merchantPosId, isDefault);
				for (OrderR orderR : dbOrderList) {

					String orderDetails = "";
					
					Map<String, String> notification = new HashMap<String, String>();
					
					Map<String, Map<String, String>> notf = new HashMap<String, Map<String, String>>();
					List<Map<String, String>> productList = new ArrayList<Map<String, String>>();
					
					Map<String, String> order = new HashMap<String, String>();
					order.put("total", Double.toString(orderR.getOrderPrice()));
					order.put("tax", orderR.getTax());
					
					notification.put("appEvent", "notification");

					order.put("total", Double.toString(orderR.getOrderPrice()));
					order.put("tax", orderR.getTax());

					List<Map<String, String>> extraList = null;
					Map<String, String> poduct = null;
					Map<String, String> exctra = null;

					List<OrderItem> orderItems = orderItemRepository.findByOrderId(orderR.getId());
					for (OrderItem orderItem : orderItems) {
						poduct = new HashMap<String, String>();
						if (null != orderItem) {
							if (null != orderItem.getItem()) {

								String items = "<tr style='font-weight:600;text-transform:capitalize;font-family:arial'><td width='200px;'>"
										+ orderItem.getItem().getName()
										+ "</td><td width='100px;' style='text-align:center'>" + orderItem.getQuantity()
										+ "</td><td width='100px;' style='text-align:center'>" + "$"
										+ String.format("%.2f", orderItem.getItem().getPrice()) + "</td></tr>";
								orderDetails = orderDetails + "" + "<b>" + items + "</b>";

								poduct.put("product_id", orderItem.getItem().getPosItemId());
								poduct.put("name", orderItem.getItem().getName());
								poduct.put("price", Double.toString(orderItem.getItem().getPrice()));
								poduct.put("qty", Integer.toString(orderItem.getQuantity()));

								extraList = new ArrayList<Map<String, String>>();
								List<OrderItemModifier> itemModifiers = itemModifierRepository
										.findByOrderItemId(orderItem.getId());
								String itemModifierName = "";
								if (itemModifiers.size() > 0) {
									for (OrderItemModifier itemModifier : itemModifiers) {
										exctra = new HashMap<String, String>();

										if (itemModifier != null && itemModifier.getModifiers() != null
												&& itemModifier.getModifiers().getName() != null) {
											itemModifierName = itemModifier.getModifiers().getName();
										}

										String modifiers = "<tr style='text-transform:capitalize;font-family:arial;margin-top:5px;font-size:12px'><td width='200px;'>"
												+ itemModifierName
												+ "</td><td width='100px;' style='text-align:center'>"
												+ itemModifier.getQuantity()
												+ " </td><td width='100px;' style='text-align:center'> " + "$"
												+ String.format("%.2f", itemModifier.getModifiers().getPrice())
												+ "</td></tr>";
										orderDetails = orderDetails + "" + modifiers;
										exctra.put("id", itemModifier.getModifiers().getPosModifierId());
										exctra.put("price", Double.toString(itemModifier.getModifiers().getPrice()));
										exctra.put("name", itemModifier.getModifiers().getName());
										exctra.put("qty", Integer.toString(orderItem.getQuantity()));
										extraList.add(exctra);
									}
								}

								
								if (extraList.size() > 0) {
									String extraJson = gson.toJson(extraList);
									poduct.put("extras", extraJson);
									productList.add(poduct);
								} else {
									productList.add(poduct);
								}
							}
						}
					}
					
					String productJson = gson.toJson(productList);
					LOGGER.error("OrderServiceImpl :: updateOrderStatus :  notificationJson : " + productJson);
					LOGGER.info("notificationJson=" + productJson);

					order.put("productItems", productJson);
					// order.put("productItems", productList.toString());
					String orderJson = gson.toJson(order);
					
					notification.put("payload", orderR.getOrderPosId() + "@#You got a new order(" + orderR.getOrderPosId() + ") at "
							+ orderR.getCreatedOn() + "@# $" + orderR.getOrderPrice() + "@# " + orderR.getOrderType() + "@#"
							+ orderJson + "@#" + orderR.getMerchant().getPosMerchantId() + "@#"
							+ orderR.getOrderNote() + " @#" + orderR.getPosPaymentId() + "@#"
							+ (orderR.getIsFutureOrder() != null && orderR.getIsFutureOrder() == 1)
							+ "@#" + orderR.getFulfilled_on() + " @#" + orderR.getPaymentMethod());

					// +"@#"+tip add this to notification payload if we add
					// tip amount to show on popup on
					// notification app side
					notf.put("notification", notification);
					orderList.add(notf);
					
					
				}
				
			    notificationJson = gson.toJson(orderList);
				notificationJson = notificationJson.trim().replaceAll("\\\\+\"", "'").replace("'[", "[")
						.replace("]'", "]");
				
				LOGGER.info("notificationJson : "+notificationJson);

			}

		} catch (Exception e) {
			LOGGER.info("Exception in getOrdersByPosIdAndStatus : " + e);
		}
		return notificationJson;
	}
	
	public void updatePrintJobStatus(Integer orderId, boolean printStatus) {
		OrderPrintStatus OrderPrint = orderPrintStatusRepo.findByOrderId(orderId);
			if (OrderPrint == null) {
				OrderPrint = new OrderPrintStatus();
				OrderPrint.setOrderId(orderId);
			}
				OrderPrint.setPrintStatus(printStatus);
				orderPrintStatusRepo.save(OrderPrint);
     
	}
	public void storeOrderIntoFreekwent(OrderR orderR,String merchantUid)
	{
		LOGGER.info("inside  storeOrderIntoFreekwent : start" );
		try{
			if(orderR!=null && merchantUid!=null&& orderR.getCustomer()!=null)
			{
				String customerOrderDetailJson;
				JSONObject json = new JSONObject();
				json.put("locationUid", merchantUid);
				json.put("mobileNumber", orderR.getCustomer().getPhoneNumber());
				json.put("amount", orderR.getOrderPrice());
				json.put("emailId",orderR.getCustomer().getEmailId());
        		json.put("firstName",orderR.getCustomer().getFirstName());
        		json.put("lastName",orderR.getCustomer().getLastName());
        		json.put("orderType","online");
				customerOrderDetailJson=json.toString();
				LOGGER.info("inside  storeOrderIntoFreekwent : customerOrderDetailJson ::"+customerOrderDetailJson );
				ProducerUtil.callFreekWentApi(customerOrderDetailJson,environment);
			}
		
		} catch (Exception e) {
			LOGGER.info("Exception in storeOrderIntoFreekwent : " + e);
			MailSendUtil.sendExceptionByMail(e, environment);
		}
		LOGGER.info("inside  storeOrderIntoFreekwent : end" );
	}
}
