package com.foodkonnekt.serviceImpl;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.servlet.http.HttpSession;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import com.foodkonnekt.model.Category;
import com.foodkonnekt.model.CategoryItem;
import com.foodkonnekt.model.Customer;
import com.foodkonnekt.model.ExcelWorkbook;
import com.foodkonnekt.model.ExcelWorksheet;
import com.foodkonnekt.model.Item;
import com.foodkonnekt.model.ItemModifierGroup;
import com.foodkonnekt.model.ItemModifiers;
import com.foodkonnekt.model.Merchant;
import com.foodkonnekt.model.ModifierGroup;
import com.foodkonnekt.model.Modifiers;
import com.foodkonnekt.model.OrderR;
import com.foodkonnekt.model.PizzaCrust;
import com.foodkonnekt.model.PizzaCrustSizes;
import com.foodkonnekt.model.PizzaSize;
import com.foodkonnekt.model.PizzaTemplate;
import com.foodkonnekt.model.PizzaTemplateCategory;
import com.foodkonnekt.model.PizzaTemplateCrust;
import com.foodkonnekt.model.PizzaTemplateSize;
import com.foodkonnekt.model.PizzaTemplateTopping;
import com.foodkonnekt.model.PizzaTopping;
import com.foodkonnekt.model.PizzaToppingSize;
import com.foodkonnekt.model.TaxRates;
import com.foodkonnekt.repository.CategoryItemRepository;
import com.foodkonnekt.repository.CategoryRepository;
import com.foodkonnekt.repository.CustomerrRepository;
import com.foodkonnekt.repository.ItemModifierGroupRepository;
import com.foodkonnekt.repository.ItemModifiersRepository;
import com.foodkonnekt.repository.ItemmRepository;
import com.foodkonnekt.repository.ModifierGroupRepository;
import com.foodkonnekt.repository.ModifierModifierGroupRepository;
import com.foodkonnekt.repository.ModifiersRepository;
import com.foodkonnekt.repository.OrderRepository;
import com.foodkonnekt.repository.PizzaCrustRepository;
import com.foodkonnekt.repository.PizzaCrustSizeRepository;
import com.foodkonnekt.repository.PizzaSizeRepository;
import com.foodkonnekt.repository.PizzaTemplateCategoryRepository;
import com.foodkonnekt.repository.PizzaTemplateCrustRepository;
import com.foodkonnekt.repository.PizzaTemplateRepository;
import com.foodkonnekt.repository.PizzaTemplateSizeRepository;
import com.foodkonnekt.repository.PizzaTemplateToppingRepository;
import com.foodkonnekt.repository.PizzaToppingRepository;
import com.foodkonnekt.repository.PizzaToppingSizeRepository;
import com.foodkonnekt.repository.TaxRateRepository;
import com.foodkonnekt.service.ImportExcelService;
import com.foodkonnekt.util.DateUtil;
import com.foodkonnekt.util.IConstant;
import com.foodkonnekt.util.MailSendUtil;
import com.foodkonnekt.util.ProducerUtil;

@Service
public class ImportExcelServiceImpl implements ImportExcelService {

	private static final Logger LOGGER= LoggerFactory.getLogger(ImportExcelServiceImpl.class);
	
	public ExcelToJsonConverterConfig config = null;

	@Autowired
    private Environment environment;
	
	@Autowired
	private TaxRateRepository taxRateRepository;

	@Autowired
	private ModifierGroupRepository modifierGroupRepository;

	@Autowired
	private ItemmRepository itemmRepository;

	@Autowired
	private ItemModifierGroupRepository itemModifierGroupRepository;

	@Autowired
	private ModifierModifierGroupRepository modifierModifierGroupRepository;

	@Autowired
	private ItemModifiersRepository itemModifiersRepository;

	@Autowired
	private ModifiersRepository modifiersRepository;

	@Autowired
	private CategoryRepository categoryRepository;

	@Autowired
	private CategoryItemRepository categoryItemRepository;

	@Autowired
	private PizzaSizeRepository pizzaSizeRepository;

	@Autowired
	private PizzaTemplateRepository pizzaTemplateRepository;

	@Autowired
	private PizzaToppingRepository pizzaToppingRepository;

	@Autowired
	PizzaTemplateSizeRepository pizzaTemplateSizeRepository;

	@Autowired
	private PizzaTemplateCategoryRepository pizzaTemplateCategoryRepository;

	@Autowired
	private PizzaToppingSizeRepository pizzaToppingSizeRepository;
	
	@Autowired
	private PizzaTemplateToppingRepository pizzaTemplateToppingRepository;
	
	@Autowired
	private PizzaCrustRepository pizzaCrustRepository;
	
	@Autowired
	private PizzaTemplateCrustRepository pizzaTemplateCrustRepository;
	
	@Autowired
	private PizzaCrustSizeRepository pizzaCrustSizeRepository;
	
	@Autowired
	private OrderRepository orderRepository;
	
	@Autowired
	private CustomerrRepository customerrRepository;
	
	@Autowired
	private MailSendUtil mailSendUtil;
	
	/* @Override */
	/*
	 * public String saveInventoryByExcel(ExcelToJsonConverterConfig
	 * config,Merchant merchant) throws InvalidFormatException, IOException {
	 * 
	 * String taxRateSheetColumns[]={"Name","Tax Rate","Default"}; int
	 * noOfTaxRateSheetColumns=3; String
	 * modifierGroupsSheetColumns[]={"Modifier Group Name","Modifier","Price"};
	 * int noOfModifierGroupsSheetColumns=3; String
	 * itemsSheetColumns[]={"Name","Price","Price Type","Tax Rates"
	 * ,"Modifier Groups","Hidden"}; int noOfItemsSheetColumns=6; String
	 * categoriesSheetColumns[]={"Category Name","Items in Category"}; int
	 * noOfCategoriesSheetColumns=2;
	 * 
	 * String response=""; this.config=config;
	 * 
	 * ExcelWorkbook book = new ExcelWorkbook();
	 * 
	 * InputStream inp = new FileInputStream(config.getSourceFile()); Workbook
	 * wb = WorkbookFactory.create(inp);
	 * 
	 * book.setFileName(config.getSourceFile());
	 * 
	 * int loopLimit = wb.getNumberOfSheets();
	 * if(loopLimit==config.getNumberOfSheets()){ if (config.getNumberOfSheets()
	 * > 0 && loopLimit > config.getNumberOfSheets()) { loopLimit =
	 * config.getNumberOfSheets(); } int
	 * taxRateSheetIndex=wb.getSheetIndex("Tax Rates"); int
	 * modifierGroupsSheetIndex=wb.getSheetIndex("Modifier Groups"); int
	 * itemsSheetIndex=wb.getSheetIndex("Items"); int
	 * categoriesSheetIndex=wb.getSheetIndex("Categories");
	 * 
	 * if(taxRateSheetIndex==0&&modifierGroupsSheetIndex==1&&itemsSheetIndex==2&
	 * &categoriesSheetIndex==3){
	 * 
	 * for (int i = 0; i < loopLimit; i++) { Sheet sheet = wb.getSheetAt(i);
	 * 
	 * 
	 * 
	 * if (sheet == null) { continue; } ExcelWorksheet tmp = new
	 * ExcelWorksheet(); tmp.setName(sheet.getSheetName());
	 * 
	 * if(sheet.getSheetName().equals("Tax Rates")){ boolean status=
	 * validateColumns(sheet,noOfTaxRateSheetColumns,taxRateSheetColumns);
	 * if(status){ ArrayList<TaxRates> taxRates = setTaxRates(sheet,merchant);
	 * taxRateRepository.save(taxRates); }else{
	 * response="Tax Rates sheet: Please make sure you have the following headers in the following order: Name,Tax Rate, Default"
	 * ; break; }
	 * 
	 * } if(sheet.getSheetName().equals("Modifier Groups")){ boolean status=
	 * validateColumns(sheet,noOfModifierGroupsSheetColumns,
	 * modifierGroupsSheetColumns); if(status){ ArrayList<ModifierGroup>
	 * modifierGroups =setModifierGroups(sheet,merchant);
	 * modifierGroupRepository.save(modifierGroups); }else{
	 * response="Modifier Groups sheet: Please make sure you have the following headers in the following order: Modifier Group Name,Modifier,Price"
	 * ; break; }
	 * 
	 * } if(sheet.getSheetName().equals("Items")){ boolean status=
	 * validateColumns(sheet,noOfItemsSheetColumns,itemsSheetColumns);
	 * if(status){ ArrayList<Item> items =setLineItems(sheet,merchant);
	 * itemmRepository.save(items); for(Item item:items){
	 * itemModifierGroupRepository.save(item.getItemModifierGroups());
	 * List<ItemModifierGroup> itemModifierGroups=item.getItemModifierGroups();
	 * for(ItemModifierGroup itemModifierGroup:itemModifierGroups){
	 * 
	 * ModifierGroup modifierGroup=itemModifierGroup.getModifierGroup();
	 * List<Modifiers>
	 * modifiers=modifierModifierGroupRepository.findByModifierGroupId(
	 * modifierGroup.getId()); for(Modifiers modifier:modifiers){ ItemModifiers
	 * itemModifiers=new ItemModifiers(); itemModifiers.setItem(item);
	 * itemModifiers.setModifierGroup(modifierGroup);
	 * itemModifiers.setModifiers(modifier);
	 * itemModifiers.setModifierStatus(IConstant.BOOLEAN_TRUE);
	 * itemModifiersRepository.save(itemModifiers); } } } }else{
	 * response="Items sheet: Please make sure you have the following headers in the following order: Name,Price,Price Type,Tax Rates,Modifier Groups,Hidden"
	 * ; break; }
	 * 
	 * 
	 * } if(sheet.getSheetName().equals("Categories")){
	 * 
	 * boolean status=
	 * validateColumns(sheet,noOfCategoriesSheetColumns,categoriesSheetColumns);
	 * if(status){ List<Category> categories=setCategories(sheet,merchant);
	 * for(Category category:categories){ Set<Item> items=category.getItems();
	 * category.setItems(null); categoryRepository.save(category); for(Item
	 * item:items){ CategoryItem categoryItem = new CategoryItem();
	 * categoryItem.setCategory(category); categoryItem.setItem(item);
	 * categoryItemRepository.save(categoryItem); } }
	 * response="All the inventory imported successfully."; }else{
	 * response="Categories sheet: Please make sure you have the following headers in the following order: Category Name,Items in Category"
	 * ; break; }
	 * 
	 * }
	 * 
	 * book.addExcelWorksheet(tmp); }
	 * 
	 * }else{
	 * response="You must have four sheets in the following order: Tax Rates,Modifier Groups, Items, Categories "
	 * ; } }else{
	 * response="You must have four sheets in the following order: Tax Rates,Modifier Groups, Items, Categories "
	 * ; } System.out.println(book.toJson(true)); return response;
	 * 
	 * }
	 */

	public boolean validateColumns(Sheet sheet, int noOfColumns, String columnArray[]) {
		LOGGER.info("----------------Start :: ImportExcelServiceImpl : validateColumns------------------------");

		boolean status = false;
		System.out.println(sheet.getSheetName());
		LOGGER.info(": ImportExcelServiceImpl : validateColumns :sheetName:"+sheet.getSheetName());

		Row firstRow = sheet.getRow(0);
		System.out.println(firstRow.getLastCellNum());
		LOGGER.info(": ImportExcelServiceImpl : validateColumns :lastCellName:"+firstRow.getLastCellNum());

		if (firstRow != null && firstRow.getLastCellNum() == noOfColumns) {
			for (int k = 0; k < firstRow.getLastCellNum(); k++) {

				Cell cell = firstRow.getCell(k);
				if (cell != null) {
					String value = (String) cellToObject(cell);
					if (value.equals(columnArray[k])) {
						status = true;
					} else {
						status = false;
						break;
					}
				} else {
					status = false;
					break;
				}
			}

		} else {
			status = false;
		}
		LOGGER.info(" End :: ImportExcelServiceImpl : validateColumns :return Status:"+status);

		return status;
	}

	public Object cellToObject(Cell cell) {
		LOGGER.info("----------------Start :: ImportExcelServiceImpl : cellToObject------------------------");

		int type = cell.getCellType();
		LOGGER.info(": ImportExcelServiceImpl : cellToObject : cellType:"+type);

		if (type == Cell.CELL_TYPE_STRING) {
			LOGGER.info("----------------End :: ImportExcelServiceImpl : cellToObject------------------------");

			return cleanString(cell.getStringCellValue());
		}

		if (type == Cell.CELL_TYPE_BOOLEAN) {
			LOGGER.info("----------------End :: ImportExcelServiceImpl : cellToObject------------------------");

			return cell.getBooleanCellValue();
		}

		if (type == Cell.CELL_TYPE_NUMERIC) {

			if (cell.getCellStyle().getDataFormatString().contains("%")) {
				LOGGER.info("----------------End :: ImportExcelServiceImpl : cellToObject------------------------");

				return cell.getNumericCellValue() * 100;
			}
			LOGGER.info("----------------End :: ImportExcelServiceImpl : cellToObject------------------------");

			return numeric(cell);
		}

		if (type == Cell.CELL_TYPE_FORMULA) {
			switch (cell.getCachedFormulaResultType()) {
			case Cell.CELL_TYPE_NUMERIC:
				LOGGER.info("----------------End :: ImportExcelServiceImpl : cellToObject------------------------");

				return numeric(cell);
			case Cell.CELL_TYPE_STRING:
				LOGGER.info("----------------End :: ImportExcelServiceImpl : cellToObject------------------------");

				return cleanString(cell.getRichStringCellValue().toString());
			}
		}
		LOGGER.info("----------------End :: ImportExcelServiceImpl : cellToObject : return null------------------------");

		return null;

	}

	public String cleanString(String str) {
		LOGGER.info("----------------Start :: ImportExcelServiceImpl : cleanString :------------------------");
		LOGGER.info("ImportExcelServiceImpl : cleanString : String :"+str);
		
		LOGGER.info("----------------End :: ImportExcelServiceImpl : cleanString :------------------------");

		return str.replace("\n", "").replace("\r", "");
	}

	public Object numeric(Cell cell) {
		LOGGER.info("----------------Start :: ImportExcelServiceImpl : numeric :------------------------");

		if (HSSFDateUtil.isCellDateFormatted(cell)) {
			if (config.getFormatDate() != null) {
				LOGGER.info("----------------End :: ImportExcelServiceImpl : numeric :------------------------");

				return config.getFormatDate().format(cell.getDateCellValue());
			}
			LOGGER.info("----------------End :: ImportExcelServiceImpl : numeric :------------------------");

			return cell.getDateCellValue();
		}
		LOGGER.info("----------------End :: ImportExcelServiceImpl : numeric :------------------------");

		return Double.valueOf(cell.getNumericCellValue());
	}

	public ArrayList<ModifierGroup> setModifierGroups(Sheet sheet, Merchant merchant, HttpSession session) {
		LOGGER.info("----------------Start :: ImportExcelServiceImpl : setModifierGroups :------------------------");

		ArrayList<ModifierGroup> modifierGroups = new ArrayList<ModifierGroup>();
		Integer modifierGroupCount = 0;
		ModifierGroup duplicatModifierGroup = null;
		Set<Modifiers> modifiers = null;
		LOGGER.info("ImportExcelServiceImpl : setModifierGroups :cellLastName:"+sheet.getRow(0).getLastCellNum());

		if (sheet.getRow(0).getLastCellNum() == 3) {
			for (int j = 1; j <= sheet.getLastRowNum(); j++) {
				boolean isDuplicateModifierGroup = true;
				ModifierGroup modifierGroup = duplicatModifierGroup;

				Row row = sheet.getRow(j);
				if (row == null) {
					continue;
				}
				if (row.getCell(0) != null) {
					modifierGroupCount++;
				}
				boolean hasValues = false;
				Modifiers modifier = new Modifiers();
				modifier.setMerchant(merchant);

				for (int k = 0; k <= row.getLastCellNum(); k++) {
					Cell cell = row.getCell(k);
					if (cell != null) {
						Object value = cellToObject(cell);

						hasValues = hasValues || value != null;
						switch (k) {
						case 0:
							if (value != null) {
								String modifierGroupName = (String) value;
								LOGGER.info("ImportExcelServiceImpl : setModifierGroups :merchantId:"+merchant.getId()+":modifierGroupName:"+modifierGroupName);

								List<ModifierGroup> groups = modifierGroupRepository
										.findByMerchantIdAndName(merchant.getId(), modifierGroupName);
								LOGGER.info("ImportExcelServiceImpl : setModifierGroups :ModifierGroupSize:"+groups.size());

								if (groups != null && groups.size() > 0) {
									for (ModifierGroup group : groups) {
										modifierGroup = group;
										break;
									}
								} else {
									modifierGroup = new ModifierGroup();
									modifierGroup.setMerchant(merchant);
									modifierGroup.setName((String) value);
									modifierGroup.setShowByDefault(IConstant.BOOLEAN_TRUE);
									modifierGroup.setActive(IConstant.BOOLEAN_TRUE);
								}
								modifierGroup.setActive(1);
								duplicatModifierGroup = modifierGroup;
								modifiers = new HashSet<Modifiers>();
								isDuplicateModifierGroup = false;
							} else {
								isDuplicateModifierGroup = true;
							}
							break;

						case 1:
							if (value != null) {
								String modifierName = String.valueOf(value);
								LOGGER.info("ImportExcelServiceImpl : setModifierGroups :merchantId:"+merchant.getId()+":modifierGroupName:"+modifierName);
	
								List<Modifiers> modifierList = modifiersRepository
										.findByMerchantIdAndName(merchant.getId(), modifierName);
								LOGGER.info("ImportExcelServiceImpl : setModifierGroups :ModifiersSize:"+ modifierList.size());

								if (modifierList != null && modifierList.size() > 0) {
									for (Modifiers modi : modifierList) {
										modifier = modi;
										break;
									}
								} else {
									modifier.setName(String.valueOf(value));
									modifier.setStatus(IConstant.BOOLEAN_TRUE);
								}
							}
							break;
						case 2:
							modifier.setPrice((Double) value);

						}
					} else {
					}
				}
				modifier.setStatus(1);
				modifiersRepository.save(modifier);
				LOGGER.info("ImportExcelServiceImpl : setModifierGroups :save modifier ");

				session.setAttribute("excelSheetModifier", modifier);
				if(modifiers != null)
				modifiers.add(modifier);
				/*
				 * if(hasValues||!config.isOmitEmpty()) { currentRowOffset++; if
				 * (rowLimit > 0 && totalRowsAdded == rowLimit) { break; } if
				 * (startRowOffset > 0 && currentRowOffset < startRowOffset) {
				 * continue; } //tmp.addRow(rowData); totalRowsAdded++; }
				 */
				if (!isDuplicateModifierGroup) {
					modifierGroup.setModifiers(modifiers);
					modifierGroup.setModifierGroupCountForExcelSheet(modifierGroupCount);
					modifierGroups.add(modifierGroup);
				}
			}
		} else {
			LOGGER.info("ImportExcelServiceImpl : setModifierGroups :lastRowNameSize:"+sheet.getLastRowNum());

			for (int j = 1; j <= sheet.getLastRowNum(); j++) {
				boolean isDuplicateModifierGroup = true;
				ModifierGroup modifierGroup = duplicatModifierGroup;

				Row row = sheet.getRow(j);
				if (row == null) {
					continue;
				}
				if (row.getCell(0) != null) {
					modifierGroupCount++;
				}
				boolean hasValues = false;
				Modifiers modifier = new Modifiers();
				modifier.setMerchant(merchant);

				for (int k = 0; k <= row.getLastCellNum(); k++) {
					Cell cell = row.getCell(k);
					if (cell != null) {
						Object value = cellToObject(cell);

						hasValues = hasValues || value != null;
						switch (k) {
						case 0:
							if (value != null) {
								String modifierGroupId = Double.toString((Double) value);
								LOGGER.info("ImportExcelServiceImpl : setModifierGroups :modifierGroupId:"+modifierGroupId+":merchantId:"+merchant.getId());

								modifierGroup = modifierGroupRepository
										.findByPosModifierGroupIdAndMerchantId(modifierGroupId, merchant.getId());
								if (duplicatModifierGroup == null) {
									modifierGroup = new ModifierGroup();
								}
								modifierGroup.setMerchant(merchant);
								modifierGroup.setPosModifierGroupId(modifierGroupId);
								modifierGroup.setShowByDefault(IConstant.BOOLEAN_TRUE);
								modifierGroup.setActive(1);
								duplicatModifierGroup = modifierGroup;
								modifiers = new HashSet<Modifiers>();
								isDuplicateModifierGroup = false;
							} else {
								isDuplicateModifierGroup = true;
							}
							break;

						case 1:
							if (value != null) {
								String modifierGroupName = (String) value;
								LOGGER.info("ImportExcelServiceImpl : setModifierGroups :modifierGroupName:"+modifierGroupName+":merchantId:"+merchant.getId());

								List<ModifierGroup> groups = modifierGroupRepository
										.findByMerchantIdAndName(merchant.getId(), modifierGroupName);
								LOGGER.info("ImportExcelServiceImpl : setModifierGroups :modifierGroupSize:"+groups.size());

								if (groups != null && groups.size() > 0) {
									for (ModifierGroup group : groups) {
										modifierGroup = group;
										break;
									}
								} else {
									if (duplicatModifierGroup == null) {
										modifierGroup = new ModifierGroup();
									}
									modifierGroup.setMerchant(merchant);
									modifierGroup.setName((String) value);
									modifierGroup.setShowByDefault(IConstant.BOOLEAN_TRUE);
								}
								duplicatModifierGroup = modifierGroup;
								modifiers = new HashSet<Modifiers>();
								isDuplicateModifierGroup = false;
							} else {
								isDuplicateModifierGroup = true;
							}
							break;
						case 2:
							if (value != null) {
								String modifierId = (String) value;
								LOGGER.info("ImportExcelServiceImpl : setModifierGroups :modifierId:"+modifierId+":merchantId:"+merchant.getId());

								Modifiers availableModifier = modifiersRepository
										.findByPosModifierIdAndMerchantId(modifierId, merchant.getId());
								if (availableModifier != null) {
									modifier = availableModifier;
								} else {
									modifier.setPosModifierId((String) value);
								}
							}
							break;
						case 3:
							if (value != null) {
								String modifierName = (String) value;

								String id = modifier.getPosModifierId();
								LOGGER.info("ImportExcelServiceImpl : setModifierGroups :PosModifierId:"+id+":merchantId:"+merchant.getId());

								Modifiers modifiers2 = modifiersRepository.findByPosModifierIdAndMerchantId(id,
										merchant.getId());
								if (modifiers2 == null) {
									modifier.setName((String) value);
								} else {
									modifier = modifiers2;
									break;
								}
								break;
							}
						case 4:
							modifier.setPrice((Double) value);

						}
					} else {
					}
				}
				modifier.setStatus(1);
				modifiersRepository.save(modifier);
				session.setAttribute("excelSheetModifier", modifier);
				if(modifiers != null)
				modifiers.add(modifier);

				/*
				 * if(hasValues||!config.isOmitEmpty()) { currentRowOffset++; if
				 * (rowLimit > 0 && totalRowsAdded == rowLimit) { break; } if
				 * (startRowOffset > 0 && currentRowOffset < startRowOffset) {
				 * continue; } //tmp.addRow(rowData); totalRowsAdded++; }
				 */
				if (!isDuplicateModifierGroup) {
					modifierGroup.setModifiers(modifiers);
					modifierGroup.setModifierGroupCountForExcelSheet(modifierGroupCount);
					modifierGroups.add(modifierGroup);
					duplicatModifierGroup = null;
				}
			}
		}
		LOGGER.info("----------------End :: ImportExcelServiceImpl : setModifierGroups :-------------------");
		return modifierGroups;
	}

	public ArrayList<Item> setLineItems(Sheet sheet, Merchant merchant) {
		LOGGER.info("----------------Start :: ImportExcelServiceImpl : setLineItems :-------------------");

		Integer savedItemCounts = 0;
		// Integer totalItemCounts = 0;
		ArrayList<Item> items = new ArrayList<Item>();
		Item duplicateItem = null;
		Set<TaxRates> taxRates = null;
		List<ItemModifierGroup> itemModifierGroups = null;
		LOGGER.info(": ImportExcelServiceImpl : setLineItems : lastCellnub:"+sheet.getRow(0).getLastCellNum());

		if (sheet.getRow(0).getLastCellNum() == 7) {
			for (int j = 1; j <= sheet.getLastRowNum(); j++) {
				boolean isDuplicateItem = true;
				Item item = duplicateItem;

				Row row = sheet.getRow(j);
				if (row == null) {
					continue;
				}
				if (row.getCell(0) != null) {
					savedItemCounts++;
				}
				boolean hasValues = false;

				for (int k = 0; k <= row.getLastCellNum(); k++) {
					// if(row.getCell(1)!=null){

					Cell cell = row.getCell(k);
					if (cell != null) {
						Object value = cellToObject(cell);

						hasValues = hasValues || value != null;
						switch (k) {
						case 0:
							String itemName = null;
							if (value != null) {
								itemName = (String) value;
								LOGGER.info(": ImportExcelServiceImpl : setLineItems : merchantId:"+merchant.getId()+":itemName:"+itemName);

								List<Item> itms = itemmRepository.findByMerchantIdAndName(merchant.getId(), itemName);
								LOGGER.info(": ImportExcelServiceImpl : setLineItems : itmsListSize:"+itms.size());

								if (itms != null && itms.size() > 0) {
									for (Item itm : itms) {
										item = itm;
										break;
									}
								} else {
									itemName = (String) value;
									item = new Item();
									item.setName(itemName.trim());
									item.setMerchant(merchant);
									item.setItemStatus(IConstant.BOOLEAN_FALSE);
								}

								duplicateItem = item;
								taxRates = new HashSet<TaxRates>();
								itemModifierGroups = new ArrayList<ItemModifierGroup>();
								isDuplicateItem = false;
							}
							break;
						case 1:
							if (value != null) {
								item.setPrice((Double) value);
							}
							break;
						case 2:
							if (value != null) {
								item.setPriceType((String) value);
							}
							break;
						case 3:
							if (value != null) {
								String taxName = (String) value;
								if (taxName.toUpperCase().equals("DEFAULT")) {
									item.setIsDefaultTaxRates(true);
									List<TaxRates> detaulTaxList = taxRateRepository
											.findByMerchantIdAndIsDefault(merchant.getId(), 1);
									if (!detaulTaxList.isEmpty() && detaulTaxList != null) {
										Set<TaxRates> detaulTaxSet = new HashSet<TaxRates>(detaulTaxList);
										taxRates.addAll(detaulTaxSet);
									} else {
										TaxRates rates = new TaxRates();
										rates.setName(taxName);
										taxRates.add(rates);
									}
								} else {
									List<TaxRates> detaulTaxList = taxRateRepository
											.findByMerchantIdAndName(merchant.getId(), taxName);
									if (!detaulTaxList.isEmpty() && detaulTaxList != null) {
										Set<TaxRates> detaulTaxSet = new HashSet<TaxRates>(detaulTaxList);
										taxRates.addAll(detaulTaxSet);
									} else {
										TaxRates rates = new TaxRates();
										rates.setName(taxName);
										taxRates.add(rates);
									}
								}
							}
							break;

						case 4:
							if (value != null) {
								String modifierGroupName = (String) value;
								LOGGER.info(": ImportExcelServiceImpl : setLineItems : merchantId:"+merchant.getId()+":modifierGroupName:"+modifierGroupName);

								List<ModifierGroup> modifierGroup = modifierGroupRepository
										.findByMerchantIdAndName(merchant.getId(), modifierGroupName);
								if (modifierGroup != null && !modifierGroup.isEmpty()) {
									for (ModifierGroup mg : modifierGroup) {
										LOGGER.info(": ImportExcelServiceImpl : setLineItems : itemId:"+item.getId()+":ModifierGroupId:"+mg.getId() );

										if (item != null && item.getId() != null) {
											ItemModifierGroup itemModifierGroup = itemModifierGroupRepository
													.findByModifierGroupIdAndItemId(mg.getId(), item.getId());
											if (itemModifierGroup != null) {
												itemModifierGroups.add(itemModifierGroup);
												break;
											}
										} else {
											ItemModifierGroup itemModifierGroup = new ItemModifierGroup();
											itemModifierGroup.setItem(item);
											itemModifierGroup.setModifierGroup(mg);
											itemModifierGroup.setModifierGroupStatus(IConstant.BOOLEAN_TRUE);
											itemModifierGroups.add(itemModifierGroup);
											break;
										}
									}
								} else {
									ModifierGroup mg = new ModifierGroup();
									mg.setMerchant(merchant);
									mg.setName(modifierGroupName);

									ItemModifierGroup itemModifierGroup = new ItemModifierGroup();
									itemModifierGroup.setItem(item);
									itemModifierGroup.setModifierGroup(mg);
									itemModifierGroup.setModifierGroupStatus(IConstant.BOOLEAN_TRUE);
									itemModifierGroups.add(itemModifierGroup);
									break;
								}
							}
							break;
						case 5:
							if (value != null) {
								item.setIsHidden((Boolean) value);
							}
							break;

						case 6:
							if(value!=null){
								System.out.println("value :: "+value.toString());
								String description=value.toString();
								item.setDescription(description);
							}
							break;
						}
					} else {
					}
				}

				if (!isDuplicateItem) {
					item.setTaxes(taxRates);

					item.setItemModifierGroups(itemModifierGroups);
					item.setSavedItemCountForExcelSheet(savedItemCounts);
					// item.setTotalItemCountForExcelSheet(totalItemCounts);

					items.add(item);
				}
			}
		} else {
			for (int j = 1; j <= sheet.getLastRowNum(); j++) {
				boolean isDuplicateItem = true;
				Item item = duplicateItem;

				Row row = sheet.getRow(j);
				if (row == null) {
					continue;
				}
				if (row.getCell(0) != null) {
					savedItemCounts++;
				}
				boolean hasValues = false;
				for (int k = 0; k <= row.getLastCellNum(); k++) {
					// totalItemCounts++;
					// if(row.getCell(1)!=null){

					Cell cell = row.getCell(k);
					if (cell != null) {
						Object value = cellToObject(cell);

						hasValues = hasValues || value != null;
						switch (k) {

						case 0:
							if (value != null) {
								String posItemId = (String) value;
								/*
								 * if(value!=null){ posItemIdd=(String)value; }
								 */
								LOGGER.info(": ImportExcelServiceImpl : setLineItems : posItemId:"+posItemId+":merchantId:"+merchant.getId());

								Item itm = itemmRepository.findByPosItemIdAndMerchantId(posItemId, merchant.getId());
								if (itm != null) {
									item = itm;
								} else {
									if (duplicateItem == null) {
										item = new Item();
									}
									item.setOriginalPosItemId(posItemId);
									posItemId = posItemId.replaceAll("[^a-zA-Z0-9]", "");
									item.setPosItemId(posItemId);

									item.setMerchant(merchant);

									item.setItemStatus(IConstant.BOOLEAN_FALSE);
									duplicateItem = item;
									taxRates = new HashSet<TaxRates>();
									itemModifierGroups = new ArrayList<ItemModifierGroup>();
									isDuplicateItem = false;
								}
							} else {
								isDuplicateItem = true;

							}
							break;
						case 1:
							String itemName = null;
							if (value != null) {
								itemName = (String) value;
								LOGGER.info(": ImportExcelServiceImpl : setLineItems : itemName:"+itemName+":merchantId:"+merchant.getId());

								List<Item> itms = itemmRepository.findByMerchantIdAndName(merchant.getId(), itemName);
								if (itms != null && itms.size() > 0) {
									for (Item itm : itms) {
										item = itm;
										break;
									}
								} else {
									itemName = (String) value;
									if (duplicateItem == null) {
										item = new Item();
									}
									item.setName(itemName.trim());
									item.setMerchant(merchant);
									item.setItemStatus(IConstant.BOOLEAN_FALSE);
								}

								if (duplicateItem != null) {
									String posItemId = duplicateItem.getPosItemId();
									String originalPosItemId = duplicateItem.getOriginalPosItemId();
									if (posItemId != null) {
										item.setPosItemId(posItemId);
									}
									if (originalPosItemId != null) {
										item.setOriginalPosItemId(originalPosItemId);
									}
								}
								duplicateItem = item;
								taxRates = new HashSet<TaxRates>();
								itemModifierGroups = new ArrayList<ItemModifierGroup>();
								isDuplicateItem = false;
							}
							break;
						case 2:
							if (value != null) {
								item.setPrice((Double) value);
							}
							break;
						case 3:
							if (value != null) {
								item.setPriceType((String) value);
							}
							break;
						case 4:
							if (value != null) {
								String taxName = (String) value;
								if (taxName.toUpperCase().equals("DEFAULT")) {
									item.setIsDefaultTaxRates(true);
									LOGGER.info(": ImportExcelServiceImpl : setLineItems :merchantId:"+merchant.getId());

									List<TaxRates> detaulTaxList = taxRateRepository
											.findByMerchantIdAndIsDefault(merchant.getId(), 1);
									if (!detaulTaxList.isEmpty() && detaulTaxList != null) {
										Set<TaxRates> detaulTaxSet = new HashSet<TaxRates>(detaulTaxList);
										taxRates.addAll(detaulTaxSet);
									} else {
										TaxRates rates = new TaxRates();
										rates.setName(taxName);
										taxRates.add(rates);
									}
								} else {
									LOGGER.info(": ImportExcelServiceImpl : setLineItems :merchantId:"+merchant.getId()+":taxName:"+taxName);

									List<TaxRates> detaulTaxList = taxRateRepository
											.findByMerchantIdAndName(merchant.getId(), taxName);
									if (!detaulTaxList.isEmpty() && detaulTaxList != null) {
										Set<TaxRates> detaulTaxSet = new HashSet<TaxRates>(detaulTaxList);
										taxRates.addAll(detaulTaxSet);
									} else {
										TaxRates rates = new TaxRates();
										rates.setName(taxName);
										taxRates.add(rates);
									}
								}
							}
							break;

						case 5:
							String modifierGroupName = (String) value;
							LOGGER.info(": ImportExcelServiceImpl : setLineItems :merchantId:"+merchant.getId()+":modifierGroupName:"+modifierGroupName);

							List<ModifierGroup> modifierGroup = modifierGroupRepository
									.findByMerchantIdAndName(merchant.getId(), modifierGroupName);
							if (modifierGroup != null && !modifierGroup.isEmpty()) {
								for (ModifierGroup mg : modifierGroup) {
									if (item != null && item.getId() != null) {
										LOGGER.info(": ImportExcelServiceImpl : setLineItems :ModifierGroupId:"+mg.getId()+":itemId:"+item.getId());

										ItemModifierGroup itemModifierGroup = itemModifierGroupRepository
												.findByModifierGroupIdAndItemId(mg.getId(), item.getId());
										if (itemModifierGroup != null) {
											itemModifierGroups.add(itemModifierGroup);
											break;
										}
									} else {
										ItemModifierGroup itemModifierGroup = new ItemModifierGroup();
										itemModifierGroup.setItem(item);
										itemModifierGroup.setModifierGroup(mg);
										itemModifierGroup.setModifierGroupStatus(IConstant.BOOLEAN_TRUE);
										itemModifierGroups.add(itemModifierGroup);
										break;
									}
								}
							} else {
								ModifierGroup mg = new ModifierGroup();
								mg.setMerchant(merchant);
								mg.setName(modifierGroupName);

								ItemModifierGroup itemModifierGroup = new ItemModifierGroup();
								itemModifierGroup.setItem(item);
								itemModifierGroup.setModifierGroup(mg);
								itemModifierGroup.setModifierGroupStatus(IConstant.BOOLEAN_TRUE);
								itemModifierGroups.add(itemModifierGroup);
								break;
							}
							break;
						case 6:
							if (value != null) {
								item.setIsHidden((Boolean) value);
							}
							break;
						
						case 7:
							if(value!=null){
								String description= (String)value;
								item.setDescription(description);
							}
							break;
						}
					} else {
					}
				}
				// }

				if (!isDuplicateItem) {
					item.setTaxes(taxRates);

					item.setItemModifierGroups(itemModifierGroups);
					item.setSavedItemCountForExcelSheet(savedItemCounts);
					// item.setTotalItemCountForExcelSheet(totalItemCounts);

					items.add(item);
					duplicateItem = null;
				}
			}
		}
		LOGGER.info("------------------End:: ImportExcelServiceImpl : setLineItems :-------------------------");

		return items;
	}

	public ArrayList<Category> setCategories(Sheet sheet, Merchant merchant) {
		LOGGER.info("------------------Start:: ImportExcelServiceImpl : setCategories :-------------------------");

		Integer categoryCounts = 0;
		ArrayList<Category> categories = new ArrayList<Category>();
		Category duplicatCategory = null;
		Set<Item> items = null;
		int categoryOrder = 1;
		LOGGER.info(" ImportExcelServiceImpl : setCategories :lastCellNumber:"+sheet.getRow(0).getLastCellNum());

		if (sheet.getRow(0).getLastCellNum() == 3) {
			for (int j = 1; j <= sheet.getLastRowNum(); j++) {
				boolean isDuplicateCategory = true;
				Category category = duplicatCategory;

				Row row = sheet.getRow(j);
				if (row == null) {
					continue;
				}
				if (row.getCell(0) != null) {
					categoryCounts++;
				}
				boolean hasValues = false;
				LOGGER.info(" ImportExcelServiceImpl : setCategories :merchantId:"+merchant.getId());

				for (int k = 0; k <= row.getLastCellNum(); k++) {
					Cell cell = row.getCell(k);
					if (cell != null) {
						Object value = cellToObject(cell);

						hasValues = hasValues || value != null;
						switch (k) {
						case 0:
							String categoryName = null;
							if (value != null) {
								categoryName = (String) value;
								LOGGER.info(" ImportExcelServiceImpl : setCategories :categoryName:"+categoryName);

								List<Category> cats = categoryRepository.findByMerchantIdAndName(merchant.getId(),
										categoryName);
								LOGGER.info(" ImportExcelServiceImpl : setCategories :CategorySize:"+cats.size());

								if (cats != null && cats.size() > 0) {
									for (Category cat : cats) {
										category = cat;
										break;
									}
								} else {
									categoryName = (String) value;
									if (category == null) {
										category = new Category();
									}
									category.setMerchant(merchant);
									category.setName(categoryName.trim());
									category.setItemStatus(IConstant.BOOLEAN_FALSE);
									category.setSortOrder(categoryOrder++);
								}

								duplicatCategory = category;
								items = new HashSet<Item>();
								isDuplicateCategory = false;
							} else {
								isDuplicateCategory = true;
							}
							break;

						case 1:if(value!=null){
							String itemName = (String) value;
							LOGGER.info(" ImportExcelServiceImpl : setCategories :itemName:"+itemName);

							List<Item> itemList = itemmRepository.findByMerchantIdAndName(merchant.getId(), itemName);
							if (!itemList.isEmpty() && itemList != null) {
								for (Item item : itemList) {
									item.setCategories(null);
									items.add(item);
									break;
								}
							} else {
								itemName = (String) value;
								Item item = new Item();
								item.setName(itemName.trim());
								item.setCategories(null);
								items.add(item);
							}
						}
							break;

						case 2:
							if (value != null) {
								Boolean isPizzaVal = (Boolean) value;
								Integer isPizza = 0;
								if (isPizzaVal == true) {
									isPizza = 1;
								}
								if(isPizza!=null){
									category.setIsPizza(isPizza);
								}
								break;
							}
						}
					} else {
					}
				}

				/*
				 * if(hasValues||!config.isOmitEmpty()) { currentRowOffset++; if
				 * (rowLimit > 0 && totalRowsAdded == rowLimit) { break; } if
				 * (startRowOffset > 0 && currentRowOffset < startRowOffset) {
				 * continue; } //tmp.addRow(rowData); totalRowsAdded++; }
				 */
				if (!isDuplicateCategory) {
					category.setItems(items);
					category.setCategoryCountFoeExcelSheet(categoryCounts);
					categories.add(category);
					duplicatCategory = null;
				}

			}
		} else {

			for (int j = 1; j <= sheet.getLastRowNum(); j++) {
				boolean isDuplicateCategory = true;
				Category category = duplicatCategory;

				Row row = sheet.getRow(j);
				if (row == null) {
					continue;
				}
				if (row.getCell(0) != null) {
					categoryCounts++;
				}
				boolean hasValues = false;

				for (int k = 0; k <= row.getLastCellNum(); k++) {
					Cell cell = row.getCell(k);
					if (cell != null) {
						Object value = cellToObject(cell);

						hasValues = hasValues || value != null;
						switch (k) {
						case 0:
							if (value != null) {
								String posId = Double.toString((Double) value);
								LOGGER.info(" ImportExcelServiceImpl : setCategories :posId:"+posId);

								Category cat = categoryRepository.findByMerchantIdAndPosCategoryId(merchant.getId(),
										posId);

								if (cat != null) {
									category = cat;
								} else {
									if (duplicatCategory == null) {
										category = new Category();
									}
									category.setMerchant(merchant);
									category.setPosCategoryId(posId);
									category.setItemStatus(IConstant.BOOLEAN_FALSE);
									category.setSortOrder(categoryOrder++);

									duplicatCategory = category;
									items = new HashSet<Item>();
									isDuplicateCategory = false;
								}
							} else {
								isDuplicateCategory = true;
							}
							break;
						case 1:
							String categoryName = null;
							if (value != null) {
								categoryName = (String) value;
								LOGGER.info(" ImportExcelServiceImpl : setCategories :categoryName:"+categoryName);

								List<Category> cats = categoryRepository.findByMerchantIdAndName(merchant.getId(),
										categoryName);
								LOGGER.info(" ImportExcelServiceImpl : setCategories :categorySize:"+cats.size());

								if (cats != null && cats.size() > 0) {
									for (Category cat : cats) {
										category = cat;
										break;
									}
								} else {
									categoryName = (String) value;
									if (category == null) {
										category = new Category();
									}
									category.setMerchant(merchant);
									category.setName(categoryName.trim());
									category.setItemStatus(IConstant.BOOLEAN_FALSE);
									category.setSortOrder(categoryOrder++);
								}

								duplicatCategory = category;
								items = new HashSet<Item>();
								isDuplicateCategory = false;
							} else {
								isDuplicateCategory = true;
							}
							break;

						case 2:
							String itemName = null;
							if (value != null) {
								itemName = (String) value;
								LOGGER.info(" ImportExcelServiceImpl : setCategories :itemName:"+itemName);

								List<Item> itemList = itemmRepository.findByMerchantIdAndName(merchant.getId(),
										itemName);
								if (!itemList.isEmpty() && itemList != null) {
									for (Item item : itemList) {
										item.setCategories(null);
										items.add(item);
										break;
									}
								} else {
									itemName = (String) value;
									Item item = new Item();
									item.setName(itemName.trim());
									item.setCategories(null);
									items.add(item);
								}
							}
							break;

						case 3:if(value!=null){
							Boolean isPizzaVal = (Boolean) value;
							Integer isPizza = 0;
							if (isPizzaVal == true) {
								isPizza = 1;
							}
							if(isPizza!=null){
								category.setIsPizza(isPizza);
							}
							break;
						}
							break;
						}
					} else {
					}
				}

				/*
				 * if(hasValues||!config.isOmitEmpty()) { currentRowOffset++; if
				 * (rowLimit > 0 && totalRowsAdded == rowLimit) { break; } if
				 * (startRowOffset > 0 && currentRowOffset < startRowOffset) {
				 * continue; } //tmp.addRow(rowData); totalRowsAdded++; }
				 */
				if (!isDuplicateCategory) {
					category.setItems(items);
					category.setCategoryCountFoeExcelSheet(categoryCounts);
					categories.add(category);
					duplicatCategory = null;
				}
			}

		}
		LOGGER.info("-------------End :: ImportExcelServiceImpl : setCategories :---------------------");

		return categories;
	}

	public ArrayList<TaxRates> setTaxRates(Sheet sheet, Merchant merchant) {
		LOGGER.info("-------------Start :: ImportExcelServiceImpl : setTaxRates :---------------------");

		ArrayList<TaxRates> taxRates = new ArrayList<TaxRates>();
		LOGGER.info(" ImportExcelServiceImpl : setTaxRates :merchantId:"+merchant.getId());

		LOGGER.info(" ImportExcelServiceImpl : setTaxRates :sheetLastRowSize:"+sheet.getLastRowNum());

		for (int j = 1; j <= sheet.getLastRowNum(); j++) {
			TaxRates taxrate = new TaxRates();
			taxrate.setMerchant(merchant);
			Row row = sheet.getRow(j);
			if (row == null) {
				continue;
			}
			boolean hasValues = false;
			LOGGER.info(" ImportExcelServiceImpl : setTaxRates :sheetLastRowCellSize:"+ row.getLastCellNum());

			for (int k = 0; k <= row.getLastCellNum(); k++) {
				Cell cell = row.getCell(k);
				if (cell != null) {
					Object value = cellToObject(cell);

					hasValues = hasValues || value != null;
					switch (k) {
					case 0:
						String taxName = (String) value;
						LOGGER.info(" ImportExcelServiceImpl : setTaxRates :taxName:"+taxName);

						List<TaxRates> taxes = taxRateRepository.findByMerchantIdAndName(merchant.getId(), taxName);
						LOGGER.info(" ImportExcelServiceImpl : setTaxRates :TaxRatesListSize:"+taxes.size());

						if (taxes != null & taxes.size() > 0) {
							for (TaxRates rates : taxes) {
								taxrate = rates;
								break;
							}
						} else {
							taxrate.setName(taxName);
						}
						break;
					case 1:
						taxrate.setRate((Double) value);
						break;
					case 2:
						String isDefault = (String) value;
						if (isDefault.equalsIgnoreCase("Yes"))
							taxrate.setIsDefault(IConstant.BOOLEAN_TRUE);
						else
							taxrate.setIsDefault(IConstant.BOOLEAN_FALSE);
						break;

					}
				} else {
				}
			}
			taxRates.add(taxrate);
		}
		LOGGER.info("---------------End:: ImportExcelServiceImpl : setTaxRates :----------------");

		return taxRates;
	}

	public String saveInventoryByExcel(ExcelToJsonConverterConfig config, Merchant merchant, HttpSession session)
			throws InvalidFormatException, IOException {
		LOGGER.info("----------------Start :: ImportExcelServiceImpl : saveInventoryByExcel------------------------");
		
		String taxRateSheetColumns[] = { "Name", "Tax Rate", "Default" };
		int noOfTaxRateSheetColumns = 3;
		String modifierGroupsSheetColumns[] = { "ModifierGroupId", "Modifier Group Name", "ModifierId", "Modifier", "Price" };
		String modifierGroupsSheetColumns1[] = { "Modifier Group Name", "Modifier", "Price" };
		int noOfModifierGroupsSheetColumns = 5;
		int noOfModifierGroupsSheetColumns1 = 3;
		/*
		 * String itemsSheetColumns[]={"Name","Price","Price Type","Tax Rates"
		 * ,"Modifier Groups","Hidden"}; int noOfItemsSheetColumns=6;
		 */
		/*
		 * String
		 * categoriesSheetColumns[]={"Category Name","Items in Category"}; int
		 * noOfCategoriesSheetColumns=2;
		 */

		String pizzaSizeSheetColumns[] = { "Id", "Name" };
		int noOfPizzaSizeSheetColumns = 2;
		String pizaTemplateSheetColumns[] = { "Id", "Name", "Active", "Pizza Size", "Price", "Tax Rate" };
		int noOfPizzaTemplateSheetColumns = 6;
		
		String pizzaCrustSheetColumn[]= {"Id","Name","Active","Pizza Template","Pizza Size","Price"};
		int noOfPizzaCrustSheetColumn= 6;
		
		
		String pizzaToppingSheetColumns[] = { "Id", "Name", "Active", "Pizza Template", "Pizza Size", "Price" };
		int noOfPizzaToppingSheetColumns = 6;

		String attributeCounts = "";
		String response = "";
		this.config = config;

		ExcelWorkbook book = new ExcelWorkbook();

		InputStream inp = new FileInputStream(config.getSourceFile());
		Workbook wb = WorkbookFactory.create(inp);

		book.setFileName(config.getSourceFile());

		int loopLimit = wb.getNumberOfSheets();
		if (loopLimit >= config.getNumberOfSheets()) {
			int taxRateSheetIndex = wb.getSheetIndex("Tax Rates");
			int modifierGroupsSheetIndex = wb.getSheetIndex("Modifier Groups");
			int itemsSheetIndex = wb.getSheetIndex("Items");
			// int categoriesSheetIndex=wb.getSheetIndex("Categories");

			int pizzaSizeSheetIndex = wb.getSheetIndex("Pizza Size");
			int pizzaTemplateSheetIndex = wb.getSheetIndex("Pizza Template");
			int pizzaCrustSheetIndex = wb.getSheetIndex("Pizza Crust");
			int pizzaToppingIndex = wb.getSheetIndex("Pizza Topping");

			int categoriesSheetIndex = wb.getSheetIndex("Categories");

			if ((taxRateSheetIndex == 0 && modifierGroupsSheetIndex == 1 && itemsSheetIndex == 2
					&& categoriesSheetIndex == 3)
					|| (taxRateSheetIndex == 0 && modifierGroupsSheetIndex == 1 && itemsSheetIndex == 2
							&& pizzaSizeSheetIndex == 3 && pizzaTemplateSheetIndex == 4 && pizzaCrustSheetIndex==5 && pizzaToppingIndex == 6
							&& categoriesSheetIndex == 7)) {

				for (int i = 0; i < loopLimit; i++) {
					Sheet sheet = wb.getSheetAt(i);

					if (sheet == null) {
						continue;
					}
					ExcelWorksheet tmp = new ExcelWorksheet();
					tmp.setName(sheet.getSheetName());

					if (sheet.getSheetName().equals("Tax Rates")) {
						LOGGER.info("ImportExcelServiceImpl :: saveInventoryByExcel : sheetName "+sheet.getSheetName());
						boolean status = validateColumns(sheet, noOfTaxRateSheetColumns, taxRateSheetColumns);
						if (status) {
							int taxRatesCount = sheet.getLastRowNum();
							ArrayList<TaxRates> taxRates = setTaxRates(sheet, merchant);
							session.setAttribute("taxRates", taxRates);
							// taxRateRepository.save(taxRates);
							attributeCounts = attributeCounts + " Tax Rates count-" + taxRatesCount + ",";

						} else {
							attributeCounts = "Tax Rates sheet: Please make sure you have the following headers in the following order: Name,Tax Rate, Default";
							break;
						}

					}
					if (sheet.getSheetName().equals("Modifier Groups")) {
						LOGGER.info("ImportExcelServiceImpl :: saveInventoryByExcel : sheetName "+sheet.getSheetName());
						boolean status = false;
						if (sheet.getRow(0).getLastCellNum() == 5) {
							status = validateColumns(sheet, noOfModifierGroupsSheetColumns, modifierGroupsSheetColumns);
						} else {
							status = validateColumns(sheet, noOfModifierGroupsSheetColumns1,
									modifierGroupsSheetColumns1);
						}
						if (status) {
							// int modifierGroupsCounts = sheet.getLastRowNum();
							ArrayList<ModifierGroup> modifierGroups = setModifierGroups(sheet, merchant, session);
							session.setAttribute("modifierGroups", modifierGroups);
							// modifierGroupRepository.save(modifierGroups);
							if (modifierGroups.size() > 0) {
								attributeCounts = attributeCounts + " modifier Groups Count-" + modifierGroups
										.get(modifierGroups.size() - 1).getModifierGroupCountForExcelSheet() + ",";
							} else {
								attributeCounts = attributeCounts + " modifier Groups Count-0,";
							}

						} else {
							attributeCounts = "Modifier Groups sheet: Please make sure you have the following headers in the following order: Modifier Group Name,Modifier,Price";
							break;
						}

					}
					if (sheet.getSheetName().equals("Items")) {
						LOGGER.info("ImportExcelServiceImpl :: saveInventoryByExcel : sheetName "+sheet.getSheetName());
						String itemsSheetColumns[] = { "Name", "Price", "Price Type", "Tax Rates", "Modifier Groups",
								"Hidden" , "Description"};
						String itemSheetColumns1[] = { "PosId", "Name", "Price", "Price Type", "Tax Rates",
								"Modifier Groups", "Hidden", "Description" };

						int noOfItemsSheetColumns = 7;
						int noOfItemsSheetColumns1 = 8;
						boolean status = false;

						if (sheet.getRow(0).getLastCellNum() == 7) {
							status = validateColumns(sheet, noOfItemsSheetColumns, itemsSheetColumns);
						} else {
							status = validateColumns(sheet, noOfItemsSheetColumns1, itemSheetColumns1);
						}

						if (status) {
							int totalItemsCounts = sheet.getLastRowNum() + 1;
							ArrayList<Item> items = setLineItems(sheet, merchant);
							if (items.size() > 0) {
								attributeCounts = attributeCounts + "  items count-"
										+ items.get(items.size() - 1).getSavedItemCountForExcelSheet() + ",";
							} else {
								attributeCounts = attributeCounts + "  items count-" + 0 + "/" + 0 + ",";
							}
							// itemmRepository.save(items);
							session.setAttribute("items", items);

						} else {
							attributeCounts = "Items sheet: Please make sure you have the following headers in the following order: Name,Price,Price Type,Tax Rates,Modifier Groups,Hidden,Description";
							break;
						}

					}

					if (sheet.getSheetName().equals("Pizza Size")) {
						LOGGER.info("ImportExcelServiceImpl :: saveInventoryByExcel : sheetName "+sheet.getSheetName());
						boolean status = validateColumns(sheet, noOfPizzaSizeSheetColumns, pizzaSizeSheetColumns);
						if (status) {
							int pizzaSizeCount = sheet.getLastRowNum();
							ArrayList<PizzaSize> pizzaSize = setPizzaSize(sheet, merchant);
							session.setAttribute("pizzaSize", pizzaSize);
							// taxRateRepository.save(taxRates);
							attributeCounts = attributeCounts + " pizza Size count-" + pizzaSizeCount + ",";

						} else {
							attributeCounts = "pizzaSize sheet: Please make sure you have the following headers in the following order: Id,Name in Pizza Size";
							break;
						}
					}
					if (sheet.getSheetName().equals("Pizza Template")) {
						LOGGER.info("ImportExcelServiceImpl :: saveInventoryByExcel : sheetName "+sheet.getSheetName());
						boolean status = validateColumns(sheet, noOfPizzaTemplateSheetColumns,
								pizaTemplateSheetColumns);
						if (status) {
							int pizzaTemlateCount = sheet.getLastRowNum();
							ArrayList<PizzaTemplate> pizzaTemplate = setPizzaTemplate(sheet, merchant);
							session.setAttribute("pizzaTemplate", pizzaTemplate);
							// taxRateRepository.save(taxRates);
							attributeCounts = attributeCounts + " pizza Template count-" + pizzaTemlateCount + ",";

						} else {
							attributeCounts = "PizzaTemplate sheet: Please make sure you have the following headers in the following order: Id,Name,Active,Pizza Size,Price in PizzaTemplate";
							break;
						}
					}
					
					
					if (sheet.getSheetName().equals("Pizza Crust")) {
						LOGGER.info("ImportExcelServiceImpl :: saveInventoryByExcel : sheetName "+sheet.getSheetName());
						boolean status = validateColumns(sheet, noOfPizzaCrustSheetColumn,
								pizzaCrustSheetColumn);
						if (status) {
							int pizzaCrustCount = sheet.getLastRowNum();
							ArrayList<PizzaCrust> pizzaCrusts = setPizzaCrust(sheet, merchant);
							session.setAttribute("pizzaCrusts", pizzaCrusts);
							// taxRateRepository.save(taxRates);
							attributeCounts = attributeCounts + " pizza Crust count-" + pizzaCrustCount + ",";

						} else {
							attributeCounts = "PizzaCrust sheet: Please make sure you have the following headers in the following order: Id,Name,Active,Pizza Template,Pizza Size,Price in PizzaCrust";
							break;
						}
					}
					if (sheet.getSheetName().equals("Pizza Topping")) {
						LOGGER.info("ImportExcelServiceImpl :: saveInventoryByExcel : sheetName "+sheet.getSheetName());
						boolean status = validateColumns(sheet, noOfPizzaToppingSheetColumns, pizzaToppingSheetColumns);
						if (status) {
							int pizzaToppingCount = sheet.getLastRowNum();
							ArrayList<PizzaTopping> pizzaTopping = setPizzaTopping(sheet, merchant);
							session.setAttribute("pizzaTopping", pizzaTopping);
							// taxRateRepository.save(taxRates);
							attributeCounts = attributeCounts + " pizza Topping count-" + pizzaToppingCount + ",";

						} else {
							attributeCounts = "PizzaTopping sheet: Please make sure you have the following headers in the following order: Id,Name,Active,Pizza Template, Pizza Size,Price in PizzaTopping";
							break;
						}
					}

					if (sheet.getSheetName().equals("Categories")) {
						LOGGER.info("ImportExcelServiceImpl :: saveInventoryByExcel : sheetName "+sheet.getSheetName());

						String categoriesSheetColumns[] = { "Category Name", "Items in Category", "isPizza" };
						String categoriesSheetColumns1[] = { "PosId", "Category Name", "Items in Category", "isPizza" };
						int noOfCategoriesSheetColumns = 3;
						int noOfCategoriesSheetColumns1 = 4;
						boolean status = false;
						if (sheet.getRow(0).getLastCellNum() == 3) {
							status = validateColumns(sheet, noOfCategoriesSheetColumns, categoriesSheetColumns);
						} else {
							status = validateColumns(sheet, noOfCategoriesSheetColumns1, categoriesSheetColumns1);
						}

						if (status) {
							// int categoriesCounts = sheet.getLastRowNum()+1;
							List<Category> categories = setCategories(sheet, merchant);
							if (categories.size() > 0) {
								attributeCounts = attributeCounts + "  categories count-"
										+ categories.get(categories.size() - 1).getCategoryCountFoeExcelSheet();
							} else {
								attributeCounts = attributeCounts + "  categories count-0";
							}

							session.setAttribute("categories", categories);
							response = "All the inventory imported successfully.";
						} else {
							attributeCounts = "Categories sheet: Please make sure you have the following headers in the following order: Category Name,Items in Category, isPizza";
							break;
						}
					}

					book.addExcelWorksheet(tmp);
				}

			} else {
				attributeCounts = "You must have four sheets in the following order: Tax Rates,Modifier Groups,Items,PizzaSize,PizzaTemplate,PizzaCrust,PizzaTopping,Categories";
			}
		} else {
			attributeCounts = "You must have four sheets in the following order: Tax Rates,Modifier Groups,Items,PizzaSize,PizzaTemplate,PizzaCrust,PizzaTopping,Categories";
		}
		System.out.println(book.toJson(true));
		LOGGER.info("----------------End :: ImportExcelServiceImpl : saveInventoryByExcel------------------------");
		return attributeCounts;

	}

	public String saveInvetoryDataOfExcelSheet(ArrayList<TaxRates> taxRates, ArrayList<ModifierGroup> modifierGroups,
			ArrayList<Item> items, List<Category> categories, List<PizzaSize> pizzaSizes,
			List<PizzaTemplate> pizaaTemplates,List<PizzaCrust> pizzaCrusts, List<PizzaTopping> pizzaToppings) {
		LOGGER.info("----------------Start :: ImportExcelServiceImpl : saveInvetoryDataOfExcelSheet:------------------------");

		String response = "FAIL";
		Boolean isFocus = false;
		try {
			taxRateRepository.save(taxRates);
			LOGGER.info(" ImportExcelServiceImpl : saveInvetoryDataOfExcelSheet:Save taxRates ");

			modifierGroupRepository.save(modifierGroups);
			LOGGER.info(" ImportExcelServiceImpl : saveInvetoryDataOfExcelSheet:Save modifierGroups ");

			if (items != null) {
				for (Item item : items) {
					Set<TaxRates> itemTaxes = item.getTaxes();
					Set<TaxRates> finalTaxRates = new HashSet<TaxRates>();
					for (TaxRates taxRates2 : itemTaxes) {

						if (taxRates2.getName().equalsIgnoreCase("DEFAULT")) {
							item.setIsDefaultTaxRates(true);
							LOGGER.info(" ImportExcelServiceImpl : saveInvetoryDataOfExcelSheet: merchantId:"+item.getMerchant().getId());

							List<TaxRates> defaultTaxList = taxRateRepository
									.findByMerchantIdAndIsDefault(item.getMerchant().getId(), 1);
							Set<TaxRates> detaulTaxSet = new HashSet<TaxRates>(defaultTaxList);
							finalTaxRates.addAll(detaulTaxSet);
						} else {
							item.setIsDefaultTaxRates(false);
							LOGGER.info(" ImportExcelServiceImpl : saveInvetoryDataOfExcelSheet: merchantId:"+item.getMerchant().getId()+":taxRatesName:"+ taxRates2.getName());

							List<TaxRates> defaultTaxList = taxRateRepository
									.findByMerchantIdAndName(item.getMerchant().getId(), taxRates2.getName());
							Set<TaxRates> detaulTaxSet = new HashSet<TaxRates>(defaultTaxList);
							finalTaxRates.addAll(detaulTaxSet);
						}
					}
					item.setTaxes(finalTaxRates);
					
					/*if(item.getPosItemId()!=null && !item.getPosItemId().isEmpty() && item.getMerchant().getOwner().getPos().getPosId()==IConstant.FOCUS){
						Item finalItem= itemmRepository.findByPosItemIdAndMerchantId(item.getPosItemId(), item.getMerchant().getId());
						if(finalItem==null){
							itemmRepository.save(item);
							isFocus = true;
						}
					}*/
				}
					List<Item> withoutDuplicateItems = new ArrayList<Item>();
					LOGGER.info(" ImportExcelServiceImpl : saveInvetoryDataOfExcelSheet: itemsSize:"+items.size());

					for (int i=0; i<items.size(); i++) {
						LOGGER.info(" ImportExcelServiceImpl : saveInvetoryDataOfExcelSheet: withoutDuplicateItemsSize:"+withoutDuplicateItems.size());

						if(withoutDuplicateItems.size()>0){
						Boolean flag = false;
						for (int j=0; j<withoutDuplicateItems.size(); j++) {
							if(!items.get(i).getName().equals(withoutDuplicateItems.get(j).getName())){
								flag = true;
							}else{
								flag = false;
								break;
							}
						}
						if(flag){
							withoutDuplicateItems.add(items.get(i));
						}
					}else{
							withoutDuplicateItems.add(items.get(i));
						}
					}
					items.retainAll(withoutDuplicateItems);
					itemmRepository.save(withoutDuplicateItems);
					LOGGER.info(" ImportExcelServiceImpl : saveInvetoryDataOfExcelSheet: save withoutDuplicateItems");

			}
			for (Item item : items) {
				List<ItemModifierGroup> itemModifierGroups = item.getItemModifierGroups();
				if (itemModifierGroups != null) {
					for (ItemModifierGroup itemModifierGroup : itemModifierGroups) {
						ModifierGroup modifierGroup = itemModifierGroup.getModifierGroup();
						LOGGER.info(" ImportExcelServiceImpl : saveInvetoryDataOfExcelSheet: merchantId :"+modifierGroup.getMerchant().getId()+":modifierGroupName:"+ modifierGroup.getName());

						List<ModifierGroup> groups = modifierGroupRepository
								.findByMerchantIdAndName(modifierGroup.getMerchant().getId(), modifierGroup.getName());
						ModifierGroup group = null;
						if (groups != null && !groups.isEmpty()) {
							group = groups.get(0);
							LOGGER.info(" ImportExcelServiceImpl : saveInvetoryDataOfExcelSheet: ItemModifierGroupId :"+group.getId()+":itemId:"+item.getId());

							ItemModifierGroup itemGroup = itemModifierGroupRepository
									.findByModifierGroupIdAndItemId(group.getId(), item.getId());
							if (itemGroup == null && group != null && group.getId() != null) {
								itemGroup = new ItemModifierGroup();
								//itemGroup.setItem(item);
								
								if(item.getId() != null){
									Item dbItem = itemmRepository.findOne(item.getId());
									if(dbItem!=null){
										itemGroup.setItem(dbItem);
									}
								}
								itemGroup.setModifierGroup(group);
								itemGroup.setModifierGroupStatus(1);
								itemModifierGroupRepository.save(itemGroup);
								LOGGER.info(" ImportExcelServiceImpl : saveInvetoryDataOfExcelSheet: ItemModifierGroup saved :");

							}
						}
						if (group != null) {
							List<ItemModifiers> itemModifiers = itemModifiersRepository.findByModifierGroupIdAndItemId(group.getId(),item.getId());
							if(itemModifiers==null ||  itemModifiers.isEmpty()){
							List<Modifiers> modifiers = modifierModifierGroupRepository.findByModifierGroupId(group.getId());
							
							if(modifiers!=null && !modifiers.isEmpty() &&  item != null){
							for (Modifiers modifier : modifiers) {
								ItemModifiers finalItemModifiers = new ItemModifiers();
								finalItemModifiers.setItem(item);
								finalItemModifiers.setModifierGroup(group);
								finalItemModifiers.setModifiers(modifier);
								finalItemModifiers.setModifierStatus(IConstant.BOOLEAN_TRUE);
								itemModifiersRepository.save(finalItemModifiers);
								LOGGER.info(" ImportExcelServiceImpl : saveInvetoryDataOfExcelSheet: ItemModifiers saved :");

								}
							}
						}
					}
				}
			}
		}

			if (pizzaSizes != null) {
				pizzaSizeRepository.save(pizzaSizes);
				LOGGER.info(" ImportExcelServiceImpl : saveInvetoryDataOfExcelSheet: pizzaSizes saved :");

			}

			if (pizaaTemplates != null) {
				for (PizzaTemplate pizaaTemplatess : pizaaTemplates) {
					pizaaTemplatess.setPizzaToppings(null);
					pizaaTemplatess.getMerchant().setAddresses(null);
					pizaaTemplatess.getMerchant().setTaxRates(null);
					pizaaTemplatess.getMerchant().setMerchantLogin(null);
					pizaaTemplatess.getMerchant().setOrderRs(null);
					pizaaTemplatess.getMerchant().setModifierGroups(null);

					Set<TaxRates> pizaaTemplat = pizaaTemplatess.getTaxes();
					Set<TaxRates> finalTaxRates = new HashSet<TaxRates>();
					for (TaxRates taxRates2 : pizaaTemplat) {

						if (taxRates2.getName().equalsIgnoreCase("DEFAULT")) {
							List<TaxRates> defaultTaxList = taxRateRepository
									.findByMerchantIdAndIsDefault(pizaaTemplatess.getMerchant().getId(), 1);
							Set<TaxRates> defaultTaxSet = new HashSet<TaxRates>(defaultTaxList);
							for (TaxRates taxRates3 : defaultTaxList) {
								Boolean flag = true;
								for (TaxRates finalRate : finalTaxRates) {
									if (finalRate.getName() != null
											&& finalRate.getName().equals(taxRates3.getName())) {
										flag = false;
										break;
									}
								}

								if (flag) {
									defaultTaxSet.add(taxRates3);
									finalTaxRates.addAll(defaultTaxSet);
								}
							}
							
						} else {
							LOGGER.info(" ImportExcelServiceImpl : saveInvetoryDataOfExcelSheet: merchantId:"+pizaaTemplatess.getMerchant().getId()+":taxRatesName:"+taxRates2.getName());

							List<TaxRates> defaultTaxList = taxRateRepository.findByMerchantIdAndName(
									pizaaTemplatess.getMerchant().getId(), taxRates2.getName());
							Set<TaxRates> defaultTaxSet = new HashSet<TaxRates>(defaultTaxList);
							for (TaxRates taxRates3 : defaultTaxList) {
								Boolean flag = true;
								for (TaxRates finalRate : finalTaxRates) {
									if (finalRate.getName() != null
											&& finalRate.getName().equals(taxRates3.getName())) {
										flag = false;
										break;
									}
								}
								if (flag) {
									defaultTaxSet.add(taxRates3);
									finalTaxRates.addAll(defaultTaxSet);
								}
							}
							
						}
					}
					pizaaTemplatess.setTaxes(finalTaxRates);
				}

				pizzaTemplateRepository.save(pizaaTemplates);
				LOGGER.info(" ImportExcelServiceImpl : saveInvetoryDataOfExcelSheet: pizaaTemplates saved:");

			}
			
			if (pizaaTemplates != null) {
				List<PizzaTemplateSize> pizzaTemplateSizes = new ArrayList<PizzaTemplateSize>();
				for (PizzaTemplate template : pizaaTemplates) {
					LOGGER.info(" ImportExcelServiceImpl : saveInvetoryDataOfExcelSheet:merchantId:"+template.getMerchant().getId()+": templatePosPizzaTemplateId :"+ template.getPosPizzaTemplateId());

					PizzaTemplate pizzaTemplate = pizzaTemplateRepository.findByMerchantIdAndPosPizzaTemplateId(
							template.getMerchant().getId(), template.getPosPizzaTemplateId());
					List<PizzaTemplateSize> templateSizes = template.getPizzaTemplateSizes();
					if (templateSizes != null && !templateSizes.isEmpty()) {
						
						for (PizzaTemplateSize templateSize : templateSizes) {
							LOGGER.info(" ImportExcelServiceImpl : saveInvetoryDataOfExcelSheet: Description:"+templateSize.getPizzaSize().getDescription()+": templateId :"+template.getId());

							List<PizzaTemplateSize> pizzaTemplateSize = pizzaTemplateSizeRepository
									.findByPizzaSizeDescriptionAndPizzaTemplateId(templateSize.getPizzaSize().getDescription(),
											template.getId());
							if (pizzaTemplateSize == null || pizzaTemplateSize.isEmpty()) {
								LOGGER.info(" ImportExcelServiceImpl : saveInvetoryDataOfExcelSheet:merchantId:"+template.getMerchant().getId()+": Description :"+templateSize.getPizzaSize().getDescription());

								List<PizzaSize> pizzaSize = pizzaSizeRepository.findByMerchantIdAndDescription(template.getMerchant().getId(),
										templateSize.getPizzaSize().getDescription());
								if (!pizzaSize.isEmpty()) {
									for (PizzaSize pizzaSize2 : pizzaSize) {
										pizzaSize2.setCategories(null);
										pizzaSize2.setPizzaToppingSizes(null);
										pizzaSize2.setPizzaTemplates(null);
										pizzaSize2.setMerchant(null);
										templateSize.setPizzaSize(pizzaSize2);
									}
									pizzaTemplate.setPizzaSizes(null);
									pizzaTemplate.setTaxes(null);
									pizzaTemplate.setCategory(null);
									pizzaTemplate.setMerchant(null);
									pizzaTemplate.setPizzaToppings(null);
									pizzaTemplate.setPizzaTemplateSizes(null);
									templateSize.setPizzaTemplate(pizzaTemplate);
									templateSize.setActive(1);
									pizzaTemplateSizes.add(templateSize);
									pizzaTemplateSizeRepository.save(pizzaTemplateSizes);
									LOGGER.info(" ImportExcelServiceImpl : saveInvetoryDataOfExcelSheet: pizzaTemplateSizes saved:");

								}
								
							} else {
								for (PizzaTemplateSize finalPizzaTemplateSize : pizzaTemplateSize) {
									LOGGER.info(" ImportExcelServiceImpl : saveInvetoryDataOfExcelSheet: PosPizzaSizeId :"+templateSize.getPizzaSize().getPosPizzaSizeId()+": merchantId :"+template.getMerchant().getId());

								PizzaSize pizzaSize = pizzaSizeRepository.findByPosPizzaSizeIdAndMerchantId(
										templateSize.getPizzaSize().getPosPizzaSizeId(),
										template.getMerchant().getId());
								if (pizzaSize != null) {
									pizzaSize.setCategories(null);
									pizzaSize.setPizzaToppingSizes(null);
									finalPizzaTemplateSize.setPizzaSize(pizzaSize);
								}
								pizzaTemplate.setPizzaSizes(null);
								finalPizzaTemplateSize.setActive(1);
								finalPizzaTemplateSize.setPizzaTemplate(pizzaTemplate);
								pizzaTemplateSizeRepository.save(finalPizzaTemplateSize);
								LOGGER.info(" ImportExcelServiceImpl : saveInvetoryDataOfExcelSheet: PizzaTemplateSize save :");

								}
							}
						}
					}
				}
			}
			
			if (pizzaCrusts != null) {
				pizzaCrustRepository.save(pizzaCrusts);
				LOGGER.info(" ImportExcelServiceImpl : saveInvetoryDataOfExcelSheet: pizzaCrusts save :");

				for (PizzaCrust pizzaCrust : pizzaCrusts) {
					
					List<PizzaTemplateCrust> pizzaTemplateCrusts= pizzaCrust.getPizzaTemplateCrusts();
					List<PizzaTemplateCrust> templateCrusts=new ArrayList<PizzaTemplateCrust>();
					
					if(pizzaTemplateCrusts!=null && !pizzaTemplateCrusts.isEmpty() && pizzaTemplateCrusts.size()>0){
						for(PizzaTemplateCrust pizzaTemplateCrust : pizzaTemplateCrusts){
							LOGGER.info(" ImportExcelServiceImpl : saveInvetoryDataOfExcelSheet: pizzaTemplateId:"+pizzaTemplateCrust.getPizzaTemplate().getId()+":pizzaCrustId():"+pizzaCrust.getId());

						PizzaTemplateCrust pizzaTemplateCrustList = pizzaTemplateCrustRepository.findByPizzaTemplateIdAndPizzaCrustId(
								pizzaTemplateCrust.getPizzaTemplate().getId(),pizzaCrust.getId());
						if(pizzaTemplateCrustList==null){
							
						if (pizzaCrust.getMerchant() != null && pizzaCrust.getMerchant().getId() != null
								&& pizzaTemplateCrust.getPizzaTemplate() != null
								&& pizzaTemplateCrust.getPizzaTemplate().getDescription() != null) {
							LOGGER.info(" ImportExcelServiceImpl : saveInvetoryDataOfExcelSheet: Description:"+pizzaTemplateCrust.getPizzaTemplate().getDescription()+":merchantId:"+pizzaCrust.getMerchant().getId());

							List<PizzaTemplate> pizzaTemplateList = pizzaTemplateRepository.findByDescriptionAndMerchantId(
									pizzaTemplateCrust.getPizzaTemplate().getDescription(),
									pizzaCrust.getMerchant().getId());
							if (pizzaTemplateList != null && !pizzaTemplateList.isEmpty()) {
								for (PizzaTemplate template2 : pizzaTemplateList) {
									template2.setCategory(null);
									pizzaTemplateCrust.setPizzaTemplate(template2);
								}
								pizzaTemplateCrust.setPizzaCrust(pizzaCrust);
								templateCrusts.add(pizzaTemplateCrust);
								pizzaTemplateCrustRepository.save(templateCrusts);
								LOGGER.info(" ImportExcelServiceImpl : saveInvetoryDataOfExcelSheet: save templateCrusts");

							}
						}
						}else{
							LOGGER.info(" ImportExcelServiceImpl : saveInvetoryDataOfExcelSheet: merchantId:"+pizzaCrust.getMerchant().getId()+":PosPizzaTemplateId:"+ pizzaTemplateCrust.getPizzaTemplate().getPosPizzaTemplateId());

							PizzaTemplate pizzaTemplate = pizzaTemplateRepository.findByMerchantIdAndPosPizzaTemplateId(pizzaCrust.getMerchant().getId()
									, pizzaTemplateCrust.getPizzaTemplate().getPosPizzaTemplateId());
							if (pizzaTemplate != null) {
								pizzaTemplate.setCategory(null);
								pizzaTemplateCrustList.setPizzaTemplate(pizzaTemplate);
							}
							pizzaTemplateCrustList.setPizzaCrust(pizzaCrust);
							pizzaTemplateCrustRepository.save(pizzaTemplateCrustList);
							LOGGER.info(" ImportExcelServiceImpl : saveInvetoryDataOfExcelSheet: saved pizzaTemplateCrustList:");

						}
					}
				}
					
					List<PizzaCrustSizes> pizzaCrustSizes = pizzaCrust.getPizzaCrustSizes();
					List<PizzaCrustSizes> crustSizesLists = new ArrayList<PizzaCrustSizes>();
					
					if (pizzaCrustSizes!=null && !pizzaCrustSizes.isEmpty() && pizzaCrustSizes.size() > 0) {
						for (PizzaCrustSizes crustSizes : pizzaCrustSizes) {
							
							PizzaCrustSizes crustSize = pizzaCrustSizeRepository.findByPizzaCrustIdAndPizzaSizeId(pizzaCrust.getId(),
									crustSizes.getPizzaSize().getId());
							
							if(crustSize==null){
							if (pizzaCrust.getMerchant() != null && pizzaCrust.getMerchant().getId() != null
									&& crustSizes.getPizzaSize() != null
									&& crustSizes.getPizzaSize().getDescription() != null) {
								
								List<PizzaSize> pizzaSizeList = pizzaSizeRepository.findByMerchantIdAndDescription(pizzaCrust.getMerchant().getId()
										, crustSizes.getPizzaSize().getDescription());
								
								if(pizzaSizeList!=null && !pizzaSizeList.isEmpty() && pizzaSizeList.size()>0){
									for (PizzaSize pizzaSize : pizzaSizeList) {
										pizzaSize.setCategories(null);
										pizzaSize.setPizzaTemplates(null);
										pizzaSize.setPizzaToppingSizes(null);
										pizzaSize.setPizzaCrustSizes(null);
										crustSizes.setPizzaSize(pizzaSize);
									}
									crustSizes.setPizzaCrust(pizzaCrust);
									crustSizesLists.add(crustSizes);
									pizzaCrustSizeRepository.save(crustSizesLists);
									LOGGER.info(" ImportExcelServiceImpl : saveInvetoryDataOfExcelSheet: saved crustSizesLists:");

								 }
							  }
							}else{
								PizzaSize pizzaSize = pizzaSizeRepository.findByPosPizzaSizeIdAndMerchantId(
										crustSizes.getPizzaSize().getPosPizzaSizeId(),
										pizzaCrust.getMerchant().getId());
								if (pizzaSize != null) {
									pizzaSize.setCategories(null);
									pizzaSize.setPizzaToppingSizes(null);
									crustSize.setPrice(crustSizes.getPrice());
									crustSize.setPizzaSize(pizzaSize);
								}
								crustSize.setPizzaCrust(pizzaCrust);
								crustSize.setActive(1);
								pizzaCrustSizeRepository.save(crustSize);
								LOGGER.info(" ImportExcelServiceImpl : saveInvetoryDataOfExcelSheet: saved crustSize:");

							}
						}
					}
				}
			}
			
			
			
			if (pizzaToppings != null) {
				pizzaToppingRepository.save(pizzaToppings);
				LOGGER.info(" ImportExcelServiceImpl : saveInvetoryDataOfExcelSheet: saved pizzaToppings:");

				List<PizzaToppingSize> pizzaToppingSizess = new ArrayList<PizzaToppingSize>();
				for (PizzaTopping pizzaTopping : pizzaToppings) {
					List<PizzaToppingSize> pizzaToppingSizes = pizzaTopping.getPizzaToppingSizes();
					
					if (pizzaToppingSizes != null && !pizzaToppingSizes.isEmpty()) {
						for (PizzaToppingSize pizzaToppingSize : pizzaToppingSizes) {

							List<PizzaToppingSize> pizzaToppingSize2 = pizzaToppingSizeRepository
									.findByPizzaSizeDescriptionAndPizzaToppingId(pizzaToppingSize.getPizzaSize().getDescription(),
											pizzaTopping.getId());
							
							if (pizzaToppingSize2 == null || pizzaToppingSize2.isEmpty()) {
								List<PizzaSize> pizzaSize = pizzaSizeRepository.findByMerchantIdAndDescription(pizzaTopping.getMerchant().getId(), 
										pizzaToppingSize.getPizzaSize().getDescription());
								if (!pizzaSize.isEmpty() && pizzaSize.size()>0) {
									for (PizzaSize pizzaSize3 : pizzaSize) {
										pizzaToppingSize.setPizzaSize(pizzaSize3);
									}
									pizzaToppingSize.setActive(0);
									pizzaToppingSize.setPizzaTopping(pizzaTopping);
									pizzaToppingSizess.add(pizzaToppingSize);
									pizzaToppingSizeRepository.save(pizzaToppingSizess);
									LOGGER.info(" ImportExcelServiceImpl : saveInvetoryDataOfExcelSheet: saved pizzaToppingSizess:");
								}
								
							} else {
								for (PizzaToppingSize finalPizzaToppingSize : pizzaToppingSize2) {
									PizzaSize pizzaSize = pizzaSizeRepository.findByPosPizzaSizeIdAndMerchantId(
											pizzaToppingSize.getPizzaSize().getPosPizzaSizeId(),
											pizzaTopping.getMerchant().getId());
									if (pizzaSize != null) {
										finalPizzaToppingSize.setPizzaSize(pizzaSize);
									}
									finalPizzaToppingSize.setActive(1);
									finalPizzaToppingSize.setPizzaTopping(pizzaTopping);
									pizzaToppingSizeRepository.save(finalPizzaToppingSize);
									LOGGER.info(" ImportExcelServiceImpl : saveInvetoryDataOfExcelSheet: saved PizzaToppingSize:");

								}
							}
						}
					}
					
					List<PizzaTemplateTopping> pizzaTemplateToppings=pizzaTopping.getPizzaTemplateToppings();
					List<PizzaTemplateTopping> templateToppings = new ArrayList<PizzaTemplateTopping>();
					
					if(!pizzaTemplateToppings.isEmpty() && pizzaTemplateToppings.size()>0){
					for (PizzaTemplateTopping pizzaTemplateTopping : pizzaTemplateToppings) {
						
						PizzaTemplateTopping templateTopping=pizzaTemplateToppingRepository.findByPizzaTemplateIdAndPizzaToppingId(pizzaTemplateTopping.getPizzaTemplate().getId()
								, pizzaTopping.getId());
						
						if(templateTopping==null){
							List<PizzaTemplate> pizzaTemplateList = pizzaTemplateRepository.findByMerchantIdAndDescriptionByQuery(pizzaTopping.getMerchant().getId()
									, pizzaTemplateTopping.getPizzaTemplate().getDescription());
							if(!pizzaTemplateList.isEmpty() && pizzaTemplateList.size()>0){
								for (PizzaTemplate pizzaTemplate : pizzaTemplateList) {
									pizzaTemplateTopping.setPizzaTemplate(pizzaTemplate);
								}
								pizzaTemplateTopping.setPizzaTopping(pizzaTopping);
								pizzaTemplateTopping.setActive(true);
								templateToppings.add(pizzaTemplateTopping);
								pizzaTemplateToppingRepository.save(templateToppings);
								LOGGER.info(" ImportExcelServiceImpl : saveInvetoryDataOfExcelSheet: saved PizzaTemplateTopping:");

							}
						}else{
							PizzaTemplate pizzaTemplate = pizzaTemplateRepository.findByMerchantIdAndPosPizzaTemplateId(pizzaTopping.getMerchant().getId()
									, pizzaTemplateTopping.getPizzaTemplate().getPosPizzaTemplateId());
							if(pizzaTemplate!=null){
								templateTopping.setPizzaTemplate(pizzaTemplate);
							}
							templateTopping.setPizzaTopping(pizzaTopping);
							templateTopping.setActive(true);
							pizzaTemplateToppingRepository.save(templateTopping);
							LOGGER.info(" ImportExcelServiceImpl : saveInvetoryDataOfExcelSheet: saved PizzaTemplate:");

						}
						
					}
					}
				}
			}

			for (Category category : categories) {
				if (category.getIsPizza()!=null && category.getIsPizza() == 1 && pizaaTemplates != null) {/*
					
					
					category.setItems(null);
					categoryRepository.save(category);

					for (PizzaTemplate pizzaTemplate : pizaaTemplates) {
						
						//pizzaTemplate.setCategory(category);
						
						if (category.getId() != null && pizzaTemplate.getCategory() != null
								&& pizzaTemplate.getCategory().getId() != null
								&& (pizzaTemplate.getCategory().getId() == category.getId())) {
							
							List<PizzaTemplateCategory> pizzaTemplateCategories = pizzaTemplateCategoryRepository
									.findByCategoryIdAndPizzaTemplateId(category.getId(), pizzaTemplate.getId());
							if (pizzaTemplateCategories == null || pizzaTemplateCategories.isEmpty()) {
								
							List<PizzaTemplate> pizzaTemplates2 = pizzaTemplateRepository
										.findByDescriptionAndMerchantId(pizzaTemplate.getDescription(),
												category.getMerchant().getId());
								for (PizzaTemplate pizzaTemplate2 : pizzaTemplates2) {
									PizzaTemplateCategory pizzaTemplateCategory = new PizzaTemplateCategory();
									pizzaTemplateCategory.setCategory(category);
									pizzaTemplateCategory.setPizzaTemplate(pizzaTemplate2);
									pizzaTemplateCategoryRepository.save(pizzaTemplateCategory);
								}
							
								} else {
								// pizzaTemplateCategoryRepository.deleteInBatch(pizzaTemplateCategories);
								List<PizzaTemplate> pizzaTemplates2 = pizzaTemplateRepository
										.findByDescriptionAndMerchantId(pizzaTemplate.getDescription(),
												category.getMerchant().getId());

								for (PizzaTemplateCategory pizzaTemplateCategory : pizzaTemplateCategories) {
									for (PizzaTemplate pizzaTemplate2 : pizzaTemplates2) {
										pizzaTemplateCategory.setCategory(category);
										pizzaTemplateCategory.setPizzaTemplate(pizzaTemplate2);
										pizzaTemplateCategoryRepository.save(pizzaTemplateCategory);
									}
								}
							}
						//}
						//pizzaTemplateRepository.save(pizzaTemplate);
					}
						//}
				*/
					Set<Item> itemss = category.getItems();
					if(itemss!=null){
						category.setItems(null);
						Category dbCategory = categoryRepository.save(category);
						LOGGER.info(" ImportExcelServiceImpl : saveInvetoryDataOfExcelSheet: saved category:");

						for (Item itemm : itemss) {
							List<PizzaTemplate> pizzaTemplates2 = pizzaTemplateRepository
									.findByDescriptionAndMerchantId(itemm.getName(),
											category.getMerchant().getId());
							for (PizzaTemplate pizzaTemplate2 : pizzaTemplates2) {
								
								PizzaTemplateCategory dePizzaTemplateCategory = pizzaTemplateCategoryRepository.findByPizzaTemplateIdAndCategoryId(pizzaTemplate2.getId(), 
										dbCategory.getId());
								if(dePizzaTemplateCategory != null){
									System.out.println("PizzaTemplate and Category mapping already exists..!!");
									LOGGER.info(" ImportExcelServiceImpl : saveInvetoryDataOfExcelSheet: PizzaTemplate and Category mapping already exists..!!:");

								}else{
									PizzaTemplateCategory pizzaTemplateCategory = new PizzaTemplateCategory();
									pizzaTemplateCategory.setCategory(category);
									pizzaTemplateCategory.setPizzaTemplate(pizzaTemplate2);
									pizzaTemplateCategoryRepository.save(pizzaTemplateCategory);
									LOGGER.info(" ImportExcelServiceImpl : saveInvetoryDataOfExcelSheet: saved PizzaTemplateCategory:");

								}
							}
						}
					}
				} else {
					Set<Item> itemss = category.getItems();
					if (itemss != null) {
						category.setItems(null);
						
						List<Category> categoryList = categoryRepository.findByMerchantIdAndName(category.getMerchant().getId(), category.getName());
						if(categoryList==null || categoryList.isEmpty()){
							categoryRepository.save(category);
							LOGGER.info(" ImportExcelServiceImpl : saveInvetoryDataOfExcelSheet: saved category:");

						}
						for (Item itemm : itemss) {
							List<Item> item2 = itemmRepository.findByMerchantIdAndName(category.getMerchant().getId(),
									itemm.getName());
							for (Item item : item2) {
								CategoryItem categoryItem = new CategoryItem();
								categoryItem.setCategory(category);
								categoryItem.setItem(item);
								categoryItem.setActive(1);
								categoryItemRepository.save(categoryItem);
								LOGGER.info(" ImportExcelServiceImpl : saveInvetoryDataOfExcelSheet: saved CategoryItem:");

							}
						}
					}
				}
			}
			response = "Inventory uploaded succesfully";
		} catch (Exception e) {
			LOGGER.error("error: " + e.getMessage());
			LOGGER.error("End :: ImportExcelServiceImpl : saveInvetoryDataOfExcelSheet: Exeption:"+e);

		}
		LOGGER.info("End :: ImportExcelServiceImpl : saveInvetoryDataOfExcelSheet:return response :"+response);

		return response;

	}

	public ArrayList<PizzaSize> setPizzaSize(Sheet sheet, Merchant merchant) {
		LOGGER.info("---------------- Start :: ImportExcelServiceImpl : setPizzaSize:----------------");

		ArrayList<PizzaSize> pizzaSizes = new ArrayList<PizzaSize>();
		LOGGER.info("ImportExcelServiceImpl : setPizzaSize: sheetLastRowSize"+sheet.getLastRowNum());
		LOGGER.info("ImportExcelServiceImpl : setPizzaSize: merchantId:"+merchant.getId());

		for (int j = 1; j <= sheet.getLastRowNum(); j++) {
			PizzaSize pizzaSize = new PizzaSize();

			Row row = sheet.getRow(j);
			if (row == null) {
				continue;
			}
			boolean hasValues = false;
			for (int k = 0; k <= row.getLastCellNum(); k++) {
				Cell cell = row.getCell(k);
				if (cell != null) {
					Object value = cellToObject(cell);

					hasValues = hasValues || value != null;
					switch (k) {

					case 0:
						if(pizzaSize!=null && value!=null)
						pizzaSize.setPosPizzaSizeId(Double.toString((Double) value));
						break;
					case 1:
						String sizeName = (String) value;
						LOGGER.info("ImportExcelServiceImpl : setPizzaSize: sizeName:"+sizeName);

						List<PizzaSize> sizes = pizzaSizeRepository.findByMerchantIdAndDescription(merchant.getId(),
								sizeName);
						if (sizes != null && !sizes.isEmpty()) {
							for (PizzaSize size : sizes) {
								pizzaSize = size;
								break;
							}
						} else {
							pizzaSize.setActive(1);
							pizzaSize.setDescription(sizeName);
							merchant.setItems(null);
							merchant.setAddresses(null);
							merchant.setTaxRates(null);
							merchant.setSocialMediaLinks(null);
							merchant.setPaymentModes(null);
							merchant.setOrderRs(null);
							merchant.setModifiers(null);
							merchant.setItems(null);
							merchant.setMerchantSubscriptions(null);
							merchant.setModifierGroups(null);
							merchant.setOpeningClosingDays(null);
							merchant.setOrderTypes(null);
							merchant.setSubscription(null);
							// merchant.setOwner(null);
							merchant.getOwner().setRole(null);
							merchant.setTimeZone(null);
							merchant.setVouchars(null);
							pizzaSize.setMerchant(merchant);
						}
						break;
					}
				} else {
				}
			}
			pizzaSizes.add(pizzaSize);
		}
		LOGGER.info("----------------End :: ImportExcelServiceImpl : setPizzaSize:------------------");

		return pizzaSizes;

	}
	
	public ArrayList<PizzaCrust> setPizzaCrust(Sheet sheet, Merchant merchant) {
		LOGGER.info("----------------Start :: ImportExcelServiceImpl : setPizzaCrust() :------------------");

		ArrayList<PizzaCrust> pizzaCrustList = new ArrayList<PizzaCrust>();
		List<PizzaTemplateCrust> pizzaTemplateCrusts=null;
		
		List<PizzaCrustSizes> pizzaCrustSizes=null;
		PizzaCrustSizes pizzaCrustSize=null;
		
		PizzaTemplateCrust templateCrust=null;
		boolean isDuplicatePizzaCrust =true;
		PizzaCrust duplicatePizzaCrust=null;
		PizzaCrust pizzaCrust=null;
		String crustId=null;
		LOGGER.info("ImportExcelServiceImpl : setPizzaCrust(): sheetLastRowSize:"+sheet.getLastRowNum());

		for (int j = 1; j <= sheet.getLastRowNum(); j++) {
			Row row = sheet.getRow(j);
			if (row == null) {
				continue;
			}
			boolean hasValues = false;
			templateCrust=new PizzaTemplateCrust();
			pizzaCrustSize=new PizzaCrustSizes();
			for (int k = 0; k <= row.getLastCellNum(); k++) {
				Cell cell = row.getCell(k);
				if (cell != null) {
					Object value = cellToObject(cell);
					hasValues = hasValues || value != null;
					
					switch (k) {
					case 0:if(value!=null){
						if (!isDuplicatePizzaCrust) {
							pizzaCrust.setPizzaTemplateCrusts(pizzaTemplateCrusts);
							pizzaCrustList.add(pizzaCrust);
						}
						duplicatePizzaCrust=new PizzaCrust();
						pizzaCrust = duplicatePizzaCrust;
						
						pizzaTemplateCrusts=new ArrayList<PizzaTemplateCrust>();
						pizzaCrustSizes=new ArrayList<PizzaCrustSizes>();
						
						
						pizzaCrust.setPosPizzaCrustId(Double.toString((Double) value));
						if (value != null) {
							crustId = (Double.toString((Double) value));
						}
						
					}
						break;

					case 1: if(value!=null){
						String crustName = (String) value;
						LOGGER.info("ImportExcelServiceImpl : setPizzaCrust(): crustName:"+crustName);

						List<PizzaCrust> crusts = pizzaCrustRepository
								.findByMerchantIdAndDescription(
										merchant.getId(), crustName);
						if (crusts != null && !crusts.isEmpty()) {
							for (PizzaCrust pizzaCrusts : crusts) {
								pizzaCrust = pizzaCrusts;
								break;
							}
						} else {
							pizzaCrust.setDescription(crustName);
							pizzaCrust.setMerchant(merchant);
						}
						
						duplicatePizzaCrust = pizzaCrust;
						isDuplicatePizzaCrust = false;
					}
						break;

					case 2: if(value!=null){
						String active = (String) value;
						if (active.equalsIgnoreCase("Yes"))
							pizzaCrust.setActive(IConstant.BOOLEAN_TRUE);
						else
							pizzaCrust.setActive(IConstant.BOOLEAN_FALSE);
					}
						break;

					case 3:if(value!=null){
						String templateName = (String) value;
						LOGGER.info("ImportExcelServiceImpl : setPizzaCrust(): templateName :"+templateName);

						List<PizzaTemplate> pizzaTemplateList =pizzaTemplateRepository.findByDescriptionAndMerchantId(templateName, merchant.getId());
						if(!pizzaTemplateList.isEmpty() && pizzaTemplateList.size()>0){
							for(PizzaTemplate pizzaTemplate: pizzaTemplateList){
								templateCrust.setPizzaTemplate(pizzaTemplate);
							}
							pizzaTemplateCrusts.add(templateCrust);
							pizzaCrust.setPizzaTemplateCrusts(pizzaTemplateCrusts);
						}else{
							PizzaTemplate pizzaTemplate=new PizzaTemplate();
							pizzaTemplate.setDescription(templateName);
							templateCrust.setPizzaTemplate(pizzaTemplate);
							templateCrust.setPizzaCrust(pizzaCrust);
							pizzaTemplateCrusts.add(templateCrust);
							pizzaCrust.setPizzaTemplateCrusts(pizzaTemplateCrusts);
						}
						
					}
						break;

					case 4:if(value!=null){
						String sizeName = (String) value;
						LOGGER.info("ImportExcelServiceImpl : setPizzaCrust(): sizeName :"+sizeName);

						List<PizzaSize> pizzaSizeList=pizzaSizeRepository.findByMerchantIdAndDescription(merchant.getId(), sizeName);
						LOGGER.info("ImportExcelServiceImpl : setPizzaCrust(): pizzaSizeListSize :"+pizzaSizeList.size());

						if(!pizzaSizeList.isEmpty() && pizzaSizeList.size()>0){
							for (PizzaSize pizzaSize : pizzaSizeList) {
								pizzaCrustSize.setPizzaSize(pizzaSize);
								pizzaCrustSize.setActive(1);
							}
							pizzaCrustSizes.add(pizzaCrustSize);
							pizzaCrust.setPizzaCrustSizes(pizzaCrustSizes);
						}else{
							PizzaSize pizzaSize= new PizzaSize();
							pizzaSize.setDescription(sizeName);
							pizzaCrustSize.setPizzaSize(pizzaSize);
							pizzaCrustSize.setPizzaCrust(pizzaCrust);
							pizzaCrustSize.setActive(1);
							pizzaCrustSizes.add(pizzaCrustSize);
							pizzaCrust.setPizzaCrustSizes(pizzaCrustSizes);
							
						}
					}
						break;

					case 5:if(value!=null){
						Double price = (Double) value;
						pizzaCrustSize.setPrice(price);
					}
						break;
					}

				}else{
				}
			}
			if (j == sheet.getLastRowNum() && pizzaCrust!=null) {
				pizzaCrust.setPizzaTemplateCrusts(pizzaTemplateCrusts);
				pizzaCrust.setPizzaCrustSizes(pizzaCrustSizes);
				pizzaCrustList.add(pizzaCrust);
			}
		}
		LOGGER.info("--------------End :: ImportExcelServiceImpl : setPizzaCrust(): ---------------------");

		return pizzaCrustList;
	}

	public ArrayList<PizzaTemplate> setPizzaTemplate(Sheet sheet, Merchant merchant) {
		LOGGER.info("--------------Start :: ImportExcelServiceImpl : setPizzaTemplate(): ---------------------");

		List<PizzaTemplateSize> pizzaTemplateSizesList = null;
		ArrayList<PizzaTemplate> pizzaTemplates = new ArrayList<PizzaTemplate>();
		String templateId = null;
		PizzaTemplate dupliPizzaTemplate = null;
		PizzaTemplate pizzaTemplate = null;
		PizzaTemplateSize pizzaTemplateSize = null;
		boolean isDuplicatePizzaTemplate = true;
		Set<TaxRates> taxRates = null;
		LOGGER.info("ImportExcelServiceImpl : setPizzaTemplate():sheetLastRowSize:"+sheet.getLastRowNum());

		for (int j = 1; j <= sheet.getLastRowNum(); j++) {
			Row row = sheet.getRow(j);
			if (row == null) {
				continue;
			}
			boolean hasValues = false;
			pizzaTemplateSize = new PizzaTemplateSize();
			for (int k = 0; k <= row.getLastCellNum(); k++) {

				Cell cell = row.getCell(k);

				if (cell != null) {
					Object value = cellToObject(cell);
					hasValues = hasValues || value != null;

					if (k == 0 && value != null && templateId != null) {
						if (!(((Double) value).toString()).equals(templateId)) {
							isDuplicatePizzaTemplate = false;
						} else {
							isDuplicatePizzaTemplate = true;
						}
					}
					switch (k) {
					case 0:
						if (value != null) {
							if (!isDuplicatePizzaTemplate) {
								pizzaTemplate.setPizzaTemplateSizes(pizzaTemplateSizesList);
								pizzaTemplate.setTaxes(taxRates);
								pizzaTemplates.add(pizzaTemplate);
							}

							dupliPizzaTemplate = new PizzaTemplate();
							pizzaTemplate = dupliPizzaTemplate;

							pizzaTemplateSizesList = new ArrayList<PizzaTemplateSize>();
							pizzaTemplate.setPosPizzaTemplateId(Double.toString((Double) value));
							if (value != null) {
								templateId = (Double.toString((Double) value));
							}
						}
						break;
					case 1:
						if (value != null) {
							String templateName = (String) value;
							LOGGER.info("ImportExcelServiceImpl : setPizzaTemplate():templateName:"+templateName+":merchantId:"+merchant.getId());

							List<PizzaTemplate> templates = pizzaTemplateRepository
									.findByDescriptionAndMerchantId(templateName, merchant.getId());
							if (templates != null && !templates.isEmpty()) {
								for (PizzaTemplate template : templates) {
									pizzaTemplate = template;
									break;
								}
							} else {
								// pizzaTemplate=new PizzaTemplate();
								pizzaTemplate.setMerchant(merchant);
								pizzaTemplate.setDescription((String) value);
							}
							dupliPizzaTemplate = pizzaTemplate;
							taxRates = new HashSet<TaxRates>();
							isDuplicatePizzaTemplate = false;
						}
						break;
					case 2:
						if (value != null) {
							String active = (String) value;
							if (active.equalsIgnoreCase("Yes"))
								pizzaTemplate.setActive(IConstant.BOOLEAN_TRUE);
							else
								pizzaTemplate.setActive(IConstant.BOOLEAN_FALSE);
						}
						break;

					case 3:
						if (value != null) {
							String size = (String) value;
							LOGGER.info("ImportExcelServiceImpl : setPizzaTemplate():size:"+size+":merchantId:"+merchant.getId());

							List<PizzaSize> pizzaSizes = pizzaSizeRepository
									.findByMerchantIdAndDescription(merchant.getId(), size);
							LOGGER.info("ImportExcelServiceImpl : setPizzaTemplate():pizzaSizesSize:"+pizzaSizes.size());

							if (pizzaSizes != null & pizzaSizes.size() > 0) {
								for (PizzaSize pizzaSize : pizzaSizes) {
									pizzaTemplateSize.setPizzaSize(pizzaSize);
								}
								pizzaTemplateSizesList.add(pizzaTemplateSize);
								pizzaTemplate.setPizzaTemplateSizes(pizzaTemplateSizesList);
							} else {
								PizzaSize pizzaSize = new PizzaSize();
								pizzaSize.setDescription(size);
								pizzaSize.setMerchant(merchant);
								pizzaTemplateSize.setPizzaTemplate(pizzaTemplate);
								pizzaTemplateSize.setPizzaSize(pizzaSize);
								pizzaTemplateSizesList.add(pizzaTemplateSize);
							}
						}
						break;

					case 4:
						if (value != null) {
							pizzaTemplateSize.setPrice((Double) value);
						}
						break;

					case 5:
						if (value != null) {
							String taxName = (String) value;
							if (taxName.toUpperCase().equals("DEFAULT")) {
								pizzaTemplate.setIsDefaultTaxRates(true);
								List<TaxRates> detaulTaxList = taxRateRepository
										.findByMerchantIdAndIsDefault(merchant.getId(), 1);
								if (!detaulTaxList.isEmpty() && detaulTaxList != null) {
									Set<TaxRates> detaulTaxSet = new HashSet<TaxRates>(detaulTaxList);
									if (taxRates == null) {
										taxRates = new HashSet<TaxRates>();
									}
									taxRates.addAll(detaulTaxSet);
								} else {
									if (taxRates == null) {
										taxRates = new HashSet<TaxRates>();
									}
									TaxRates rates = new TaxRates();
									rates.setName(taxName);
									taxRates.add(rates);
								}
							} else {
								LOGGER.info("ImportExcelServiceImpl : setPizzaTemplate():taxName:"+taxName+":mercantId:"+merchant.getId());

								List<TaxRates> detaulTaxList = taxRateRepository
										.findByMerchantIdAndName(merchant.getId(), taxName);
								if (!detaulTaxList.isEmpty() && detaulTaxList != null) {
									Set<TaxRates> detaulTaxSet = new HashSet<TaxRates>(detaulTaxList);
									if (taxRates == null) {
										taxRates = new HashSet<TaxRates>();
									}
									taxRates.addAll(detaulTaxSet);
								} else {
									if (taxRates == null) {
										taxRates = new HashSet<TaxRates>();
									}
									TaxRates rates = new TaxRates();
									rates.setName(taxName);
									taxRates.add(rates);
								}
							}
						}
						break;
					}
				} else {
				}
			}
			if (j == sheet.getLastRowNum()) {
				pizzaTemplate.setPizzaTemplateSizes(pizzaTemplateSizesList);
				pizzaTemplate.setTaxes(taxRates);
				pizzaTemplates.add(pizzaTemplate);
			}
		}
		LOGGER.info("-------------------End :: ImportExcelServiceImpl : setPizzaTemplate():-----------------------");

		return pizzaTemplates;

	}
	
	
	

	public ArrayList<PizzaTopping> setPizzaTopping(Sheet sheet, Merchant merchant) {
		LOGGER.info("-------------------Start :: ImportExcelServiceImpl : setPizzaTopping():-----------------------");
		LOGGER.info("ImportExcelServiceImpl : setPizzaTopping():merchantId:"+merchant.getId());

		List<PizzaToppingSize> pizzaToppingSizes = null;
		ArrayList<PizzaTopping> pizzaToppings = new ArrayList<PizzaTopping>();
		String toppingId = null;
		PizzaTopping dupliPizzaTopping = null;
		PizzaTopping pizzaTopping = null;
		PizzaToppingSize pizzaToppingSize = null;
		PizzaTemplateTopping pizzaTemplateTopping=null;
		List<PizzaTemplateTopping> pizzaTemplateToppings=null;
		boolean isDuplicatePizzaTopping = true;
		LOGGER.info("ImportExcelServiceImpl : setPizzaTopping():sheetLastRowSize:"+sheet.getLastRowNum());

		for (int j = 1; j <= sheet.getLastRowNum(); j++) {
			Row row = sheet.getRow(j);
			if (row == null) {
				continue;
			}
			boolean hasValues = false;
			pizzaToppingSize = new PizzaToppingSize();
			pizzaTemplateTopping=new PizzaTemplateTopping();
			for (int k = 0; k <= row.getLastCellNum(); k++) {

				Cell cell = row.getCell(k);
				if (cell != null) {
					Object value = cellToObject(cell);
					hasValues = hasValues || value != null;

					if (k == 0 && value != null && toppingId != null) {
						if (!(((Double) value).toString()).equals(toppingId)) {
							isDuplicatePizzaTopping = false;
						} else {
							isDuplicatePizzaTopping = true;
						}
					}
					switch (k) {
					case 0:
						if (value != null) {
							if (!isDuplicatePizzaTopping) {
								pizzaTopping.setPizzaTemplateToppings(pizzaTemplateToppings);
								pizzaTopping.setPizzaToppingSizes(pizzaToppingSizes);
								pizzaToppings.add(pizzaTopping);
							}
							dupliPizzaTopping = new PizzaTopping();
							pizzaTopping = dupliPizzaTopping;
							pizzaToppingSizes = new ArrayList<PizzaToppingSize>();
							pizzaTemplateToppings=new ArrayList<PizzaTemplateTopping>();

							pizzaTopping.setPosPizzaToppingId(Double.toString((Double) value));
							if (value != null) {
								toppingId = (Double.toString((Double) value));
							}
						}
						break;

					case 1:
						if (value != null) {
							String toppingName = (String) value;
							LOGGER.info("ImportExcelServiceImpl : setPizzaTopping():toppingName:"+toppingName);

							List<PizzaTopping> topping = pizzaToppingRepository
									.findByDescriptionAndMerchantId(toppingName, merchant.getId());
							if (topping != null && !topping.isEmpty()) {
								for (PizzaTopping pizzaTopping2 : topping) {
									pizzaTopping = pizzaTopping2;
									break;
								}

							} else {
								pizzaTopping.setDescription(toppingName);
								pizzaTopping.setMerchant(merchant);
							}
							dupliPizzaTopping = pizzaTopping;
							isDuplicatePizzaTopping = false;
						} else {
							isDuplicatePizzaTopping = true;
						}
						break;

					case 2:
						if (value != null) {
							String active = (String) value;
							if (active.equalsIgnoreCase("Yes")) {
								pizzaTopping.setActive(IConstant.BOOLEAN_TRUE);
							} else {
								pizzaTopping.setActive(IConstant.BOOLEAN_FALSE);
							}
						}
						break;

					case 3:
						if (value != null) {
							String templateName = (String) value;
							LOGGER.info("ImportExcelServiceImpl : setPizzaTopping():templateName:"+templateName);

							List<PizzaTemplate> tamplates = pizzaTemplateRepository
									.findByDescriptionAndMerchantId(templateName, merchant.getId());
							LOGGER.info("ImportExcelServiceImpl : setPizzaTopping():PizzaTemplateSize:"+ tamplates.size() );

							if (tamplates != null & tamplates.size() > 0) {
								for (PizzaTemplate tamplate : tamplates) {
									/*if (pizzaTopping != null && pizzaTopping.getId() != null) {
										PizzaTemplateTopping templateTopping = pizzaTemplateToppingRepository
												.findByPizzaTemplateIdAndPizzaToppingId(tamplate.getId(),
														pizzaTopping.getId());
										if (templateTopping != null) {
											pizzaTemplateToppings.add(templateTopping);
											break;
										} 
									}*/
									pizzaTemplateTopping.setPizzaTemplate(tamplate);
								}
								pizzaTemplateToppings.add(pizzaTemplateTopping);
								pizzaTopping.setPizzaTemplateToppings(pizzaTemplateToppings);
								
							}else{
								PizzaTemplate pizzaTemplate=new PizzaTemplate();
								pizzaTemplate.setDescription(templateName);
								pizzaTemplate.setMerchant(merchant);
								pizzaTemplateTopping.setPizzaTemplate(pizzaTemplate);
								pizzaTemplateTopping.setPizzaTopping(pizzaTopping);
								pizzaTemplateToppings.add(pizzaTemplateTopping);
							}
						}
						break;

					case 4:
						if (value != null) {
							String size = (String) value;
							LOGGER.info("ImportExcelServiceImpl : setPizzaTopping():size:"+size);

							List<PizzaSize> pizzaSizes = pizzaSizeRepository
									.findByMerchantIdAndDescription(merchant.getId(), size);
							LOGGER.info("ImportExcelServiceImpl : setPizzaTopping(): pizzaSizesSize:"+ pizzaSizes.size());

							if (pizzaSizes != null & pizzaSizes.size() > 0) {
								for (PizzaSize pizzaSize : pizzaSizes) {
									pizzaToppingSize.setPizzaSize(pizzaSize);
								}
								pizzaToppingSizes.add(pizzaToppingSize);
								pizzaTopping.setPizzaToppingSizes(pizzaToppingSizes);
							} else {
								PizzaSize pizzaSize = new PizzaSize();
								pizzaSize.setDescription(size);
								pizzaSize.setMerchant(merchant);
								pizzaToppingSize.setPizzaTopping(pizzaTopping);
								pizzaToppingSize.setPizzaSize(pizzaSize);
								pizzaToppingSizes.add(pizzaToppingSize);
							}
						}
						break;

					case 5:
						if (value != null) {
							Double price = (Double) value;
							pizzaToppingSize.setPrice(price);
						}
						break;
					}
				} else {
				}
			}
			if (j == sheet.getLastRowNum()) {
					pizzaTopping.setPizzaTemplateToppings(pizzaTemplateToppings);
					pizzaTopping.setPizzaToppingSizes(pizzaToppingSizes);
					pizzaToppings.add(pizzaTopping);
			}
		}
		LOGGER.info("-----------End :: ImportExcelServiceImpl : setPizzaTopping():---------------------");

		return pizzaToppings;
	}
	public String saveOfflineCustomerByExcel(ExcelToJsonConverterConfig config, Merchant merchant, HttpSession session)
			throws InvalidFormatException, IOException, ParseException 
	{
		String attributeCounts = "";
		Integer mailCount=0;
		Integer customerCount=0;
		try{
			
			Date date1 = (merchant != null && merchant.getTimeZone() != null
					&& merchant.getTimeZone().getTimeZoneCode() != null)
							? DateUtil.getCurrentDateForTimeZonee(merchant.getTimeZone().getTimeZoneCode())
							: new Date();
		LOGGER.info("Start: ImportExcelServiceImpl saveOfflineCustomerByExcel()");
		if(merchant!=null && merchant.getId()!=null)
		LOGGER.info("ImportExcelServiceImpl saveOfflineCustomerByExcel() merchent Id : "+merchant.getId());

		String SheetColumns[] = { "Order Id","First Name", "Last Name", "Email" , "Phone Number", "Order Name", "Order Price", "Order Date","Order Type" ,"Payment Method"};
		int noOfSheetColumns = 10;
		
		
		this.config = config;
		String response = "";

		ExcelWorkbook book = new ExcelWorkbook();

		InputStream inp = new FileInputStream(config.getSourceFile());
		LOGGER.info(" ImportExcelServiceImpl saveOfflineCustomerByExcel() : ");
		Workbook wb = WorkbookFactory.create(inp);
		LOGGER.info(" ImportExcelServiceImpl saveOfflineCustomerByExcel() : config.getSourceFile() " +config.getSourceFile());
		book.setFileName(config.getSourceFile());

		int loopLimit = wb.getNumberOfSheets();
		LOGGER.info(" ImportExcelServiceImpl wb.getNumberOfSheets() loopLimit : "+loopLimit);
		LOGGER.info(": ImportExcelServiceImpl config.getNumberOfSheets() : " +config.getNumberOfSheets());
		if (loopLimit <= config.getNumberOfSheets())
		{
			System.out.println("Customers sheet name :"+wb.getSheetIndex("Customers"));
			LOGGER.info(" ImportExcelServiceImpl saveOfflineCustomerByExcel() Customers sheet nam : "+wb.getSheetIndex("Customers"));
			int excelSheetIndex = wb.getSheetIndex("Customers");
			LOGGER.info(" ImportExcelServiceImpl saveOfflineCustomerByExcel() excelSheetIndex: " +excelSheetIndex);
			if ((excelSheetIndex == 0)) 
			{
				for (int i = 0; i < loopLimit; i++)
				{
					LOGGER.info("Start read excel sheet: ImportExcelServiceImpl saveOfflineCustomerByExcel() :Sheet  "+i);
					Sheet sheet = wb.getSheetAt(i);
					if (sheet == null)
					{
						continue;
					}
					ExcelWorksheet tmp = new ExcelWorksheet();
					tmp.setName(sheet.getSheetName());
					if (sheet.getSheetName().equals("Customers"))
					{
						boolean status = validateColumns(sheet, noOfSheetColumns, SheetColumns);
						LOGGER.info("Start read excel sheet: ImportExcelServiceImpl saveOfflineCustomerByExcel() :status  "+status);
						System.out.println(status);
						if (status) 
						{
							for (int j = 1; j <= sheet.getLastRowNum(); j++) {
								LOGGER.info("Start read excel sheet: ImportExcelServiceImpl saveOfflineCustomerByExcel() :row  "+j);
								//List<OrderR> orderList=new ArrayList<OrderR>();
								Customer customer = new Customer();
								OrderR orderR=new OrderR();
								customer.setMerchantt(merchant);
								customer.setCustomerType("kritiq");
								Row row = sheet.getRow(j);
								if (row == null) {
									continue;
								}
								boolean hasValues = false;
								
								for (int k = 0; k <= row.getLastCellNum(); k++) {
									LOGGER.info("Start read excel sheet: ImportExcelServiceImpl saveOfflineCustomerByExcel() :column  "+k);
									Cell cell = row.getCell(k);
									if (cell != null) {
										Object value = cellToObject(cell);
										hasValues = hasValues || value != null;
										switch (k) {
										case 0:
											String orderId =String.format("%.0f", value);
											if(orderId!=null && !orderId.isEmpty())
												orderR.setOrderPosId(orderId);
											break;
										case 1:
											String firstName = (String) value;
											if(firstName!=null && !firstName.isEmpty())
											customer.setFirstName(firstName);
											break;
										case 2:
											String lastName = (String) value;
											if(lastName!=null && !lastName.isEmpty())
											customer.setLastName(lastName);
											break;
										case 3:
											String emailId = (String) value;
											if(emailId!=null && !emailId.isEmpty())
											customer.setEmailId(emailId);
											break;
										case 4:
											cell.setCellType(Cell.CELL_TYPE_STRING);
								            String phone= cell.getStringCellValue();
								            if(phone!=null && !phone.isEmpty())
											customer.setPhoneNumber(phone);
											break;
										case 5:
											String orderName = (String) value;
											if(orderName!=null && !orderName.isEmpty())
											orderR.setOrderName(orderName);
											break;
										case 6:
											try{
											Double orderPrice = (Double) value;
											if(orderPrice!=null )
											orderR.setOrderPrice(orderPrice);
											}catch (Exception e) {
												orderR.setOrderPrice(0d);
											}
											break;
										case 7:
											try{
											if(value!=null){
											DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
											String date = dateFormat.format(value); 
											Date orderDate=new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").parse(date);
											 if(orderDate!=null)
											orderR.setCreatedOn(orderDate);
											}orderR.setCreatedOn(date1);
											}catch (Exception e) {
												orderR.setCreatedOn(date1);
											}
											break;
										case 8:
											String orderType = (String) value;
											   if(orderType!=null && !orderType.isEmpty())
											   orderR.setOrderType(orderType);
											break;
										case 9:
											String paymentMethod = (String) value;
											if(paymentMethod!=null && !paymentMethod.isEmpty())
											orderR.setPaymentMethod(paymentMethod);
											break;
										}
									} 
								}
							
								if(customer.getFirstName()!=null && customer.getEmailId()!=null &&customer.getPhoneNumber()!=null)
								{
									List<Customer> customersByPhone=customerrRepository.findByPhoneNumberAndMerchanttId(customer.getPhoneNumber(),merchant.getId());
									List<Customer> customersByEmail=customerrRepository.findByEmailIdAndMerchantId(customer.getPhoneNumber(),merchant.getId());
									 String customerUld=null;
									 Boolean flag=false;
									if((customersByPhone!=null && !customersByPhone.isEmpty() && customersByPhone.size()>0))
									{
										if(customersByPhone.get(0)!=null)
										{
										 customer=	ProducerUtil.customerPojo(customer,customersByPhone.get(0));
										 if(customersByPhone.get(0).getCustomerUid()!=null && !customersByPhone.get(0).getCustomerUid().isEmpty())
										 customerUld=customersByPhone.get(0).getCustomerUid();
										 else {
												 flag=true;
												 String customerEntityDTOJson = ProducerUtil.getCustomerEntityDTO(customer);
											     customerUld=	ProducerUtil.createKritiqCustomerMkonnektPlatform(customerEntityDTOJson, merchant.getMerchantUid(),environment);
											     LOGGER.info("Start read excel sheet: ImportExcelServiceImpl saveOfflineCustomerByExcel() :createcustomer  customerUld "+customerUld);
											}
										}
										
									}else if(customersByEmail!=null && !customersByEmail.isEmpty() && customersByEmail.size()>0)
									{
										if(customersByEmail.get(0)!=null)
										{
											customer=	ProducerUtil.customerPojo(customer,customersByEmail.get(0));
										  if(customersByEmail.get(0).getCustomerUid()!=null && !customersByEmail.get(0).getCustomerUid().isEmpty())
											 customerUld=customersByEmail.get(0).getCustomerUid();
											else {
												flag=true;
												  String customerEntityDTOJson = ProducerUtil.getCustomerEntityDTO(customer);
												  customerUld=ProducerUtil.createKritiqCustomerMkonnektPlatform(customerEntityDTOJson, merchant.getMerchantUid(),environment);
												  LOGGER.info("Start read excel sheet: ImportExcelServiceImpl saveOfflineCustomerByExcel() :createcustomer  customerUld "+customerUld);
											}
										}
									}else {
										flag=true;
										String customerEntityDTOJson = ProducerUtil.getCustomerEntityDTO(customer);
										customerUld=ProducerUtil.createKritiqCustomerMkonnektPlatform(customerEntityDTOJson, merchant.getMerchantUid(),environment);
										 LOGGER.info("Start read excel sheet: ImportExcelServiceImpl saveOfflineCustomerByExcel() :createcustomer  customerUld "+customerUld);
									}
										if(flag && customerUld!=null)
										{	
											customer.setCustomerUid(customerUld);
									        customer=customerrRepository.save(customer);
									        
										}else LOGGER.info("Start read excel sheet: ImportExcelServiceImpl saveOfflineCustomerByExcel() :customer already Exist  ");
										orderR.setCustomer(customer);
								        orderR.setMerchant(merchant);
								        orderR=orderRepository.save(orderR);
								        LOGGER.info("Start read excel sheet: ImportExcelServiceImpl saveOfflineCustomerByExcel() :order  created ");
								        if(orderR.getOrderPosId()==null || orderR.getOrderPosId().isEmpty())
								        orderR.setOrderPosId(orderR.getId().toString());
								        try
								        {
								        mailSendUtil.sendOrderReceiptAndFeedbackEmail(orderR, null,environment);
								        mailCount++;
								       }catch(Exception e)
								        {
								    	   MailSendUtil.sendExceptionByMail(e,environment);
								    	   LOGGER.error("End:: read excel sheet: ImportExcelServiceImpl saveOfflineCustomerByExcel() : Exception While Sending mail  "+e);
								    	   LOGGER.error("error: " + e.getMessage());
								        }
								        if(orderR!=null && orderR.getCustomer()!=null && orderR.getCustomer().getEmailId()!=null)
								        LOGGER.info("Start read excel sheet: ImportExcelServiceImpl saveOfflineCustomerByExcel() :Kritiq Mail sent on " +orderR.getCustomer().getEmailId());
									
								}
								LOGGER.info(" read excel sheet: ImportExcelServiceImpl saveOfflineCustomerByExcel() : end row  "+i);
								customerCount++;
							}
							System.out.println("SHEET UPLOADED SUCCESSFULLY");
							
							attributeCounts="SHEET UPLOADED SUCCESSFULLY with Total CustomerCount : "+customerCount +"  : Total MailSendCount"+mailCount;
							LOGGER.info(" read excel sheet: ImportExcelServiceImpl saveOfflineCustomerByExcel() : SHEET UPLOADED SUCCESSFULLY  ");
							LOGGER.info(" read excel sheet: ImportExcelServiceImpl saveOfflineCustomerByExcel() : TotalCustomerCount  " +customerCount);
							LOGGER.info(" read excel sheet: ImportExcelServiceImpl saveOfflineCustomerByExcel() : TotalSendMailCount  " +mailCount);
							
						}
						else 
						{
							    LOGGER.info("Start: ImportExcelServiceImpl saveOfflineCustomerByExcel() please Check ecxcel sheet format ");
								attributeCounts = "In Customers sheet: Please make sure you have the  the Coloumn Headers in the following order:  Order Id,First Name, Last Name, Email , Phone Number, Order Name, Order Price, Order Date,Order Type ,Payment Method";
								break;
						}
					}
					else
					{
						attributeCounts = "Please Make Sure Your Sheet Name as \"Customers\"";
						break;
					}					
					book.addExcelWorksheet(tmp);
					break;
				}
				}	
				else
				{
					attributeCounts = "Please Make Sure Your Sheet Name as \"Customers\"";
				}
			}

		System.out.println(book.toJson(true));
		}catch(Exception e)
		{
			LOGGER.error("End: ImportExcelServiceImpl saveOfflineCustomerByExcel() Exception  : "+e);	
			MailSendUtil.sendExceptionByMail(e,environment);
			attributeCounts="please Check ecxcel sheet format & Data which you insert into Excel sheet";
		}
		LOGGER.info("---------------End: ImportExcelServiceImpl saveOfflineCustomerByExcel():--------------");	

		return attributeCounts;
	}
	public void kritiqFeedbackByGranbury(Customer customer,Merchant merchant,OrderR orderR)
	{
		LOGGER.info("Start  : kritiqFeedbackByGranbury :");
		if(customer.getFirstName()!=null && customer.getEmailId()!=null &&customer.getPhoneNumber()!=null && !customer.getFirstName().isEmpty() && !customer.getEmailId().isEmpty() && !customer.getPhoneNumber().isEmpty())
		{
			List<Customer> customersByPhone=customerrRepository.findByPhoneNumberAndMerchanttId(customer.getPhoneNumber(),merchant.getId());
			List<Customer> customersByEmail=customerrRepository.findByEmailIdAndMerchantId(customer.getPhoneNumber(),merchant.getId());
			 String customerUld=null;
			 Boolean flag=false;
			if((customersByPhone!=null && !customersByPhone.isEmpty() && customersByPhone.size()>0))
			{
				if(customersByPhone.get(0)!=null)
				{
				 customer=	ProducerUtil.customerPojo(customer,customersByPhone.get(0));
				 if(customersByPhone.get(0).getCustomerUid()!=null && !customersByPhone.get(0).getCustomerUid().isEmpty())
				 customerUld=customersByPhone.get(0).getCustomerUid();
				 else {
						 flag=true;
						 String customerEntityDTOJson = ProducerUtil.getCustomerEntityDTO(customer);
					     customerUld=	ProducerUtil.createKritiqCustomerMkonnektPlatform(customerEntityDTOJson, merchant.getMerchantUid(),environment);
					     LOGGER.info(" : kritiqFeedbackByGranbury : customerUld "+customerUld);
					}
				}
				
			}else if(customersByEmail!=null && !customersByEmail.isEmpty() && customersByEmail.size()>0)
			{
				if(customersByEmail.get(0)!=null)
				{
					customer=	ProducerUtil.customerPojo(customer,customersByEmail.get(0));
				  if(customersByEmail.get(0).getCustomerUid()!=null && !customersByEmail.get(0).getCustomerUid().isEmpty())
					 customerUld=customersByEmail.get(0).getCustomerUid();
					else {
						flag=true;
						  String customerEntityDTOJson = ProducerUtil.getCustomerEntityDTO(customer);
						  customerUld=ProducerUtil.createKritiqCustomerMkonnektPlatform(customerEntityDTOJson, merchant.getMerchantUid(),environment);
						  LOGGER.info(" : kritiqFeedbackByGranbury : customerUld "+customerUld);
					}
				}
			}else {
				flag=true;
				String customerEntityDTOJson = ProducerUtil.getCustomerEntityDTO(customer);
				customerUld=ProducerUtil.createKritiqCustomerMkonnektPlatform(customerEntityDTOJson, merchant.getMerchantUid(),environment);
				 LOGGER.info(" : kritiqFeedbackByGranbury : customerUld "+customerUld);
			}
				if(flag && customerUld!=null)
				{	
					customer.setCustomerUid(customerUld);
			        customer=customerrRepository.save(customer);
			        
				}else  LOGGER.info(" : kritiqFeedbackByGranbury : csutomer already exist ");
				
				Date today = (merchant != null && merchant.getTimeZone() != null
						&& merchant.getTimeZone().getTimeZoneCode() != null)
								? DateUtil.getCurrentDateForTimeZonee(merchant.getTimeZone().getTimeZoneCode())
								: new Date();
				orderR.setCustomer(customer);
		        orderR.setMerchant(merchant);
		        orderR.setIsDefaults(0);
		        orderR.setCreatedOn(today);
		        orderR.setOrderType("Granbury");
		        orderR=orderRepository.save(orderR);
		        LOGGER.info(" : kritiqFeedbackByGranbury : order  created  ");
		        if(orderR.getOrderPosId()==null || orderR.getOrderPosId().isEmpty())
		        orderR.setOrderPosId(orderR.getId().toString());
		        try
		        {
		        mailSendUtil.sendOrderReceiptAndFeedbackEmail(orderR, null,environment);
		       }catch(Exception e)
		        {
		    	   MailSendUtil.sendExceptionByMail(e,environment);
		    	   LOGGER.info(" : kritiqFeedbackByGranbury : Exception  "+e);
		    	   LOGGER.error("error: " + e.getMessage());
		        }
		        if(orderR!=null && orderR.getCustomer()!=null && orderR.getCustomer().getEmailId()!=null)
		        LOGGER.info(" kritiqFeedbackByGranbury() :Kritiq Mail sent on " +orderR.getCustomer().getEmailId());
			
		}else LOGGER.info(" : kritiqFeedbackByGranbury : data missing");
	}
}




