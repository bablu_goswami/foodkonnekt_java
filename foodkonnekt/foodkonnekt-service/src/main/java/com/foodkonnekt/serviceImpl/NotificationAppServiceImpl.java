package com.foodkonnekt.serviceImpl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.foodkonnekt.model.NotificationApp;
import com.foodkonnekt.repository.NotificationAppRepository;
import com.foodkonnekt.service.NotificationAppService;

@Service
public class NotificationAppServiceImpl implements NotificationAppService {
	
	private static final Logger LOGGER= LoggerFactory.getLogger(NotificationAppServiceImpl.class);

	@Autowired
	private NotificationAppRepository notificationAppRepository;

	public NotificationApp FindById(Integer id) {
		LOGGER.info("===============  NotificationAppServiceImpl : Inside FindById :: Start  =============id "+id);

		NotificationApp notificationApp=null;
		try{
			
			 notificationApp =notificationAppRepository.findById(id);
			
		}catch(Exception e)
		{
			LOGGER.error("===============  NotificationAppServiceImpl : Inside FindById :: Exception  ============= " + e);

			LOGGER.error("error: " + e.getMessage());
			
		}
		LOGGER.info("===============  NotificationAppServiceImpl : Inside FindById :: End  ============= ");

		return notificationApp;
	}

}
