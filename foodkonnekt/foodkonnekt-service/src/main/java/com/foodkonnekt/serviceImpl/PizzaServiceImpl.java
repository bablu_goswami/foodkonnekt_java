package com.foodkonnekt.serviceImpl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.foodkonnekt.clover.vo.PersonJsonObject;
import com.foodkonnekt.model.Category;
import com.foodkonnekt.model.Merchant;
import com.foodkonnekt.model.ModifierGroup;
import com.foodkonnekt.model.ModifierModifierGroupDto;
import com.foodkonnekt.model.Modifiers;
import com.foodkonnekt.model.PizzaCrust;
import com.foodkonnekt.model.PizzaCrustSizes;
import com.foodkonnekt.model.PizzaSize;
import com.foodkonnekt.model.PizzaTemplate;
import com.foodkonnekt.model.PizzaTemplateCategory;
import com.foodkonnekt.model.PizzaTemplateCrust;
import com.foodkonnekt.model.PizzaTemplateSize;
import com.foodkonnekt.model.PizzaTemplateTax;
import com.foodkonnekt.model.PizzaTemplateTopping;
import com.foodkonnekt.model.PizzaTopping;
import com.foodkonnekt.model.PizzaToppingSize;
import com.foodkonnekt.model.TaxRates;
import com.foodkonnekt.repository.PizzaCrustRepository;
import com.foodkonnekt.repository.PizzaCrustSizeRepository;
import com.foodkonnekt.repository.PizzaSizeRepository;
import com.foodkonnekt.repository.PizzaTemplateCategoryRepository;
import com.foodkonnekt.repository.PizzaTemplateCrustRepository;
import com.foodkonnekt.repository.PizzaTemplateRepository;
import com.foodkonnekt.repository.PizzaTemplateSizeRepository;
import com.foodkonnekt.repository.PizzaTemplateTaxRepository;
import com.foodkonnekt.repository.PizzaTemplateToppingRepository;
import com.foodkonnekt.repository.PizzaToppingRepository;
import com.foodkonnekt.repository.PizzaToppingSizeRepository;
import com.foodkonnekt.repository.TaxRateRepository;
import com.foodkonnekt.service.PizzaService;
import com.foodkonnekt.util.IConstant;
import com.foodkonnekt.util.MailSendUtil;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Service
public class PizzaServiceImpl implements PizzaService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(PizzaServiceImpl.class);
	
	@Autowired
	private Environment environment;
	
	@Autowired
	private PizzaSizeRepository pizzaSizeRepository;
	
	@Autowired
	private PizzaTemplateRepository pizzaTemplateRepository;
	
	@Autowired
	private PizzaTemplateSizeRepository pizzaTemplateSizeRepository;
	
	@Autowired
	private PizzaToppingRepository pizzaToppingRepository;
	
	@Autowired
	private PizzaToppingSizeRepository pizzaToppingSizeRepository;
	
	@Autowired
	private PizzaTemplateToppingRepository pizzaTemplateToppingRepository;
	
	@Autowired
	private PizzaCrustSizeRepository pizzaCrustSizeRepository;
	
	@Autowired
	private PizzaTemplateCrustRepository pizzaTemplateCrustRepository;
	
	@Autowired
	private PizzaCrustRepository pizzaCrustRepository;
	
	@Autowired
	private PizzaTemplateCategoryRepository pizzaTemplateCategoryRepository;
	
	@Autowired
	private TaxRateRepository taxRateRepository;
	
	@Autowired
	private PizzaTemplateTaxRepository pizzaTemplateTaxRepository;
	
	public List<PizzaSize> findAllSizes(Integer id) {
		LOGGER.info("===============  PizzaServiceImpl : Inside findAllSizes :: Start  ============= ");
		LOGGER.info(" === id == " + id);
		LOGGER.info("===============  PizzaServiceImpl : Inside findAllSizes :: End  ============= ");
		
		return pizzaSizeRepository.findByMerchantId(id);
	}
	
	public String findPizzaTamplateByMerchantId(Integer merchantId, Integer pageDisplayLength, Integer pageNumber,
												String searchParameter) {
		LOGGER.info(
				"===============  PizzaServiceImpl : Inside findPizzaTamplateByMerchantId :: Start  ============= ");
		LOGGER.info(" pageDisplayLength  : " + pageDisplayLength + " merchant " + merchantId + " pageNumber "
							+ pageNumber + " searchParameter " + searchParameter);
		
		Pageable pageable = new PageRequest(pageNumber - 1, pageDisplayLength, Sort.Direction.ASC, "id");
		Page<PizzaTemplate> page = pizzaTemplateRepository.findByMerchantIdAndActive(merchantId, pageable,
																					 IConstant.BOOLEAN_TRUE);
		List<PizzaTemplateSize> pizzaTemplateSizes = new ArrayList<PizzaTemplateSize>();
		try {
			for (PizzaTemplate pizzaTamplate : page.getContent()) {
				LOGGER.info(" ==pizzaTamplate : " + pizzaTamplate.getId());
				PizzaTemplateSize pizzaTemplateSize2 = new PizzaTemplateSize();
				List<PizzaTemplateSize> pizzaTamplateSize = pizzaTemplateSizeRepository
						.findByPizzaTemplateId(pizzaTamplate.getId());
				for (PizzaTemplateSize tamplateSize : pizzaTamplateSize) {
					String toppingName = "";
					LOGGER.info(" === tamplateSize.getPizzaTemplate().getId() : "
										+ tamplateSize.getPizzaTemplate().getId());
					List<PizzaTemplateTopping> pizzaTemplateToppingRepositories = pizzaTemplateToppingRepository
							.findByPizzaTemplateId(tamplateSize.getPizzaTemplate().getId());
					for (PizzaTemplateTopping templateTopping : pizzaTemplateToppingRepositories) {
						
						// PizzaTopping pizzaTopping =
						// pizzaToppingRepository.findOne(templateTopping.getPizzaToppingId());
						LOGGER.info(" ===templateTopping.getPizzaTopping().getId() == "
											+ templateTopping.getPizzaTopping().getId());
						PizzaTopping pizzaTopping = pizzaToppingRepository
								.findOne(templateTopping.getPizzaTopping().getId());
						toppingName = toppingName + "," + pizzaTopping.getDescription();
						
					}
					toppingName = toppingName.trim();
					if (!toppingName.isEmpty()) {
						pizzaTemplateSize2.setTopping(toppingName.substring(1, toppingName.length()));
					} else {
						pizzaTemplateSize2.setTopping("");
					}
					
					PizzaTemplate template = new PizzaTemplate();
					template.setActive(pizzaTamplate.getActive());
					template.setDescription(pizzaTamplate.getDescription());
					template.setId(pizzaTamplate.getId());
					
					pizzaTemplateSize2.setDescription(tamplateSize.getPizzaSize().getDescription());
					pizzaTemplateSize2.setName(pizzaTamplate.getDescription());
					pizzaTemplateSize2.setPrice(tamplateSize.getPrice());
					pizzaTemplateSize2.setId(tamplateSize.getId());
					pizzaTemplateSize2.setSize(tamplateSize.getPizzaSize().getDescription());
					if (pizzaTamplate.getActive() == 1) {
						pizzaTemplateSize2.setStatus("<div><a href=javascript:void(0) class='nav-toggle' itmId="
															 + pizzaTamplate.getId() + " style='color: blue;'>Active</a></div>");
					} else {
						pizzaTemplateSize2.setStatus("<div><a href=javascript:void(0) class='nav-toggle' itmId="
															 + pizzaTamplate.getId() + " style='color: blue;'>InActive</a></div>");
					}
					pizzaTemplateSize2.setAction("<a href=editPizzaTemplate?templateId=" + pizzaTamplate.getId()
														 + " class='edit'><i class='fa fa-pencil' aria-hidden='true'></i></a>");
					
					pizzaTemplateSize2.setPizzaTemplate(template);
				}
				pizzaTemplateSizes.add(pizzaTemplateSize2);
				
			}
		} catch (Exception e) {
			LOGGER.error(
					"===============  PizzaServiceImpl : Inside findPizzaTamplateByMerchantId :: Exception  ============= "
							+ e);
			
			LOGGER.error("error: " + e.getMessage());
		}
		pizzaTemplateSizes = getListBasedOnSearchParameter(searchParameter, pizzaTemplateSizes);
		
		PersonJsonObject personJsonObject = new PersonJsonObject();
		personJsonObject.setiTotalDisplayRecords((int) page.getTotalElements());
		personJsonObject.setiTotalRecords((int) page.getTotalElements());
		personJsonObject.setPizzaData(pizzaTemplateSizes);
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String json2 = gson.toJson(personJsonObject);
		LOGGER.info("===============  PizzaServiceImpl : Inside findPizzaTamplateByMerchantId :: returns json2  == "
							+ json2);
		
		LOGGER.info("===============  PizzaServiceImpl : Inside findPizzaTamplateByMerchantId :: End  ============= ");
		
		return json2;
		
	}
	
	private List<PizzaTemplateSize> getListBasedOnSearchParameter(String searchParameter,
																  List<PizzaTemplateSize> personsList) {
		LOGGER.info(
				"===============  PizzaServiceImpl : Inside getListBasedOnSearchParameter :: Start  ============= ");
		LOGGER.info(" ===searchParameter :  " + searchParameter);
		if (null != searchParameter && !searchParameter.equals("")) {
			List<PizzaTemplateSize> personsListForSearch = new ArrayList<PizzaTemplateSize>();
			searchParameter = searchParameter.toUpperCase();
			for (PizzaTemplateSize person : personsList) {
				LOGGER.info(" ===person : " + person.getId());
				
				if (person.getDescription().toUpperCase().indexOf(searchParameter) != -1) {
					personsListForSearch.add(person);
				}
			}
			personsList = personsListForSearch;
			personsListForSearch = null;
		}
		LOGGER.info("===============  PizzaServiceImpl : Inside getListBasedOnSearchParameter :: End  ============= ");
		
		return personsList;
	}
	
	private List<PizzaToppingSize> getToppingListBasedOnSearchParameter(String searchParameter,
																		List<PizzaToppingSize> personsList) {
		LOGGER.info(
				"===============  PizzaServiceImpl : Inside getToppingListBasedOnSearchParameter :: Start  ============= ");
		LOGGER.info(" ===searchParameter :  " + searchParameter);
		
		if (null != searchParameter && !searchParameter.equals("")) {
			List<PizzaToppingSize> personsListForSearch = new ArrayList<PizzaToppingSize>();
			searchParameter = searchParameter.toUpperCase();
			for (PizzaToppingSize person : personsList) {
				LOGGER.info(" ===person : " + person.getId());
				personsListForSearch.add(person);
			}
			personsList = personsListForSearch;
			personsListForSearch = null;
		}
		LOGGER.info(
				"===============  PizzaServiceImpl : Inside getToppingListBasedOnSearchParameter :: End  ============= ");
		
		return personsList;
	}
	
	public String findPizzaToppingByMerchantId(Integer merchantId, Integer pageDisplayLength, Integer pageNumber,
											   String searchParameter) {
		LOGGER.info("===============  PizzaServiceImpl : Inside findPizzaToppingByMerchantId :: Start  ============= ");
		
		LOGGER.info(" pageDisplayLength  : " + pageDisplayLength + " merchant " + merchantId + " pageNumber "
							+ pageNumber + " searchParameter " + searchParameter);
		
		Pageable pageable = new PageRequest(pageNumber - 1, pageDisplayLength, Sort.Direction.ASC, "id");
		Page<PizzaTopping> page = pizzaToppingRepository.findByMerchantIdAndActive(merchantId, pageable,
																				   IConstant.BOOLEAN_TRUE);
		List<PizzaToppingSize> pizzaToppingSizes = new ArrayList<PizzaToppingSize>();
		try {
			for (PizzaTopping pizzaTopping : page.getContent()) {
				LOGGER.info(" === pizzaTopping  : " + pizzaTopping.getId());
				PizzaToppingSize pizzaToppingSize2 = new PizzaToppingSize();
				List<PizzaToppingSize> pizzaToppingSize = pizzaToppingSizeRepository
						.findByPizzaToppingId(pizzaTopping.getId());
				for (PizzaToppingSize toppingSize : pizzaToppingSize) {
					LOGGER.info(" === toppingSize : " + toppingSize.getId());
					PizzaTopping topping = new PizzaTopping();
					topping.setId(pizzaTopping.getId());
					topping.setDescription(pizzaTopping.getDescription());
					topping.setActive(pizzaTopping.getActive());
					
					pizzaToppingSize2.setPrice(toppingSize.getPrice());
					pizzaToppingSize2.setId(toppingSize.getId());
					pizzaToppingSize2.setName(pizzaTopping.getDescription());
					pizzaToppingSize2.setSize(toppingSize.getPizzaSize().getDescription());
					
					if (pizzaTopping.getActive() == 1) {
						pizzaToppingSize2.setStatus("<div><a href=javascript:void(0) class='nav-toggle' itmId="
															+ pizzaTopping.getId() + " style='color: blue;'>Active</a></div>");
					} else {
						pizzaToppingSize2.setStatus("<div><a href=javascript:void(0) class='nav-toggle' itmId="
															+ pizzaTopping.getId() + " style='color: blue;'>InActive</a></div>");
					}
					pizzaToppingSize2.setPizzaTopping(topping);
				}
				
				pizzaToppingSizes.add(pizzaToppingSize2);
				
			}
		} catch (Exception e) {
			LOGGER.error(
					"===============  PizzaServiceImpl : Inside findPizzaToppingByMerchantId :: Exception  ============= "
							+ e);
			
			LOGGER.error("error: " + e.getMessage());
		}
		pizzaToppingSizes = getToppingListBasedOnSearchParameter(searchParameter, pizzaToppingSizes);
		
		PersonJsonObject personJsonObject = new PersonJsonObject();
		personJsonObject.setiTotalDisplayRecords((int) page.getTotalElements());
		personJsonObject.setiTotalRecords((int) page.getTotalElements());
		personJsonObject.setToping(pizzaToppingSizes);
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String json2 = gson.toJson(personJsonObject);
		LOGGER.info("===============  PizzaServiceImpl : Inside findPizzaToppingByMerchantId :: returns json2 ==== "
							+ json2);
		LOGGER.info("===============  PizzaServiceImpl : Inside findPizzaToppingByMerchantId :: End  ============= ");
		
		return json2;
	}
	
	/*
	 * public String filterTamplateBypizzaSizeId(Integer merchantId, int
	 * pizzaSizeId) { if (pizzaSizeId == 0) { List<PizzaTemplate> page =
	 * pizzaTamplateRepository.findByMerchantId(merchantId); List<PizzaTemplateSize>
	 * pizzaTemplateSizes = new ArrayList<PizzaTemplateSize>();
	 *
	 * try { for (PizzaTemplate pizzaTemplate : page) { // PizzaTemplateSize
	 * pizzaTemplateSize = new // PizzaTemplateSize(); String tamplateName = "";
	 * PizzaTemplateSize pizzaTemplateSize2 = new PizzaTemplateSize();
	 * List<PizzaTemplateSize> pizzaTamplateSize = pizzaTemplateSizeRepository
	 * .findByPizzaTemplateId(pizzaTemplate.getId()); for (PizzaTemplateSize
	 * tamplateSize : pizzaTamplateSize) { tamplateName = tamplateName + "," +
	 * tamplateSize.getPizzaTemplate().getDescription();
	 * pizzaTemplateSize2.setDescription(tamplateSize.getPizzaSize().getDescription(
	 * )); pizzaTemplateSize2.setPrice(tamplateSize.getPrice());
	 * pizzaTemplateSize2.setActive(tamplateSize.getActive());
	 * if(tamplateSize.getActive()==1) { pizzaTemplateSize2.
	 * setStatus("<div><a href=javascript:void(0) class='nav-toggle' itmId=" +
	 * tamplateSize.getId() + " style='color: blue;'>Active</a></div>"); }else{
	 * pizzaTemplateSize2.
	 * setStatus("<div><a href=javascript:void(0) class='nav-toggle' itmId=" +
	 * tamplateSize.getId() + " style='color: blue;'>InActive</a></div>"); }
	 * pizzaTemplateSize2.setId(tamplateSize.getId());
	 * pizzaTemplate.setPizzaTemplateSizes(null); pizzaTemplate.setPizzaSizes(null);
	 * pizzaTemplate.setCategory(null); pizzaTemplate.setMerchant(null);
	 * pizzaTemplateSize2.setPizzaTemplate(pizzaTemplate);
	 *
	 * }
	 *
	 * pizzaTemplateSizes.add(pizzaTemplateSize2); } } catch (Exception e) {
	 * LOGGER.error("error: " + e.getMessage()); } Gson gson = new
	 * GsonBuilder().setPrettyPrinting().create(); return
	 * gson.toJson(pizzaTemplateSizes);
	 *
	 * } else { System.out.println("filterTamplateBypizzaSizeId");
	 *
	 * List<PizzaTemplateSize> pizzaTempalteWithSize =
	 * pizzaTemplateSizeRepository.findByPizzaSizeId(pizzaSizeId);
	 * List<PizzaTemplateSize> pizzaTemplateSizes = new
	 * ArrayList<PizzaTemplateSize>();
	 *
	 * for (PizzaTemplateSize pizzaTempalteWithSize1 : pizzaTempalteWithSize) {
	 * PizzaTemplate pizzaTemplate = pizzaTamplateRepository
	 * .findOne(pizzaTempalteWithSize1.getPizzaTemplate().getId());
	 * PizzaTemplateSize pizzaTemplateSize2 = new PizzaTemplateSize();
	 * pizzaTemplateSize2.setId(pizzaTempalteWithSize1.getId());
	 * pizzaTemplateSize2.setDescription(pizzaTempalteWithSize1.getDescription());
	 * pizzaTemplateSize2.setPrice(pizzaTempalteWithSize1.getPrice());
	 * pizzaTemplateSize2.setActive(pizzaTempalteWithSize1.getActive());
	 * if(pizzaTempalteWithSize1.getActive()==1) { pizzaTemplateSize2.
	 * setStatus("<div><a href=javascript:void(0) class='nav-toggle' itmId=" +
	 * pizzaTempalteWithSize1.getId() + " style='color: blue;'>Active</a></div>");
	 * }else{ pizzaTemplateSize2.
	 * setStatus("<div><a href=javascript:void(0) class='nav-toggle' itmId=" +
	 * pizzaTempalteWithSize1.getId() + " style='color: blue;'>InActive</a></div>");
	 * }
	 *
	 *
	 * pizzaTemplate.setCategory(null); pizzaTemplate.setMerchant(null);
	 * pizzaTemplate.setPizzaSizes(null); pizzaTemplate.setPizzaTemplateSizes(null);
	 * pizzaTemplateSize2.setPizzaTemplate(pizzaTemplate);
	 * pizzaTemplateSizes.add(pizzaTemplateSize2); }
	 *
	 * Gson gson = new GsonBuilder().setPrettyPrinting().create(); return
	 * gson.toJson(pizzaTemplateSizes); } }
	 */
	
	public String filterTamplateBypizzaSizeId(Integer merchantId, int pizzaSizeId) {
		LOGGER.info("===============  PizzaServiceImpl : Inside filterTamplateBypizzaSizeId :: Start  ============= ");
		LOGGER.info(" pizzaSizeId  : " + pizzaSizeId + " merchant " + merchantId);
		
		LOGGER.info("filterTamplateBypizzaSizeId");
		List<PizzaTemplateSize> pizzaTempalteWithSize = null;
		
		List<PizzaTemplate> pizzaTemplateobject = pizzaTemplateRepository.findByMerchantId(merchantId);
		List<Integer> pizzatemplateids = new ArrayList<Integer>();
		for (PizzaTemplate pizzaTemplateobj : pizzaTemplateobject) {
			pizzatemplateids.add(pizzaTemplateobj.getId());
		}
		
		
		if (pizzaSizeId != 0) {
			pizzaTempalteWithSize = pizzaTemplateSizeRepository.findByPizzaSizeIdAndPizzaTemplateMerchantId(pizzaSizeId,
																											merchantId);
		} else {
			pizzaTempalteWithSize = pizzaTemplateSizeRepository.findByPizzaTemplateMerchantId(merchantId);
		}
		List<PizzaTemplateSize> pizzaTemplateSizes = new ArrayList<PizzaTemplateSize>();
		List<Integer> templateId=new ArrayList<Integer>();
		for (PizzaTemplateSize pizzaTempalteWithSize1 : pizzaTempalteWithSize) {
			//            PizzaTemplate pizzaTemplate = pizzaTemplateRepository
			//                    .findOne(pizzaTempalteWithSize1.getPizzaTemplate().getId());
			Boolean flag =true;
			
			if(templateId!=null && templateId.size()>0)
			{
				for (Integer templateId1 : templateId) {
					
					if(templateId1.equals(pizzaTempalteWithSize1.getPizzaTemplate().getId()))
					{
						flag=false;
					}
				}
				
			}
			String size = "";
			if(flag)
			{
				
				templateId.add(pizzaTempalteWithSize1.getPizzaTemplate().getId());
				String toppingName = "";
				String categoryName = "";
				
				List<PizzaTemplateTopping> pizzaTemplateToppingRepositories = pizzaTemplateToppingRepository
						.findByPizzaTemplateId(pizzaTempalteWithSize1.getPizzaTemplate().getId());
				for (PizzaTemplateTopping templateTopping : pizzaTemplateToppingRepositories) {
					LOGGER.info(
							" ===templateTopping.getPizzaTopping().getId() : " + templateTopping.getPizzaTopping().getId());
					if (templateTopping.getActive() == true && templateTopping.getPizzaTopping().getActive()==1) {
						//                PizzaTopping pizzaTopping = pizzaToppingRepository.findOne(templateTopping.getPizzaTopping().getId());
						//                if(pizzaTopping!=null && pizzaTopping.getDescription()!=null)
						//                toppingName = toppingName + "," + pizzaTopping.getDescription();
						
						if (templateTopping != null && templateTopping.getPizzaTopping().getDescription() != null)
							toppingName = toppingName + "," + templateTopping.getPizzaTopping().getDescription();
						
					}
					
				}
				
				List<PizzaTemplateCategory> pizzaTemplatecategoryRepositories = pizzaTemplateCategoryRepository
						.findByPizzaTemplateIdAndCategoryItemStatus(pizzaTempalteWithSize1.getPizzaTemplate().getId() , 0);
				for (PizzaTemplateCategory templateCategory : pizzaTemplatecategoryRepositories) {
					LOGGER.info(
							" ===templateTopping.getPizzaTopping().getId() : " + templateCategory.getCategory().getId());
					
					//                PizzaTopping pizzaTopping = pizzaToppingRepository.findOne(templateTopping.getPizzaTopping().getId());
					//                if(pizzaTopping!=null && pizzaTopping.getDescription()!=null)
					//                toppingName = toppingName + "," + pizzaTopping.getDescription();
					
					if (templateCategory != null && templateCategory.getCategory().getName() != null)
						categoryName = categoryName + " , " + templateCategory.getCategory().getName();
					
					
				}
				
				PizzaTemplate template = new PizzaTemplate();
				template.setDescription(pizzaTempalteWithSize1.getPizzaTemplate().getDescription());
				template.setActive(pizzaTempalteWithSize1.getPizzaTemplate().getActive());
				template.setId(pizzaTempalteWithSize1.getPizzaTemplate().getId());
				
				PizzaTemplateSize pizzaTemplateSize2 = new PizzaTemplateSize();
				
				toppingName = toppingName.trim();
				LOGGER.info(" === toppingName : " + toppingName);
				if (!toppingName.isEmpty()) {
					pizzaTemplateSize2.setTopping(toppingName.substring(1, toppingName.length()));
				} else {
					pizzaTemplateSize2.setTopping("");
				}
				
				pizzaTemplateSize2.setId(pizzaTempalteWithSize1.getId());
				pizzaTemplateSize2.setDescription(pizzaTempalteWithSize1.getDescription());
				if(pizzaTempalteWithSize1.getActive() ==1)
					pizzaTemplateSize2.setPrice(pizzaTempalteWithSize1.getPrice());
				pizzaTemplateSize2.setActive(pizzaTempalteWithSize1.getActive());
				if (pizzaTempalteWithSize1.getActive() == 1 && pizzaTempalteWithSize1.getPizzaSize().getActive()==1) {
					pizzaTemplateSize2.setSize(pizzaTempalteWithSize1.getPizzaSize().getDescription());
				} else {
					pizzaTemplateSize2.setSize("");
				}
				
				pizzaTemplateSize2.setName(pizzaTempalteWithSize1.getPizzaTemplate().getDescription());
				
				if (pizzaTempalteWithSize1.getPizzaTemplate().getActive() == 1) {
					pizzaTemplateSize2.setStatus("<div><a href=javascript:void(0) class='nav-toggle' itmId="
														 + pizzaTempalteWithSize1.getPizzaTemplate().getId()
														 + " style='color: blue;'>Active</a></div>");
				} else {
					pizzaTemplateSize2.setStatus("<div><a href=javascript:void(0) class='nav-toggle' itmId="
														 + pizzaTempalteWithSize1.getPizzaTemplate().getId()
														 + " style='color: blue;'>InActive</a></div>");
				}
				
				pizzaTemplateSize2.setAction(
						"<a href=editPizzaTemplate?templateId=" + pizzaTempalteWithSize1.getPizzaTemplate().getId()
								+ " class='edit'><i class='fa fa-pencil' aria-hidden='true'></i></a>");
				pizzaTemplateSize2.setPizzaTemplate(template);
				
				//			LOGGER.info(" ===pizzaTemplate.getId() :  " + pizzaTempalteWithSize1.getPizzaTemplate().getId());
				//			PizzaTemplateCategory pizzaTemplateCategory = pizzaTemplateCategoryRepository
				//					.findByPizzaTemplateId(pizzaTempalteWithSize1.getPizzaTemplate().getId());
				//			if (pizzaTemplateCategory != null) {
				//				pizzaTemplateCategory.setPizzaTemplate(null);
				//				pizzaTemplateCategory.getCategory().setCategoryTimings(null);
				//				pizzaTemplateCategory.getCategory().setItems(null);
				//				pizzaTemplateCategory.getCategory().setMerchant(null);
				//				pizzaTemplateCategory.getCategory().setPizzaSizes(null);
				//				pizzaTemplateSize2.setCategoryName(pizzaTemplateCategory.getCategory().getName());
				//
				//			}
				
				
				categoryName = categoryName.trim();
				LOGGER.info(" === categoryName : " + categoryName);
				if (!categoryName.isEmpty()) {
					pizzaTemplateSize2.setCategoryName(categoryName.substring(1, categoryName.length()));
				} else {
					pizzaTemplateSize2.setCategoryName("");
				}
				
				pizzaTemplateSizes.add(pizzaTemplateSize2);
				
			}
			else {
				
				if(pizzaTemplateSizes!=null && pizzaTemplateSizes.size()>0)
				{
					for (PizzaTemplateSize pizzaTemplateSize : pizzaTemplateSizes) {
						
						if(pizzaTemplateSize.getPizzaTemplate().getId().equals(pizzaTempalteWithSize1.getPizzaTemplate().getId()))
						{
							if (pizzaTempalteWithSize1.getActive() == 1 && pizzaTempalteWithSize1.getPizzaSize().getActive()==1
									&& (pizzaTemplateSize.getSize().isEmpty() || pizzaTemplateSize.getSize()==null)) {
								pizzaTemplateSize.setSize(pizzaTemplateSize.getSize()+pizzaTempalteWithSize1.getPizzaSize().getDescription());
							}
							else if(pizzaTempalteWithSize1.getActive() == 1 && pizzaTempalteWithSize1.getPizzaSize().getActive()==1)
								pizzaTemplateSize.setSize(pizzaTemplateSize.getSize()+","+pizzaTempalteWithSize1.getPizzaSize().getDescription());
							if((pizzaTemplateSize.getPrice() == 0 || pizzaTemplateSize.getPrice() == 0.0) && pizzaTempalteWithSize1.getActive() == 1)
								pizzaTemplateSize.setPrice(pizzaTempalteWithSize1.getPrice());
						}
					}
					
				}
				
			}
		}
		
		
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		LOGGER.info("===============  PizzaServiceImpl : Inside filterTamplateBypizzaSizeId :: End  ============= ");
		
		return gson.toJson(pizzaTemplateSizes);
	}
	
	public String filterToppingBypizzaSizeId(Integer merchantId, int pizzaSizeId) {
		LOGGER.info("===============  PizzaServiceImpl : Inside filterToppingBypizzaSizeId :: Start  ============= ");
		LOGGER.info(" pizzaSizeId  : " + pizzaSizeId + " merchant " + merchantId);
		
		LOGGER.info("filterToppingBypizzaSizeId");
		
		List<PizzaTopping> pizzaToppings = pizzaToppingRepository.findByMerchantId(merchantId);
		
		List<PizzaTopping> pizzaToppingslist = new ArrayList<PizzaTopping>();
		
		for (PizzaTopping pizzaToppings1 : pizzaToppings) {
			
			
			PizzaTopping topping = new PizzaTopping();
			topping.setId(pizzaToppings1.getId());
			topping.setDescription(pizzaToppings1.getDescription());
			topping.setActive(pizzaToppings1.getActive());
			
			PizzaTopping pizzaToppings2 = new PizzaTopping();
			
			String sizeName = "";
			double price =0.0;
			List<PizzaToppingSize> pizzatoppingSizelist =
					pizzaToppingSizeRepository.findByPizzaToppingId(pizzaToppings1.getId());
			for(PizzaToppingSize pizzatoppingSize : pizzatoppingSizelist) {
				if (pizzatoppingSize.getPizzaSize().getActive()==1 && pizzatoppingSize.getActive()==0)
					sizeName = sizeName + "," + pizzatoppingSize.getPizzaSize().getDescription();
				
				if(price == 0.0 && pizzatoppingSize.getPizzaSize().getActive()==1 && pizzatoppingSize.getActive()==0)
					price =	pizzatoppingSize.getPrice();
			}
			
			pizzaToppings2.setDescription(pizzaToppings1.getDescription());
			
			pizzaToppings2.setSizeprice(price);
			
			sizeName = sizeName.trim();
			LOGGER.info(" === toppingName : " + sizeName);
			if (!sizeName.isEmpty()) {
				pizzaToppings2.setSize(sizeName.substring(1, sizeName.length()));
			} else {
				pizzaToppings2.setSize("");
			}
			
			
			if (pizzaToppings1.getActive() == 1) {
				pizzaToppings2.setStatus("<div><a href=javascript:void(0) class='nav-toggle' itmId="
												 + pizzaToppings1.getId() + " style='color: blue;'>Active</a></div>");
			} else {
				pizzaToppings2.setStatus("<div><a href=javascript:void(0) class='nav-toggle' itmId="
												 + pizzaToppings1.getId() + " style='color: blue;'>InActive</a></div>");
			}
			
			pizzaToppings2.setAction("<a href=editPizzaTopping?toppingId=" + pizzaToppings1.getId()
											 + " class='edit'><i class='fa fa-pencil' aria-hidden='true'></i></a>");
			
			
			pizzaToppingslist.add(pizzaToppings2);
			
			
		}
		
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		LOGGER.info("===============  PizzaServiceImpl : Inside filterToppingBypizzaSizeId :: End  ============= ");
		
		return gson.toJson(pizzaToppingslist);
		
	}
	
	public String searchTemplateByTxt(Integer merchantId, String searchTxt) {
		LOGGER.info("===============  PizzaServiceImpl : Inside searchTemplateByTxt :: Start  ============= ");
		LOGGER.info(" === merchant : " + merchantId + " searchTxt : " + searchTxt);
		List<PizzaTemplate> pizzaTemplates = pizzaTemplateRepository.findByMerchantIdAndDescriptionByQuery(merchantId,
																										   searchTxt);
		
		List<PizzaTemplateSize> pizzaTemplateSizes = new ArrayList<PizzaTemplateSize>();
		try {
			for (PizzaTemplate data : pizzaTemplates) {
				LOGGER.info(" === PizzaTemplate : " + data.getId());
				List<PizzaTemplateSize> pizzaTemplateSize = pizzaTemplateSizeRepository
						.findByPizzaTemplateId(data.getId());
				// PizzaTemplate pizzaTemplate =
				// pizzaTamplateRepository.findOne(data.getPizzaTemplate().getId());
				for (PizzaTemplateSize pizzaTemplateSize1 : pizzaTemplateSize) {
					PizzaTemplate template = new PizzaTemplate();
					template.setId(data.getId());
					template.setDescription(data.getDescription());
					template.setActive(data.getActive());
					
					PizzaTemplateSize pizzaTemplateSize2 = new PizzaTemplateSize();
					pizzaTemplateSize2.setId(pizzaTemplateSize1.getId());
					pizzaTemplateSize2.setDescription(pizzaTemplateSize1.getDescription());
					pizzaTemplateSize2.setPrice(pizzaTemplateSize1.getPrice());
					pizzaTemplateSize2.setActive(pizzaTemplateSize1.getActive());
					pizzaTemplateSize2.setSize(pizzaTemplateSize1.getPizzaSize().getDescription());
					pizzaTemplateSize2.setName(data.getDescription());
					
					String toppingName = "";
					List<PizzaTemplateTopping> pizzaTemplateToppingRepositories = pizzaTemplateToppingRepository
							.findByPizzaTemplateId(data.getId());
					for (PizzaTemplateTopping templateTopping : pizzaTemplateToppingRepositories) {
						
						// PizzaTopping pizzaTopping =
						// pizzaToppingRepository.findOne(templateTopping.getPizzaToppingId());
						LOGGER.info(" === templateTopping : " + templateTopping.getPizzaTopping().getId());
						PizzaTopping pizzaTopping = pizzaToppingRepository
								.findOne(templateTopping.getPizzaTopping().getId());
						toppingName = toppingName + "," + pizzaTopping.getDescription();
						
					}
					toppingName = toppingName.trim();
					if (!toppingName.isEmpty()) {
						pizzaTemplateSize2.setTopping(toppingName.substring(1, toppingName.length()));
					} else {
						pizzaTemplateSize2.setTopping("");
					}
					
					if (data.getActive() == 1) {
						pizzaTemplateSize2.setStatus("<div><a href=javascript:void(0) class='nav-toggle' itmId="
															 + data.getId() + " style='color: blue;'>Active</a></div>");
					} else {
						pizzaTemplateSize2.setStatus("<div><a href=javascript:void(0) class='nav-toggle' itmId="
															 + data.getId() + " style='color: blue;'>InActive</a></div>");
					}
					pizzaTemplateSize2.setAction("<a href=editPizzaTemplate?templateId=" + data.getId()
														 + " class='edit'><i class='fa fa-pencil' aria-hidden='true'></i></a>");
					pizzaTemplateSize2.setPizzaTemplate(template);
					
					PizzaTemplateCategory pizzaTemplateCategory = pizzaTemplateCategoryRepository
							.findByPizzaTemplateId(data.getId());
					if (pizzaTemplateCategory != null) {
						pizzaTemplateCategory.setPizzaTemplate(null);
						pizzaTemplateCategory.getCategory().setCategoryTimings(null);
						pizzaTemplateCategory.getCategory().setItems(null);
						pizzaTemplateCategory.getCategory().setMerchant(null);
						pizzaTemplateCategory.getCategory().setPizzaSizes(null);
						pizzaTemplateSize2.setCategoryName(pizzaTemplateCategory.getCategory().getName());
					}
					
					pizzaTemplateSizes.add(pizzaTemplateSize2);
				}
				
			}
		} catch (Exception e) {
			LOGGER.error(
					"===============  PizzaServiceImpl : Inside searchTemplateByTxt :: Exception  ============= " + e);
			
			LOGGER.error("error: " + e.getMessage());
		}
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		LOGGER.info("===============  PizzaServiceImpl : Inside searchTemplateByTxt :: End  ============= ");
		
		return gson.toJson(pizzaTemplateSizes);
	}
	
	public String searchToppingsByTxt(Integer merchantId, String searchTxt) {
		LOGGER.info("===============  PizzaServiceImpl : Inside searchToppingsByTxt :: Start  ============= ");
		
		LOGGER.info(" === merchant : " + merchantId + " searchTxt : " + searchTxt);
		
		List<PizzaTopping> pizzaToppingList = pizzaToppingRepository.findByMerchantIdAndDescription(merchantId,
																									searchTxt);
		List<PizzaToppingSize> pizzaToppingSizes = new ArrayList<PizzaToppingSize>();
		for (PizzaTopping pizzaTopping : pizzaToppingList) {
			LOGGER.info(" === pizzaTopping : " + pizzaTopping.getId());
			
			PizzaToppingSize pizzaToppingSize2 = new PizzaToppingSize();
			List<PizzaToppingSize> pizzaToppingSize = pizzaToppingSizeRepository
					.findByPizzaToppingId(pizzaTopping.getId());
			for (PizzaToppingSize toppingSize : pizzaToppingSize) {
				LOGGER.info(" === toppingSize : " + toppingSize.getId());
				
				PizzaTopping topping = new PizzaTopping();
				topping.setId(pizzaTopping.getId());
				topping.setDescription(pizzaTopping.getDescription());
				topping.setActive(pizzaTopping.getActive());
				
				pizzaToppingSize2.setPrice(toppingSize.getPrice());
				pizzaToppingSize2.setActive(toppingSize.getActive());
				pizzaToppingSize2.setSize(toppingSize.getPizzaSize().getDescription());
				pizzaToppingSize2.setName(pizzaTopping.getDescription());
				if (pizzaTopping.getActive() == 1) {
					pizzaToppingSize2.setStatus("<div><a href=javascript:void(0) class='nav-toggle' itmId="
														+ pizzaTopping.getId() + " style='color: blue;'>Active</a></div>");
				} else {
					pizzaToppingSize2.setStatus("<div><a href=javascript:void(0) class='nav-toggle' itmId="
														+ pizzaTopping.getId() + " style='color: blue;'>InActive</a></div>");
				}
				
				pizzaToppingSize2.setAction("<a href=editPizzaTopping?toppingId=" + pizzaTopping.getId()
													+ " class='edit'><i class='fa fa-pencil' aria-hidden='true'></i></a>");
				
				pizzaToppingSize2.setId(toppingSize.getId());
				
				pizzaToppingSize2.setPizzaTopping(topping);
			}
			pizzaToppingSizes.add(pizzaToppingSize2);
			
		}
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		LOGGER.info("===============  PizzaServiceImpl : Inside searchToppingsByTxt :: End  ============= ");
		
		return gson.toJson(pizzaToppingSizes);
	}
	
	public void updateTamplateStatusById(PizzaTemplate template) {
		LOGGER.info("===============  PizzaServiceImpl : Inside updateTamplateStatusById :: Start  ============= ");
		LOGGER.info(" ===template : " + template.getId());
		PizzaTemplate result = pizzaTemplateRepository.findOne(template.getId());
		result.setActive(template.getActive());
		pizzaTemplateRepository.save(result);
		LOGGER.info("===============  PizzaServiceImpl : Inside updateTamplateStatusById :: End  ============= ");
		
	}
	
	public void updateToppingStatusById(PizzaTopping topping) {
		LOGGER.info("===============  PizzaServiceImpl : Inside updateToppingStatusById :: Start  ============= ");
		
		LOGGER.info(" ===topping : " + topping.getId());
		
		PizzaTopping result = pizzaToppingRepository.findOne(topping.getId());
		result.setActive(topping.getActive());
		pizzaToppingRepository.save(result);
		LOGGER.info("===============  PizzaServiceImpl : Inside updateToppingStatusById :: End  ============= ");
		
	}
	
	public PizzaTemplate updatePizzaTemplateById(PizzaTemplate pizzaTemplate) {
		LOGGER.info("===============  PizzaServiceImpl : Inside updatePizzaTemplateById :: Start  ============= ");
		
		LOGGER.info(" ===pizzaTemplate : " + pizzaTemplate.getId());
		
		/// code for pizzaTopping active and inactive
		String pizzatoppings = pizzaTemplate.getPizzaToppingsIds();
		LOGGER.info(" ===pizzatoppings : " + pizzatoppings);
		PizzaTemplate template = pizzaTemplateRepository.findOne(pizzaTemplate.getId());
		
		if (template != null) {
			template.setActive(pizzaTemplate.getActive());
			template.setDescription(pizzaTemplate.getDescription());
			template.setAllowToppingLimit(pizzaTemplate.getAllowToppingLimit());
			template.setIsMaxLimit(pizzaTemplate.getIsMaxLimit());
			template.setIsMinLimit(pizzaTemplate.getIsMinLimit());
			if (pizzaTemplate.getDescription2() != null) {
				template.setDescription2(pizzaTemplate.getDescription2());
			}
			pizzaTemplateRepository.save(template);
		}
		
		//String pizzaSizes = pizzaTemplate.getPizzaSizesIds();
		List<PizzaTemplateSize> pizzaTemplateSizes = pizzaTemplateSizeRepository
				.findByPizzaTemplateId(pizzaTemplate.getId());
		if (!pizzaTemplateSizes.isEmpty() && pizzaTemplateSizes.size() > 0) {
			//			String[] pizzaSizeIds = null;
			Set<PizzaSize> pizzaSizes2 = new HashSet<PizzaSize>();
			//			if (pizzaSizes != null) {
			//				pizzaSizeIds = pizzaSizes.split(",");
			//			}
			
			for (PizzaTemplateSize pizzaTemplateSize : pizzaTemplateSizes) {
				//				if (pizzaSizeIds != null) {
				//					if (pizzaSizes.contains(pizzaTemplateSize.getPizzaSize().getId().toString())) {
				//						pizzaTemplateSize.setActive(1);
				//						pizzaTemplateSize.getPizzaSize().setActive(1);
				//					} else {
				//						pizzaTemplateSize.setActive(0);
				//						pizzaTemplateSize.getPizzaSize().setActive(0);
				//					}
				//				} else {
				//					pizzaTemplateSize.setActive(0);
				//					pizzaTemplateSize.getPizzaSize().setActive(0);
				//				}
				pizzaSizes2.add(pizzaTemplateSize.getPizzaSize());
			}
			//pizzaTemplateSizeRepository.save(pizzaTemplateSizes);
			template.setPizzaSizes(pizzaSizes2);
			template.setPizzaTemplateSizes(pizzaTemplateSizes);
		}
		
		List<Integer> taxRateId = pizzaTemplate.getTaxId();
		List<PizzaTemplateTax> pizzaTemplateTaxs = pizzaTemplateTaxRepository.findByPizzaTemplateId(pizzaTemplate.getId());
		
		if(pizzaTemplateTaxs != null && !pizzaTemplateTaxs.isEmpty()){
			template.setPizzaTemplateTaxs(pizzaTemplateTaxs);
			
			for (PizzaTemplateTax pizzaTemplateTax : pizzaTemplateTaxs) {
				if(taxRateId != null && !taxRateId.isEmpty()){
					for (Integer id : taxRateId) {
						if(id.equals(pizzaTemplateTax.getId())){
							pizzaTemplateTax.setIsActive(1);
							break;
						}else{
							pizzaTemplateTax.setIsActive(0);
						}
					}
				}else{
					pizzaTemplateTax.setIsActive(0);
				}
				pizzaTemplateTaxRepository.save(pizzaTemplateTax);
			}
		}
		
		
		String[] pizzaToppingIds = null;
		List<PizzaTopping> pizzaToppings2 = new ArrayList<PizzaTopping>();
		if (pizzatoppings != null) {
			pizzaToppingIds = pizzatoppings.split(",");
		}
		
		if (pizzaToppingIds != null && pizzaToppingIds.length > 0)
			for (String pizzaToppingId : pizzaToppingIds) {
				
				List<PizzaTemplateTopping> pizzaTemplateTopping = pizzaTemplateToppingRepository
						.findByPizzaToppingIdAndPizzaTemplateId(Integer.parseInt(pizzaToppingId),
																pizzaTemplate.getId());
				if (pizzaTemplateTopping == null || pizzaTemplateTopping.size() == 0) {
					PizzaTopping pizzaTopping = pizzaToppingRepository.findOne(Integer.parseInt(pizzaToppingId));
					if (pizzaTopping != null && pizzaTopping.getId() != null) {
						PizzaTemplateTopping pizzaTemplateTopping1 = new PizzaTemplateTopping();
						pizzaTemplateTopping1.setActive(true);
						pizzaTemplateTopping1.setIncluded(true);
						pizzaTemplateTopping1.setPizzaTemplate(pizzaTemplate);
						pizzaTemplateTopping1
								.setPizzaTopping(pizzaToppingRepository.findOne(Integer.parseInt(pizzaToppingId)));
						pizzaTemplateTopping1.setPrice(0);
						pizzaTemplateToppingRepository.save(pizzaTemplateTopping1);
					}
				}
			}
		
		return template;
	}
	
	public PizzaTemplate findByTemplateId(int templateId) {
		LOGGER.info("===============  PizzaServiceImpl : Inside findByTemplateId :: Start  ============= ");
		LOGGER.info(" ===templateId : " + templateId);
		PizzaTemplate pizzaTemplate;
		Integer merchantId = 0;
		pizzaTemplate = pizzaTemplateRepository.findOne(templateId);
		if (pizzaTemplate != null) {
			LOGGER.info("===============  PizzaServiceImpl : Inside findByTemplateId :: pizzaTemplate : "+pizzaTemplate.getId());
			pizzaTemplate.setCategory(null);
			pizzaTemplate.setPizzaSizes(null);
			pizzaTemplate.setPizzaTemplateSizes(null);
			pizzaTemplate.setPizzaToppings(null);
			if(pizzaTemplate.getMerchant() != null)
				merchantId = pizzaTemplate.getMerchant().getId();
			pizzaTemplate.setMerchant(null);
			pizzaTemplate.setTaxes(null);
			
			List<PizzaTemplateSize> pizzaTemplateSizes = pizzaTemplateSizeRepository
					.findByPizzaTemplateId(pizzaTemplate.getId());
			Set<PizzaSize> pizzaSizes = new HashSet<PizzaSize>();
			
			if (!pizzaTemplateSizes.isEmpty() && pizzaTemplateSizes.size() > 0) {
				for (PizzaTemplateSize pizzaTemplateSize : pizzaTemplateSizes) {
					LOGGER.info(" ===pizzaTemplateSize.getPizzaSize().getId() : "
										+ pizzaTemplateSize.getPizzaSize().getId());
					PizzaSize pizzaSize = pizzaSizeRepository.findOne(pizzaTemplateSize.getPizzaSize().getId());
					pizzaSize.setCategories(null);
					pizzaSize.setPizzaToppingSizes(null);
					pizzaSize.setPizzaTemplates(null);
					pizzaSizes.add(pizzaSize);
				}
			}
			
			pizzaTemplate.setPizzaSizes(pizzaSizes);
			
			List<PizzaTemplateTopping> pizzaTemplateToppingRepositories = pizzaTemplateToppingRepository
					.findByPizzaTemplateId(pizzaTemplate.getId());
			List<PizzaTopping> toppingName = new ArrayList<PizzaTopping>();
			List<PizzaTopping> topingList = pizzaToppingRepository.findByMerchantId(merchantId);
			for (PizzaTemplateTopping templateTopping : pizzaTemplateToppingRepositories) {
				PizzaTemplateTopping pizzaTemplateTopping = new PizzaTemplateTopping();
				
				// PizzaTopping pizzaTopping =
				// pizzaToppingRepository.findOne(templateTopping.getPizzaToppingId());
				PizzaTopping pizzaTopping = pizzaToppingRepository.findOne(templateTopping.getPizzaTopping().getId());
				pizzaTopping.setMerchant(null);
				pizzaTopping.setPizzaToppingSizes(null);
				toppingName.add(pizzaTopping);
				
			}
			
			for (PizzaTopping pizzaToppings : topingList) {
				Boolean status = false;
				for (PizzaTemplateTopping templateTopping : pizzaTemplateToppingRepositories) {
					if (pizzaToppings != null && pizzaToppings.getId() != null)
						if (pizzaToppings.getId() == templateTopping.getPizzaTopping().getId())
							status = true;
				}
				if (!status) {
					PizzaTemplateTopping pizzaTemplateTopping = new PizzaTemplateTopping();
					pizzaTemplateTopping.setId(0);
					pizzaTemplateTopping.setPizzaTopping(pizzaToppings);
					pizzaTemplateTopping.setIncluded(false);
					pizzaTemplateTopping.setReplacable(false);
					pizzaTemplateToppingRepositories.add(pizzaTemplateTopping);
				}
			}
			pizzaTemplate.setPizzaToppings(toppingName);
			pizzaTemplate.setPizzaTemplateToppings(pizzaTemplateToppingRepositories);
			pizzaTemplate.setPizzaTemplateSizes(pizzaTemplateSizes);
			
		}
		LOGGER.info("===============  PizzaServiceImpl : Inside findByTemplateId :: End  ============= ");
		
		return pizzaTemplate;
	}
	
	public List<PizzaTemplate> findPizzaTemplateByMerchantId(Integer merchantId) {
		
		LOGGER.info(
				"----------------Start :: PizzaServiceImpl : findPizzaTemplateByMerchantId------------------------");
		LOGGER.info(" === merchantId : " + merchantId);
		List<PizzaTemplate> pizzaTemplates = null;
		pizzaTemplates = pizzaTemplateRepository.findByMerchantIdAndActive(merchantId, 1);
		for (PizzaTemplate pizzaTemplate : pizzaTemplates) {
			LOGGER.info(" === pizzaTemplate : " + pizzaTemplate.getId());
			pizzaTemplate.setCategory(null);
			pizzaTemplate.setPizzaSizes(null);
			pizzaTemplate.setPizzaTemplateSizes(null);
			pizzaTemplate.setPizzaToppings(null);
			pizzaTemplate.setMerchant(null);
			pizzaTemplate.setTaxes(null);
			pizzaTemplate.setPizzaCrust(null);
		}
		
		LOGGER.info("----------------End :: PizzaServiceImpl : findPizzaTemplateByMerchantId------------------------");
		return pizzaTemplates;
	}
	
	public List<PizzaTemplateSize> findPizzaTemplateSizeByTemplateId(Integer templateId) {
		
		LOGGER.info(
				"----------------Start :: PizzaServiceImpl : findPizzaTemplateSizeByTemplateId------------------------");
		LOGGER.info("PizzaServiceImpl :: findPizzaTemplateSizeByTemplateId : templateId " + templateId);
		List<PizzaTemplateSize> pizzaTemplateSizes = pizzaTemplateSizeRepository
				.findByPizzaTemplateIdAndActive(templateId, 1);
		
		for (PizzaTemplateSize pizzaTemplateSize : pizzaTemplateSizes) {
			pizzaTemplateSize.setPizzaTemplate(null);
			
			if (pizzaTemplateSize.getPizzaSize() != null) {
				pizzaTemplateSize.getPizzaSize().setMerchant(null);
				pizzaTemplateSize.getPizzaSize().setCategories(null);
				pizzaTemplateSize.getPizzaSize().setPizzaToppingSizes(null);
				pizzaTemplateSize.getPizzaSize().setPizzaTemplates(null);
				pizzaTemplateSize.getPizzaSize().setPizzaCrustSizes(null);
			}
		}
		LOGGER.info(
				"----------------End :: PizzaServiceImpl : findPizzaTemplateSizeByTemplateId------------------------");
		return pizzaTemplateSizes;
	}
	
	public List<PizzaTopping> findByToppingByMerchantId(Integer merchantId) {
		LOGGER.info("===============  PizzaServiceImpl : Inside findByToppingByMerchantId :: Start  ============= ");
		
		LOGGER.info(" === merchantId : " + merchantId);
		
		List<PizzaTopping> pizzaToppings = pizzaToppingRepository.findByMerchantId(merchantId);
		
		for (PizzaTopping pizzaTopping : pizzaToppings) {
			LOGGER.info(" === pizzaTopping : " + pizzaTopping.getId());
			pizzaTopping.setMerchant(null);
			pizzaTopping.setPizzaToppingSizes(null);
			
		}
		LOGGER.info("===============  PizzaServiceImpl : Inside findByToppingByMerchantId :: End  ============= ");
		
		return pizzaToppings;
	}
	
	public List<PizzaToppingSize> findByToppingIdAndSizeId(List<Integer> id, Integer sizeId) {
		LOGGER.info("===============  PizzaServiceImpl : Inside findByToppingIdAndSizeId :: Start  ============= ");
		LOGGER.info(" === id : " + id + " sizeId : " + sizeId);
		List<PizzaToppingSize> pizzaToppingSizes = pizzaToppingSizeRepository
				.findByPizzaSizeIdAndPizzaToppingIdAndActive(sizeId, id, 0);
		//      for (PizzaToppingSize pizzaToppingSize : pizzaToppingSizes) {
		//          if(pizzaToppingSize.getPizzaSize()!=null) {
		//              pizzaToppingSize.getPizzaSize().setMerchant(null);
		//              pizzaToppingSize.getPizzaSize().setCategories(null);
		//              pizzaToppingSize.getPizzaSize().setPizzaToppingSizes(null);
		//              pizzaToppingSize.getPizzaSize().setPizzaCrustSizes(null);
		//              pizzaToppingSize.getPizzaSize().setPizzaTemplates(null);
		//          }
		LOGGER.info("===============  PizzaServiceImpl : Inside findByToppingIdAndSizeId :: End  ============= ");
		//      }
		return pizzaToppingSizes;
	}
	
	public List<PizzaCrustSizes> findByCrustIdAndSizeId(Integer id, Integer sizeId) {
		LOGGER.info("===============  PizzaServiceImpl : Inside findByCrustIdAndSizeId :: Start  ============= ");
		LOGGER.info(" === id : " + id + " sizeId : " + sizeId);
		List<PizzaCrustSizes> pizzaCrustSizes = pizzaCrustSizeRepository
				.findByPizzaSizeIdAndPizzaCrustIdAndActive(sizeId, id, 1);
		
		
		for (PizzaCrustSizes pizzaCrustSize : pizzaCrustSizes) {
			// pizzaCrustSize.setPizzaCrust(null);
			if (pizzaCrustSize.getPizzaSize() != null) {
				pizzaCrustSize.getPizzaSize().setMerchant(null);
				pizzaCrustSize.getPizzaSize().setCategories(null);
				pizzaCrustSize.getPizzaSize().setPizzaToppingSizes(null);
				pizzaCrustSize.getPizzaSize().setPizzaCrustSizes(null);
				
				pizzaCrustSize.getPizzaSize().setPizzaTemplates(null);
			}
		}
		LOGGER.info("===============  PizzaServiceImpl : Inside findByCrustIdAndSizeId :: End  ============= ");
		
		return pizzaCrustSizes;
	}
	
	public PizzaCrustSizes findByCrustId(Integer id, Integer sizeId) {
		LOGGER.info("===============  PizzaServiceImpl : Inside findByCrustId :: Start  ============= ");
		
		LOGGER.info(" === id : " + id + " sizeId : " + sizeId);
		
		PizzaCrustSizes pizzaCrustSize = pizzaCrustSizeRepository.findByPizzaCrustIdAndPizzaSizeId(id, sizeId);
		// pizzaCrustSize.setPizzaCrust(null);
		if (pizzaCrustSize.getPizzaSize() != null) {
			pizzaCrustSize.getPizzaSize().setMerchant(null);
			pizzaCrustSize.getPizzaSize().setCategories(null);
			pizzaCrustSize.getPizzaSize().setPizzaToppingSizes(null);
			pizzaCrustSize.getPizzaSize().setPizzaCrustSizes(null);
			
			pizzaCrustSize.getPizzaSize().setPizzaTemplates(null);
		}
		LOGGER.info("===============  PizzaServiceImpl : Inside findByCrustId :: End  ============= ");
		
		return pizzaCrustSize;
	}
	
	public PizzaTemplate findByTemplateIdAndMerchantId(int templateId, int merchantId, int sizeId) {
		LOGGER.info(
				"===============  PizzaServiceImpl : Inside findByTemplateIdAndMerchantId :: Start  ============= ");
		
		LOGGER.info(" === templateId : " + templateId + " sizeId : " + sizeId + " merchantId : " + merchantId);
		
		PizzaTemplate pizzaTemplate;
		pizzaTemplate = pizzaTemplateRepository.findByIdAndMerchantId(templateId, merchantId);
		if (pizzaTemplate != null) {
			List<PizzaTopping> pizzaToppingList = new ArrayList<PizzaTopping>();
			
			
			List<PizzaToppingSize> pizzaToppingSizes = pizzaToppingSizeRepository.findByPizzaSizeIdAndActive(sizeId,0);
			if (pizzaToppingSizes != null && pizzaToppingSizes.size() > 0) {
				for (PizzaToppingSize pizzaToppingSize : pizzaToppingSizes) {
					LOGGER.info(" ===pizzaToppingSize.getPizzaTopping().getId() : "
										+ pizzaToppingSize.getPizzaTopping().getId());
					if (pizzaToppingSize.getPizzaTopping() != null
							&& pizzaToppingSize.getPizzaTopping().getId() != null ) {
						//						PizzaTopping pizzaTopping = pizzaToppingRepository
						//								.findOne(pizzaToppingSize.getPizzaTopping().getId());
						//if (pizzaTopping != null) {
						pizzaToppingList.add(pizzaToppingSize.getPizzaTopping());
						//}
					}
				}
				pizzaTemplate.setPizzaToppings(pizzaToppingList);
			}
			
			List<PizzaTemplateCrust> pizzaTemplateCrustList = pizzaTemplateCrustRepository
					.findByPizzaTemplateIdAndActive(templateId , 1);
			List<PizzaCrust> pizzaCrustList = new ArrayList<PizzaCrust>();
			
			for (PizzaTemplateCrust templateCrust : pizzaTemplateCrustList) {
				LOGGER.info(" ===templateCrust.getPizzaCrust().getId() : " + templateCrust.getPizzaCrust().getId());
				
				//PizzaCrust pizzaCrust = pizzaCrustRepository.findOne(templateCrust.getPizzaCrust().getId());
				pizzaCrustList.add(templateCrust.getPizzaCrust());
			}
			pizzaTemplate.setPizzaCrust(pizzaCrustList);
		}
		LOGGER.info("===============  PizzaServiceImpl : Inside findByTemplateIdAndMerchantId :: End  ============= ");
		
		return pizzaTemplate;
	}
	
	public List<PizzaTemplate> findByMerchantId(int merchantId) {
		LOGGER.info("===============  PizzaServiceImpl : Inside findByMerchantId :: Start  ============= ");
		LOGGER.info(" === mercahnt : " + merchantId);
		LOGGER.info("===============  PizzaServiceImpl : Inside findByMerchantId :: End  ============= ");
		
		return pizzaTemplateRepository.findByMerchantId(merchantId);
	}
	
	public List<PizzaToppingSize> findByPizzaSizeId(Integer sizeId) {
		LOGGER.info("===============  PizzaServiceImpl : Inside findByPizzaSizeId :: Start  ============= ");
		LOGGER.info(" === sizeId : " + sizeId);
		List<PizzaToppingSize> pizzaToppingSizes = pizzaToppingSizeRepository.findByPizzaSizeId(sizeId);
		for (PizzaToppingSize pizzaToppingSize : pizzaToppingSizes) {
			pizzaToppingSize.setPizzaSize(null);
			pizzaToppingSize.getPizzaTopping().setMerchant(null);
			pizzaToppingSize.getPizzaTopping().setPizzaTemplateToppings(null);
			pizzaToppingSize.getPizzaTopping().setPizzaToppingSizes(null);
		}
		LOGGER.info("===============  PizzaServiceImpl : Inside findByPizzaSizeId :: End  ============= ");
		
		return pizzaToppingSizes;
	}
	
	public List<PizzaTemplateTopping> findAllPizzaToppings(Integer templateId, Integer sizeId, Integer merchantId) {
		LOGGER.info("===============  PizzaServiceImpl : Inside findAllPizzaToppings :: Start  ============= ");
		
		LOGGER.info(" === templateId : " + templateId + " sizeId : " + sizeId + " merchantId : " + merchantId);
		
		List<PizzaTemplateTopping> toppings = pizzaTemplateToppingRepository.findBySizeIdAndTemplateId(templateId,
																									   sizeId, merchantId);
		LOGGER.info("===============  PizzaServiceImpl : Inside findAllPizzaToppings :: End  ============= ");
		
		return toppings;
	}
	
	public List<PizzaTopping> findPizzaToppingMerchantId(Integer merchantId) {
		LOGGER.info("===============  PizzaServiceImpl : Inside findPizzaToppingMerchantId :: Start  ============= ");
		LOGGER.info(" ===merchantId : " + merchantId);
		LOGGER.info("===============  PizzaServiceImpl : Inside findPizzaToppingMerchantId :: End  ============= ");
		
		return pizzaToppingRepository.findByMerchantId(merchantId);
	}
	
	public List<PizzaSize> findPizzaSizeMerchantId(Integer merchantId) {
		LOGGER.info("===============  PizzaServiceImpl : Inside findPizzaSizeMerchantId :: Start  ============= ");
		
		LOGGER.info(" ===merchantId : " + merchantId);
		
		LOGGER.info("===============  PizzaServiceImpl : Inside findPizzaSizeMerchantId :: End  ============= ");
		
		return pizzaSizeRepository.findByMerchantId(merchantId);
	}
	
	public List<PizzaCrust> findPizzaCrustMerchantId(Integer merchantId) {
		LOGGER.info("===============  PizzaServiceImpl : Inside findPizzaCrustMerchantId :: Start  ============= ");
		
		LOGGER.info(" ===merchantId : " + merchantId);
		LOGGER.info("===============  PizzaServiceImpl : Inside findPizzaCrustMerchantId :: End  ============= ");
		
		return pizzaCrustRepository.findByMerchantId(merchantId);
	}
	
	public PizzaTopping findByToppingId(int toppingId) {
		LOGGER.info("===============  PizzaServiceImpl : Inside findByToppingId :: Start  ============= ");
		
		LOGGER.info(" ===toppingId : " + toppingId);
		
		PizzaTopping pizzaTopping = pizzaToppingRepository.findOne(toppingId);
		
		LOGGER.info("===============  PizzaServiceImpl : Inside findByToppingId :: End  ============= ");
		
		return pizzaTopping;
		
	}
	
	public PizzaTopping updatePizzaToppingById(PizzaTopping pizzaTopping) {
		LOGGER.info("===============  PizzaServiceImpl : Inside updatePizzaToppingById :: Start  ============= ");
		LOGGER.info(" === pizzaTopping : " + pizzaTopping.getId());
		PizzaTopping pizzaToppings = pizzaToppingRepository.findOne(pizzaTopping.getId());
		if (pizzaToppings != null) {
			pizzaToppings.setDescription(pizzaTopping.getDescription());
			pizzaToppings.setStatus(pizzaTopping.getStatus());
			pizzaToppings.setActive(pizzaTopping.getActive());
			
			pizzaToppingRepository.save(pizzaToppings);
		}
		
		List<Integer> pizzaSizeIdList = pizzaTopping.getSizeId();
		List<Double> pizzaPrice = pizzaTopping.getPrice();
		
		List<PizzaToppingSize> pizzaTopping1 = pizzaToppingSizeRepository.findByPizzaToppingIdAndPizzaSizeActive(pizzaTopping.getId() , 1);
		if (pizzaTopping1 != null && pizzaSizeIdList != null) {
			for (int i = 0; i < pizzaTopping1.size(); i++) {
				
				for (int j = 0; j < pizzaSizeIdList.size(); j++) {
					
					if (pizzaTopping1.get(i).getId().equals(pizzaSizeIdList.get(j))) {
						pizzaTopping1.get(i).setPrice(pizzaPrice.get(i));
						pizzaTopping1.get(i).setActive(0);
						break;
					} else {
						pizzaTopping1.get(i).setActive(1);
					}
				}
				
				pizzaToppingSizeRepository.saveAndFlush(pizzaTopping1.get(i));
				
			}
			
		}else if(pizzaSizeIdList == null
				|| pizzaSizeIdList.isEmpty()) {
			for(PizzaToppingSize pizzatoppingsize1 : pizzaTopping1) {
				pizzatoppingsize1.setActive(1);
				pizzaToppingSizeRepository.saveAndFlush(pizzatoppingsize1);
			}
		}
		
		
		//		if(pizzaSizeIdList!=null && !pizzaSizeIdList.isEmpty())
		//		{for(int i=0;i<pizzaSizeIdList.size();i++){
		//
		//			PizzaToppingSize pizzaTopping1=pizzaToppingSizeRepository.findByPizzaToppingId(pizzaSizeIdList.);
		//					if(pizzaTopping1!=null && pizzaPrice.get(i)!=null )
		//					{
		//						pizzaTopping1.setPrice(pizzaPrice.get(i));
		//						pizzaToppingSizeRepository.save(pizzaTopping1);
		//
		//					}
		//		}
		//
		//		}
		LOGGER.info("===============  PizzaServiceImpl : Inside updatePizzaToppingById :: End  ============= ");
		
		return pizzaToppings;
	}
	
	public List<PizzaTemplateSize> findByPizzaTemplateSizeById(Integer templateId) {
		LOGGER.info("===============  PizzaServiceImpl : Inside findByPizzaTemplateSizeById :: Start  ============= ");
		LOGGER.info(" === templateId : " + templateId);
		LOGGER.info("===============  PizzaServiceImpl : Inside findByPizzaTemplateSizeById :: End  ============= ");
		
		return pizzaTemplateSizeRepository.findByPizzaTemplateIdAndPizzaSizeActive(templateId,1);
	}
	
	public void savePizzaCrust(PizzaCrust pizzaCrust, Merchant merchant) {
		
		
		List<Integer> sizeIds = pizzaCrust.getSizeId();
		List<Double> sizePrice = pizzaCrust.getSizePrice();
		
		LOGGER.info(
				"===============  AdminHomeController : Inside savePizzaToppingMasterForm :: SizeId  ============= "
						+ sizeIds);
		
		int k = 0;
		pizzaCrust.setMerchant(merchant);
		pizzaCrust.setActive(1);
		
		PizzaCrust dbPizzacrust = pizzaCrustRepository.save(pizzaCrust);
		if (sizeIds != null && !sizeIds.isEmpty() && sizePrice != null && !sizePrice.isEmpty()) {
			for (int i = 0; i < sizeIds.size(); i++) {
				LOGGER.info(
						"===============  AdminHomeController : Inside savePizzaToppingMasterForm :: inside first loop  ============= ");
				Double sPrice = 0.0;
				for (int j = k; j < sizePrice.size(); j++) {
					LOGGER.info(
							"===============  AdminHomeController : Inside savePizzaToppingMasterForm :: inside second loop  ============= ");
					if (sizePrice.get(j) != null) {
						sPrice = sizePrice.get(j);
						k = i + 1;
						break;
					}
				}
				PizzaCrustSizes pizzaCrustSize = new PizzaCrustSizes();
				PizzaSize pizzaSize = new PizzaSize();
				pizzaSize.setId(sizeIds.get(i));
				pizzaCrustSize.setPizzaCrust(dbPizzacrust);
				pizzaCrustSize.setPrice(sPrice);
				pizzaCrustSize.setPizzaSize(pizzaSize);
				pizzaCrustSize.setActive(pizzaCrust.getActive());
				
				pizzaCrustSizeRepository.save(pizzaCrustSize);
				LOGGER.info(
						"===============  AdminHomeController : Inside savePizzaToppingMasterForm :: pizzaToppingSize save ============= ");
			}
		}
		
		if (pizzaCrust.getTempId() != null && pizzaCrust.getTempId().size() > 0) {
			
			List<PizzaTemplateCrust> pizzaTemplateCrusts = new ArrayList<PizzaTemplateCrust>();
			PizzaTemplate pizzaTemplate = null;
			PizzaTemplateCrust pizzaTemplateCrust = null;
			LOGGER.info(" ===pizzaCrust.getTempId() : " + pizzaCrust.getTempId());
			
			List<Integer> templateId = pizzaCrust.getTempId();
			
			for (Integer id : templateId) {
				pizzaTemplate = new PizzaTemplate();
				pizzaTemplate.setId(id);
				
				pizzaTemplateCrust = new PizzaTemplateCrust();
				pizzaTemplateCrust.setPizzaTemplate(pizzaTemplate);
				pizzaTemplateCrust.setPizzaCrust(dbPizzacrust);
				pizzaTemplateCrust.setActive(1);
				
				pizzaTemplateCrusts.add(pizzaTemplateCrust);
			}
			pizzaTemplateCrustRepository.save(pizzaTemplateCrusts);
			LOGGER.info("===============  PizzaServiceImpl : Inside savePizzaCrust :: End  ============= ");
			
		}
		
	}
	
	public List<PizzaTemplateSize> findPizzaTemplateSizeByTemplateId(List<Integer> templateId) {
		LOGGER.info(
				"===============  PizzaServiceImpl : Inside findPizzaTemplateSizeByTemplateId :: Start  ============= ");
		
		for (Integer id : templateId) {
			LOGGER.info(" === templateId : " + id);
		}
		List<PizzaTemplateSize> pizzaTemplateSizes = pizzaTemplateSizeRepository
				.findByPizzaTemplateIdAndActive(templateId, 1);
		
		for (PizzaTemplateSize pizzaTemplateSize : pizzaTemplateSizes) {
			pizzaTemplateSize.setPizzaTemplate(null);
			
			if (pizzaTemplateSize.getPizzaSize() != null) {
				pizzaTemplateSize.getPizzaSize().setMerchant(null);
				pizzaTemplateSize.getPizzaSize().setCategories(null);
				pizzaTemplateSize.getPizzaSize().setPizzaToppingSizes(null);
				pizzaTemplateSize.getPizzaSize().setPizzaTemplates(null);
				pizzaTemplateSize.getPizzaSize().setPizzaCrustSizes(null);
			}
		}
		LOGGER.info(
				"===============  PizzaServiceImpl : Inside findPizzaTemplateSizeByTemplateId :: End  ============= ");
		return pizzaTemplateSizes;
	}
	
	public String savePizzaSize(PizzaSize pizzaSize, Merchant merchant) {
		try {
			LOGGER.info("===============  PizzaServiceImpl : Inside savePizzaSize :: Start  ============= ");
			
			Double price = pizzaSize.getPrice();
			pizzaSize.setMerchant(merchant);
			pizzaSize.setActive(1);
			LOGGER.info(" === pizzaSize : " + pizzaSize.getDescription() + " merchant : " + merchant.getId());
			List<PizzaSize> pizzaSizeList = pizzaSizeRepository
					.findByMerchantIdAndDescriptionIgnoreCase(merchant.getId(), pizzaSize.getDescription());
			if (pizzaSizeList != null && pizzaSizeList.size() > 0) {
				LOGGER.info(" ==== pizzasize already exist ==== ");
				LOGGER.info("===============  PizzaServiceImpl : Inside savePizzaSize :: End  ============= ");
				
				return "Already Exist";
			}
			pizzaSize = pizzaSizeRepository.save(pizzaSize);
			if (pizzaSize.getTempId() != null && pizzaSize.getTempId().size() > 0) {
				Set<PizzaTemplate> pizzaTemplates = new HashSet<PizzaTemplate>();
				for (Integer templateId : pizzaSize.getTempId()) {
					LOGGER.info(" === templateId : " + templateId);
					PizzaTemplate pizzaTemplate = pizzaTemplateRepository.findById(templateId);
					PizzaTemplateSize pizzaTemplateSize = new PizzaTemplateSize();
					pizzaTemplateSize.setPizzaSize(pizzaSize);
					pizzaTemplateSize.setPizzaTemplate(pizzaTemplate);
					pizzaTemplateSize.setPrice(price);
					pizzaTemplateSize.setActive(1);
					pizzaTemplateSizeRepository.save(pizzaTemplateSize);
				}
				
			}
		} catch (Exception e) {
			LOGGER.error("===============  PizzaServiceImpl : Inside savePizzaSize :: Exception  ============= " + e);
			
			LOGGER.error("error: " + e.getMessage());
			MailSendUtil.sendExceptionByMail(e,environment);
			return "Failed";
		}
		LOGGER.info("===============  PizzaServiceImpl : Inside savePizzaSize :: End  ============= ");
		
		return "success";
	}
	
	public String saveNewTax(TaxRates taxRates, Merchant merchant) {
		try {
			LOGGER.info("===============  PizzaServiceImpl : Inside saveNewTax :: Start  ============= ");
			
			Double price = taxRates.getRate();
			taxRates.setMerchant(merchant);
			taxRates.setIsDefault(1);
			LOGGER.info(" === taxrates.getName() : " + taxRates.getName() + " merchant : " + merchant.getId());
			List<TaxRates> taxRatesList = taxRateRepository.findByMerchantIdAndNameIgnoreCase(merchant.getId(),
																							  taxRates.getName());
			if (taxRatesList != null && taxRatesList.size() > 0) {
				LOGGER.info(" === taxRates already exist === ");
				return "Already Exist";
			}
			taxRates = taxRateRepository.save(taxRates);
			//		if(taxRates.getTempId() != null && taxRates.getTempId().size() > 0){
			//			Set<PizzaTemplate> pizzaTemplates = new HashSet<PizzaTemplate>();
			//			for (Integer templateId : taxRates.getTempId()) {
			//				PizzaTemplate pizzaTemplate= pizzaTemplateRepository.findById(templateId);
			//				PizzaTemplateTax pizzaTemplateTax=new PizzaTemplateTax();
			//				pizzaTemplateTax.setPizzaTemplate(pizzaTemplate);
			//				pizzaTemplateTax.setTaxRates(taxRates);
			//				pizzaTemplateTax.setIsActive(1);
			//				pizzaTemplateTaxRepository.save(pizzaTemplateTax);
			//			}
			//		}
		} catch (Exception e) {
			LOGGER.error("===============  PizzaServiceImpl : Inside saveNewTax :: Exception  ============= " + e);
			
			LOGGER.error("error: " + e.getMessage());
			MailSendUtil.sendExceptionByMail(e,environment);
			return "Failed";
		}
		LOGGER.info("===============  PizzaServiceImpl : Inside saveNewTax :: End  ============= ");
		
		return "success";
	}
	
	public String filterPizzaTemplateIdAndPizzaCrustId(int merchant_id) {
		List<PizzaCrust> pizzacrustlist = null;
		pizzacrustlist = pizzaCrustRepository.findByMerchantId(merchant_id);
		
		//List<PizzaTemplateCrust> pizzatemplatecrustlist1 = new ArrayList<PizzaTemplateCrust>();
		List<PizzaCrust> pizzacrustlist1 = new ArrayList<PizzaCrust>();
		
		if (pizzacrustlist != null) {
			for (PizzaCrust pizzacrustlist3 : pizzacrustlist) {
				
				pizzacrustlist3.setPizzaCrustSizes(null);
				pizzacrustlist3.setMerchant(null);;
				PizzaCrust pizzaCrust = new PizzaCrust();
				
				String templateName = "";
				
				List<PizzaTemplateCrust> pizzatemplatecrustlist =
						pizzaTemplateCrustRepository.findByPizzaCrustId(pizzacrustlist3.getId());
				for(PizzaTemplateCrust pizzatemplateCrust : pizzatemplatecrustlist) {
					if (pizzatemplateCrust.getPizzaTemplate().getActive()==1 && pizzatemplateCrust.getActive()==1)
						templateName = templateName + "," + pizzatemplateCrust.getPizzaTemplate().getDescription();
					
				}
				
				pizzaCrust.setDescription(pizzacrustlist3.getDescription());
				
				templateName = templateName.trim();
				LOGGER.info(" === toppingName : " + templateName);
				if (!templateName.isEmpty()) {
					pizzaCrust.setPizza(templateName.substring(1, templateName.length()));
				} else {
					pizzaCrust.setPizza("");
				}
				
				
				//pizzaCrust.setCrust(pizzatemplatecrustlist3.getPizzaCrust().getDescription());
				
				if (pizzacrustlist3.getActive() == 1) {
					pizzaCrust.setStatus("<div><a href=javascript:void(0) class='nav-toggle' crstId="
												 + pizzacrustlist3.getId()
												 + " style='color: blue;'>Active</a></div>");
				} else {
					pizzaCrust.setStatus("<div><a href=javascript:void(0) class='nav-toggle' crstId="
												 + pizzacrustlist3.getId()
												 + " style='color: blue;'>InActive</a></div>");
				}
				pizzaCrust
						.setAction("<a href=editPizzaCrust?crstId=" + pizzacrustlist3.getId()
										   + " class='edit'><i class='fa fa-pencil' aria-hidden='true'></i></a>");
				
				//pizzaTemplateCrust.setPizzaCrust(pizzacrustlist3);
				pizzacrustlist1.add(pizzaCrust);
				
			}
		} else {
			System.out.println("PIZZA CRUST OBJECT IS NULLLLL");
		}
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		return gson.toJson(pizzacrustlist1);
	}
	
	
	public PizzaCrust findByCrustId(Integer crustId) {
		PizzaCrust pizzacrust = pizzaCrustRepository.findOne(crustId);
		return pizzacrust;
	}
	
	public PizzaCrust updatePizzaCrustById(PizzaCrust pizzaCrust, Integer merchant_id) {
		PizzaCrust pizzaCrusts = pizzaCrustRepository.findOne(pizzaCrust.getId());
		if (pizzaCrusts != null) {
			pizzaCrusts.setDescription(pizzaCrust.getDescription());
			pizzaCrusts.setStatus(pizzaCrust.getStatus());
			pizzaCrusts.setActive(pizzaCrust.getActive());
			
			pizzaCrustRepository.save(pizzaCrusts);
		}
		
		List<Integer> pizzaSizeIdList = pizzaCrust.getSizeId();
		List<Double> pizzaPrice = pizzaCrust.getPricelist();
		
		List<PizzaCrustSizes> pizzaCrust1 = pizzaCrustSizeRepository.findByPizzaCrustIdAndPizzaSizeActive(pizzaCrust.getId() , 1);
		
		List<PizzaSize> pizzaSizes = pizzaSizeRepository.findByIdAndMerchantId(pizzaSizeIdList, merchant_id);
		
		if (pizzaCrust1 != null && pizzaSizeIdList != null) {
			for (int i = 0; i < pizzaCrust1.size(); i++) {
				for (int j = 0; j < pizzaSizeIdList.size(); j++) {
					
					if (pizzaCrust1.get(i).getId().equals(pizzaSizeIdList.get(j))) {
						pizzaCrust1.get(i).setPrice(pizzaPrice.get(i));
						pizzaCrust1.get(i).setActive(1);
						break;
					}
					else {
						pizzaCrust1.get(i).setActive(0);
					}
				}
				pizzaCrustSizeRepository.saveAndFlush(pizzaCrust1.get(i));
				
			}
			
		}else if(pizzaSizeIdList == null
				|| pizzaSizeIdList.isEmpty()) {
			for(PizzaCrustSizes pizzaCrustsize1 : pizzaCrust1) {
				pizzaCrustsize1.setActive(0);
				pizzaCrustSizeRepository.saveAndFlush(pizzaCrustsize1);
			}
		}
		return pizzaCrusts;
		
	}
	
	public void updateCrustStatusById(PizzaCrust crust) {
		LOGGER.info("===== PizzaServiceImpl : inside updateCrustStatusById : Starts ========");
		LOGGER.info("===== PizzaCrust id ==== " + crust.getId());
		LOGGER.info("===== PizzaCrust getActive() ==== " + crust.getActive());
		PizzaCrust result = pizzaCrustRepository.findOne(crust.getId());
		result.setActive(crust.getActive());
		pizzaCrustRepository.save(result);
		LOGGER.info("===== PizzaServiceImpl : inside updateCrustStatusById : End ========");
		
	}
	
	public void updatePizzaCrustMapping(PizzaCrust pizzaCrust , Integer merchantId) {
		// TODO Auto-generated method stub
		PizzaCrust resultPizzaCrust = pizzaCrustRepository.findOne(pizzaCrust.getId());
		
		
		List<PizzaTemplateCrust> pizzaTemplateCrust=
				pizzaTemplateCrustRepository.findByPizzaCrustIdAndPizzaTemplateActive(pizzaCrust.getId(), 1);
		
		if(pizzaTemplateCrust!=null &&  !pizzaTemplateCrust.isEmpty())
		{
			for (PizzaTemplateCrust pizzaTemplateCrust2 : pizzaTemplateCrust) {
				if(pizzaCrust.getMappizzaId()!=null && !pizzaCrust.getMappizzaId().isEmpty())
				{
					for(Integer ids:pizzaCrust.getMappizzaId())
					{
						if(pizzaTemplateCrust2.getPizzaTemplate().getId().equals(ids))
						{
							pizzaTemplateCrust2.setActive(1);
							break;
						}
						else pizzaTemplateCrust2.setActive(0);
					}
					pizzaTemplateCrustRepository.save(pizzaTemplateCrust2);
				}else { pizzaTemplateCrust2.setActive(0);
					pizzaTemplateCrustRepository.save(pizzaTemplateCrust2);
				}
			}
			
		}
		
		
		
		List<PizzaTemplate> dbunmappedPizzas = pizzaTemplateRepository
				.findUnMappedpizzaByCrustIdAndPizzaActive(pizzaCrust.getId(),merchantId , 1);
		System.err.println("");
		if(dbunmappedPizzas!=null &&  !dbunmappedPizzas.isEmpty())
		{
			for (PizzaTemplate unMapPizzas : dbunmappedPizzas) {
				if(pizzaCrust.getUnmappizzaId()!=null && !pizzaCrust.getUnmappizzaId().isEmpty())
				{
					for(Integer ids:pizzaCrust.getUnmappizzaId())
					{
						if(unMapPizzas.getId().equals(ids))
						{
							PizzaTemplateCrust pizzatemplateCrust =new PizzaTemplateCrust();
							pizzatemplateCrust.setPizzaTemplate(unMapPizzas);
							pizzatemplateCrust.setPizzaCrust(resultPizzaCrust);
							pizzatemplateCrust.setActive(1);
							pizzaTemplateCrustRepository.save(pizzatemplateCrust);
							break;
						}
					}
					
				}
			}
			
		}
		
		LOGGER.info("===============  ModifierServiceImpl : Inside updateModifierGroupValue :: End  ============= ");
		
	}
	
	public String showPizzaSizeData(Integer merchantId) {
		
		LOGGER.info("===============  PizzaServiceImpl : Inside showPizzaSizeData :: Start  ============= ");
		LOGGER.info(" pizzaSizeId  :  merchant " + merchantId);
		
		LOGGER.info("showPizzaSizeData");
		
		List<PizzaSize> pizzaSizelist = pizzaSizeRepository.findByMerchantId(merchantId);
		
		
		List<PizzaSize> pizzaSizesdata = new ArrayList<PizzaSize>();
		List<Integer> sizeIds = new ArrayList<Integer>();
		for(PizzaSize pizzaSizes : pizzaSizelist) {
			
			
			Boolean flag = true;
			
			pizzaSizes.setCategories(null);
			pizzaSizes.setPizzaCrustSizes(null);
			pizzaSizes.setPizzaTemplates(null);
			pizzaSizes.setPizzaToppingSizes(null);
			pizzaSizes.setMerchant(null);
			PizzaSize size = new PizzaSize();
			size.setId(pizzaSizes.getId());
			size.setDescription(pizzaSizes.getDescription());
			size.setActive(pizzaSizes.getActive());
			
			PizzaToppingSize pizzaToppingSize2 = new PizzaToppingSize();
			PizzaSize pizzaSize2 = new PizzaSize();
			pizzaSize2.setId(pizzaSizes.getId());
			pizzaSize2.setActive(pizzaSizes.getActive());
			
			String toppingName = "";
			
			List<PizzaToppingSize> pizzatoppinglist = pizzaToppingSizeRepository.findByPizzaSizeId(pizzaSizes.getId());
			for(PizzaToppingSize pizzatoppinglist2 : pizzatoppinglist) {
				if(pizzatoppinglist2.getPizzaTopping().getActive()==1 && pizzatoppinglist2.getActive() == 0)
					toppingName = toppingName + "," + pizzatoppinglist2.getPizzaTopping().getDescription();
			}
			
			toppingName = toppingName.trim();
			LOGGER.info(" === toppingName : " + toppingName);
			if (!toppingName.isEmpty()) {
				pizzaSize2.setTopping(toppingName.substring(1, toppingName.length()));
			} else {
				pizzaSize2.setTopping("");
			}
			
			pizzaSize2.setDescription(pizzaSizes.getDescription());
			
			if (pizzaSizes.getActive() == 1) {
				pizzaSize2.setStatus("<div><a href=javascript:void(0) class='nav-toggle' itmId="
											 + pizzaSizes.getId() + " style='color: blue;'>Active</a></div>");
			} else {
				pizzaSize2.setStatus("<div><a href=javascript:void(0) class='nav-toggle' itmId="
											 + pizzaSizes.getId() + " style='color: blue;'>InActive</a></div>");
			}
			
			pizzaSize2.setAction("<a href=editPizzaSize?sizeId=" + pizzaSizes.getId()
										 + " class='edit'><i class='fa fa-pencil' aria-hidden='true'></i></a>");
			
			//pizzaToppingSize2.setPizzaSize(pizzaSizes);
			pizzaSizes.setId(pizzaSizes.getId());
			pizzaSizesdata.add(pizzaSize2);
		}
		
		
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		LOGGER.info("===============  PizzaServiceImpl : Inside showPizzaSizeData :: End  ============= ");
		
		return gson.toJson(pizzaSizesdata);
		
	}
	
	
	public void updateSizeStatusById(PizzaSize pizzaSize) {
		LOGGER.info("===============  PizzaServiceImpl : Inside updateSizeStatusById :: Start  ============= ");
		
		LOGGER.info(" ===pizzaSize : " + pizzaSize.getId());
		
		PizzaSize result = pizzaSizeRepository.findOne(pizzaSize.getId());
		result.setActive(pizzaSize.getActive());
		pizzaSizeRepository.save(result);
		LOGGER.info("===============  PizzaServiceImpl : Inside updateSizeStatusById :: End  ============= ");
		
	}
	
	public PizzaSize findSizeByPizzaSizeId(int sizeId) {
		
		LOGGER.info("===============  PizzaServiceImpl : Inside findByPizzaSizeId :: Start  ============= ");
		
		LOGGER.info(" ===sizeId : " + sizeId);
		
		PizzaSize pizzaSize = pizzaSizeRepository.findOne(sizeId);
		
		LOGGER.info("===============  PizzaServiceImpl : Inside findByPizzaSizeId :: End  ============= ");
		
		return pizzaSize;
		
	}
	
	public void updatePizzaSizeValue(PizzaSize pizzaSize, Integer merchantId) {
		// TODO Auto-generated method stub
		PizzaSize resultPizzaSize = pizzaSizeRepository.findOne(pizzaSize.getId());
		
		resultPizzaSize.setDescription(pizzaSize.getDescription());
		resultPizzaSize.setActive(pizzaSize.getActive());
		List<PizzaTemplateSize> pizzaTemplateSize=
				pizzaTemplateSizeRepository.findByPizzaSizeIdAndPizzaTemplateActive(pizzaSize.getId(),1);
		
		if(pizzaTemplateSize!=null &&  !pizzaTemplateSize.isEmpty())
		{
			for (PizzaTemplateSize pizzaTemplateSize2 : pizzaTemplateSize) {
				if(pizzaSize.getTempId()!=null && !pizzaSize.getTempId().isEmpty())
				{
					for(Integer ids:pizzaSize.getTempId())
					{
						if(pizzaTemplateSize2.getPizzaTemplate().getId().equals(ids))
						{
							pizzaTemplateSize2.setActive(1);
							break;
						}
						else pizzaTemplateSize2.setActive(0);
					}
					pizzaTemplateSizeRepository.save(pizzaTemplateSize2);
				}else { pizzaTemplateSize2.setActive(0);
					pizzaTemplateSizeRepository.save(pizzaTemplateSize2);
				}
			}
			
		}
		
		
		
		List<PizzaTemplate> dbunmappedTemplates = pizzaTemplateRepository
				.findUnMappedpizzaBySizeIdAndPizzaActive(pizzaSize.getId(),merchantId ,1);
		if(dbunmappedTemplates!=null &&  !dbunmappedTemplates.isEmpty())
		{
			for (PizzaTemplate unMapTemplate : dbunmappedTemplates) {
				if(pizzaSize.getUnmaptempId()!=null && !pizzaSize.getUnmaptempId().isEmpty())
				{
					for(Integer ids:pizzaSize.getUnmaptempId())
					{
						if(unMapTemplate.getId().equals(ids))
						{
							PizzaTemplateSize pizzaTemplateSizeobj =new PizzaTemplateSize();
							pizzaTemplateSizeobj.setPizzaTemplate(unMapTemplate);
							pizzaTemplateSizeobj.setPizzaSize(resultPizzaSize);
							pizzaTemplateSizeobj.setActive(1);
							pizzaTemplateSizeRepository.save(pizzaTemplateSizeobj);
							break;
						}
					}
					
				}
			}
			
		}
		
		List<PizzaToppingSize> pizzaToppingSize=
				pizzaToppingSizeRepository.findByPizzaSizeIdAndPizzaToppingActive(pizzaSize.getId() ,1);
		
		if(pizzaToppingSize!=null &&  !pizzaToppingSize.isEmpty())
		{
			for (PizzaToppingSize pizzaToppingSize2 : pizzaToppingSize) {
				if(pizzaSize.getMappizzatopping()!=null && !pizzaSize.getMappizzatopping().isEmpty())
				{
					for(Integer ids:pizzaSize.getMappizzatopping())
					{
						if(pizzaToppingSize2.getPizzaTopping().getId().equals(ids))
						{
							pizzaToppingSize2.setActive(0);
							break;
						}
						else pizzaToppingSize2.setActive(1);
					}
					pizzaToppingSizeRepository.save(pizzaToppingSize2);
				}else { pizzaToppingSize2.setActive(1);
					pizzaToppingSizeRepository.save(pizzaToppingSize2);
				}
			}
			
		}
		
		
		
		List<PizzaTopping> dbunmappedTopping = pizzaToppingRepository
				.findUnMappedToppingBySizeIdAndToppingActive(pizzaSize.getId(),merchantId,1);
		if(dbunmappedTopping!=null &&  !dbunmappedTopping.isEmpty())
		{
			for (PizzaTopping unMapTopping : dbunmappedTopping) {
				if(pizzaSize.getUnmappizzatopping()!=null && !pizzaSize.getUnmappizzatopping().isEmpty())
				{
					for(Integer ids:pizzaSize.getUnmappizzatopping())
					{
						if(unMapTopping.getId().equals(ids))
						{
							PizzaToppingSize pizzaToppingSizeobj =new PizzaToppingSize();
							pizzaToppingSizeobj.setPizzaTopping(unMapTopping);;
							pizzaToppingSizeobj.setPizzaSize(resultPizzaSize);
							pizzaToppingSizeobj.setActive(0);
							pizzaToppingSizeRepository.save(pizzaToppingSizeobj);
							break;
						}
					}
					
				}
			}
			
		}
		
		List<PizzaCrustSizes> pizzaCrustSizes=
				pizzaCrustSizeRepository.findByPizzaSizeIdAndPizzaCrustActive(pizzaSize.getId(),1);
		
		if(pizzaCrustSizes!=null &&  !pizzaCrustSizes.isEmpty())
		{
			for (PizzaCrustSizes pizzaCrustSizes2 : pizzaCrustSizes) {
				if(pizzaSize.getMappizzaCrust()!=null && !pizzaSize.getMappizzaCrust().isEmpty())
				{
					for(Integer ids:pizzaSize.getMappizzaCrust())
					{
						if(pizzaCrustSizes2.getPizzaCrust().getId().equals(ids))
						{
							pizzaCrustSizes2.setActive(1);
							break;
						}
						else pizzaCrustSizes2.setActive(0);
					}
					pizzaCrustSizeRepository.save(pizzaCrustSizes2);
				}else { pizzaCrustSizes2.setActive(0);
					pizzaCrustSizeRepository.save(pizzaCrustSizes2);
				}
			}
			
		}
		
		
		
		List<PizzaCrust> dbunmappedCrust = pizzaCrustRepository
				.findUnMappedCrustBySizeIdAndCrustActive(pizzaSize.getId(),merchantId ,1);
		if(dbunmappedCrust!=null &&  !dbunmappedCrust.isEmpty())
		{
			for (PizzaCrust unMapCrust : dbunmappedCrust) {
				if(pizzaSize.getUnmappizzaCrust()!=null && !pizzaSize.getUnmappizzaCrust().isEmpty())
				{
					for(Integer ids:pizzaSize.getUnmappizzaCrust())
					{
						if(unMapCrust.getId().equals(ids))
						{
							PizzaCrustSizes pizzaCrustSizeobj =new PizzaCrustSizes();
							pizzaCrustSizeobj.setPizzaCrust(unMapCrust);
							pizzaCrustSizeobj.setPizzaSize(resultPizzaSize);
							pizzaCrustSizeobj.setActive(1);
							pizzaCrustSizeRepository.save(pizzaCrustSizeobj);
							break;
						}
					}
					
				}
			}
			
		}
		
		pizzaSizeRepository.save(resultPizzaSize);
		LOGGER.info("===============  ModifierServiceImpl : Inside updateModifierGroupValue :: End  ============= ");
		
		
		
	}
	
	
	
}
