package com.foodkonnekt.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.foodkonnekt.model.*;
import com.foodkonnekt.repository.PrintJobRepository;
import com.foodkonnekt.service.PrintJobService;

@Service
public class PrintJobServiceImpl implements PrintJobService {

	@Autowired
	private PrintJobRepository printJobRepository;
	public PrintJob saveOrUpdate(PrintJob printJob) {
		
		return printJobRepository.save(printJob);
	}
	public PrintJob findByOrderId(String orderId) {
		
		return printJobRepository.findByOrderId(orderId);
		
	}
	public List<PrintJob> findByMerchantId(Integer merchantId) {
		
		return printJobRepository.findByMerchantId(merchantId);
		
	}

}
