package com.foodkonnekt.serviceImpl;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.foodkonnekt.model.Merchant;
import com.foodkonnekt.model.NotificationApp;
import com.foodkonnekt.model.NotificationAppStatus;
import com.foodkonnekt.repository.MerchantRepository;
import com.foodkonnekt.repository.NotificationAppStatusRepository;
import com.foodkonnekt.service.NotificationAppStatusService;
import com.foodkonnekt.util.DateUtil;

@Service
public class NotificationAppStatusServiceImpl implements
		NotificationAppStatusService {
	private static final Logger LOGGER= LoggerFactory.getLogger(NotificationAppStatusServiceImpl.class);

    @Autowired
    private NotificationAppStatusRepository notificationAppStatusRepository;
    
    @Autowired
    private MerchantRepository merchantRepository;

	public List<NotificationAppStatus> findByMerchantid(Integer merchantId) {
		LOGGER.info("===============  NotificationAppStatusServiceImpl : Inside findByMerchantid :: Start  =============merchantId "+merchantId);

		List<NotificationAppStatus> listNotificationAppStatus = notificationAppStatusRepository.findAll();
LOGGER.info("===============  NotificationAppStatusServiceImpl : Inside findByMerchantid :: End  ============= ");

		return listNotificationAppStatus;
	}
	
	public List<NotificationAppStatus> findByAppId(Integer appId){
LOGGER.info("===============  NotificationAppStatusServiceImpl : Inside findByAppId :: Start  =============appId "+appId);

		List<NotificationAppStatus> listNotificationAppStatus = notificationAppStatusRepository.findByAppId(appId);
		for(NotificationAppStatus l : listNotificationAppStatus ) {
		System.out.println(l.getMerchantid());
		}
LOGGER.info("===============  NotificationAppStatusServiceImpl : Inside findByAppId :: End  ============= ");

		return listNotificationAppStatus;
	}

	public NotificationAppStatus findByMerchantIdandAppId(Integer merchantId, NotificationApp notificationApp) {
LOGGER.info("===============  NotificationAppStatusServiceImpl : Inside findByMerchantIdandAppId :: Start  =============merchantId "+merchantId+" notificationApp "+notificationApp.getId());

		Merchant merchant = merchantRepository.findById(merchantId);
		Date today = (merchant != null && merchant.getTimeZone() != null
				&& merchant.getTimeZone().getTimeZoneCode() != null)
						? DateUtil.getCurrentDateForTimeZonee(merchant.getTimeZone().getTimeZoneCode())
						: new Date();
		NotificationAppStatus notificationAppStatus = new NotificationAppStatus();
		notificationAppStatus.setMerchantid(merchantId);
		notificationAppStatus.setAppId(notificationApp.getId());
		notificationAppStatus.setIsRunning(0);
		notificationAppStatus.setUpdatedDate(today);	
		notificationAppStatus.setIsActive(0);
		NotificationAppStatus appStatus = notificationAppStatusRepository.save(notificationAppStatus);
LOGGER.info("===============  NotificationAppStatusServiceImpl : Inside findByMerchantIdandAppId :: End  ============= ");

		return appStatus;
	}

}


