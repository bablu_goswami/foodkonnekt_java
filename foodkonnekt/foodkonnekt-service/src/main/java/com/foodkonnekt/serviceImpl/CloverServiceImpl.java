package com.foodkonnekt.serviceImpl;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.foodkonnekt.clover.vo.CloverDiscountVO;
import com.foodkonnekt.clover.vo.CloverOrderVO;
import com.foodkonnekt.clover.vo.Modifications;
import com.foodkonnekt.clover.vo.OrderItemVO;
import com.foodkonnekt.clover.vo.OrderVO;
import com.foodkonnekt.model.Address;
import com.foodkonnekt.model.Category;
import com.foodkonnekt.model.Clover;
import com.foodkonnekt.model.Customer;
import com.foodkonnekt.model.Item;
import com.foodkonnekt.model.ItemDto;
import com.foodkonnekt.model.Merchant;
import com.foodkonnekt.model.MerchantConfiguration;
import com.foodkonnekt.model.MerchantOrders;
import com.foodkonnekt.model.MerchantSubscription;
import com.foodkonnekt.model.OpeningClosingDay;
import com.foodkonnekt.model.OpeningClosingTime;
import com.foodkonnekt.model.OrderR;
import com.foodkonnekt.model.OrderType;
import com.foodkonnekt.model.PaymentMode;
import com.foodkonnekt.model.Pos;
import com.foodkonnekt.model.Subscription;
import com.foodkonnekt.model.TaxRates;
import com.foodkonnekt.model.TimeZone;
import com.foodkonnekt.model.Vendor;
import com.foodkonnekt.repository.AddressRepository;
import com.foodkonnekt.repository.CategoryRepository;
import com.foodkonnekt.repository.CustomerrRepository;
import com.foodkonnekt.repository.ItemmRepository;
import com.foodkonnekt.repository.MerchantConfigurationRepository;
import com.foodkonnekt.repository.MerchantOrdersRepository;
import com.foodkonnekt.repository.MerchantRepository;
import com.foodkonnekt.repository.MerchantSubscriptionRepository;
import com.foodkonnekt.repository.OpeningClosingDayRepository;
import com.foodkonnekt.repository.OpeningClosingTimeRepository;
import com.foodkonnekt.repository.OrderRepository;
import com.foodkonnekt.repository.OrderTypeRepository;
import com.foodkonnekt.repository.PaymentModeRepository;
import com.foodkonnekt.repository.RoleRepository;
import com.foodkonnekt.repository.SubscriptionRepository;
import com.foodkonnekt.repository.TaxRateRepository;
import com.foodkonnekt.repository.TimeZoneRepository;
import com.foodkonnekt.repository.VendorRepository;
import com.foodkonnekt.service.CloverService;
import com.foodkonnekt.service.MerchantConfigurationService;
import com.foodkonnekt.util.CloverUrlUtil;
import com.foodkonnekt.util.DateUtil;
import com.foodkonnekt.util.EncryptionDecryptionUtil;
import com.foodkonnekt.util.IConstant;
import com.foodkonnekt.util.JsonUtil;
import com.foodkonnekt.util.MailSendUtil;
import com.google.gson.Gson;

@Service
@Transactional
public class CloverServiceImpl extends JsonUtil implements CloverService {
	private static final Logger LOGGER= LoggerFactory.getLogger(CloverServiceImpl.class);

	@Autowired
    private Environment environment;
	
    @Autowired
    private MerchantRepository merchantRepository;
    
    @Autowired
    private TimeZoneRepository timeZoneRepository;

    @Autowired
    private SubscriptionRepository subscriptionRepository;

    @Autowired
    private VendorRepository vendorRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private AddressRepository addressRepository;

    @Autowired
    private PaymentModeRepository paymentModeRepository;

    @Autowired
    private OrderTypeRepository orderTypeRepository;

    @Autowired
    private TaxRateRepository taxRateRepository;

    @Autowired
    private OpeningClosingDayRepository openingClosingDayRepository;

    @Autowired
    private OpeningClosingTimeRepository openingClosingTimeRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private ItemmRepository itemRepository;

    @Autowired
    private OrderRepository ordeRepo;

    @Autowired
    private MerchantSubscriptionRepository merchantSubscriptionRepository;

    @Autowired
    private MerchantOrdersRepository merchantOrdersRepository;
    
    @Autowired
    private CustomerrRepository customerrRepository;
    
    @Autowired
	private MerchantConfigurationRepository merchantConfigurationRepository;
    
    @Autowired
   	private MerchantConfigurationService merchantConfigurationService;
    
    /**
     * Save vendor,merchant and address
     */
    public void deleteCloverEmployees(Clover clover){
    	LOGGER.info("----------------Start :: CloverServiceImpl : deleteCloverEmployees------------------------");

    	String employeeJson=CloverUrlUtil.getEmployeesFromClover(clover);
        if(employeeJson.contains("{")){
        	JSONObject employeeJsonObject = new JSONObject(employeeJson);
        	if(employeeJsonObject.has("elements")){
	        JSONArray jSONArray = employeeJsonObject.getJSONArray("elements");
        for (Object jObj : jSONArray) {
            JSONObject jObject = (JSONObject) jObj;
            

            if(jObject.has("id") && jObject.has("name")){
				if(jObject.getString("name").equals("FoodKonnekt Employee")){
					String deleteEmployeeResponse =CloverUrlUtil.deleteEmployee(clover,jObject.getString("id"));
					System.out.println("Employee Delete Response ->"+deleteEmployeeResponse);
			    	LOGGER.info(" CloverServiceImpl : deleteCloverEmployees: Employee Delete Response ->"+deleteEmployeeResponse);

				}
				
			}
               
            
        }
        	}
        }
    }
    public Merchant saveMerchant(Clover clover) {
    	LOGGER.info("----------------Start :: CloverServiceImpl : saveMerchant------------------------");

        String billingInfo = CloverUrlUtil.getMerchantBilling(clover,environment);
        String merchantDetails = CloverUrlUtil.getMerchantDetails(clover);
    	LOGGER.info(" CloverServiceImpl : saveMerchant: billingInfo:"+billingInfo+":merchantDetails:"+merchantDetails);
 
        MerchantSubscription  merchantSubscription = JsonUtil.setSubscription(billingInfo);
                                
        //Subscription subscription = JsonUtil.setSubscription(billingInfo);
        Subscription subscription=null;
        if (merchantSubscription != null && merchantSubscription.getSubscription() != null) {
        	 subscription = merchantSubscription.getSubscription();
        	LOGGER.info(" CloverServiceImpl : saveMerchant: SubscriptionPlanId:"+subscription.getSubscriptionPlanId());
             subscription = subscriptionRepository.findBySubscriptionPlanId(subscription.getSubscriptionPlanId());
            /* System.out.println(subscription.getSubscriptionPlanId()); */
        }
    	LOGGER.info(" CloverServiceImpl : saveMerchant: merchantId:"+clover.getMerchantId());

        Merchant newMerchant = merchantRepository.findByPosMerchantId(clover.getMerchantId());
        if (null == newMerchant) {
            newMerchant = new Merchant();
            newMerchant.setIsInstall(IConstant.BOOLEAN_FALSE);
            newMerchant.setUpdatedDate(DateUtil.findCurrentDate());
            newMerchant.setCreatedDate(DateUtil.findCurrentDate());
            newMerchant.setActiveCustomerFeedback(0);
            newMerchant.setAllowDeliveryTiming(0);
            newMerchant.setFutureDaysAhead(0);
            newMerchant.setAllowReOrder(false);
            newMerchant.setAllowAuxTax(0);
            newMerchant.setAllowMultiPay(false);
            newMerchant.setAllowMultipleKoupon(0);
            /*deleteCloverEmployees(clover);
            String employeePosId= CloverUrlUtil.createEmployeeOnClover(clover);
            if(employeePosId!=null)
            newMerchant.setEmployeePosId(employeePosId);*/
            
        } else {
        	String employeePosId=newMerchant.getEmployeePosId();
        	LOGGER.info(" CloverServiceImpl : saveMerchant: employeePosId:"+employeePosId);

        	if(employeePosId==null || employeePosId.isEmpty() || employeePosId.equals("")){
        		 employeePosId= CloverUrlUtil.createEmployeeOnClover(clover);
        	}
        	LOGGER.info(" CloverServiceImpl : saveMerchant: IsInstall:"+newMerchant.getIsInstall());

        	if(newMerchant.getIsInstall()!=null && newMerchant.getIsInstall()==2)
            newMerchant.setIsInstall(IConstant.BOOLEAN_TRUE);
        }
        newMerchant = JsonUtil.setMerchant(merchantDetails, newMerchant);
    	LOGGER.info(" CloverServiceImpl : saveMerchant: EmailId:"+newMerchant.getOwner().getEmail());

        Vendor vendor = vendorRepository.findByEmail(newMerchant.getOwner().getEmail());
        Pos pos = null;
        if (vendor == null) {
            pos = new Pos();
            pos.setPosId(IConstant.POS_CLOVER);
            newMerchant.getOwner().setPos(pos);
            System.out.println("if insde clverserviceImpl---");
        	LOGGER.info(" CloverServiceImpl : saveMerchant: if insde clverserviceImpl---:");

            vendorRepository.save(newMerchant.getOwner());
        } else {
            System.out.println("else insde clverserviceImpl---" + vendor);
        	LOGGER.info(" CloverServiceImpl : saveMerchant: else insde clverserviceImpl---" + vendor);

            newMerchant.setOwner(vendor);
        }
        newMerchant.setAccessToken(clover.getAuthToken());
        newMerchant.setSubscription(subscription);
        newMerchant.setUpdatedDate(DateUtil.findCurrentDate());
        newMerchant = merchantRepository.save(newMerchant);
    	LOGGER.info(" CloverServiceImpl : saveMerchant: merchant save ");
 
        String addressJson = CloverUrlUtil.getMerchantAddress(clover);
        List<Address> addresses = addressRepository.findByMerchantId(newMerchant.getId());
        Address address = null;
        if (addresses.isEmpty()) {
            address = new Address();
            
            
        } else {
            address = addresses.get(IConstant.BOOLEAN_FALSE);
        }
        address = JsonUtil.setAddress(addressJson, newMerchant, address);
        if(addresses==null ||addresses.isEmpty() ||addresses.size()==0){
        	addresses = new ArrayList<Address>();
        	addresses.add(address);
        	
        }
        
        
        
        
        if (addressJson.contains("phoneNumber"))
            merchantRepository.save(address.getMerchant());
    	LOGGER.info(" CloverServiceImpl : saveMerchant: merchant save ");

        addressRepository.save(address);
    	LOGGER.info(" CloverServiceImpl : saveMerchant: Address save ");

        if(address!=null && address.getZip()!=null){
        	String timeZoneCode=DateUtil.getTimeZone(address,environment);
        	if(timeZoneCode!=null){
            	LOGGER.info(" CloverServiceImpl : saveMerchant: timeZone :"+timeZoneCode);

        		TimeZone timeZone=timeZoneRepository.findByTimeZoneCode(timeZoneCode);
        		newMerchant.setTimeZone(timeZone);
            	LOGGER.info(" CloverServiceImpl : saveMerchant: merchant save ");

        		merchantRepository.save(newMerchant);
        	}
        	}

        // Save merchant subscription
    	LOGGER.info(" CloverServiceImpl : saveMerchant:  Save merchant subscription");
    	LOGGER.info(" CloverServiceImpl : saveMerchant:  new merchantId:"+newMerchant.getId());

        Integer merchantSubscriptionId = merchantSubscriptionRepository.findByMerchantId(newMerchant.getId());
    	LOGGER.info(" CloverServiceImpl : saveMerchant: merchantSubscriptionId:"+merchantSubscriptionId);

        if (merchantSubscriptionId != null) {
            merchantSubscription = merchantSubscriptionRepository.findOne(merchantSubscriptionId);
        } else {
            
            merchantSubscription.setMerchant(newMerchant);
        }
        merchantSubscription.setSubscription(subscription);
        merchantSubscriptionRepository.save(merchantSubscription);
    	LOGGER.info(" CloverServiceImpl : saveMerchant: save merchantSubscription:");

        List<MerchantSubscription> merchantSubscriptions = new ArrayList<MerchantSubscription>();
        merchantSubscriptions.add(merchantSubscription);
        newMerchant.setMerchantSubscriptions(merchantSubscriptions);
        
        // Save merchant orders count information
    	LOGGER.info(" CloverServiceImpl : saveMerchant:Save merchant orders count information:");
    	LOGGER.info(" CloverServiceImpl : saveMerchant:  merchantSubscriptionId :"+merchantSubscription.getId());

        MerchantOrders merchantOrders = merchantOrdersRepository.findByMerchantSubscriptionId(merchantSubscription
                        .getId());
        if (merchantOrders == null) {
            merchantOrders = new MerchantOrders();
            merchantOrders.setOrderCount(IConstant.BOOLEAN_FALSE);
            merchantOrders.setMerchantSubscription(merchantSubscription);
            merchantOrders.setStartDate(DateUtil.currentDate());
            merchantOrders.setEndDate(DateUtil.incrementedDate());
            merchantOrdersRepository.save(merchantOrders);
        	LOGGER.info(" CloverServiceImpl : saveMerchant:Save merchantOrders:");

        }
        newMerchant.setAddresses(addresses);
        merchantConfigurationService.saveMerchantConfiguration(newMerchant.getId(),IConstant.TRUE_STATUS,IConstant.FALSE_STATUS,IConstant.FALSE_STATUS);
		LOGGER.info("CloverServiceImpl : Inside saveMerchant : End");
  
        return newMerchant;
    }

    public void saveOrderType(String orderTypeDetail, Merchant merchant, Clover clover) {
    	LOGGER.info(" start :: CloverServiceImpl : saveOrderType :");

        List<OrderType> orderTypeList = setOrderType(orderTypeDetail, merchant, clover);
    	LOGGER.info(" End :: CloverServiceImpl : saveOrderType : save orderTypeList");

        orderTypeRepository.save(orderTypeList);
    }
    
    public void saveOrderType(Merchant merchant) {
    	LOGGER.info(" start :: CloverServiceImpl : saveOrderType :");

        List<OrderType> orderTypeList = setOrderTypesForNonPos(merchant);
    	LOGGER.info(" End :: CloverServiceImpl : saveOrderType : save orderTypeList:");
 
        orderTypeRepository.save(orderTypeList);
    }
   
    
    public void savePaymentMode( Merchant merchant) {
    	LOGGER.info(" start :: CloverServiceImpl : savePaymentMode :");

        List<PaymentMode> paymentModeList = setPaymentMode(merchant);
        if(paymentModeList!=null)
        	LOGGER.info(" End :: CloverServiceImpl : savePaymentMode :");

        paymentModeRepository.save(paymentModeList);

    }
   
    
    public void savePaymentMode(String paymentModeJson, Merchant merchant) {
    	LOGGER.info("------------ start :: CloverServiceImpl : savePaymentMode :------------");

        List<PaymentMode> paymentModeList = setPaymentMode(paymentModeJson, merchant);
        if(paymentModeList!=null)
        	LOGGER.info("------------ End :: CloverServiceImpl : savePaymentMode : save paymentModeList------------");

        paymentModeRepository.save(paymentModeList);

    }

    /**
     * Save tax rate
     */
    public void saveTaxRate(String taxRates, Merchant merchant) {
    	LOGGER.info("------------ start :: CloverServiceImpl : savePaymentMode :------------");

        List<TaxRates> rates = setTaxRates(taxRates, merchant);
        if (rates != null)
        	LOGGER.info("------------ End :: CloverServiceImpl : savePaymentMode :------------");

            taxRateRepository.save(rates);
    }

    /**
     * Save opening closing hour
     */
    public void saveOpeningClosing(Merchant merchant, String openingHour) {
    	LOGGER.info("------------ start :: CloverServiceImpl : saveOpeningClosing :------------");
    	LOGGER.info(" CloverServiceImpl : saveOpeningClosing : saveOpeningClosing :"+openingHour);

        JSONObject jObject = new JSONObject(openingHour);
        if(jObject.has("elements")){
        JSONArray jSONArray = jObject.getJSONArray("elements");
        if (jSONArray != null) {
            for (Object jObj : jSONArray) {
                JSONObject openingClosing = (JSONObject) jObj;
                OpeningClosingDay sundayDay = getDay(openingClosing, "sunday", merchant);
            	LOGGER.info(" CloverServiceImpl : saveOpeningClosing : save OpeningClosingDay :");

                openingClosingDayRepository.save(sundayDay);

                List<OpeningClosingTime> sunday = getOpeingClosingTime("sunday", sundayDay, openingClosing);
            	LOGGER.info(" CloverServiceImpl : saveOpeningClosing : save OpeningClosingDay :");

                openingClosingTimeRepository.save(sunday);

                OpeningClosingDay mondayDay = getDay(openingClosing, "monday", merchant);
            	LOGGER.info(" CloverServiceImpl : saveOpeningClosing : save OpeningClosingDay :");

                openingClosingDayRepository.save(mondayDay);

                List<OpeningClosingTime> monday = getOpeingClosingTime("monday", mondayDay, openingClosing);
            	LOGGER.info(" CloverServiceImpl : saveOpeningClosing : save OpeningClosingDay :");

                openingClosingTimeRepository.save(monday);

                OpeningClosingDay tuesdayDay = getDay(openingClosing, "tuesday", merchant);
            	LOGGER.info(" CloverServiceImpl : saveOpeningClosing : save OpeningClosingDay :");

                openingClosingDayRepository.save(tuesdayDay);

                List<OpeningClosingTime> tuesday = getOpeingClosingTime("tuesday", tuesdayDay, openingClosing);
            	LOGGER.info(" CloverServiceImpl : saveOpeningClosing : save OpeningClosingDay :");

                openingClosingTimeRepository.save(tuesday);

                OpeningClosingDay wednesdayDay = getDay(openingClosing, "wednesday", merchant);
            	LOGGER.info(" CloverServiceImpl : saveOpeningClosing : save OpeningClosingDay :");

                openingClosingDayRepository.save(wednesdayDay);

                List<OpeningClosingTime> wednesday = getOpeingClosingTime("wednesday", wednesdayDay, openingClosing);
            	LOGGER.info(" CloverServiceImpl : saveOpeningClosing : save OpeningClosingDay :");

                openingClosingTimeRepository.save(wednesday);

                OpeningClosingDay thursdayDay = getDay(openingClosing, "thursday", merchant);
            	LOGGER.info(" CloverServiceImpl : saveOpeningClosing : save OpeningClosingDay :");

                openingClosingDayRepository.save(thursdayDay);

                List<OpeningClosingTime> thursday = getOpeingClosingTime("thursday", thursdayDay, openingClosing);
            	LOGGER.info(" CloverServiceImpl : saveOpeningClosing : save OpeningClosingDay :");

                openingClosingTimeRepository.save(thursday);

                OpeningClosingDay fridayDay = getDay(openingClosing, "friday", merchant);
            	LOGGER.info(" CloverServiceImpl : saveOpeningClosing : save OpeningClosingDay :");

                openingClosingDayRepository.save(fridayDay);

                List<OpeningClosingTime> friday = getOpeingClosingTime("friday", fridayDay, openingClosing);
            	LOGGER.info(" CloverServiceImpl : saveOpeningClosing : save OpeningClosingDay :");

                openingClosingTimeRepository.save(friday);

                OpeningClosingDay saturdayDay = getDay(openingClosing, "saturday", merchant);
            	LOGGER.info(" CloverServiceImpl : saveOpeningClosing : save OpeningClosingDay :");

                openingClosingDayRepository.save(saturdayDay);

                List<OpeningClosingTime> saturday = getOpeingClosingTime("saturday", saturdayDay, openingClosing);
            	LOGGER.info(" CloverServiceImpl : saveOpeningClosing : save OpeningClosingDay :");

                openingClosingTimeRepository.save(saturday);
            }
        }
        }
    }

    private Set<Item> getItemForCategory(JSONArray itemJsonArray, Merchant merchant, Clover clover) {
    	LOGGER.info("----------------Start :: CloverServiceImpl : getItemForCategory:--------------");

        Set<Item> itemSet = new HashSet<Item>();
    	LOGGER.info("CloverServiceImpl : getItemForCategory: itemJsonArray:"+itemJsonArray.length());

        for (int i = 0; i < itemJsonArray.length(); i++) {
            JSONObject itemJson = itemJsonArray.getJSONObject(i);
            String posItemId = itemJson.getString("id");

            Item item = null;
        	LOGGER.info("CloverServiceImpl : getItemForCategory: posItemId:"+posItemId+":merchantId:"+merchant.getId());

            item = itemRepository.findByPosItemIdAndMerchantId(posItemId, merchant.getId());
            if (item != null) {
                itemSet.add(item);
            } else {
                String itemResponse = CloverUrlUtil.getCloverItem(clover, merchant, posItemId);
                JSONObject jObject = new JSONObject(itemResponse);
                item = createNewLineItem(jObject, merchant);
                itemSet.add(item);
            }
        }
    	LOGGER.info(" -------------End :: CloverServiceImpl : getItemForCategory:------------------- ");

        return itemSet;
    }

    private List<Category> createCategoryObject(String responseString, Merchant merchant, Clover clover) {
    	LOGGER.info(" -------------Start :: CloverServiceImpl : createCategoryObject:------------------- ");

        List<Category> categories = new ArrayList<Category>();
        JSONObject jsonObject = new JSONObject(responseString);
        JSONArray jsonArray = jsonObject.getJSONArray("elements");
    	LOGGER.info("  CloverServiceImpl : createCategoryObject: ArrayLenght:"+jsonArray.length());

        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                JSONObject categoryItem = jsonArray.getJSONObject(i);
            	LOGGER.info("  CloverServiceImpl : createCategoryObject: merchantId:"+merchant.getId());

                Category category = categoryRepository.findByMerchantIdAndPosCategoryId(merchant.getId(),
                                categoryItem.getString("id"));

                if (category == null){
                    category = new Category();
                    category.setItemStatus(IConstant.BOOLEAN_FALSE);
                    category.setSortOrder(i+1);
                }
                   category.setMerchant(merchant);

                if (categoryItem.toString().contains("name"))
                    category.setName(categoryItem.getString("name"));

                if (categoryItem.toString().contains("id"))
                    category.setPosCategoryId(categoryItem.getString("id"));

                /*if (categoryItem.toString().contains("sortOrder"))
                    category.setSortOrder(categoryItem.getInt("sortOrder"));
                else
                	category.setSortOrder(0);*/
                
                

                if (categoryItem.toString().contains("items")) {
                    JSONObject itemJsonObject = categoryItem.getJSONObject("items");

                    JSONArray itemJsonArray = itemJsonObject.getJSONArray("elements");
                    Set<Item> items = getItemForCategory(itemJsonArray, merchant, clover);
                    if (!items.isEmpty() || items != null) {
                        category.setItems(items);
                    }
                }
                
                categoryRepository.save(category);
            	LOGGER.info("  CloverServiceImpl : createCategoryObject: save category:");

                categories.add(category);
            } catch (Exception e) {
                LOGGER.error("error: " + e.getMessage());
            	LOGGER.error(" End :: CloverServiceImpl : createCategoryObject: Exception:"+e);

            }
        }
    	LOGGER.info("------------- End :: CloverServiceImpl : createCategoryObject:--------------");

        return categories;
    }

    public void saveCategory(Clover clover, Merchant merchant) {
    	LOGGER.info("------------- Start :: CloverServiceImpl : saveCategory :--------------");

        String responseString = CloverUrlUtil.getAllCategories(clover);
        if (!responseString.isEmpty() && responseString != null) {
        	LOGGER.info("------------- End :: CloverServiceImpl : saveCategory :--------------");

            createCategoryObject(responseString, merchant, clover);
        }
    }

    public List<Category> getAllCategory(Integer merchantId) {
    	LOGGER.info("------------- Start :: CloverServiceImpl : getAllCategory :--------------");
    	LOGGER.info("CloverServiceImpl : getAllCategory :merchantId:"+merchantId);
    	LOGGER.info("------------- End :: CloverServiceImpl : getAllCategory :--------------");

        return categoryRepository.findByMerchantId(merchantId);
    }

    public Item getItemByItemID(String itemId, Merchant merchant) {
    	LOGGER.info("------------- Start :: CloverServiceImpl : getItemByItemID :--------------");
    	LOGGER.info("CloverServiceImpl : getItemByItemID :itemId:"+itemId+":merchantId:"+merchant.getId());
    	LOGGER.info("------------- End :: CloverServiceImpl : getItemByItemID :--------------");

        return itemRepository.findByPosItemIdAndMerchantId(itemId, merchant.getId());
    }

    public void saveItem(Clover clover, Merchant merchant) {
    	LOGGER.info("------------- Start :: CloverServiceImpl : saveItem :--------------");

        String responseString = CloverUrlUtil.getAllItem(clover, merchant);
    	LOGGER.info("------------- End :: CloverServiceImpl : saveItem :--------------");

        createItemObject(responseString, merchant);
    }

    public String createCustomer(Customer customer, Clover clover) {
    	LOGGER.info("------------- Start :: CloverServiceImpl : createCustomer :--------------");

        HttpPost postRequest = null;
        StringBuilder responseBuilder = new StringBuilder();
        try {
            HttpClient httpClient = HttpClientBuilder.create().build();
            String lastName = "";
            String emailPosId = "";
            String phonePosId = "";
        	LOGGER.info("CloverServiceImpl : createCustomer : LastName :"+ customer.getLastName());

            if (customer.getLastName() != null)
                lastName = customer.getLastName();
        	LOGGER.info("CloverServiceImpl : createCustomer : CustomerPosId :"+ customer.getCustomerPosId());

            if (customer.getCustomerPosId() != null) {
                postRequest = new HttpPost(clover.getUrl() + clover.getInstantUrl() + clover.getMerchantId()
                                + "/customers/" + customer.getCustomerPosId()
                                + "?expand=addresses,emailAddresses,phoneNumbers&access_token=" + clover.getAuthToken());
            	LOGGER.info("CloverServiceImpl : createCustomer : customerEmailPosId :"+customer.getEmailPosId()+":customerPhonePosId():"+customer.getPhonePosId());

                if (customer.getEmailPosId() != null && customer.getPhonePosId() != null) {
                    emailPosId = customer.getEmailPosId();
                    phonePosId = customer.getPhonePosId();
                }
            } else {
                postRequest = new HttpPost(clover.getUrl() + clover.getInstantUrl() + clover.getMerchantId()
                                + "/customers?expand=addresses,emailAddresses,phoneNumbers&access_token="
                                + clover.getAuthToken());
            }

            /*
             * List<AddressVO> addresses = new ArrayList<AddressVO>(); for(Address address:customer.getAddresses()){
             * AddressVO addressVO = new AddressVO(); addressVO.setAddress1(address.getAddress1());
             * addressVO.setAddress2(address.getAddress2()); addressVO.setAddress3(address.getAddress3());
             * addressVO.setZip(address.getZip()); addressVO.setState(address.getState());
             * addressVO.setCity(address.getCity()); addressVO.setCountry(address.getCountry()); addressVO.setId(""); }
             */
            String customerwholeAddress = "";
            String comma = "";
            if (customer.getAddresses() != null && customer.getAddresses().size() > 0) {
            	
                for (Address address : customer.getAddresses()) {
                    String addressPosId = "";
                	LOGGER.info("CloverServiceImpl : createCustomer : AddressPosId:"+address.getAddressPosId() );

                    if (address.getAddressPosId() != null) {
                        addressPosId = address.getAddressPosId();
                    }
                    if (address.getZip() != null && !address.getZip().isEmpty()) {
                        String customerAddress = "{		\"zip\": \"" + address.getZip()
                                        + "\",		\"country\": \"\",		\"address3\": \"\",		\"address2\": \""
                                        + address.getAddress2() + "\",		\"city\": \"" + address.getCity()
                                        + "\",		\"address1\": \"" + address.getAddress1() + "\",		\"id\": \""
                                        + addressPosId + "\",		\"state\": \"" + address.getState() + "\"	}";

                        customerwholeAddress = "\"addresses\": [" + comma + customerAddress + "],";
                        comma = ",";
                    }
                }

            }
            String customerJson = "{\"customerSince\": \"\",	\"firstName\": \"" + customer.getFirstName()
                            + "\",	\"lastName\": \"" + lastName + "\",	" + customerwholeAddress
                            + "	\"emailAddresses\": [{		\"emailAddress\": \"" + customer.getEmailId()
                            + "\",		\"id\": \"" + emailPosId
                            + "\",		\"verifiedTime\": \"\"	}],	\"phoneNumbers\": [{		\"phoneNumber\": \""
                            + customer.getPhoneNumber() + "\" ,		\"id\": \"" + phonePosId + "\"	}]}";
            StringEntity input = new StringEntity(customerJson);
            input.setContentType("application/json");
            postRequest.setEntity(input);
            HttpResponse response = httpClient.execute(postRequest);

            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            String line = "";
            while ((line = rd.readLine()) != null) {
                responseBuilder.append(line);
            }
            System.out.println(responseBuilder.toString());
        	LOGGER.info("CloverServiceImpl : createCustomer : Response :"+responseBuilder.toString());

            System.out.println("Customer created/updated on clover .... \n" + responseBuilder.toString());
        	LOGGER.info("CloverServiceImpl : createCustomer : Customer created/updated on clover .... \n" + responseBuilder.toString());

            return responseBuilder.toString();
        } catch (Exception e) {
        	LOGGER.error(" End :: CloverServiceImpl : createCustomer :Exception:" +e);

            return null;
        }

    }

    /**
     * Post order on clover
     */
    public String postOrderOnClover(CloverOrderVO cloverOrderVO, Clover clover) {
    	LOGGER.info("--------------Start :: CloverServiceImpl : postOrderOnClover :-------------");

        OrderVO orderVO = cloverOrderVO.getOrderVO();
        List<OrderItemVO> orderItemVOs = cloverOrderVO.getOrderItemVOs();
        Gson gson = new Gson();
        String orderJson = gson.toJson(orderVO);
        String orderResponse = CloverUrlUtil.postOrderOnClover(orderJson, clover);
        JSONObject jsonObject = new JSONObject(orderResponse);
       // JsonParser parser = new JsonParser();
        Double orderDiscount=0.0;
        String orderDiscountName ="";
        Double itemTotalDiscount=0.0;
        Double itemDiscount=0.0;
        String itemDiscountName= "";
        
        ArrayList<ItemDto> items= new ArrayList<ItemDto>();
        ArrayList<Map<String,Object>> allDiscounts= new ArrayList<Map<String,Object>>();
       
        String itemForDiscount = cloverOrderVO.getItemsForDiscount();
        String listOfALLDiscounts= cloverOrderVO.getListOfALLDiscounts();
        if(itemForDiscount != null &&!itemForDiscount.isEmpty() &&!itemForDiscount.equals("") && listOfALLDiscounts !=null  &&!listOfALLDiscounts.isEmpty() &&!listOfALLDiscounts.equals("")){
        	
        	JSONArray jsonitemForDiscountArray = new JSONArray(itemForDiscount);
        	JSONArray jsonlistOfALLDiscountsArray = new JSONArray(listOfALLDiscounts);
        	System.out.println(listOfALLDiscounts);
        	LOGGER.info(" CloverServiceImpl : postOrderOnClover :"+listOfALLDiscounts);
        	LOGGER.info(" CloverServiceImpl : postOrderOnClover : itemForDiscountArraylength : "+jsonitemForDiscountArray.length());

        	for(int i=0 ; i<jsonitemForDiscountArray.length() ; i++){
        		JSONObject object = jsonitemForDiscountArray.getJSONObject(i);
        		ItemDto itemDto = new ItemDto();
        		itemDto.setQunatity(object.getInt("qunatity"));
        		itemDto.setTotalQunatity(object.getInt("totalQunatity"));
        		itemDto.setDiscount(object.getDouble("discount"));
        		itemDto.setPrice(object.getDouble("price"));
        		itemDto.setOriginalPrice(object.getDouble("originalPrice"));
        		itemDto.setItemModifierPrice(object.getDouble("itemModifierPrice"));
        		itemDto.setItemPosId(object.getString("itemPosId"));
        		itemDto.setDiscountName(object.getString("discountName"));
        		items.add(itemDto);
        	}
        	
        	for(int i=0 ; i<jsonlistOfALLDiscountsArray.length(); i++){
        		JSONObject object = jsonlistOfALLDiscountsArray.getJSONObject(i);
        		 Map<String,Object> discountMap= new HashMap<String, Object>();
        		discountMap.put("voucherCode", object.getString("voucherCode"));
	    		discountMap.put("discount", object.getDouble("discount"));
	    		discountMap.put("inventoryLevel", object.getString("inventoryLevel"));
	    		discountMap.put("discountType", object.getString("discountType"));
	    		allDiscounts.add(discountMap);
        	}
        }
       
    	LOGGER.info(" CloverServiceImpl : postOrderOnClover : DiscountsSize():"+allDiscounts.size());

        if(allDiscounts.size() >0){
        	
        	
        	for(Map<String,Object> allDiscount : allDiscounts){
        		System.out.println("discount value"+ allDiscount.get("discount")); 
            	LOGGER.info(" CloverServiceImpl : postOrderOnClover :discount value"+ allDiscount.get("discount"));

        		Double discount = (Double)allDiscount.get("discount");	
        		String inventoryLevel= (String)allDiscount.get("inventoryLevel");
        		String discountName = (String)allDiscount.get("voucherCode");
        		if(inventoryLevel != null && discount != null && discountName !=null)
        		{
        			
        			if(inventoryLevel.equals("order") )
        			{
        				orderDiscount= orderDiscount + discount;
        				orderDiscountName = orderDiscountName +" "+ discountName;
        				
        			}else if(inventoryLevel.equals("item") || inventoryLevel.equals("category")){
        				itemTotalDiscount = itemTotalDiscount + discount;
        				
        			}
        		}
        	}
        	
        }
        
       
       
        //ArrayList itemForDiscountlist = parser.parse(ArrayList.class, itemForDiscount);
        
        if (jsonObject.toString().contains("id")) {
            String cloverOrderId = jsonObject.getString("id");
            System.out.println("cloverOrderId--> " + cloverOrderId);
        	LOGGER.info(" CloverServiceImpl : postOrderOnClover : cloverOrderId--> " + cloverOrderId);

            String updateOrderResponse = CloverUrlUtil.updatetOrderOnClover(orderJson, clover, cloverOrderId);
            System.out.println("Order Update response -" + updateOrderResponse);
        	LOGGER.info(" CloverServiceImpl : postOrderOnClover : Order Update response -" + updateOrderResponse);

            int k=0;
            for (OrderItemVO orderItemVO : orderItemVOs) {
                String orderItemJson = gson.toJson(orderItemVO);
                JSONObject orderItemJsonObject = new JSONObject(orderItemJson);
                Integer qty = Integer.parseInt(orderItemJsonObject.getString("unitQty"));
                String orderLineItemId="";
                ItemDto item=null;
            	LOGGER.info(" CloverServiceImpl : postOrderOnClover :itemTotalDiscount:" +itemTotalDiscount);

                if(itemTotalDiscount > 0){
            		//for(ItemDto  itemDto :items){
                	ItemDto  itemDto= items.get(k++);
            			if(itemDto!=null && itemDto.getItemPosId().equals(orderItemVO.getItem().getId())){
            				itemDiscount=itemDto.getDiscount();
            				itemDiscountName=itemDto.getDiscountName();
            				item=itemDto;
            				//break;
            			}
            		//}
                }
                for (int i = 1; i <= qty; i++) {
                    String OrderLineitemResponse = CloverUrlUtil.postOrderLineItemOnClover(orderItemJson, clover,
                                    cloverOrderId);

                    JSONObject OrderLineitemResponseJsonObject = new JSONObject(OrderLineitemResponse);
                    if(OrderLineitemResponseJsonObject.toString().contains("id")){
                    orderLineItemId = OrderLineitemResponseJsonObject.getString("id");
                    List<Modifications> modificationsList = orderItemVO.getModifications();
                    for (Modifications modifications : modificationsList) {
                        String modificationsJson = gson.toJson(modifications);
                        CloverUrlUtil.postModifiersWithLineItemOnClover(modificationsJson, clover, orderLineItemId,
                                        cloverOrderId);
                    }
                    System.out.println("orderLineItemId--> " + orderLineItemId);
                	LOGGER.info(" CloverServiceImpl : postOrderOnClover : orderLineItemId--> " + orderLineItemId);

                  //write discount code here
                   
                				
                				if(itemDiscount>0){
                					System.out.println("ITEMDISCOUNT VALUE IS::::"+itemDiscount);
                                	LOGGER.info(" CloverServiceImpl : postOrderOnClover : ITEMDISCOUNT VALUE IS::::"+itemDiscount);

                					double d=itemDiscount*100;
                					Long itemLongDiscount=Math.round(d);
                					double itemPrice=item.getOriginalPrice()+item.getItemModifierPrice();
                					if(item!=null && itemDiscount>=itemPrice){
                						itemDiscount=itemDiscount-itemPrice;
                					}else{
                						itemDiscount=0.0;
                					}
                					CloverDiscountVO cloverDiscountVO= new CloverDiscountVO();
                		        	
                		        	
                		        			
                		        			cloverDiscountVO.setAmount("-"+itemLongDiscount.toString());
                		        			cloverDiscountVO.setPercentage("");
                		        		
                		        	
                		        	
                		        	
                		        	cloverDiscountVO.setName("Discount:"+itemDiscountName);
                		        	
                		        	String itemDiscountJson="";
                		        	
                		        	Gson gson2 = new Gson();
                		        	itemDiscountJson=gson2.toJson(cloverDiscountVO);
                		        	String orderDiscountResponse=CloverUrlUtil.postItemDiscountOnClover(itemDiscountJson, clover, cloverOrderId,orderLineItemId);
                	            	 System.out.println("orderDiscountResponse-->"+orderDiscountResponse);
                                 	LOGGER.info(" CloverServiceImpl : postOrderOnClover :orderDiscountResponse-->"+orderDiscountResponse);

                		        	
                		        }
                				
                			
                    
                    
                    }
                }
                
            }
            
            if(orderDiscount>0){
             CloverDiscountVO cloverDiscountVO=new CloverDiscountVO();
            if(cloverDiscountVO!=null){
            	Long orderLongDiscount=(long) (orderDiscount*100);
            	cloverDiscountVO.setAmount("-"+orderLongDiscount.toString());
    			cloverDiscountVO.setPercentage("");
    		   	cloverDiscountVO.setName("Discount:"+orderDiscountName);
            	String orderDiscountJson = gson.toJson(cloverDiscountVO);
            	 String orderDiscountResponse=CloverUrlUtil.postOrderDiscountOnClover(orderDiscountJson, clover, cloverOrderId);
            	 System.out.println("orderDiscountResponse-->"+orderDiscountResponse);
              	LOGGER.info(" CloverServiceImpl : postOrderOnClover : orderDiscountResponse-->"+orderDiscountResponse);

            }}
        }else{
        	MailSendUtil.orderFaildMail("Merchant ->"+clover.getMerchantId()+" , Order API Response -> "+orderResponse,environment);
        }
      	LOGGER.info("-----------End :: CloverServiceImpl : postOrderOnClover :-----------------");

        return orderResponse;
    }

	public String getMerchantData(String orderId, String merchantId) {
      	LOGGER.info("-----------Start :: CloverServiceImpl : getMerchantData :-----------------");
      	LOGGER.info(" CloverServiceImpl : getMerchantData :orderId:"+orderId+":merchantId:"+merchantId);

		System.out.println("cloverServiceimpl");
    	Clover clover = new  Clover();
    	clover.setInstantUrl(IConstant.CLOVER_INSTANCE_URL);
        clover.setUrl(environment.getProperty("CLOVER_URL"));
        //clover.setUrl("http://localhost:8080");
    	Customer customer = new Customer();
        OrderR orderR = new OrderR();
        Merchant newMerchant = merchantRepository.findByPosMerchantId(merchantId);
        if(newMerchant!=null)
        {
        	clover.setAuthToken(newMerchant.getAccessToken());
        	clover.setMerchantId(newMerchant.getPosMerchantId());
        	String orderResponse = CloverUrlUtil.getFeedbackClover(clover,orderId);
        	
        	JSONObject jsonObj = new JSONObject(orderResponse);
        	
        	if(jsonObj.toString().contains("customers")){
        	JSONObject customersObject = jsonObj.getJSONObject("customers");
        	JSONArray customerObjArray = customersObject.getJSONArray("elements");
        	for(Object obj: customerObjArray)
        	{
        		System.out.println("cloverServiceimpl for loop");
        		JSONObject customerObj = (JSONObject) obj;
        		customer.setCustomerPosId(customerObj.getString("id"));
        		orderR.setCustomer(customer);
        		orderR.setOrderPosId(orderId);
        		//orderR.setCreatedOn();
        		
        		
        	}
        	 customer = customerrRepository.save(customer);
           	LOGGER.info(" CloverServiceImpl : getMerchantData : save cudtomer :");

        		orderR = ordeRepo.save(orderR);
        		System.out.println("custId-"+customer.getId()+" ordId-"+orderR.getId());
               	LOGGER.info(" CloverServiceImpl : getMerchantData :custId-"+customer.getId()+" ordId-"+orderR.getId());

        		String custId = EncryptionDecryptionUtil.encryption(String.valueOf(customer.getId()));
        		String orderid = EncryptionDecryptionUtil.encryption(String.valueOf(orderR.getId()));
        		System.out.println("cloverServiceimpl before redirect");
               	LOGGER.info("End :: CloverServiceImpl : getMerchantData : cloverServiceimpl before redirect:");

        		return "/feedbackForm?customerId="+custId+"&orderId="+orderid;
        		// return "redirect:"+environment.getProperty("WEB_BASE_URL")+"/feedbackForm?customerId="+custId+"&orderId="+orderid;
        		//List<CustomerFeedback> custFeedback = kritiqService.findByCustomerIdAndOrderId(customerId, orderid);
        	}
        }
       	LOGGER.info("End :: CloverServiceImpl : getMerchantData : Return null ");

        return null;
	}
	
    public void saveUpdatedOrderType(String orderTypeDetail, Merchant merchant, Clover clover) {
    	LOGGER.info(" start :: CloverServiceImpl : saveUpdatedOrderType :");

        List<OrderType> orderTypeList = setUpdatedOrderType(orderTypeDetail, merchant, clover);
    	LOGGER.info(" End :: CloverServiceImpl : saveUpdatedOrderType : save orderTypeList");

        orderTypeRepository.save(orderTypeList);
    }
}
