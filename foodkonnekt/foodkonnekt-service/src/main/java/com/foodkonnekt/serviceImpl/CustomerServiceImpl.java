package com.foodkonnekt.serviceImpl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.foodkonnekt.clover.vo.CustomerJsonVo;
import com.foodkonnekt.clover.vo.CustomerVo;
import com.foodkonnekt.model.Address;
import com.foodkonnekt.model.CardInfo;
import com.foodkonnekt.model.Clover;
import com.foodkonnekt.model.CouponRedeemedDto;
import com.foodkonnekt.model.Customer;
import com.foodkonnekt.model.CustomerFeedback;
import com.foodkonnekt.model.Item;
import com.foodkonnekt.model.Merchant;
import com.foodkonnekt.model.OrderItem;
import com.foodkonnekt.model.OrderR;
import com.foodkonnekt.model.Vendor;
import com.foodkonnekt.model.Zone;
import com.foodkonnekt.repository.AddressRepository;
import com.foodkonnekt.repository.CardInfoRepository;
import com.foodkonnekt.repository.CustomerFeedbackRepository;
import com.foodkonnekt.repository.CustomerrRepository;
import com.foodkonnekt.repository.ItemmRepository;
import com.foodkonnekt.repository.MerchantRepository;
import com.foodkonnekt.repository.OrderDiscountRepository;
import com.foodkonnekt.repository.OrderItemRepository;
import com.foodkonnekt.repository.OrderRepository;
import com.foodkonnekt.repository.VendorRepository;
import com.foodkonnekt.repository.ZoneRepository;
import com.foodkonnekt.service.CustomerService;
import com.foodkonnekt.util.CouponUrlUtil;
import com.foodkonnekt.util.DateUtil;
import com.foodkonnekt.util.EncryptionDecryptionUtil;
import com.foodkonnekt.util.IConstant;
import com.foodkonnekt.util.MailSendUtil;
import com.foodkonnekt.util.UrlConstant;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Service
@Transactional
public class CustomerServiceImpl implements CustomerService {
	
	private static final Logger LOGGER= LoggerFactory.getLogger(CustomerServiceImpl.class);

	@Autowired
    private Environment environment;
	
	@Autowired
	private CustomerrRepository customerrRepository;

	@Autowired
	private OrderDiscountRepository orderDiscountRepository;

	@Autowired
	private AddressRepository addressRepository;

	@Autowired
	private MerchantRepository merchantRepository;

	@Autowired
	private OrderRepository orderRepo;

	@Autowired
	private ZoneRepository zoneRepository;

	@Autowired
	VendorRepository vendorRepository;

	@Autowired
	private CardInfoRepository cardInfoRepository;

	@Autowired
	private OrderItemRepository orderItemRepository;

	@Autowired
	private ItemmRepository itemmRepository;
	
	@Autowired
	private CustomerFeedbackRepository customerFeedbackRepository;

	/**
	 * Find by email , password and vendorId
	 */
	public Customer findByEmailAndPasswordAndVendorId(String emailId, String password, Integer vendorId,
			Integer merchantId) {
		LOGGER.info("----------------Start :: CustomerServiceImpl : findByEmailAndPasswordAndVendorId :------------------------");
		LOGGER.info("CustomerServiceImpl : findByEmailAndPasswordAndVendorId :emailId :"+emailId+":password:"+password+":vendorId:"+vendorId+":merchantId:"+merchantId);
	
		List<Customer> customers = customerrRepository.findByEmailIdAndPasswordAndVendorId(emailId, password, vendorId);
		Customer resultcustomer = null;
		try {
			if (customers != null) {
				if (!customers.isEmpty()) {
					for (Customer customer : customers) {

						if (customer != null) {
							customer.setAddresses(null);
							if (customer.getMerchantt() != null &&  customer.getMerchantt() != null && customer.getMerchantt().getId() != null && merchantId!=null &&  ( customer.getMerchantt().getId() == merchantId ||customer.getMerchantt().getId().equals(merchantId)) ) {
								customer.getMerchantt().setItems(null);
								customer.getMerchantt().setMerchantSubscriptions(null);
								customer.getMerchantt().setModifierGroups(null);
								customer.getMerchantt().setModifiers(null);
								customer.getMerchantt().setOpeningClosingDays(null);
								customer.getMerchantt().setOrderRs(null);
								customer.getMerchantt().setOrderTypes(null);
								customer.getMerchantt().setPaymentModes(null);
								customer.getMerchantt().setTaxRates(null);
								customer.getMerchantt().setVouchars(null);
								customer.getMerchantt().setAddresses(null);
								customer.getMerchantt().setSubscription(null);
								customer.getMerchantt().setMerchantLogin(null);
								customer.getMerchantt().setSocialMediaLinks(null);
								resultcustomer = customer;
								break;
							}
						}

					}
					if (resultcustomer == null) {
						Customer customer = customers.get(0);
						resultcustomer = new Customer();
						resultcustomer.setId(null);
						if (customer.getPassword() != null) {
							resultcustomer.setPassword("@duplicatepassword#" + customer.getPassword());
						}
						if (resultcustomer != null) {
							Merchant merchant = merchantRepository.findOne(merchantId);
							resultcustomer.setMerchantt(merchant);
							resultcustomer.setFirstName(customer.getFirstName());
							resultcustomer.setLastName(customer.getLastName());
							resultcustomer.setAnniversaryDate(customer.getAnniversaryDate());
							resultcustomer.setBirthDate(customer.getBirthDate());
							resultcustomer.setCreatedDate(customer.getCreatedDate());
							resultcustomer.setCustomerType("java");
							resultcustomer.setAddresses(customer.getAddresses());
							resultcustomer.setEmailId(customer.getEmailId());
							resultcustomer.setPhoneNumber(customer.getPhoneNumber());

							if(resultcustomer.getVendor()==null){
								if (merchant.getOwner() !=null){
									resultcustomer.setVendor(merchant.getOwner());
								}
							}
							if(customer.getLastName() != null) {
								resultcustomer.setLastName(customer.getLastName());
							}
							if (merchant.getOwner() != null && merchant.getOwner().getPos() != null
									&& merchant.getOwner().getPos().getPosId() != null
									&& merchant.getOwner().getPos().getPosId() == 1) {
								Clover clover = new Clover();
								clover.setInstantUrl(IConstant.CLOVER_INSTANCE_URL);
								clover.setUrl(environment.getProperty("CLOVER_URL"));
								clover.setMerchantId(merchant.getPosMerchantId());
								clover.setAuthToken(merchant.getAccessToken());
								String customerResponse = new CloverServiceImpl().createCustomer(resultcustomer,
										clover);
								JSONObject jObject = new JSONObject(customerResponse);
								if (customerResponse.contains("id")) {
									resultcustomer.setCustomerPosId(jObject.getString("id"));

									JSONObject addressesJsonObject = jObject.getJSONObject("addresses");

									JSONArray addressesJsonArray = addressesJsonObject.getJSONArray("elements");
									int index = 0;
									if (addressesJsonArray != null) {
										for (Object jObj : addressesJsonArray) {
											JSONObject addressesJson = (JSONObject) jObj;
											if (addressesJson.toString().contains("id")) {
												System.out.println(addressesJson.getString("id"));
												LOGGER.info(" CustomerServiceImpl : findByEmailAndPasswordAndVendorId :"+addressesJson.getString("id"));

												Address address = resultcustomer.getAddresses().get(index++);
												address.setAddressPosId(addressesJson.getString("id"));
												// System.out.println(customer.getAddresses().get(0).getCity());
												// customer.getAddresses().add(address);
											}
										}
									}

									JSONObject emailJsonObject = jObject.getJSONObject("emailAddresses");

									JSONArray emailJsonArray = emailJsonObject.getJSONArray("elements");
									if (emailJsonArray != null) {
										for (Object jObj : emailJsonArray) {
											JSONObject emailJson = (JSONObject) jObj;
											if (emailJson.toString().contains("id")) {
												System.out.println(emailJson.getString("id"));
												LOGGER.info(" CustomerServiceImpl : findByEmailAndPasswordAndVendorId :"+emailJson.getString("id"));

												resultcustomer.setEmailPosId(emailJson.getString("id"));
											}
										}
									}

									JSONObject phoneJsonObject = jObject.getJSONObject("phoneNumbers");

									JSONArray phoneJsonArray = phoneJsonObject.getJSONArray("elements");
									if (phoneJsonArray != null) {
										for (Object jObj : phoneJsonArray) {
											JSONObject phoneJson = (JSONObject) jObj;
											if (phoneJson.toString().contains("id")) {
												System.out.println(phoneJson.getString("id"));
												LOGGER.info(" CustomerServiceImpl : findByEmailAndPasswordAndVendorId :"+phoneJson.getString("id"));

												resultcustomer.setPhonePosId(phoneJson.getString("id"));
											}
										}
									}
								}
							}
							String merchantLogo = null;
							if (merchant.getMerchantLogo() == null) {
								merchantLogo = "http://www.dev.foodkonnekt.com/app/foodnew.jpg";
							} else {
								merchantLogo = environment.getProperty("BASE_PORT") + merchant.getMerchantLogo();
							}
							resultcustomer = updateCustomerProfile(resultcustomer);

							resultcustomer.setAddresses(null);
							if (resultcustomer.getMerchantt() != null) {
								resultcustomer.getMerchantt().setItems(null);
								resultcustomer.getMerchantt().setMerchantSubscriptions(null);
								resultcustomer.getMerchantt().setModifierGroups(null);
								resultcustomer.getMerchantt().setModifiers(null);
								resultcustomer.getMerchantt().setOpeningClosingDays(null);
								resultcustomer.getMerchantt().setOrderRs(null);
								resultcustomer.getMerchantt().setOrderTypes(null);
								resultcustomer.getMerchantt().setPaymentModes(null);
								resultcustomer.getMerchantt().setTaxRates(null);
								resultcustomer.getMerchantt().setVouchars(null);
								resultcustomer.getMerchantt().setAddresses(null);
								resultcustomer.getMerchantt().setSubscription(null);
								resultcustomer.getMerchantt().setMerchantLogin(null);
								resultcustomer.getMerchantt().setSocialMediaLinks(null);
							}
						}
					}
				}
			}
		} catch (Exception e) {
			if (e != null) {
				LOGGER.error(" CustomerServiceImpl : findByEmailAndPasswordAndVendorId : Exception:"+e);

				MailSendUtil.sendExceptionByMail(e,environment);
			}
			LOGGER.error("error: " + e.getMessage());
			LOGGER.error(" CustomerServiceImpl : findByEmailAndPasswordAndVendorId : Exception:"+e);

		}
		LOGGER.info("------------End :: CustomerServiceImpl : findByEmailAndPasswordAndVendorId :---------------");

		return resultcustomer;
	}

	public Customer findByEmailAndPassword(String emailId, String password) {
		LOGGER.info("------------Start :: CustomerServiceImpl : findByEmailAndPassword :---------------");
		LOGGER.info("CustomerServiceImpl : findByEmailAndPassword : emailId:"+emailId+":password:"+password);

		List<Customer> customers = customerrRepository.findByEmailIdAndPassword(emailId, password);
		Customer customer = null;
		try {
			if (customers != null) {
				if (!customers.isEmpty()) {
					customer = customers.get(0);
					if (customer != null) {
						customer.setAddresses(null);
						if (customer.getMerchantt() != null) {
							customer.getMerchantt().setItems(null);
							customer.getMerchantt().setMerchantSubscriptions(null);
							customer.getMerchantt().setModifierGroups(null);
							customer.getMerchantt().setModifiers(null);
							customer.getMerchantt().setOpeningClosingDays(null);
							customer.getMerchantt().setOrderRs(null);
							customer.getMerchantt().setOrderTypes(null);
							customer.getMerchantt().setPaymentModes(null);
							customer.getMerchantt().setTaxRates(null);
							customer.getMerchantt().setVouchars(null);
							customer.getMerchantt().setAddresses(null);
							customer.getMerchantt().setSubscription(null);
						}
					}
				}
			}
		} catch (Exception e) {
			if (e != null) {
				LOGGER.error(" End :: CustomerServiceImpl : findByEmailAndPassword : Exception:"+e);

				MailSendUtil.sendExceptionByMail(e,environment);
			}
			LOGGER.error("error: " + e.getMessage());
			LOGGER.error(" End :: CustomerServiceImpl : findByEmailAndPassword :Exception:"+e);

		}
		LOGGER.info(" End :: CustomerServiceImpl : findByEmailAndPassword :");

		return customer;
	}

	public Customer findByEmailAndPasswordForAdmin(String emailId, String password) {
		LOGGER.info(" ---------------Start :: CustomerServiceImpl : findByEmailAndPasswordForAdmin :---------------");
		LOGGER.info("CustomerServiceImpl : findByEmailAndPasswordForAdmin :emailId:"+emailId+"password"+password);

		List<Customer> customers = customerrRepository.findByEmailIdAndPassword(emailId, password);
		Customer admin = null;
		try {
			if (customers != null) {
				if (!customers.isEmpty()) {
					for (Customer customer : customers) {
						LOGGER.info("CustomerServiceImpl : findByEmailAndPasswordForAdmin :CustomerType:"+customer.getCustomerType());

						if (customer != null && customer.getCustomerType() != null
								&& (customer.getCustomerType().equals("admin")
										|| customer.getCustomerType().equals("location"))) {
							customer.setAddresses(null);
							if (customer.getMerchantt() != null) {
								customer.getMerchantt().setItems(null);
								customer.getMerchantt().setMerchantSubscriptions(null);
								customer.getMerchantt().setModifierGroups(null);
								customer.getMerchantt().setModifiers(null);
								customer.getMerchantt().setOpeningClosingDays(null);
								customer.getMerchantt().setOrderRs(null);
								customer.getMerchantt().setOrderTypes(null);
								customer.getMerchantt().setPaymentModes(null);
								customer.getMerchantt().setTaxRates(null);
								customer.getMerchantt().setVouchars(null);
								customer.getMerchantt().setAddresses(null);
								customer.getMerchantt().setSubscription(null);
							}
							admin = customer;
						}
					}
				}
			}
		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
				LOGGER.error("CustomerServiceImpl : findByEmailAndPasswordForAdmin :Exception:"+e);

			}
			LOGGER.error("error: " + e.getMessage());
			LOGGER.error("CustomerServiceImpl : findByEmailAndPasswordForAdmin :Exception:"+e);

		}
		LOGGER.info(" ------------End :: CustomerServiceImpl : findByEmailAndPasswordForAdmin :");

		return admin;
	}

	/**
	 * find by customer id
	 */
	public List<Address> getAddressByCustomerId(Integer customerId) {
		LOGGER.info(" ------------Start :: CustomerServiceImpl : getAddressByCustomerId :");
		LOGGER.info("CustomerServiceImpl : getAddressByCustomerId :customerId:"+customerId);

		List<Address> addresses = addressRepository.findByCustomerId(customerId);
		for (Address address : addresses) {
			address.setMerchant(null);
		}
		LOGGER.info(" ------------End :: CustomerServiceImpl : getAddressByCustomerId :");

		return addresses;
	}

	/**
	 * Find customer and customer address by customerId
	 */
	public Customer getCustomerProfile(Integer customerId) {
		LOGGER.info("----------------Start :: CustomerServiceImpl : getCustomerProfile----------------");
		LOGGER.info(" CustomerServiceImpl : getCustomerProfile : customerId:"+customerId);

		Customer customer = customerrRepository.findOne(customerId);
		try {
			if (customer != null) {
				List<Address> finalAddresses = new ArrayList<Address>();
				List<Address> addresses = addressRepository.findByCustomerId(customerId);
				for (Address address : addresses) {
					Address address2 = new Address();
					address2.setId(address.getId());
					address2.setAddress1(address.getAddress1());
					address2.setAddress2(address.getAddress2());
					address2.setAddress3(address.getAddress3());
					address2.setCity(address.getCity());
					address2.setState(address.getState());
					address2.setZip(address.getZip());
					address2.setAddressPosId(address.getAddressPosId());
					address2.setCountry(address.getCountry());
					LOGGER.info(" CustomerServiceImpl : getCustomerProfile : merchantId:"+customer.getMerchantt().getId());

					List<Zone> zones = zoneRepository.findByMerchantId(customer.getMerchantt().getId());
					double distance;
					final double MILES_PER_KILOMETER = 0.621;
					for (Zone zone : zones) {
						distance = checkDelivery(address, zone.getAddress());
						double miles = distance * MILES_PER_KILOMETER;
					
						if(zone.getZoneDistance()!=null) {
						if (miles <= zone.getZoneDistance()) {
							address2.setDeliveryFee(zone.getDeliveryFee());
							address2.setDeliveryPosId(zone.getDeliveryLineItemPosId());
						}
						}
					}
					finalAddresses.add(address2);
				}
				customer.setAddresses(finalAddresses);
				customer.getMerchantt().setItems(null);
				customer.getMerchantt().setMerchantSubscriptions(null);
				customer.getMerchantt().setModifierGroups(null);
				customer.getMerchantt().setModifiers(null);
				customer.getMerchantt().setOpeningClosingDays(null);
				customer.getMerchantt().setOrderRs(null);
				customer.getMerchantt().setOrderTypes(null);
				customer.getMerchantt().setPaymentModes(null);
				customer.getMerchantt().setTaxRates(null);
				customer.getMerchantt().setVouchars(null);
				customer.getMerchantt().setAddresses(null);
				customer.getMerchantt().setSubscription(null);
				customer.getMerchantt().setMerchantLogin(null);
				customer.getMerchantt().setSocialMediaLinks(null);
				;
			}
		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
				LOGGER.error(" CustomerServiceImpl : getCustomerProfile : Exception:"+e);

			}
			LOGGER.error("error: " + e.getMessage());
			LOGGER.error(" CustomerServiceImpl : getCustomerProfile : Exception:"+e);

		}
		LOGGER.info("---------------End :: CustomerServiceImpl : getCustomerProfile :--------------");

		return customer;
	}

	public Customer updateCustomerProfile(Customer customer) {
		LOGGER.info("---------------Start :: CustomerServiceImpl : updateCustomerProfile :--------------");

		try {
			String date=null;
			
			if(customer!=null && customer.getMerchantt()!=null && customer.getMerchantt().getId()!=null ) {
				Merchant merchant = merchantRepository.findById(customer.getMerchantt().getId());
				String timeZone = (merchant.getTimeZone()!=null) ? merchant.getTimeZone().getTimeZoneCode() : "America/Chicago";
				date=DateUtil.findCurrentDateWithTimeforTimeZone(timeZone);
			}else {
				date=DateUtil.findCurrentDateWithTime();
			}
			
			if (customer.getId() == null) {
				customer.setCreatedDate(date);
				customer.setUpdatedDate(date);
			}
			List<Address> customerAddress = customer.getAddresses();
			int merchantId = customer.getMerchantt().getId();
			customer.setAddresses(null);
			if (customer.getId() != null) {
				LOGGER.info(" CustomerServiceImpl : updateCustomerProfile :customerId:"+customer.getId());

				Customer existingCustomer = customerrRepository.findOne(customer.getId());
				String existingCustomerOldPassword = existingCustomer.getPassword();
				String existingCustomerUpdatedPassword = customer.getPassword();
				customer.setCreatedDate(existingCustomer.getCreatedDate());
				if (!existingCustomerOldPassword.equals(existingCustomerUpdatedPassword)) {
					customer.setPassword(EncryptionDecryptionUtil.encryptString(existingCustomerUpdatedPassword));
				}
				if (customer.getCheckId() != null && customer.getCheckId() == 1) {
					customer = existingCustomer;
				} else {
					if (customer.getImage() != null) {
						if (customer.getImage().isEmpty()) {
							customer.setImage(existingCustomer.getImage());
						}
					}
					
					if(customer.getLoyalityValue()!=null){
						if(customer.getLoyalityValue() ==true){
							customer.setLoyalityProgram(1);
						}else if(customer.getLoyalityValue() ==false){
							customer.setLoyalityProgram(0);
						}
					}
					customer.setUpdatedDate(date);
					customer = customerrRepository.save(customer);
					customer = customerrRepository.findOne(customer.getId());
				}
			} else {
				Merchant merchant = merchantRepository.findOne(merchantId);
				customer.setMerchantt(merchant);
				if (customer.getPassword() != null && !customer.getPassword().isEmpty()) {
					String pwsd[] = customer.getPassword().split("#");

					if (pwsd != null && pwsd.length > 1 && pwsd[0].equals("@duplicatepassword")) {

						customer.setPassword(pwsd[1]);
					} else {
						customer.setPassword(EncryptionDecryptionUtil.encryptString(customer.getPassword()));
					}
				}

				customer = customerrRepository.save(customer);
				LOGGER.info(" CustomerServiceImpl : updateCustomerProfile :saved customer:");

				/*
				 * String customerEntityDTOJson =
				 * ProducerUtil.getCustomerEntityDTO(customer); String
				 * resultCustomerMkonnektPlatform =
				 * ProducerUtil.createCustomerMkonnektPlatform(
				 * customerEntityDTOJson);
				 * if(!resultCustomerMkonnektPlatform.equals("") &&
				 * resultCustomerMkonnektPlatform!= null ){
				 * 
				 * JSONObject jObject = new
				 * JSONObject(resultCustomerMkonnektPlatform);
				 * if(jObject.getJSONObject("customer")!= null){ JSONObject
				 * jObjectCustomer = jObject.getJSONObject("customer");
				 * if(jObjectCustomer.getString("customerUid")!= null){ String
				 * customerUid = jObjectCustomer.getString("customerUid");
				 * System.out.println("customerUid in CustomerServiceImpl::" +
				 * customerUid); customer.setCustomerUid(customerUid); } }
				 * 
				 * } customer = customerrRepository.save(customer);
				 */
			}
			/*
			 * if (customerAddress.get(0).getAddress2() != null &&
			 * !customerAddress.get(0).getAddress2().isEmpty()) {
			 * setAddress(customerAddress, merchantId, customer); }
			 */
			// changed by sumit
			if ((customerAddress != null && customerAddress.size() > 0)
					&& (customerAddress.get(0).getZip() != null && !customerAddress.get(0).getZip().isEmpty())) {
				setAddress(customerAddress, merchantId, customer);
			}
			if (customer != null) {
				customer.setMerchantt(merchantRepository.findOne(merchantId));
				customer = customerrRepository.save(customer);
				List<Address> addresses = addressRepository.findByCustomerId(customer.getId());
				if (addresses != null && !addresses.isEmpty()) {
					for (Address address : addresses) {
						address.setZones(null);
					}
					customer.setAddresses(addresses);
				}
			}
			if (customer.getMerchantt() != null) {
				customer.getMerchantt().setItems(null);
				customer.getMerchantt().setMerchantSubscriptions(null);
				customer.getMerchantt().setModifierGroups(null);
				customer.getMerchantt().setModifiers(null);
				customer.getMerchantt().setOpeningClosingDays(null);
				customer.getMerchantt().setOrderRs(null);
				customer.getMerchantt().setOrderTypes(null);
				customer.getMerchantt().setPaymentModes(null);
				customer.getMerchantt().setTaxRates(null);
				customer.getMerchantt().setVouchars(null);
				customer.getMerchantt().setAddresses(null);
				customer.getMerchantt().setSubscription(null);
				customer.getMerchantt().setMerchantLogin(null);
				customer.getMerchantt().setSocialMediaLinks(null);
			}
		} catch (Exception e) {
			if (e != null) {
				LOGGER.error(" CustomerServiceImpl : updateCustomerProfile : Exception:"+e);

				MailSendUtil.sendExceptionByMail(e,environment);
			}
			LOGGER.error("error: " + e.getMessage());
			LOGGER.error(" CustomerServiceImpl : updateCustomerProfile : Exception:"+e);

		}
		LOGGER.info("-----------------End :: CustomerServiceImpl : updateCustomerProfile :-------------");
		return customer;

	}

	private List<Address> setAddress(List<Address> customerAddress, Integer merchantId, Customer customer) {
		LOGGER.info("-----------------start :: CustomerServiceImpl : setAddress :-------------");
		LOGGER.info(" CustomerServiceImpl : setAddress :merchantId:"+merchantId);

		try {
			for (Address address : customerAddress) {
				Address address2 = null;
				if (address.getId() != null) {
					address2 = addressRepository.findOne(address.getId());
				} else {
					Merchant merchant = merchantRepository.findOwnerId(merchantId);
					customer.setMerchantt(merchant);
					address2 = new Address();
					address2.setMerchant(merchant);
					address2.setCustomer(customer);
				}
				address2.setAddress1(address.getAddress1());
				address2.setAddress2(address.getAddress2());
				address2.setAddress3(address.getAddress3());
				address2.setCity(address.getCity());
				address2.setState(address.getState());
				address2.setZip(address.getZip());
				address2.setAddressPosId(address.getAddressPosId());
				address2.setCountry(address.getCountry());
				addressRepository.save(address2);
				LOGGER.info(" CustomerServiceImpl : setAddress :save Address:");
			}
		} catch (Exception e) {
			if (e != null) {
				LOGGER.error("End :: CustomerServiceImpl : setAddress : Exception:"+e);

				MailSendUtil.sendExceptionByMail(e,environment);
			}
			LOGGER.error("error: " + e.getMessage());
			LOGGER.error(" End :: CustomerServiceImpl : setAddress : Exception:"+e);

		}
		LOGGER.info("--------------------- End :: CustomerServiceImpl : setAddress :----------------");

		return customerAddress;
	}

	/**
	 * Find by email Id
	 */
	public boolean findAdminByEmail(String emailId) {
		
		LOGGER.info("----------------Start :: CustomerServiceImpl : findAdminByEmail------------------------");
		LOGGER.info(" CustomerServiceImpl : findAdminByEmail: emailId:"+emailId);

		List<Customer> customers = customerrRepository.findByEmailId(emailId);
		Customer admin = null;
		if (customers != null) {
			if (!customers.isEmpty()) {
				int status = 0;
				for (Customer cust : customers) {
					if (cust.getPassword() != null && cust.getCustomerType() != null
							&& (cust.getCustomerType().equals("admin") || cust.getCustomerType().equals("location"))) {
						if (!cust.getPassword().isEmpty()) {
							status++;
							admin = cust;
						}
					}
				}
				LOGGER.info(" CustomerServiceImpl : findAdminByEmail: status:"+status);

				if (status > 0) {
					if (admin != null)
						MailSendUtil.forgotAdminPasswordEmail(admin,environment);
					LOGGER.info("----------------End :: CustomerServiceImpl : findAdminByEmail------------------------");
					return true;
				} else {
					LOGGER.info("----------------End :: CustomerServiceImpl : findAdminByEmail------------------------");
					return false;
				}
			} else {
				LOGGER.info("----------------End :: CustomerServiceImpl : findAdminByEmail------------------------");
				return false;
			}
		} else {
			LOGGER.info("----------------End :: CustomerServiceImpl : findAdminByEmail------------------------");
			return false;
		}
	}

	public boolean findByEmail(String emailId) {
		LOGGER.info("----------------Start :: CustomerServiceImpl : findByEmail : ------------------------");
		LOGGER.info(" CustomerServiceImpl : findByEmail : emailId:"+emailId);

		List<Customer> customer = customerrRepository.findByEmailId(emailId);
		if (customer != null) {
			if (!customer.isEmpty()) {
				int status = 0;
				for (Customer cust : customer) {
					if (cust.getPassword() != null) {
						if (!cust.getPassword().isEmpty()) {
							status++;
						}
					}
				}
				LOGGER.info(" CustomerServiceImpl : findByEmail : status:"+status);

				if (status > 0) {
					MailSendUtil.forgotPasswordEmail(customer.get(0),environment);
					LOGGER.info("--------------End :: CustomerServiceImpl : findByEmail : return true:-----------------");

					return true;
				} else {
					LOGGER.info("--------------End :: CustomerServiceImpl : findByEmail : return false:-----------------");

					return false;
				}
			} else {
				LOGGER.info("--------------End :: CustomerServiceImpl : findByEmail : return false:-----------------");

				return false;
			}
		} else {
			LOGGER.info("--------------End :: CustomerServiceImpl : findByEmail : return false:-----------------");

			return false;
		}
	}

	public boolean findByEmailIdAndMerchantId(String emailId, Integer merchantId) {
		LOGGER.info("--------------Start :: CustomerServiceImpl : findByEmailIdAndMerchantId : -----------------");
		LOGGER.info(" CustomerServiceImpl : findByEmailIdAndMerchantId : emailId:"+emailId+":merchantId:"+merchantId);

		List<Customer> customer = customerrRepository.findByEmailIdAndMerchantId(emailId, merchantId);
		if (customer != null) {
			if (!customer.isEmpty()) {
				int status = 0;
				for (Customer cust : customer) {
					if (cust.getPassword() != null) {
						if (!cust.getPassword().isEmpty()) {
							status++;
						}
					}
				}
				LOGGER.info(" CustomerServiceImpl : findByEmailIdAndMerchantId : status:"+status);

				if (status > 0) {
					LOGGER.info("--------------End :: CustomerServiceImpl : findByEmailIdAndMerchantId :return true -----------------");

					return true;
				} else {
					LOGGER.info("--------------End :: CustomerServiceImpl : findByEmailIdAndMerchantId :return false -----------------");

					return false;
				}
			} else {
				LOGGER.info("--------------End :: CustomerServiceImpl : findByEmailIdAndMerchantId :return false -----------------");

				return false;
			}
		} else {
			LOGGER.info("--------------End :: CustomerServiceImpl : findByEmailIdAndMerchantId :return false -----------------");

			return false;
		}
	}

	public boolean findByEmailIdAndVendorId(String emailId, Integer vendorId) {
		LOGGER.info("--------------Start :: CustomerServiceImpl : findByEmailIdAndVendorId :  -----------------");
		LOGGER.info(" CustomerServiceImpl : findByEmailIdAndVendorId :emailId: "+emailId+":vendorId:"+vendorId);

		List<Customer> customer = customerrRepository.findByEmailIdAndVendorId(emailId, vendorId);
		if (customer != null) {
			if (!customer.isEmpty()) {
				int status = 0;
				for (Customer cust : customer) {
					if (cust.getPassword() != null) {
						if (!cust.getPassword().isEmpty()) {
							status++;
						}
					}
				}
				LOGGER.info(" CustomerServiceImpl : findByEmailIdAndVendorId :status: "+status);

				if (status > 0) {
					LOGGER.info("--------------End :: CustomerServiceImpl : findByEmailIdAndVendorId : return true: -----------------");

					return true;
				} else {
					LOGGER.info("--------------End :: CustomerServiceImpl : findByEmailIdAndVendorId : return false: -----------------");

					return false;
				}
			} else {
				LOGGER.info("--------------End :: CustomerServiceImpl : findByEmailIdAndVendorId : return false: -----------------");

				return false;
			}
		} else {
			LOGGER.info("--------------End :: CustomerServiceImpl : findByEmailIdAndVendorId : return false: -----------------");

			return false;
		}
	}

	/**
	 * Find by emailId
	 */
	public boolean findByEmailId(String emailId) {
		LOGGER.info("--------------start :: CustomerServiceImpl : findByEmailId :: -----------------");
		LOGGER.info("CustomerServiceImpl : findByEmailId :emailId:"+emailId);

		List<Customer> customer = customerrRepository.findByEmailId(emailId);
		if (customer != null) {
			if (!customer.isEmpty()) {
				int status = 0;
				for (Customer cust : customer) {
					if (cust.getPassword() != null) {
						if (!cust.getPassword().isEmpty()) {
							status++;
						}
					}
				}
				LOGGER.info("CustomerServiceImpl : findByEmailId :status:"+status);

				if (status > 0) {
					LOGGER.info("--------------End :: CustomerServiceImpl : findByEmailId ::return true: -----------------");

					return true;
				} else {
					LOGGER.info("--------------End :: CustomerServiceImpl : findByEmailId ::return false: -----------------");

					return false;
				}
			} else {
				LOGGER.info("--------------End :: CustomerServiceImpl : findByEmailId ::return false: -----------------");

				return false;
			}
		} else {
			LOGGER.info("--------------End :: CustomerServiceImpl : findByEmailId ::return false: -----------------");

			return false;
		}
	}

	/**
	 * Find by vendorId
	 */
	public List<Customer> findByVendorId(Integer merchantId) {
		LOGGER.info("--------------Start :: CustomerServiceImpl : findByVendorId : -----------------");
		LOGGER.info(" CustomerServiceImpl : findByVendorId :merchantId:"+merchantId);

		List<Customer> customers = customerrRepository.findByMerchantId(merchantId);
		for (Customer customer : customers) {
			LOGGER.info(" CustomerServiceImpl : findByVendorId :customerId:"+customer.getId());

			List<OrderR> orderRs = orderRepo.findByCustomerId(customer.getId());
			if (orderRs != null) {
				if (!orderRs.isEmpty()) {
					customer.setOrderCount(orderRs.size());
				}
			}
		}
		LOGGER.info("--------------End :: CustomerServiceImpl : findByVendorId : -----------------");

		return customers;
	}

	public double checkDelivery(Address address, Address merchantAddress) {
		LOGGER.info("--------------Start :: CustomerServiceImpl : checkDelivery : -----------------");

		String add2 = null;
		if (address.getAddress2() != null) {
			add2 = address.getAddress2();
		} else {
			add2 = "";
		}
		String merchantAdd2 = null;
		if (merchantAddress.getAddress2() != null) {
			merchantAdd2 = merchantAddress.getAddress2();
		} else {
			merchantAdd2 = "";
		}
		String origin = address.getAddress1() + "," + add2 + "," + address.getCity() + "," + address.getState() + ","
				+ address.getZip();
		String des = merchantAddress.getAddress1() + "," + merchantAdd2 + "," + merchantAddress.getCity() + ","
				+ merchantAddress.getState() + "," + merchantAddress.getZip();
		HttpGet request;
		double distance = 0;
		try {
			request = new HttpGet("https://maps.googleapis.com/maps/api/distancematrix/json?origins="
					+ URLEncoder.encode(origin, "UTF-8") + "&destinations=" + URLEncoder.encode(des, "UTF-8"));
			String response = convertToStringJson(request);
			JSONObject jObject = new JSONObject(response);
			String distan = null;
			if (jObject.getString("status").equals("OK")) {
				JSONArray rows = jObject.getJSONArray("rows");
				for (Object jObj : rows) {
					JSONObject jsonObj = (JSONObject) jObj;
					JSONArray elements = jsonObj.getJSONArray("elements");
					for (Object jObj2 : elements) {
						JSONObject obj = (JSONObject) jObj2;
						if (!obj.isNull("distance")) {
							JSONObject distanceObj = obj.getJSONObject("distance");
							distan = distanceObj.getString("text");
							String[] arry = distan.split(" ");
							distance = Double.parseDouble(arry[0].replace(",", ""));
						}
					}
				}
			}
		} catch (UnsupportedEncodingException e) {
			if (e != null) {
				LOGGER.error("End :: CustomerServiceImpl : checkDelivery :UnsupportedEncodingException:"+e);

				MailSendUtil.sendExceptionByMail(e,environment);
			}
		} catch (Exception e) {
			if (e != null) {
				LOGGER.error("End :: CustomerServiceImpl : checkDelivery : Exception :"+e);

				MailSendUtil.sendExceptionByMail(e,environment);
			}
		}
		LOGGER.error("------------------End :: CustomerServiceImpl : checkDelivery ----------------------- ");

		return distance;
	}

	public String convertToStringJson(HttpGet request) {
		LOGGER.info("------------------Start :: CustomerServiceImpl : convertToStringJson ----------------------- ");

		HttpClient client = HttpClientBuilder.create().build();
		HttpResponse response = null;
		StringBuilder responseBuilder = new StringBuilder();
		try {
			response = client.execute(request);
			BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			String line = "";
			while ((line = rd.readLine()) != null) {
				responseBuilder.append(line);
			}
		} catch (IOException e) {
			if (e != null) {
				LOGGER.info("End :: CustomerServiceImpl : convertToStringJson :Exception:"+e);

				MailSendUtil.sendExceptionByMail(e,environment);
			}
		}
		LOGGER.info("------------------End :: CustomerServiceImpl : convertToStringJson ----------------------- ");

		return responseBuilder.toString();
	}

	/**
	 * Set guest customer password
	 */

	public Customer setGuestCustomerPassword(Customer sessionCustomer) {
		LOGGER.info("----------------Start :: CustomerServiceImpl : setGuestCustomerPassword------------------------");
		LOGGER.info("CustomerServiceImpl :: setGuestCustomerPassword : customerId "+sessionCustomer.getId());
		Customer customer = customerrRepository.findOne(sessionCustomer.getId());
		customer.setPassword(EncryptionDecryptionUtil.encryptString(sessionCustomer.getPassword()));
		sessionCustomer.setPassword(EncryptionDecryptionUtil.encryptString(sessionCustomer.getPassword()));
		LOGGER.info("CustomerServiceImpl :: setGuestCustomerPassword : customerType:"+customer.getCustomerType());

		if (customer.getCustomerType() != null && customer.getCustomerType().equals("admin")) {
			customer.setCustomerType("admin");
		} else if (customer.getCustomerType() != null && customer.getCustomerType().equals("location")) {
			customer.setCustomerType("location");
		} else {
			customer.setCustomerType("java");
		}
		customerrRepository.save(customer);
		LOGGER.info("CustomerServiceImpl :: setGuestCustomerPassword : save customer ");

		LOGGER.info("----------------End :: CustomerServiceImpl : setGuestCustomerPassword------------------------");
		return sessionCustomer;
	}

	/**
	 * Find by emailId and merchantId
	 */
	public boolean findByEmailAndMerchantIdAndVendorId(String emailId, Integer merchantId,Integer vendorId){
		LOGGER.info("--------------Start :: CustomerServiceImpl : findByEmailAndMerchantIdAndVendorId : -----------------");
		LOGGER.info(" CustomerServiceImpl : findByEmailAndMerchantIdAndVendorId :emailId:"+emailId+":merchantId:"+merchantId+":vendorId:"+vendorId);

		List<Customer> customer = customerrRepository.findByEmailIdAndMerchantId(emailId, merchantId);
		if (customer != null) {
			if (!customer.isEmpty()) {
				int status = 0;
				for (Customer cust : customer) {
					if (cust.getPassword() != null) {
						if (!cust.getPassword().isEmpty()) {
							status++;
						}
					}
				}
				LOGGER.info(" CustomerServiceImpl : findByEmailAndMerchantIdAndVendorId :status:"+status);

				if (status > 0) {
					MailSendUtil.forgotPasswordEmailForMultiLocationCustomer(customer.get(0),vendorId,environment);
					LOGGER.info("--------------End :: CustomerServiceImpl : findByEmailAndMerchantIdAndVendorId : return true:-----------------");

					return true;
				} else {
					LOGGER.info("--------------End :: CustomerServiceImpl : findByEmailAndMerchantIdAndVendorId : return false:-----------------");

					return false;
				}
			} else {
				LOGGER.info("--------------End :: CustomerServiceImpl : findByEmailAndMerchantIdAndVendorId : return false:-----------------");

				return false;
			}
		} else {
			LOGGER.info("--------------End :: CustomerServiceImpl : findByEmailAndMerchantIdAndVendorId : return false:-----------------");

			return false;
		}
	
	}
	public boolean findByEmailAndMerchantId(String emailId, Integer merchantId) {
		LOGGER.info("--------------Start :: CustomerServiceImpl : findByEmailAndMerchantId :-----------------");
		LOGGER.info(" CustomerServiceImpl : findByEmailAndMerchantId : emailId:"+emailId+":merchantId:"+merchantId);

		List<Customer> customer = customerrRepository.findByEmailIdAndMerchantId(emailId, merchantId);
		if (customer != null) {
			if (!customer.isEmpty()) {
				int status = 0;
				for (Customer cust : customer) {
					if (cust.getPassword() != null) {
						if (!cust.getPassword().isEmpty()) {
							status++;
						}
					}
				}
				LOGGER.info(" CustomerServiceImpl : findByEmailAndMerchantId : status:"+status);

				if (status > 0) {
					MailSendUtil.forgotPasswordEmail(customer.get(0),environment);
					LOGGER.info("--------------End :: CustomerServiceImpl : findByEmailAndMerchantId : return true-----------------");

					return true;
				} else {
					LOGGER.info("--------------End :: CustomerServiceImpl : findByEmailAndMerchantId :return false-----------------");

					return false;
				}
			} else {
				LOGGER.info("--------------End :: CustomerServiceImpl : findByEmailAndMerchantId :return false-----------------");

				return false;
			}
		} else {
			LOGGER.info("--------------End :: CustomerServiceImpl : findByEmailAndMerchantId :return false-----------------");

			return false;
		}
	}

	public Customer findByEmailAndCustomerId(String emailId, Integer id) {
		LOGGER.info("--------------Start :: CustomerServiceImpl : findByEmailAndCustomerId :-----------------");
		LOGGER.info(": CustomerServiceImpl : findByEmailAndCustomerId : emailId:"+emailId+":customerId:"+id);

		Customer customer = customerrRepository.findOne(id);
		if (customer != null) {
			LOGGER.info("--------------End :: CustomerServiceImpl : findByEmailAndCustomerId :-----------------");

			return customer;
		} else {
			LOGGER.info("--------------End :: CustomerServiceImpl : findByEmailAndCustomerId :-----------------");

			return null;
		}
	}

	public Customer findCustomerByEmailAndMerchantId(String emailId, Integer merchantId) {
		LOGGER.info("--------------Start :: CustomerServiceImpl : findCustomerByEmailAndMerchantId :-----------------");
		LOGGER.info(" CustomerServiceImpl : findCustomerByEmailAndMerchantId :emailId:"+emailId+":merchantId:"+merchantId);

		List<Customer> customer = customerrRepository.findByEmailIdAndMerchantId(emailId, merchantId);
		if (customer != null) {
			if (!customer.isEmpty()) {
				LOGGER.info("--------------End :: CustomerServiceImpl : findCustomerByEmailAndMerchantId :-----------------");

				return customer.get(0);
			} else {
				LOGGER.info("--------------End :: CustomerServiceImpl : findCustomerByEmailAndMerchantId : return null-----------------");

				return null;
			}
		} else {
			LOGGER.info("--------------End :: CustomerServiceImpl : findCustomerByEmailAndMerchantId : return null-----------------");

			return null;
		}
	}

	public Customer findCustomerByEmailAndMerchantIdForAdmin(String emailId, Integer merchantId, String userId) {
		LOGGER.info("----------------Start :: CustomerServiceImpl : findCustomerByEmailAndMerchantIdForAdmin------------------------");
		List<Customer> customers = customerrRepository.findByEmailIdAndMerchantId(emailId, merchantId);
		Customer admin = null;
		
		LOGGER.info("CustomerServiceImpl :: findCustomerByEmailAndMerchantIdForAdmin : merchantId "+merchantId+":emailId:"+emailId+":userId:"+userId);
		if (customers != null && !customers.isEmpty()) {
			for (Customer customer : customers) {
				LOGGER.info("CustomerServiceImpl :: findCustomerByEmailAndMerchantIdForAdmin : customerType "+customer.getCustomerType());

				if (customer != null && customer.getCustomerType() != null
						&& (customer.getCustomerType().equals("admin") || customer.getCustomerType().equals("location"))
						&& userId.equalsIgnoreCase(EncryptionDecryptionUtil.encryptString(customer.getId() + ""))) {
					customer.setAddresses(null);
					if (customer.getMerchantt() != null) {
						customer.getMerchantt().setItems(null);
						customer.getMerchantt().setMerchantSubscriptions(null);
						customer.getMerchantt().setModifierGroups(null);
						customer.getMerchantt().setModifiers(null);
						customer.getMerchantt().setOpeningClosingDays(null);
						customer.getMerchantt().setOrderRs(null);
						customer.getMerchantt().setOrderTypes(null);
						customer.getMerchantt().setPaymentModes(null);
						customer.getMerchantt().setTaxRates(null);
						customer.getMerchantt().setVouchars(null);
						customer.getMerchantt().setAddresses(null);
						customer.getMerchantt().setSubscription(null);
					}
					admin = customer;
				}
			}

		}
		LOGGER.info("----------------End :: CustomerServiceImpl : findCustomerByEmailAndMerchantIdForAdmin------------------------");
		return admin;
	}

	public List<Customer> findByEmailIDAndMerchantId(String emailId, Integer merchantId) {
		LOGGER.info("----------------Start :: CustomerServiceImpl : findByEmailIDAndMerchantId------------------------");
		LOGGER.info(" CustomerServiceImpl : findByEmailIDAndMerchantId : emailId:"+emailId+":merchantId:"+merchantId);
		LOGGER.info("----------------End :: CustomerServiceImpl : findByEmailIDAndMerchantId------------------------");

		return customerrRepository.findByEmailIdAndMerchantId(emailId, merchantId);
	}

	public String findCustomerInventory(Integer merchantId, Integer pageDisplayLength, Integer pageNumber,
			String searchParameter) {
		LOGGER.info("----------------Start :: CustomerServiceImpl : findCustomerInventory------------------------");
		LOGGER.info(" CustomerServiceImpl : findByEmailIDAndMerchantId : pageDisplayLength:"+pageDisplayLength+":merchantId:"+merchantId+":pageNumber:"+pageNumber+":searchParameter:"+searchParameter);

		Pageable pageable = new PageRequest(pageNumber - 1, pageDisplayLength, Sort.Direction.DESC, "id");
		Page<Customer> customers = customerrRepository.findByMerchantId(merchantId, pageable);
		List<CustomerVo> result = new ArrayList<CustomerVo>();
		for (Customer customer : customers.getContent()) {
			if (customer.getCustomerType() != null && customer.getCustomerType().equals("clover")) {
				continue;
			}
			CustomerVo customerVo = new CustomerVo();
			customerVo.setId(customer.getId());
			customerVo.setFirstName(customer.getFirstName());
			customerVo.setEmailId(customer.getEmailId());
			customerVo.setPhoneNumber(customer.getPhoneNumber());
			if (customer.getCreatedDate() != null) {
				customerVo.setCreatedDate(customer.getCreatedDate());
			} else {
				customerVo.setCreatedDate("");
			}
			if(customer!=null && customer.getLastName() == null){
				customerVo.setLastName((customer.getLastName()));
			}
			LOGGER.info(" CustomerServiceImpl : findCustomerInventory: customerId :"+customer.getId());

			List<OrderR> orderRs = orderRepo.findByCustomerId(customer.getId());
			if (orderRs != null) {
				if (!orderRs.isEmpty()) {
					customerVo.setOrderCount(orderRs.size());
					customerVo.setView("<a href=customerOrders?customerId=" + customer.getId()
							+ " class='edit' style='color: blue'>view</a>");
				} else {
					customerVo.setOrderCount(orderRs.size());
					customerVo.setView("");
				}
			}
			result.add(customerVo);
		}
		result = getCustomerListBasedOnSearchParameter(searchParameter, result);
		CustomerJsonVo customerJsonVo = new CustomerJsonVo();
		customerJsonVo.setiTotalDisplayRecords((int) customers.getTotalElements());
		customerJsonVo.setiTotalRecords((int) customers.getTotalElements());
		customerJsonVo.setAaData(result);
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		LOGGER.info("----------------------End :: CustomerServiceImpl : findCustomerInventory:-----------------------------");

		return gson.toJson(customerJsonVo);
	}

	private List<CustomerVo> getCustomerListBasedOnSearchParameter(String searchParameter, List<CustomerVo> result) {
		LOGGER.info("----------------------Start :: CustomerServiceImpl : getCustomerListBasedOnSearchParameter:-----------------------------");
		LOGGER.info(" CustomerServiceImpl : getCustomerListBasedOnSearchParameter:searchParameter:"+searchParameter);

		if (null != searchParameter && !searchParameter.equals("")) {
			List<CustomerVo> personsListForSearch = new ArrayList<CustomerVo>();
			searchParameter = searchParameter.toUpperCase();
			for (CustomerVo customerVo : result) {
				if (customerVo.getFirstName().toUpperCase().indexOf(searchParameter) != -1
						|| customerVo.getEmailId().toString().indexOf(searchParameter) != -1
						|| customerVo.getPhoneNumber().toString().indexOf(searchParameter) != -1) {
					personsListForSearch.add(customerVo);
				}
			}
			result = personsListForSearch;
			personsListForSearch = null;
		}
		LOGGER.info("----------------------End :: CustomerServiceImpl : getCustomerListBasedOnSearchParameter:-----------------------------");

		return result;
	}

	public String searchCustomerByTxt(Integer merchantId, String searchTxt) {
		LOGGER.info("----------------------Start :: CustomerServiceImpl : searchCustomerByTxt:-----------------------------");
		LOGGER.info(" CustomerServiceImpl : searchCustomerByTxt:merchantId :"+merchantId+":searchTxt:"+searchTxt);

		List<Customer> customers = customerrRepository.findByFirstNameLikeIgnoreCaseAndMerchantId(searchTxt,
				merchantId);
		List<CustomerVo> result = new ArrayList<CustomerVo>();
		for (Customer customer : customers) {
			CustomerVo customerVo = new CustomerVo();
			customerVo.setId(customer.getId());
			customerVo.setFirstName(customer.getFirstName());
			customerVo.setEmailId(customer.getEmailId());
			customerVo.setPhoneNumber(customer.getPhoneNumber());
			if (customer.getCreatedDate() != null) {
				customerVo.setCreatedDate(customer.getCreatedDate());
			} else {
				customerVo.setCreatedDate("");
			}
			LOGGER.info(" CustomerServiceImpl : searchCustomerByTxt: customerId :"+customer.getId());

			List<OrderR> orderRs = orderRepo.findByCustomerId(customer.getId());
			if (orderRs != null) {
				if (!orderRs.isEmpty()) {
					customerVo.setOrderCount(orderRs.size());
					customerVo.setView("<a href=customerOrders?customerId=" + customer.getId()
							+ " class='edit' style='color: blue'>view</a>");
				} else {
					customerVo.setOrderCount(orderRs.size());
					customerVo.setView("");
				}
			}
			result.add(customerVo);
		}
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		LOGGER.info("----------------------End :: CustomerServiceImpl : searchCustomerByTxt:-----------------------------");

		return gson.toJson(result);
	}

	public List<Address> findAllCustomerAddress(Integer customerId, Integer merchantId) {
		LOGGER.info("----------------------Start :: CustomerServiceImpl : findAllCustomerAddress:-----------------------------");
		LOGGER.info(" CustomerServiceImpl : findAllCustomerAddress: customerId:"+customerId+":merchantId:"+merchantId);

		List<Address> addresses = addressRepository.findByCustomerId(customerId);
		List<Address> list = new ArrayList<Address>();
		for (Address address : addresses) {
			Address address2 = new Address();
			address2.setId(address.getId());
			address2.setAddress1(address.getAddress1());
			address2.setAddress2(address.getAddress2());
			address2.setAddress3(address.getAddress3());
			address2.setCity(address.getCity());
			address2.setCountry(address.getCountry());
			address2.setState(address.getState());
			address2.setZip(address.getZip());
			address2.setAddressPosId(address.getAddressPosId());
			list.add(address2);
		}
		LOGGER.info("----------------------End :: CustomerServiceImpl : findAllCustomerAddress:-----------------------------");

		return list;
	}

	/**
	 * Find by customerId
	 */
	public Customer findByCustomerId(Integer customerId) {
		LOGGER.info("----------------Start :: CustomerServiceImpl : findByCustomerId------------------------");
		LOGGER.info("CustomerServiceImpl : findByCustomerId : customerId "+customerId);
		LOGGER.info("----------------End :: CustomerServiceImpl : findByCustomerId------------------------");
		return customerrRepository.findOne(customerId);
	}

	public List<Map<String, Object>> checkDuplicatCouponAndRecalculate(Customer customer,
			List<Map<String, Object>> listOfALLDiscounts) {
		LOGGER.info("----------------------Start :: CustomerServiceImpl : checkDuplicatCouponAndRecalculate:-----------------------------");

		List<Map<String, Object>> orderDiscountMapList = new ArrayList<Map<String, Object>>();
		String responseKoupon = null;
		Merchant merchant =null;
		Integer vendorId = null;
		Vendor vendor = null;
		LOGGER.info(": CustomerServiceImpl : checkDuplicatCouponAndRecalculate: MerchantId:"+customer.getMerchantId());

		if(customer != null  && customer.getMerchantId() != null){
			merchant = merchantRepository.findById(customer.getMerchantId());
			LOGGER.info(": CustomerServiceImpl : checkDuplicatCouponAndRecalculate: MerchantUid :"+merchant.getMerchantUid());

			if(merchant != null && merchant.getMerchantUid() != null){
				customer.setMerchantUId(merchant.getMerchantUid());
			}
		}

		if(customer.getMerchantId() != null){
			vendorId = merchantRepository.findOwnerByMerchantId(customer.getMerchantId());
			if(vendorId != null){
				vendor = vendorRepository.findOne(vendorId);
				if(vendor != null && vendor.getVendorUid() != null){
					customer.setVendorUId(vendor.getVendorUid());
				}
			}
		}
		
		if (listOfALLDiscounts != null) {
			for (Map<String, Object> discount : listOfALLDiscounts) {
				String couponUID = (String) discount.get("couponUID");
				boolean isUseabilitySingle = (Boolean) discount.get("isUsabilitySingle");
				// boolean canApplyCoupon=true;
				boolean alreadyUsed = false;
				if (isUseabilitySingle && customer != null && customer.getId() != null && couponUID != null) {
					/*
					 * OrderDiscount orderDiscount = new OrderDiscount();
					 * orderDiscount.setCouponCode(couponUID);
					 */
					CouponRedeemedDto couponRedeemed = new CouponRedeemedDto();
					couponRedeemed.setKouponCode(couponUID);
					couponRedeemed.setCustomerContactNo(customer.getPhoneNumber());
					if(merchant != null && merchant.getMerchantUid() != null){
						couponRedeemed.setMerchantUId(merchant.getMerchantUid());
					}
					if(vendor != null && vendor.getVendorUid() != null){
						couponRedeemed.setVendorUId(vendor.getVendorUid());
					}
					String url = environment.getProperty("KOUPONS_BASE_URL") + "getCustomerDuplicateKoupon";
					Gson gson = new Gson();
					String appliedDate = null;
					String couponRedeemedJson = gson.toJson(couponRedeemed);
					System.out.println(couponRedeemedJson);
					LOGGER.info(": CustomerServiceImpl : checkDuplicatCouponAndRecalculate: couponRedeemedJson:"+couponRedeemedJson);

					responseKoupon = CouponUrlUtil.getCouponData(url, couponRedeemedJson);
					System.out.println("@$%@#%@#$% " + responseKoupon);
					LOGGER.info(": CustomerServiceImpl : checkDuplicatCouponAndRecalculate:@$%@#%@#$% " + responseKoupon);

					JSONObject jsonObj = new JSONObject(responseKoupon);
					String responseCode = jsonObj.getString("response");
					// JSONObject responseDataObj=
					// jsonObj.getJSONObject("DATA");
					JSONArray responseDataArray = jsonObj.getJSONArray("DATA");
					System.out.println("responseDataArray-" + responseDataArray);
					LOGGER.info(": CustomerServiceImpl : checkDuplicatCouponAndRecalculate:responseDataArray-" + responseDataArray);

					for (int i = 0; i < responseDataArray.length(); i++) {
						JSONObject kouponItemObject = responseDataArray.getJSONObject(i);

						if (kouponItemObject.has("alreadyUsed"))
							alreadyUsed = kouponItemObject.getBoolean("alreadyUsed");

						if (kouponItemObject.has("appliedDate"))
							appliedDate = (String) kouponItemObject.get("appliedDate");

						if (kouponItemObject.has("isUsabilitySingle"))
							isUseabilitySingle = kouponItemObject.getBoolean("isUsabilitySingle");

						Map<String, Object> orderDiscountMap = new HashMap<String, Object>();
						orderDiscountMap.put("couponCode", couponUID);
						orderDiscountMap.put("alreadyUsed", alreadyUsed);
						orderDiscountMap.put("appliedDate", appliedDate);
						orderDiscountMapList.add(orderDiscountMap);
					}

					/*
					 * if(responseCode.equals(IConstant.
					 * RESPONSE_SUCCESS_MESSAGE) && responseDataArray!=null &&
					 * responseDataArray.toString().contains("isUsabilitySingle"
					 * )){ if(!responseDataArray.get("kouponCode").equals(null)
					 * && !responseDataArray.get("kouponCode").equals("")){
					 * //couponUID =
					 * (String)responseDataArray.get("kouponCode"); appliedDate
					 * = (String)responseDataObj.get("appliedDate");
					 * SimpleDateFormat sdf = new
					 * SimpleDateFormat("YYYY-MM-DD"); Date d = new
					 * Date(appliedDate); String s =sdf.format(d);
					 * System.out.println(s); Map<String,Object>
					 * orderDiscountMap = new HashMap<String, Object>();
					 * orderDiscountMap.put("couponCode", couponUID);
					 * orderDiscountMap.put("appliedDate",appliedDate);
					 * orderDiscountMapList.add(orderDiscountMap); } }
					 */

					// List<OrderDiscount> orderDiscounts=
					// orderDiscountRepository.findByCustomerIdAndCouponCode(customer.getId(),couponUID);
					/*
					 * if(orderDiscounts!=null && !orderDiscounts.isEmpty() &&
					 * orderDiscounts.size()>0){ Map<String,Object>
					 * orderDiscountMap = new HashMap<String, Object>();
					 * orderDiscountMap.put("couponCode",
					 * orderDiscounts.get(0).getCouponCode());
					 * orderDiscountMap.put("appliedDate",
					 * orderDiscounts.get(0).getCouponDate());
					 * orderDiscountMapList.add(orderDiscountMap); }
					 */

				}
			}
		}
		LOGGER.info("--------------------End :: CustomerServiceImpl : checkDuplicatCouponAndRecalculate:-------------------------");

		return orderDiscountMapList;
	}

	public Customer findByEmailAndPasswordAndMerchantId(String emailId, String password, Integer merchantId) {
		LOGGER.info("--------------------Start :: CustomerServiceImpl : findByEmailAndPasswordAndMerchantId :-------------------------");
		LOGGER.info(" CustomerServiceImpl : findByEmailAndPasswordAndMerchantId :emailId:"+emailId+":password:"+password+":merchantId:"+merchantId);

		List<Customer> customers = customerrRepository.findByEmailIdAndPasswordAndMerchanttId(emailId, password,
				merchantId);
		Customer customer = null;
		try {
			if (customers != null) {
				if (!customers.isEmpty()) {
					customer = customers.get(0);
					if (customer != null) {
						customer.setAddresses(null);
						if (customer.getMerchantt() != null) {
							customer.getMerchantt().setItems(null);
							customer.getMerchantt().setMerchantSubscriptions(null);
							customer.getMerchantt().setModifierGroups(null);
							customer.getMerchantt().setModifiers(null);
							customer.getMerchantt().setOpeningClosingDays(null);
							customer.getMerchantt().setOrderRs(null);
							customer.getMerchantt().setOrderTypes(null);
							customer.getMerchantt().setPaymentModes(null);
							customer.getMerchantt().setTaxRates(null);
							customer.getMerchantt().setVouchars(null);
							customer.getMerchantt().setAddresses(null);
							customer.getMerchantt().setSubscription(null);
							customer.getMerchantt().setMerchantLogin(null);
							customer.getMerchantt().setSocialMediaLinks(null);
						}
					}
				}
			}
		} catch (Exception e) {
			if (e != null) {
				LOGGER.error(" End :: CustomerServiceImpl : findByEmailAndPasswordAndMerchantId :Exception :"+e);

				MailSendUtil.sendExceptionByMail(e,environment);
			}
			LOGGER.error("error: " + e.getMessage());
			LOGGER.error(" End :: CustomerServiceImpl : findByEmailAndPasswordAndMerchantId :Exception :"+e);

		}
		LOGGER.info("----------------End :: CustomerServiceImpl : findByEmailAndPasswordAndMerchantId------------------------");

		return customer;
	}

	public List<Customer> findByEmailIDAndVendorId(String emailId, Integer vendorId) {
		LOGGER.info("----------------Start :: CustomerServiceImpl : findByEmailIDAndVendorId------------------------");
		LOGGER.info("----------------End :: CustomerServiceImpl : findByEmailIDAndVendorId------------------------");
		return customerrRepository.findByEmailIdAndVendorId(emailId, vendorId);
	}

	public List<CardInfo> getCustomerCardInfo(Integer customerId) {
		LOGGER.info("----------------Start :: CustomerServiceImpl : getCustomerCardInfo:------------------------");
		LOGGER.info("CustomerServiceImpl : getCustomerCardInfo: customerId:"+customerId);

		List<CardInfo> cardsInfos = new ArrayList<CardInfo>();
		if (customerId != null) {
			cardsInfos = cardInfoRepository.findByCustomerIdAndStatus(customerId,true);
			/*
			 * for(CardInfo cardInfo : cardsInfo){ //cardInfo.setCustomer(null);
			 * cardsInfos.add(cardInfo); }
			 */
		} else {
			cardsInfos = null;
		}
		LOGGER.info("----------------End :: CustomerServiceImpl : getCustomerCardInfo:------------------------");

		return cardsInfos;
	}
	
	public List<CardInfo> getCustomerCardInfo(Integer customerId,Integer merchantId) {
		LOGGER.info("----------------Start :: CustomerServiceImpl : getCustomerCardInfo :------------------------");
		LOGGER.info(" CustomerServiceImpl : getCustomerCardInfo : customerId:"+customerId+":merchantId:"+merchantId);

		List<CardInfo> cardsInfos = new ArrayList<CardInfo>();
		if (customerId != null) {
			cardsInfos = cardInfoRepository.findByCustomerIdAndMerchantIdAndStatus(customerId,merchantId,true);
			/*
			 * for(CardInfo cardInfo : cardsInfo){ //cardInfo.setCustomer(null);
			 * cardsInfos.add(cardInfo); }
			 */
		} else {
			cardsInfos = null;
		}
		LOGGER.info("----------------End :: CustomerServiceImpl : getCustomerCardInfo :------------------------");

		return cardsInfos;
	}

	public List<Customer> findByMerchantId(Integer merchantId) {
		LOGGER.info("----------------Start :: CustomerServiceImpl : findByMerchantId :------------------------");
		LOGGER.info(" CustomerServiceImpl : findByMerchantId : merchantId:"+merchantId);
		LOGGER.info("----------------End :: CustomerServiceImpl : findByMerchantId :------------------------");

		return customerrRepository.findByMerchantId(merchantId);
	}

	/**
	 * Get Customers by merchantUId
	 * 
	 * @param merchantUId
	 * @return List<Customer>
	 */
	public List<Customer> findByMerchantUId(String merchantUId) {
		LOGGER.info("----------------Start :: CustomerServiceImpl : findByMerchantUId :------------------------");
		LOGGER.info(" CustomerServiceImpl : findByMerchantUId : merchantUId:"+merchantUId);
		LOGGER.info("----------------End :: CustomerServiceImpl : findByMerchantUId :------------------------");

		return customerrRepository.findByMerchantMerchantUid(merchantUId);
	}

	public List<Customer> findByMerchantIdAndDateRangeAndSpent(Integer merchantId, Date startDate, Date endDate,
			Double orderPrice) {
		LOGGER.info("----------------Start :: CustomerServiceImpl : findByMerchantIdAndDateRangeAndSpent :------------------------");
		LOGGER.info(":CustomerServiceImpl : findByMerchantIdAndDateRangeAndSpent : merchantId:"+merchantId+":startDate:"+startDate+":endDate:"+endDate+":orderPrice:"+orderPrice);

		List<OrderR> orderList = orderRepo.findByMerchantIdDateRangeAndSpent(endDate, startDate, merchantId,
				orderPrice);
		List<Customer> customerList = new ArrayList<Customer>();
		for (OrderR orderR : orderList) {
			Customer customer = orderR.getCustomer();
			List<Address> addressList = customer.getAddresses();
			for (Address address : addressList) {
				address.setMerchant(null);
				address.setCustomer(null);
				List<Zone> zoneList = address.getZones();
				for (Zone zone : zoneList) {
					zone.setAddress(null);
					zone.setMerchant(null);
				}
				address.setZones(zoneList);
			}
			customer.setAddresses(addressList);
			customer.setMerchantt(null);
			customerList.add(customer);
			System.out.println(orderR.getCustomer().getFirstName() + " price " + orderR.getOrderPrice());
			LOGGER.info(" CustomerServiceImpl : findByMerchantIdAndDateRangeAndSpent :"+orderR.getCustomer().getFirstName() + " price " + orderR.getOrderPrice());

		}
		LOGGER.info("----------------End :: CustomerServiceImpl : findByMerchantIdAndDateRangeAndSpent :------------------------");

		return customerList;
	}

	public Customer searchCustomerByUUId(String searchCustomerByUUId) {
		// TODO Auto-generated method stu
		LOGGER.info("----------------Start :: CustomerServiceImpl : searchCustomerByUUId :------------------------");

		Customer customer = new Customer();
		LOGGER.info(" CustomerServiceImpl : searchCustomerByUUId :searchCustomerByUUId:"+searchCustomerByUUId);

		if (searchCustomerByUUId != null) {
			customer = customerrRepository.findByCustomerUid(searchCustomerByUUId);
			System.out.println(customer);
			LOGGER.info(" CustomerServiceImpl : searchCustomerByUUId :customer:"+customer);


		} else {
			customer = null;
		}
		LOGGER.info("----------------End :: CustomerServiceImpl : searchCustomerByUUId :------------------------");

		return customer;
	}

	public List<OrderItem> findItemByOrderId(Integer orderId) {
		// TODO Auto-generated method stub
		LOGGER.info("----------------Start :: CustomerServiceImpl : findItemByOrderId :------------------------");

		List<OrderItem> orderItems = new ArrayList<OrderItem>();
		LOGGER.info(" CustomerServiceImpl : findItemByOrderId :orderId :"+orderId);

		if (orderId == 0) {
			orderItems = null;
		} else {
			orderItems = orderItemRepository.findByOrderId(orderId);
		}
		LOGGER.info("----------------End :: CustomerServiceImpl : findItemByOrderId :------------------------");

		return orderItems;
	}

	public Item findItemByItemId(Integer itemId) {
		LOGGER.info("----------------Start :: CustomerServiceImpl : findItemByItemId :------------------------");

		Item item = new Item();
		LOGGER.info(" CustomerServiceImpl : findItemByItemId :itemId:"+itemId);

		if (itemId != 0) {

			item = itemmRepository.findOne(itemId);
		} else {

			item = null;

		}
		LOGGER.info("----------------End :: CustomerServiceImpl : findItemByItemId :------------------------");

		return item;
	}

	
	//findByCustomerIdAndMerchantIdWithOrderGraterThan
	public List<Customer> findByPhoneNumber(String phonenumber) {
		LOGGER.info("----------------Start :: CustomerServiceImpl : findByPhoneNumber :------------------------");

		 List<Customer>  customers = null;
		try 
		{
			LOGGER.info("CustomerServiceImpl : findByPhoneNumber :phonenumber:"+phonenumber);

			if(phonenumber!= null) 
			{
				customers=	customerrRepository.findByPhoneNumber(phonenumber);		
				
				for (Customer customer : customers) 
				{				
					customer.setAddresses(null);
					customer.setAddress1(null);
					customer.setAddress2(null);
					//customer.setAnniversaryDate(null);
					customer.setImage(null);
					customer.setCheckId(null);
					//customer.setBirthDate(null);
					customer.setCountry(null);
					customer.setCustomerType(null);
					customer.setEmailPosId(null);
					customer.setLastName(null);
					customer.setOrderCount(null);
					customer.setOrderType(null);
					customer.setListOfALLDiscounts(null);
					customer.setVendor(null);
					customer.setPassword(null);			
					if(customer.getMerchantt()!=null)
					{
						customer.getMerchantt().setItems(null);
						customer.getMerchantt().setMerchantSubscriptions(null);
						customer.getMerchantt().setModifierGroups(null);
						customer.getMerchantt().setModifiers(null);
						customer.getMerchantt().setOpeningClosingDays(null);
						customer.getMerchantt().setOrderRs(null);
						customer.getMerchantt().setOrderTypes(null);
						customer.getMerchantt().setPaymentModes(null);
						customer.getMerchantt().setTaxRates(null);
						customer.getMerchantt().setVouchars(null);
						customer.getMerchantt().setAddresses(null);
						customer.getMerchantt().setSubscription(null);
						customer.getMerchantt().setMerchantLogin(null);
						customer.getMerchantt().setSocialMediaLinks(null);
						List<OrderR> oderList=orderRepo.findByCustomerIdAndMerchantIdWithOrderGraterThan(customer.getMerchantt().getId(), customer.getId());
					
						int rewardPoint=0;
						if(oderList!=null && !oderList.isEmpty())
						{
							rewardPoint=5*oderList.size();
						}
						
						customer.setRewardPoint(rewardPoint);
						
					}
					
			}
			}
		}
		catch(Exception exception){
			System.out.println(exception);
			LOGGER.error("End :: CustomerServiceImpl : findByPhoneNumber :Exception :"+exception);

			
		}
		LOGGER.info("----------------End :: CustomerServiceImpl : findByPhoneNumber :------------------------");

		return customers;
	}

	public List<Customer> findByDateAndCustomerId(String startDate, String endDate) {
		LOGGER.info("----------------Start :: CustomerServiceImpl : findByDateAndCustomerId :------------------------");
		LOGGER.info(" CustomerServiceImpl : findByDateAndCustomerId :startDate:"+startDate+":endDate:"+endDate);

		List<Customer> customers = new ArrayList<Customer>();
		customers = customerrRepository.findByCreatedDate(startDate, endDate);
		LOGGER.info("----------------End :: CustomerServiceImpl : findByDateAndCustomerId :------------------------");

		return customers;
	}
	
	public List<CustomerFeedback> getAllFeedbackOfCustomer(String customerUid) {
		LOGGER.info("----------------Start :: CustomerServiceImpl : getAllFeedbackOfCustomer :------------------------");
		LOGGER.info(" CustomerServiceImpl : getAllFeedbackOfCustomer :customerUid:"+customerUid);

		List<CustomerFeedback> customerFeedbacks = null;
		if (customerUid != null && !customerUid.isEmpty()) {
			Customer customer = customerrRepository
					.findByCustomerUid(customerUid);
			LOGGER.info(" CustomerServiceImpl : getAllFeedbackOfCustomer :customerId:"+customer.getId());

			if (customer != null && customer.getId() != null) {
				customerFeedbacks = customerFeedbackRepository
						.findByCustomerId(customer.getId());
			}
			if (customerFeedbacks != null && !customerFeedbacks.isEmpty()) {
				for (CustomerFeedback customerFeedback : customerFeedbacks) {
					customerFeedback.setCustomer(null);
					customerFeedback.setMerchant(null);
					customerFeedback.setOrderR(null);
					customerFeedback.setCustomerFeedbackAnswers(null);
				}
			}
		}
		LOGGER.info("----------------End :: CustomerServiceImpl : getAllFeedbackOfCustomer :------------------------");

		return customerFeedbacks;
	}
	
	/*-------------------------------------------------------------------------------*/
	//ServiceImpl for design V2
		
		public boolean findByEmailAndMerchantIdAndVendorIdV2(String emailId, Integer merchantId,Integer vendorId){
			LOGGER.info("----------------Start :: CustomerServiceImpl : findByEmailAndMerchantIdAndVendorIdV2 :------------------------");
			LOGGER.info(" CustomerServiceImpl : findByEmailAndMerchantIdAndVendorIdV2 :emailId:"+emailId+":merchantId:"+merchantId+":vendorId:"+vendorId);

			List<Customer> customer = customerrRepository.findByEmailIdAndMerchantId(emailId, merchantId);
			if (customer != null) {
				if (!customer.isEmpty()) {
					int status = 0;
					for (Customer cust : customer) {
						if (cust.getPassword() != null) {
							if (!cust.getPassword().isEmpty()) {
								status++;
							}
						}
					}
					LOGGER.info(" CustomerServiceImpl : findByEmailAndMerchantIdAndVendorIdV2 :status:"+status);

					if (status > 0) {
						MailSendUtil.forgotPasswordEmailForMultiLocationCustomerV2(customer.get(0),vendorId,environment);
						LOGGER.info("----------------End :: CustomerServiceImpl : findByEmailAndMerchantIdAndVendorIdV2 : return true:------------------------");

						return true;
					} else {
						LOGGER.info("----------------End :: CustomerServiceImpl : findByEmailAndMerchantIdAndVendorIdV2 : return false:------------------------");

						return false;
					}
				} else {
					LOGGER.info("----------------End :: CustomerServiceImpl : findByEmailAndMerchantIdAndVendorIdV2 : return false:------------------------");

					return false;
				}
			} else {
				LOGGER.info("----------------End :: CustomerServiceImpl : findByEmailAndMerchantIdAndVendorIdV2 : return false:------------------------");

				return false;
			}
		
		}
		public boolean findByEmailAndMerchantIdV2(String emailId, Integer merchantId) {
			LOGGER.info("----------------Start :: CustomerServiceImpl : findByEmailAndMerchantIdV2 :------------------------");
			LOGGER.info(" CustomerServiceImpl : findByEmailAndMerchantIdV2 : emailId:"+emailId+":merchantId:"+merchantId);

			List<Customer> customer = customerrRepository.findByEmailIdAndMerchantId(emailId, merchantId);
			if (customer != null) {
				if (!customer.isEmpty()) {
					int status = 0;
					for (Customer cust : customer) {
						if (cust.getPassword() != null) {
							if (!cust.getPassword().isEmpty()) {
								status++;
							}
						}
					}
					LOGGER.info(" CustomerServiceImpl : findByEmailAndMerchantIdV2 : status:"+status);

					if (status > 0) {
						MailSendUtil.forgotPasswordEmailV2(customer.get(0),environment);
						LOGGER.info("----------------End :: CustomerServiceImpl : findByEmailAndMerchantIdV2 :return true------------------------");

						return true;
					} else {
						LOGGER.info("----------------End :: CustomerServiceImpl : findByEmailAndMerchantIdV2 :return false------------------------");

						return false;
					}
				} else {
					LOGGER.info("----------------End :: CustomerServiceImpl : findByEmailAndMerchantIdV2 :return false------------------------");

					return false;
				}
			} else {
				LOGGER.info("----------------End :: CustomerServiceImpl : findByEmailAndMerchantIdV2 :return false------------------------");

				return false;
			}
		}

		public void save(Customer customer) {
			
			if(customer!=null)
			{
				customerrRepository.save(customer);
			}
		}
		
		public String showUserByMerchantId(Merchant merchant) {

			LOGGER.info("===============  PizzaServiceImpl : Inside showPizzaSizeData :: Start  ============= ");
			LOGGER.info(" pizzaSizeId  :  merchant " + merchant.getId());
			
			LOGGER.info("showUserByMerchantId");

			String response="";
			
			List<String> customerType =new ArrayList<String>();
			customerType.add("admin");
			customerType.add("location");
			List<Customer> customerdata = new ArrayList<Customer>();
			if(merchant !=null) {
			List<Customer> customerlist = customerrRepository.findByMerchantIdAndVendorIdAndCustomerType(merchant.getId(), merchant.getOwner().getId());
			
			
			for(Customer customers : customerlist) {
				
		
					Customer customer2 = new Customer();
					
					customer2.setId(customers.getId());
					
					if(customers.getLastName() == null)
					customer2.setName(customers.getFirstName());
					else
					customer2.setName((customers.getFirstName()+" "+customers.getLastName()).replace("null", ""));
					
					customer2.setEmailId(customers.getEmailId());
					
					if(customers.getPhoneNumber() != null)
			        customer2.setPhoneNumber(customers.getPhoneNumber());
					else
					customer2.setPhoneNumber("");

			        customer2.setCustomerType(customers.getCustomerType());
			       
			        customer2.
			        setPasswordlink("<input type=\"button\" id="+customers.getId()+" value=\"forgot password\" onclick=\"getCustomer("+customers.getId()+")\" class=\"link\">");
			        
			        customer2.setAction("<a href=editUser?userId=" + customers.getId()
						+ " class='edit'><i class='fa fa-pencil' aria-hidden='true'></i></a>");

			        customerdata.add(customer2);
				} 
			
			
			Gson gson = new GsonBuilder().setPrettyPrinting().create();
			if(customerdata.size()>0) {
			response = gson.toJson(customerdata);
			}
			
			LOGGER.info("===============  PizzaServiceImpl : Inside showPizzaSizeData :: End  ============= ");
			}
			
			return response;
		
		}

		public void updateCustomerOnAdmin(Customer customer ,Merchant merchant) {
			try {
				boolean emailFlag =false;
				boolean merchantFlag = false;
				boolean vendorFlag = false;
				String date=null;
				if(merchant!=null && merchant.getTimeZone()!=null && merchant.getTimeZone().getTimeZoneCode()!=null) {
					date=DateUtil.findCurrentDateWithTimeforTimeZone(merchant.getTimeZone().getTimeZoneCode());
				}else {
					date=DateUtil.findCurrentDateWithTime();
				}
				
				if (customer!= null) {
					
                    Customer existingcustomer =customerrRepository.findOne(customer.getId());
					existingcustomer.setFirstName(customer.getFirstName());
					existingcustomer.setLastName(customer.getLastName());
					existingcustomer.setPhoneNumber(customer.getPhoneNumber());
					existingcustomer.setUpdatedDate(date);
			
			        customerrRepository.save(existingcustomer);

							
				
		
				}
					}
					
			catch (Exception e) {
					LOGGER.info("Exception in updateCustomerOnAdmin : "+e);
					 //return "redirect:" + environment.getProperty("BASE_URL") + "/support";
					
				}
			}

		public String saveCustomerOnAdmin(Customer customer , Merchant merchant) {
			String result="";
			try {
				boolean merchantFlag = false;
				boolean vendorFlag = false;
				String date=null;
				if(merchant!=null && merchant.getTimeZone()!=null && merchant.getTimeZone().getTimeZoneCode()!=null) {
					date=DateUtil.findCurrentDateWithTimeforTimeZone(merchant.getTimeZone().getTimeZoneCode());
				}else {
					date=DateUtil.findCurrentDateWithTime();
				}
				if (customer != null) {
					
					List<Customer> customerChck = customerrRepository.
							findByEmailAndPasswordNotNullAndCustomerType(customer.getEmailId());
					
					if(customerChck == null || customerChck.size()==0) {
						
						List<Customer> deletedCustomer = customerrRepository.
								findDeletedCustomerByEmailAndPasswordNotNull(customer.getEmailId());
						Customer newCustomer = (deletedCustomer != null && deletedCustomer.size()>0) ? deletedCustomer.get(0) : new Customer();
						
						 if(newCustomer.getId() == null)
					     newCustomer.setFirstName(customer.getFirstName());
						 newCustomer.setLastName(customer.getLastName());
						 newCustomer.setEmailId(customer.getEmailId());
						 newCustomer.setPassword(EncryptionDecryptionUtil.encryptString(customer.getPassword()));
						 newCustomer.setPhoneNumber(customer.getPhoneNumber());
                         newCustomer.setCreatedDate(date);	 
						 newCustomer.setUpdatedDate(date);
						 newCustomer.setCustomerType(customer.getCustomerType());
						 newCustomer.setMerchantt(merchant);
						 newCustomer.setVendor(merchant.getOwner());
						customer = customerrRepository.save(newCustomer);
						}else {
							result="Email Id already exists, Plz try using another mail";
					}
						
					}
					
				}catch (Exception e) {
					LOGGER.info("Exception in updateCustomerOnAdmin : "+e);
					 return "redirect:" + environment.getProperty("BASE_URL") + "/support";
					
				}
			return result;
			}
		
    public boolean findUserByEmail(String emailId) {
			
			LOGGER.info("----------------Start :: CustomerServiceImpl : findUserByEmail------------------------");
			LOGGER.info(" CustomerServiceImpl : findUserByEmail: emailId:"+emailId);
			boolean flag = false;
	  
			List<Customer> customers = customerrRepository.findByEmailId(emailId);
			Customer admin = null;
			if (customers != null) {
				if (!customers.isEmpty()) {
					int status = 0;
					for (Customer cust : customers) {
						if (cust.getPassword() != null && cust.getCustomerType() != null
								&& (cust.getCustomerType().equals("admin") || cust.getCustomerType().equals("location"))) {
							if (!cust.getPassword().isEmpty()) {
								status++;
								cust.setUserPage("user");
								admin = cust;
							}
						}
					}
					LOGGER.info(" CustomerServiceImpl : findUserByEmail: status:"+status);

					if (status > 0) {
						if (admin != null)
							MailSendUtil.forgotAdminPasswordEmail(admin,environment);
						LOGGER.info("----------------End :: CustomerServiceImpl : findUserByEmail------------------------");
						return true;
					} else {
						LOGGER.info("----------------End :: CustomerServiceImpl : findUserByEmail------------------------");
						return false;
					}
				} else {
					LOGGER.info("----------------End :: CustomerServiceImpl : findUserByEmail------------------------");
					return false;
				}
			} else {
				LOGGER.info("----------------End :: CustomerServiceImpl : findUserByEmail------------------------");
				return false;
			}
		}
}
