package com.foodkonnekt.serviceImpl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.hibernate.Hibernate;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.foodkonnekt.clover.vo.SearchVO;
import com.foodkonnekt.model.Address;
import com.foodkonnekt.model.ApplicationDto;
import com.foodkonnekt.model.CardInfo;
import com.foodkonnekt.model.ConvenienceFee;
import com.foodkonnekt.model.Customer;
import com.foodkonnekt.model.EncryptedKey;
import com.foodkonnekt.model.Item;
import com.foodkonnekt.model.ItemTax;
import com.foodkonnekt.model.LocationDto;
import com.foodkonnekt.model.Merchant;
import com.foodkonnekt.model.MerchantConfiguration;
import com.foodkonnekt.model.MerchantKritiq;
import com.foodkonnekt.model.MerchantLogin;
import com.foodkonnekt.model.MerchantOrders;
import com.foodkonnekt.model.MerchantSubscription;
import com.foodkonnekt.model.NotificationMethod;
import com.foodkonnekt.model.OpeningClosingDay;
import com.foodkonnekt.model.OpeningClosingTime;
import com.foodkonnekt.model.OrderPaymentDetail;
import com.foodkonnekt.model.OrderR;
import com.foodkonnekt.model.PaymentGateWay;
import com.foodkonnekt.model.PaymentGatewayName;
import com.foodkonnekt.model.PaymentMode;
import com.foodkonnekt.model.PickUpTime;
import com.foodkonnekt.model.Pos;
import com.foodkonnekt.model.Role;
import com.foodkonnekt.model.SocialMediaLinks;
import com.foodkonnekt.model.Subscription;
import com.foodkonnekt.model.TaxRates;
import com.foodkonnekt.model.TimeZone;
import com.foodkonnekt.model.Vendor;
import com.foodkonnekt.model.VirtualFund;
import com.foodkonnekt.repository.AddressRepository;
import com.foodkonnekt.repository.CardInfoRepository;
import com.foodkonnekt.repository.ConvenienceFeeRepository;
import com.foodkonnekt.repository.CustomerrRepository;
import com.foodkonnekt.repository.ItemTaxRepository;
import com.foodkonnekt.repository.ItemmRepository;
import com.foodkonnekt.repository.MerchantConfigurationRepository;
import com.foodkonnekt.repository.MerchantEncryptedKeyRepository;
import com.foodkonnekt.repository.MerchantKritiqRepository;
import com.foodkonnekt.repository.MerchantLoginRepository;
import com.foodkonnekt.repository.MerchantRepository;
import com.foodkonnekt.repository.MerchantSubscriptionRepository;
import com.foodkonnekt.repository.NotificationMethodRepository;
import com.foodkonnekt.repository.OpeningClosingDayRepository;
import com.foodkonnekt.repository.OpeningClosingTimeRepository;
import com.foodkonnekt.repository.OrderPaymentDetailRepository;
import com.foodkonnekt.repository.OrderRepository;
import com.foodkonnekt.repository.PaymentGateWayRepository;
import com.foodkonnekt.repository.PaymentGatewayNameRepository;
import com.foodkonnekt.repository.PaymentModeRepository;
import com.foodkonnekt.repository.PickUpTimeRepository;
import com.foodkonnekt.repository.SocialMediaLinksRepository;
import com.foodkonnekt.repository.SubscriptionRepository;
import com.foodkonnekt.repository.TaxRateRepository;
import com.foodkonnekt.repository.TimeZoneRepository;
import com.foodkonnekt.repository.VendorRepository;
import com.foodkonnekt.repository.VirtualFundRepository;
import com.foodkonnekt.service.MerchantService;
import com.foodkonnekt.util.CloverUrlUtil;
import com.foodkonnekt.util.DateUtil;
import com.foodkonnekt.util.EncryptionDecryptionToken;
import com.foodkonnekt.util.EncryptionDecryptionUtil;
import com.foodkonnekt.util.IConstant;
import com.foodkonnekt.util.JsonUtil;
import com.foodkonnekt.util.MailSendUtil;
import com.foodkonnekt.util.NonPosUrlUtil;
import com.foodkonnekt.util.ProducerUtil;
import com.foodkonnekt.util.UrlConstant;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mysql.fabric.xmlrpc.base.Array;
import com.restfb.json.JsonArray;

@Service
public class MerchantServiceImpl implements MerchantService {
	
	private static final Logger LOGGER= LoggerFactory.getLogger(MerchantServiceImpl.class);
	
	@Autowired
	private Environment environment;
	
	@Autowired
    private VirtualFundRepository virtualFundRepository;
	
	@Autowired
    private NotificationMethodRepository notificationMethodRepository;
	
    @Autowired
	private MerchantRepository merchantRepository;
	
	@Autowired
	private TaxRateRepository taxRateRepository;
	
	@Autowired
	private SocialMediaLinksRepository socialMediaLinksRepository;
	
	@Autowired
	private SubscriptionRepository subscriptionRepository;
	
	@Autowired
	private AddressRepository addressRepository;
	
	@Autowired
	private TimeZoneRepository timeZoneRepository;
	
	@Autowired
	private OpeningClosingDayRepository openingClosingDayRepository;
	
	@Autowired
	private OpeningClosingTimeRepository openingClosingTimeRepository;
	
	@Autowired
	private PickUpTimeRepository pickUpTimeRepository;
	
	@Autowired
	private ConvenienceFeeRepository convenienceFeeRepository;
	
	@Autowired
	private MerchantSubscriptionRepository merchantSubscriptionRepository;
	
	@Autowired
	private OrderRepository orderRepository;
	
	@Autowired
	private MerchantLoginRepository merchantLoginRepository;
	
	@Autowired
	private VendorRepository vendorRepository;
	
	@Autowired
	private MerchantKritiqRepository merchantKritiqRepository;
	
	@Autowired
	private ItemmRepository itemmRepository;
	
	@Autowired
	private ItemTaxRepository itemTaxRepository;
	
	@Autowired
	private PaymentGateWayRepository paymentGateWayRepository;
	
	@Autowired
	private PaymentGatewayNameRepository paymentGatewayNameRepository;
	
	@Autowired
	private MerchantEncryptedKeyRepository merchantEncryptedKeyRepository;
	
	@Autowired
	private OrderPaymentDetailRepository paymentDetailRepository;
	
	@Autowired
	private PaymentModeRepository paymentModeRepository;
	
	@Autowired
	private CardInfoRepository cardInfoRepository;
	
	@Autowired
	private CustomerrRepository customerrRepository;
	
	@Autowired
	private MerchantConfigurationRepository merchantConfigurationRepository;
	
	/**
	 * Find by posMerchantId
	 */
	public Merchant getMerchantByMerchantPsoId(String posMerchantId) {
		LOGGER.info("============================Start :: MerchantServiceImpl:: getMerchantByMerchantPsoId:========================");
		LOGGER.info("MerchantServiceImpl:: getMerchantByMerchantPsoId::posMerchantId:"+posMerchantId);
		LOGGER.info("============================End :: MerchantServiceImpl:: getMerchantByMerchantPsoId:========================");
		
		return merchantRepository.findByPosMerchantId(posMerchantId);
	}
	
	/**
	 * Find address by merchantId
	 */
	public List<Address> findAddressByMerchantId(Integer merchantId) {
		LOGGER.info("============================Start :: MerchantServiceImpl:: findAddressByMerchantId:========================");
		LOGGER.info("MerchantServiceImpl:: findAddressByMerchantId::merchantId:"+merchantId);
		LOGGER.info("============================End :: MerchantServiceImpl:: findAddressByMerchantId:========================");
		
		return addressRepository.findByMerchantId(merchantId);
	}
	
	/**
	 * Save and update merchant logo
	 */
	public Merchant addMerchantLogo(Merchant merchant) {
		LOGGER.info("============================Start :: MerchantServiceImpl:: addMerchantLogo:========================");
		LOGGER.info("MerchantServiceImpl:: addMerchantLogo::merchantId:"+merchant.getId());
		
		Merchant resultMerchant = merchantRepository.findOne(merchant.getId());
		resultMerchant.setMerchantLogo(merchant.getMerchantLogo());
		merchantRepository.save(resultMerchant);
		LOGGER.info("============================End :: MerchantServiceImpl:: addMerchantLogo:========================");
		
		LOGGER.info("MerchantServiceImpl:: addMerchantLogo::save :merchant");
		
		return resultMerchant;
	}
	
	/**
	 * Find by merchant id
	 */
	public Merchant findByMerchantId(Integer merchantId) {
		LOGGER.info("============================Start :: MerchantServiceImpl:: findByMerchantId:========================");
		LOGGER.info("MerchantServiceImpl:: findByMerchantId::merchantId:"+merchantId);
		LOGGER.info("============================End :: MerchantServiceImpl:: findByMerchantId:========================");
		
		return merchantRepository.findOne(merchantId);
	}
	
	/**
	 * find by opening closing id
	 */
	public List<OpeningClosingTime> findOpeningClosingHourByMerchantId(
			Integer merchantId) {
		LOGGER.info("============================Start :: MerchantServiceImpl:: findOpeningClosingHourByMerchantId:========================");
		LOGGER.info(" MerchantServiceImpl:: findOpeningClosingHourByMerchantId: merchantId :"+merchantId);
		
		List<OpeningClosingDay> openingClosingDays = openingClosingDayRepository
				.findByMerchantId(merchantId);
		List<OpeningClosingTime> openingClosingTimes = new ArrayList<OpeningClosingTime>();
		for (OpeningClosingDay day : openingClosingDays) {
			LOGGER.info(" MerchantServiceImpl:: findOpeningClosingHourByMerchantId: DayId :"+day.getId());
			
			List<OpeningClosingTime> times = openingClosingTimeRepository
					.findByOpeningClosingDayId(day.getId());
			for (OpeningClosingTime openingClosingTime : times) {
				openingClosingTimes.add(openingClosingTime);
			}
		}
		LOGGER.info("============================End :: MerchantServiceImpl:: findOpeningClosingHourByMerchantId:========================");
		
		return openingClosingTimes;
	}
	
	/**
	 * Find by merchantId
	 */
	public Integer findVendorIdByMerchantId(Integer merchantId) {
		LOGGER.info("============================Start :: MerchantServiceImpl:: findVendorIdByMerchantId:========================");
		LOGGER.info("MerchantServiceImpl:: findVendorIdByMerchantId:merchantId:"+merchantId);
		LOGGER.info("============================End :: MerchantServiceImpl:: findVendorIdByMerchantId:========================");
		
		return merchantRepository.findOwnerByMerchantId(merchantId);
	}
	
	/**
	 * Find by merchant id
	 */
	public Merchant findById(int merchantId) {
		LOGGER.info("============================Start :: MerchantServiceImpl:: findById:========================");
		LOGGER.info("MerchantServiceImpl:: findById :merchantId:"+merchantId);
		LOGGER.info("============================End :: MerchantServiceImpl:: findById:========================");
		
		return merchantRepository.findOne(merchantId);
	}
	
	public List<Merchant> findByFBTabIds(String pageId) {
		LOGGER.info("============================Start :: MerchantServiceImpl:: findByFBTabIds:========================");
		LOGGER.info("MerchantServiceImpl:: findByFBTabIds :pageId:"+pageId);
		LOGGER.info("============================End :: MerchantServiceImpl:: findByFBTabIds:========================");
		
		return merchantRepository.findByFBTabIds(pageId);
	}
	
	/**
	 * Find by merchant posId
	 */
	public Merchant findbyPosId(String merchantId) {
		LOGGER.info("============================Start :: MerchantServiceImpl:: findbyPosId:========================");
		LOGGER.info("MerchantServiceImpl:: findbyPosId :merchantId:"+merchantId);
		
		Merchant merchant = merchantRepository.findByPosMerchantId(merchantId);
		if (merchant != null && merchant.getTimeZone() == null) {
			List<Address> addresses = addressRepository
					.findByMerchantId(merchant.getId());
			if (addresses != null && addresses.size() > 0) {
				Address address = addresses.get(0);
				address.setMerchant(merchant);
				if (address != null && address.getZip() != null && merchant.getTimeZone()!=null) {
					String timeZoneCode = DateUtil.getTimeZone(address,environment);
					if (timeZoneCode != null) {
						TimeZone timeZone = timeZoneRepository
								.findByTimeZoneCode(timeZoneCode);
						merchant.setTimeZone(timeZone);
						merchantRepository.save(merchant);
						LOGGER.info("MerchantServiceImpl:: findbyPosId :save merchant:");
						
					}
				}
				
			}
		}
		LOGGER.info("============================End :: MerchantServiceImpl:: findbyPosId:========================");
		
		return merchant;
	}
	
	public Merchant findbyStoreId(String storeId) {
		LOGGER.info("============================Start :: MerchantServiceImpl:: findbyStoreId:========================");
		LOGGER.info(" MerchantServiceImpl:: findbyStoreId: storeId :"+storeId);
		LOGGER.info("============================End :: MerchantServiceImpl:: findbyStoreId:========================");
		
		return merchantRepository.findByStoreId(storeId);
	}
	
	/**
	 * Find openingClosingHours by merchantId
	 */
	public List<OpeningClosingDay> findOpeningClosingDayByMerchantId(
			Integer merchantId) {
		LOGGER.info("============================Start :: MerchantServiceImpl:: findOpeningClosingDayByMerchantId:========================");
		LOGGER.info("MerchantServiceImpl:: findOpeningClosingDayByMerchantId:merchantId :"+merchantId);
		
		List<OpeningClosingDay> openingClosingDays = openingClosingDayRepository
				.findByMerchantId(merchantId);
		for (OpeningClosingDay day : openingClosingDays) {
			LOGGER.info("MerchantServiceImpl:: findOpeningClosingDayByMerchantId: DayId :"+day.getId());
			
			List<OpeningClosingTime> times = openingClosingTimeRepository
					.findByOpeningClosingDayId(day.getId());
			day.setTimes(times);
		}
		LOGGER.info("============================End :: MerchantServiceImpl:: findOpeningClosingDayByMerchantId:========================");
		
		return openingClosingDays;
	}
	
	/**
	 * Find by merchantId
	 */
	public List<Merchant> findMerchantById(Integer merchantId) {
		LOGGER.info("============================Start :: MerchantServiceImpl:: findMerchantById:========================");
		LOGGER.info(" MerchantServiceImpl:: findMerchantById: merchantId:"+merchantId);
		
		List<Merchant> merchants = new ArrayList<Merchant>();
		Merchant merchant = merchantRepository.findOne(merchantId);
		List<Address> addresses = addressRepository
				.findByMerchantId(merchantId);
		merchant.setAddress(addresses.get(0).getAddress1() + " "
									+ addresses.get(0).getAddress2() + " "
									+ addresses.get(0).getAddress3() + " "
									+ addresses.get(0).getCity() + " "
									+ addresses.get(0).getState() + " "
									+ addresses.get(0).getCountry() + "-"
									+ addresses.get(0).getZip());
		merchants.add(merchant);
		LOGGER.info("============================End :: MerchantServiceImpl:: findMerchantById:========================");
		
		return merchants;
	}
	
	/**
	 * Save merchant
	 */
	public Merchant save(Merchant merchant) {
		LOGGER.info("============================Start :: MerchantServiceImpl:: save:========================");
		
		Vendor vendor = merchant.getOwner();
		if (vendor != null) {
			vendor = vendorRepository.save(vendor);
			LOGGER.info("MerchantServiceImpl:: save: save vendor");
			
			merchant.setOwner(vendor);
		}
		
		Merchant result = merchantRepository.save(merchant);
		LOGGER.info("MerchantServiceImpl:: save: save mechant");
		
		if (merchant.getAddresses() != null
				&& Hibernate.isInitialized(merchant.getAddresses())
				&& merchant.getAddresses().size() > 0) {
			Address address = merchant.getAddresses().get(0);
			address.setMerchant(merchant);
			if (address != null && address.getZip() != null) {
				String timeZoneCode = DateUtil.getTimeZone(address,environment);
				if (timeZoneCode != null) {
					TimeZone timeZone = timeZoneRepository
							.findByTimeZoneCode(timeZoneCode);
					merchant.setTimeZone(timeZone);
					merchantRepository.save(merchant);
					LOGGER.info("MerchantServiceImpl:: save: save mechant");
					
				}
			}
			addressRepository.save(address);
			LOGGER.info("MerchantServiceImpl:: save: save address");
			
		}
		LOGGER.info("============================ End :: MerchantServiceImpl:: save:========================");
		
		return merchant;
	}
	
	/**
	 * Save pickup time
	 */
	public void savePickupTime(PickUpTime pickUpTime) {
		LOGGER.info("----------------Start :: MerchantServiceImpl : savePickupTime------------------------");
		try {
			if(pickUpTime!=null && pickUpTime.getMerchantId()!=null)
			{
				LOGGER.info(" MerchantServiceImpl : savePickupTime : pickuptime id:" +pickUpTime.getId());
				PickUpTime  pickUpTime1=pickUpTimeRepository.findByMerchantId(pickUpTime.getMerchantId());
				if(pickUpTime1!=null)
				{
					if(pickUpTime.getPickUpTime()!=null && !pickUpTime.getPickUpTime().isEmpty())
						pickUpTime1.setPickUpTime(pickUpTime.getPickUpTime());
					if(pickUpTime.getMinOrderAmount()!=null && pickUpTime.getMinOrderAmount()>0)
						pickUpTime1.setMinOrderAmount(pickUpTime.getMinOrderAmount());
					pickUpTimeRepository.save(pickUpTime1);
					LOGGER.info(" MerchantServiceImpl : savePickupTime : update Pickup time for MerchantId: " +pickUpTime.getMerchantId());
				}
				else {
					if(pickUpTime!=null)
					{
						if (pickUpTime.getPickUpTime() == null || pickUpTime.getPickUpTime().isEmpty() ) {
							pickUpTime.setPickUpTime(IConstant.DEFAULT_PICKUP_TIME);
						}
						pickUpTimeRepository.save(pickUpTime);
						LOGGER.info(" MerchantServiceImpl : savePickupTime : Create Pickup time for MerchantId:" +pickUpTime.getMerchantId());
					}
				}
			}
		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
				LOGGER.error(" End :: MerchantServiceImpl :: savePickupTime : Exception "+e);
			}
			LOGGER.error("error: " + e.getMessage());
		}
	}
	
	/**
	 * Save convenience fee
	 */
	public void saveConvenienceFee(ConvenienceFee convenienceFee,
								   Merchant merchant) {
		LOGGER.info("----------------- Start :: MerchantServiceImpl :: saveConvenienceFee---------------");
		
		try {
			if (convenienceFee.getConvenienceFee() != null) {
				if (convenienceFee.getConvenienceFee().isEmpty()) {
					convenienceFee.setConvenienceFee("0");
				}
				Double convenienceFees = Double.parseDouble(convenienceFee
																	.getConvenienceFee());
				LOGGER.info(": MerchantServiceImpl :: saveConvenienceFee :: convenienceFees: "+convenienceFees);
				
				if (merchant.getOwner() != null
						&& merchant.getOwner().getPos() != null
						&& merchant.getOwner().getPos().getPosId() == 1) {
					if (convenienceFee.getId() == null
							|| convenienceFee.getConvenienceFeeLineItemPosId() == null) {
						/*String result = CloverUrlUtil.addDeliveryFeeLineItem(
								convenienceFees, merchant.getPosMerchantId(),
								merchant.getAccessToken(), "Convenience Fee",
								convenienceFee.getIsTaxable());*/
						//CHANGE Convenience to Online
						LOGGER.info(": MerchantServiceImpl :: saveConvenienceFee :: posMerchantId: "+merchant.getPosMerchantId()+":AccessToken:"+merchant.getAccessToken());
						
						String result = CloverUrlUtil.addDeliveryFeeLineItem(
								convenienceFees, merchant.getPosMerchantId(),
								merchant.getAccessToken(), "Online Fee",
								convenienceFee.getIsTaxable(),environment);
						JSONObject jObject = new JSONObject(result);
						convenienceFee.setConvenienceFeeLineItemPosId(jObject
																			  .getString("id"));
					} else {
						//CHANGE Convenience to Online
						/*String result = CloverUrlUtil
								.updateDeliveryFeeLineItem(
										convenienceFees,
										merchant.getPosMerchantId(),
										merchant.getAccessToken(),
										"Convenience Fee",
										convenienceFee
												.getConvenienceFeeLineItemPosId(),
										convenienceFee.getIsTaxable());*/
						LOGGER.info(": MerchantServiceImpl :: saveConvenienceFee :: posMerchantId: "+merchant.getPosMerchantId()+":AccessToken:"+merchant.getAccessToken());
						
						String result = CloverUrlUtil
								.updateDeliveryFeeLineItem(
										convenienceFees,
										merchant.getPosMerchantId(),
										merchant.getAccessToken(),
										"Online Fee",
										convenienceFee
												.getConvenienceFeeLineItemPosId(),
										convenienceFee.getIsTaxable(),environment);
						JSONObject jObject = new JSONObject(result);
						if (!jObject.toString().contains("Not Found")
								&& jObject.has("id"))
							convenienceFee
									.setConvenienceFeeLineItemPosId(jObject
																			.getString("id"));
					}
				}
				convenienceFeeRepository.save(convenienceFee);
				LOGGER.info(": MerchantServiceImpl :: saveConvenienceFee ::save convenienceFee ");
				
			}
		} catch (Exception e) {
			if (e != null) {
				LOGGER.error(":End:: MerchantServiceImpl :: saveConvenienceFee :: Exception:"+e);
				
				MailSendUtil.sendExceptionByMail(e,environment);
			}
			LOGGER.error("error: " + e.getMessage());
			LOGGER.error(":End:: MerchantServiceImpl :: saveConvenienceFee :: Exception:"+e);
			
		}
	}
	
	public PickUpTime findPickupTime(Integer merchantId) {
		LOGGER.info("----------------Start :: MerchantServiceImpl : findPickupTime------------------------");
		LOGGER.info("MerchantServiceImpl :: findPickupTime : merchantId "+merchantId);
		LOGGER.info("----------------End :: MerchantServiceImpl : findPickupTime------------------------");
		return pickUpTimeRepository.findByMerchantId(merchantId);
	}
	
	public List<Merchant> findAllMerchants() {
		// TODO Auto-generated method stub
		LOGGER.info("----------------Start :: MerchantServiceImpl : findAllMerchants------------------------");
		List<Merchant> merchants = merchantRepository.findAll();
		for (Merchant merchant : merchants) {
			LOGGER.info(" MerchantServiceImpl : findAllMerchants: merchantId:"+merchant.getId());
			
			List<OrderR> orderRs = orderRepository.findByMerchantId(merchant.getId());
			if (orderRs != null) {
				merchant.setTotalOrders(orderRs.size());
			} else {
				merchant.setTotalOrders(0);
			}
			
		}
		LOGGER.info("----------------End :: MerchantServiceImpl : findAllMerchants------------------------");
		
		return merchants;
	}
	
	public List<Merchant> findAllMerchants(String startDate, String endDate) {
		return findAllMerchants(startDate,endDate,"Detail");
	}
	
	public List<Merchant> findAllMerchants(String startDate, String endDate, String view) {
		// TODO Auto-generated method stub
		LOGGER.info("----------------Start :: MerchantServiceImpl : findAllMerchants------------------------");
		LOGGER.info("MerchantServiceImpl : findAllMerchants : startDate:"+startDate+":endDate:"+endDate);
		
		List<Merchant> merchants = merchantRepository.findAll();
		for (Merchant merchant : merchants) {
			
			if (merchant.getAllowMultiPay()!=null && merchant.getAllowMultiPay()) {
				//inventoryItemVo.setStatus("Active");
				merchant.setAction("<div><a href=javascript:void(0) id='nav_"+merchant.getId()+"' class='nav-toggle' itmId=" + merchant.getId()+ "  style='color: blue;'>Disable MultiPay</a></div>");
				
				
			} else {
				merchant.setAction("<div><a href=javascript:void(0) id='nav_"+merchant.getId()+"' class='nav-toggle' itmId=" + merchant.getId()+ "  style='color: blue;'>Enable MultiPay</a></div>");
			}
			java.util.Date startDate1 = null;
			java.util.Date endDate1 = null;
			
			try {
				if(startDate!= null && endDate!= null){
					
					startDate1=new SimpleDateFormat(IConstant.YYYYMMDD).parse(startDate);
					//endDate1=new SimpleDateFormat(IConstant.YYYYMMDD).parse(endDate);
					
					SimpleDateFormat dateFormat = new SimpleDateFormat( "yyyy-MM-dd" );
					Calendar cal = Calendar.getInstance();
					cal.setTime( dateFormat.parse( endDate ) );
					cal.add( Calendar.DATE, 1 );
					//endDate = dateFormat.format(cal.getTime());
					endDate1=new SimpleDateFormat(IConstant.YYYYMMDD).parse(dateFormat.format(cal.getTime()));
					
					
					
					
				}
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				LOGGER.error("error: " + e.getMessage());
				LOGGER.error(" End :: MerchantServiceImpl : findAllMerchants : Exception :"+e);
				
			}
			LOGGER.info(" MerchantServiceImpl : findAllMerchants : startDate1 :"+startDate1+":endDate1:"+endDate1+":merchantId:"+merchant.getId());
			
			if("Detail".equals(view)) {
				
				List<OrderR> orderRs = orderRepository.findByMerchantsId(startDate1, endDate1, merchant
						.getId());
				
				if (orderRs != null) {
					merchant.setTotalOrders(orderRs.size());
				} else {
					merchant.setTotalOrders(0);
				}
				LOGGER.info(" MerchantServiceImpl : findAllMerchants : startDate1 :" + startDate1 + ":endDate1:" +
									endDate1 + ":merchantId:" + merchant.getId() + "case");
				
				Integer cashOrderCount =
						orderRepository.getCashOrderCount(startDate1, endDate1, merchant.getId(), "cash");
				
				if (orderRs != null) {
					merchant.setCashOrder(cashOrderCount);
				} else {
					merchant.setCashOrder(0);
				}
				LOGGER.info(" MerchantServiceImpl : findAllMerchants : startDate1 :" + startDate1 + ":endDate1:" +
									endDate1 + ":merchantId:" + merchant.getId() + "credit cart");
				
				Integer creditCardOrderCount =
						orderRepository.getCashOrderCount(startDate1, endDate1, merchant.getId(), "credit Card");
				
				if (orderRs != null) {
					merchant.setCreditOrder(creditCardOrderCount);
				} else {
					merchant.setCreditOrder(0);
				}
				LOGGER.info(" MerchantServiceImpl : findAllMerchants : startDate1 :" + startDate1 + ":endDate1:" +
									endDate1 + ":merchantId:" + merchant.getId() + "pickup");
				
				Integer pickupCount = orderRepository.getPickupCount(startDate1, endDate1, merchant.getId(), "pickup");
				
				if (orderRs != null) {
					merchant.setPickup(pickupCount);
				} else {
					merchant.setPickup(0);
				}
				LOGGER.info(" MerchantServiceImpl : findAllMerchants : startDate1 :" + startDate1 + ":endDate1:" +
									endDate1 + ":merchantId:" + merchant.getId() + "delivery");
				
				Integer deliveryCount =
						orderRepository.getPickupCount(startDate1, endDate1, merchant.getId(), "delivery");
				
				if (orderRs != null) {
					merchant.setDelivery(deliveryCount);
				} else {
					merchant.setDelivery(0);
				}
				
				List<MerchantSubscription> merchantSubscriptions = merchantSubscriptionRepository
						.findByMId(merchant.getId());
				merchant.setMerchantSubscriptions(merchantSubscriptions);
			}
			if (merchant.getIsInstall() != null
					&& merchant.getIsInstall() == IConstant.BOOLEAN_TRUE) {
				merchant.setStatus("Installed");
			} else if (merchant.getIsInstall() != null
					&& merchant.getIsInstall() == IConstant.SOFT_DELETE) {
				merchant.setStatus("Un-installed");
			} else {
				merchant.setStatus("In Process");
			}
		}
		LOGGER.info(" End :: MerchantServiceImpl : findAllMerchants :");
		
		return merchants;
	}
	
	/* @Override */
	public MerchantLogin findByEmailAndPassword(String emailId, String password) {
		LOGGER.info(" Start :: MerchantServiceImpl : findByEmailAndPassword :");
		LOGGER.info(" MerchantServiceImpl : findByEmailAndPassword :emailId:"+emailId+":password:"+password);
		
		List<MerchantLogin> merchantLogins = merchantLoginRepository
				.findByEmailIdAndPassword(emailId, password);
		MerchantLogin merchantLogin = null;
		try {
			if (merchantLogins != null) {
				if (!merchantLogins.isEmpty()) {
					merchantLogin = merchantLogins.get(0);
					
				}
			}
		} catch (Exception e) {
			if (e != null) {
				LOGGER.error(" End :: MerchantServiceImpl : findByEmailAndPassword : Exception :"+e);
				
				MailSendUtil.sendExceptionByMail(e,environment);
			}
			LOGGER.error("error: " + e.getMessage());
			LOGGER.error(" End :: MerchantServiceImpl : findByEmailAndPassword : Exception :"+e);
			
		}
		LOGGER.info(" End :: MerchantServiceImpl : findByEmailAndPassword ");
		
		return merchantLogin;
	}
	
	/* @Override */
	public boolean findByEmailId(String emailId) {
		LOGGER.info(" Start :: MerchantServiceImpl : findByEmailId :");
		LOGGER.info(" MerchantServiceImpl : findByEmailId :emailId :"+emailId);
		
		List<MerchantLogin> merchantLogins = merchantLoginRepository
				.findByEmailId(emailId);
		if (merchantLogins != null) {
			if (!merchantLogins.isEmpty()) {
				int status = 0;
				for (MerchantLogin merchantLogin : merchantLogins) {
					if (merchantLogin.getPassword() != null) {
						if (!merchantLogin.getPassword().isEmpty()) {
							status++;
						}
					}
				}
				LOGGER.info(" MerchantServiceImpl : findByEmailId :status :"+status);
				
				if (status > 0) {
					MailSendUtil.forgotMerchantPasswordEmail(merchantLogins
																	 .get(0),environment);
					LOGGER.info("End :: MerchantServiceImpl : findByEmailId :Return true ");
					
					return true;
				} else {
					LOGGER.info("End :: MerchantServiceImpl : findByEmailId :Return false ");
					
					return false;
				}
			} else {
				LOGGER.info("End :: MerchantServiceImpl : findByEmailId :Return false ");
				
				return false;
			}
		} else {
			LOGGER.info("End :: MerchantServiceImpl : findByEmailId :Return false ");
			
			return false;
		}
		
	}
	
	/* @Override */
	public MerchantLogin findByMerchantEmailId(String emailId) {
		LOGGER.info("Start :: MerchantServiceImpl : findByMerchantEmailId ");
		LOGGER.info(" MerchantServiceImpl : findByMerchantEmailId :emailId:"+emailId);
		
		List<MerchantLogin> merchantLogins = merchantLoginRepository
				.findByEmailId(emailId);
		if (merchantLogins != null) {
			if (!merchantLogins.isEmpty()) {
				LOGGER.info(" End:: MerchantServiceImpl : findByMerchantEmailId ");
				
				return merchantLogins.get(0);
			}
		}
		LOGGER.info(" End:: MerchantServiceImpl : findByMerchantEmailId ");
		
		return null;
	}
	
	/* @Override */
	public MerchantLogin saveMerchantLogin(MerchantLogin merchantLogin) {
		LOGGER.info(" Start:: MerchantServiceImpl : saveMerchantLogin ");
		
		if (merchantLogin != null){
			LOGGER.info(" End :: MerchantServiceImpl : saveMerchantLogin : save merchant ");
			
			return merchantLoginRepository.save(merchantLogin);}
		else
			LOGGER.info(" End :: MerchantServiceImpl : saveMerchantLogin : Return null");
		
		return null;
	}
	
	/* @Override */
	public List<Merchant> findAllMerchantsByVendorId(Integer vendorId) {
		LOGGER.info(" Start:: MerchantServiceImpl : findAllMerchantsByVendorId ");
		LOGGER.info(" MerchantServiceImpl : findAllMerchantsByVendorId : vendorId:"+vendorId);
		
		List<Merchant> merchants = merchantRepository
				.findByOwnerIdAndIsInstallNot(vendorId, IConstant.SOFT_DELETE);
		for (Merchant merchant : merchants) {
			LOGGER.info(" MerchantServiceImpl : findAllMerchantsByVendorId : merchantId:"+merchant.getId());
			
			List<OrderR> orderRs = orderRepository.findByMerchantId(merchant
																			.getId());
			if (orderRs != null) {
				merchant.setTotalOrders(orderRs.size());
			} else {
				merchant.setTotalOrders(0);
			}
			Double totalValue = 0.0;
			for (OrderR orderR : orderRs) {
				Double orderPrice = orderR.getOrderPrice();
				if (orderPrice != null) {
					totalValue = totalValue + orderPrice;
				}
			}
			double roundOff = (double) Math.round(totalValue * 100) / 100;
			LOGGER.info(" MerchantServiceImpl : findAllMerchantsByVendorId : roundOff:"+roundOff);
			
			merchant.setTotalOrdersPrice(roundOff);
		}
		LOGGER.info("End:: MerchantServiceImpl : findAllMerchantsByVendorId : ");
		
		return merchants;
	}
	
	public List<Merchant> findAllMerchantsByVendorIdAndDateRange(Integer vendorId, String startDate, String endDate) {
		LOGGER.info("Start:: MerchantServiceImpl : findAllMerchantsByVendorIdAndDateRange ");
		LOGGER.info(" MerchantServiceImpl : findAllMerchantsByVendorIdAndDateRange :vendorId:"+vendorId+":startDate:"+startDate+":endDate:"+endDate);
		
		List<Merchant> merchants = merchantRepository
				.findByOwnerIdAndIsInstallNot(vendorId, IConstant.SOFT_DELETE);
		
		java.util.Date startDateRange = null;
		java.util.Date endDateRange = null;
		
		try {
			if(startDate!= null && endDate!= null){
				
				startDateRange=new SimpleDateFormat(IConstant.YYYYMMDD).parse(startDate);
				//endDateRange=new SimpleDateFormat(IConstant.YYYYMMDD).parse(endDate);
				
				SimpleDateFormat dateFormat = new SimpleDateFormat( "yyyy-MM-dd" );
				Calendar cal = Calendar.getInstance();
				cal.setTime( dateFormat.parse( endDate ) );
				cal.add( Calendar.DATE, 1 );
				//endDate = dateFormat.format(cal.getTime());
				endDateRange=new SimpleDateFormat(IConstant.YYYYMMDD).parse(dateFormat.format(cal.getTime()));
				
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			LOGGER.error("error: " + e.getMessage());
			LOGGER.error(" End :: MerchantServiceImpl : findAllMerchantsByVendorIdAndDateRange : Exception:"+e);
			
		}
		
		
		for (Merchant merchant : merchants) {
			List<OrderR> orderRs = orderRepository.findByMerchantIdAndDateRange(merchant.getId(), startDateRange , endDateRange);
			if (orderRs != null) {
				merchant.setTotalOrders(orderRs.size());
			} else {
				merchant.setTotalOrders(0);
			}
			Double totalValue = 0.0;
			for (OrderR orderR : orderRs) {
				Double orderPrice = orderR.getOrderPrice();
				if (orderPrice != null) {
					totalValue = totalValue + orderPrice;
				}
			}
			double roundOff = (double) Math.round(totalValue * 100) / 100;
			LOGGER.info("MerchantServiceImpl : findAllMerchantsByVendorIdAndDateRange : roundOff :"+roundOff);
			
			merchant.setTotalOrdersPrice(roundOff);
		}
		LOGGER.info(" End :: MerchantServiceImpl : findAllMerchantsByVendorIdAndDateRange  :");
		
		return merchants;
	}
	
	
	
	public SocialMediaLinks saveSocialMediaLinks(
			SocialMediaLinks socialMediaLinks) {
		LOGGER.info(" Start :: MerchantServiceImpl : saveSocialMediaLinks  :");
		
		// TODO Auto-generated method stub
		LOGGER.info(" End :: MerchantServiceImpl : saveSocialMediaLinks  :");
		
		return socialMediaLinksRepository.save(socialMediaLinks);
	}
	
	public SocialMediaLinks getSocialMediaLinksByMerchantId(Integer merchantId) {
		// TODO Auto-generated method stub
		LOGGER.info(" Start :: MerchantServiceImpl : getSocialMediaLinksByMerchantId  :");
		LOGGER.info("  MerchantServiceImpl : getSocialMediaLinksByMerchantId : merchantId :"+merchantId);
		LOGGER.info(" End :: MerchantServiceImpl : getSocialMediaLinksByMerchantId  :");
		
		return socialMediaLinksRepository.findByMerchantId(merchantId);
	}
	
	public String generateLinkActiveCustomerFeedback(Integer merchantId) {
		// TODO Auto-generated method stub
		LOGGER.info(" Start :: MerchantServiceImpl : generateLinkActiveCustomerFeedback  :");
		LOGGER.info("  MerchantServiceImpl : generateLinkActiveCustomerFeedback  : merchantId:"+merchantId);
		
		String merchId = merchantId.toString();
		String base64encode = EncryptionDecryptionUtil.encryption(merchId);
		String orderLink = environment.getProperty("WEB_BASE_URL")
				+ "/kritiq/ofeedbackForm?merchantId=" + base64encode;
		System.out.println(orderLink);
		LOGGER.info("End::  MerchantServiceImpl : generateLinkActiveCustomerFeedback  : orderLink:"+orderLink);
		
		return orderLink;
	}
	
	public String getMerchantSubscription(Integer merchantId, String status) {
		// TODO Auto-generated method stub
		LOGGER.info(" Start :: MerchantServiceImpl : getMerchantSubscription  :");
		LOGGER.info(" MerchantServiceImpl : getMerchantSubscription  :merchantId:"+merchantId+":Status:"+merchantId);
		
		String orderLink = "";
		MerchantKritiq merchantKritiq = null;
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		if (merchantId != null && status != null && !status.isEmpty()
				&& status != "") {
			List<MerchantSubscription> merchantSubscriptions = merchantSubscriptionRepository
					.findByMId(merchantId);
			Merchant merchant = merchantRepository.findById(merchantId);
			Subscription subscription = null;
			if (merchantSubscriptions != null
					&& merchantSubscriptions.size() > 0)
				subscription = subscriptionRepository
						.findOne(merchantSubscriptions.get(0).getSubscription()
										 .getId());
			List<MerchantKritiq> kritiqs = merchantKritiqRepository
					.findByMerchantId(merchantId);
			boolean isFreeSubcription = false;
			if (kritiqs != null && !kritiqs.isEmpty()) {
				// for(MerchantKritiq ktitiq:kritiqs){
				MerchantKritiq ktitiq = kritiqs.get(kritiqs.size() - 1);
				if (ktitiq != null && ktitiq.getSubscrptionType() != null
						&& ktitiq.getSubscrptionType().equals("free")) {
					isFreeSubcription = true;
				} else {
					isFreeSubcription = false;
				}
				if (ktitiq != null && ktitiq.getActive() != null
						&& ktitiq.getActive() == true) {
					merchantKritiq = ktitiq;
				}
				// }
			}
			Date currentDate = (merchant != null && merchant.getTimeZone() != null
					&& merchant.getTimeZone().getTimeZoneCode() != null)
					? DateUtil.getCurrentDateForTimeZonee(merchant.getTimeZone().getTimeZoneCode())
					: new Date();
			if (status.equals("yes")) {
				
				if (subscription != null && subscription.getPrice() >= 49.9) {
					// MERCHANT IS PAID
					
					if (!kritiqs.isEmpty()) {
						// OLD PAID MERCHANT
						try {
							Date date1 = format.parse(kritiqs.get(
									kritiqs.size() - 1).getValidityDate());
							/* if (date1.compareTo(currentDate) >= 0) { */
							// validate date is greater then current date
							orderLink = this
									.generateLinkActiveCustomerFeedback(merchantId);
							merchantKritiq = new MerchantKritiq();
							merchant.setActiveCustomerFeedback(1);
							merchantKritiq.setMerchant(merchant);
							merchantKritiq.setCreateDate(kritiqs.get(
									kritiqs.size() - 1).getCreateDate());
							merchantKritiq
									.setUpdateDate(currentDate.toString());
							
							Calendar cal = Calendar.getInstance();
							cal.setTime(currentDate);
							cal.add(Calendar.DATE, 30);
							currentDate = cal.getTime();
							merchantKritiq.setValidityDate(DateUtil.getYYYYMMDD(currentDate));
							
							merchantKritiq.setActive(true);
							merchantKritiq.setSubscrptionType("paid");
							
							merchantRepository.save(merchant);
							LOGGER.info("  MerchantServiceImpl : getMerchantSubscription  : save merchant");
							
							merchantKritiqRepository.save(merchantKritiq);
							LOGGER.info("  MerchantServiceImpl : getMerchantSubscription  : save merchantKritiq");
							
							/*
							 * }else{
							 *
							 * orderLink = "VALIDITY EXPAIRED "; return orderLink; }
							 */
							
						} catch (ParseException e) {
							// TODO Auto-generated catch block
							LOGGER.error("error: " + e.getMessage());
							LOGGER.error(" End:: MerchantServiceImpl : getMerchantSubscription  : Exception:"+e);
							
						}
						
					} else {
						
						// NEW PAID MERCHANT
						LOGGER.info("  MerchantServiceImpl : getMerchantSubscription  :  NEW PAID MERCHANT");
						
						orderLink = this.generateLinkActiveCustomerFeedback(merchantId);
						merchantKritiq = new MerchantKritiq();
						merchant.setActiveCustomerFeedback(1);
						merchantKritiq.setMerchant(merchant);
						merchantKritiq.setCreateDate(DateUtil.getYYYYMMDD(currentDate));
						
						merchantKritiq.setUpdateDate(DateUtil.getYYYYMMDD(currentDate));
						
						Calendar cal = Calendar.getInstance();
						cal.setTime(currentDate);
						cal.add(Calendar.DATE, 30);
						currentDate = cal.getTime();
						merchantKritiq.setValidityDate(DateUtil.getYYYYMMDD(currentDate));
						
						merchantKritiq.setActive(true);
						merchantKritiq.setSubscrptionType("paid");
						
						merchantRepository.save(merchant);
						LOGGER.info("  MerchantServiceImpl : getMerchantSubscription  :  save MERCHANT");
						
						merchantKritiqRepository.save(merchantKritiq);
						LOGGER.info("  MerchantServiceImpl : getMerchantSubscription  :  save merchantKritiq");
						
					}
					
					/*
					 * merchant.setActiveCustomerFeedback(1); orderLink =
					 * this.generateLinkActiveCustomerFeedback(merchantId); merchantKritiq.setMerchant(merchant);
					 *
					 * merchantKritiq.setUpdateDate(currentDate.toString()); Calendar cal = Calendar.getInstance();
					 * cal.setTime(currentDate); cal.add(Calendar.DATE, 30); currentDate = cal.getTime();
					 * merchantKritiq.setValidityDate(currentDate.toString()); merchantKritiq.setActive(true);
					 *
					 *
					 * merchantRepository.save(merchant); merchantKritiqRepository.save(merchantKritiq);
					 * System.out.println(merchantKritiq.getValidityDate());
					 */
				} else {
					// MERCHANT IS FREE
					LOGGER.info("  MerchantServiceImpl : getMerchantSubscription  :   MERCHANT IS FREE");
					
					if (kritiqs != null && !kritiqs.isEmpty() && kritiqs.size() > 0) {
						// OLD FREE MERCHANT
						LOGGER.info("  MerchantServiceImpl : getMerchantSubscription  : OLD FREE MERCHANT  ");
						
						try {
							Date date1 = DateUtil
									.convertStringToDate(kritiqs.get(kritiqs.size() - 1).getValidityDate());
							// currentDate=DateUtil.convertStringToDate(currentDate.toString());
							System.out.println(date1.compareTo(currentDate));
							LOGGER.info("  MerchantServiceImpl : getMerchantSubscription  :date compare to current date "+date1.compareTo(currentDate));
							
							if (isFreeSubcription && date1.compareTo(currentDate) >= 0) {
								// validate date is greater then current date
								orderLink = this.generateLinkActiveCustomerFeedback(merchantId);
								merchantKritiq = new MerchantKritiq();
								merchant.setActiveCustomerFeedback(1);
								merchantKritiq.setMerchant(merchant);
								merchantKritiq.setCreateDate(DateUtil.getYYYYMMDD(currentDate));
								
								merchantKritiq.setUpdateDate(DateUtil.getYYYYMMDD(currentDate));
								
								Calendar cal = Calendar.getInstance();
								cal.setTime(currentDate);
								cal.add(Calendar.DATE, 30);
								currentDate = cal.getTime();
								merchantKritiq.setValidityDate(kritiqs.get(kritiqs.size() - 1).getValidityDate());
								
								merchantKritiq.setActive(true);
								merchantKritiq.setSubscrptionType("free");
								
								merchantRepository.save(merchant);
								LOGGER.info("  MerchantServiceImpl : getMerchantSubscription  : save merchant ");
								
								merchantKritiqRepository.save(merchantKritiq);
								LOGGER.info("  MerchantServiceImpl : getMerchantSubscription  : save merchantKritiq ");
								
							} else {
								
								orderLink = "Please upgrade your subscription to use Kritiq functionalities";
								LOGGER.info("End :: MerchantServiceImpl : getMerchantSubscription  :  Return:"+orderLink);
								
								return orderLink;
							}
							
						} catch (Exception e) {
							// TODO Auto-generated catch block
							LOGGER.error("error: " + e.getMessage());
							LOGGER.error("End :: MerchantServiceImpl : getMerchantSubscription  :  Exception:"+e);
							
						}
						
					} else {
						
						// NEW PAID MERCHANT
						LOGGER.info(": MerchantServiceImpl : getMerchantSubscription  :  NEW PAID MERCHANT");
						
						merchantKritiq = new MerchantKritiq();
						orderLink = this
								.generateLinkActiveCustomerFeedback(merchantId);
						merchant.setActiveCustomerFeedback(1);
						merchantKritiq.setMerchant(merchant);
						merchantKritiq.setCreateDate(DateUtil
															 .getYYYYMMDD(currentDate));
						
						merchantKritiq.setUpdateDate(DateUtil
															 .getYYYYMMDD(currentDate));
						
						Calendar cal = Calendar.getInstance();
						cal.setTime(currentDate);
						cal.add(Calendar.DATE, 30);
						currentDate = cal.getTime();
						
						// Date currentDateFormated=
						// format.parse(currentDate.toString());
						merchantKritiq.setValidityDate(DateUtil
															   .getYYYYMMDD(currentDate));
						
						merchantKritiq.setActive(true);
						merchantKritiq.setSubscrptionType("free");
						
						merchantRepository.save(merchant);
						LOGGER.info(": MerchantServiceImpl : getMerchantSubscription  :  Save MERCHANT");
						
						merchantKritiqRepository.save(merchantKritiq);
						LOGGER.info(": MerchantServiceImpl : getMerchantSubscription  :  Save merchantKritiq");
						
					}
					
				}
			} else {
				// for NO CLICK
				LOGGER.info(": MerchantServiceImpl : getMerchantSubscription  : for NO CLICK ");
				
				if (!kritiqs.isEmpty()) {
					for (MerchantKritiq kritiqs2 : kritiqs) {
						if (kritiqs2.getActive()) {
							kritiqs2.setActive(false);
							kritiqs2.setUpdateDate(DateUtil
														   .getYYYYMMDD(currentDate));
							merchantKritiqRepository.save(kritiqs2);
						}
					}
					
					merchant.setActiveCustomerFeedback(0);
					merchantRepository.save(merchant);
					LOGGER.info(": MerchantServiceImpl : getMerchantSubscription  : save merchant ");
					
				}
			}
			
		}
		LOGGER.info(" End : MerchantServiceImpl : getMerchantSubscription  : ");
		
		return orderLink;
	}
	
	public List<TaxRates> findAllTaxRatesByMerchantId(Integer merchantId) {
		LOGGER.info(" Start : MerchantServiceImpl : findAllTaxRatesByMerchantId  : ");
		LOGGER.info(" MerchantServiceImpl : findAllTaxRatesByMerchantId  :merchantId:"+merchantId);
		LOGGER.info(" End : MerchantServiceImpl : findAllTaxRatesByMerchantId  : ");
		
		return taxRateRepository.findByMerchantIdAndNameNot(merchantId,
															"NO_TAX_APPLIED");
	}
	
	public String addDefaultTaxByMerchantId(Integer merchantId) {
		LOGGER.info(" Start : MerchantServiceImpl : addDefaultTaxByMerchantId  : ");
		
		String response = "failed";
		try {
			LOGGER.info(" MerchantServiceImpl : addDefaultTaxByMerchantId  : merchantId:"+merchantId);
			
			Merchant merchant = merchantRepository.findOne(merchantId);
			if (merchant != null) {
				List<Item> items = itemmRepository.findByMerchantId(merchantId);
				TaxRates rate = new TaxRates();
				rate.setIsDefault(IConstant.BOOLEAN_TRUE);
				rate.setMerchant(merchant);
				rate.setName("Default_Sales_Tax");
				rate.setRate(0.0);
				taxRateRepository.save(rate);
				LOGGER.info(" MerchantServiceImpl : addDefaultTaxByMerchantId  : save TaxRates :");
				
				for (Item item : items) {
					if (item.getIsDefaultTaxRates() == true) {
						
						ItemTax itemTax = new ItemTax();
						itemTax.setItem(item);
						itemTax.setTaxRates(rate);
						itemTaxRepository.save(itemTax);
						LOGGER.info(" MerchantServiceImpl : addDefaultTaxByMerchantId  : save ItemTax :");
						
					}
					
				}
				response = "success";
			}
		} catch (Exception e) {
			response = "failed";
			LOGGER.error("End::MerchantServiceImpl : addDefaultTaxByMerchantId  : Exception  "+e);
			
		}
		LOGGER.info("End::MerchantServiceImpl : addDefaultTaxByMerchantId  : Return  "+response);
		
		return response;
	}
	
	public Vendor findVendorById(Integer intVendorId) {
		LOGGER.info("Start ::MerchantServiceImpl : findVendorById ");
		LOGGER.info("MerchantServiceImpl : findVendorById : intVendorId: "+intVendorId);
		
		Vendor vendor = new Vendor();
		if (intVendorId != null) {
			vendor = vendorRepository.findOne(intVendorId);
		} else {
			vendor = null;
		}
		LOGGER.info("End ::MerchantServiceImpl : findVendorById ");
		
		return vendor;
	}
	
	public void updateMerchantUid(Merchant result, String locationUid) {
		LOGGER.info("Start ::MerchantServiceImpl : updateMerchantUid ");
		LOGGER.info(" MerchantServiceImpl : updateMerchantUid :locationUid:"+locationUid);
		
		result.setMerchantUid(locationUid);
		LOGGER.info("End :: MerchantServiceImpl : updateMerchantUid ");
		merchantRepository.save(result);
	}
	
	public Merchant findByMerchantUid(String merchantUid) {
		LOGGER.info("Start ::MerchantServiceImpl : findByMerchantUid ");
		LOGGER.info("MerchantServiceImpl : findByMerchantUid:merchantUid:"+merchantUid);
		LOGGER.info("End ::MerchantServiceImpl : findByMerchantUid ");
		
		return merchantRepository.findByMerchantUid(merchantUid);
	}
	
	public String createMerchantViaClientModuleLocation(LocationDto location) {
		LOGGER.info("Start ::MerchantServiceImpl : createMerchantViaClientModuleLocation ");
		LOGGER.info("MerchantServiceImpl : createMerchantViaClientModuleLocation :: createMerchantViaClientModuleLocation ");
		
		System.out.println("createMerchantViaClientModuleLocation::");
		String status = "yes";
		String result = null;
		Merchant merchant = new Merchant();
		merchant.setAllowAuxTax(0);
		merchant.setAllowDeliveryTiming(0);
		merchant.setAllowFutureOrder(0);
		merchant.setAllowMultiPay(false);
		merchant.setAllowMultipleKoupon(0);
		merchant.setAllowReOrder(false);
		merchant.setActiveCustomerFeedback(0);
		if(location!=null)
		{
			System.out.println("createMerchantViaClientModuleLocation:: location not null");
			LOGGER.info("MerchantServiceImpl : createMerchantViaClientModuleLocation :: createMerchantViaClientModuleLocation:: location not null");
			
			merchant.setName(location.getLocationName());
			merchant.setAddress(location.getAddress1());
			merchant.setMerchantUid(location.getLocationUid());
			merchant.setPhoneNumber(location.getPhone());
			merchant.setMerchantLogo(location.getLocationLogo());
			Merchant response = merchantRepository.save(merchant);
			LOGGER.info("MerchantServiceImpl : createMerchantViaClientModuleLocation : merchant save");
			
			if(response!=null)
			{
				System.out.println("createMerchantViaClientModuleLocation:: response not null");
				LOGGER.info("MerchantServiceImpl : createMerchantViaClientModuleLocation : createMerchantViaClientModuleLocation:: response not null");
				
				result =getMerchantSubscription(response.getId(), status);
			}
			System.out.println("result url:: "+result);
			LOGGER.info("MerchantServiceImpl : createMerchantViaClientModuleLocation : result url:: "+result);
			
		}
		LOGGER.info(" End :: MerchantServiceImpl : createMerchantViaClientModuleLocation : ");
		
		return result;
	}
	
	public Boolean checkAllowMultiPay(Merchant merchantCheck, String posMerchantId, String accessToken , Boolean allowMultiPay) throws IOException {
		LOGGER.info(" Start :: MerchantServiceImpl : checkAllowMultiPay : ");
		LOGGER.info(" MerchantServiceImpl : checkAllowMultiPay :posMerchantId:"+posMerchantId+":accessToken:"+accessToken+":allowMultiPay:"+allowMultiPay);
		
		Boolean status = false;
		if(posMerchantId!= null && accessToken!= null){
			
			//		String url = "https://sandbox.dev.clover.com/v3/merchants/"+posMerchantId+"/gateway?access_token="+accessToken;
			String url = environment.getProperty("CLOVER_MAIN_URL")+"/v3/merchants/"+posMerchantId+"/gateway?access_token="+accessToken;
			HttpClient client = HttpClientBuilder.create().build();
			HttpGet httpRequest = new HttpGet(url);
			
			HttpResponse httpResponse = client.execute(httpRequest);
			BufferedReader rd = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent()));
			StringBuffer result = new StringBuffer();
			String line = "";
			while ((line = rd.readLine()) != null) {
				result.append(line);
			}
			String tokenString = result.toString();
			
			JSONObject output = new JSONObject(tokenString);
			LOGGER.info("MerchantServiceImpl : checkAllowMultiPay : output:"+output);
			
			System.out.println(output);
			if(output.has("authorizationFrontEnd") && output.has("tokenType")){
				
				String authorizationFrontEnd = output.getString("authorizationFrontEnd");
				String tokenType = output.getString("tokenType");
				System.out.println("outhToken------>  " + tokenType);
				LOGGER.info("MerchantServiceImpl : checkAllowMultiPay :outhToken------>  " + tokenType);
				
				if(!authorizationFrontEnd.equals("BLACKHOLE")&& !tokenType.equals("0001")){
					if(allowMultiPay!=null&&!allowMultiPay){
						merchantCheck.setAllowMultiPay(true);
						merchantRepository.save(merchantCheck);
						LOGGER.info("MerchantServiceImpl : checkAllowMultiPay : save merchant:");
						
					}
					
					EncryptedKey encryptedKey = merchantEncryptedKeyRepository.findByMerchantId(merchantCheck.getId());
					if(encryptedKey== null){
						encryptedKey = new EncryptedKey();
						encryptedKey.setKey(EncryptionDecryptionToken.randomString(32));
						encryptedKey.setMerchant(merchantCheck);
						merchantEncryptedKeyRepository.save(encryptedKey);
						LOGGER.info("MerchantServiceImpl : checkAllowMultiPay : save encryptedKey:"+encryptedKey);
						
					}
					
					status= true;
				}else{
					if(allowMultiPay!=null && allowMultiPay){
						merchantCheck.setAllowMultiPay(false);
						merchantRepository.save(merchantCheck);
						LOGGER.info("MerchantServiceImpl : checkAllowMultiPay : save merchantCheck:");
						
					}
					
				}
				
			}
			
			
			
		}
		
		LOGGER.info(" End :: MerchantServiceImpl : checkAllowMultiPay : return :"+status);
		
		return status;
	}
	
	public void savePaymentGateWay(PaymentGateWay paymentGateWay) {
		LOGGER.info(" Start :: MerchantServiceImpl : savePaymentGateWay");
		
		try {
			paymentGateWayRepository.save(paymentGateWay);
			LOGGER.info("  MerchantServiceImpl : savePaymentGateWay :save paymentGateWay ");
			
		} catch (Exception e) {
			if (e != null) {
				LOGGER.error("  MerchantServiceImpl : savePaymentGateWay : Exception:"+e);
				
				MailSendUtil.sendExceptionByMail(e,environment);
			}
			LOGGER.error("  MerchantServiceImpl : savePaymentGateWay : Exception:"+e);
			
			LOGGER.error("error: " + e.getMessage());
		}
		
	}
	public Merchant saveMerchantLogin(String merchantDetails, String merchantUid,String merchantId) {
		LOGGER.info("Start:: MerchantServiceImpl : saveMerchantLogin");
		LOGGER.info("MerchantServiceImpl : saveMerchantLogin :merchantDetails:"+merchantDetails+":merchantUid:"+merchantUid+":merchantId:"+merchantId);
		
		/*
		 * String billingInfo = CloverUrlUtil.getMerchantBilling(clover); String merchantDetails =
		 * CloverUrlUtil.getMerchantDetails(clover);
		 *
		 * MerchantSubscription merchantSubscription = JsonUtil.setSubscription(billingInfo);
		 */
		
		// Subscription subscription = JsonUtil.setSubscription(billingInfo);
		/*
		 * Subscription subscription=null; if (merchantSubscription != null) { subscription =
		 * merchantSubscription.getSubscription(); if(subscription!=null) subscription =
		 * subscriptionRepository.findBySubscriptionPlanId(subscription.getSubscriptionPlanId());
		 * System.out.println(subscription.getSubscriptionPlanId()); }
		 */
		LOGGER.info("MerchantServiceImpl : Inside saveMerchantLogin :: Start");
		Merchant newMerchant = null;
		Customer customer = new Customer();
		if(merchantId!=null && !merchantId.equals("") && !merchantId.isEmpty()){
			newMerchant = merchantRepository.findByPosMerchantId(merchantId);
		}
		if(newMerchant == null){
			
			newMerchant = merchantRepository.findByMerchantUid(merchantUid);
		}
		if (newMerchant == null) {
			newMerchant = new Merchant();
			newMerchant.setAllowAuxTax(0);
			newMerchant.setAllowDeliveryTiming(0);
			newMerchant.setAllowFutureOrder(0);
			newMerchant.setAllowMultiPay(false);
			newMerchant.setAllowMultipleKoupon(0);
			newMerchant.setAllowReOrder(false);
			newMerchant.setActiveCustomerFeedback(0);
			
			
		}
		newMerchant.setPosMerchantId(merchantId);
		newMerchant.setIsInstall(IConstant.BOOLEAN_FALSE);
		newMerchant.setUpdatedDate(DateUtil.findCurrentDate());
		newMerchant.setCreatedDate(DateUtil.findCurrentDate());
		/*
		 * deleteCloverEmployees(clover); String employeePosId= CloverUrlUtil.createEmployeeOnClover(clover);
		 * if(employeePosId!=null) newMerchant.setEmployeePosId(employeePosId);
		 */
		
		newMerchant = JsonUtil.setNonPosMerchant(merchantDetails, newMerchant);
		if(newMerchant!=null){
			Vendor vendor = vendorRepository.findByEmail(newMerchant.getOwner().getEmail());
			Pos pos = null;
			if (vendor == null) {
				pos = new Pos();
				if(merchantId!=null && !merchantId.equals("") && !merchantId.isEmpty()){
					pos.setPosId(IConstant.POS_FOODTRONIX);
				}else{
					pos.setPosId(IConstant.NON_POS);
				}
				
				newMerchant.getOwner().setPos(pos);
				LOGGER.info("MerchantServiceImpl : Inside saveMerchantLogin :: if inside clverserviceImpl---");
				vendorRepository.save(newMerchant.getOwner());
			} else {
				LOGGER.info("MerchantServiceImpl : Inside saveMerchantLogin :: else insde clverserviceImpl---"+ vendor);
				newMerchant.setOwner(vendor);
			}
			/*
			 * newMerchant.setAccessToken(clover.getAuthToken()); newMerchant.setSubscription(subscription);
			 */
			newMerchant.setUpdatedDate(DateUtil.findCurrentDate());
			newMerchant = merchantRepository.save(newMerchant);
			LOGGER.info("MerchantServiceImpl : saveMerchantLogin :save merchant");
			
			if(newMerchant.getPosMerchantId() == null){
				newMerchant.setPosMerchantId(""+newMerchant.getId());
				newMerchant = merchantRepository.save(newMerchant);
			}
			DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			Date date = (newMerchant != null && newMerchant.getTimeZone() != null
					&& newMerchant.getTimeZone().getTimeZoneCode() != null)
					? DateUtil.getCurrentDateForTimeZonee(newMerchant.getTimeZone().getTimeZoneCode())
					: new Date();
			
			try {
				JSONObject jObject = new JSONObject(merchantDetails);
				JSONObject owner = null;
				String firstName = "";
				String lastName = "";
				String email = "";
				String pass = "";
				int password = ThreadLocalRandom.current().nextInt();
				if(jObject.has("contactInformationDetails")){
					owner = jObject.getJSONObject("contactInformationDetails");
					firstName = owner.getString("firstName");
					lastName = owner.getString("lastName");
					email = owner.getString("email");
					customer.setFirstName(owner.getString("firstName"));
					customer.setLastName(owner.getString("lastName"));
					customer.setEmailId(owner.getString("email"));
					customer.setPassword(EncryptionDecryptionUtil
												 .encryptString(String.valueOf(password)));
					customer.setCustomerType("location");
					customer.setMerchantt(newMerchant);
					customer.setVendor(newMerchant.getOwner());
					customer.setCreatedDate(dateFormat.format(date));
				}
				
				if (customer != null) {
					if(customer.getEmailId()!=null)
					{
						List<Customer>customerList=customerrRepository.findByEmailIdAndVendorId(customer.getEmailId(), newMerchant.getOwner().getId());
						if(customerList!=null && customerList.size()>0)
						{
							customer=customerList.get(0);
							customer.setCustomerType("admin");
							customer.setPassword(EncryptionDecryptionUtil
														 .encryptString(String.valueOf("123456789")));
							password=123456789;
						}
						
					}
					customerrRepository.save(customer);
				}
				
				pass = (String.valueOf(password));
				//MailSendUtil.customerLoginDetail(firstName,lastName,email,pass ,environment);
				//TODO Sumit to fix above compilation error
				MailSendUtil.customerLoginDetail(firstName,lastName,email,pass);
				
			} catch (Exception e) {
				LOGGER.error("MerchantServiceImpl : Inside saveMerchantLogin :: Exception : " + e);
				return null;
			}
			// String addressJs+on = CloverUrlUtil.getMerchantAddress(clover);
			List<Address> addresses = addressRepository.findByMerchantId(newMerchant.getId());
			Address address = null;
			if (addresses.isEmpty()) {
				address = new Address();
				
			} else {
				address = addresses.get(IConstant.BOOLEAN_FALSE);
			}
			address = JsonUtil.setNonPosAddress(merchantDetails, newMerchant, address);
			if (addresses == null || addresses.isEmpty() || addresses.size() == 0) {
				addresses = new ArrayList<Address>();
				addresses.add(address);
			}
			
			/*
			 * if (addressJson.contains("phoneNumber")) merchantRepository.save(address.getMerchant());
			 */
			
			addressRepository.save(address);
			LOGGER.info("MerchantServiceImpl : saveMerchantLogin :save address");
			
			if (address != null && address.getZip() != null) {
				String timeZoneCode = DateUtil.getTimeZone(address,environment);
				if (timeZoneCode != null) {
					TimeZone timeZone = timeZoneRepository.findByTimeZoneCode(timeZoneCode);
					newMerchant.setTimeZone(timeZone);
					merchantRepository.save(newMerchant);
					LOGGER.info("MerchantServiceImpl : saveMerchantLogin :save new merchant");
					
				}
			}
			
			// Save merchant subscription
			
			/*
			 * Integer merchantSubscriptionId = merchantSubscriptionRepository.findByMerchantId(newMerchant.getId()); if
			 * (merchantSubscriptionId != null) { merchantSubscription =
			 * merchantSubscriptionRepository.findOne(merchantSubscriptionId); } else {
			 *
			 * merchantSubscription.setMerchant(newMerchant); } merchantSubscription.setSubscription(subscription);
			 * merchantSubscriptionRepository.save(merchantSubscription); List<MerchantSubscription> merchantSubscriptions =
			 * new ArrayList<MerchantSubscription>(); merchantSubscriptions.add(merchantSubscription);
			 * newMerchant.setMerchantSubscriptions(merchantSubscriptions);
			 *
			 * // Save merchant orders count information MerchantOrders merchantOrders =
			 * merchantOrdersRepository.findByMerchantSubscriptionId(merchantSubscription .getId()); if (merchantOrders ==
			 * null) { merchantOrders = new MerchantOrders(); merchantOrders.setOrderCount(IConstant.BOOLEAN_FALSE);
			 * merchantOrders.setMerchantSubscription(merchantSubscription);
			 * merchantOrders.setStartDate(DateUtil.currentDate()); merchantOrders.setEndDate(DateUtil.incrementedDate());
			 * merchantOrdersRepository.save(merchantOrders); }
			 */
			newMerchant.setAddresses(addresses);
			LOGGER.info("End :: MerchantServiceImpl : saveMerchantLogin :");
			
			return newMerchant;
		}else{
			LOGGER.info("End :: MerchantServiceImpl : saveMerchantLogin : Return null");
			
			return null;
		}
		
		
	}
	
	public Merchant findByMerchantUidAndMerchantPosId(String merchantUid,String posMerchantId) {
		LOGGER.info("Start :: MerchantServiceImpl : findByMerchantUidAndMerchantPosId");
		LOGGER.info(" MerchantServiceImpl : findByMerchantUidAndMerchantPosId : merchantUid :"+merchantUid+":posMerchantId:"+posMerchantId);
		LOGGER.info("End :: MerchantServiceImpl : findByMerchantUidAndMerchantPosId");
		
		return merchantRepository.findByMerchantUidAndPosMerchantId(merchantUid, posMerchantId);
	}
	public Merchant updateMerchantDeviceDetail(Merchant result, String modileDeviceId,String modieImeiCode) {
		LOGGER.info("Start :: MerchantServiceImpl : updateMerchantDeviceDetail");
		LOGGER.info(" MerchantServiceImpl : updateMerchantDeviceDetail :modileDeviceId:"+modileDeviceId+":modieImeiCode:"+modieImeiCode);
		
		result.setModileDeviceId(modileDeviceId);
		result.setModieImeiCode(modieImeiCode);
		LOGGER.info("End :: MerchantServiceImpl : updateMerchantDeviceDetail");
		
		return merchantRepository.save(result);
	}
	
	public List<PaymentGatewayName> findAllPaymentGatewayName() {
		LOGGER.info("Start :: MerchantServiceImpl : findAllPaymentGatewayName");
		LOGGER.info("End :: MerchantServiceImpl : findAllPaymentGatewayName");
		
		return paymentGatewayNameRepository.findAll();
	}
	
	public PaymentGateWay findbyMerchantId(Integer id, Boolean isDeleted) {
		LOGGER.info("Start :: MerchantServiceImpl : findbyMerchantId");
		LOGGER.info(" MerchantServiceImpl : findbyMerchantId : merchantid :"+id);
		LOGGER.info("End :: MerchantServiceImpl : findbyMerchantId");
		
		return paymentGateWayRepository.findByMerchantIdAndIsDeleted(id, isDeleted);
	}
	
	public void saveOrderPaymentDetail(OrderPaymentDetail orderPaymentDetail) {
		LOGGER.info("Start :: MerchantServiceImpl : saveOrderPaymentDetail");
		paymentDetailRepository.save(orderPaymentDetail);
		LOGGER.info("End :: MerchantServiceImpl : saveOrderPaymentDetail");
	}
	
	public void savePaymentMode(PaymentMode mode) {
		LOGGER.info("Start :: MerchantServiceImpl : savePaymentMode");
		
		mode.setLabel("Credit Card");
		LOGGER.info("End :: MerchantServiceImpl : savePaymentMode");
		
		paymentModeRepository.save(mode);
		
	}
	
	public PaymentMode findByMerchantIdAndLabel(Integer merchantId, String label) {
		LOGGER.info("Start :: MerchantServiceImpl : findByMerchantIdAndLabel");
		LOGGER.info("MerchantServiceImpl : findByMerchantIdAndLabel :merchantId:"+merchantId+":label:"+label);
		LOGGER.info("End :: MerchantServiceImpl : findByMerchantIdAndLabel");
		
		return paymentModeRepository.findByMerchantIdAndLabel(merchantId, label);
	}
	
	public CardInfo saveCustomerCards(CardInfo cardInfo) {
		// TODO Auto-generated method stub
		LOGGER.info("Start :: MerchantServiceImpl : saveCustomerCards");
		LOGGER.info("End :: MerchantServiceImpl : saveCustomerCards");
		
		return cardInfoRepository.save(cardInfo);
	}
	
	public PaymentGateWay findPaymentGatewayDetails(Integer merchantId, Boolean isDeleted, Boolean isActive) {
		LOGGER.info("Start :: MerchantServiceImpl : findPaymentGatewayDetails");
		LOGGER.info(" MerchantServiceImpl : findPaymentGatewayDetails :merchantId:"+merchantId+":isDeleted:"+isDeleted+":isActive:"+isActive);
		LOGGER.info("End :: MerchantServiceImpl : findPaymentGatewayDetails");
		
		
		return paymentGateWayRepository.findByMerchantIdAndIsDeletedAndIsActive(merchantId,isDeleted,isActive);
		
	}
	
	public OrderPaymentDetail findPaymentDetailsByOrderId(Integer orderId) {
		LOGGER.info("Start :: MerchantServiceImpl : findPaymentDetailsByOrderId");
		LOGGER.info(" MerchantServiceImpl : findPaymentDetailsByOrderId :orderId:"+orderId);
		
		OrderPaymentDetail detail=	paymentDetailRepository.findByOrderId(orderId);
		LOGGER.info("End :: MerchantServiceImpl : findPaymentDetailsByOrderId");
		
		return detail;
	}
	
	public CardInfo findDetailsByCardId(Integer cardInfoId) {
		LOGGER.info("Start :: MerchantServiceImpl : findDetailsByCardId");
		LOGGER.info(" MerchantServiceImpl : findDetailsByCardId :cardInfoId:"+cardInfoId);
		
		CardInfo cardInfo =	cardInfoRepository.findOne(cardInfoId);
		LOGGER.info("End :: MerchantServiceImpl : findPaymentGatewayDetails");
		
		return cardInfo;
	}
	
	public Boolean updateMerchantDetail(Merchant merchant) {
		LOGGER.info("Start :: MerchantServiceImpl : updateMerchantDetail");
		
		Boolean status = false;
		if (merchant != null) {
			LOGGER.info(" MerchantServiceImpl : updateMerchantDetail : MerchantUid()"+merchant.getMerchantUid());
			
			Merchant merchantPersist = merchantRepository.findByMerchantUid(merchant.getMerchantUid());
			
			if (merchantPersist != null) {
				merchantPersist.setName(merchant.getName());
				if (merchant.getAddresses() != null && !merchant.getAddresses().isEmpty()) {
					for (Address address : merchant.getAddresses()) {
						Address fAdd = addressRepository.findOne(address.getId());
						fAdd.setAddress1(address.getAddress1());
						//fAdd.setAddress2(address.getAddress2());
						fAdd.setCity(address.getCity());
						fAdd.setZip(address.getZip());
						fAdd.setState(address.getState());
						addressRepository.save(fAdd);
					}
				}
				merchantPersist.setPhoneNumber(merchant.getPhoneNumber());
				merchantPersist.setWebsite(merchant.getWebsite());
				merchantRepository.save(merchantPersist);
				status = true;
			}
		}
		LOGGER.info(" End :: MerchantServiceImpl : updateMerchantDetail :");
		
		return status;
	}
	public List<Merchant> getWeeklyReportOfMerchant(String startDate,String endDate) {
		LOGGER.info(" Start :: MerchantServiceImpl : getWeeklyReportOfMerchant:");
		LOGGER.info(" MerchantServiceImpl : getWeeklyReportOfMerchant: startDate:"+startDate+":endDate:"+endDate);
		
		java.util.Date startDate1 = null;
		java.util.Date endDate1 = null;
		List<Merchant> merchantListObj = new ArrayList<Merchant>();
		try {
			List<Merchant> merchantList = merchantRepository.findMerchantByIsInstall();
			if (!merchantList.isEmpty()) {
				for (Merchant merchant : merchantList) {
					
					System.out.println("Enter in loop-----merchant Name--->"+merchant.getName());
					LOGGER.info(" MerchantServiceImpl : getWeeklyReportOfMerchant:Enter in loop-----merchant Name--->"+merchant.getName());
					
					//if(merchant.getId() == 321){
					if (startDate != null && endDate != null) {
						startDate1 = new SimpleDateFormat(IConstant.YYYYMMDD)
								.parse(startDate);
						SimpleDateFormat dateFormat = new SimpleDateFormat(
								"yyyy-MM-dd");
						Calendar cal = Calendar.getInstance();
						cal.setTime(dateFormat.parse(endDate));
						cal.add(Calendar.DATE, 1);
						endDate1 = new SimpleDateFormat(IConstant.YYYYMMDD)
								.parse(dateFormat.format(cal.getTime()));
					}
					//List<OrderR> orderRs = orderRepository.findByMerchantsId(startDate1, endDate1, merchant.getId());
					List<OrderR> orderRs = orderRepository.findOrderhistoryByMerchantsIdsAndIsDefault(startDate1, endDate1, merchant.getId(),1);
					if (orderRs.size() > 0) {
						
						System.out.println("Orders size for merchant------>"+orderRs.size());
						LOGGER.info(" MerchantServiceImpl : getWeeklyReportOfMerchant:Orders size for merchant------>"+orderRs.size());
						
						merchant.setTotalOrders(orderRs.size());
						Integer cashOrderCount = orderRepository
								.getCashOrderCount(startDate1, endDate1,
												   merchant.getId(), "cash");
						LOGGER.info(" MerchantServiceImpl : getWeeklyReportOfMerchant:cashOrderCount:"+cashOrderCount);
						
						if (cashOrderCount != null) {
							merchant.setCashOrder(cashOrderCount);
						} else {
							merchant.setCashOrder(0);
						}
						Integer creditCardOrderCount = orderRepository
								.getCashOrderCount(startDate1, endDate1,
												   merchant.getId(), "credit Card");
						LOGGER.info(" MerchantServiceImpl : getWeeklyReportOfMerchant: creditCardOrderCount :"+creditCardOrderCount);
						
						if (creditCardOrderCount != null) {
							merchant.setCreditOrder(creditCardOrderCount);
						} else {
							merchant.setCreditOrder(0);
						}
						Integer pickupCount = orderRepository.getPickupCountByIsDefault(
								startDate1, endDate1, merchant.getId(),1,"pickup");
						LOGGER.info(" MerchantServiceImpl : getWeeklyReportOfMerchant: pickupCount :"+pickupCount);
						
						if (pickupCount != null) {
							merchant.setPickup(pickupCount);
						} else {
							merchant.setPickup(0);
						}
						
						Integer deliveryCount = orderRepository.getPickupCountByIsDefault(
								startDate1, endDate1, merchant.getId(),1,"delivery");
						LOGGER.info(" MerchantServiceImpl : getWeeklyReportOfMerchant: deliveryCount :"+deliveryCount);
						
						if (deliveryCount != null) {
							merchant.setDelivery(deliveryCount);
						} else {
							merchant.setDelivery(0);
						}
						double totalValue = 0.0;
						for (OrderR order : orderRs) {
							Double orderPrice = order.getOrderPrice();
							if (orderPrice != null) {
								totalValue = totalValue + orderPrice;
							}
						}
						double roundOff = (double) Math.round(totalValue * 100) / 100;
						merchant.setTotalOrdersPrice(roundOff);
						LOGGER.info(" MerchantServiceImpl : getWeeklyReportOfMerchant: deliveryCount :"+deliveryCount);
						
						merchantListObj.add(merchant);
					}
				}
			}
			//}
		}
		catch (Exception e) {
			LOGGER.error("error: " + e.getMessage());
			LOGGER.error(" MerchantServiceImpl : getWeeklyReportOfMerchant: Exception :"+e);
			
		}
		
		if (!merchantListObj.isEmpty()) {
			for (Merchant merchant : merchantListObj) {
				merchant.setItems(null);
				merchant.setAddresses(null);
				merchant.setMerchantLogin(null);
				merchant.setMerchantSubscriptions(null);
				merchant.setModifierGroups(null);
				merchant.setModifiers(null);
				merchant.setOpeningClosingDays(null);
				merchant.setOrderTypes(null);
				merchant.setPaymentModes(null);
				merchant.setSocialMediaLinks(null);
				merchant.setSubscription(null);
				merchant.setTaxRates(null);
				merchant.setTimeZone(null);
				merchant.setVouchars(null);
				merchant.setOrderRs(null);
			}
		}
		LOGGER.error(" End:: MerchantServiceImpl : getWeeklyReportOfMerchant :");
		
		return merchantListObj;
	}
	
	public Merchant getWeeklyReportOfMerchant(String startDate,String endDate, int merchantId) {
		LOGGER.error(" MerchantServiceImpl : getWeeklyReportOfMerchant : start ");
		java.util.Date startDate1 = null;
		java.util.Date endDate1 = null;
		//List<Merchant> merchantListObj = new ArrayList<Merchant>();
		Merchant merchant=null;
		try {
			//List<Merchant> merchantList = merchantRepository.findMerchantByIsInstall();
			
			//for (Merchant merchant : merchantList) {
			merchant=merchantRepository.findById(merchantId);
			if (merchant!=null) {
				System.out.println("Enter in loop-----merchant Name--->"+merchant.getName());
				LOGGER.error(" MerchantServiceImpl :getWeeklyReportOfMerchant : merchant Name:  "+merchant.getName());
				
				//if(merchant.getId() == 321){
				if (startDate != null && endDate != null) {
					startDate1 = new SimpleDateFormat(IConstant.YYYYMMDD)
							.parse(startDate);
					SimpleDateFormat dateFormat = new SimpleDateFormat(
							"yyyy-MM-dd");
					Calendar cal = Calendar.getInstance();
					cal.setTime(dateFormat.parse(endDate));
					cal.add(Calendar.DATE, 1);
					endDate1 = new SimpleDateFormat(IConstant.YYYYMMDD)
							.parse(dateFormat.format(cal.getTime()));
					LOGGER.error(" MerchantServiceImpl : getWeeklyReportOfMerchant: startDate1:  "+startDate1);
					LOGGER.error(" MerchantServiceImpl : getWeeklyReportOfMerchant: endDate1:  "+endDate1);
				}
				//List<OrderR> orderRs = orderRepository.findByMerchantsId(startDate1, endDate1, merchant.getId());
				List<OrderR> orderRs = orderRepository.findOrderhistoryByMerchantsIdsAndIsDefault(startDate1, endDate1, merchant.getId(),1);
				if (orderRs.size() > 0) {
					
					System.out.println("Orders size for merchant------>"+orderRs.size());
					LOGGER.error(" MerchantServiceImpl : getWeeklyReportOfMerchant: orderRs.size():  "+orderRs.size());
					
					merchant.setTotalOrders(orderRs.size());
					Integer cashOrderCount = orderRepository.getCashOrderCount(startDate1, endDate1,merchant.getId(), "cash");
					if (cashOrderCount != null) {
						merchant.setCashOrder(cashOrderCount);
						LOGGER.error(" MerchantServiceImpl : getWeeklyReportOfMerchant: cashOrderCount:  "+cashOrderCount);
					} else {
						merchant.setCashOrder(0);
					}
					Integer creditCardOrderCount = orderRepository
							.getCashOrderCount(startDate1, endDate1,
											   merchant.getId(), "credit Card");
					
					if (creditCardOrderCount != null) {
						merchant.setCreditOrder(creditCardOrderCount);
						LOGGER.error(" MerchantServiceImpl : getWeeklyReportOfMerchant: creditCardOrderCount:  "+creditCardOrderCount);
					} else {
						merchant.setCreditOrder(0);
					}
					Integer pickupCount = orderRepository.getPickupCountByIsDefault(
							startDate1, endDate1, merchant.getId(),1,"pickup");
					if (pickupCount != null) {
						merchant.setPickup(pickupCount);
						LOGGER.error(" MerchantServiceImpl : getWeeklyReportOfMerchant: pickupCount:  "+pickupCount);
					} else {
						merchant.setPickup(0);
					}
					
					Integer deliveryCount = orderRepository.getPickupCountByIsDefault(
							startDate1, endDate1, merchant.getId(),1,"delivery");
					
					if (deliveryCount != null) {
						merchant.setDelivery(deliveryCount);
						LOGGER.error(" MerchantServiceImpl : getWeeklyReportOfMerchant: deliveryCount:  "+deliveryCount);
					} else {
						merchant.setDelivery(0);
					}
					double totalValue = 0.0;
					for (OrderR order : orderRs) {
						Double orderPrice = order.getOrderPrice();
						if (orderPrice != null) {
							totalValue = totalValue + orderPrice;
							LOGGER.error(" MerchantServiceImpl : getWeeklyReportOfMerchant: totalValue:  "+totalValue);
						}
					}
					double roundOff = (double) Math.round(totalValue * 100) / 100;
					merchant.setTotalOrdersPrice(roundOff);
				}
				//}
			}
			//}
		}
		catch (Exception e) {
			LOGGER.error("error: " + e.getMessage());
		}
		
		if (merchant!=null) {
			//for (Merchant merchant : merchantListObj) {
			merchant.setItems(null);
			merchant.setAddresses(null);
			merchant.setMerchantLogin(null);
			merchant.setMerchantSubscriptions(null);
			merchant.setModifierGroups(null);
			merchant.setModifiers(null);
			merchant.setOpeningClosingDays(null);
			merchant.setOrderTypes(null);
			merchant.setPaymentModes(null);
			merchant.setSocialMediaLinks(null);
			merchant.setSubscription(null);
			merchant.setTaxRates(null);
			merchant.setTimeZone(null);
			merchant.setVouchars(null);
			merchant.setOrderRs(null);
			//}
		}
		LOGGER.error(" MerchantServiceImpl : getWeeklyReportOfMerchant: End:  ");
		return merchant;
	}
	
	public Merchant findByPosMerchantId(String merchantPosId) {
		
		return merchantRepository.findByPosMerchantId(merchantPosId);
		
	}
	
	public Merchant merhcnatUninstallProcess(Merchant merchant) {
		LOGGER.info(" MerchantServiceImpl : merhcnatUninstallProcess: Start");
		merchant.setIsInstall(2);
		List<PaymentMode> paymentModes=paymentModeRepository.findByMerchantId(merchant.getId());
		for (PaymentMode paymentMode : paymentModes) {
			paymentMode.setEnabled(false);
		}
		merchant.setPaymentModes(paymentModes);
		MerchantConfiguration merchantConfiguration = merchantConfigurationRepository
				.findByMerchantId(merchant.getId());
		if(merchantConfiguration!=null) {
			merchantConfiguration.setAutoAccept(false);
			merchantConfiguration.setAutoAcceptOrder(false);
			merchantConfiguration.setEnableNotification(false);
			merchantConfigurationRepository.save(merchantConfiguration);
		}
		LOGGER.info(" MerchantServiceImpl : merhcnatUninstallProcess: Start");
		return merchantRepository.save(merchant);
	}
	
	public String showVirtualFundByMerchantId(Merchant merchant) {

		LOGGER.info("===============  MerchantServiceImpl : Inside showVirtualFundByMerchantId :: Start  ============= ");
		LOGGER.info(" MerchantServiceImpl  :  merchant " + merchant.getId());
		
		String response="";
		List<VirtualFund> virtualFunddata = new ArrayList<VirtualFund>();
		if(merchant !=null) {
		List<VirtualFund> virtualFundlist = virtualFundRepository.findByMerchantId(merchant.getId());
		
		
		for(VirtualFund virtualFunds : virtualFundlist) {
			
	
			VirtualFund virtualFund2 = new VirtualFund();
				
			virtualFund2.setId(virtualFunds.getId());
				
			virtualFund2.setPartnerName(virtualFunds.getPartnerName());
			
			virtualFund2.setPartnerEmailId(virtualFunds.getPartnerEmailId());
		       
			virtualFund2.setCode(virtualFunds.getCode());
			
			virtualFund2.setPercentage(virtualFunds.getPercentage());
			
			virtualFund2.setStartDate(virtualFunds.getStartDate());
			
			virtualFund2.setEndDate(virtualFunds.getEndDate());
			
			if (virtualFunds.getStatus()) {
				virtualFund2.setShowStatus("<div><a href=javascript:void(0) class='nav-toggle' fundId="
						+ virtualFunds.getId() + " style='color: blue;'>Active</a></div>");

			} else {
				virtualFund2.setShowStatus("<div><a href=javascript:void(0) class='nav-toggle' fundId="
						+ virtualFunds.getId() + " style='color: blue;'>InActive</a></div>");
			}
			
			virtualFund2.setAction("<a href=editVirtualFund?virtualFundId=" + virtualFund2.getId()
					+ " class='edit'><i class='fa fa-pencil' aria-hidden='true'></i></a>");

		    virtualFunddata.add(virtualFund2);
			} 
		
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		if(virtualFunddata.size()>0) {
		response = gson.toJson(virtualFunddata);
		}
		
		LOGGER.info("===============  MerchantServiceImpl : Inside showVirtualFundByMerchantId :: End  ============= ");
		}
		
		return response;
	
	}
	
	public String updateVirtualFund(VirtualFund virtualFund, Merchant merchant) {
		String result = "";
		boolean status = false;
		String timeZonecode = (merchant.getTimeZone() != null && merchant.getTimeZone().getTimeZoneCode() != null)
				? merchant.getTimeZone().getTimeZoneCode()
				: "America?Chicago";
		try {
			LOGGER.info("MerchantServiceImpl in updateVirtualFund : ");
			if (virtualFund != null) {
				List<VirtualFund> virtualFundChck = virtualFundRepository.findByPartnerEmailIdAndMerchantIdAndIdNot(
						virtualFund.getPartnerEmailId(), merchant.getId(), virtualFund.getId());

				if (virtualFundChck == null || virtualFundChck.size() == 0) {

					VirtualFund checkDuplicateCode = virtualFundRepository.findByCodeAndIdNot(virtualFund.getCode(),
							virtualFund.getId());
					if (checkDuplicateCode == null)
						status = true;
					else if (checkDuplicateCode.getMerchant().getId() != merchant.getId()
							&& checkDuplicateCode.getPartnerEmailId().equalsIgnoreCase(virtualFund.getPartnerEmailId()))
						status = true;

					if (status) {
						if (DateUtil.checkDateIsAfterTodayDate(virtualFund.getStartDate(), virtualFund.getEndDate(),
								timeZonecode)) {
							VirtualFund existingVirtualFund = virtualFundRepository.findOne(virtualFund.getId());
							existingVirtualFund.setPartnerName(virtualFund.getPartnerName());
							existingVirtualFund.setPartnerEmailId(virtualFund.getPartnerEmailId());
							existingVirtualFund.setCode(virtualFund.getCode());
							existingVirtualFund.setPercentage(virtualFund.getPercentage());
							existingVirtualFund.setStartDate(virtualFund.getStartDate());
							existingVirtualFund.setEndDate(virtualFund.getEndDate());
							existingVirtualFund.setStatus(virtualFund.getStatus());

							virtualFundRepository.save(existingVirtualFund);
						} else
							result = "Plz Select a valid date for FundRaiser Code Validity";
					} else
						result = "Fundraiser Code already exists, Plz try using another code";
				} else {
					result = "Email Id already exists, Plz try using another mail";
				}
			}

		} catch (Exception e) {
			LOGGER.info("Exception in updateVirtualFund : " + e);
			result = "failed to Update Partner details";
		}
		return result;
	}
	
	public String saveVirtualFund(VirtualFund virtualFund, Merchant merchant) {
		String result = "";
		boolean status = false;
		String timeZonecode = (merchant.getTimeZone() != null&& merchant.getTimeZone().getTimeZoneCode() != null)
						? merchant.getTimeZone().getTimeZoneCode(): "America?Chicago";
		try {
        
			LOGGER.info("MerchantServiceImpl in saveVirtualFund ");
			if (virtualFund != null) {

				List<VirtualFund> virtualFundChck = virtualFundRepository
						.findByPartnerEmailIdAndMerchantId(virtualFund.getPartnerEmailId(), merchant.getId());

				if (virtualFundChck == null || virtualFundChck.size() == 0) {
					
					VirtualFund checkDuplicateCode = virtualFundRepository.findByCode(virtualFund.getCode());
					if (checkDuplicateCode == null)
						status = true;
					else if (checkDuplicateCode.getMerchant().getId() != merchant.getId()
							&& checkDuplicateCode.getPartnerEmailId().equalsIgnoreCase(virtualFund.getPartnerEmailId()))
						status = true;

					if (status) {
						if (DateUtil.checkDateIsAfterTodayDate(virtualFund.getStartDate(),virtualFund.getEndDate(), timeZonecode)) {
							VirtualFund newVirtualFund = new VirtualFund();

							newVirtualFund.setPartnerName(virtualFund.getPartnerName());
							newVirtualFund.setPartnerEmailId(virtualFund.getPartnerEmailId());
							newVirtualFund.setCode(virtualFund.getCode());
							newVirtualFund.setPercentage(virtualFund.getPercentage());
							newVirtualFund.setStartDate(virtualFund.getStartDate());
							newVirtualFund.setEndDate(virtualFund.getEndDate());
							newVirtualFund.setStatus(virtualFund.getStatus());
							newVirtualFund.setMerchant(merchant);
							virtualFund = virtualFundRepository.save(newVirtualFund);
						} else
							result = "Plz Select a valid date for FundRaiser Code Validity";
					} else
						result = "Fundraiser Code already exists, Plz try using another code";
				} else {
					result = "Email Id already exists, Plz try using another mail";
				}
			}

		} catch (Exception e) {
			LOGGER.info("Exception in saveVirtualFund : " + e);
			return "failed to Save Partner details";

		}
		return result;
	}

	public List<Merchant> getMonthlyFundRaiserReport(String startDate, String endDate) {
		LOGGER.info(" Start :: MerchantServiceImpl : getMonthlyFundRaiserReport:");
		LOGGER.info(" MerchantServiceImpl : getMonthlyFundRaiserReport: startDate:"+startDate+":endDate:"+endDate);
		
		java.util.Date startDate1 = null;
		java.util.Date endDate1 = null;
		String month = "";
		List<Merchant> merchantListObj = new ArrayList<Merchant>();
		try {
			if (startDate != null && endDate != null) {
				startDate1 = new SimpleDateFormat(IConstant.YYYYMMDD)
						.parse(startDate);
				SimpleDateFormat dateFormat = new SimpleDateFormat(
						"yyyy-MM-dd");
				Calendar cal = Calendar.getInstance();
				cal.setTime(dateFormat.parse(endDate));
				cal.add(Calendar.DATE, 1);
				endDate1 = new SimpleDateFormat(IConstant.YYYYMMDD)
						.parse(dateFormat.format(cal.getTime()));
				
				month = DateUtil.findMonthNameFromDate(startDate);
				month = month.substring(0, 1).toUpperCase() + month.substring(1);
			}
				
			List<Merchant> merchantList = merchantRepository.findMerchantHavingFundRaiserPartner();
			if (!merchantList.isEmpty()) {
				for (Merchant merchant : merchantList) {
					Merchant merchantObj = new Merchant();
					List<NotificationMethod> notificationMethodList = notificationMethodRepository.findByMerchantIdAndMethodTypeIdAndActive(merchant.getId(),1,true);
					List<VirtualFund> partnersList = virtualFundRepository.findByMerchantId(merchant.getId());
					String merchantMails = "";
					for(NotificationMethod notification : notificationMethodList) {
					merchantMails = merchantMails +","+ notification.getContact();
				    }
					for (VirtualFund partner : partnersList) {
						List<OrderR> partnersOrders = new ArrayList<OrderR>();
						List<OrderR> orderList = orderRepository.
								findOrdersWithFundToSchoolsByPartnerId(merchant.getId(), partner.getId(), startDate1, endDate1, 1);
						
						String toMails = (!merchantMails.isEmpty() && merchantMails.length()>1) 
								? merchantMails.substring(1) + ","+partner.getPartnerEmailId() : "sumit.a.gangrade@gmail.com,"+partner.getPartnerEmailId();
						LOGGER.info(" MerchantServiceImpl : getMonthlyFundRaiserReport toMails------>"+toMails);
						merchantObj.setId(merchant.getId());
						merchantObj.setName(merchant.getName());
						merchantObj.setPartnerName(partner.getPartnerName());
						merchantObj.setToMail(toMails);
						LOGGER.info(" MerchantServiceImpl : getMonthlyFundRaiserReport size for merchant------>"+orderList.size());
						if (orderList.size() > 0) {
							for(OrderR orderR : orderList) {
							OrderR order = new OrderR();
							String orderDate=DateUtil.getYYYYMMDD(orderR.getCreatedOn());
							Double fundPercent = new Double(orderR.getVirtualFund().getPercentage()); 
							Double donatedAmount = orderR.getOrderPrice()*(fundPercent/100.0);
							order.setOrderDate(orderDate);
							order.setOrderId(orderR.getId());
							order.setOrderAmount(orderR.getOrderPrice());
							order.setDonatedAmount(new DecimalFormat("##.##").format(donatedAmount));
							partnersOrders.add(order);
						}
							
						}
						
						merchantObj.setOrdersList(partnersOrders);
						
						MailSendUtil.sendMonthlyFundRaiserReport(merchantObj,month);
					}
				}
			}
			
		}
		catch (Exception e) {
			LOGGER.error("error: " + e.getMessage());
			LOGGER.error(" MerchantServiceImpl : getMonthlyFundRaiserReport: Exception :"+e);
			
		}
		
		LOGGER.error(" End:: MerchantServiceImpl : getMonthlyFundRaiserReport :");
		
		return merchantListObj;
	}
	
	public String showAllApplicationstatus(Merchant merchant) {

		LOGGER.info("===============  MerchantServiceImpl : Inside showAllApplicationstatus :: Start  ============= ");

		String response = "";

		List<ApplicationDto> applicationData = new ArrayList<ApplicationDto>();

			LOGGER.info("====== merchant : " + merchant.getId());
			String reviewResponse = ProducerUtil.getAllApplicationstatus(merchant.getMerchantUid(), environment);
			if (reviewResponse != null && !reviewResponse.isEmpty()) {
				JsonArray jsonArray = new JsonArray(reviewResponse);
				
				for (int i = 0; i < jsonArray.length(); i++) {
					com.restfb.json.JsonObject obj = jsonArray.getJsonObject(i);
					
					ApplicationDto applicationDto = new ApplicationDto();
					if (obj.has("application") && !obj.isNull("application")) {
						applicationDto.setApplication((String) obj.get("application"));
					}

					
					if (obj.has("isInstalled") && !obj.isNull("isInstalled")) {
						Boolean status = (Boolean) obj.get("isInstalled");
						String isInstalled = (status) ? "Is Installed" : "Is Not Installed";
						applicationDto.setAppStatus(
								"<div><a style='color: blue;'>" + isInstalled + "</a></div>");
					}
					applicationData.add(applicationDto);
				}

				Gson gson = new GsonBuilder().setPrettyPrinting().create();
				if (applicationData.size() > 0) {
					response = gson.toJson(applicationData);
				}

				LOGGER.info(
						"===============  MerchantServiceImpl : Inside showAllApplicationstatus :: End  ============= ");
			}
		return response;

	}
}
