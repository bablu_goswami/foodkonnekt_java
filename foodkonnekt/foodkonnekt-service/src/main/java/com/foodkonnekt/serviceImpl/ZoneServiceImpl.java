package com.foodkonnekt.serviceImpl;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.foodkonnekt.clover.vo.ZoneVo;
import com.foodkonnekt.model.Address;
import com.foodkonnekt.model.Customer;
import com.foodkonnekt.model.DeliveryOpeningClosingDay;
import com.foodkonnekt.model.DeliveryOpeningClosingTime;
import com.foodkonnekt.model.DeliveryZoneTiming;
import com.foodkonnekt.model.Merchant;
import com.foodkonnekt.model.OpeningClosingDay;
import com.foodkonnekt.model.OpeningClosingTime;
import com.foodkonnekt.model.Zone;
import com.foodkonnekt.repository.AddressRepository;
import com.foodkonnekt.repository.CustomerrRepository;
import com.foodkonnekt.repository.DeliveryOpeningClosingDayRepository;
import com.foodkonnekt.repository.DeliveryOpeningClosingTimeRepository;
import com.foodkonnekt.repository.DeliveryZoneTimingRepository;
import com.foodkonnekt.repository.MerchantRepository;
import com.foodkonnekt.repository.ZoneRepository;
import com.foodkonnekt.service.ZoneService;
import com.foodkonnekt.util.CloverUrlUtil;
import com.foodkonnekt.util.DateUtil;
import com.foodkonnekt.util.IConstant;
import com.foodkonnekt.util.MailSendUtil;
import com.foodkonnekt.util.ProducerUtil;
import com.itextpdf.text.pdf.PdfStructTreeController.returnType;

@Service
public class ZoneServiceImpl implements ZoneService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ZoneServiceImpl.class);

	@Autowired
    private Environment environment;
	
    @Autowired
    private ZoneRepository zoneRepository;

    @Autowired
    private AddressRepository addressRepository;

    @Autowired
    private CustomerrRepository customerrRepository;
    
    @Autowired
    private DeliveryZoneTimingRepository deliveryZoneTimingRepository;

    @Autowired
    private MerchantRepository merchantRepository;
    
    @Autowired
    private DeliveryOpeningClosingTimeRepository deliveryOpeningClosingTimeRepository ;
    
    @Autowired
    private DeliveryOpeningClosingDayRepository deliveryOpeningClosingDayRepository ;

    /**
     * Save zone into database
     */
    public Zone createZone(Zone zone) {
    	LOGGER.info("===============  ZoneServiceImpl : Inside createZone :: Start  ============= ");
        Date today = (zone != null && zone.getMerchant()!=null && zone.getMerchant().getTimeZone() !=null 
				&& zone.getMerchant().getTimeZone().getTimeZoneCode()!=null) ? 
						DateUtil.getCurrentDateForTimeZonee(zone.getMerchant().getTimeZone().getTimeZoneCode()) : new Date();
        zone.setCreatedOn(today);
        zone.setModifiedOn(today);
        zone = zoneRepository.save(zone);
        LOGGER.info("===============  ZoneServiceImpl : Inside createZone :: End  =============zone "+zone.getId());

        return zone;
    }

    public Zone updateZoneDetail(Zone zone) {
    	LOGGER.info("===============  ZoneServiceImpl : Inside updateZoneDetail :: Start  =============zone "+zone.getId());

        if (zone.getId() != null) {
        	Date today = (zone.getMerchant()!=null && zone.getMerchant().getTimeZone() !=null 
    				&& zone.getMerchant().getTimeZone().getTimeZoneCode()!=null) ? 
    						DateUtil.getCurrentDateForTimeZonee(zone.getMerchant().getTimeZone().getTimeZoneCode()) : new Date();
            Zone dbZone = zoneRepository.findOne(zone.getId());
            dbZone.setModifiedOn(today);
            dbZone.setZoneName(zone.getZoneName());
            dbZone = zoneRepository.save(dbZone);
            LOGGER.info("===============  ZoneServiceImpl : Inside updateZoneDetail :: End  ============= ");

            return dbZone;
        } else {
            createZone(zone);
            LOGGER.info("===============  ZoneServiceImpl : Inside updateZoneDetail :: End  ============= ");

            return zone;
        }
    }

    public String deleteZone(Integer zoneId) {
    	LOGGER.info("===============  ZoneServiceImpl : Inside deleteZone :: Start  =============zoneId "+zoneId);

        if (zoneId != null) {
            Zone zone = zoneRepository.findOne(zoneId);
            if (zone == null) {
            	LOGGER.info("===============  ZoneServiceImpl : Inside deleteZone :: End  ============= ");

                return "No Zone exist for the ID : " + zoneId;
            } else {
                try {
                    zoneRepository.delete(zoneId);
                	LOGGER.info("===============  ZoneServiceImpl : Inside deleteZone :: End  ============= ");

                    return "Zone deleted for Id: " + zoneId;
                } catch (Exception e) {
                	LOGGER.info("===============  ZoneServiceImpl : Inside deleteZone :: Exception  ============= "+e);

                    return "Exception occure while deleting the zone for ID: " + zoneId + "  is : " + e.getMessage();
                }
            }
        } else {
        	LOGGER.info("===============  ZoneServiceImpl : Inside deleteZone :: End  ============= ");

            return "Zone Id is empty";
        }

    }

    public List<Zone> getAllZone() {
        return zoneRepository.findAll();
    }

    /**
     * Check delivery zone
     */
    public String checkAddressForDeliveryZone(Address address) {
    	LOGGER.info("===============  ZoneServiceImpl : Inside checkAddressForDeliveryZone :: Start  =============addressMerchantId "+address.getMerchId());

        List<Zone> zones = zoneRepository.findByMerchantIdAndZoneStatus(address.getMerchId(), 0);
        Integer addressId = null;
        
        List<Address> addresses= addressRepository.findByMerchantId(address.getMerchId());
        if(addresses!=null && !addresses.isEmpty()){
        	for (Address address2 : addresses) {
        		if(address2.getId()!=null){
        			addressId = address2.getId();
        		}
			}
        }
        String status = null;
        try {
            double distance;
            for (int i = 0; i < zones.size(); i++) {
                if(!zones.isEmpty()&& zones.get(i).getZoneDistance()!=null)  {
                    Collections.sort(zones, new Comparator<Zone>() {
                        public int compare(Zone z1, Zone z2) {
                            return z1.getZoneDistance().compareTo(z2.getZoneDistance());
                        }
                    });
                }  
            }
     
            final double MILES_PER_KILOMETER = 0.621;
            for (Zone zone : zones) {
                distance = checkDelivery(address, zone.getAddress());
                LOGGER.info("===distance ===="+distance);
                double miles = distance * MILES_PER_KILOMETER;
                LOGGER.info("===miles ===="+miles);

                if (zone.getZoneDistance() !=  null) {
                    if (miles <= zone.getZoneDistance()) {
                        status = zone.getDeliveryLineItemPosId();
                        return status + "#" + zone.getDeliveryFee() + "#" + zone.getIsDeliveryZoneTaxable() + "#"
                                        + zone.getMinDollarAmount() + "#" + zone.getAvgDeliveryTime() + "#"
                                        + zone.getId().toString() + "#" + addressId.toString();
                    }
                }
                
                if(zone.getZoneType()!=null && zone.getZoneType().equalsIgnoreCase("map")) {
                    return status + "#" + zone.getDeliveryFee() + "#" + zone.getIsDeliveryZoneTaxable() + "#"
                                    + zone.getMinDollarAmount() + "#" + zone.getAvgDeliveryTime() + "#"
                                    + zone.getId().toString() + "#" + addressId.toString();
                }
                
            }
        } catch (Exception e) {
            if (e != null) {
                MailSendUtil.sendExceptionByMail(e,environment);
            }
            LOGGER.error("===============  ZoneServiceImpl : Inside checkAddressForDeliveryZone :: Exception  ============= " + e);

            LOGGER.error("error: " + e.getMessage());
        }
        LOGGER.info("===status ===="+status);

        LOGGER.info("===============  ZoneServiceImpl : Inside checkAddressForDeliveryZone :: End  ==== ");

        return status;
    }
    
    public List<DeliveryOpeningClosingTime> findDeliveryOpeningClosingHoursByMerchantId(Integer merchantId,String timeZoneCode) {
        /*Calendar calendar = Calendar.getInstance();
        calendar.setTimeZone(value);
        Date date = calendar.getTime();
        String toDay = new SimpleDateFormat("EEEE", Locale.ENGLISH).format(date.getTime()).toLowerCase();*/
    	LOGGER.info("===============  ZoneServiceImpl : Inside findDeliveryOpeningClosingHoursByMerchantId :: Start  ============= ");
       LOGGER.info(" merchantId "+merchantId+" timeZoneCode "+timeZoneCode);
    	Date date = new Date();
    	DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

    	// Use Madrid's time zone to format the date in
    	df.setTimeZone(TimeZone.getTimeZone(timeZoneCode));

    	String toDay=DateUtil.findDayNameFromDate( df.format(date));
    	DeliveryOpeningClosingDay openingClosingDay = deliveryOpeningClosingDayRepository.findByDayAndMerchantId(toDay,merchantId);
        LOGGER.info("openingClosingDay ===== "+openingClosingDay.getId());
    	if (openingClosingDay != null) {
    		LOGGER.info("===============  ZoneServiceImpl : Inside findDeliveryOpeningClosingHoursByMerchantId :: End  ============= ");

            return deliveryOpeningClosingTimeRepository.findByDeliveryOpeningClosingDayId(openingClosingDay.getId());
        } else {
    		LOGGER.info("===============  ZoneServiceImpl : Inside findDeliveryOpeningClosingHoursByMerchantId :: End  ============= ");

            return null;
        }
    }
    
    public List<DeliveryOpeningClosingTime> findNextOpeningDeliveryClosingHoursByMerchantId(Integer merchantId,String timeZoneCode) {
        LOGGER.info("===============  ZoneServiceImpl : Inside findNextOpeningDeliveryClosingHoursByMerchantId :: Start  ============= ");

    	Date date = new Date();
    	DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
    	
    	Calendar c = Calendar.getInstance();
    	c.setTime(date);
    	c.add(Calendar.DATE, 1);  // number of days to add
    	date = c.getTime();
    	
    	df.setTimeZone(TimeZone.getTimeZone(timeZoneCode));

    	String toDay=DateUtil.findDayNameFromDate( df.format(date));
    	DeliveryOpeningClosingDay openingClosingDay = deliveryOpeningClosingDayRepository.findByDayAndMerchantId(toDay,merchantId);
        LOGGER.info("openingClosingDay ===== "+openingClosingDay.getId());

    	if (openingClosingDay != null) {
           LOGGER.info("===============  ZoneServiceImpl : Inside findNextOpeningDeliveryClosingHoursByMerchantId :: End  ============= ");

    		return deliveryOpeningClosingTimeRepository.findByDeliveryOpeningClosingDayId(openingClosingDay.getId());
        } else {
            LOGGER.info("===============  ZoneServiceImpl : Inside findNextOpeningDeliveryClosingHoursByMerchantId :: End  ============= ");

            return null;
        }
    }

    /**
     * Find distance between two address
     * 
     * @param address
     * @param merchantAddress
     * @return
     */
    private double checkDelivery(Address address, Address merchantAddress) {
    	LOGGER.info("===============  ZoneServiceImpl : Inside checkDelivery :: Start  ============= ");

        String add2 = null;
        if (address.getAddress2() != null) {
            add2 = address.getAddress2();
        } else {
            add2 = "";
        }
        String merchantAdd2 = null;
        if (merchantAddress.getAddress2() != null) {
            merchantAdd2 = merchantAddress.getAddress2();
        } else {
            merchantAdd2 = "";
        }
        String origin =  merchantAddress.getAddress1() + "," + merchantAdd2 + "," + merchantAddress.getCity() + ","
                + merchantAddress.getState() + "," + merchantAddress.getZip();
        String des =address.getAddress1() + "," + add2 + "," + address.getCity() + "," + address.getState() + ","
                + address.getZip();
        LOGGER.info(" original address ==== " + origin);
        LOGGER.info(" destination ==== " + des);
        HttpGet request;
        double distance = 0;
        String parameter="";
        try {
            request = new HttpGet("https://maps.googleapis.com/maps/api/distancematrix/json?origins="
                            + URLEncoder.encode(origin, "UTF-8") + "&destinations=" + URLEncoder.encode(des, "UTF-8")
                            + "&key=" + IConstant.GOOGLE_DIRECTIONS_API_KEY);

        	/*request = new HttpGet("https://maps.googleapis.com/maps/api/directions/json?origin="+URLEncoder.encode(origin, "UTF-8")+"&destination="+URLEncoder.encode(des, "UTF-8")+"&sensor=false"
        			 + "&key=" + IConstant.GOOGLE_DIRECTIONS_API_KEY);*/

            String response = convertToStringJson(request);
            if(response.contains("error")){
            	MailSendUtil.webhookMail("delivery address api", response,environment);
            }
            LOGGER.info(" response === "+response);
            if(ProducerUtil.isJSONValid(response)) {
            JSONObject jObject = new JSONObject(response);
            String distan = null;
            LOGGER.info(" status === "+jObject.getString("status"));
            if (jObject.getString("status").equals("OK")) {
                JSONArray rows = jObject.getJSONArray("rows");
                for (Object jObj : rows) {
                    JSONObject jsonObj = (JSONObject) jObj;
                    JSONArray elements = jsonObj.getJSONArray("elements");
                    for (Object jObj2 : elements) {
                        JSONObject obj = (JSONObject) jObj2;
                        if(obj!=null && obj.has("distance")){
                        JSONObject distanceObj = obj.getJSONObject("distance");
                        distan = distanceObj.getString("text");
                        String[] arry = distan.split(" ");
                        if (arry[0] != null) {
                            if (arry[0].contains(",")) {
                                distance = Double.parseDouble(arry[0].replace(",", ""));
                            } else {
                                distance = Double.parseDouble(arry[0]);
                            }
                        }
                        if(arry.length>0 && arry[1] != null)
                    	{
                    	parameter=	arry[1];
                    	}
                        }
                    }
                }
            }
        }
        } catch (Exception e) {
            if (e != null) {
                MailSendUtil.sendExceptionByMail(e,environment);
            }
            LOGGER.error("===============  ZoneServiceImpl : Inside checkDelivery :: Exception  ============= " + e);

            LOGGER.error("error: " + e.getMessage());
        }
        if(!parameter.isEmpty() && parameter.equalsIgnoreCase("m"))
        	distance=distance/1000;
        if(!parameter.isEmpty() && parameter.equalsIgnoreCase("ft"))
        	distance=distance* 0.000305;
        LOGGER.info(" distance === "+distance);
        LOGGER.info("===============  ZoneServiceImpl : Inside checkDelivery :: End  ============= ");

        return distance;
    }

    /**
     * convert http response to string
     * 
     * @param request
     * @return
     */
    public String convertToStringJson(HttpGet request) {
        HttpClient client = HttpClientBuilder.create().build();
        HttpResponse response = null;
        StringBuilder responseBuilder = new StringBuilder();
        try {
            response = client.execute(request);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            String line = "";
            while ((line = rd.readLine()) != null) {
                responseBuilder.append(line);
            }
        } catch (Exception e) {
            if (e != null) {
                MailSendUtil.sendExceptionByMail(e,environment);
            }
            LOGGER.error("error: " + e.getMessage());
        }
        return responseBuilder.toString();
    }

    /**
     * Save zone
     */
    public void save(Zone zone, Merchant merchant) {
        try {
        	LOGGER.info("===============  ZoneServiceImpl : Inside save :: Start  ============= ");
        	LOGGER.info(" merchant === "+merchant.getId());
        	LOGGER.info(" zone === "+zone.getId());

            List<Address> addresses = addressRepository.findByMerchantId(merchant.getId());
            if (zone != null) {
            	LOGGER.info(" inside if (zone != null) ");
            	if(merchant.getOwner()!=null && merchant.getOwner().getPos()!=null && merchant.getOwner().getPos().getPosId()!=null && merchant.getOwner().getPos().getPosId()==1){
                if (zone.getId() == null) {
                    String result = CloverUrlUtil.addDeliveryFeeLineItem(zone.getDeliveryFee(),
                                    merchant.getPosMerchantId(), merchant.getAccessToken(), "Delivery Fee",
                                    zone.getIsDeliveryZoneTaxable(),environment);
                    if (result != null) {
                        JSONObject jObject = new JSONObject(result);
                        if (jObject.toString().contains("id")) {
                            zone.setDeliveryLineItemPosId(jObject.getString("id"));
                        }
                    }
                } else {
                    String result = CloverUrlUtil.updateDeliveryFeeLineItem(zone.getDeliveryFee(),
                                    merchant.getPosMerchantId(), merchant.getAccessToken(), "Delivery Fee",
                                    zone.getDeliveryLineItemPosId(), zone.getIsDeliveryZoneTaxable(),environment);
                }
            	}
            	zone.setMerchant(merchant);
            }
            
            if (addresses != null) {
                Address address = addresses.get(0);
                zone.setAddress(address);
            }
            zoneRepository.save(zone);
        } catch (Exception e) {
            if (e != null) {
                MailSendUtil.sendExceptionByMail(e,environment);
            }
            LOGGER.error("===============  ZoneServiceImpl : Inside save :: Exception  ============= " + e);

            LOGGER.error("error: " + e.getMessage());
        }
    }

    /**
     * Find zone by merchantId
     */
    public List<Zone> findZoneByMerchantId(Integer merchantId) {
     	LOGGER.info("----------------Start :: ZoneServiceImpl : findZoneByMerchantId------------------------");
     	LOGGER.info("ZoneServiceImpl :: findZoneByMerchantId : merchantId "+merchantId);
     	LOGGER.info("----------------End :: ZoneServiceImpl : findZoneByMerchantId------------------------");
        return zoneRepository.findByMerchantId(merchantId);
    }

    /**
     * Find by delivery zone
     */
    public Zone findById(int deliveryZoneId) {
    	LOGGER.info("===============  ZoneServiceImpl : Inside findById :: Start  ============= ");
LOGGER.info(" deliveryZoneId === "+deliveryZoneId);
        return zoneRepository.findOne(deliveryZoneId);
    }

    public String findByMerchantIdAndDeliveryZoneName(Integer merchantId, String deliveryZoneName) {
    	LOGGER.info("===============  ZoneServiceImpl : Inside findByMerchantIdAndDeliveryZoneName :: Start  ============= ");
LOGGER.info(" merchantId  === "+merchantId);
LOGGER.info(" deliveryZoneName  === "+deliveryZoneName );
        Zone zone = zoneRepository.findByMerchantIdAndZoneName(merchantId, deliveryZoneName);
        if (zone != null) {
        	LOGGER.info("===============  ZoneServiceImpl : Inside findByMerchantIdAndDeliveryZoneName :: End  ============= ");

            return "true";
        } else {
        	LOGGER.info("===============  ZoneServiceImpl : Inside findByMerchantIdAndDeliveryZoneName :: End  ============= ");

            return "false";
        }

    }

    /**
     * Save valid address into database
     */
    public void saveAddress(Address address) {
    	LOGGER.info("===============  ZoneServiceImpl : Inside saveAddress :: Start  ============= ");
        if(address != null) {
        if (address.getCustomer() != null) {
        	LOGGER.info("address.getCustomer().getId() === "+address.getCustomer().getId());
            Customer customer = customerrRepository.findOne(address.getCustomer().getId());
            address.setCustomer(customer);
            if(address.getCountry()==null || address.getCountry()==""){
            	address.setCountry("");
            }
            addressRepository.save(address);
        }
        }
        LOGGER.info("===============  ZoneServiceImpl : Inside saveAddress :: End  ============= ");

    }

    public List<Zone> findZoneByMerchantIdAndStatus(Integer merchantId) {
    	LOGGER.info("----------------Start :: ZoneServiceImpl : findZoneByMerchantIdAndStatus------------------------");
    	LOGGER.info("ZoneServiceImpl : findZoneByMerchantIdAndStatus : merchantId "+merchantId);
    	LOGGER.info("----------------End :: ZoneServiceImpl : findZoneByMerchantIdAndStatus------------------------");
        return zoneRepository.findByMerchantIdAndZoneStatus(merchantId, 0);
    }

    public String findByMerchantIdAndDeliveryZoneNameAndZoneId(Integer merchantId, String deliveryZoneName,
                    Integer zoneId) {
    	LOGGER.info("===============  ZoneServiceImpl : Inside findByMerchantIdAndDeliveryZoneNameAndZoneId :: Start  ============= ");
LOGGER.info(" merchantId === "+merchantId+" deliveryZoneName === "+deliveryZoneName+" zoneId == "+zoneId);
        Zone zone = zoneRepository.findByMerchantIdAndZoneName(merchantId, deliveryZoneName);
        if (zone != null) {
            if (zone.getId() != zoneId) {
            	LOGGER.info("===============  ZoneServiceImpl : Inside findByMerchantIdAndDeliveryZoneNameAndZoneId :: End  ============= ");

                return "true";}
            else {
            	LOGGER.info("===============  ZoneServiceImpl : Inside findByMerchantIdAndDeliveryZoneNameAndZoneId :: End  ============= ");

                return "false";
        } }else {
        	LOGGER.info("===============  ZoneServiceImpl : Inside findByMerchantIdAndDeliveryZoneNameAndZoneId :: End  ============= ");

            return "false";
        }

    }

    /**
     * Find Address by addressId
     */
    public Address findAddressById(Integer addressId) {
    	
    	LOGGER.info("----------------Start :: ZoneServiceImpl : findAddressById------------------------");
    	LOGGER.info("ZoneServiceImpl :: findAddressById : addressId "+addressId);
        Address address = addressRepository.findOne(addressId);
        address.setCustomer(null);
        address.setMerchant(null);
        address.setZones(null);
    	LOGGER.info("----------------Start :: ZoneServiceImpl : findAddressById------------------------");
        return address;
    }

    public double checkMinDeliveryAmount(Double subTotalAmount, Integer merchantId) {
        LOGGER.info("===============  ZoneServiceImpl : Inside checkMinDeliveryAmount :: Start  ============= ");
        LOGGER.info(" merchantId === "+merchantId);
        LOGGER.info(" subTotalAmount === "+subTotalAmount);
    	List<Zone> zones = zoneRepository.findByMerchantIdAndZoneStatus(merchantId, 0);
        Zone minAmountZone;
        if (!zones.isEmpty()) {
            if (zones.size() >= 2) {
                minAmountZone = Collections.min(zones, new ZoneVo());
            } else {
                minAmountZone = zones.get(IConstant.BOOLEAN_FALSE);
            }
            int retval = Double.compare(Double.parseDouble(minAmountZone.getMinDollarAmount()), subTotalAmount);
            if (retval > 0) {
            	LOGGER.info("===============  ZoneServiceImpl : Inside checkMinDeliveryAmount :: End  ============= ");

                return Double.parseDouble(minAmountZone.getMinDollarAmount());
            } else if (retval < 0) {
            	LOGGER.info("===============  ZoneServiceImpl : Inside checkMinDeliveryAmount :: End  ============= ");

            	return 0;
            } else {
            	LOGGER.info("===============  ZoneServiceImpl : Inside checkMinDeliveryAmount :: End  ============= ");
                 return 0;
            }
        } else {
        	LOGGER.info("===============  ZoneServiceImpl : Inside checkMinDeliveryAmount :: End  ============= ");

            return 0;
        }
    }
    
    public String saveDeliveryTime(String days, String startTime, String endTime, int merchantId,Integer allowDeliveryTiming) {
		try {
			LOGGER.info("===============  ZoneServiceImpl : Inside saveDeliveryTime :: Start  ============= ");
			LOGGER.info("days === "+days+" startTime ==== "+startTime+" endTime ==== "+endTime+" merchantId === "+merchantId+" alowDeliveryTiming ===="+allowDeliveryTiming);

			// deliveryZoneTimingRepository.save(deliveryZoneTiming);
			String weekDays[] = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" };
			List<DeliveryZoneTiming> deliveryZoneTimings = deliveryZoneTimingRepository.findByMerchantId(merchantId);
			if (deliveryZoneTimings != null && deliveryZoneTimings.size() > 0) {
				deliveryZoneTimingRepository.deleteByMerchantId(merchantId);
			}
			try {
				startTime = DateUtil.convert12hoursTo24HourseFormate(startTime);
				endTime = DateUtil.convert12hoursTo24HourseFormate(endTime);
			} catch (Exception e) {
				LOGGER.error("===============  ZoneServiceImpl : Inside saveDeliveryTime :: Exception  ============= " + e);

				System.out.println(e);
			}
			//
			//Zone zone = zoneRepository.findOne(zoneId);
			Merchant merchant= merchantRepository.findById(merchantId);
			deliveryZoneTimings = new ArrayList<DeliveryZoneTiming>();
			if (days != null && !days.isEmpty() && !days.equals("")&& allowDeliveryTiming!=null && allowDeliveryTiming!=0) {
				String timingDays[] = days.split(",");
				for (String day : weekDays) {
					if (!days.contains(day)) {
						//zone.setZoneDeliveryTiming(zoneStatus);
						merchant.setAllowDeliveryTiming(allowDeliveryTiming);
						DeliveryZoneTiming deliveryTiming = new DeliveryZoneTiming();
						deliveryTiming.setDay(day);
						deliveryTiming.setStartTime("00:00:00");
						deliveryTiming.setEndTime("24:00:00");
						deliveryTiming.setMerchant(merchant);
						deliveryTiming.setHoliday(true);
						deliveryZoneTimings.add(deliveryTiming);
					} else {
						//zone.setZoneDeliveryTiming(zoneStatus);
						merchant.setAllowDeliveryTiming(allowDeliveryTiming);
						DeliveryZoneTiming deliveryZoneTimingTiming = new DeliveryZoneTiming();
						deliveryZoneTimingTiming.setDay(day);
						deliveryZoneTimingTiming.setStartTime(startTime);
						if (endTime.equals("00:00")) {
							endTime = "24:00";
						}
						deliveryZoneTimingTiming.setEndTime(endTime);
						deliveryZoneTimingTiming.setMerchant(merchant);
						deliveryZoneTimingTiming.setHoliday(false);
						deliveryZoneTimings.add(deliveryZoneTimingTiming);
					}

				}
			} else {

				for (String day : weekDays) {
					//zone.setZoneDeliveryTiming(zoneStatus);
					merchant.setAllowDeliveryTiming(allowDeliveryTiming);
					DeliveryZoneTiming zoneTiming = new DeliveryZoneTiming();
					zoneTiming.setDay(day);
					zoneTiming.setStartTime("00:00:00");
					zoneTiming.setEndTime("24:00:00");
					zoneTiming.setMerchant(merchant);
					zoneTiming.setHoliday(false);
					deliveryZoneTimings.add(zoneTiming);
				}
			}
			//zone.setZoneDeliveryTiming(zoneStatus);
			//merchant.setAllowDeliveryTiming(allowDeliveryTiming);
			merchantRepository.save(merchant);
			//zoneRepository.save(zone);
			deliveryZoneTimingRepository.save(deliveryZoneTimings);
		} catch (Exception e) {
			LOGGER.error("===============  ZoneServiceImpl : Inside saveDeliveryTime :: Exception  ============= " + e);

			LOGGER.error("error: " + e.getMessage());
		}
		LOGGER.info("===============  ZoneServiceImpl : Inside saveDeliveryTime :: End  ============= ");

		return null;
	}

	public List<DeliveryZoneTiming> getDeliveryTiming(int merchantId) {
		LOGGER.info("===============  ZoneServiceImpl : Inside getDeliveryTiming :: Start  =============merchantId "+merchantId);

		List<DeliveryZoneTiming> deliveryZoneTimings = deliveryZoneTimingRepository.findByMerchantId(merchantId);
		//Zone zone=null;
		for (DeliveryZoneTiming zoneTiming : deliveryZoneTimings) {
			zoneTiming.setMerchant(null);
			zoneTiming.setStartTime(DateUtil.get12Format(zoneTiming.getStartTime()));
			zoneTiming.setEndTime(DateUtil.get12Format(zoneTiming.getEndTime()));
		}
		LOGGER.info("===============  ZoneServiceImpl : Inside getDeliveryTiming :: End  ============= ");

		return deliveryZoneTimings;
	}
	
	public List<Zone> findZoneByDeliveryFeesAndMerchantId(Double deliveryFee, Integer id) {
    	LOGGER.info("----------------Start :: ZoneServiceImpl : findZoneByDeliveryFeesAndMerchantId------------------------");
    	LOGGER.info("----------------End :: ZoneServiceImpl : findZoneByDeliveryFeesAndMerchantId------------------------");
		return zoneRepository.findZoneByDeliveryFeeAndMerchantIdAndZoneStatus(deliveryFee,id,0);
		}

	public DeliveryZoneTiming getZoneTimingByTimeZoneCodeAndMerchantId(Integer merchantId,String timeZoneCode) {
		LOGGER.info("===============  ZoneServiceImpl : Inside getZoneTimingByTimeZoneCodeAndMerchantId :: Start  =============merchantId "+merchantId+" timeZoneCode =="+timeZoneCode);

		Date date = new Date();
    	DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

    	
    	// Use Madrid's time zone to format the date in
    	df.setTimeZone(TimeZone.getTimeZone(timeZoneCode));
    	String toDay=DateUtil.findDayNameFromDate(df.format(date));
    	boolean isHoliday=false;
    	LOGGER.info(" isHoliday === "+isHoliday+" toDay === "+toDay+" merchantId === "+merchantId);
    	DeliveryZoneTiming deliveryZoneTiming = deliveryZoneTimingRepository.findByDayAndMerchantIdAndIsHoliday(toDay,merchantId,isHoliday);
		LOGGER.info("===============  ZoneServiceImpl : Inside getZoneTimingByTimeZoneCodeAndMerchantId :: End  ============= ");

    	return deliveryZoneTiming;
	}

	public String updateDeliveryStatus(List<Zone> zones ,Integer deliveryStatus) {
		LOGGER.info("===============  ZoneServiceImpl : Inside updateDeliveryStatus :: Start  ============= deliveryStatus == "+deliveryStatus);

		String status = "false";
		if(zones!=null && !zones.isEmpty()){
			for (Zone zone : zones) {
				LOGGER.info("zone === "+zone.getId());
				Zone finalZone = zoneRepository.findById(zone.getId());
				finalZone.setZoneStatus(deliveryStatus);
				zoneRepository.save(finalZone);
			}
			status = "true";
		}
		LOGGER.info("===============  ZoneServiceImpl : Inside save :: End  =============status "+status);

		return status;
	}

	public String deleteDeliveryZone(Integer zoneId) {
		LOGGER.info("===============  ZoneServiceImpl : Inside deleteDeliveryZone :: Start  ============= ");

		LOGGER.info("zoneId === "+zoneId);
        if (zoneId != null) {
            Zone zone = zoneRepository.findOne(zoneId);
            if (zone == null) {
                return "No Zone exist for the ID : " + zoneId;
            } else {
                try {
                	zone.setIsDeleted(1);
                    zoneRepository.save(zone);
                    return "Zone deleted for Id: " + zoneId;
                } catch (Exception e) {
                	LOGGER.error("error: " + e.getMessage());
                	LOGGER.error("===============  ZoneServiceImpl : Inside deleteDeliveryZone :: Exception  ============= " + e);

                    return "Exception occure while deleting the zone for ID: " + zoneId + "  is : " + e.getMessage();
                }
            }
        } else {
        	LOGGER.info("===============  ZoneServiceImpl : Inside deleteDeliveryZone :: End  ============= ");

            return "Zone Id is empty";
        }

    }
}
