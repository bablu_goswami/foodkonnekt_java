package com.foodkonnekt.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.foodkonnekt.model.Merchant;
import com.foodkonnekt.model.MerchantSocialPlatform;
import com.foodkonnekt.repository.MerchantSocialPlateformRepository;
import com.foodkonnekt.service.MerchantSocialPlatformService;

@Service
public class MerchantSocialPlateformServiceImpl implements MerchantSocialPlatformService {
	
	@Autowired
	private MerchantSocialPlateformRepository merchantSocialPlateformRepository;

	
	public MerchantSocialPlatform findByMerchantId(Integer merchantId) {
    	MerchantSocialPlatform merchantSocialPlatform = null;
        List<MerchantSocialPlatform> data = merchantSocialPlateformRepository.findByMerchantId(merchantId);
        if(data!=null && data.size()>0)
        	merchantSocialPlatform = data.get(0);
        
        return merchantSocialPlatform;
    
	}
	public void saveAndFlush(MerchantSocialPlatform merchantSocialPlatform) {
		merchantSocialPlateformRepository.saveAndFlush(merchantSocialPlatform);
		
	}
	
}
 
	
	

