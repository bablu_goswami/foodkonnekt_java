package com.foodkonnekt.serviceImpl;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
/*import java.time.LocalDate;*/
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.joda.time.LocalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.foodkonnekt.model.Address;
import com.foodkonnekt.model.ConvenienceFee;
import com.foodkonnekt.model.DeliveryOpeningClosingDay;
import com.foodkonnekt.model.DeliveryOpeningClosingTime;
import com.foodkonnekt.model.Merchant;
import com.foodkonnekt.model.OpeningClosingDay;
import com.foodkonnekt.model.OpeningClosingTime;
import com.foodkonnekt.model.PaymentMode;
import com.foodkonnekt.model.PickUpTime;
import com.foodkonnekt.model.Zone;
import com.foodkonnekt.repository.AddressRepository;
import com.foodkonnekt.repository.ConvenienceFeeRepository;
import com.foodkonnekt.repository.DeliveryOpeningClosingDayRepository;
import com.foodkonnekt.repository.DeliveryOpeningClosingTimeRepository;
import com.foodkonnekt.repository.MerchantRepository;
import com.foodkonnekt.repository.OpeningClosingDayRepository;
import com.foodkonnekt.repository.OpeningClosingTimeRepository;
import com.foodkonnekt.repository.PaymentModeRepository;
import com.foodkonnekt.repository.PickUpTimeRepository;
import com.foodkonnekt.repository.TimeZoneRepository;
import com.foodkonnekt.repository.ZoneRepository;
import com.foodkonnekt.service.BusinessService;
import com.foodkonnekt.util.DateUtil;
import com.foodkonnekt.util.IConstant;

@Service
public class BusinessServiceImpl implements BusinessService {
	
	private static final Logger LOGGER= LoggerFactory.getLogger(BusinessServiceImpl.class);

    @Autowired
    private OpeningClosingDayRepository openingClosingDayRepository;

    @Autowired
    private OpeningClosingTimeRepository openingClosingTimeRepository;
    
    @Autowired
    private DeliveryOpeningClosingTimeRepository deliveryOpeningClosingTimeRepository ;
    
    @Autowired
    private DeliveryOpeningClosingDayRepository deliveryOpeningClosingDayRepository ;

    @Autowired
    private AddressRepository addressRepository;

    @Autowired
    private ConvenienceFeeRepository convenienceFeeRepository;

    @Autowired
    private PaymentModeRepository paymentModeRepository;

    @Autowired
    private PickUpTimeRepository pickUpTimeRepository;

    @Autowired
    private ZoneRepository zoneRepository;
    
    @Autowired
    private MerchantRepository merchantRepository;
    
    @Autowired
    private TimeZoneRepository timeZoneRepository;

    /**
     * Find opening closing day and time by merchantId
     */
    public List<OpeningClosingDay> findBusinessHourByMerchantId(Integer merchantId) {
       	LOGGER.info(" Start :: BusinessServiceImpl : findBusinessHourByMerchantId : ");
       	LOGGER.info("BusinessServiceImpl : findBusinessHourByMerchantId : merchantId:"+merchantId);

        List<OpeningClosingDay> openingClosingDays = openingClosingDayRepository.findByMerchantId(merchantId);
        for (OpeningClosingDay day : openingClosingDays) {
           	LOGGER.info("BusinessServiceImpl : findBusinessHourByMerchantId : dayId:"+day.getId());

            List<OpeningClosingTime> times = openingClosingTimeRepository.findByOpeningClosingDayId(day.getId());
            for (OpeningClosingTime closingTime : times) {
               	LOGGER.info("BusinessServiceImpl : findBusinessHourByMerchantId : StartTime:"+closingTime.getStartTime());

                closingTime.setStartTime(DateUtil.get12Format(closingTime.getStartTime()));
               	LOGGER.info("BusinessServiceImpl : findBusinessHourByMerchantId : EndTime:"+closingTime.getEndTime());

                closingTime.setEndTime(DateUtil.get12Format(closingTime.getEndTime()));
            }
            day.setTimes(times);
        }
       	LOGGER.info("------------------- End :: BusinessServiceImpl : findBusinessHourByMerchantId :--------------------");

        return openingClosingDays;
    }

    /**
     * Find Location details by merchantId
     */
    public Address findLocationByMerchantId(Integer merchantId) {
       	LOGGER.info("------------------- Start :: BusinessServiceImpl : findLocationByMerchantId :--------------------");
       	LOGGER.info(" BusinessServiceImpl : findLocationByMerchantId : merchantId :"+merchantId);

        List<Address> addresses = addressRepository.findByMerchantId(merchantId);
        if (addresses != null) {
            if (!addresses.isEmpty()) {
               	LOGGER.info("------------------- End :: BusinessServiceImpl : findLocationByMerchantId :--------------------");

                return addresses.get(0);
            }
        }
       	LOGGER.info("------------------- End :: BusinessServiceImpl : findLocationByMerchantId :--------------------");

        return null;
    }
    
    public void updateDeliveryHour(String startTime, String endTime, String selectedDay,Integer allowDeliveryTiming,Merchant merchant) {
       	LOGGER.info("------------------- Start :: BusinessServiceImpl : updateDeliveryHour :--------------------");
       	LOGGER.info(" BusinessServiceImpl : updateDeliveryHour : startTime:"+startTime+":endTime:"+endTime+":selectedDay:"+selectedDay+":allowDeliveryTiming:"+allowDeliveryTiming);

    	if(allowDeliveryTiming!=null && allowDeliveryTiming==IConstant.BOOLEAN_TRUE){
        String startT[] = startTime.split(",");
        String endT[] = endTime.split(",");
        for (int i = 0; i < startT.length; i++) {
            String startIndex = startT[i];
            String sArray[] = startIndex.split("_");
            int dayId = Integer.valueOf(sArray[0]);
           	LOGGER.info(" BusinessServiceImpl : updateDeliveryHour : dayId:"+dayId);

            List<DeliveryOpeningClosingTime> openingClosingTimes = deliveryOpeningClosingTimeRepository
                            .findByDeliveryOpeningClosingDayId(dayId);
            if (openingClosingTimes != null) {
                if (!openingClosingTimes.isEmpty()) {
                    for (DeliveryOpeningClosingTime closingTime : openingClosingTimes) {
                       	LOGGER.info(" BusinessServiceImpl : updateDeliveryHour : deliveryOpeningClosingTime deleted:");

                    	deliveryOpeningClosingTimeRepository.delete(closingTime.getId());
                    }
                }
            }
        }

        List<DeliveryOpeningClosingTime> closingTimes = new ArrayList<DeliveryOpeningClosingTime>();
        for (int i = 0; i < startT.length; i++) {
        	DeliveryOpeningClosingTime closingTime = new DeliveryOpeningClosingTime();
            String startIndex = startT[i];
            String sArray[] = startIndex.split("_");
            int dayId = Integer.valueOf(sArray[0]);
           	LOGGER.info(" BusinessServiceImpl : updateDeliveryHour : dayId :"+dayId);

            DeliveryOpeningClosingDay openingClosingDay = deliveryOpeningClosingDayRepository.findOne(dayId);
            openingClosingDay.setIsHoliday(0);
           	LOGGER.info(" BusinessServiceImpl : updateDeliveryHour : DeliveryOpeningClosingDay save :");

            deliveryOpeningClosingDayRepository.save(openingClosingDay);
            openingClosingDay.setMerchant(null);
            String startTim = sArray[1];
            String endTm = endT[i];
            closingTime.setStartTime(DateUtil.getTwentyFourFormat(startTim));
            closingTime.setEndTime(DateUtil.getTwentyFourFormat(endTm));
            closingTime.setDeliveryOpeningClosingDay(openingClosingDay);
            closingTimes.add(closingTime);
        }
       	LOGGER.info(" BusinessServiceImpl : updateDeliveryHour : save DeliveryOpeningClosingTime:");

        deliveryOpeningClosingTimeRepository.save(closingTimes);

        String days[] = selectedDay.split(",");
        for (int i = 0; i < days.length; i++) {
            String dayArray = days[i];
            String dArray[] = dayArray.split(":");
            int dayId = Integer.valueOf(dArray[0]);
            String status = dArray[1];
            DeliveryOpeningClosingDay openingClosingDay = deliveryOpeningClosingDayRepository.findOne(dayId);
            if (status.equals("close")) {
                openingClosingDay.setIsHoliday(1);
               	LOGGER.info(" BusinessServiceImpl : updateDeliveryHour : save openingClosingDay:");

                deliveryOpeningClosingDayRepository.save(openingClosingDay);
                List<DeliveryOpeningClosingTime> openingClosingTimes = deliveryOpeningClosingTimeRepository
                                .findByDeliveryOpeningClosingDayId(dayId);
                if (openingClosingTimes != null) {
                    if (!openingClosingTimes.isEmpty()) {
                        for (DeliveryOpeningClosingTime closingTime : openingClosingTimes) {
                           	LOGGER.info(" BusinessServiceImpl : updateDeliveryHour : deliveryOpeningClosingTime deliveryOpeningClosingTimeRepositoryId:"+closingTime.getId());

                        	deliveryOpeningClosingTimeRepository.delete(closingTime.getId());
                        }
                    }
                }
            }
        }
    	}else{
    		allowDeliveryTiming=0;
    	}
    	merchant.setAllowDeliveryTiming(allowDeliveryTiming);
       	LOGGER.info(" BusinessServiceImpl : updateDeliveryHour : merchant save:");

    	merchantRepository.save(merchant);
    	
    }
    
    public List<DeliveryOpeningClosingDay> findDeliveryHourByMerchantId(Merchant merchant) {
       	LOGGER.info("---------------Start :: BusinessServiceImpl : findDeliveryHourByMerchantId :--------------------");
       	LOGGER.info(" BusinessServiceImpl : findDeliveryHourByMerchantId : merchantId :"+merchant.getId());

        List<DeliveryOpeningClosingDay> openingClosingDays = deliveryOpeningClosingDayRepository.findByMerchantId(merchant.getId());
        if(openingClosingDays==null || openingClosingDays.isEmpty()){
        	saveDefaultDeliveryHours(merchant);
        }
        openingClosingDays = deliveryOpeningClosingDayRepository.findByMerchantId(merchant.getId());
        for (DeliveryOpeningClosingDay day : openingClosingDays) {
           	LOGGER.info(" BusinessServiceImpl : findDeliveryHourByMerchantId : dayId :"+day.getId());

            List<DeliveryOpeningClosingTime> times = deliveryOpeningClosingTimeRepository.findByDeliveryOpeningClosingDayId(day.getId());
            for (DeliveryOpeningClosingTime closingTime : times) {
               	LOGGER.info(" BusinessServiceImpl : findDeliveryHourByMerchantId : satartTime :"+closingTime.getStartTime());
               	LOGGER.info(" BusinessServiceImpl : findDeliveryHourByMerchantId : EndTime :"+closingTime.getEndTime());

                closingTime.setStartTime(DateUtil.get12Format(closingTime.getStartTime()));
                closingTime.setEndTime(DateUtil.get12Format(closingTime.getEndTime()));
            }
            day.setTimes(times);
        }
       	LOGGER.info("---------------End :: BusinessServiceImpl : findDeliveryHourByMerchantId :--------------------");

        return openingClosingDays;
    }
    
    public void saveDefaultDeliveryHours(Merchant merchant){
       	LOGGER.info("---------------Start :: BusinessServiceImpl : saveDefaultDeliveryHours :--------------------");

    	String days[]={"sunday","monday","tuesday","wednesday","thursday","friday","saturday"};
    	for(String day:days){
    		DeliveryOpeningClosingDay openingClosingDay=null;
           	LOGGER.info(" BusinessServiceImpl : saveDefaultDeliveryHours :day :"+day+":merchantId:"+merchant.getId());

    		openingClosingDay=deliveryOpeningClosingDayRepository.findByDayAndMerchantId(day, merchant.getId());
       if(openingClosingDay==null){
        openingClosingDay = new DeliveryOpeningClosingDay();
    	openingClosingDay.setIsHoliday(IConstant.BOOLEAN_FALSE);
    	openingClosingDay.setDay(day);
    	openingClosingDay.setMerchant(merchant);
       	LOGGER.info(" BusinessServiceImpl : saveDefaultDeliveryHours :save DeliveryOpeningClosingDay :");

    	deliveryOpeningClosingDayRepository.save(openingClosingDay);
       }
       List<DeliveryOpeningClosingTime> openingClosingTimes=null;
       if(openingClosingDay!=null){
          	LOGGER.info(" BusinessServiceImpl : saveDefaultDeliveryHours :DeliveryOpeningClosingDayId :"+openingClosingDay.getId());

    	   openingClosingTimes= deliveryOpeningClosingTimeRepository.findByDeliveryOpeningClosingDayId(openingClosingDay.getId());
       }
       if(openingClosingTimes==null || openingClosingTimes.size()==0){
    	   DeliveryOpeningClosingTime openingClosingTime = new DeliveryOpeningClosingTime();
     openingClosingTime.setStartTime("09:00");
     openingClosingTime.setEndTime("18:00");
     openingClosingTime.setDeliveryOpeningClosingDay(openingClosingDay);
   	LOGGER.info(" BusinessServiceImpl : saveDefaultDeliveryHours :save DeliveryOpeningClosingTime :");

     deliveryOpeningClosingTimeRepository.save(openingClosingTime);
       }
    	}
    }
    
    public void saveDefaultBusinessHours(Merchant merchant){
       	LOGGER.info("-----------------Start ::  BusinessServiceImpl : saveDefaultBusinessHours :------------------------");
    	System.out.println("============================inside uisness hours service Impl========================");
    	String days[]={"sunday","monday","tuesday","wednesday","thursday","friday","saturday"};
    	for(String day:days){
    	OpeningClosingDay openingClosingDay = null;
       	LOGGER.info(" BusinessServiceImpl : saveDefaultBusinessHours :merchantId :"+merchant.getId()+":day:"+day);

    	openingClosingDay=openingClosingDayRepository.findByMerchantIdAndDay(merchant.getId(),day);
    	if(openingClosingDay==null){
    		openingClosingDay=new OpeningClosingDay();
    	}
    	openingClosingDay.setIsHoliday(IConstant.BOOLEAN_FALSE);
    	openingClosingDay.setDay(day);
    	openingClosingDay.setMerchant(merchant);
       	LOGGER.info(" BusinessServiceImpl : saveDefaultBusinessHours :save OpeningClosingDay :");

        openingClosingDayRepository.save(openingClosingDay);
       	LOGGER.info(" BusinessServiceImpl : saveDefaultBusinessHours : openingClosingTimesId:"+openingClosingDay.getId());

        List<OpeningClosingTime> openingClosingTimes = openingClosingTimeRepository
                .findByOpeningClosingDayId(openingClosingDay.getId());
if (openingClosingTimes != null) {
    if (!openingClosingTimes.isEmpty()) {
        for (OpeningClosingTime closingTime : openingClosingTimes) {
           	LOGGER.info(" BusinessServiceImpl : saveDefaultBusinessHours : openingClosingTime deleted by openingClosingTimesId:"+closingTime.getId());

            openingClosingTimeRepository.delete(closingTime.getId());
        }
    }
}
     OpeningClosingTime openingClosingTime = new OpeningClosingTime();
     openingClosingTime.setStartTime("09:00");
     openingClosingTime.setEndTime("18:00");
     openingClosingTime.setOpeningClosingDay(openingClosingDay);
    	LOGGER.info(" BusinessServiceImpl : saveDefaultBusinessHours : openingClosingTime save");

        openingClosingTimeRepository.save(openingClosingTime);
    	}
    }
    
    public void saveDefaultPaymentMode(Merchant merchant){
    	LOGGER.info("--------------Start : : BusinessServiceImpl : saveDefaultPaymentMode--------------------");

    	PaymentMode paymentMode= new PaymentMode();
    	paymentMode.setLabel("Credit Card");
    	paymentMode.setAllowPaymentMode(IConstant.BOOLEAN_TRUE);
    	paymentMode.setMerchant(merchant);
    	paymentModeRepository.save(paymentMode);
    	LOGGER.info(" BusinessServiceImpl : saveDefaultPaymentMode : PaymentMode save");

    	PaymentMode paymentMode1= new PaymentMode();
    	paymentMode1.setLabel("Cash");
    	paymentMode1.setAllowPaymentMode(IConstant.BOOLEAN_FALSE);
    	paymentMode1.setMerchant(merchant);
    	LOGGER.info(" BusinessServiceImpl : saveDefaultPaymentMode : PaymentMode save");

    	paymentModeRepository.save(paymentMode1);
    }

    public ConvenienceFee findConvenienceFeeByMerchantId(Integer merchantId) {
    	LOGGER.info("-----------------Start:: BusinessServiceImpl : findConvenienceFeeByMerchantId :---------------");

    	ConvenienceFee convenienceFee=null;
    	LOGGER.info(" BusinessServiceImpl : findConvenienceFeeByMerchantId :merchantId:"+merchantId);

    	List<ConvenienceFee> convenienceFees=convenienceFeeRepository.findByMerchantId(merchantId);
    	LOGGER.info(" BusinessServiceImpl : findConvenienceFeeByMerchantId :convenienceFeesSize:"+convenienceFees.size());
    	if(convenienceFees!=null && convenienceFees.size()>0){
    		int index=convenienceFees.size()-1;
    		 convenienceFee=convenienceFees.get(index);
    		
    	}
    	LOGGER.info("-----------------End:: BusinessServiceImpl : findConvenienceFeeByMerchantId :---------------");

    	return convenienceFee;
    }

    public PaymentMode findByMerchantIdAndLabel(Integer merchantId, String label) {
    	 LOGGER.info("----------------Start :: BusinessServiceImpl : findByMerchantIdAndLabel------------------------");
    	 LOGGER.info("BusinessServiceImpl :: findByMerchantIdAndLabel : merchantId "+merchantId);
    	 LOGGER.info("----------------End :: BusinessServiceImpl : findByMerchantIdAndLabel------------------------");
        return paymentModeRepository.findByMerchantIdAndLabel(merchantId, label);
    }

    public void updateBusinessHour(String startTime, String endTime, String selectedDay) {
    	LOGGER.info("-----------------Start:: BusinessServiceImpl : updateBusinessHour :---------------");
    	LOGGER.info("BusinessServiceImpl : updateBusinessHour :startTimea:"+startTime+":endTime:"+endTime+":selectedDay:"+selectedDay);
        String startT[] = startTime.split(",");
        String endT[] = endTime.split(",");
        for (int i = 0; i < startT.length; i++) {
            String startIndex = startT[i];
            String sArray[] = startIndex.split("_");
            int dayId = Integer.valueOf(sArray[0]);
        	LOGGER.info(" BusinessServiceImpl : updateBusinessHour : dayId:"+dayId);

            List<OpeningClosingTime> openingClosingTimes = openingClosingTimeRepository
                            .findByOpeningClosingDayId(dayId);
            if (openingClosingTimes != null) {
                if (!openingClosingTimes.isEmpty()) {
                    for (OpeningClosingTime closingTime : openingClosingTimes) {
                    	LOGGER.info(" BusinessServiceImpl : updateBusinessHour : deleted openingClosingTime By openingClosingTimeId:"+closingTime.getId());

                        openingClosingTimeRepository.delete(closingTime.getId());
                    }
                }
            }
        }

        List<OpeningClosingTime> closingTimes = new ArrayList<OpeningClosingTime>();
        for (int i = 0; i < startT.length; i++) {
            OpeningClosingTime closingTime = new OpeningClosingTime();
            String startIndex = startT[i];
            String sArray[] = startIndex.split("_");
            int dayId = Integer.valueOf(sArray[0]);
        	LOGGER.info(" BusinessServiceImpl : updateBusinessHour :dayId:"+dayId);

            OpeningClosingDay openingClosingDay = openingClosingDayRepository.findOne(dayId);
            openingClosingDay.setIsHoliday(0);
        	LOGGER.info(" BusinessServiceImpl : updateBusinessHour :save openingClosingDay");

            openingClosingDayRepository.save(openingClosingDay);
            openingClosingDay.setMerchant(null);
            String startTim = sArray[1];
            String endTm = endT[i];
            closingTime.setStartTime(DateUtil.getTwentyFourFormat(startTim));
            closingTime.setEndTime(DateUtil.getTwentyFourFormat(endTm));
            closingTime.setOpeningClosingDay(openingClosingDay);
            closingTimes.add(closingTime);
        }
    	LOGGER.info(" BusinessServiceImpl : updateBusinessHour :save openingClosingTime");

        openingClosingTimeRepository.save(closingTimes);

        String days[] = selectedDay.split(",");
        for (int i = 0; i < days.length; i++) {
            String dayArray = days[i];
            String dArray[] = dayArray.split(":");
            int dayId = Integer.valueOf(dArray[0]);
            String status = dArray[1];
        	LOGGER.info(" BusinessServiceImpl : updateBusinessHour : dayId:"+dayId);

            OpeningClosingDay openingClosingDay = openingClosingDayRepository.findOne(dayId);
            if (status.equals("close")) {
                openingClosingDay.setIsHoliday(1);
            	LOGGER.info(" BusinessServiceImpl : updateBusinessHour : save openingClosingDay:");

                openingClosingDayRepository.save(openingClosingDay);
            	LOGGER.info(" BusinessServiceImpl : updateBusinessHour :dayId:"+dayId);

                List<OpeningClosingTime> openingClosingTimes = openingClosingTimeRepository
                                .findByOpeningClosingDayId(dayId);
                if (openingClosingTimes != null) {
                    if (!openingClosingTimes.isEmpty()) {
                        for (OpeningClosingTime closingTime : openingClosingTimes) {
                        	LOGGER.info(" BusinessServiceImpl : updateBusinessHour : openingClosingTime deleted:");

                            openingClosingTimeRepository.delete(closingTime.getId());
                        }
                    }
                }
            }
        }
    }

    public void savePaymentMode(PaymentMode mode) {
    	LOGGER.info("Start:: BusinessServiceImpl : savePaymentMode :");
    	LOGGER.info("End:: BusinessServiceImpl : savePaymentMode : PaymentMode save");

        paymentModeRepository.save(mode);

    }

    public PickUpTime findPickUpTimeByMerchantId(Integer merchantId) {
    	LOGGER.info("----------------Start :: BusinessServiceImpl : findPickUpTimeByMerchantId------------------------");
        PickUpTime pickUpTime = pickUpTimeRepository.findByMerchantId(merchantId);
        LOGGER.info("BusinessServiceImpl :: findPickUpTimeByMerchantId : merchantId "+merchantId);
        if (pickUpTime == null) {
            pickUpTime = new PickUpTime();
            pickUpTime.setPickUpTime("0");
        }
        LOGGER.info("BusinessServiceImpl :: findPickUpTimeByMerchantId : pickUpTime "+pickUpTime);
        LOGGER.info("----------------End :: BusinessServiceImpl : findPickUpTimeByMerchantId------------------------");
        return pickUpTime;
    }

    /**
     * Find opening hour for future orders
     */
    public List<String> findFutureOrderOpeningHours(String futureDate, Integer merchantId, String orderType) {
    	LOGGER.info("----------------Start :: BusinessServiceImpl : findFutureOrderOpeningHours------------------------");
        if(futureDate!=null && !futureDate.equals("select")){
    	Merchant merchant= merchantRepository.findById(merchantId);
    	LOGGER.info("BusinessServiceImpl :: findFutureOrderOpeningHours : merchantId "+merchantId);
//    	Integer hourDifference=0;
//    	Integer minutDifference=0;
//    	if(merchant!=null && merchant.getTimeZone()!=null && merchant.getTimeZone().getHourDifference()!=null){
//    		hourDifference=merchant.getTimeZone().getHourDifference();
//    		if(merchant.getTimeZone().getMinutDifference()!=null)
//    			minutDifference=merchant.getTimeZone().getMinutDifference();
//    	}
        	String pickOrDeliverytime = "30";
        //if ("Pickup".equals(orderType)) {
            PickUpTime pickUpTimes = pickUpTimeRepository.findByMerchantId(merchantId);
            if (pickUpTimes != null) {
                if (pickUpTimes.getPickUpTime() != null) {
                    pickOrDeliverytime = pickUpTimes.getPickUpTime();
                }
            }
        //} else {
            /*List<Zone> zones = zoneRepository.findByMerchantId(merchantId);
            if (!zones.isEmpty()) {
                pickOrDeliverytime = zones.get(0).getAvgDeliveryTime();
            }*/
        	String zones = zoneRepository.findMaxDeliveryAvgTimeByMerchantId(merchantId);
            if (zones != null) {
            //	 System.out.println("maximum avg"+zones);
            		LOGGER.info("===============  BusinessServiceImpl : Inside findFutureOrderOpeningHours :: maximum avg  ============= "+zones);
            		try{
            		if(Integer.valueOf(zones)>Integer.valueOf(pickOrDeliverytime))
                     pickOrDeliverytime = zones;
            		}catch (Exception e) {
        				LOGGER.error("error: " + e.getMessage());
        		    	LOGGER.error(" BusinessServiceImpl : findFutureOrderOpeningHours: Exception:"+e);

        			}
           // }
        }
            String timeZoneCode=timeZoneRepository.findTimeZoneCodeByMerchentId(merchantId);
        OpeningClosingDay openingClosingDay = openingClosingDayRepository.findByDayAndMerchantId(
                        DateUtil.findDayNameFromDate(futureDate), merchantId);
        List<OpeningClosingTime> openingClosingTimes = openingClosingTimeRepository
                        .findByOpeningClosingDayId(openingClosingDay.getId());
        
        timeZoneCode = (timeZoneCode!=null) ? timeZoneCode : "America/Chicago";
        Date date= DateUtil.getCurrentDateForTimeZonee(timeZoneCode);
        DateFormat df = new SimpleDateFormat("HH:mm");
        
        String currentTimeForTimeZone = df.format(date);
        
        String[] currentTimeArray1 =currentTimeForTimeZone.split(":");
        int current_hour=Integer.parseInt(currentTimeArray1[0]);
        
        
    	SimpleDateFormat dateForma=  new SimpleDateFormat("yyyy-MM-dd");
    	
    	
    	
    	
    	String futureDay=DateUtil.findDayNameFromDate(futureDate);
    	
        List<java.sql.Time> intervals = new ArrayList<java.sql.Time>();
        for (OpeningClosingTime closingTime : openingClosingTimes) {
        	String[] opeingArray = closingTime.getStartTime().split(":");
        	
            
            String endtime = "";
            if ("00:00".equals(closingTime.getEndTime())) {
                endtime = "24:00";
            } else {
                endtime = closingTime.getEndTime();
            }
            String[] closingArray = endtime.split(":");
            @SuppressWarnings("deprecation")
            java.sql.Time startTime = new java.sql.Time(Integer.parseInt(opeingArray[0]),
                            Integer.parseInt(opeingArray[1]), 0);
            
            
            
            @SuppressWarnings("deprecation")
            java.sql.Time endTime = new java.sql.Time(Integer.parseInt(closingArray[0]),
                            Integer.parseInt(closingArray[1]), 0);
            Calendar cal = Calendar.getInstance();
            cal.setTime(startTime);
            Calendar endCal = Calendar.getInstance();
            endCal.setTime(endTime);
            String currentDate=dateForma.format(date);
        	
        	LocalDate start = LocalDate.parse(dateForma.format(date));
        	if(current_hour<0)
            	{
            		start = LocalDate.parse(dateForma.format(date));
            		start= start.minusDays(1);
        		currentDate = start .toString( );
        		cal.add(Calendar.HOUR, -24);
        		endCal.add(Calendar.HOUR, -24);
            	}
        	String currentDay=DateUtil.findDayNameFromDate(currentDate);
            LOGGER.info("Start time->"+cal.getTime());
            
            LOGGER.info("===============  BusinessServiceImpl : Inside findFutureOrderOpeningHours :: Start time->============= "+cal.getTime());	
            
            String[] currentTimeArray =currentTimeForTimeZone.split(":");
       	 @SuppressWarnings("deprecation")
       	 java.sql.Time currentTime = new java.sql.Time(Integer.parseInt(currentTimeArray[0]),
                    Integer.parseInt(currentTimeArray[1]), 0);
       	
       	 Calendar currentCal = Calendar.getInstance();
       	 currentCal.setTime(currentTime);
//       	 currentCal.add(Calendar.HOUR, hourDifference);
       	 currentCal.add(Calendar.MINUTE, 0);
       	
    	 //currentDate=dateForma.format(currentCal.getTime());
    	 //currentDay=DateUtil.findDayNameFromDate(currentDate);
       	//System.out.println(dateForma.format(currentCal.getTime()));
       	LOGGER.info("BusinessServiceImpl :: findFutureOrderOpeningHours : dateForma.format(currentCal.getTime()) "+dateForma.format(currentCal.getTime()));
       	//System.out.println("Current time->"+currentCal.getTime());
    	LOGGER.info("BusinessServiceImpl :: findFutureOrderOpeningHours : Current time "+currentCal.getTime());	
    //   	System.out.println("Start time->"+cal.getTime());
    	LOGGER.info("BusinessServiceImpl :: findFutureOrderOpeningHours : Start time "+cal.getTime());	
             if(currentDay.equals(futureDay)){
        		
            	 
            	 
            	 if(!cal.getTime().after(currentCal.getTime())){
            		// startTime=currentTime;
            		 cal.setTime(currentCal.getTime());
            		 int unroundedMinutes = cal.get(Calendar.MINUTE);
            		 int mod = unroundedMinutes % 15;
            		 cal.add(Calendar.MINUTE, mod < 8 ? -mod : (15-mod));
            	 }
            	 
        	}
//             if (!pickOrDeliverytime.isEmpty()) {
//                 cal.add(Calendar.MINUTE, Integer.parseInt(pickOrDeliverytime));
//             }else{
            	 cal.add(Calendar.MINUTE, 60);
            // }
             
            if(!cal.getTime().after(endCal.getTime()) && !cal.getTime().equals(endCal.getTime())){
            
                intervals.add(new java.sql.Time(cal.getTimeInMillis()));
            
            while (cal.getTime().before(endCal.getTime())) {
                cal.add(Calendar.MINUTE, 15);
                if (cal.getTime().before(endCal.getTime())) {
                    intervals.add(new java.sql.Time(cal.getTimeInMillis()));
                }
            }
            
            intervals.add(endTime);
            }
            
        }
        
        List<String> times = new ArrayList<String>();
        for (java.sql.Time time : intervals) {
            times.add(DateUtil.get12Format(time.toString()));
        }
        LOGGER.info("----------------End :: BusinessServiceImpl : findFutureOrderOpeningHours------------------------");
        
        return times;
        }else{
        	LOGGER.info("----------------End :: BusinessServiceImpl : findFutureOrderOpeningHours------------------------");
        	return null;
        }
    }
    
    
    public List<String> findFutureDates(Merchant merchant) {
    	LOGGER.info("----------------Start :: BusinessServiceImpl : findFutureDates------------------------");
    	//Merchant merchant= merchantRepository.findById(merchantId);
//    	Integer hourDifference=0;
//    	Integer minutDifference=0;
    	
    	String timeZoneCode = "America/Chicago";
    	
 //  	if(merchant!=null && merchant.getTimeZone()!=null && merchant.getTimeZone().getHourDifference()!=null){
    		
    		if(merchant.getTimeZone().getTimeZoneCode() != null){
    			timeZoneCode = merchant.getTimeZone().getTimeZoneCode();
    		}
    		
    		//hourDifference=merchant.getTimeZone().getHourDifference();
    		//if(merchant.getTimeZone().getMinutDifference()!=null)
    			//minutDifference=merchant.getTimeZone().getMinutDifference();
 //   	}
    	
    	LOGGER.info("BusinessServiceImpl :: findFutureDates : merchantId "+merchant.getId());
    	
    	List<OpeningClosingDay> openingClosingDays = openingClosingDayRepository.findByMerchantIdAndIsHolidayNot(merchant.getId(),IConstant.BOOLEAN_TRUE);
    	if(openingClosingDays!=null && openingClosingDays.size()>0){
    	String days="";
    	
    	for(OpeningClosingDay day:openingClosingDays){
    		days=days+", "+day.getDay();
    	}
    	
    	Date timeZonedate = DateUtil.getCurrentDateForTimeZonee(timeZoneCode);
    	 DateFormat df = new SimpleDateFormat("HH:mm");
         
         String currentTime = df.format(timeZonedate);
         
    	String[] currentTimeArray =currentTime.split(":");
   	 @SuppressWarnings("deprecation")
//   	 java.sql.Time currentTime = new java.sql.Time(Integer.parseInt(currentTimeArray[0]),
//                Integer.parseInt(currentTimeArray[1]), 0);
//   	 Calendar currentCal = Calendar.getInstance();
//   	 currentCal.setTime(currentTime);
   	int a	=Integer.parseInt(currentTimeArray[0]);
    	
    	Date date= DateUtil.getCurrentDateForTimeZonee(timeZoneCode);
    	SimpleDateFormat dateForma=  new SimpleDateFormat("yyyy-MM-dd");
    	dateForma.format(date);
    	LocalDate start = LocalDate.parse(dateForma.format(date));
    	if(a<0)
    	{
    		start = LocalDate.parse(dateForma.format(date));
    		start= start.minusDays(1);
    	}else{
    		 start = LocalDate.parse(dateForma.format(date));
    	}
    	 
    	 /*String dateStart=start.toString();
    		
    	 System.out.println(start.plusDays(1));*/
    	 LocalDate nextDate=start;
    	 OpeningClosingDay closingDay = openingClosingDayRepository.findByDayAndMerchantId(DateUtil.findDayNameFromDate(nextDate.toString()), merchant.getId());
    	 
    	 List<String> dates = new ArrayList<String>();
    	 int futureDaysAhead=10;
    	 if(merchant.getFutureDaysAhead()!=null && merchant.getFutureDaysAhead()>0){
    		 futureDaysAhead=merchant.getFutureDaysAhead();
    	 }
    	 int i=1;
    	 String  dateToAdd= null;
        while(i<=futureDaysAhead){
        	String dateStart=nextDate.toString();
        	String dayName=DateUtil.findDayNameFromDate(dateStart);
        	
        	if(closingDay != null && closingDay.getDay().equalsIgnoreCase(dayName)){
        	List<OpeningClosingTime> closingTimes = openingClosingTimeRepository.findByOpeningClosingDayId(closingDay.getId());
        	    if(closingTimes !=null && closingTimes.size() > 0){
            		for (OpeningClosingTime openingClosingTime : closingTimes) {
            			DateUtil dateUtil=new DateUtil();
            	     		if(dateUtil.checkCurrentTimeExistBetweenTwoTimeForFutureDate(openingClosingTime.getStartTime(),openingClosingTime.getEndTime(),timeZoneCode)){
            					if(days.contains(dayName)){
            		         		//dates.add(dateStart);
            						dateToAdd = dateStart;
            	        	}
            			}
    				}
            		if(dateToAdd!=null)
            		dates.add(dateToAdd);
            		i++;
            	}
        	}else{
        		if(dateStart!=null && days.contains(dayName)){
		        	dates.add(dateStart);
		            i++;
		        }
        	}
        	nextDate= nextDate.plusDays(1);
        	
        }
        
        LOGGER.info("----------------End :: BusinessServiceImpl : findFutureDates------------------------");
        return dates;
    	}else{
    		LOGGER.info("----------------End :: BusinessServiceImpl : findFutureDates------------------------");
    		return null;
    	}
    }

    public void updateBuisnessHoursTime(List<OpeningClosingDay> openingClosingDay, Merchant merchant) {
        LOGGER.info("----------------Start :: BusinessServiceImpl : updateBuisnessHoursTime------------------------");

		for (OpeningClosingDay openingClosingDayList : openingClosingDay) {
			if (openingClosingDayList.getIsHoliday() != 0) {
				openingClosingDayList.setIsHoliday(1);
		        LOGGER.info("BusinessServiceImpl : updateBuisnessHoursTime: save openingClosingDay");

				openingClosingDayRepository.save(openingClosingDayList);
		        LOGGER.info("BusinessServiceImpl : updateBuisnessHoursTime:  openingClosingDayId:"+openingClosingDayList.getId());

				List<OpeningClosingTime> openingClosingTimes = openingClosingTimeRepository
						.findByOpeningClosingDayId(openingClosingDayList.getId());
				for (OpeningClosingTime openingClosingTime : openingClosingTimes) {
					openingClosingTimeRepository.delete(openingClosingTime.getId());
			        LOGGER.info("BusinessServiceImpl : updateBuisnessHoursTime:  openingClosingTime deleted:");

				}
			} else {
		        LOGGER.info("BusinessServiceImpl : updateBuisnessHoursTime:  merchantId :"+merchant.getId()+":day:"+openingClosingDayList.getDay());

				OpeningClosingDay closingDay = openingClosingDayRepository.findByMerchantIdAndDay(merchant.getId(),
						openingClosingDayList.getDay());
				if (closingDay != null) {
					closingDay.setIsHoliday(openingClosingDayList.getIsHoliday());
					OpeningClosingDay day2 = openingClosingDayRepository.save(closingDay);
			        LOGGER.info("BusinessServiceImpl : updateBuisnessHoursTime:  save OpeningClosingDay :");

					List<OpeningClosingTime> openingClosingTime = openingClosingDayList.getTimes();
					if (openingClosingTime != null && !openingClosingTime.isEmpty()) {
				        LOGGER.info("BusinessServiceImpl : updateBuisnessHoursTime: OpeningClosingTimeId:"+openingClosingDayList.getId());

						List<OpeningClosingTime> duplicateOpeningClosingTime = openingClosingTimeRepository
								.findByOpeningClosingDayId(openingClosingDayList.getId());
						if (duplicateOpeningClosingTime != null && !duplicateOpeningClosingTime.isEmpty()) {
							for (OpeningClosingTime openingClosingTime2 : duplicateOpeningClosingTime) {
								openingClosingTimeRepository.delete(openingClosingTime2.getId());
						        LOGGER.info("BusinessServiceImpl : updateBuisnessHoursTime: deleted OpeningClosingTime :");

							}
						}
							
						for (OpeningClosingTime oct : openingClosingTime) {
							/*OpeningClosingTime closingTime = openingClosingTimeRepository.findOne(oct.getId());
							if(closingTime !=null){
								closingTime.setOpeningClosingDay(openingClosingDayList);
								closingTime.setStartTime(DateUtil.getTwentyFourFormat(oct.getStartTime()));
								closingTime.setEndTime(DateUtil.getTwentyFourFormat(oct.getEndTime()));
								openingClosingTimeRepository.save(oct);
								
								System.out.println("Id " + oct.getId());
							} else {*/
								oct.setOpeningClosingDay(day2);
								String startTime = DateUtil.getTwentyFourFormat(oct.getStartTime());
								String endTime = DateUtil.getTwentyFourFormat(oct.getEndTime());
								oct.setStartTime(startTime);
								oct.setEndTime(endTime);
								openingClosingTimeRepository.save(oct);
						        LOGGER.info("BusinessServiceImpl : updateBuisnessHoursTime:  save OpeningClosingTime :");

							//}
						}
					}
				} else {
					OpeningClosingDay obj = new OpeningClosingDay();

					obj.setMerchant(merchant);
					obj.setDay(openingClosingDayList.getDay());
					obj.setIsHoliday(openingClosingDayList.getIsHoliday());

					OpeningClosingDay day = openingClosingDayRepository.save(obj);
			        LOGGER.info("BusinessServiceImpl : updateBuisnessHoursTime:  save OpeningClosingDay :");

					for (OpeningClosingTime openingClosingTime : openingClosingDayList.getTimes()) {
						OpeningClosingTime finalOpeningClosingTime = new OpeningClosingTime();
						finalOpeningClosingTime.setOpeningClosingDay(day);
						finalOpeningClosingTime.setStartTime(DateUtil.getTwentyFourFormat(openingClosingTime.getStartTime()));
						finalOpeningClosingTime.setEndTime(DateUtil.getTwentyFourFormat(openingClosingTime.getEndTime()));

						openingClosingTimeRepository.save(finalOpeningClosingTime);
				        LOGGER.info("BusinessServiceImpl : updateBuisnessHoursTime:  save OpeningClosingTime :");
					}
				}
			}
		}
	}


	public Merchant getBusinessProfileInfo(Merchant merchant) {
        LOGGER.info("-------------Start :: BusinessServiceImpl : getBusinessProfileInfo:---------------");

		Merchant finalMerchant = new Merchant();
		finalMerchant.setFutureDaysAhead(null);
		finalMerchant.setActiveCustomerFeedback(null);
		finalMerchant.setIsFBAppPublished(null);
		finalMerchant.setName(merchant.getName());
		finalMerchant.setPhoneNumber(merchant.getPhoneNumber());
		finalMerchant.setWebsite(merchant.getWebsite());
		finalMerchant.setAddress(null);
        LOGGER.info(" BusinessServiceImpl : getBusinessProfileInfo : merchantId :"+merchant.getId());

		List<Zone> zones = zoneRepository.findByMerchantId(merchant.getId());
		if(zones!=null && !zones.isEmpty()){
			for (Zone zone : zones) {
		        LOGGER.info(" BusinessServiceImpl : getBusinessProfileInfo : zoneId :"+zone.getId());

				Zone finalZone = zoneRepository.findById(zone.getId());
				finalMerchant.setDeliverystatus(finalZone.getZoneStatus());
			}
		}
        LOGGER.info("---------------End :: BusinessServiceImpl : getBusinessProfileInfo :------------- ");
		return finalMerchant;
	}
	
	
	public boolean getOpenhoursStatus(Integer merchantId) {
        LOGGER.info("---------------Start :: BusinessServiceImpl : getOpenhoursStatus :------------- ");
        LOGGER.info(" BusinessServiceImpl : getOpenhoursStatus :merchantId:"+merchantId);

		String toDay = DateUtil.getCurrentDay();
	boolean flag = false;
    LOGGER.info(" BusinessServiceImpl : getOpenhoursStatus :day:"+toDay);

	OpeningClosingDay openingClosingDay = openingClosingDayRepository.findByDayAndMerchantId(toDay, merchantId);
		if(openingClosingDay != null && openingClosingDay.getId() != null){
		    LOGGER.info(" BusinessServiceImpl : getOpenhoursStatus :openingClosingDayId:"+openingClosingDay.getId());

			List<OpeningClosingTime> openingClosingTimes = openingClosingTimeRepository.findByOpeningClosingDayId(openingClosingDay.getId());
			
			if(openingClosingTimes != null && !openingClosingTimes.isEmpty()){
				
				String timeZoneCode=timeZoneRepository.findTimeZoneCodeByMerchentId(merchantId);
				
				for (OpeningClosingTime openingClosingTime2 : openingClosingTimes) {
					
					String startTime = openingClosingTime2.getStartTime().toString();
					String endTime = openingClosingTime2.getEndTime().toString();							    
					Boolean status=false;
					try {
					    LOGGER.info(" BusinessServiceImpl : getOpenhoursStatus :timeZoneCode:"+timeZoneCode+":startTime:"+startTime+":endTime:"+endTime);
					    if(timeZoneCode != null && startTime != null && endTime != null) {
						status = new DateUtil().checkCurrentTimeExistBetweenTwoTime(startTime,endTime,timeZoneCode);
					    LOGGER.info(" BusinessServiceImpl : getOpenhoursStatus :status:"+status);

						  if(status == true){
							  flag = true;
							}
					    }
					} catch (Exception e) {
						LOGGER.error("error: " + e.getMessage());
					    LOGGER.error(" End :: BusinessServiceImpl : getOpenhoursStatus :Exception:"+e);

					}
					
                }//for-loop		
				
		    } //if opening-closing times
			
         } //if- opening-closing day	
	    LOGGER.info("---------------- End :: BusinessServiceImpl : getOpenhoursStatus :---------------------");

		return flag;
		}
}
