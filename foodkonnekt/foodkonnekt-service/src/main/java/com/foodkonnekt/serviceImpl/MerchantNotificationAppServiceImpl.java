package com.foodkonnekt.serviceImpl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.foodkonnekt.model.MerchantNotificationApp;
import com.foodkonnekt.repository.MerchantNotificationAppRepository;
import com.foodkonnekt.service.MerchantNotificationAppService;

@Service
public class MerchantNotificationAppServiceImpl implements MerchantNotificationAppService {
	private static final Logger LOGGER = LoggerFactory.getLogger(MerchantNotificationAppServiceImpl.class);

	@Autowired
	private MerchantNotificationAppRepository merchantNotificationAppRepository;

	public List<MerchantNotificationApp> findAll() {

		List<MerchantNotificationApp> list = merchantNotificationAppRepository
				.findAll();
		return list;
	}

	public MerchantNotificationApp FindBymerchantIdAndAppId(Integer merchantId,
			Integer appd) {
		LOGGER.info("====MerchantNotificationAppServiceImpl : inside FindBymerchantIdAndAppId starts : merchantId : "+merchantId+" appd "+appd);
		MerchantNotificationApp merchantNotificationApp=merchantNotificationAppRepository.FindByMerchantIdAndNotificationAppId(merchantId,appd);
		LOGGER.info("====MerchantNotificationAppServiceImpl : inside FindBymerchantIdAndAppId Ends "); 
		return merchantNotificationApp;
	}
	public MerchantNotificationApp save(MerchantNotificationApp merchantNotificationApp) {
		return merchantNotificationAppRepository.save(merchantNotificationApp);
	}

}
