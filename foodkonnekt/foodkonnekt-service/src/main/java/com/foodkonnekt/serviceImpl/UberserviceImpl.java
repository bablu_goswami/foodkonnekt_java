package com.foodkonnekt.serviceImpl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.foodkonnekt.clover.vo.Modifications;
import com.foodkonnekt.clover.vo.Modifier;
import com.foodkonnekt.clover.vo.ModifierVO;
import com.foodkonnekt.clover.vo.OrderItemVO;
import com.foodkonnekt.model.Address;
import com.foodkonnekt.model.CardInfo;
import com.foodkonnekt.model.Category;
import com.foodkonnekt.model.CategoryItem;
import com.foodkonnekt.model.ConvenienceFee;
import com.foodkonnekt.model.CouponRedeemedDto;
import com.foodkonnekt.model.Customer;
import com.foodkonnekt.model.EncryptedKey;
import com.foodkonnekt.model.Item;
import com.foodkonnekt.model.ItemModifierGroup;
import com.foodkonnekt.model.ItemModifiers;
import com.foodkonnekt.model.ItemTax;
import com.foodkonnekt.model.Koupons;
import com.foodkonnekt.model.Merchant;
import com.foodkonnekt.model.MerchantConfiguration;
import com.foodkonnekt.model.MerchantOrders;
import com.foodkonnekt.model.ModifierModifierGroupDto;
import com.foodkonnekt.model.Modifiers;
import com.foodkonnekt.model.NotificationMethod;
import com.foodkonnekt.model.NotificationTracker;
import com.foodkonnekt.model.OpeningClosingDay;
import com.foodkonnekt.model.OpeningClosingTime;
import com.foodkonnekt.model.OrderDiscount;
import com.foodkonnekt.model.OrderItem;
import com.foodkonnekt.model.OrderItemModifier;
import com.foodkonnekt.model.OrderPizza;
import com.foodkonnekt.model.OrderPizzaCrust;
import com.foodkonnekt.model.OrderPizzaToppings;
import com.foodkonnekt.model.OrderR;
import com.foodkonnekt.model.OrderType;
import com.foodkonnekt.model.PickUpTime;
import com.foodkonnekt.model.PizzaCrust;
import com.foodkonnekt.model.PizzaCrustSizes;
import com.foodkonnekt.model.PizzaTemplate;
import com.foodkonnekt.model.PizzaTemplateCategory;
import com.foodkonnekt.model.PizzaTemplateCrust;
import com.foodkonnekt.model.PizzaTemplateSize;
import com.foodkonnekt.model.PizzaTemplateTax;
import com.foodkonnekt.model.PizzaTemplateTopping;
import com.foodkonnekt.model.PizzaToppingSize;
import com.foodkonnekt.model.Printer;
import com.foodkonnekt.model.UberStore;
import com.foodkonnekt.model.Vendor;
import com.foodkonnekt.model.Zone;
import com.foodkonnekt.repository.AddressRepository;
import com.foodkonnekt.repository.CategoryItemRepository;
import com.foodkonnekt.repository.CategoryRepository;
import com.foodkonnekt.repository.ConvenienceFeeRepository;
import com.foodkonnekt.repository.CustomerrRepository;
import com.foodkonnekt.repository.ItemModifierGroupRepository;
import com.foodkonnekt.repository.ItemModifiersRepository;
import com.foodkonnekt.repository.ItemTaxRepository;
import com.foodkonnekt.repository.ItemmRepository;
import com.foodkonnekt.repository.MerchantOrdersRepository;
import com.foodkonnekt.repository.MerchantRepository;
import com.foodkonnekt.repository.MerchantSubscriptionRepository;
import com.foodkonnekt.repository.ModifierGroupRepository;
import com.foodkonnekt.repository.ModifierModifierGroupRepository;
import com.foodkonnekt.repository.ModifiersRepository;
import com.foodkonnekt.repository.NotificationMethodRepository;
import com.foodkonnekt.repository.NotificationTrackerRepository;
import com.foodkonnekt.repository.OnlineNotificationStatusRepository;
import com.foodkonnekt.repository.OpeningClosingDayRepository;
import com.foodkonnekt.repository.OpeningClosingTimeRepository;
import com.foodkonnekt.repository.OrderDiscountRepository;
import com.foodkonnekt.repository.OrderItemModifierRepository;
import com.foodkonnekt.repository.OrderItemRepository;
import com.foodkonnekt.repository.OrderPizzaCrustRepository;
import com.foodkonnekt.repository.OrderPizzaRepository;
import com.foodkonnekt.repository.OrderPizzaToppingsRepository;
import com.foodkonnekt.repository.OrderRepository;
import com.foodkonnekt.repository.PickUpTimeRepository;
import com.foodkonnekt.repository.PizzaCrustRepository;
import com.foodkonnekt.repository.PizzaCrustSizeRepository;
import com.foodkonnekt.repository.PizzaSizeRepository;
import com.foodkonnekt.repository.PizzaTemplateCategoryRepository;
import com.foodkonnekt.repository.PizzaTemplateCrustRepository;
import com.foodkonnekt.repository.PizzaTemplateRepository;
import com.foodkonnekt.repository.PizzaTemplateSizeRepository;
import com.foodkonnekt.repository.PizzaTemplateTaxRepository;
import com.foodkonnekt.repository.PizzaTemplateToppingRepository;
import com.foodkonnekt.repository.PizzaToppingRepository;
import com.foodkonnekt.repository.PizzaToppingSizeRepository;
import com.foodkonnekt.repository.PrinterRepository;
import com.foodkonnekt.repository.TaxRateRepository;
import com.foodkonnekt.repository.UberRepository;
import com.foodkonnekt.repository.VendorRepository;
import com.foodkonnekt.repository.ZoneRepository;
import com.foodkonnekt.service.MerchantConfigurationService;
import com.foodkonnekt.service.MerchantService;
import com.foodkonnekt.service.OrderService;
import com.foodkonnekt.service.UberService;
import com.foodkonnekt.util.AppNotification;
import com.foodkonnekt.util.CloudPrintUtil;
import com.foodkonnekt.util.CloverUrlUtil;
import com.foodkonnekt.util.DateUtil;
import com.foodkonnekt.util.EncryptionDecryptionToken;
import com.foodkonnekt.util.EncryptionDecryptionUtil;
import com.foodkonnekt.util.FaxUtility;
import com.foodkonnekt.util.IConstant;
import com.foodkonnekt.util.MailSendUtil;
import com.foodkonnekt.util.OrderUtil;
import com.foodkonnekt.util.ProducerUtil;
import com.foodkonnekt.util.ShortCodeUtil;
import com.foodkonnekt.util.UrlConstant;
import com.google.gson.Gson;

@Service
public class UberserviceImpl implements UberService {

	private static final Logger LOGGER = LoggerFactory.getLogger(UberserviceImpl.class);

	@Autowired
    private Environment environment;
	
	@Autowired
	private AddressRepository addressRepository;
	
	@Autowired
	private PickUpTimeRepository pickUpTimeRepository;

	@Autowired
	private UberRepository uberRepository;

	@Autowired
	private PizzaTemplateCategoryRepository pizzaTemplateCategoryRepository;

	@Autowired
	private PizzaTemplateTaxRepository pizzaTemplateTaxRepository;

	@Autowired
	private PizzaToppingSizeRepository pizzaToppingSizeRepository;

	@Autowired
	private PizzaTemplateSizeRepository pizzaTemplateSizeRepository;

	@Autowired
	private PizzaCrustSizeRepository pizzaCrustSizeRepository;

	@Autowired
	private ItemmRepository itemRepository;

	@Autowired
	private OpeningClosingDayRepository openingClosingDayRepository;

	@Autowired
	private OpeningClosingTimeRepository openingClosingTimeRepository;

	@Autowired
	private TaxRateRepository taxRateRepository;

	@Autowired
	private ModifierGroupRepository modifierGroupRepository;

	@Autowired
	private ItemModifierGroupRepository itemModifierGroupRepository;

	@Autowired
	private ModifiersRepository modifiersRepository;

	@Autowired
	private ModifierModifierGroupRepository modifierModifierGroupRepository;

	@Autowired
	private ItemModifiersRepository itemModifiersRepository;

	@Autowired
	private CategoryRepository categoryRepository;

	@Autowired
	private ItemTaxRepository itemTaxRepository;

	@Autowired
	private CategoryItemRepository categoryItemRepository;

	@Autowired
	private OrderRepository orderRepo;

	@Autowired
	private OrderItemModifierRepository orderItemModifierRepository;

	@Autowired
	private PizzaTemplateCrustRepository pizzaTemplateCrustRepository;
	
	@Autowired
	private MailSendUtil mailSendUtil;
	
	@Autowired
	private ItemmRepository itemmRepository;

	@Autowired
	private OrderService orderService;

	@Autowired
	private OrderItemRepository orderItemRepository;

	@Autowired
	private OrderItemModifierRepository itemModifierRepository;

	@Autowired
	private PizzaSizeRepository pizzaSizeRepository;

	@Autowired
	private MerchantService merchantService;

	@Autowired
	private OpeningClosingDayRepository openingCosingDayRepository;

	@Autowired
	private NotificationMethodRepository methodRepository;

	@Autowired
	private PrinterRepository printerRepository;
	
	@Autowired
	private MerchantRepository merchantRepository;

	@Autowired
	private MerchantSubscriptionRepository merchantSubscriptionRepository;

	@Autowired
	private MerchantOrdersRepository merchantOrdersRepository;
	
	@Autowired
	private PizzaTemplateToppingRepository pizzaTemplateToppingRepository;

	@Autowired
	private OrderPizzaRepository orderPizzaRepository;

	@Autowired
	private OrderPizzaToppingsRepository orderPizzaToppingsRepository;

	@Autowired
	private OnlineNotificationStatusRepository onlineNotificationStatusRepository;
	
	@Autowired
	private PizzaTemplateRepository pizzaTemplateRepository;

	@Autowired
	private PizzaToppingRepository pizzaToppingRepository;

	@Autowired
	private PizzaCrustRepository pizzaCrustRepository;

	@Autowired
	private ZoneRepository zoneRepository;
	
	@Autowired
	private NotificationTrackerRepository notificatonRepo;
	
	@Autowired
	private OrderPizzaCrustRepository orderPizzaCrustRepository;

	@Autowired
	private ConvenienceFeeRepository convenienceFeeRepository;

	@Autowired
	private OrderDiscountRepository orderDiscountRepository;
	
	@Autowired
	private MerchantConfigurationService merchantConfigurationService;

	@Autowired
	private VendorRepository vendorRepository;
	
	@Autowired
	private CustomerrRepository customerRepository;
	
	public static Integer custId;
	public static Integer orderId;
	public static Integer posTypeId = IConstant.POS_FOODTRONIX;
    public static String name1;
	public static String paymentYtpe1;
	public static String orderPosId1;
	public static String subTotal1;
	public static String tax1;
	public static String orderDetails1;
	public static String email1;
	public static Double orderPrice1;
	public static String note1;
	public static String merchantName1;
	public static String merchantLogo1;
	public static String orderType1;
	public static String convenienceFeeValue1 = "0";
	public static String avgTime = "30";
	public static String deliverFee = "0";
	public static Double orderDiscount = 0.0;
	public static Double tipAmount = 0.0;
	public static String listOfCoupons = "";
	public static Integer merchantId;
	public static Integer isfutureorder;
	
	public List<UberStore> findStorebyMerchnatId(Merchant merchant) {
		List<UberStore> uberStore = uberRepository.findByMerchantId(merchant.getId());
		return uberStore;
	}

	public String saveUberStore(UberStore uberStore, Merchant merchant) {
		LOGGER.info("inside UberServiceImpl : saveUberstore started");
		String response = null;
		
		try {
			response = ProducerUtil.validateStore(uberStore.getStoreId());
			
				if (response.equalsIgnoreCase("success")) {
					LOGGER.info("inside UberServiceImpl : Store Id is Valid ");
					UberStore dbUberStore = uberRepository.findByStoreId(uberStore.getStoreId());
					if(dbUberStore !=null) {
						response ="Store Id Exists for Another merchant";
						LOGGER.info("inside UberServiceImpl : Store Id Exists for Another merchant ");
					}else {
					String responseCode = uploadMenuOnUber(merchant.getId(), uberStore.getStoreId());
					if(responseCode != null && responseCode.contains("204")) {
					UberStore store = new UberStore();
					store.setStoreId(uberStore.getStoreId());
					store.setStoreName(uberStore.getStoreName());
					store.setMerchant(merchant);
					uberRepository.save(store);
					response = "Store Details Saved Succesfully";
					LOGGER.info("inside UberServiceImpl : saveUberstore : succesfully saved store details");
					}else {
						LOGGER.info("inside UberServiceImpl : responseCode from uploadmenuOnUber == "+responseCode);
						response = "Failed to save Store Details plz try again";
					}
					
				}
				} 
			

		} catch (Exception e) {
			response = "Failed to save Store Details plz try again";
			LOGGER.info("exception in : saveUberstore : exception : " + e);
		}

		return response;
	}

	public String updateStoreDetails(UberStore uberStore) {
		LOGGER.info("inside UberServiceImpl : updateStoreDetails started");
		String response = null;
		String responseCode = null;
		boolean flag = false;
		try {

			response = ProducerUtil.validateStore(uberStore.getStoreId());
			if(response.equalsIgnoreCase("success")) {
					UberStore dbUberStore = uberRepository.findOne(uberStore.getId());
					UberStore duplicateUberStore = uberRepository.findByStoreIdAndMerchantNot(uberStore.getStoreId(),dbUberStore.getMerchant().getId());
					if(duplicateUberStore !=null) {
						response ="Store Id Exists for Another merchant";
						LOGGER.info("inside UberServiceImpl : updateStoreDetails : Store Id Exists for Another merchant");
					}else {
						if(!dbUberStore.getStoreId().equals(uberStore.getStoreId())) {
							flag= true;
					    responseCode = uploadMenuOnUber(dbUberStore.getMerchant().getId(), uberStore.getStoreId());
					    LOGGER.info("inside UberServiceImpl : updateStoreDetails :responseCode from uploadmenuOnUber == "+responseCode);
					}
						if((flag == true && responseCode!=null && responseCode.contains("204")) || flag == false ) {
					dbUberStore.setStoreId(uberStore.getStoreId());
					dbUberStore.setStoreName(uberStore.getStoreName());
					uberRepository.saveAndFlush(dbUberStore);
					response = "Store Details Updated Succesfully";
					LOGGER.info("inside UberServiceImpl : saveUberstore : succesfully saved store details");
					}else {
						response = "Failed to save Store Details plz try again";
						LOGGER.info("inside UberServiceImpl : updateStoreDetails :responseCode from uploadmenuOnUber == "+responseCode);
					}
					
				}
			}

		} catch (Exception e) {
			response = "Failed to update Store Details plz try again";
			LOGGER.info("exception in : updateStoreDetails : exception : " + e);
		}
		
		return response;
	}

	public String uploadMenuOnUber(Integer merchantId ,String storeId) {
		LOGGER.info(" UberServiceImpl :: uploadMenuOnUber: started:");
		LOGGER.info(" UberServiceImpl :: uploadMenuOnUber: merchantId : "+merchantId+" storeId : "+storeId);
		String response = null;
		JSONObject finalJson = new JSONObject();

		List<JSONObject> itemsJsonlist = new ArrayList<JSONObject>();
		List<JSONObject> modifier_groupsJsonlist = new ArrayList<JSONObject>();
		List<JSONObject> categoryJsonList = new ArrayList<JSONObject>();
		List<JSONObject> menuJsonList = new ArrayList<JSONObject>();

		String[] days = { "monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday" };

		JSONObject itemmenuJson = new JSONObject();
		JSONObject displayOptionjson = new JSONObject();
		JSONObject menutranslationJson = new JSONObject();
		JSONObject menutitleJson = new JSONObject();
		List<String> ItemcategoryForMenulist = new ArrayList<String>();
       
		try {
			LOGGER.info(" UberServiceImpl :: uploadMenuOnUber: inside try block");
		menutranslationJson.put("en_us", "ITEMS");
		menutitleJson.put("translations", menutranslationJson);
		itemmenuJson.put("title", menutitleJson);
		itemmenuJson.put("id", "ITEMS");

		List<OpeningClosingDay> openingClosingDays = openingClosingDayRepository.findByMerchantId(merchantId);
		
		JSONObject serviceAvailabiltiy = new JSONObject();
		List<JSONObject> serviceAvailabiltiyList = new ArrayList<JSONObject>();

		for (OpeningClosingDay openingDays : openingClosingDays) {
			List<OpeningClosingTime> openingclosingTime = openingClosingTimeRepository
					.findByOpeningClosingDayId(openingDays.getId());
			List<JSONObject> timePeriod = new ArrayList<JSONObject>();
			for (OpeningClosingTime time : openingclosingTime) {
				JSONObject startEndTime = new JSONObject();
            if(openingDays.getIsHoliday() == 0) {
				startEndTime.put("start_time", time.getStartTime());
				if(!time.getEndTime().equals("00:00"))
				startEndTime.put("end_time", time.getEndTime());
				else
				startEndTime.put("end_time", "23:59");
               }else {
	startEndTime.put("start_time", "01:00");
	startEndTime.put("end_time", "01:00");
                }
				timePeriod.add(startEndTime);
			}

			JSONObject time_periods = new JSONObject();

			time_periods.put("time_periods", timePeriod);
			time_periods.put("day_of_week", openingDays.getDay().toLowerCase());
			serviceAvailabiltiyList.add(time_periods);
		}

		itemmenuJson.put("service_availability", serviceAvailabiltiyList);
		JSONObject duplicateModJson = new JSONObject();
		JSONObject duplicateToppingJson = new JSONObject();
		JSONObject duplicateCrustJson = new JSONObject();
		JSONObject duplicateItemJson = new JSONObject();
		JSONObject duplicateGrpJson = new JSONObject();
		JSONObject duplicatePizzaJson = new JSONObject();
		JSONObject duplicateSizeJson = new JSONObject();

		List<Category> item_categories = categoryRepository.findByMerchantIdAndIsPizzaAndItemStatus(merchantId,0,0);

		for (Category category : item_categories) {
			LOGGER.info(" UberServiceImpl :: uploadMenuOnUber: category : "+category.getId() +" : "+category.getName());
			
			ItemcategoryForMenulist.add(category.getId().toString());

			JSONObject categoryJson = new JSONObject();
			JSONObject categorytranslationJson = new JSONObject();
			JSONObject categorytitleJson = new JSONObject();
			List<JSONObject> entitiJsonList = new ArrayList<JSONObject>();

			categorytranslationJson.put("en_us", category.getName());
			categorytitleJson.put("translations", categorytranslationJson);
			categoryJson.put("title", categorytitleJson);
			categoryJson.put("id", category.getId().toString());

			List<CategoryItem> dbitemslist = categoryItemRepository.findByCategoryIdAndActiveAnditemStatustaxActive(category.getId());

			LOGGER.info("dbitemslist.size = "+dbitemslist.size());
			
			for (CategoryItem item : dbitemslist) {
				LOGGER.info(" UberServiceImpl :: uploadMenuOnUber: getCategory : "+item.getCategory().getId() +" : "+item.getCategory().getName());
				LOGGER.info(" UberServiceImpl :: uploadMenuOnUber: item : "+item.getItem().getId() +" : "+item.getItem().getName());
				JSONObject entitiJson = new JSONObject();
				List<ItemTax> taxes = itemTaxRepository.findByItemIdAndActive(item.getItem().getId(),1);
				
			entitiJson.put("type", "ITEM");
			entitiJson.put("id", item.getItem().getId().toString());
			entitiJsonList.add(entitiJson);
				
				if (!duplicateItemJson.has("item" + item.getItem().getId())) {
					LOGGER.info(" UberServiceImpl :: inside duplicate : item : "+item.getItem().getId()+" : "+item.getItem().getName());
					List<Integer> suspensionModifier = new ArrayList<Integer>();
					
					JSONObject itemJson = new JSONObject();
					JSONObject itemtranslationJson = new JSONObject();
					JSONObject itemtitleJson = new JSONObject();
					JSONObject itemDescJson = new JSONObject();
					JSONObject itemdesctranslationJson = new JSONObject();
					JSONObject itempriceJson = new JSONObject();
					JSONObject itemtaxJson = new JSONObject();
					JSONObject GrpIdsForItemJson = new JSONObject();
					JSONObject checkItemId = new JSONObject();

					List<String> GrpIdsForItemlist = new ArrayList<String>();

					itemtranslationJson.put("en_us", item.getItem().getName());
					itemtitleJson.put("translations", itemtranslationJson);
					itemdesctranslationJson.put("en_us", item.getItem().getDescription());
					itemDescJson.put("translations", itemdesctranslationJson);
					if (item.getItem().getPrice() != null)
						itempriceJson.put("price", Math.round(item.getItem().getPrice() * 100));
					else
						itempriceJson.put("price", 0);

					itemJson.put("title", itemtitleJson);
					itemJson.put("description", itemDescJson);
					itemJson.put("external_data", "isItem");
					itemJson.put("id", item.getItem().getId().toString());
					itemJson.put("price_info", itempriceJson);

					List<ItemModifierGroup> modifierGroups = itemModifierGroupRepository
							.findByItemIdAndModifierGroupStatus(item.getItem().getId() ,1);

					for (ItemModifierGroup modifierGroup : modifierGroups) {
						
						LOGGER.info(" UberServiceImpl :: uploadMenuOnUber: modifierGroup : "+modifierGroup.getModifierGroup().getId()+" : "+modifierGroup.getModifierGroup().getName());
						
						
							LOGGER.info(" UberServiceImpl :: inside uploadMenuOnUber: modifierGroup : "+modifierGroup.getModifierGroup().getId()+" : "+modifierGroup.getModifierGroup().getName());
							
							JSONObject modifier_groupsJson = new JSONObject();
							JSONObject grptranslationJson = new JSONObject();
							JSONObject grptitleJson = new JSONObject();

							List<JSONObject> modifiersForGroupslist = new ArrayList<JSONObject>();

							grptranslationJson.put("en_us", modifierGroup.getModifierGroup().getName());
							grptitleJson.put("translations", grptranslationJson);

							if ((modifierGroup.getModifiersLimit() != null && modifierGroup.getIsMinLimit() != null)
									&& (modifierGroup.getModifiersLimit() >= modifierGroup.getIsMinLimit())) {
								JSONObject quantity_info = new JSONObject();
								JSONObject quantity = new JSONObject();
								quantity.put("max_permitted", modifierGroup.getModifiersLimit());
								quantity.put("min_permitted", modifierGroup.getIsMinLimit());

								quantity_info.put("quantity", quantity);
								modifier_groupsJson.put("quantity_info", quantity_info);
							}
							modifier_groupsJson.put("title", grptitleJson);
							modifier_groupsJson.put("external_data",
									modifierGroup.getModifierGroup().getId().toString());
							modifier_groupsJson.put("id", modifierGroup.getModifierGroup().getId()+"&"+item.getItem().getId());
							modifier_groupsJson.put("display_type", "collapsed");

							List<ItemModifiers> modifiers = itemModifiersRepository.findByModifierGroupIdAndItemIdAndModifiersStatusAndGrpMapping(modifierGroup.getModifierGroup().getId(), item.getItem().getId(), 1);
									

							for (ItemModifiers modifier : modifiers) {
								JSONObject modifierForGrp = new JSONObject();

								
									JSONObject modifierJson = new JSONObject();
									JSONObject translationJson = new JSONObject();
									JSONObject titleJson = new JSONObject();
									JSONObject priceInfoJson = new JSONObject();
									JSONObject tax_info = new JSONObject();

									translationJson.put("en_us", modifier.getModifiers().getName());
									titleJson.put("translations", translationJson);
									if (modifier.getModifiers().getPrice() != null)
										priceInfoJson.put("price", Math.round(modifier.getModifiers().getPrice() * 100));
									else
										priceInfoJson.put("price", 0);
									tax_info.put("tax_rate", 0);
									modifierJson.put("price_info", priceInfoJson);
									modifierJson.put("external_data", modifier.getModifiers().getId().toString());
									modifierJson.put("tax_info", tax_info);
									modifierJson.put("id", modifier.getModifiers().getId()+"&"+modifier.getModifierGroup().getId()+"&"+modifier.getItem().getId());
									modifierJson.put("title", titleJson);

									itemsJsonlist.add(modifierJson);
									
								
								if(modifier.getModifiers().getStatus() == 1) {
								modifierForGrp.put("type", "ITEM");
								modifierForGrp.put("id", modifier.getModifiers().getId()+"&"+modifier.getModifierGroup().getId()+"&"+modifier.getItem().getId());
								modifiersForGroupslist.add(modifierForGrp);
								}
								

							}
							modifier_groupsJson.put("modifier_options", modifiersForGroupslist);

							modifier_groupsJsonlist.add(modifier_groupsJson);
							
					  if(modifierGroup.getModifierGroupStatus() == 1 && modifierGroup.getModifierGroup().getActive() != null 
							   && modifierGroup.getModifierGroup().getActive() == 1) {
						GrpIdsForItemlist.add(modifierGroup.getModifierGroup().getId()+"&"+item.getItem().getId());
					  }
					}

					for (ItemTax tax : taxes) {
						if (tax != null && tax.getTaxRates() != null)
							itemtaxJson.put("tax_rate", tax.getTaxRates().getRate());
					}

					itemJson.put("tax_info", itemtaxJson);

					GrpIdsForItemJson.put("ids", GrpIdsForItemlist);
					itemJson.put("modifier_group_ids", GrpIdsForItemJson);
					itemsJsonlist.add(itemJson);

						
					duplicateItemJson.put("item" + item.getItem().getId(), item.getItem().getId());
				}
			}
			categoryJson.put("entities", entitiJsonList);
			categoryJsonList.add(categoryJson);

		}

		itemmenuJson.put("category_ids", ItemcategoryForMenulist);

		if(itemmenuJson.length()>0)
		menuJsonList.add(itemmenuJson);

		LOGGER.info("Item Inventory Completed ");
		JSONObject pizzamenuJson = new JSONObject();
		JSONObject pizzamenutranslationJson = new JSONObject();
		JSONObject pizzamenutitleJson = new JSONObject();
		List<String> pizzaCategoryIdsForMenulist = new ArrayList<String>();

		pizzamenutranslationJson.put("en_us", "PIZZAS");
		pizzamenutitleJson.put("translations", pizzamenutranslationJson);
		pizzamenuJson.put("title", pizzamenutitleJson);
		pizzamenuJson.put("id", "PIZZAS");

		List<Category> pizza_categories = categoryRepository.findByMerchantIdAndIsPizzaAndItemStatus(merchantId,1,0);

		
		for (Category category : pizza_categories) {
			LOGGER.info("pizza category : "+category.getId()+" : "+category.getName());
			
			pizzaCategoryIdsForMenulist.add(category.getId().toString());

			JSONObject categoryJson = new JSONObject();
			JSONObject categorytranslationJson = new JSONObject();
			JSONObject categorytitleJson = new JSONObject();
			List<JSONObject> entitiJsonList = new ArrayList<JSONObject>();

			categorytranslationJson.put("en_us", category.getName());
			categorytitleJson.put("translations", categorytranslationJson);
			categoryJson.put("title", categorytitleJson);
			categoryJson.put("id", category.getId().toString());

			List<PizzaTemplateCategory> dbpizzalist = pizzaTemplateCategoryRepository
					.findByCategoryIdAndPizzaTemplateActiveAndTax(category.getId(),1,1);
			
			for (PizzaTemplateCategory pizza : dbpizzalist) {
				LOGGER.info("pizza Template : "+pizza.getPizzaTemplate().getId()+" : "+pizza.getPizzaTemplate().getDescription());
				JSONObject templateId = new JSONObject();
				JSONObject entitiJson = new JSONObject();
				List<PizzaTemplateTax> taxes = pizzaTemplateTaxRepository
						.findByPizzaTemplateIdAndIsActive(pizza.getPizzaTemplate().getId() ,1);
			
					entitiJson.put("type", "ITEM");
					entitiJson.put("id", pizza.getPizzaTemplate().getId().toString());
					entitiJsonList.add(entitiJson);
					
				if (!duplicatePizzaJson.has("pizza" + pizza.getPizzaTemplate().getId())) {
					LOGGER.info("Inside pizza Template : "+pizza.getPizzaTemplate().getId()+" : "+pizza.getPizzaTemplate().getDescription());
					
					JSONObject itemJson = new JSONObject();
					JSONObject itemtranslationJson = new JSONObject();
					JSONObject itemtitleJson = new JSONObject();
					JSONObject itemDescJson = new JSONObject();
					JSONObject itemdesctranslationJson = new JSONObject();
					JSONObject itempriceJson = new JSONObject();
					JSONObject itemtaxJson = new JSONObject();
					List<JSONObject> sizeOverrideList = new ArrayList<JSONObject>();

					JSONObject GrpIdsForItemJson = new JSONObject();

					List<String> GrpIdsForItemlist = new ArrayList<String>();
					
					

					itemtranslationJson.put("en_us", pizza.getPizzaTemplate().getDescription());
					itemtitleJson.put("translations", itemtranslationJson);
					itemdesctranslationJson.put("en_us", pizza.getPizzaTemplate().getDescription2());
					itemDescJson.put("translations", itemdesctranslationJson);

					itemJson.put("title", itemtitleJson);
					itemJson.put("description", itemDescJson);
					itemJson.put("external_data", "isPizza");
					itemJson.put("id", pizza.getPizzaTemplate().getId().toString());
					
					List<PizzaTemplateSize> sizes = pizzaTemplateSizeRepository
							.findByPizzaTemplateIdAndActiveAndPizzaSizeActive(pizza.getPizzaTemplate().getId(),1 ,1);

					JSONObject size_modifier_groupsJson = new JSONObject();
					JSONObject grptranslationJson = new JSONObject();
					JSONObject grptitleJson = new JSONObject();

					List<JSONObject> size_modifiersForGroupslist = new ArrayList<JSONObject>();

					grptranslationJson.put("en_us", "Sizes");
					grptitleJson.put("translations", grptranslationJson);

					JSONObject mod = new JSONObject();
					JSONObject quantity_info = new JSONObject();
					JSONObject quantity = new JSONObject();
					quantity.put("max_permitted", 1);
					quantity.put("min_permitted", 1);

					quantity_info.put("quantity", quantity);

					size_modifier_groupsJson.put("title", grptitleJson);
					size_modifier_groupsJson.put("external_data",
							"size_" + pizza.getPizzaTemplate().getId().toString());
					size_modifier_groupsJson.put("id", "size_" + pizza.getPizzaTemplate().getId().toString());
					size_modifier_groupsJson.put("display_type", "collapsed");
					size_modifier_groupsJson.put("quantity_info", quantity_info);

					for (PizzaTemplateSize size : sizes) {
						LOGGER.info("pizza size : "+size.getPizzaSize().getId()+" : "+size.getPizzaSize().getDescription());
						
					JSONObject size_modifierForGrp = new JSONObject();

						
							LOGGER.info("Inside pizza size : "+size.getPizzaSize().getId()+" : "+size.getPizzaSize().getDescription());
							
							JSONObject size_modifierJson = new JSONObject();
							JSONObject translationJson = new JSONObject();
							JSONObject titleJson = new JSONObject();
							JSONObject sizepriceInfoJson = new JSONObject();
							JSONObject tax_info = new JSONObject();
							JSONObject GrpIdsForSizeJson = new JSONObject();
							List<String> GrpIdsForSizelist = new ArrayList<String>();

							translationJson.put("en_us", size.getPizzaSize().getDescription());
							titleJson.put("translations", translationJson);

							List<JSONObject> size_priceoverridelist = new ArrayList<JSONObject>();
							JSONObject size_priceoverride = new JSONObject();

							size_priceoverride.put("context_type", "ITEM");
							size_priceoverride.put("context_value", pizza.getPizzaTemplate().getId().toString());
							if (new Double(size.getPrice()) != null)
								size_priceoverride.put("price", Math.round(size.getPrice() * 100));
							else
								size_priceoverride.put("price", 0);

							size_priceoverridelist.add(size_priceoverride);
							sizepriceInfoJson.put("overrides", size_priceoverridelist);

							tax_info.put("tax_rate", 0);
							size_modifierJson.put("price_info", sizepriceInfoJson);
							size_modifierJson.put("external_data", size.getPizzaSize().getId().toString());
							size_modifierJson.put("tax_info", tax_info);
							size_modifierJson.put("id", size.getPizzaSize().getId()+"&"+pizza.getPizzaTemplate().getId());
							size_modifierJson.put("title", titleJson);

							List<PizzaToppingSize> toppings = pizzaToppingSizeRepository
									.findByPizzaSizeIdAndActiveAndPizzaToppingActive(size.getPizzaSize().getId() ,0,1);

							List<PizzaCrustSizes> crusts = pizzaCrustSizeRepository
									.findByPizzaCrustIdAndActiveAndPizzaSizeActive(size.getPizzaSize().getId(),pizza.getPizzaTemplate().getId());

							JSONObject topping_modifier_groupsJson = new JSONObject();
							JSONObject topping_grptranslationJson = new JSONObject();
							JSONObject topping_grptitleJson = new JSONObject();

							List<JSONObject> topping_modifiersForGroupslist = new ArrayList<JSONObject>();

							topping_grptranslationJson.put("en_us", "Pizza Topping");
							topping_grptitleJson.put("translations", topping_grptranslationJson);

							topping_modifier_groupsJson.put("title", topping_grptitleJson);
							topping_modifier_groupsJson.put("external_data",
									"topping_" + size.getPizzaSize().getId().toString());
							topping_modifier_groupsJson.put("id", "topping_" + size.getPizzaSize().getId()+"&"+pizza.getPizzaTemplate().getId());
							topping_modifier_groupsJson.put("display_type", "collapsed");

							for (PizzaToppingSize topping : toppings) {
								LOGGER.info("pizza topping : "+topping.getPizzaTopping().getId()+" : "+topping.getPizzaTopping().getDescription());
								
								PizzaTemplateTopping pizzaTemplateTopping = pizzaTemplateToppingRepository
										.findByPizzaTemplateIdAndPizzaToppingId(pizza.getPizzaTemplate().getId(),
												topping.getPizzaTopping().getId());

								JSONObject topping_modifierForGrp = new JSONObject();
								
									LOGGER.info("Inside pizza topping : "+topping.getPizzaTopping().getId()+" : "+topping.getPizzaTopping().getDescription());
									
									JSONObject modifierJson = new JSONObject();
									JSONObject toppingtranslationJson = new JSONObject();
									JSONObject toppingtitleJson = new JSONObject();
									JSONObject toppingpriceInfoJson = new JSONObject();
									JSONObject toppingtax_info = new JSONObject();

									toppingtranslationJson.put("en_us", topping.getPizzaTopping().getDescription());
									toppingtitleJson.put("translations", toppingtranslationJson);

									List<JSONObject> topping_priceoverridelist = new ArrayList<JSONObject>();

									JSONObject topping_quantity_info = new JSONObject();
									List<JSONObject> topping_quantityoverridelist = new ArrayList<JSONObject>();

									if (pizzaTemplateTopping != null && pizzaTemplateTopping.isIncluded() == true) {

										JSONObject topping_quantityoverride = new JSONObject();
										JSONObject topping_quantity = new JSONObject();
										topping_quantity.put("default_quantity", 1);
										topping_quantityoverride.put("context_type", "ITEM");
										topping_quantityoverride.put("context_value", size.getPizzaSize().getId()+"&"+pizza.getPizzaTemplate().getId());
										topping_quantityoverride.put("quantity", topping_quantity);

										topping_quantityoverridelist.add(topping_quantityoverride);

										JSONObject topping_priceoverride = new JSONObject();

										topping_priceoverride.put("context_type", "ITEM");
										topping_priceoverride.put("context_value", size.getPizzaSize().getId()+"&"+pizza.getPizzaTemplate().getId());
										
											topping_priceoverride.put("price", 0);

										topping_priceoverridelist.add(topping_priceoverride);
										
									} else {

									JSONObject topping_priceoverride = new JSONObject();

									topping_priceoverride.put("context_type", "ITEM");
									topping_priceoverride.put("context_value", size.getPizzaSize().getId()+"&"+pizza.getPizzaTemplate().getId());
									if (new Double(topping.getPrice()) != null)
										topping_priceoverride.put("price", Math.round(topping.getPrice() * 100));
									else
										topping_priceoverride.put("price", 0);

									topping_priceoverridelist.add(topping_priceoverride);

									 }

									topping_quantity_info.put("overrides", topping_quantityoverridelist);
									toppingpriceInfoJson.put("overrides", topping_priceoverridelist);

									toppingpriceInfoJson.put("price", 0);

									toppingtax_info.put("tax_rate", 0);

									modifierJson.put("price_info", toppingpriceInfoJson);
									modifierJson.put("external_data", topping.getPizzaTopping().getId().toString());
									modifierJson.put("tax_info", toppingtax_info);
									modifierJson.put("id", topping.getPizzaTopping().getId()+"&"+size.getPizzaSize().getId()+"&"+pizza.getPizzaTemplate().getId());
									modifierJson.put("title", toppingtitleJson);
									modifierJson.put("quantity_info", topping_quantity_info);

									itemsJsonlist.add(modifierJson);

						

								topping_modifierForGrp.put("type", "ITEM");
								topping_modifierForGrp.put("id", topping.getPizzaTopping().getId()+"&"+size.getPizzaSize().getId()+"&"+pizza.getPizzaTemplate().getId());
								topping_modifiersForGroupslist.add(topping_modifierForGrp);
								
							}

							topping_modifier_groupsJson.put("modifier_options", topping_modifiersForGroupslist);

							JSONObject crust_modifier_groupsJson = new JSONObject();
							JSONObject crust_grptranslationJson = new JSONObject();
							JSONObject crust_grptitleJson = new JSONObject();

							List<JSONObject> crust_modifiersForGroupslist = new ArrayList<JSONObject>();

							crust_grptranslationJson.put("en_us", "Pizza Crust");
							crust_grptitleJson.put("translations", crust_grptranslationJson);

							
							crust_modifier_groupsJson.put("title", crust_grptitleJson);
							crust_modifier_groupsJson.put("external_data",
									"crust_" + size.getPizzaSize().getId().toString());
							crust_modifier_groupsJson.put("id", "crust_" + size.getPizzaSize().getId()+"&"+pizza.getPizzaTemplate().getId());
							crust_modifier_groupsJson.put("display_type", "collapsed");
							

							for (PizzaCrustSizes crust : crusts) {
								LOGGER.info(" pizza crust : "+crust.getPizzaCrust().getId()+" : "+crust.getPizzaCrust().getDescription());
								
								JSONObject crust_modifierForGrp = new JSONObject();

								
									LOGGER.info("Inside pizza crust : "+crust.getPizzaCrust().getId()+" : "+crust.getPizzaCrust().getDescription());
									JSONObject modifierJson = new JSONObject();
									JSONObject crusttranslationJson = new JSONObject();
									JSONObject crusttitleJson = new JSONObject();
									JSONObject crustpriceInfoJson = new JSONObject();
									JSONObject crusttax_info = new JSONObject();

									crusttranslationJson.put("en_us", crust.getPizzaCrust().getDescription());
									crusttitleJson.put("translations", crusttranslationJson);
									crustpriceInfoJson.put("price", 0);

									List<JSONObject> crust_priceoverridelist = new ArrayList<JSONObject>();
									JSONObject crust_priceoverride = new JSONObject();

									crust_priceoverride.put("context_type", "ITEM");
									crust_priceoverride.put("context_value", size.getPizzaSize().getId()+"&"+pizza.getPizzaTemplate().getId());
									if (new Double(crust.getPrice()) != null)
										crust_priceoverride.put("price", Math.round(crust.getPrice() * 100));
									else
										crust_priceoverride.put("price", 0);

									crust_priceoverridelist.add(crust_priceoverride);

									crustpriceInfoJson.put("overrides", crust_priceoverridelist);

									crusttax_info.put("tax_rate", 0);
									modifierJson.put("price_info", crustpriceInfoJson);
									modifierJson.put("external_data", crust.getPizzaCrust().getId().toString());
									modifierJson.put("tax_info", crusttax_info);
									modifierJson.put("id", crust.getPizzaCrust().getId()+"&"+size.getPizzaSize().getId()+"&"+pizza.getPizzaTemplate().getId());
									modifierJson.put("title", crusttitleJson);

									itemsJsonlist.add(modifierJson);

									
                 
								crust_modifierForGrp.put("type", "ITEM");
								crust_modifierForGrp.put("id", crust.getPizzaCrust().getId()+"&"+size.getPizzaSize().getId()+"&"+pizza.getPizzaTemplate().getId());
								crust_modifiersForGroupslist.add(crust_modifierForGrp);
								
							}
							if(crust_modifiersForGroupslist.size()>0 ) {
								
							JSONObject crust_quantity_info = new JSONObject();
							JSONObject crust_quantity = new JSONObject();
							crust_quantity.put("max_permitted", 1);
							crust_quantity.put("min_permitted", 1);

							crust_quantity_info.put("quantity", crust_quantity);
							
							
							crust_modifier_groupsJson.put("quantity_info", crust_quantity_info);
							}
							
							crust_modifier_groupsJson.put("modifier_options", crust_modifiersForGroupslist);

							modifier_groupsJsonlist.add(topping_modifier_groupsJson);
							modifier_groupsJsonlist.add(crust_modifier_groupsJson);

							GrpIdsForSizelist.add("topping_" + size.getPizzaSize().getId()+"&"+pizza.getPizzaTemplate().getId());
							GrpIdsForSizelist.add("crust_" + size.getPizzaSize().getId()+"&"+pizza.getPizzaTemplate().getId());

							GrpIdsForSizeJson.put("ids", GrpIdsForSizelist);
							size_modifierJson.put("modifier_group_ids", GrpIdsForSizeJson);
							itemsJsonlist.add(size_modifierJson);
		
						
						size_modifierForGrp.put("type", "ITEM");
						size_modifierForGrp.put("id", size.getPizzaSize().getId()+"&"+pizza.getPizzaTemplate().getId());
						size_modifiersForGroupslist.add(size_modifierForGrp);
						

					}

					 
						
					size_modifier_groupsJson.put("modifier_options", size_modifiersForGroupslist);

					modifier_groupsJsonlist.add(size_modifier_groupsJson);

					GrpIdsForItemlist.add("size_" + pizza.getPizzaTemplate().getId().toString());

					GrpIdsForItemJson.put("ids", GrpIdsForItemlist);
					// modifier_groupsJson.put("quantity_info",quantityInfo);
					// itempriceJson.put("overrides", sizeOverrideList);
					itemJson.put("price_info", itempriceJson);

					for (PizzaTemplateTax tax : taxes) {
						if (tax != null && tax.getTaxRates() != null)
							itemtaxJson.put("tax_rate", tax.getTaxRates().getRate());
					}

					itemJson.put("tax_info", itemtaxJson);

					itemJson.put("modifier_group_ids", GrpIdsForItemJson);
					itemsJsonlist.add(itemJson);
					
				  
					
					duplicatePizzaJson.put("pizza" + pizza.getPizzaTemplate().getId(),
							pizza.getPizzaTemplate().getId());
				}
			}
			categoryJson.put("entities", entitiJsonList);
			categoryJsonList.add(categoryJson);

		}

		LOGGER.info("Pizza Inventory Completed ");
		pizzamenuJson.put("category_ids", pizzaCategoryIdsForMenulist);
		pizzamenuJson.put("service_availability", serviceAvailabiltiyList);
		menuJsonList.add(pizzamenuJson);
        LOGGER.info("itemsJsonlist : "+itemsJsonlist);
		finalJson.put("items", itemsJsonlist);
		finalJson.put("categories", categoryJsonList);
		finalJson.put("modifier_groups", modifier_groupsJsonlist);
		finalJson.put("menus", menuJsonList);
		finalJson.put("display_options", displayOptionjson);
		
		LOGGER.info("UberServiceImpl :: uploadMenuOnUber: Inventoryjson To Upload :"+finalJson.toString());
		
		 response = ProducerUtil.uploadUberMenu(finalJson.toString() ,storeId);
	}catch(Exception e) {
		LOGGER.info("UberServiceImpl :: uploadMenuOnUber: exception :"+e);
	}
		return response;
	}

	public String getOrderDetails(String orderNotification) {
		LOGGER.info("UberServiceImpl :: getOrderDetails: started");
		
		JSONObject jsonObject = new JSONObject(orderNotification);
		StringBuffer getUberOrderUrl = null;
		String resource_href = null;
		String orderDetail = null;
		try {
		if (jsonObject.has("resource_href")) {
			resource_href = jsonObject.getString("resource_href");
			getUberOrderUrl = new StringBuffer(resource_href);
			if(getUberOrderUrl.toString().contains("v1/") && getUberOrderUrl.toString().contains("orders")) {
	    		   getUberOrderUrl.replace(21, 24, "v2/");
		    	   getUberOrderUrl.replace(29, 35, "order");
		    	   LOGGER.info("inside if  ::"+getUberOrderUrl.toString());
		    	   }else {
		    	   LOGGER.info(getUberOrderUrl.toString());
		    	   }
			LOGGER.info("UberServiceImpl :: getOrderDetails: getUberOrderUrl : "+getUberOrderUrl.toString());  
			orderDetail = ProducerUtil.getUberOrderDetails(getUberOrderUrl.toString());
			
			LOGGER.info("UberServiceImpl :: getOrderDetails: orderDetail : "+orderDetail);  
		} else
			LOGGER.info("didn't get the url ");
		}catch (Exception e) {
			LOGGER.info("UberServiceImpl :: getOrderDetails: exception :"+e);
		}
		return orderDetail;
	}

	public String updateOrderDetails(String orderDetails) {
		LOGGER.info("UberServiceImpl :: updateOrderDetails : started ");
		JSONObject jObject = new JSONObject(orderDetails);

		String uberOrderId = jObject.getString("id");
		String response = "success";
		OrderR orderR = null;
		Customer newcustomer = null;
		OrderR orderCheck = orderRepo.findByUberorderId(uberOrderId);
		if(orderCheck ==null){
		JSONObject storeJson = jObject.getJSONObject("store");
		String orderState = jObject.getString("current_state");
		JSONObject eaterJson = jObject.getJSONObject("eater");
		String first_name = null;
		String last_name = null;
		String timeZoneCode="America/Chicago";
		
		if(eaterJson.has("first_name"))
		 first_name = eaterJson.getString("first_name");
		if(eaterJson.has("first_name"))
		 last_name = eaterJson.getString("last_name");
		
		
		
		boolean denied = false;
		boolean failed = false;

		SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
		long diffMinutes =45;
		Date currentDate = null;
		Date fullFiledfDate =null;
		
		String storeId = storeJson.getString("id");
		Merchant merchant = merchantRepository.findByUberStoreId(storeId);

		try {
			if (merchant != null && merchant.getTimeZone() != null
					&& merchant.getTimeZone().getTimeZoneCode() != null) {

				timeZoneCode=merchant.getTimeZone().getTimeZoneCode();
					LOGGER.info("UberServiceImpl :: updateOrderDetails : merchantTimeZone " + merchant.getTimeZone().getTimeZoneName());
			}
			currentDate = DateUtil.getCurrentDateForTimeZonee(timeZoneCode);
			fullFiledfDate =DateUtil.getCurrentDateForTimeZonee(timeZoneCode);
			
			if(currentDate==null)
				currentDate=new Date();
			if(fullFiledfDate==null)
			{
				fullFiledfDate=new Date();
			}
			fullFiledfDate.setMinutes(fullFiledfDate.getMinutes()+30);
			
     if(currentDate != null && fullFiledfDate !=null) {
			
			long diff = fullFiledfDate.getTime() - currentDate.getTime();
			diffMinutes = diff / (60 * 1000) % 60;
			LOGGER.info("dif = " + diffMinutes);
      }
		} catch (Exception e1) {
			failed=true;
			e1.printStackTrace();
		}

		
		String storeAddress = null;
		String country = null;
		try {
			String storeDetails = ProducerUtil.getStore(storeId);
			JSONObject storeDetailsJson= new JSONObject(storeDetails);
			if(storeDetailsJson.has("location")) {
			JSONObject locationJson= storeDetailsJson.getJSONObject("location");
			storeAddress = locationJson.getString("address");
			country = locationJson.getString("country");
			}
		}catch (Exception e) {
			LOGGER.info("Exception in updateOrderDetails : "+e);
		}
		
		if(merchant != null) {
		JSONObject payment = jObject.getJSONObject("payment");
		JSONObject charges = payment.getJSONObject("charges");
		JSONObject totalJson = charges.getJSONObject("total");
		JSONObject subTotalJson = charges.getJSONObject("sub_total");
		JSONObject taxJson = null;
		if(charges.has("tax")) {
		taxJson = charges.getJSONObject("tax");
		}
		JSONObject totalFeeJson = charges.getJSONObject("total_fee");
		JSONObject cart = jObject.getJSONObject("cart");
		JSONArray itemsJsonArray = cart.getJSONArray("items");
		 String orderNote = null;
		if(cart.has("special_instructions")) 
        orderNote = cart.getString("special_instructions");
		String orderType = null;
		String convenience = "0.0";
		if (jObject.has("type")) {
			String type = jObject.getString("type");
			if (type.equalsIgnoreCase("DELIVERY_BY_UBER") || type.equalsIgnoreCase("DELIVERY_BY_RESTAURANT"))
				orderType = "delivery";
			else if (type.equalsIgnoreCase("PICK_UP"))
				orderType = "pickUp";
		}

		Double price = null;
		if (totalJson.has("amount"))
			price = new Double(totalJson.getInt("amount"));
		
		LOGGER.info("dollar amount : " + price);

		String tax = "0.0";
		String totaltax = "0.0";
		if(taxJson != null) {
		if (taxJson.has("formatted_amount"))
			tax = taxJson.getString("formatted_amount").replace("$", "");
		LOGGER.info("tax : " + tax);

		if (totalFeeJson.has("formatted_amount"))
			totaltax = taxJson.getString("formatted_amount").replace("$", "");
		LOGGER.info("totaltax : " + totaltax);
	}
		String subTotal = null;
		if (subTotalJson.has("formatted_amount"))
		subTotal = subTotalJson.getString("formatted_amount").replace("$", "");

		
		newcustomer = customerRepository.findByCustomertypeAndMerchantId( "Uber-Eats" , merchant.getId());
		if(newcustomer ==null) {
			Customer customer = new Customer();
			 customer.setFirstName(first_name);
			 customer.setLastName(last_name);
			 customer.setEmailId("uber@ubereats.com");
			 customer.setCustomerType("Uber-Eats");
			 customer.setPhoneNumber(merchant.getPhoneNumber());
			 customer.setMerchantt(merchant);
			 customer.setCreatedDate(currentDate.toString());
			 customer.setUpdatedDate(currentDate.toString());
			 //newcustomer = customerRepo.save(customer);
			 LOGGER.info("currentDate.toString() : "+currentDate.toString());
			 newcustomer = customerRepository.save(customer);
			 
			}
		
		
		List<Address> addresses = addressRepository.findByCustomerId(newcustomer.getId());
		Address savedAddress = null;
		
		if(!(addresses.size() >0) && storeAddress !=null) {
			Address addressobj = new Address();
			int k = storeAddress.indexOf(" ", storeAddress.lastIndexOf(" "));
			String zip = storeAddress.substring(k);
			storeAddress = storeAddress.substring(0, k).trim();
			LOGGER.info("Full Storee Address"+storeAddress);
			LOGGER.info("zip"+zip);
			
			int k1 = storeAddress.indexOf(" ", storeAddress.lastIndexOf(" "));
			String state = storeAddress.substring(k1);
			storeAddress = storeAddress.substring(0, k1).trim();
			LOGGER.info("state"+state);
			
			int k2 = storeAddress.indexOf(" ", storeAddress.lastIndexOf(" "));
			String city = storeAddress.substring(k2);
			storeAddress = storeAddress.substring(0, k2).trim();
			LOGGER.info("city"+city);
			
			int k3 = storeAddress.indexOf(" ", storeAddress.lastIndexOf(" "));
			storeAddress = storeAddress.substring(0, k3).trim();
			LOGGER.info("storeAddress"+storeAddress);
			
			addressobj.setZip(zip);
			addressobj.setState(state);
			addressobj.setCity(city);
			addressobj.setCountry(country);
			addressobj.setAddress1(storeAddress);
			addressobj.setMerchant(merchant);
			addressobj.setCustomer(newcustomer);
			
			savedAddress = addressRepository.save(addressobj);
			LOGGER.info("saved Address Object successfully");
		}else {
			for(Address address : addresses) {
				savedAddress = address;
			}
		}
		
		
		
		if (merchant.getOwner() != null && merchant.getOwner().getPos() != null
				&& merchant.getOwner().getPos().getPosId() != null && merchant.getOwner().getPos().getPosId() == 1) {

			LOGGER.info("inside clover 1");
			JSONObject finalJson = new JSONObject();
			JSONObject orderVO = new JSONObject();
			JSONObject employee = new JSONObject();
			List<JSONObject> customerList = new ArrayList<JSONObject>();
			JSONObject customers = new JSONObject();

			List<JSONObject> orderItemVOsList = new ArrayList<JSONObject>();
			

			employee.put("id", merchant.getEmployeePosId());
			
			for (Object object : itemsJsonArray) {
				JSONObject itemJsonobject = (JSONObject) object;
				JSONObject orderItemVOs = new JSONObject();
				JSONObject item = new JSONObject();

				Item itemObject = itemRepository.findByIdAndMerchantId(Integer.parseInt(itemJsonobject.getString("id")),merchant.getId());
				

				item.put("id", itemObject.getPosItemId());
				item.put("itemPosId", itemObject.getPosItemId());
				item.put("extraCharge", false);
				item.put("isPizza", false);

				List<JSONObject> modificationsList = new ArrayList<JSONObject>();
			
				JSONArray modifierGroupJsonArray = null;
				if((!itemJsonobject.isNull("selected_modifier_groups")) && itemJsonobject.has("selected_modifier_groups")) {
					 modifierGroupJsonArray = itemJsonobject.getJSONArray("selected_modifier_groups");
				}
				
				if(modifierGroupJsonArray != null) {
					for (Object object1 : modifierGroupJsonArray) {
						JSONObject modifierGroupJsonobject = (JSONObject) object1;
						JSONArray modifiersJsonArray = modifierGroupJsonobject.getJSONArray("selected_items");
						for (Object object2 : modifiersJsonArray) {
							JSONObject modifiersJsonobject = (JSONObject) object2;
							Modifiers modfier = modifiersRepository.findByIdAndMerchantId(Integer.parseInt(modifiersJsonobject.getString("external_data")),merchant.getId());
							
							JSONObject modifications = new JSONObject();
							JSONObject modifierJson = new JSONObject();
							modifierJson.put("id", modfier.getPosModifierId());
							modifications.put("modifier", modifierJson);
							modificationsList.add(modifications);
						}
					}
			}
					orderItemVOs.put("unitQty", itemJsonobject.getInt("quantity"));
					orderItemVOs.put("discountAmount", "");
					orderItemVOs.put("quantitySold", itemJsonobject.getInt("quantity"));
					orderItemVOs.put("item", item);
					orderItemVOs.put("modifications", modificationsList);
					orderItemVOsList.add(orderItemVOs);
				}

			customers.put("id", newcustomer.getId().toString());
			customerList.add(customers);
			orderVO.put("total", String.valueOf(totalJson.getInt("amount")));
			orderVO.put("state", "Open");
			orderVO.put("title", "");
			orderVO.put("testMode", false);
			orderVO.put("note", "Special Instructions : ");
			orderVO.put("currency", totalJson.getString("currency_code"));
			orderVO.put("taxRemoved", false);
			orderVO.put("customers", customerList);
			orderVO.put("employee", employee);

			finalJson.put("itemsForDiscount", "");
			finalJson.put("listOfALLDiscounts", "");
			finalJson.put("orderVO", orderVO);
			finalJson.put("orderItemVOs", orderItemVOsList);
			
			LOGGER.info("finalJson ==  "+finalJson);
			String result = ProducerUtil.placeOrder(finalJson.toString(), merchant,environment);
		
			 if (result != null && result.contains("title") && result.startsWith("{")) {
                 JSONObject jObject1 = new JSONObject(result);
			orderR = OrderUtil.getObjectFromJson(jObject1, null, merchant);
			if(savedAddress != null)
				orderR.setAddressId(savedAddress.getId());
				else
				orderR.setAddressId(null);	
			orderR.setZoneId(null);
			
			 }
		}
		else {
		orderR = new OrderR();
		
		orderR.setCustomer(newcustomer);
	
		orderR.setMerchant(merchant);
		if(savedAddress != null)
		orderR.setAddressId(savedAddress.getId());
		else
		orderR.setAddressId(null);	
		
		orderR.setZoneId(null);

		String instruction = "";
		orderR.setOrderNote(instruction);
		
		orderR.setIsFutureOrder(0);
		
		
		
		if(orderState.equalsIgnoreCase("CREATED"))
		orderR.setIsDefaults(0);
		if(orderState.equalsIgnoreCase("ACCEPTED"))
			orderR.setIsDefaults(1);
		if(orderState.equalsIgnoreCase("DENIED"))
			orderR.setIsDefaults(2);
		if(orderState.equalsIgnoreCase("FINISHED"))
			orderR.setIsDefaults(1);
		if(orderState.equalsIgnoreCase("CANCELLED"))
			orderR.setIsDefaults(3);

		orderR.setOrderNote(orderNote);
		orderR.setSalesTax(tax);
		orderR.setTax(totaltax);

		orderR.setOrderPrice(price / 100.0);
		orderR.setSubTotal(subTotal);
		orderRepo.save(orderR);
		orderR.setOrderPosId(Integer.toString(orderR.getId()));
		LOGGER.info("orderSaved For First Time : "+orderR.getId());
		}
		if (null != orderR) {
			try {

				orderR.setCreatedOn(currentDate);
				orderR.setFulfilled_on(fullFiledfDate);
	        
	        orderR.setUberorderId(uberOrderId);
			orderR.setSalesTax(tax);
			orderR.setTax(totaltax);
           
			orderR.setSubTotal(subTotal);
			orderR.setConvenienceFee(convenience);
			orderR.setPaymentMethod("Cash");
			orderR.setDeliveryFee("0");
			orderR.setOrderDiscount(0.0);
			orderR.setOrderAvgTime(String.valueOf(diffMinutes));
			orderR.setOrderType(orderType);
			orderRepo.save(orderR);
			LOGGER.info("orderSaved For Second Time");
			DecimalFormat df2 = new DecimalFormat("###.##");
			DecimalFormat df3 = new DecimalFormat("###.###");
			DecimalFormat df4 = new DecimalFormat("###.####");

			Map<String, Double> taxMap = new HashMap<String, Double>();
			for (Object object : itemsJsonArray) {
				JSONObject itemJsonobject = (JSONObject) object;
				
				JSONObject itemprice = itemJsonobject.getJSONObject("price");
				JSONObject item_unit_price = itemprice.getJSONObject("unit_price");
				String itemAmount = null;
				if (item_unit_price.has("formatted_amount"))
					itemAmount = item_unit_price.getString("formatted_amount").replace("$", "");
				
				Item itemObject = itemRepository.findByIdAndMerchantId(Integer.parseInt(itemJsonobject.getString("id")),merchant.getId());
				
				if ((itemJsonobject.getString("external_data").equalsIgnoreCase("isItem"))) {
					if(itemJsonobject.has("special_instructions"))
					LOGGER.info(itemJsonobject.getString("special_instructions"));

					OrderItem orderItem = new OrderItem();
					Item item2 = null;
					if (merchant != null && merchant.getOwner() != null && merchant.getOwner().getPos() != null
							&& merchant.getOwner().getPos().getPosId() != null
							&& (merchant.getOwner().getPos().getPosId() == IConstant.NON_POS
									|| merchant.getOwner().getPos().getPosId() == IConstant.FOCUS)) {
						Integer itemId = 0;
						try {

							itemId = Integer.parseInt(itemJsonobject.getString("id"));
							item2 = itemmRepository.findByIdAndMerchantId(itemId, merchant.getId());

						} catch (Exception e) {
							LOGGER.error("OrderServiceImpl :: savePizzaOrder : Exception " + e.getMessage());
							MailSendUtil.sendExceptionByMail(e,environment);
						}
					} else {

				item2 = itemmRepository.findByPosItemIdAndMerchantId(itemObject.getPosItemId(),
							merchant.getId());

					}
					if (item2 != null && item2.getAuxTaxAble() != null && item2.getAuxTaxAble()) {
						OrderType orderTypes = orderService.findByMerchantIdAndLabel(merchant.getId(), orderType);
						if (orderTypes != null && orderTypes.getAuxTax() != null && orderTypes.getAuxTax() != 0) {
							double totalItemAuxTax = (item2.getPrice() / 100) * orderTypes.getAuxTax();
							orderItem.setAuxTax(String.valueOf(df3.format(totalItemAuxTax)));
							if (taxMap.containsKey("auxTaxValue")) {
								Double total = taxMap.get("auxTaxValue")
										+ (item2.getPrice() * itemJsonobject.getInt("quantity"));
								taxMap.put("auxTaxValue", total);
							} else {
								taxMap.put("auxTaxValue",
										item2.getPrice() * itemJsonobject.getInt("quantity"));
							}
						}
					}

					if (item2 != null && item2.getTaxAble() != null && item2.getTaxAble()) {
						OrderType orderTypes = orderService.findByMerchantIdAndLabel(merchant.getId(), orderType);
						if (orderTypes != null && orderTypes.getSalesTax() != null && orderTypes.getSalesTax() != 0) {
							double totalItemTax = (item2.getPrice() / 100) * orderTypes.getSalesTax();
							orderItem.setSalesTax(String.valueOf(df4.format(totalItemTax)));
							if (taxMap.containsKey("saleTaxValue")) {
								Double total = taxMap.get("saleTaxValue")
										+ (item2.getPrice() * itemJsonobject.getInt("quantity"));
								taxMap.put("saleTaxValue", total);
							} else {
								taxMap.put("saleTaxValue",
										item2.getPrice() * itemJsonobject.getInt("quantity"));
							}
						}
					}

					orderItem.setItem(item2);
					orderItem.setOrder(orderR);
					orderItem.setQuantity(itemJsonobject.getInt("quantity"));
					orderItem.setOrderItemPrice(Double.parseDouble(itemAmount));
					// orderItemRepository.save(orderItem);
					Double itemPrice = 0.0;
					Double taxablleItemPrice = 0.0;
					if (orderItem.getItem() != null && orderItem.getItem().getPrice() != null) {
						if (merchant != null && merchant.getOwner() != null && merchant.getOwner().getPos() != null
								&& merchant.getOwner().getPos().getPosId() != null
								&& (merchant.getOwner().getPos().getPosId() == IConstant.NON_POS
										|| merchant.getOwner().getPos().getPosId() == IConstant.FOCUS)) {
							itemPrice = orderItem.getItem().getPrice();
						} else {
							itemPrice = orderItem.getItem().getPrice()
									* itemJsonobject.getInt("quantity");
							taxablleItemPrice = taxablleItemPrice + orderItem.getItem().getPrice();
						}
					}

					List<OrderItemModifier> itemModifiers = new ArrayList<OrderItemModifier>();
					Double modifierPrice = 0.0;
					Double taxableModifierPrice = 0.0;

					List<Integer> modifiersIdList = new ArrayList<Integer>();
					JSONArray modifierGroupJsonArray = null;
					
						if((!itemJsonobject.isNull("selected_modifier_groups")) && itemJsonobject.has("selected_modifier_groups") ) {
					modifierGroupJsonArray = itemJsonobject.getJSONArray("selected_modifier_groups");
						}else {
						LOGGER.info("No modifiers Selected ");
					}
					
					if(modifierGroupJsonArray !=null) {
					for (Object object1 : modifierGroupJsonArray) {
						JSONObject modifierGroupJsonobject = (JSONObject) object1;
						JSONArray modifiersJsonArray = modifierGroupJsonobject.getJSONArray("selected_items");
						for (Object object2 : modifiersJsonArray) {
							JSONObject modifiersJsonobject = (JSONObject) object2;
							Integer modifierId = Integer.parseInt(modifiersJsonobject.getString("external_data"));
							modifiersIdList.add(modifierId);
						}
					}
				}
				
					for (Integer modifierId : modifiersIdList) {
						LOGGER.info(" ===modifications.getModifier().getId() : " + modifierId);

						OrderItemModifier itemModifier = new OrderItemModifier();
						if (merchant != null && modifierId != null) {
							if (merchant != null && merchant.getOwner() != null && merchant.getOwner().getPos() != null
									&& merchant.getOwner().getPos().getPosId() != null
									&& (merchant.getOwner().getPos().getPosId() == IConstant.NON_POS
											|| merchant.getOwner().getPos().getPosId() == IConstant.FOCUS)) {
								itemModifier.setModifiers(
										modifiersRepository.findByIdAndMerchantId(modifierId, merchant.getId()));

								if (itemModifier.getModifiers() != null
										&& itemModifier.getModifiers().getPrice() != null) {
									itemPrice += modifierPrice + itemModifier.getModifiers().getPrice();
									taxableModifierPrice = taxableModifierPrice
											+ itemModifier.getModifiers().getPrice();
								}

							} else {

								Modifiers modfier = modifiersRepository.findByIdAndMerchantId(modifierId,merchant.getId());
								
								itemModifier.setModifiers(modifiersRepository.findByPosModifierIdAndMerchantId(
										modfier.getPosModifierId(), merchant.getId()));

								if (itemModifier.getModifiers() != null
										&& itemModifier.getModifiers().getPrice() != null) {
									modifierPrice = modifierPrice + (itemModifier.getModifiers().getPrice()
											* itemJsonobject.getInt("quantity"));
									taxableModifierPrice = taxableModifierPrice
											+ itemModifier.getModifiers().getPrice();
								}

							}
						}
						itemModifier.setOrderItem(orderItem);
						itemModifier.setQuantity(itemJsonobject.getInt("quantity"));
						itemModifiers.add(itemModifier);
					}

					if (item2 != null && item2.getAuxTaxAble() != null && item2.getAuxTaxAble()) {
						OrderType orderTypes = orderService.findByMerchantIdAndLabel(merchant.getId(), orderType);
						if (orderTypes != null && orderTypes.getAuxTax() != null && orderTypes.getAuxTax() != 0) {
							Double totalModifierAuxTax = ((taxablleItemPrice + taxableModifierPrice) / 100)
									* orderTypes.getAuxTax();
							orderItem.setAuxTax(String.valueOf(df3.format(totalModifierAuxTax)));

							if (taxMap.containsKey("auxTaxValue")) {
								Double total = taxMap.get("auxTaxValue") + modifierPrice;
								// Double total = taxMap.get("auxTaxValue");
								taxMap.put("auxTaxValue", total);
							} else {
								taxMap.put("auxTaxValue", itemPrice);
							}
						}
					}

					if (item2 != null && item2.getTaxAble() != null && item2.getTaxAble()) {
						OrderType orderTypes = orderService.findByMerchantIdAndLabel(merchant.getId(), orderType);
						if (orderTypes != null && orderTypes.getSalesTax() != null && orderTypes.getSalesTax() != 0) {
							double totalModifierTax = ((taxablleItemPrice + taxableModifierPrice) / 100)
									* orderTypes.getSalesTax();
							orderItem.setSalesTax(String.valueOf(df4.format(totalModifierTax)));

							if (taxMap.containsKey("saleTaxValue")) {
								Double total = taxMap.get("saleTaxValue") + modifierPrice;
//                    	Double total = taxMap.get("saleTaxValue");
								taxMap.put("saleTaxValue", total);
							} else {
								taxMap.put("saleTaxValue", itemPrice);
							}
						}
					}
					// end
					orderItem.setOrderItemPrice(taxablleItemPrice);
					orderItemRepository.save(orderItem);
					itemModifierRepository.save(itemModifiers);
					LOGGER.info("Saved OrderItem and ItemModifiers Objects");
				} else {

					OrderPizza orderPizza = new OrderPizza();
					PizzaTemplate pizza = null;

					List<Integer> sizeIdList = new ArrayList<Integer>();

					JSONArray size_modifierGroupJsonArray = null;
					if((!itemJsonobject.isNull("selected_modifier_groups")) && itemJsonobject.has("selected_modifier_groups") ) {
						 size_modifierGroupJsonArray = itemJsonobject.getJSONArray("selected_modifier_groups");
					}

					JSONObject size_Json = null;
					if(size_modifierGroupJsonArray != null) {
					for (Object object1 : size_modifierGroupJsonArray) {
						JSONObject modifierGroupJsonobject = (JSONObject) object1;
						JSONArray modifiersJsonArray = modifierGroupJsonobject.getJSONArray("selected_items");
						for (Object object2 : modifiersJsonArray) {
							JSONObject modifiersJsonobject = (JSONObject) object2;
							Integer sizeId = Integer.parseInt(modifiersJsonobject.getString("external_data"));
							sizeIdList.add(sizeId);
							size_Json = modifiersJsonobject;
						}
					}
				}
					
					JSONArray topping_crust_modifierGroupJsonArray = null;
					if((!size_Json.isNull("selected_modifier_groups")) && size_Json.has("selected_modifier_groups") ) {
						 topping_crust_modifierGroupJsonArray = size_Json.getJSONArray("selected_modifier_groups");
					}
					
					JSONArray toppingJsonArray = null;
					JSONArray crustJsonArray = null;
					List<JSONObject> crustJsonlist = new ArrayList<JSONObject>();

					if(topping_crust_modifierGroupJsonArray != null) {
					for (Object object1 : topping_crust_modifierGroupJsonArray) {
						JSONObject modifierGroupJsonobject = (JSONObject) object1;
						if(modifierGroupJsonobject.has("title")) {
						String modifierCheck = modifierGroupJsonobject.getString("title");
						if (modifierCheck.equalsIgnoreCase("Pizza Topping")) {
							toppingJsonArray = modifierGroupJsonobject.getJSONArray("selected_items");

						} else if (modifierCheck.equalsIgnoreCase("Pizza Crust")) {
							crustJsonArray = modifierGroupJsonobject.getJSONArray("selected_items");
						}
						}
					}
					}
					
					pizza = pizzaTemplateRepository
							.findByIdAndMerchantId(Integer.parseInt(itemJsonobject.getString("id")), merchant.getId());
					PizzaTemplateSize pizzaTemplateSize = pizzaTemplateSizeRepository
							.findByPizzaSizeIdAndPizzaTemplateId(sizeIdList.get(0),
									Integer.parseInt(itemJsonobject.getString("id")));
					if (pizza != null && pizza.getAuxTaxable() != null && pizza.getAuxTaxable()) {

						OrderType orderTypes = orderService.findByMerchantIdAndLabel(merchant.getId(), orderType);
						if (orderTypes != null && orderTypes.getAuxTax() != null && orderTypes.getAuxTax() != 0) {
							double totalItemAuxTax = (pizzaTemplateSize.getPrice() / 100) * orderTypes.getAuxTax();
							orderPizza.setAuxTax(String.valueOf(df3.format(totalItemAuxTax)));
							if (taxMap.containsKey("auxTaxValue")) {
								Double total = taxMap.get("auxTaxValue") + pizzaTemplateSize.getPrice();
								taxMap.put("auxTaxValue", total);
							} else {
								taxMap.put("auxTaxValue", pizzaTemplateSize.getPrice());
							}
						}
					}

					if (pizza != null && pizza.getTaxable() != null && pizza.getTaxable()) {
						OrderType orderTypes = orderService.findByMerchantIdAndLabel(merchant.getId(), orderType);
						if (orderTypes != null && orderTypes.getSalesTax() != null && orderTypes.getSalesTax() != 0) {
							double totalItemTax = (pizzaTemplateSize.getPrice() / 100) * orderTypes.getSalesTax();
							orderPizza.setSalesTax(String.valueOf(df4.format(totalItemTax)));
							if (taxMap.containsKey("saleTaxValue")) {
								Double total = taxMap.get("saleTaxValue") + pizzaTemplateSize.getPrice();
								taxMap.put("saleTaxValue", total);
							} else {
								taxMap.put("saleTaxValue", pizzaTemplateSize.getPrice());
							}
						}
					}

					orderPizza.setPizzaTemplate(pizza);
					if (pizzaTemplateSize != null && pizzaTemplateSize.getPizzaSize() != null) {
						orderPizza.setPizzaSize(pizzaTemplateSize.getPizzaSize());
					}
					orderPizza.setPrice(pizzaTemplateSize.getPrice());
					orderPizza.setOrder(orderR);
					orderPizza.setCreatedOn(currentDate);
					orderPizza.setQuantity(itemJsonobject.getInt("quantity"));
					orderPizza = orderPizzaRepository.save(orderPizza);
					if(orderPizza!=null)
					LOGGER.info("orderPizza saved succesfully : ");
					else
						LOGGER.info("failed to save orderPizza  : ");	
					List<com.foodkonnekt.model.OrderPizzaToppings> orderPizzaToppingList = new ArrayList<com.foodkonnekt.model.OrderPizzaToppings>();
					
					LOGGER.info("crustJsonArray : "+crustJsonArray);
					if(crustJsonArray !=null) {
					for (Object object1 : crustJsonArray) {
						JSONObject crustJsonobject = (JSONObject) object1;
						Integer quantity = crustJsonobject.getInt("quantity");
						JSONObject crustprice = crustJsonobject.getJSONObject("price");
						JSONObject unit_price = crustprice.getJSONObject("unit_price");
						String amount = null;
						if (unit_price.has("formatted_amount"))
							amount = unit_price.getString("formatted_amount").replace("$", "");
						PizzaCrust pizzaCrust = pizzaCrustRepository.findByMerchantIdAndId(merchant.getId(),
								Integer.parseInt(crustJsonobject.getString("external_data")));
						OrderPizzaCrust orderPizzaCrust = new OrderPizzaCrust();
						orderPizzaCrust.setOrderPizza(orderPizza);
						orderPizzaCrust.setPizzacrust(pizzaCrust);
						orderPizzaCrust.setPrice(Double.parseDouble(amount));
						orderPizzaCrust.setQuantity(quantity);

						orderPizzaCrustRepository.save(orderPizzaCrust);
					}
				}
					
					LOGGER.info("crustJsonArray : "+crustJsonArray);
					if(toppingJsonArray !=null) {
					for (Object object1 : toppingJsonArray) {
						JSONObject toppingJsonobject = (JSONObject) object1;
						Integer quantity = toppingJsonobject.getInt("quantity");
						JSONObject toppingprice = toppingJsonobject.getJSONObject("price");
						JSONObject unit_price = toppingprice.getJSONObject("unit_price");
						String amount = null;
						if (unit_price.has("formatted_amount"))
							amount = unit_price.getString("formatted_amount").replace("$", "");

						com.foodkonnekt.model.OrderPizzaToppings orderPizzaTopping = new com.foodkonnekt.model.OrderPizzaToppings();
						orderPizzaTopping.setPizzaTopping(pizzaToppingRepository.findByMerchantIdAndId(merchant.getId(),
								Integer.parseInt(toppingJsonobject.getString("external_data"))));

						orderPizzaTopping.setOrderPizza(orderPizza);
						orderPizzaTopping.setQuantity(quantity);
						orderPizzaTopping.setSide1(false);
						orderPizzaTopping.setSide2(false);
						orderPizzaTopping.setWhole(true);
						orderPizzaTopping.setPrice(Double.parseDouble(amount));
						orderPizzaToppingList.add(orderPizzaTopping);
					}
					}
					orderPizzaToppingsRepository.save(orderPizzaToppingList);
					LOGGER.info("Saved OrderPizza and orderPizzaTopping and orderPizzaCrust Objects");
				}

				if (taxMap.containsKey("saleTaxValue")) {
					orderR.setTaxableTotal(taxMap.get("saleTaxValue").toString());
				}
				if (taxMap.containsKey("auxTaxValue")) {
					orderR.setAuxTaxableTotal(taxMap.get("auxTaxValue").toString());
				}
				
			}
			orderR.setIsComplete(1);
			
			orderRepo.save(orderR);
			LOGGER.info("order Saved successfully");
			}
			catch (Exception e) {
				String denyResponse ="";
				denied = true;
				response="failed";
				LOGGER.info("exception in : updateOrderDetails : exception : " + e);
				orderR.setIsDefaults(3);
				orderR.setUberorderId(null);
				orderRepo.save(orderR);
				if(!jObject.getString("current_state").equalsIgnoreCase("DENIED"))
				denyResponse = ProducerUtil.denyUberOrder(jObject.getString("id"));
				LOGGER.info("denyResponse : "+denyResponse);
				
			}
		}
					
		if (merchant.getOwner() != null && merchant.getOwner().getPos() != null
				&& merchant.getOwner().getPos().getPosId() != null
				&& merchant.getOwner().getPos().getPosId() == IConstant.POS_CLOVER) {

			if (orderR != null && orderR.getOrderPosId() != null) {

				Integer merchantSubscriptionId = merchantSubscriptionRepository
						.findByMerchantId(merchant.getId());
				LOGGER.info(" === merchantSubscriptionId : " + merchantSubscriptionId);
				MerchantOrders merchantOrders = merchantOrdersRepository
						.findByMerchantSubscriptionId(merchantSubscriptionId);
				// call metered API for increase order count
				if (merchantOrders != null && merchantOrders.getMerchantSubscription() != null
						&& merchantOrders.getMerchantSubscription().getSubscription() != null) {
					java.util.TimeZone timezone = null;
					Date date =null;
					 if(merchant.getTimeZone()!=null && merchant.getTimeZone().getTimeZoneCode()!=null) {
				            timezone = java.util.TimeZone.getTimeZone(merchant.getTimeZone().getTimeZoneCode());
				           date= DateUtil.currentDateAsYYYYMMdd(timezone);
				            }
				            else {
				            	 date= DateUtil.currentDate();
				            }
					long difference = DateUtil.findDifferenceBetweenTwoDates(merchantOrders.getEndDate(),date);
					
					if ((difference > 0) && (merchantOrders.getOrderCount() <= merchantOrders
							.getMerchantSubscription().getSubscription().getOrderLimit())) {
						LOGGER.info("-------Metered if block----");
					} else {
						LOGGER.info("-------Metered else block----");
						merchant.setSubscription(merchantOrders.getMerchantSubscription().getSubscription());
						//String clover_response = CloverUrlUtil.addMteredPrice(merchant,environment);
						//LOGGER.info("-------Metered API response-----" + clover_response);
					}
					merchantOrders.setOrderCount(merchantOrders.getOrderCount() + 1);
					merchantOrdersRepository.save(merchantOrders);
				} else {
					LOGGER.error("OrderServiceImpl :: savePizzaOrder : Merchant " + merchant.getName()
							+ "'doesn't have any Subscription");
					MailSendUtil.sendErrorMailToAdmin(
							"Merchant '" + merchant.getName() + "'doesn't have any Subscription",environment);
				}
			}
		}
	}else {
		LOGGER.info("updateOrderDetails : no merchant found for store Id : "+storeId);
	}
String customerName = newcustomer.getFirstName()+" "+newcustomer.getLastName();
		
		String merchantLogo = "";
		String futureOrderTypeAndTime = "";
		if(orderR != null) {
		if (orderR.getMerchant() != null) {
			if (orderR.getMerchant().getMerchantLogo() != null && !orderR.getMerchant().getMerchantLogo().isEmpty()
					&& !orderR.getMerchant().getMerchantLogo().equals("")) {

				merchantLogo = environment.getProperty("BASE_PORT") + orderR.getMerchant().getMerchantLogo();
			} else {
				merchantLogo = "http://www.dev.foodkonnekt.com/app/foodnew.jpg";
			}
		}
		if(!denied) {
		updateUberOrderStatus(orderR.getOrderPosId(),orderR.getCustomer().getId(), orderR.getPaymentMethod(), orderR.getSubTotal(),
				orderR.getTax(), customerName, orderR.getCustomer().getEmailId(), orderR.getMerchant().getName(), merchantLogo,
				orderR.getOrderType(), orderR.getOrderPrice().toString(),futureOrderTypeAndTime ,"select",futureOrderTypeAndTime,orderR.getMerchant() ,orderR.getIsDefaults());
		}else{
			
		}
	}
		}else {
			LOGGER.info(" : Uber Order Id Already exists");
		}
		
		
		LOGGER.info("updateOrderDetails returns response : "+response);
		return response;
	}
	
	@SuppressWarnings("deprecation")
	public boolean updateUberOrderStatus(String orderPosId, Integer customerId, String paymentYtpe,
			String subTotal, String tax, String name, String email, String merchantName, String merchantLogo,
			String orderType, String orderPrice, String futureOrderType, String futureDate,
			String futureTime, Merchant merchant ,Integer orderState) {
		


		LOGGER.info("----------------Start :: OrderServiceImpl : updateUberOrderStatust------------------");
		LOGGER.info(" ====paymentype : " + paymentYtpe + " subTotal : " + subTotal + " name : "
				+ name + "  email : " + email + " merchantName : " + merchantName + " tax : " + tax + " orderType : "
				+ orderType  + " orderPrice : " + orderPrice + "  futureOrderType : " + futureOrderType
				+ " futureDate : " + futureDate + " futureTime : " + futureTime 
				+ " orderPosId : " + orderPosId + " customerId : " + customerId);
		try {
			IConstant.flag = false;
			OrderR orderR = null;
			Vendor vendor = null;
			String orderDetails = "";
			Map<String, String> notification = new HashMap<String, String>();
			String orderStatus = "orderNow";
			Map<String, Map<String, String>> notf = new HashMap<String, Map<String, String>>();
			List<Map<String, String>> productList = new ArrayList<Map<String, String>>();

			Map<String, String> order = new HashMap<String, String>();
			if (customerId != null && orderPosId != null) {
				orderR = orderRepo.findByCustomerIdAndOrderPosId(customerId, orderPosId);

				LOGGER.info("OrderServiceImpl :: updateUberOrderStatus : customerId " + customerId);
				LOGGER.info("OrderServiceImpl :: updateUberOrderStatus : orderPosId " + orderPosId);

				if (orderR != null) {
					orderR.setSubTotal(subTotal);
					orderR.setTax(tax);
					orderR.setPaymentMethod(paymentYtpe);
					orderR.setOrderType(orderType);
					merchant = orderR.getMerchant();

					vendor = vendorRepository.findOne(merchant.getOwner().getId());

					LOGGER.info("OrderServiceImpl :: updateUberOrderStatus : vendorId " + vendor.getId());

					PickUpTime pickUpTime = null;
					Zone zone = null;
					String convenienceFeeValue = "0";
					String pickUpTimeValue = "0";
					if (null != merchant) {
						
						if ("pickup".equals(orderType.toLowerCase())) {
							pickUpTime = pickUpTimeRepository.findByMerchantId(merchant.getId());
							if (pickUpTime != null) {
								try {
									Integer.parseInt(pickUpTime.getPickUpTime());
									avgTime = pickUpTime.getPickUpTime();
								} catch (NumberFormatException e) {
									avgTime = "45";
									LOGGER.error("OrderServiceImpl :: updateUberOrderStatus : Exception " + e);
								}

							}
						} else {
							if (orderR.getZoneId() != null) {
								zone = zoneRepository.findById(orderR.getZoneId());
								LOGGER.info("OrderServiceImpl :: updateUberOrderStatus : zoneId " + orderR.getZoneId());
							}
							try {
								if (zone != null && zone.getAvgDeliveryTime() != null) {
									Integer.parseInt(zone.getAvgDeliveryTime());
									avgTime = zone.getAvgDeliveryTime();
								}
							} catch (NumberFormatException e) {
								avgTime = "45";
								LOGGER.error("OrderServiceImpl :: updateUberOrderStatus : Exception " + e);
							}

						}
					}

					notification.put("appEvent", "notification");

					order.put("total", Double.toString(orderR.getOrderPrice()));
					order.put("tax", tax);

					List<Map<String, String>> extraList = null;
					Map<String, String> poduct = null;
					Map<String, String> exctra = null;

					List<OrderItem> orderItems = orderItemRepository.findByOrderId(orderR.getId());
					for (OrderItem orderItem : orderItems) {
						poduct = new HashMap<String, String>();
						if (null != orderItem) {
							if (null != orderItem.getItem()) {

								String items = "<tr style='font-weight:600;text-transform:capitalize;font-family:arial'><td width='200px;'>"
										+ orderItem.getItem().getName()
										+ "</td><td width='100px;' style='text-align:center'>" + orderItem.getQuantity()
										+ "</td><td width='100px;' style='text-align:center'>" + "$"
										+ String.format("%.2f", orderItem.getItem().getPrice()) + "</td></tr>";
								orderDetails = orderDetails + "" + "<b>" + items + "</b>";

								poduct.put("product_id", orderItem.getItem().getPosItemId());
								poduct.put("name", orderItem.getItem().getName());
								poduct.put("price", Double.toString(orderItem.getItem().getPrice()));
								poduct.put("qty", Integer.toString(orderItem.getQuantity()));

								extraList = new ArrayList<Map<String, String>>();
								List<OrderItemModifier> itemModifiers = itemModifierRepository
										.findByOrderItemId(orderItem.getId());
								String itemModifierName = "";
								if (itemModifiers.size() > 0) {
									for (OrderItemModifier itemModifier : itemModifiers) {
										exctra = new HashMap<String, String>();

										if (itemModifier != null && itemModifier.getModifiers() != null
												&& itemModifier.getModifiers().getName() != null) {
											itemModifierName = itemModifier.getModifiers().getName();
										}

										String modifiers = "<tr style='text-transform:capitalize;font-family:arial;margin-top:5px;font-size:12px'><td width='200px;'>"
												+ itemModifierName
												+ "</td><td width='100px;' style='text-align:center'>"
												+ itemModifier.getQuantity()
												+ " </td><td width='100px;' style='text-align:center'> " + "$"
												+ String.format("%.2f", itemModifier.getModifiers().getPrice())
												+ "</td></tr>";
										orderDetails = orderDetails + "" + modifiers;
										exctra.put("id", itemModifier.getModifiers().getPosModifierId());
										exctra.put("price", Double.toString(itemModifier.getModifiers().getPrice()));
										exctra.put("name", itemModifier.getModifiers().getName());
										exctra.put("qty", Integer.toString(orderItem.getQuantity()));
										extraList.add(exctra);
									}
								}

								Gson gson = new Gson();
								if (extraList.size() > 0) {
									String extraJson = gson.toJson(extraList);
									poduct.put("extras", extraJson);
									productList.add(poduct);
								} else {
									productList.add(poduct);
								}
							}
						}
					}

					/*
					 * -------------------------------------------------------------For Pizza
					 * View-------------------------------------------------------------------------
					 * ---------
					 */
					LOGGER.info("OrderServiceImpl :: updateUberOrderStatus : orderR.getId : " + orderR.getId());

					List<OrderPizza> orderPizzas = orderPizzaRepository.findByOrderId(orderR.getId());
					if (!orderPizzas.isEmpty() && orderPizzas != null) {
						for (OrderPizza orderPizza : orderPizzas) {
							poduct = new HashMap<String, String>();
							if (orderPizza != null && orderPizza.getPizzaTemplate() != null) {

								Item item = new Item();
								if (orderPizza != null && orderPizza.getPizzaTemplate() != null
										&& orderPizza.getPizzaTemplate().getDescription() != null
										&& orderPizza.getPizzaSize() != null
										&& orderPizza.getPizzaSize().getDescription() != null) {
									item.setName(orderPizza.getPizzaTemplate().getDescription() + "("
											+ orderPizza.getPizzaSize().getDescription() + ")");

									String pizza = "<tr style='font-weight:600;text-transform:capitalize;font-family:arial'><td width='200px;'>"
											+ orderPizza.getPizzaTemplate().getDescription() + "("
											+ orderPizza.getPizzaSize().getDescription() + ")"
											+ "</td><td width='100px;' style='text-align:center'>"
											+ orderPizza.getQuantity()
											+ "</td><td width='100px;' style='text-align:center'>" + "$"
											+ String.format("%.2f", orderPizza.getPrice()) + "</td></tr>";
									orderDetails = orderDetails + "" + "<b>" + pizza + "</b>";

									poduct.put("product_id", orderPizza.getPizzaTemplate().getPosPizzaTemplateId());
									poduct.put("name", orderPizza.getPizzaTemplate().getDescription() + "("
											+ orderPizza.getPizzaSize().getDescription() + ")");
									poduct.put("price", Double.toString(orderPizza.getPrice()));
									poduct.put("qty", Integer.toString(orderPizza.getQuantity()));

								}
								item.setPrice(orderPizza.getPrice());
								List<OrderPizzaToppings> orderPizzaToppings = orderPizzaToppingsRepository
										.findByOrderPizzaId(orderPizza.getId());
								extraList = new ArrayList<Map<String, String>>();
								if (!orderPizzaToppings.isEmpty() && orderPizzaToppings != null) {
									for (OrderPizzaToppings pizzaToppings : orderPizzaToppings) {
										exctra = new HashMap<String, String>();
										Modifiers itemModifier = new Modifiers();
										if (pizzaToppings != null && pizzaToppings.getPizzaTopping() != null
												&& pizzaToppings.getPizzaTopping().getDescription() != null) {
											itemModifier.setName(pizzaToppings.getPizzaTopping().getDescription());
											itemModifier.setPrice(pizzaToppings.getPrice());

											String modifiers = "<tr style='text-transform:capitalize;font-family:arial;margin-top:5px;font-size:12px'><td width='200px;'>"
													+ pizzaToppings.getPizzaTopping().getDescription();

											if (pizzaToppings.getSide1() != null && pizzaToppings.getSide1()) {
												modifiers += " (First Half)";
											} else if (pizzaToppings.getSide1() != null && pizzaToppings.getSide2()) {
												modifiers += " (Second Half)";
											} else {
												modifiers += " (Full)";
											}
											modifiers += "</td><td width='100px;' style='text-align:center'>"
													+ orderPizza.getQuantity()
													+ " </td><td width='100px;' style='text-align:center'> " + "$"
													+ String.format("%.2f", pizzaToppings.getPrice()) + "</td></tr>";

											orderDetails = orderDetails + "" + modifiers;

											exctra.put("id", pizzaToppings.getPizzaTopping().getPosPizzaToppingId());
											exctra.put("price", Double.toString(pizzaToppings.getPrice()));
											exctra.put("name", pizzaToppings.getPizzaTopping().getDescription());
											exctra.put("qty", orderPizza.getQuantity().toString());
											extraList.add(exctra);
										}
									}
								}

								OrderPizzaCrust orderPizzaCrust = orderPizzaCrustRepository
										.findByOrderPizzaId(orderPizza.getId());
								if (orderPizzaCrust != null && orderPizzaCrust.getPizzacrust() != null) {
									exctra = new HashMap<String, String>();
									String crust = "<tr style='text-transform:capitalize;font-family:arial;margin-top:5px;font-size:12px'><td width='200px;'>"
											+ orderPizzaCrust.getPizzacrust().getDescription();

									crust += "</td><td width='100px;' style='text-align:center'>"
											+ orderPizza.getQuantity()
											+ " </td><td width='100px;' style='text-align:center'> " + "$"
											+ String.format("%.2f", orderPizzaCrust.getPrice()) + "</td></tr>";
									orderDetails = orderDetails + "" + crust;

									exctra.put("id", orderPizzaCrust.getPizzacrust().getId().toString());
									exctra.put("price", Double.toString(orderPizzaCrust.getPrice()));
									exctra.put("name", orderPizzaCrust.getPizzacrust().getDescription());
									exctra.put("qty", orderPizza.getQuantity().toString());
								}
							}
							Gson gson = new Gson();
							if (extraList.size() > 0) {
								String extraJson = gson.toJson(extraList);
								poduct.put("extras", extraJson);
								productList.add(poduct);
							} else {
								productList.add(poduct);
							}
						}
					}

					/*
					 * -----------------------------------------------------------------------------
					 * -------------------------
					 * ---------------------------------------------------------
					 */

					/*
					 * Gson gson = new Gson(); String extraJson = gson.toJson(extraList);
					 * poduct.put("extras", extraJson); productList.add(poduct);
					 */

					orderDetails = "<table width='300px;'><tbody>" + orderDetails + "</table></tbody>";
					if (orderPrice != null && !orderPrice.isEmpty()) {
						double orderTotalPrice = Math.round(Double.valueOf(orderPrice) * 100.0) / 100.0;
						orderR.setOrderPrice(orderTotalPrice);
					}
					double taxValue = 0;
					double totalTax = 0;
					
					if (tax != null) {
						taxValue = Double.valueOf(tax);
					}
					totalTax =  taxValue;
					LOGGER.error("OrderServiceImpl :: updateUberOrderStatus : totalTax : "+ taxValue);
					orderR.setSalesTax(tax);
					orderR.setAuxTax("0");
					orderR.setTax(String.valueOf(totalTax));

					orderRepo.save(orderR);

					LOGGER.error("OrderServiceImpl :: updateUberOrderStatus : orderId " + orderR.getId());

					custId = customerId;
					orderId = orderR.getId();
					convenienceFeeValue1 = convenienceFeeValue;
					name1 = name;
					paymentYtpe1 = paymentYtpe;
					orderPosId1 = orderPosId;
					subTotal1 = subTotal;
					tax1 = tax;
					orderDetails1 = orderDetails;
					email1 = email;
					orderPrice1 = orderR.getOrderPrice();
					note1 = orderR.getOrderNote();
					merchantName1 = merchantName;
					merchantLogo1 = merchantLogo;
					orderType1 = orderType;
					tipAmount = orderR.getTipAmount();
					merchantId = merchant.getId();
					isfutureorder = orderR.getIsFutureOrder();

					if (orderR.getDeliveryFee() != null && !orderR.getDeliveryFee().isEmpty())
						deliverFee = orderR.getDeliveryFee();
					if (orderR.getOrderDiscount() != null)
						orderDiscount = orderR.getOrderDiscount();
				}
			} else {
				return false;
			}
			String timeZoneCode = "America/Chicago";
			
	// Notification Method Starts
	String methodType = null;
	int orderAccepted = 0;
	String orderLink = null;
	String requestId = null;
	String contact = null;
	String orderIdd = null;
	String customerrId = null;

	if(orderR!=null&&orderR.getId()!=null)
	{
		orderIdd = EncryptionDecryptionUtil.encryption(Integer.toString(orderR.getId()));
	}

	if(orderR!=null&&orderR.getCustomer()!=null&&orderR.getCustomer().getId()!=null)
	{
				customerrId = EncryptionDecryptionUtil.encryption(Integer.toString(orderR.getCustomer().getId()));
			}

	Boolean enableNotification = false;
	Boolean autoAcceptFutureOrder = false;
	Boolean autoAccpetOrder = false;
	String notificationType = "";
	MerchantConfiguration merchantConfiguration = merchantConfigurationService
					.findMerchantConfigurationBymerchantId(merchant.getId());if(merchantConfiguration!=null)
	{
		if (merchantConfiguration.getEnableNotification() != null) {

			if (merchantConfiguration.getEnableNotification() == true) {
				notificationType = "Enable Notification";
				enableNotification = merchantConfiguration.getEnableNotification();
				LOGGER.info("OrderServiceImpl :: updateUberOrderStatus : enableNotification  :: " + enableNotification);
			}
		}

		if (merchantConfiguration.getAutoAcceptOrder() != null) {

			if (merchantConfiguration.getAutoAcceptOrder() == true) {
				notificationType = "Auto Accept";
				autoAccpetOrder = merchantConfiguration.getAutoAcceptOrder();
				LOGGER.info("OrderServiceImpl :: updateUberOrderStatus : autoAccpetOrder  :: " + autoAccpetOrder);
			}
		}
		if (merchantConfiguration.getAutoAccept() != null) {

			if (merchantConfiguration.getAutoAccept() == true) {
				notificationType = "Auto Accept Future Order";
				autoAcceptFutureOrder = merchantConfiguration.getAutoAccept();
				LOGGER.info(
						"OrderServiceImpl :: updateUberOrderStatus : autoAcceptFutureOrder  :: " + autoAcceptFutureOrder);
			}
		}

	}

	String toDay = DateUtil.getCurrentDayForTimeZoneAsString(timeZoneCode);
	String openingTime = null;
	String closinggTime = null;LOGGER.error("OrderServiceImpl :: updateUberOrderStatus :  toDay : "+toDay);
	OpeningClosingDay openingClosingDay = openingCosingDayRepository.findByDayAndMerchantId(toDay, merchant.getId());
	List<OpeningClosingTime> openingClosingTimes = openingClosingTimeRepository.findByOpeningClosingDayId(
			openingClosingDay.getId());if(openingClosingTimes!=null&&!openingClosingTimes.isEmpty())
	{

		openingTime = openingClosingTimes.get(0).getStartTime();
		closinggTime = openingClosingTimes.get(0).getEndTime();
		LOGGER.error("OrderServiceImpl :: updateUberOrderStatus :  openingTime : " + openingTime + " closinggTime "
				+ closinggTime);
	}

	String reviewResponse = null;
	String url = environment.getProperty("WEB_BASE_URL") + "/thermalPrinterReceiptforOrder?orderid=" + orderR.getId() + "&merchantId="
			+ merchant.getId() + "&customerId=" + orderR.getCustomer()
					.getId();LOGGER.error("OrderServiceImpl :: updateUberOrderStatus :  Thermal receipt URL=== : "+url);LOGGER.info("Thermal receipt URL==="+url);try
	{
		HttpClient client = HttpClientBuilder.create().build();
		HttpGet httpRequest = new HttpGet(url);
		HttpResponse httpResponse = client.execute(httpRequest);

		BufferedReader rd = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent()));
		StringBuffer result1 = new StringBuffer();
		String line = "";
		while ((line = rd.readLine()) != null) {
			result1.append(line);
		}
		reviewResponse = result1.toString();
	}catch(
	Exception e)
	{
		MailSendUtil.sendExceptionByMail(e,environment);
		LOGGER.error("OrderServiceImpl :: updateUberOrderStatus :  Exception : " + e);
		LOGGER.error("error: " + e.getMessage());
	}

	// send order receipt in printer starts
	Printer printer = printerRepository.findByMerchantId(merchant.getId());if(printer!=null&&merchant!=null)
	{
		LOGGER.info("OrderServiceImpl :: updateUberOrderStatus : printOrderReceipt call start");
		printOrderReceipt(printer, orderIdd, customerrId, merchant,environment);
		LOGGER.info("OrderServiceImpl :: updateUberOrderStatus : printOrderReceipt call end");
	}
	
	
	// send order receipt in printer ends
//orderR.getIsDefaults() != null && orderR.getIsDefaults() == 0
	// payment processing according to notification type
//	if(enableNotification==false)
//	{
//			if (autoAccpetOrder == true && autoAcceptFutureOrder == false
//					&& (((futureOrderType.equals("on")) && (!futureDate.equals("select"))))) {
//				orderStatus = "success";
//				LOGGER.info("OrderServiceImpl :: updateUberOrderStatus : autoAccpetOrder  :: " + autoAccpetOrder);
//			} else if (autoAccpetOrder == true && autoAcceptFutureOrder == true
//					&& (((futureOrderType.equals("on")) && (!futureDate.equals("select"))))
//					&& merchant.getOwner() != null && merchant.getOwner().getPos() != null
//					&& merchant.getOwner().getPos().getPosId() != null
//					&& merchant.getOwner().getPos().getPosId() != IConstant.FOCUS
//					&& merchant.getOwner().getPos().getPosId() != IConstant.POS_FOODTRONIX) {
//				LOGGER.info(
//						"OrderServiceImpl :: updateUberOrderStatus : autoAcceptFutureOrder  :: " + autoAcceptFutureOrder);
//				if (merchant.getOwner().getPos().getPosId() != IConstant.POS_CLOVER)
//					orderStatus = OrderUtil.orderConfirmationByIdByMerchantUId(orderR.getId(),
//							merchant.getMerchantUid(), enableNotification);
//				else
//					orderStatus = OrderUtil.orderConfirmationByOrderPosId(orderR.getOrderPosId());
//			} else if (merchant.getOwner().getPos() != null && merchant.getOwner().getPos().getPosId() != null
//					&& merchant.getOwner().getPos().getPosId() != IConstant.FOCUS
//					&& merchant.getOwner().getPos().getPosId() != IConstant.POS_FOODTRONIX) {
//				if (merchant.getOwner().getPos().getPosId() != IConstant.POS_CLOVER)
//					orderStatus = OrderUtil.orderConfirmationByIdByMerchantUId(orderR.getId(),
//							merchant.getMerchantUid(), enableNotification);
//				else
//					orderStatus = OrderUtil.orderConfirmationByOrderPosId(orderR.getOrderPosId());
//				LOGGER.info("OrderServiceImpl :: updateUberOrderStatus : autoAccpetOrder but order now case  :: "
//						+ autoAcceptFutureOrder);
//			}
//		
//	}else if(enableNotification==true)
//	{
//		orderStatus = "success";
//		LOGGER.info("OrderServiceImpl :: updateUberOrderStatus : enableNotification  :: " + enableNotification);
//	}

	
	if(enableNotification==false)
		{
		 if (autoAccpetOrder == true 
				&& merchant.getOwner() != null && merchant.getOwner().getPos() != null
				&& merchant.getOwner().getPos().getPosId() != null
				&& merchant.getOwner().getPos().getPosId() != IConstant.FOCUS
				&& merchant.getOwner().getPos().getPosId() != IConstant.POS_FOODTRONIX) {
			LOGGER.info(
					"OrderServiceImpl :: updateUberOrderStatus : autoAcceptFutureOrder  :: " + autoAcceptFutureOrder);
			if (merchant.getOwner().getPos().getPosId() != IConstant.POS_CLOVER) {
				if(orderR.getIsDefaults() != null && orderR.getIsDefaults() == 0) {
				orderR.setIsDefaults(1);
				orderRepo.save(orderR);
				}
			}
			else
				orderStatus = OrderUtil.orderConfirmationByOrderPosId(orderR.getOrderPosId(),environment);
	} 
//		 else if (merchant.getOwner().getPos() != null && merchant.getOwner().getPos().getPosId() != null
//				&& merchant.getOwner().getPos().getPosId() != IConstant.FOCUS
//				&& merchant.getOwner().getPos().getPosId() != IConstant.POS_FOODTRONIX) {
//			if (merchant.getOwner().getPos().getPosId() != IConstant.POS_CLOVER){
//				if(orderR.getIsDefaults() != null && orderR.getIsDefaults() == 0) {
//				orderR.setIsDefaults(1);
//				orderRepo.save(orderR);
//				}
//				}else
//				orderStatus = OrderUtil.orderConfirmationByOrderPosId(orderR.getOrderPosId());
//			LOGGER.info("OrderServiceImpl :: updateUberOrderStatus : autoAccpetOrder but order now case  :: "
//					+ autoAcceptFutureOrder);
//		} 
		}
	orderStatus = "success";
	LOGGER.info("OrderServiceImpl :: updateUberOrderStatus : enableNotification  :: " + enableNotification);
	List<NotificationMethod> methods = methodRepository
			.findByMerchantIdAndIsActive(merchant.getId());if(methods!=null&&methods.size()>0)
	{
		for (NotificationMethod method : methods) {
			NotificationTracker notificationTracker = new NotificationTracker();
			notificationTracker.setMerchantId(merchant.getId());
			notificationTracker.setOrderId(orderR.getId());
			methodType = method.getNotificationMethodType().getMethodType();
			orderAccepted = method.getIsOrderAccepted();
			requestId = method.getSmsRequestId();
			contact = method.getContact();
			notificationTracker.setContactId(contact);
			notificationTracker.setEnableNotification(notificationType);
			if (merchant.getMerchantLogo() != null) {
				merchantLogo = environment.getProperty("BASE_PORT") + merchant.getMerchantLogo();
			} else {
				merchantLogo = "http://www.dev.foodkonnekt.com/app/foodnew.jpg";
			}

			if (orderR != null) {
				if (methodType.equals("Mail") && enableNotification != null) {
					notificationTracker.setNotificationType("Mail");
					if (!IConstant.flag) {
						ProducerUtil.sendFax(orderIdd, customerrId,environment);
					}

					final String FINALORDERIDD = orderIdd;
					final String FINALCUSTOMERID = customerrId;

					final Integer MERCHANTID = merchant.getId();

					double ordrPrice = 0;
					String customerName = null;
					if (orderR.getOrderPrice() != null) {
						ordrPrice = orderR.getOrderPrice();
					}
					final Double FINALORDERPRICE = ordrPrice;
					if (orderR.getCustomer().getFirstName() != null && orderR.getCustomer().getLastName() != null) {
						customerName = orderR.getCustomer().getFirstName().concat(" ")
								.concat(orderR.getCustomer().getLastName());
					} else if (orderR.getCustomer().getFirstName() != null) {
						customerName = orderR.getCustomer().getFirstName();
					}
					final String FINALCUSTOMERFIRSTNAME = customerName;
					final String FINALORDERTYPE = orderR.getOrderType();
					final String FINALPAYMENTMETHOD = orderR.getPaymentMethod();

					final String FINALCONTACT = contact;
					final Date FINALCREATEDDATE = orderR.getCreatedOn();
					final String FINALMERCHANTLOGO = merchantLogo;
					final String FINALSUBTOTAL = orderR.getSubTotal();
					final Double FINALUBER_CHARGES = orderR.getOrderPrice()-new Double(orderR.getSubTotal())-new Double(orderR.getTax());
					final String FINALDELIVERYFEE = orderR.getDeliveryFee();
					final String FINALTAX = orderR.getTax();
					final Double FINALTIPAMOUNT = orderR.getTipAmount();
					final String ORDERNOTE = orderR.getOrderNote();
					final String MERCHANTNAME = merchant.getName();
					final String ORDERDETAILS = orderDetails;
					final String ORDERAVGTIME = orderR.getOrderAvgTime();
					final Double ORDERDISCOUNT = orderR.getOrderDiscount();
					String discountCoupon = "";
					if (orderR.getOrderDiscount() != null) {
						List<OrderDiscount> orderDiscounts = orderDiscountRepository.findByOrderId(orderR.getId());
						if (!orderDiscounts.isEmpty() && orderDiscounts != null) {
							discountCoupon = discountCoupon + "<tr>" + "<td>Discount Coupon</td>" + "<td>&nbsp;</td>"
									+ "<td>" + orderDiscounts.get(0).getCouponCode() + "</td>" + "</tr>";
							if (orderDiscounts.size() > 1) {
								for (int i = 1; i < orderDiscounts.size(); i++) {
									discountCoupon = discountCoupon + "<tr>" + "<td></td>" + "<td>&nbsp;</td>" + "<td>"
											+ orderDiscounts.get(i).getCouponCode() + "</td>" + "</tr>";
								}
							}
						}
					}
					listOfCoupons = discountCoupon;
					final String ORDERCOUPON = discountCoupon;
					String orderLinkUrl = null;
					LOGGER.info("OrderServiceImpl :: updateUberOrderStatus :  method type====Mail== : " + methodType);
					LOGGER.info("method type====Mail==" + methodType);
					if (enableNotification == false) {
						LOGGER.info("OrderServiceImpl :: updateUberOrderStatus :  when enable notification is false : ");
						LOGGER.info("when enable notification is false");
						if (orderR.getPaymentMethod().equals("Cash")) {
							// when merchant is not focus and foodtronix then this order confirmation will
							// run because we will auto accept this order from focus/foodtronix

							// when auto accept is on but auto accept future order is off
							if (autoAccpetOrder == true && autoAcceptFutureOrder == false
									&& (((futureOrderType.equals("on")) && (!futureDate.equals("select"))))) {

								LOGGER.info("OrderServiceImpl :: updateUberOrderStatus :  when autoAccept is true : ");

								DateUtil date = new DateUtil();
								if (date.checkCurrentTimeExistBetweenTwoTime(openingTime, closinggTime, timeZoneCode)) {
									LOGGER.info(
											"OrderServiceImpl :: updateUberOrderStatus :   when autoAcceptFutureOrder is false : opening closing hours is matched");
									LOGGER.info("opening closing hours is matched");
									if (merchant != null && merchant.getOwner() != null
											&& merchant.getOwner().getPos() != null
											&& merchant.getOwner().getPos().getPosId() != null) {
										orderLinkUrl = environment.getProperty("WEB_BASE_URL") + "/receipt?orderid=" + orderIdd
												+ "&customerid=" + customerrId;
									}
									LOGGER.info(
											"OrderServiceImpl :: updateUberOrderStatus : orderLinkUrl : " + orderLinkUrl);
									final String FINALORDERLINK = orderLinkUrl;
									final NotificationTracker notificationTracker2 = notificationTracker;
									new Thread() {
										public void run() {
											MailSendUtil.onlineUberOrderNotification(FINALORDERIDD, FINALCUSTOMERID,
													FINALCUSTOMERFIRSTNAME, FINALORDERPRICE, FINALORDERTYPE,
													FINALPAYMENTMETHOD, FINALCREATEDDATE, FINALORDERLINK, FINALCONTACT,
													FINALMERCHANTLOGO, FINALSUBTOTAL, FINALUBER_CHARGES,
													FINALTAX, ORDERNOTE, MERCHANTNAME, ORDERDETAILS,
													ORDERAVGTIME, ORDERDISCOUNT, ORDERCOUPON, MERCHANTID, orderPosId1,environment);
											if (notificationTracker2.getId() != null)
												notificationTracker2.setId(null);
											notificationTracker2.setStatus(IConstant.mailStatus);
											notificatonRepo.save(notificationTracker2);
											IConstant.mailStatus = "Not sent";
										}
									}.start();
									IConstant.flagofMail = true;
								} else {
									notificationTracker.setStatus("Not sent");
									notificatonRepo.save(notificationTracker);
								}

								// when auto accept order is on and auto accept future order is on
							} else if (autoAccpetOrder == true && autoAcceptFutureOrder == true
									&& (((futureOrderType.equals("on")) && (!futureDate.equals("select"))))) {

								LOGGER.info("autoAccpetOrder is true & autoAcceptFutureOrder is true");
								LOGGER.info(
										"OrderServiceImpl :: updateUberOrderStatus :   autoAccpetOrder is true & autoAcceptFutureOrder is true : ");
								if (merchant != null && merchant.getOwner() != null
										&& merchant.getOwner().getPos() != null
										&& merchant.getOwner().getPos().getPosId() != null) {
									// orderStatus= OrderUtil.orderConfirmationByIdByMerchantUId(orderR.getId(),
									// merchant.getMerchantUid(), enableNotification);

									orderLinkUrl = environment.getProperty("WEB_BASE_URL") + "/receipt?orderid=" + orderIdd
											+ "&customerid=" + customerrId;
								}
								DateUtil date = new DateUtil();
								if (date.checkCurrentTimeExistBetweenTwoTime(openingTime, closinggTime, timeZoneCode)) {
									LOGGER.info(
											"OrderServiceImpl :: updateUberOrderStatus :   autoAccpetOrder is true & autoAcceptFutureOrder is true : opening closing hours is matched");
									LOGGER.info("when time matches");
									LOGGER.info(
											"OrderServiceImpl :: updateUberOrderStatus : orderLinkUrl : " + orderLinkUrl);
									final String FINALORDERLINK = orderLinkUrl;
									final NotificationTracker notificationTracker2 = notificationTracker;
									new Thread() {
										public void run() {
											MailSendUtil.onlineUberOrderNotification(FINALORDERIDD, FINALCUSTOMERID,
													FINALCUSTOMERFIRSTNAME, FINALORDERPRICE, FINALORDERTYPE,
													FINALPAYMENTMETHOD, FINALCREATEDDATE, FINALORDERLINK, FINALCONTACT,
													FINALMERCHANTLOGO, FINALSUBTOTAL, FINALUBER_CHARGES,
													FINALTAX, ORDERNOTE, MERCHANTNAME, ORDERDETAILS,
													ORDERAVGTIME, ORDERDISCOUNT, ORDERCOUPON, MERCHANTID, orderPosId1,environment);
											if (notificationTracker2.getId() != null)
												notificationTracker2.setId(null);
											notificationTracker2.setStatus(IConstant.mailStatus);
											notificatonRepo.save(notificationTracker2);
											IConstant.mailStatus = "No";
										}
									}.start();
									IConstant.flagofMail = true;
								} else {
									notificationTracker.setStatus("Not sent");
									notificatonRepo.save(notificationTracker);
								}

								// case only for order now
							} else {
								LOGGER.info(
										"OrderServiceImpl :: updateUberOrderStatus : When Enablenotification is false  and order now case");
								LOGGER.info("order now case");
								if (merchant != null && merchant.getOwner() != null
										&& merchant.getOwner().getPos() != null
										&& merchant.getOwner().getPos().getPosId() != null) {
									// orderStatus=OrderUtil.orderConfirmationByIdByMerchantUId(orderR.getId(),
									// merchant.getMerchantUid(), enableNotification);
									orderLinkUrl = environment.getProperty("WEB_BASE_URL") + "/receipt?orderid=" + orderIdd
											+ "&customerid=" + customerrId;
								}
								LOGGER.info("OrderServiceImpl :: updateUberOrderStatus : orderLinkUrl : " + orderLinkUrl);

								final String FINALORDERLINK = orderLinkUrl;
								if (!((futureOrderType.equals("on")) && (!futureDate.equals("select")))) {
									final NotificationTracker notificationTracker2 = notificationTracker;
									new Thread() {
										public void run() {
											MailSendUtil.onlineUberOrderNotification(FINALORDERIDD, FINALCUSTOMERID,
													FINALCUSTOMERFIRSTNAME, FINALORDERPRICE, FINALORDERTYPE,
													FINALPAYMENTMETHOD, FINALCREATEDDATE, FINALORDERLINK, FINALCONTACT,
													FINALMERCHANTLOGO, FINALSUBTOTAL, FINALUBER_CHARGES,
													FINALTAX, ORDERNOTE, MERCHANTNAME, ORDERDETAILS,
													ORDERAVGTIME, ORDERDISCOUNT, ORDERCOUPON, MERCHANTID, orderPosId1,environment);
											if (notificationTracker2.getId() != null)
												notificationTracker2.setId(null);
											notificationTracker2.setStatus(IConstant.mailStatus);
											notificatonRepo.save(notificationTracker2);
											IConstant.mailStatus = "No";
										}
									}.start();
									IConstant.flagofMail = true;
								}

							}
						}
					} else if (enableNotification == true) {
						if (orderR.getPaymentMethod().equals("Cash")) {
							LOGGER.info("OrderServiceImpl :: updateUberOrderStatus : enable notification is true");
							LOGGER.info("enable notification is true");

							// if order type is future
							if ((futureOrderType.equals("on")) && (!futureDate.equals("select"))) {
								LOGGER.info("order is future order");
								DateUtil date = new DateUtil();
								if (date.checkCurrentTimeExistBetweenTwoTime(openingTime, closinggTime, timeZoneCode)) {
									LOGGER.info(
											"OrderServiceImpl :: updateUberOrderStatus : enable notification is true :time between opening closing hours");
									LOGGER.info("time matches");
									orderLinkUrl = environment.getProperty("WEB_BASE_URL") + "/receipt?orderid=" + orderIdd
											+ "&customerid=" + customerrId;
									LOGGER.info(
											"OrderServiceImpl :: updateUberOrderStatus : orderLinkUrl : " + orderLinkUrl);
									final String FINALORDERLINK = orderLinkUrl;
									final NotificationTracker notificationTracker2 = notificationTracker;
									new Thread() {
										public void run() {
											MailSendUtil.onlineUberOrderNotification(FINALORDERIDD, FINALCUSTOMERID,
													FINALCUSTOMERFIRSTNAME, FINALORDERPRICE, FINALORDERTYPE,
													FINALPAYMENTMETHOD, FINALCREATEDDATE, FINALORDERLINK, FINALCONTACT,
													FINALMERCHANTLOGO, FINALSUBTOTAL, FINALUBER_CHARGES,
													FINALTAX, ORDERNOTE, MERCHANTNAME, ORDERDETAILS,
													ORDERAVGTIME, ORDERDISCOUNT, ORDERCOUPON, MERCHANTID, orderPosId1,environment);
											if (notificationTracker2.getId() != null)
												notificationTracker2.setId(null);
											notificationTracker2.setStatus(IConstant.mailStatus);
											notificatonRepo.save(notificationTracker2);
											IConstant.mailStatus = "No";
										}
									}.start();
									IConstant.flagofMail = true;
								} else {
									notificationTracker.setStatus("Not sent");
									notificatonRepo.save(notificationTracker);
								}

								// if order type in not future
							} else {
								LOGGER.info(
										"OrderServiceImpl :: updateUberOrderStatus : enable notification is true:order now  ");
								LOGGER.info("order now");
								orderLinkUrl = environment.getProperty("WEB_BASE_URL") + "/receipt?orderid=" + orderIdd
										+ "&customerid=" + customerrId;
								LOGGER.info(
										"OrderServiceImpl :: updateUberOrderStatus : enable notification is true: orderLinkUrl  "
												+ orderLinkUrl);
								final String FINALORDERLINK = orderLinkUrl;
								final NotificationTracker notificationTracker2 = notificationTracker;
								new Thread() {
									public void run() {
										MailSendUtil.onlineUberOrderNotification(FINALORDERIDD, FINALCUSTOMERID,
												FINALCUSTOMERFIRSTNAME, FINALORDERPRICE, FINALORDERTYPE,
												FINALPAYMENTMETHOD, FINALCREATEDDATE, FINALORDERLINK, FINALCONTACT,
												FINALMERCHANTLOGO, FINALSUBTOTAL, FINALUBER_CHARGES,
												FINALTAX, ORDERNOTE, MERCHANTNAME, ORDERDETAILS,
												ORDERAVGTIME, ORDERDISCOUNT, ORDERCOUPON, MERCHANTID, orderPosId1,environment);
										if (notificationTracker2.getId() != null)
											notificationTracker2.setId(null);
										notificationTracker2.setStatus(IConstant.mailStatus);
										notificatonRepo.save(notificationTracker2);
										IConstant.mailStatus = "No";
									}
								}.start();
								IConstant.flagofMail = true;
							}
						}
					}
				} else if (methodType.equals("SMS") && enableNotification != null) {
					notificationTracker.setNotificationType("SMS");
					LOGGER.info("Customer name=" + orderR.getCustomer().getFirstName());
					LOGGER.info("Order Price=" + orderR.getOrderPrice());
					LOGGER.info("Order Type=" + orderR.getOrderType());
					LOGGER.info("Payment type=" + orderR.getPaymentMethod());
					LOGGER.info("Order Date=" + orderR.getCreatedOn());
					LOGGER.info("Order Link=" + orderLink);
					LOGGER.info("OrderServiceImpl :: updateUberOrderStatus : enable notification is true: Customer name="
							+ orderR.getCustomer().getFirstName());
					LOGGER.info("OrderServiceImpl :: updateUberOrderStatus : enable notification is true: Order Price="
							+ orderR.getOrderPrice());
					LOGGER.info("OrderServiceImpl :: updateUberOrderStatus : enable notification is true: Order Type="
							+ orderR.getOrderType());
					LOGGER.info("OrderServiceImpl :: updateUberOrderStatus : enable notification is true: Payment type="
							+ orderR.getPaymentMethod());
					LOGGER.info("OrderServiceImpl :: updateUberOrderStatus : enable notification is true: Order Date="
							+ orderR.getCreatedOn());
					LOGGER.info("OrderServiceImpl :: updateUberOrderStatus : enable notification is true: Order Link="
							+ orderLink);
					final String FINALCONTACT = contact;
					final String FINALREQUESTID = requestId;
					if (enableNotification == false) {

						if (orderR.getPaymentMethod().equals("Cash")) {
							LOGGER.info("OrderServiceImpl :: updateUberOrderStatus : method type===sms==" + methodType);
							LOGGER.info("method type===sms==" + methodType);

							final String FINALSMSORDERLINK = environment.getProperty("WEB_BASE_URL") + "/receipt?orderid="
									+ EncryptionDecryptionUtil.encryption(orderR.getId().toString()) + "&customerid="
									+ EncryptionDecryptionUtil.encryption(orderR.getCustomer().getId().toString());
							LOGGER.info(FINALSMSORDERLINK);
							LOGGER.info(
									"OrderServiceImpl :: updateUberOrderStatus : FINALSMSORDERLINK " + FINALSMSORDERLINK);

							if (autoAccpetOrder == true && autoAcceptFutureOrder == false
									&& (((futureOrderType.equals("on")) && (!futureDate.equals("select"))))) {
								LOGGER.info("OrderServiceImpl :: updateUberOrderStatus : when autoAccpetOrder is true");
								LOGGER.info("OrderServiceImpl :: updateUberOrderStatus :when autoAccpetOrder is true");

								DateUtil date = new DateUtil();
								if (date.checkCurrentTimeExistBetweenTwoTime(openingTime, closinggTime, timeZoneCode)) {

									LOGGER.info(
											"OrderServiceImpl :: updateUberOrderStatus : when autoAccpetOrder is false : current time is between opening closing hours");
									LOGGER.info("opening closing hours is matched");
									final NotificationTracker notificationTracker2 = notificationTracker;
									new Thread() {
										public void run() {
											String googleShortURL = null;
											// googleShortURL=ShortCodeUtil.generateShortUrl(FINALSMSORDERLINK);
											googleShortURL = ShortCodeUtil.getTinyUrl(FINALSMSORDERLINK);
											HttpPost postRequest = new HttpPost(
													environment.getProperty("CommunicationPlatform_BASE_URL_SERVER")
															+ "/sendFoodKonnketSMSForOrder");
											String request = "{\"mobileNumber\":\"" + FINALCONTACT
													+ "\",\"messsage\":\"You got a new online order please view this link "
													+ googleShortURL + "\",\"orgId\":\"" + FINALREQUESTID + "\"} ";
											String response = convertToStringJson(postRequest, request,environment);
											LOGGER.info("response for SMS=== " + response);
											LOGGER.info("OrderServiceImpl :: updateUberOrderStatus : response for SMS=== "
													+ response);
											if (response.contains("SMS sent successfully")) {
												IConstant.smsStatus = "sent";
											} else
												IConstant.smsStatus = "not sent";
											notificationTracker2.setStatus(IConstant.smsStatus);
											notificatonRepo.save(notificationTracker2);
										}
									}.start();
									notificationTracker.setId(null);
								} else {
									notificationTracker.setStatus("Not sent");
									notificatonRepo.save(notificationTracker);
								}
							} else if (autoAccpetOrder == true && autoAcceptFutureOrder == true
									&& (((futureOrderType.equals("on")) && (!futureDate.equals("select"))))) {
								LOGGER.info(
										"OrderServiceImpl :: updateUberOrderStatus : autoAccpetOrder is true & autoAcceptFutureOrder is true");
								LOGGER.info("autoAccpetOrder is true & autoAcceptFutureOrder is true");
								DateUtil date = new DateUtil();
								if (date.checkCurrentTimeExistBetweenTwoTime(openingTime, closinggTime, timeZoneCode)) {
									LOGGER.info(
											"OrderServiceImpl :: updateUberOrderStatus : autoAccpetOrder is true & autoAcceptFutureOrder is true : current time is between opening closing hours");
									LOGGER.info("when time matches");
									final NotificationTracker notificationTracker2 = notificationTracker;
									new Thread() {
										public void run() {
											String googleShortURL = null;
											// googleShortURL=ShortCodeUtil.generateShortUrl(FINALSMSORDERLINK);
											googleShortURL = ShortCodeUtil.getTinyUrl(FINALSMSORDERLINK);
											HttpPost postRequest = new HttpPost(
													environment.getProperty("CommunicationPlatform_BASE_URL_SERVER")
															+ "/sendFoodKonnketSMSForOrder");
											String request = "{\"mobileNumber\":\"" + FINALCONTACT
													+ "\",\"messsage\":\"You got a new online order please view this link "
													+ googleShortURL + "\",\"orgId\":\"" + FINALREQUESTID + "\"} ";
											String response = convertToStringJson(postRequest, request,environment);
											LOGGER.info("response for SMS=== " + response);
											if (response.contains("SMS sent successfully")) {
												IConstant.smsStatus = "sent";
											} else
												IConstant.smsStatus = "not sent";
											notificationTracker2.setStatus(IConstant.smsStatus);
											notificatonRepo.save(notificationTracker2);
											LOGGER.info("OrderServiceImpl :: updateUberOrderStatus : response for SMS=== "
													+ response);
										}
									}.start();
								} else {
									notificationTracker.setStatus("Not sent");
									notificatonRepo.save(notificationTracker);
								}
							} else {

								LOGGER.info("order now case");
								final NotificationTracker notificationTracker2 = notificationTracker;
								new Thread() {
									public void run() {
										String googleShortURL = null;
										// googleShortURL=ShortCodeUtil.generateShortUrl(FINALSMSORDERLINK);
										googleShortURL = ShortCodeUtil.getTinyUrl(FINALSMSORDERLINK);
										HttpPost postRequest = new HttpPost(
												environment.getProperty("CommunicationPlatform_BASE_URL_SERVER")
														+ "/sendFoodKonnketSMSForOrder");
										String request = "{\"mobileNumber\":\"" + FINALCONTACT
												+ "\",\"messsage\":\"You got a new online order please view this link "
												+ googleShortURL + "\",\"orgId\":\"" + FINALREQUESTID + "\"} ";
										String response = convertToStringJson(postRequest, request,environment);
										LOGGER.info("response for SMS=== " + response);
										if (response.contains("SMS sent successfully")) {
											IConstant.smsStatus = "sent";
										} else
											IConstant.smsStatus = "not sent";
										notificationTracker2.setStatus(IConstant.smsStatus);
										notificatonRepo.save(notificationTracker2);
									}
								}.start();
							}
						}
					} else if (enableNotification == true) {
						if (orderR.getPaymentMethod().equals("Cash")) {
							LOGGER.info("method type===sms==" + methodType);

							final String FINALSMSORDERLINK = environment.getProperty("WEB_BASE_URL") + "/receipt?orderid="
									+ EncryptionDecryptionUtil.encryption(orderR.getId().toString()) + "&customerid="
									+ EncryptionDecryptionUtil.encryption(orderR.getCustomer().getId().toString());
							LOGGER.info(FINALSMSORDERLINK);

							if ((futureOrderType.equals("on")) && (!futureDate.equals("select"))) {
								LOGGER.info("order is future order");
								DateUtil date = new DateUtil();
								if (date.checkCurrentTimeExistBetweenTwoTime(openingTime, closinggTime, timeZoneCode)) {
									LOGGER.info("time matches");
									final NotificationTracker notificationTracker2 = notificationTracker;
									new Thread() {
										public void run() {
											String googleShortURL = null;
											// googleShortURL=ShortCodeUtil.generateShortUrl(FINALSMSORDERLINK);
											googleShortURL = ShortCodeUtil.getTinyUrl(FINALSMSORDERLINK);
											HttpPost postRequest = new HttpPost(
													environment.getProperty("CommunicationPlatform_BASE_URL_SERVER")
															+ "/sendFoodKonnketSMSForOrder");
											String request = "{\"mobileNumber\":\"" + FINALCONTACT
													+ "\",\"messsage\":\"You got a new online order please view this link "
													+ googleShortURL + "\",\"orgId\":\"" + FINALREQUESTID + "\"} ";
											String response = convertToStringJson(postRequest, request,environment);
											LOGGER.info("response for SMS=== " + response);
											if (response.contains("SMS sent successfully")) {
												IConstant.smsStatus = "sent";
											} else
												IConstant.smsStatus = "not sent";
											notificationTracker2.setStatus(IConstant.smsStatus);
											notificatonRepo.save(notificationTracker2);
										}
									}.start();

								} else {
									notificationTracker.setStatus("Not sent");
									notificatonRepo.save(notificationTracker);
								}
							} else {
								LOGGER.info("order now case");
								final NotificationTracker notificationTracker2 = notificationTracker;
								new Thread() {
									public void run() {
										String googleShortURL = null;
										// googleShortURL=ShortCodeUtil.generateShortUrl(FINALSMSORDERLINK);
										googleShortURL = ShortCodeUtil.getTinyUrl(FINALSMSORDERLINK);
										HttpPost postRequest = new HttpPost(
												environment.getProperty("CommunicationPlatform_BASE_URL_SERVER")
														+ "/sendFoodKonnketSMSForOrder");
										String request = "{\"mobileNumber\":\"" + FINALCONTACT
												+ "\",\"messsage\":\"You got a new online order please view this link "
												+ googleShortURL + "\",\"orgId\":\"" + FINALREQUESTID + "\"} ";
										String response = convertToStringJson(postRequest, request,environment);
										LOGGER.info("response for SMS=== " + response);
										if (response.contains("SMS sent successfully")) {
											IConstant.smsStatus = "sent";
										} else
											IConstant.smsStatus = "not sent";
										notificationTracker2.setStatus(IConstant.smsStatus);
										notificatonRepo.save(notificationTracker2);
									}
								}.start();
							}
						}
					}
				} else if (methodType.equals("FAX") && enableNotification != null) {

					LOGGER.info("OrderServiceImpl :: updateUberOrderStatus : inside methodType " + methodType);

					contact = method.getContact();
					LOGGER.info(orderIdd);
					LOGGER.info(customerrId);
					final String FINALCONTACT = contact;
					final String FINALCUSTOMERID = customerrId;
					final String FINALORDERIDD = orderIdd;
					final OrderR FINALORDERR = orderR;
					if (orderR.getPaymentMethod().equals("Cash")) {
						if (orderIdd != null && customerrId != null && contact != null) {
							new Thread() {
								public void run() {
									String response = null;
									try {
										if (IConstant.flag == false) {
											response = ProducerUtil.sendFax(FINALORDERIDD, FINALCUSTOMERID,environment);
										}
										try {
											if (FINALCONTACT.contains("@")) {
												if (ProducerUtil.isValid(FINALCONTACT)) {
													LOGGER.info(FINALCONTACT + " is valid");
													boolean faxResponse = MailSendUtil.sendFaxThroughMail(FINALORDERIDD,
                                                                                                          FINALCONTACT,environment);
												} else {
													LOGGER.info(FINALCONTACT + " is invalid");
												}
											} else {
												FaxUtility.sendFaxFile(FINALCONTACT,
														environment.getProperty("FAX_FILE_PATH") + Integer.parseInt(
																EncryptionDecryptionUtil.decryption(FINALORDERIDD))
																+ ".pdf",
														vendorRepository, onlineNotificationStatusRepository,
														FINALORDERR, 1,environment);
											}
										} catch (NumberFormatException e) {
											LOGGER.error("error: " + e.getMessage());
											LOGGER.error("OrderServiceImpl :: updateUberOrderStatus : Exception " + e);
										} catch (ClientProtocolException e) {
											LOGGER.error("error: " + e.getMessage());
											LOGGER.error("OrderServiceImpl :: updateUberOrderStatus : Exception " + e);
										} catch (IOException e) {
											LOGGER.error("error: " + e.getMessage());
											LOGGER.error("OrderServiceImpl :: updateUberOrderStatus : Exception " + e);
										}
									} catch (UnsupportedEncodingException e) {
										LOGGER.error("error: " + e.getMessage());
										LOGGER.error("OrderServiceImpl :: updateUberOrderStatus : Exception " + e);
									}
									LOGGER.info("OrderServiceImpl :: updateUberOrderStatus : fax response " + response);
								}
							}.start();
						}
					}
				}

				else if (methodType.equals("APP")) {
					LOGGER.info("method type====APP==" + methodType);
					LOGGER.error("OrderServiceImpl :: updateUberOrderStatus : method type====APP== " + methodType);
				}
			}
		}
	}else
	{
		NotificationTracker notificationTracker = new NotificationTracker();
		notificationTracker.setContactId("No");
		notificationTracker.setEnableNotification(notificationType);
		notificationTracker.setMerchantId(merchant.getId());
		notificationTracker.setNotificationType("No");
		notificationTracker.setOrderId(orderR.getId());
		notificationTracker.setStatus("Ok");
		notificatonRepo.save(notificationTracker);

	}
	// Notification method ends
	LOGGER.info("----------------End :: OrderServiceImpl : updateUberOrderStatus :------------------");
	return true;
	}
	catch(
	Exception exception)
	{
		exception.printStackTrace();
		LOGGER.error("OrderServiceImpl :: updateUberOrderStatus : Exception " + exception);
		MailSendUtil.sendExceptionByMail(exception,environment);
		return false;
	}
	
	}
	
	public static void printOrderReceipt(Printer printer, String orderIdd, String customerrId, Merchant merchant,Environment environment)
			throws Exception {
		LOGGER.info(
				"-----------------------OrderServiceImpl :: inside printOrderReceipt : Start-----------------------------------------");
		String mailMessage = null;
		if (printer != null && printer.getPrinterId() != null && printer.getPrinterRefreshToken() != null) {
			LOGGER.info("OrderServiceImpl : printOrderReceipt :: printerId " + printer.getPrinterId());
			LOGGER.info("OrderServiceImpl : printOrderReceipt :: refreshToken " + printer.getPrinterRefreshToken());
			LOGGER.info(" ===orderId :  " + orderIdd + " customerId : " + customerrId);
			ProducerUtil.sendFax(orderIdd, customerrId,environment);
			Boolean isOnline = CloudPrintUtil.checkIfPrinterOnline(printer.getPrinterId(),
					printer.getPrinterRefreshToken(),environment);
			LOGGER.info("OrderServiceImpl : printOrderReceipt :: printerOnlineOrNot " + isOnline);
			if (isOnline == true) {
				CloudPrintUtil.submitPrintJobs(
						environment.getProperty("THERMAL_RECEIPT") + EncryptionDecryptionUtil.decryption(orderIdd) + ".pdf",
						printer.getPrinterId(), printer.getPrinterRefreshToken(),environment);
			} else {
				if (merchant.getEmailId() != null && !merchant.getEmailId().isEmpty()) {
					LOGGER.info("OrderServiceImpl : printOrderReceipt :: Your printer is Offline.");
					mailMessage = "Your printer is Offline.";
					MailSendUtil.cloudPrinterMail(merchant.getEmailId(), mailMessage, merchant.getName(),environment);
					LOGGER.info(
							"OrderServiceImpl : printOrderReceipt :: Mail Send successfully " + merchant.getEmailId());
				}
			}
		} else {
			if (merchant.getEmailId() != null && !merchant.getEmailId().isEmpty()) {
				LOGGER.info("OrderServiceImpl : printOrderReceipt :: PrinterId is not found.");
				mailMessage = "PrinterId is not found.";
				MailSendUtil.cloudPrinterMail(merchant.getEmailId(), mailMessage, merchant.getName(),environment);
				LOGGER.info("OrderServiceImpl : printOrderReceipt :: Mail Send successfully " + merchant.getEmailId());
			}
		}
		LOGGER.info(
				"-----------------------OrderServiceImpl :: inside printOrderReceipt : End-------------------------------------------");
	}
	
	public static String convertToStringJson(HttpPost postRequest, String customerJson,Environment environment) {
		StringBuilder responseBuilder = new StringBuilder();
		try {
			HttpClient httpClient = HttpClientBuilder.create().build();
			StringEntity input = new StringEntity(customerJson);
			input.setContentType("application/json");
			postRequest.setEntity(input);
			HttpResponse response = httpClient.execute(postRequest);
			LOGGER.info("Output from Server .... \n");
			BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			String line = "";
			while ((line = rd.readLine()) != null) {
				responseBuilder.append(line);
			}
		} catch (MalformedURLException e) {
			MailSendUtil.sendExceptionByMail(e,environment);
			LOGGER.error("error: " + e.getMessage());
		} catch (IOException e) {
			MailSendUtil.sendExceptionByMail(e,environment);
			LOGGER.error("error: " + e.getMessage());
		}
		return responseBuilder.toString();
	}
	
	public String updateUberInventory(String uberStoreId, Merchant merchant) {
		LOGGER.info("inside UberServiceImpl : updateUberInventory started");
		String response = null;
		
		try {
			response = ProducerUtil.validateStore(uberStoreId);
			
				if (response.equalsIgnoreCase("success")) {
					LOGGER.info("inside UberServiceImpl : Store Id is Valid ");
					
					String responseCode = uploadMenuOnUber(merchant.getId(), uberStoreId);
					if(responseCode != null && responseCode.contains("204")) {
					response = "Inventory updated Succesfully";
					LOGGER.info("inside UberServiceImpl : updateUberInventory : succesfully saved store details");
					}else {
						LOGGER.info("inside UberServiceImpl : responseCode from uploadmenuOnUber == "+responseCode);
						response = "Failed to update Inventory plz try again";
					}
					
				
				} 
			

		} catch (Exception e) {
			response = "Failed to update Inventory plz try again";
			LOGGER.info("exception in : saveUberstore : exception : " + e);
		}

		return response;
	}

//	public String updateUberBusinessHours(String uberStoreId, Merchant merchant) {
//		String response = null;
//		String menuJson = null;
//		try {
//			LOGGER.info("Inside UberserviceImpl : updateUberBusinessHours ");
//			 menuJson = ProducerUtil.getStoreMenu(uberStoreId);
//			
//			 LOGGER.info("Inside UberserviceImpl : updateUberBusinessHours menuJson : "+menuJson);
//			 
//			JSONObject menujsonObject = new JSONObject(menuJson);
//			
//			JSONArray menuArray = menujsonObject.getJSONArray("menus");
//			LOGGER.info("Inside UberserviceImpl : updateUberBusinessHours menuArray : "+menuArray);
//			for (Object object : menuArray) {
//				JSONObject menuObject = (JSONObject) object;
//				JSONArray serviceAvailibilityArray = menuObject.getJSONArray("service_availability");
//				
//				int i=0;
//				while(serviceAvailibilityArray.length()>0)
//				{
//					serviceAvailibilityArray.remove(i);
//				}
//				
//				List<OpeningClosingDay> openingClosingDays = openingClosingDayRepository.findByMerchantId(merchant.getId());
//				
//				for (OpeningClosingDay openingDays : openingClosingDays) {
//					List<OpeningClosingTime> openingclosingTime = openingClosingTimeRepository
//							.findByOpeningClosingDayId(openingDays.getId());
//					List<JSONObject> timePeriod = new ArrayList<JSONObject>();
//					for (OpeningClosingTime time : openingclosingTime) {
//						JSONObject startEndTime = new JSONObject();
//		            if(openingDays.getIsHoliday() == 0) {
//						startEndTime.put("start_time", time.getStartTime());
//						if(!time.getEndTime().equals("00:00"))
//						startEndTime.put("end_time", time.getEndTime());
//						else
//						startEndTime.put("end_time", "23:59");
//		               }else {
//			startEndTime.put("start_time", "01:00");
//			startEndTime.put("end_time", "01:00");
//		                }
//						timePeriod.add(startEndTime);
//					}
//
//					JSONObject time_periods = new JSONObject();
//
//					time_periods.put("time_periods", timePeriod);
//					time_periods.put("day_of_week", openingDays.getDay().toLowerCase());
//					serviceAvailibilityArray.put(time_periods);
//			}
//				LOGGER.info("serviceAvailibilityArray : "+serviceAvailibilityArray);
//			}
//			LOGGER.info("menujsonObject : "+menujsonObject.toString());
//			String responseCode = ProducerUtil.uploadUberMenu(menujsonObject.toString() ,uberStoreId);
//			if(responseCode != null && responseCode.contains("204")) {
//			response = "Business Hours updated Succesfully";
//			LOGGER.info("inside UberServiceImpl : updateUberInventory : succesfully saved store details");
//			}else {
//				LOGGER.info("inside UberServiceImpl : responseCode from uploadmenuOnUber == "+responseCode);
//				response = "Failed to update Business Hours plz try again";
//			}
//		}catch (Exception e) {
//			response = "Failed to update Business Hours plz try again";
//			LOGGER.info("inside UberServiceImpl : updateUberBusinessHours menuJson :  "+menuJson);
//			LOGGER.info("exception in : saveUberstore : exception : " + e);
//		}
//		return response;
//	}
}
