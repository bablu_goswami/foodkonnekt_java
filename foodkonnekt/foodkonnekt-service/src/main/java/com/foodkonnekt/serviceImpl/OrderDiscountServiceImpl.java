package com.foodkonnekt.serviceImpl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.foodkonnekt.model.Merchant;
import com.foodkonnekt.repository.OrderDiscountRepository;
import com.foodkonnekt.service.MerchantService;
import com.foodkonnekt.service.OrderDiscountService;

@Service
public class OrderDiscountServiceImpl implements OrderDiscountService{
		
	@Autowired
	private OrderDiscountRepository orderDiscountRepository;
	
	@Autowired
	private MerchantService merchantService;
	
	public Map<String,Object> getOfferWithHighestRevenue(String merchantUId, String fromDate, String toDate){
		Map<String,Object> responseMap = new HashMap<String,Object>();
		Merchant merchant = merchantService.findByMerchantUid(merchantUId);
		if(merchant!=null && merchant.getId()!=null){
			List<String> couponNameList = orderDiscountRepository.findOfferNameWithHighestRevenue(merchant.getId(), fromDate, toDate);
			List<Double> couponRevenueList = orderDiscountRepository.findOfferValueWithHighestRevenue(merchant.getId(), fromDate, toDate);
			responseMap.put("couponNameList", couponNameList);
			responseMap.put("couponRevenueList", couponRevenueList);
		}
		else{
			responseMap.put("status", "failed");

		}
	    return responseMap;
	}
}
