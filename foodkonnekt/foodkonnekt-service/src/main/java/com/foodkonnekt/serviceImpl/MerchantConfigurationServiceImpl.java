package com.foodkonnekt.serviceImpl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.foodkonnekt.model.Merchant;
import com.foodkonnekt.model.MerchantConfiguration;
import com.foodkonnekt.model.PosIntegration;
import com.foodkonnekt.repository.MerchantConfigurationRepository;
import com.foodkonnekt.repository.PosIntegrationRepository;
import com.foodkonnekt.service.MerchantConfigurationService;

@Service
public class MerchantConfigurationServiceImpl implements MerchantConfigurationService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(MerchantConfigurationServiceImpl.class);
	
	@Autowired
	private MerchantConfigurationRepository merchantConfigurationRepository;

	@Autowired
	private PosIntegrationRepository posIntegrationRepository;


	public MerchantConfiguration saveMerchantConfiguration(Integer merchantId, Boolean enableNotification,Boolean autoAccept,Boolean autoAcceptOrder) {
		LOGGER.info("MerchantConfigurationServiceImpl : Inside saveMerchantConfiguration : Start");
		LOGGER.info("MerchantConfigurationServiceImpl : Inside saveMerchantConfiguration : MerchantId "+merchantId);
		MerchantConfiguration merchantConfigurations = null;
		
		merchantConfigurations = 	merchantConfigurationRepository.findByMerchantId(merchantId);
		if(merchantId!=null && enableNotification!=null) {	
			PosIntegration   posIntegration   = posIntegrationRepository.findByMerchantId(merchantId);
			if(posIntegration!=null) {
				LOGGER.info("MerchantConfigurationServiceImpl : Inside saveMerchantConfiguration : inside posIntegration check");
				posIntegration.setNotificationStatus(enableNotification);
				posIntegrationRepository.save(posIntegration);
			}
		}
		if(merchantConfigurations!=null) {
		merchantConfigurations.setEnableNotification(enableNotification);
		merchantConfigurations.getMerchant().setId(merchantConfigurations.getMerchant().getId());
		merchantConfigurations.setAutoAccept(autoAccept);
		merchantConfigurations.setAutoAcceptOrder(autoAcceptOrder);
		MerchantConfiguration merchantConfigurations1 = merchantConfigurationRepository.save(merchantConfigurations);
		LOGGER.info("MerchantConfigurationServiceImpl : Inside saveMerchantConfiguration : End");
		return merchantConfigurations1;
		}
		else {
			merchantConfigurations = new MerchantConfiguration();
			merchantConfigurations.setEnableNotification(enableNotification);
			merchantConfigurations.setAutoAccept(autoAccept);
			merchantConfigurations.setAutoAcceptOrder(autoAcceptOrder);
			Merchant merchant = new Merchant();
			merchant.setId(merchantId);
			merchantConfigurations.setMerchant(merchant);
			MerchantConfiguration merchantConfigurations1 = merchantConfigurationRepository.save(merchantConfigurations);
			LOGGER.info("MerchantConfigurationServiceImpl : Inside saveMerchantConfiguration : End");
			return merchantConfigurations1;
			
		}
	}


	public MerchantConfiguration findMerchantConfigurationBymerchantId(Integer merchantId) {
		LOGGER.info("MerchantConfigurationServiceImpl : Inside findMerchantConfigurationBymerchantId : Start");
		LOGGER.info("MerchantConfigurationServiceImpl : Inside findMerchantConfigurationBymerchantId : MerchantId "+merchantId);
		
		MerchantConfiguration	merchantConfigurations = 	merchantConfigurationRepository.findByMerchantId(merchantId);
		LOGGER.info("MerchantConfigurationServiceImpl : Inside findMerchantConfigurationBymerchantId : End");
		return merchantConfigurations;
	}

	
}
