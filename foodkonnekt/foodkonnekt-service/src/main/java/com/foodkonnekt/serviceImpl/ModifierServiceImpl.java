package com.foodkonnekt.serviceImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.foodkonnekt.clover.vo.ItemModifiersVO;
import com.foodkonnekt.clover.vo.ModifierGroupViewJsonVo;
import com.foodkonnekt.clover.vo.ModifierGroupViewVo;
import com.foodkonnekt.clover.vo.ModifierViewVo;
import com.foodkonnekt.clover.vo.ModifirViewJsonVo;
import com.foodkonnekt.model.CategoryItem;
import com.foodkonnekt.model.Item;
import com.foodkonnekt.model.ItemModifierGroup;
import com.foodkonnekt.model.ItemModifiers;
import com.foodkonnekt.model.Merchant;
import com.foodkonnekt.model.ModifierGroup;
import com.foodkonnekt.model.ModifierModifierGroupDto;
import com.foodkonnekt.model.Modifiers;
import com.foodkonnekt.repository.ItemModifierGroupRepository;
import com.foodkonnekt.repository.ItemModifiersRepository;
import com.foodkonnekt.repository.ItemmRepository;
import com.foodkonnekt.repository.ModifierGroupRepository;
import com.foodkonnekt.repository.ModifierModifierGroupRepository;
import com.foodkonnekt.repository.ModifiersRepository;
import com.foodkonnekt.service.ModifierService;
import com.foodkonnekt.util.IConstant;
import com.foodkonnekt.util.JsonUtil;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Service
public class ModifierServiceImpl implements ModifierService {

	private static final Logger LOGGER= LoggerFactory.getLogger(ModifierServiceImpl.class);
	
    @Autowired
    private ModifierGroupRepository modifierGroupRepository;

    @Autowired
    private ItemmRepository itemmRepository;

    @Autowired
    private ItemModifierGroupRepository itemModifierGroupRepository;
    
    @Autowired
    private ItemModifiersRepository   itemModifiersRepository;

    @Autowired
    private ModifiersRepository modifiersRepository;

    @Autowired
    private ModifierModifierGroupRepository modifierModifierGroupRepository;

    /**
     * Save modifier group and modifier
     */
    public void saveModifierAndModifierGroup(String modifierJson, Merchant merchant) {
    	LOGGER.info("===============  ModifierServiceImpl : Inside saveModifierAndModifierGroup :: Start  ============= "
    			+ "modifierJson "+modifierJson+" merchant "+merchant.getId());

        JSONObject modifierJObject = new JSONObject(modifierJson);
        JSONArray modifierJSONArray = modifierJObject.getJSONArray("elements");
        for (Object jObj : modifierJSONArray) {

            JSONObject modifierJsonObject = (JSONObject) jObj;

            ModifierGroup modifierGroup = JsonUtil.setModifier(modifierJsonObject, merchant);

            Set<Modifiers> modifiers = JsonUtil.getModifiers(modifierJsonObject, modifierGroup, merchant);
            if (modifiers != null && !modifiers.isEmpty()) {
                modifiersRepository.save(modifiers);          
                
            }
            modifierGroup.setModifiers(modifiers);
            modifierGroupRepository.save(modifierGroup);

            JSONObject itemsJsonObject = modifierJsonObject.getJSONObject("items");
            JSONArray itemJSONArray = itemsJsonObject.getJSONArray("elements");
            if (modifierJSONArray != null) {
                for (Object itemjObj : itemJSONArray) {
                	
                    ItemModifierGroup itemModifierGroup = new ItemModifierGroup();
                    itemModifierGroup.setModifierGroup(modifierGroup);
                    itemModifierGroup.setModifierGroupStatus(IConstant.BOOLEAN_TRUE);
                    JSONObject itemObject = (JSONObject) itemjObj;
                        String itemPosId=null;
                    if (itemObject.toString().contains("id"))
                    	itemPosId=itemObject.getString("id");
                    
                    Item item=null;
                    if (itemPosId != null) {
                    	LOGGER.info(
								"===============  ModifierServiceImpl : Inside saveModifierAndModifierGroup starts :: itemPosId  : "
										+ itemPosId + " merchant " + merchant.getId());

                    	item = itemmRepository.findByPosItemIdAndMerchantId(itemPosId,merchant.getId());
                        if (item != null) {
                            itemModifierGroup.setItem(item);
                        } else {
                        	 item = JsonUtil.getItem(itemObject);
                            if (item != null) {
                                item.setMerchant(merchant);
                                itemmRepository.save(item);
                                itemModifierGroup.setItem(item);
                            }
                        }
                          if(item!=null){
                        	  for(Modifiers modifier:modifiers){
                        		  ItemModifiers itemModifiers = new ItemModifiers();
                        		  itemModifiers.setItem(item);
                        		  itemModifiers.setModifiers(modifier);
                        		  itemModifiers.setModifierStatus(IConstant.BOOLEAN_TRUE);
                        		  itemModifiers.setModifierGroup(modifierGroup);
                        		  itemModifiersRepository.save(itemModifiers);
                        	  }
                          }
                        
                        itemModifierGroupRepository.save(itemModifierGroup);
                        LOGGER.info("===============  ModifierServiceImpl : Inside saveModifierAndModifierGroup :: End  ============= ");

                    }
                }
            }
        }
    }

    
	public void updateModifierAndModifierGroup(String modifierJson, Merchant merchant) {
		 LOGGER.info("===============  ModifierServiceImpl : Inside updateModifierAndModifierGroup :: start  ============= "+"modifierJson "+modifierJson+" merchant "+merchant.getId());
		
		JSONObject modifierJObject = new JSONObject(modifierJson);
		JSONArray modifierJSONArray = modifierJObject.getJSONArray("elements");
		
		for (Object jObj : modifierJSONArray) {
			// loop through the group modifiers
			JSONObject modifierJsonObject = (JSONObject) jObj;
			// ModifierGroup modifierGroup = JsonUtil.setModifier(modifierJsonObject,
			// merchant);
			// Set<Modifiers> modifiers = JsonUtil.getModifiers(modifierJsonObject,
			// modifierGroup, merchant);

			Integer modifierGroupId = null;

			ModifierGroup modifierGroup = modifierGroupRepository
					.findByPosModifierGroupIdAndMerchantId(modifierJsonObject.getString("id"), merchant.getId());
			if (modifierGroup == null) {
				modifierGroup = new ModifierGroup();
				modifierGroupId = null;
			} else {
				modifierGroupId = modifierGroup.getId();
			}
			modifierGroup.setMerchant(merchant);
			if (modifierJsonObject.toString().contains("name"))
				modifierGroup.setName(modifierJsonObject.getString("name"));

			if (modifierJsonObject.toString().contains("id"))
				modifierGroup.setPosModifierGroupId(modifierJsonObject.getString("id"));

			if (modifierJsonObject.toString().contains("showByDefault"))
				if (modifierJsonObject.getBoolean("showByDefault")) {
					modifierGroup.setShowByDefault(IConstant.BOOLEAN_TRUE);
				} else {
					modifierGroup.setShowByDefault(IConstant.BOOLEAN_FALSE);
				}
			LOGGER.info(" modifierGroup " + modifierGroup.getId());
			modifierGroup.setActive(IConstant.BOOLEAN_TRUE);
			modifierGroupRepository.save(modifierGroup);

			// modifiergroup saved, now saving the modifiers
//			JSONObject modifierJson = new JSONObject(modifierJsonOutput);
//			JSONObject modifiersObject = modifierJson.getJSONObject("modifiers");
//			JSONArray modifiersArray = modifiersObject.getJSONArray("elements");
//			
			JSONObject modifiersJsonObject = modifierJsonObject.getJSONObject("modifiers");
			JSONArray modifiersArray = modifiersJsonObject.getJSONArray("elements");

			for (Object modiObject : modifiersArray) {
				JSONObject ModifierObject = (JSONObject) modiObject;
				LOGGER.info(" ===== ModifierObject ==== " + ModifierObject.toString());
				String posModifierId = null;
				Integer modifierId = null;
				if (ModifierObject.toString().contains("id")) {
					posModifierId = ModifierObject.getString("id");
				}
				Modifiers modifier = null;
				if (posModifierId != null) {
					LOGGER.info(" ===== posModifierId === " + posModifierId);
					modifier = modifiersRepository.findByPosModifierIdAndMerchantId(posModifierId, merchant.getId());
					if (modifier != null && modifier.getId() != null) {
						modifierId = modifier.getId();
					}
				}
				if (modifier == null) {
					modifier = new Modifiers();
				}

				if (ModifierObject.toString().contains("id")) {
					modifier.setPosModifierId(ModifierObject.getString("id"));
				}

				if (ModifierObject.toString().contains("name"))
					modifier.setName(ModifierObject.getString("name"));

				if (ModifierObject.toString().contains("price"))
					modifier.setPrice(ModifierObject.getDouble("price") / 100);
				modifier.setMerchant(merchant);
				//TODO: check with sumit 
				modifier.setStatus(IConstant.BOOLEAN_TRUE);
				modifiersRepository.save(modifier);

				if (modifierId == null && modifierGroup != null && modifier != null) {
					//new modifier hence add the linkage				
					ModifierModifierGroupDto groupDto = new ModifierModifierGroupDto();
					groupDto.setModifierGroup(modifierGroup);
					groupDto.setModifiers(modifier);
					groupDto.setStatus(IConstant.BOOLEAN_TRUE);
					modifierModifierGroupRepository.save(groupDto);
				}else if(modifierId != null && modifierGroup != null && modifier != null) {
					//modifier is existing in db , howver the link with this modifier group is new , so only establish the link
					ModifierModifierGroupDto groupDtoExisting =modifierModifierGroupRepository.findByModifierGroupIdAndModifiersId(modifierGroupId, modifierId);
						if(groupDtoExisting == null) {
							groupDtoExisting = new ModifierModifierGroupDto();
							groupDtoExisting.setModifierGroup(modifierGroup);
							groupDtoExisting.setModifiers(modifier);
							groupDtoExisting.setStatus(IConstant.BOOLEAN_TRUE);
							modifierModifierGroupRepository.save(groupDtoExisting);
						}			
				}
			}//modifiers update done

		}//modifierGroups update done
		
		 LOGGER.info("===============  ModifierServiceImpl : Inside updateModifierAndModifierGroup :: End  ============= ");

	}
    
    
    
    
  
    
  

    
	
    
	/**
     * Find modifiers by modifierGroupId
     */
    public List<Modifiers> findModifiersByMerchantId(Integer modifierGroupId) {
        return modifiersRepository.findByModifierGroupId(modifierGroupId);
    }

    /**
     * Find modifierGroups by merchantId
     */
    public List<ModifierGroup> findModifierGroupsByMerchantById(int merchantId) {
    	LOGGER.info("===============  ModifierServiceImpl : Inside findModifierGroupsByMerchantById :: Start  =============merchantId "+merchantId);

        List<ModifierGroup> modifierGroups = modifierGroupRepository.findByMerchantIdAndActive(merchantId,1);
        for (ModifierGroup modifierGroup : modifierGroups) {
            String modifierName = "";
        	LOGGER.info("===============  ModifierServiceImpl : Inside findModifierGroupsByMerchantById :: Start  =============modifierGroup "+modifierGroup.getId());

            List<Modifiers> modifiers = modifierModifierGroupRepository.findByModifierGroupId(modifierGroup.getId());
            int i = 0;
            for (Modifiers modifier : modifiers) {
                modifierName = modifierName + "," + modifier.getName();
                i++;
                if (i == 3)
                    break;
            }
            modifierName = modifierName.trim();
            if (!modifierName.isEmpty())
                modifierGroup.setModifierNames((modifierName.substring(1, modifierName.length() - 1)) + "...");
        }
        LOGGER.info("===============  ModifierServiceImpl : Inside findModifierGroupsByMerchantById :: End  ============= ");

        return modifierGroups;
    }

    /**
     * Find Modifiers by merchantId
     */
    public List<Modifiers> findModifierByMerchantById(int merchantId) {
    	LOGGER.info("===============  ModifierServiceImpl : Inside findModifierByMerchantById :: Start  =============merchantId "+merchantId);

        List<Modifiers> result = new ArrayList<Modifiers>();
        List<Modifiers> modifiers = modifiersRepository.findByMerchantId(merchantId);
        for (Modifiers modifier : modifiers) {
        	modifier.setAction("<a href=editModifier?modifierId=" + modifier.getId()
                    + " class='edit'><i class='fa fa-pencil' aria-hidden='true'></i></a>");
            Set<ModifierGroup> groups = modifier.getModifierGroup();
            int count = 0;
            for (ModifierGroup modifierGroup : groups) {
            	LOGGER.info("===============  ModifierServiceImpl : Inside findModifierByMerchantById :: =============modifierGroup "+modifierGroup.getId());

                List<ItemModifierGroup> list = itemModifierGroupRepository.findByModifierGroupId(modifierGroup.getId());
                if (list != null) {
                    if (!list.isEmpty()) {
                        count = count + list.size();
                    }
                }
            }
            modifier.setItemCount(count);
            modifier.setModifierGroup(null);
            modifier.setId(null);
            modifier.setMerchant(null);
            modifier.setPosModifierId(null);
            result.add(modifier);
        }
        LOGGER.info("===============  ModifierServiceImpl : Inside findModifierByMerchantById :: End  ============= ");

        return result;
    }

    public List<Modifiers> getModifiers(int merchantId) {
    	LOGGER.info("===============  ModifierServiceImpl : Inside getModifiers :: Start  =============merchantId "+merchantId);

        List<Modifiers> modifiers = modifiersRepository.findByMerchantId(merchantId);
        for (Modifiers modifier : modifiers) {
            Set<ModifierGroup> groups = modifier.getModifierGroup();
            int count = 0;
            for (ModifierGroup modifierGroup : groups) {
            	LOGGER.info("===============  ModifierServiceImpl : Inside getModifiers :: =============modifierGroup "+modifierGroup.getId());

                List<ItemModifierGroup> list = itemModifierGroupRepository.findByModifierGroupId(modifierGroup.getId());
                if (list != null) {
                    if (!list.isEmpty()) {
                        count = count + list.size();
                    }
                }
            }
            modifier.setItemCount(count);
        }
        LOGGER.info("===============  ModifierServiceImpl : Inside getModifiers :: Ends");
        return modifiers;
    }

    /**
     * Find modifierGroup count by merchantId
     */
    public Long modifierGroupCountByMerchantId(Integer merchantId) {
    	LOGGER.info("===============  ModifierServiceImpl : Inside modifierGroupCountByMerchantId :: Start  =============merchantId "+merchantId);

        return modifierGroupRepository.modifierGroupCountByMerchantId(merchantId);
    }

    /**
     * Find modifier count by merchantId
     */
    public Long modifierCountByMerchantId(Integer merchantId) {
    	LOGGER.info("===============  ModifierServiceImpl : Inside modifierCountByMerchantId :: Start  =============merchantId "+merchantId);

        return modifiersRepository.modifierCountByMerchantId(merchantId);
    }

    /**
     * Find modifier count of modifierGroup
     */
    public String findModifierCountOfModifierGroup(Integer modifierGroupId, Integer modiferCount) {
    	LOGGER.info("===============  ModifierServiceImpl : Inside findModifierCountOfModifierGroup starts :: modiferCount  : "
				+ modiferCount + " modifierGroupId " + modifierGroupId);

        List<Modifiers> modifiers = modifierModifierGroupRepository.findByModifierGroupId(modifierGroupId);
        if (modifiers != null) {
            if (!modifiers.isEmpty()) {
                int modiferSize = modifiers.size();
                if (modiferCount <= modiferSize) {
                    LOGGER.info("===============  ModifierServiceImpl : Inside findModifierCountOfModifierGroup :: End  ============= ");

                    return "false";
                } else {
                    LOGGER.info("===============  ModifierServiceImpl : Inside findModifierCountOfModifierGroup :: End  ============= ");

                    return "true";
                }
            }
        }
        LOGGER.info("===============  ModifierServiceImpl : Inside findModifierCountOfModifierGroup :: End  ============= ");

        return "true";
    }

    public String findModifierByMerchantById(Integer merchantId, Integer pageDisplayLength, Integer pageNumber,
                    String searchParameter) {
    	LOGGER.info("===============  ModifierServiceImpl : Inside findModifierByMerchantById starts :: pageDisplayLength  : "
				+ pageDisplayLength + " merchant " + merchantId+" pageNumber "+pageNumber+" searchParameter "+searchParameter);

        Pageable pageable = new PageRequest(pageNumber - 1, pageDisplayLength, Sort.Direction.ASC, "id");
        List<ModifierViewVo> result = new ArrayList<ModifierViewVo>();
        Page<Modifiers> modifiers=null;
        if(searchParameter!=null && !searchParameter.isEmpty())
        {
        	modifiers = modifiersRepository.findByMerchantIdAndNameContaining(merchantId,searchParameter, pageable);
        	
        }else    modifiers = modifiersRepository.findByMerchantId(merchantId, pageable);

        for (Modifiers modifier : modifiers.getContent()) {
            
            Set<ModifierGroup> groups = modifier.getModifierGroup();
            int count = 0;
            for (ModifierGroup modifierGroup : groups) {
            	if(modifierGroup.getShowByDefault()!=null && modifierGroup.getShowByDefault()!=IConstant.SOFT_DELETE){
                	LOGGER.info("===============  ModifierServiceImpl : Inside findModifierByMerchantById starts ::  modifierGroupId " + modifierGroup.getId());
                List<ItemModifierGroup> list = itemModifierGroupRepository.findByModifierGroupId(modifierGroup.getId());
                if (list != null) {
                    if (!list.isEmpty()) {
                        count = count + list.size();
                    }
                }
            	}
            }
          
            ModifierViewVo modifierViewVo = new ModifierViewVo();
            modifierViewVo.setId(modifier.getId());
            modifierViewVo.setName(modifier.getName());
            modifierViewVo.setPrice(modifier.getPrice());
            modifierViewVo.setProductUsed(count);
            
            if(modifier.getStatus()==null || modifier.getStatus()==1)
            	modifierViewVo.setStatus("<div><a href=javascript:void(0) class='nav-toggle' mId="
						+ modifier.getId() + " style='color: blue;'>Active</a></div>");
            else
            		modifierViewVo.setStatus("<div><a href=javascript:void(0) class='nav-toggle' mId="
    						+ modifier.getId() + " style='color: blue;'>InActive</a></div>");
            
            modifierViewVo.setAction("<a href=editModifier?modifierId=" + modifier.getId()
                    + " class='edit'><i class='fa fa-pencil' aria-hidden='true'></i></a>");
            result.add(modifierViewVo);
            
        }
        result = getListBasedOnSearchParameter(searchParameter, result);
        ModifirViewJsonVo modifirViewJsonVo = new ModifirViewJsonVo();
        modifirViewJsonVo.setiTotalDisplayRecords((int)modifiers.getTotalElements());
        modifirViewJsonVo.setiTotalRecords((int)modifiers.getTotalElements());
        modifirViewJsonVo.setAaData(result);
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        LOGGER.info("===============  ModifierServiceImpl : Inside findModifierByMerchantById :: End  ============= ");

        return gson.toJson(modifirViewJsonVo);
    }

    private List<ModifierViewVo> getListBasedOnSearchParameter(String searchParameter, List<ModifierViewVo> result) {
    	LOGGER.info("===============  ModifierServiceImpl : Inside getListBasedOnSearchParameter :: Start  =============searchParameter "+searchParameter);

        if (null != searchParameter && !searchParameter.equals("")) {
            List<ModifierViewVo> personsListForSearch = new ArrayList<ModifierViewVo>();
            searchParameter = searchParameter.toUpperCase();
            for (ModifierViewVo modifierViewVo : result) {
                if (modifierViewVo.getName().toUpperCase().indexOf(searchParameter) != -1
                                || modifierViewVo.getPrice().toString().indexOf(searchParameter) != -1
                                || modifierViewVo.getProductUsed().toString().indexOf(searchParameter) != -1) {
                    personsListForSearch.add(modifierViewVo);
                }
            }
            result = personsListForSearch;
            personsListForSearch = null;
        }
        LOGGER.info("===============  ModifierServiceImpl : Inside getListBasedOnSearchParameter :: End  ============= ");

        return result;
    }

    public String findModifierGroupsByMerchantById(Integer merchantId, Integer pageDisplayLength, Integer pageNumber,
                    String searchParameter) {
    	LOGGER.info("===============  ModifierServiceImpl : Inside findModifierGroupsByMerchantById :: Start  =============merchantId "+merchantId
    			+" pageDisplayLength "+pageDisplayLength+" pageNumber "+pageNumber+" searchParameter "+searchParameter);

        Pageable pageable = new PageRequest(pageNumber - 1, pageDisplayLength, Sort.Direction.ASC, "id");
        Page<ModifierGroup> modifierGroups = modifierGroupRepository.findByMerchantIdAndShowByDefaultNot(merchantId, pageable,IConstant.SOFT_DELETE);
        List<ModifierGroupViewVo> result = new ArrayList<ModifierGroupViewVo>();
        for (ModifierGroup modifierGroup : modifierGroups.getContent()) {
        	if(modifierGroup.getShowByDefault()!=null && modifierGroup.getShowByDefault()!=IConstant.SOFT_DELETE){
            ModifierGroupViewVo modifierGroupViewVo = new ModifierGroupViewVo();
            String modifierName = "";
            LOGGER.info("===============  ModifierServiceImpl : Inside findModifierGroupsByMerchantById starts :: modifierGroup " + modifierGroup.getId());

            List<Modifiers> modifiers = modifierModifierGroupRepository.findByModifierGroupId(modifierGroup.getId());
            int i = 0;
            for (Modifiers modifier : modifiers) {
                modifierName = modifierName + "," + modifier.getName();
                i++;
                if (i == 3)
                    break;
            }
            modifierName = modifierName.trim();
            if (!modifierName.isEmpty()) {
                modifierGroupViewVo.setModifiers((modifierName.substring(1, modifierName.length() - 1)) + "...");
            } else {
                modifierGroupViewVo.setModifiers("");
            }
            modifierGroupViewVo.setId(modifierGroup.getId());
            String modifiergroupName = (modifierGroup.getName() !=null) ? modifierGroup.getName() : "";
            modifierGroupViewVo.setModifierGroupName(modifiergroupName);

            modifierGroupViewVo.setAction("<a href=editModifierGroup?modifierGroupId=" + modifierGroup.getId()
                    + " class='edit'><i class='fa fa-pencil' aria-hidden='true'></i></a>");
            if(modifierGroup.getActive()==null || modifierGroup.getActive()==1)
            modifierGroupViewVo.setStatus("<div><a href=javascript:void(0) class='nav-toggle' mgId="
					+ modifierGroup.getId() + " style='color: blue;'>Active</a></div>");
            else 
            modifierGroupViewVo.setStatus("<div><a href=javascript:void(0) class='nav-toggle' mgId="
					+ modifierGroup.getId() + " style='color: blue;'>InActive</a></div>");
            result.add(modifierGroupViewVo);
        }
        }
        result = getGroupListBasedOnSearchParameter(searchParameter, result);
        ModifierGroupViewJsonVo modifirGroupViewJsonVo = new ModifierGroupViewJsonVo();
        modifirGroupViewJsonVo.setiTotalDisplayRecords((int)modifierGroups.getTotalElements());
        modifirGroupViewJsonVo.setiTotalRecords((int)modifierGroups.getTotalElements());
        modifirGroupViewJsonVo.setAaData(result);
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        LOGGER.info("===============  ModifierServiceImpl : Inside findModifierGroupsByMerchantById :: End  ============= ");

        return gson.toJson(modifirGroupViewJsonVo);
    }

    private List<ModifierGroupViewVo> getGroupListBasedOnSearchParameter(String searchParameter,
                    List<ModifierGroupViewVo> result) {
    	LOGGER.info("===============  ModifierServiceImpl : Inside getGroupListBasedOnSearchParameter :: Start  =============searchParameter "+searchParameter);

        if (null != searchParameter && !searchParameter.equals("")) {
            List<ModifierGroupViewVo> personsListForSearch = new ArrayList<ModifierGroupViewVo>();
            searchParameter = searchParameter.toUpperCase();
            for (ModifierGroupViewVo modifierGroupViewVo : result) {
                if (modifierGroupViewVo.getModifierGroupName().toUpperCase().indexOf(searchParameter) != -1
                                || modifierGroupViewVo.getModifiers().toString().indexOf(searchParameter) != -1) {
                    personsListForSearch.add(modifierGroupViewVo);
                }
            }
            result = personsListForSearch;
            personsListForSearch = null;
        }
        LOGGER.info("===============  ModifierServiceImpl : Inside getGroupListBasedOnSearchParameter :: End  ============= ");

        return result;
    }

    public String searchModifiersByTxt(Integer merchantId, String searchTxt) {
    	LOGGER.info("===============  ModifierServiceImpl : Inside searchModifiersByTxt :: Start  =============merchantId "+merchantId+" searchTxt "+searchTxt);

        List<ModifierViewVo> result = new ArrayList<ModifierViewVo>();
        List<Modifiers> modifiers = modifiersRepository.findByMerchantIdAndModifierName(merchantId, searchTxt);

        for (Modifiers modifier : modifiers) {
            ModifierViewVo modifierViewVo = new ModifierViewVo();
            Set<ModifierGroup> groups = modifier.getModifierGroup();
            int count = 0;
            for (ModifierGroup modifierGroup : groups) {
                LOGGER.info("===============  ModifierServiceImpl : Inside searchModifiersByTxt starts :: modifierGroup " + modifierGroup.getId());

                List<ItemModifierGroup> list = itemModifierGroupRepository.findByModifierGroupId(modifierGroup.getId());
                if (list != null) {
                    if (!list.isEmpty()) {
                        count = count + list.size();
                    }
                }
            }
            modifierViewVo.setId(modifier.getId());
            modifierViewVo.setName(modifier.getName());
            modifierViewVo.setPrice(modifier.getPrice());
            modifierViewVo.setProductUsed(count);
            
            modifierViewVo.setAction("<a href=editModifier?modifierId=" + modifier.getId()
                    + " class='edit'><i class='fa fa-pencil' aria-hidden='true'></i></a>");
            if(modifier.getStatus()==null || modifier.getStatus()==1)
            	modifierViewVo.setStatus("<div><a href=javascript:void(0) class='nav-toggle' mId="
						+ modifier.getId() + " style='color: blue;'>Active</a></div>");
            else
            		modifierViewVo.setStatus("<div><a href=javascript:void(0) class='nav-toggle' mId="
    						+ modifier.getId() + " style='color: blue;'>InActive</a></div>");
            
            result.add(modifierViewVo);
        }
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        LOGGER.info("===============  ModifierServiceImpl : Inside searchModifiersByTxt :: End  ============= ");

        return gson.toJson(result);
    }

    public String searchModifierGroupByTxt(Integer merchantId, String searchTxt) {
    	LOGGER.info("===============  ModifierServiceImpl : Inside searchModifierGroupByTxt :: Start  =============merchantId "+merchantId+" searchTxt "+searchTxt);

        List<ModifierGroup> modifierGroups = modifierGroupRepository.findByMerchantIdAndModifierGroupName(merchantId, searchTxt);
        List<ModifierGroupViewVo> result = new ArrayList<ModifierGroupViewVo>();
        for (ModifierGroup modifierGroup : modifierGroups) {
        	if(modifierGroup.getShowByDefault()!=null && modifierGroup.getShowByDefault()!=IConstant.SOFT_DELETE){
            ModifierGroupViewVo modifierGroupViewVo = new ModifierGroupViewVo();
            String modifierName = "";
            LOGGER.info("===============  ModifierServiceImpl : Inside searchModifierGroupByTxt starts :: modifierGroup "
					+ modifierGroup.getId());

            List<Modifiers> modifiers = modifierModifierGroupRepository.findByModifierGroupId(modifierGroup.getId());
            int i = 0;
            for (Modifiers modifier : modifiers) {
                modifierName = modifierName + "," + modifier.getName();
                i++;
                if (i == 3)
                    break;
            }
            modifierName = modifierName.trim();
            if (!modifierName.isEmpty()) {
                modifierGroupViewVo.setModifiers((modifierName.substring(1, modifierName.length() - 1)) + "...");
            } else {
                modifierGroupViewVo.setModifiers("");
            }
            modifierGroupViewVo.setId(modifierGroup.getId());
            modifierGroupViewVo.setModifierGroupName(modifierGroup.getName());
            modifierGroupViewVo.setAction("<a href=editModifierGroup?modifierGroupId=" + modifierGroup.getId()
                    + " class='edit'><i class='fa fa-pencil' aria-hidden='true'></i></a>");
            
            
            result.add(modifierGroupViewVo);
        	}
        }
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        LOGGER.info("===============  ModifierServiceImpl : Inside searchModifierGroupByTxt :: End  ============= ");

        return gson.toJson(result);
    }

	public List<ItemModifiersVO> findModifiersbyModifierGroup(Integer modifierGroupId,Integer itemId) {
		LOGGER.info("===============  ModifierServiceImpl : Inside findModifiersbyModifierGroup :: Start  =============modifierGroupId "+modifierGroupId+" itemId "+itemId);

		// TODO Auto-generated method stub
		ItemModifierGroup itemModifierGroup=itemModifierGroupRepository.findOne(modifierGroupId);
		List<ItemModifiers> itemModifiers=itemModifiersRepository.findByModifierGroupIdAndItemIdAndModifiersStatus(itemModifierGroup.getModifierGroup().getId(),itemId,1);
		List<ItemModifiersVO> itemModifiersVOs= null;
		if(itemModifiers!=null && itemModifiers.size()>0){
			itemModifiersVOs= new ArrayList<ItemModifiersVO>();
			for(ItemModifiers itemModifier:itemModifiers){
				ItemModifiersVO itemModifiersVO = new ItemModifiersVO();
				itemModifiersVO.setId(itemModifier.getId());
				itemModifiersVO.setModifierId(itemModifier.getModifiers().getId());
				itemModifiersVO.setModifierNmae(itemModifier.getModifiers().getName());
				itemModifiersVO.setModifierGroupName(itemModifier.getModifierGroup().getName());
				itemModifiersVO.setModifierStatus(itemModifier.getModifierStatus());
				itemModifiersVOs.add(itemModifiersVO);
			}
			
		}
		
		LOGGER.info("===============  ModifierServiceImpl : Inside findModifiersbyModifierGroup :: End  ============= ");

		return itemModifiersVOs;
	}

	public Modifiers findModifiersByModifiersId(int modifierId) {
		LOGGER.info("===============  ModifierServiceImpl : Inside findModifiersByModifiersId :: Start  =============modifierId "+modifierId);
 
		Modifiers modifiers = modifiersRepository.findOne(modifierId);
		LOGGER.info("===============  ModifierServiceImpl : Inside findModifiersByModifiersId :: End  ============= ");

		return modifiers;
		}
		
		
	public ModifierGroup findModifierGroupByModifiersId(int modifierGroupId) {
		// TODO Auto-generated method stub
		LOGGER.info("----------------Start :: ModifierServiceImpl : findModifierGroupByModifiersId------------------------");
		ModifierGroup modifierGroup = modifierGroupRepository.findOne(modifierGroupId);
		LOGGER.info("ModifierServiceImpl :: findModifierGroupByModifiersId : modifierGroupId "+modifierGroupId);
		LOGGER.info("----------------End :: ModifierServiceImpl : findModifierGroupByModifiersId------------------------");
		return modifierGroup;
	}
	
	
	

//	public void updateLineModifierValue(Modifiers modifier) {
//		LOGGER.info("===============  ModifierServiceImpl : Inside updateLineModifierValue :: Start  =============modifier "+modifier.getId());
//
//		Modifiers resultModifiers = modifiersRepository.findOne(modifier.getId());
//		 
//		if(resultModifiers!=null)
//		 {
//			 if(modifier.getName()!=null && !modifier.getName().isEmpty())
//		       resultModifiers.setName(modifier.getName());
//			 if(modifier.getPrice()!=null)
//		        resultModifiers.setPrice(modifier.getPrice());
//            modifiersRepository.save(resultModifiers);
//		 }
//		LOGGER.info("===============  ModifierServiceImpl : Inside updateLineModifierValue :: End  ============= ");
//
//	}
	
	public void updateLineModifierValue(Modifiers modifier,Integer merchantId) {
		Modifiers resultModifier = modifiersRepository.findOne(modifier.getId());;
		 
		if(resultModifier!=null)
		 {
			 if(modifier.getName()!=null && !modifier.getName().isEmpty())
				 resultModifier.setName(modifier.getName());
			 if(modifier.getPrice()!=null)
				 resultModifier.setPrice(modifier.getPrice());
            
		 }
		


		List<ModifierModifierGroupDto> modifierModifierGroup=
				modifierModifierGroupRepository.findByModifiersIdAndModifierGroupActive(modifier.getId() ,1);
		
			if(modifierModifierGroup!=null &&  !modifierModifierGroup.isEmpty())
			{
				for (ModifierModifierGroupDto modifierModifierGroup2 : modifierModifierGroup) {
					if(modifier.getModifiergroupId()!=null && !modifier.getModifiergroupId().isEmpty())
					{
					for(Integer ids:modifier.getModifiergroupId())
					{
						if(modifierModifierGroup2.getModifierGroup().getId().equals(ids))
						{
							modifierModifierGroup2.setStatus(1);
							break;
						}
						else modifierModifierGroup2.setStatus(0);
					}
					modifierModifierGroupRepository.save(modifierModifierGroup2);
				}else { modifierModifierGroup2.setStatus(0);
				modifierModifierGroupRepository.save(modifierModifierGroup2);
				}
				}
				
			}
				
			
			
			List<ModifierGroup> dbunmappedModifierGroup = modifierGroupRepository
					.findByMerchantIdAndModifierIdAndStatus(merchantId,modifier.getId() ,1);
			System.err.println("");
				if(dbunmappedModifierGroup!=null &&  !dbunmappedModifierGroup.isEmpty())
				{
					for (ModifierGroup unMapmodifierGroup : dbunmappedModifierGroup) {
						if(modifier.getUnmapmodifiergroupId()!=null && !modifier.getUnmapmodifiergroupId().isEmpty())
						{
						for(Integer ids:modifier.getUnmapmodifiergroupId())
						{
							if(unMapmodifierGroup.getId().equals(ids))
							{
								ModifierModifierGroupDto modifierModifierGroupDto =new ModifierModifierGroupDto();
								modifierModifierGroupDto.setModifiers(resultModifier);
								modifierModifierGroupDto.setModifierGroup(unMapmodifierGroup);
								modifierModifierGroupDto.setStatus(1);
								modifierModifierGroupRepository.save(modifierModifierGroupDto);
								break;
							}
						}
						
					}
				}
					
			}
	        LOGGER.info("===============  ModifierServiceImpl : Inside updateLineModifierValue :: End  ============= ");

				modifiersRepository.save(resultModifier);
	}

	
//	public void updateModifierGroupValue(ModifierGroup modifierGroup) {
//		LOGGER.info("===============  ModifierServiceImpl : Inside updateModifierGroupValue :: Start  =============modifierGroup "+modifierGroup.getId());
//
//		// TODO Auto-generated method stub
//		ModifierGroup resultModifierGroup = modifierGroupRepository.findOne(modifierGroup.getId());
//		
//		resultModifierGroup.setName(modifierGroup.getName());
//	        
//		modifierGroupRepository.save(resultModifierGroup);
//		LOGGER.info("===============  ModifierServiceImpl : Inside updateModifierGroupValue :: End  ============= ");
//
//	}
	
	public void updateModifierGroupValue(ModifierGroup modifierGroup , Integer merchantId) {
		// TODO Auto-generated method stub
		ModifierGroup resultModifierGroup = modifierGroupRepository.findOne(modifierGroup.getId());
		
		resultModifierGroup.setName(modifierGroup.getName());
		
		List<ModifierModifierGroupDto> modifierModifierGroup=
				modifierModifierGroupRepository.findByModifierGroupIdAndModifiersStatus(modifierGroup.getId(),1);
		
			if(modifierModifierGroup!=null &&  !modifierModifierGroup.isEmpty())
			{
				for (ModifierModifierGroupDto modifierModifierGroup2 : modifierModifierGroup) {
					if(modifierGroup.getModifiersId()!=null && !modifierGroup.getModifiersId().isEmpty())
					{
					for(Integer ids:modifierGroup.getModifiersId())
					{
						if(modifierModifierGroup2.getModifiers().getId().equals(ids))
						{
							modifierModifierGroup2.setStatus(1);
							break;
						}
						else modifierModifierGroup2.setStatus(0);
					}
					modifierModifierGroupRepository.save(modifierModifierGroup2);
				}else { modifierModifierGroup2.setStatus(0);
				modifierModifierGroupRepository.save(modifierModifierGroup2);
				}
				}
				
			}
				
			
			
			List<Modifiers> dbunmappedModifiers = modifiersRepository
					.findByMerchantIdAndModifierGroupIdAndStatus(merchantId,modifierGroup.getId(),1);
			System.err.println("");
				if(dbunmappedModifiers!=null &&  !dbunmappedModifiers.isEmpty())
				{
					for (Modifiers unMapmodifier : dbunmappedModifiers) {
						if(modifierGroup.getUnmapmodifiersId()!=null && !modifierGroup.getUnmapmodifiersId().isEmpty())
						{
						for(Integer ids:modifierGroup.getUnmapmodifiersId())
						{
							if(unMapmodifier.getId().equals(ids))
							{
								ModifierModifierGroupDto modifierModifierGroupDto =new ModifierModifierGroupDto();
								modifierModifierGroupDto.setModifiers(unMapmodifier);
								modifierModifierGroupDto.setModifierGroup(resultModifierGroup);
								modifierModifierGroupDto.setStatus(1);
								modifierModifierGroupRepository.save(modifierModifierGroupDto);
								break;
							}
						}
						
					}
				}
					
			}
	        
		modifierGroupRepository.save(resultModifierGroup);
		LOGGER.info("===============  ModifierServiceImpl : Inside updateModifierGroupValue :: End  ============= ");

	}

	public void updateMasterFromModifierGroupOfItems(
			ItemModifierGroup itemModifierGroup) {
		LOGGER.info("===============  ModifierServiceImpl : Inside updateMasterFromModifierGroupOfItems :: Start  =============itemModifierGroup "+itemModifierGroup.getId());

		List<Integer> listItemIds = itemModifierGroup.getItemsIds();
		Integer modifierGroupId = itemModifierGroup.getModifiresGrpId();

		ItemModifierGroup itemModifierGrp = null;
		ModifierGroup modifierGroup = null;
		Item item = null;
		ModifierModifierGroupDto modifierModifierGroupDto = null;
		Modifiers modifiers = null;
		ItemModifiers itemModifiers = null;
		if (listItemIds != null && !listItemIds.isEmpty() && modifierGroupId != null) {
			for (Integer itemIds : listItemIds) {
				    itemModifierGrp = new ItemModifierGroup();
				    item = new Item();
					
					itemModifierGrp = new ItemModifierGroup();
					modifierGroup = new ModifierGroup();
					LOGGER.info(
							"===============  ModifierServiceImpl : Inside updateMasterFromModifierGroupOfItems :: modifierGroup "
									+ modifierGroupId+" itemIds "+itemIds);

					ItemModifierGroup dbItemModifierGroup = itemModifierGroupRepository.findByModifierGroupIdAndItemId(modifierGroupId, itemIds);
					if(dbItemModifierGroup != null){
						LOGGER.info("ItemModifierGroups mapping already exists..");
					}else{
						item.setId(itemIds);
						modifierGroup.setId(modifierGroupId);
						
						itemModifierGrp.setModifierGroup(modifierGroup);
						itemModifierGrp.setItem(item);
						itemModifierGrp.setModifierGroupStatus(1);
						itemModifierGroupRepository.save(itemModifierGrp);
					}
				}
			}

		if (modifierGroupId != null && itemModifierGroup.getModisId() != null
				&& !itemModifierGroup.getModisId().isEmpty()) {

			for (Integer modifierId : itemModifierGroup.getModisId()) {
				modifierModifierGroupDto = new ModifierModifierGroupDto();
				modifiers = new Modifiers();
				modifierGroup = new ModifierGroup();

				ModifierModifierGroupDto dbModifierModifierGroupDto = modifierModifierGroupRepository.findByModifierGroupIdAndModifiersId(modifierGroupId, modifierId);
				if(dbModifierModifierGroupDto != null){
					LOGGER.info("ModifiersModifierGroups Mapping already exists..");
				}else{
					modifiers.setId(modifierId);
					modifierGroup.setId(modifierGroupId);
					modifierModifierGroupDto.setModifiers(modifiers);
					modifierModifierGroupDto.setModifierGroup(modifierGroup);
					modifierModifierGroupDto.setStatus(IConstant.BOOLEAN_TRUE);
					modifierModifierGroupRepository.save(modifierModifierGroupDto);
				}
			}
		}

		if (modifierGroupId != null && itemModifierGroup.getModisId() != null
				&& !itemModifierGroup.getModisId().isEmpty() && listItemIds != null && !listItemIds.isEmpty()) {
			for (Integer itemIds : listItemIds) {
				item = new Item();
				item.setId(itemIds);

				modifierGroup = new ModifierGroup();
				modifierGroup.setId(modifierGroupId);

				for (Integer modifierId : itemModifierGroup.getModisId()) {
					modifiers = new Modifiers();
					modifiers.setId(modifierId);
					itemModifiers = new ItemModifiers();
					
					ItemModifiers dbItemModifiers = itemModifiersRepository.findByModifiersIdAndItemId(modifierId, itemIds);
					if(dbItemModifiers !=null){
						LOGGER.info("ItemModifiers Mapping already exists..");
					}else{
						itemModifiers.setModifiers(modifiers);
						itemModifiers.setModifierGroup(modifierGroup);
						itemModifiers.setItem(item);
						itemModifiers.setModifierStatus(1);
						itemModifiersRepository.save(itemModifiers);
					}
				}
			}
		}
		LOGGER.info("===============  ModifierServiceImpl : Inside updateMasterFromModifierGroupOfItems :: End  ============= ");

	}

	public void createModifier(Modifiers modifiers) {
	        modifiersRepository.save(modifiers);	        
	    }

	public void updateItemeModifierGroupMasterForm(ItemModifierGroup itemModifierGroup) {
		
	}

	public List<Modifiers> findByMerchantIdAndName(Integer merchantId, String name) {
		return modifiersRepository.findByMerchantIdAndName(merchantId, name);
		
	}
	
	
	
	
}