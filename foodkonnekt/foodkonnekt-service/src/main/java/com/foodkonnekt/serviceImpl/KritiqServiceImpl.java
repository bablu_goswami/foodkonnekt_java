package com.foodkonnekt.serviceImpl;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.foodkonnekt.clover.vo.KritiqCustomerVO;
import com.foodkonnekt.clover.vo.KritiqMerchantVO;
import com.foodkonnekt.clover.vo.KritiqQuestionAnswersVO;
import com.foodkonnekt.model.Address;
import com.foodkonnekt.model.Clover;
import com.foodkonnekt.model.Customer;
import com.foodkonnekt.model.CustomerFeedback;
import com.foodkonnekt.model.CustomerFeedbackAnswer;
import com.foodkonnekt.model.FeedbackQuestion;
import com.foodkonnekt.model.FeedbackQuestionCategory;
import com.foodkonnekt.model.Item;
import com.foodkonnekt.model.Merchant;
import com.foodkonnekt.model.MerchantKritiq;
import com.foodkonnekt.model.MerchantSubscription;
import com.foodkonnekt.model.Modifiers;
import com.foodkonnekt.model.OrderDiscount;
import com.foodkonnekt.model.OrderItem;
import com.foodkonnekt.model.OrderItemModifier;
import com.foodkonnekt.model.OrderPizza;
import com.foodkonnekt.model.OrderPizzaCrust;
import com.foodkonnekt.model.OrderPizzaToppings;
import com.foodkonnekt.model.OrderR;
import com.foodkonnekt.model.SocialMediaLinks;
import com.foodkonnekt.model.Subscription;
import com.foodkonnekt.model.Vendor;
import com.foodkonnekt.repository.CustomerFeedbackAnswerRepository;
import com.foodkonnekt.repository.CustomerFeedbackRepository;
import com.foodkonnekt.repository.CustomerrRepository;
import com.foodkonnekt.repository.FeedbackQuestionCategoryRepository;
import com.foodkonnekt.repository.FeedbackQuestionRepository;
import com.foodkonnekt.repository.MerchantKritiqRepository;
import com.foodkonnekt.repository.MerchantRepository;
import com.foodkonnekt.repository.MerchantSubscriptionRepository;
import com.foodkonnekt.repository.OrderDiscountRepository;
import com.foodkonnekt.repository.OrderItemModifierRepository;
import com.foodkonnekt.repository.OrderItemRepository;
import com.foodkonnekt.repository.OrderPizzaCrustRepository;
import com.foodkonnekt.repository.OrderPizzaRepository;
import com.foodkonnekt.repository.OrderPizzaToppingsRepository;
import com.foodkonnekt.repository.OrderRepository;
import com.foodkonnekt.repository.SocialMediaLinksRepository;
import com.foodkonnekt.repository.SubscriptionRepository;
import com.foodkonnekt.repository.VendorRepository;
import com.foodkonnekt.service.BusinessService;
import com.foodkonnekt.service.CustomerService;
import com.foodkonnekt.service.KritiqService;
import com.foodkonnekt.service.MerchantService;
import com.foodkonnekt.util.CloverUrlUtil;
import com.foodkonnekt.util.DateUtil;
import com.foodkonnekt.util.EncryptionDecryptionUtil;
import com.foodkonnekt.util.IConstant;
import com.foodkonnekt.util.ItextFaxUtil;
import com.foodkonnekt.util.UrlConstant;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.property.TextAlignment;
import com.itextpdf.layout.property.UnitValue;

@Service
public class KritiqServiceImpl implements KritiqService {
	private static final Logger LOGGER= LoggerFactory.getLogger(KritiqServiceImpl.class);

	@Autowired
    private Environment environment;
	
	@Autowired
	private FeedbackQuestionCategoryRepository feedbackQuestionCategoryRepository;

	@Autowired
	private FeedbackQuestionRepository feedbackQuestionRepository;

	@Autowired
	private OrderItemRepository orderItemRepository;

	@Autowired
	private OrderRepository orderRepository;
	
	
	@Autowired
	private CustomerFeedbackRepository customerFeedbackRepository;
	
	@Autowired
    private SubscriptionRepository  subscriptionRepository;
	
	@Autowired
	private MerchantSubscriptionRepository merchantSubscriptionRepository;
	
	@Autowired
	private MerchantKritiqRepository merchantKritiqRepository;
	    
	
	@Autowired
	private CustomerrRepository customerrRepository;
	
	@Autowired
	private CustomerFeedbackAnswerRepository customerFeedbackAnswerRepository ;

	@Autowired
	private OrderItemModifierRepository itemModifierRepository;

	@Autowired
	private VendorRepository vendorRepository;
	
	@Autowired
	MerchantRepository merchantRepository;
	
	@Autowired
	private MerchantService merchantService;
	
	@Autowired
	private CustomerService customerService;
	
    @Autowired
    private  BusinessService businessService;
    
    @Autowired
    private SocialMediaLinksRepository mediaLinksRepository;
    
    @Autowired
	private OrderPizzaRepository orderPizzaRepository;
	
	@Autowired 
	private OrderPizzaToppingsRepository orderPizzaToppingsRepository;
	
	@Autowired
	private OrderPizzaCrustRepository orderPizzaCrustRepository;
	
	@Autowired
	private OrderDiscountRepository orderDiscountRepository;
	
	public OrderR getAllOrderInfo(Integer customerID, Integer orderId) {
    	LOGGER.info("----------------Start :: KritiqServiceImpl : getAllOrderInfo()------------------------");
    	LOGGER.info(": KritiqServiceImpl : getAllOrderInfo() :customerID:"+customerID+":orderId:"+orderId);

		// TODO Auto-generated method stub
		OrderR orderRs = orderRepository.findByCustomerIdAndId(customerID, orderId);
		if(orderRs!=null && orderRs.getId()!=null)
		{
		 List<OrderDiscount> orderDiscounts = orderDiscountRepository.findByOrderId(orderRs.getId());
		 if(orderDiscounts!=null && orderDiscounts.size() > 0){
			 for (OrderDiscount orderDiscount : orderDiscounts) {
				 orderDiscount.setOrder(null);
				 orderDiscount.setCustomer(null);
			 }
			 orderRs.setOrderDiscountsList(orderDiscounts);
		 }
	}
	    	LOGGER.info("----------------End :: KritiqServiceImpl : getAllOrderInfo()------------------------");

		return orderRs;
	}

	public String getOrderDetail(Integer orderId) {
    	LOGGER.info("----------------Start :: KritiqServiceImpl : getOrderDetail()------------------------");
    	LOGGER.info(": KritiqServiceImpl : getOrderDetail() : orderId:"+orderId);

		String orderDetails = "";
		List<Map<String, String>> extraList = new ArrayList<Map<String, String>>();
        Map<String, String> poduct = new HashMap<String, String>();
		List<OrderItem> orderItems = orderItemRepository.findByOrderId(orderId);
		 for (OrderItem orderItem : orderItems) {
        if (null != orderItem) {
            if (null != orderItem.getItem()) {
                String items = "<tr><td class='col-md-3 col-sm-3 text-left'><h5><strong>"
                                + orderItem.getItem().getName()
                                + "</strong></h5></td><td class='col-md-3 col-sm-3 text-center'><h5><strong>$"
                                //+ Math.round(orderItem.getItem().getPrice()*100.0)/100.0
                                + String.format("%.2f", orderItem.getItem().getPrice())
                                + "</strong></h5></td><td class='col-md-3 col-sm-3 text-center'><h4>"
                                + orderItem.getQuantity()
                                + "</h4></td><td class='col-md-3 col-sm-3 text-right'><h4>"
                                + "$"
                                //+ Math.round((orderItem.getItem().getPrice() * orderItem.getQuantity())*100.0)/100.0 + "</h4></td></tr>";
                                + String.format("%.2f", orderItem.getItem().getPrice() * orderItem.getQuantity())+ "</h4></td></tr>";
                orderDetails = orderDetails+ "" +items;
                
                poduct.put("product_id", orderItem.getItem().getPosItemId());
                poduct.put("name", orderItem.getItem().getName());
                poduct.put("price", Double.toString(orderItem.getItem().getPrice()));
                poduct.put("qty", Integer.toString(orderItem.getQuantity()));
                List<OrderItemModifier> itemModifiers = itemModifierRepository
                                .findByOrderItemId(orderItem.getId());
                for (OrderItemModifier itemModifier : itemModifiers) {
                    String modifiers = "<tr><td class='col-md-3 col-sm-3 text-left'><h5>"
                                    + itemModifier.getModifiers().getName()
                                    + "</h5></td><td class='col-md-3 col-sm-3 text-center'><h5><strong>$"
                                    //+ Math.round(itemModifier.getModifiers().getPrice()*100.0)/100.0
                                    + String.format("%.2f", itemModifier.getModifiers().getPrice())
                                    + " </strong></h5></td><td class='col-md-3 col-sm-3 text-center'><h4> "
                                    + itemModifier.getQuantity()
                                    + "</h4></td><td class='col-md-3 col-sm-3 text-right'><h4>"
                                    + "$"
                                    //+ Math.round((itemModifier.getModifiers().getPrice() * itemModifier.getQuantity())*100.0)/100.0 + "</h4></td></tr>";
                                    +  String.format("%.2f", itemModifier.getModifiers().getPrice() * itemModifier.getQuantity())+ "</h4></td></tr>";
                    
                    orderDetails = orderDetails + "" + modifiers;
                    Map<String, String> exctra = new HashMap<String, String>();
                    exctra.put("id", itemModifier.getModifiers().getPosModifierId());
                    exctra.put("price", Double.toString(Math.round(itemModifier.getModifiers().getPrice()*100.0)/100.0));
                    exctra.put("name", itemModifier.getModifiers().getName());
                    exctra.put("qty", Integer.toString(orderItem.getQuantity()));
                    extraList.add(exctra);
                }
               
            }
        }
	}
	
		 /* -------------------------------------------------------------For Pizza View---------------------------------------------------------------------------------- */				
			List<OrderPizza> orderPizzas = orderPizzaRepository.findByOrderId(orderId);
			if(!orderPizzas.isEmpty() && orderPizzas != null){
				for (OrderPizza orderPizza : orderPizzas) {

					if (orderPizza != null && orderPizza.getPizzaTemplate() != null) {
						Map<String, String> exctra = new HashMap<String, String>();
								
								Item item = new Item();
								if(orderPizza!=null && orderPizza.getPizzaTemplate()!=null && orderPizza.getPizzaTemplate().getDescription()!= null &&  orderPizza.getPizzaSize() != null && orderPizza.getPizzaSize().getDescription()!=null){
									item.setName(orderPizza.getPizzaTemplate().getDescription() + "(" +orderPizza.getPizzaSize().getDescription() + ")");
									
									String pizza = "<tr><td class='col-md-3 col-sm-3 text-left'><h5><strong>"
			                                + orderPizza.getPizzaTemplate().getDescription() + "(" +orderPizza.getPizzaSize().getDescription() + ")"
			                                + "</strong></h5></td><td class='col-md-3 col-sm-3 text-center'><h5><strong>$"
			                                //+ Math.round(orderPizza.getPrice()*100.0)/100.0
			                                + String.format("%.2f", orderPizza.getPrice())
			                                + "</strong></h5></td><td class='col-md-3 col-sm-3 text-center'><h4>"
			                                + orderPizza.getQuantity()
			                                + "</h4></td><td class='col-md-3 col-sm-3 text-right'><h4>"
			                                + "$"
			                                //+ Math.round(orderPizza.getPrice() * orderPizza.getQuantity()*100.0)/100.0 + "</h4></td></tr>";
			                                + String.format("%.2f", orderPizza.getPrice() * orderPizza.getQuantity()) + "</h4></td></tr>"; 
									orderDetails = orderDetails + ""+ pizza ;
									
									poduct.put("product_id", orderPizza.getPizzaTemplate().getPosPizzaTemplateId());
									poduct.put("name", orderPizza.getPizzaTemplate().getDescription() + "(" +orderPizza.getPizzaSize().getDescription() + ")");
									poduct.put("price", Double.toString(Math.round(orderPizza.getPrice()*100.0)/100.0));
									poduct.put("qty", Integer.toString(orderPizza.getQuantity()));
									
								}
								item.setPrice(Math.round(orderPizza.getPrice()*100.0)/100.0);
								List<OrderPizzaToppings> orderPizzaToppings = orderPizzaToppingsRepository.findByOrderPizzaId(orderPizza.getId());
								if(!orderPizzaToppings.isEmpty() && orderPizzaToppings!=null){
								for (OrderPizzaToppings pizzaToppings : orderPizzaToppings) {
									
									Modifiers itemModifier = new Modifiers();
									if(pizzaToppings!=null && pizzaToppings.getPizzaTopping()!=null && pizzaToppings.getPizzaTopping().getDescription()!=null){
										itemModifier.setName(pizzaToppings.getPizzaTopping().getDescription());
										itemModifier.setPrice(Math.round(pizzaToppings.getPrice()*100.0)/100.0);
										
										String modifiers = "<tr><td class='col-md-3 col-sm-3 text-left'><h5>"
			                                    + pizzaToppings.getPizzaTopping().getDescription();
										        if(pizzaToppings.getSide1()!=null && pizzaToppings.getSide1()){
										            modifiers += " (First Half)";
										        }else if(pizzaToppings.getSide2()!=null && pizzaToppings.getSide2()){
										            modifiers += " (Second Half)";
										        }else{
										            modifiers += " (Full)";
										        }
			                                   modifiers += "</h5></td><td class='col-md-3 col-sm-3 text-center'><h5><strong>$"
			                                    //+ Math.round(pizzaToppings.getPrice()*100.0)/100.0
			                                    + String.format("%.2f", pizzaToppings.getPrice())
			                                    + " </strong></h5></td><td class='col-md-3 col-sm-3 text-center'><h4> "
			                                    + pizzaToppings.getQuantity()
			                                    + "</h4></td><td class='col-md-3 col-sm-3 text-right'><h4>"
			                                    + "$"
			                                    //+ Math.round(pizzaToppings.getPrice() * pizzaToppings.getQuantity()*100)/100 + "</h4></td></tr>";
			                                    + String.format("%.2f", pizzaToppings.getPrice() * pizzaToppings.getQuantity())+ "</h4></td></tr>";
										orderDetails = orderDetails + "" + modifiers;
										
										exctra.put("id", pizzaToppings.getPizzaTopping().getPosPizzaToppingId());
										exctra.put("price", Double.toString(pizzaToppings.getPrice()));
										exctra.put("name", pizzaToppings.getPizzaTopping().getDescription());
										exctra.put("qty", orderPizza.getQuantity().toString());
										extraList.add(exctra);
									}
								}
								
								}
								OrderPizzaCrust orderPizzaCrust = orderPizzaCrustRepository.findByOrderPizzaId(orderPizza.getId());
                                if(orderPizzaCrust != null && orderPizzaCrust.getPizzacrust()!=null){
                                    
                                    String crust = "<tr><td class='col-md-3 col-sm-3 text-left'><h5>"
                                                    + orderPizzaCrust.getPizzacrust().getDescription();
                                    
                                    crust += "</h5></td><td class='col-md-3 col-sm-3 text-center'><h5><strong>$"
                                                   // + orderPizzaCrust.getPrice()
                                    				+ String.format("%.2f", orderPizzaCrust.getPrice())
                                                    + " </strong></h5></td><td class='col-md-3 col-sm-3 text-center'><h4> "
                                                    + orderPizza.getQuantity()
                                                    + "</h4></td><td class='col-md-3 col-sm-3 text-right'><h4>"
                                                    + "$"
                                                    //+  Math.round((orderPizza.getQuantity() * orderPizzaCrust.getPrice())*100.0)/100.0 +"</h4></td></tr>";
                                                    + String.format("%.2f",orderPizza.getQuantity() * orderPizzaCrust.getPrice())+ "</h4></td></tr>";
                                    orderDetails = orderDetails + "" + crust;
                                }
					}
				}
			}
			
/* --------------------------------------------------------------------------------------------------------------------------------------------------------------- */
	    	LOGGER.info("-------------End :: KritiqServiceImpl : getOrderDetail() : --------------------");
 
		 return orderDetails;

	}

//	public List<FeedbackQuestionCategory> getFeedbackQuestionCategorys(String orderType) {
	public List<FeedbackQuestionCategory> getFeedbackQuestionCategorys(String orderType, boolean pickup_flag,boolean delivery_flag, boolean dineIn_flag) {
		// TODO Auto-generated method stub
    	LOGGER.info("-------------Start :: KritiqServiceImpl : getFeedbackQuestionCategorys() : --------------------");
    	LOGGER.info(": KritiqServiceImpl : getFeedbackQuestionCategorys() :orderType:"+orderType+":pickup_flag:"+pickup_flag+":delivery_flag:"+delivery_flag+":dineIn_flag:"+dineIn_flag);
	
		List<FeedbackQuestionCategory> feedbackQuestionCategoriesResult = feedbackQuestionCategoryRepository.findAll();
		List<FeedbackQuestionCategory> feedbackQuestionCategories= new ArrayList<FeedbackQuestionCategory>();
		for (FeedbackQuestionCategory feedbackQuestionCategory : feedbackQuestionCategoriesResult) {
			
			if(pickup_flag && !delivery_flag  && feedbackQuestionCategory.getId()==IConstant.FEEDBACK_CATEGORY_DELIVERY ){
				continue;
				
			}else if(!pickup_flag && delivery_flag  && feedbackQuestionCategory.getId()==IConstant.FEEDBACK_CATEGORY_PICKUP ){
				continue;
			}else if(!dineIn_flag && (feedbackQuestionCategory.getId()==IConstant.FEEDBACK_CATEGORY_DINE_IN || feedbackQuestionCategory.getId()==IConstant.FEEDBACK_OUR_SERVICES )){
				continue;
			}else{
				feedbackQuestionCategories.add(feedbackQuestionCategory);
			}
			List<FeedbackQuestion> feedbackQuestions = feedbackQuestionRepository
					.findByFeedbackQuestionCategoryId(feedbackQuestionCategory.getId());
			feedbackQuestionCategory.setFeedbackQuestions(feedbackQuestions);
		}
    	LOGGER.info("----------------------End :: KritiqServiceImpl : getFeedbackQuestionCategorys() :-----------------------");

		return feedbackQuestionCategories;
	}

	public OrderR validateCustomerAndOrder(String customerId, String orderId) {
    	LOGGER.info("----------------------Start :: KritiqServiceImpl : validateCustomerAndOrder() :-----------------------");
    	LOGGER.info(": KritiqServiceImpl : validateCustomerAndOrder() : customerId:"+customerId+":orderId:"+orderId);

		try {
			if(orderId!=null && orderId.contains(",")){
				orderId=orderId.split(",")[0];
			}
			Integer custId = Integer.parseInt(EncryptionDecryptionUtil.decryption(customerId));
			Integer ordId = Integer.parseInt(EncryptionDecryptionUtil.decryption(orderId));
			OrderR orderR = orderRepository.findByCustomerIdAndId(custId, ordId);
			if (orderR != null) {
		    	LOGGER.info("----------------------End :: KritiqServiceImpl : validateCustomerAndOrder() :-----------------------");

				return orderR;
			} else {
		    	LOGGER.info("----------------------End :: KritiqServiceImpl : validateCustomerAndOrder() :-----------------------");

				return null;
			}

		} catch (Exception exception) {
	    	LOGGER.info("End :: KritiqServiceImpl : validateCustomerAndOrder() :Exception:"+exception);

			return null;
		}

	}

	public void saveCustomerFeedback(CustomerFeedback customerFeedback) {
    	LOGGER.info("----------------------Start :: KritiqServiceImpl : saveCustomerFeedback() :-----------------------");

    	Date date = (customerFeedback.getCustomer()!=null && customerFeedback.getCustomer().getMerchantt().getTimeZone() !=null 
				&& customerFeedback.getCustomer().getMerchantt().getTimeZone().getTimeZoneCode()!=null) ? 
						DateUtil.getCurrentDateForTimeZonee(customerFeedback.getCustomer().getMerchantt().getTimeZone().getTimeZoneCode()) : new Date();
		customerFeedback.setCreateDate(date);
		if(customerFeedback.getMerchant()== null){
	    	LOGGER.info(": KritiqServiceImpl : saveCustomerFeedback() : OrderId:"+customerFeedback.getOrderR().getId());

			OrderR orderR = orderRepository.findOne(customerFeedback.getOrderR().getId());
			if(orderR.getMerchant()!= null){
				customerFeedback.setMerchant(orderR.getMerchant());
			}
			
		}
		CustomerFeedback customerFeedbackResult=customerFeedbackRepository.save(customerFeedback);
    	LOGGER.info(": KritiqServiceImpl : saveCustomerFeedback() : save customerFeedback:");

		  Customer customer= customerFeedback.getCustomer();
		  Customer customerResult = new Customer();
		  final String emailId= customer.getMerchantt().getOwner().getEmail();
		  boolean merchantFeedback=false;
		  final boolean merchantFeedbackStatus;
		  boolean onlineFeedback=false;
		 // final boolean onlineFeedbackStatus;
		   String email = null;
	    	LOGGER.info(": KritiqServiceImpl : saveCustomerFeedback() :  customerId:"+customer.getId());

		  if(customer!=null && customer.getId()!=null){
			  
			   customerResult=customerrRepository.findOne(customer.getId());
		  }else{
		    	LOGGER.info(": KritiqServiceImpl : saveCustomerFeedback() :  customerEmailId():"+customer.getEmailId()+":customerPhoneNumber:"+customer.getPhoneNumber());

		    	if(customer.getEmailId()!=null  && !customer.getEmailId().equals(""))
			 		   customerResult.setEmailId(customer.getEmailId());
			 		   if(customer.getPhoneNumber()!=null && !customer.getPhoneNumber().equals(""))
			 		    customerResult.setPhoneNumber(customer.getPhoneNumber());
				    	LOGGER.info(": KritiqServiceImpl : saveCustomerFeedback() : customerFirstName:"+customer.getFirstName());

			 		  if(customer.getFirstName()!=null && !customer.getFirstName().equals(""))
				 		    customerResult.setFirstName(customer.getFirstName());
			    }
	    	LOGGER.info(": KritiqServiceImpl : saveCustomerFeedback() : customerBirthDayDate:"+customerFeedback.getBdayDate()+":customerAnniversaryDate:"+customerFeedback.getAnniversaryDate());

		   if(customerFeedback.getBdayDate()!=null && !customerFeedback.getBdayDate().equals("")&& customerFeedback.getBdayMonth()!=null && !customerFeedback.getBdayMonth().equals(""))
		    customerResult.setBirthDate(customerFeedback.getBdayMonth()+","+customerFeedback.getBdayDate());
		   if(customerFeedback.getAnniversaryDate()!=null && !customerFeedback.getAnniversaryDate().equals("")&& customerFeedback.getAnniversaryMonth()!=null && !customerFeedback.getAnniversaryMonth().equals(""))
		    customerResult.setAnniversaryDate(customerFeedback.getAnniversaryMonth()+","+customerFeedback.getAnniversaryDate());
		   
		   
		    
		  customerrRepository.save(customerResult);
	    	LOGGER.info(": KritiqServiceImpl : saveCustomerFeedback() : save customer:");

		  int merchantCount=0;
		  final int merchantCountStatus;
		  int supportCount=0;
		  Integer offlineRate = 0;
		  final Integer offlineRateStatus ;
		  Integer onlineRate = 0;
		  List<CustomerFeedbackAnswer> cfaForMerchant = new ArrayList<CustomerFeedbackAnswer>();
		  List<CustomerFeedbackAnswer> cfaForSupport = new ArrayList<CustomerFeedbackAnswer>();
		  List<CustomerFeedbackAnswer> allFeedback = new ArrayList<CustomerFeedbackAnswer>();
		  
		  for(CustomerFeedbackAnswer customerFeedbackAnswer:customerFeedback.getCustomerFeedbackAnswers()){
		   if(customerFeedbackAnswer.getAnswer()!=null){
		   customerFeedbackAnswer.setCustomerFeedback(customerFeedbackResult);
		   customerFeedbackAnswerRepository.save(customerFeedbackAnswer);
	    	LOGGER.info(": KritiqServiceImpl : saveCustomerFeedback() : save customerFeedbackAnswer:");

		   FeedbackQuestionCategory feedbackQuestionCategory=customerFeedbackAnswer.getFeedbackQuestion().getFeedbackQuestionCategory();
		   Integer customerRate=customerFeedbackAnswer.getAnswer();
		   
		   
 		 
		  /* if((feedbackQuestionCategory.getId()==IConstant.FEEDBACK_CATEGORY_DELIVERY ||feedbackQuestionCategory.getId()==IConstant.FEEDBACK_CATEGORY_PICKUP ||feedbackQuestionCategory.getId()==IConstant.FEEDBACK_CATEGORY_FOOD||feedbackQuestionCategory.getId()==IConstant.FEEDBACK_CATEGORY_DINE_IN) && customerRate<3 ){
		    merchantFeedback=true;
		    cfaForMerchant.add(customerFeedbackAnswer);
		    merchantCount++;
		   }*/
		   
		   if((feedbackQuestionCategory.getId()==IConstant.FEEDBACK_CATEGORY_DELIVERY ||feedbackQuestionCategory.getId()==IConstant.FEEDBACK_CATEGORY_PICKUP ||feedbackQuestionCategory.getId()==IConstant.FEEDBACK_CATEGORY_FOOD||feedbackQuestionCategory.getId()==IConstant.FEEDBACK_CATEGORY_DINE_IN||feedbackQuestionCategory.getId()==IConstant.FEEDBACK_OUR_SERVICES)  ){
			   //offlineRate = offlineRate + customerRate;
			   if(customerRate!=null && customerRate<3)
			   {
				   cfaForMerchant.add(customerFeedbackAnswer);
		      } 
			   allFeedback.add(customerFeedbackAnswer);
			    merchantCount++;
		   }
		   
		   /*if(feedbackQuestionCategory.getId()==IConstant.FEEDBACK_CATEGORY_ONLINE_ORDER && customerRate<3 ){
		    onlineFeedback=true;
		    cfaForSupport.add(customerFeedbackAnswer);
		    supportCount++;
		   	}*/
		   
		   if(feedbackQuestionCategory.getId()==IConstant.FEEDBACK_CATEGORY_ONLINE_ORDER  ){
			   //onlineRate = onlineRate + customerRate;
			   if(customerRate!=null && customerRate<3)
			   {
			    cfaForSupport.add(customerFeedbackAnswer);
		      }
			   allFeedback.add(customerFeedbackAnswer);
			    supportCount++;
			   	}
		     }
		 }
		
		  
		 if(customer!= null &&  customer.getMerchantt()!= null && customer.getMerchantt().getOwner()!= null && customer.getMerchantt().getOwner().getEmail()!= null){
			  email = customer.getMerchantt().getOwner().getEmail();
		  }
		  if(customerResult!= null &&  customerResult.getMerchantt()!= null && customerResult.getMerchantt().getOwner()!= null && customerResult.getMerchantt().getOwner().getEmail()!= null){
			  email = customerResult.getMerchantt().getOwner().getEmail();
		  }

		  offlineRateStatus=offlineRate;
		  merchantCountStatus=merchantCount;
		  merchantFeedbackStatus=merchantFeedback;
		  
		  final String merchantTypeFoodKonnekt = "FoodKonnekt";
		  final String merchantTypeMerchant = "";
		  final int supportCountStatus=supportCount;
		  final int onlineRateStatus=onlineRate;
		 // onlineFeedbackStatus=onlineFeedback;
		  final String emailForMerchant = email;
		 
          final String emailForSupport= "support@foodkonnekt.com";
		  final CustomerFeedback feedback=customerFeedback;
		  final Customer customer2=customerResult;
		  
		  final List<CustomerFeedbackAnswer> cfaForMerchantFinal = cfaForMerchant;
		  final List<CustomerFeedbackAnswer> cfaForSupportFinal = cfaForSupport;
		  final List<CustomerFeedbackAnswer> allFeedbackForSupport = allFeedback;
		  Runnable inventoryRunnable = new Runnable() {

			 
			  
              public void run() {
            	  
            	  //MailSendUtil.feedbackResultOfClientToMerchantMail(feedback, customer2, 1, allFeedbackForSupport,emailForSupport, merchantTypeMerchant,"Customer Feedback");
            	System.out.println("fromEmail===="+emailId);
    	    	LOGGER.info(": KritiqServiceImpl : saveCustomerFeedback() : fromEmail===="+emailId);

            	  try {
        			  thankyouMailByMailChamp(customer2,emailId);
        			  }catch (Exception e) {
        				System.out.println(e);
            	    	LOGGER.info(": KritiqServiceImpl : saveCustomerFeedback() : Exception :"+e);

        			   }
            	
            	  if(cfaForMerchantFinal!=null && cfaForMerchantFinal.size()>0){
            		 
            		  try {
          				positiveNegativeFeedbackByMailChamp(customer2, cfaForMerchantFinal,emailForMerchant,environment);
          				positiveNegativeFeedbackByMailChamp(customer2, cfaForMerchantFinal,emailForSupport,environment);
          			} catch (Exception e) {
          				System.out.println(e);
            	    	LOGGER.info(": KritiqServiceImpl : saveCustomerFeedback() : Exception :"+e);

          			} 
            		  
            		  
        			  //MailSendUtil.thankyouMailForFeedback(feedback, customer2, true);
        			 
            		 //MailSendUtil.feedbackResultOfClientToMerchantMail(feedback, customer2, offlineRateStatus/merchantCountStatus, cfaForMerchantFinal,emailForMerchant, merchantTypeFoodKonnekt,"Customer Feedback - Attention Required");
        			 //MailSendUtil.feedbackResultOfClientToMerchantMail(feedback, customer2, offlineRateStatus/merchantCountStatus, cfaForMerchantFinal,emailForSupport, merchantTypeFoodKonnekt,"Customer Feedback - Attention Required");
            	  }
			  if(cfaForSupportFinal!=null && cfaForSupportFinal.size()>0){
				
				  try {
        				positiveNegativeFeedbackByMailChamp(customer2, cfaForMerchantFinal,emailForSupport,environment);
        			} catch (Exception e) {
        				System.out.println(e);
            	    	LOGGER.info(": KritiqServiceImpl : saveCustomerFeedback() : Exception :"+e);

        			} 
				  
        			   //MailSendUtil.thankyouMailForFeedback(feedback, customer2, true);
        			  //MailSendUtil.feedbackResultOfClientToMerchantMail(feedback, customer2, onlineRateStatus/supportCountStatus, cfaForSupportFinal,emailForSupport, merchantTypeMerchant,"Customer Feedback - Attention Required");
				     //MailSendUtil.feedbackResultOfClientToSupportMail(feedback, customer2,supportCountStatus,cfaForSupportFinal);
			  }else{
        			  /*try {
        				  thankyouMailByMailChamp(customer2);
        				  }catch (Exception e) {
        					System.out.println(e);
        				   }
        			  */
        			 // MailSendUtil.thankyouMailForFeedback(feedback, customer2, false);
            	  System.out.println("mail sent");
      	    	LOGGER.info(": KritiqServiceImpl : saveCustomerFeedback() : mail sent :");

        		  }  
              }
          };
          Thread inventoryThread = new Thread(inventoryRunnable);
          inventoryThread.setName("KritiqMailThread");
          inventoryThread.start();
		  		  
		        System.out.println("feedback done");
      	    	LOGGER.info(": KritiqServiceImpl : saveCustomerFeedback() :feedback done:");

		 }
	
	public List<CustomerFeedback> findByCustomerIdAndOrderId(String customerId, String orderid) {
    	LOGGER.info("-----------------Start :: KritiqServiceImpl : findByCustomerIdAndOrderId():---------------");
    	LOGGER.info(": KritiqServiceImpl : findByCustomerIdAndOrderId():customerId:"+customerId+":orderid:"+orderid);

		if(orderid!=null && orderid.contains(",")){
			orderid=orderid.split(",")[0];
		}
		Integer custId = Integer.parseInt(EncryptionDecryptionUtil.decryption(customerId));
		Integer ordId = Integer.parseInt(EncryptionDecryptionUtil.decryption(orderid));
    	LOGGER.info("-------------End : : KritiqServiceImpl : findByCustomerIdAndOrderId():------------------");

		return customerFeedbackRepository.findByCustomerIdAndOrderId(custId, ordId);
	}
	
	public List<CustomerFeedback> findByOrderPosID(String posID) {
    	LOGGER.info("-----------------Start :: KritiqServiceImpl : findByOrderPosID():---------------");
    	LOGGER.info(": KritiqServiceImpl : findByOrderPosID(): posID :"+posID);

		OrderR orderR=orderRepository.findByOrderPosId(posID);
		//System.out.println("merchant id from orderR=="+orderR.getMerchant().getId());
		if(orderR!=null){
	    	LOGGER.info("-----------------End :: KritiqServiceImpl : findByOrderPosID():---------------");

		return customerFeedbackRepository.findByOrderIdAndMerchantId(orderR.getId(),orderR.getMerchant().getId());}
		else
	    	LOGGER.info("-----------------Start :: KritiqServiceImpl : findByOrderPosID(): return null---------------");

			return null;
	}
	
        public List<CustomerFeedback> findByOrderPosIDAndMerchantId(String posID,Integer merchantId) {
        	LOGGER.info("-----------------Start :: KritiqServiceImpl : findByOrderPosIDAndMerchantId():---------------");
        	LOGGER.info(": KritiqServiceImpl : findByOrderPosIDAndMerchantId():posID :"+posID+":merchantId:"+merchantId);

		OrderR orderR=orderRepository.findByOrderPosIdAndMerchantId(posID, merchantId);
		//System.out.println("merchant id from orderR=="+orderR.getMerchant().getId());
		if(orderR!=null){
        	LOGGER.info("-----------------End :: KritiqServiceImpl : findByOrderPosIDAndMerchantId():---------------");

		return customerFeedbackRepository.findByOrderIdAndMerchantId(orderR.getId(),orderR.getMerchant().getId());}
		else
        	LOGGER.info("-----------------End :: KritiqServiceImpl : findByOrderPosIDAndMerchantId():---------------");

			return null;
	}

	public OrderR findByOrderId(String orderid) {
    	LOGGER.info("-----------------Start :: KritiqServiceImpl : findByOrderId():---------------");
    	LOGGER.info(": KritiqServiceImpl : findByOrderId(): orderid:"+orderid);

		if(orderid!=null && orderid.contains(",")){
			orderid=orderid.split(",")[0];
		}
		Integer orderId = Integer.parseInt(EncryptionDecryptionUtil.decryption(orderid));
    	LOGGER.info("-----------------End :: KritiqServiceImpl : findByOrderId():---------------");

		return orderRepository.findOne(orderId);
	}

	public List<OrderR> multipleOrdersListOfCustomer(Date startDate, Date endDate, int custId) {
    	LOGGER.info("-----------------Start :: KritiqServiceImpl : multipleOrdersListOfCustomer():---------------");
    	LOGGER.info(": KritiqServiceImpl : multipleOrdersListOfCustomer(): startDate:"+startDate+":endDate:"+endDate+":customerId:"+custId);
    	LOGGER.info("-----------------End :: KritiqServiceImpl : multipleOrdersListOfCustomer():---------------");

		return orderRepository.findByStartDateAndEndDateAndCustomerId(startDate, endDate, custId);
	}

	public List<FeedbackQuestionCategory> getFeedbackQuestionCategorys() {
    	LOGGER.info("-----------------Start :: KritiqServiceImpl : getFeedbackQuestionCategorys():---------------");

		List<FeedbackQuestionCategory> feedbackQuestionCategoriesResult = feedbackQuestionCategoryRepository.findAll();
		List<FeedbackQuestionCategory> feedbackQuestionCategories= new ArrayList<FeedbackQuestionCategory>();
		for (FeedbackQuestionCategory feedbackQuestionCategory : feedbackQuestionCategoriesResult) {
	    	LOGGER.info(": KritiqServiceImpl : getFeedbackQuestionCategorys(): feedbackQuestionCategoryId:"+feedbackQuestionCategory.getId());

			
			
			if(feedbackQuestionCategory.getId()!=IConstant.FEEDBACK_CATEGORY_PICKUP && feedbackQuestionCategory.getId()!=IConstant.FEEDBACK_CATEGORY_ONLINE_ORDER &&feedbackQuestionCategory.getId()!=IConstant.FEEDBACK_CATEGORY_DELIVERY ){
			
				feedbackQuestionCategories.add(feedbackQuestionCategory);
			}
				List<FeedbackQuestion> feedbackQuestions = feedbackQuestionRepository
						.findByFeedbackQuestionCategoryId(feedbackQuestionCategory.getId());
				feedbackQuestionCategory.setFeedbackQuestions(feedbackQuestions);
				System.out.println(feedbackQuestionCategory.getFeedbackQuestions());
		    	LOGGER.info(": KritiqServiceImpl : getFeedbackQuestionCategorys(): feedbackQuestionCategoryId:"+feedbackQuestionCategory.getFeedbackQuestions());

		}
    	LOGGER.info("-----------------End :: KritiqServiceImpl : getFeedbackQuestionCategorys():---------------");

		return feedbackQuestionCategories;		
		
	}

	
/*	public List<FeedbackQuestionCategory> getFeedbackQuestionCategorysByPosId(Integer posId) {
		List<FeedbackQuestionCategory> feedbackQuestionCategoriesResult = feedbackQuestionCategoryRepository.findAll();
		List<FeedbackQuestionCategory> feedbackQuestionCategories= new ArrayList<FeedbackQuestionCategory>();
		for (FeedbackQuestionCategory feedbackQuestionCategory : feedbackQuestionCategoriesResult) {
			
			
			
			if(feedbackQuestionCategory.getId()!=IConstant.FEEDBACK_CATEGORY_PICKUP && feedbackQuestionCategory.getId()!=IConstant.FEEDBACK_CATEGORY_ONLINE_ORDER &&feedbackQuestionCategory.getId()!=IConstant.FEEDBACK_CATEGORY_DELIVERY ){
			
				feedbackQuestionCategories.add(feedbackQuestionCategory);
			}
				List<FeedbackQuestion> feedbackQuestions = feedbackQuestionRepository.findByFeedbackQuestionCategoryId(feedbackQuestionCategory.getId());
				feedbackQuestionCategory.setFeedbackQuestions(feedbackQuestions);
		}
		return feedbackQuestionCategories;		
		
	}*/

	
	
	
	
	
	public Customer saveWalkInCustomerDetail(Customer customer) {
    	LOGGER.info("-----------------Start :: KritiqServiceImpl : saveWalkInCustomerDetail():---------------");

		if(customer!=null){
			customer.setCustomerType("clover");
		}
    	LOGGER.info("-----------------End :: KritiqServiceImpl : saveWalkInCustomerDetail(): customer saved:---------------");

		return customerrRepository.save(customer);
	}

	public OrderR saveOrderDetails(OrderR orderR) {
    	LOGGER.info("-----------------Start :: KritiqServiceImpl : saveOrderDetails():---------------");
    	LOGGER.info("-----------------End :: KritiqServiceImpl : saveOrderDetails(): order saved:---------------");

		return orderRepository.save(orderR);
		
	}

	public Merchant validateMerchant(String merchantId){
    	LOGGER.info("-----------------Start :: KritiqServiceImpl : validateMerchant():---------------");
    	LOGGER.info(": KritiqServiceImpl : validateMerchant(): merchantId :"+merchantId);

		Integer merchantid = Integer.parseInt(EncryptionDecryptionUtil.decryption(merchantId));
    	LOGGER.info("-----------------End :: KritiqServiceImpl : validateMerchant():---------------");

		return merchantRepository.findOne(merchantid);
		
	}

	public Vendor validateVendor(String vendorId) {
		LOGGER.info("-----------------Start :: KritiqServiceImpl : validateVendor():---------------");
    	LOGGER.info(": KritiqServiceImpl : validateVendor(): vendorId :"+vendorId);

		Integer vendorid = Integer.parseInt(EncryptionDecryptionUtil.decryption(vendorId));
		LOGGER.info("-----------------End :: KritiqServiceImpl : validateVendor():---------------");

		return vendorRepository.findOne(vendorid);
	}

	public List<Merchant> getMerchantsByVendorId(Integer id) {
		LOGGER.info("-----------------Start :: KritiqServiceImpl : getMerchantsByVendorId():---------------");
    	LOGGER.info(": KritiqServiceImpl : getMerchantsByVendorId(): vendorId :"+id);
		LOGGER.info("-----------------End :: KritiqServiceImpl : getMerchantsByVendorId():---------------");

		return merchantRepository.findByOwnerId(id);
	}
	
	public Merchant getMerchantDetailsByMerchantId(Integer id) {
		LOGGER.info("-----------------Start :: KritiqServiceImpl : getMerchantDetailsByMerchantId():---------------");
		LOGGER.info(": KritiqServiceImpl : getMerchantDetailsByMerchantId(): MerchantId :"+id);

		// TODO Auto-generated method stub
		LOGGER.info("-----------------End :: KritiqServiceImpl : getMerchantDetailsByMerchantId():---------------");

		return merchantRepository.findById(id);
	}
	
	
	

	public Vendor getVendorDetailsByVendorId(Integer vendorid) {
		LOGGER.info("-----------------Start :: KritiqServiceImpl : getVendorDetailsByVendorId():---------------");
		LOGGER.info(": KritiqServiceImpl : getVendorDetailsByVendorId(): vendorid:"+vendorid);

		// TODO Auto-generated method stub
		LOGGER.info("-----------------End :: KritiqServiceImpl : getVendorDetailsByVendorId():---------------");

		return vendorRepository.findOne(vendorid);
	}
	
	
	public Merchant getMerchantDetailsByPosMerchantId(String merchantPosId) {
		LOGGER.info("-----------------Start :: KritiqServiceImpl : getMerchantDetailsByPosMerchantId():---------------");
		LOGGER.info(": KritiqServiceImpl : getMerchantDetailsByPosMerchantId(): merchantId :"+merchantPosId);

		// TODO Auto-generated method stub
		LOGGER.info("-----------------End :: KritiqServiceImpl : getMerchantDetailsByPosMerchantId():---------------");

		return  merchantRepository.findByPosMerchantId(merchantPosId);
	}

	public boolean verifyOrderFromClover(String orderId, String merchantId) {
		LOGGER.info("-----------------Start :: KritiqServiceImpl : verifyOrderFromClover():---------------");
		LOGGER.info(": KritiqServiceImpl : verifyOrderFromClover(): merchantId :"+merchantId+":orderId: "+orderId);

		System.out.println("kritiqServiceimpl-"+merchantId+" "+orderId);
    	Clover clover = new  Clover();
    	clover.setInstantUrl(IConstant.CLOVER_INSTANCE_URL);
        clover.setUrl(environment.getProperty("CLOVER_URL"));
        //clover.setUrl("http://localhost:8080");
    	Customer customer = new Customer();
        OrderR orderR = new OrderR();
        Merchant newMerchant = merchantRepository.findByPosMerchantId(merchantId);
        System.out.println(newMerchant.getId());
		LOGGER.info(": KritiqServiceImpl : verifyOrderFromClover(): newMerchantId() :"+newMerchant.getId());

        if(newMerchant!=null)
        {
        	System.out.println("in merchant");
        	clover.setAuthToken(newMerchant.getAccessToken());
        	clover.setMerchantId(newMerchant.getPosMerchantId());
        	String orderResponse = CloverUrlUtil.getFeedbackClover(clover,orderId);
        	if(orderResponse!=null && orderResponse.contains("id")){
        		LOGGER.info("-----------------End :: KritiqServiceImpl : verifyOrderFromClover(): return true ---------------");

        		return true;
        	}else{
        		LOGGER.info("-----------------End :: KritiqServiceImpl : verifyOrderFromClover(): return false ---------------");

        		return false;
        	}
        	/*JSONObject jsonObj = new JSONObject(orderResponse);
        	
        	if(jsonObj.toString().contains("customers")){
        	JSONObject customersObject = jsonObj.getJSONObject("customers");
        	JSONArray customerObjArray = customersObject.getJSONArray("elements");
        	for(Object obj: customerObjArray)
        	{
        		System.out.println("cloverServiceimpl for loop");
        		JSONObject customerObj = (JSONObject) obj;
        		customer.setCustomerPosId(customerObj.getString("id"));
        		orderR.setCustomer(customer);
        		orderR.setOrderPosId(orderId);
        		orderR.setCreatedOn();
        		
        		
        	}
        	 customer = customerrRepository.save(customer);
        		orderR = orderRepository.save(orderR);
        		System.out.println("custId-"+customer.getId()+" ordId-"+orderR.getId());
        		String custId = EncryptionDecryptionUtil.encryption(String.valueOf(customer.getId()));
        		String orderid = EncryptionDecryptionUtil.encryption(String.valueOf(orderR.getId()));
        		System.out.println("cloverServiceimpl before redirect");
        		return "/feedbackForm?customerId="+custId+"&orderId="+orderid;
        		// return "redirect:"+environment.getProperty("WEB_BASE_URL")+"/feedbackForm?customerId="+custId+"&orderId="+orderid;
        		//List<CustomerFeedback> custFeedback = kritiqService.findByCustomerIdAndOrderId(customerId, orderid);
        	*/}
        
		LOGGER.info("-----------------End :: KritiqServiceImpl : verifyOrderFromClover(): return false ---------------");

        return false;
	}

	public OrderR getOrderDetailsByOrderPosId(String o) {
		LOGGER.info("-----------------start :: KritiqServiceImpl : getOrderDetailsByOrderPosId():---------------");
		LOGGER.info(": KritiqServiceImpl : getOrderDetailsByOrderPosId(): OrderPosId :"+o);
		LOGGER.info("-----------------End :: KritiqServiceImpl : getOrderDetailsByOrderPosId():---------------");

		return orderRepository.findByOrderPosId(o);
	}

	public OrderR getOrderDetailsByOrderPosIdAndMerchantId(String orderId,Integer merchantId) {
		LOGGER.info("-----------------start :: KritiqServiceImpl : getOrderDetailsByOrderPosIdAndMerchantId():---------------");
		LOGGER.info(": KritiqServiceImpl : getOrderDetailsByOrderPosIdAndMerchantId(): orderId :"+orderId+":merchantId:"+merchantId);
		LOGGER.info("-----------------End :: KritiqServiceImpl : getOrderDetailsByOrderPosIdAndMerchantId():---------------");

		return orderRepository.findByOrderPosIdAndMerchantId(orderId,merchantId);
	}
	
	public List<CustomerFeedback> findByOrderId(Integer id) {
		LOGGER.info("-----------------start :: KritiqServiceImpl : findByOrderId():---------------");
		LOGGER.info(": KritiqServiceImpl : findByOrderId(): orderId :"+id);
		LOGGER.info("-----------------End :: KritiqServiceImpl : findByOrderId():---------------");

		return customerFeedbackRepository.findByOrderId(id);
	}

	public String checkMerchantExpiration(Merchant merchant) {
		LOGGER.info("-----------------start :: KritiqServiceImpl : checkMerchantExpiration():---------------");

		String merchantKritiqExpiryStatus=null;
		if(merchant!=null ){
			LOGGER.info(": KritiqServiceImpl : checkMerchantExpiration(): ActiveCustomerFeedback:"+ merchant.getActiveCustomerFeedback());

		if( merchant.getActiveCustomerFeedback()!=null && merchant.getActiveCustomerFeedback()==0){
			merchantKritiqExpiryStatus="disabled";
		}else{
			merchantKritiqExpiryStatus="enabled";
		}
		List<MerchantSubscription> merchantSubscriptions =merchantSubscriptionRepository.findByMId(merchant.getId());
		Subscription subscription =null;
		if(merchantSubscriptions!=null && merchantSubscriptions.size()>0){
			if(merchantSubscriptions.get(0).getSubscription()!=null && merchantSubscriptions.get(0).getSubscription().getId()!=null)
		 subscription =subscriptionRepository.findOne(merchantSubscriptions.get(0).getSubscription().getId());
		}
		if(subscription!=null &&  subscription.getPrice()<49.99){
		List<MerchantKritiq> kritiqs= merchantKritiqRepository.findByMerchantId(merchant.getId());
		boolean isFreeSubcription=false;
		if(kritiqs!=null && !kritiqs.isEmpty()){
			//for(MerchantKritiq ktitiq:kritiqs){
			MerchantKritiq ktitiq=kritiqs.get(kritiqs.size()-1);
				if(ktitiq!=null && ktitiq.getSubscrptionType()!=null && ktitiq.getSubscrptionType().equals("free")){
					isFreeSubcription=true;
				}else{
					isFreeSubcription=false;	
				}
				
					Date date1 = DateUtil.convertStringToDate(kritiqs.get(kritiqs.size()-1).getValidityDate());
					//currentDate=DateUtil.convertStringToDate(currentDate.toString());
					Date currentDate=(merchant != null && merchant.getTimeZone() != null
					&& merchant.getTimeZone().getTimeZoneCode() != null)
							? DateUtil.getCurrentDateForTimeZonee(merchant.getTimeZone().getTimeZoneCode())
							: new Date();
					System.out.println(date1.compareTo(currentDate));
					if ( date1.compareTo(currentDate) < 0) {
						merchantKritiqExpiryStatus="expired";
				}
		}
			//}
		}
		}
		LOGGER.info("-----------------End :: KritiqServiceImpl : checkMerchantExpiration():---------------");

		return merchantKritiqExpiryStatus;
	}

	public List<CustomerFeedbackAnswer> getCustomerFeedbackResult(Integer custFeedbackId) {
		LOGGER.info("-----------------Starts :: KritiqServiceImpl : getCustomerFeedbackResult():---------------custFeedbackId : "+custFeedbackId);

	// Map<Integer, Integer> queAnswer = null;
		List<CustomerFeedbackAnswer> customerFeedbackList = null;
		if(custFeedbackId!= null && custFeedbackId!=0){
				 customerFeedbackList = customerFeedbackAnswerRepository.findCustomerFeedbackInfo(custFeedbackId);
				/*if(customerFeedbackList!= null && !customerFeedbackList.isEmpty()){
					for(CustomerFeedbackAnswer listCustomerFeedback :customerFeedbackList){
						System.out.println("FEEDBACKQUESTIONCATEGORY"+ listCustomerFeedback.getFeedbackQuestion().getFeedbackQuestionCategory().getFeedbackQuestionCategory());
						System.out.println("FEEDBACKQUESTION::"+listCustomerFeedback.getFeedbackQuestion().getQuestion()+"FEEDBACKANSWER"+listCustomerFeedback.getAnswer());
						//queAnswer.put(listCustomerFeedback.getFeedbackQuestion().getId(), listCustomerFeedback.getAnswer());
					}
				}*/
				 
			}
		LOGGER.info("-----------------End :: KritiqServiceImpl : getCustomerFeedbackResult():---------------");

		return customerFeedbackList;
		}

	
	public FeedbackQuestionCategory getFeedbackQuestionCategory(Integer feedbackQuestionCategoryId) {
		LOGGER.info("===============  KritiqServiceImpl : Inside getFeedbackQuestionCategory :: Start  =============feedbackQuestionCategoryId "+feedbackQuestionCategoryId);

		if(feedbackQuestionCategoryId!= null && feedbackQuestionCategoryId!=0){
		FeedbackQuestionCategory feedbackQuestionCategoriesResult = feedbackQuestionCategoryRepository.findOne(feedbackQuestionCategoryId);
		
		List<FeedbackQuestion> feedbackQuestions = feedbackQuestionRepository
				.findByFeedbackQuestionCategoryId(feedbackQuestionCategoriesResult.getId());
		feedbackQuestionCategoriesResult.setFeedbackQuestions(feedbackQuestions);
		LOGGER.info("===============  KritiqServiceImpl : Inside getFeedbackQuestionCategory :: End  ============= ");

		return feedbackQuestionCategoriesResult;
		}
		LOGGER.info("===============  KritiqServiceImpl : Inside getFeedbackQuestionCategory :: End  ============= ");

		return null;		
		
	}

	public CustomerFeedback getCustomerFeedback(Integer id) {
		
		return customerFeedbackRepository.findById(id);
	}

	public Map<String, Integer> getPositiveAndNegetiveFeedbackCount(String merchantUid) {
		LOGGER.info("===============  KritiqServiceImpl : Inside getPositiveAndNegetiveFeedbackCount :: Start  =============merchantUid "+merchantUid);

		Map<String, Integer> feedbackMap  = new HashMap<String, Integer>();
		List<Customer> customers = new ArrayList<Customer>();
		List<CustomerFeedback> customerFeedbacks = new ArrayList<CustomerFeedback>();
		List<Integer> customerFeedbackAnswers = new ArrayList<Integer>();
		Integer positiveFeedbackCount =0;
		Integer negtiveFeedbackCount =0;
		Merchant merchant =merchantService.findByMerchantUid(merchantUid);
		if(merchant!=null)
		{
		customers=	customerService.findByMerchantId(merchant.getId());
			for(Customer customer:customers)
			{
				
				customerFeedbacks = customerFeedbackRepository.findByCustomerId(customer.getId());
				for(CustomerFeedback customerFeedback:customerFeedbacks)
				{
					LOGGER.info("===============  KritiqServiceImpl : Inside getPositiveAndNegetiveFeedbackCount :: Start  =============CustomerFeedbackId "+customerFeedback.getId());

					Integer count = 0;
					Integer feedbackAnswerSize =0;
					customerFeedbackAnswers = customerFeedbackAnswerRepository.findByCustomerFeedbackId(customerFeedback.getId());
					for(Integer answer : customerFeedbackAnswers)
					{
						
						count=count+answer;
						feedbackAnswerSize++;
					}
					if(feedbackAnswerSize!=0){
						Integer ratting =	count/feedbackAnswerSize;
						if(ratting<3)
						{
							negtiveFeedbackCount++;
						}else{
							positiveFeedbackCount++;
						}
					}
				
					
					/*feedbackMap.put("positiveFeedback", positiveFeedbackCount);
					feedbackMap.put("negetiveFeedback", negtiveFeedbackCount);*/
				}
			}
		
		}
		feedbackMap.put("positiveFeedback", positiveFeedbackCount);
		feedbackMap.put("negetiveFeedback", negtiveFeedbackCount);
		LOGGER.info("===============  KritiqServiceImpl : Inside getPositiveAndNegetiveFeedbackCount :: End  ============= ");

		return feedbackMap;
		
		
	}
	
	
	 public Map<String, Integer> getPositiveAndNegetiveFeedbackCountByDateRange(String merchantUid, String startDate,
             String endDate) {
		 LOGGER.info("===============  KritiqServiceImpl : Inside getPositiveAndNegetiveFeedbackCountByDateRange :: Start  =============merchantUid "+merchantUid+" startDate "+startDate+" endDate "+endDate);

 Map<String, Integer> feedbackMap  = new HashMap<String, Integer>();
 List<Customer> customers = new ArrayList<Customer>();
 List<CustomerFeedback> customerFeedbacks = new ArrayList<CustomerFeedback>();
 List<Integer> customerFeedbackAnswers = new ArrayList<Integer>();
 Integer positiveFeedbackCount =0;
 Integer negtiveFeedbackCount =0;
 Merchant merchant =merchantService.findByMerchantUid(merchantUid);
 Date startDate1 = null;
 Date endDate1 = null;
 try{
     if(startDate!=null && endDate!=null){
         //startDate = startDate.replace("/","-");
        
         String startDateIndex[] = startDate.split("/");
         String finalStartDate = startDateIndex[2]+"-"+startDateIndex[0]+"-"+startDateIndex[1];
        
         String endDateIndex[] = endDate.split("/");
         String finalEndDate = endDateIndex[2]+"-"+endDateIndex[0]+"-"+endDateIndex[1];
         //endDate = endDate.replace("/", "-");
         DateFormat dateFormat = new SimpleDateFormat(IConstant.YYYYMMDD);
         String start = finalStartDate+IConstant.STARTDATE;
         String end = finalEndDate+IConstant.ENDDATE;
         
         startDate1 = dateFormat.parse(start);
         endDate1 = dateFormat.parse(end);
         
         
         LOGGER.info("662 startDate1-->"+startDate1+" endDate1--->"+endDate1);
     }
 }catch(Exception e){
	 LOGGER.error("===============  KritiqServiceImpl : Inside getPositiveAndNegetiveFeedbackCountByDateRange :: Exception  ============= " + e);

     LOGGER.error("error: " + e.getMessage());
 }
 if(merchant!=null)
 {
 customers=  customerService.findByMerchantId(merchant.getId());
     for(Customer customer:customers)
     {
         customerFeedbacks = customerFeedbackRepository.findByCustomerIdAndCreateDate(customer.getId(),startDate1,endDate1);
         for(CustomerFeedback customerFeedback:customerFeedbacks)
         {
             Integer count = 0;
             Integer feedbackAnswerSize =0;
             customerFeedbackAnswers = customerFeedbackAnswerRepository.findByCustomerFeedbackId(customerFeedback.getId());
             for(Integer answer : customerFeedbackAnswers)
             {
                 
                 count=count+answer;
                 feedbackAnswerSize++;
             }
             if(feedbackAnswerSize!=0){
                 Integer ratting =   count/feedbackAnswerSize;
                 if(ratting<3)
                 {
                     negtiveFeedbackCount++;
                 }else{
                     positiveFeedbackCount++;
                 }
             }
         LOGGER.info("positiveFeedbackCount--"+positiveFeedbackCount);
         LOGGER.info("negtiveFeedbackCount---"+negtiveFeedbackCount);
             
             /*feedbackMap.put("positiveFeedback", positiveFeedbackCount);
             feedbackMap.put("negetiveFeedback", negtiveFeedbackCount);*/
         }
     }
 
 }
 feedbackMap.put("positiveFeedback", positiveFeedbackCount);
 feedbackMap.put("negetiveFeedback", negtiveFeedbackCount);
 LOGGER.info("===============  KritiqServiceImpl : Inside getPositiveAndNegetiveFeedbackCountByDateRange :: End  ============= ");

 return feedbackMap;
 
 

}

	public List<KritiqMerchantVO> getKritiqMerchantDetail(String startDate,String endDate,Boolean isFoodkonnektAdmin,Integer vendorId) {
		LOGGER.info("===============  KritiqServiceImpl : Inside getKritiqMerchantDetail :: Start  =============startDate "+startDate+" endDate "+endDate+" isFoodkonnektAdmin "+isFoodkonnektAdmin+" vendorId "+vendorId);

		List<KritiqMerchantVO> kritiqMerchantVOs= new ArrayList<KritiqMerchantVO>();
		List<Merchant> merchants=null;
		if(isFoodkonnektAdmin!=null ){
			if(isFoodkonnektAdmin){
		 merchants=merchantRepository.findByActiveCustomerFeedback(1);
			}else{
				if(vendorId!=null)
		 merchants=merchantRepository.findByOwnerIdAndActiveCustomerFeedbackAndIsInstallNot(vendorId,1,IConstant.SOFT_DELETE);
			}
		}
		for(Merchant merchant:merchants){
			KritiqMerchantVO kritiqMerchantVO=new KritiqMerchantVO();
			java.util.Date startDate1 = null;
			java.util.Date endDate1 = null;
			java.util.Date endDateforMailSent = null;
			
				try {
					if(startDate!= null && endDate!= null){
					    	
					 startDate1=new SimpleDateFormat(IConstant.YYYYMMDD).parse(startDate);  
					 //endDate1=new SimpleDateFormat(IConstant.YYYYMMDD).parse(endDate);
					 
					 SimpleDateFormat dateFormat = new SimpleDateFormat( "yyyy-MM-dd" );
					 Calendar cal = Calendar.getInstance();
					 cal.setTime( dateFormat.parse( endDate ) );
					 endDateforMailSent=endDate1=new SimpleDateFormat(IConstant.YYYYMMDD).parse(dateFormat.format(cal.getTime()));
					 cal.add( Calendar.DATE, 1 );
					 //endDate = dateFormat.format(cal.getTime());
					 endDate1=new SimpleDateFormat(IConstant.YYYYMMDD).parse(dateFormat.format(cal.getTime()));
					 
					 
					 
					
					}
				} catch (ParseException e) {
					LOGGER.error("===============  KritiqServiceImpl : Inside getKritiqMerchantDetail :: Exception  ============= " + e);

					// TODO Auto-generated catch block
					LOGGER.error("error: " + e.getMessage());
				}
			LOGGER.info("===============  KritiqServiceImpl : Inside getKritiqMerchantDetail starts :: endDateforMailSent  : "
					+ endDateforMailSent + " merchant " + merchant.getId());

			Double avgRate=customerFeedbackRepository.findAverageRating(merchant.getId(), startDate1, endDate1);
			Integer noOfMailSent=customerFeedbackRepository.findMailSentCount(merchant.getId(),startDate1, endDateforMailSent);
			Integer noOfResponse=customerFeedbackRepository.findResponseCount(merchant.getId(),startDate1, endDate1);
			
			if(avgRate!=null)
			kritiqMerchantVO.setAvgRate(avgRate);
			else
		    kritiqMerchantVO.setAvgRate(0.0);
			
			if(noOfMailSent!=null)
			kritiqMerchantVO.setNoOfMailSent(noOfMailSent);
			else
			kritiqMerchantVO.setNoOfMailSent(0);
			
			if(noOfResponse!=null)
			kritiqMerchantVO.setNoOfResponse(noOfResponse);
			else
				kritiqMerchantVO.setNoOfResponse(0);
			
			kritiqMerchantVO.setMerchantId(merchant.getId());
			kritiqMerchantVO.setMerchantName(merchant.getName());
			kritiqMerchantVOs.add(kritiqMerchantVO);
		}
		LOGGER.info("===============  KritiqServiceImpl : Inside getKritiqMerchantDetail :: End  ============= ");

		return kritiqMerchantVOs;
	}

	public List<KritiqCustomerVO> getKritiqCustomerDetail(Integer merchantId,String startDate,String endDate) {
		List<KritiqCustomerVO> kritiqCustomerVOs= new ArrayList<KritiqCustomerVO>();
		java.util.Date startDate1 = null;
		java.util.Date endDate1 = null;
		
			try {
				LOGGER.info("===============  KritiqServiceImpl : Inside getKritiqCustomerDetail :: Start  ============= merchantId "+merchantId+" startDate "+startDate+" endDate "+endDate);

				if(startDate!= null && endDate!= null){
				    	
				 startDate1=new SimpleDateFormat(IConstant.YYYYMMDD).parse(startDate);  
				 //endDate1=new SimpleDateFormat(IConstant.YYYYMMDD).parse(endDate);
				 
				 SimpleDateFormat dateFormat = new SimpleDateFormat( "yyyy-MM-dd" );
				 Calendar cal = Calendar.getInstance();
				 cal.setTime( dateFormat.parse( endDate ) );
				 cal.add( Calendar.DATE, 1 );
				 //endDate = dateFormat.format(cal.getTime());
				 endDate1=new SimpleDateFormat(IConstant.YYYYMMDD).parse(dateFormat.format(cal.getTime()));
				}
			} catch (ParseException e) {
				LOGGER.error("===============  KritiqServiceImpl : Inside getKritiqCustomerDetail :: Exception  ============= " + e);

				// TODO Auto-generated catch block
				LOGGER.error("error: " + e.getMessage());
			}
		List<CustomerFeedback>  customerFeedbacks=customerFeedbackRepository.findByMerchantId(merchantId,startDate1,endDate1);
		for(CustomerFeedback customerFeedback:customerFeedbacks){
			KritiqCustomerVO kritiqCustomerVO= new KritiqCustomerVO();
			String merchantName="";
			if(customerFeedback.getMerchant()!=null && customerFeedback.getMerchant().getName()!=null){
				merchantName=customerFeedback.getMerchant().getName();
			}
			Date date= customerFeedback.getCreateDate();
			kritiqCustomerVO.setDateOfReview(date.toString());
			kritiqCustomerVO.setMerchantName(merchantName);
			kritiqCustomerVO.setCustomerComments(customerFeedback.getCustomerComments());
			Customer customer=customerFeedback.getCustomer();
			if(customer!=null){
				if(customer.getFirstName()!=null)
			kritiqCustomerVO.setCustomerName(customer.getFirstName());
				if(customer.getFirstName()!=null)
			kritiqCustomerVO.setCustomerEmailId(customer.getEmailId());;
			if(customer.getFirstName()!=null)
			kritiqCustomerVO.setPhoneNo(customer.getPhoneNumber());
			}
			List<KritiqQuestionAnswersVO> kritiqQuestionAnswersVOs= new ArrayList<KritiqQuestionAnswersVO>();
			LOGGER.info("===============  KritiqServiceImpl : Inside getKritiqCustomerDetail starts :: customerFeedbackId  : "
					+ customerFeedback.getId());

			List<CustomerFeedbackAnswer> customerFeedbackAnswers = customerFeedbackAnswerRepository.findCustomerFeedbackInfo(customerFeedback.getId());
//			if(customerFeedbackAnswers!=null && customerFeedbackAnswers.size()>0){
//				Integer totalQuestions=customerFeedbackAnswers.size();
//				double rating=0.0;
//			for(CustomerFeedbackAnswer customerFeedbackAnswer:customerFeedbackAnswers){
//				KritiqQuestionAnswersVO questionAnswersVO= new KritiqQuestionAnswersVO();
//				
//				if(customerFeedbackAnswer !=null && customerFeedbackAnswer.getFeedbackQuestion() != null && customerFeedbackAnswer.getFeedbackQuestion().getQuestion() != null){
//					questionAnswersVO.setQuestion(customerFeedbackAnswer.getFeedbackQuestion().getQuestion());
//				}
//				questionAnswersVO.setAnswer(customerFeedbackAnswer.getAnswer());
//				if(customerFeedbackAnswer.getAnswer() != null){
//					rating=rating+customerFeedbackAnswer.getAnswer();
//				}
//				
//				kritiqQuestionAnswersVOs.add(questionAnswersVO);
//			}
//			avgRate= rating/totalQuestions;
//			DecimalFormat df = new DecimalFormat("#.#");
//			System.out.print(df.format(avgRate));
//			avgRate=Double.parseDouble(df.format(avgRate));
//			}
			
			Double avgRate = customerFeedbackRepository.findCustomersAverageRating(customerFeedback.getId(), startDate1, endDate1);
			if (avgRate == null)
				avgRate = 0.0;
			kritiqCustomerVO.setAvgRating(avgRate);
			kritiqCustomerVO.setKritiqQuestionAnswersVOs(kritiqQuestionAnswersVOs);
			kritiqCustomerVOs.add(kritiqCustomerVO);
		}
		LOGGER.info("===============  KritiqServiceImpl : Inside getKritiqCustomerDetail :: End  ============= ");

		return kritiqCustomerVOs;
	}
	
	public  String thankyouMailByMailChamp(Customer customer, String email) throws Exception {
		LOGGER.info("===============  KritiqServiceImpl : Inside thankyouMailByMailChamp :: Start  =============email "+email);

		String address="";
		
		Address addresses=businessService.findLocationByMerchantId(customer.getMerchantt().getId());
		LOGGER.info("===============  KritiqServiceImpl : Inside thankyouMailByMailChamp starts :: merchant " + customer.getMerchantt().getId());

		SocialMediaLinks mediaLinks=mediaLinksRepository.findByMerchantId(customer.getMerchantt().getId());
		
		LOGGER.info("===============  KritiqServiceImpl : Inside thankyouMailByMailChamp addresses ==  "+addresses.toString());
		if(addresses!=null){
		address= "";
		if(addresses.getAddress1()!=null)
			address=address+ addresses.getAddress1();
		if(addresses.getAddress2()!=null)
			address=address +" "+ addresses.getAddress2();
		if(addresses.getCity()!=null)
			address=address +" "+ addresses.getCity();
		if(addresses.getState()!=null)
			address=address +" "+ addresses.getState();
		if(addresses.getZip()!=null)
			address=address +" "+ addresses.getZip();
			
		addresses.setMerchant(null);
		addresses.setCustomer(null);
		}else{
			address="";
		}
		
		String fromEmail = "services@mkonnekt.com";
		if (email != null && email!= " " && !email.isEmpty()) {
			fromEmail = email;
		}
		
        String logo= "";
        if(customer!=null && customer.getMerchantt()!=null && customer.getMerchantt().getMerchantLogo()!=null){
            logo = environment.getProperty("BASE_PORT") + customer.getMerchantt().getMerchantLogo();
        }else{
            logo= environment.getProperty("WEB_BASE_URL")+"/resources/kritiq/img/logo.jpg";
            LOGGER.info("===============  KritiqServiceImpl : Inside thankyouMailByMailChamp Kritiq Logo="+logo);
        }
		
		String website=customer.getMerchantt().getWebsite();
		if(website==null || " ".equals(website) || website.isEmpty()){
			website="https://kritiq.us";
		}
		String fblink="#";
		String instalink="#";
		String twitterlink="#";
		if(mediaLinks!=null){
		 fblink=mediaLinks.getFaceBookLink();
		if(fblink== null || " ".equals(fblink) || fblink.isEmpty()){
			fblink="";
		}
		 instalink=mediaLinks.getInstagramLink();
		if(instalink== null || " ".equals(instalink) || instalink.isEmpty()){
			instalink="";
		}
		 twitterlink=mediaLinks.getTwitterLink();
		if(twitterlink== null || " ".equals(twitterlink) || twitterlink.isEmpty()){
			twitterlink="";
		}
		}
		String mailChampApiKey=customer.getMerchantt().getMailchimpApiKey();
		if(mailChampApiKey==null || mailChampApiKey.isEmpty()){
			mailChampApiKey=IConstant.MAILCHAMP_API_KEY;
		}

		LOGGER.info("===============  KritiqServiceImpl : Inside thankyouMailByMailChamp fblink="+fblink+"instalink="+instalink+"twitterlink="+twitterlink);
		
		String POST_URL = environment.getProperty("CommunicationPlatform_BASE_URL_SERVER") + "/thankYouMail";
		System.out.println("GET_URL=" + POST_URL);
		HttpPost post = new HttpPost(POST_URL);
		String locationJson = "{\"customerName\": \"" + customer.getFirstName() + "\",	\"customerEmail\": \""
				+ customer.getEmailId() + "\",	\"merchantName\": \"" + customer.getMerchantt().getName()
				+ "\",	\"logoUrl\": \"" + logo
				+ "\",	\"merchantEmail\": \"" + fromEmail 
				+ "\",	\"phoneNumber\": \"" + customer.getMerchantt().getPhoneNumber()
				+ "\",	\"website\": \"" + website
				+ "\",	\"address\": \"" + address
				+ "\",	\"birthdayKouponUrl\": \"" + customer.getMerchantt().getBirthdayKouponUrl()
				+ "\",	\"mailChimpApiKey\": \"" + mailChampApiKey
				+ "\",	\"facebookUrl\": \"" + fblink
				+ "\",	\"twitterUrl\": \"" + twitterlink
				+ "\",	\"instagramUrl\": \"" + instalink
				+ "\"}";

		
		StringBuilder responseBuilder = new StringBuilder();
		HttpClient httpClient = HttpClientBuilder.create().build();
		StringEntity input = new StringEntity(locationJson);
		input.setContentType("application/json");
		post.setEntity(input);
		HttpResponse response = httpClient.execute(post);
		System.out.println("Output from Server .... \n");
		BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
		String line = "";
		while ((line = rd.readLine()) != null) {
			responseBuilder.append(line);
		}
		System.out.println(responseBuilder.toString());
LOGGER.info("===============  KritiqServiceImpl : Inside thankyouMailByMailChamp :: End  ============= ");

		return responseBuilder.toString();
	}

	public static String positiveNegativeFeedbackByMailChamp(Customer customer, List<CustomerFeedbackAnswer> answers,
			String email,Environment environment) throws Exception {
		LOGGER.info("===============  KritiqServiceImpl : Inside positiveNegativeFeedbackByMailChamp :: Start  =============email "+email);


		String POST_URL = environment.getProperty("CommunicationPlatform_BASE_URL_SERVER") + "/kritiqNegativeMail";
		// String POST_URL=
		// "https://www.mkonnekt.com/CommunicationPlatform/kritiqNegativeMail";
		System.out.println("GET_URL=" + POST_URL);
		HttpPost post = new HttpPost(POST_URL);

		String locationJson = "{\"customerName\": \"" + customer.getFirstName()  
				+ "\",	\"customerEmail\": \""+ customer.getEmailId() + "\",	"
				+ "\"customerPhoneNumber\": \"" + customer.getPhoneNumber()
				+ "\",	\"merchantName\": \"" + customer.getMerchantt().getName()
				// + "\", \"merchantLogo\": \"" +// "https://www.foodkonnekt.com/web/resources/kritiq/img/logo.jpg"
				+ "\",	\"merchantEmail\": \"" + email 
				+ "\",	\"fromEmail\": \"" + "services@mkonnekt.com"
				+ "\",	\"encryptedOrderId\": \""+ EncryptionDecryptionUtil.encryption(answers.get(0).getCustomerFeedback().getId().toString())
				+ "\",	\"mailChimpApiKey\": \"" + customer.getMerchantt().getMailchimpApiKey()
				+ "\",	\"locationLink\": \"" + environment.getProperty("WEB_KRITIQ_BASE_URL") + "\",	\"questionCustomerRateDto\":[";

		for (int i = 0; i <= answers.size() - 1; i++) {
			locationJson += "{ \"question\":\"" + answers.get(i).getFeedbackQuestion().getQuestion() + "\","
					+ "\"customerRate\":\"" + answers.get(i).getAnswer() + "\"},";

		}
		locationJson = locationJson.substring(0, locationJson.lastIndexOf(","));
		locationJson += "]}";

		StringBuilder responseBuilder = new StringBuilder();
		HttpClient httpClient = HttpClientBuilder.create().build();
		StringEntity input = new StringEntity(locationJson);
		input.setContentType("application/json");
		post.setEntity(input);
		HttpResponse response = httpClient.execute(post);
		System.out.println("Output from Server .... \n");
		BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
		String line = "";
		while ((line = rd.readLine()) != null) {
			responseBuilder.append(line);
		}
		System.out.println(responseBuilder.toString());
LOGGER.info("===============  KritiqServiceImpl : Inside positiveNegativeFeedbackByMailChamp :: End  ============= ");

		return responseBuilder.toString();
	}

/*	public String getOrderDetailForFax(Integer orderId) {
        String orderDetails = "";

        List<Map<String, String>> extraList = new ArrayList<Map<String, String>>();
        Map<String, String> poduct = new HashMap<String, String>();
        List<OrderItem> orderItems = orderItemRepository.findByOrderId(orderId);
		for (OrderItem orderItem : orderItems) {
			if (null != orderItem) {
				if (null != orderItem.getItem()) {
					String items = "<tr><td >" + orderItem.getQuantity() +" &nbsp; &nbsp; &nbsp;"
							+ "</td>" + "<td><b>"
							+ orderItem.getItem().getName() + "</b><br />";
					orderDetails = orderDetails + "" + items;
					List<OrderItemModifier> itemModifiers = itemModifierRepository
							.findByOrderItemId(orderItem.getId());
					for (OrderItemModifier itemModifier : itemModifiers) {
						String modifiers = ""
								+ itemModifier.getModifiers().getName()
								+ "<br/>";
						orderDetails = orderDetails + "" + modifiers;
					}
					orderDetails += "</td>";
					orderDetails = orderDetails + "" + "<td><b>$" + Math.round((orderItem.getItem().getPrice() * orderItem .getQuantity()) * 100.0) / 100.0 + "</b><br />";
					for (OrderItemModifier itemModifier : itemModifiers) {
						String modifiers = ""
								+"$"+itemModifier.getModifiers().getPrice()
								+ "<br/>";
						orderDetails = orderDetails + "" + modifiers;
					}
					orderDetails += "</td></tr>";
				}
			}
		}
    
         
         
         
         
          -------------------------------------------------------------For Pizza View----------------------------------------------------------------------------------                 
            List<OrderPizza> orderPizzas = orderPizzaRepository.findByOrderId(orderId);
            if(!orderPizzas.isEmpty() && orderPizzas != null){
                for (OrderPizza orderPizza : orderPizzas) {

                    if (orderPizza != null && orderPizza.getPizzaTemplate() != null) {
                                
                                Item item = new Item();
                                if(orderPizza!=null && orderPizza.getPizzaTemplate()!=null && orderPizza.getPizzaTemplate().getDescription()!= null &&  orderPizza.getPizzaSize() != null && orderPizza.getPizzaSize().getDescription()!=null){
                                    
                                    String pizza = "<tr><td>"+orderPizza.getQuantity()+"</td><td><b>"
                                            + orderPizza.getPizzaTemplate().getDescription() + "(" +orderPizza.getPizzaSize().getDescription() + ")"
                                            + "</b><br/>";
                                    		orderDetails = orderDetails + "" + pizza;
                                }
                                List<OrderPizzaToppings> orderPizzaToppings = orderPizzaToppingsRepository.findByOrderPizzaId(orderPizza.getId());
                                if(!orderPizzaToppings.isEmpty() && orderPizzaToppings!=null){
                                for (OrderPizzaToppings pizzaToppings : orderPizzaToppings) {
                                    if(pizzaToppings!=null && pizzaToppings.getPizzaTopping()!=null && pizzaToppings.getPizzaTopping().getDescription()!=null){
                                        String modifiers = ""
                                                + pizzaToppings.getPizzaTopping().getDescription();
                                                if(pizzaToppings.getSide1()!=null && pizzaToppings.getSide1()){
                                                    modifiers += " (First Half)";
                                                }else if(pizzaToppings.getSide2()!=null && pizzaToppings.getSide2()){
                                                    modifiers += " (Second Half)";
                                                }else{
                                                    modifiers += " (Full)";
                                                }
                                               modifiers += "<br/>";
                                        orderDetails = orderDetails + "" + modifiers;
                                    }
                                }
                                
                                }
                                OrderPizzaCrust orderPizzaCrust = orderPizzaCrustRepository.findByOrderPizzaId(orderPizza.getId());
                                if(orderPizzaCrust != null && orderPizzaCrust.getPizzacrust()!=null){
                                    String crust = ""     + orderPizzaCrust.getPizzacrust().getDescription();
                                    crust += "<br/>";
                                    orderDetails = orderDetails + "" + crust;
                                }
                                orderDetails += "</td>";
            					orderDetails = orderDetails + "" + "<td><b>$"+Math.round(orderPizza.getPrice() * orderPizza.getQuantity()*100.0)/100.0+"</b><br />";
            				      if(!orderPizzaToppings.isEmpty() && orderPizzaToppings!=null){
                                      for (OrderPizzaToppings pizzaToppings : orderPizzaToppings) {
                                          if(pizzaToppings!=null && pizzaToppings.getPizzaTopping()!=null && pizzaToppings.getPizzaTopping().getDescription()!=null){
                                              String modifiers = "";
                                                     
                                                      if(pizzaToppings.getSide1()!=null && pizzaToppings.getSide1()){
                                                          modifiers += "$"+pizzaToppings.getPrice();
                                                      }else if(pizzaToppings.getSide2()!=null && pizzaToppings.getSide2()){
                                                          modifiers += "$"+pizzaToppings.getPrice();
                                                      }else{
                                                          modifiers +=  "$"+pizzaToppings.getPrice();
                                                      }
                                                     modifiers += "<br/>";
                                              orderDetails = orderDetails + "" + modifiers;
                                          }
                                      }
                                   }
            						orderDetails += "</td></tr>";
                             	//+ "<td><b>$"+Math.round(orderPizza.getPrice() * orderPizza.getQuantity()*100.0)/100.0+"</b></td></tr>";
                    }
                }
            }
            
 --------------------------------------------------------------------------------------------------------------------------------------------------------------- 
         return orderDetails;
    }*/
	
	
	public Table getOrderDetailForFax(Integer orderId) {
LOGGER.info("===============  KritiqServiceImpl : Inside getOrderDetailForFax :: Start  =============orderId "+orderId);

        Table table = new Table(new UnitValue[] { new UnitValue(UnitValue.PERCENT, 6.25f),
                        new UnitValue(UnitValue.PERCENT, 20.5f), new UnitValue(UnitValue.PERCENT, 25.5f),
                        new UnitValue(UnitValue.PERCENT, 5.5f) }).setWidth(UnitValue.createPercentValue(100))
                                        .setMarginTop(10).setMarginBottom(10);
        try {
            PdfFont bold = PdfFontFactory.createFont(environment.getProperty("BOLD"), true);
            table.addHeaderCell(ItextFaxUtil.createCell("Qty", bold).setTextAlignment(TextAlignment.CENTER));
            table.addHeaderCell(ItextFaxUtil.createCell("Item", bold).setTextAlignment(TextAlignment.CENTER));
            table.addHeaderCell(ItextFaxUtil.createCell("Options", bold).setTextAlignment(TextAlignment.CENTER));
            table.addHeaderCell(ItextFaxUtil.createCell("Price", bold).setTextAlignment(TextAlignment.CENTER));
          
            
            System.out.println("orderId "+orderId);
            List<OrderItem> orderItems = orderItemRepository.findByOrderId(orderId);
            LOGGER.info("orderItems--- "+orderItems.size());
            for (OrderItem orderItem : orderItems) 
            {
                Double modifiersPrice = 0.0;

            	if(orderItem.getItem()!=null &&orderItem.getItem().getId()!=null) {
            	System.out.println("orderItem "+orderItem);
                table.addCell(ItextFaxUtil.createCell(orderItem.getQuantity().toString()) .setTextAlignment(TextAlignment.CENTER));
                if(orderItem.getItem()!=null &&orderItem.getItem().getName()!=null){
                table.addCell(ItextFaxUtil.createCell(orderItem.getItem().getName())  .setTextAlignment(TextAlignment.CENTER));
                }
                else{
                    table.addCell(ItextFaxUtil.createCell("-")  .setTextAlignment(TextAlignment.CENTER));
                }
                if ((orderItem.getId() != null)) {
                    List<OrderItemModifier> itemModifiers = itemModifierRepository.findByOrderItemId(orderItem.getId());
                    String modifiers = "";
                    
                    if (!itemModifiers.isEmpty() && itemModifiers != null) 
                    {

                    	
                        for (OrderItemModifier orderItemModifier : itemModifiers) {
                           modifiers += " "+ orderItemModifier.getModifiers().getName()+""+ "$"+orderItemModifier.getModifiers().getPrice()+"\n";
                           
                           modifiersPrice += orderItemModifier.getModifiers().getPrice();
                        }
                        table.addCell(ItextFaxUtil.createCell(modifiers).setTextAlignment(TextAlignment.CENTER));
                    } 
                    else {
                        table.addCell(ItextFaxUtil.createCell(String.valueOf("-"))  .setTextAlignment(TextAlignment.CENTER));
                    }
                }
                LOGGER.info("orderItem.getItem().getPrice()"+orderItem.getItem().getPrice());
                LOGGER.info("orderItem.getQuantity()"+orderItem.getQuantity());
               
                table.addCell(ItextFaxUtil.createCell(String.valueOf("$" + Math.round(((orderItem.getItem().getPrice()+modifiersPrice) * orderItem.getQuantity()) * 100.0)     / 100.0))   .setTextAlignment(TextAlignment.CENTER));
                modifiersPrice =0.0;
            	}
           }
            
            
            List<OrderPizza> orderPizzas = orderPizzaRepository.findByOrderId(orderId);
            if(!orderPizzas.isEmpty() && orderPizzas != null){
                for (OrderPizza orderPizza : orderPizzas) {

                	Double pizzaToppingPrice = 0.0;
                    if (orderPizza != null && orderPizza.getPizzaTemplate() != null) {
                                if(orderPizza!=null && orderPizza.getPizzaTemplate()!=null && orderPizza.getPizzaTemplate().getDescription()!= null &&  orderPizza.getPizzaSize() != null && orderPizza.getPizzaSize().getDescription()!=null){
      
                                            table.addCell(ItextFaxUtil.createCell(orderPizza.getQuantity().toString()) .setTextAlignment(TextAlignment.CENTER));
                                            table.addCell(ItextFaxUtil.createCell( orderPizza.getPizzaTemplate().getDescription() + "(" +orderPizza.getPizzaSize().getDescription() + ")").setTextAlignment(TextAlignment.CENTER));
                                }
                                String modifiers = "";
                                LOGGER.info(
										"===============  KritiqServiceImpl : Inside getOrderDetailForFax :: Start  =============orderPizza "+orderPizza.getId());

                                List<OrderPizzaToppings> orderPizzaToppings = orderPizzaToppingsRepository.findByOrderPizzaId(orderPizza.getId());
                                if(!orderPizzaToppings.isEmpty() && orderPizzaToppings!=null){
                                    for (OrderPizzaToppings pizzaToppings : orderPizzaToppings) {
                                    if(pizzaToppings!=null && pizzaToppings.getPizzaTopping()!=null && pizzaToppings.getPizzaTopping().getDescription()!=null){
                                                	modifiers +=  pizzaToppings.getPizzaTopping().getDescription();
                                                if(pizzaToppings.getSide1()!=null && pizzaToppings.getSide1()){
                                                    modifiers += "(First Half)"+"" +"$"+pizzaToppings.getPrice();
                                                }else if(pizzaToppings.getSide2()!=null && pizzaToppings.getSide2()){
                                                    modifiers += " (Second Half)"+""+"$"+pizzaToppings.getPrice();
                                                }else{
                                                    modifiers += " (Full)"+""+"$"+pizzaToppings.getPrice();
                                                }
                                               modifiers += "\n";
                                               pizzaToppingPrice += pizzaToppings.getPrice();
                                    }
                                }
                                }
                                OrderPizzaCrust orderPizzaCrust = orderPizzaCrustRepository.findByOrderPizzaId(orderPizza.getId());
                                Double pizzaCrustPrice = 0.0;
                                if(orderPizzaCrust != null && orderPizzaCrust.getPizzacrust()!=null){
                                	if(orderPizzaCrust.getPrice() != null){
                                		pizzaCrustPrice = orderPizzaCrust.getPrice();
                                	}
                                    String crust = ""     + orderPizzaCrust.getPizzacrust().getDescription()+""+"$"+pizzaCrustPrice;
                                    crust += "\n";
                                    modifiers = modifiers + "" + crust;
                                    pizzaToppingPrice += pizzaCrustPrice;
                                }
                                table.addCell(ItextFaxUtil.createCell(modifiers).setTextAlignment(TextAlignment.CENTER));
                                modifiers="";
                                //table.addCell(ItextFaxUtil.createCell(String.valueOf(   "$" + Math.round(orderPizza.getPrice() + pizzaToppingPrice) * orderPizza.getQuantity()*100.0)/100.0)) .setTextAlignment(TextAlignment.CENTER));
                                table.addCell(ItextFaxUtil.createCell(String.valueOf("$" + Math.round(((orderPizza.getPrice()+ pizzaToppingPrice) * orderPizza.getQuantity()) * 100.0)     / 100.0))   .setTextAlignment(TextAlignment.CENTER));
                                pizzaToppingPrice = 0.0;
                    }
                }
            }

        }

        catch (Exception e) {
LOGGER.error("===============  KritiqServiceImpl : Inside getOrderDetailForFax :: Exception  ============= " + e);

        	LOGGER.error("error: " + e.getMessage());
        }
        LOGGER.info("===============  KritiqServiceImpl : Inside getOrderDetailForFax :: End  ============= ");

        return table;

    }

	public void saveCustomerFeedbackNew(CustomerFeedback customerFeedback) {
		LOGGER.info("===============  KritiqServiceImpl : Inside saveCustomerFeedbackNew :: Start  ============= ");
        List<CustomerFeedback> customerFeedbackResults=new ArrayList<CustomerFeedback>();
		
		if(customerFeedback!=null && customerFeedback.getOrderR()!=null && customerFeedback.getOrderR().getId()!=null)
		{
			LOGGER.info("===============  KritiqServiceImpl : Inside saveCustomerFeedbackNew :: OrderR  === "+customerFeedback.getOrderR().getId());

			customerFeedbackResults=customerFeedbackRepository.findByOrderId(customerFeedback.getOrderR().getId());
		}
		if(customerFeedback.getMerchant()== null){
			OrderR orderR = orderRepository.findOne(customerFeedback.getOrderR().getId());
			if(orderR.getMerchant()!= null){
				customerFeedback.setMerchant(orderR.getMerchant());
			}
		}
		
		Date date = (customerFeedback.getMerchant()!=null && customerFeedback.getMerchant().getTimeZone() != null
				&& customerFeedback.getMerchant().getTimeZone().getTimeZoneCode() != null)
						? DateUtil.getCurrentDateForTimeZonee(
								customerFeedback.getMerchant().getTimeZone().getTimeZoneCode())
						: new Date();
								
		customerFeedback.setCreateDate(date);
		if(!(customerFeedbackResults!= null && customerFeedbackResults.size()>0))
		{
		CustomerFeedback customerFeedbackResult=customerFeedbackRepository.save(customerFeedback);
		  Customer customer = customerFeedback.getCustomer();
		  Customer customerResult = new Customer();
		  LOGGER.info("===============  KritiqServiceImpl : Inside saveCustomerFeedbackNew :: customer  == "+customer.getId());

		  if(customer!=null && customer.getId()!=null){
			   customerResult=customerrRepository.findOne(customer.getId());
		    	if(customer.getEmailId()!=null  && !customer.getEmailId().equals(""))
			 		   customerResult.setEmailId(customer.getEmailId());
			 		   if(customer.getPhoneNumber()!=null && !customer.getPhoneNumber().equals(""))
			 		    customerResult.setPhoneNumber(customer.getPhoneNumber());
			 		   
			 		  if(customer.getFirstName()!=null && !customer.getFirstName().equals(""))
				 		    customerResult.setFirstName(customer.getFirstName());
		  		}
		   if(customerFeedback.getBdayDate()!=null && !customerFeedback.getBdayDate().equals("")&& customerFeedback.getBdayMonth()!=null && !customerFeedback.getBdayMonth().equals(""))
		    customerResult.setBirthDate(customerFeedback.getBdayMonth()+","+customerFeedback.getBdayDate());
		   if(customerFeedback.getAnniversaryDate()!=null && !customerFeedback.getAnniversaryDate().equals("")&& customerFeedback.getAnniversaryMonth()!=null && !customerFeedback.getAnniversaryMonth().equals(""))
		    customerResult.setAnniversaryDate(customerFeedback.getAnniversaryMonth()+","+customerFeedback.getAnniversaryDate());
		    
		  customerrRepository.save(customerResult);
		  
		  CustomerFeedbackAnswer customerFeedbackAnswer = new CustomerFeedbackAnswer();
		  if(customerFeedbackResult!=null){
			  customerFeedbackAnswer.setCustomerFeedback(customerFeedbackResult);
		  }
		  
		  if(customerFeedbackResult.getFoodQuality()!=null){
			  customerFeedbackAnswer.setFoodQuality(customerFeedbackResult.getFoodQuality());
		  }
		  
		  if(customerFeedbackResult.getCustomerService()!=null){
			  customerFeedbackAnswer.setCustomerService(customerFeedbackResult.getCustomerService());
		  }
		  
		  if(customerFeedbackResult.getOrderExperience()!=null){
			  customerFeedbackAnswer.setOrderExperience(customerFeedbackResult.getOrderExperience());
		  }
		  
		  customerFeedbackAnswerRepository.save(customerFeedbackAnswer);
	LOGGER.info("===============  KritiqServiceImpl : Inside saveCustomerFeedbackNew :: End  ============= ");

		} 
	}
	
public List<Customer> getAllkritiqCustomerByMerchantUid(String merchantUid) {
		
		
		List<Customer> customerlist = new ArrayList<Customer>();
		int count=0;

		List<Customer> customers = customerrRepository.findAllkritiqCustomerByMerchantUid(merchantUid);
		
		for (Customer customer : customers) {
			LOGGER.info("KritqServiceImpl : getCustomerByMerchantUid :Customer : "+customer.getId());

			if (customer != null ) {
				customer.setAddresses(null);
				if (customer.getMerchantt() != null) {

					customer.setMerchantt(null);
					customer.setListOfALLDiscounts(null);
					customer.setVendor(null);
					customer.setAddresses(null);
				}
				customerlist.add(customer);
			}
		
			
				count++;
		}
	
		
		System.out.println("count ===== "+count);
//		result.put(count, customerlist);
		return customerlist;
	}

	public List<Customer> getAllkritiqCustomerByMerchantUidAndDateRange(String merchantUid, String startDate,
			String endDate) {
     
		
		List<Customer> customerlist = new ArrayList<Customer>();
		int count=0;

		if(startDate==null  && endDate==null)
		{
			startDate=DateUtil.getBefore7Daydate();
			endDate=DateUtil.findCurrentDateWithYYYYMMDD();
		}
		else if(endDate==null)
		{
			endDate=DateUtil.findCurrentDateWithYYYYMMDD();
		}
		else if(startDate.equals(endDate)) {
			endDate=DateUtil.findOneDate(endDate);
		}
		startDate = startDate.replace("/", "-");
		endDate = endDate.replace("/", "-");
		SimpleDateFormat simpleDateFormate = new SimpleDateFormat("yyyy-MM-dd");
		
		Date fromdate=null;
		Date beforeDate=null;
		try {
			fromdate = simpleDateFormate.parse(startDate);
			beforeDate = simpleDateFormate.parse(endDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			LOGGER.error("error: " + e.getMessage());
		}
		
		List<Customer> customers = customerrRepository.findAllkritiqCustomerByMerchantUidAndDate(merchantUid,fromdate,beforeDate);
		for (Customer customer : customers) {

			LOGGER.info("KritqServiceImpl : getCustomerByMerchantUid :Customer : "+customer.getId());

			if (customer != null ) {
				customer.setAddresses(null);
				if (customer.getMerchantt() != null) {
					customer.setMerchantt(null);
					customer.setListOfALLDiscounts(null);
					customer.setVendor(null);
					customer.setAddresses(null);
				}
				customerlist.add(customer);
			}
		
			
				count++;
		}
	
		
		System.out.println("count ===== "+count);
		return customerlist;
	
	}
	
public  List<Customer> getActivekritiqCustomerByMerchantUid(String merchantUid) {
	
		List<Customer> customerlist = new ArrayList<Customer>();
		int count=0;
//		List<CustomerFeedback> customerfeedbacks = customerFeedbackRepository.findCustomerFeedback(merchantUid);
//		if(customerfeedbacks!=null){
//		for (CustomerFeedback customerfeedbacks2 : customerfeedbacks) {
//			System.out.println("customerfeedbacks2 Id ====  "+customerfeedbacks2.getId());
//			customerFeedbackId.add(customerfeedbacks2.getId());
//		}
		List<Customer> customers = customerrRepository.findActivekritiqCustomerByMerchantUid(merchantUid);
		
		for (Customer customer : customers) {

			LOGGER.info("KritqServiceImpl : getCustomerByMerchantUid :Customer : "+customer.getId());

			if (customer != null ) {
				customer.setAddresses(null);
				if (customer.getMerchantt() != null) {
					customer.setMerchantt(null);
					customer.setListOfALLDiscounts(null);
					customer.setVendor(null);
					customer.setAddresses(null);
				}
				customerlist.add(customer);
			}
		
			
				count++;
		}
	
		
		System.out.println("count ===== "+count);
		return customerlist;
	}

	public List<Customer> getActivekritiqCustomerByMerchantUidAndDateRange(String merchantUid, String startDate,
			String endDate) {
     
		List<Customer> customerlist = new ArrayList<Customer>();
		int count=0;

		if(startDate==null  && endDate==null)
		{
			startDate=DateUtil.getBefore7Daydate();
			endDate=DateUtil.findCurrentDateWithYYYYMMDD();
		}
		else if(endDate==null)
		{
			endDate=DateUtil.findCurrentDateWithYYYYMMDD();
		}
		else if(startDate.equals(endDate)) {
			endDate=DateUtil.findOneDate(endDate);
		}
		startDate = startDate.replace("/", "-");
		endDate = endDate.replace("/", "-");
		SimpleDateFormat simpleDateFormate = new SimpleDateFormat("yyyy-MM-dd");
		
		Date fromdate=null;
		Date beforeDate=null;
		try {
			fromdate = simpleDateFormate.parse(startDate);
			beforeDate = simpleDateFormate.parse(endDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			LOGGER.error("error: " + e.getMessage());
		}
		
		List<Customer> customers = customerrRepository.findActivekritiqCustomerByMerchantUidAndDate(merchantUid,fromdate,beforeDate);
		for (Customer customer : customers) {

			LOGGER.info("KritqServiceImpl : getCustomerByMerchantUid :Customer : "+customer.getId());

			if (customer != null ) {
				customer.setAddresses(null);
				if (customer.getMerchantt() != null) {
					customer.setMerchantt(null);
					customer.setListOfALLDiscounts(null);
					customer.setVendor(null);
					customer.setAddresses(null);
				}
				customerlist.add(customer);
			}
		
			
				count++;
		}
	
		
		System.out.println("count ===== "+count);
		return customerlist;
	
	}
	
	public List<Map<String, Object>> getKritiqReviewData(List<Integer> customerFeedbackIds) {
		List<Map<String, Object>> kritiqCustomerVOs= new ArrayList<Map<String,Object>>();		
			try {
				
				List<CustomerFeedback> cust = customerFeedbackRepository.findByCustomerFeedbackIds(customerFeedbackIds);
			
			 String merchantUid = (cust.size()>0 && cust.get(0).getCustomer().getMerchantt() != null) ? 
	            		cust.get(0).getCustomer().getMerchantt().getMerchantUid() : null;
			for(CustomerFeedback customerFeedback : cust) {
			List<CustomerFeedbackAnswer> customerFeedbackAnswers = customerFeedbackAnswerRepository.findCustomerFeedbackInfo(customerFeedback.getId());
           
			for(CustomerFeedbackAnswer customerFeedbackAnswerObj : customerFeedbackAnswers) {
			customerFeedbackAnswerObj.setCustomerFeedback(null);
			Map<String, Object> kritiqCustomerVO= new HashMap<String, Object>();
            kritiqCustomerVO.put("clientUid", merchantUid);
			Date date= customerFeedback.getCreateDate();
			String dateOfreview = DateUtil.dateFormatddMMMYYY(date);
			kritiqCustomerVO.put("reviewId", customerFeedback.getId());
			kritiqCustomerVO.put("dateOfReview", dateOfreview);
			kritiqCustomerVO.put("customerComments", customerFeedback.getCustomerComments());
			Customer customer=customerFeedback.getCustomer();
			if(customer!=null){
				if(customer.getFirstName()!=null)
			kritiqCustomerVO.put("customerHandle", customer.getFirstName());
				
			}
			LOGGER.info("===============  KritiqServiceImpl : Inside getKritiqCustomerDetail starts :: customerFeedbackId  : "
					+ customerFeedback.getId());

			
			Double avgRate = customerFeedbackAnswerRepository.findIndividualCustomersAverageRating(customerFeedbackAnswerObj.getId());
			if (avgRate == null)
				avgRate = 0.0;
			
			Date dateOfResponse = customerFeedback.getResponseDate();
			String response = (dateOfResponse!=null) ? DateUtil.dateFormatddMMMYYY(dateOfResponse) : "";
			kritiqCustomerVO.put("avgRating", avgRate);
			kritiqCustomerVO.put("orderExperience", customerFeedbackAnswerObj.getOrderExperience());
			kritiqCustomerVO.put("customerService", customerFeedbackAnswerObj.getCustomerService());
			kritiqCustomerVO.put("foodQuality", customerFeedbackAnswerObj.getFoodQuality());
			kritiqCustomerVO.put("response", customerFeedback.getResponse());
			kritiqCustomerVO.put("responseDate", dateOfResponse);
			kritiqCustomerVO.put("responseStatus", customerFeedback.getResponseStatus());
			//Will add a new column as 'response'(String) & 'dateOfResponse'(Date) & 'responseStatus' (String)
		    //in customerFeedbackAnswer which will store the response sent from 4Q side to show for next time
			//make responsedate as Date Object -> 'yyyy-MM-dd HH:mm:ss'
			              
			//kritiqCustomerVO.setKritiqQuestionAnswersVOs(kritiqQuestionAnswersVOs);
			kritiqCustomerVOs.add(kritiqCustomerVO);
			}
			}
			} catch (Exception e) {
				LOGGER.error("===============  KritiqServiceImpl : Inside getKritiqCustomerDetail :: Exception  ============= " + e);

				// TODO Auto-generated catch block
				LOGGER.error("error: " + e.getMessage());
			}
		LOGGER.info("===============  KritiqServiceImpl : Inside getKritiqCustomerDetail :: End  ============= ");

		return kritiqCustomerVOs;
	}

	public List<String> getKritiqIdsReviewDataByMerchantId(Integer merchantId, String startDate, String endDate) {
		List<String> kritiqCustomerVOs = new ArrayList<String>();
		java.util.Date startDate1 = null;
		java.util.Date endDate1 = null;
		List<Integer> customerFeedbacks = null;
		try {
			LOGGER.info("===============  KritiqServiceImpl : Inside getKritiqCustomerDetail :: Start  ============= merchantId "
							+ merchantId + " startDate " + startDate + " endDate " + endDate);

			if (startDate != null && endDate != null && !startDate.equalsIgnoreCase("NC")
					&& !endDate.equalsIgnoreCase("NC")) {
				startDate1=new SimpleDateFormat(IConstant.YYYYMMDD).parse(startDate);  
				 //endDate1=new SimpleDateFormat(IConstant.YYYYMMDD).parse(endDate);
				 
				 SimpleDateFormat dateFormat = new SimpleDateFormat( "yyyy-MM-dd" );
				 Calendar cal = Calendar.getInstance();
				 cal.setTime( dateFormat.parse( endDate ) );
				 cal.add( Calendar.DATE, 1 );
				 //endDate = dateFormat.format(cal.getTime());
				 endDate1=new SimpleDateFormat(IConstant.YYYYMMDD).parse(dateFormat.format(cal.getTime()));
				customerFeedbacks = customerFeedbackRepository.findFeedbackIdsByMerchantIdAndDate(merchantId,
						startDate1, endDate1);
			} else {
				customerFeedbacks = customerFeedbackRepository.findFeedbackIdsByMerchantId(merchantId);
			}

			if (customerFeedbacks != null && customerFeedbacks.size() > 0) {
				for (Integer feedBackId : customerFeedbacks) {
					kritiqCustomerVOs.add(feedBackId.toString());
				}
			}
		} catch (Exception e) {
			LOGGER.error("===============  KritiqServiceImpl : Inside getKritiqCustomerDetail :: Exception  ============= "+ e);
			LOGGER.error("error: " + e.getMessage());
		}
		LOGGER.info("===============  KritiqServiceImpl : Inside getKritiqCustomerDetail :: End  ============= ");

		return kritiqCustomerVOs;
	}

	public KritiqCustomerVO getKritiqReviewDataByFeedbackId(Integer customerFeedBackId) throws ParseException {
		KritiqCustomerVO kritiqVo = new KritiqCustomerVO();
		CustomerFeedback customerFeedback = customerFeedbackRepository.findById(customerFeedBackId);
		if(customerFeedback!=null) {
		
		Date reviewDate = customerFeedback.getCreateDate();
		String dateOfreview = (reviewDate!=null) ? DateUtil.dateFormatddMMMYYY(reviewDate) : "";
		
		Date dateOfResponse = customerFeedback.getResponseDate();
		String responseDate = (dateOfResponse!=null) ? DateUtil.dateFormatddMMMYYY(dateOfResponse) : "";
		
		kritiqVo.setDateOfReview(dateOfreview);
		Customer customer=customerFeedback.getCustomer();
		if(customer!=null) {
		kritiqVo.setCustomerName(customer.getFirstName());
		kritiqVo.setCustomerEmailId(customer.getEmailId());
		}
		else {
		kritiqVo.setCustomerName("");
		kritiqVo.setCustomerEmailId("");
		}
		
		kritiqVo.setCustomerComments(customerFeedback.getCustomerComments());
		kritiqVo.setResponse(customerFeedback.getResponse());
		kritiqVo.setResponseDate(responseDate);
		kritiqVo.setResponseStatus(customerFeedback.getResponseStatus());
	
		}
		return kritiqVo;
	}

	public void saveResponseFrom4Q(Map<String, Object> feedBackResponse) {
		String feedBackId = (String)feedBackResponse.get("feedBackId");
		CustomerFeedback customerFeedBack = customerFeedbackRepository.findById(Integer.parseInt(feedBackId));
		if(customerFeedBack!=null) {
			String date = (String)feedBackResponse.get("responseDate");
			Date responseDate = (date!=null) ? new Date(date) : new Date();
			customerFeedBack.setResponse((String)feedBackResponse.get("response"));
			customerFeedBack.setResponseDate(responseDate);
			customerFeedBack.setResponseStatus((String)feedBackResponse.get("responseStatus"));
			customerFeedbackRepository.save(customerFeedBack);
		}
	}

	public void updateResponseInCustomerFeedBack(String feedBackId, String response) {
		CustomerFeedback customerFeedBack = customerFeedbackRepository.findById(Integer.parseInt(feedBackId));
		if(customerFeedBack!=null) {
			customerFeedBack.setResponse(response);
			customerFeedbackRepository.save(customerFeedBack);
		}
	}

	public List<Map<String, Object>> getKrtiqLowRating(String startDate, String endDate) {
		List<Map<String, Object>> kritiqCustomerVOs= new ArrayList<Map<String,Object>>();		
		try {
			
			String s1 = startDate + IConstant.STARTDATE;
			String s2 = endDate + IConstant.STARTDATE;

			DateFormat dateFormat1 = new SimpleDateFormat(IConstant.YYYYMMDDHHMMSS);
				
			java.util.Date startDate1 = null;
			java.util.Date endDate1 = null;
			if (startDate != null && endDate != null ) {
				 startDate1 = dateFormat1.parse(s1);
				 endDate1 = dateFormat1.parse(s2);
	
			List<CustomerFeedback> customerFeedbackList = customerFeedbackRepository.findRatingBelowThree(startDate1, endDate1);
			for(CustomerFeedback customerFeedback : customerFeedbackList) {
				List<CustomerFeedbackAnswer> customerFeedBackAnswerList = customerFeedbackAnswerRepository.findCustomerFeedbackInfo(customerFeedback.getId());
			for(CustomerFeedbackAnswer customerFeedbackAns : customerFeedBackAnswerList) {
				customerFeedbackAns.setCustomerFeedback(null);
		     
		Map<String, Object> kritiqCustomerVO= new HashMap<String, Object>();
        
		Date date= customerFeedback.getCreateDate();
		String dateOfreview = DateUtil.dateFormatddMMMYYY(date);
		kritiqCustomerVO.put("reviewId", customerFeedback.getId());
		kritiqCustomerVO.put("dateOfReview", dateOfreview);
		kritiqCustomerVO.put("customerComments", customerFeedback.getCustomerComments());
		kritiqCustomerVO.put("orderExperience", customerFeedbackAns.getOrderExperience().toString());
		kritiqCustomerVO.put("customerService", customerFeedbackAns.getCustomerService().toString());
		kritiqCustomerVO.put("foodQuality", customerFeedbackAns.getFoodQuality().toString());
		Customer customer=customerFeedback.getCustomer();
		if(customer!=null){
			if(customer.getFirstName()!=null)
		kritiqCustomerVO.put("customerHandle", customer.getFirstName());
		kritiqCustomerVO.put("clientUid", customer.getMerchantt().getMerchantUid());
		kritiqCustomerVO.put("clientHandle", customer.getMerchantt().getName());	
		}
		LOGGER.info("===============  KritiqServiceImpl : Inside getKritiqCustomerDetail starts :: customerFeedbackId  : "
				+ customerFeedback.getId());

		kritiqCustomerVOs.add(kritiqCustomerVO);
		}
			}
	}
		
		} catch (Exception e) {
			LOGGER.error("===============  KritiqServiceImpl : Inside getKritiqCustomerDetail :: Exception  ============= " + e);

			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	LOGGER.info("===============  KritiqServiceImpl : Inside getKritiqCustomerDetail :: End  ============= ");

	return kritiqCustomerVOs;
}
}
