package com.foodkonnekt.serviceImpl;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.foodkonnekt.clover.vo.CategoryInVo;
import com.foodkonnekt.clover.vo.CeategoryJsonVo;
import com.foodkonnekt.model.Category;
import com.foodkonnekt.model.CategoryDto;
import com.foodkonnekt.model.CategoryItem;
import com.foodkonnekt.model.CategoryTiming;
import com.foodkonnekt.model.Item;
import com.foodkonnekt.model.ItemDto;
import com.foodkonnekt.model.ItemModifierGroup;
import com.foodkonnekt.model.ItemModifiers;
import com.foodkonnekt.model.Merchant;
import com.foodkonnekt.model.ModifierGroup;
import com.foodkonnekt.model.Modifiers;
import com.foodkonnekt.model.PizzaTemplate;
import com.foodkonnekt.model.PizzaTemplateCategory;
import com.foodkonnekt.repository.CategoryItemRepository;
import com.foodkonnekt.repository.CategoryRepository;
import com.foodkonnekt.repository.CategoryTimingRepository;
import com.foodkonnekt.repository.ItemModifierGroupRepository;
import com.foodkonnekt.repository.ItemModifiersRepository;
import com.foodkonnekt.repository.ItemmRepository;
import com.foodkonnekt.repository.MerchantRepository;
import com.foodkonnekt.repository.ModifierModifierGroupRepository;
import com.foodkonnekt.repository.ModifiersRepository;
import com.foodkonnekt.repository.PizzaTemplateCategoryRepository;
import com.foodkonnekt.service.CategoryService;
import com.foodkonnekt.util.DateUtil;
import com.foodkonnekt.util.IConstant;
import com.foodkonnekt.util.MailSendUtil;
import com.foodkonnekt.util.UrlConstant;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Service
public class CategoryServiceImpl implements CategoryService {

	private static final Logger LOGGER= LoggerFactory.getLogger(CategoryServiceImpl.class);
	
	@Autowired
    private Environment environment;
	
    @Autowired
    private CategoryRepository categoryRepository;
    
    @Autowired
    private MerchantRepository merchantRepository;

    @Autowired
    private ItemModifierGroupRepository itemModifierGroupRepository;
    
    @Autowired
    private ItemModifiersRepository itemModifiersRepository;
    
    @Autowired
    private CategoryTimingRepository categoryTimingRepository;
    
    

    @Autowired
    private ModifierModifierGroupRepository modifierModifierGroupRepository;

    @Autowired
    private CategoryItemRepository categoryItemRepository;

    @Autowired
    private ItemmRepository itemmRepository;
    
    @Autowired
    private ModifiersRepository modifiersRepository;

    @Autowired
    private PizzaTemplateCategoryRepository pizzaTemplateCategoryRepository;
    /**
     * find category by merchantId
     */
    public List<Category> findAllCategory(Integer merchantId) {
    	
    	LOGGER.info("----------------Start :: CategoryServiceImpl : findAllCategory------------------------");
        List<Category> categories = categoryRepository.findByMerchantIdAndItemStatus(merchantId,0);
        LOGGER.info("CategoryServiceImpl :: findAllCategory : merchantId "+merchantId);
        List<Item> items=itemmRepository.findByMerchantId(merchantId);
        for(Item item:items){
            LOGGER.info("CategoryServiceImpl :: findAllCategory : itemId :"+item.getId());

        	List<ItemModifiers> itemModifiers=itemModifiersRepository.findByItemId(item.getId());
        	List<ItemModifierGroup> itemModifierGroups=itemModifierGroupRepository.findByItemId(item.getId());
        	if((itemModifierGroups!=null && itemModifierGroups.size()>0) ){
                LOGGER.info("CategoryServiceImpl :: findAllCategory : itemModifierGroupSize :"+itemModifierGroups.size());

        		if((itemModifiers==null || itemModifiers.size()==0)){
        		for(ItemModifierGroup itemModifierGroup:itemModifierGroups){
                    LOGGER.info("CategoryServiceImpl :: findAllCategory : itemModifiersSize:"+itemModifiers.size());
                    LOGGER.info("CategoryServiceImpl :: findAllCategory : ModifierGroupId:"+itemModifierGroup.getModifierGroup().getId());

        			List<Modifiers> modifiers=modifiersRepository.findByModifierGroupId(itemModifierGroup.getModifierGroup().getId());
        			for(Modifiers modifier:modifiers){
        				ItemModifiers itemModifier = new ItemModifiers();
        				itemModifier.setItem(item);
        				itemModifier.setModifierGroup(itemModifierGroup.getModifierGroup());
        				itemModifier.setModifiers(modifier);
        				itemModifier.setModifierStatus(IConstant.BOOLEAN_TRUE);
              		  itemModifiersRepository.save(itemModifier);
                      LOGGER.info("CategoryServiceImpl :: findAllCategory :  save itemModifiers:");

        			}
        		}
        	}else{
        		break;
        	}
        	}
        }
        List<Category> finalCategories = new ArrayList<Category>();
        for (Category category : categories) {
        	if(category.getItemStatus()!=null && category.getItemStatus()!=IConstant.SOFT_DELETE && category.getIsPizza()!=null && category.getIsPizza()==0){
            Category category2 = new Category();
            category2.setId(category.getId());
            category2.setName(category.getName());
            category2.setItemStatus(category.getItemStatus());
            LOGGER.info("CategoryServiceImpl :: findAllCategory : categoryId:"+category.getId());

            List<CategoryItem> categoryItems = categoryItemRepository.findByCategoryId(category.getId());
            if (categoryItems != null) {
                if (!categoryItems.isEmpty()) {
                    category2.setItemCount(categoryItems.size());
                }
            }
            finalCategories.add(category2);
        	}
        }
        LOGGER.info("----------------End :: CategoryServiceImpl : findAllCategory------------------------");
        return finalCategories;
    }

    /**
     * Find category count by merchantId
     */
    public Long categoryCountByMerchantId(Integer merchantId) {
        LOGGER.info("----------------Start :: CategoryServiceImpl : categoryCountByMerchantId------------------------");
        LOGGER.info(" : CategoryServiceImpl : categoryCountByMerchantId :merchantId:"+merchantId);
        LOGGER.info("----------------End :: CategoryServiceImpl : categoryCountByMerchantId------------------------");

        return categoryRepository.categoryCountByMerchantId(merchantId);
    }

    /**
     * Find by categoryId
     */
    public Category findCategoryById(int categoryId) {
    	 LOGGER.info("----------------Start :: CategoryServiceImpl : findCategoryById------------------------");
         LOGGER.info(" : CategoryServiceImpl : findCategoryById :categoryId:"+categoryId);
         LOGGER.info("----------------End :: CategoryServiceImpl : findCategoryById------------------------");

        return categoryRepository.findOne(categoryId);
    }
   
    public String changeCategoryOrder(){
   	 LOGGER.info("----------------Start :: CategoryServiceImpl : changeCategoryOrder------------------------");

    	List<Merchant> merchants=merchantRepository.findAll();
    	for(Merchant merchant:merchants){
    		int categoryOrder=1;
    	   	 LOGGER.info("CategoryServiceImpl : changeCategoryOrder :merchantId:"+merchant.getId());

    		List<Category> categories = categoryRepository.findByMerchantId(merchant.getId());
    		for(Category category:categories){
    			if(category.getItemStatus()!=IConstant.SOFT_DELETE && category.getItemStatus()!=IConstant.BOOLEAN_TRUE){
    				category.setSortOrder(categoryOrder++);
    			}else{
    				category.setSortOrder(IConstant.BOOLEAN_FALSE);
    			}
    			categoryRepository.save(category);
       	   	 LOGGER.info("CategoryServiceImpl : changeCategoryOrder :save category:");

    		}
    	}
  	   	 LOGGER.info("---------End :: CategoryServiceImpl : changeCategoryOrder :---------------");

    	return null;
    }
    /**
     * Update category status
     */
    public String updateCategoryStatusById(Category category) {
 	   	 LOGGER.info("---------Start :: CategoryServiceImpl : updateCategoryStatusById :---------------");

        Map<String, Object> map= new HashMap();
    	try{
    	   	 LOGGER.info("CategoryServiceImpl : updateCategoryStatusById : categoryId:"+category.getId());

    	   Category result = categoryRepository.findOne(category.getId());
      
        int mostAvailableOrder=0;
        if(category.getItemStatus()==IConstant.BOOLEAN_FALSE){
        	Pageable pageable = new PageRequest(1 - 1, 10, Sort.Direction.DESC, "id");
   	   	 LOGGER.info("CategoryServiceImpl : updateCategoryStatusById : merchantId :"+result.getMerchant().getId());

        	Page<Category> allCats= categoryRepository.findByMerchantIdAndItemStatusNot(result.getMerchant().getId(),pageable,IConstant.SOFT_DELETE);
        	int totalCategories=(int)allCats.getTotalElements();
        	List<Category> categories= categoryRepository.findByMerchantIdAndSortOrderNotOrderBySortOrderAsc(result.getMerchant().getId(), IConstant.BOOLEAN_FALSE);
        	int cateListSize=categories.size();
        	if(categories!=null && categories.size()>0){
          	   	 LOGGER.info("CategoryServiceImpl : updateCategoryStatusById : categoriesSize :"+categories.size());

        	for(int i=1;i<=totalCategories;i++){
        		if(i<=cateListSize){
        		Category category2=categories.get(i-1);
        		if(category2!=null){
        		if(category2.getSortOrder()!=i){
        			mostAvailableOrder=i;
        			break;
        		}}}else{
        			mostAvailableOrder=i;
        			break;
        		}
        	}
        	}else{
        		mostAvailableOrder=1;
        	}
        	result.setSortOrder(mostAvailableOrder);
        }else{
        	result.setSortOrder(IConstant.BOOLEAN_FALSE);
        	mostAvailableOrder=IConstant.BOOLEAN_FALSE;
        }
        result.setItemStatus(category.getItemStatus());
        categoryRepository.save(result);
        map.put("result", "success");
        map.put("menuOrder", mostAvailableOrder);
       }catch(Exception e){
    	   map.put("result", "failed");
   	   	 LOGGER.info(" CategoryServiceImpl : updateCategoryStatusById :Exception:"+e);

       }
 	   	 LOGGER.info("----------------- End :: CategoryServiceImpl : updateCategoryStatusById :");

        return new Gson().toJson(map);
    }
    
    public boolean updateCategorySortOrderById(Category category ,String action) {
	   	 LOGGER.info("----------------- Start :: CategoryServiceImpl : updateCategorySortOrderById :-------------");
	   	 LOGGER.info(" CategoryServiceImpl : updateCategorySortOrderById :categoryId():"+category.getId());

        Category result = categoryRepository.findOne(category.getId());
        if(result!=null){
        int oldOrder=0;
        if(result.getSortOrder()!=null){
        	oldOrder=result.getSortOrder();
        }
	   	 LOGGER.info(" CategoryServiceImpl : updateCategorySortOrderById :merchantId:"+result.getMerchant().getId());

       List<Category> categories= categoryRepository.findByMerchantIdAndSortOrderAndIdNot(result.getMerchant().getId(),category.getSortOrder(),category.getId());
       if(action!=null&&!action.isEmpty()){
    	   if(action.equals("check")){
    		   	 LOGGER.info(" CategoryServiceImpl : updateCategorySortOrderById :categoriesSize():"+categories.size());

       if(categories!=null && categories.size()>0){
		   	 LOGGER.info(" End :: CategoryServiceImpl : updateCategorySortOrderById : Return false:");

        	return false;
        }else{
        result.setSortOrder(category.getSortOrder());
        categoryRepository.save(result);
	   	 LOGGER.info(" End :: CategoryServiceImpl : updateCategorySortOrderById : Return true:");

        return true;
        }}else{
        	//oldOrder=result.getSortOrder();
        	 result.setSortOrder(category.getSortOrder());
             categoryRepository.save(result);
        	for(Category category2:categories){
        		category2.setSortOrder(oldOrder);
        		categoryRepository.save(category2);
        	}
   	   	 LOGGER.info(" End :: CategoryServiceImpl : updateCategorySortOrderById : Return true:");

        	return true;
        }
       }else{
  	   	 LOGGER.info(" End :: CategoryServiceImpl : updateCategorySortOrderById : Return true:");

        	return true;
        }}else{
   	   	 LOGGER.info(" End :: CategoryServiceImpl : updateCategorySortOrderById : Return true:");

        	return true;
        }
    }

    /**
     * Find modifiers by itemId
     */
    public List<Modifiers> findByItemId(Integer itemId) {
	   	 LOGGER.info(" Start :: CategoryServiceImpl : findByItemId : ");
	   	 LOGGER.info("  CategoryServiceImpl : findByItemId : itemId:"+itemId);

        List<Modifiers> result = new ArrayList<Modifiers>();
        List<ItemModifierGroup> itemModifierGroups = itemModifierGroupRepository.findByItemId(itemId);
        for (ItemModifierGroup group : itemModifierGroups) {
   	   	 LOGGER.info("  CategoryServiceImpl : findByItemId : ModifierGroupId:"+group.getModifierGroup().getId());

            List<Modifiers> modifiers = modifierModifierGroupRepository.findByModifierGroupId(group.getModifierGroup()
                            .getId());
            for (Modifiers modifier : modifiers) {
                int count = 0;
                modifier.setAction("<a href=editModifier?modifierId=" + modifier.getId()
                        + " class='edit'><i class='fa fa-pencil' aria-hidden='true'></i></a>");
                Set<ModifierGroup> groups = modifier.getModifierGroup();
                for (ModifierGroup modifierGroup : groups) {
                    List<ItemModifierGroup> list = itemModifierGroupRepository.findByModifierGroupId(modifierGroup
                                    .getId());
                    if (list != null) {
                        if (!list.isEmpty()) {
                            count = count + list.size();
                        }
                    }
                }
                modifier.setItemCount(count);
                modifier.setModifierGroup(null);
                result.add(modifier);
            }
        }
  	   	 LOGGER.info(" End :: CategoryServiceImpl : findByItemId :");

        return result;
    }

    /**
     * Find categories by merchantId
     */
    public List<CategoryDto> findCategoriesByMerchantId(Integer merchantId) {
	   	 LOGGER.info(" Start :: CategoryServiceImpl : findCategoriesByMerchantId :");
 	   	 LOGGER.info(" CategoryServiceImpl : findCategoriesByMerchantId : merchantId:"+merchantId);

        List<Category> categories = categoryRepository.findByMerchantIdOrderBySortOrderAsc(merchantId);
        List<CategoryDto> finalCategories = new ArrayList<CategoryDto>();
        for (Category category : categories) {
        	if(category.getItemStatus()!=null && category.getItemStatus()!=IConstant.SOFT_DELETE){
        	   	 LOGGER.info(" CategoryServiceImpl : findCategoriesByMerchantId : categoryId():"+category.getId());
	
            List<Item> items = itemmRepository.findByCategoriesId(category.getId());
            List<ItemDto> itemDtos=getItems(items);
   	   	 LOGGER.info(" CategoryServiceImpl : findCategoriesByMerchantId : itemsSize():"+items.size());

            if (items != null && items.size() > 0 && itemDtos!=null && itemDtos.size() >0) {
                CategoryDto categoryDto = new CategoryDto();
                categoryDto.setId(category.getId());
                categoryDto.setCategoryName(category.getName());
                categoryDto.setCategoryStatus(category.getItemStatus());
                finalCategories.add(categoryDto);
            }
        	}
        }
  	   	 LOGGER.info("End :: CategoryServiceImpl : findCategoriesByMerchantId : ");

        return finalCategories;
    }
    
    
    List<ItemDto> getItems(List<Item> items){
 	   	 LOGGER.info("Start :: CategoryServiceImpl : getItems ");

    	List<ItemDto> itemDtos = new ArrayList<ItemDto>();
        try {
    	   	 LOGGER.info(" CategoryServiceImpl : getItems : itemSize:"+items.size());

            if (items.size() != 0) {
                for (Item item : items) {
                    if (item.getItemStatus()!=null && item.getItemStatus() == 0) {
                        ItemDto itemDto = new ItemDto();
                        itemDto.setId(item.getId());
                        itemDto.setItemName(item.getName());
                        itemDto.setPrice(item.getPrice());
                        itemDto.setItemPosId(item.getPosItemId());
                        itemDto.setAllowModifierLimit(item.getAllowModifierLimit());
                        if(item.getDescription()!=null){
                        	itemDto.setDescription(item.getDescription());
                        }else{
                        	itemDto.setDescription("");
                        }
                        itemDtos.add(itemDto);
                    }
                }
            }
   	   	 LOGGER.info("--------------- End :: CategoryServiceImpl : getItems :------------------ ");

            return itemDtos;
        } catch (Exception e) {
            if (e != null) {
          	   	 LOGGER.info("End :: CategoryServiceImpl : getItems :Exception:"+e);

                MailSendUtil.sendExceptionByMail(e,environment);
            }
            LOGGER.error("error: " + e.getMessage());
     	   	 LOGGER.info("End :: CategoryServiceImpl : getItems :Exception:"+e);

            return null;
        }
    }

    public String findCAtegoryInventory(Integer merchantId, Integer pageDisplayLength, Integer pageNumber,
                    String searchParameter) {
	   	 LOGGER.info("--------------Start :: CategoryServiceImpl : findCAtegoryInventory :----------------");
 	   	 LOGGER.info(" CategoryServiceImpl : findCAtegoryInventory : merchantId:"+merchantId+"pageDisplayLength"+pageDisplayLength+":pageNumber:"+pageNumber+":searchParameter:"+searchParameter); 

        Pageable pageable = new PageRequest(pageNumber - 1, pageDisplayLength);
        Page<Category> categories = categoryRepository.findByMerchantIdAndItemStatus(merchantId, pageable,IConstant.SOFT_DELETE);
        List<CategoryInVo> finalCategories = new ArrayList<CategoryInVo>();
        for (Category category : categories.getContent()) {
        	if(category.getItemStatus()!=null && category.getItemStatus()!=IConstant.SOFT_DELETE){
        	   	 LOGGER.info(" CategoryServiceImpl : findCAtegoryInventory : categoryId:"+category.getId()); 

        		List<CategoryTiming> categoryTimings=categoryTimingRepository.findByCategoryId(category.getId());
        		String days="";
        		String startTime="12:00 AM";
        		String endTime="12:00 AM";
        		if(categoryTimings!=null){
        			for(CategoryTiming categoryTiming:categoryTimings){
        				if(!categoryTiming.isHoliday()){
        					days=days+","+categoryTiming.getDay();
        					startTime=categoryTiming.getStartTime();
        					endTime=categoryTiming.getEndTime();
        				}
        			}
        		}
        	CategoryInVo category2 = new CategoryInVo();
   	   	 LOGGER.info(" CategoryServiceImpl : findCAtegoryInventory : days:"+days+":startTime:"+startTime+":endTime:"+endTime); 

        	category2.setTiming("<div><a href=javascript:void(0) id='categoryTiming_"+ category.getId()+"' class='nav-toggle-timing' catid=" + category.getId()+ " days=" + days + " startTime=" + startTime+ " endTime=" + endTime+ " allowCategoryTimings=" + category.getAllowCategoryTimings()+ "  style='color: blue;'>Set Display Timings</a></div>");
        	
            
        	category2.setImage("<div><a href=javascript:void(0)  class=''  onclick='openBrowseImage("+ category.getId()+")' style='color: blue;'>Upload Image</a></div>");
            
        	
        	category2.setId(category.getId());
            category2.setName(category.getName());
            int totalRecords=(int)categories.getTotalElements();
            Integer sortOrder=category.getSortOrder();
            int selectedMenuOrder=0;
            if(sortOrder!=null && !sortOrder.equals("")){
            	try{
            		selectedMenuOrder=sortOrder;
            	}catch(Exception e){
              	   	 LOGGER.error("End :: CategoryServiceImpl : findCAtegoryInventory :Exception:"+e); 

            	}
            }
            String menuOrder="";
      	   	 LOGGER.info(" CategoryServiceImpl : findCAtegoryInventory : Status:"+category.getItemStatus()); 

            if (category.getItemStatus()!=null && category.getItemStatus() == 0) {
             menuOrder="<select  class='category_order' id='menuOrdr_"+category.getId()+"' style='width: 89%' catid=" + category.getId()+ ">";
            
            }else {
            	menuOrder="<select class='category_order' id='menuOrdr_"+category.getId()+"' style='width: 89%' catid=" + category.getId()+ " disabled>";
            	menuOrder=menuOrder+"<option id='0' value='0' selected>0</option>";
            }
            for(int i=1;i<=totalRecords;i++){
            	if(selectedMenuOrder==i){
            		menuOrder=menuOrder+"<option id='"+i+"' value='"+i+"' selected>"+i+"</option>";
            	}else{
            		menuOrder=menuOrder+"<option id='"+i+"' value='"+i+"'>"+i+"</option>";
            	}
            	
            	}
            menuOrder=menuOrder+"</select>";
            category2.setMenuOrder(menuOrder);

            
            if (category.getItemStatus()!=null && category.getItemStatus() == 0) {
                category2.setAction("<div><a href=javascript:void(0) class='nav-toggle' catid=" + category.getId()
                                + " style='color: blue;'>Active</a></div>");
             }else {
                category2.setAction("<div><a href=javascript:void(0) class='nav-toggle' catid=" + category.getId()
                                + " style='color: blue;'>Inactive</a></div>");
            }
            
           // List<CategoryItem> categoryItems = categoryItemRepository.findByCategoryId(category.getId());
            Long itemCount=categoryItemRepository.categoryItemCountByCategoryIdAndItemStatus(category.getId(),IConstant.SOFT_DELETE);
            Long pizzaCount = pizzaTemplateCategoryRepository.countByCategoryId(category.getId());
            itemCount += pizzaCount != null?pizzaCount:0;
            
           /* if (categoryItems != null) {
                if (!categoryItems.isEmpty()) {*/
     	   	 LOGGER.info(" CategoryServiceImpl : findCAtegoryInventory : itemCount:"+itemCount); 
 	
            	if(itemCount!=null){
                   
            		if(category.getIsPizza()==1){
            			category2.setItemCount("<a href=pizzaTamplate style='color: blue'>" + itemCount + "</a>");
            		}else{
            			category2.setItemCount("<a href=findItemsByCategoryId?categoryId=" + category.getId()
                                + " style='color: blue'>" + itemCount + "</a>");
            		}
                } else {
                    category2.setItemCount("<a href=findItemsByCategoryId?categoryId=" + category.getId()
                                    + " style='color: blue'></a>");
                }
            //}
            
            if(category.getIsPizza()==1){
            	category2.setIsPizza(true);
            }else{
            	category2.setIsPizza(false);
            }
            
            category2.setEdit("<a href=editCategory?categoryId=" + category.getId()
                    + " class='edit'><i class='fa fa-pencil' aria-hidden='true'></i></a>");
            
            finalCategories.add(category2);
            }
        }
        CeategoryJsonVo customerJsonVo = new CeategoryJsonVo();
        customerJsonVo.setiTotalDisplayRecords((int)categories.getTotalElements());
        customerJsonVo.setiTotalRecords((int)categories.getTotalElements());
        customerJsonVo.setAaData(finalCategories);
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
	   	 LOGGER.info("-----------End :: CategoryServiceImpl : findCAtegoryInventory :--------------- "); 

        return gson.toJson(customerJsonVo);
    }

    public String searchCategoryByTxt(Integer merchantId, String searchTxt) {
	   	 LOGGER.info("-----------Start :: CategoryServiceImpl : searchCategoryByTxt :--------------- "); 
	   	 LOGGER.info(" CategoryServiceImpl : searchCategoryByTxt :merchantId :"+merchantId+":searchTxt:"+searchTxt); 

    	Pageable pageable = new PageRequest(1 - 1, 10, Sort.Direction.DESC, "id");
        Page<Category> allCategories = categoryRepository.findByMerchantIdAndItemStatusNot(merchantId, pageable,IConstant.SOFT_DELETE);
        List<Category> categories = categoryRepository.findByMerchantIdAndCategoryName(merchantId, searchTxt);
        List<CategoryInVo> finalCategories = new ArrayList<CategoryInVo>();
        for (Category category : categories) {
            if(category.getItemStatus()!=null && category.getItemStatus()!=IConstant.SOFT_DELETE){
       	   	 LOGGER.info(" CategoryServiceImpl : searchCategoryByTxt :itemStatus :"+category.getItemStatus()+":categoryId():"+category.getId()); 

            	List<CategoryTiming> categoryTimings=categoryTimingRepository.findByCategoryId(category.getId());
        		String days="";
        		String startTime="12:00 AM";
        		String endTime="12:00 AM";
        		if(categoryTimings!=null){
        			for(CategoryTiming categoryTiming:categoryTimings){
        				if(!categoryTiming.isHoliday()){
        					days=days+","+categoryTiming.getDay();
        					startTime=categoryTiming.getStartTime();
        					endTime=categoryTiming.getEndTime();
        				}
        			}
        		}
        	CategoryInVo category2 = new CategoryInVo();
      	   	 LOGGER.info(" CategoryServiceImpl : searchCategoryByTxt :days :"+days+":startTime:"+startTime+":endTime:"+endTime); 

        	category2.setTiming("<div><a href=javascript:void(0) id='categoryTiming_"+ category.getId()+"' class='nav-toggle-timing' catid=" + category.getId()+ " days=" + days + " startTime=" + startTime+ " endTime=" + endTime+ " allowCategoryTimings=" + category.getAllowCategoryTimings()+ "  style='color: blue;'>Set Display Timings</a></div>");
        	
        	category2.setImage("<div><a href=javascript:void(0) ' class='nav-toggle-timing'  style='color: blue;'>Upload Image</a></div>");
            	
        	/*CategoryInVo category2 = new CategoryInVo();*/
            category2.setId(category.getId());
            category2.setName(category.getName());
            int totalRecords=(int)allCategories.getTotalElements();
           // int totalRecords=(int)categories.size();
            Integer sortOrder=category.getSortOrder();
            int selectedMenuOrder=0;
            if(sortOrder!=null && !sortOrder.equals("")){
            	try{
            		selectedMenuOrder=sortOrder;
            	}catch(Exception e){
            		
            	}
            }
            String menuOrder="";
     	   	 LOGGER.info(" CategoryServiceImpl : searchCategoryByTxt :itemStatus :"+category.getItemStatus()+":categoryId():"+category.getId() ); 

            if (category.getItemStatus()!=null && category.getItemStatus() == 0) {
             menuOrder="<select class='category_order' id='menuOrdr_"+category.getId()+"' style='width: 45%' catid=" + category.getId()+ ">";
            
            }else {
            	menuOrder="<select class='category_order' id='menuOrdr_"+category.getId()+"' style='width: 45%' catid=" + category.getId()+ " disabled>";
            	menuOrder=menuOrder+"<option id='0' value='0' selected>0</option>";
            }
            for(int i=1;i<=totalRecords;i++){
            	if(selectedMenuOrder==i){
            		menuOrder=menuOrder+"<option id='"+i+"' value='"+i+"' selected>"+i+"</option>";
            	}else{
            		menuOrder=menuOrder+"<option id='"+i+"' value='"+i+"'>"+i+"</option>";
            	}
            	
            	}
            menuOrder=menuOrder+"</select>";
            category2.setMenuOrder(menuOrder);

            if (category.getItemStatus() == 0) {
                category2.setAction("<div><a href=javascript:void(0) class='nav-toggle' catid=" + category.getId()
                                + " style='color: blue;'>Active</a></div>");
            } else {
                category2.setAction("<div><a href=javascript:void(0) class='nav-toggle' catid=" + category.getId()
                                + " style='color: blue;'>Inactive</a></div>");
            }
           // List<CategoryItem> categoryItems = categoryItemRepository.findByCategoryId(category.getId());
            Long itemCount=categoryItemRepository.categoryItemCountByCategoryIdAndItemStatus(category.getId(),IConstant.SOFT_DELETE);
            if (itemCount != null) {
                /*if (!categoryItems.isEmpty()) {*/
                    category2.setItemCount("<a href=findItemsByCategoryId?categoryId=" + category.getId()
                                    + " style='color: blue'>" + itemCount + "</a>");
                } else {
                    category2.setItemCount("<a href=findItemsByCategoryId?categoryId=" + category.getId()
                                    + " style='color: blue'></a>");
                }
            /*}*/
    	   	 LOGGER.info(" CategoryServiceImpl : searchCategoryByTxt :isPizza :"+category.getIsPizza()); 

            if(category.getIsPizza()==1){
            	category2.setIsPizza(true);
            }else{
            	category2.setIsPizza(false);
            }
            
            finalCategories.add(category2);
            }
        }
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
	   	 LOGGER.info("--------------End :: CategoryServiceImpl : searchCategoryByTxt :--------------------"); 

        return gson.toJson(finalCategories);
    }

	public List<Category> getAllCategoriesByMerchantUId(String merchantUId) {
	   	 LOGGER.info("--------------Start :: CategoryServiceImpl : getAllCategoriesByMerchantUId :--------------------"); 
	   	 LOGGER.info("CategoryServiceImpl : getAllCategoriesByMerchantUId :merchantUId:"+merchantUId); 

		List<Category> categories=  categoryRepository.findByMerchantMerchantUidAndIsPizza(merchantUId,0);
             List<Item> items=itemmRepository.findByMerchantMerchantUid(merchantUId);
             for(Item item:items){
        	   	 LOGGER.info("CategoryServiceImpl : getAllCategoriesByMerchantUId : itemId :"+item.getId()); 

             	List<ItemModifiers> itemModifiers=itemModifiersRepository.findByItemId(item.getId());
             	List<ItemModifierGroup> itemModifierGroups=itemModifierGroupRepository.findByItemId(item.getId());
       	   	 LOGGER.info("CategoryServiceImpl : getAllCategoriesByMerchantUId : itemModifierGroupsSize :"+itemModifierGroups.size()); 

             	if((itemModifierGroups!=null && itemModifierGroups.size()>0) ){
              	   	 LOGGER.info("CategoryServiceImpl : getAllCategoriesByMerchantUId : itemModifiersSize:"+itemModifiers.size()); 

             		if((itemModifiers==null || itemModifiers.size()==0)){
             		for(ItemModifierGroup itemModifierGroup:itemModifierGroups){
                 	   	 LOGGER.info("CategoryServiceImpl : getAllCategoriesByMerchantUId : ModifierGroupId():"+itemModifierGroup.getModifierGroup().getId()); 

             			List<Modifiers> modifiers=modifiersRepository.findByModifierGroupId(itemModifierGroup.getModifierGroup().getId());
             			for(Modifiers modifier:modifiers){
             				ItemModifiers itemModifier = new ItemModifiers();
             				itemModifier.setItem(item);
             				itemModifier.setModifierGroup(itemModifierGroup.getModifierGroup());
             				itemModifier.setModifiers(modifier);
             				itemModifier.setModifierStatus(IConstant.BOOLEAN_TRUE);
                   		  itemModifiersRepository.save(itemModifier);
                  	   	 LOGGER.info("CategoryServiceImpl : getAllCategoriesByMerchantUId : itemModifier  save"); 

             			}
             		}
             	}else{
             		break;
             	}
             	}
             }
            // Merchant merchantObj = merchantRepository.findByUniqueId(merchantUId);
             List<Category> finalCategories = new ArrayList<Category>();
             for (Category category : categories) {
             	if(category.getItemStatus()!=null && category.getItemStatus()!=IConstant.SOFT_DELETE){
                 Category category2 = new Category();
                 category2.setId(category.getId());
                 category2.setName(category.getName());
                 category2.setItems(null);
                 category2.setPizzaSizes(null);
                category2.setSortOrder(null);
                // category2.setItemStatus(category.getItemStatus());
                 /*if(merchantObj!=null){
                 category2.setMerchant(merchantObj);
                 }*/
                /* List<CategoryItem> categoryItems = categoryItemRepository.findByCategoryId(category.getId());
                 if (categoryItems != null) {
                     if (!categoryItems.isEmpty()) {
                         category2.setItemCount(categoryItems.size());
                     }
                 }*/
                 finalCategories.add(category2);
             	}
             }
      	   	 LOGGER.info("--------- End :: CategoryServiceImpl : getAllCategoriesByMerchantUId :-------------------"); 

             return finalCategories; 
             
	}

	public List<Category> getAllCategoriesByVendorUId(String vendorUId) {
	   	 LOGGER.info("--------- Start :: CategoryServiceImpl : getAllCategoriesByVendorUId :-------------------"); 
 	   	 LOGGER.info(" CategoryServiceImpl : getAllCategoriesByVendorUId : vendorUId :"+vendorUId); 

		List<Category> categories= categoryRepository.findByMerchantOwnerVendorUid(vendorUId);
             List<Item> items=itemmRepository.findByMerchantOwnerVendorUid(vendorUId);
             for(Item item:items){
         	   	 LOGGER.info(" CategoryServiceImpl : getAllCategoriesByVendorUId : itemId :"+item.getId()); 

              	List<ItemModifiers> itemModifiers=itemModifiersRepository.findByItemId(item.getId());
              	List<ItemModifierGroup> itemModifierGroups=itemModifierGroupRepository.findByItemId(item.getId());
        	   	 LOGGER.info(" CategoryServiceImpl : getAllCategoriesByVendorUId : itemModifierGroupsSize :"+itemModifierGroups.size()); 

              	if((itemModifierGroups!=null && itemModifierGroups.size()>0) ){
           	   	 LOGGER.info(" CategoryServiceImpl : getAllCategoriesByVendorUId : itemModifiersSize:"+itemModifiers.size()); 

              		if((itemModifiers==null || itemModifiers.size()==0)){
              		for(ItemModifierGroup itemModifierGroup:itemModifierGroups){
                  	   	 LOGGER.info(" CategoryServiceImpl : getAllCategoriesByVendorUId : ModifiersGroupId:"+itemModifierGroup.getModifierGroup().getId()); 

              			List<Modifiers> modifiers=modifiersRepository.findByModifierGroupId(itemModifierGroup.getModifierGroup().getId());
              			for(Modifiers modifier:modifiers){
              				ItemModifiers itemModifier = new ItemModifiers();
              				itemModifier.setItem(item);
              				itemModifier.setModifierGroup(itemModifierGroup.getModifierGroup());
              				itemModifier.setModifiers(modifier);
              				itemModifier.setModifierStatus(IConstant.BOOLEAN_TRUE);
                    		  itemModifiersRepository.save(itemModifier);
                       	   	 LOGGER.info(" CategoryServiceImpl : getAllCategoriesByVendorUId : save itemModifier:"); 

              			}
              		}
              	}else{
              		break;
              	}
              	}
              }
              List<Category> finalCategories = new ArrayList<Category>();
              for (Category category : categories) {
              	if(category.getItemStatus()!=null && category.getItemStatus()!=IConstant.SOFT_DELETE){
                  Category category2 = new Category();
                  category2.setId(category.getId());
                  category2.setName(category.getName());
                  category2.setItems(null);
                  category2.setPizzaSizes(null);
                  category2.setSortOrder(null);
                  //category2.setItemStatus(category.getItemStatus());
                 // category2.setMerchant(category.getMerchant());
                  /* List<CategoryItem> categoryItems = categoryItemRepository.findByCategoryId(category.getId());
                  if (categoryItems != null) {
                      if (!categoryItems.isEmpty()) {
                          category2.setItemCount(categoryItems.size());
                      }
                  }*/
                  finalCategories.add(category2);
              	}
              }
        	   	 LOGGER.info("------------------End :: CategoryServiceImpl : getAllCategoriesByVendorUId :--------------------------"); 

              return finalCategories; 
	}

	public String updateCategoryTiming(int categoryId, String days,
			String startTime, String endTime,Integer allowCategoryTimings) {
	   	 LOGGER.info("------------------Start :: CategoryServiceImpl : updateCategoryTiming :--------------------------"); 
   	   	 LOGGER.info(" CategoryServiceImpl : updateCategoryTiming : categoryId :"+categoryId+":days:"+days+":startTime:"+startTime+":endTime:"+endTime+":allowCategoryTimings:"+allowCategoryTimings); 

		try {
        	String weekDays[]={"Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"};
        	List<CategoryTiming> categoryTimings=categoryTimingRepository.findByCategoryId(categoryId);
      	   	 LOGGER.info(" CategoryServiceImpl : updateCategoryTiming :  categoryTimingsSize:"+ categoryTimings.size()); 

        	if(categoryTimings!=null && categoryTimings.size()>0){
        	categoryTimingRepository.deleteByCategoryId(categoryId);
     	   	 LOGGER.info(" CategoryServiceImpl : updateCategoryTiming :  categoryTimings deleted:"); 

        	}
        	try{
        	startTime=DateUtil.convert12hoursTo24HourseFormate(startTime);
        	endTime=DateUtil.convert12hoursTo24HourseFormate(endTime);
        	}catch(Exception e){
        		System.out.println(e);
        	   	 LOGGER.error("End :: CategoryServiceImpl : updateCategoryTiming : Exception :"+e); 

        	}
        	//
            Category category = categoryRepository.findOne(categoryId);
            categoryTimings= new ArrayList<CategoryTiming>();
            if(days!=null && !days.isEmpty() && !days.equals("") && allowCategoryTimings!=null && allowCategoryTimings!=0){
            	String timingDays[]=days.split(",");
            	for(String day:weekDays){
            		if(!days.contains(day)){
            			CategoryTiming categoryTiming= new CategoryTiming();
            			categoryTiming.setDay(day);
            			categoryTiming.setStartTime("00:00:00");
            			categoryTiming.setEndTime("24:00:00");
            			categoryTiming.setCategory(category);
            			categoryTiming.setHoliday(true);
            		categoryTimings.add(categoryTiming);
            		}else{
            			CategoryTiming categoryTiming= new CategoryTiming();
            			categoryTiming.setDay(day);
            			categoryTiming.setStartTime(startTime);
            			if(endTime.equals("00:00")){
            				endTime="24:00";
            			}
            			categoryTiming.setEndTime(endTime);
            			categoryTiming.setCategory(category);
            			categoryTiming.setHoliday(false);
            			categoryTimings.add(categoryTiming);
            		}
            	}
            	
            }else{
            	
            	for(String day:weekDays){
            		CategoryTiming categoryTiming= new CategoryTiming();
        			categoryTiming.setDay(day);
        			categoryTiming.setStartTime("00:00:00");
        			categoryTiming.setEndTime("24:00:00");
        			categoryTiming.setCategory(category);
        			categoryTiming.setHoliday(false);
        			categoryTimings.add(categoryTiming);
            	}
            }
           // category.setCategoryTimings(categoryTimings);
            category.setAllowCategoryTimings(allowCategoryTimings);
            categoryRepository.save(category);
      	   	 LOGGER.info(" CategoryServiceImpl : updateCategoryTiming : save category :"); 

            categoryTimingRepository.save(categoryTimings);
     	   	 LOGGER.info(" CategoryServiceImpl : updateCategoryTiming : save categoryTimings :"); 

		}catch(Exception exception){
			System.out.println(exception);
    	   	 LOGGER.error(" CategoryServiceImpl : updateCategoryTiming : Exception :"+exception); 

		}
	   	 LOGGER.info("---------------End :: CategoryServiceImpl : updateCategoryTiming :Return null:-----------"); 

		return null;
	}

	public List<CategoryTiming> getCategoryTiming(int categoryId) {
	   	 LOGGER.info("---------------Start :: CategoryServiceImpl : getCategoryTiming:-------------"); 
	   	 LOGGER.info(" CategoryServiceImpl : getCategoryTiming: categoryId :"+categoryId); 

		List<CategoryTiming> categoryTimings=categoryTimingRepository.findByCategoryId(categoryId);
		for(CategoryTiming categoryTiming:categoryTimings){
			categoryTiming.setCategory(null);
		   	 LOGGER.info(" CategoryServiceImpl : getCategoryTiming: StartTime :"+categoryTiming.getStartTime()); 
		   	 LOGGER.info(" CategoryServiceImpl : getCategoryTiming: endTime :"+categoryTiming.getEndTime()); 

			categoryTiming.setStartTime(DateUtil.get12Format(categoryTiming.getStartTime()));
			categoryTiming.setEndTime(DateUtil.get12Format(categoryTiming.getEndTime()));
		}
	   	 LOGGER.info("---------------End :: CategoryServiceImpl : getCategoryTiming:-------------"); 

		return categoryTimings;
	}
	public void updateCategoryValue(Category category, MultipartFile file) {
	   	 LOGGER.info("---------------Start :: CategoryServiceImpl : updateCategoryValue :-------------"); 
	   	 LOGGER.info(" CategoryServiceImpl : updateCategoryValue :categoryId:"+category.getId()); 

		// TODO Auto-generated method stub
		Category resultCategory = categoryRepository.findOne(category.getId());
	   	 LOGGER.info(" CategoryServiceImpl : updateCategoryValue :categoryName:"+category.getName()); 

		if(category.getName()!= null && !category.getName().isEmpty() && category.getName()!=""){
			resultCategory.setName(category.getName());
		}
		
	   	 LOGGER.info(" CategoryServiceImpl : updateCategoryValue :fileSize:"+file.getSize()); 

		if (!(file.getSize() == 0)) {
            // images
            Long size = file.getSize();
            if (size >= 2097152) {
                //model.addAttribute("filesize", "Maximum Allowed File Size is 2 MB");
                //return "uploadLogo";
            }

            String orgName = file.getOriginalFilename();

            String exts[] = orgName.split("\\.");

            String ext = exts[1];
            String logoName = resultCategory.getId() + "_" + resultCategory.getName().replaceAll("[^a-zA-Z0-9]", "") + "." + ext;
            //String logoName = item.getId() + "_" + "." + ext;
            String filePath = environment.getProperty("ADMIN_SERVER_LOGO_PATH") + logoName;
            File dest = new File(filePath);
            try {
                file.transferTo(dest);
            } catch (IllegalStateException e) {
                LOGGER.error("error: " + e.getMessage());
       	   	 LOGGER.error(" End :: CategoryServiceImpl : updateCategoryValue : IllegalStateException :"+e); 

                //return "File uploaded failed:" + orgName;
            } catch (IOException e) {
                LOGGER.error("error: " + e.getMessage());
          	   	 LOGGER.error(" End :: CategoryServiceImpl : updateCategoryValue : IOException :"+e); 

                //return "File uploaded failed:" + orgName;
            }
            System.out.println("File uploaded:" + orgName);
     	   	 LOGGER.info("CategoryServiceImpl : updateCategoryValue : File uploaded:" + orgName); 

            resultCategory.setCategoryImage(environment.getProperty("ADMIN_LOGO_PATH_TO_SHOW") + logoName);
        }
		
		
	        
		categoryRepository.save(resultCategory);
	   	 LOGGER.info("CategoryServiceImpl : updateCategoryValue : save category:"); 

	
	}
	
	 public Category findCategoryByCategoryId(int categoryId){
	   	 LOGGER.info("-----------Start :: CategoryServiceImpl : findCategoryByCategoryId :"); 
	   	 LOGGER.info(" CategoryServiceImpl : findCategoryByCategoryId : categoryId:"+categoryId); 

		 Category category = categoryRepository.findOne(categoryId);
	   	 LOGGER.info("-----------End :: CategoryServiceImpl : findCategoryByCategoryId :"); 

			return category;
		 
	 }

	public Category findByMerchantIdAndPosCategoryId(Integer merchantId, String posId) {
		LOGGER.info("-----------Start :: CategoryServiceImpl : findByMerchantIdAndPosCategoryId :"); 
	   	 LOGGER.info(" CategoryServiceImpl : findByMerchantIdAndPosCategoryId : merchantId:"+merchantId+":posId:"+posId); 


		 Category category = categoryRepository.findByMerchantIdAndPosCategoryId(merchantId, posId);
			LOGGER.info("-----------End :: CategoryServiceImpl : findByMerchantIdAndPosCategoryId :"); 
	
		 return category;
	}

	public Category createCategory(Category category) {
		LOGGER.info("-----------Start :: CategoryServiceImpl : createCategory :"); 
		LOGGER.info("-----------End :: CategoryServiceImpl : createCategory : save category "); 
        return categoryRepository.save(category);
    }

	public void updateCategoryItem(CategoryItem categoryItem) {
		LOGGER.info("-----------Start :: CategoryServiceImpl : updateCategoryItem :"); 
		LOGGER.info("CategoryServiceImpl : updateCategoryItem : categoryId :"+categoryItem.getCategory().getId()); 
       
		Category category = null;
		if(categoryItem.getCategory()!=null && categoryItem.getCategory().getId()!=null){
			 category = categoryRepository.findOne(categoryItem.getCategory().getId());
			if(category!=null){
				if(categoryItem.getCategory().getName()!=null){
					category.setName(categoryItem.getCategory().getName());
					categoryRepository.save(category);
					LOGGER.info("CategoryServiceImpl : updateCategoryItem : save category :"); 

				}
			}
		}
		
		if (categoryItem != null && categoryItem.getCategory() != null && categoryItem.getCategory().getId() != null) {
			if(category != null && category.getIsPizza() == 0)
			{
			List<CategoryItem> categoryItems = categoryItemRepository.findByCategoryId(categoryItem.getCategory().getId());
			if(categoryItems!=null && !categoryItems.isEmpty()){
				categoryItemRepository.delete(categoryItems);
				LOGGER.info("CategoryServiceImpl : updateCategoryItem : deleted categoryItems :"); 

			}
			LOGGER.info("CategoryServiceImpl : updateCategoryItem :  categoryId :"+ categoryItem.getCategory().getId()); 

			
			List<Integer> itemIds = categoryItem.getiId();
			if (itemIds != null && !itemIds.isEmpty()) {
				for (Integer id : itemIds) {
					Item item = new Item();
					item.setId(id);

					Category category1 = new Category();
					category1.setId(categoryItem.getCategory().getId());

					CategoryItem finaCategoryItem = new CategoryItem();
					finaCategoryItem.setItem(item);
					finaCategoryItem.setCategory(category1);
					finaCategoryItem.setActive(1);
					categoryItemRepository.save(finaCategoryItem);
					LOGGER.info("CategoryServiceImpl : updateCategoryItem : save CategoryItem :"); 

				}
			}
		
		}
			
			if(category != null &&  category.getIsPizza() == 1)
			{

				List<PizzaTemplateCategory> categoryItems = pizzaTemplateCategoryRepository.findByCategoryId(categoryItem.getCategory().getId());
				if(categoryItems!=null && !categoryItems.isEmpty()){
					pizzaTemplateCategoryRepository.delete(categoryItems);
					LOGGER.info("CategoryServiceImpl : updateCategoryItem : deleted categoryItems :"); 

				}
				LOGGER.info("CategoryServiceImpl : updateCategoryItem :  categoryId :"+ categoryItem.getCategory().getId()); 

				
				List<Integer> itemIds = categoryItem.getiId();
				if (itemIds != null && !itemIds.isEmpty()) {
					for (Integer id : itemIds) {
						PizzaTemplate pizzaTemplate = new PizzaTemplate();
						pizzaTemplate.setId(id);

						Category category1 = new Category();
						category1.setId(categoryItem.getCategory().getId());

						PizzaTemplateCategory finaCategoryItem = new PizzaTemplateCategory();
						finaCategoryItem.setPizzaTemplate(pizzaTemplate);
						finaCategoryItem.setCategory(category1);
						pizzaTemplateCategoryRepository.save(finaCategoryItem);
						LOGGER.info("CategoryServiceImpl : updateCategoryItem : save CategoryItem :"); 

					}
				}
			
			
			}
		}
	}
}
