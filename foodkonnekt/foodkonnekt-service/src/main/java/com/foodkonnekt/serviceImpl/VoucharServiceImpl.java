package com.foodkonnekt.serviceImpl;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.foodkonnekt.clover.vo.MerchantVo;
import com.foodkonnekt.model.ConvenienceFee;
import com.foodkonnekt.model.Customer;
import com.foodkonnekt.model.Item;
import com.foodkonnekt.model.ItemDto;
import com.foodkonnekt.model.ItemTax;
import com.foodkonnekt.model.Koupons;
import com.foodkonnekt.model.Merchant;
import com.foodkonnekt.model.OpeningClosingDay;
import com.foodkonnekt.model.OpeningClosingTime;
import com.foodkonnekt.model.OrderDiscount;
import com.foodkonnekt.model.OrderType;
import com.foodkonnekt.model.PizzaTemplate;
import com.foodkonnekt.model.PizzaTemplateSize;
import com.foodkonnekt.model.PizzaTemplateTax;
import com.foodkonnekt.model.TaxRates;
import com.foodkonnekt.model.Vendor;
import com.foodkonnekt.model.Vouchar;
import com.foodkonnekt.repository.ConvenienceFeeRepository;
import com.foodkonnekt.repository.ItemTaxRepository;
import com.foodkonnekt.repository.ItemmRepository;
import com.foodkonnekt.repository.MerchantRepository;
import com.foodkonnekt.repository.OpeningClosingDayRepository;
import com.foodkonnekt.repository.OpeningClosingTimeRepository;
import com.foodkonnekt.repository.OrderDiscountRepository;
import com.foodkonnekt.repository.OrderTypeRepository;
import com.foodkonnekt.repository.PizzaTemplateRepository;
import com.foodkonnekt.repository.PizzaTemplateSizeRepository;
import com.foodkonnekt.repository.PizzaTemplateTaxRepository;
import com.foodkonnekt.repository.TaxRateRepository;
import com.foodkonnekt.repository.VendorRepository;
import com.foodkonnekt.repository.VoucharRepository;
import com.foodkonnekt.service.ItemService;
import com.foodkonnekt.service.VoucharService;
import com.foodkonnekt.util.CommonUtil;
import com.foodkonnekt.util.CouponUrlUtil;
import com.foodkonnekt.util.DateUtil;
import com.foodkonnekt.util.IConstant;
import com.foodkonnekt.util.UrlConstant;
import com.google.gson.Gson;

@Service
public class VoucharServiceImpl implements VoucharService {

	private static final Logger LOGGER = LoggerFactory.getLogger(VoucharServiceImpl.class);

	@Autowired
    private Environment environment;
	
	@Autowired
	private VoucharRepository voucharRepository;

	@Autowired
	private OpeningClosingDayRepository openingClosingDayRepository;

	@Autowired
	private OpeningClosingTimeRepository openingClosingTimeRepository;

	@Autowired
	private MerchantRepository merchantRepository;

	@Autowired
	private VendorRepository vendorRepository;

	@Autowired
	private TaxRateRepository taxRateRepository;

	@Autowired
	private ItemTaxRepository itemTaxRepository;

	@Autowired
	private ItemmRepository itemmRepository;

	@Autowired
	private ConvenienceFeeRepository convenienceFeeRepository;

	@Autowired
	private OrderDiscountRepository orderDiscountRepository;

	@Autowired
	private PizzaTemplateTaxRepository pizzaTemplateTaxRepository;

	@Autowired
	private OrderTypeRepository orderTypeRepository;

	@Autowired
	private ItemService itemService;

	@Autowired
	private PizzaTemplateRepository pizzaTemplateRepository;

	@Autowired
	private PizzaTemplateSizeRepository pizzaTemplateSizeRepository;

	/**
	 * Save vouchar into database
	 */
	public void save(Vouchar vouchar) {
		LOGGER.info("===============  VoucharServiceImpl : Inside save :: Start  ============= ");

		vouchar.setStartTime(CommonUtil.getStartTime(vouchar.getStartTime(), vouchar.getValidity()));
		vouchar.setEndTime(CommonUtil.getStartTime(vouchar.getEndTime(), vouchar.getValidity()));
		vouchar.setDate(DateUtil.convertStringToDate(vouchar.getStrDate()));
		vouchar.setCreatedOn(DateUtil.currentDate());
		voucharRepository.save(vouchar);
		LOGGER.info("===============  VoucharServiceImpl : Inside save :: End  ============= ");

	}

	/**
	 * Find by merchantId
	 */
	public List<Vouchar> findByMerchantId(Integer merchantId) {
		return voucharRepository.findByMerchantId(merchantId);
	}

	/**
	 * Find by Id
	 */
	public Vouchar findById(int id) {
		LOGGER.info("===============  VoucharServiceImpl : Inside findById :: Start  ============= id == " + id);

		// TODO Auto-generated method stub
		Vouchar vouchar = voucharRepository.findOne(id);
		if (vouchar.getDate() != null) {
			vouchar.setStrDate(DateUtil.getYYYYMMDD(vouchar.getDate()));
		}

		if (vouchar.getFromDate() != null) {
			vouchar.setFrmDate(DateUtil.getYYYYMMDD(vouchar.getFromDate()));
		}

		if (vouchar.getEndDate() != null) {
			vouchar.setToDate(DateUtil.getYYYYMMDD(vouchar.getEndDate()));
		}
		vouchar.setStartTime(DateUtil.convert12hoursTo24HourseFormate(vouchar.getStartTime()));
		vouchar.setEndTime(DateUtil.convert12hoursTo24HourseFormate(vouchar.getEndTime()));
		LOGGER.info("===============  VoucharServiceImpl : Inside findById :: End  ============= id == " + id);

		return vouchar;
	}

	/**
	 * Check coupon validity
	 */
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	// public String checkCouponVaidity(String itemJson,String cartJson,String
	// voucherCode, Integer merchantId) {
	public Map<String, Object> checkCouponVaidity(String itemJson, String cartJson, String voucherCode,
			Integer merchantId, String kouponCount, Map<String, Object> appliedDiscounts, double orderLeveldiscount,
			Customer customer) {
		LOGGER.info("===============  VoucharServiceImpl : Inside checkCouponVaidity :: Start  ============= ");
		LOGGER.info(" itemJson === " + itemJson + " cartJson === " + cartJson + " voucherCode " + voucherCode
				+ "merchantId === " + merchantId + " kouponCount === " + kouponCount + " orderLeveldiscount === "
				+ orderLeveldiscount);
		Map<String, Object> responseKouponMap = new HashMap<String, Object>();
		String responseKoupon = null;
		Boolean flag = false;
		int couponCount = 0;
		String responseJson = null;
		Map<String, Object> response = new HashMap<String, Object>();
		try {

			JSONObject voucherCodeJsonObject = new JSONObject(voucherCode);
			// cartJsonObject=cartJsonObject.getAsJsonObject(cartJson);
			if (voucherCodeJsonObject.has("couponCode"))
				voucherCode = voucherCodeJsonObject.getString("couponCode");

			JSONObject kouponCountJsonObject = new JSONObject(kouponCount);
			// cartJsonObject=cartJsonObject.getAsJsonObject(cartJson);
			if (kouponCountJsonObject != null && kouponCountJsonObject.has("kouponCount")) {
				kouponCount = kouponCountJsonObject.getString("kouponCount");
				if (kouponCount != null && !kouponCount.equals("") && !kouponCount.isEmpty())
					couponCount = Integer.parseInt(kouponCount);

			}

			Merchant merchant = merchantRepository.findById(merchantId);
			LOGGER.info(" MinutDifference ===== " + merchant.getTimeZone().getMinutDifference());
			LOGGER.info("Allow multiple koupon-->" + merchant.getAllowMultipleKoupon());
			Vendor vendor = null;
			if (merchant.getOwner() != null && merchant.getOwner().getId() != null) {
				vendor = vendorRepository.findOne(merchant.getOwner().getId());
			}
			SimpleDateFormat sdf1 = new SimpleDateFormat(IConstant.YYYYMMDD);
			SimpleDateFormat sdf2 = new SimpleDateFormat("HH:mm:ss");
//			Calendar calobjs = Calendar.getInstance();
//			if (merchant.getTimeZone() != null) {
//				calobjs.add(Calendar.HOUR, merchant.getTimeZone().getHourDifference());
//				calobjs.add(Calendar.MINUTE, merchant.getTimeZone().getMinutDifference());
//			}
//			Date d = calobjs.getTime();

			Date d =  (merchant != null && merchant.getTimeZone() != null
					&& merchant.getTimeZone().getTimeZoneCode() != null)
					? DateUtil.getCurrentDateForTimeZonee(merchant.getTimeZone().getTimeZoneCode())
					: new Date();
			String time = sdf2.format(d);
			String date = sdf1.format(d);
			Double subtotal = 0.0;

			JSONObject cartJsonObjects = new JSONObject(cartJson);
			if (cartJsonObjects != null && cartJsonObjects.has("cartJson")) {
				JSONObject cartJsonObject = cartJsonObjects.getJSONObject("cartJson");
				if (cartJsonObject != null && cartJsonObject.has("subtotal")) {

					subtotal = Double.parseDouble(cartJsonObject.getString("subtotal"));
				}
			}
			MerchantVo merchant2 = new MerchantVo();
			merchant2.setMerchantUid(merchant.getMerchantUid());
			// merchant2.setFutureDaysAhead(null);
			Vendor vendor2 = new Vendor();
			vendor2.setVendorUid(vendor.getVendorUid());
			Koupons koupons = new Koupons();
			koupons.setMerchant(merchant2);
			koupons.setVendor(vendor2);
			koupons.setName(voucherCode);
			koupons.getMerchant().setMerchantUid(merchant.getMerchantUid());
			koupons.getVendor().setVendorUid(vendor.getVendorUid());
			koupons.setCurrentTime(time);
			koupons.setCurrentDate(date);
			koupons.setMinimumOrderAmount(subtotal);
			String url = environment.getProperty("KOUPONS_BASE_URL") + "validateKoupon";
			// String url = "http://52.90.190.122:8080/koupons/" + "validateKoupon";
			Gson gson = new Gson();
			String voucherJson = gson.toJson(koupons);
			LOGGER.info("voucherJson-" + voucherJson);
			responseKoupon = CouponUrlUtil.getCouponData(url, voucherJson);
			// System.out.println("################## " + responseKoupon);
			JSONObject jsonObj = new JSONObject(responseKoupon);
			if (jsonObj.has("response") && jsonObj.has("DATA")) {
				String responseCode = jsonObj.getString("response");

				String eventName = null;
				Boolean isDeliveryFeeKoupon = false;

				boolean isUseabilitySingle = false;
				boolean canApplyCoupon = true;
				JSONObject responseDataObj = null;
				try {
					responseDataObj = jsonObj.getJSONObject("DATA");
					// System.out.println("responseDataObj-" + responseDataObj);
				} catch (Exception e) {
					responseDataObj = new JSONObject("{\"DATA\":\"" + jsonObj.getString("DATA") + "\"}");
				}

				if (responseCode.equals(IConstant.RESPONSE_SUCCESS_MESSAGE) && responseDataObj != null
						&& responseDataObj.toString().contains("isUsabilitySingle")) {
					String couponUID = null;
					if (!responseDataObj.get("kouponCode").equals(null)
							&& !responseDataObj.get("kouponCode").equals("")) {
						couponUID = (String) responseDataObj.get("kouponCode");
					}
					isUseabilitySingle = responseDataObj.getBoolean("isUsabilitySingle");
					LOGGER.info(" couponUID === " + couponUID);
					if (isUseabilitySingle && customer != null && customer.getId() != null && couponUID != null) {
						List<OrderDiscount> orderDiscounts = orderDiscountRepository
								.findByCustomerIdAndCouponCode(customer.getId(), couponUID);
						if (orderDiscounts != null && !orderDiscounts.isEmpty() && orderDiscounts.size() > 0) {
							canApplyCoupon = false;
						}
					}
				}
				if (responseCode.equals(IConstant.RESPONSE_SUCCESS_MESSAGE) && canApplyCoupon) {

					if (responseDataObj != null && responseDataObj.toString().contains("eventCategory")) {
						JSONObject eventCategoryObject = responseDataObj.getJSONObject("eventCategory");
						if (eventCategoryObject != null && eventCategoryObject.has("eventName")) {
							eventName = eventCategoryObject.getString("eventName");
							LOGGER.info("eventName === " + eventName);
							if (eventName.equals("freeDeliveryFee")) {
								List<Map<String, Object>> discountMapList = new ArrayList<Map<String, Object>>();
								isDeliveryFeeKoupon = true;
								response.put("DATA", isDeliveryFeeKoupon);
								response.put("responsCode", "300");
								Map<String, Object> discountMap = new HashMap<String, Object>();
								discountMap.put("voucherCode", voucherCode);
								discountMap.put("discount", "0");
								discountMap.put("inventoryLevel", "item");
								discountMap.put("discountType", "amount");
								discountMap.put("couponUID", (String) responseDataObj.get("kouponCode"));
								discountMap.put("isUsabilitySingle", responseDataObj.getBoolean("isUsabilitySingle"));
								discountMapList.add(discountMap);
								response.put("discountList", discountMapList);
								// responseJson = gson.toJson(response);
								return response;
								// return responseJson;
							} else {
								// responseKoupon= applyCoupon(responseKoupon,itemJson,cartJson,merchant,
								// voucherCode);
								/*
								 * if(appliedDiscounts == null || appliedDiscounts.isEmpty() ||
								 * appliedDiscounts.equals("") ) { appliedDiscounts = null; }
								 */
								/*
								 * double orderLeveldiscount=0.0; if(couponCount > 0){
								 * 
								 * Map<String,Object> appliedData= new HashMap<String, Object>();
								 * List<Map<String,Object>> discountMapList = new
								 * ArrayList<Map<String,Object>>(); appliedData =
								 * (Map<String,Object>)appliedDiscounts.get("DATA"); discountMapList =
								 * (List<Map<String,Object>>)appliedData.get("discountList");
								 * for(Map<String,Object> discountMap : discountMapList){ String vCode =
								 * (String)discountMap.get("voucherCode"); String inventoryLevel =
								 * (String)discountMap.get("inventoryLevel"); double couponDiscount =
								 * (Double)discountMap.get("discount"); if(inventoryLevel!=null &&
								 * inventoryLevel.equals("order")){
								 * orderLeveldiscount=orderLeveldiscount+couponDiscount; }
								 * 
								 * }}
								 */
								responseKouponMap = applyCoupon(responseKoupon, itemJson, cartJson, merchant,
										voucherCode, couponCount, appliedDiscounts, orderLeveldiscount);
								responseKouponMap.put("allowMultipleKoupon", merchant.getAllowMultipleKoupon());
							}
						}
					}

				} else if (!responseCode.equals(IConstant.RESPONSE_SUCCESS_MESSAGE)) {
					if (responseCode.equals(IConstant.RESPONSE_NOT_EXIST_KOUPON)) {
						// response.put("DATA", IConstant.MESSAGE_NOT_EXIST_KOUPON);
						response.put("DATA", appliedDiscounts.get("DATA"));
						response.put("responsMessage", IConstant.MESSAGE_NOT_EXIST_KOUPON);
						response.put("responsCode", IConstant.RESPONSE_INVALID_KOUPON);
						responseJson = gson.toJson(response);
						flag = true;
					} else if (responseCode.equals(IConstant.RESPONSE_EXPIRED_KOUPON)) {
						LOGGER.info("in side else if responseCode---" + responseCode);
						// response.put("DATA", IConstant.MESSAGE_EXPIRED_KOUPON);
						response.put("DATA", appliedDiscounts.get("DATA"));
						response.put("responsMessage", IConstant.MESSAGE_EXPIRED_KOUPON);
						response.put("responsCode", IConstant.RESPONSE_INVALID_KOUPON);
						responseJson = gson.toJson(response);
						flag = true;
					} else if (responseCode.equals(IConstant.RESPONSE_INVALID_KOUPON)) {
						// response.put("DATA", IConstant.MESSAGE_INVALID_KOUPON);
						response.put("DATA", appliedDiscounts.get("DATA"));
						String message = jsonObj.getString("DATA");
						if (message.contains("Minimum"))
							response.put("responsMessage", message);
						else
							response.put("responsMessage", IConstant.MESSAGE_INVALID_KOUPON);
						response.put("responsCode", IConstant.RESPONSE_INVALID_KOUPON);
						responseJson = gson.toJson(response);
						flag = true;
					} else if (responseCode.equals(IConstant.RESPONSE_RECURRING_KOUPON)) {
						// response.put("DATA", IConstant.MESSAGE_RECURRING_KOUPON);
						response.put("DATA", appliedDiscounts.get("DATA"));
						response.put("responsMessage", IConstant.MESSAGE_RECURRING_KOUPON);
						response.put("responsCode", IConstant.RESPONSE_INVALID_KOUPON);
						responseJson = gson.toJson(response);
						flag = true;
					}
				} else if (!canApplyCoupon) {
					if (appliedDiscounts != null)
						response.put("DATA", appliedDiscounts.get("DATA"));
					response.put("responsMessage", IConstant.MESSAGE_ALREADY_USED_KOUPON);
					response.put("responsCode", IConstant.RESPONSE_INVALID_KOUPON);
					responseJson = gson.toJson(response);
					flag = true;
				}

			} else {
				response.put("DATA", appliedDiscounts.get("DATA"));
				response.put("responsMessage", IConstant.MESSAGE_INVALID_KOUPON);
				response.put("responsCode", IConstant.RESPONSE_INVALID_KOUPON);
				responseJson = gson.toJson(response);
				flag = true;
			}
			LOGGER.info("responseJson === " + responseJson);
		} catch (Exception e) {
			LOGGER.error(
					"===============  VoucharServiceImpl : Inside checkCouponVaidity :: Exception  ============= " + e);

			LOGGER.error("error: " + e.getMessage());
		}
		LOGGER.info("flag === " + flag);
		if (flag) {
			LOGGER.info("===============  VoucharServiceImpl : Inside checkCouponVaidity :: End  ============= ");

			return response;
		} else {
			LOGGER.info("===============  VoucharServiceImpl : Inside checkCouponVaidity :: End  ============= ");

			return responseKouponMap;
		}
	}

	// String applyCoupon(String couponResponse,String itemJson,String
	// cartJson,Merchant merchant,String voucherCode){
	Map<String, Object> applyCoupon(String responseKoupon, String itemJson, String cartJson, Merchant merchant,
			String voucherCode, int kouponCount, Map<String, Object> appliedDiscounts, double orderLeveldiscount) {
		LOGGER.info("===============  VoucharServiceImpl : Inside applyCoupon :: Start  ============= ");
		LOGGER.info("responseKoupon === " + responseKoupon + " itemJson==== " + itemJson + " cartJson==== " + cartJson
				+ " merchant " + merchant.getId() + " voucherCode == " + voucherCode + " kouponCount == " + kouponCount
				+ " orderLeveldiscount === " + orderLeveldiscount);

		String discountType = null;
		String eventName = null;
		Integer eventId = null;
		Double discount = null;
		String personalLevel = null;
		String voucherJson = null;
		String limit = null;
		String couponUID = null;
		Boolean isUsabilitySingle = false;
		Boolean isLeastPrice = false;
		String inventoryLevel = "order";
		Map<String, Object> voucherResponse = new HashMap<String, Object>();
		// Boolean isDeliveryFeeKoupon = false;

		JSONObject jsonObj = new JSONObject(responseKoupon);
		String responseCode = jsonObj.getString("response");

		// String responseData= jsonObj.getString("DATA");
		JSONObject responseDataObj = jsonObj.getJSONObject("DATA");
		// System.out.println("responseDataObj-"+responseDataObj);
		LOGGER.info("=== responseCode ====" + responseCode);
		if (responseCode.equals(IConstant.RESPONSE_SUCCESS_MESSAGE)) {
			if (responseDataObj != null && responseDataObj.toString().contains("invenotryLevel")) {
				JSONObject invenotryLevelObject = responseDataObj.getJSONObject("invenotryLevel");

				JSONObject eventCategoryObject = responseDataObj.getJSONObject("eventCategory");
				JSONObject personalLevelObject = responseDataObj.getJSONObject("personalLevel");

				if (eventCategoryObject != null && eventCategoryObject.has("eventName")) {
					eventName = eventCategoryObject.getString("eventName");
					eventId = eventCategoryObject.getInt("id");
					LOGGER.info("====eventName====" + eventName);
					if (!eventName.equals("buyOneGetOne")) {
						JSONObject discountLevelObject = responseDataObj.getJSONObject("discountLevel");
						if (discountLevelObject != null && discountLevelObject.has("discountType")) {
							discountType = discountLevelObject.getString("discountType");
						}
					}
				}
				if (responseDataObj != null && responseDataObj.has("discount")) {
					discount = responseDataObj.getDouble("discount");
				}
				if (responseDataObj != null && responseDataObj.has("limit")) {
					limit = responseDataObj.getString("limit");
					LOGGER.info("--===limit=== " + limit);
				}
				if (responseDataObj != null && responseDataObj.has("isLeastPrice")) {
					isLeastPrice = responseDataObj.getBoolean("isLeastPrice");
					LOGGER.info("--====isLeastPrice=== " + isLeastPrice);
				}
				if (personalLevelObject != null && personalLevelObject.has("name")) {
					personalLevel = personalLevelObject.getString("name");
				}
				if (!responseDataObj.get("kouponCode").equals(null) && !responseDataObj.get("kouponCode").equals("")) {
					couponUID = (String) responseDataObj.get("kouponCode");
				}
				if (!responseDataObj.get("isUsabilitySingle").equals(null)
						&& !responseDataObj.get("isUsabilitySingle").equals("")) {
					isUsabilitySingle = responseDataObj.getBoolean("isUsabilitySingle");
				}
				LOGGER.info("=====DISCOUNT COUPON UNIQUE ID::::" + couponUID);
				LOGGER.info("=====isUsabilitySingle-->" + isUsabilitySingle);
				LOGGER.info("=====invenotryLevelObject" + invenotryLevelObject.toString());
				if (invenotryLevelObject.getInt("id") == 1
						&& invenotryLevelObject.getString("invenotryName").equals("item")) {
					inventoryLevel = "item";
					if (responseDataObj.toString().contains("kouponItem")) {
						JSONArray kouponItemArray = responseDataObj.getJSONArray("kouponItem");
						LOGGER.info("kouponItemArray" + kouponItemArray.toString());
						for (int i = 0; i < kouponItemArray.length(); i++) {
							JSONObject kouponItemObject = kouponItemArray.getJSONObject(i);
							LOGGER.info("kouponItemObject-" + kouponItemObject.toString());
							if (kouponItemObject.getInt("itemId") != 0
									&& kouponItemObject.getString("itemName") != null) {
								LOGGER.info("==itemName ===== " + kouponItemObject.getString("itemName") + "------"
										+ " ===== ItemId =====" + kouponItemObject.getInt("itemId"));
								int itemId = kouponItemObject.getInt("itemId");
								String itemName = kouponItemObject.getString("itemName");
								Item item = itemmRepository.findOne(itemId);

								if (item != null && item.getName().equals(itemName)
										&& ((item.getPosItemId() != null && itemJson.contains(item.getPosItemId()))
												|| (item.getId() != null
														&& itemJson.contains(item.getId().toString())))) {
									LOGGER.info("item exists ==== " + item.getPosItemId());
									List<String> itemArrayList = new ArrayList<String>();
									List<String> freeItemArrayList = new ArrayList<String>();

									/*
									 * if (isUsabilitySingle == true) { limit = "single"; } else { limit =
									 * "multiple"; }
									 */
									if (eventId != null && eventId == 8 && limit != null && !limit.isEmpty()) {

										int freeItemId = kouponItemObject.getInt("freeItemId");
										String freeItemName = kouponItemObject.getString("freeItemName");
										Item freeItem = itemmRepository.findOne(freeItemId);
										LOGGER.info(" === DB ItemName ==== " + item.getName());
										if (freeItem.getName().equals(freeItemName)) {
											/*
											 * if(limit.equals("single")) { double freeItemPrice=freeItem.getPrice();
											 * if(discount>freeItemPrice){ discount=freeItemPrice ; } }else
											 * if(limit.equals("multiple")){
											 * 
											 * }
											 */
											if (merchant != null && merchant.getOwner() != null
													&& merchant.getOwner().getPos() != null
													&& merchant.getOwner().getPos().getPosId() != null
													&& merchant.getOwner().getPos().getPosId() == IConstant.NON_POS) {
												itemArrayList.add(item.getId().toString());
												freeItemArrayList.add(freeItem.getId().toString());
											} else {
												itemArrayList.add(item.getPosItemId());
												freeItemArrayList.add(freeItem.getPosItemId());
											}

										}
									} else {
										if (merchant != null && merchant.getOwner() != null
												&& merchant.getOwner().getPos() != null
												&& merchant.getOwner().getPos().getPosId() != null
												&& merchant.getOwner().getPos().getPosId() == IConstant.NON_POS) {
											itemArrayList.add(item.getId().toString());
										} else {
											itemArrayList.add(item.getPosItemId());
										}
									}

									// voucherJson=
									// calculateDiscount(itemJson,cartJson,merchant,discountType,eventName,personalLevel,inventoryLevel,discount,
									// voucherCode,itemArrayList, limit, isLeastPrice);
									voucherResponse = calculateDiscount(itemJson, cartJson, merchant, discountType,
											eventName, eventId, personalLevel, inventoryLevel, discount, voucherCode,
											itemArrayList, limit, isLeastPrice, kouponCount, appliedDiscounts,
											freeItemArrayList, orderLeveldiscount, couponUID, isUsabilitySingle);
								}
							} else {
								// return "Invalid Koupon";
								// voucherResponse.put("DATA", "Invalid Koupon");
								voucherResponse.put("DATA", appliedDiscounts.get("DATA"));
								voucherResponse.put("responsMessage", "Invalid Koupon");
								LOGGER.info(
										"===============  VoucharServiceImpl : Inside applyCoupon :: End  ============= ");

								return voucherResponse;
							}
						}
					}
				} else if (invenotryLevelObject.getInt("id") == 2
						&& invenotryLevelObject.getString("invenotryName").equals("category")) {
					inventoryLevel = "category";
					if (responseDataObj.toString().contains("kouponCategory")) {
						JSONArray kouponCategoryArray = responseDataObj.getJSONArray("kouponCategory");
						for (int i = 0; i < kouponCategoryArray.length(); i++) {
							JSONObject kouponCategoryObject = kouponCategoryArray.getJSONObject(i);
							LOGGER.info("=====kouponCategoryObject===== " + kouponCategoryObject.toString());
							if (kouponCategoryObject.getInt("categoryId") != 0
									&& kouponCategoryObject.getString("categoryName") != null) {
								int categoryId = kouponCategoryObject.getInt("categoryId");
								LOGGER.info(" ==== categoryId ===== " + categoryId);
								List<Item> categoryItemList = null;
								List<Item> freeCategoryItemList = null;
								List<String> freeItemArrayList = null;
								if (eventId != null && eventId == 8) {
									int freeCategoryId = kouponCategoryObject.getInt("freeCategoryId");
									String freeCategoryName = kouponCategoryObject.getString("freeCategoryName");
									freeCategoryItemList = itemmRepository.findByCategoriesId(freeCategoryId);
									if (freeCategoryItemList != null && !freeCategoryItemList.isEmpty()) {
										LOGGER.info("for all items of category====  " + freeCategoryItemList.get(0));
										freeItemArrayList = new ArrayList<String>();

										for (Item item : freeCategoryItemList) {
											if (merchant != null && merchant.getOwner() != null
													&& merchant.getOwner().getPos() != null
													&& merchant.getOwner().getPos().getPosId() != null
													&& merchant.getOwner().getPos().getPosId() == IConstant.NON_POS) {
												freeItemArrayList.add(item.getId().toString());
											} else {
												freeItemArrayList.add(item.getPosItemId());
											}
										}
										LOGGER.info(" ===== discpount ===== " + discount + " ===== discountType ===== "
												+ discountType);
										// voucherJson=
										// calculateDiscount(itemJson,cartJson,merchant,discountType,eventName,personalLevel,inventoryLevel,discount,
										// voucherCode,itemArrayList, limit, isLeastPrice);
									}
									categoryItemList = itemmRepository.findByCategoriesId(freeCategoryId);
								} else {
									categoryItemList = itemmRepository.findByCategoriesId(categoryId);
								}
								// int categoryName=kouponCategoryObject.getInt("categoryName");

								if (categoryItemList != null && !categoryItemList.isEmpty()) {
									LOGGER.info(" =====for all items of category === " + categoryItemList.get(0));
									List<String> itemArrayList = new ArrayList<String>();
									for (Item item : categoryItemList) {
										if (merchant != null && merchant.getOwner() != null
												&& merchant.getOwner().getPos() != null
												&& merchant.getOwner().getPos().getPosId() != null
												&& merchant.getOwner().getPos().getPosId() == IConstant.NON_POS) {
											itemArrayList.add(item.getId().toString());
										} else {
											itemArrayList.add(item.getPosItemId());
										}
									}
									LOGGER.info(" ===== discpount ==== " + discount + " =====discountType ====== "
											+ discountType);
									// voucherJson=
									// calculateDiscount(itemJson,cartJson,merchant,discountType,eventName,personalLevel,inventoryLevel,discount,
									// voucherCode,itemArrayList, limit, isLeastPrice);
									voucherResponse = calculateDiscount(itemJson, cartJson, merchant, discountType,
											eventName, eventId, personalLevel, inventoryLevel, discount, voucherCode,
											itemArrayList, limit, isLeastPrice, kouponCount, appliedDiscounts,
											freeItemArrayList, orderLeveldiscount, couponUID, isUsabilitySingle);
								}

							} else {
								// return "Invalid Koupon";
								// voucherResponse.put("DATA", "Invalid Koupon");
								voucherResponse.put("DATA", appliedDiscounts.get("DATA"));
								voucherResponse.put("responsMessage", "Invalid Koupon");
								LOGGER.info(
										"===============  VoucharServiceImpl : Inside applyCoupon :: End  ============= ");

								return voucherResponse;
							}
						}

					}
				} else if (invenotryLevelObject.getInt("id") == 3
						&& invenotryLevelObject.getString("invenotryName").equals("order")) {
					inventoryLevel = "order";
					// voucherJson=
					// calculateDiscount(itemJson,cartJson,merchant,discountType,eventName,personalLevel,inventoryLevel,discount,
					// voucherCode,null, limit, isLeastPrice);
					// voucherJson=
					// calculateDiscount(itemJson,cartJson,merchant,discountType,eventName,personalLevel,inventoryLevel,discount,
					// voucherCode,null, limit, isLeastPrice);
					voucherResponse = calculateDiscount(itemJson, cartJson, merchant, discountType, eventName, eventId,
							personalLevel, inventoryLevel, discount, voucherCode, null, limit, isLeastPrice,
							kouponCount, appliedDiscounts, null, orderLeveldiscount, couponUID, isUsabilitySingle);
				}
			}

		}
		LOGGER.info("===============  VoucharServiceImpl : Inside applyCoupon :: End  ============= ");

		// return voucherJson;
		return voucherResponse;
	}

	Map<String, Object> calculateDiscount(String itemJson, String cartJson, Merchant merchant, String discountType,
			String eventName, Integer eventId, String personalLevel, String inventoryLevel, Double discount,
			String voucherCode, List<String> itemArrayList, String limit, Boolean isLeastPrice, int kouponCount,
			Map<String, Object> appliedDiscounts, List<String> freeItemArrayList, double orderLeveldiscount,
			String couponUID, Boolean isUsabilitySingle) {
		LOGGER.info("===============  VoucharServiceImpl : Inside calculateDiscount :: Start  ============= ");
		LOGGER.info(" ===itemJson  " + itemJson + " cartJson " + cartJson + " merchant " + merchant.getId()
				+ " discountType " + discountType + " eventName " + eventName + " eventId " + eventId
				+ "  personalLevel " + personalLevel + " inventoryLevel " + inventoryLevel + " discount " + discount
				+ " voucherCode " + voucherCode + " limit " + limit + " isLeastPrice " + isLeastPrice + " kouponCount "
				+ kouponCount + " orderLeveldiscount " + orderLeveldiscount + " couponUID " + couponUID
				+ " isUsabilitySingle " + isUsabilitySingle);
		ArrayList<ItemDto> items = new ArrayList<ItemDto>();
		Map<String, ArrayList<ItemDto>> itemTaxes = new HashMap<String, ArrayList<ItemDto>>();
		Map<String, Object> response = new HashMap<String, Object>();
		Gson gson = new Gson();
		String voucherJson = "";

		Double orderPrice = null;
		double couponDiscount = 0.0;
		if (discount != null)
			couponDiscount = discount;

		ConvenienceFee convenienceFeeObject = null;
		Double tipAmount = 0.0;
		Double tax = null;
		Double convFee = null;
		Double subtotal = null;
		Double deliveryFee = null;
		Boolean deliveryTaxable = false;
		String deliveryPosId = "";
		double discountedSubTotal = 0;
		double oldDiscountedSubTotal = 0;
		double discountedTotal = 0;
		double discountedTax = 0;
		boolean discountStatus = false;
		Double categoryItemPrice = 0.0;
		Double orderDiscount = 0.0;
		Double totalOrderDiscount = 0.0;
		double auxTax = 0.0;
		// Integer totalQuantity =0;
		Double minPriceAmount = 0.0;
		int freeItemQty = 0;
		Double minOrderPriceAmount = 0.0;
		ItemDto convenienceFeeItem = null;
		boolean isConvenienceFeeTaxable = false;
		List<Map<String, Object>> discountMapList = new ArrayList<Map<String, Object>>();
		int numberOfItemsForDiscountCount = 0;
		int appliedDiscountOnItemCount = 0;
		Double finalDiscount = 0.0;
		double auxTaxCount = 0.0;
		double itemTaxCount = 0.0;
		double orderSubtotal = 0.0;
		Double ConvenienceFeeAfterDiscount = null;
		if (merchant != null) {

			if (kouponCount > 0) {
				LOGGER.info(" === inside if (KouponCount > 0 ) ");
				if (appliedDiscounts.containsKey("DATA") && appliedDiscounts.get("DATA") != null
						&& appliedDiscounts.containsKey("responsCode") && appliedDiscounts.get("responsCode") != null) {
					String responsCode = (String) appliedDiscounts.get("responsCode");
					/* if(responsCode.equals(IConstant.RESPONSE_SUCCESS_MESSAGE)){ */
					Map<String, Object> data = (Map<String, Object>) appliedDiscounts.get("DATA");
					if (data.containsKey("items") && data.get("items") != null)
						items = (ArrayList<ItemDto>) data.get("items");

					if (data.containsKey("discount") && data.get("discount") != null)
						totalOrderDiscount = (Double) data.get("discount");

					if (data.containsKey("subtotal") && data.get("subtotal") != null)
						oldDiscountedSubTotal = (Double) data.get("subtotal");

					subtotal = oldDiscountedSubTotal;

					if (orderLeveldiscount > 0) {
						oldDiscountedSubTotal = 0.0;
						for (ItemDto dto : items) {
							dto.setPrice((dto.getOriginalPrice() + dto.getItemModifierPrice()) * dto.getQunatity()
									- dto.getDiscount());
							oldDiscountedSubTotal = oldDiscountedSubTotal + dto.getPrice();

						}
						totalOrderDiscount = totalOrderDiscount - orderLeveldiscount;
					}

					if (data.containsKey("discountList") && data.get("discountList") != null)
						discountMapList = (List<Map<String, Object>>) data.get("discountList");

					if (data.containsKey("isConvenienceFeeTaxable") && data.get("isConvenienceFeeTaxable") != null)
						isConvenienceFeeTaxable = (Boolean) data.get("isConvenienceFeeTaxable");

					if (data.containsKey("convenienceFeeItem") && data.get("convenienceFeeItem") != null)
						convenienceFeeItem = (ItemDto) data.get("convenienceFeeItem");

					/* } */

				} else {

					JSONObject cartJsonObjects = new JSONObject(cartJson);
					if (cartJsonObjects != null && cartJsonObjects.has("cartJson")) {
						JSONObject cartJsonObject = cartJsonObjects.getJSONObject("cartJson");
						if (cartJsonObject != null && cartJsonObject.has("tax") && cartJsonObject.has("subtotal")
								&& cartJsonObject.has("total")) {
							orderPrice = Double.parseDouble(cartJsonObject.getString("total"));
							tax = Double.parseDouble(cartJsonObject.getString("tax"));
							subtotal = Double.parseDouble(cartJsonObject.getString("subtotal"));
							orderSubtotal = Double.parseDouble(cartJsonObject.getString("subtotal"));
							if (cartJsonObject.has("convFee"))
								convFee = cartJsonObject.getDouble("convFee");
							if (cartJsonObject.has("deliveryFee"))
								deliveryFee = cartJsonObject.getDouble("deliveryFee");
							if (cartJsonObject.has("tipAmount"))
								tipAmount = cartJsonObject.getDouble("tipAmount");
							if (cartJsonObject.has("deliveryPosId"))
								deliveryPosId = cartJsonObject.getString("deliveryPosId");
							if (cartJsonObject.has("deliveryTaxable"))
								deliveryTaxable = cartJsonObject.getBoolean("deliveryTaxable");
						}
					} else {
						LOGGER.info(
								"===============  VoucharServiceImpl : Inside calculateDiscount :: End  ============= ");

						return null;
					}

					JSONObject itemJsonObjects = new JSONObject(itemJson);
					JSONArray jsonObj = itemJsonObjects.getJSONArray("orderJson");
					LOGGER.info("======= Order ==== " + jsonObj.toString());
					if (jsonObj != null && jsonObj.length() > 0) {
						for (int i = 0; i < jsonObj.length(); i++) {
							JSONObject itemJsonObject = jsonObj.getJSONObject(i);
							if (itemJsonObject != null) {
								if (itemJsonObject.has("itemid") && itemJsonObject.has("amount")
										&& itemJsonObject.has("price")) {
									ItemDto item = new ItemDto();
									String itemPosId = itemJsonObject.getString("itemid");
									item.setItemPosId(itemPosId);
									item.setQunatity(Integer.parseInt(itemJsonObject.getString("amount")));
									item.setTotalQunatity(item.getQunatity());
									for (ItemDto dto : items) {
										if (dto.getItemPosId().equals(itemPosId)) {
											LOGGER.info("====== inside duplicate check ==== ");
											item.setTotalQunatity(item.getQunatity() + dto.getQunatity());
											// totalQuantity = item.getTotalQunatity();
											dto.setTotalQunatity(item.getQunatity() + dto.getQunatity());
										}
									}
									Double itemPrice = Double.parseDouble(itemJsonObject.getString("price"));
									Double modifierPrice = 0.0;
									if (itemJsonObject.has("modifier")) {
										JSONArray modifierJsonArray = itemJsonObject.getJSONArray("modifier");
										if (modifierJsonArray != null && modifierJsonArray.length() > 0) {

											for (int j = 0; j < modifierJsonArray.length(); j++) {
												JSONObject modifierJsonObject = modifierJsonArray.getJSONObject(j);
												modifierPrice = modifierPrice + modifierJsonObject.getDouble("price");
											}
										}
									}

									LOGGER.info("====== item Quantity-" + item.getQunatity());
									LOGGER.info("====== item total Quantity-" + item.getTotalQunatity());

									item.setPrice(itemPrice);
									item.setOriginalPrice(itemPrice);
									item.setItemModifierPrice(modifierPrice);

									/*
									 * if(limit!=null) { System.out.println("in limit not null");
									 * if(limit.equals("single")) { item.setPrice(itemPrice);
									 * item.setItemModifierPrice(modifierPrice); }else if(limit.equals("multiple"))
									 * { item.setPrice(itemPrice); item.setItemModifierPrice(modifierPrice); } }
									 *//*
										 * else{ System.out.println("in limit null");
										 * item.setPrice(itemPrice+modifierPrice); }
										 */
									items.add(item);
								}
							}
						}
					}

					ConvenienceFee convenienceFee = null;
					List<ConvenienceFee> convenienceFees = convenienceFeeRepository.findByMerchantId(merchant.getId());
					LOGGER.info(" ====== convenienceFees.size() ==== " + convenienceFees.size());
					if (convenienceFees != null && convenienceFees.size() > 0) {
						int index = convenienceFees.size() - 1;
						convenienceFee = convenienceFees.get(index);
                        convenienceFeeObject = convenienceFees.get(index);
					}
					LOGGER.info(" ===convenienceFee.getConvenienceFee() ===== " + convenienceFee.getConvenienceFee());
					if (convenienceFee != null) {
						if (convenienceFee.getConvenienceFee() != null
								&& Double.valueOf(convenienceFee.getConvenienceFee()) > 0) {

							convenienceFeeItem = new ItemDto();
							convenienceFeeItem.setItemPosId(convenienceFee.getConvenienceFeeLineItemPosId());
							convenienceFeeItem.setQunatity(1);
							convenienceFeeItem.setPrice(Double.valueOf(convenienceFee.getConvenienceFee()));
							convenienceFeeItem.setOriginalPrice(Double.valueOf(convenienceFee.getConvenienceFee()));

							subtotal = subtotal + Double.valueOf(convenienceFee.getConvenienceFee());
//							double ConvenienceFeePercent =  (convenienceFee.getConvenienceFeePercent()!=null) ? convenienceFee.getConvenienceFeePercent() : 0;
//							double mapConvenienceFee = Double.valueOf(convenienceFee.getConvenienceFee()) + (subtotal*(ConvenienceFeePercent/100.0));
//							subtotal = subtotal + mapConvenienceFee;
							
							items.add(convenienceFeeItem);
							LOGGER.info(" ===convenienceFee.getIsTaxable() ===== " + convenienceFee.getIsTaxable());

							if (convenienceFee.getIsTaxable() != null) {
								if (convenienceFee.getIsTaxable() == 1) {
									isConvenienceFeeTaxable = true;

								} else {
									isConvenienceFeeTaxable = false;
								}
							} else {
								isConvenienceFeeTaxable = false;
							}
						}
					}

				}

			} else {
				JSONObject cartJsonObjects = new JSONObject(cartJson);
				if (cartJsonObjects != null && cartJsonObjects.has("cartJson")) {
					JSONObject cartJsonObject = cartJsonObjects.getJSONObject("cartJson");
					if (cartJsonObject != null && cartJsonObject.has("tax") && cartJsonObject.has("subtotal")
							&& cartJsonObject.has("total")) {
						// orderPrice=Double.parseDouble(cartJsonObject.getString("total"));
						orderPrice = cartJsonObject.getDouble("total");

						tax = Double.parseDouble(cartJsonObject.getString("tax"));
						subtotal = Double.parseDouble(cartJsonObject.getString("subtotal"));
						orderSubtotal = Double.parseDouble(cartJsonObject.getString("subtotal"));
						if (cartJsonObject.has("convFee"))
							convFee = cartJsonObject.getDouble("convFee");
						if (cartJsonObject.has("deliveryFee"))
							deliveryFee = cartJsonObject.getDouble("deliveryFee");
						if (cartJsonObject.has("tipAmount"))
							tipAmount = cartJsonObject.getDouble("tipAmount");
						if (cartJsonObject.has("deliveryPosId"))
							deliveryPosId = cartJsonObject.getString("deliveryPosId");
						if (cartJsonObject.has("deliveryTaxable"))
							deliveryTaxable = cartJsonObject.getBoolean("deliveryTaxable");
					}
				} else {
					LOGGER.info(
							"===============  VoucharServiceImpl : Inside calculateDiscount :: End  ============= ");

					return null;
				}

				JSONObject itemJsonObjects = new JSONObject(itemJson);
				JSONArray jsonObj = itemJsonObjects.getJSONArray("orderJson");
				LOGGER.info("======  Order ===== " + jsonObj.toString());
				if (jsonObj != null && jsonObj.length() > 0) {
					for (int i = 0; i < jsonObj.length(); i++) {
						JSONObject itemJsonObject = jsonObj.getJSONObject(i);
						if (itemJsonObject != null) {
							if (itemJsonObject.has("itemid") && itemJsonObject.has("amount")
									&& itemJsonObject.has("price")) {
								ItemDto item = new ItemDto();
								String itemPosId = itemJsonObject.getString("itemid");
								Boolean isPizza = itemJsonObject.getBoolean("isPizza");
								;
								if (itemArrayList != null && !itemArrayList.isEmpty()
										&& itemArrayList.contains(itemPosId)) {
									numberOfItemsForDiscountCount += Integer
											.parseInt(itemJsonObject.getString("amount"));
								}
								item.setItemPosId(itemPosId);
								item.setIsPizza(isPizza);
								item.setQunatity(Integer.parseInt(itemJsonObject.getString("amount")));
								item.setTotalQunatity(item.getQunatity());
								for (ItemDto dto : items) {
									if (dto.getItemPosId().equals(itemPosId)) {
										LOGGER.info("====== inside duplicate check === ");
										item.setTotalQunatity(item.getQunatity() + dto.getQunatity());
										// totalQuantity = item.getTotalQunatity();
										dto.setTotalQunatity(item.getQunatity() + dto.getQunatity());
									}
								}
								// Double itemPrice=Double.parseDouble(itemJsonObject.getString("price"));
								Double itemPrice = itemJsonObject.getDouble("price");
								Double modifierPrice = 0.0;
								if (itemJsonObject.has("modifier")) {
									JSONArray modifierJsonArray = itemJsonObject.getJSONArray("modifier");
									if (modifierJsonArray != null && modifierJsonArray.length() > 0) {

										for (int j = 0; j < modifierJsonArray.length(); j++) {
											JSONObject modifierJsonObject = modifierJsonArray.getJSONObject(j);
											modifierPrice = modifierPrice + modifierJsonObject.getDouble("price");
										}
									}
								}

								LOGGER.info("====== item Quantity-" + item.getQunatity());
								LOGGER.info("====== item total Quantity-" + item.getTotalQunatity());

								item.setPrice(itemPrice);
								item.setOriginalPrice(itemPrice);
								item.setItemModifierPrice(modifierPrice);

								/*
								 * if(limit!=null) { System.out.println("in limit not null");
								 * if(limit.equals("single")) { item.setPrice(itemPrice);
								 * item.setItemModifierPrice(modifierPrice); }else if(limit.equals("multiple"))
								 * { item.setPrice(itemPrice); item.setItemModifierPrice(modifierPrice); } }
								 *//*
									 * else{ System.out.println("in limit null");
									 * item.setPrice(itemPrice+modifierPrice); }
									 */
								// item.setPrice(itemPrice+modifierPrice);
								items.add(item);
							}
						}
					}
				}
				LOGGER.info(" ===== deliveryFee === " + deliveryFee);
				if (deliveryFee != null && deliveryFee > 0) {
					ItemDto deliveryItemDto = new ItemDto();
					deliveryItemDto.setItemPosId(deliveryPosId);
					deliveryItemDto.setOriginalPrice(deliveryFee);
					deliveryItemDto.setPrice(deliveryFee);
					deliveryItemDto.setQunatity(1);
					subtotal += deliveryFee;
					items.add(deliveryItemDto);
				}

				ConvenienceFee convenienceFee = null;
				List<ConvenienceFee> convenienceFees = convenienceFeeRepository.findByMerchantId(merchant.getId());
				LOGGER.info(" ===== convenienceFees.size() === " + convenienceFees.size());
				if (convenienceFees != null && convenienceFees.size() > 0) {
					int index = convenienceFees.size() - 1;
					convenienceFee = convenienceFees.get(index);
					convenienceFeeObject = convenienceFees.get(index);
				}
				if (convenienceFee != null) {
					if (convenienceFee.getConvenienceFee() != null
							&& Double.valueOf(convenienceFee.getConvenienceFee()) > 0) {

						convenienceFeeItem = new ItemDto();
						convenienceFeeItem.setItemPosId(convenienceFee.getConvenienceFeeLineItemPosId());
						convenienceFeeItem.setQunatity(1);
						convenienceFeeItem.setPrice(Double.valueOf(convenienceFee.getConvenienceFee()));
						convenienceFeeItem.setOriginalPrice(Double.valueOf(convenienceFee.getConvenienceFee()));

						subtotal=subtotal + Double.valueOf(convenienceFee.getConvenienceFee());
//						double ConvenienceFeePercent =  (convenienceFee.getConvenienceFeePercent()!=null) ? convenienceFee.getConvenienceFeePercent() : 0;
//						double mapConvenienceFee = Double.valueOf(convenienceFee.getConvenienceFee()) + (subtotal*(ConvenienceFeePercent/100.0));
//						subtotal = subtotal + mapConvenienceFee;
						
						items.add(convenienceFeeItem);
						if (convenienceFee.getIsTaxable() != null) {
							if (convenienceFee.getIsTaxable() == 1) {
								isConvenienceFeeTaxable = true;

							} else {
								isConvenienceFeeTaxable = false;
							}
						} else {
							isConvenienceFeeTaxable = false;
						}
					}
				}

			}
			if (merchant.getOwner() != null && merchant.getOwner().getPos() != null) {
				// if(merchant.getOwner().getPos().getPosId()==IConstant.POS_CLOVER ||
				// merchant.getOwner().getPos().getName().toLowerCase().equals("clover") ){

				for (ItemDto itemDto : items) {
					if (freeItemArrayList != null && !freeItemArrayList.isEmpty()) {
						for (String itemPosId : freeItemArrayList) {
							if (itemDto.getItemPosId() != null && itemDto.getItemPosId().equals(itemPosId)) {
								freeItemQty = freeItemQty + itemDto.getQunatity();
							}
						}
					}
				}

				if (inventoryLevel.equals("order")) {

					if (limit != null && !limit.isEmpty()) {
						if (limit.equals("single")) {
							orderDiscount = minPriceAmount;
							// totalOrderDiscount=totalOrderDiscount+orderDiscount;=//totalOrderDiscount=totalOrderDiscount+orderDiscount;+orderDiscount;
							ArrayList<Double> orderItemPriceList = new ArrayList<Double>();

							LOGGER.info("====== itemSize :-" + items.size());
							for (int i = 0; i < items.size(); i++) {
								System.out.println("prices---" + items.get(i).getPrice() + " "
										+ items.get(i).getItemPosId() + " " + items.get(i).getQunatity());
								orderItemPriceList.add(items.get(i).getPrice());
							}
							System.out.println(isLeastPrice);
							// if(limit!=null && limit.equals("single")){
							if (isLeastPrice) {
								minOrderPriceAmount = Collections.min(orderItemPriceList);
								LOGGER.info("====== minOrderpriceAmount----" + minOrderPriceAmount);
							} else {
								minOrderPriceAmount = Collections.max(orderItemPriceList);
								LOGGER.info("====== maxOrderPriceAmount-" + minOrderPriceAmount);
							}
							// }

						}
					} else {

						if (discountType.equals("amount")) {
							orderDiscount = discount;

						} else {
							// old calculation
							/*
							 * orderDiscount=subtotal*discount/100; orderDiscount=Math.round(orderDiscount *
							 * 100.0) / 100.0;
							 */

							double doubleSubTotal = subtotal * 100;
							long longval = (long) doubleSubTotal;
							longval = (long) (longval * discount / 100);
							orderDiscount = ((double) longval / 100);

							System.out.println(Math.round(orderDiscount / 100) * 100);
						}
					}
				}

				List<TaxRates> taxRates = taxRateRepository.findByMerchantId(merchant.getId());
				List<TaxRates> defaultTaxRates = taxRateRepository.findByMerchantIdAndIsDefault(merchant.getId(),
						IConstant.BOOLEAN_TRUE);
				for (TaxRates rates : taxRates) {
					LOGGER.info(" ===== rateName ====  " + rates.getName());
					itemTaxes.put(rates.getName(), new ArrayList<ItemDto>());
				}
				int categorySize = 0;
				int categoryCount = 1;
				double categoryAppliedCount = 0;
				if (inventoryLevel.equals("category")) {
					ArrayList<Double> categoryItemPriceList = new ArrayList<Double>();
					for (ItemDto itemDto : items) {

						if (itemArrayList != null && !itemArrayList.isEmpty()) {
							for (String itemPosId : itemArrayList) {
								if (itemDto.getItemPosId() != null) {
									if (itemDto.getItemPosId().equals(itemPosId)) {

										LOGGER.info("====== itemSize:-" + items.size());

										// categoryItemPriceList.add(itemDto.getPrice());
										categoryItemPriceList
												.add((itemDto.getPrice() + itemDto.getItemModifierPrice()));

										// here code
										/*
										 * if (isUsabilitySingle == true) { limit = "single"; } else { limit =
										 * "multiple"; }
										 */
										if (limit != null && !limit.isEmpty() && eventId == 8) {
											if (limit.equals("single")) {
												categoryItemPrice = categoryItemPrice
														+ ((itemDto.getOriginalPrice() + itemDto.getItemModifierPrice())
																* itemDto.getQunatity());
											} else if (limit.equals("multiple")) {
												categoryItemPrice = categoryItemPrice
														+ ((itemDto.getOriginalPrice() + itemDto.getItemModifierPrice())
																* itemDto.getQunatity());
											}
										} else {
											// categoryItemPrice=categoryItemPrice+(itemDto.getPrice()*itemDto.getQunatity());
											if (kouponCount > 0) {
												categoryItemPrice = categoryItemPrice + itemDto.getPrice();
											} else {
												categoryItemPrice = categoryItemPrice
														+ ((itemDto.getPrice() + itemDto.getItemModifierPrice())
																* itemDto.getQunatity());
											}
										}

										LOGGER.info("====== categoryItemPrice---" + categoryItemPrice);
									}
								}
							}

						}

					}

					categorySize = categoryItemPriceList.size();

					LOGGER.info("====== isleastPrice ===== " + isLeastPrice);
					if (limit != null && !limit.isEmpty() && eventId == 8) {
						if (isLeastPrice) {
							minPriceAmount = Collections.min(categoryItemPriceList);
							LOGGER.info("====== minpriceAmount ===== " + minPriceAmount);
						} else {
							minPriceAmount = Collections.max(categoryItemPriceList);
							LOGGER.info("====== maxPriceAmount ======= " + minPriceAmount);
						}
					}
				}
				LOGGER.info("====== items-->size === " + items.size());

				List<ItemDto> extraFreeItems = new ArrayList<ItemDto>();
				if ("single".equalsIgnoreCase(limit) && freeItemQty >= numberOfItemsForDiscountCount) {
					for (ItemDto itemDto : items) {
						if (freeItemArrayList.contains(itemDto.getItemPosId()) && itemDto.getQunatity() >= 2) {
							for (int i = 1; i < itemDto.getQunatity(); i++) {
								ItemDto itemDto2 = new ItemDto();
								itemDto2.setAllowModifierLimit(itemDto.getAllowModifierLimit());
								itemDto2.setAuxTaxAble(itemDto.getAuxTaxAble());
								itemDto2.setDescription(itemDto.getDescription());
								itemDto2.setDiscount(itemDto.getDiscount());
								itemDto2.setId(itemDto.getId());
								itemDto2.setItemImage(itemDto.getItemImage());
								itemDto2.setItemModifierPrice(itemDto.getItemModifierPrice());
								itemDto2.setItemName(itemDto.getItemImage());
								itemDto2.setItemPosId(itemDto.getItemPosId());
								itemDto2.setItemTax(itemDto.getItemTax());
								itemDto2.setItemTaxName(itemDto.getItemTaxName());
								itemDto2.setModifierGroupDtos(itemDto.getModifierGroupDtos());
								itemDto2.setOriginalPrice(itemDto.getOriginalPrice());
								itemDto2.setPrice(itemDto.getOriginalPrice());
								itemDto2.setQunatity(1);
								itemDto2.setTaxAble(itemDto.getTaxAble());
								itemDto2.setTotalQunatity(1);

								extraFreeItems.add(itemDto2);
							}
							itemDto.setQunatity(1);
							itemDto.setTotalQunatity(1);
						}
					}
					if (!extraFreeItems.isEmpty()) {
						items.addAll(extraFreeItems);
					}
				}

				if(convenienceFeeObject !=null && convenienceFeeObject.getConvenienceFeePercent()!=null) {
					double ConvenienceFeePercent =  (convenienceFeeObject.getConvenienceFeePercent()!=null) ? convenienceFeeObject.getConvenienceFeePercent() : 0;
					double convPercent = ((orderSubtotal-orderDiscount)*ConvenienceFeePercent/100);
					discountedSubTotal = discountedSubTotal + convPercent;
					ConvenienceFeeAfterDiscount = new Double(convenienceFeeObject.getConvenienceFee()) + convPercent;
					}
				
				for (ItemDto itemDto : items) {
					if (itemDto != null && itemDto.getItemPosId() != null && !itemDto.getItemPosId().isEmpty()) {
						Double discountedItemPrices = itemDto.getPrice();
						if (inventoryLevel.equals("order")) {
							if (limit != null && !limit.isEmpty() && convenienceFeeItem != null
									&& convenienceFeeItem.getItemPosId() != null
									&& itemDto.getItemPosId() != convenienceFeeItem.getItemPosId() && eventId == 8) {
								/*
								 * if (isUsabilitySingle == true) { limit = "single"; } else { limit =
								 * "multiple"; }
								 */
								if (limit.equals("single")) {
									LOGGER.info("====== order--- itemDto.getTotalQunatity()-->"
											+ itemDto.getTotalQunatity() + " == minOrderPriceAmount ==== "
											+ minOrderPriceAmount + " ===== getPrice ==== " + itemDto.getPrice()
											+ " ===== getQuantity ==== " + itemDto.getQunatity()
											+ "==== Discount_status ===== " + discountStatus);
									if (itemDto.getQunatity() > 1) {
										LOGGER.info(" ====== inside if (itemDto.getQunatity() > 1) ");
										if (minOrderPriceAmount == itemDto.getPrice() && !discountStatus) {
											LOGGER.info("====== minPriceAmount -->" + minOrderPriceAmount);
											orderDiscount = itemDto.getPrice();
											LOGGER.info(" ======  orderDiscount -->" + orderDiscount);
											LOGGER.info(
													" ======discountedItemPrices before ==== " + discountedItemPrices);
											discountedItemPrices = ((itemDto.getPrice()
													+ itemDto.getItemModifierPrice()) * itemDto.getQunatity()
													- orderDiscount);
											LOGGER.info(
													" ======  discountedItemPrices after ==== " + discountedItemPrices);
											discountStatus = true;
										} else {
											discountedItemPrices = ((itemDto.getPrice()
													+ itemDto.getItemModifierPrice()) * itemDto.getQunatity());
										}

										LOGGER.info(" ======  discountedItemPrices ==== " + discountedItemPrices);
									} else {
										// response.put("DATA", "plz add more than 1 item to apply this koupon");
										response.put("responsMessage", "plz add more than 1 item to apply this koupon");
										response.put("responsCode", "400");
										response.put("DATA", appliedDiscounts.get("DATA"));
										// voucherJson = gson.toJson(response);
										return response;
										// return voucherJson;
									}
								} else if (limit.equals("multiple")) {
									LOGGER.info(" ====== sub total-" + itemDto.getPrice() * itemDto.getQunatity()
											+ " === price ==== " + itemDto.getPrice());
									LOGGER.info(" ====== order--itemDto.getQunatity()-->" + itemDto.getQunatity());

									if (itemDto.getQunatity() > 1) {
										Integer itemQuantity;
										itemQuantity = itemDto.getQunatity() / 2;
										LOGGER.info(" ====== discountedItemPrices before--->> " + discountedItemPrices);
										discountedItemPrices = (discountedItemPrices - itemDto.getPrice())
												+ ((itemDto.getPrice() + itemDto.getItemModifierPrice())
														* itemDto.getQunatity() - (itemDto.getPrice() * itemQuantity));
										LOGGER.info(
												" ====== discountedItemPrices after ======= " + discountedItemPrices);
										orderDiscount = orderDiscount + (itemDto.getPrice() * itemQuantity);
										System.out.println(orderDiscount);
										discountStatus = true;
									} else {
										// response.put("DATA", "plz add more than 1 item to apply this koupon");
										response.put("responsMessage", "plz add more than 1 item to apply this koupon");
										response.put("responsCode", "400");
										response.put("DATA", appliedDiscounts.get("DATA"));
										// voucherJson = gson.toJson(response);
										// return voucherJson;
										return response;
									}
								}

								LOGGER.info(
										"===subtotal ==== " + subtotal + "====== orderDiscount =====" + orderDiscount);

								// discountStatus=true;

							} else {

								if (kouponCount > 0) {
									if (orderDiscount > oldDiscountedSubTotal) {
										orderDiscount = oldDiscountedSubTotal;
									}
									discountedItemPrices = (itemDto.getPrice()
											- ((itemDto.getPrice() / oldDiscountedSubTotal) * orderDiscount));

								} else {
									if (orderDiscount > subtotal) {
										orderDiscount = subtotal;
									}
									discountedItemPrices = ((itemDto.getPrice() + itemDto.getItemModifierPrice())
											- (((itemDto.getPrice() + itemDto.getItemModifierPrice()) / subtotal)
													* orderDiscount))
											* itemDto.getQunatity();
									// discountedItemPrices=Math.round(discountedItemPrices * 100.0) / 100.0;
								}

								discountStatus = true;
							}
						}
						System.out.println("itemDto.getTotalQunatity()-->>>" + itemDto.getTotalQunatity()
								+ " discountedItemPrices------>>>" + discountedItemPrices);

						if (inventoryLevel.equals("item")) {
							if ((freeItemArrayList == null || freeItemArrayList.isEmpty())
									&& (limit == null || limit.isEmpty())) {
								freeItemArrayList = itemArrayList;
							}
							if (freeItemArrayList != null && !freeItemArrayList.isEmpty()) {

								for (String itemPosId : freeItemArrayList) {

									if (itemDto.getItemPosId() != null && itemDto.getItemPosId().equals(itemPosId)) {
										if (eventId == 8) {
											/*
											 * if (isUsabilitySingle == true) { limit = "single"; } else { limit =
											 * "multiple"; }
											 */
											if (limit != null && !limit.isEmpty()) {
												if (freeItemArrayList != null && !freeItemArrayList.isEmpty()) {
													if (limit.equals("single")) {
														if (!discountStatus) {
															appliedDiscountOnItemCount += itemDto.getQunatity();
															/* if(itemDto.getTotalQunatity()>1){ */
															System.out.println("sub total-"
																	+ itemDto.getPrice() * itemDto.getQunatity()
																	+ " price-" + itemDto.getPrice());
															// orderDiscount = discount;

															if (discountType.equals("amount")) {
																orderDiscount = discount;
															} else {
																double itemPercentDiscount = 0.0;

																itemPercentDiscount = (itemDto.getOriginalPrice()
																		* itemDto.getQunatity()) * discount / 100;
																orderDiscount += Math.round(itemPercentDiscount * 100.0)
																		/ 100.0;
															}

															if (kouponCount > 0) {
																if (itemDto.getOriginalPrice() > itemDto.getPrice()
																		&& orderDiscount > itemDto.getPrice()) {
																	orderDiscount = itemDto.getPrice();
																} else if (orderDiscount > itemDto.getOriginalPrice()) {
																	orderDiscount = itemDto.getOriginalPrice();
																}
																discountedItemPrices = itemDto.getPrice()
																		- orderDiscount;
															} else {
																// if (orderDiscount > itemDto.getOriginalPrice()) {
																if (orderDiscount > (itemDto.getOriginalPrice()
																		* itemDto.getQunatity())) {
																	orderDiscount = itemDto.getOriginalPrice();
																}
																discountedItemPrices = ((itemDto.getPrice()
																		+ itemDto.getItemModifierPrice())
																		* itemDto.getQunatity() - orderDiscount);
															}

															discountStatus = true;
															itemDto.setDiscount(itemDto.getDiscount() + orderDiscount);
															itemDto.setDiscountName(
																	itemDto.getDiscountName() + " " + voucherCode);
														} else {
															if (kouponCount > 0) {
																discountedItemPrices = itemDto.getPrice();
															} else {
																discountedItemPrices = (itemDto.getPrice()
																		+ itemDto.getItemModifierPrice())
																		* itemDto.getQunatity();
															}
														}

													} else if (limit
															.equals("multiple")/*
																				 * && appliedDiscountOnItemCount <
																				 * numberOfItemsForDiscountCount
																				 */) {
														if (!discountStatus) {
															appliedDiscountOnItemCount += itemDto.getQunatity();
															System.out.println("sub total-"
																	+ itemDto.getPrice() * itemDto.getQunatity()
																	+ " price-" + itemDto.getPrice());
															/* if(itemDto.getQunatity()>1){ */
															if (freeItemQty > numberOfItemsForDiscountCount) {
																freeItemQty = numberOfItemsForDiscountCount;
															}
															Integer itemQuantity = freeItemQty;
															double multiItemPrice = 0.0;
															// itemQuantity = itemDto.getTotalQunatity()/2;
															// System.out.println("eligible
															// itemQuantity-"+itemQuantity);
															// orderDiscount = itemDto.getOriginalPrice()*itemQuantity;
															multiItemPrice = itemDto.getOriginalPrice() * itemQuantity;
															if (discountType.equals("amount")) {
																if (discount > itemDto.getOriginalPrice()) {
																	discount = itemDto.getOriginalPrice();
																}
																orderDiscount = discount * itemQuantity;
																;
															} else {
																double itemPercentDiscount = 0.0;

																itemPercentDiscount = (multiItemPrice) * discount / 100;
																orderDiscount = Math.round(itemPercentDiscount * 100.0)
																		/ 100.0;
															}

															if (kouponCount > 0) {
																if (multiItemPrice > itemDto.getPrice()
																		&& orderDiscount > itemDto.getPrice()) {
																	orderDiscount = itemDto.getPrice();
																} else if (orderDiscount > multiItemPrice) {
																	orderDiscount = multiItemPrice;
																}
																discountedItemPrices = itemDto.getPrice()
																		- orderDiscount;
															} else {
																if (orderDiscount > multiItemPrice) {
																	orderDiscount = multiItemPrice;
																}
																discountedItemPrices = ((itemDto.getPrice()
																		+ itemDto.getItemModifierPrice())
																		* itemDto.getQunatity() - orderDiscount);
															}
															discountStatus = true;
															itemDto.setDiscount(itemDto.getDiscount() + orderDiscount);
															itemDto.setDiscountName(
																	itemDto.getDiscountName() + " " + voucherCode);
															/*
															 * }else{ response.put("DATA",
															 * "plz add more than 1 item to apply this koupon");
															 * response.put("responsCode", "400"); voucherJson =
															 * gson.toJson(response); return voucherJson; return
															 * response; }
															 * 
															 */
														} else {
															if (kouponCount > 0) {
																discountedItemPrices = itemDto.getPrice();
															} else {
																discountedItemPrices = (itemDto.getPrice()
																		+ itemDto.getItemModifierPrice())
																		* itemDto.getQunatity();
															}
														}
													}
												} else {
													response.put("responsMessage",
															"plz add more than 1 item to apply this koupon");
													response.put("responsCode", "400");
													response.put("DATA", appliedDiscounts.get("DATA"));
													/*
													 * voucherJson = gson.toJson(response); return voucherJson;
													 */
													LOGGER.info(
															"===============  VoucharServiceImpl : Inside calculateDiscount :: End  ============= ");

													return response;
												}

											} else {
												response.put("responsMessage", "koupon not valid");
												response.put("responsCode", "400");
												response.put("DATA", appliedDiscounts.get("DATA"));
												/*
												 * voucherJson = gson.toJson(response); return voucherJson;
												 */
												LOGGER.info(
														"===============  VoucharServiceImpl : Inside calculateDiscount :: End  ============= ");

												return response;
											}
										} else {
											if (discountType.equals("amount")) {
												System.out.println("discountType`" + discountType + " discountStatus="
														+ discountStatus + " discount=" + discount
														+ " itemDto.getQunatity()=" + itemDto.getQunatity());

												if (discount > 0) {
													orderDiscount = discount;
													// finalDiscount = discount;
													if (kouponCount > 0) {
														if (orderDiscount > itemDto.getPrice())
															orderDiscount = itemDto.getPrice();

														discountedItemPrices = (itemDto.getPrice() - orderDiscount);
														itemDto.setDiscount(itemDto.getDiscount() + orderDiscount);
														itemDto.setDiscountName(
																itemDto.getDiscountName() + " " + voucherCode);
													} else {
														if (orderDiscount > ((itemDto.getPrice()
																+ itemDto.getItemModifierPrice())
																* itemDto.getQunatity())) {
															orderDiscount = (itemDto.getPrice()
																	+ itemDto.getItemModifierPrice())
																	* itemDto.getQunatity();
														}
														discountedItemPrices = (((itemDto.getPrice()
																+ itemDto.getItemModifierPrice())
																* itemDto.getQunatity()) - orderDiscount);
														itemDto.setDiscount(itemDto.getDiscount() + orderDiscount);
														itemDto.setDiscountName(
																itemDto.getDiscountName() + " " + voucherCode);
													}
												} else {
													if (kouponCount > 0) {
														discountedItemPrices = itemDto.getPrice();
													} else {
														discountedItemPrices = ((itemDto.getPrice()
																+ itemDto.getItemModifierPrice())
																* itemDto.getQunatity());
													}
												}
												discount = discount - orderDiscount;
												finalDiscount += orderDiscount;
												if (discount == 0 && finalDiscount > 0) {
													orderDiscount = finalDiscount;
												}
											} else {
												double itemPercentDiscount = 0.0;
												if (kouponCount > 0) {
													itemPercentDiscount = (itemDto.getPrice()) * discount / 100;
												} else {
													itemPercentDiscount = ((itemDto.getPrice()
															+ itemDto.getItemModifierPrice()) * itemDto.getQunatity())
															* discount / 100;
												}

												itemPercentDiscount = Math.round(itemPercentDiscount * 100.0) / 100.0;
												orderDiscount = orderDiscount + itemPercentDiscount;
												discountedItemPrices = ((itemDto.getPrice()
														+ itemDto.getItemModifierPrice()) * itemDto.getQunatity()
														- itemPercentDiscount);
												itemDto.setDiscount(itemDto.getDiscount() + orderDiscount);
												itemDto.setDiscountName(itemDto.getDiscountName() + " " + voucherCode);
											}

											// orderDiscount=orderDiscount+discount;

										}

										discountStatus = true;
									} else {
										if (kouponCount > 0) {
											discountedItemPrices = itemDto.getPrice();
										} else {
											discountedItemPrices = (itemDto.getPrice() + itemDto.getItemModifierPrice())
													* itemDto.getQunatity();
										}
									}
								}

							} else {
								// return "invalid coupon";
								// response.put("DATA", "Invalid koupon");
								response.put("DATA", appliedDiscounts.get("DATA"));
								response.put("responsMessage", "Invalid koupon");
								response.put("responsCode", "400");
								LOGGER.info(
										"===============  VoucharServiceImpl : Inside calculateDiscount :: End  ============= ");

								return response;
							}
						}

						if (inventoryLevel.equals("category")) {
							if (itemArrayList != null && !itemArrayList.isEmpty()) {

								for (String itemPosId : itemArrayList) {
									if (itemDto.getItemPosId() != null && itemDto.getItemPosId().equals(itemPosId)) {
										LOGGER.info("===limit ==== " + limit);
										/*
										 * if (isUsabilitySingle == true) { limit = "single"; } else { limit =
										 * "multiple"; }
										 */
										if (limit != null && !limit.isEmpty()) {
											if (limit.equals("single")) {
												if (freeItemQty > 0) {
													System.out.println(minPriceAmount.equals(itemDto.getPrice()));
													if (minPriceAmount.equals(
															(itemDto.getPrice() + itemDto.getItemModifierPrice()))
															&& !discountStatus) {
														// int itemQry=(itemDto.getTotalQunatity()/2);
														// System.out.println("itemQry -->"+itemQry);
														if (discountType.equals("amount")) {
															orderDiscount = discount;
														} else {
															orderDiscount = (itemDto.getOriginalPrice() * discount)
																	/ 100;
															orderDiscount = Math.round(orderDiscount * 100.0) / 100.0;
														}

														if (kouponCount > 0) {
															if (itemDto.getOriginalPrice() > itemDto.getPrice()
																	&& orderDiscount > itemDto.getPrice()) {
																orderDiscount = itemDto.getPrice();
															} else if (orderDiscount > itemDto.getOriginalPrice()) {
																orderDiscount = itemDto.getOriginalPrice();
															}
															discountedItemPrices = ((itemDto.getPrice()
																	+ itemDto.getItemModifierPrice())
																	* itemDto.getQunatity() - orderDiscount);

														} else {
															if (orderDiscount > itemDto.getOriginalPrice()) {
																orderDiscount = itemDto.getOriginalPrice();
															}
															discountedItemPrices = ((itemDto.getPrice()
																	+ itemDto.getItemModifierPrice())
																	* itemDto.getQunatity() - orderDiscount);

														}

														itemDto.setDiscount(itemDto.getDiscount() + orderDiscount);
														itemDto.setDiscountName(
																itemDto.getDiscountName() + " " + voucherCode);
														discountStatus = true;
													} else {
														discountedItemPrices = ((itemDto.getPrice()
																+ itemDto.getItemModifierPrice())
																* itemDto.getQunatity());
													}
													System.out.println(
															"sumit .. discountedItemPrices -->" + discountedItemPrices);
												} else {
													// response.put("DATA",
													// "plz add more than 1 item to apply this koupon");
													response.put("DATA", appliedDiscounts.get("DATA"));
													response.put("responsMessage",
															"plz add more than 1 item to apply this koupon");
													response.put("responsCode", "400");
													/*
													 * voucherJson = gson.toJson(response); return voucherJson;
													 */
													LOGGER.info(
															"===============  VoucharServiceImpl : Inside calculateDiscount :: End  ============= ");

													return response;
												}
												LOGGER.info("===== original discounted price-" + discountedItemPrices);
											} else if (limit.equals("multiple")) {
												System.out.println(
														"sub total-" + itemDto.getPrice() * itemDto.getQunatity()
																+ " price-" + itemDto.getPrice());
												if (freeItemQty > 0) {
													System.out.println(minPriceAmount.equals(itemDto.getPrice()));
													if (minPriceAmount.equals(itemDto.getPrice()) && !discountStatus) {
														// int itemQry=(itemDto.getTotalQunatity()/2);
														// System.out.println("itemQry -->"+itemQry);
														if (discountType.equals("amount")) {
															orderDiscount = discount;
														} else {
															orderDiscount = itemDto.getOriginalPrice()
																	* itemDto.getQunatity() * discount / 100;
															orderDiscount = Math.round(orderDiscount * 100.0) / 100.0;
														}

														if (kouponCount > 0) {

															if (itemDto.getOriginalPrice() > itemDto.getPrice()
																	&& orderDiscount > itemDto.getPrice()) {
																orderDiscount = itemDto.getPrice();
															} else if (orderDiscount > itemDto.getOriginalPrice()) {
																orderDiscount = itemDto.getOriginalPrice();
																if (itemDto.getOriginalPrice() * freeItemQty > itemDto
																		.getOriginalPrice() * itemDto.getQunatity()) {
																	orderDiscount = itemDto.getOriginalPrice()
																			* itemDto.getQunatity();
																} else {
																	orderDiscount = itemDto.getOriginalPrice()
																			* freeItemQty;
																}
															} else {
																orderDiscount = orderDiscount * freeItemQty;
															}
															discountedItemPrices = (itemDto.getPrice() - orderDiscount);

														} else {
															if (orderDiscount > itemDto.getOriginalPrice()) {
																orderDiscount = itemDto.getOriginalPrice();
																if (itemDto.getOriginalPrice() * freeItemQty > itemDto
																		.getOriginalPrice() * itemDto.getQunatity()) {
																	orderDiscount = itemDto.getOriginalPrice()
																			* itemDto.getQunatity();
																} else {
																	orderDiscount = itemDto.getOriginalPrice()
																			* freeItemQty;
																}
															} else {
																if (orderDiscount > (itemDto.getPrice()
																		+ itemDto.getItemModifierPrice())
																		* itemDto.getQunatity()) {
																	orderDiscount = Math
																			.round(((itemDto.getPrice()
																					+ itemDto.getItemModifierPrice())
																					* itemDto.getQunatity()) * 100.0)
																			/ 100.0;
																}
															}
															discountedItemPrices = ((itemDto.getPrice()
																	+ itemDto.getItemModifierPrice())
																	* itemDto.getQunatity() - orderDiscount);

														}

														itemDto.setDiscount(itemDto.getDiscount() + orderDiscount);
														itemDto.setDiscountName(
																itemDto.getDiscountName() + " " + voucherCode);
														discountStatus = true;
													} else {
														discountedItemPrices = (itemDto.getPrice()
																+ itemDto.getItemModifierPrice())
																* itemDto.getQunatity();
													}
													LOGGER.info(
															" === discountedItemPrices ==== " + discountedItemPrices);
												} else {
													// response.put("DATA",
													// "plz add more than 1 item to apply this koupon");
													response.put("DATA", appliedDiscounts.get("DATA"));
													response.put("responsMessage",
															"plz add more than 1 item to apply this koupon");
													response.put("responsCode", "400");
													/*
													 * voucherJson = gson.toJson(response); return voucherJson;
													 */
													LOGGER.info(
															"===============  VoucharServiceImpl : Inside calculateDiscount :: End  ============= ");

													return response;
												}
											}
										} else {
											if (discountType.equals("amount")) {
												orderDiscount = discount;
												// totalOrderDiscount=totalOrderDiscount+orderDiscount;

												if (orderDiscount > categoryItemPrice) {
													orderDiscount = categoryItemPrice;
												}

											} else {

												double categoryItemPercentDiscount = (categoryItemPrice) * discount
														/ 100;
												System.out.println(
														"categoryItemPercentDiscount-" + categoryItemPercentDiscount);
												categoryItemPercentDiscount = Math
														.round(categoryItemPercentDiscount * 100.0) / 100.0;
												LOGGER.info(" ===== categoryItemPercentDiscount ====="
														+ categoryItemPercentDiscount);
												orderDiscount = categoryItemPercentDiscount;
												LOGGER.info(" ===== orderDiscount ===== " + orderDiscount);

											}
											double categoryDiscount = 0.0;

											if ((categorySize) > categoryCount) {
												if (kouponCount > 0) {
													categoryDiscount = (itemDto.getPrice() / categoryItemPrice)
															* orderDiscount;
												} else {
													categoryDiscount = ((itemDto.getPrice()
															+ itemDto.getItemModifierPrice()) * itemDto.getQunatity()
															/ categoryItemPrice) * orderDiscount;
												}

												categoryDiscount = Math.round(categoryDiscount * 100.0) / 100.0;
												categoryAppliedCount = categoryAppliedCount + categoryDiscount;
											} else {
												categoryDiscount = orderDiscount - categoryAppliedCount;
												categoryDiscount = Math.round(categoryDiscount * 100.0) / 100.0;
												categoryAppliedCount = categoryAppliedCount + categoryDiscount;
											}
											if (kouponCount > 0) {
												discountedItemPrices = (itemDto.getPrice() - categoryDiscount);
											} else {
												discountedItemPrices = ((itemDto.getPrice()
														+ itemDto.getItemModifierPrice()) * itemDto.getQunatity()
														- categoryDiscount);
											}

											itemDto.setDiscount(itemDto.getDiscount() + categoryDiscount);
											itemDto.setDiscountName(itemDto.getDiscountName() + " " + voucherCode);
											discountStatus = true;
											categoryCount++;
										}

										break;
									} else {
										if (kouponCount > 0) {
											discountedItemPrices = itemDto.getPrice();
										} else {
											discountedItemPrices = (itemDto.getPrice() + itemDto.getItemModifierPrice())
													* itemDto.getQunatity();
										}
									}
								}

							} else {
								// return "invalid coupon";
								// response.put("DATA", "Invalid koupon");
								response.put("DATA", appliedDiscounts.get("DATA"));
								response.put("responsMessage", "Invalid koupon");
								response.put("responsCode", "400");
								LOGGER.info(
										"===============  VoucharServiceImpl : Inside calculateDiscount :: End  ============= ");

								return response;
							}
						}
						discountedSubTotal = discountedSubTotal + discountedItemPrices;
						
						LOGGER.info(" ==== discountedSubTotal final ===== " + discountedSubTotal
								+ " ===== discountedItemPrices ===== " + discountedItemPrices);
						itemDto.setPrice(discountedItemPrices);

						LOGGER.info(" ======= item Id ========== " + itemDto.getItemPosId());

						List<ItemTax> itemTaxs = null;
						List<PizzaTemplateTax> pizzaTemplateTaxs = null;

						if (merchant != null && merchant.getOwner() != null && merchant.getOwner().getPos() != null
								&& merchant.getOwner().getPos().getPosId() != null
								&& merchant.getOwner().getPos().getPosId() == IConstant.NON_POS) {
							System.out
									.println("inside if============item Id==================" + itemDto.getItemPosId());
							if (!itemDto.getItemPosId().isEmpty() && itemDto.getItemPosId() != null) {
								if (itemDto.getIsPizza() == false) {
									itemTaxs = itemTaxRepository.findByItemIdAndItemMerchantId(
											Integer.parseInt(itemDto.getItemPosId()), merchant.getId());
								} else {
									pizzaTemplateTaxs = pizzaTemplateTaxRepository
											.findByPizzaTemplateIdAndPizzaTemplateMerchantId(
													Integer.parseInt(itemDto.getItemPosId()), merchant.getId());
								}
							}

						} else {
							System.out.println(
									"inside else============item Id==================" + itemDto.getItemPosId());
							if (itemDto.getIsPizza() != null) {
								if (itemDto.getIsPizza() == true) {
									pizzaTemplateTaxs = pizzaTemplateTaxRepository
											.findByPizzaTemplateIdAndPizzaTemplateMerchantId(
													Integer.parseInt(itemDto.getItemPosId()), merchant.getId());
								} else {
									itemTaxs = itemTaxRepository.findByItemPosItemIdAndItemMerchantId(
											itemDto.getItemPosId(), merchant.getId());
								}
							} else {
								itemTaxs = itemTaxRepository
										.findByItemPosItemIdAndItemMerchantId(itemDto.getItemPosId(), merchant.getId());
							}
						}

						if (itemDto.getIsPizza() != null && itemDto.getIsPizza() == true) {
							if (!pizzaTemplateTaxs.isEmpty()) {
								for (PizzaTemplateTax itemTax : pizzaTemplateTaxs) {
									if (itemTax != null && itemTax.getTaxRates() != null) {
										TaxRates rates = itemTax.getTaxRates();
										if (itemTaxes.containsKey(rates.getName()) && rates.getName() != null) {
											ArrayList<ItemDto> itemTx = (ArrayList) itemTaxes.get(rates.getName());
											itemTx.add(itemDto);
										}
									}
								}
							}
						} else {
							for (ItemTax itemTax : itemTaxs) {
								TaxRates rates = itemTax.getTaxRates();
								if (itemTaxes.containsKey(rates.getName())) {
									ArrayList<ItemDto> itemTx = (ArrayList) itemTaxes.get(rates.getName());
									itemTx.add(itemDto);
								}
							}
						}

					} else {
						Double discountedConvenienceFee = Math
								.round((((itemDto.getPrice()) - (((itemDto.getPrice()) / subtotal) * orderDiscount))
										* itemDto.getQunatity()) * 100.0)
								/ 100.0;
						discountedSubTotal += discountedConvenienceFee;
						if (inventoryLevel.equals("order") && convenienceFeeItem != null) {

							convenienceFeeItem.setPrice(discountedConvenienceFee);
						}
					}
				}

				if (merchant != null && merchant.getOwner() != null && merchant.getOwner().getPos() != null
						&& merchant.getOwner().getPos().getPosId() != null
						&& merchant.getOwner().getPos().getPosId() != 1) {
					if (convenienceFeeItem != null && isConvenienceFeeTaxable) {
						for (TaxRates defaultTax : defaultTaxRates) {
							ArrayList<ItemDto> itemTx = (ArrayList) itemTaxes.get(defaultTax.getName());
							itemTx.add(convenienceFeeItem);
						}
					}

					if (deliveryTaxable && deliveryFee != null && deliveryFee > 0) {
						ItemDto deliveryItemDto = new ItemDto();
						deliveryItemDto.setItemPosId(deliveryPosId);
						deliveryItemDto.setOriginalPrice(deliveryFee);
						deliveryItemDto.setPrice(deliveryFee);
						deliveryItemDto.setQunatity(1);
						discountedSubTotal += deliveryFee;
						for (TaxRates defaultTax : defaultTaxRates) {
							ArrayList<ItemDto> itemTx = (ArrayList) itemTaxes.get(defaultTax.getName());
							itemTx.add(deliveryItemDto);
						}
					}
				}

				if (merchant.getOwner().getPos().getPosId() != 2) {
					for (TaxRates rates : taxRates) {
						ArrayList<ItemDto> itemTx = (ArrayList) itemTaxes.get(rates.getName());
						double totalItemPrice = 0;
						for (ItemDto itemDto : itemTx) {
							totalItemPrice = (totalItemPrice + itemDto.getPrice());
							// System.out.println("totalItemPrice-"+totalItemPrice);
						}
						if (rates != null && rates.getRate() != null) {
							discountedTax = Math
									.round((discountedTax + (totalItemPrice * rates.getRate() / 100)) * 100.0) / 100.0;
						}

					}
				} else {

					if (items != null && items.size() > 0) {
						for (ItemDto itemDto : items) {
							if (itemDto != null && itemDto.getItemPosId() != null && itemDto.getIsPizza() != null
									&& !itemDto.getIsPizza()) {
								Item dbItem = itemmRepository.findByIdAndMerchantId(
										Integer.parseInt(itemDto.getItemPosId()), merchant.getId());
								if (dbItem != null && dbItem.getPrice() != null) {

									if (dbItem.getTaxAble()) {
										itemTaxCount = itemTaxCount + itemDto.getPrice();
									}
									if (dbItem.getAuxTaxAble()) {
										auxTaxCount = auxTaxCount + itemDto.getPrice();
									}
								}
							}
							if (itemDto != null && itemDto.getItemPosId() != null && itemDto.getIsPizza() != null
									&& itemDto.getIsPizza()) {
								PizzaTemplate pizzaTemplate = pizzaTemplateRepository.findByIdAndMerchantId(
										Integer.parseInt(itemDto.getItemPosId()), merchant.getId());

								if (pizzaTemplate != null) {

									if (pizzaTemplate.getTaxable() != null && pizzaTemplate.getTaxable()) {
										itemTaxCount = itemTaxCount + itemDto.getPrice();
									}
									if (pizzaTemplate.getAuxTaxable() != null && pizzaTemplate.getAuxTaxable()) {
										auxTaxCount = auxTaxCount + itemDto.getPrice();
									}
								}
							}
						}
					}
					JSONObject cartJsonObjects = new JSONObject(cartJson);
					if (cartJsonObjects != null && cartJsonObjects.has("cartJson")) {
						JSONObject cartJsonObject = cartJsonObjects.getJSONObject("cartJson");
						if (cartJsonObject != null && cartJsonObject.has("label")) {
							String label = cartJsonObject.getString("label");
							OrderType orderType = orderTypeRepository.findByMerchantIdAndLabel(merchant.getId(), label);
							if (orderType != null) {
								if (orderType.getAuxTax() != null) {
									auxTax = (auxTaxCount * orderType.getAuxTax() / 100);
								}

								if (orderType.getSalesTax() != null) {
									discountedTax = (itemTaxCount * orderType.getSalesTax() / 100);
								}
							}
						}
					}
				}
				discountedTax = Math.round(discountedTax * 100.0) / 100.0;
				discountedSubTotal = Math.round(discountedSubTotal * 100.0) / 100.0;
				discountedTotal = discountedSubTotal + discountedTax + tipAmount + auxTax;
				discountedTotal = Math.round(discountedTotal * 100.0) / 100.0;

				/*
				 * }else{
				 * 
				 * 
				 * }
				 */
			}
		}
		LOGGER.info(" ======= discountedTax ===== " + discountedTax + " discountedSubTotal " + discountedSubTotal
				+ " tipAmount " + tipAmount + " discountedTotal " + discountedTotal);
		LOGGER.info(" ======= discountStatus ===== " + discountStatus);
		if (discountStatus) {
			kouponCount++;

			Map<String, Object> discountMap = new HashMap<String, Object>();
			/*
			 * discountMap.put("voucherCode", voucherCode); discountMap.put("discount",
			 * couponDiscount); discountMap.put("inventoryLevel", inventoryLevel);
			 * discountMap.put("discountType", "amount"); discountMap.put("couponUID",
			 * couponUID);
			 */

			if (kouponCount > 0) {
				// discountMapList=cart.get("discountList");
				boolean existingCouon = false;
				for (Map<String, Object> map : discountMapList) {
					String coupon = (String) map.get("voucherCode");
					if (coupon != null && coupon.equals(voucherCode)) {
						// discountMap=map;
						existingCouon = true;
						totalOrderDiscount = totalOrderDiscount + orderLeveldiscount;
					}
				}
				LOGGER.info("==== existingCouon === " + existingCouon);
				if (!existingCouon) {
					discountMapList.add(discountMap);
				}
			} else {
				discountMapList = new ArrayList<Map<String, Object>>();
				discountMapList.add(discountMap);
			}
            
			totalOrderDiscount = totalOrderDiscount + orderDiscount;
			Double totalOrderDiscountRounded = Math.round(totalOrderDiscount * 100.0) / 100.0;
			LOGGER.info("==== totalOrderDiscountRounded=== " + totalOrderDiscountRounded);
			discountMap.put("voucherCode", voucherCode);
			discountMap.put("discount", totalOrderDiscountRounded);
			discountMap.put("inventoryLevel", inventoryLevel);
			discountMap.put("discountType", "amount");
			discountMap.put("couponUID", couponUID);
			discountMap.put("isUsabilitySingle", isUsabilitySingle);

			Map<String, Object> cart = new HashMap<String, Object>();
			cart.put("tax", discountedTax);
			cart.put("subtotal", discountedSubTotal);
			cart.put("total", discountedTotal);
			cart.put("convFee", "5");
			cart.put("deliveryFee", deliveryFee);
			cart.put("discount", totalOrderDiscountRounded);
			cart.put("discountType", "amount");
			// cart.put("voucherCode", voucherCode);
			// cart.put("inventoryLevel", inventoryLevel);
			cart.put("itemList", itemArrayList);
			cart.put("items", items);
			cart.put("discountList", discountMapList);
			cart.put("kouponCount", kouponCount);
			cart.put("convenienceFeeItem", convenienceFeeItem);
			cart.put("isConvenienceFeeTaxable", isConvenienceFeeTaxable);
			cart.put("auxTax", auxTax);
			cart.put("ConvenienceFeeAfterDiscount", ConvenienceFeeAfterDiscount);			
			// cart.put("isDeliveryFeeKoupon", isDeliveryFeeKoupon);

			response.put("DATA", cart);
			response.put("responsCode", "200");

			LOGGER.info(" ======= kouponCount  -->>>" + kouponCount);

		} else {

			// response.put("DATA",IConstant.MESSAGE_INVALID_KOUPON);
			response.put("DATA", appliedDiscounts.get("DATA"));
			response.put("responsMessage", IConstant.MESSAGE_INVALID_KOUPON);
			response.put("responsCode", IConstant.RESPONSE_INVALID_KOUPON);
		}

		// voucherJson = gson.toJson(response);
		// System.out.println("final json-"+voucherJson);
		// return voucherJson;
		LOGGER.info("===============  VoucharServiceImpl : Inside calculateDiscount :: End  ============= ");

		return response;
	}

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	/*
	 * public Double checkCouponVaidity(String couponCode, Integer merchantId) {
	 * Vouchar vouchar =
	 * voucharRepository.findByVoucharCodeAndMerchantId(couponCode, merchantId);
	 * Date todayDate = new Date(); if (vouchar != null) { if
	 * (vouchar.getVoucharCode().equals(couponCode)) { try { if (vouchar.getDate()
	 * != null) { int status = 0; int timeStatuscount = 0; if
	 * (todayDate.before(vouchar.getDate())) { System.out.println("fixed date");
	 * return vouchar.getDiscount(); } else { if
	 * (todayDate.compareTo(vouchar.getDate()) == 0) { status++; } SimpleDateFormat
	 * sdf = new SimpleDateFormat("HH:mm:ss"); String str = sdf.format(new Date());
	 * boolean timeStatus = VoucharServiceImpl
	 * .isTimeBetweenTwoTime(convert12To24(vouchar.getStartTime()),
	 * convert12To24(vouchar.getEndTime()), str); if (timeStatus) {
	 * timeStatuscount++; } if (status > 0 && timeStatuscount > 0) { return
	 * vouchar.getDiscount(); } } } if (vouchar.getFromDate() != null) { int status
	 * = 0; int timeStatuscount = 0; if (todayDate.after(vouchar.getFromDate()) &&
	 * todayDate.before(vouchar.getEndDate())) { return vouchar.getDiscount(); }
	 * else { if (todayDate.compareTo(vouchar.getEndDate()) == 0) { status++; }
	 * SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss"); String str =
	 * sdf.format(new Date()); boolean timeStatus = VoucharServiceImpl
	 * .isTimeBetweenTwoTime(convert12To24(vouchar.getStartTime()),
	 * convert12To24(vouchar.getEndTime()), str); if (timeStatus) {
	 * timeStatuscount++; } if (status > 0 && timeStatuscount > 0) { return
	 * vouchar.getDiscount(); } } } if (vouchar.getDate() == null &&
	 * vouchar.getFromDate() == null) { Calendar calendar = Calendar.getInstance();
	 * Date date = calendar.getTime(); System.out.println(new
	 * SimpleDateFormat("EEEE", Locale.ENGLISH).format(date.getTime())); String
	 * toDay = new SimpleDateFormat("EEEE", Locale.ENGLISH).format(date.getTime());
	 * if (toDay.equals(vouchar.getRecurringR())) { SimpleDateFormat sdf = new
	 * SimpleDateFormat("HH:mm:ss"); String str = sdf.format(new Date()); boolean
	 * timeStatus = VoucharServiceImpl
	 * .isTimeBetweenTwoTime(convert12To24(vouchar.getStartTime()),
	 * convert12To24(vouchar.getEndTime()), str); if (timeStatus) { return
	 * vouchar.getDiscount(); } else { return null; } } else { return null; } } }
	 * catch (ParseException e) { LOGGER.error("error: " + e.getMessage()); } } else { return null; } }
	 * else { return null; } return null; }
	 */

	public static void main(String[] args) {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
			String str = sdf.format(new Date());
			System.out.println(
					VoucharServiceImpl.isTimeBetweenTwoTime(convert12To24("11:30 AM"), convert12To24("02:02 PM"), str));
			Calendar calendar = Calendar.getInstance();
			Date date = calendar.getTime();
			// 3 letter name form of the day
			System.out.println(new SimpleDateFormat("EE", Locale.ENGLISH).format(date.getTime()));
			// full name form of the day
			System.out.println(new SimpleDateFormat("EEEE", Locale.ENGLISH).format(date.getTime()).toLowerCase());
		} catch (ParseException e) {
			LOGGER.error("error: " + e.getMessage());
		}
	}

	public static boolean isTimeBetweenTwoTime(String initialTime, String finalTime, String currentTime)
			throws ParseException {
		LOGGER.info("===============  VoucharServiceImpl : Inside isTimeBetweenTwoTime :: Start  ============= ");

		LOGGER.info(" =======initialTime ==== " + initialTime + "=======finalTime ====" + finalTime
				+ " =======currentTime ==== " + currentTime);
		String reg = "^([0-1][0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])$";
		if (initialTime.matches(reg) && finalTime.matches(reg) && currentTime.matches(reg)) {
			boolean valid = false;
			// Start Time
			java.util.Date inTime = new SimpleDateFormat("HH:mm:ss").parse(initialTime);
			Calendar calendar1 = Calendar.getInstance();
			calendar1.setTime(inTime);

			// Current Time
			java.util.Date checkTime = new SimpleDateFormat("HH:mm:ss").parse(currentTime);
			Calendar calendar3 = Calendar.getInstance();
			calendar3.setTime(checkTime);

			// End Time
			java.util.Date finTime = new SimpleDateFormat("HH:mm:ss").parse(finalTime);
			Calendar calendar2 = Calendar.getInstance();
			calendar2.setTime(finTime);

			if (finalTime.compareTo(initialTime) < 0) {
				calendar2.add(Calendar.DATE, 1);
				calendar3.add(Calendar.DATE, 1);
			}

			java.util.Date actualTime = calendar3.getTime();
			if ((actualTime.after(calendar1.getTime()) || actualTime.compareTo(calendar1.getTime()) == 0)
					&& actualTime.before(calendar2.getTime())) {
				valid = true;
			}
			LOGGER.info("===============  VoucharServiceImpl : Inside isTimeBetweenTwoTime :: End  ============= ");

			return valid;
		} else {
			LOGGER.error("===============  VoucharServiceImpl : Inside isTimeBetweenTwoTime ::Throw IllegalArgumentException ============= ");

			throw new IllegalArgumentException("Not a valid time, expecting HH:MM:SS format");
		}

	}

	public static String convert12To24(String str) {
		SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm:ss");
		SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm a");
		Date date = null;
		try {
			date = parseFormat.parse(str);
		} catch (ParseException e) {
			LOGGER.error("error: " + e.getMessage());
		}
		return displayFormat.format(date);
	}

	/**
	 * find opening closing hour by merchantId and day
	 */
	public List<OpeningClosingTime> findOpeningClosingHoursByMerchantId(Integer merchantId, String timeZoneCode) {

		LOGGER.info(
				"----------------Start :: VoucherServiceImpl : findOpeningClosingHoursByMerchantId------------------------");

		/*
		 * Calendar calendar = Calendar.getInstance(); calendar.setTimeZone(value); Date
		 * date = calendar.getTime(); String toDay = new SimpleDateFormat("EEEE",
		 * Locale.ENGLISH).format(date.getTime()).toLowerCase();
		 */
		Date date = new Date();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

		LOGGER.info("VoucherServiceImpl :: findOpeningClosingHoursByMerchantId : merchantId " + merchantId);

		// Use Madrid's time zone to format the date in
		df.setTimeZone(TimeZone.getTimeZone(timeZoneCode));

		String toDay = DateUtil.findDayNameFromDate(df.format(date));
		OpeningClosingDay openingClosingDay = openingClosingDayRepository.findByMerchantIdAndDay(merchantId, toDay);
		if (openingClosingDay != null) {
			return openingClosingTimeRepository.findByOpeningClosingDayId(openingClosingDay.getId());
		} else {
			LOGGER.info(
					"----------------End :: VoucherServiceImpl : findOpeningClosingHoursByMerchantId------------------------");
			return null;
		}
	}

	public List<OpeningClosingTime> findNextOpeningClosingHoursByMerchantId(Integer merchantId, String timeZoneCode) {

		LOGGER.info(
				"----------------Start :: VoucherServiceImpl : findNextOpeningClosingHoursByMerchantId------------------------");
		Date date = new Date();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.DATE, 1); // number of days to add
		date = c.getTime();

		df.setTimeZone(TimeZone.getTimeZone(timeZoneCode));
		LOGGER.info("VoucherServiceImpl : findNextOpeningClosingHoursByMerchantId : merchantId " + merchantId);
		String toDay = DateUtil.findDayNameFromDate(df.format(date));
		OpeningClosingDay openingClosingDay = openingClosingDayRepository.findByMerchantIdAndDay(merchantId, toDay);
		if (openingClosingDay != null) {
			LOGGER.info(
					"----------------End :: VoucherServiceImpl : findNextOpeningClosingHoursByMerchantId------------------------");
			return openingClosingTimeRepository.findByOpeningClosingDayId(openingClosingDay.getId());
		} else {
			LOGGER.info(
					"----------------End :: VoucherServiceImpl : findNextOpeningClosingHoursByMerchantId------------------------");
			return null;
		}
	}

	public String findByMerchantIdAndCouponCode(Integer merchantId, String couponCode) {
		LOGGER.info("===============  VoucharServiceImpl : Inside findByMerchantIdAndCouponCode :: Start  ============="
				+ " merchantId "+merchantId+" couponCode "+couponCode);

		Vouchar vouchar = voucharRepository.findByVoucharCodeAndMerchantId(couponCode, merchantId);
		if (vouchar != null) {
			LOGGER.info("===============  VoucharServiceImpl : Inside findByMerchantIdAndCouponCode :: End  =============");
			return "true";
		} else {
			LOGGER.info("===============  VoucharServiceImpl : Inside findByMerchantIdAndCouponCode :: End  =============");

			return "false";
		}
	}

	public String findByMerchantIdAndCouponCodeAndCouponId(Integer merchantId, String couponCode, Integer couponId) {
		LOGGER.info("===============  VoucharServiceImpl : Inside findByMerchantIdAndCouponCodeAndCouponId :: Start  ============="
				+ " merchantId "+merchantId+" couponCode "+couponCode+" couponId "+couponId);
		Vouchar vouchar = voucharRepository.findByVoucharCodeAndMerchantId(couponCode, merchantId);
		if (vouchar != null) {
			if (vouchar.getId() != couponId){
				LOGGER.info("===============  VoucharServiceImpl : Inside findByMerchantIdAndCouponCodeAndCouponId :: End  =============");
				return "true";
				}
			else {
				LOGGER.info("===============  VoucharServiceImpl : Inside findByMerchantIdAndCouponCodeAndCouponId :: End  =============");

				return "false";
		}
		} else {
			LOGGER.info("===============  VoucharServiceImpl : Inside findByMerchantIdAndCouponCodeAndCouponId :: End  =============");
			return "false";
		}
	}

}
