package com.foodkonnekt.serviceImpl;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.merchantwarehouse.schemas.merchantware._40.Credit.CayanTest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;
import org.springframework.web.multipart.MultipartFile;

import com.foodkonnekt.model.Merchant;
import com.foodkonnekt.model.MerchantSliders;
import com.foodkonnekt.repository.MerchantSliderRepository;
import com.foodkonnekt.service.MerchantSliderService;
import com.foodkonnekt.util.DateUtil;
import com.foodkonnekt.util.IConstant;
import com.foodkonnekt.util.UrlConstant;

@Service
public class MerchantSliderServiceImpl implements MerchantSliderService{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(MerchantSliderServiceImpl.class);
	
	@Autowired
    private Environment environment;
	
	@Autowired
	private MerchantSliderRepository merchantSliderRepository;
	
	public void saveMerchantSliderValue(Merchant merchant,MultipartFile file){
		if(merchant!=null){
		MerchantSliders merchantSliders=new MerchantSliders();
		
		if (!(file.getSize() == 0)) {
			// images
			Long size = file.getSize();
			if (size >= 2097152) {
				// model.addAttribute("filesize", "Maximum Allowed File Size
				// is 2 MB");
				// return "uploadLogo";
			}

			String orgName = file.getOriginalFilename();

			String exts[] = orgName.split("\\.");

			String ext = exts[1];
			String logoName = exts[0]+ "."
					+ ext;
			// String logoName = item.getId() + "_" + "." + ext;
			String filePath = environment.getProperty("ADMIN_SERVER_LOGO_PATH") + logoName;
			File dest = new File(filePath);
			try {
				file.transferTo(dest);
			} catch (IllegalStateException e) {
				LOGGER.error("error: " + e.getMessage());
				// return "File uploaded failed:" + orgName;
			} catch (IOException e) {
				LOGGER.error("error: " + e.getMessage());
				// return "File uploaded failed:" + orgName;
			}
			System.out.println("File uploaded:" + orgName);
			merchantSliders.setSliderImage(environment.getProperty("ADMIN_LOGO_PATH_TO_SHOW") + logoName);
			merchantSliders.setMerchant(merchant);
			
				Date date = (merchant != null && merchant.getTimeZone() != null
						&& merchant.getTimeZone().getTimeZoneCode() != null)
								? DateUtil.getCurrentDateForTimeZonee(merchant.getTimeZone().getTimeZoneCode())
								: new Date();
			DateFormat dateFormat = new SimpleDateFormat(IConstant.YYYYMMDDHHMMSS);
			String createdDate = dateFormat.format(date).toString();
			
			merchantSliders.setCreatedDate(createdDate);
			merchantSliderRepository.save(merchantSliders);
			
			
		}
	  }
	}

	public List<MerchantSliders> findByMerchantId(Integer merchantId) {
		List<MerchantSliders> merchantSlidersList=merchantSliderRepository.findByMerchantId(merchantId);
		return merchantSlidersList;
	}

	public Integer findSliderImageCountByMerchantId(Integer merchantId) {
		Integer imageCount=merchantSliderRepository.findSliderImageCountByMerchantId(merchantId);
		return imageCount;
	}

	public MerchantSliders findById(Integer sliderId) {
		MerchantSliders merchantSliders= merchantSliderRepository.findById(sliderId);
		return merchantSliders;
	}

	public void deleteItemImage(MerchantSliders merchantSliders) {
		merchantSliderRepository.delete(merchantSliders);
		
	}

}
