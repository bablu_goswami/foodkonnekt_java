package com.foodkonnekt.serviceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.foodkonnekt.model.OrderNotification;
import com.foodkonnekt.repository.OrderNotificationRepository;
import com.foodkonnekt.service.OrderNotificationService;

@Service
public class OrderNotificationServiceImpl implements OrderNotificationService {

	@Autowired
	private OrderNotificationRepository orderNotificationRepository;
	
	public OrderNotification findMerchantId(Integer merchantId) {
		
		return orderNotificationRepository.findByMerchantId(merchantId);
		
	}

	public OrderNotification save(OrderNotification orderNotification) {

		return orderNotificationRepository.save(orderNotification);
	}

}
