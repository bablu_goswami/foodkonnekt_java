package com.foodkonnekt.clover.vo;

import java.util.List;

import javax.persistence.Column;

public class KritiqCustomerVO {

	private String dateOfReview;
	private String customerName;
	private String customerEmailId;
	private String phoneNo;
	private String customerComments;
	private String merchantName;
	private String response;
	private String responseDate;
	private String responseStatus;
	
	public String getMerchantName() {
		return merchantName;
	}
	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}
	public String getCustomerComments() {
		return customerComments;
	}
	public void setCustomerComments(String customerComments) {
		this.customerComments = customerComments;
	}
	public String getCustomerEmailId() {
		return customerEmailId;
	}
	public void setCustomerEmailId(String customerEmailId) {
		this.customerEmailId = customerEmailId;
	}
	public String getPhoneNo() {
		return phoneNo;
	}
	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}
	private Double avgRating;
	private List<KritiqQuestionAnswersVO> kritiqQuestionAnswersVOs;
	public List<KritiqQuestionAnswersVO> getKritiqQuestionAnswersVOs() {
		return kritiqQuestionAnswersVOs;
	}
	public void setKritiqQuestionAnswersVOs(
			List<KritiqQuestionAnswersVO> kritiqQuestionAnswersVOs) {
		this.kritiqQuestionAnswersVOs = kritiqQuestionAnswersVOs;
	}
	public String getDateOfReview() {
		return dateOfReview;
	}
	public void setDateOfReview(String dateOfReview) {
		this.dateOfReview = dateOfReview;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public Double getAvgRating() {
		return avgRating;
	}
	public void setAvgRating(Double avgRating) {
		this.avgRating = avgRating;
	}
	public String getResponse() {
		return response;
	}
	public void setResponse(String response) {
		this.response = response;
	}
	
	public String getResponseDate() {
		return responseDate;
	}
	public void setResponseDate(String responseDate) {
		this.responseDate = responseDate;
	}
	public String getResponseStatus() {
		return responseStatus;
	}
	public void setResponseStatus(String responseStatus) {
		this.responseStatus = responseStatus;
	}
	
}
