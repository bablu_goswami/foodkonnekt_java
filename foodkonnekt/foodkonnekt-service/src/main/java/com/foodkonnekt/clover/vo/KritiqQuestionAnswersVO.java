package com.foodkonnekt.clover.vo;

public class KritiqQuestionAnswersVO {
	
	public String getQuestion() {
		return question;
	}
	public void setQuestion(String question) {
		this.question = question;
	}
	public Integer getAnswer() {
		return answer;
	}
	public void setAnswer(Integer answer) {
		this.answer = answer;
	}
	private String question;
	private Integer answer;

}
