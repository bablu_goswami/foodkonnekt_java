package com.foodkonnekt.controller;

import java.awt.Polygon;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.httpclient.util.URIUtil;
//import org.apache.commons.httpclient.util.URIUtil;
import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.foodkonnekt.model.Address;
import com.foodkonnekt.model.Customer;
import com.foodkonnekt.model.Item;
import com.foodkonnekt.model.LocationDto;
import com.foodkonnekt.model.Merchant;
import com.foodkonnekt.model.MerchantConfiguration;
import com.foodkonnekt.model.MerchantLogin;
import com.foodkonnekt.model.Pos;
import com.foodkonnekt.model.Role;
import com.foodkonnekt.model.Vendor;
import com.foodkonnekt.model.VirtualFund;
import com.foodkonnekt.model.Zone;
import com.foodkonnekt.repository.VirtualFundRepository;
import com.foodkonnekt.service.BusinessService;
import com.foodkonnekt.service.CategoryService;
import com.foodkonnekt.service.ItemService;
import com.foodkonnekt.service.MerchantService;
import com.foodkonnekt.service.ZoneService;
import com.foodkonnekt.util.DateUtil;
import com.foodkonnekt.util.EncryptionDecryptionUtil;
import com.foodkonnekt.util.IConstant;
import com.foodkonnekt.util.MailSendUtil;
import com.foodkonnekt.util.UrlConstant;
import com.google.gson.Gson;
//import com.google.maps.model.LatLng;

@Controller
public class MerchantController {

	private static final Logger LOGGER = LoggerFactory.getLogger(MerchantController.class);

	@Autowired
    private Environment environment;
	
	@Autowired
	private VirtualFundRepository virtualFundRepository;
	
	@Autowired
	private MerchantService merchantService;

	@Autowired
	private BusinessService businessService;

	@Autowired
	CategoryService categoryService;

	@Autowired
	private ZoneService zoneService;

	@Autowired
	ItemService itemService;
	private static int EXPIRY_TIME = 30 * 60 * 1000;

//https://www.foodkonnekt.com/sssfsfs?tabs_added%5B644460505599545%5D=1#_=_

	@RequestMapping(value = "/fbUri", method = RequestMethod.GET)
	public String faceBookUri(ModelMap model, HttpServletRequest request,
			@RequestParam(required = false) String tabs_added) {
		LOGGER.info("===============  MerchantController : Inside faceBookUri :: Start  ============= ");

		// LOOGER.log("start: controllername servicename merchantid date")
		HttpSession session = request.getSession();
		String pageId = request.getQueryString();
		try {
			if (pageId != null) {
				pageId = URLDecoder.decode(pageId, "UTF-8");
				pageId = pageId.replaceFirst("tabs_added", "").replaceFirst("=1", "");
				pageId = pageId.replace("[", "").replace("]", "");
			}

			//
			if (session != null) {
				Merchant merchant = (Merchant) session.getAttribute("merchant");
				if (merchant != null && merchant.getId() != null) {
					merchant = merchantService.findById(merchant.getId());
					merchant.setIsFBAppPublished(true);
					merchant.setfBTabIds(pageId);
					merchant = merchantService.save(merchant);
					session.setAttribute("merchant", merchant);
					// return "redirect:http://www.facebook.com/"+pageId+"/app/608947722608987/";
					LOGGER.info("===============  MerchantController : Inside faceBookUri :: End  ============= ");

					return "redirect:" + environment.getProperty("BASE_URL") + "/onLineOrderLink";
				}
			}
		} catch (Exception e) {
			LOGGER.error("===============  MerchantController : Inside faceBookUri :: Exception  ============= " + e);

			// TODO Auto-generated catch block
			LOGGER.error("error: " + e.getMessage());
			MailSendUtil.sendExceptionByMail(e,environment);
		}
		LOGGER.info("===============  MerchantController : Inside faceBookUri :: End  ============= ");

		return "redirect:https://www.foodkonnekt.com";

	}

	@RequestMapping(value = "/fbCallbackUrl", method = RequestMethod.GET)
	@ResponseBody
	public String fbCallbackUrl(HttpServletRequest request) {
LOGGER.info("===============  MerchantController : Inside fbCallbackUrl :: Start  ============= ");

		String callBackResponse = request.getQueryString();
		String[] parameters = callBackResponse.split("&");
		String challenge = "";
		for (String parameter : parameters) {
			String[] params = parameter.split("=");
			if (params != null && params.length > 1) {
				if (params[0].equals("hub.challenge")) {
					challenge = params[1];
				}
			}
		}
		// MailSendUtil.webhookMail("callBackResponse", "callBackResponse-->
		// "+callBackResponse+" challenge-->"+challenge);
		LOGGER.info("===============  MerchantController : Inside fbCallbackUrl :: End  ============= ");

		return challenge;
	}

	@RequestMapping(value = "/updateMerchant", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, Object> updateMerchant(HttpServletRequest request,
			@RequestParam(required = true) String locationUid, @RequestParam(required = true) String deviceId,
			@RequestParam(required = false) String mobileIMEICode) {
		Map<String, Object> responseJson = new HashMap<String, Object>();
		try {
			LOGGER.info("===============  MerchantController : Inside updateMerchant :: Start  ============= ");

			if (locationUid != null && locationUid != "" && !locationUid.isEmpty()) {
				Merchant merchant = merchantService.findByMerchantUid(locationUid);
				if (merchant != null && merchant.getIsInstall() != null
						&& merchant.getIsInstall() == IConstant.BOOLEAN_TRUE) {
					if (deviceId != null) {
						merchant = merchantService.updateMerchantDeviceDetail(merchant, deviceId, mobileIMEICode);
						responseJson.put("result", true);
						responseJson.put("merchantId", merchant.getId());
						responseJson.put("deviceId", merchant.getModileDeviceId());
						responseJson.put("locationUid", merchant.getMerchantUid());
					}
				} else {
					responseJson.put("result", false);
				}
			} else {
				responseJson.put("result", false);
			}

		} catch (Exception e) {
			responseJson.put("result", false);
			LOGGER.error("===============  MerchantController : Inside updateMerchant :: Exception  ============= " + e);

			MailSendUtil.sendExceptionByMail(e,environment);
		}
		LOGGER.info("===============  MerchantController : Inside updateMerchant :: End  ============= ");

		return responseJson;
	}

	@RequestMapping(value = "/logOutFromApp", method = RequestMethod.GET, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> logOutFromApp(HttpServletRequest request,
			@RequestParam(required = true) String locationUid) {
		Map<String, Object> responseJson = new HashMap<String, Object>();
		try {
			LOGGER.info("===============  MerchantController : Inside logOutFromApp :: Start  ======= locationUid = "+locationUid);

			if (locationUid != null && locationUid != "" && !locationUid.isEmpty()) {
				Merchant merchant = merchantService.findByMerchantUid(locationUid);
				LOGGER.info("===== MerchantController : Inside logOutFromApp :: merchant  == " + merchant.getId());

				if (merchant != null && merchant.getIsInstall() != null
						&& merchant.getIsInstall() == IConstant.BOOLEAN_TRUE) {

					merchant = merchantService.updateMerchantDeviceDetail(merchant, "", "");
					responseJson.put("result", true);
					responseJson.put("merchantId", merchant.getId());
					responseJson.put("deviceId", merchant.getModileDeviceId());
					responseJson.put("locationUid", merchant.getMerchantUid());

				} else {
					responseJson.put("result", false);
				}
			} else {
				responseJson.put("result", false);
			}

		} catch (Exception e) {
			responseJson.put("result", false);
			MailSendUtil.sendExceptionByMail(e,environment);
		}
		LOGGER.info("===============  MerchantController : Inside logOutFromApp :: End  ============= ");

		return responseJson;
	}

	@RequestMapping(value = "/fbCallbackUrl", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public void fbCallbackUrlPost(HttpServletRequest request, @RequestBody(required = false) String signed_request) {

		MailSendUtil.webhookMail("FB webhook signed_request", "signed_request--> " + signed_request,environment);

	}

	/**
	 * Find address by merchantId
	 * 
	 * @param merchantId
	 * @return List<Address>
	 */
	@RequestMapping(value = "/getMerchantLocationDB", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Map<Object, Object> getAddress(@RequestParam(required = true) Integer merchantId) {
		Map<Object, Object> locationMap = new HashMap<Object, Object>();
		try {
			LOGGER.info("===============  MerchantController : Inside getAddress :: Start  ============= ");

			List<Address> addresses = merchantService.findAddressByMerchantId(merchantId);
			if (addresses != null && !addresses.isEmpty()) {
				locationMap.put(IConstant.RESPONSE, IConstant.RESPONSE_SUCCESS_MESSAGE);
				locationMap.put(IConstant.DATA, addresses);
			} else {
				locationMap.put(IConstant.RESPONSE, IConstant.RESPONSE_NO_DATA_MESSAGE);
				locationMap.put(IConstant.DATA, addresses);
			}
		} catch (Exception e) {
			if (e != null) {
				LOGGER.error("===============  MerchantController : Inside getAddress :: Exception  ============= " + e);

				MailSendUtil.sendExceptionByMail(e,environment);

				LOGGER.error("error: " + e.getMessage());

			}
		}
		LOGGER.info("===============  MerchantController : Inside getAddress :: End  ============= ");

		return locationMap;
	}

	/**
	 * Add merchant logo
	 * 
	 * @param merchant
	 * @return
	 */
	@RequestMapping(value = "/updateMerchanrLogo", method = RequestMethod.POST)
	@ResponseBody
	public Map<Object, Object> addLogo(@RequestBody Merchant merchant) {
		Map<Object, Object> logoMap = new HashMap<Object, Object>();
		try {
			LOGGER.info("===============  MerchantController : Inside addLogo :: Start  ============= ");

			Merchant result = merchantService.addMerchantLogo(merchant);
			if (result != null) {
				logoMap.put(IConstant.RESPONSE, IConstant.RESPONSE_SUCCESS_MESSAGE);
				logoMap.put(IConstant.MESSAGE, "Merchant Logo add successfully");
			} else {
				logoMap.put(IConstant.RESPONSE, IConstant.RESPONSE_NO_DATA_MESSAGE);
				logoMap.put(IConstant.MESSAGE, "Merchant not found");
			}
		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
			}
			LOGGER.error("===============  MerchantController : Inside addLogo :: Exception  ============= " + e);

			LOGGER.error("error: " + e.getMessage());
		}
		return logoMap;
	}

	/**
	 * Find merchant logo by merchant Id
	 * 
	 * @param merchantId
	 * @return Merchant
	 */

	@RequestMapping(value = "/signup", method = RequestMethod.GET)
	public String signup(@ModelAttribute("Merchant") Merchant merchant, ModelMap model, HttpServletRequest request,
			@RequestParam(required = false) String saveStatus) {

		model.addAttribute("Merchant", new Merchant());
		return "signup";
	}

	@RequestMapping(value = "/saveMerchant", method = RequestMethod.POST)
	public String saveMerchant(@ModelAttribute("Merchant") Merchant merchant, ModelMap model,
			HttpServletRequest request, @RequestParam(required = false) String saveStatus) {
		LOGGER.info("===============  MerchantController : Inside saveMerchant :: Start  ============= ");

		merchant.getMerchantLogin()
				.setPassword(EncryptionDecryptionUtil.encryptString(merchant.getMerchantLogin().getPassword()));
		Vendor vendor = new Vendor();
		vendor.setEmail(merchant.getMerchantLogin().getEmailId());
		vendor.setName(merchant.getName());
		Pos pos = new Pos();
		Role role = new Role();
		role.setId(3);
		pos.setPosId(3);
		vendor.setPos(pos);
		vendor.setRole(role);
		merchant.setOwner(vendor);
		merchant.setIsInstall(1);
		merchant.getMerchantLogin().setMerchant(merchant);
		Merchant result = merchantService.save(merchant);
		businessService.saveDefaultBusinessHours(result);
		businessService.saveDefaultPaymentMode(result);
		HttpSession session = request.getSession();
		// String image = ImageUploadUtils.getImage(file);
		session.setAttribute("merchant", result);
		String merchId = result.getId().toString();
		String base64encode = EncryptionDecryptionUtil.encryption(merchId);
		String merchantName = merchant.getName().replaceAll("[^a-zA-Z0-9]", "");
		String orderLink = environment.getProperty("WEB_BASE_URL") + "/" + merchantName + "/clover/" + base64encode;

		session.setAttribute("onlineOrderLink", orderLink);
LOGGER.info("===============  MerchantController : Inside saveMerchant :: End  ============= ");

		return "uploadLogo";
	}

	@RequestMapping(value = "/getMerchantAndLogo", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Map<Object, Object> getMerchantLogo(@RequestParam(required = true) Integer merchantId) {
		
		LOGGER.info("===============  MerchantController : Inside getMerchantLogo :: Start  ============= ");

		Map<Object, Object> merchantMap = new HashMap<Object, Object>();
		try {
			Merchant merchant = merchantService.findByMerchantId(merchantId);
			if (merchant != null) {
				merchantMap.put(IConstant.RESPONSE, IConstant.RESPONSE_SUCCESS_MESSAGE);
				merchantMap.put(IConstant.DATA, merchant);
			} else {
				merchantMap.put(IConstant.RESPONSE, IConstant.RESPONSE_NO_DATA_MESSAGE);
				merchantMap.put(IConstant.MESSAGE, "Merchant not found");
			}
		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
			}
			LOGGER.error("===============  MerchantController : Inside getMerchantLogo :: Exception  ============= " + e);

			LOGGER.error("error: " + e.getMessage());
		}
		LOGGER.info("===============  MerchantController : Inside getMerchantLogo :: End  ============= ");

		return merchantMap;
	}

	@RequestMapping(value = "/checkDuplicateMerchant", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public boolean checkDuplicateMerchant(@RequestParam(required = true) String emailId) {
		// Map<Object, Object> merchantMap = new HashMap<Object, Object>();
		try {
			LOGGER.info("===============  MerchantController : Inside checkDuplicateMerchant :: Start  ============= ");

			MerchantLogin merchantLogin = merchantService.findByMerchantEmailId(emailId);
            return merchantLogin != null;
		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
			}
			LOGGER.error("===============  MerchantController : Inside checkDuplicateMerchant :: Exception  ============= " + e);

			LOGGER.error("error: " + e.getMessage());
			return false;
		}

	}

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String login(@ModelAttribute("MerchantLogin") MerchantLogin merchantLogin, ModelMap model,
			HttpServletRequest request) {

		return "login";
	}

	@RequestMapping(value = "/forgotpassword", method = RequestMethod.GET)
	public String forgotpassword(@ModelAttribute("MerchantLogin") MerchantLogin merchantLogin, ModelMap model,
			HttpServletRequest request) {

		return "forgotpassword";
	}

	@RequestMapping(value = "/merchantForgotPassword", method = RequestMethod.POST)
	public String merchantForgotPassword(@ModelAttribute("MerchantLogin") MerchantLogin merchantLogin, ModelMap model,
			HttpServletRequest request) {
		try {
			LOGGER.info("===============  MerchantController : Inside merchantForgotPassword :: Start  ============= ");

			boolean status = merchantService.findByEmailId(merchantLogin.getEmailId());
			if (status) {
				model.addAttribute("response",
						"Please check your inbox - we have sent you an email with instructions.");
			} else {
				model.addAttribute("response", "Email does not exist! Please sing up.");
			}
		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
			}
			LOGGER.error("===============  MerchantController : Inside merchantForgotPassword :: Exception  ============= " + e);

			LOGGER.error("error: " + e.getMessage());
		}
LOGGER.info("===============  MerchantController : Inside merchantForgotPassword :: End  ============= ");

		// return "forgotpassword";
		return "redirect:" + environment.getProperty("BASE_URL") + "/forgotpassword";
	}

	/*
	 * @RequestMapping(value = "/changepassword", method = RequestMethod.GET) public
	 * String changepassword(@ModelAttribute("MerchantLogin") MerchantLogin
	 * merchantLogin, ModelMap model, HttpServletRequest request) {
	 * 
	 * return "changepassword"; }
	 */

	@RequestMapping(value = "/changepassword", params = { "email", "merchantId" }, method = RequestMethod.GET)
	public String changepassword(Map<String, Object> map, ModelMap model, HttpServletRequest request,
			@RequestParam(value = "email") String email, @RequestParam(value = "merchantId") String merchantId,
			@RequestParam(value = "tLog") String tLog) {

		try {
			LOGGER.info("===============  MerchantController : Inside changepassword :: Start  ============= ");

			Long time = Long.valueOf(EncryptionDecryptionUtil.decryption(tLog));
			Long currentTime = System.currentTimeMillis();
			int diff = (int) (currentTime - time);

			if (diff < EXPIRY_TIME) {
LOGGER.info("===============  MerchantController : Inside changepassword ::inside if (diff < EXPIRY_TIME)");

				MerchantLogin merchantLogin = merchantService.findByMerchantEmailId(email);
				if (merchantLogin != null && merchantId
						.equalsIgnoreCase(EncryptionDecryptionUtil.encryptString(merchantLogin.getId() + ""))) {
					merchantLogin.setPassword("");
					model.addAttribute("MerchantLogin", merchantLogin);
					LOGGER.info("===============  MerchantController : Inside changepassword :: End  ============= ");

					return "changepassword";

				} else {
					map.put("MerchantLogin", new MerchantLogin());
					LOGGER.info("===============  MerchantController : Inside changepassword :: End  ============= ");
					LOGGER.info("===============  MerchantController : Inside changepassword :: End  ============= ");

					return "forgotpassword";
				}

			} else {
				LOGGER.info("Link is expired");
				model.addAttribute("response", "Link is expired !Please try again.");
				map.put("MerchantLogin", new MerchantLogin());
				LOGGER.info("===============  MerchantController : Inside changepassword :: End  ============= ");

				return "redirect:" + environment.getProperty("BASE_URL") + "/forgotpassword";
				// return "forgotpassword";
			}
		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
			}
			LOGGER.error("error: " + e.getMessage());
			LOGGER.error("===============  MerchantController : Inside changepassword :: Exception  ============= " + e);

		}

		return null;
	}

	@RequestMapping(value = "/resetMerchantPassword", method = RequestMethod.POST)
	public String resetMerchantPassword(@ModelAttribute("MerchantLogin") MerchantLogin merchantLogin, ModelMap model,
			HttpServletResponse response, HttpServletRequest request) {

		Map<Object, Object> loginMap = new HashMap<Object, Object>();
		try {
			LOGGER.info("===============  MerchantController : Inside resetMerchantPassword :: Start  ============= ");

			MerchantLogin result = merchantService.findByMerchantEmailId(merchantLogin.getEmailId());
			if (result != null) {
				if (result.getEmailId().equals(merchantLogin.getEmailId())) {
					if (merchantLogin.getPassword() != null && !merchantLogin.getPassword().isEmpty()) {
						result.setPassword(EncryptionDecryptionUtil.encryptString(merchantLogin.getPassword()));

						merchantService.saveMerchantLogin(result);
					}
				}

				model.addAttribute("loginresponse", "Your password has been reset.");
				LOGGER.info("===============  MerchantController : Inside resetMerchantPassword :: End  ============= ");

				return "redirect:" + environment.getProperty("BASE_URL") + "/login";
			} else {

				model.addAttribute("loginresponse", "Invalid Emailid or Password ! Please try again");
				LOGGER.info("===============  MerchantController : Inside resetMerchantPassword :: End  ============= ");

				return "redirect:" + environment.getProperty("BASE_URL") + "/login";
			}
		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
			}
			LOGGER.error("===============  MerchantController : Inside resetMerchantPassword :: Exception  ============= " + e);

			LOGGER.error("error: " + e.getMessage());
			model.addAttribute("loginresponse", "Something went wrong there! Please try again");
			return "redirect:" + environment.getProperty("BASE_URL") + "/login";
		}

	}

	@RequestMapping(value = "/merchantLogin", method = RequestMethod.POST)
	public String merchantLogin(@ModelAttribute("MerchantLogin") MerchantLogin merchantLogin, ModelMap model,
			HttpServletResponse response, HttpServletRequest request) {

		Map<Object, Object> loginMap = new HashMap<Object, Object>();
		try {
			LOGGER.info("===============  MerchantController : Inside merchantLogin :: Start  ============= ");

			MerchantLogin result = merchantService.findByEmailAndPassword(merchantLogin.getEmailId(),
					EncryptionDecryptionUtil.encryptString(merchantLogin.getPassword()));
			if (result != null) {
LOGGER.info("===============  MerchantController : Inside merchantLogin : inside if (result != null)  ");

				Merchant merchant = result.getMerchant();

				HttpSession session = request.getSession();

				session.setAttribute("merchant", merchant);
				String merchId = merchant.getId().toString();
				String base64encode = EncryptionDecryptionUtil.encryption(merchId);
				String merchantName = merchant.getName().replaceAll("[^a-zA-Z0-9]", "");
				String orderLink = environment.getProperty("WEB_BASE_URL") + "/" + merchantName + "/clover/" + base64encode;

				session.setAttribute("onlineOrderLink", orderLink);
				LOGGER.info("===============  MerchantController : Inside merchantLogin :: End  ============= ");

				return "redirect:" + environment.getProperty("BASE_URL") + "/adminHome";
			} else {

				model.addAttribute("loginresponse", "Invalid Emailid or Password ! Please try again");
				LOGGER.info("===============  MerchantController : Inside merchantLogin :: End  ============= ");

				return "redirect:" + environment.getProperty("BASE_URL") + "/login";
			}
		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
			}
			LOGGER.error("error: " + e.getMessage());
			LOGGER.error("===============  MerchantController : Inside merchantLogin :: Exception  ============= " + e);

			model.addAttribute("loginresponse", "Something went wrong there! Please try again");
			return "redirect:" + environment.getProperty("BASE_URL") + "/login";
		}

	}

	/**
	 * Get all categories *
	 * 
	 * @return Map<Object, Object>
	 */
	@RequestMapping(value = "getAllItems", method = RequestMethod.GET)
	public @ResponseBody Map<Object, Object> getAllItemsByMerchantUId(@RequestParam("merchantUId") String merchantUId,
			HttpServletResponse response) {
		LOGGER.info("===============  MerchantController : inside getAllItemsByMerchantUId : start ====");
		Map<Object, Object> allItemsMap = new HashMap<Object, Object>();
		List<Item> allItems = itemService.getAllItemsByMerchantUId(merchantUId);
		Gson gson = new Gson();
		// String eventTypesJson = gson.toJson(allItems);
		if (!allItems.isEmpty()) {
			allItemsMap.put(IConstant.RESPONSE, IConstant.RESPONSE_SUCCESS_MESSAGE);
			allItemsMap.put(IConstant.DATA, allItems);
		} else {
			allItemsMap.put(IConstant.RESPONSE, IConstant.RESPONSE_NO_DATA_MESSAGE);
			allItemsMap.put(IConstant.DATA, allItems);
		}
		LOGGER.info("===============  MerchantController : Inside getAllItemsByMerchantUId :: End  ============= ");

		return allItemsMap;
	}

	/**
	 * Get all categories *
	 * 
	 * @return Map<Object, Object>
	 */
	@RequestMapping(value = "getAllItemsByVendorUId", method = RequestMethod.GET)
	public @ResponseBody Map<Object, Object> getAllItemsByVenderUId(@RequestParam("vendorUId") String vendorUId,
			HttpServletResponse response) {
		LOGGER.info("===============  MerchantController : inside getAllItemsByVenderUId : start ====");
		Map<Object, Object> allItemsMap = new HashMap<Object, Object>();
		List<Item> allItems = itemService.getAllItemsByVenderUId(vendorUId);
		Gson gson = new Gson();
		String eventTypesJson = gson.toJson(allItems);
		if (!allItems.isEmpty()) {
			allItemsMap.put(IConstant.RESPONSE, IConstant.RESPONSE_SUCCESS_MESSAGE);
			allItemsMap.put(IConstant.DATA, eventTypesJson);
		} else {
			allItemsMap.put(IConstant.RESPONSE, IConstant.RESPONSE_NO_DATA_MESSAGE);
			allItemsMap.put(IConstant.DATA, eventTypesJson);
		}
		LOGGER.info("===============  MerchantController : Inside getAllItemsByVenderUId :: End  ============= ");

		return allItemsMap;
	}

	@RequestMapping(value = "/installation", method = RequestMethod.GET)
	@ResponseBody
	public Map<Object, Object> installation(@RequestParam("merchantUid") String merchantUid,
			@RequestParam("locationUid") String locationUid, @RequestParam("productUid") String productUid) {
		Map<Object, Object> logoMap = new HashMap<Object, Object>();
		try {
			LOGGER.info("===============  MerchantController : Inside installation :: Start  ============= merchantUid "+merchantUid);

			if (merchantUid != null && locationUid != null) {

				Merchant merchant = merchantService.getMerchantByMerchantPsoId(merchantUid);

				if (merchant != null) {
					merchantService.updateMerchantUid(merchant, locationUid);
					logoMap.put(IConstant.RESPONSE, IConstant.RESPONSE_SUCCESS_MESSAGE);
					logoMap.put(IConstant.MESSAGE, "Merchant UPDATE successfully");
				} else {
					logoMap.put(IConstant.RESPONSE, IConstant.RESPONSE_NO_DATA_MESSAGE);
					logoMap.put(IConstant.MESSAGE, "Merchant not found");
				}
			} else {
				logoMap.put(IConstant.RESPONSE, IConstant.RESPONSE_NO_DATA_MESSAGE);
				logoMap.put(IConstant.MESSAGE, "Merchant not found");
			}
		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
			}
			LOGGER.error("===============  MerchantController : Inside installation :: Exception  ============= " + e);

			LOGGER.error("error: " + e.getMessage());
		}
		LOGGER.info("===============  MerchantController : Inside installation :: End  ============= ");

		return logoMap;
	}

	@RequestMapping(value = "/updateMerchantSubscrition", method = RequestMethod.POST)
	// public String findMerchant(@ModelAttribute("MerchantLogin") MerchantLogin
	// merchantLogin, ModelMap model,HttpServletResponse response,HttpServletRequest
	// request) {

	public @ResponseBody String findMerchant(@RequestBody LocationDto location,
			@RequestParam("merchantUid") String merchantUid, @RequestParam("status") String status) {

		String result = null;
		// Map<Object, Object> loginMap = new HashMap<Object, Object>();
		try {
			LOGGER.info("===============  MerchantController : Inside findMerchant :: Start  ============= ");

			Merchant merchantData = merchantService.findByMerchantUid(merchantUid);
			LOGGER.info("merchant - " + merchantData.getId());
			if (merchantData != null) {
				LOGGER.info("merchantId in not null condition-" + merchantData.getId());
				result = merchantService.getMerchantSubscription(merchantData.getId(), status);
				LOGGER.info("result--" + result);
			} else {
				LOGGER.info("in else condition");
				result = merchantService.createMerchantViaClientModuleLocation(location);

			}
		} catch (Exception e) {
			LOGGER.error("===============  MerchantController : Inside findMerchant :: Exception  ============= " + e);

			LOGGER.error("error: " + e.getMessage());
		}
LOGGER.info("===============  MerchantController : Inside findMerchant :: End  ============= ");

		return result;
	}

	@RequestMapping(value = "deliveryZonesWithMap", method = RequestMethod.GET)
	public String googleMap(@RequestParam(required = true) String locationAddress, ModelMap model) {
		try {
			LOGGER.info("===============  MerchantController : Inside googleMap :: Start  ============= ");

			// String addres = "Sayaji Hotel, Near balewadi stadium, pune";
			URL url = new URL(
					"https://maps.googleapis.com/maps/api/geocode/json?address=" + URIUtil.encodeQuery(locationAddress)
							+ "&sensor=true&key=AIzaSyDjnluKhome9lmt5LLKnIgqot7HtyDetes");
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");

			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
			}
			BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

			String output = "", full = "";
			// LOGGER.info("output-###-");
			while ((output = br.readLine()) != null) {

				// LOGGER.info(output);
				full += output;
			}
			if (full != "") {
				JSONObject jsonObject = new JSONObject(full);
				if (jsonObject != null && jsonObject.toString().contains("results")) {
					JSONArray result = (JSONArray) jsonObject.getJSONArray("results");
					LOGGER.info("result.length()---" + result.length());
					if (result.length() > 0 && result.toString().contains("geometry")) {
						for (int i = 0; i < result.length(); i++) {
							LOGGER.info("##############");
							JSONObject viewPort = (JSONObject) result.getJSONObject(i).get("geometry");
							if (viewPort != null && viewPort.toString().contains("location")) {
								JSONObject location = (JSONObject) viewPort.get("location");
								if (location != null && location.toString().contains("lng")
										&& location.toString().contains("lat")) {
									LOGGER.info(location.get("lng") + "---###----" + location.get("lat"));
									model.addAttribute("latitude", location.get("lat"));
									model.addAttribute("longitude", location.get("lng"));

									// location.getString("lng");
									// location.getString("lat");
									location.getInt("lng");
									location.getInt("lat");
								}
							}
						}
					}
				}
			}
		} catch (Exception e) {
			LOGGER.error("===============  MerchantController : Inside googleMap :: Exception  ============= " + e);

			LOGGER.error("error: " + e.getMessage());
		}
		LOGGER.info("===============  MerchantController : Inside googleMap :: End  ============= ");

		return "deliveryZonesWithMap";
	}

	@RequestMapping(value = "getDeliveryZonePolygones", method = RequestMethod.GET)
	@ResponseBody
	String getDeliveryZonePolygonesForMerchant(@RequestParam("merchantId") Integer merchantId,
			@RequestParam("polygones") String polygones) {
		LOGGER.info("===============  MerchantController : Inside getDeliveryZonePolygonesForMerchant :: Start  =============merchantId "+merchantId+" polygones "+polygones);

		String[] polyArray = polygones.split(",");
		String[] poly = new String[polyArray.length];
		String p = null;
		int j = 0;
		for (int i = 0; i < polyArray.length; i++) {
			p = polyArray[i] + "," + polyArray[i + 1];
			poly[j] = p;
			i = i + 1;
			j++;
		}
		ArrayList<String> arrayList = new ArrayList<String>(Arrays.asList(poly));

		Zone zoneObj = null;
		if (merchantId != null && polygones != null) {
			Merchant merchant = merchantService.findByMerchantId(merchantId);
			if (merchant != null) {
				LOGGER.info("===============  MerchantController : Inside getDeliveryZonePolygonesForMerchant :: inside if (merchant != null)");

				List<Zone> savedZone = zoneService.findZoneByMerchantId(merchant.getId());
				if (!savedZone.isEmpty() && savedZone.size() > 0) {
					for (Zone finalZone : savedZone) {
						Zone zone = new Zone();
						zone.setId(finalZone.getId());
						zone.setMerchant(merchant);
						zone.setPolygons(polygones);
						zone.setDeliveryFee(finalZone.getDeliveryFee());
						zone.setZoneName(finalZone.getZoneName());
						if (finalZone.getZoneDistance() != null) {
							zone.setZoneDistance(finalZone.getZoneDistance());
						}
						zone.setAvgDeliveryTime(finalZone.getAvgDeliveryTime());
						zone.setMinDollarAmount(finalZone.getMinDollarAmount());
						zoneObj = zoneService.createZone(zone);
					}
				} else {
					Zone zone = new Zone();
					zone.setMerchant(merchant);
					zone.setPolygons(polygones);
					zoneObj = zoneService.createZone(zone);
				}

			}
		}
		LOGGER.info("===============  MerchantController : Inside getDeliveryZonePolygonesForMerchant :: End  ============= ");

		return "success";
	}
	@RequestMapping(value = "/getTimezoneCode", method = RequestMethod.GET)
	@ResponseBody
	public String getNotification(@RequestParam("merchantId") Integer merchantId)
	{
		LOGGER.info("===============  AdminHomeController : Inside merchantId :: start  ============= ");
		Gson gson = new Gson();
		try{
			if(merchantId!=null)
			{
				Merchant merchant =merchantService.findById(merchantId);
				if(merchant!=null && merchant.getTimeZone()!=null)
				{
					
					return merchant.getTimeZone().getTimeZoneCode();
				}
			}
		}catch(Exception e)
		{
			MailSendUtil.sendExceptionByMail(e,environment);
		}
		return "Contact to support";
	}

	@RequestMapping(value = "/kouponlinkWithFoodkonnekt", method = RequestMethod.GET)
	@ResponseBody
	public String kouponlinkWithFoodkonnekt(@RequestParam("merchantUid") String merchantUid,
			@RequestParam("vendorUid") String vendorUid) {
		LOGGER.info("===============  MerchantController : Inside kouponlinkWithFoodkonnekt :: start  ============= ");
		try {
			if (merchantUid != null) {
				Merchant merchant = merchantService.findByMerchantUid(merchantUid);
				if (merchant != null) {
					merchant.getOwner().setVendorUid(vendorUid);
					merchantService.save(merchant);
					return "true";
				}
			}
		} catch (Exception e) {
			MailSendUtil.sendExceptionByMail(e,environment);
		}
		return "Contact to support";
	}
	
	  @RequestMapping(value = "/virtualFund")
			public String user(@ModelAttribute("VirtualFund") VirtualFund virtualFund) {
				
				return "virtualFund";
			}
		  
		  @RequestMapping(value = "/addVirtualFund")
			public String addUser(@ModelAttribute("VirtualFund") VirtualFund virtualFund) {
				
				return "addVirtualFund";
			}
		  
	@RequestMapping(value = "/getVirtualfundByMerchantId", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getVirtualfundByMerchantId(HttpServletRequest request) throws IOException {
		LOGGER.info("===============  MerchantController : Inside getVirtualfundByMerchantId :: Start  ============= ");

		HttpSession session = request.getSession(false);
		Merchant merchant = (Merchant) session.getAttribute("merchant");
		
		String jsonOutput = "";
		if (merchant != null) {
			LOGGER.info("===== MerchantController : Inside getVirtualfundByMerchantId :: merchant  == " + merchant.getId());

			jsonOutput = merchantService.showVirtualFundByMerchantId(merchant);
		}
		LOGGER.info("===============  MerchantController : Inside getVirtualfundByMerchantId :: End  ============= ");

		return jsonOutput;
	}
	
	@RequestMapping(value = "/editVirtualFund", method = RequestMethod.GET)
	public String viewEditVirtualFund(@ModelAttribute("VirtualFund") VirtualFund virtualFund, ModelMap model,
			HttpServletRequest request, @RequestParam(required = false) int virtualFundId) {
		try {
			LOGGER.info(
					"=============== MerchantController : Inside viewEditVirtualFund :: Start  ============= virtualFundId "
							+ virtualFundId);

			HttpSession session = request.getSession(false);
			if (session != null) {
				Merchant merchant = (Merchant) session.getAttribute("merchant");
				if (merchant != null) {
					VirtualFund result = virtualFundRepository.findOne(virtualFundId);

					model.addAttribute("virtualfund", result);

				}
			} else {
				return "redirect:" + environment.getProperty("BASE_URL") + "/support";
			}
		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
				LOGGER.error("error: " + e.getMessage());
				LOGGER.info("===============  MerchantController : Inside viewEditVirtualFund :: End  ============= ");

				return "redirect:" + environment.getProperty("BASE_URL") + "/support";
			}
		}
		return "editVirtualFund";
	}
	
	@RequestMapping(value = "/updateVirtualFund", method = RequestMethod.POST)
	public String updateVirtualFund(@ModelAttribute("VirtualFund") VirtualFund virtualFund, 
			HttpServletRequest request,ModelMap model) {
		try {
			HttpSession session = request.getSession(false);
			if (session != null) {
				Merchant merchant = (Merchant) session.getAttribute("merchant");
				if (merchant != null) {
			String result = merchantService.updateVirtualFund(virtualFund , merchant);
			model.addAttribute("result", result);
			if(!result.equals("")) {
				return "editVirtualFund";
			}
				}
			}

	} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
				LOGGER.error("error: " + e.getMessage());
				LOGGER.info("===============  CustomerController : Inside updateUserData :: Exception  ============= "
						+ e);

				return "redirect:" + environment.getProperty("BASE_URL") + "/support";
			}
	}
	LOGGER.info("===============  AdminHomeController : Inside updateUserData :: End  ============= ");

		return "virtualFund";
	}
	
	@RequestMapping(value = "/addNewVirtualFund", method = RequestMethod.POST)
	public String addNewUser(@ModelAttribute("VirtualFund") VirtualFund virtualFund,
			HttpServletRequest request,ModelMap model) {
		try {
			HttpSession session = request.getSession(false);
			if (session != null) {
				Merchant merchant = (Merchant) session.getAttribute("merchant");
				
				if (merchant != null) {
			String result = merchantService.saveVirtualFund(virtualFund ,merchant);
			model.addAttribute("result", result);
			if(!result.equals("")) {
				return "addVirtualFund";
			}
		   
		}else
			return "redirect:" + environment.getProperty("BASE_URL") + "/support";	
	}

	} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
				LOGGER.error("error: " + e.getMessage());
				LOGGER.info("===============  CustomerController : Inside updateUserData :: Exception  ============= "
						+ e);

				return "redirect:" + environment.getProperty("BASE_URL") + "/support";
			}
	}
	LOGGER.info("===============  AdminHomeController : Inside updateUserData :: End  ============= ");

	return "redirect:" + environment.getProperty("BASE_URL") + "/virtualFund";
	}
	
	
	@RequestMapping(value = "/updatefundStatusById", method = RequestMethod.GET)
	@ResponseBody
	public String updatefundStatusById(@RequestParam(required = true) Integer fundId,
			@RequestParam(required = true) boolean fundStatus) {
		try {
			LOGGER.info(
					"===============  MerchantController : Inside updatefundStatusById :: Start  ============= fundId "
							+ fundId + " fundStatus " + fundStatus);
			VirtualFund result = virtualFundRepository.findOne(fundId);
			if(result!=null) {
				result.setStatus(fundStatus);
				virtualFundRepository.save(result);
			}
		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
				LOGGER.error("error: " + e.getMessage());
				LOGGER.info(
						"===============  MerchantController : Inside updatefundStatusById :: Exception  ============= "
								+ e);

				return "redirect:" + environment.getProperty("BASE_URL") + "/support";
			}
		}
		LOGGER.info(
				"===============  MerchantController : Inside updatefundStatusById :: End  ============= ");

		return "succuss";
	}

// For Showing product install status	
//	@RequestMapping(value = "/getAllApplicationStatus", method = RequestMethod.GET, produces = "application/json")
//	public @ResponseBody String getAllApplicationStatus(HttpServletRequest request) throws IOException {
//		LOGGER.info("===============  MerchantController : Inside getAllApplicationStatus :: Start  ============= ");
//
//		HttpSession session = request.getSession(false);
//		Merchant merchant = (Merchant) session.getAttribute("merchant");
//		String jsonOutput = "";
//		try {
//		if (merchant != null) {
//			LOGGER.info("===== MerchantController : Inside getAllApplicationStatus :: merchant  == " + merchant.getId());
//
//			jsonOutput = merchantService.showAllApplicationstatus(merchant);
//			
//		}
//		LOGGER.info("===============  MerchantController : Inside getAllApplicationStatus :: End  ============= ");
//		}catch (Exception e) {
//			LOGGER.info("===== MerchantController : Inside getAllApplicationStatus :: exception :" +e);
//		}
//		return jsonOutput;
//	}
}
