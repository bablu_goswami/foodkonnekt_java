package com.foodkonnekt.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.foodkonnekt.model.Category;
import com.foodkonnekt.service.CategoryService;
import com.foodkonnekt.util.IConstant;
import com.google.gson.Gson;

@Controller
public class AdminCategoryController {

    @Autowired
    private CategoryService categoryService;
    
    private static final Logger LOGGER = LoggerFactory
			.getLogger(AdminCategoryController.class);

    /**
     * Find all category name
     * 
     * @return
     */
    @RequestMapping(value = "/AllCategory", method = RequestMethod.GET)
    public @ResponseBody Map<Object, Object> findCaegory(@RequestParam Integer merchantId) {
		LOGGER.info("AdminCategoryController : Inside findCaegory start merchnatId= "+merchantId);

        Map<Object, Object> categoryMap = new HashMap<Object, Object>();
        List<Category> categories = categoryService.findAllCategory(merchantId);
        if (categories != null) {
        	LOGGER.info("findCaegory inside if (categories != null)");

            categoryMap.put(IConstant.RESPONSE, IConstant.RESPONSE_SUCCESS_MESSAGE);
            categoryMap.put(IConstant.DATA, categories);
        } else {
            categoryMap.put(IConstant.RESPONSE, IConstant.RESPONSE_NO_DATA_MESSAGE);
            categoryMap.put(IConstant.MESSAGE, IConstant.CATEGORY_FAILURE);
        }
		LOGGER.info("AdminCategoryController : findCaegory returns categoryMap= "+categoryMap);

        return categoryMap;
    }
    
    
    
    /**
     * Get all categories     * 
     * @return Map<Object, Object>
     */
    @RequestMapping(value = "getAllCategories", method = RequestMethod.GET)
    public @ResponseBody Map<Object, Object> getAllCategoriesByMerchantUId(@RequestParam("merchantUId") String merchantUId) {
    	LOGGER.info("getAllCategories merchantUId "+merchantUId);
        Map<Object, Object> allCategoriesMap = new HashMap<Object, Object>();
        List<Category> allCategories = categoryService.getAllCategoriesByMerchantUId(merchantUId);
        Gson gson = new Gson();
        //String allCategoriesJson = gson.toJson(allCategories);
        if (!allCategories.isEmpty()) {
        	LOGGER.info("getAllCategories  inside if (!allCategories.isEmpty())");

        	allCategoriesMap.put(IConstant.RESPONSE, IConstant.RESPONSE_SUCCESS_MESSAGE);
        	allCategoriesMap.put(IConstant.DATA, allCategories);
        } else {
        	allCategoriesMap.put(IConstant.RESPONSE, IConstant.RESPONSE_NO_DATA_MESSAGE);
        	allCategoriesMap.put(IConstant.DATA, allCategories);
        }
        return allCategoriesMap;
    }   
    
    
    /**
     * Get all categories     * 
     * @return Map<Object, Object>
     */
    @RequestMapping(value = "getAllCategoriesByVendorUId", method = RequestMethod.GET)
    public @ResponseBody Map<Object, Object> getAllCategoriesByVendorUId(@RequestParam("vendorUId") String vendorUId,HttpServletResponse response) {
    	LOGGER.info("getAllCategoriesByVendorUId vendorUId "+vendorUId);

        Map<Object, Object> allCategoriesMap = new HashMap<Object, Object>();
        List<Category> allCategories = categoryService.getAllCategoriesByVendorUId(vendorUId);
        Gson gson = new Gson();
        String eventTypesJson = gson.toJson(allCategories);
        if (!allCategories.isEmpty()) {
        	LOGGER.info("getAllCategoriesByVendorUId  inside if (!allCategories.isEmpty())");

        	allCategoriesMap.put(IConstant.RESPONSE, IConstant.RESPONSE_SUCCESS_MESSAGE);
        	allCategoriesMap.put(IConstant.DATA, eventTypesJson);
        } else {
        	allCategoriesMap.put(IConstant.RESPONSE, IConstant.RESPONSE_NO_DATA_MESSAGE);
        	allCategoriesMap.put(IConstant.DATA, eventTypesJson);
        }
        return allCategoriesMap;
    }   
    
}
