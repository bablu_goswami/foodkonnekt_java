package com.foodkonnekt.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Iterator;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.foodkonnekt.model.Clover;
import com.foodkonnekt.model.Merchant;
import com.foodkonnekt.model.OrderR;
import com.foodkonnekt.model.ProductMerchantMap;
import com.foodkonnekt.service.MerchantService;
import com.foodkonnekt.serviceImpl.OrderServiceImpl;
import com.foodkonnekt.util.CloverUrlUtil;
import com.foodkonnekt.util.IConstant;
import com.foodkonnekt.util.MailSendUtil;
import com.foodkonnekt.util.UrlConstant;


@Controller
public class FreekwentWebhookController {

	@Autowired
    private Environment environment;
	
	@Autowired
    private MerchantService merchantService;
    
 
	private static final Logger LOGGER = LoggerFactory.getLogger(FreekwentWebhookController.class);
	
    
    @Autowired
    private com.foodkonnekt.service.OrderService orderService;
    
    @Autowired
    private com.foodkonnekt.repository.ProductMerchantMapRepository productMerchantMapRepository; 

    @SuppressWarnings("rawtypes")
    @RequestMapping(value = "/freekwentWebHooks", method = { RequestMethod.POST })
    public @ResponseBody String sendCode(HttpServletRequest request) {
    	LOGGER.info("===============  FreekwentWebhookController : Inside sendCode :: Start  ============= ");

         String actionType="";
         String merchantPOSID="";
         String objectId="";
         String objectType="";
         String responseData = "";
         try {
            String json = IOUtils.toString(request.getInputStream());
//            MailSendUtil.webhookMail("Webhook Json",json);
            if (!json.contains("verificationCode")) {
                
                /*if(!environment.getProperty("FOODKONNEKT_APP_TYPE").equals("Prod"))
                 MailSendUtil.webhookMail("Check webhook event",json);*/
                
                JSONObject Object = new JSONObject(json);
                String appId = Object.getString("appId");
                JSONObject merchantJsonObject = Object.getJSONObject("merchants");
                Iterator merchantIds = merchantJsonObject.keys();
                 while (merchantIds.hasNext()) {
                    merchantPOSID = (String) merchantIds.next();
                    LOGGER.info("-------merchantPOSID----" + merchantPOSID);
                    Merchant merchant = merchantService.findbyPosId(merchantPOSID);
                    LOGGER.info("===== FreekwentWebhookController : Inside sendCode :: merchant  == "
							+ merchant.getId());

                    ProductMerchantMap productMerchantMap = productMerchantMapRepository.findByMerchantIdAndProductId(merchant.getId(),5); 
                    if(merchant!=null && merchant.getIsInstall()!=IConstant.SOFT_DELETE && productMerchantMap.getProductId()==5);{
                   
                    JSONArray jsonArray = merchantJsonObject.getJSONArray(merchantPOSID);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject objects = jsonArray.getJSONObject(i);
                         objectId = objects.getString("objectId");
                        String objectIds[] = objectId.split(":");
                         objectType = objectIds[0];
                        objectId = objectIds[1];
                         actionType = objects.getString("type");
                       // webhook(appId, merchantPOSID, objectType, objectId, actionType);
                         if (objectType.equals("P") && actionType.equals("CREATE")) {
                        	 Merchant result = merchantService.findbyPosId(merchantPOSID);
                             if (result != null) {
                                 String url = "";
                                 String reviewResponse = null;
                                 Clover clover = new Clover();
                                 clover.setMerchantId(merchantPOSID);
                                 clover.setAuthToken(result.getAccessToken());
                                 clover.setInstantUrl(IConstant.CLOVER_INSTANCE_URL);
                                 clover.setUrl(environment.getProperty("CLOVER_URL"));
                                 // Merchant merchant = cloverService.saveMerchant(clover);
                                 String paymentDetails = CloverUrlUtil.getPaymentDetails(clover, objectId);
                                 LOGGER.info("PaymentDetails: " + paymentDetails);

                                 JSONObject paymentDetailsObject = new JSONObject(paymentDetails);
                                 JSONObject orderJsonObject = paymentDetailsObject.getJSONObject("order");
                               //  String orderPosId = orderJsonObject.getJSONObject("orderType").getString("id");
                                 String orderPosId = orderJsonObject.getString("id");
                                 Double orderTotal = (orderJsonObject.getDouble("total")/100);
                                 OrderR orderR = orderService.findOrderByOrderID(orderPosId);
                                 if (orderR == null) {
                                     JSONObject paymentDetailJsonObject = paymentDetailsObject.getJSONObject("tender");
                                     if (paymentDetailJsonObject.getString("labelKey").equalsIgnoreCase("com.clover.tender.credit_card")) {
                                         JSONObject cardTransactionJsonObject = paymentDetailsObject    .getJSONObject("cardTransaction");
                                         responseData = "{first6:" + cardTransactionJsonObject.getString("first6") + ", last4:"
                                                         + cardTransactionJsonObject.getString("last4") + ", cardholderName:"
                                                         + cardTransactionJsonObject.getString("cardholderName")+", OrderTotal:"+orderTotal+", merchantPosId:"+merchantPOSID+"}";
                                         LOGGER.info(
												"===============  FreekwentWebhookController : Inside sendCode :: responseData == "+responseData);

                                         String cardHolderName="";
                                           if(cardTransactionJsonObject.has("cardholderName")) {
                                               cardHolderName  = cardTransactionJsonObject.getString("cardholderName");
                                               cardHolderName = URLEncoder.encode(cardHolderName, "UTF-8");
                                           }
                                         url = environment.getProperty("FREEKWENTBASEURL")+"validateCard?firstSixNumber="+cardTransactionJsonObject.getString("first6")
                                             +"&lastFourNumber="+cardTransactionJsonObject.getString("last4")+"&cardHolderName="+cardHolderName
                                             +"&orderPayment="+orderTotal+"&posid="+merchantPOSID+"&paymentType=CreditCard";
                                            
                                     }else  if (paymentDetailJsonObject.getString("labelKey").equalsIgnoreCase(
                                                     "com.clover.tender.cash") || paymentDetailJsonObject.getString("label").equalsIgnoreCase("Cash")) {
                                         
                                         url = environment.getProperty("FREEKWENTBASEURL")+"validateCard?paymentType=Cash"
                                         +"&orderPayment="+orderTotal+"&posid="+merchantPOSID;
                                     }
                                     LOGGER.info("before uri encode---"+url);
                                     // rest call for 
                                     if(url!="" && !url.isEmpty()) {
                                    // url = URLEncoder.encode(url, "UTF-8");
                                     LOGGER.info("after uri encode---"+url);
                                     try {
                                         HttpClient client = HttpClientBuilder.create().build();
                                         HttpGet httpRequest = new HttpGet(url.trim());
                                         HttpResponse httpResponse = client.execute(httpRequest);
                                         BufferedReader rd = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent()));
                                         StringBuffer apiResponse = new StringBuffer();
                                         String line = "";
                                         while ((line = rd.readLine()) != null) {
                                             apiResponse.append(line);
                                         }
                                         reviewResponse = apiResponse.toString();
                                     } catch (Exception e) {
                                    	 LOGGER.error(
												"===============  FreekwentWebhookController : Inside sendCode :: Exception  ============= "
														+ e);

                                         LOGGER.error("error: " + e.getMessage());
                                     }
                                     }
                                 }
                             }
                         }
                    }
                    }
                }
            } else {
                MailSendUtil.webhookMail("Webhook verificationCode from",json,environment);
            }
        } catch (IOException ioException) {
        	LOGGER.error("===============  FreekwentWebhookController : Inside sendCode :: Exception  ============= " + ioException);

            ioException.printStackTrace();
            StringWriter errors = new StringWriter();
            ioException.printStackTrace(new PrintWriter(errors));
            MailSendUtil.webhookMail("Webhook mail from","merchantId->"+merchantPOSID+" Object Id ->"+objectId+" Object Type ->"+objectType+" action type ->"+actionType+" Exception--> "+errors.toString(),environment);
        }
         LOGGER.info("===============  FreekwentWebhookController : Inside sendCode :: End  ============= ");

        return responseData;
    }
    
    
    
  
    @RequestMapping(value = "/sendNotification", method = RequestMethod.GET)
    @ResponseBody
    public String sendNotification(@RequestParam("merchant_id") String merchantId,  @RequestParam("payload") String payload) {
       LOGGER.info("===============  FreekwentWebhookController : Inside sendNotification :: Start  ============= ");

    	Merchant merchant = null;
        String  response =null;
        if (merchantId != null) {
            merchant = new Merchant();
            merchant = merchantService.findbyPosId(merchantId);
            
            String notificationJson = "{\"notification\": {\"payload\": \""+payload+"\",\"appEvent\": \"notification\"}}";
            if (merchant != null && merchant.getAccessToken() != null) {
                HttpPost postRequest = new HttpPost(environment.getProperty("CLOVER_V2_URL") + merchantId + "/apps/"
                                + IConstant.FREEKWENT_APP_ID + "/notify?access_token=" + merchant.getAccessToken());
             response    =OrderServiceImpl.convertToStringJson(postRequest, notificationJson,environment);
            }
            
        }
        LOGGER.info("===============  FreekwentWebhookController : Inside sendNotification :: End  ============= ");

        return response;

    }
    
    
//  public static void sendNotification(String merchantId, String accessToken, String notificationJson){
//      HttpPost postRequest = new HttpPost(environment.getProperty("CLOVER_V2_URL") + merchantId + "/apps/" + IConstant.FREEKWENT_APP_ID
//                      + "/notify?access_token=" + accessToken);
//      
//      
//      
//      LOGGER.info("=====" + OrderServiceImpl.convertToStringJson(postRequest, notificationJson));
//  }

 
    public static void main(String[] args) {
        String notificationJson = "{\"notification\": {\"payload\": \"ZX5VVW1RTS81A@#You got a new order(ZX5VVW1RTS81A) at Mon Sep 24 12:17:32 IST 2018@# $22.24@# pickUp@#{'total':'22.24','productItems':[{'price':'3.0','product_id':'F9RPAAWKHHPH4','qty':'5','name':'Buffalo Mozz'}],'tax':'4.24'}@#VMVPQ69S43JDE@#  @#null\",\"appEvent\": \"notification\"}}";
        HttpPost postRequest = new HttpPost("https://apisandbox.dev.clover.com/v2/merchant/HV4QCRSDYE536/apps/8F3S9TFYK3TY0/notify?access_token=8e88455a-f7e7-b033-6f4c-37f400ba1c7b");
       LOGGER.info("https://apisandbox.dev.clover.com/v2/merchant/HV4QCRSDYE536/apps/8F3S9TFYK3TY0/notify?access_token=8e88455a-f7e7-b033-6f4c-37f400ba1c7b");
        LOGGER.info(notificationJson);
        //LOGGER.info("=====" + OrderServiceImpl.convertToStringJson(postRequest, notificationJson),environment);
        
        //44f7c326-48e5-a454-bcbf-ac2efde99159
    }
}
