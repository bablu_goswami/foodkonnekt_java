package com.foodkonnekt.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.foodkonnekt.clover.vo.AddressVO;
import com.foodkonnekt.model.Address;
import com.foodkonnekt.model.Clover;
import com.foodkonnekt.model.DeliveryOpenHours;
import com.foodkonnekt.model.DeliveryZoneTiming;
import com.foodkonnekt.model.Merchant;
import com.foodkonnekt.model.OpenHours;
import com.foodkonnekt.model.Zone;
import com.foodkonnekt.service.BusinessService;
import com.foodkonnekt.service.MerchantService;
import com.foodkonnekt.service.ZoneService;
import com.foodkonnekt.util.CloverUrlUtil;
import com.foodkonnekt.util.DateUtil;
import com.foodkonnekt.util.IConstant;
import com.foodkonnekt.util.MailSendUtil;
import com.foodkonnekt.util.UrlConstant;
import com.google.gson.Gson;

@Controller
public class ZoneController {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(ZoneController.class);
    
	@Autowired
    private Environment environment;
	
	@Autowired
    private ZoneService zoneService;
    
    @Autowired
	private BusinessService businessService;
    
    @Autowired
    private MerchantService merchantService;

    
    @RequestMapping(value = "/createZone", method = RequestMethod.POST)
    public @ResponseBody Zone getAllOrderData(@RequestBody Zone zone, HttpServletResponse response) {
        LOGGER.info("IN ZoneController inside getAllOrderData ");
        return zoneService.createZone(zone);
    }
    
    @RequestMapping(value = "/deliveryTimings", method = RequestMethod.GET)
    public String pickUpTime(@ModelAttribute("DeliveryOpenHours") DeliveryOpenHours openHours, ModelMap model,
                    HttpServletRequest request) {
    	try {
    		LOGGER.info("IN ZoneController inside pickUpTime start");
			HttpSession session = request.getSession(false);
			if (session != null) {
				Merchant merchant = (Merchant) session.getAttribute("merchant");
				LOGGER.info("===== WebhooksController : Inside pickUpTime :: merchant  == " + merchant.getId());

				if (merchant != null) {
					merchant=merchantService.findById(merchant.getId());
					Integer allowDeliveryTiming=0;
					if(merchant.getAllowDeliveryTiming()!=null){
						allowDeliveryTiming=merchant.getAllowDeliveryTiming();
					}
		session.setAttribute("merchant", merchant);
    	model.addAttribute("businessHours", businessService.findDeliveryHourByMerchantId(merchant));
    	model.addAttribute("allowDeliveryTiming", allowDeliveryTiming);
		model.addAttribute("times", DateUtil.findAllTime());
				}
			}
    	}catch(Exception e){
    	    LOGGER.info("ZoneController.pickUpTime() : ERROR" + e);
    		LOGGER.error("error: " + e.getMessage());
    	}
		LOGGER.info("IN ZoneController inside pickUpTime End");

        return "deliveryTimings";
    }
    @RequestMapping(value = "/saveDeliveryHours", method = RequestMethod.POST)
	public String save(@ModelAttribute("DeliveryOpenHours") DeliveryOpenHours openHours, ModelMap model, HttpServletResponse response,
			HttpServletRequest request) {
    	   
    	LOGGER.info("IN ZoneController inside save start");
			HttpSession session = request.getSession(false);
			if (session != null) {
				Merchant merchant = (Merchant) session.getAttribute("merchant");
				LOGGER.info("===== WebhooksController : Inside save :: merchant  == " + merchant.getId());

				if (merchant != null) {
					merchant=merchantService.findById(merchant.getId());
					if (openHours.getsTimeToSave() != null && openHours.getSelectedDay() != null) {
						businessService.updateDeliveryHour(openHours.getsTimeToSave(), openHours.geteTimeToSave(),
								openHours.getSelectedDay(),openHours.getAllowDeliveryTimings(),merchant);
					}
					
				}
			}
    	
			LOGGER.info("IN ZoneController inside save End");

    	
			return "redirect:" + environment.getProperty("BASE_URL") + "/deliveryZones";
    }

    @RequestMapping(value = "/updateZoneDetail", method = RequestMethod.POST)
    public @ResponseBody Zone updateZoneDetail(@RequestBody Zone zone, HttpServletResponse response) {
    	LOGGER.info("IN ZoneController inside updateZoneDetail start");
        return zoneService.updateZoneDetail(zone);
    }

    @RequestMapping(value = "/getAllZone", method = RequestMethod.GET)
    public @ResponseBody List<Zone> getAllZone(HttpServletResponse response) {
    	LOGGER.info("IN ZoneController inside getAllZone start");   
    	return zoneService.getAllZone();
    }

    @RequestMapping(value = "/deleteZone", method = RequestMethod.GET)
    public @ResponseBody String deleteZone(@RequestParam(required = true) Integer zoneID) {
    	LOGGER.info("IN ZoneController inside deleteZone start");   
    	return zoneService.deleteZone(zoneID);
    }

    /**
     * Check delivery zone for address
     * 
     * @param address
     * @param response
     * @return
     */
    @RequestMapping(value = "/checkDeliveryZone", method = RequestMethod.POST)
    public @ResponseBody Map<Object, Object> update(@RequestBody Address address, HttpServletResponse response) {
        Map<Object, Object> zoneMap = new HashMap<Object, Object>();
        	try{
        		LOGGER.info("IN ZoneController inside update start");
            String status = zoneService.checkAddressForDeliveryZone(address);
            if (status != null) {
            	
                zoneMap.put(IConstant.RESPONSE, IConstant.RESPONSE_SUCCESS_MESSAGE);
                zoneMap.put(IConstant.DATA, status);
            } else {
                zoneMap.put(IConstant.RESPONSE, IConstant.RESPONSE_NO_DATA_MESSAGE);
                zoneMap.put(IConstant.MESSAGE, IConstant.ZONE);
            }
         
        } catch (Exception e) {
            if (e != null) {
                MailSendUtil.sendExceptionByMail(e,environment);
        	    LOGGER.info("ZoneController.update() : ERROR" + e);
                LOGGER.error("error: " + e.getMessage());
             
            }
          
        }
    		LOGGER.info("IN ZoneController inside update End");

        return zoneMap;
    }
    
    @RequestMapping(value = "/createUpdateCloverAddress", method = RequestMethod.POST)
    public @ResponseBody String createUpdateCloverAddress(@RequestBody Address newAddress, HttpServletResponse response) {
        try {
        	LOGGER.info("IN ZoneController inside createUpdateCloverAddress start");
        	if(newAddress.getCustPosId()!=null){
        	Clover clover = new Clover();
        	Merchant merchant = merchantService.findById(newAddress.getMerchId());
            clover.setInstantUrl(IConstant.CLOVER_INSTANCE_URL);
            clover.setUrl(environment.getProperty("CLOVER_URL"));
            clover.setMerchantId(merchant.getPosMerchantId());
            clover.setAuthToken(merchant.getAccessToken());
        	Gson gson = new Gson();
            
            AddressVO address = new AddressVO();
            
            address.setAddress1(newAddress.getAddress1());
            address.setAddress2(newAddress.getAddress2());
            address.setAddress3(newAddress.getAddress3());
            address.setZip(newAddress.getZip());
            address.setState(newAddress.getState());
            address.setCity(newAddress.getCity());
            address.setCountry("US");
            address.setId(newAddress.getAddressPosId());
            
            String addressJson = gson.toJson(address);
            
            Map<Object, Object> addressMap = new HashMap<Object, Object>();
           
            	String addressResponse=CloverUrlUtil.createUpdateCustomerAddress(clover, addressJson,newAddress.getCustPosId());
                        
            	if(addressResponse.contains("id")){
            		LOGGER.info("IN ZoneController inside createUpdateCloverAddress End");

            		 return addressResponse;
            	}else{
            		LOGGER.info("IN ZoneController inside createUpdateCloverAddress End");

            		return null;
            	}
        	}
    		LOGGER.info("IN ZoneController inside createUpdateCloverAddress End");

        	return null;
        } catch (Exception e) {
            if (e != null) {
                MailSendUtil.sendExceptionByMail(e,environment);
        	    LOGGER.info("ZoneController.createUpdateCloverAddress() : ERROR" + e);
                LOGGER.error("error: " + e.getMessage());
             
            }
            return null;
        }
        
    }
    
	@RequestMapping(value = "/saveDeliveryTime", method = RequestMethod.GET)
	@ResponseBody
	public String saveDeliveryTime(@RequestParam String days, @RequestParam String startTime,
			@RequestParam String endTime, @RequestParam int merchantId, DeliveryZoneTiming deliveryZoneTiming,
			Zone zone,@RequestParam Integer allowDeliveryTiming) {
		LOGGER.info("IN ZoneController inside saveDeliveryTime start");
		zoneService.saveDeliveryTime(days, startTime, endTime, merchantId,allowDeliveryTiming);
		LOGGER.info("IN ZoneController inside saveDeliveryTime End");

		return "success";
	}

	@RequestMapping(value = "/getDeliveryTiming", method = RequestMethod.GET)
	@ResponseBody
	public String getCategoryTiming(@RequestParam int merchantId) {
		String responseJson;
		Map<Object,Object> response= new HashMap<Object,Object>();
		try {
			LOGGER.info("IN ZoneController inside getCategoryTiming start");
			List<DeliveryZoneTiming> result = zoneService.getDeliveryTiming(merchantId);
			Merchant merchant=merchantService.findById(merchantId);
			LOGGER.info("===== WebhooksController : Inside getCategoryTiming :: merchant  == " + merchant.getId());

			Integer allowDeliveryTiming=merchant.getAllowDeliveryTiming();
			if(merchant!=null){
			response.put("allowDeliveryTiming", allowDeliveryTiming);
			response.put("deliveryZoneTiming", result);
			}
			Gson gson=new Gson();
			responseJson=gson.toJson(response);
			LOGGER.info("IN ZoneController inside getCategoryTiming End");

			return responseJson;
		} catch (Exception e) {
    	    LOGGER.info("ZoneController.getCategoryTiming() : ERROR" + e);
			LOGGER.error("error: " + e.getMessage());
			return null;
		}
	}
}
