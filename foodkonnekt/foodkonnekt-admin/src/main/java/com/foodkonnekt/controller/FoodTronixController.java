package com.foodkonnekt.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.foodkonnekt.foodtronix.service.FoodTronixService;
import com.foodkonnekt.model.Customer;
import com.foodkonnekt.model.FoodTronix;
import com.foodkonnekt.model.Merchant;
import com.foodkonnekt.service.MerchantService;
import com.foodkonnekt.util.EncryptionDecryptionUtil;
import com.foodkonnekt.util.IConstant;
import com.foodkonnekt.util.MailSendUtil;
import com.foodkonnekt.util.OrderDTOForFoodtronix;
import com.foodkonnekt.util.ProducerUtil;

@Controller
public class FoodTronixController {
	
	
	private static final Logger LOGGER = LoggerFactory.getLogger(FoodTronixController.class);
	
	@Autowired
    private Environment environment;
	
	@Autowired
	FoodTronixService foodTronixService;
	
	 @Autowired
	    private MerchantService merchantService;

	@RequestMapping(value = "/getFoodTronixData", method = RequestMethod.POST)
	public @ResponseBody String getCloverData(@RequestBody FoodTronix foodtronix, HttpServletResponse response) {
       LOGGER.info("===============  FoodTronixController : Inside getCloverData :: Start  ============= ");

		foodtronix.setUrl(IConstant.FOODTONIX_URL);
		foodtronix.setInstantUrl(IConstant.FOODTONIX_INSTANCE_URL);
		
		Merchant merchant = foodTronixService.saveMerchant(foodtronix);
		
		LOGGER.info("Merchant saved");
		
		foodTronixService.saveItemProperty(foodtronix,merchant);
		
		LOGGER.info("Item Property saved");
		
		foodTronixService.saveItemPropertyGroup(foodtronix,merchant);
		
		LOGGER.info("Item Property Group saved");
		
		foodTronixService.saveDishCategory(foodtronix,merchant);
		
		LOGGER.info("DishCatefory saved");
		
		/*foodTronixService.savePizzaTemplate(foodtronix,merchant);
		
		LOGGER.info("Pizza templa saved");
		
		foodTronixService.savePizzaTemplateSize(foodtronix,merchant);
		
		LOGGER.info("Pizza Template size saved");
		
		foodTronixService.savePizzaSize(foodtronix,merchant);
		
		LOGGER.info("Pizza Size saved");
		
		foodTronixService.savePizzaTopping(foodtronix,merchant);
		
		LOGGER.info("Pizza Topping saved");
		
		foodTronixService.savePizzaToppingSize(foodtronix,merchant);
		
		LOGGER.info("Pizza Toppig Size saved");*/
				if (merchant != null) {
					LOGGER.info("===============  FoodTronixController : Inside getCloverData :: End  ============= ");

					return IConstant.RESPONSE_SUCCESS_MESSAGE;
				} else {
					LOGGER.info("===============  FoodTronixController : Inside getCloverData :: End  ============= ");

					return IConstant.RESPONSE_NO_DATA_MESSAGE;
				}

	}

	
	@RequestMapping(value = "/saveItemProperty", method = RequestMethod.POST)
	public @ResponseBody void saveItemProperty(@RequestBody FoodTronix foodtronix, HttpServletResponse response) {
		LOGGER.info("===============  FoodTronixController : Inside saveItemProperty :: Start  ============= ");

		foodtronix.setUrl(IConstant.FOODTONIX_URL);
		foodtronix.setInstantUrl(IConstant.FOODTONIX_INSTANCE_URL);
		Merchant merchant = foodTronixService.saveMerchant(foodtronix);
         
		foodTronixService.saveItemProperty(foodtronix,merchant);
		LOGGER.info("===============  FoodTronixController : Inside saveItemProperty :: End  ============= ");

	}

	@RequestMapping(value = "/saveItemPropertyGroups", method = RequestMethod.POST)
	public @ResponseBody void saveItemPropertyGroups(@RequestBody FoodTronix foodtronix, HttpServletResponse response) {
		LOGGER.info("===============  FoodTronixController : Inside saveItemPropertyGroups :: Start  ============= ");

		foodtronix.setUrl(IConstant.FOODTONIX_URL);
		foodtronix.setInstantUrl(IConstant.FOODTONIX_INSTANCE_URL);
		
		Merchant merchant = foodTronixService.saveMerchant(foodtronix);
        
         
		foodTronixService.saveItemPropertyGroup(foodtronix,merchant);
		LOGGER.info("===============  FoodTronixController : Inside saveItemPropertyGroups :: End  ============= ");

	}
	
	@RequestMapping(value = "/saveDishCategory", method = RequestMethod.POST)
    public @ResponseBody void saveDishCategory(@RequestBody FoodTronix foodtronix, HttpServletResponse response) {
        LOGGER.info("IN saveDishCategory");
        foodtronix.setUrl(IConstant.FOODTONIX_URL);
		foodtronix.setInstantUrl(IConstant.FOODTONIX_INSTANCE_URL);
		Merchant merchant = foodTronixService.saveMerchant(foodtronix);
        
        foodTronixService.saveDishCategory(foodtronix,merchant);
        LOGGER.info("All items are saved");
    }
	
	@RequestMapping(value = "/savePizzaSize", method = RequestMethod.POST)
    public @ResponseBody void savePizzaSize(@RequestBody FoodTronix foodtronix, HttpServletResponse response) {
        LOGGER.info("IN savePizzaSize");
        foodtronix.setUrl(IConstant.FOODTONIX_URL);
		foodtronix.setInstantUrl(IConstant.FOODTONIX_INSTANCE_URL);
		Merchant merchant = foodTronixService.saveMerchant(foodtronix);
        
        foodTronixService.savePizzaSize(foodtronix,merchant);
        LOGGER.info("All items are saved");
    }
	
	@RequestMapping(value = "/savePizzaTemplate", method = RequestMethod.POST)
    public @ResponseBody void savePizzaTemplate(@RequestBody FoodTronix foodtronix, HttpServletResponse response) {
        LOGGER.info("IN savePizzaTemplate");
        foodtronix.setUrl(IConstant.FOODTONIX_URL);
		foodtronix.setInstantUrl(IConstant.FOODTONIX_INSTANCE_URL);
		Merchant merchant = foodTronixService.saveMerchant(foodtronix);
        
        foodTronixService.savePizzaTemplate(foodtronix,merchant);
        LOGGER.info("All items are saved");
    }
	
	@RequestMapping(value = "/savePizzaTemplateSize", method = RequestMethod.POST)
    public @ResponseBody void savePizzaTemplateSize(@RequestBody FoodTronix foodtronix, HttpServletResponse response) {
        LOGGER.info("IN savePizzaTemplateSize");
        foodtronix.setUrl(IConstant.FOODTONIX_URL);
		foodtronix.setInstantUrl(IConstant.FOODTONIX_INSTANCE_URL);
		Merchant merchant = foodTronixService.saveMerchant(foodtronix);
        
        foodTronixService.savePizzaTemplateSize(foodtronix,merchant);
        LOGGER.info("All items are saved");
    }
	
	@RequestMapping(value = "/savePizzaTopping", method = RequestMethod.POST)
    public @ResponseBody void savePizzaTopping(@RequestBody FoodTronix foodtronix, HttpServletResponse response) {
        LOGGER.info("IN savePizzaTopping");
        foodtronix.setUrl(IConstant.FOODTONIX_URL);
		foodtronix.setInstantUrl(IConstant.FOODTONIX_INSTANCE_URL);
		Merchant merchant = foodTronixService.saveMerchant(foodtronix);
        foodTronixService.savePizzaTopping(foodtronix,merchant);
        LOGGER.info("All items are saved");
    }
	
	@RequestMapping(value = "/savePizzaToppingSize", method = RequestMethod.POST)
    public @ResponseBody void savePizzaToppingSize(@RequestBody FoodTronix foodtronix, HttpServletResponse response) {
        LOGGER.info("IN savePizzaToppingSize");
        foodtronix.setUrl(IConstant.FOODTONIX_URL);
		foodtronix.setInstantUrl(IConstant.FOODTONIX_INSTANCE_URL);
		Merchant merchant = foodTronixService.saveMerchant(foodtronix);
        foodTronixService.savePizzaToppingSize(foodtronix,merchant);
        LOGGER.info("All items are saved");
    }
	
	@RequestMapping(value = "/foodtronix", method = RequestMethod.GET)
    @ResponseBody
    public String clover(ModelMap model,@RequestParam("authToken") String authToken,
                    @RequestParam("company_id") String companyId, @RequestParam("store_id") String storeId,HttpServletResponse response,
                    HttpServletRequest request) throws Exception {
		LOGGER.info("===============  FoodTronixController : Inside clover :: Start  ============= ");

		FoodTronix foodtronix = new FoodTronix();
		foodtronix.setAuthToken(authToken);
		foodtronix.setCompanyId(companyId);
		foodtronix.setStoreId(storeId);
		foodtronix.setUrl(IConstant.FOODTONIX_URL);
		foodtronix.setInstantUrl(IConstant.FOODTONIX_INSTANCE_URL);
		        // merchantId = "9GK2J085R8A3A";
        Merchant merchantCheck = merchantService.findbyStoreId(storeId);
        final HttpSession session = request.getSession();
        model.addAttribute("adminUrl", environment.getProperty("BASE_URL"));
        if (null != merchantCheck && merchantCheck.getIsInstall()!=null && (merchantCheck.getIsInstall() == IConstant.BOOLEAN_TRUE ||merchantCheck.getIsInstall() == 2)) {
        	
        	
            String merchId = merchantCheck.getId().toString();
            String base64encode = EncryptionDecryptionUtil.encryption(merchId);
            String merchantName=merchantCheck.getName().replaceAll("[^a-zA-Z0-9]", "");
            String orderLink = environment.getProperty("WEB_BASE_URL") + "/"+merchantName+"/clover/" + base64encode;
            
            session.setAttribute("merchant", merchantCheck);
            session.setAttribute("onlineOrderLink", orderLink);
           // response.sendRedirect("adminHome");
           LOGGER.info("===============  FoodTronixController : Inside clover :: End  ============= ");

            return "adminHome";
        } else {
        	
        	Merchant merchant = foodTronixService.saveMerchant(foodtronix);
    		
    		LOGGER.info("Merchant saved");
    		
    		foodTronixService.saveItemProperty(foodtronix,merchant);
    		
    		LOGGER.info("Item Property saved");
    		
    		foodTronixService.saveItemPropertyGroup(foodtronix,merchant);
    		
    		LOGGER.info("Item Property Group saved");
    		
    		foodTronixService.saveDishCategory(foodtronix,merchant);
    		
    		LOGGER.info("DishCatefory saved");
           
           
            String merchId = merchant.getId().toString();
            String merchantName=merchant.getName().replaceAll("\\s+","");
            String base64encode = EncryptionDecryptionUtil.encryption(merchId);
            String orderLink = environment.getProperty("WEB_BASE_URL") + "/"+merchantName+"/foodtronix/" + base64encode;
            
            session.setAttribute("onlineOrderLink", orderLink);
            session.setAttribute("merchantId", merchant.getId());
            session.setAttribute("merchant", merchant);
           // response.sendRedirect("welcome");
            LOGGER.info("===============  FoodTronixController : Inside clover :: End  ============= ");

            return "uploadLogo";
        }
    }

	@RequestMapping(value = "/foodtronixException", method = RequestMethod.GET)
    @ResponseBody
    public String foodtronixException(@RequestParam("merchantId") Integer merchantId,
                    @RequestParam("exception") String exception) throws Exception {
		try{
			if(merchantId!=null && exception!=null )
			{
			Merchant merchant=merchantService.findById(merchantId);
			if(merchant!=null && merchant.getName()!=null)
				exception=EncryptionDecryptionUtil.decryption(exception);
				//MailSendUtil.sendFoodtronixErrorMailToAdmin(exception,merchant.getName(),environment);
			}
		}catch(Exception e){
			
		}
		return null;
	}
	
	@RequestMapping(value = "/getFoodtronixOfflineOrder", method = RequestMethod.POST)
    @ResponseBody
    public String getFoodtronixOfflineOrder(@RequestBody List<OrderDTOForFoodtronix> orderDto ,@RequestParam Integer merchantId) throws Exception {
		String response="failed";
		try{
			LOGGER.info("getFoodtronixOfflineOrder :inside  : start");
			if(orderDto!=null && orderDto.size()>0)
    	    {	
				if(merchantId!=null)
				{
					LOGGER.info("getFoodtronixOfflineOrder :inside  : merchantId"+merchantId);
					Merchant merchant =merchantService.findById(merchantId);
					if(merchant!=null && merchant.getMerchantUid()!=null)
					{
						LOGGER.info("getFoodtronixOfflineOrder :inside  : merchantId"+merchantId);
						Merchant merchantt=new Merchant();
						merchantt.setId(merchant.getId());
						merchantt.setMerchantUid(merchant.getMerchantUid());
						LOGGER.info("getFoodtronixOfflineOrder :inside  : check is freekwent is installed or not");
						LOGGER.info("getFoodtronixOfflineOrder :inside  : MerchantUid :"+merchant.getMerchantUid());
						if(ProducerUtil.isFreekwentInstallOrNot(merchant.getMerchantUid(), IConstant.FREEKWENT, environment))
						{
							LOGGER.info("getFoodtronixOfflineOrder :inside  :freekwent is installed");
							for (OrderDTOForFoodtronix orderDTO : orderDto) 
							{
							Customer customer=new Customer(); 
							customer.setFirstName(orderDTO.getFirstName());
							customer.setLastName(orderDTO.getLastName());
							customer.setPhoneNumber(orderDTO.getPhoneNumber());
							customer.setEmailId(orderDTO.getEmail());
							customer.setLoyalityProgram(1);
							customer.setMerchantt(merchantt);
							String customerEntityDTO = ProducerUtil.getCustomerEntityDTO(customer);
			        		LOGGER.info("getFoodtronixOfflineOrder :customerEntityDTO  :" +customerEntityDTO);
							String customerUid= ProducerUtil.createCustomerIntoCentralDb(customer,environment);
							LOGGER.info("getFoodtronixOfflineOrder :inside  : customerUid :"+customerUid);
							ProducerUtil.createCustomerIntoFreekwent(customerEntityDTO,environment);
			        		LOGGER.info("getFoodtronixOfflineOrder :response  createCustomerIntoCentralDb  :" + (customerUid!=null?customerUid:0));
			        		JSONObject json = new JSONObject();
			        		json.put("emailId",orderDTO.getEmail());
			        		json.put("mobileNumber",orderDTO.getPhoneNumber());
			        		json.put("locationUid",merchant.getMerchantUid());
			        		json.put("firstName",orderDTO.getFirstName());
			        		json.put("lastName",orderDTO.getLastName());
			        		json.put("amount",orderDTO.getOrderPrice());
			        		json.put("orderType","FoodTronixOffline");
			        		String customerOrderDetailJson=json.toString();
			        		LOGGER.info("getFoodtronixOfflineOrder :customerOrderDetailJson  :" +customerOrderDetailJson);
			        		LOGGER.info("getFoodtronixOfflineOrder :call callFreekWentApi  :");
			        		LOGGER.info("getFoodtronixOfflineOrder :response callFreekWentApi  :"+ProducerUtil.callFreekWentApi(customerOrderDetailJson,environment));
			        		response="Integration is done";
							}
						}
					}
				}
    	    }
		}catch(Exception e){
			
			MailSendUtil.sendExceptionByMail(e, environment);
			response="somthing went wrong";
		}
		return response;
	}
	
}
