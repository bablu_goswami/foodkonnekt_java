package com.foodkonnekt.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Random;

import com.foodkonnekt.serviceImpl.MerchantSliderServiceImpl;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.HmacAlgorithms;
import org.apache.commons.codec.digest.HmacUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.foodkonnekt.util.Requestbody;
import com.foodkonnekt.util.UrlConstant;

public class PayzeeyTest {
	private static final Logger LOGGER = LoggerFactory.getLogger(PayzeeyTest.class);

/*	public static final String GATEWAY = "PAYEEZY";
	public static final String PAYMENTJS_SECRET = "D5TZdNyoi1P9oQ3o";
	public static final String API_KEY = "AfSmenj6a06xqOOqoverwRTyr2IUdLzW";
	public static final String API_SECERET = "39b666fd691edf1e76388368261569cf566d94066dcda6cb3425b3b3581dd5c7";
	public static final String AUTH_TOKEN = "fdoa-d96de38a3e80c48575f2cdf14c5d26f7023df7d9bf9b4cb9";
	public static final String TRANSARMOR_TOKEN = "NOIW";
*/
	
	public static final String GATEWAY = "PAYEEZY";
	public static final String PAYMENTJS_SECRET = "07MwhBsITAoyDEc0";
	public static final String API_KEY = "mTCVId2jTJbitCi3B5KrpB3cGUXwnaGI";
	public static final String API_SECERET = "845da49f498873586f13ad3b404a2ee0ae55ffa79f53c8d5b62faa94de881e21";
	public static final String AUTH_TOKEN = "fdoa-b39b4ff11685ab046e37e9ab7a0ed9d5a7fb0b192f7598df";
	public static final String TRANSARMOR_TOKEN = "NOIW";
	
	public static final String AUTHORIZE_SESSION_URL = "https://cert.api.firstdata.com/paymentjs/v2/merchant/authorize-session";

//	{
//		  "gateway": "PAYEEZY",
//		  "apiKey": "",
//		  "apiSecret": "",
//		  "authToken": "",
//		  "transarmorToken": "",
//		  "currency": "USD",
//		  "zeroDollarAuth": false
//		}

	public static Requestbody getRequestBody() throws JsonProcessingException {
		Requestbody requestbody = new Requestbody();
		requestbody.setGateway(GATEWAY);
		requestbody.setApiKey(API_KEY);
		requestbody.setApiSecret(API_SECERET);
		requestbody.setAuthToken(AUTH_TOKEN);
		requestbody.setTransarmorToken(TRANSARMOR_TOKEN);
		requestbody.setZeroDollarAuth(false);
		return requestbody;
	}

	public static void main(String[] args) throws URISyntaxException, JsonProcessingException {

		String nonce = String.valueOf(System.currentTimeMillis() + Math.random());  // generateNonce();
		String timestamp = String.valueOf(System.currentTimeMillis());
		
		String message = generateMessage(nonce, timestamp);
		String hmac = genHmac(message, PAYMENTJS_SECRET);

		System.out.println("nonce ============> " + nonce);
		System.out.println("timestamp ============> " + timestamp);
		System.out.println("Hmac =============>" + hmac);

		postRequest(hmac, nonce, timestamp);
	}
	public static void payeezyTestMethod() throws URISyntaxException, JsonProcessingException {

		String nonce = String.valueOf(System.currentTimeMillis() + Math.random());  // generateNonce();
		String timestamp = String.valueOf(System.currentTimeMillis());
		
		String message = generateMessage(nonce, timestamp);
		String hmac = genHmac(message, PAYMENTJS_SECRET);

		System.out.println("nonce ============> " + nonce);
		System.out.println("timestamp ============> " + timestamp);
		System.out.println("Hmac =============>" + hmac);

		postRequest(hmac, nonce, timestamp);
	}

	
	public static void sendData() throws JsonProcessingException, URISyntaxException {
		String nonce = String.valueOf(System.currentTimeMillis() + Math.random());  // generateNonce();
		String timestamp = String.valueOf(System.currentTimeMillis());
		
		String message = generateMessage(nonce, timestamp);
		String hmac = genHmac(message, PAYMENTJS_SECRET);

		System.out.println("nonce ============> " + nonce);
		System.out.println("timestamp ============> " + timestamp);
		System.out.println("Hmac =============>" + hmac);

		postRequest(hmac, nonce, timestamp);
	}
	
	public static String generateMessage(String nonce, String timestamp) throws JsonProcessingException {

		ObjectMapper mapper = new ObjectMapper();
		String requestBody = mapper.writeValueAsString(getRequestBody());

		return API_KEY + nonce + timestamp + requestBody;
	}

	public static void postRequest(String message, String nonce, String timestamp)
			throws URISyntaxException, JsonProcessingException {


		Requestbody requestbody = getRequestBody();

		String length = String.valueOf(requestbody.toString().length());
		System.out.println("Request body length ==========> " + length);
		
		HttpPost postRequest = new HttpPost(AUTHORIZE_SESSION_URL);
		StringBuilder responseBuilder = new StringBuilder();
        try {
            HttpClient httpClient = HttpClientBuilder.create().build();
            StringEntity input = new StringEntity(new com.google.gson.Gson().toJson(requestbody));
            input.setContentType("application/json");
            postRequest.setEntity(input);
            postRequest.setHeader("Api-Key", API_KEY);
            postRequest.setHeader("Message-Signature", message);
            postRequest.setHeader("Nonce", nonce);
            postRequest.setHeader("Timestamp", timestamp);
            
            System.out.println("convertToStringJson : "+postRequest.toString());
            HttpResponse response = httpClient.execute(postRequest);
            System.out.println("Output from Server .... \n");
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            String line = "";
            while ((line = rd.readLine()) != null) {
                responseBuilder.append(line);
            }
            // System.out.println(responseBuilder.toString());
        } catch (MalformedURLException e) {
            LOGGER.error("error: " + e.getMessage());
        } catch (IOException e) {
            LOGGER.error("error: " + e.getMessage());
        }
System.out.println("responseBuilder :: "+responseBuilder);
	}

	public static String genHmac(String msg, String secret) {
		HmacAlgorithms algorithm = HmacAlgorithms.HMAC_SHA_256;
		HmacUtils hmacUtils = new HmacUtils(algorithm, secret);
		Hex hexEncoder = new Hex();

		byte[] binaryEncodedHash = hmacUtils.hmac(msg);
		byte[] hexEncodedHash = hexEncoder.encode(binaryEncodedHash);
		String base64EncodedHash = Base64.encodeBase64String(hexEncodedHash);
		return base64EncodedHash;
	}

	public static String generateNonce() {
		
		String generatedString = new Random().nextInt()+"";

		return generatedString;
	}

}
