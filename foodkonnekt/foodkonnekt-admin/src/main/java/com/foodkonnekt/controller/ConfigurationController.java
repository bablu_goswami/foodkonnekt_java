package com.foodkonnekt.controller;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.firstData.payeezy.PayeezyClientHelperf;
import net.authorize.api.contract.v1.GetCustomerProfileRequest;
import net.authorize.api.contract.v1.GetCustomerProfileResponse;
import net.authorize.api.contract.v1.MerchantAuthenticationType;
import net.authorize.api.contract.v1.MessagesType.Message;
import net.authorize.api.controller.GetCustomerProfileController;
import net.authorize.api.controller.base.ApiOperationBase;

import org.apache.commons.httpclient.util.URIUtil;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.TPISoft.SmartPayments.Response;
import com.TPISoft.SmartPayments.SmartPaymentsSoapProxy;
import com.firstData.paymentGatway.CardConnectRestClientExample;
import com.firstdata.payeezy.PayeezyClientHelper;
import com.firstdata.payeezy.models.transaction.PayeezyResponse;
import com.firstdata.payeezy.models.transaction.ThreeDomainSecureToken;
import com.firstdata.payeezy.models.transaction.Token;
import com.firstdata.payeezy.models.transaction.TransactionRequest;
import com.firstdata.payeezy.models.transaction.Transarmor;
import com.foodkonnekt.model.Address;
import com.foodkonnekt.model.ConvenienceFee;
import com.foodkonnekt.model.LatLongHolder;
import com.foodkonnekt.model.Merchant;
import com.foodkonnekt.model.PaymentGateWay;
import com.foodkonnekt.model.PaymentGatewayName;
import com.foodkonnekt.model.PaymentMode;
import com.foodkonnekt.model.PickUpTime;
import com.foodkonnekt.model.Zone;
import com.foodkonnekt.service.CategoryService;
import com.foodkonnekt.service.ItemService;
import com.foodkonnekt.service.MerchantService;
import com.foodkonnekt.service.ModifierService;
import com.foodkonnekt.service.ZoneService;
import com.foodkonnekt.util.DateUtil;
import com.foodkonnekt.util.EncryptionDecryptionUtil;
import com.foodkonnekt.util.IConstant;
import com.foodkonnekt.util.MailSendUtil;
import com.foodkonnekt.util.PayMentGatwayUtil;
import com.foodkonnekt.util.PayeezyUtilProperties;
import com.foodkonnekt.util.Payeezytest;
import com.foodkonnekt.util.ProducerUtil;
import com.foodkonnekt.util.generateJWT;
import com.stripe.Stripe;
import com.stripe.model.Charge;

@Controller
public class ConfigurationController {

	private static final Logger LOGGER = LoggerFactory.getLogger(ConfigurationController.class);

	@Autowired
    private Environment environment;
	
	@Autowired
	private MerchantService merchantService;

	@Autowired
	private CategoryService categoryService;

	@Autowired
	private ItemService itemService;

	@Autowired
	private ModifierService modifierService;

	@Autowired
	private ZoneService zoneService;

	/**
	 * Get all inventory count from database
	 * 
	 * @param model
	 * @param response
	 * @param request
	 * @return String
	 */

	@RequestMapping(value = "/setup", method = RequestMethod.GET)
	@ResponseBody
	public Map<Object, Object> setup(ModelMap model, HttpServletResponse response, HttpServletRequest request) {
		Map<Object, Object> inventoryCountMap = new HashMap<Object, Object>();
		try {
			LOGGER.info("===============  ConfigurationController : Inside setup :: Start  ============= ");

			HttpSession session = request.getSession();
			String merchantId = (String) session.getAttribute("merchantId");
			LOGGER.info("----------Configuration controller-----merchanid--t" + merchantId);
			Merchant merchant = merchantService.findbyPosId(merchantId);
			LOGGER.info("---merchant---" + merchant);
			if (merchant != null) {

				Long categoryCount = categoryService.categoryCountByMerchantId(merchant.getId());
				Long itemCount = itemService.itemCountByMerchantId(merchant.getId());
				Long modifierGroupCount = modifierService.modifierGroupCountByMerchantId(merchant.getId());
				Long modifierCount = modifierService.modifierCountByMerchantId(merchant.getId());

				inventoryCountMap.put("categoryCount", categoryCount);
				inventoryCountMap.put("itemCount", itemCount);
				inventoryCountMap.put("modifierGroupCount", modifierGroupCount);
				inventoryCountMap.put("modifierCount", modifierCount);

				Integer inventoryThreadStatus = (Integer) session.getAttribute("inventoryThread");
				inventoryCountMap.put("inventoryThreadStatus", inventoryThreadStatus);
				session.setAttribute("merchant", merchant);
				// session.setMaxInactiveInterval(60);
			}
		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
				LOGGER.error(
						"===============  ConfigurationController : Inside setup :: Exception  ============= " + e);

				LOGGER.error("error: " + e.getMessage());
				return null;
			}
		}
		LOGGER.info("===============  ConfigurationController : Inside setup :: End  ============= ");

		return inventoryCountMap;
	}

	@RequestMapping(value = "/welcome", method = RequestMethod.GET)
	public String viewAdminHome(ModelMap model, HttpServletResponse response, HttpServletRequest request) {
		Integer inventoryThreadStatus = 0;
		try {
			LOGGER.info("===============  ConfigurationController : Inside viewAdminHome :: Start  ============= ");

			HttpSession session = request.getSession();
			String merchantId = (String) session.getAttribute("merchantId");
			LOGGER.info("----------Configuration controller-----merchanid--t" + merchantId);
			Merchant merchant = merchantService.findbyPosId(merchantId);

			if (merchant != null) {
				List<Address> address = merchantService.findAddressByMerchantId(merchant.getId());
				String merchantLocation = address.get(0).getAddress1() + " " + address.get(0).getAddress2() + " "
						+ address.get(0).getAddress3() + " " + address.get(0).getCity() + " "
						+ address.get(0).getCountry() + "-" + address.get(0).getZip();
				LOGGER.info("===============  ConfigurationController : Inside viewAdminHome :: merchantLocation == "
						+ merchantLocation);

				/*
				 * merchant.setIsInstall(IConstant.BOOLEAN_FALSE);
				 * merchantService.save(merchant);
				 */
				model.addAttribute("phoneNumber", merchant.getPhoneNumber());
				model.addAttribute("email", merchant.getOwner().getEmail());
				model.addAttribute("address", merchantLocation);

				session.setAttribute("merchant", merchant);
				// session.setMaxInactiveInterval(60);
			}
			if (session.getAttribute("inventoryThread") != null)
				inventoryThreadStatus = (Integer) session.getAttribute("inventoryThread");

		} catch (Exception e) {
			if (e != null) {
				LOGGER.error(
						"===============  ConfigurationController : Inside viewAdminHome :: Exception  ============= "
								+ e);

				MailSendUtil.sendExceptionByMail(e,environment);

				LOGGER.error("error: " + e.getMessage());
				return "redirect:https://www.foodkonnekt.com";
			}
		}
		if (inventoryThreadStatus == 1) {
			LOGGER.info("===============  ConfigurationController : Inside viewAdminHome :: End  ============= ");

			return "welcome";
		} else {
			LOGGER.info("===============  ConfigurationController : Inside viewAdminHome :: End  ============= ");

			return "adminPanel";
		}

	}

	/*
	 * @RequestMapping(value = "/welcome", method = RequestMethod.GET) public String
	 * viewAdminHome(ModelMap model, HttpServletResponse response,
	 * HttpServletRequest request) { try { HttpSession session =
	 * request.getSession(); String merchantId = (String)
	 * session.getAttribute("merchantId");
	 * LOGGER.info("----------Configuration controller-----merchanid--t" +
	 * merchantId); Merchant merchant = merchantService.findbyPosId(merchantId);
	 * LOGGER.info("---merchant---" + merchant); if (merchant != null) {
	 * List<Address> address =
	 * merchantService.findAddressByMerchantId(merchant.getId()); String
	 * merchantLocation = address.get(0).getAddress1() + " " +
	 * address.get(0).getAddress2() + " " + address.get(0).getAddress3() + " " +
	 * address.get(0).getCity() + " " + address.get(0).getCountry() + "-" +
	 * address.get(0).getZip(); Long categoryCount =
	 * categoryService.categoryCountByMerchantId(merchant.getId()); Long itemCount =
	 * itemService.itemCountByMerchantId(merchant.getId()); Long modifierGroupCount
	 * = modifierService.modifierGroupCountByMerchantId(merchant.getId()); Long
	 * modifierCount = modifierService.modifierCountByMerchantId(merchant.getId());
	 * merchant.setIsInstall(IConstant.BOOLEAN_FALSE);
	 * merchantService.save(merchant); model.addAttribute("phoneNumber",
	 * merchant.getPhoneNumber()); model.addAttribute("email",
	 * merchant.getOwner().getEmail()); model.addAttribute("address",
	 * merchantLocation); model.addAttribute("categoryCount", categoryCount);
	 * model.addAttribute("itemCount", itemCount);
	 * model.addAttribute("modifierGroupCount", modifierGroupCount);
	 * model.addAttribute("modifierCount", modifierCount);
	 * session.setAttribute("merchant", merchant); //
	 * session.setMaxInactiveInterval(60); } } catch (Exception e) { if (e != null)
	 * { MailSendUtil.sendExceptionByMail(e,environment);
	 * 
	 * LOGGER.error("error: " + e.getMessage()); return "redirect:https://www.foodkonnekt.com"; } } }
	 * return "welcome"; }
	 */

	/**
	 * Upload Logo of merchant
	 * 
	 * @param model
	 * @return String
	 */
	@RequestMapping(value = "/uploadLogo", method = RequestMethod.GET)
	public String uploadLogo(ModelMap model) {
		return "uploadLogo";
	}

	@SuppressWarnings("unused")
	@RequestMapping(value = "/saveLogo", method = RequestMethod.POST)
	public String save(ModelMap model, HttpServletResponse response, HttpServletRequest request,
			@RequestParam("file") MultipartFile file) {
		try {
			LOGGER.info("===============  ConfigurationController : Inside save :: Start  ============= ");

			HttpSession session = request.getSession();
			// String image = ImageUploadUtils.getImage(file);
			Merchant merchant = (Merchant) session.getAttribute("merchant");
			Long size = file.getSize();
			String orgName = file.getOriginalFilename();
			LOGGER.info("===============  ConfigurationController : Inside save :: orgName== " + orgName);

			if (orgName == null || orgName.isEmpty() || orgName.equals("")) {
				model.addAttribute("filesize", "Please select the logo");
				LOGGER.info("===============  ConfigurationController : Inside save :: End  ============= ");

				return "uploadLogo";
			}
			if (size >= 2097152) {
				model.addAttribute("filesize", "Maximum Allowed File Size is 2 MB");
				LOGGER.info("===============  ConfigurationController : Inside save :: End  ============= ");

				return "uploadLogo";
			}

			String exts[] = orgName.split("\\.");
			if (exts.length > 0) {
				String ext = exts[1];

				String logoName = merchant.getId() + "_" + merchant.getName().replaceAll("[^a-zA-Z0-9]", "") + "."
						+ ext;
				String filePath = environment.getProperty("ADMIN_SERVER_LOGO_PATH") + logoName;
				LOGGER.info("===============  ConfigurationController : Inside save :: filePath== " + filePath);

				File dest = new File(filePath);
				try {
					file.transferTo(dest);
				} catch (IllegalStateException e) {
					LOGGER.error("error: " + e.getMessage());
					LOGGER.error(
							"===============  ConfigurationController : Inside save :: Exception  ============= " + e);
					return "File uploaded failed:" + orgName;
				} catch (IOException e) {
					LOGGER.error("error: " + e.getMessage());
					LOGGER.error(
							"===============  ConfigurationController : Inside save :: Exception  ============= " + e);

					return "File uploaded failed:" + orgName;
				}

				LOGGER.info("File uploaded:" + orgName);

				if (merchant != null) {
					merchant.setMerchantLogo(environment.getProperty("ADMIN_LOGO_PATH_TO_SHOW") + logoName);
					merchantService.save(merchant);
					session.setAttribute("merchant", merchant);
					model.addAttribute("imagePath", environment.getProperty("BASE_PORT") + merchant.getMerchantLogo());
				} else {
					LOGGER.info("===============  ConfigurationController : Inside save :: End  ============= ");

					return "redirect:https://www.foodkonnekt.com";
				}
			}
		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
				LOGGER.error("===============  ConfigurationController : Inside save :: Exception  ============= " + e);

				LOGGER.error("error: " + e.getMessage());
				return "redirect:https://www.foodkonnekt.com";
			}
		}
		String status = request.getParameter("admin");
		if ("admin".equals(status)) {
			LOGGER.info("===============  ConfigurationController : Inside save :: End  ============= ");

			return "redirect:" + environment.getProperty("BASE_URL") + "/adminLogo";
		} else {
			LOGGER.info("===============  ConfigurationController : Inside save :: End  ============= ");

			/* return "redirect:"+environment.getProperty("BASE_URL")+"/setDeliveryZone"; */
			return "uploadLogo";
		}
	}

	/**
	 * Set delivery zone
	 * 
	 * @param model
	 * @return String
	 */
	@RequestMapping(value = "/setDeliveryZone", method = RequestMethod.GET)
	public String setDeliveryZone(@ModelAttribute("Zone") Zone zone, ModelMap model, HttpServletRequest request,
			@RequestParam(required = false) String saveStatus) {
		try {
			LOGGER.info(
					"===============  ConfigurationController : Inside setDeliveryZone :: Start  ============= saveStatus "
							+ saveStatus);

			HttpSession session = request.getSession(false);
			if (session != null) {
				Merchant merchant = (Merchant) session.getAttribute("merchant");
				if (merchant != null) {
					List<Address> addresses = merchantService.findAddressByMerchantId(merchant.getId());
					if (!addresses.isEmpty()) {
						Address address = addresses.get(0);
						String address1 = "";
						if (address.getAddress1() != null) {
							address1 = address.getAddress1();
						}
						;
						String address2 = "";
						if (address.getAddress2() != null) {
							address2 = address.getAddress2();
						}
						;
						String address3 = "";
						if (address.getAddress3() != null) {
							address3 = address.getAddress3();
						};
						
						String country = (address.getCountry()!=null) ? address.getCountry() : "";
						
						model.addAttribute("address", address1 + " " + address2 + " " + address3 + " "
								+ address.getCity() + " " +address.getState()+ " " +country + "-" + address.getZip());
						model.addAttribute("id", address.getId());
					}

				} else {
					LOGGER.info(
							"===============  ConfigurationController : Inside setDeliveryZone :: End  ============= ");

					return "redirect:https://www.foodkonnekt.com";
				}
			}
		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
				LOGGER.error(
						"===============  ConfigurationController : Inside setDeliveryZone :: Exception  ============= "
								+ e);

				LOGGER.error("error: " + e.getMessage());
				return "redirect:https://www.foodkonnekt.com";
			}
		}
		model.addAttribute("saveStatus", saveStatus);
		LOGGER.info("===============  ConfigurationController : Inside setDeliveryZone :: End  ============= ");

		return "setDeliveryZone";
	}

	/**
	 * Save delivery zone
	 * 
	 * @param zone
	 * @param model
	 * @param response
	 * @param request
	 * @return String
	 */
	@RequestMapping(value = "/saveDeliveryZone", method = RequestMethod.POST)
	public String saveDeliveryZone(@ModelAttribute("Zone") Zone zone, ModelMap model, HttpServletResponse response,
			HttpServletRequest request) {
		try {
			LOGGER.info("===============  ConfigurationController : Inside saveDeliveryZone :: Start  ============= ");

			HttpSession session = request.getSession(false);
			Merchant merchant = null;
			if (session != null)
				merchant = (Merchant) session.getAttribute("merchant");
			if (merchant != null) {
				zoneService.save(zone, merchant);
			} else {
				LOGGER.info(
						"===============  ConfigurationController : Inside saveDeliveryZone :: End  ============= ");

				return "redirect:https://www.foodkonnekt.com";
			}
		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
				LOGGER.error(
						"===============  ConfigurationController : Inside saveDeliveryZone :: Exception  ============= "
								+ e);

				LOGGER.error("error: " + e.getMessage());
				return "redirect:https://www.foodkonnekt.com";
			}
		}
		String status = request.getParameter("admin");
		if ("admin".equals(status)) {
			LOGGER.info("===============  ConfigurationController : Inside saveDeliveryZone :: End  ============= ");

			return "redirect:" + environment.getProperty("BASE_URL") + "/deliveryZones";
		} else {
			// return "redirect:"+environment.getProperty("BASE_URL")+"/setPickupTime";
			model.addAttribute("saveStatus", "yes");
			LOGGER.info("===============  ConfigurationController : Inside saveDeliveryZone :: End  ============= ");

			return "redirect:" + environment.getProperty("BASE_URL") + "/setDeliveryZone";
		}
	}

	@RequestMapping(value = "/checkDeliveryZoneName", method = RequestMethod.GET)
	@ResponseBody
	public String checkDeliveryZoneName(@RequestParam(required = true) String diliveryZoneName,
			@RequestParam(required = false) Integer diliveryZoneId, HttpServletRequest request) {

		try {
			LOGGER.info(
					"===============  ConfigurationController : Inside checkDeliveryZoneName :: Start  ============= ");

			HttpSession session = request.getSession(false);
			Merchant merchant = null;
			if (session != null)
				merchant = (Merchant) session.getAttribute("merchant");
			if (merchant != null) {
				if (diliveryZoneId == null) {
					LOGGER.info(
							"===============  ConfigurationController : Inside checkDeliveryZoneName :: End  ============= ");

					return zoneService.findByMerchantIdAndDeliveryZoneName(merchant.getId(), diliveryZoneName);
				} else {
					LOGGER.info(
							"===============  ConfigurationController : Inside checkDeliveryZoneName :: End  ============= ");

					return zoneService.findByMerchantIdAndDeliveryZoneNameAndZoneId(merchant.getId(), diliveryZoneName,
							diliveryZoneId);
				}
			} else {
				LOGGER.info(
						"===============  ConfigurationController : Inside checkDeliveryZoneName :: End  ============= ");

				return "redirect:https://www.foodkonnekt.com";
			}
		} catch (Exception e) {
			if (e != null) {
				LOGGER.error(
						"===============  ConfigurationController : Inside checkDeliveryZoneName :: Exception  ============= "
								+ e);

				MailSendUtil.sendExceptionByMail(e,environment);

				LOGGER.error("error: " + e.getMessage());

			}
			LOGGER.info(
					"===============  ConfigurationController : Inside checkDeliveryZoneName :: End  ============= ");

			return "redirect:https://www.foodkonnekt.com";
		}

	}

	@RequestMapping(value = "/logs", method = RequestMethod.GET)
	@ResponseBody
	public String logs(@RequestParam(required = true) String appName, @RequestParam(required = true) String event,
			@RequestParam(required = true) String log, HttpServletRequest request) {

		try {
			LOGGER.info("===============  ConfigurationController : Inside logs :: Start  ============= ");

			File file = new File(
					environment.getProperty("FILEPATH") + appName + "/" + "logs_" + DateUtil.findCurrentDateWithYYYYMMDD() + ".txt");
			if (file.getParentFile().mkdir())
				try {
					file.createNewFile();
				} catch (IOException e1) {
					LOGGER.error("===============  ConfigurationController : Inside logs :: IOException  ============= "
							+ e1);

					MailSendUtil.sendExceptionByMail(e1,environment);
					return "failed";
				}

			try {
				FileWriter writer = new FileWriter(file, true);
				BufferedWriter bw = new BufferedWriter(writer);
				bw.write(event + "----->" + log);
				bw.newLine();
				bw.flush();
			} catch (IOException e) {
				LOGGER.error(
						"===============  ConfigurationController : Inside logs :: IOException  ============= " + e);

				MailSendUtil.sendExceptionByMail(e,environment);
				return "failed";
			}
			return "success";
		} catch (Exception e) {
			if (e != null) {
				LOGGER.error("===============  ConfigurationController : Inside logs :: Exception  ============= " + e);

				MailSendUtil.sendExceptionByMail(e,environment);
				LOGGER.error("error: " + e.getMessage());
			}
			return "failed";
		}

	}

	/**
	 * Show set pickup time page
	 * 
	 * @param pickUpTime
	 * @param model
	 * @param request
	 * @return String
	 */
	@RequestMapping(value = "/setPickupTime", method = RequestMethod.GET)
	public String setPickUpTime(@ModelAttribute("PickUpTime") PickUpTime pickUpTime, ModelMap model,
			HttpServletRequest request) {
		try {
			LOGGER.info("===============  ConfigurationController : Inside setPickUpTime :: Start  ============= ");

			HttpSession session = request.getSession(false);
			Merchant merchant = null;
			if (session != null)
				merchant = (Merchant) session.getAttribute("merchant");
			if (merchant != null) {
				List<Address> addresses = merchantService.findAddressByMerchantId(merchant.getId());
				if (!addresses.isEmpty()) {
					Address address = addresses.get(0);
					String address1 = "";
					if (address.getAddress1() != null) {
						address1 = address.getAddress1();
					}
					;
					String address2 = "";
					if (address.getAddress2() != null) {
						address2 = address.getAddress2();
					}
					;
					String address3 = "";
					if (address.getAddress3() != null) {
						address3 = address.getAddress3();
					}
					;
					model.addAttribute("address", address1 + " " + address2 + " " + address3 + " " + address.getCity()
							+ " " + address.getCountry() + "-" + address.getZip());
					model.addAttribute("id", address.getId());
				}
			} else {
				LOGGER.info("===============  ConfigurationController : Inside setPickUpTime :: End  ============= ");

				return "redirect:https://www.foodkonnekt.com";
			}
		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
				LOGGER.error(
						"===============  ConfigurationController : Inside setPickUpTime :: Exception  ============= "
								+ e);

				LOGGER.error("error: " + e.getMessage());
				return "redirect:https://www.foodkonnekt.com";
			}
		}
		LOGGER.info("===============  ConfigurationController : Inside setPickUpTime :: End  ============= ");

		return "setPickupTime";
	}

	/**
	 * Save pickup time
	 * 
	 * @param pickUpTime
	 * @param model
	 * @param request
	 * @return String
	 */
	@RequestMapping(value = "/savePickupTime", method = RequestMethod.POST)
	public String savePickUpTime(@ModelAttribute("PickUpTime") PickUpTime pickUpTime, ModelMap model,
			HttpServletRequest request) {
		try {
			LOGGER.info("===============  ConfigurationController : Inside savePickUpTime :: Start  ============= ");

			HttpSession session = request.getSession(false);
			Merchant merchant = null;
			if (session != null)
				merchant = (Merchant) session.getAttribute("merchant");
			if (merchant != null) {
				pickUpTime.setMerchantId(merchant.getId());
				merchantService.savePickupTime(pickUpTime);
			} else {
				LOGGER.info("===============  ConfigurationController : Inside savePickUpTime :: End  ============= ");

				return "redirect:https://www.foodkonnekt.com";
			}
		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
				LOGGER.error(
						"===============  ConfigurationController : Inside savePickUpTime :: Exception  ============= "
								+ e);

				LOGGER.error("error: " + e.getMessage());
				return "redirect:https://www.foodkonnekt.com";
			}
		}
		String status = request.getParameter("admin");
		if ("admin".equals(status)) {
			LOGGER.info("===============  ConfigurationController : Inside savePickUpTime :: End  ============= ");

			return "redirect:" + environment.getProperty("BASE_URL") + "/pickUpTime";
		} else {
			LOGGER.info("===============  ConfigurationController : Inside savePickUpTime :: End  ============= ");

			model.addAttribute("saveStatus", "yes");
			// return "redirect:"+environment.getProperty("BASE_URL")+"/setConvenienceFee";
			return "setPickupTime";
		}
	}

	/**
	 * Show set convenience fee page
	 * 
	 * @param model
	 * @return String
	 */
	@RequestMapping(value = "/setConvenienceFee", method = RequestMethod.GET)
	public String setConvenienceFee(@ModelAttribute("ConvenienceFee") ConvenienceFee convenienceFee, ModelMap model,
			HttpServletRequest request) {
		try {
			LOGGER.info("===============  ConfigurationController : Inside setConvenienceFee :: Start  ============= ");

			HttpSession session = request.getSession(false);
			Merchant merchant = null;
			if (session != null)
				merchant = (Merchant) session.getAttribute("merchant");
			if (merchant != null) {
				List<Address> addresses = merchantService.findAddressByMerchantId(merchant.getId());
				if (!addresses.isEmpty()) {
					Address address = addresses.get(0);

					String address1 = "";
					if (address.getAddress1() != null) {
						address1 = address.getAddress1();
					}
					;
					String address2 = "";
					if (address.getAddress2() != null) {
						address2 = address.getAddress2();
					}
					;
					String address3 = "";
					if (address.getAddress3() != null) {
						address3 = address.getAddress3();
					}
					;
					model.addAttribute("address", address1 + " " + address2 + " " + address3 + " " + address.getCity()
							+ " " + address.getCountry() + "-" + address.getZip());
					model.addAttribute("id", address.getId());
				}
			} else {
				LOGGER.info(
						"===============  ConfigurationController : Inside setConvenienceFee :: End  ============= ");

				return "redirect:https://www.foodkonnekt.com";
			}
		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
				LOGGER.error(
						"===============  ConfigurationController : Inside setConvenienceFee :: Exception  ============= "
								+ e);

				LOGGER.error("error: " + e.getMessage());
				return "redirect:https://www.foodkonnekt.com";
			}
		}
		LOGGER.info("===============  ConfigurationController : Inside setConvenienceFee :: End  ============= ");

		return "setConvenienceFee";
	}

	/**
	 * Save conveninece fee
	 * 
	 * @param convenienceFee
	 * @param model
	 * @param request
	 * @return String
	 */
	@RequestMapping(value = "/saveConvenienceFee", method = RequestMethod.POST)
	public String saveConvenienceFee(@ModelAttribute("ConvenienceFee") ConvenienceFee convenienceFee, ModelMap model,
			HttpServletRequest request) {
		try {
			LOGGER.info(
					"===============  ConfigurationController : Inside saveConvenienceFee :: Start  ============= ");

			HttpSession session = request.getSession(false);
			Merchant merchant = null;
			if (session != null)
				merchant = (Merchant) session.getAttribute("merchant");
			if (merchant != null) {
				convenienceFee.setMerchantId(merchant.getId());
				merchantService.saveConvenienceFee(convenienceFee, merchant);
			} else {
				LOGGER.info(
						"===============  ConfigurationController : Inside saveConvenienceFee :: End  ============= ");

				return "redirect:https://www.foodkonnekt.com";
			}
		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);

				LOGGER.error("error: " + e.getMessage());
				LOGGER.error(
						"===============  ConfigurationController : Inside saveConvenienceFee :: Exception  ============= "
								+ e);

				return "redirect:https://www.foodkonnekt.com";
			}
		}
		String status = request.getParameter("admin");
		if ("admin".equals(status)) {
			LOGGER.info("===============  ConfigurationController : Inside saveConvenienceFee :: End  ============= ");

			return "redirect:" + environment.getProperty("BASE_URL") + "/convenienceFee";
		} else {
			LOGGER.info("===============  ConfigurationController : Inside saveConvenienceFee :: End  ============= ");

			model.addAttribute("saveStatus", "yes");
			// return "redirect:"+environment.getProperty("BASE_URL")+"/adminPanel";
			return "setConvenienceFee";
		}
	}

	/**
	 * Show admin panel page
	 * 
	 * @param model
	 * @return String
	 */
	@RequestMapping(value = "/adminPanel", method = RequestMethod.GET)
	public String setAdminPanel(ModelMap model, HttpServletRequest request) {
		LOGGER.info("===============  ConfigurationController : Inside setAdminPanel :: Start  ============= ");

		HttpSession session = request.getSession(false);
		int inventoryThreadStatus = 0;
		if (session != null) {
			if (session.getAttribute("inventoryThread") != null)
				inventoryThreadStatus = (Integer) session.getAttribute("inventoryThread");
		}
		model.addAttribute("inventoryThreadStatus", inventoryThreadStatus);
		LOGGER.info("===============  ConfigurationController : Inside setAdminPanel :: End  ============= ");

		return "adminPanel";
	}

	@RequestMapping(value = "/logOut", method = RequestMethod.GET)
	public void logOutRedirect(ModelMap model, HttpServletResponse response) {
		try {
			response.sendRedirect("" + environment.getProperty("BASE_URL") + "/support");
		} catch (IOException e) {
			LOGGER.error("===============  ConfigurationController : Inside logOutRedirect :: Exception  ============= "
					+ e);

			LOGGER.error("error: " + e.getMessage());
		}
	}

	@RequestMapping(value = "/adminLogo", method = RequestMethod.GET)
	public String adminLogo(ModelMap model) {
		return "adminLogo";
	}

	@RequestMapping(value = "/addDeliveryZone", method = RequestMethod.GET)
	public String addDeliveryZone(@ModelAttribute("Zone") Zone zone, ModelMap model, HttpServletRequest request) {
		try {
			LOGGER.info("===============  ConfigurationController : Inside addDeliveryZone :: Start  ============= ");

			HttpSession session = request.getSession(false);
			Merchant merchant = null;
			if (session != null)
				merchant = (Merchant) session.getAttribute("merchant");
			if (merchant != null) {
				List<Address> addresses = merchantService.findAddressByMerchantId(merchant.getId());
				if (!addresses.isEmpty()) {
					Address address = addresses.get(0);
					if (address != null) {
						String merchantAddress = "";
						if (address.getAddress1() != null)
							merchantAddress = merchantAddress + address.getAddress1();
						if (address.getAddress2() != null)
							merchantAddress = merchantAddress + " " + address.getAddress2();
						if (address.getAddress3() != null)
							merchantAddress = merchantAddress + " " + address.getAddress3();
						if (address.getCity() != null)
							merchantAddress = merchantAddress + " " + address.getCity();
						if (address.getCountry() != null)
							merchantAddress = merchantAddress + " " + address.getCountry();
						if (address.getZip() != null)
							merchantAddress = merchantAddress + "-" + address.getZip();

						model.addAttribute("address", merchantAddress);
						googleMap(merchantAddress, model);

					}

					model.addAttribute("id", address.getId());
				}
				// model.addAttribute("zones",
				// zoneService.findZoneByMerchantId(merchant.getId()));
				List<Zone> zoneList = zoneService.findZoneByMerchantId(merchant.getId());
				model.addAttribute("zones", zoneList);
				if (zoneList != null && zoneList.size() > 0) {
					if (zoneList.get(0).getPolygons() != null && zoneList.get(0).getPolygons() != "")
						model.addAttribute("lastPolygone", zoneList.get(0).getPolygons());

				} else {
					model.addAttribute("lastPolygone", "not available");
				}
			} else {
				LOGGER.info("===============  ConfigurationController : Inside addDeliveryZone :: End  ============= ");

				return "redirect:https://www.foodkonnekt.com";
			}
		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
				LOGGER.error(
						"===============  ConfigurationController : Inside addDeliveryZone :: Exception  ============= "
								+ e);

				LOGGER.error("error: " + e.getMessage());
				return "redirect:https://www.foodkonnekt.com";
			}
		}
		LOGGER.info("===============  ConfigurationController : Inside addDeliveryZone :: End  ============= ");

		return "addDeliveryZone";
	}

	@RequestMapping(value = "/deliveryZones", method = RequestMethod.GET)
	public String showZones(@ModelAttribute("Zone") Zone zone, ModelMap model, HttpServletRequest request) {
		try {
			LOGGER.info("===============  ConfigurationController : Inside showZones :: Start  ============= ");

			HttpSession session = request.getSession(false);
			Merchant merchant = null;
			if (session != null) {
				merchant = (Merchant) session.getAttribute("merchant");
				if (merchant != null) {
					model.addAttribute("zones", zoneService.findZoneByMerchantId(merchant.getId()));
					model.addAttribute("times", DateUtil.findAllTime());
					model.addAttribute("weekDays", DateUtil.findAllWeekDays());
				} else {
					LOGGER.info("===============  ConfigurationController : Inside showZones :: End  ============= ");

					return "redirect:" + environment.getProperty("BASE_URL") + "/support";
				}
			} else
				return "redirect:" + environment.getProperty("BASE_URL") + "/support";
		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
				LOGGER.error(
						"===============  ConfigurationController : Inside showZones :: Exception  ============= " + e);

				LOGGER.error("error: " + e.getMessage());
				return "redirect:" + environment.getProperty("BASE_URL") + "/support";
			}
		}
		LOGGER.info("===============  ConfigurationController : Inside showZones :: End  ============= ");

		return "deliveryZones";
	}

	/**
	 * Update delivery zone
	 * 
	 * @param zone
	 * @param model
	 * @param deliveryZoneId
	 * @return String
	 */
	@RequestMapping(value = "/updateDelivery", method = RequestMethod.GET)
	public String viewCustomerOrders(@ModelAttribute("Zone") Zone zone, ModelMap model, HttpServletRequest request,
			@RequestParam(required = false) int deliveryZoneId) {
		try {
			LOGGER.info(
					"===============  ConfigurationController : Inside viewCustomerOrders :: Start  ============= ");

			Zone zoneDetail = zoneService.findById(deliveryZoneId);
			model.addAttribute("Zone", zoneDetail);
			model.addAttribute("ZoneStatus", zoneDetail.getZoneStatus());
			HttpSession session = request.getSession(false);
			Merchant merchant = null;
			if (session != null)
				merchant = (Merchant) session.getAttribute("merchant");
			if (merchant != null) {
				List<Address> addresses = merchantService.findAddressByMerchantId(merchant.getId());
				if (!addresses.isEmpty()) {
					Address address = addresses.get(0);
					if (address != null) {
						String merchantAddress = "";
						if (address.getAddress1() != null)
							merchantAddress = merchantAddress + address.getAddress1();
						if (address.getAddress2() != null)
							merchantAddress = merchantAddress + " " + address.getAddress2();
						if (address.getAddress3() != null)
							merchantAddress = merchantAddress + " " + address.getAddress3();
						if (address.getCity() != null)
							merchantAddress = merchantAddress + " " + address.getCity();
						if (address.getCountry() != null)
							merchantAddress = merchantAddress + " " + address.getCountry();
						if (address.getZip() != null)
							merchantAddress = merchantAddress + "-" + address.getZip();

						model.addAttribute("address", merchantAddress);
						googleMap(merchantAddress, model);
					}
					model.addAttribute("id", address.getId());
				}
				List<LatLongHolder> latLongList = new ArrayList<LatLongHolder>();
				if (zoneDetail.getPolygons() != null && zoneDetail.getPolygons() != "") {

					String polygones = zoneDetail.getPolygons();
					LOGGER.info(polygones);

					String pN = polygones.replaceAll("\\(", "").replaceAll("\\)", "");
					String spiltedPolygon[] = pN.split(",");
					String poly[] = new String[spiltedPolygon.length / 2];
					int j = 0;

					for (int k = 0; k < spiltedPolygon.length; k++) {
						LatLongHolder holder = new LatLongHolder();
						if (spiltedPolygon[k] != null) {
							holder.setLat(Double.parseDouble(spiltedPolygon[k]));
							holder.setLng(Double.parseDouble(spiltedPolygon[k + 1]));
						}
						// polygones=[k]+", lng:"+spiltedPolygon[k+1];
						poly[j] = polygones;
						k = k + 1;
						j++;
						latLongList.add(holder);
					}

					model.addAttribute("lastPolygone", latLongList);

				} else {
					model.addAttribute("lastPolygone", "not available");
				}
			} else {
				LOGGER.info(
						"===============  ConfigurationController : Inside viewCustomerOrders :: End  ============= ");

				return "redirect:https://www.foodkonnekt.com";
			}
		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
				LOGGER.error(
						"===============  ConfigurationController : Inside viewCustomerOrders :: Exception  ============= "
								+ e);

				LOGGER.error("error: " + e.getMessage());
				return "redirect:https://www.foodkonnekt.com";
			}
		}
		LOGGER.info("===============  ConfigurationController : Inside viewCustomerOrders :: End  ============= ");

		return "updateDeliveryZone";
	}

	@RequestMapping(value = "/pickUpTime", method = RequestMethod.GET)
	public String pickUpTime(@ModelAttribute("PickUpTime") PickUpTime pickUpTime, ModelMap model,
			HttpServletRequest request) {
		try {
			LOGGER.info("===============  ConfigurationController : Inside pickUpTime :: Start  ============= ");

			HttpSession session = request.getSession(false);
			Merchant merchant = null;
			if (session != null)
				merchant = (Merchant) session.getAttribute("merchant");
			if (merchant != null) {
				List<Address> addresses = merchantService.findAddressByMerchantId(merchant.getId());
				if (!addresses.isEmpty()) {
					Address address = addresses.get(0);
					String address1 = "";
					if (address.getAddress1() != null) {
						address1 = address.getAddress1();
					}
					;
					String address2 = "";
					if (address.getAddress2() != null) {
						address2 = address.getAddress2();
					}
					;
					String address3 = "";
					if (address.getAddress3() != null) {
						address3 = address.getAddress3();
					}
					;
					model.addAttribute("address", address1 + " " + address2 + " " + address3 + " " + address.getCity()
							+ " " + address.getCountry() + "-" + address.getZip());
					model.addAttribute("id", address.getId());
				}
			} else {
				LOGGER.info("===============  ConfigurationController : Inside pickUpTime :: End  ============= ");

				return "redirect:https://www.foodkonnekt.com";
			}
		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
				LOGGER.error("===============  ConfigurationController : Inside pickUpTime :: Exception  ============= "
						+ e);

				LOGGER.error("error: " + e.getMessage());
				return "redirect:https://www.foodkonnekt.com";
			}
		}
		LOGGER.info("===============  ConfigurationController : Inside pickUpTime :: End  ============= ");

		return "pickUpTime";
	}

	@RequestMapping(value = "/convenienceFee", method = RequestMethod.GET)
	public String convenienceFee(@ModelAttribute("ConvenienceFee") ConvenienceFee convenienceFee, ModelMap model,
			HttpServletRequest request) {
		try {
			LOGGER.info("===============  ConfigurationController : Inside convenienceFee :: Start  ============= ");

			HttpSession session = request.getSession(false);
			Merchant merchant = null;
			if (session != null)
				merchant = (Merchant) session.getAttribute("merchant");
			if (merchant != null) {
				List<Address> addresses = merchantService.findAddressByMerchantId(merchant.getId());
				if (!addresses.isEmpty()) {
					Address address = addresses.get(0);
					String address1 = "";
					if (address.getAddress1() != null) {
						address1 = address.getAddress1();
					}
					;
					String address2 = "";
					if (address.getAddress2() != null) {
						address2 = address.getAddress2();
					}
					;
					String address3 = "";
					if (address.getAddress3() != null) {
						address3 = address.getAddress3();
					}
					;
					model.addAttribute("address", address1 + " " + address2 + " " + address3 + " " + address.getCity()
							+ " " + address.getCountry() + "-" + address.getZip());
					model.addAttribute("id", address.getId());
				}
			} else {
				LOGGER.info("===============  ConfigurationController : Inside convenienceFee :: End  ============= ");

				return "redirect:https://www.foodkonnekt.com";
			}
		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
				LOGGER.error(
						"===============  ConfigurationController : Inside convenienceFee :: Exception  ============= "
								+ e);

				LOGGER.error("error: " + e.getMessage());
				return "redirect:https://www.foodkonnekt.com";
			}
		}
		LOGGER.info("===============  ConfigurationController : Inside convenienceFee :: End  ============= ");

		return "convenienceFee";
	}

	@RequestMapping(value = "/sessionOut", method = RequestMethod.GET)
	public String sessionOut(ModelMap model, HttpServletRequest request) {
		LOGGER.info("===============  ConfigurationController : Inside sessionOut :: Start  ============= ");

		HttpSession session = request.getSession();
		session.invalidate();

		LOGGER.info("===============  ConfigurationController : Inside sessionOut :: End  ============= ");

		return "redirect:https://www.foodkonnekt.com";
	}

	@RequestMapping(value = "/setupPaymentGateway", method = RequestMethod.GET)
	public String setPaymentGateWay(@ModelAttribute("paymentGateWay") PaymentGateWay paymentGateWay, ModelMap model,
			HttpServletRequest request) {
		try {
			LOGGER.info("===============  ConfigurationController : Inside setPaymentGateWay :: Start  ============= ");

			HttpSession session = request.getSession(false);
			Merchant merchant = null;
			List<PaymentGatewayName> gatewayName = merchantService.findAllPaymentGatewayName();
			model.addAttribute("gatewayName", gatewayName);

			if (session != null) {
				merchant = (Merchant) session.getAttribute("merchant");
				if (merchant != null) {
					Boolean isDeleted = false;
					PaymentGateWay details = merchantService.findbyMerchantId(merchant.getId(), isDeleted);
					if (details != null) {
						if (details.getStripeAPIKey() != null) {
							details.setStripeAPIKey(EncryptionDecryptionUtil.decryption(details.getStripeAPIKey()));
						}
						if (details.getAuthorizeLoginId() != null && details.getAuthorizeTransactionKey() != null) {
							details.setAuthorizeLoginId(
									EncryptionDecryptionUtil.decryption(details.getAuthorizeLoginId()));
							details.setAuthorizeTransactionKey(
									EncryptionDecryptionUtil.decryption(details.getAuthorizeTransactionKey()));
						}
						if (details.getCayanMerchantKey() != null && details.getCayanMerchantName() != null
								&& details.getCayanMerchantSiteId() != null) {
							details.setCayanMerchantKey(
									EncryptionDecryptionUtil.decryption(details.getCayanMerchantKey()));
							details.setCayanMerchantName(
									EncryptionDecryptionUtil.decryption(details.getCayanMerchantName()));
							details.setCayanMerchantSiteId(
									EncryptionDecryptionUtil.decryption(details.getCayanMerchantSiteId()));
						}
						if (details.getiGatePassword() != null && details.getiGateUserName() != null) {
							details.setiGatePassword(EncryptionDecryptionUtil.decryption(details.getiGatePassword()));
							details.setiGateUserName(EncryptionDecryptionUtil.decryption(details.getiGateUserName()));
						}

						model.addAttribute("paymentGatewayDetails", details);
					}
				} else {
					LOGGER.info(
							"===============  ConfigurationController : Inside setPaymentGateWay :: End  ============= ");

					return "redirect:" + environment.getProperty("BASE_URL") + "/support";
				}
			} else
				return "redirect:" + environment.getProperty("BASE_URL") + "/support";
		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
				LOGGER.error(
						"===============  ConfigurationController : Inside setPaymentGateWay :: Exception  ============= "
								+ e);

				LOGGER.error("error: " + e.getMessage());
				return "redirect:" + environment.getProperty("BASE_URL") + "/support";
			}
		}
		LOGGER.info("===============  ConfigurationController : Inside setPaymentGateWay :: End  ============= ");

		return "setupPaymentGateway";
	}

	@RequestMapping(value = "/savePaymentGatewayDetails", method = RequestMethod.POST)
	public String savePaymentGatewayDetails(@ModelAttribute("paymentGateWay") PaymentGateWay paymentGateWay,
			ModelMap model, HttpServletRequest request) {
		String response = null;
		try {
			LOGGER.info(
					"===============  ConfigurationController : Inside savePaymentGatewayDetails :: Start  ============= ");

			HttpSession session = request.getSession(false);
			Merchant merchant = null;
			if (session != null)
				merchant = (Merchant) session.getAttribute("merchant");
			if (merchant != null) {
				Date today = (merchant.getTimeZone() != null
						&& merchant.getTimeZone().getTimeZoneCode() != null)
						? DateUtil.getCurrentDateForTimeZonee(merchant.getTimeZone().getTimeZoneCode())
						: new Date();
				Boolean isDeleted = false;
				Boolean isActive = false;
				PaymentGateWay details = merchantService.findbyMerchantId(merchant.getId(), isDeleted);
				if (details != null) {
					details.setDeleted(true);
					details.setActive(false);
					details.setUpdatedDate(today);
					merchantService.savePaymentGateWay(details);
					paymentGateWay.setActive(true);
					paymentGateWay.setMerchantId(merchant.getId());
					paymentGateWay.setCreatedDate(today);
					paymentGateWay.setUpdatedDate(today);
					if (paymentGateWay.getStripeAPIKey() != null && !paymentGateWay.getStripeAPIKey().isEmpty()) {
						response = validateStripeKey(paymentGateWay.getStripeAPIKey());

						if (response == "Success" || response.equals("Success")) {
							paymentGateWay.setStripeAPIKey(
									EncryptionDecryptionUtil.encryption(paymentGateWay.getStripeAPIKey()));

						} else {
							model.addAttribute("response", response);
							LOGGER.info(
									"===============  ConfigurationController : Inside savePaymentGatewayDetails :: End  ============= ");

							return "redirect:" + environment.getProperty("BASE_URL") + "/setupPaymentGateway";
						}
					}
					/*
					 * if(paymentGateWay.getCayanMerchantKey()!=null &&
					 * paymentGateWay.getCayanMerchantName()!=null &&
					 * paymentGateWay.getCayanMerchantSiteId()!=null) {
					 * paymentGateWay.setCayanMerchantKey(EncryptionDecryptionUtil.encryption(
					 * paymentGateWay.getCayanMerchantKey()));
					 * paymentGateWay.setCayanMerchantName(EncryptionDecryptionUtil.encryption(
					 * paymentGateWay.getCayanMerchantName()));
					 * paymentGateWay.setCayanMerchantSiteId(EncryptionDecryptionUtil.encryption(
					 * paymentGateWay.0getCayanMerchantSiteId())); }
					 */
					if (paymentGateWay.getAuthorizeLoginId() != null && !paymentGateWay.getAuthorizeLoginId().isEmpty()
							&& paymentGateWay.getAuthorizeTransactionKey() != null
							&& !paymentGateWay.getAuthorizeTransactionKey().isEmpty()) {

						response = validateAuthrizeDetails(paymentGateWay.getAuthorizeLoginId(),
								paymentGateWay.getAuthorizeTransactionKey(), "1503118872");

						if (response == "Success" || response.equals("Success")) {
							paymentGateWay.setAuthorizeLoginId(
									EncryptionDecryptionUtil.encryption(paymentGateWay.getAuthorizeLoginId()));
							paymentGateWay.setAuthorizeTransactionKey(
									EncryptionDecryptionUtil.encryption(paymentGateWay.getAuthorizeTransactionKey()));

						} else {
							model.addAttribute("response", response);

							LOGGER.info(
									"===============  ConfigurationController : Inside savePaymentGatewayDetails :: End  ============= ");
							return "redirect:" + environment.getProperty("BASE_URL") + "/setupPaymentGateway";
						}

					}
					if (paymentGateWay.getiGatePassword() != null && !paymentGateWay.getiGatePassword().isEmpty()
							&& paymentGateWay.getiGateUserName() != null
							&& !paymentGateWay.getiGateUserName().isEmpty()) {
						response = validateBridgePayDetails(paymentGateWay.getiGateUserName(),
								paymentGateWay.getiGatePassword());
						if (response == "Approved" || response.equals("Approved")) {
							paymentGateWay.setiGateUserName(
									EncryptionDecryptionUtil.encryption(paymentGateWay.getiGateUserName()));
							paymentGateWay.setiGatePassword(
									EncryptionDecryptionUtil.encryption(paymentGateWay.getiGatePassword()));

						} else {
							model.addAttribute("response", response);
							LOGGER.info(
									"===============  ConfigurationController : Inside savePaymentGatewayDetails :: End  ============= ");
							return "redirect:" + environment.getProperty("BASE_URL") + "/setupPaymentGateway";
						}
					}

					if (paymentGateWay.getFirstDataPassword() != null
							&& !paymentGateWay.getFirstDataPassword().isEmpty()
							&& paymentGateWay.getFirstDataUserName() != null
							&& !paymentGateWay.getFirstDataUserName().isEmpty()
							&& paymentGateWay.getFirstDataMerchantId() != null
							&& !paymentGateWay.getFirstDataMerchantId().isEmpty()) {

						boolean apiResponse = validateFirstDataDetails(paymentGateWay.getFirstDataMerchantId(),
								paymentGateWay.getFirstDataUserName(), paymentGateWay.getFirstDataPassword());
						if (apiResponse) {
							paymentGateWay.setFirstDataUserName(
									EncryptionDecryptionUtil.encryption(paymentGateWay.getFirstDataUserName()));
							paymentGateWay.setFirstDataPassword(
									EncryptionDecryptionUtil.encryption(paymentGateWay.getFirstDataPassword()));
							paymentGateWay.setFirstDataMerchantId(
									EncryptionDecryptionUtil.encryption(paymentGateWay.getFirstDataMerchantId()));
						} else {
							model.addAttribute("response", "Unauthorized");
							LOGGER.info(
									"===============  ConfigurationController : Inside savePaymentGatewayDetails :: End  ============= ");
							return "redirect:" + environment.getProperty("BASE_URL") + "/setupPaymentGateway";
						}
					}

					if (paymentGateWay.getPayeezyMerchnatToken() != null
							&& !paymentGateWay.getPayeezyMerchnatToken().isEmpty()) {
						PayeezyClientHelper client = PayeezyUtilProperties.getPayeezyProperties(paymentGateWay,environment);
						String responseDoAuthorizePayeezyPayment = doAuthorizePayeezyPayment(client);
						if (responseDoAuthorizePayeezyPayment.equals("approved")) {
							paymentGateWay.setPayeezyMerchnatToken(
									EncryptionDecryptionUtil.encryption(paymentGateWay.getPayeezyMerchnatToken()));
						} else {
							model.addAttribute("response", "Unauthorized");
							LOGGER.info(
									"===============  ConfigurationController : Inside savePaymentGatewayDetails :: End  ============= ");
							return "redirect:" + environment.getProperty("BASE_URL") + "/setupPaymentGateway";
						}

					}
					merchantService.savePaymentGateWay(paymentGateWay);

					PaymentMode mode = merchantService.findByMerchantIdAndLabel(merchant.getId(), "Credit Card");
					if (mode != null && mode.getAllowPaymentMode() == 0) {
						mode.setMerchant(merchant);
						mode.setAllowPaymentMode(1);
						merchantService.savePaymentMode(mode);
					}
				} else {
					paymentGateWay.setActive(true);
					paymentGateWay.setMerchantId(merchant.getId());
					paymentGateWay.setCreatedDate(today);
					paymentGateWay.setUpdatedDate(today);
					if (paymentGateWay.getStripeAPIKey() != null && !paymentGateWay.getStripeAPIKey().isEmpty()) {
						response = validateStripeKey(paymentGateWay.getStripeAPIKey());

						if (response == "Success" || response.equals("Success")) {
							paymentGateWay.setStripeAPIKey(
									EncryptionDecryptionUtil.encryption(paymentGateWay.getStripeAPIKey()));
						} else {
							model.addAttribute("response", response);
							LOGGER.info(
									"===============  ConfigurationController : Inside savePaymentGatewayDetails :: End  ============= ");
							return "redirect:" + environment.getProperty("BASE_URL") + "/setupPaymentGateway";
						}
					}
					/*
					 * if(paymentGateWay.getCayanMerchantKey()!=null &&
					 * paymentGateWay.getCayanMerchantName()!=null &&
					 * paymentGateWay.getCayanMerchantSiteId()!=null) {
					 * paymentGateWay.setCayanMerchantKey(EncryptionDecryptionUtil.encryption(
					 * paymentGateWay.getCayanMerchantKey()));
					 * paymentGateWay.setCayanMerchantName(EncryptionDecryptionUtil.encryption(
					 * paymentGateWay.getCayanMerchantName()));
					 * paymentGateWay.setCayanMerchantSiteId(EncryptionDecryptionUtil.encryption(
					 * paymentGateWay.getCayanMerchantSiteId())); }
					 */
					if (paymentGateWay.getAuthorizeLoginId() != null && !paymentGateWay.getAuthorizeLoginId().isEmpty()
							&& paymentGateWay.getAuthorizeTransactionKey() != null
							&& !paymentGateWay.getAuthorizeTransactionKey().isEmpty()) {

						response = validateAuthrizeDetails(paymentGateWay.getAuthorizeLoginId(),
								paymentGateWay.getAuthorizeTransactionKey(), "1503118872");

						if (response == "Success" || response.equals("Success")) {
							paymentGateWay.setAuthorizeLoginId(
									EncryptionDecryptionUtil.encryption(paymentGateWay.getAuthorizeLoginId()));
							paymentGateWay.setAuthorizeTransactionKey(
									EncryptionDecryptionUtil.encryption(paymentGateWay.getAuthorizeTransactionKey()));
						} else {
							model.addAttribute("response", response);
							LOGGER.info(
									"===============  ConfigurationController : Inside savePaymentGatewayDetails :: End  ============= ");
							return "redirect:" + environment.getProperty("BASE_URL") + "/setupPaymentGateway";
						}

					}
					if (paymentGateWay.getiGatePassword() != null && !paymentGateWay.getiGatePassword().isEmpty()
							&& paymentGateWay.getiGateUserName() != null
							&& !paymentGateWay.getiGateUserName().isEmpty()) {
						response = validateBridgePayDetails(paymentGateWay.getiGateUserName(),
								paymentGateWay.getiGatePassword());
						if (response == "Approved" || response.equals("Approved")) {
							paymentGateWay.setiGateUserName(
									EncryptionDecryptionUtil.encryption(paymentGateWay.getiGateUserName()));
							paymentGateWay.setiGatePassword(
									EncryptionDecryptionUtil.encryption(paymentGateWay.getiGatePassword()));

						} else {
							model.addAttribute("response", response);
							LOGGER.info(
									"===============  ConfigurationController : Inside savePaymentGatewayDetails :: End  ============= ");
							return "redirect:" + environment.getProperty("BASE_URL") + "/setupPaymentGateway";
						}
					}

					if (paymentGateWay.getFirstDataPassword() != null
							&& !paymentGateWay.getFirstDataPassword().isEmpty()
							&& paymentGateWay.getFirstDataUserName() != null
							&& !paymentGateWay.getFirstDataUserName().isEmpty()
							&& paymentGateWay.getFirstDataMerchantId() != null
							&& !paymentGateWay.getFirstDataMerchantId().isEmpty()) {
						boolean apiResponse = validateFirstDataDetails(paymentGateWay.getFirstDataMerchantId(),
								paymentGateWay.getFirstDataUserName(), paymentGateWay.getFirstDataPassword());

						if (apiResponse) {
							paymentGateWay.setFirstDataUserName(
									EncryptionDecryptionUtil.encryption(paymentGateWay.getFirstDataUserName()));
							paymentGateWay.setFirstDataPassword(
									EncryptionDecryptionUtil.encryption(paymentGateWay.getFirstDataPassword()));
							paymentGateWay.setFirstDataMerchantId(
									EncryptionDecryptionUtil.encryption(paymentGateWay.getFirstDataMerchantId()));
						} else {
							model.addAttribute("response", "Unauthorized");
							LOGGER.info(
									"===============  ConfigurationController : Inside savePaymentGatewayDetails :: End  ============= ");
							return "redirect:" + environment.getProperty("BASE_URL") + "/setupPaymentGateway";
						}
					}

					if (paymentGateWay.getPayeezyMerchnatToken() != null
							&& !paymentGateWay.getPayeezyMerchnatToken().isEmpty()) {
						PayeezyClientHelper client = PayeezyUtilProperties.getPayeezyProperties(paymentGateWay,environment);
						String responseDoAuthorizePayeezyPayment = doAuthorizePayeezyPayment(client);
						if (responseDoAuthorizePayeezyPayment != null
								&& responseDoAuthorizePayeezyPayment.equals("approved")) {
							paymentGateWay.setPayeezyMerchnatToken(
									EncryptionDecryptionUtil.encryption(paymentGateWay.getPayeezyMerchnatToken()));
						} else {
							model.addAttribute("response", "Unauthorized");
							LOGGER.info(
									"===============  ConfigurationController : Inside savePaymentGatewayDetails :: End  ============= ");
							return "redirect:" + environment.getProperty("BASE_URL") + "/setupPaymentGateway";
						}
					}
					merchantService.savePaymentGateWay(paymentGateWay);
					PaymentMode mode = merchantService.findByMerchantIdAndLabel(merchant.getId(), "Credit Card");
					if (mode != null && mode.getAllowPaymentMode() == 0) {
						mode.setMerchant(merchant);
						mode.setAllowPaymentMode(1);
						merchantService.savePaymentMode(mode);
					}
				}

			} else {
				LOGGER.info(
						"===============  ConfigurationController : Inside savePaymentGatewayDetails :: End  ============= ");
				return "redirect:https://www.foodkonnekt.com";
			}
		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
				LOGGER.error(
						"===============  ConfigurationController : Inside savePaymentGatewayDetails :: Exception  ============= "
								+ e);

				LOGGER.error("error: " + e.getMessage());
				return "redirect:https://www.foodkonnekt.com";
			}
		}
		String status = request.getParameter("admin");
		if ("admin".equals(status)) {
			LOGGER.info(
					"===============  ConfigurationController : Inside savePaymentGatewayDetails :: End  ============= ");
			return "redirect:" + environment.getProperty("BASE_URL") + "/setupPaymentGateway";
		} else {
			LOGGER.info(
					"===============  ConfigurationController : Inside savePaymentGatewayDetails :: End  ============= ");
			return "redirect:" + environment.getProperty("BASE_URL") + "/setupPaymentGateway";
			// return "setupPaymentGateway";
		}

	}

	@RequestMapping(value = "/removePaymentGateway", method = RequestMethod.GET)
	@ResponseBody
	public String removePaymentGateway(HttpServletRequest request,
			@RequestParam(value = "gateWayType") String gateWayType) {
		LOGGER.info("===============  ConfigurationController : Inside removePaymentGateway :: Start  ============= ");

		String response = "FAIL";
		try {
			HttpSession session = request.getSession();
			Merchant merchant = (Merchant) session.getAttribute("merchant");
			PaymentGateWay details = new PaymentGateWay();
			if (merchant != null) {
				/*
				 * details.setActive(false); details.setDeleted(true);
				 * details.setMerchantId(merchant.getId());
				 */
				Boolean isDeleted = false;
				Boolean isActive = true;
				PaymentGateWay paymentDetails = merchantService.findPaymentGatewayDetails(merchant.getId(), isDeleted,
						isActive);
				if (paymentDetails != null) {
					LOGGER.info("===============  ConfigurationController : Inside if (paymentDetails != null) ");

					java.util.Date date = new java.util.Date();
					paymentDetails.setActive(false);
					paymentDetails.setDeleted(true);
					paymentDetails.setUpdatedDate(date);
					merchantService.savePaymentGateWay(paymentDetails);
					PaymentMode mode = merchantService.findByMerchantIdAndLabel(merchant.getId(), "Credit Card");
					if (mode != null) {
						mode.setAllowPaymentMode(0);
						merchantService.savePaymentMode(mode);
					}
					LOGGER.info(
							"===============  ConfigurationController : Inside removePaymentGateway :: End  ============= ");

					response = "SUCCESS";
				}
			}

		} catch (Exception exception) {
			if (exception != null) {
				// MailSendUtil.sendExceptionByMail(exception,environment);
			}
			LOGGER.error(
					"===============  ConfigurationController : Inside removePaymentGateway :: Exception  ============= "
							+ exception);

			exception.printStackTrace();

		}

		LOGGER.info("===============  ConfigurationController : Inside removePaymentGateway :: End  ============= ");
		return response;
	}

	// setPaymentGateway
	@RequestMapping(value = "/setPaymentGateway", method = RequestMethod.GET)
	public String setPaymentGatewayViaInstallMerchant(@ModelAttribute("paymentGateWay") PaymentGateWay paymentGateWay,
			ModelMap model, HttpServletRequest request) {
		try {
			LOGGER.info(
					"===============  ConfigurationController : Inside setPaymentGatewayViaInstallMerchant :: Start  ============= ");

			HttpSession session = request.getSession(false);
			Merchant merchant = null;
			List<PaymentGatewayName> gatewayName = merchantService.findAllPaymentGatewayName();
			model.addAttribute("gatewayName", gatewayName);

			if (session != null)
				merchant = (Merchant) session.getAttribute("merchant");
			if (merchant != null) {
				Boolean isDeleted = false;
				PaymentGateWay details = merchantService.findbyMerchantId(merchant.getId(), isDeleted);
				if (details != null) {
					if (details.getStripeAPIKey() != null) {
						details.setStripeAPIKey(EncryptionDecryptionUtil.decryption(details.getStripeAPIKey()));
					}
					if (details.getAuthorizeLoginId() != null && details.getAuthorizeTransactionKey() != null) {
						details.setAuthorizeLoginId(EncryptionDecryptionUtil.decryption(details.getAuthorizeLoginId()));
						details.setAuthorizeTransactionKey(
								EncryptionDecryptionUtil.decryption(details.getAuthorizeTransactionKey()));
					}
					if (details.getCayanMerchantKey() != null && details.getCayanMerchantName() != null
							&& details.getCayanMerchantSiteId() != null) {
						details.setCayanMerchantKey(EncryptionDecryptionUtil.decryption(details.getCayanMerchantKey()));
						details.setCayanMerchantName(
								EncryptionDecryptionUtil.decryption(details.getCayanMerchantName()));
						details.setCayanMerchantSiteId(
								EncryptionDecryptionUtil.decryption(details.getCayanMerchantSiteId()));
					}
					if (details.getiGatePassword() != null && details.getiGateUserName() != null) {
						details.setiGatePassword(EncryptionDecryptionUtil.decryption(details.getiGatePassword()));
						details.setiGateUserName(EncryptionDecryptionUtil.decryption(details.getiGateUserName()));
					}
					model.addAttribute("paymentGatewayDetails", details);

				}
			} else {
				LOGGER.info(
						"===============  ConfigurationController : Inside setPaymentGatewayViaInstallMerchant :: End  ============= ");

				return "redirect:https://www.foodkonnekt.com";
			}
		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
				LOGGER.error(
						"===============  ConfigurationController : Inside setPaymentGatewayViaInstallMerchant :: Exception  ============= "
								+ e);

				LOGGER.error("error: " + e.getMessage());
				return "redirect:https://www.foodkonnekt.com";
			}
		}
		LOGGER.info(
				"===============  ConfigurationController : Inside setPaymentGatewayViaInstallMerchant :: End  ============= ");

		return "setPaymentGateway";
	}

	@RequestMapping(value = "/savePaymentGatewayDetailsViaInstallMerchant", method = RequestMethod.POST)
	public String savePaymentGatewayDetailsViaInstallMerchant(
			@ModelAttribute("paymentGateWay") PaymentGateWay paymentGateWay, ModelMap model,
			HttpServletRequest request) {
		try {
			LOGGER.info(
					"===============  ConfigurationController : Inside savePaymentGatewayDetailsViaInstallMerchant :: Start  ============= ");

			HttpSession session = request.getSession(false);
			Merchant merchant = null;
			if (session != null)
				merchant = (Merchant) session.getAttribute("merchant");
			if (merchant != null) {
				Date today = (merchant.getTimeZone() != null
						&& merchant.getTimeZone().getTimeZoneCode() != null)
						? DateUtil.getCurrentDateForTimeZonee(merchant.getTimeZone().getTimeZoneCode())
						: new Date();
				Boolean isDeleted = false;
				Boolean isActive = false;
				PaymentGateWay details = merchantService.findbyMerchantId(merchant.getId(), isDeleted);
				if (details != null) {
					details.setDeleted(true);
					details.setActive(false);
					details.setUpdatedDate(today);
					merchantService.savePaymentGateWay(details);
					paymentGateWay.setActive(true);
					paymentGateWay.setMerchantId(merchant.getId());
					paymentGateWay.setCreatedDate(today);
					paymentGateWay.setUpdatedDate(today);

					if (paymentGateWay.getStripeAPIKey() != null && !paymentGateWay.getStripeAPIKey().isEmpty()) {
						String response = validateStripeKey(paymentGateWay.getStripeAPIKey());

						if (response == "Success" || response.equals("Success")) {
							paymentGateWay.setStripeAPIKey(
									EncryptionDecryptionUtil.encryption(paymentGateWay.getStripeAPIKey()));
						} else {
							LOGGER.info(
									"===============  ConfigurationController : Inside savePaymentGatewayDetailsViaInstallMerchant :: End  ============= ");
							LOGGER.info(
									"===============  ConfigurationController : Inside savePaymentGatewayDetailsViaInstallMerchant :: response== "
											+ response);

							return response;
						}
					}
					/*
					 * if(paymentGateWay.getCayanMerchantKey()!=null &&
					 * paymentGateWay.getCayanMerchantName()!=null &&
					 * paymentGateWay.getCayanMerchantSiteId()!=null) {
					 * paymentGateWay.setCayanMerchantKey(EncryptionDecryptionUtil.encryption(
					 * paymentGateWay.getCayanMerchantKey()));
					 * paymentGateWay.setCayanMerchantName(EncryptionDecryptionUtil.encryption(
					 * paymentGateWay.getCayanMerchantName()));
					 * paymentGateWay.setCayanMerchantSiteId(EncryptionDecryptionUtil.encryption(
					 * paymentGateWay.getCayanMerchantSiteId())); }
					 */
					if (paymentGateWay.getAuthorizeLoginId() != null && !paymentGateWay.getAuthorizeLoginId().isEmpty()
							&& paymentGateWay.getAuthorizeTransactionKey() != null
							&& !paymentGateWay.getAuthorizeTransactionKey().isEmpty()) {

						String response = validateAuthrizeDetails(paymentGateWay.getAuthorizeLoginId(),
								paymentGateWay.getAuthorizeTransactionKey(), "1503118872");

						if (response == "Success" || response.equals("Success")) {
							paymentGateWay.setAuthorizeLoginId(
									EncryptionDecryptionUtil.encryption(paymentGateWay.getAuthorizeLoginId()));
							paymentGateWay.setAuthorizeTransactionKey(
									EncryptionDecryptionUtil.encryption(paymentGateWay.getAuthorizeTransactionKey()));
						} else {
							return response;
						}

					}
					if (paymentGateWay.getiGatePassword() != null && !paymentGateWay.getiGatePassword().isEmpty()
							&& paymentGateWay.getiGateUserName() != null
							&& !paymentGateWay.getiGateUserName().isEmpty()) {
						String response = validateBridgePayDetails(paymentGateWay.getiGateUserName(),
								paymentGateWay.getiGatePassword());
						if (response == "Approved" || response.equals("Approved")) {
							paymentGateWay.setiGateUserName(
									EncryptionDecryptionUtil.encryption(paymentGateWay.getiGateUserName()));
							paymentGateWay.setiGatePassword(
									EncryptionDecryptionUtil.encryption(paymentGateWay.getiGatePassword()));

						} else {
							LOGGER.info(
									"===============  ConfigurationController : Inside savePaymentGatewayDetailsViaInstallMerchant returns response== "
											+ response);

							return response;
						}
					}

					merchantService.savePaymentGateWay(paymentGateWay);

					PaymentMode mode = merchantService.findByMerchantIdAndLabel(merchant.getId(), "Credit Card");
					if (mode != null && mode.getAllowPaymentMode() == 0) {
						mode.setMerchant(merchant);
						mode.setAllowPaymentMode(1);
						merchantService.savePaymentMode(mode);
					}
				} else {
					paymentGateWay.setActive(true);
					paymentGateWay.setMerchantId(merchant.getId());
					paymentGateWay.setCreatedDate(today);
					paymentGateWay.setUpdatedDate(today);

					/*
					 * if(paymentGateWay.getStripeAPIKey()!=null){
					 * paymentGateWay.setStripeAPIKey(EncryptionDecryptionUtil.encryption(
					 * paymentGateWay.getStripeAPIKey())); }
					 * if(paymentGateWay.getCayanMerchantKey()!=null &&
					 * paymentGateWay.getCayanMerchantName()!=null &&
					 * paymentGateWay.getCayanMerchantSiteId()!=null){
					 * paymentGateWay.setCayanMerchantKey(EncryptionDecryptionUtil.encryption(
					 * paymentGateWay.getCayanMerchantKey()));
					 * paymentGateWay.setCayanMerchantName(EncryptionDecryptionUtil.encryption(
					 * paymentGateWay.getCayanMerchantName()));
					 * paymentGateWay.setCayanMerchantSiteId(EncryptionDecryptionUtil.encryption(
					 * paymentGateWay.getCayanMerchantSiteId())); }
					 * if(paymentGateWay.getAuthorizeLoginId()!=null &&
					 * paymentGateWay.getAuthorizeTransactionKey()!=null){
					 * paymentGateWay.setAuthorizeLoginId(EncryptionDecryptionUtil.encryption(
					 * paymentGateWay.getAuthorizeLoginId()));
					 * paymentGateWay.setAuthorizeTransactionKey(EncryptionDecryptionUtil.encryption
					 * (paymentGateWay.getAuthorizeTransactionKey())); }
					 * if(paymentGateWay.getiGatePassword()!=null &&
					 * paymentGateWay.getiGateUserName()!=null ){
					 * paymentGateWay.setiGatePassword(EncryptionDecryptionUtil.encryption(
					 * paymentGateWay.getiGatePassword()));
					 * paymentGateWay.setiGateUserName(EncryptionDecryptionUtil.encryption(
					 * paymentGateWay.getiGateUserName())); }
					 */

					if (paymentGateWay.getStripeAPIKey() != null && !paymentGateWay.getStripeAPIKey().isEmpty()) {
						String response = validateStripeKey(paymentGateWay.getStripeAPIKey());

						if (response == "Success" || response.equals("Success")) {
							paymentGateWay.setStripeAPIKey(
									EncryptionDecryptionUtil.encryption(paymentGateWay.getStripeAPIKey()));
						} else {
							LOGGER.info(
									"===============  ConfigurationController : Inside savePaymentGatewayDetailsViaInstallMerchant returns response== "
											+ response);

							return response;
						}
					}
					/*
					 * if(paymentGateWay.getCayanMerchantKey()!=null &&
					 * paymentGateWay.getCayanMerchantName()!=null &&
					 * paymentGateWay.getCayanMerchantSiteId()!=null) {
					 * paymentGateWay.setCayanMerchantKey(EncryptionDecryptionUtil.encryption(
					 * paymentGateWay.getCayanMerchantKey()));
					 * paymentGateWay.setCayanMerchantName(EncryptionDecryptionUtil.encryption(
					 * paymentGateWay.getCayanMerchantName()));
					 * paymentGateWay.setCayanMerchantSiteId(EncryptionDecryptionUtil.encryption(
					 * paymentGateWay.getCayanMerchantSiteId())); }
					 */
					if (paymentGateWay.getAuthorizeLoginId() != null && !paymentGateWay.getAuthorizeLoginId().isEmpty()
							&& paymentGateWay.getAuthorizeTransactionKey() != null
							&& !paymentGateWay.getAuthorizeTransactionKey().isEmpty()) {

						String response = validateAuthrizeDetails(paymentGateWay.getAuthorizeLoginId(),
								paymentGateWay.getAuthorizeTransactionKey(), "1503118872");

						if (response == "Success" || response.equals("Success")) {
							paymentGateWay.setAuthorizeLoginId(
									EncryptionDecryptionUtil.encryption(paymentGateWay.getAuthorizeLoginId()));
							paymentGateWay.setAuthorizeTransactionKey(
									EncryptionDecryptionUtil.encryption(paymentGateWay.getAuthorizeTransactionKey()));
						} else {
							LOGGER.info(
									"===============  ConfigurationController : Inside savePaymentGatewayDetailsViaInstallMerchant returns response== "
											+ response);

							return response;
						}

					}
					if (paymentGateWay.getiGatePassword() != null && !paymentGateWay.getiGatePassword().isEmpty()
							&& paymentGateWay.getiGateUserName() != null
							&& !paymentGateWay.getiGateUserName().isEmpty()) {
						String response = validateBridgePayDetails(paymentGateWay.getiGateUserName(),
								paymentGateWay.getiGatePassword());
						if (response == "Approved" || response.equals("Approved")) {
							paymentGateWay.setiGateUserName(
									EncryptionDecryptionUtil.encryption(paymentGateWay.getiGateUserName()));
							paymentGateWay.setiGatePassword(
									EncryptionDecryptionUtil.encryption(paymentGateWay.getiGatePassword()));

						} else {
							LOGGER.info(
									"===============  ConfigurationController : Inside savePaymentGatewayDetailsViaInstallMerchant returns response== "
											+ response);

							return response;
						}
					}

					merchantService.savePaymentGateWay(paymentGateWay);

					PaymentMode mode = merchantService.findByMerchantIdAndLabel(merchant.getId(), "Credit Card");
					if (mode != null && mode.getAllowPaymentMode() == 0) {
						mode.setMerchant(merchant);
						mode.setAllowPaymentMode(1);
						merchantService.savePaymentMode(mode);
					}
				}

			} else {
				LOGGER.info(
						"===============  ConfigurationController : Inside savePaymentGatewayDetailsViaInstallMerchant :: End  ============= ");

				return "redirect:https://www.foodkonnekt.com";
			}
		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
				LOGGER.error(
						"===============  ConfigurationController : Inside savePaymentGatewayDetailsViaInstallMerchant :: Exception  ============= "
								+ e);

				LOGGER.error("error: " + e.getMessage());
				return "redirect:https://www.foodkonnekt.com";
			}
		}
		String status = request.getParameter("admin");
		if ("admin".equals(status)) {
			LOGGER.info(
					"===============  ConfigurationController : Inside savePaymentGatewayDetailsViaInstallMerchant :: End  ============= ");

			return "redirect:" + environment.getProperty("BASE_URL") + "/setupPaymentGateway";
		} else {
			model.addAttribute("saveStatus", "yes");
			LOGGER.info(
					"===============  ConfigurationController : Inside savePaymentGatewayDetailsViaInstallMerchant :: End  ============= ");

			// return "redirect:"+environment.getProperty("BASE_URL")+"/setConvenienceFeoze";
			return "setPaymentGateway";
		}
	}

	private String validateAuthrizeDetails(String apiLoginId, String transactionKey, String customerProfileId) {
		LOGGER.info(
				"===============  ConfigurationController : Inside validateAuthrizeDetails :: Start  ============= ");

		LOGGER.info("apiLoginId " + apiLoginId);
		LOGGER.info("transactionKey " + transactionKey);
		ApiOperationBase.setEnvironment(ProducerUtil.getAuthorizeEnv(environment.getProperty("AuthorizeEnv")));
		MerchantAuthenticationType merchantAuthenticationType = new MerchantAuthenticationType();
		merchantAuthenticationType.setName(apiLoginId);
		merchantAuthenticationType.setTransactionKey(transactionKey);
		ApiOperationBase.setMerchantAuthentication(merchantAuthenticationType);

		GetCustomerProfileRequest apiRequest = new GetCustomerProfileRequest();
		apiRequest.setCustomerProfileId(customerProfileId);

		GetCustomerProfileController controller = new GetCustomerProfileController(apiRequest);
		controller.execute();

		GetCustomerProfileResponse response = new GetCustomerProfileResponse();
		response = controller.getApiResponse();

		if (response != null) {
			for (Message message : response.getMessages().getMessage()) {
				if (message.getCode() == "E00007" || message.getCode().equals("E00007")) {
					return message.getText();
				}
			}
			LOGGER.info(
					"===============  ConfigurationController : Inside validateAuthrizeDetails :: End  ============= ");

			return "Success";
		} else {
			LOGGER.info(
					"===============  ConfigurationController : Inside validateAuthrizeDetails :: End  ============= ");

			return "User authentication failed due to invalid authentication values.";
		}
	}

	private String validateBridgePayDetails(String username, String password) {
		LOGGER.info(
				"===============  ConfigurationController : Inside validateBridgePayDetails :: Start  ============= ");

		SmartPaymentsSoapProxy soapProxy = new SmartPaymentsSoapProxy(environment.getProperty("ENDPOINT_PROD"));
		try {
			Response response = soapProxy.getInfo(username, password, "Setup", "T");
			return response.getRespMSG();
		} catch (RemoteException e) {
			LOGGER.error(
					"===============  ConfigurationController : Inside validateBridgePayDetails :: RemoteException  ============= "
							+ e);

			LOGGER.error("error: " + e.getMessage());
			return "Failed";
		}

	}

	private boolean validateFirstDataDetails(String merchantId, String userName, String password) {
		String profileid = "1";
		LOGGER.info("inside validateFirstDataDetails userName " + userName);
		LOGGER.info("inside validateFirstDataDetails password " + password);
		boolean reponse = CardConnectRestClientExample.validateLoginCredentails(profileid, merchantId, userName,
				password);
		return reponse;

	}

	private String validateStripeKey(String stripeAPIKey) {
		LOGGER.info("inside validateStripeKey stripeAPIKey " + stripeAPIKey);
		Charge ch = null;
		Stripe.apiKey = stripeAPIKey;
		try {
			ch = Charge.retrieve("ch_1CDod2EOUDb088Q9jk6UQBzG");
			return "Success";

		} catch (Exception e) {
			LOGGER.error("error: " + e.getMessage());
			if (e.getMessage() != null && e.getMessage().contains("Invalid API Key provided")) {
				return e.getMessage();

			} else {
				return "Success";

			}
		}

	}

	public String googleMap(String locationAddress, ModelMap model) {
		try {
			LOGGER.info("===============  ConfigurationController : Inside googleMap :: Start  ============= ");

			// String addres = "Sayaji Hotel, Near balewadi stadium, pune";
			URL url = new URL(
					"https://maps.googleapis.com/maps/api/geocode/json?address=" + URIUtil.encodeQuery(locationAddress)
							+ "&sensor=true&key=AIzaSyDjnluKhome9lmt5LLKnIgqot7HtyDetes");
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");

			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
			}
			BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

			String output = "", full = "";
			// LOGGER.info("output-###-");
			while ((output = br.readLine()) != null) {

				// LOGGER.info(output);
				full += output;
			}
			if (full != "") {
				JSONObject jsonObject = new JSONObject(full);
				if (jsonObject != null && jsonObject.toString().contains("results")) {
					JSONArray result = (JSONArray) jsonObject.getJSONArray("results");
					LOGGER.info("result.length()---" + result.length());
					if (result.length() > 0 && result.toString().contains("geometry")) {
						for (int i = 0; i < result.length(); i++) {

							JSONObject viewPort = (JSONObject) result.getJSONObject(i).get("geometry");
							if (viewPort != null && viewPort.toString().contains("location")) {
								JSONObject location = (JSONObject) viewPort.get("location");
								if (location != null && location.toString().contains("lng")
										&& location.toString().contains("lat")) {
									LOGGER.info(location.get("lng") + "---###----" + location.get("lat"));
									model.addAttribute("latitude", location.get("lat"));
									model.addAttribute("longitude", location.get("lng"));

									/*
									 * location.getString("lng"); location.getString("lat");
									 */
								}
							}

						}
					}

				}
			}

			// geometry
			// LOGGER.info(result);
		} catch (Exception e) {
			LOGGER.error(
					"===============  ConfigurationController : Inside googleMap :: Exception  ============= " + e);

			LOGGER.error("error: " + e.getMessage());
		}
		LOGGER.info("===============  ConfigurationController : Inside googleMap :: End  ============= ");

		return "success";
	}

	public static String doAuthorizePayeezyPayment(PayeezyClientHelper client) {
		String response = null;
		try {
			LOGGER.info(
					"===============  ConfigurationController : Inside doAuthorizePayeezyPayment :: Start  ============= ");

			String code = "";
			TransactionRequest request = new TransactionRequest();
			PayeezyResponse payeezyResponse = client.doPrimaryTransaction(request);
			LOGGER.info("payeezyResponse " + payeezyResponse.getResponseBody());
			JSONObject jObject = new JSONObject(payeezyResponse.getResponseBody());
			if (jObject.has("Error")) {
				JSONObject ja_data = jObject.getJSONObject("Error");
				JSONArray arr = ja_data.getJSONArray("messages");
				for (int i = 0; i < arr.length(); i++) {
					LOGGER.info(arr.getJSONObject(i).getString("code"));
					code = arr.getJSONObject(i).getString("code");
				}
			}
			if (code != "" && code.equalsIgnoreCase("401")) {
				response = code;
			}

			if (code != "" && code.equalsIgnoreCase("403")) {
				response = code;
			} else {

				if (code != "" && code.equalsIgnoreCase("missing_transaction_type"))
					response = "approved";
			}
		} catch (Exception e) {
			LOGGER.error(
					"===============  ConfigurationController : Inside doAuthorizePayeezyPayment :: Exception  ============= "
							+ e);

			LOGGER.error("error: " + e.getMessage());
		}
		// TransactionRequest request = getPrimaryTransaction();
		/*
		 * PayeezyResponse payeezyResponse = client.doPrimaryTransaction(request);
		 * JSONObject jObject = new JSONObject(payeezyResponse.getResponseBody());
		 * if(jObject.has("transaction_status")){ response =
		 * jObject.getString("transaction_status"); }
		 */
		LOGGER.info(
				"===============  ConfigurationController : Inside doAuthorizePayeezyPayment :: End  ============= ");

		return response;
	}

	@RequestMapping(value = "/verifyPaymentGatewayDetails", method = RequestMethod.POST)
	public String verifyPaymentGatewayDetails(@ModelAttribute("paymentGateWay") PaymentGateWay paymentGateWay,
			ModelMap model, HttpServletRequest request) {
		String response = "failed";
		try {
			LOGGER.info(
					"===============  ConfigurationController : Inside verifyPaymentGatewayDetails :: Start  ============= ");

			HttpSession session = request.getSession(false);
			session.setAttribute("paymentGateWay", paymentGateWay);
			Merchant merchant = null;
			model.addAttribute("paymentGateWay", paymentGateWay);
			if (session != null)
				merchant = (Merchant) session.getAttribute("merchant");
			if (merchant != null) {
				if (paymentGateWay != null) {
					if (paymentGateWay.getStripeAPIKey() != null && !paymentGateWay.getStripeAPIKey().isEmpty()) {
						response = validateStripeKey(paymentGateWay.getStripeAPIKey());
						if (response == "Success" || response.equals("Success")) {
							model.addAttribute("response", "Success");
						} else {
							model.addAttribute("response", "\"failed\"");
							LOGGER.error("Error in validateStripeKey:" + response);
							LOGGER.info(
									"===============  ConfigurationController : Inside verifyPaymentGatewayDetails :: End  ============= ");

							return "redirect:" + environment.getProperty("BASE_URL") + "/setupPaymentGateway";
						}
					}

					if (paymentGateWay.getAuthorizeLoginId() != null && !paymentGateWay.getAuthorizeLoginId().isEmpty()
							&& paymentGateWay.getAuthorizeTransactionKey() != null
							&& !paymentGateWay.getAuthorizeTransactionKey().isEmpty()) {
						response = validateAuthrizeDetails(paymentGateWay.getAuthorizeLoginId(),
								paymentGateWay.getAuthorizeTransactionKey(), "1503118872");
						if (response == "Success" || response.equals("Success")) {
							model.addAttribute("response", "Success");
						} else {
							model.addAttribute("response", "failed");
							LOGGER.error("Error in validateAuthrizeDetails:" + response);
							LOGGER.info(
									"===============  ConfigurationController : Inside verifyPaymentGatewayDetails :: End  ============= ");
							return "redirect:" + environment.getProperty("BASE_URL") + "/setupPaymentGateway";
						}

					}
					if (paymentGateWay.getiGatePassword() != null && !paymentGateWay.getiGatePassword().isEmpty()
							&& paymentGateWay.getiGateUserName() != null
							&& !paymentGateWay.getiGateUserName().isEmpty()) {
						response = validateBridgePayDetails(paymentGateWay.getiGateUserName(),
								paymentGateWay.getiGatePassword());
						if (response == "Approved" || response.equals("Approved")) {
							model.addAttribute("response", "Success");
						} else {
							model.addAttribute("response", "failed");
							LOGGER.error("Error in validateBridgePayDetails:" + response);
							LOGGER.info(
									"===============  ConfigurationController : Inside verifyPaymentGatewayDetails :: End  ============= ");
							return "redirect:" + environment.getProperty("BASE_URL") + "/setupPaymentGateway";
						}
					}

					if (paymentGateWay.getFirstDataPassword() != null
							&& !paymentGateWay.getFirstDataPassword().isEmpty()
							&& paymentGateWay.getFirstDataUserName() != null
							&& !paymentGateWay.getFirstDataUserName().isEmpty()
							&& paymentGateWay.getFirstDataMerchantId() != null
							&& !paymentGateWay.getFirstDataMerchantId().isEmpty()) {
						boolean apiResponse = validateFirstDataDetails(paymentGateWay.getFirstDataMerchantId(),
								paymentGateWay.getFirstDataUserName(), paymentGateWay.getFirstDataPassword());
						if (apiResponse) {
							model.addAttribute("response", "Success");
						} else {
							model.addAttribute("response", "failed");
							LOGGER.error("Error in validateFirstDataDetails:" + response);
							LOGGER.info(
									"===============  ConfigurationController : Inside verifyPaymentGatewayDetails :: End  ============= ");
							return "redirect:" + environment.getProperty("BASE_URL") + "/setupPaymentGateway";
						}
					}

					if (paymentGateWay.getPayeezyMerchnatToken() != null
							&& !paymentGateWay.getPayeezyMerchnatToken().isEmpty()) {
						PayeezyClientHelper client = PayeezyUtilProperties.getPayeezyProperties(paymentGateWay,environment);
						String responseDoAuthorizePayeezyPayment = doAuthorizePayeezyPayment(client);
						if (responseDoAuthorizePayeezyPayment != null
								&& responseDoAuthorizePayeezyPayment.equals("approved")) {
							model.addAttribute("response", "Success");
						} else {
							model.addAttribute("response", "failed");
							LOGGER.error("Error in doAuthorizePayeezyPayment:" + response);
							LOGGER.info(
									"===============  ConfigurationController : Inside verifyPaymentGatewayDetails :: End  ============= ");
							return "redirect:" + environment.getProperty("BASE_URL") + "/setupPaymentGateway";
						}
					}
				}
			} else {
				LOGGER.info("condition failed------------------------------------------------");
				LOGGER.info(
						"===============  ConfigurationController : Inside verifyPaymentGatewayDetails :: End  ============= ");
				return "redirect:https://www.foodkonnekt.com";
			}
		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
				LOGGER.error("error: " + e.getMessage());
				LOGGER.error(
						"===============  ConfigurationController : Inside verifyPaymentGatewayDetails :: Exception  ============= "
								+ e);

				return "redirect:https://www.foodkonnekt.com";
			}
		}
		String status = request.getParameter("admin");
		if ("admin".equals(status)) {
			LOGGER.info(
					"===============  ConfigurationController : Inside verifyPaymentGatewayDetails :: End  ============= ");
			return "redirect:" + environment.getProperty("BASE_URL") + "/setupPaymentGateway";
		} else {
			LOGGER.info(
					"===============  ConfigurationController : Inside verifyPaymentGatewayDetails :: End  ============= ");
			return "redirect:" + environment.getProperty("BASE_URL") + "/setupPaymentGateway";
		}
	}

	@RequestMapping(value = "/testPaymentGatewayDetails", method = RequestMethod.POST)
	public String testPaymentGatewayDetails(@ModelAttribute("paymentGateWay") PaymentGateWay paymentGateWay,
			ModelMap model, HttpServletRequest request) {
		LOGGER.info(
				"===============  ConfigurationController : Inside testPaymentGatewayDetails :: Start  ============= ");

		String response = null;
		PaymentGateWay paymentGateWayObj = null;
		try {
			HttpSession session = request.getSession(false);
			Merchant merchant = null;
			if (session != null) {
				merchant = (Merchant) session.getAttribute("merchant");
				if (merchant.getName() != null) {
					paymentGateWay.setMerchanttName(merchant.getName());
				}
				paymentGateWayObj = (PaymentGateWay) session.getAttribute("paymentGateWay");
			}
			if (merchant != null) {
				Date today = (merchant.getTimeZone() != null
						&& merchant.getTimeZone().getTimeZoneCode() != null)
						? DateUtil.getCurrentDateForTimeZonee(merchant.getTimeZone().getTimeZoneCode())
						: new Date();
				Boolean isDeleted = false;
				PaymentGateWay details = merchantService.findbyMerchantId(merchant.getId(), isDeleted);
				if (details != null) {
					details.setDeleted(true);
					details.setActive(false);
					details.setUpdatedDate(today);

					paymentGateWay.setActive(true);
					paymentGateWay.setMerchantId(merchant.getId());
					paymentGateWay.setCreatedDate(today);
					paymentGateWay.setUpdatedDate(today);
					paymentGateWay.setGateWayType(paymentGateWayObj.getGateWayType());

					if (paymentGateWayObj.getStripeAPIKey() != null && !paymentGateWayObj.getStripeAPIKey().isEmpty()) {
						response = validateStripeKey(paymentGateWayObj.getStripeAPIKey());

						if (response == "Success" || response.equals("Success")) {
							paymentGateWay.setStripeAPIKey(
									EncryptionDecryptionUtil.encryption(paymentGateWayObj.getStripeAPIKey()));

						} else {
							model.addAttribute("response", response);
							LOGGER.info(
									"===============  ConfigurationController : Inside testPaymentGatewayDetails :: End  ============= ");

							return "redirect:" + environment.getProperty("BASE_URL") + "/setupPaymentGateway";
						}
					}
					if (paymentGateWayObj.getAuthorizeLoginId() != null
							&& !paymentGateWayObj.getAuthorizeLoginId().isEmpty()
							&& paymentGateWayObj.getAuthorizeTransactionKey() != null
							&& !paymentGateWayObj.getAuthorizeTransactionKey().isEmpty()) {

						response = validateAuthrizeDetails(paymentGateWayObj.getAuthorizeLoginId(),
								paymentGateWayObj.getAuthorizeTransactionKey(), "1503118872");

						if (response == "Success" || response.equals("Success")) {
							paymentGateWay.setAuthorizeLoginId(
									EncryptionDecryptionUtil.encryption(paymentGateWayObj.getAuthorizeLoginId()));
							paymentGateWay.setAuthorizeTransactionKey(EncryptionDecryptionUtil
									.encryption(paymentGateWayObj.getAuthorizeTransactionKey()));

						} else {
							model.addAttribute("response", response);
							LOGGER.info(
									"===============  ConfigurationController : Inside testPaymentGatewayDetails :: End  ============= ");
							return "redirect:" + environment.getProperty("BASE_URL") + "/setupPaymentGateway";
						}

					}
					if (paymentGateWayObj.getiGatePassword() != null && !paymentGateWayObj.getiGatePassword().isEmpty()
							&& paymentGateWayObj.getiGateUserName() != null
							&& !paymentGateWayObj.getiGateUserName().isEmpty()) {
						response = validateBridgePayDetails(paymentGateWayObj.getiGateUserName(),
								paymentGateWayObj.getiGatePassword());
						if (response == "Approved" || response.equals("Approved")) {
							paymentGateWay.setiGateUserName(
									EncryptionDecryptionUtil.encryption(paymentGateWayObj.getiGateUserName()));
							paymentGateWay.setiGatePassword(
									EncryptionDecryptionUtil.encryption(paymentGateWayObj.getiGatePassword()));

						} else {
							model.addAttribute("response", response);
							LOGGER.info(
									"===============  ConfigurationController : Inside testPaymentGatewayDetails :: End  ============= ");
							return "redirect:" + environment.getProperty("BASE_URL") + "/setupPaymentGateway";
						}
					}

					if (paymentGateWayObj.getFirstDataPassword() != null
							&& !paymentGateWayObj.getFirstDataPassword().isEmpty()
							&& paymentGateWayObj.getFirstDataUserName() != null
							&& !paymentGateWayObj.getFirstDataUserName().isEmpty()
							&& paymentGateWayObj.getFirstDataMerchantId() != null
							&& !paymentGateWayObj.getFirstDataMerchantId().isEmpty()) {

						boolean apiResponse = validateFirstDataDetails(paymentGateWayObj.getFirstDataMerchantId(),
								paymentGateWayObj.getFirstDataUserName(), paymentGateWayObj.getFirstDataPassword());
						if (apiResponse) {
							paymentGateWay.setFirstDataUserName(
									EncryptionDecryptionUtil.encryption(paymentGateWayObj.getFirstDataUserName()));
							paymentGateWay.setFirstDataPassword(
									EncryptionDecryptionUtil.encryption(paymentGateWayObj.getFirstDataPassword()));
							paymentGateWay.setFirstDataMerchantId(
									EncryptionDecryptionUtil.encryption(paymentGateWayObj.getFirstDataMerchantId()));
						} else {
							model.addAttribute("response", "Unauthorized");
							LOGGER.info(
									"===============  ConfigurationController : Inside testPaymentGatewayDetails :: End  ============= ");
							return "redirect:" + environment.getProperty("BASE_URL") + "/setupPaymentGateway";
						}
					}

					if (paymentGateWayObj.getPayeezyMerchnatToken() != null
							&& !paymentGateWayObj.getPayeezyMerchnatToken().isEmpty()) {
						PayeezyClientHelper client = PayeezyUtilProperties.getPayeezyProperties(paymentGateWay,environment);
						String responseDoAuthorizePayeezyPayment = doAuthorizePayeezyPayment(client);
						if (responseDoAuthorizePayeezyPayment.equals("approved")) {
							paymentGateWay.setPayeezyMerchnatToken(
									EncryptionDecryptionUtil.encryption(paymentGateWayObj.getPayeezyMerchnatToken()));
						} else {
							model.addAttribute("response", "Unauthorized");
							LOGGER.info(
									"===============  ConfigurationController : Inside testPaymentGatewayDetails :: End  ============= ");
							return "redirect:" + environment.getProperty("BASE_URL") + "/setupPaymentGateway";
						}

					}
					merchantService.savePaymentGateWay(details);
					merchantService.savePaymentGateWay(paymentGateWay);

					PaymentMode mode = merchantService.findByMerchantIdAndLabel(merchant.getId(), "Credit Card");
					if (mode != null && mode.getAllowPaymentMode() == 0) {
						mode.setMerchant(merchant);
						mode.setAllowPaymentMode(1);
						merchantService.savePaymentMode(mode);
					}
				} else {
					paymentGateWay.setActive(true);
					paymentGateWay.setMerchantId(merchant.getId());
					paymentGateWay.setCreatedDate(today);

					paymentGateWay.setUpdatedDate(today);

					if (paymentGateWay != null) {
						paymentGateWay.setGateWayType(paymentGateWayObj.getGateWayType());
						if (paymentGateWayObj.getStripeAPIKey() != null
								&& !paymentGateWayObj.getStripeAPIKey().isEmpty()) {
							paymentGateWay.setStripeAPIKey(paymentGateWayObj.getStripeAPIKey());
							response = PayMentGatwayUtil.testAllPaymentGateway(paymentGateWay, IConstant.STRIPE,environment);
							if (response == "Success" || response.equalsIgnoreCase("Success")) {
								paymentGateWay.setStripeAPIKey(
										EncryptionDecryptionUtil.encryption(paymentGateWayObj.getStripeAPIKey()));
							} else {
								model.addAttribute("response", response);
								LOGGER.info(
										"===============  ConfigurationController : Inside testPaymentGatewayDetails :: End  ============= ");
								return "redirect:" + environment.getProperty("BASE_URL") + "/setupPaymentGateway";
							}
						}
						if (paymentGateWayObj.getAuthorizeLoginId() != null
								&& !paymentGateWayObj.getAuthorizeLoginId().isEmpty()
								&& paymentGateWayObj.getAuthorizeTransactionKey() != null
								&& !paymentGateWayObj.getAuthorizeTransactionKey().isEmpty()) {
							paymentGateWay.setAuthorizeLoginId(paymentGateWayObj.getAuthorizeLoginId());
							paymentGateWay.setAuthorizeTransactionKey(paymentGateWayObj.getAuthorizeTransactionKey());
							// response=
							// validateAuthrizeDetails(paymentGateWayObj.getAuthorizeLoginId(),paymentGateWayObj.getAuthorizeTransactionKey(),"1503118872");
							response = PayMentGatwayUtil.testAllPaymentGateway(paymentGateWay, IConstant.AUTHORIZE_NET,environment);
							if (response == "Success" || response.equalsIgnoreCase("Success")) {
								paymentGateWay.setAuthorizeLoginId(
										EncryptionDecryptionUtil.encryption(paymentGateWayObj.getAuthorizeLoginId()));
								paymentGateWay.setAuthorizeTransactionKey(EncryptionDecryptionUtil
										.encryption(paymentGateWayObj.getAuthorizeTransactionKey()));
							} else {
								model.addAttribute("response", response);
								LOGGER.info(
										"===============  ConfigurationController : Inside testPaymentGatewayDetails :: End  ============= ");
								return "redirect:" + environment.getProperty("BASE_URL") + "/setupPaymentGateway";
							}
						}
						if (paymentGateWayObj.getiGatePassword() != null
								&& !paymentGateWayObj.getiGatePassword().isEmpty()
								&& paymentGateWayObj.getiGateUserName() != null
								&& !paymentGateWayObj.getiGateUserName().isEmpty()) {
							// response=validateBridgePayDetails(paymentGateWayObj.getiGateUserName(),paymentGateWayObj.getiGatePassword());
							paymentGateWay.setiGatePassword(paymentGateWayObj.getiGatePassword());
							paymentGateWay.setiGateUserName(paymentGateWayObj.getiGateUserName());

							response = PayMentGatwayUtil.testAllPaymentGateway(paymentGateWay, IConstant.T_GATE,environment);
							if (response == "Success" || response.equals("Success") || response == "Approved"
									|| response.equals("Approved")) {
								paymentGateWay.setiGateUserName(
										EncryptionDecryptionUtil.encryption(paymentGateWayObj.getiGateUserName()));
								paymentGateWay.setiGatePassword(
										EncryptionDecryptionUtil.encryption(paymentGateWayObj.getiGatePassword()));
							} else {
								model.addAttribute("response", response);
								LOGGER.info(
										"===============  ConfigurationController : Inside testPaymentGatewayDetails :: End  ============= ");
								return "redirect:" + environment.getProperty("BASE_URL") + "/setupPaymentGateway";
							}
						}

						if (paymentGateWayObj.getFirstDataPassword() != null
								&& !paymentGateWayObj.getFirstDataPassword().isEmpty()
								&& paymentGateWayObj.getFirstDataUserName() != null
								&& !paymentGateWayObj.getFirstDataUserName().isEmpty()
								&& paymentGateWayObj.getFirstDataMerchantId() != null
								&& !paymentGateWayObj.getFirstDataMerchantId().isEmpty()) {

							paymentGateWay.setFirstDataUserName(paymentGateWayObj.getFirstDataUserName());
							paymentGateWay.setFirstDataPassword(paymentGateWayObj.getFirstDataPassword());
							paymentGateWay.setFirstDataMerchantId(paymentGateWayObj.getFirstDataMerchantId());
							response = PayMentGatwayUtil.testAllPaymentGateway(paymentGateWay, IConstant.FIRST_DATA,environment);

							if (response.equalsIgnoreCase("Success")) {
								paymentGateWay.setFirstDataUserName(
										EncryptionDecryptionUtil.encryption(paymentGateWayObj.getFirstDataUserName()));
								paymentGateWay.setFirstDataPassword(
										EncryptionDecryptionUtil.encryption(paymentGateWayObj.getFirstDataPassword()));
								paymentGateWay.setFirstDataMerchantId(EncryptionDecryptionUtil
										.encryption(paymentGateWayObj.getFirstDataMerchantId()));
							} else {
								model.addAttribute("response", response);
								LOGGER.info(
										"===============  ConfigurationController : Inside testPaymentGatewayDetails :: End  ============= ");
								return "redirect:" + environment.getProperty("BASE_URL") + "/setupPaymentGateway";
							}
						}

						if (paymentGateWayObj.getPayeezyMerchnatToken() != null
								&& !paymentGateWayObj.getPayeezyMerchnatToken().isEmpty()) {
							paymentGateWay.setPayeezyMerchnatToken(paymentGateWayObj.getPayeezyMerchnatToken());
							response = PayMentGatwayUtil.testAllPaymentGateway(paymentGateWay, IConstant.PAYEEZY,environment);
							if (response.equalsIgnoreCase("Success")) {
								paymentGateWay.setPayeezyMerchnatToken(EncryptionDecryptionUtil
										.encryption(paymentGateWayObj.getPayeezyMerchnatToken()));
							} else {
								model.addAttribute("response", response);
								LOGGER.info(
										"===============  ConfigurationController : Inside testPaymentGatewayDetails :: End  ============= ");
								return "redirect:" + environment.getProperty("BASE_URL") + "/setupPaymentGateway";
							}
						}

						merchantService.savePaymentGateWay(paymentGateWay);
						PaymentMode mode = merchantService.findByMerchantIdAndLabel(merchant.getId(), "Credit Card");
						if (mode != null && mode.getAllowPaymentMode() == 0) {
							mode.setMerchant(merchant);
							mode.setAllowPaymentMode(1);
							merchantService.savePaymentMode(mode);
						}

					}
				}
			} else {
				LOGGER.info("condtion failed------------------------------------------------");
				LOGGER.info(
						"===============  ConfigurationController : Inside testPaymentGatewayDetails :: End  ============= ");
				return "redirect:https://www.foodkonnekt.com";
			}

			session = null;
		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
				LOGGER.error("error: " + e.getMessage());
				LOGGER.info(
						"===============  ConfigurationController : Inside testPaymentGatewayDetails :: End  ============= ");
				return "redirect:https://www.foodkonnekt.com";
			}
		}
		String status = request.getParameter("admin");
		if ("admin".equals(status)) {
			LOGGER.info(
					"===============  ConfigurationController : Inside testPaymentGatewayDetails :: End  ============= ");
			return "redirect:" + environment.getProperty("BASE_URL") + "/setupPaymentGateway";
		} else {
			LOGGER.info(
					"===============  ConfigurationController : Inside testPaymentGatewayDetails :: End  ============= ");
			return "redirect:" + environment.getProperty("BASE_URL") + "/setupPaymentGateway";
			// return "setupPaymentGateway";
		}
	}

	@RequestMapping(value = "/3dSeure", method = RequestMethod.POST)
	public @ResponseBody String ThreeDSeureByToken(@RequestBody PaymentGateWay paymentGateWay) {
		String reviewResponse = "failed";
		String jwt = null;
		try {
			LOGGER.info(
					"===============  ConfigurationController : Inside ThreeDSeureByToken :: Start  ============= ");

			if (paymentGateWay != null) {
				String expiryDate = Integer.toString(paymentGateWay.getExpYear());
				expiryDate = expiryDate.substring(expiryDate.length() - 2);
				String expireMonth = Integer.toString(paymentGateWay.getExpMonth());
				if (expireMonth.length() < 2) {
					expireMonth = "0" + expireMonth;
				}
				LOGGER.info("expireMonth-->" + expireMonth);
				String expMonthDate = expireMonth.concat(expiryDate);
				paymentGateWay.setExpMonthYear(expMonthDate);
				PayeezyClientHelperf client = PayeezyUtilProperties
						.getPayeezyProperty(paymentGateWay,environment);
				TransactionRequest request1 = new TransactionRequest();
				request1.setReferenceNo("ORD-1234777");
				request1.setAmount("5");
				request1.setPaymentMethod("token");
				request1.setCurrency("USD");
				Token token = new Token();
				token.setTokenType("FDToken");
				Transarmor tokenData = new Transarmor();
				tokenData.setType(paymentGateWay.getCcType());
				tokenData.setValue(paymentGateWay.getToken());
				tokenData.setExpiryDt(paymentGateWay.getExpMonthYear());
				tokenData.setName(paymentGateWay.getCustomerName());
				token.setTokenData(tokenData);
				request1.setToken(token);
				PayeezyResponse payeezyResponse = client.do3DSecureTransaction(request1);
				LOGGER.info(
						"payeezyResponse.do3DSecureTransaction()----------------------------------------------------------");
				LOGGER.info(payeezyResponse.getResponseBody());
				JSONObject jObject = new JSONObject(payeezyResponse.getResponseBody());
				if (jObject.has("status")) {
					reviewResponse = jObject.getString("status");
					jwt = generateJWT.createJWTPayeezy(paymentGateWay.getPayeezy3DSApiKey(),
							paymentGateWay.getPayeezy3DSApiIdentifier(), paymentGateWay.getPayeezy3DSOrgUnitId());
					LOGGER.info("jwt " + jwt);
					reviewResponse = jwt;
				} else {
					if (jObject.has("status")) {
						reviewResponse = jObject.getString("status");
					}
				}
			}
		} catch (Exception e) {
			LOGGER.error(
					"===============  ConfigurationController : Inside ThreeDSeureByToken :: Exception  ============= "
							+ e);

			LOGGER.error("error: " + e.getMessage());
			reviewResponse = jwt;

		}
		LOGGER.info("===============  ConfigurationController : Inside ThreeDSeureByToken :: End  ============= ");

		return reviewResponse;
	}

	@RequestMapping(value = "/3dSeureFinalStage{Cavv}", method = RequestMethod.POST)
	public @ResponseBody String ThreeDFinalStage(@RequestBody PaymentGateWay paymentGateWay,
			@PathVariable("Cavv") String Cavv, HttpServletRequest request) {
		LOGGER.info("===============  ConfigurationController : Inside ThreeDFinalStage :: Start  ============= ");

		String reviewResponse = "failed";
		String transctiontag = null;
		String transaction_id = null;
		String response = "failed";
		boolean isDeleted = false;

		try {
			HttpSession session = request.getSession(false);
			Merchant merchant = null;
			if (session != null) {
				merchant = (Merchant) session.getAttribute("merchant");
				Date today = (merchant != null && merchant.getTimeZone() != null
						&& merchant.getTimeZone().getTimeZoneCode() != null)
						? DateUtil.getCurrentDateForTimeZonee(merchant.getTimeZone().getTimeZoneCode())
						: new Date();
				if (paymentGateWay != null && Cavv != null) {

					PaymentGateWay paymentGateWay1 = merchantService.findbyMerchantId(merchant.getId(), isDeleted);
					String expiryDate = Integer.toString(paymentGateWay.getExpYear());
					expiryDate = expiryDate.substring(expiryDate.length() - 2);
					String expireMonth = Integer.toString(paymentGateWay.getExpMonth());
					if (expireMonth.length() < 2) {
						expireMonth = "0" + expireMonth;
					}
					LOGGER.info("expireMonth-->" + expireMonth);
					String expMonthDate = expireMonth.concat(expiryDate);
					paymentGateWay.setExpMonthYear(expMonthDate);

					PayeezyClientHelperf client = PayeezyUtilProperties
							.getPayeezyProperty(paymentGateWay,environment);
					TransactionRequest transactionRequest = new TransactionRequest();
					transactionRequest.setReferenceNo("ORD-1234777");
					transactionRequest.setAmount("5");
					transactionRequest.setPaymentMethod("3DS");
					transactionRequest.setCurrency("USD");
					transactionRequest.setTransactionType("AUTHORIZE");
					transactionRequest.setAmount("5");
					{
						transactionRequest.setReferenceNo("Online order for -  " + merchant.getName());
					}

					Token token = new Token();
					token.setTokenType("FDToken");
					Transarmor tokenData = new Transarmor();
					tokenData.setType(paymentGateWay.getCcType());
					tokenData.setValue(paymentGateWay.getToken());
					tokenData.setExpiryDt(paymentGateWay.getExpMonthYear());
					tokenData.setName(paymentGateWay.getCustomerName());
					token.setTokenData(tokenData);
					transactionRequest.setToken(token);
					ThreeDomainSecureToken threeDomainSecureToken = new ThreeDomainSecureToken();
					threeDomainSecureToken.setCavv(Cavv);
					threeDomainSecureToken.setType("D");
					transactionRequest.setThreeDomainSecureToken(threeDomainSecureToken);
					PayeezyResponse payeezyResponse = client.do3DSecureTransactionFinal(transactionRequest);
					LOGGER.info(
							"payeezyResponse.do3DSecureTransactionFinal()----------------------------------------------------------");

					LOGGER.info(payeezyResponse.getResponseBody());
					JSONObject jObject = new JSONObject(payeezyResponse.getResponseBody());
					if (jObject.has("Error")) {
						JSONObject ja_data = jObject.getJSONObject("Error");
						JSONArray arr = ja_data.getJSONArray("messages");
						for (int i = 0; i < arr.length(); i++) {
							LOGGER.info(arr.getJSONObject(i).getString("code"));
							reviewResponse = arr.getJSONObject(i).getString("code");
						}
					}
					if (jObject.has("transaction_status")) {
						LOGGER.info("doPrimaryTransaction-----Authorized" + jObject);
						if (jObject.getString("transaction_status").equals("approved")) {
							transctiontag = jObject.getString("transaction_tag");
							transaction_id = jObject.getString("transaction_id");
							transactionRequest.setTransactionType("CAPTURE");
							transactionRequest.setTransactionTag(transctiontag);
							transactionRequest.setAmount("5");
							{
								transactionRequest.setReferenceNo("Online order for -  " + merchant.getName());
							}
							try {
								payeezyResponse = client.doSecondaryTransaction(transaction_id, transactionRequest);
								JSONObject payeezyCaputrejObject = new JSONObject(payeezyResponse.getResponseBody());
								if (payeezyCaputrejObject.has("transaction_status")) {
									LOGGER.info("doSecondaryTransaction-----CAPTURE" + payeezyCaputrejObject);
									response = payeezyCaputrejObject.getString("transaction_status");
									if (response != null && response.equals("approved")) {
										TransactionRequest refundRequest = Payeezytest.getSecondaryTransaction();
										transctiontag = payeezyCaputrejObject.getString("transaction_tag");
										transaction_id = payeezyCaputrejObject.getString("transaction_id");
										refundRequest.setTransactionType("REFUND");
										refundRequest.setTransactionTag(transctiontag);
										refundRequest.setAmount("5");
										transactionRequest.setAmount("5");
										{
											transactionRequest
													.setReferenceNo("Online order for -  " + merchant.getName());
										}
										try {
											payeezyResponse = client.doSecondaryTransaction(transaction_id,
													refundRequest);
											payeezyCaputrejObject = new JSONObject(payeezyResponse.getResponseBody());
											if (payeezyCaputrejObject.has("transaction_status")) {
												LOGGER.info("doSecondaryTransaction-----REFUND" + payeezyResponse);
												response = payeezyCaputrejObject.getString("transaction_status");
												if (response != null && response.equals("approved")) {
													LOGGER.info("doSecondaryTransaction-- approved---REFUND"
															+ payeezyCaputrejObject);
													transctiontag = payeezyCaputrejObject.getString("transaction_tag");
													transaction_id = payeezyCaputrejObject.getString("transaction_id");
													paymentGateWay.setActive(true);
													paymentGateWay.setGateWayType("6");
													paymentGateWay.setMerchantId(merchant.getId());
													paymentGateWay.setCreatedDate(today);
													paymentGateWay.setUpdatedDate(today);
													reviewResponse = "Success";
													paymentGateWay.setPayeezyMerchnatToken(EncryptionDecryptionUtil
															.encryption(paymentGateWay.getPayeezyMerchnatToken()));
													paymentGateWay.setPayeezyMerchnatTokenJs(EncryptionDecryptionUtil
															.encryption(paymentGateWay.getPayeezyMerchnatTokenJs()));

													paymentGateWay.setPayeezy3DSApiIdentifier(EncryptionDecryptionUtil
															.encryption(paymentGateWay.getPayeezy3DSApiIdentifier()));
													paymentGateWay.setPayeezy3DSApiKey(EncryptionDecryptionUtil
															.encryption(paymentGateWay.getPayeezy3DSApiKey()));
													paymentGateWay.setPayeezy3DSOrgUnitId(EncryptionDecryptionUtil
															.encryption(paymentGateWay.getPayeezy3DSOrgUnitId()));
													paymentGateWay.setPayeezy3DTaToken(EncryptionDecryptionUtil
															.encryption(paymentGateWay.getPayeezy3DTaToken()));
													if (paymentGateWay1 != null
															&& paymentGateWay1.getIsActive() == true) {
														paymentGateWay1.setDeleted(true);
														merchantService.savePaymentGateWay(paymentGateWay1);

													}
													merchantService.savePaymentGateWay(paymentGateWay);
													PaymentMode mode = merchantService
															.findByMerchantIdAndLabel(merchant.getId(), "Credit Card");
													if (mode != null && mode.getAllowPaymentMode() == 0) {
														mode.setMerchant(merchant);
														mode.setAllowPaymentMode(1);
														merchantService.savePaymentMode(mode);
													}
												}
											} else {
												LOGGER.info("error in refund");
											}
										} catch (Exception e) {
											LOGGER.error(
													"===============  ConfigurationController : Inside ThreeDFinalStage :: Exception  ============= "
															+ e);

											LOGGER.error("error: " + e.getMessage());
										}
									} else {
										LOGGER.info("error in capture");
										LOGGER.info("payeezyCaputrejObject-------" + payeezyResponse.getResponseBody());
									}
								}

							} catch (Exception e) {
								LOGGER.error("error: " + e.getMessage());
							}
						} else {
							reviewResponse = jObject.getString("transaction_status");
						}
					}
				}
			}
		} catch (Exception e) {
			LOGGER.error("error: " + e.getMessage());
			LOGGER.info("ThreeDFinalStage EXception " + e.getMessage());
			reviewResponse = e.getMessage();
		}
		LOGGER.info("===============  ConfigurationController : Inside ThreeDFinalStage :: End  ============= ");

		return reviewResponse;
	}

	@RequestMapping(value = "/genrateJwtTokenfor3Dseure", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> genrateJwtTokenfor3Dseure(@RequestBody PaymentGateWay paymentGateWay) {
		String reviewResponse = "failed";
		String jwt = null;
		String get3DToken3DResponse = null;
		String get3DToken3DStatus = null;
		HashMap<String, Object> map = null;
		try {
			LOGGER.info(
					"===============  ConfigurationController : Inside genrateJwtTokenfor3Dseure :: Start  ============= ");

			Boolean isDeleted = false;
			if (paymentGateWay != null) {
				map = new HashMap<String, Object>();
				String expiryDate = Integer.toString(paymentGateWay.getExpYear());
				expiryDate = expiryDate.substring(expiryDate.length() - 2);
				String expireMonth = Integer.toString(paymentGateWay.getExpMonth());
				if (expireMonth.length() < 2) {
					expireMonth = "0" + expireMonth;
				}
				LOGGER.info("expireMonth-->" + expireMonth);
				String expMonthDate = expireMonth.concat(expiryDate);
				paymentGateWay.setExpMonthYear(expMonthDate);
				PaymentGateWay details = merchantService.findbyMerchantId(paymentGateWay.getMerchantId(), isDeleted);
				String threeDTokenReponse = ProducerUtil.get3DToken(paymentGateWay, details,environment);
				LOGGER.info("threeDTokenReponse ------------------->" + threeDTokenReponse);

				threeDTokenReponse = threeDTokenReponse.trim();
				String callbackRes = threeDTokenReponse.replace("Payeezy.callback(", "").replace(")", "");

				LOGGER.info("callbackRes " + callbackRes);

				JSONObject jObject = new JSONObject(callbackRes);

				if (jObject.has("results")) {
					JSONObject resultsObj = jObject.getJSONObject("results");
					if (resultsObj.has("status")) {
						LOGGER.info(resultsObj.getString("status"));
						get3DToken3DStatus = resultsObj.getString("status");
					}
					if (resultsObj.has("token")) {
						JSONObject tokenObj = resultsObj.getJSONObject("token");
						if (tokenObj.has("value")) {
							LOGGER.info(tokenObj.getString("value"));
							get3DToken3DResponse = tokenObj.getString("value");

							LOGGER.info("get3DToken3DResponse " + get3DToken3DResponse);
						}
					}
					if (resultsObj.has("Error")) {
						JSONObject error = resultsObj.getJSONObject("Error");
						if (error.has("messages")) {
							JSONArray jsonArray = error.getJSONArray("messages");
							for (Object object : jsonArray) {
								JSONObject object2 = (JSONObject) object;
								if (object2.has("code")) {
									String code = object2.getString("code");
									LOGGER.info(code);
								}
								if (object2.has("description")) {
									String errorDescription = object2.getString("description");
									LOGGER.info(errorDescription);
									reviewResponse = errorDescription;
									map.put("stuatus", reviewResponse);
								}
							}
						}
					}
				}
				if (get3DToken3DStatus != null && get3DToken3DStatus.equalsIgnoreCase("success")) {
					LOGGER.info("I am here ------------------------------------>");

					PayeezyClientHelperf client = PayeezyUtilProperties
							.getPayeezyProperty(paymentGateWay,environment);

					TransactionRequest request1 = new TransactionRequest();

					request1.setReferenceNo("ORD-1234777");
					request1.setAmount("5");
					request1.setPaymentMethod("token");
					request1.setCurrency("USD");
					Token token = new Token();
					token.setTokenType("FDToken");

					Transarmor tokenData = new Transarmor();
					tokenData.setType(paymentGateWay.getCcType());
					tokenData.setValue(get3DToken3DResponse);
					tokenData.setExpiryDt(paymentGateWay.getExpMonthYear());
					tokenData.setName(paymentGateWay.getCustomerName());
					token.setTokenData(tokenData);
					request1.setToken(token);

					PayeezyResponse payeezyResponse = client.do3DSecureTransaction(request1);

					LOGGER.info(
							"payeezyResponse.do3DSecureTransaction()----------------------------------------------------------");
					LOGGER.info("payeezyResponse  " + payeezyResponse.getResponseBody());

					JSONObject payeezyResponsejObject = new JSONObject(payeezyResponse.getResponseBody());

					if (payeezyResponsejObject.has("status")) {
						reviewResponse = payeezyResponsejObject.getString("status");
						jwt = generateJWT.createJWTPayeezy(
								EncryptionDecryptionUtil.decryption(details.getPayeezy3DSApiKey()),
								EncryptionDecryptionUtil.decryption(details.getPayeezy3DSApiIdentifier()),
								EncryptionDecryptionUtil.decryption(details.getPayeezy3DSOrgUnitId()));
						LOGGER.info("Generated JWT is  : " + jwt);
						reviewResponse = jwt;
						map.put("token", get3DToken3DResponse);
						map.put("jwt", reviewResponse);
						map.put("stuatus", "Success");
					} else {
						reviewResponse = payeezyResponsejObject.getString("status");
						map.put("stuatus", reviewResponse);
					}
				}
			}
		} catch (Exception e) {
			LOGGER.error(
					"===============  ConfigurationController : Inside genrateJwtTokenfor3Dseure :: Exception  ============= "
							+ e);

			LOGGER.error("error: " + e.getMessage());
		}
		LOGGER.info(
				"===============  ConfigurationController : Inside genrateJwtTokenfor3Dseure :: End  ============= ");

		return map;
	}

	@RequestMapping(value = "/systemLogs", method = RequestMethod.GET)
	@ResponseBody
	public String systemLogs(@RequestParam(required = true) String appName, @RequestParam(required = true) String event,
			@RequestParam(required = true) String log, @RequestParam(required = true) String source,
			HttpServletRequest request) {
		try {
			LOGGER.info("===============  ConfigurationController : Inside systemLogs :: Start  ============= ");

			// source = "Server Name Here";
			File file = new File(
					environment.getProperty("FILEPATH") + appName + "/" + source + DateUtil.findCurrentDateWithYYYYMMDD() + ".txt");
			if (file.getParentFile().mkdir())
				file.createNewFile();
			FileWriter writer = new FileWriter(file, true);
			BufferedWriter bw = new BufferedWriter(writer);
			bw.write(event + "----->" + log);
			bw.newLine();
			bw.flush();

			MailSendUtil.sendMailSystemLog(appName, source, event, log,environment);
			LOGGER.info("===============  ConfigurationController : Inside systemLogs :: End  ============= ");

			return "success";
		} catch (Exception e) {
			if (e != null) {
				LOGGER.error("===============  ConfigurationController : Inside systemLogs :: Exception  ============= "
						+ e);

				MailSendUtil.sendExceptionByMail(e,environment);
				LOGGER.error("error: " + e.getMessage());
			}
			LOGGER.info("===============  ConfigurationController : Inside systemLogs :: End  ============= ");

			return "failed";
		}
	}
}
