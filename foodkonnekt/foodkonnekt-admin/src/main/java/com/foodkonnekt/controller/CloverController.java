package com.foodkonnekt.controller;

import com.foodkonnekt.clover.vo.CashVO;
import com.foodkonnekt.clover.vo.CloverOrderVO;
import com.foodkonnekt.clover.vo.PaymentVO;
import com.foodkonnekt.clover.vo.Tender;
import com.foodkonnekt.model.Clover;
import com.foodkonnekt.model.Merchant;
import com.foodkonnekt.model.PaymentMode;
import com.foodkonnekt.model.PizzaTemplate;
import com.foodkonnekt.model.Pos;
import com.foodkonnekt.model.PrintJob;
import com.foodkonnekt.model.Vendor;
import com.foodkonnekt.service.BusinessService;
import com.foodkonnekt.service.CloverService;
import com.foodkonnekt.service.MerchantService;
import com.foodkonnekt.service.ModifierService;
import com.foodkonnekt.service.OrderService;
import com.foodkonnekt.service.PizzaService;
import com.foodkonnekt.service.PrintJobService;
import com.foodkonnekt.service.WebhookAppService;
import com.foodkonnekt.util.CloverUrlUtil;
import com.foodkonnekt.util.DateUtil;
import com.foodkonnekt.util.EncryptionDecryptionUtil;
import com.foodkonnekt.util.IConstant;
import com.foodkonnekt.util.MailSendUtil;
import com.foodkonnekt.util.Main;
import com.foodkonnekt.util.NonPosUrlUtil;
import com.foodkonnekt.util.ProducerUtil;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

//import com.foodkonnekt.model.PrintJob;

@Controller
public class CloverController {

	@Autowired
    private Environment environment;
	
	@Autowired
	private CloverService cloverService;

	@Autowired
	private MerchantService merchantService;

	@Autowired
	private ModifierService modifierService;

	@Autowired
	private OrderService orderService;

	@Autowired
	private BusinessService businessService;

	@Autowired
	private WebhookAppService webhookAppService;
	
	@Autowired
	private PizzaService pizzaService;
	
	@Autowired
	private PrintJobService printJobService;
	

	/**
	 * Get merchant , payment mode, taxrate and orderType from clover
	 * 
	 * @param clover
	 * @param response
	 */

	private static final Logger LOGGER = LoggerFactory.getLogger(CloverController.class);
	
	@RequestMapping(value = "/getCloverData", method = RequestMethod.POST)
	public @ResponseBody void getCloverData(@RequestBody Clover clover,
			HttpServletResponse response) {
		LOGGER.info("===============  CloverController : Inside getCloverData :: Start  ============= ");
		clover.setInstantUrl(IConstant.CLOVER_INSTANCE_URL);
		clover.setUrl(environment.getProperty("CLOVER_URL"));
		Merchant merchant = cloverService.saveMerchant(clover);
		LOGGER.info("-----Save Merchant Done-----" + merchant.getId());

		String paymentModes = CloverUrlUtil.getPaymentModes(clover);
		cloverService.savePaymentMode(paymentModes, merchant);
		LOGGER.info("-----Save PaymentModes Done-----");

		/*
		 * String orderTypeDetail = CloverUrlUtil.getOderType(clover);
		 * cloverService.saveOrderType(orderTypeDetail, merchant, clover);
		 * System.out.println("-----Save OrderType Done-----");
		 */

		String taxRates = CloverUrlUtil.getTaxRate(clover);
		cloverService.saveTaxRate(taxRates, merchant);
		LOGGER.info("-----Save TaxRate Done-----");

		String openingHour = CloverUrlUtil.getOpeningHour(clover);
		cloverService.saveOpeningClosing(merchant, openingHour);
		LOGGER.info("-----Save OpeningAndClosingHour Done-----");

		cloverService.saveCategory(clover, merchant);
		LOGGER.info("-----Save Category Done-----");

		cloverService.saveItem(clover, merchant);
		LOGGER.info("-----Save Item Done-----");

		String modifierJson = CloverUrlUtil.getModifierAndModifierGroup(clover);
		modifierService.saveModifierAndModifierGroup(modifierJson, merchant);
		LOGGER.info("-----Save ModifiersAndModifierGroups Done-----");

		LOGGER.info("-------final Done------");
	}

	@RequestMapping(value = "/installFoodkonnekt", method = RequestMethod.GET)
	public String installFoodkonnekt(
			@RequestParam("location_uid") String localtion_uid,
			@RequestParam(value = "merchantId", required = false) String merchantId,
			@RequestParam(value = "posId", required = false) String posId, ModelMap model,
			@RequestParam(value = "scopeKey", required = false) String scopeKey,
			@RequestParam(value = "viewType", required = false) String viewType,
			HttpServletResponse response, HttpServletRequest request)
			throws Exception {
		if(viewType == null) viewType = "Detail";
		LOGGER.info("===============  CloverController : Inside installFoodkonnekt :: Start  =============localtion_uid "+localtion_uid
				+" merchantId "+merchantId+" posId "+posId+" scopeKey "+scopeKey);

		Merchant merchantCheck = null;
		model.addAttribute("adminUrl", environment.getProperty("BASE_URL"));
		if (merchantId != null && !merchantId.equals("")
				&& !merchantId.isEmpty()) {
			merchantId = EncryptionDecryptionUtil.decryption(merchantId);
			merchantCheck = merchantService.findByMerchantUidAndMerchantPosId(
					localtion_uid, merchantId);
		} else {
			merchantCheck = merchantService.findByMerchantUid(localtion_uid);
		}
		final HttpSession session = request.getSession();
		if (null != merchantCheck && merchantCheck.getIsInstall() != null
				&& merchantCheck.getIsInstall() == IConstant.BOOLEAN_TRUE) {

			if (merchantCheck.getEmployeePosId() == null
					|| merchantCheck.getEmployeePosId().isEmpty()
					|| merchantCheck.equals("")) {
				/*
				 * String employeePosId=
				 * CloverUrlUtil.createEmployeeOnClover(clover);
				 * if(employeePosId!=null){
				 * merchantCheck.setEmployeePosId(employeePosId); merchantCheck
				 * = merchantService.save(merchantCheck); }
				 */
			}
			LOGGER.info("---Inisde if bock---"
					+ merchantCheck.getIsInstall());
			String merchId = merchantCheck.getId().toString();
			String base64encode = EncryptionDecryptionUtil.encryption(merchId);
			String merchantName = merchantCheck.getName().replaceAll(
					"[^a-zA-Z0-9]", "");
			String orderLink=null;
			
			Integer merchantID = Integer.parseInt(merchId);
			
			/*if(merchantID.equals(IConstant.TEXAN_REWARDS_MERCHANT_ID)){
				orderLink= environment.getProperty("TEXAN_BASE_URL") + "/" + merchantName
						+ "/clover/" + base64encode;
			}else{*/
				orderLink= environment.getProperty("WEB_BASE_URL") + "/" + merchantName
						+ "/clover/" + base64encode;
			//}
			
			//String orderLink = environment.getProperty("WEB_BASE_URL") + "/" + merchantName+ "/clover/" + base64encode;

			  if (merchantCheck != null) {
		        	List<PizzaTemplate> pizzaTemplates = pizzaService.findByMerchantId(merchantCheck.getId());
		            if(!pizzaTemplates.isEmpty() && pizzaTemplates.size()>0){
		            	session.setAttribute("pizzaShowStatus", "success");
		            }else{
		            	session.setAttribute("pizzaShowStatus", "failed");
		            }
		        }
			
			if ("Detail".equals(viewType)) {
				// Orders
				  model.addAttribute("noOfDeliveryOrder", orderService.findNoOfDeliveryOrder(merchantCheck.getId()));
				  model.addAttribute("noOfPickUpOrder", orderService.findNoOfPickUpOrder(merchantCheck.getId()));
				  model.addAttribute("avgOrderValue", orderService.findAverageOrderValue(merchantCheck.getId()));
				  model.addAttribute("totalOrderValue", orderService.findtotalOrderValue(merchantCheck.getId()));
				
				  // Customers
				  model.addAttribute("orderFrequency", orderService.findOrderFrequency(merchantCheck.getId(), merchantCheck.getOwner().getId()));
				  model.addAttribute("customerOrderAverage", orderService.findCustomerOrderAverage(merchantCheck.getId(), merchantCheck.getOwner().getId()));
				  model.addAttribute("totalNoOfCustomer",     orderService.findTotalCustomer(merchantCheck.getId(), merchantCheck.getOwner().getId()));
				  // Item
				  model.addAttribute("trendingItem", orderService.findTrendingItem(merchantCheck.getId()));
				  model.addAttribute("averageNumberOfItemPerOrder", orderService .findAverageNumberOfItemPerOrder(merchantCheck.getId(), merchantCheck.getOwner().getId()));
			  }
			  
			
			session.setAttribute("merchant", merchantCheck);
			session.setAttribute("inventoryThread", 1);
			session.setAttribute("onlineOrderLink", orderLink);

			// response.sendRedirect("adminHome");
			
			LOGGER.info("===============  CloverController : Inside installFoodkonnekt :: End  ============= ");

			return "adminHome";

		} else {

			String merchantDetails = NonPosUrlUtil
					.getMerchantDetails(localtion_uid,environment);
			if (merchantDetails.contains("locationUid")) {
				Merchant merchant = merchantService.saveMerchantLogin(
						merchantDetails, localtion_uid, merchantId);
				if (merchant != null) {
					if (posId != null && !posId.isEmpty()) {
						Vendor vendor = merchant.getOwner();
						Pos pos = vendor.getPos();
						pos.setPosId(Integer.parseInt(posId));
						vendor.setPos(pos);
						merchant.setOwner(vendor);
						merchant.setActiveCustomerFeedback(0);
						merchant.setAllowDeliveryTiming(0);
						merchant.setFutureDaysAhead(0);
						merchant.setAllowReOrder(false);
						merchant.setAllowAuxTax(0);
						merchant.setAllowMultiPay(false);
						merchant.setAllowMultipleKoupon(0);

						merchant = merchantService.save(merchant);
						if(merchant != null && merchant.getId() != null && posId !=null){
							ProducerUtil.saveMerchantNotificationAppStatus(merchant.getId(), posId,environment);
						}
					}
					String merchId = merchant.getId().toString();
					String merchantName = merchant.getName().replaceAll(
							"[^a-zA-Z0-9]", "");
					String base64encode = EncryptionDecryptionUtil
							.encryption(merchId);
					String orderLink=null;
					Integer merchantID = Integer.parseInt(merchId);
    				
    				/*if(merchantID.equals(IConstant.TEXAN_REWARDS_MERCHANT_ID)){
						orderLink= environment.getProperty("TEXAN_BASE_URL") + "/" + merchantName
								+ "/clover/" + base64encode;
					}else{*/
						orderLink= environment.getProperty("WEB_BASE_URL") + "/" + merchantName
								+ "/clover/" + base64encode;
					//}
					
					//String orderLink =  environment.getProperty("WEB_BASE_URL") + "/" + merchantName + "/clover/" + base64encode;

					session.setAttribute("onlineOrderLink", orderLink);
					session.setAttribute("merchant", merchant);

					cloverService.saveOrderType(merchant);
					businessService.saveDefaultBusinessHours(merchant);
					cloverService.savePaymentMode(merchant);

					// response.sendRedirect("welcome");
					LOGGER.info("===============  CloverController : Inside installFoodkonnekt :: End  ============= ");

					return "uploadLogo";
				} else {
					LOGGER.info("===============  CloverController : Inside installFoodkonnekt :: End  ============= ");

					return "redirect:" + "https://www.foodkonnekt.com";
				}

			} else {
				LOGGER.info("===============  CloverController : Inside installFoodkonnekt :: End  ============= ");

				return "redirect:" + "https://www.foodkonnekt.com";
			}

		}

	}

	@RequestMapping(value = "/existingMerchant", method = RequestMethod.GET)
	public String existingMerchant(ModelMap model,
			@RequestParam("merchant_id") String merchantId,
			@RequestParam("access_token") String accessToken,
			HttpServletResponse response, HttpServletRequest request)
			throws Exception {
		LOGGER.info("===============  CloverController : Inside existingMerchant :: Start  ============= ");

		final Clover clover = new Clover();
		clover.setMerchantId(merchantId);
		clover.setAuthToken(accessToken);
		clover.setInstantUrl(IConstant.CLOVER_INSTANCE_URL);
		clover.setUrl(environment.getProperty("CLOVER_URL"));
		LOGGER.info("---------MerchantId-----" + merchantId);
		LOGGER.info("---------accessToken-----" + accessToken);
		model.addAttribute("adminUrl", environment.getProperty("BASE_URL"));
		// merchantId = "9GK2J085R8A3A";
		Merchant merchantCheck = merchantService.findbyPosId(merchantId);
		if (merchantCheck != null && merchantCheck.getAllowMultiPay() != null
				&& merchantCheck.getAllowMultiPay()) {
			Boolean allowMultiPay = merchantService.checkAllowMultiPay(
					merchantCheck, merchantCheck.getPosMerchantId(),
					merchantCheck.getAccessToken(),
					merchantCheck.getAllowMultiPay());
		}
		final HttpSession session = request.getSession();
		if (null != merchantCheck
				&& merchantCheck.getIsInstall() != null
				&& (merchantCheck.getIsInstall() == IConstant.BOOLEAN_TRUE || merchantCheck
						.getIsInstall() == 2)) {
			if (merchantCheck.getIsInstall() == 2) {
				merchantCheck = cloverService.saveMerchant(clover);
				String orderTypeDetail = CloverUrlUtil.getOderType(clover);
				cloverService.saveOrderType(orderTypeDetail, merchantCheck,
						clover);

				LOGGER.info("reset order types");
			}
			if (merchantCheck.getEmployeePosId() == null
					|| merchantCheck.getEmployeePosId().isEmpty()
					|| merchantCheck.equals("")) {
				String employeePosId = CloverUrlUtil
						.createEmployeeOnClover(clover);
				if (employeePosId != null) {
					merchantCheck.setEmployeePosId(employeePosId);
					merchantCheck = merchantService.save(merchantCheck);
				}

			}
			LOGGER.info("---Inisde if bock---"
					+ merchantCheck.getIsInstall());
			String merchId = merchantCheck.getId().toString();
			String base64encode = EncryptionDecryptionUtil.encryption(merchId);
			String merchantName = merchantCheck.getName().replaceAll(
					"[^a-zA-Z0-9]", "");
			
			 String orderLink= environment.getProperty("WEB_BASE_URL") + "/" + merchantName
						+ "/clover/" + base64encode;

			session.setAttribute("merchant", merchantCheck);
			session.setAttribute("inventoryThread", 1);
			session.setAttribute("onlineOrderLink", orderLink);
			// response.sendRedirect("adminHome");
			LOGGER.info("===============  CloverController : Inside existingMerchant :: End  ============= ");

			return "adminHome";
		} else {

			String merchantDetails = CloverUrlUtil.getMerchantDetails(clover);
			if (merchantDetails.contains("id")) {
				// if (null != merchantCheck &&
				// (merchantCheck.getIsInstall()==null
				// ||merchantCheck.getIsInstall() == IConstant.BOOLEAN_FALSE
				// ||merchantCheck.getIsInstall() == IConstant.SOFT_DELETE) ) {
				// webhookAppService.appUnInstall(merchantId);
				//
				// System.out.println("merchant  " + merchantId + " deleted ");
				// }
				LOGGER.info("---Inisde else bock---");
				final Merchant merchant = cloverService.saveMerchant(clover);

				if(!environment.getProperty("FOODKONNEKT_APP_TYPE").equals("Local"))
				MailSendUtil.productInstallationMail(merchant, "Clover",
						"Installed",environment);

				Runnable merchantDetailRunnable = new Runnable() {

					public void run() {
						// TODO Auto-generated method stub
						LOGGER.info("Clover merchantDetailThread Start");
						String paymentModes = CloverUrlUtil
								.getPaymentModes(clover);

						cloverService.savePaymentMode(paymentModes, merchant);

						 String orderTypeDetail =
						 CloverUrlUtil.getOderType(clover);
						 cloverService.saveOrderType(orderTypeDetail,
						 merchant, clover);
						String taxRates = CloverUrlUtil.getTaxRate(clover);
						cloverService.saveTaxRate(taxRates, merchant);

						String openingHour = CloverUrlUtil
								.getOpeningHour(clover);
						cloverService.saveOpeningClosing(merchant, openingHour);
						LOGGER.info("merchantDetailThread Done");
					}
				};
				Thread merchantDetailThread = new Thread(merchantDetailRunnable);
				merchantDetailThread.setName("merchantDetailThread");
				merchantDetailThread.start();

				System.out.println("Done");
				String merchId = merchant.getId().toString();
				String merchantName = merchant.getName().replaceAll(
						"[^a-zA-Z0-9]", "");
				String base64encode = EncryptionDecryptionUtil
						.encryption(merchId);
				
				 String	orderLink= environment.getProperty("WEB_BASE_URL") + "/"
							+ merchantName + "/clover/" + base64encode;

				session.setAttribute("onlineOrderLink", orderLink);
				session.setAttribute("merchantId", merchantId);
				session.setAttribute("merchant", merchant);
				// response.sendRedirect("welcome");
				LOGGER.info("===============  CloverController : Inside existingMerchant :: End  ============= ");

				return "uploadLogo";
			} else {
				LOGGER.info("===============  CloverController : Inside existingMerchant :: End  ============= ");

				return "redirect:" + "https://www.foodkonnekt.com";
			
			}

		}
	}

	@RequestMapping(value = "/clover", method = RequestMethod.GET)
	@ResponseBody
	public String clover(ModelMap model,@RequestParam("merchant_id") String merchantId,
			@RequestParam("code") String code, HttpServletResponse response,
			HttpServletRequest request) throws Exception {
		LOGGER.info("===============  CloverController : Inside clover :: Start  ============= ");

		model.addAttribute("adminUrl", environment.getProperty("BASE_URL"));
		String accessToken = CloverUrlUtil.getMerchantAccessToken(code,environment);
		if (accessToken != null && !accessToken.equals("Failed")) {
			final Clover clover = new Clover();
			clover.setMerchantId(merchantId);
			clover.setAuthToken(accessToken);
			clover.setInstantUrl(IConstant.CLOVER_INSTANCE_URL);
			clover.setUrl(environment.getProperty("CLOVER_URL"));
			LOGGER.info("---------MerchantId-----" + merchantId);
			LOGGER.info("---------accessToken-----" + accessToken);

			// merchantId = "9GK2J085R8A3A";
			Merchant merchantCheck = merchantService.findbyPosId(merchantId);
			// Boolean allowMultiPay =
			// merchantService.checkAllowMultiPay(merchantCheck.getPosMerchantId()
			// , merchantCheck.getAccessToken());
			// Boolean allowMultiPay =
			// merchantService.checkAllowMultiPay(merchantCheck
			// ,merchantCheck.getPosMerchantId() ,
			// merchantCheck.getAccessToken(),
			// merchantCheck.getAllowMultiPay());
			if (merchantCheck != null
					&& merchantCheck.getAllowMultiPay() != null
					&& merchantCheck.getAllowMultiPay()) {
				Boolean allowMultiPay = merchantService.checkAllowMultiPay(
						merchantCheck, merchantCheck.getPosMerchantId(),
						merchantCheck.getAccessToken(),
						merchantCheck.getAllowMultiPay());
			}
			final HttpSession session = request.getSession();
			if (null != merchantCheck
					&& merchantCheck.getIsInstall() != null
					&& (merchantCheck.getIsInstall() == IConstant.BOOLEAN_TRUE || merchantCheck
							.getIsInstall() == 2)) {
				if (merchantCheck.getIsInstall() == 2) {
					merchantCheck = cloverService.saveMerchant(clover);
					String orderTypeDetail = CloverUrlUtil.getOderType(clover);
					cloverService.saveOrderType(orderTypeDetail, merchantCheck,
							clover);

					LOGGER.info("reset order types");
				}
				if (merchantCheck.getEmployeePosId() == null
						|| merchantCheck.getEmployeePosId().isEmpty()
						|| merchantCheck.equals("")) {
					String employeePosId = CloverUrlUtil
							.createEmployeeOnClover(clover);
					if (employeePosId != null) {
						merchantCheck.setEmployeePosId(employeePosId);

					}

				}
				merchantCheck.setAccessToken(accessToken);
				merchantCheck.setUpdatedDate(DateUtil.findCurrentDate());
				merchantCheck = merchantService.save(merchantCheck);
				LOGGER.info("---Inisde if bock---"
						+ merchantCheck.getIsInstall());
				String merchId = merchantCheck.getId().toString();
				String base64encode = EncryptionDecryptionUtil
						.encryption(merchId);
				String merchantName = merchantCheck.getName().replaceAll(
						"[^a-zA-Z0-9]", "");
				String orderLink = environment.getProperty("WEB_BASE_URL") + "/"
						+ merchantName + "/clover/" + base64encode;

				session.setAttribute("merchant", merchantCheck);
				session.setAttribute("onlineOrderLink", orderLink);
				// response.sendRedirect("adminHome");
				
				LOGGER.info("===============  CloverController : Inside clover :: End  ============= ");

				return "adminHome";
			} else {

				String merchantDetails = CloverUrlUtil
						.getMerchantDetails(clover);
				if (merchantDetails.contains("id")) {
					/*
					 * if (null != merchantCheck &&
					 * (merchantCheck.getIsInstall()==null
					 * ||merchantCheck.getIsInstall() == IConstant.BOOLEAN_FALSE
					 * ||merchantCheck.getIsInstall() == IConstant.SOFT_DELETE)
					 * ) { webhookAppService.appUnInstall(merchantId);
					 * 
					 * System.out.println("merchant  " + merchantId +
					 * " deleted "); }
					 */
					System.out.println("---Inisde else bock---");
					final Merchant merchant = cloverService
							.saveMerchant(clover);

					MailSendUtil.productInstallationMail(merchant, "Clover",
							"Installed",environment);

					Runnable merchantDetailRunnable = new Runnable() {

						public void run() {
							// TODO Auto-generated method stub
							System.out
									.println("Clover merchantDetailThread Start");
							String paymentModes = CloverUrlUtil
									.getPaymentModes(clover);

							cloverService.savePaymentMode(paymentModes,
									merchant);

							String orderTypeDetail = CloverUrlUtil
									.getOderType(clover);
							cloverService.saveOrderType(orderTypeDetail,
									merchant, clover);
							String taxRates = CloverUrlUtil.getTaxRate(clover);
							cloverService.saveTaxRate(taxRates, merchant);

							String openingHour = CloverUrlUtil
									.getOpeningHour(clover);
							cloverService.saveOpeningClosing(merchant,
									openingHour);
							LOGGER.info("merchantDetailThread Done");
						}
					};
					Thread merchantDetailThread = new Thread(
							merchantDetailRunnable);
					merchantDetailThread.setName("merchantDetailThread");
					merchantDetailThread.start();

					System.out.println("Done");
					String merchId = merchant.getId().toString();
					String merchantName = merchant.getName().replaceAll(
							"[^a-zA-Z0-9]", "");
					String base64encode = EncryptionDecryptionUtil
							.encryption(merchId);
					String orderLink = environment.getProperty("WEB_BASE_URL") + "/"
							+ merchantName + "/clover/" + base64encode;

					session.setAttribute("onlineOrderLink", orderLink);
					session.setAttribute("merchantId", merchantId);
					session.setAttribute("merchant", merchant);
					// response.sendRedirect("welcome");
					LOGGER.info("===============  CloverController : Inside clover :: End  ============= ");

					return "uploadLogo";
				} else {
					LOGGER.info("===============  CloverController : Inside clover :: End  ============= ");

					return "https://www.foodkonnekt.com";
				}

			}
		} else {
			LOGGER.info("===============  CloverController : Inside clover :: End  ============= ");

			return "https://www.foodkonnekt.com";
		}
	}

	@RequestMapping(value = "/addPerOrderCharge", method = RequestMethod.GET)
	@ResponseBody
	public String addPerOrderCharge(
			@RequestParam(required = true) Integer merchantId) {
		LOGGER.info("===== CloverController : Inside addPerOrderCharge :: merchant  == " + merchantId);

		Merchant merchant = merchantService.findById(merchantId);
		String response = CloverUrlUtil.addMteredPrice(merchant,environment);
		LOGGER.info("===============  CloverController : Inside addPerOrderCharge :: End  ============= ");

		return response;
	}

	@RequestMapping(value = "/getMetererCount", method = RequestMethod.GET)
	@ResponseBody
	public String getMetererCount(
			@RequestParam(required = true) Integer merchantId) {
		LOGGER.info("===============  CloverController : Inside getMetererCount :: Start  ============= merchantId "+merchantId);

		Merchant merchant = merchantService.findById(merchantId);
		String response = CloverUrlUtil.getMeteredCount(merchant,environment);
		LOGGER.info("===============  CloverController : Inside addPerOrderCharge :: End  ============= response "+response);

		return response;
	}

	/**
	 * Place order on clover
	 * 
	 * @param merchantId
	 * @param accessToken
	 * @param cloverOrderVO
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/postOrderOnClover", method = RequestMethod.POST)
	public @ResponseBody String postOrderOnClover(
			@RequestParam("merchant_id") String merchantId,
			@RequestParam("access_token") String accessToken,
			@RequestBody CloverOrderVO cloverOrderVO,
			HttpServletResponse response) {
		LOGGER.info("===============  CloverController : Inside postOrderOnClover :: Start  ============= merchantId "+merchantId+
				" accessToken "+accessToken);

		Clover clover = new Clover();
		clover.setInstantUrl(IConstant.CLOVER_INSTANCE_URL);
		clover.setUrl(environment.getProperty("CLOVER_URL"));
		clover.setMerchantId(merchantId);
		clover.setAuthToken(accessToken);
		LOGGER.info("===============  CloverController : Inside postOrderOnClover :: End  ============= ");
		
		String result= cloverService.postOrderOnClover(cloverOrderVO, clover);
		if(result.contains(IConstant.CLOVER_ORDER_TYPE_EXCEPTION))
		{
			updateCloverOrderType(merchantId,clover);
		}
		return result;
	}

	/**
	 * Payment API method of clover order
	 * 
	 * @param merchantId
	 * @param accessToken
	 * @param paymentVO
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/cloverOrderPayment", method = RequestMethod.POST)
	public @ResponseBody String cloverPayment(
			@RequestParam("merchant_id") String merchantId,
			@RequestParam("access_token") String accessToken,
			@RequestBody PaymentVO paymentVO, HttpServletResponse response) {
		LOGGER.info("===============  CloverController : Inside cloverPayment :: Start  ============= ");

		JsonObject jsonObject = null;
		Clover clover = new Clover();
		clover.setInstantUrl(IConstant.CLOVER_INSTANCE_URL);
		clover.setUrl(environment.getProperty("CLOVER_URL"));
		clover.setMerchantId(merchantId);
		clover.setAuthToken(accessToken);
		/*
		 * double aDouble = Double.parseDouble(paymentVO.getTotal()); // long
		 * orderTotalAmount=(long) (aDouble*100); double d=aDouble*100; Long
		 * orderTotalAmount=Math.round(d);
		 * System.out.println("Long"+orderTotalAmount); double roundOff =
		 * Math.round(aDouble * 100.0) / 100.0; double total = roundOff * 100;
		 * double grandTotal= Math.round(total);
		 * System.out.println("Double"+grandTotal); long tip =
		 * (long)paymentVO.getTip()*100; if
		 * (!paymentVO.getPaymentMethod().equals("Cash")) { try {
		 * 
		 * 
		 * final String MERCHANT_ID = merchantId; final String ACCESS_TOKEN =
		 * accessToken; // Test Credit Card Info final String CC_NUMBER =
		 * paymentVO.getCcNumber(); final String CVV_NUMBER =
		 * paymentVO.getCcCode(); final int EXP_MONTH = paymentVO.getExpMonth();
		 * final int EXP_YEAR = paymentVO.getExpYear(); jsonObject =
		 * Main.testWebPay(MERCHANT_ID, ACCESS_TOKEN, CC_NUMBER, CVV_NUMBER,
		 * EXP_MONTH, EXP_YEAR, paymentVO.getOrderPosId(), orderTotalAmount
		 * ,tip); } catch (Exception e) { LOGGER.error("error: " + e.getMessage());
		 * 
		 * } System.out.println("-----CloverController-----" + jsonObject); if
		 * (jsonObject != null) { return jsonObject.toString(); } else { return
		 * new JSONObject("{}").toString(); } } else { PaymentMode paymentMode =
		 * null; if (merchantId != null && !merchantId.isEmpty()) { Merchant
		 * merchant = merchantService.findbyPosId(merchantId); paymentMode =
		 * businessService.findByMerchantIdAndLabel(merchant.getId(), "Cash"); }
		 * 
		 * CashVO cashVO = new CashVO(); cashVO.setAmount(total);
		 * cashVO.setCashTendered(total); cashVO.setOffline(1);
		 * cashVO.setResult("SUCCESS"); cashVO.setTipAmount(tip);
		 * 
		 * Tender tender = new Tender(); tender.setEditable(0);
		 * tender.setEnabled(0); if (paymentMode != null) {
		 * tender.setId(paymentMode.getPosPaymentModeId()); }
		 * tender.setOpensCashDrawer(0); tender.setVisible(0);
		 * tender.setSupportsTipping(1); cashVO.setTender(tender); Gson gson =
		 * new Gson(); String cashJson = gson.toJson(cashVO);
		 * System.out.println("----Cash input json---" + cashJson); String
		 * result = CloverUrlUtil.cashPayment(cashJson, merchantId, accessToken,
		 * paymentVO.getOrderPosId()); System.out.println("---Cash result---" +
		 * result); return result; }
		 */

		Long orderTotalAmount = 0l;
		String orderId = paymentVO.getOrderPosId();
		// String orderResponse =
		// CloverUrlUtil.getFeedbackClover(clover,orderId);
		if (orderId != null && !orderId.isEmpty() && orderId != "") {
			String orderResponse = CloverUrlUtil.getFeedbackClover(clover,
					orderId);
			LOGGER.info("CLOVER CONTROLLER clover order payment"
					+ orderResponse);
			if (orderResponse != null && orderResponse.contains("id")) {
				JSONObject jObject = new JSONObject(orderResponse);
				// Number tmp = (Number) (jObject.getLong("total"));
				orderTotalAmount = (jObject.getLong("total"));
				LOGGER.info("orderTotalAmount"+orderTotalAmount);
			}

		}

		// long tip = (long)(paymentVO.getTip()*100);
		double d = paymentVO.getTip() * 100;
		Long tip = Math.round(d);

		if (orderTotalAmount != null) {

			if (!paymentVO.getPaymentMethod().equals("Cash")) {
				try {

					Boolean status = false;
					final String MERCHANT_ID = merchantId;
					final String ACCESS_TOKEN = accessToken;
					// Test Credit Card Info
					final String CC_NUMBER = paymentVO.getCcNumber();
					final String CVV_NUMBER = paymentVO.getCcCode();
					final Integer EXP_MONTH = paymentVO.getExpMonth();
					final Integer EXP_YEAR = paymentVO.getExpYear();
					final String token = paymentVO.getToken();
					final String expirationDate = paymentVO.getExpirationDate();
					final String first6 = paymentVO.getFirst6();
					final String last4 = paymentVO.getLast4();
					final Boolean isVaulted = paymentVO.getIsVaulted();
					if (paymentVO.getIsVaulted() != null
							&& paymentVO.getIsVaulted() == true) {
						if (token != null && expirationDate != null
								&& first6 != null && last4 != null) {
							status = true;
						}
					} else {
						if (CC_NUMBER != null && CVV_NUMBER != null
								&& EXP_MONTH != null && EXP_YEAR != null) {
							status = true;
						}
					}

					if (status) {
						jsonObject = Main.testWebPay(MERCHANT_ID, ACCESS_TOKEN,
								CC_NUMBER, CVV_NUMBER, EXP_MONTH, EXP_YEAR,
								paymentVO.getOrderPosId(), orderTotalAmount,
								tip, token, expirationDate, first6, last4,
								isVaulted,environment);
					}

				} catch (Exception e) {
					LOGGER.error("error: " + e.getMessage());

				}
				System.out.println("-----CloverController-----" + jsonObject);
				if (jsonObject != null) {
					LOGGER.info("-----CloverController-----inside if (jsonObject != null)" );

					LOGGER.info("===============  CloverController : Inside cloverPayment :: End  ============= ");

					return jsonObject.toString();
				} else {
					LOGGER.info("===============  CloverController : Inside cloverPayment :: End  ============= ");
					return new JSONObject("{}").toString();
				}
			} else {
				if (tip != null && tip > 0) {
					PaymentMode paymentMode = null;
					if (merchantId != null && !merchantId.isEmpty()) {
						Merchant merchant = merchantService
								.findbyPosId(merchantId);
						paymentMode = businessService.findByMerchantIdAndLabel(
								merchant.getId(), "Cash");
					}

					CashVO cashVO = new CashVO();
					cashVO.setAmount(0l);
					cashVO.setCashTendered(0l);
					cashVO.setOffline(1);
					cashVO.setResult("SUCCESS");
					cashVO.setTipAmount(tip);

					Tender tender = new Tender();
					tender.setEditable(0);
					tender.setEnabled(0);
					if (paymentMode != null) {
						tender.setId(paymentMode.getPosPaymentModeId());
					}
					tender.setOpensCashDrawer(0);
					tender.setVisible(0);
					tender.setSupportsTipping(1);
					cashVO.setTender(tender);
					Gson gson = new Gson();
					String cashJson = gson.toJson(cashVO);
					LOGGER.info("----Cash input json---" + cashJson);
					String result = CloverUrlUtil.cashPayment(cashJson,
							merchantId, accessToken, paymentVO.getOrderPosId(),environment);
					LOGGER.info("---Cash result---" + result);
					if (result != null) {
						return result;
					} else {
						return new JSONObject("{}").toString();
					}
				} else {
					LOGGER.info("===============  CloverController : Inside cloverPayment :: End  ============= ");

					return "{\"result\":\"SUCCESS\"}";
				}
				// return "{\"result\":\"DECLINED\"}";
			}

		} else {
			LOGGER.info("===============  CloverController : Inside cloverPayment :: End  ============= ");

			return new JSONObject("{}").toString();
		}
	}

	@RequestMapping(value = "/orderConfirmation", method = RequestMethod.GET)
	public @ResponseBody String postOrderOnClover(
			@RequestParam("orderId") String orderId,
			@RequestParam("type") String type,
			@RequestParam(required = false) String reason,
			HttpServletResponse response) {
		 LOGGER.info("CloverController :: postOrderOnClover : starts : orderId "+orderId);
		 LOGGER.info("CloverController :: postOrderOnClover : orderType "+type);
		boolean status = orderService.setOrderStatus(orderId, type, reason);
		  LOGGER.info("CloverController :: postOrderOnClover : status "+status);

		if (status) {
			LOGGER.info("CloverController :: postOrderOnClover : End : ");
			return "success";
		} else {
			LOGGER.info("CloverController :: postOrderOnClover : End : ");

			return "OrderId doesn't exist";
		}
	}

	@RequestMapping(value = "/orderConfirmationById", method = RequestMethod.GET)
	public @ResponseBody String orderConfirmationById(
			@RequestParam("orderId") Integer orderId,
			@RequestParam("type") String type,
			@RequestParam(required = false) String reason,
			@RequestParam(required = false) String changedOrderAvgTime,
			HttpServletResponse response) {
		 LOGGER.info("----------------Start :: CloverController : orderConfirmationById------------------");
		 
		 LOGGER.info("CloverController :: orderConfirmationById : orderId "+orderId);
		 LOGGER.info("CloverController :: orderConfirmationById : orderType "+type);
		 
		String status = orderService.setOrderStatus(orderId, type, reason,changedOrderAvgTime);
		  LOGGER.info("CloverController :: orderConfirmationById : status "+status);
		  if (status!=null) {
			 LOGGER.info("----------------End :: CloverController : orderConfirmationById------------------");
        	 return  status;
          }
		LOGGER.info("----------------End :: CloverController : orderConfirmationById------------------");
        return "OrderId doesn't exist";
	}

	@RequestMapping(value = "/getFutureOrders", method = RequestMethod.GET)
	public @ResponseBody String getFutureOrders(
			@RequestParam("merchantId") String merchantId,
			HttpServletResponse response) {
		LOGGER.info("===== CloverController : Inside getFutureOrders :: merchant  == " + merchantId);

		String status = orderService.getFutureOrders(merchantId);
		  LOGGER.info("CloverController :: getFutureOrders : status "+status);

		if (status != null) {
			LOGGER.info("===============  CloverController : Inside getFutureOrders :: End  ============= ");

			return status;
		} else {
			LOGGER.info("===============  CloverController : Inside getFutureOrders :: End  ============= ");

			return "Not Found";
		}
	}

	/*
	 * @RequestMapping(value = "/getMerchantData", method = RequestMethod.GET)
	 * public @ResponseBody String getMerchantData(String orderId, String
	 * merchantId, HttpServletResponse response) {
	 * System.out.println("inside /getMerchantData"); String data =
	 * cloverService.getMerchantData(orderId,merchantId); //String data =
	 * cloverService.getMerchantData("MTAHJTWSNC4N8","9GK2J085R8A3A");
	 * System.out.println(data); return
	 * "redirect:"+environment.getProperty("WEB_BASE_URL")+data; }
	 */
	@RequestMapping(value = "/getAllFutureOrders", method = RequestMethod.GET)
	public @ResponseBody String getAllFutureOrders(
			@RequestParam("merchantId") String merchantId,
			HttpServletResponse response) {
		LOGGER.info("===== CloverController : Inside getAllFutureOrders starts:: merchant  == " + merchantId);

		String status = orderService.getAllFutureOrders(merchantId);
		if (status != null) {
			LOGGER.info("===============  CloverController : Inside getAllFutureOrders :: End  ============= ");

			return status;
		} else {
			LOGGER.info("===============  CloverController : Inside getAllFutureOrders :: End  ============= ");

			return "Not Found";
		}
	}

	@RequestMapping(value = "/getAllFutureOrdersByLocationUid", method = RequestMethod.GET)
	public @ResponseBody String getAllFutureOrdersByLocationUid(
			@RequestParam(value = "locationUid") String locationUid,
			HttpServletResponse response) {
		LOGGER.info("===== CloverController : Inside getFutureOrders :: locationUid  == " + locationUid);

		String status = orderService
				.getAllFutureOrdersByLocationUid(locationUid);
		LOGGER.info("===== CloverController : Inside getFutureOrders :: status  == " + status);

		if (status != null) {
			LOGGER.info("===============  CloverController : Inside getFutureOrders :: End  ============= ");

			return status;
		} else {

			status = "";
			status = status + "{\"code\":0 ,";
			status = status + "\"orders\":[]}";
			LOGGER.info("===============  CloverController : Inside getFutureOrders :: End  ============= ");

			return status.toString();
		}

	}

	@RequestMapping(value = "/orderConfirmationByIdByMerchantUId", method = RequestMethod.GET)
	public @ResponseBody String orderConfirmationByIdByMerchantUId(
			@RequestParam("orderId") Integer orderId,
			@RequestParam("merchantUId") String merchantUId,
			@RequestParam("type") String type,
			@RequestParam(required = false) String reason,
			@RequestParam(required = false) String changedOrderAvgTime,
			HttpServletResponse response) {
		LOGGER.info("===============  CloverController : Inside orderConfirmationByIdByMerchantUId :: Start  ============= ");
		String status = "false";
		Merchant merchant = merchantService.findByMerchantUid(merchantUId);
		if (merchant != null) {
			status = orderService.setOrderStatus(orderId, type, reason,
					changedOrderAvgTime, merchant.getId());
			if (status.contains("true")) {
				LOGGER.info("===============  CloverController : Inside orderConfirmationByIdByMerchantUId :: End  ============= ");

				return "success";
			} else {
				LOGGER.info("===============  CloverController : Inside orderConfirmationByIdByMerchantUId :: End  ============= ");

				return status;
			}
		} else {
			LOGGER.info("===============  CloverController : Inside orderConfirmationByIdByMerchantUId :: End  ============= ");

			return "Unauthorize merchant";
		}
	}
	
	@RequestMapping(value = "/uninstallMerchant", method = RequestMethod.GET)
	@ResponseBody
	public String uninstallMerchant(@RequestParam("merchantUid") String merchantUid ,@RequestParam("isClover") Boolean isClover) {
		LOGGER.info("===============  CloverController : Inside uninstallMerchant() : :: start  ============= ");
		Gson gson = new Gson();
		Map<String, Object> map = new HashMap<String, Object>();
		Merchant merchant=null;
		try {
			if(merchantUid!=null)
			{
			
			if(isClover!=null && isClover==IConstant.TRUE_STATUS)
			{
					merchant = merchantService.findByPosMerchantId(merchantUid);
			}
			else merchant = merchantService.findByMerchantUid(merchantUid);
			}	
			if (merchant != null) {
				merchant=merchantService.merhcnatUninstallProcess(merchant);
				map.put("status", true);
				map.put("message", "Merchant uninstalled successfully");
			}else {
				map.put("status", false);
				map.put("message", "Merchant Not found");
			}

		} catch (Exception e) {
			LOGGER.error("error: " + e.getMessage());
			MailSendUtil.sendExceptionByMail(e,environment);
		}
		LOGGER.info("===============  CloverController : Inside uninstallMerchant() : :: End  ============= ");
		return gson.toJson(map);
	}
	
	@RequestMapping(value = "/saveOrUpdatePosPrintStatus", method = RequestMethod.GET)
	@ResponseBody
	public String saveOrUpdatePosPrintStatus(@RequestParam("merchantPosId") String merchantPosId,@RequestParam("orderId") String orderId,@RequestParam("isPrint") Boolean isPrint) {
		LOGGER.info("===============  CloverController : Inside saveOrUpdatePosPrintStatus() : :: start  ============= ");
		Gson gson = new Gson();
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			Merchant merchant = merchantService.findByPosMerchantId(merchantPosId);
			if (merchant != null && orderId!=null) {
				PrintJob printJob =printJobService.findByOrderId(orderId);
				Integer attempt=1;
				if(printJob==null)
				{
				 printJob =new PrintJob();
				}
				attempt=(printJob.getAttempt()!=null)?printJob.getAttempt()+1:1;
				printJob.setOrderId(orderId);
				printJob.setAttempt(attempt);
				printJob.setMerchant(merchant);
				printJob.setPrint(isPrint);
				printJobService.saveOrUpdate(printJob);
				//merchant=merchantService.merhcnatUninstallProcess(merchant);
				map.put("status", true);
				map.put("message", "Print Status Updated");
			}else {
				map.put("status", false);
				map.put("message", "Merchant Not found");
			}
		} catch (Exception e) {
			LOGGER.error("error: " + e.getMessage());
			MailSendUtil.sendExceptionByMail(e,environment);
		}
		LOGGER.info("===============  CloverController : Inside uninstallMerchant() : :: End  ============= ");
		return gson.toJson(map);
	}
	
	@RequestMapping(value = "/getPendingPosPrint", method = RequestMethod.GET)
	@ResponseBody
	public String getPendingPosPrint(@RequestParam("merchantPosId") String merchantPosId) {
		LOGGER.info("===============  CloverController : Inside saveOrUpdatePosPrintStatus() : :: start  ============= ");
		Gson gson = new Gson();
		Map<String, Object> map = new HashMap<String, Object>();
		List<String> ordersList=new ArrayList<String>();
		String message="Data Not Found";
		try {
			Merchant merchant = merchantService.findByPosMerchantId(merchantPosId);
			if (merchant != null) {
				List<PrintJob> printJob =printJobService.findByMerchantId(merchant.getId());
				if(printJob!=null && printJob.size()>0)
				{
					for (PrintJob printJob2 : printJob) {
						if(printJob2.getAttempt()!=null && (printJob2.isMailSendStatus()==null || printJob2.isMailSendStatus()==false) & printJob2.getAttempt()>3)
						{
							MailSendUtil.sendPrintFailMail(printJob2,environment);
							printJob2.setMailSendStatus(IConstant.TRUE_STATUS);
						}else ordersList.add(printJob2.getOrderId());
					}
					message="Data Found";
				}
				map.put("status", true);
				map.put("message", message);
				map.put("List", ordersList);
				
			}else {
				map.put("status", false);
				map.put("message", "Merchant Not found");
			}
		} catch (Exception e) {
			LOGGER.info("===============  CloverController : Inside saveOrUpdatePosPrintStatus() : :: Exceptuon "+e);
			MailSendUtil.sendExceptionByMail(e,environment);
		}
		LOGGER.info("===============  CloverController : Inside saveOrUpdatePosPrintStatus() : :: End  ============= ");
		return gson.toJson(map);
	}


	public void updateCloverOrderType(String merchantPosId ,Clover clover) {
		LOGGER.info("===============  CloverController : Inside updateCloverOrderType() : :: start  ============= ");
		try {
			if (merchantPosId != null) {
				LOGGER.info("CloverController : Inside updateCloverOrderType() : :merchantPosId: " + merchantPosId);
				Merchant merchant = merchantService.findByPosMerchantId(merchantPosId);
				if (merchant != null) {
					String orderTypeDetail = CloverUrlUtil.getOderType(clover);
					cloverService.saveUpdatedOrderType(orderTypeDetail, merchant, clover);
					LOGGER.info("CloverController : Inside updateCloverOrderType() : :updated: ");
				}
			}
		} catch (Exception e) {
			LOGGER.info("CloverController : Inside updateCloverOrderType() : :Exception: " + e);
		}
	}

	
	
	
	@RequestMapping(value = "/getAllInventory", method = RequestMethod.GET)
	public @ResponseBody String getAllInventory(@RequestParam("merchant_id") String merchantId,
			@RequestParam("access_token") String accessToken, HttpServletResponse response,
			HttpServletRequest request) {
		LOGGER.info("===============  CloverController : Inside getAllInventory() : :: start  ============= ");
		LOGGER.info("---------MerchantId-----" + merchantId);
		LOGGER.info("---------accessToken-----" + accessToken);

		final Clover clover = new Clover();
		clover.setMerchantId(merchantId);
		clover.setAuthToken(accessToken);
		clover.setInstantUrl(IConstant.CLOVER_INSTANCE_URL);
		clover.setUrl(environment.getProperty("CLOVER_URL"));

		final Merchant merchant = merchantService.findbyPosId(merchantId);

		// Re-fetch paymentModes,taxRates ,openingHour

		String paymentModes = CloverUrlUtil.getPaymentModes(clover);

		cloverService.savePaymentMode(paymentModes, merchant);

		String taxRates = CloverUrlUtil.getTaxRate(clover);
		cloverService.saveTaxRate(taxRates, merchant);

		String openingHour = CloverUrlUtil.getOpeningHour(clover);
		cloverService.saveOpeningClosing(merchant, openingHour);

		// Re-fetch Category,Item and ModifierAndModifierGroup

		String modifierJson = CloverUrlUtil.getModifierAndModifierGroup(clover);
		// modifierService.saveModifierAndModifierGroup(modifierJson, merchant);
		modifierService.updateModifierAndModifierGroup(modifierJson, merchant);
		LOGGER.info("-----Update ModifiersAndModifierGroups Done-----");

		cloverService.saveCategory(clover, merchant);
		LOGGER.info("-----Save Category Done-----");

		cloverService.saveItem(clover, merchant);
		LOGGER.info("-----Save Item Done-----");

		return "success";
	}
	
	

}
