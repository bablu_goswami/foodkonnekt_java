package com.foodkonnekt.controller;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.csvreader.CsvWriter;
import com.foodkonnekt.clover.vo.AllOrderVo;
import com.foodkonnekt.model.Item;
import com.foodkonnekt.model.Merchant;
import com.foodkonnekt.model.ModifierGroup;
import com.foodkonnekt.model.ModifierModifierGroupDto;
import com.foodkonnekt.model.Modifiers;
import com.foodkonnekt.model.OrderItem;
import com.foodkonnekt.model.OrderItemModifier;
import com.foodkonnekt.model.OrderPizza;
import com.foodkonnekt.model.OrderR;
import com.foodkonnekt.model.PizzaCrust;
import com.foodkonnekt.model.PizzaTemplate;
import com.foodkonnekt.model.PizzaTopping;
import com.foodkonnekt.repository.ItemModifiersRepository;
import com.foodkonnekt.repository.ItemmRepository;
import com.foodkonnekt.repository.MerchantRepository;
import com.foodkonnekt.repository.ModifierGroupRepository;
import com.foodkonnekt.repository.ModifierModifierGroupRepository;
import com.foodkonnekt.repository.ModifiersRepository;
import com.foodkonnekt.repository.OrderItemModifierRepository;
import com.foodkonnekt.repository.OrderItemRepository;
import com.foodkonnekt.repository.OrderRepository;
import com.foodkonnekt.service.CategoryService;
import com.foodkonnekt.service.CreateInventoryService;
import com.foodkonnekt.service.CustomerService;
import com.foodkonnekt.service.ItemService;
import com.foodkonnekt.service.ModifierService;
import com.foodkonnekt.service.OrderService;
import com.foodkonnekt.service.PizzaService;
import com.foodkonnekt.util.DateUtil;
import com.foodkonnekt.util.IConstant;
import com.foodkonnekt.util.MailSendUtil;
import com.foodkonnekt.util.ProducerUtil;
import com.foodkonnekt.util.UrlConstant;

@Controller
public class InventoryController {

	private static final Logger LOGGER= LoggerFactory.getLogger(InventoryController.class);
	
	@Autowired
    private Environment environment;
	
	@Autowired
	private MerchantRepository merchantRepository;
	
	@Autowired
	private ItemModifiersRepository itemModifierRepository;
	
    @Autowired
    private ItemService itemService;

    @Autowired
    private ModifierService modifierService;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private OrderService orderService;
    
    @Autowired
    private ModifierModifierGroupRepository modifierModifierGroupRepository;
    
    @Autowired
    private ModifierGroupRepository modifierGroupRepository;
    
    @Autowired
    private OrderItemRepository orderItemRepository;
    
    @Autowired
    private OrderItemModifierRepository orderItemModifierRepository;
    
    @Autowired
    private ModifiersRepository modifierRepository;
    
    @Autowired
	private ItemmRepository itemmRepository;
    
    @Autowired
	private OrderRepository orderRepository;

	@Autowired
	private ModifiersRepository modifiersRepository;
	
	@Autowired
	private CreateInventoryService createInventoryService;
    
    @RequestMapping(value = "/helloWorld", method = RequestMethod.GET)
    public String printWelcome(ModelMap model, HttpServletRequest request, HttpServletResponse response) {
        return "helloWorld";

    }

    @RequestMapping(value = "/inventoryDataUsingAjax", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody String springPaginationDataTables(HttpServletRequest request) throws IOException {
    	LOGGER.info("===============  InventoryController : Inside springPaginationDataTables :: Start  ============= ");

        HttpSession session = request.getSession(false);
        Merchant merchant = (Merchant) session.getAttribute("merchant");
        Integer pageNumber = 0;
        Integer pageDisplayLength=0;
        if (null != request.getParameter("iDisplayStart") && null != request.getParameter("iDisplayLength")){
       	 pageDisplayLength = Integer.valueOf(request.getParameter("iDisplayLength"));
           pageNumber = (Integer.valueOf(request.getParameter("iDisplayStart")) / pageDisplayLength) + 1;
       }
        // Fetch search parameter
        String searchParameter = request.getParameter("sSearch");
       // Integer pageDisplayLength = Integer.valueOf(request.getParameter("iDisplayLength"));
        // Create page list data
        String jsonOutput = "";
        
        if (merchant != null) {
        	List<PizzaTemplate> pizzaTemplates = pizzaService.findByMerchantId(merchant.getId());
            if(!pizzaTemplates.isEmpty() && pizzaTemplates.size()>0){
            	session.setAttribute("pizzaShowStatus", "success");
            }else{
            	session.setAttribute("pizzaShowStatus", "failed");
            }
        }
        if (merchant != null) {
            jsonOutput = itemService.findinventoryLineItemByMerchantId(merchant.getId(), pageDisplayLength, pageNumber,
                            searchParameter);
        }
        LOGGER.info("===============  InventoryController : Inside springPaginationDataTables :: End  ============= ");

        return jsonOutput;

    }

    @RequestMapping(value = "/filterByCategory", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody String filterByCategory(HttpServletRequest request,
                    @RequestParam(required = false) int categoryId) throws IOException {
    	LOGGER.info("===============  InventoryController : Inside filterByCategory :: Start  ============= ");

        HttpSession session = request.getSession(false);
        Merchant merchant = (Merchant) session.getAttribute("merchant");
        String jsonOutput = "";
        if (merchant != null) {
            jsonOutput = itemService.filterInventoryByCategoryId(merchant.getId(), categoryId);
        }
        LOGGER.info("===============  InventoryController : Inside filterByCategory :: End  ============= ");

        return jsonOutput;
    }

    @RequestMapping(value = "/modifiersDataTables", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody String findAllModifiers(HttpServletRequest request) throws IOException {
       
    	LOGGER.info("===============  InventoryController : Inside findAllModifiers :: Start  ============= ");

    	HttpSession session = request.getSession(false);
        Merchant merchant = (Merchant) session.getAttribute("merchant");
        Integer pageNumber = 0;
        Integer pageDisplayLength=0;
        if (null != request.getParameter("iDisplayStart") && null != request.getParameter("iDisplayLength")){
       	 pageDisplayLength = Integer.valueOf(request.getParameter("iDisplayLength"));
           pageNumber = (Integer.valueOf(request.getParameter("iDisplayStart")) / pageDisplayLength) + 1;
       }
        // Fetch search parameter
        String searchParameter = request.getParameter("sSearch");
       // Integer pageDisplayLength = Integer.valueOf(request.getParameter("iDisplayLength"));
        String jsonOutput = "";
        if (merchant != null) {
            jsonOutput = modifierService.findModifierByMerchantById(merchant.getId(), pageDisplayLength, pageNumber,
                            searchParameter);
        }
        LOGGER.info("===============  InventoryController : Inside findAllModifiers :: End  ============= ");

        return jsonOutput;
    }

    @RequestMapping(value = "/modifiersGroupsDataTables", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody String findAllModifierGroups(HttpServletRequest request) throws IOException {
        LOGGER.info("===============  InventoryController : Inside findAllModifierGroups :: Start  ============= ");

    	HttpSession session = request.getSession(false);
        Merchant merchant = (Merchant) session.getAttribute("merchant");
        Integer pageNumber = 0;
        Integer pageDisplayLength=0;
        if (null != request.getParameter("iDisplayStart") && null != request.getParameter("iDisplayLength")){
       	 pageDisplayLength = Integer.valueOf(request.getParameter("iDisplayLength"));
           pageNumber = (Integer.valueOf(request.getParameter("iDisplayStart")) / pageDisplayLength) + 1;
       }
        // Fetch search parameter
        String searchParameter = request.getParameter("sSearch");
       // Integer pageDisplayLength = Integer.valueOf(request.getParameter("iDisplayLength"));
        String jsonOutput = "";
        if (merchant != null) {
            jsonOutput = modifierService.findModifierGroupsByMerchantById(merchant.getId(), pageDisplayLength,
                            pageNumber, searchParameter);
        }
        LOGGER.info("===============  InventoryController : Inside findAllModifierGroups :: End  ============= ");

        return jsonOutput;
    }

    @RequestMapping(value = "/customersDataTables", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody String findCustomerInventory(HttpServletRequest request) throws IOException {
       LOGGER.info("===============  InventoryController : Inside findCustomerInventory :: Start  ============= ");

    	HttpSession session = request.getSession(false);
        Merchant merchant = (Merchant) session.getAttribute("merchant");
        LOGGER.info("===== InventoryController : Inside findCustomerInventory :: merchant  == " + merchant.getId());

        Integer pageNumber = 0;
        Integer pageDisplayLength=0;
        if (null != request.getParameter("iDisplayStart") && null != request.getParameter("iDisplayLength")){
       	 pageDisplayLength = Integer.valueOf(request.getParameter("iDisplayLength"));
           pageNumber = (Integer.valueOf(request.getParameter("iDisplayStart")) / pageDisplayLength) + 1;
       }
        // Fetch search parameter
        String searchParameter = request.getParameter("sSearch");
       // Integer pageDisplayLength = Integer.valueOf(request.getParameter("iDisplayLength"));
        String jsonOutput = "";
        if (merchant != null) {
            jsonOutput = customerService.findCustomerInventory(merchant.getId(), pageDisplayLength, pageNumber,searchParameter);
        }
        LOGGER.info("===============  InventoryController : Inside findCustomerInventory :: End  ============= ");

        return jsonOutput;
    }

    @RequestMapping(value = "/searchModifiersByText", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody String searchModifiers(HttpServletRequest request,
                    @RequestParam(required = true) String searchTxt) throws IOException {
        LOGGER.info("===============  InventoryController : Inside searchModifiers :: Start  ============= ");

    	HttpSession session = request.getSession(false);
        Merchant merchant = (Merchant) session.getAttribute("merchant");
        LOGGER.info("===== InventoryController : Inside searchModifiers :: merchant  == " + merchant.getId());

        String searchResult = "";
        if (merchant != null) {
            searchResult = modifierService.searchModifiersByTxt(merchant.getId(), searchTxt);
        }
        LOGGER.info("===============  InventoryController : Inside searchModifiers :: End  ============= ");

        return searchResult;
    }

    @RequestMapping(value = "/searchModifierGroupByText", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody String searchModifierGroup(HttpServletRequest request,
                    @RequestParam(required = true) String searchTxt) throws IOException {
       LOGGER.info("===============  InventoryController : Inside searchModifierGroup :: Start  ============= ");

    	HttpSession session = request.getSession(false);
        Merchant merchant = (Merchant) session.getAttribute("merchant");
        LOGGER.info("===== InventoryController : Inside searchModifierGroup :: merchant  == " + merchant.getId());

        String searchResult = "";
        if (merchant != null) {
            searchResult = modifierService.searchModifierGroupByTxt(merchant.getId(), searchTxt);
        }
        LOGGER.info("===============  InventoryController : Inside searchModifierGroup :: End  ============= ");

        return searchResult;
    }

    @RequestMapping(value = "/searchItemByText", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody String searchItems(HttpServletRequest request, @RequestParam(required = true) String searchTxt)
                    throws IOException {
    	LOGGER.info("===============  InventoryController : Inside searchItems :: Start  ============= ");

        HttpSession session = request.getSession(false);
        Merchant merchant = (Merchant) session.getAttribute("merchant");
        LOGGER.info("===== InventoryController : Inside searchItems :: merchant  == " + merchant.getId());

        String searchResult = "";
        if (merchant != null) {
            searchResult = itemService.searchItemByTxt(merchant.getId(), searchTxt);
        }
        LOGGER.info("===============  InventoryController : Inside searchItems :: End  ============= ");

        return searchResult;
    }

    @RequestMapping(value = "/categoryDataTables", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody String findCategoryInventory(HttpServletRequest request) throws IOException {
       LOGGER.info("===============  InventoryController : Inside findCategoryInventory :: Start  ============= ");

    	HttpSession session = request.getSession(false);
        Merchant merchant = (Merchant) session.getAttribute("merchant");
        Integer pageNumber = 0;
        Integer pageDisplayLength=0;
        if (null != request.getParameter("iDisplayStart") && null != request.getParameter("iDisplayLength")){
        	 pageDisplayLength = Integer.valueOf(request.getParameter("iDisplayLength"));
            pageNumber = (Integer.valueOf(request.getParameter("iDisplayStart")) / pageDisplayLength) + 1;
        }
        // Fetch search parameter
        String searchParameter = request.getParameter("sSearch");
        
        String jsonOutput = "";
        if (merchant != null) {
            jsonOutput = categoryService.findCAtegoryInventory(merchant.getId(), pageDisplayLength, pageNumber,
                            searchParameter);
        }
        LOGGER.info("===============  InventoryController : Inside findCategoryInventory :: End  ============= ");

        return jsonOutput;
    }

    @RequestMapping(value = "/searchCategoryByText", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody String searchCategoryByTxt(HttpServletRequest request,
                    @RequestParam(required = true) String searchTxt) throws IOException {
        LOGGER.info("===============  InventoryController : Inside searchCategoryByTxt :: Start  ============= ");

    	HttpSession session = request.getSession(false);
        Merchant merchant = (Merchant) session.getAttribute("merchant");
        LOGGER.info("===== InventoryController : Inside searchCategoryByTxt :: merchant  == " + merchant.getId());

        String searchResult = "";
        if (merchant != null) {
            searchResult = categoryService.searchCategoryByTxt(merchant.getId(), searchTxt);
        }
        LOGGER.info("===============  InventoryController : Inside searchCategoryByTxt :: End  ============= ");

        return searchResult;
    }

	/* @RequestMapping(value = "/allOrdersDataTables", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody String findAllOrders(HttpServletRequest request) throws IOException {
        HttpSession session = request.getSession(false);
        Merchant merchant = (Merchant) session.getAttribute("merchant");
        Integer pageNumber = 0;
        if (null != request.getParameter("iDisplayStart"))
            pageNumber = (Integer.valueOf(request.getParameter("iDisplayStart")) / 10) + 1;
        // Fetch search parameter
        String searchParameter = request.getParameter("sSearch");
        Integer pageDisplayLength = Integer.valueOf(request.getParameter("iDisplayLength"));
        String jsonOutput = "";
        if (merchant != null) {
            jsonOutput = orderService.findAllOrderFromDataTable(merchant.getId(), pageDisplayLength, pageNumber,
                            searchParameter);
        }
        return jsonOutput;
    }*/

    @RequestMapping(value = "/allOrdersDataTables", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody String findAllOrders(HttpServletRequest request, ModelMap model,  @RequestParam(required = false) String startDate,
    		@RequestParam(required = false) String endDate) throws IOException {
        LOGGER.info("===============  InventoryController : Inside findAllOrders :: Start  ============= ");

    	HttpSession session = request.getSession(false);
        Merchant merchant = (Merchant) session.getAttribute("merchant");
        LOGGER.info("===== InventoryController : Inside findAllOrders :: merchant  == " + merchant.getId());

        model.addAttribute("times", DateUtil.findAllTime());
        Integer pageNumber = 0;
        Integer pageDisplayLength=0;
        if (null != request.getParameter("iDisplayStart") && null != request.getParameter("iDisplayLength")){
       	 pageDisplayLength = Integer.valueOf(request.getParameter("iDisplayLength"));
           pageNumber = (Integer.valueOf(request.getParameter("iDisplayStart")) / pageDisplayLength) + 1;
       }
        // Fetch search parameter
        String searchParameter = request.getParameter("sSearch");
       // Integer pageDisplayLength = Integer.valueOf(request.getParameter("iDisplayLength"));
        String jsonOutput = "";
        Date today30 = (merchant != null && merchant.getTimeZone() != null
				&& merchant.getTimeZone().getTimeZoneCode() != null)
				? DateUtil.getCurrentDateForTimeZonee(merchant.getTimeZone().getTimeZoneCode())
				: new Date();
        if(startDate== null && endDate== null){
    		DateFormat dateFormat = new SimpleDateFormat(IConstant.YYYYMMDD);
    		
    		Date today =  (merchant != null && merchant.getTimeZone() != null
    				&& merchant.getTimeZone().getTimeZoneCode() != null)
    				? DateUtil.getCurrentDateForTimeZonee(merchant.getTimeZone().getTimeZoneCode())
    				: new Date();
    		Calendar cal = new GregorianCalendar();
    		cal.setTime(today);
    		cal.add(Calendar.DAY_OF_MONTH, -2);
    		 today30 = cal.getTime();
    		String current = dateFormat.format(today);
    		String thirty = dateFormat.format(today30);
    		endDate=current;
    		startDate=thirty;
    	}
        if (merchant != null) {
        	
            jsonOutput = orderService.findAllOrderFromDataTable(merchant.getId(), pageDisplayLength, pageNumber,
                            searchParameter, startDate, endDate);
        }
        LOGGER.info("===============  InventoryController : Inside findAllOrders :: End  ============= ");

        return jsonOutput;
    }

    @RequestMapping(value = "/findOrderDetailsById", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody String orderDetails(HttpServletRequest request, @RequestParam(required = true) Integer orderId)
                    throws IOException {
        return orderService.findOrderDetailsById(orderId);
    }

    @RequestMapping(value = "/searchOrderByText", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody String searchOrder(HttpServletRequest request, @RequestParam(required = true) String searchTxt)
                    throws IOException {
    	LOGGER.info("===============  InventoryController : Inside searchOrderByText :: Start  ============= ");

    	HttpSession session = request.getSession(false);
        Merchant merchant = (Merchant) session.getAttribute("merchant");
        LOGGER.info("===== InventoryController : Inside searchOrderByText :: merchant  == " + merchant.getId());

        Integer pageNumber = 0;
        Integer pageDisplayLength=0;
        if (null != request.getParameter("iDisplayStart") && null != request.getParameter("iDisplayLength")){
       	 pageDisplayLength = Integer.valueOf(request.getParameter("iDisplayLength"));
           pageNumber = (Integer.valueOf(request.getParameter("iDisplayStart")) / pageDisplayLength) + 1;
       }
        // Fetch search parameter
        String searchParameter = request.getParameter("sSearch");
       // Integer pageDisplayLength = Integer.valueOf(request.getParameter("iDisplayLength"));
        //String jsonOutput = "";
        String searchResult = "";
        if (merchant != null) {
            //jsonOutput = orderService.findAllOrderFromDataTable(merchant.getId(), pageDisplayLength, pageNumber,searchParameter);
            searchResult = orderService.searchOrderByText(merchant.getId(), searchTxt,pageDisplayLength,pageNumber);
        }
        /*HttpSession session = request.getSession(false);
        Merchant merchant = (Merchant) session.getAttribute("merchant");
        String searchResult = "";
        if (merchant != null) {
            searchResult = orderService.searchOrderByText(merchant.getId(), searchTxt);
        }*/
        LOGGER.info("===============  InventoryController : Inside searchOrderByText :: End  ============= ");

        return searchResult;
    }
    
    @RequestMapping(value = "/searchOrderByTextAndDate", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody String searchOrder(HttpServletRequest request, @RequestParam(required = false) String searchTxt,  @RequestParam(required = false) String startDate,
    		@RequestParam(required = false) String endDate)
                    throws IOException {
    	LOGGER.info("===============  InventoryController : Inside searchOrderByTextAndDate :: Start  ============= ");

    	HttpSession session = request.getSession(false);
        Merchant merchant = (Merchant) session.getAttribute("merchant");
        LOGGER.info("===== InventoryController : Inside searchOrderByTextAndDate :: merchant  == " + merchant.getId());

        Integer pageNumber = 0;
        Integer pageDisplayLength=0;
        if (null != request.getParameter("iDisplayStart") && null != request.getParameter("iDisplayLength")){
       	 pageDisplayLength = Integer.valueOf(request.getParameter("iDisplayLength"));
           pageNumber = (Integer.valueOf(request.getParameter("iDisplayStart")) / pageDisplayLength) + 1;
       }
        // Fetch search parameter
        String searchParameter = request.getParameter("sSearch");
       // Integer pageDisplayLength = Integer.valueOf(request.getParameter("iDisplayLength"));
        String searchResult = "";
        if (merchant != null) {
        	if(searchTxt!= ""){
        		 searchResult = orderService.searchOrderByTextAndDate(merchant.getId(), searchTxt,pageDisplayLength,pageNumber,startDate,endDate);
        	}else{
        		searchResult = orderService.findAllOrderFromDataTable(merchant.getId(), pageDisplayLength, pageNumber,searchParameter, startDate, endDate);
        	}
           
        }
        LOGGER.info("===============  InventoryController : Inside searchOrderByTextAndDate :: End  ============= ");

        return searchResult;
    }
    
    
    @RequestMapping(value = "/downloadCSV")
    public void downloadCSV(HttpServletResponse response, HttpServletRequest request) throws IOException {
    
    	LOGGER.info("===============  InventoryController : Inside downloadCSV :: Start  ============= ");

    	response.setContentType("text/csv");
		String reportName = "CSV_Report_Name.csv";
		response.setHeader("Content-disposition", "attachment;filename="+reportName);
 
		ArrayList<String> rows = new ArrayList<String>();
		rows.add("Name,Result");
		rows.add("\n");
 
		for (int i = 0; i < 10; i++) {
			rows.add("Java Honk,Success");
			rows.add("\n");
		}
 
		Iterator<String> iter = rows.iterator();
		while (iter.hasNext()) {
			String outputString = (String) iter.next();
			response.getOutputStream().print(outputString);
		}
		LOGGER.info("===============  InventoryController : Inside downloadCSV :: End  ============= ");

		response.getOutputStream().flush();
    }
    
    
    @RequestMapping(value = "/generateExcel")
    public void generateExcel(HttpServletRequest request, HttpServletResponse response, @RequestParam(required = false) String searchTxt,  @RequestParam(required = false) String startDate,
    		@RequestParam(required = false) String endDate)
                    throws IOException {
    	LOGGER.info("===============  InventoryController : Inside generateExcel :: Start  ============= ");

    	HttpSession session = request.getSession(false);
        Merchant merchant = (Merchant) session.getAttribute("merchant");
        LOGGER.info("===== InventoryController : Inside generateExcel :: merchant  == " + merchant.getId());

        //String searchResult = "";
        List<AllOrderVo> allOrderVo = new ArrayList<AllOrderVo>();
        if (merchant != null) {
        	allOrderVo = orderService.generateExcelForOrder(merchant.getId(),searchTxt,startDate,endDate);
        }
        
        if(!allOrderVo.isEmpty()){
        	
        response.setContentType("text/csv");
		String reportName = "orders_"+startDate+"_"+endDate+".csv";
		response.setHeader("Content-disposition", "attachment;filename="+reportName);
		
		ArrayList<String> rows = new ArrayList<String>();
		
		  rows.add("Customer Name,Created On,Order Total,Order Type,Order Notes, Customer Email,Customer Phone,Payment Type,OrderStatus,Order Item ,Order Item Price, Quantity");
		  rows.add("\n");

		  for(AllOrderVo allOrderVo2 : allOrderVo){
			if(allOrderVo2.getStatus().equals("Confirmed") ||  allOrderVo2.getStatus().equals("Pending")){
				List<OrderItem> orderItems=allOrderVo2.getOrderItems();
				List<OrderPizza> orderPizzas = allOrderVo2.getOrderPizza();
				
				if(orderItems!=null && orderItems.size()>0)
				{
					for(OrderItem orderItem:orderItems){
					  if(orderItem.getItem() !=null){
						 String data="";
							if(allOrderVo2!=null && allOrderVo2.getFirstName()!= null){
								data=data+allOrderVo2.getFirstName().toString();
							}else{
								data=data+" ";
							}
							
			                if(allOrderVo2!=null &&allOrderVo2.getCreatedOn()!= null){
			                	/*allOrderVo2.getCreatedOn().toString().replace(" ", "")*/
								data=data+","+allOrderVo2.getCreatedOn().toString().replace(" ", "_" );
							}else{
								data=data+","+" ";
							}
							if(allOrderVo2!=null &&allOrderVo2.getOrderPrice()!= null){
								data=data+","+allOrderVo2.getOrderPrice().toString();
							}else{
								data=data+","+" ";
							}
							if(allOrderVo2!=null && allOrderVo2.getOrderType()!= null){
								data=data+","+allOrderVo2.getOrderType().toString();
							}else{
								data=data+","+" ";
							}
							/*if(allOrderVo2.getStatus()!= null){
								data=data+","+allOrderVo2.getStatus().toString();
							}else{
								data=data+","+" ";
							}*/
							if(allOrderVo2!=null && allOrderVo2.getOrderName()!= null){
								
								if(allOrderVo2.getOrderName().contains(",")){
									data=data+","+allOrderVo2.getOrderName().replace(",", " ");
								}else{
									data=data+","+allOrderVo2.getOrderName();
								}
								
							}else{
								data=data+","+" ";
							}
							
							if(allOrderVo2!=null && allOrderVo2.getCustomerEmail()!= null){
								data=data+","+allOrderVo2.getCustomerEmail();
							}else{
								data=data+","+" ";
							}
							
							if(allOrderVo2!=null && allOrderVo2.getCustomerPhone()!= null){
								data=data+","+allOrderVo2.getCustomerPhone();
							}else{
								data=data+","+" ";
							}
							
							if(allOrderVo2!=null && allOrderVo2.getPaymentMethod()!= null){
								data=data+","+allOrderVo2.getPaymentMethod();
							}else{
								data=data+","+" ";
							}
							
							if(allOrderVo2!=null && allOrderVo2.getStatus()!=null){
								data = data+","+ allOrderVo2.getStatus();
							}else{
								data=data+","+" ";
							}
							
							if(orderItem!=null && orderItem.getItem()!=null){
								if(orderItem.getItem().getName()!=null){
									data=data+","+orderItem.getItem().getName();
								}else{
									data=data+","+" ";
								}
								if(orderItem.getItem().getPrice()!=null){
									data=data+","+orderItem.getItem().getPrice();
								}else{
									data=data+","+" ";
								}
								if(orderItem.getQuantity()!=null){
									data=data+","+orderItem.getQuantity();
								}else{
									data=data+","+" ";
								}
								allOrderVo2=null;
							}
							
							rows.add(data);
							rows.add("\n");
						}
					}
				}
				
				if(orderPizzas != null && orderPizzas.size() > 0){
					for (OrderPizza orderPizza : orderPizzas) {

					    String data="";
						if(allOrderVo2!=null && allOrderVo2.getFirstName()!= null){
							data=data+allOrderVo2.getFirstName().toString();
						}else{
							data=data+" ";
						}
						
		                if(allOrderVo2!=null &&allOrderVo2.getCreatedOn()!= null){
		                	/*allOrderVo2.getCreatedOn().toString().replace(" ", "")*/
							data=data+","+allOrderVo2.getCreatedOn().toString().replace(" ", "_" );
						}else{
							data=data+","+" ";
						}
						if(allOrderVo2!=null &&allOrderVo2.getOrderPrice()!= null){
							data=data+","+allOrderVo2.getOrderPrice().toString();
						}else{
							data=data+","+" ";
						}
						if(allOrderVo2!=null && allOrderVo2.getOrderType()!= null){
							data=data+","+allOrderVo2.getOrderType().toString();
						}else{
							data=data+","+" ";
						}
						/*if(allOrderVo2.getStatus()!= null){
							data=data+","+allOrderVo2.getStatus().toString();
						}else{
							data=data+","+" ";
						}*/
						if(allOrderVo2!=null && allOrderVo2.getOrderName()!= null){
							
							if(allOrderVo2.getOrderName().contains(",")){
								data=data+","+allOrderVo2.getOrderName().replace(",", " ");
							}else{
								data=data+","+allOrderVo2.getOrderName();
							}
							
						}else{
							data=data+","+" ";
						}
						
						if(allOrderVo2!=null && allOrderVo2.getCustomerEmail()!= null){
							data=data+","+allOrderVo2.getCustomerEmail();
						}else{
							data=data+","+" ";
						}
						
						if(allOrderVo2!=null && allOrderVo2.getCustomerPhone()!= null){
							data=data+","+allOrderVo2.getCustomerPhone();
						}else{
							data=data+","+" ";
						}
						
						if(allOrderVo2!=null && allOrderVo2.getPaymentMethod()!= null){
							data=data+","+allOrderVo2.getPaymentMethod();
						}else{
							data=data+","+" ";
						}
						
						if(allOrderVo2!=null && allOrderVo2.getStatus()!=null){
							data = data+","+ allOrderVo2.getStatus();
						}else{
							data=data+","+" ";
						}
						
						if(orderPizza!=null && orderPizza.getPizzaTemplate()!=null){
							if(orderPizza.getPizzaTemplate().getDescription()!=null){
								data=data+","+orderPizza.getPizzaTemplate().getDescription();
							}else{
								data=data+","+" ";
							}
							if(orderPizza.getPrice()!=null){
								data=data+","+orderPizza.getPrice();
							}else{
								data=data+","+" ";
							}
							if(orderPizza.getQuantity()!=null){
								data=data+","+orderPizza.getQuantity();
							}else{
								data=data+","+" ";
							}
							allOrderVo2=null;
						}
						rows.add(data);
						rows.add("\n");
						LOGGER.info(
								"===============  InventoryController : Inside generateExcel :: data == "+data);

					}
				}
			}
		}
		
		  /*Iterator<String> iter = rows.iterator();
		while (iter.hasNext()) {
			String outputString = (String) iter.next();
			response.getOutputStream().print(outputString);
			LOGGER.info("done");
		response.getOutputStream().flush();
		}*/
		  try{
				Iterator<String> iter = rows.iterator();
				while (iter.hasNext()) {
					
					String outputString = (String) iter.next();
					LOGGER.info(outputString);
					byte[] bytesRead = null;
					byte[] buffer = outputString.getBytes();
					LOGGER.info("String to byte array: " + Arrays.toString(buffer));
						response.getOutputStream().write(outputString.getBytes("UTF-8"));
					}
				LOGGER.info("done");
				response.getOutputStream().flush();
		  	}catch(Exception e){
		  		LOGGER.error(
						"===============  InventoryController : Inside generateExcel :: Exception  ============= " + e);

				LOGGER.error("error: " + e.getMessage());
			}
       	}else{
        	//HttpSession session = request.getSession();
        	session.setAttribute("errorMessage", "Do not generate the sheet due to order is empty");
        	LOGGER.info("===============  InventoryController : Inside generateExcel :: End  ============= ");

        	response.sendRedirect(environment.getProperty("BASE_URL") + "/allOrders");
        }
    }
    
    @Autowired
    PizzaService pizzaService;
    
    @RequestMapping(value = "/pizzaTamplateDataUsingAjax", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody String pizzaTamplatePaginationDataTables(HttpServletRequest request) throws IOException {
       LOGGER.info("===============  InventoryController : Inside pizzaTamplatePaginationDataTables :: Start  ============= ");

    	HttpSession session = request.getSession(false);
        Merchant merchant = (Merchant) session.getAttribute("merchant");
        LOGGER.info("===== InventoryController : Inside pizzaTamplatePaginationDataTables :: merchant  == " + merchant.getId());

        Integer pageNumber = 0;
        Integer pageDisplayLength=0;
        if (null != request.getParameter("iDisplayStart") && null != request.getParameter("iDisplayLength")){
       	 pageDisplayLength = Integer.valueOf(request.getParameter("iDisplayLength"));
           pageNumber = (Integer.valueOf(request.getParameter("iDisplayStart")) / pageDisplayLength) + 1;
       }
        // Fetch search parameter
        String searchParameter = request.getParameter("sSearch");
       // Integer pageDisplayLength = Integer.valueOf(request.getParameter("iDisplayLength"));
        // Create page list data
        String jsonOutput = "";
        if (merchant != null) {
            jsonOutput = pizzaService.findPizzaTamplateByMerchantId(merchant.getId(), pageDisplayLength, pageNumber,
                            searchParameter);
        }
        LOGGER.info("===============  InventoryController : Inside pizzaTamplatePaginationDataTables :: End  ============= ");

        return jsonOutput;

    }

    @RequestMapping(value = "/pizzaToppingDataUsingAjax", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody String pizzaToppingPaginationDataTables(HttpServletRequest request) throws IOException {
        LOGGER.info("===============  InventoryController : Inside pizzaToppingPaginationDataTables :: Start  ============= ");

    	HttpSession session = request.getSession(false);
        Merchant merchant = (Merchant) session.getAttribute("merchant");
        LOGGER.info("===== InventoryController : Inside pizzaToppingPaginationDataTables :: merchant  == " + merchant.getId());

        Integer pageNumber = 0;
        Integer pageDisplayLength=0;
        if (null != request.getParameter("iDisplayStart") && null != request.getParameter("iDisplayLength")){
       	 pageDisplayLength = Integer.valueOf(request.getParameter("iDisplayLength"));
           pageNumber = (Integer.valueOf(request.getParameter("iDisplayStart")) / pageDisplayLength) + 1;
       }
        // Fetch search parameter
        String searchParameter = request.getParameter("sSearch");
       // Integer pageDisplayLength = Integer.valueOf(request.getParameter("iDisplayLength"));
        // Create page list data
        String jsonOutput = "";
        if (merchant != null) {
            jsonOutput = pizzaService.findPizzaToppingByMerchantId(merchant.getId(), pageDisplayLength, pageNumber,
                            searchParameter);
        }
        LOGGER.info("===============  InventoryController : Inside pizzaToppingPaginationDataTables :: End  ============= ");

        return jsonOutput;

    }
    
    
    @RequestMapping(value = "/filterTamplateByPizzaSize", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody String filterTamplateByPizzaSize(HttpServletRequest request,
                    @RequestParam(required = false) int pizzaSizeId) throws IOException {
       
    	LOGGER.info("===============  InventoryController : Inside filterTamplateByPizzaSize :: Start  ============= ");

    	HttpSession session = request.getSession(false);
        Merchant merchant = (Merchant) session.getAttribute("merchant");
        LOGGER.info("===== InventoryController : Inside filterTamplateByPizzaSize :: merchant  == " + merchant.getId());

        String jsonOutput = "";
        if (merchant != null) {
            jsonOutput = pizzaService.filterTamplateBypizzaSizeId(merchant.getId(), pizzaSizeId);
            if(jsonOutput!=null &&jsonOutput.contains("id")){
            	session.setAttribute("pizzaShowStatus", "success");
            }else{
            	session.setAttribute("pizzaShowStatus", "failed");
            }
        }
        LOGGER.info("===============  InventoryController : Inside filterTamplateByPizzaSize :: End  ============= ");

        return jsonOutput;
    }
    
    @RequestMapping(value = "/filterToppingByPizzaSize", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody String filterToppingByPizzaSize(HttpServletRequest request,
                    @RequestParam(required = false) int pizzaSizeId) throws IOException {
        LOGGER.info("===============  InventoryController : Inside filterToppingByPizzaSize :: Start  ============= ");

    	HttpSession session = request.getSession(false);
        Merchant merchant = (Merchant) session.getAttribute("merchant");
        LOGGER.info("===== InventoryController : Inside filterToppingByPizzaSize :: merchant  == " + merchant.getId());

        String jsonOutput = "";
        if (merchant != null) {
            jsonOutput = pizzaService.filterToppingBypizzaSizeId(merchant.getId(), pizzaSizeId);
        }
        LOGGER.info("===============  InventoryController : Inside filterToppingByPizzaSize :: End  ============= ");

        return jsonOutput;
    }
    
    @RequestMapping(value = "/searchTemplateByText", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody String searchTemplates(HttpServletRequest request, @RequestParam(required = true) String searchTxt)
                    throws IOException {
       LOGGER.info("===============  InventoryController : Inside searchTemplates :: Start  ============= ");

    	HttpSession session = request.getSession(false);
        Merchant merchant = (Merchant) session.getAttribute("merchant");
        LOGGER.info("===== InventoryController : Inside searchTemplates :: merchant  == " + merchant.getId());

        String searchResult = "";
        if (merchant != null) {
            searchResult = pizzaService.searchTemplateByTxt(merchant.getId(), searchTxt);
        }
        LOGGER.info("===============  InventoryController : Inside searchTemplates :: End  ============= ");

        return searchResult;
    }
    
    @RequestMapping(value = "/searchToppingByText", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody String searchToppings(HttpServletRequest request, @RequestParam(required = true) String searchTxt)
                    throws IOException {
        LOGGER.info("===============  InventoryController : Inside searchToppings :: Start  ============= ");

    	HttpSession session = request.getSession(false);
        Merchant merchant = (Merchant) session.getAttribute("merchant");
        LOGGER.info("===== InventoryController : Inside searchToppings :: merchant  == " + merchant.getId());

        String searchResult = "";
        if (merchant != null) {
            searchResult = pizzaService.searchToppingsByTxt(merchant.getId(), searchTxt);
        }
        LOGGER.info("===============  InventoryController : Inside searchToppings :: End  ============= ");

        return searchResult;
    }
    
    @RequestMapping(value = "/updateTamplateStatusById", method = RequestMethod.GET)
    @ResponseBody
    public String updateTamplateStatusById(@RequestParam(required = true) Integer tamplateId,
                    @RequestParam(required = true) Integer itemStatus) {
        try {
        	LOGGER.info("===============  InventoryController : Inside updateTamplateStatusById :: Start  ============= ");

            PizzaTemplate template = new PizzaTemplate();
            template.setId(tamplateId);
            template.setActive(itemStatus);
            pizzaService.updateTamplateStatusById(template);
        } catch (Exception e) {
            if (e != null) {
            	LOGGER.error(
						"===============  InventoryController : Inside updateTamplateStatusById :: Exception  ============= " + e);

                MailSendUtil.sendExceptionByMail(e,environment);
                LOGGER.error("error: " + e.getMessage());
                return "redirect:https://www.foodkonnekt.com";
            }
        }
        LOGGER.info("===============  InventoryController : Inside updateTamplateStatusById :: End  ============= ");

        return "succuss";
    }
    
    
    @RequestMapping(value = "/updateToppingStatusById", method = RequestMethod.GET)
    @ResponseBody
    public String updateToppingStatusById(@RequestParam(required = true) Integer toppingId,
                    @RequestParam(required = true) Integer itemStatus) {
        try {
        	LOGGER.info("===============  InventoryController : Inside updateToppingStatusById :: Start  ============= ");

            PizzaTopping topping = new PizzaTopping();
            topping.setId(toppingId);
            topping.setActive(itemStatus);
            pizzaService.updateToppingStatusById(topping);
        } catch (Exception e) {
            if (e != null) {
            	LOGGER.error(
						"===============  InventoryController : Inside updateToppingStatusById :: Exception  ============= " + e);

                MailSendUtil.sendExceptionByMail(e,environment);
                LOGGER.error("error: " + e.getMessage());
                return "redirect:https://www.foodkonnekt.com";
            }
        }
        LOGGER.info("===============  InventoryController : Inside updateToppingStatusById :: End  ============= ");

        return "succuss";
    }
    
    @RequestMapping(value="/cancelOrder", method = RequestMethod.GET)
    @ResponseBody
    public String cancelOrder(@RequestParam("orderId")String orderId){
    	LOGGER.info("===============  InventoryController : Inside cancelOrder :: Start  ============= ");

    	String status = "";
    	OrderR order = orderService.findOrderByOrderID(orderId);
    	if(order!=null){
    		 status = orderService.setOrderStatus(order.getId(), "decline", "","");
    	}
    	LOGGER.info("===============  InventoryController : Inside cancelOrder :: End  ============= ");

    	return status;
    }	
    
    @RequestMapping(value="/updateOrderDeliveryTime", method = RequestMethod.GET)
    @ResponseBody
    public Boolean updateOrderDeliveryTime(@RequestParam("orderId")String orderId, @RequestParam("deliveryTime")String deliveryTime) throws Exception{
    		LOGGER.info("===============  InventoryController : Inside updateOrderDeliveryTime :: Start  ============= ");

    	return orderService.updateDeliveryTime(orderId, deliveryTime);
    }
    
    
    
    @RequestMapping(value= "/generateExcelForKidsCaters")
    public void generateExcelForKidsCaters(HttpServletRequest request, HttpServletResponse response, @RequestParam(required = false) String searchTxt,  @RequestParam(required = false) String startDate,
    		@RequestParam(required = false) String endDate)
                    throws IOException {
    	LOGGER.info("===============  InventoryController : Inside generateExcelForKidsCaters :: Start  ============= ");

    	HttpSession session = request.getSession(false);
        Merchant merchant = (Merchant) session.getAttribute("merchant");
        
        response.setContentType("text/csv");
		String reportName = "orders_"+startDate+"_"+endDate+".csv";
		response.setHeader("Content-disposition", "attachment;filename="+reportName);
        ArrayList<String> rows = new ArrayList<String>();
        
        List<Date> dates = new ArrayList<Date>();
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Modifiers modifiers=null;
        Date parseStartDate;
        Date  parseEndDate ;
		try {
			parseStartDate = (Date)formatter.parse(startDate);
			parseEndDate = (Date)formatter.parse(endDate);
			
			long interval = 24*1000 * 60 * 60; // 1 hour in millis
			long endTime =parseEndDate.getTime() ; // create your endtime here, possibly using Calendar or Date
			long curTime = parseStartDate.getTime();
			while (curTime <= endTime) {
			    dates.add(new Date(curTime));
			    curTime += interval;
			}
			
			rows.add("Customer Name,Created On,Order Total,Order Type,Order Notes, Customer Email,Customer Phone,Payment Type, Day/Date , Item Option ,  Order Item , Quantity");
			rows.add("\n");
			
			List<Item> item = itemService.findByMerchantId(merchant.getId());
			
			for(int i = 0; i < dates.size();i++){
			    Date lDate = (Date)dates.get(i);
			    
			    for (Item dbItems : item) {
			    	boolean newRow=true;
			    	String itemName = dbItems.getName();
			    	
			    	String itemDate = null;
			    	
			    	String [] splittedItems = itemName.split("_");
			    	if(splittedItems!=null && splittedItems.length>1 ){
			    		itemDate = splittedItems[1];
			    	}
 			    	
			    	if(itemDate!=null){
					Date itemDateFormat = new SimpleDateFormat("MM-dd-yyyy").parse(itemDate);
					String itemParseDateString =  new SimpleDateFormat("yyyy-MM-dd").format(itemDateFormat);
					Date itemParseDate =  new SimpleDateFormat("yyyy-MM-dd").parse(itemParseDateString);
					
					
					String currentDateNew = new SimpleDateFormat("yyyy-MM-dd").format(lDate);
					
					
					 SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				        Date date1 = sdf.parse(itemParseDateString);
				        Date date2 = sdf.parse(currentDateNew);
					
						
 					if(date1.compareTo(date2)==0){
 						LOGGER.info("date matched" + lDate + "iemDate" +itemParseDate);
 			    		List<OrderItem> orderItems = orderItemRepository.findByItemId(dbItems.getId());
 			    		if(orderItems !=null && orderItems.size()>0){
 			    			for (OrderItem orderItem : orderItems) {
 			    				 String data="";
 			    				if(orderItem.getOrder()!=null && orderItem.getOrder().getId()!=null){
 			    					
 			    					OrderR orderR= orderService.findById(orderItem.getOrder().getId());
 			    					
 			    					if(orderR.getIsDefaults()==0 || orderR.getIsDefaults()==1){
 			    						if(orderR.getCustomer()!=null && orderR.getCustomer().getId()!=null && orderR.getMerchant()!=null && 
 	 											orderR.getMerchant().getId()!=null){
 			    							LOGGER.info(
													"===============  InventoryController : Inside generateExcelForKidsCaters :: orderItem == "+orderItem.getId());

 	 										List<OrderItemModifier> orderItemModifiers = orderItemModifierRepository
 	 	 											.findByOrderItemId(orderItem.getId());
 	 	 			    					
 	 	 			    					if (orderItemModifiers != null && !orderItemModifiers.isEmpty()) {
 	 	 			    					
 	 	 			    						for (OrderItemModifier orderItemModifier : orderItemModifiers) {
 	 	 			    							if (orderItemModifier.getModifiers() != null
 	 	 													&& orderItemModifier.getModifiers().getId() != null) {
 	 	 			    							
 	 	 			    								if(orderR!=null){

 	 	 			    									if (newRow && orderR.getCustomer() != null && orderR
																	.getCustomer().getFirstName() != null && orderR
																	.getCustomer().getLastName() != null) {
																data = data + (orderR.getCustomer().getFirstName()
																		.toString().concat(" ").concat(orderR.getCustomer().getLastName().toString()));
															}else if(newRow && orderR.getCustomer() != null && orderR
																	.getCustomer().getFirstName() != null) {
																data = data + (orderR.getCustomer().getFirstName().toString());
																		
															} else {
																data = data + " ";
															}
 	 			   									
 	 			   									if (newRow && orderR.getCreatedOn() != null) {
 	 			   									data=data+","+(orderR.getCreatedOn().toString().replace(" ", "_"));
 	 			   									} else {
 	 			   										data=data+","+" ";
 	 			   									}

 	 			   									if (newRow && orderR.getOrderPrice() != null) {
 	 			   									data=data+","+(orderR.getOrderPrice().toString());
 	 			   									} else {
 	 			   									data=data+","+" ";
 	 			   									}
 	 			   									
 	 			   									if (newRow && orderR.getOrderType() != null) {
 	 			   									data=data+","+(orderR.getOrderType().toString());
 	 			   									} else {
 	 			   									data=data+","+" ";
 	 			   									}

 	 			   									if (newRow && orderR.getOrderNote() != null) {
 	 			   									//data=data+","+(orderR.getOrderNote().replace(",", " ").replace("'", ""));
 	 			   									
 	 			   								if(orderR.getOrderNote().contains(",")){
 	 			   									data=data+","+orderR.getOrderNote().replace(",", " ");
 	 			   								}else{
 	 			   									data=data+","+orderR.getOrderNote();
 	 			   								}
 	 			   									} else {
 	 			   									data=data+","+" ";
 	 			   									}
 	 			   									
 	 			   								

 	 			   									if (newRow && orderR.getCustomer() != null
 	 			   											&& orderR.getCustomer().getEmailId() != null) {
 	 			   									data=data+","+(orderR.getCustomer().getEmailId());
 	 			   									} else {
 	 			   									data=data+","+" ";
 	 			   									}

 	 			   									if (newRow && orderR.getCustomer() != null
 	 			   											&& orderR.getCustomer().getPhoneNumber() != null) {
 	 			   									data=data+","+(orderR.getCustomer().getPhoneNumber());
 	 			   									} else {
 	 			   									data=data+","+" ";
 	 			   									}

 	 	 			   									if (newRow && orderR.getPaymentMethod() != null) {
 	 	 			   									data=data+","+(orderR.getPaymentMethod());
 	 	 			   									} else {
 	 	 			   									data=data+","+" ";
 	 	 			   									}
 	 	 			   									
 	 	 			   									if (item != null && dbItems.getName() != null) {
 	 	 			   									data=data+","+(dbItems.getName());
 	 	 			   									} else {
 	 	 			   									data=data+","+" ";
 	 	 			   									}
 	 	 			   								
 	 	 			    								}
 	 	 			    							}
 	 	 			    							LOGGER.info(
 	 														"===============  InventoryController : Inside generateExcelForKidsCaters :: orderItemModifier == "+orderItemModifier.getModifiers().getId());

 	 	 			    							modifiers = modifierRepository.findOne(orderItemModifier.getModifiers().getId());
 	 	 			    							List<ModifierModifierGroupDto> modifierModifierGroupDtos = modifierModifierGroupRepository.
 	 	 													findByModifiersId(modifiers.getId());
 	 	 			    							for (ModifierModifierGroupDto modifierModifierGroupDto : modifierModifierGroupDtos) {
 	 	 			    								
 	 	 			    								if(modifierModifierGroupDto.getModifierGroup()!=null &&
 	 	 														modifierModifierGroupDto.getModifierGroup().getId() !=null){
 	 	 			    									ModifierGroup modifierGroup=modifierGroupRepository.findOne
 	 	 															(modifierModifierGroupDto.getModifierGroup().getId());
 	 	 			    									if(modifierGroup!=null && modifierGroup.getName()!=null){
 	 	 			    										
 	 	 			    										String [] groupName = modifierGroup.getName().split("_");
 	 	 			    										
 	 	 			    										String modifierGroupName = null;
 	 	 			    										if(groupName!=null && groupName.length> 1) {
 	 	 			    											modifierGroupName = groupName[1];
 	 	 			    										 }
 	 	 			    										
 	 	 			    										Date modifierGroupDateFormat = new SimpleDateFormat("MM-dd-yyyy").parse(modifierGroupName);
 	 	 			    										String groupParseDateString =  new SimpleDateFormat("yyyy-MM-dd").format(modifierGroupDateFormat);
 	 	 			    										Date groupParseDate =  new SimpleDateFormat("yyyy-MM-dd").parse(groupParseDateString);
 	 	 			    										
 	 	 			    									  Date date3 = sdf.parse(groupParseDateString);
 	 	 			    										
 	 	 			    										
 	 	 			    										if(date3.compareTo(date2)==0){
 	 	 			    											data=data+","+(modifierGroup.getName());
 	 	 			    										}
 	 	 													}else{
 	 	 														data=data+","+" ";
 	 	 													}
 	 	 			    								}
 	 	 			    							}
 	 	 			    							if(modifiers.getName()!=null){
 	 	 			    								data=data+","+(modifiers.getName());
 	 	 											}else{
 	 	 												data=data+","+" ";
 	 	 											}
 	 	 			    							
 	 	 			    							if (orderItemModifier.getQuantity() != null) {
 	 	 			    								data=data+","+(orderItemModifier.getQuantity().toString());
 	 	 											} else {
 	 	 												data=data+","+" ";
 	 	 											}
 	 	 			    							
 	 	 			    							newRow = false;
 	 	 			    							rows.add(data);
 	 	 			    							 data="";
 	 	 			    							rows.add("\n");
 	 	 			    						}
 	 	 			    						newRow = true;
 	 	 			    						rows.add(data);
 	 	 		 			    				rows.add("\n");
 	 	 		 			    			LOGGER.info(
													"===============  InventoryController : Inside generateExcelForKidsCaters :: data  == "+data);

 	 	 			    					}
 	 									}
 			    					}
 			    				}
 			    			}
 			    		}
 			    	}
			    }
			}
		}
			 Iterator<String> iter = rows.iterator();
				while (iter.hasNext()) {
					String outputString = (String) iter.next();
					response.getOutputStream().print(outputString);
				}
				LOGGER.info("downloaded successfully");
				response.getOutputStream().flush();
			
		} catch (ParseException e) {
			LOGGER.error("===============  InventoryController : Inside generateExcelForKidsCaters :: Exception  ============= " + e);

			LOGGER.error("error: " + e.getMessage());
		} 
    }
    @RequestMapping(value = "/downloadCSVFile")
	public void downloadCSVFile(HttpServletResponse response, HttpServletRequest request) throws IOException {

		try {
			LOGGER.info("-----------------InventoryController : Inside downloadCSVFile :: Start---------------------");
			LOGGER.info("Download CSV File from KidsCateringScheduler schedular runs");
			String toDay = DateUtil.getCurrentDay();
			String toDayAndDate = toDay.substring(0, 1).toUpperCase() + toDay.substring(1).toLowerCase() + "_"
					+ DateUtil.findCurrentDate();

			if (toDayAndDate != null) {
				LOGGER.info("InventoryController : Inside downloadCSVFile :: toDayAndDate: "+toDayAndDate);
				String outputFile = null;
				try {
					String emailId = null;
					String vendorEmail = null;
					//ResponseBuilder response = null;
					
					OutputStream servletOutputStream = response.getOutputStream(); // retrieve OutputStream from HttpServletResponse
					ZipOutputStream zos = new ZipOutputStream(servletOutputStream); // create a ZipOutputStream from servletOutputStream
					//ByteArrayOutputStream baos = new ByteArrayOutputStream();
					
					List<Item> items = itemmRepository.findByName(toDayAndDate);
					if (items != null && items.size() > 0) {
						LOGGER.info("InventoryController : Inside downloadCSVFile :: items.size(): "+items.size());
						for (Item item : items) {
							boolean newRow = true;
							outputFile = environment.getProperty("FILEPATH") + item.getMerchant().getName() + "_report.csv";
							LOGGER.info("InventoryController : Inside downloadCSVFile :: outputFile: "+outputFile);
							boolean alreadyExists = new File(outputFile).exists();
							
							CsvWriter csvOutput = new CsvWriter(new FileWriter(outputFile, true), ',');
							
							if (!alreadyExists) {
								csvOutput.write("Customer Name");
								csvOutput.write("Created On");
								csvOutput.write("Order Total");
								csvOutput.write("Order Type");
								csvOutput.write("Order Notes");
								csvOutput.write("Customer Email");
								csvOutput.write("Customer Phone");
								csvOutput.write("Payment Type");
								csvOutput.write("Day/Date");
								csvOutput.write("Item Option");
								csvOutput.write("Order Item");
								csvOutput.write("Quantity");

								csvOutput.endRecord();
							}

							Modifiers modifiers = null;

							Merchant merchant = null;
							if (item.getMerchant() != null) {
								merchant = item.getMerchant();
							}
LOGGER.info("===============  InventoryController : Inside downloadCSVFile :: item  == "+item.getId());

							List<OrderItem> orderItems = orderItemRepository.findByItemId(item.getId());
							if (orderItems != null && orderItems.size() > 0) {
								LOGGER.info("InventoryController : Inside downloadCSVFile :: orderItems.size(): "+orderItems.size());
								for (OrderItem orderItem : orderItems) {
									if (orderItem.getOrder() != null && orderItem.getOrder().getId() != null) {
										LOGGER.info("InventoryController : Inside downloadCSVFile :: orderId: "+orderItem.getOrder().getId());
										OrderR orderR = orderRepository.findOne(orderItem.getOrder().getId());

										if (orderR.getCustomer() != null && orderR.getCustomer().getId() != null
												&& orderR.getMerchant() != null && orderR.getMerchant().getId() != null
												&& (orderR.getIsDefaults() == 1 || orderR.getIsDefaults() == 0)) {
											List<OrderItemModifier> orderItemModifiers = orderItemModifierRepository
													.findByOrderItemId(orderItem.getId());

											if (orderItemModifiers != null && !orderItemModifiers.isEmpty()) {
												LOGGER.info("InventoryController : Inside downloadCSVFile :: orderItemModifiers.size(): "+orderItemModifiers.size());
												for (OrderItemModifier orderItemModifier : orderItemModifiers) {
													if (orderItemModifier.getModifiers() != null
															&& orderItemModifier.getModifiers().getId() != null) {

														if (orderR != null) {	
															if (newRow && orderR.getCustomer() != null
																	&& orderR.getCustomer().getFirstName() != null
																	&& orderR.getCustomer().getLastName() != null
																	&& orderR.getMerchant() != null) {
																csvOutput.write(orderR.getCustomer().getFirstName().toString().concat(" ").concat(orderR.getCustomer().getLastName().toString()));
															}else if (newRow && orderR.getCustomer() != null
																	&& orderR.getCustomer().getFirstName() != null) {
																csvOutput.write(orderR.getCustomer().getFirstName().toString());
																LOGGER.info("InventoryController : Inside downloadCSVFile :: customer Name: "+orderR.getCustomer().getFirstName());
															} else {
																csvOutput.write(" ");
															}

															if (newRow && orderR.getCreatedOn() != null) {
																csvOutput.write(
																		orderR.getCreatedOn().toString().replace(" ", "_"));
																LOGGER.info("InventoryController : Inside downloadCSVFile :: CreatedOn: "+orderR.getCreatedOn());
															} else {
																csvOutput.write(" ");
															}

															if (newRow && orderR.getOrderPrice() != null) {
																csvOutput.write(orderR.getOrderPrice().toString());
															} else {
																csvOutput.write(" ");
															}

															if (newRow && orderR.getOrderType() != null) {
																csvOutput.write(orderR.getOrderType().toString());
															} else {
																csvOutput.write(" ");
															}

															if (newRow && orderR.getOrderNote() != null) {
																csvOutput.write(orderR.getOrderNote());
															} else {
																csvOutput.write(" ");
															}

															if (newRow && orderR.getCustomer() != null
																	&& orderR.getCustomer().getEmailId() != null) {
																csvOutput.write(orderR.getCustomer().getEmailId());
															} else {
																csvOutput.write(" ");
															}

															if (newRow && orderR.getCustomer() != null
																	&& orderR.getCustomer().getPhoneNumber() != null) {
																csvOutput.write(orderR.getCustomer().getPhoneNumber());
															} else {
																csvOutput.write(" ");
															}

															if (newRow && orderR.getPaymentMethod() != null) {
																csvOutput.write(orderR.getPaymentMethod());
															} else {
																csvOutput.write(" ");
															}

															if (item != null && item.getName() != null) {
																csvOutput.write(item.getName());
															} else {
																csvOutput.write(" ");
															}
														}
													}
LOGGER.info("===============  InventoryController : Inside downloadCSVFile :: OrderItemModifier  == "+orderItemModifier.getModifiers().getId());

													modifiers = modifiersRepository
															.findOne(orderItemModifier.getModifiers().getId());
													LOGGER.info("===============  InventoryController : Inside downloadCSVFile :: modifiers  == "+modifiers.getId());

													List<ModifierModifierGroupDto> modifierModifierGroupDtos = modifierModifierGroupRepository
															.findByModifiersId(modifiers.getId());

													for (ModifierModifierGroupDto modifierModifierGroupDto : modifierModifierGroupDtos) {
														if (modifierModifierGroupDto.getModifierGroup() != null
																&& modifierModifierGroupDto.getModifierGroup()
																		.getId() != null) {
															LOGGER.info("===============  InventoryController : Inside downloadCSVFile :: modifierGroup  == "+modifierModifierGroupDto.getModifierGroup().getId());

															ModifierGroup modifierGroup = modifierGroupRepository.findOne(
																	modifierModifierGroupDto.getModifierGroup().getId());
															if (modifierGroup != null && modifierGroup.getName() != null) {

																String[] groupName = modifierGroup.getName().split("_");

																String[] toDayAndDateSplit = toDayAndDate.split("_");

																String parseTodayDate = toDayAndDateSplit[1];

																String modifierGroupName = null;
																if (groupName != null && groupName.length > 1) {
																	modifierGroupName = groupName[1];
																}
																if (modifierGroupName.equals(parseTodayDate)) {
																	csvOutput.write(modifierGroup.getName());
																}
															} else {
																csvOutput.write(" ");
															}
														}
													}

													if (modifiers.getName() != null) {
														csvOutput.write(modifiers.getName());
													} else {
														csvOutput.write(" ");
													}

													if (orderItemModifier.getQuantity() != null) {
														csvOutput.write(orderItemModifier.getQuantity().toString());
													} else {
														csvOutput.write(" ");
													}
													newRow = false;
													csvOutput.write(" ");
													csvOutput.endRecord();
												}
											}
											newRow = true;
											csvOutput.write(" ");
											csvOutput.endRecord();
											
										}
									}
								}
								csvOutput.close();
								String[] arrOfStr = environment.getProperty("FILEPATH").split("/", 1);
							     
								ZipEntry entry = new ZipEntry(environment.getProperty("FILEPATH") + item.getMerchant().getName() + "_report.csv"); 
								LOGGER.info("InventoryController : Inside downloadCSVFile :: ZipEntry: "+environment.getProperty("FILEPATH") + item.getMerchant().getName() + "_report.csv");
								
								Scanner scanner = new Scanner(new File(outputFile));
						        scanner.useDelimiter(",");
						        while (scanner.hasNext())
						        {
						            zos.write((scanner.next() + ",").getBytes());
						        }
							        scanner.close();
									zos.closeEntry();
									response.setContentType("application/zip");
									response.addHeader("Content-Disposition", "attachment; filename=Test.zip");
								
							} else {
								LOGGER.info("InventoryController : Inside downloadCSVFile :: No Orders found: ");
								LOGGER.info("No Orders found");
							}
						}
						
						 zos.close();
					} else {
						LOGGER.info("InventoryController : Inside downloadCSVFile :: No Orders found: ");
						LOGGER.info("No item found");
					}
				} catch (Throwable e) {
					LOGGER.error("error: " + e.getMessage());
					LOGGER.info("InventoryController : Inside downloadCSVFile :: Exception: "+e);
					// MailSendUtil.webhookMail("DaySchedular Error", "Error"+e);
				}
			}
		} catch (Throwable t) {
			t.printStackTrace();
			// MailSendUtil.webhookMail("DaySchedular Error", "Error"+t);
		}
		
}
    @RequestMapping(value = "/filterPizzaTemplateIdAndPizzaCrustId", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String filterTamplateByPizzaSize(HttpServletRequest request) throws IOException {
		HttpSession session = request.getSession(false);
		Merchant merchant = (Merchant) session.getAttribute("merchant");
		String jsonOutput = "";
		if (merchant != null) {
			jsonOutput = pizzaService.filterPizzaTemplateIdAndPizzaCrustId(merchant.getId());
			if (jsonOutput != null && jsonOutput.contains("id")) {
				session.setAttribute("pizzaShowStatus", "success");
			} else {
				session.setAttribute("pizzaShowStatus", "failed");
			}
		}
		return jsonOutput;
	}
    
    @RequestMapping(value = "/updateCrustStatusById", method = RequestMethod.GET)
	@ResponseBody
	public String updateCrustStatusById(@RequestParam(required = true) Integer crstId,
			@RequestParam(required = true) Integer crstStatus) {
		try {
			LOGGER.info("-----------------InventoryController : Inside updateCrustStatusById :: Start---------------------");
			LOGGER.info("==== crstId ====="+crstId);
			LOGGER.info("==== crstStatus ====="+crstStatus);
			PizzaCrust crust = new PizzaCrust();
			crust.setId(crstId);
			crust.setActive(crstStatus);
			pizzaService.updateCrustStatusById(crust);
		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
				LOGGER.error("error: " + e.getMessage());
				LOGGER.info("-----------------InventoryController : Inside updateCrustStatusById :: Exception----------"+e);

				return "redirect:" + environment.getProperty("BASE_URL") + "/support";
			}
		}
		LOGGER.info("-----------------InventoryController : Inside updateCrustStatusById :: End---------------------");

		return "succuss";
	}
    
    @RequestMapping(value = "/deleteDuplicateModifiersByMerchantId", method = RequestMethod.GET)
	@ResponseBody
	public String deleteDuplicateModifiersByMerchantId(@RequestParam(value="merchantId",required = false) Integer merchanId) {
    	
    	String response = "";
		try {
			LOGGER.info("-----------------InventoryController : Inside deleteDuplicateModifiers :: Start---------------------");
			LOGGER.info("==== merchantId ====="+merchanId);
			if(merchanId!=null) {
			List<Modifiers> modifiersList = modifierRepository.findDuplicatePosModifiers(merchanId);
			for (Modifiers modifier : modifiersList) {
				List<Modifiers> modifierList = modifierRepository.findListByPosModifierIdAndMerchantId(modifier.getPosModifierId(), merchanId);
				for(int i=1;i<modifierList.size();i++) {
				LOGGER.info("modifierId : "+modifierList.get(i).getId());
				itemModifierRepository.deleteDuplicateItemModifiersMapping(modifierList.get(i).getId());
				modifierModifierGroupRepository.deleteDuplicateModifierGroupModifiersMapping(modifierList.get(i).getId());
				modifierRepository.deleteDuplicatePosModifierId(modifierList.get(i).getId());
				response = response+" ,"+modifierList.get(i).getId();
				}
			}
		}
			else {
				List<Merchant> merchants = merchantRepository.findMerchantByIsInstall();
				for(Merchant merchant : merchants) {
				LOGGER.info("==== merchantId ====="+merchanId);
				List<Modifiers> modifiersList = modifierRepository.findDuplicatePosModifiers(merchant.getId());
				for (Modifiers modifier : modifiersList) {
					List<Modifiers> modifierList = modifierRepository.findListByPosModifierIdAndMerchantId(modifier.getPosModifierId(), merchant.getId());
					for(int i=1;i<modifierList.size();i++) {
					LOGGER.info("modifierId : "+modifierList.get(i).getId());
					itemModifierRepository.deleteDuplicateItemModifiersMapping(modifierList.get(i).getId());
					modifierModifierGroupRepository.deleteDuplicateModifierGroupModifiersMapping(modifierList.get(i).getId());
					modifierRepository.deleteDuplicatePosModifierId(modifierList.get(i).getId());
					response = response+" ,"+modifierList.get(i).getId();
					}
				}
				
				}
			}
			response = "Deleted modifiers  : "+response;
		
			
			} catch (Exception e) {
			response = "Exception While deleting modifiers : "+e;
		}
		LOGGER.info("-----------------InventoryController : Inside deleteDuplicateModifiers :: End---------------------");

		return response;
	}
    
}
