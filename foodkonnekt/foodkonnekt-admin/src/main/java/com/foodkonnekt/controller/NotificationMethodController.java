package com.foodkonnekt.controller;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.foodkonnekt.model.Merchant;
import com.foodkonnekt.model.MerchantConfiguration;
import com.foodkonnekt.model.NotificationMethod;
import com.foodkonnekt.repository.MerchantConfigurationRepository;
import com.foodkonnekt.service.MerchantConfigurationService;
import com.foodkonnekt.service.MerchantService;
import com.foodkonnekt.service.NotificationMethodService;
import com.foodkonnekt.util.IConstant;
import com.foodkonnekt.util.MailSendUtil;
import com.foodkonnekt.util.ProducerUtil;
import com.google.gson.Gson;

@Controller
public class NotificationMethodController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(NotificationMethodController.class);
	
	@Autowired
    private Environment environment;
	
	@Autowired
	private NotificationMethodService notificationMethodService;
	
	@Autowired
	private MerchantConfigurationService merchantConfigurationService;
	
	@Autowired
	private MerchantConfigurationRepository merchantConfigurationRepository;
	
	@Autowired
    private MerchantService merchantService;
	
	
	@RequestMapping(value = "/notificationMethod", method = RequestMethod.GET)
	public String notificationMethodLink(ModelMap model,HttpServletRequest request) {
		LOGGER.info("NotificationMethodController : Inside notificationMethodLink :: Start");
		HttpSession session = request.getSession(false);
		try{
		if (session != null) {
			Merchant merchant = (Merchant) session.getAttribute("merchant");
			Boolean status=false;
			Boolean autoAcceptFutureOrder=false;
			Boolean autoAcceptOrder = false;
			if (merchant != null) {
				LOGGER.info("NotificationMethodController : Inside notificationMethodLink : MerchantId :: "+merchant.getId());
				MerchantConfiguration dbConfiguration= merchantConfigurationService.findMerchantConfigurationBymerchantId(merchant.getId());
				
				String appStatus = "NA";

				if ((merchant.getOwner() != null && merchant.getOwner().getPos() != null)
						&& (merchant.getOwner().getPos().getPosId() == IConstant.POS_FOODTRONIX
								|| merchant.getOwner().getPos().getPosId() == IConstant.FOCUS))
					appStatus = ProducerUtil.isFoodTronixRunning(merchant.getId(), environment);

				 model.addAttribute("appRunningStatus", appStatus);
					
				if (dbConfiguration != null && dbConfiguration.getEnableNotification() != null) {
					status = dbConfiguration.getEnableNotification();
					autoAcceptFutureOrder=dbConfiguration.getAutoAccept();
					autoAcceptOrder = dbConfiguration.getAutoAcceptOrder();
				}else{
					MerchantConfiguration configuration = new MerchantConfiguration();
					configuration.setEnableNotification(true);
					configuration.setMerchant(merchant);
					
					MerchantConfiguration finalMerchantConfiguration = merchantConfigurationRepository.save(configuration);
					status = finalMerchantConfiguration.getEnableNotification();
				}
				model.addAttribute("NotificationMethod",new NotificationMethod());
				model.addAttribute("methodTypes",notificationMethodService.getNotificationMethodTypes());
				model.addAttribute("methods",notificationMethodService.getNotificationMethods(merchant.getId()));
				
				if (status != null && autoAcceptFutureOrder!=null && autoAcceptOrder!=null) {
					model.addAttribute("merchantConfigurationStatus", status);
					model.addAttribute("merchantConfigurationAutoAccept", autoAcceptFutureOrder);
					model.addAttribute("autoAcceptOrder", autoAcceptOrder);
				}
				LOGGER.info("NotificationMethodController : Inside notificationMethodLink :: End");
				return "notificationMethodLink";
			}else return "redirect:"+environment.getProperty("BASE_URL")+"/support";
		}else return "redirect:"+environment.getProperty("BASE_URL")+"/support";
		}catch(Exception e) {
			LOGGER.info("NotificationMethodController : Inside notificationMethodLink :: Exception "+e);
		MailSendUtil.sendExceptionByMail(e,environment);
			return "redirect:"+environment.getProperty("BASE_URL")+"/support";
		}
	 }
	
	
	
	@RequestMapping(value = "/saveNotificationMethod", method = RequestMethod.POST)
	public String save(@ModelAttribute("NotificationMethod") NotificationMethod notificationMethod, ModelMap model, 
			HttpServletResponse response,HttpServletRequest request) {
		LOGGER.info("NotificationMethodController  : Inside saveNotificationMethod :: Start");
		String message="";
		try {
			
			HttpSession session = request.getSession(false);
			if (session != null) {
				Merchant merchant = (Merchant) session.getAttribute("merchant");
				if(merchant!=null){
					LOGGER.info("NotificationMethodController : Inside saveNotificationMethod : MerchantId :: "+merchant.getId());
					
					MerchantConfiguration merchantConfiguration = merchantConfigurationService.findMerchantConfigurationBymerchantId(merchant.getId());
					if(merchantConfiguration == null){
						MerchantConfiguration configuration = new MerchantConfiguration();
						configuration.setEnableNotification(true);
						configuration.setMerchant(merchant);
						merchantConfigurationRepository.save(configuration);
					}
					
					NotificationMethod notificationMethod1=new NotificationMethod();
					notificationMethod1=notificationMethodService.findByMerchantIdAndContact(merchant.getId(),
							notificationMethod.getContact());
				if(notificationMethod1==null){
					notificationMethod.setMerchant(merchant);
					notificationMethod=notificationMethodService.saveNotificationMethod(notificationMethod);
				}else{
					message= "Details already exists";
				}
				 
			}
		}
			System.out.println("done");
			
		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
				LOGGER.error("error: " + e.getMessage());
				LOGGER.info("NotificationMethodController  : Inside saveNotificationMethod :: Exception "+e);
				return "redirect:https://www.foodkonnekt.com";
			}
		}
		LOGGER.info("NotificationMethodController  : Inside saveNotificationMethod :: End");
		return "redirect:" + environment.getProperty("BASE_URL") + "/notificationMethod?message="+message;
	}
	
	@RequestMapping(value = "/updateStatus", method = RequestMethod.GET)
	@ResponseBody
	public String updateStatus(@RequestParam("status") Boolean status, @RequestParam("id") Integer notificationId) {
		LOGGER.info("NotificationMethodController  : Inside updateStatus :: Start");
		if (status != null && notificationId != null) {
			LOGGER.info("NotificationMethodController  : Inside updateStatus :: NotificationId :"+notificationId);
			notificationMethodService.updateNotificationById(notificationId, status);
		}
		LOGGER.info("NotificationMethodController  : Inside updateStatus :: End");
		return "success";
	}

	@RequestMapping(value = "/deleteNotificationMethod", method = RequestMethod.GET)
	@ResponseBody
	public String deleteNotificationMethod(@RequestParam("id") Integer id) {
		LOGGER.info("NotificationMethodController  : Inside deleteNotificationMethod :: Start");
		if (id != null) {
			LOGGER.info("NotificationMethodController  : Inside deleteNotificationMethod :: NotificationId :"+id);
			notificationMethodService.deleteNotificationMethod(id);
			System.out.println("notification method deleted");
		}
		LOGGER.info("NotificationMethodController  : Inside deleteNotificationMethod :: End");
		return "success";
	}

	@RequestMapping(value = "/getOrderAcceptance", method = RequestMethod.GET)
	@ResponseBody
	public Integer getOrderAcceptance(@RequestParam Integer merchantId, @RequestParam Integer methodTypeid) {
		Integer orderAcceptance = null;
		List<NotificationMethod> method = notificationMethodService.findByMerchantIdAndMethodTypeId(merchantId, methodTypeid);
		if (!method.isEmpty() && method != null && method.size() > 0) {
			orderAcceptance = method.get(0).getIsOrderAccepted();
		}
		return orderAcceptance;
	}
	
	@RequestMapping(value = "/saveMerchantConfiguration", method = RequestMethod.GET)
	@ResponseBody
	public MerchantConfiguration saveMerchantConfiguration(@RequestParam Integer merchantId, @RequestParam Boolean enableNotification ,@RequestParam Boolean autoAccept,
			@RequestParam Boolean autoAcceptOrder) {
		MerchantConfiguration merchantConfigurations=null;
		try{
		LOGGER.info("NotificationMethodController  : Inside saveMerchantConfiguration :: Start");
		LOGGER.info("NotificationMethodController  : Inside saveMerchantConfiguration :: MerchantId "+merchantId);
		LOGGER.info("NotificationMethodController  : Inside saveMerchantConfiguration :: EnableNotification "+enableNotification);
		LOGGER.info("NotificationMethodController  : Inside saveMerchantConfiguration :: autoAccept "+autoAccept);
		LOGGER.info("NotificationMethodController  : Inside saveMerchantConfiguration :: autoAcceptOrder "+autoAcceptOrder);
		
		 merchantConfigurations =	merchantConfigurationService.saveMerchantConfiguration(merchantId,enableNotification,autoAccept,autoAcceptOrder);
		merchantConfigurations.setMerchant(null);
		LOGGER.info("NotificationMethodController  : Inside saveMerchantConfiguration :: End");
		return merchantConfigurations;
		}catch(Exception e)
		{
			LOGGER.info("NotificationMethodController  : Inside saveMerchantConfiguration :: autoAcceptOrder "+autoAcceptOrder);
			MailSendUtil.sendExceptionByMail(e,environment);
		}
		return merchantConfigurations;
	}
	@RequestMapping(value = "/saveMerchantConfigurationByPosId", method = RequestMethod.GET)
	@ResponseBody
	public MerchantConfiguration saveMerchantConfigurationByPosId(@RequestParam String merchantPosId, @RequestParam Boolean enableNotification ,@RequestParam Boolean autoAccept,
			@RequestParam Boolean autoAcceptOrder) {
		MerchantConfiguration merchantConfigurations=null;
		try{
		LOGGER.info("NotificationMethodController  : Inside saveMerchantConfiguration :: Start");
		LOGGER.info("NotificationMethodController  : Inside saveMerchantConfiguration :: merchantPosId "+merchantPosId);
		LOGGER.info("NotificationMethodController  : Inside saveMerchantConfiguration :: EnableNotification "+enableNotification);
		LOGGER.info("NotificationMethodController  : Inside saveMerchantConfiguration :: autoAccept "+autoAccept);
		LOGGER.info("NotificationMethodController  : Inside saveMerchantConfiguration :: autoAcceptOrder "+autoAcceptOrder);
		Merchant merchant=merchantService.findbyPosId(merchantPosId);
		if(merchant!=null && merchant.getId()!=null)
		 merchantConfigurations =	merchantConfigurationService.saveMerchantConfiguration(merchant.getId(),enableNotification,autoAccept,autoAcceptOrder);
		
		if(merchantConfigurations != null)
		merchantConfigurations.setMerchant(null);
		
		LOGGER.info("NotificationMethodController  : Inside saveMerchantConfiguration :: End");
		return merchantConfigurations;
		}catch(Exception e)
		{
			LOGGER.info("NotificationMethodController  : Inside saveMerchantConfiguration :: autoAcceptOrder "+autoAcceptOrder);
			MailSendUtil.sendExceptionByMail(e,environment);
		}
		return merchantConfigurations;
	}
	
	@RequestMapping(value = "/getNotificationByPosId", method = RequestMethod.GET)
	@ResponseBody
	public String getNotificationByPosId(@RequestParam("merchantPosId") String merchantId)
	{
		LOGGER.info("===============  AdminHomeController : Inside merchantId :: start  ============= ");
		Gson gson = new Gson();
		try{
			if(merchantId!=null)
			{
				Merchant merchant=merchantService.findbyPosId(merchantId);
				MerchantConfiguration merchantConfiguration=null;
				if(merchant!=null && merchant.getId()!=null)
				merchantConfiguration =merchantConfigurationService.findMerchantConfigurationBymerchantId(merchant.getId());
				if(merchantConfiguration!=null)
				{
					merchantConfiguration.setMerchant(null);
					return gson.toJson(merchantConfiguration);
				}
			}
		}catch(Exception e)
		{
			MailSendUtil.sendExceptionByMail(e,environment);
		}
		return "{'Error':'no datafound'}";
	}
}
