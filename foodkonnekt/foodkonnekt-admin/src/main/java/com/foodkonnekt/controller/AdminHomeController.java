package com.foodkonnekt.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.axiom.util.base64.Base64Utils;
import org.apache.commons.io.IOUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.foodkonnekt.clover.vo.ItemModifiersVO;
import com.foodkonnekt.clover.vo.KritiqCustomerVO;
import com.foodkonnekt.clover.vo.KritiqMerchantVO;
import com.foodkonnekt.clover.vo.SearchVO;
import com.foodkonnekt.configuration.IConstantProperties;
import com.foodkonnekt.model.Address;
import com.foodkonnekt.model.Category;
import com.foodkonnekt.model.CategoryItem;
import com.foodkonnekt.model.CategoryTiming;
import com.foodkonnekt.model.Clover;
import com.foodkonnekt.model.ConvenienceFee;
import com.foodkonnekt.model.Customer;
import com.foodkonnekt.model.DesktopVesion;
import com.foodkonnekt.model.Item;
import com.foodkonnekt.model.ItemModifierGroup;
import com.foodkonnekt.model.ItemTax;
import com.foodkonnekt.model.Merchant;
import com.foodkonnekt.model.MerchantConfiguration;
import com.foodkonnekt.model.MerchantNotificationApp;
import com.foodkonnekt.model.MerchantSliders;
import com.foodkonnekt.model.MerchantSubscriptionLevel;
import com.foodkonnekt.model.ModifierGroup;
import com.foodkonnekt.model.ModifierModifierGroupDto;
import com.foodkonnekt.model.Modifiers;
import com.foodkonnekt.model.NotificationApp;
import com.foodkonnekt.model.NotificationAppStatus;
import com.foodkonnekt.model.OpenHours;
import com.foodkonnekt.model.OpeningClosingDay;
import com.foodkonnekt.model.OpeningClosingTime;
import com.foodkonnekt.model.OrderNotification;
import com.foodkonnekt.model.OrderR;
import com.foodkonnekt.model.PaymentGateWay;
import com.foodkonnekt.model.PaymentMode;
import com.foodkonnekt.model.PickUpTime;
import com.foodkonnekt.model.PizzaCrust;
import com.foodkonnekt.model.PizzaCrustSizes;
import com.foodkonnekt.model.PizzaSize;
import com.foodkonnekt.model.PizzaTemplate;
import com.foodkonnekt.model.PizzaTemplateCategory;
import com.foodkonnekt.model.PizzaTemplateCrust;
import com.foodkonnekt.model.PizzaTemplateSize;
import com.foodkonnekt.model.PizzaTemplateTax;
import com.foodkonnekt.model.PizzaTemplateTopping;
import com.foodkonnekt.model.PizzaTopping;
import com.foodkonnekt.model.PizzaToppingSize;
import com.foodkonnekt.model.Pos;
import com.foodkonnekt.model.PosIntegration;
import com.foodkonnekt.model.Printer;
import com.foodkonnekt.model.SocialMediaLinks;
import com.foodkonnekt.model.TaxRates;
import com.foodkonnekt.model.TimeZone;
import com.foodkonnekt.model.UpdatedAndCurrentVersion;
import com.foodkonnekt.model.Vendor;
import com.foodkonnekt.model.Zone;
import com.foodkonnekt.repository.CategoryItemRepository;
import com.foodkonnekt.repository.CategoryRepository;
import com.foodkonnekt.repository.DesktopversionRepository;
import com.foodkonnekt.repository.ItemModifierGroupRepository;
import com.foodkonnekt.repository.ItemTaxRepository;
import com.foodkonnekt.repository.ItemmRepository;
import com.foodkonnekt.repository.MerchantRepository;
import com.foodkonnekt.repository.MerchantSubscriptionLevelRepository;
import com.foodkonnekt.repository.ModifierGroupRepository;
import com.foodkonnekt.repository.ModifierModifierGroupRepository;
import com.foodkonnekt.repository.ModifiersRepository;
import com.foodkonnekt.repository.NotificationAppStatusRepository;
import com.foodkonnekt.repository.PizzaCrustRepository;
import com.foodkonnekt.repository.PizzaCrustSizeRepository;
import com.foodkonnekt.repository.PizzaSizeRepository;
import com.foodkonnekt.repository.PizzaTemplateCategoryRepository;
import com.foodkonnekt.repository.PizzaTemplateCrustRepository;
import com.foodkonnekt.repository.PizzaTemplateRepository;
import com.foodkonnekt.repository.PizzaTemplateSizeRepository;
import com.foodkonnekt.repository.PizzaTemplateTaxRepository;
import com.foodkonnekt.repository.PizzaTemplateToppingRepository;
import com.foodkonnekt.repository.PizzaToppingRepository;
import com.foodkonnekt.repository.PizzaToppingSizeRepository;
import com.foodkonnekt.repository.PosIntegrationRepository;
import com.foodkonnekt.repository.PrinterRepository;
import com.foodkonnekt.repository.TaxRateRepository;
import com.foodkonnekt.repository.TimeZoneRepository;
import com.foodkonnekt.service.BusinessService;
import com.foodkonnekt.service.CategoryService;
import com.foodkonnekt.service.CloverService;
import com.foodkonnekt.service.CustomerService;
import com.foodkonnekt.service.ImportExcelService;
import com.foodkonnekt.service.ItemService;
import com.foodkonnekt.service.KritiqService;
import com.foodkonnekt.service.MerchantConfigurationService;
import com.foodkonnekt.service.MerchantNotificationAppService;
import com.foodkonnekt.service.MerchantService;
import com.foodkonnekt.service.MerchantSliderService;
import com.foodkonnekt.service.ModifierService;
import com.foodkonnekt.service.NotificationAppService;
import com.foodkonnekt.service.NotificationAppStatusService;
import com.foodkonnekt.service.OrderNotificationService;
import com.foodkonnekt.service.OrderService;
import com.foodkonnekt.service.PizzaService;
import com.foodkonnekt.service.ZoneService;
import com.foodkonnekt.serviceImpl.ExcelToJsonConverterConfig;
import com.foodkonnekt.util.CloudPrintUtil;
import com.foodkonnekt.util.CloverUrlUtil;
import com.foodkonnekt.util.DateUtil;
import com.foodkonnekt.util.EncryptionDecryptionUtil;
import com.foodkonnekt.util.GoogleOAuthToken;
import com.foodkonnekt.util.IConstant;
import com.foodkonnekt.util.MailSendUtil;
import com.foodkonnekt.util.ProducerUtil;
import com.google.api.client.auth.oauth2.AuthorizationCodeRequestUrl;
import com.google.api.client.auth.oauth2.AuthorizationCodeResponseUrl;
import com.google.gson.Gson;

@Controller
public class AdminHomeController {

	private static final Logger LOGGER = LoggerFactory.getLogger(AdminHomeController.class);

	private static int EXPIRY_TIME = 30 * 60 * 1000;

	@Autowired
    private Environment environment;
	
	@Autowired
    private IConstantProperties iConstantProperties;
	
	@Autowired
	private DesktopversionRepository desktopversionRepository;

	@Autowired
	private PosIntegrationRepository posIntegrationRepository;

	@Autowired
	private CategoryService categoryService;

	@Autowired
	private MerchantService merchantService;

	@Autowired
	private KritiqService kritiqService;

	@Autowired
	private CustomerService customerService;

	@Autowired
	private ItemService itemService;

	@Autowired
	private ModifierService modifierService;

	@Autowired
	private OrderService orderService;

	@Autowired
	private BusinessService businessService;

	@Autowired
	private CloverService cloverService;

	@Autowired
	private ImportExcelService importExcelService;

	@Autowired
	private PizzaService pizzaService;

	@Autowired
	private TaxRateRepository taxRateRepository;

	@Autowired
	private ItemTaxRepository itemTaxRepository;

	@Autowired
	private MerchantSliderService merchantSliderService;

	@Autowired
	private PizzaTemplateToppingRepository pizzaTemplateToppingRepository;

	@Autowired
	private ItemmRepository itemRepository;

	@Autowired
	private CategoryRepository categoryRepository;

	@Autowired
	private ZoneService zoneService;

	@Autowired
	private CategoryItemRepository categoryItemRepository;

	@Autowired
	private ModifierGroupRepository modifierGroupRepository;

	@Autowired
	private ModifierModifierGroupRepository modifierModifierGroupRepository;

	@Autowired
	private ModifiersRepository modifierRepository;

	@Autowired
	private PizzaTemplateRepository pizzaTemplateRepository;

	@Autowired
	private PizzaTemplateSizeRepository pizzaTemplateSizeRepository;

	@Autowired
	private PizzaTemplateCrustRepository pizzaTemplateCrustRepository;

	@Autowired
	private PizzaToppingRepository pizzaToppingRepository;

	@Autowired
	private PizzaToppingSizeRepository pizzaToppingSizeRepository;

	@Autowired
	private PizzaTemplateCategoryRepository pizzaTemplateCategoryRepository;

	@Autowired
	private ItemModifierGroupRepository itemModifierGroupRepository;

	@Autowired
	private PizzaSizeRepository pizzaSizeRepository;

	@Autowired
	private PizzaTemplateTaxRepository pizzaTemplateTaxRepository;

	@Autowired
	private NotificationAppStatusService notificationAppStatusService;

	@Autowired
	private NotificationAppService notificationAppService;

	@Autowired
	private MerchantNotificationAppService merchantNotificationAppService;

	@Autowired
	private PrinterRepository printerRepository;

	@Autowired
	private MerchantSubscriptionLevelRepository merchantSubscriptionLevelRepository;

	@Autowired
	private NotificationAppStatusRepository notificationAppStatusRepository;

	@Autowired
	private OrderNotificationService orderNotificationService;

	@Autowired
	private PizzaCrustSizeRepository pizzaCrustSizeRepository;

	@Autowired
	private PizzaCrustRepository pizzaCrustRepository;
	
	@Autowired
    private BusinessService buisnessService;
	
	@Autowired
    private MerchantConfigurationService merchantConfigurationService;

	@Autowired
	private MerchantRepository merchantRepo;
	
    @Autowired
    private TimeZoneRepository timeZoneRepository;
	
	/**
	 * Show admin home page
	 * 
	 * @return String
	 */
	@RequestMapping(value = "/adminHome", method = RequestMethod.GET)
	public String viewAdminHome(ModelMap model, HttpServletRequest request) {
		LOGGER.info("AdminHomeController : Inside viewAdminHome start");
		final HttpSession session = request.getSession(false);
		model.addAttribute("adminUrl", environment.getProperty("BASE_URL"));
		Merchant merchant1 = null;
		try {
			if (session != null) {
				Object object = session.getAttribute("merchant");

				if (object != null) {
					merchant1 = (Merchant) session.getAttribute("merchant");
				} else {
					return "redirect:" + environment.getProperty("BASE_URL") + "/support";
				}
				if (merchant1 != null && merchant1.getOwner() != null && merchant1.getOwner().getPos() != null
						&& merchant1.getOwner().getPos().getPosId() != null)
					session.setAttribute("merchantType", merchant1.getOwner().getPos().getPosId());

				PickUpTime pickUpTime = businessService.findPickUpTimeByMerchantId(merchant1.getId());
				if (pickUpTime == null || pickUpTime.getPickUpTime() == null
						|| pickUpTime.getPickUpTime().equals("0")) {
					pickUpTime = new PickUpTime();
					pickUpTime.setMerchantId(merchant1.getId());
					pickUpTime.setPickUpTime("45");
					merchantService.savePickupTime(pickUpTime);
				}

				final Merchant merchant = (Merchant) session.getAttribute("merchant");
				if (merchant1 != null && merchant1.getOwner() != null && merchant1.getOwner().getPos() != null
						&& merchant1.getOwner().getPos().getPosId() != null
						&& merchant1.getOwner().getPos().getPosId() == IConstant.POS_CLOVER) {
					session.setAttribute("merchantType", merchant1.getOwner().getPos().getPosId());

					if (merchant != null) {
						Integer isInstall = merchant.getIsInstall();
						/* inventory Thread start */
						if (isInstall == null || isInstall == 0) {
							merchant1.setIsInstall(IConstant.BOOLEAN_TRUE); // 2
							merchantService.save(merchant1);
							final Clover clover = new Clover();
							clover.setMerchantId(merchant.getPosMerchantId());
							clover.setAuthToken(merchant.getAccessToken());
							clover.setInstantUrl(IConstant.CLOVER_INSTANCE_URL);
							clover.setUrl(environment.getProperty("CLOVER_URL"));

							Runnable inventoryRunnable = new Runnable() {

								public void run() {
									// TODO Auto-generated method stub
									session.setAttribute("inventoryThread", 0);
									LOGGER.info("Clover inventoryThread Start");
									cloverService.saveItem(clover, merchant);
									LOGGER.info("Clover item done");
									cloverService.saveCategory(clover, merchant);
									LOGGER.info("Clover category done");
									String modifierJson = CloverUrlUtil.getModifierAndModifierGroup(clover);
									modifierService.saveModifierAndModifierGroup(modifierJson, merchant);
									LOGGER.info("inventoryThread Done");
									session.setAttribute("inventoryThread", 1);
									/*
									 * try{ if(session.getAttribute("isInstall")!= null){ int
									 * isIntall=(Integer)session.getAttribute( "isInstall");
									 * merchant.setIsInstall(isIntall); merchantService.save(merchant);}
									 * }catch(Exception e){ System.out. println("isInstall is not set ->"+e); }
									 */
								}
							};
							Thread inventoryThread = new Thread(inventoryRunnable);
							inventoryThread.setName("inventoryThread");
							inventoryThread.start();
						} else if (isInstall == 1) {
							session.setAttribute("inventoryThread", 1);
						} else {
							session.setAttribute("inventoryThread", 0);
						}
						/* inventory Thread ends */

						/*
						 * if(session.getAttribute("inventoryThread")!=null){ int inventoryThreadStatus=
						 * (Integer)session.getAttribute("inventoryThread");
						 * if(inventoryThreadStatus==1){ merchant.setIsInstall(IConstant.BOOLEAN_TRUE);
						 * merchantService.save(merchant);} else{ session.setAttribute("isInstall",
						 * IConstant.BOOLEAN_TRUE); } }
						 */
						session.setAttribute("merchant", merchant1);
						// Orders
						model.addAttribute("noOfDeliveryOrder", orderService.findNoOfDeliveryOrder(merchant.getId()));
						model.addAttribute("noOfPickUpOrder", orderService.findNoOfPickUpOrder(merchant.getId()));
						model.addAttribute("avgOrderValue", orderService.findAverageOrderValue(merchant.getId()));
						model.addAttribute("totalOrderValue", orderService.findtotalOrderValue(merchant.getId()));

						// Customers
						model.addAttribute("orderFrequency",
								orderService.findOrderFrequency(merchant.getId(), merchant.getOwner().getId()));
						model.addAttribute("customerOrderAverage",
								orderService.findCustomerOrderAverage(merchant.getId(), merchant.getOwner().getId()));
						model.addAttribute("totalNoOfCustomer",
								orderService.findTotalCustomer(merchant.getId(), merchant.getOwner().getId()));
						// Item
						model.addAttribute("trendingItem", orderService.findTrendingItem(merchant.getId()));
						model.addAttribute("averageNumberOfItemPerOrder", orderService
								.findAverageNumberOfItemPerOrder(merchant.getId(), merchant.getOwner().getId()));
					} else {
						return "redirect:" + environment.getProperty("BASE_URL") + "/support";
					}
				} else {
					merchant.setIsInstall(IConstant.BOOLEAN_TRUE); // 2
					merchantService.save(merchant);
				}

			} else {
				return "redirect:" + environment.getProperty("BASE_URL") + "/support";
			}
		} catch (Exception e) {
			if (e != null) {
				if (merchant1 != null)
					MailSendUtil.sendExceptionByMail(e, merchant1,environment);
				else
					MailSendUtil.sendExceptionByMail(e,environment);
				LOGGER.error("error: " + e.getMessage());
				LOGGER.info("AdminHomeController : Inside viewAdminHome: Exception" + e);
				return "redirect:" + environment.getProperty("BASE_URL") + "/support";
			}

		}
		model.addAttribute("status", true);
		return "adminHome";
	}

	@RequestMapping(value = "/uploadInventory", method = RequestMethod.GET)
	public String uploadLogo(ModelMap model) {
		return "uploadInventory";
	}

	@RequestMapping(value = "/echofile", method = RequestMethod.POST, produces = { "application/json" })
	public @ResponseBody HashMap<String, Object> echoFile(MultipartHttpServletRequest request,
			HttpServletResponse response) throws Exception {
		LOGGER.info("AdminHomeController : Inside echoFile :: Start");
		MultipartFile multipartFile = request.getFile("file");
		Long size = multipartFile.getSize();
		String contentType = multipartFile.getContentType();
		InputStream stream = multipartFile.getInputStream();
		byte[] bytes = IOUtils.toByteArray(stream);

		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("fileoriginalsize", size);
		map.put("contenttype", contentType);
		map.put("base64", new String(Base64Utils.encode(bytes)));
		LOGGER.info("AdminHomeController : Inside echoFile :: end");
		return map;
	}

	@SuppressWarnings("unused")
	@RequestMapping(value = "/uploadInventoryByExcel", method = RequestMethod.POST)
	public String save(ModelMap model, HttpServletResponse response, HttpServletRequest request,
			@RequestParam("file") MultipartFile file,
			@RequestParam(required = false, value = "fileName") String fileName) {
		LOGGER.info("AdminHomeController : Inside uploadInventoryByExcel :: Start");
		HttpSession session = request.getSession();
		Merchant merchant = (Merchant) session.getAttribute("merchant");

		try {
			ExcelToJsonConverterConfig config = new ExcelToJsonConverterConfig();

			File temp_file = new File(file.getOriginalFilename());
			try {
				file.transferTo(temp_file);
			} catch (IllegalStateException e1) {
				LOGGER.info("AdminHomeController : Inside save: Exception" + e1);
				e1.printStackTrace();
			} catch (IOException e1) {
				LOGGER.info("AdminHomeController : Inside save: Exception" + e1);
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			config.setSourceFile(file.getOriginalFilename());
			String valid = config.valid();
			if (valid != null) {
				LOGGER.info(valid);
				// help(options);

			}

			try {
				String excelResponse = importExcelService.saveInventoryByExcel(config, merchant, session);

				LOGGER.info(excelResponse.toString());
				/*
				 * if(excelResponse. contains("All the inventory imported successfully")){
				 * session.setAttribute("inventoryThread", 1); } else {
				 * session.setAttribute("inventoryThread", 0); }
				 */

				model.addAttribute("excelResponse", excelResponse);

			} catch (InvalidFormatException e) {
				// TODO Auto-generated catch block
				MailSendUtil.sendExceptionByMail(e,environment);
				LOGGER.error("error: " + e.getMessage());
				LOGGER.info(
						"===============  AdminHomeController : Inside uploadInventoryByExcel :: Exception: InvalidFormatException  ============= "
								+ e);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				MailSendUtil.sendExceptionByMail(e,environment);
				LOGGER.error("error: " + e.getMessage());
				LOGGER.info(
						"===============  AdminHomeController : Inside uploadInventoryByExcel :: Exception: IOException  ============= "
								+ e);
			}

		} catch (Exception e) {
			MailSendUtil.sendExceptionByMail(e,environment);
			LOGGER.error("error: " + e.getMessage());
			LOGGER.info(
					"===============  AdminHomeController : Inside uploadInventoryByExcel :: Exception  ============= "
							+ e);
			return "redirect:https://www.foodkonnekt.com";
		}
		if (fileName != null && !fileName.equals("")) {
			LOGGER.info("url after upload " + "redirect:" + environment.getProperty("BASE_URL") + "/" + fileName);
			return "redirect:" + environment.getProperty("BASE_URL") + "/" + fileName;
		} else {
			LOGGER.info("url after upload " + "redirect:" + environment.getProperty("BASE_URL") + "/" + "/inventory");
			LOGGER.info("===============  AdminHomeController : Inside uploadInventoryByExcel :: End ============= ");
			return "redirect:" + environment.getProperty("BASE_URL") + "/inventory";
		}
	}

	@SuppressWarnings("unused")
	@RequestMapping(value = "/uploadInventoryByExcelByAjax", method = RequestMethod.POST)
	public @ResponseBody String uploadInventoryByExcelByAjax(MultipartHttpServletRequest request,
			HttpServletResponse response) throws Exception {
		LOGGER.info(
				"===============  AdminHomeController : Inside uploadInventoryByExcelByAjax ::Starr ============= ");
		MultipartFile file = request.getFile("file");

		HttpSession session = request.getSession();
		Merchant merchant = (Merchant) session.getAttribute("merchant");

		try {
			ExcelToJsonConverterConfig config = new ExcelToJsonConverterConfig();

			File temp_file = new File(file.getOriginalFilename());
			try {
				file.transferTo(temp_file);
			} catch (IllegalStateException e1) {
				LOGGER.info("AdminHomeController : Inside uploadInventoryByExcelByAjax: Exception" + e1);

				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IOException e1) {
				LOGGER.info("AdminHomeController : Inside uploadInventoryByExcelByAjax: Exception" + e1);

				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			config.setSourceFile(file.getOriginalFilename());
			String valid = config.valid();
			if (valid != null) {
				LOGGER.info(valid);
				// help(options);

			}

			try {
				String excelResponse = importExcelService.saveInventoryByExcel(config, merchant, session);

				LOGGER.info(excelResponse.toString());
				/*
				 * if(excelResponse. contains("All the inventory imported successfully")){
				 * session.setAttribute("inventoryThread", 1); } else {
				 * session.setAttribute("inventoryThread", 0); }
				 */
				LOGGER.info("AdminHomeController : Inside uploadInventoryByExcelByAjax: returns excelResponse"
						+ excelResponse);

				return excelResponse;

			} catch (InvalidFormatException e) {
				// TODO Auto-generated catch block
				MailSendUtil.sendExceptionByMail(e,environment);
				LOGGER.error("error: " + e.getMessage());
				LOGGER.info(
						"===============  AdminHomeController : Inside uploadInventoryByExcel :: Exception: InvalidFormatException  ============= "
								+ e);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				MailSendUtil.sendExceptionByMail(e,environment);
				LOGGER.error("error: " + e.getMessage());
				LOGGER.info(
						"===============  AdminHomeController : Inside uploadInventoryByExcel :: Exception: IOException  ============= "
								+ e);
			}

		} catch (Exception e) {
			MailSendUtil.sendExceptionByMail(e,environment);
			LOGGER.error("error: " + e.getMessage());
			LOGGER.info(
					"===============  AdminHomeController : Inside uploadInventoryByExcel :: Exception:  ============= "
							+ e);
			return "error";
		}
		LOGGER.info("===============  AdminHomeController : Inside uploadInventoryByExcel :: End:  ============= ");
		return "error";
	}

	@RequestMapping(value = "/adminSessionTimeOut", method = RequestMethod.GET)
	public String sessionTimeOut(ModelMap model) {
		LOGGER.info("===============  AdminHomeController : Inside sessionTimeOut :: Start  ============= ");
		return "adminSessionTimeOut";
	}

	/**
	 * Show Inventory page
	 * 
	 * @return String
	 */
	@RequestMapping(value = "/inventory", method = RequestMethod.GET)
	public String viewInventory(ModelMap model, HttpServletRequest request) {
		LOGGER.info("===============  AdminHomeController : Inside viewInventory :: Start  ============= ");
		HttpSession session = request.getSession(false);
		if (session != null) {
			Merchant merchant = (Merchant) session.getAttribute("merchant");
			if (merchant != null) {

				LOGGER.info("===============  AdminHomeController : Inside viewInventory :: MerchantId "
						+ merchant.getId() + " ============= ");

				model.addAttribute("categories", categoryService.findAllCategory(merchant.getId()));
				if (merchant.getOwner() != null && merchant.getOwner().getPos() != null
						&& merchant.getOwner().getPos().getPosId() != null)
					model.addAttribute("merchantType", merchant.getOwner().getPos().getPosId());
			} else {
				LOGGER.info("===============  AdminHomeController : Inside viewInventory :: End  ============= ");
				return "redirect:" + environment.getProperty("BASE_URL") + "/support";
			}
		} else {
			LOGGER.info("===============  AdminHomeController : Inside viewInventory :: End  ============= ");
			return "redirect:" + environment.getProperty("BASE_URL") + "/support";
		}
		LOGGER.info("===============  AdminHomeController : Inside viewInventory :: End  ============= ");
		return "inventory";
	}

	@RequestMapping(value = "/support", method = RequestMethod.GET)
	public String support(@ModelAttribute("Customer") Customer customer, ModelMap model, HttpServletRequest request) {
		HttpSession session = request.getSession();
		session.invalidate();
		return "adminLogin";
	}

	@RequestMapping(value = "/LoginByAdmin", method = RequestMethod.POST)
	public String login(@ModelAttribute("Customer") Customer customer, ModelMap model, HttpServletResponse response,
			HttpServletRequest request) {
		model.addAttribute("adminUrl", environment.getProperty("BASE_URL"));
		LOGGER.info("===============  AdminHomeController : Inside login :: Start  ============= ");
		Map<Object, Object> loginMap = new HashMap<Object, Object>();
		try {
			HttpSession session = request.getSession();
			Customer result = customerService.findByEmailAndPasswordForAdmin(customer.getEmailId(),
					EncryptionDecryptionUtil.encryptString(customer.getPassword()));

			if (result != null)
				LOGGER.info("===============  AdminHomeController : Inside login ::result " + result.getCustomerType());

			if (result != null && result.getCustomerType() != null && result.getCustomerType().equals("admin")) {
				LOGGER.info("===============  AdminHomeController : Inside login :: role: admin");
				if (result.getMerchantt() != null && result.getMerchantt().getName().equals("FoodkonnektAdmin")
						&& result.getMerchantt().getOwner() != null
						&& result.getMerchantt().getOwner().getRole() != null
						&& result.getMerchantt().getOwner().getRole().getId() == IConstant.POS_CLOVER) {
					LOGGER.info("===============  AdminHomeController : Inside login :: Name: FoodkonnektAdmin");
					session.setAttribute("foodkonnektAdmin", result);
					session.setAttribute("isFoodkonnektAdmin", true);
					// Customer customerResult =
					// customerService.getCustomerProfile(result.getId());
					loginMap.put(IConstant.RESPONSE, IConstant.RESPONSE_SUCCESS_MESSAGE);
					// loginMap.put(IConstant.DATA, customerResult);
					LOGGER.info("===============  AdminHomeController : Inside login :: End  ============= ");
					LOGGER.info("===============  AdminHomeController : Inside login :: BASE_URL  ============= "+environment.getProperty("BASE_URL"));
					LOGGER.info("===============  AdminHomeController : Inside login :: Url ::"+"redirect:" + environment.getProperty("BASE_URL") + "/getAllMerchants");
					return "redirect:" + environment.getProperty("BASE_URL") + "/getAllMerchants?viewType=Basic";
				} else if (result.getMerchantt() != null && result.getMerchantt().getOwner() != null
						&& result.getMerchantt().getOwner().getId() != null) {
					LOGGER.info("===============  AdminHomeController : Inside login ::   ============= ");
					session.setAttribute("foodkonnektAdmin", result);
					session.setAttribute("isFoodkonnektAdmin", false);
					// session.setAttribute("isFoodkonnektAdmin", true);
					LOGGER.info("===============  AdminHomeController : Inside login :: End  ============= ");
					LOGGER.info("===============  AdminHomeController : Inside login :: BASE_URL  ============= "+environment.getProperty("BASE_URL"));
					LOGGER.info("===============  AdminHomeController : Inside login :: Url ::"+"redirect:" + environment.getProperty("BASE_URL") + "/getAllMerchants");
					return "redirect:" + environment.getProperty("BASE_URL") + "/getAllMerchants";
				}
				return "redirect:" + environment.getProperty("BASE_URL") + "/support";
			} else if (result != null && result.getMerchantt() != null && result.getCustomerType() != null
					&& result.getCustomerType().equals("location")) {
				LOGGER.info("===============  AdminHomeController : Inside login :: Role: location");
				Merchant merchant = result.getMerchantt();
				session.setAttribute("merchant", result.getMerchantt());
				session.setAttribute("isFoodkonnektAdmin", false);
				Random rand = new Random();
				int n = rand.nextInt(900000) + 100000;
				String text1 = Integer.toString(n);
				System.out.println(n);
				session.setAttribute("test", text1);
				LOGGER.info("===============  AdminHomeController : Inside login :: Role: location");
				LOGGER.info("===============  AdminHomeController : Inside login :: BASE_URL  ============= "+environment.getProperty("BASE_URL"));
				LOGGER.info("===============  AdminHomeController : Inside login :: URl:: "+"redirect:" + environment.getProperty("BASE_URL") + "/existingMerchant?merchant_id="
						+ merchant.getPosMerchantId()
						+ "&employee_id=T03TVG9M6C7PG&client_id=SMA6T1DNS8YAE&access_token=" + merchant.getAccessToken()
						+ "");
				return "redirect:" + environment.getProperty("BASE_URL") + "/existingMerchant?merchant_id="
						+ merchant.getPosMerchantId()
						+ "&employee_id=T03TVG9M6C7PG&client_id=SMA6T1DNS8YAE&access_token=" + merchant.getAccessToken()
						+ "";
			} else {
				loginMap.put(IConstant.RESPONSE, IConstant.RESPONSE_NO_DATA_MESSAGE);
				loginMap.put(IConstant.MESSAGE, IConstant.LOGIN_FAILURE);
				LOGGER.info("===============  AdminHomeController : Inside login :: End  ============= ");
				return "redirect:" + environment.getProperty("BASE_URL") + "/support?message=Invalid username or password";
			}
		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
			}
			LOGGER.error("error: " + e.getMessage());
			LOGGER.info("===============  AdminHomeController : Inside login :: Exception : ============= " + e);
			return "redirect:" + environment.getProperty("BASE_URL") + "/support";
		}

	}

	@RequestMapping(value = "/forgotAdminPassword", method = RequestMethod.POST)
	public String forgotPasswordAction(@RequestParam(value = "emailid") String emailid, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		LOGGER.info(
				"===============  AdminHomeController : Inside forgotPasswordAction :: Start  ============= emailid "
						+ emailid);

		Map<Object, Object> forgotPasswordMap = new HashMap<Object, Object>();
		String message = "";
		try {
			HttpSession session = request.getSession();

			if (emailid != null) {
				boolean status = customerService.findAdminByEmail(emailid);
				if (status) {
					/*
					 * forgotPasswordMap.put("message",
					 * "Please check your email for future instructions");
					 */
					message = "Please check your email for further instructions";
				} else {
					forgotPasswordMap.put("message",
							"An account with the above email does not exist. Please try again");
					message = "An account with the above email does not exist. Please try again";
				}
			}
		} catch (Exception exception) {
			if (exception != null) {
				MailSendUtil.sendExceptionByMail(exception,environment);
				LOGGER.info(
						"===============  AdminHomeController : Inside forgotPasswordAction :: Exception  ============= "
								+ exception);
			}
			exception.printStackTrace();
		}
		LOGGER.info("===============  AdminHomeController : Inside forgotPasswordAction :: End  =============message "
				+ message);
		return "redirect:" + environment.getProperty("BASE_URL") + "/support?message=" + message + "";
	}

	@RequestMapping(value = "/saveAdminPassword", method = RequestMethod.POST)
	public String saveAdminPassword(@ModelAttribute("Customer") Customer customer, ModelMap model,
			HttpServletRequest request, HttpServletResponse response) {
		try {
			// HttpSession session = request.getSession();

			LOGGER.info("===============  AdminHomeController : Inside saveAdminPassword :: Start  ============= ");

			if (customer != null && customer.getId() != null) {
				Customer sessionCustomer = customerService.findByCustomerId(customer.getId());
				if (sessionCustomer != null) {
					sessionCustomer.setPassword(customer.getPassword());
					sessionCustomer = customerService.setGuestCustomerPassword(sessionCustomer);
				} else {
					LOGGER.info(
							"===============  AdminHomeController : Inside saveAdminPassword :: End  ============= ");
					return "redirect:" + environment.getProperty("BASE_URL") + "/support";
				}

			} else {
				LOGGER.info("===============  AdminHomeController : Inside saveAdminPassword :: End  ============= ");
				return "redirect:" + environment.getProperty("BASE_URL") + "/support";
			}
		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
				LOGGER.info(
						"===============  AdminHomeController : Inside saveAdminPassword :: Exception  ============= "
								+ e);
			}
			LOGGER.error("error: " + e.getMessage());
		}
		LOGGER.info("===============  AdminHomeController : Inside saveAdminPassword :: End  ============= ");
		return "redirect:" + environment.getProperty("BASE_URL") + "/support";
	}

	@RequestMapping(value = "/changeAdminpassword", params = { "email", "merchantId",
			"userId" }, method = RequestMethod.GET)
	public String changeAdminpassword(Map<String, Object> map, HttpServletRequest request,
			@RequestParam(value = "email") String email, @RequestParam(value = "merchantId") String merchantId,
			@RequestParam(value = "userId") String userId, @RequestParam(value = "tLog") String tLog) {

		LOGGER.info(
				"===============  AdminHomeController : Inside changeAdminpassword :: Start  ============= merchantId "
						+ merchantId);

		try {
			Long time = Long.valueOf(EncryptionDecryptionUtil.decryption(tLog));
			Long currentTime = System.currentTimeMillis();
			int diff = (int) (currentTime - time);

			if (diff < EXPIRY_TIME) {

				if (merchantId != null) {
					String id = merchantId;
					Customer customer = customerService.findCustomerByEmailAndMerchantIdForAdmin(email,
							Integer.valueOf(id), userId);
					if (customer != null) {
						customer.setPassword("");
						map.put("Customer", customer);
						LOGGER.info(
								"===============  AdminHomeController : Inside changeAdminpassword :: End  ============= merchantId "
										+ merchantId);
						return "resetAdminPassword";
					} else {
						LOGGER.info(
								"===============  AdminHomeController : Inside changeAdminpassword :: End  ============= merchantId "
										+ merchantId);
						return "redirect:https://www.foodkonnekt.com";
					}
				}
			} else {
				LOGGER.info("Link is expired");
				map.put("Customer", new Customer());
				LOGGER.info(
						"===============  AdminHomeController : Inside changeAdminpassword :: End  ============= merchantId "
								+ merchantId);
				return "redirect:" + environment.getProperty("BASE_URL") + "/support?message=Link is expired .Please try again";
			}
		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
				LOGGER.info(
						"===============  AdminHomeController : Inside changeAdminpassword :: Exception  ============= "
								+ e);
			}
			LOGGER.error("error: " + e.getMessage());
		}
		LOGGER.info("===============  AdminHomeController : Inside changeAdminpassword :: End  ============= ");
		return null;
	}

	/**
	 * Show add line item page
	 * 
	 * @return String
	 */
	@RequestMapping(value = "/addLineItem", method = RequestMethod.GET)
	public String viewAddLineItem(@ModelAttribute("Item") Item item, ModelMap model, HttpServletRequest request,
			@RequestParam(required = false) int itemId) {
		try {
			LOGGER.info("===============  AdminHomeController : Inside viewAddLineItem :: Start  itemId============= "
					+ itemId);
			HttpSession session = request.getSession(false);
			if (session != null) {
				Merchant merchant = (Merchant) session.getAttribute("merchant");
				if (merchant != null) {
					Item result = itemService.findItemByItemId(itemId);
					model.addAttribute("modifierLimit", result.getModifiersLimit());
					model.addAttribute("itemStatus", result.getItemStatus());
					model.addAttribute("item", result);
					model.addAttribute("times", DateUtil.findAllTime());
					model.addAttribute("times24", DateUtil.findAll24Time());
					if (merchant.getOwner() != null && merchant.getOwner().getPos() != null
							&& merchant.getOwner().getPos().getPosId() != null) {
						model.addAttribute("merchantType", merchant.getOwner().getPos().getPosId());
					}
					List<CategoryItem> categoryItem = categoryItemRepository.findByItemId(itemId);
					if (categoryItem != null && !categoryItem.isEmpty()) {
						model.addAttribute("categoryItem", categoryItem);

					} else
						model.addAttribute("categoryItem", "");

					List<ItemTax> itemTaxs = itemTaxRepository.findByItemId(itemId);
					if (!itemTaxs.isEmpty() && itemTaxs.size() > 0) {
						LOGGER.info(
								"===============  AdminHomeController : Inside viewAddLineItem :: inside if (!itemTaxs.isEmpty())  ");

						List<TaxRates> taxRates = new ArrayList<TaxRates>();
						for (ItemTax itemTax : itemTaxs) {
							if (itemTax != null && itemTax.getTaxRates() != null) {
								taxRates.add(itemTax.getTaxRates());
							}
						}
						model.addAttribute("itemTaxes", taxRates);
						model.addAttribute("itemTaxs", itemTaxs);
					}
				}
			} else {
				LOGGER.info("===============  AdminHomeController : Inside viewAddLineItem :: End  ============= ");
				return "redirect:https://www.foodkonnekt.com";
			}
		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
				LOGGER.error("error: " + e.getMessage());
				LOGGER.info("===============  AdminHomeController : Inside viewAddLineItem :: Exception  ============= "
						+ e);
				return "redirect:https://www.foodkonnekt.com";
			}
		}
		LOGGER.info("===============  AdminHomeController : Inside viewAddLineItem :: End  ============= ");
		return "addLineItem";
	}

//	@RequestMapping(value = "/editModifier", method = RequestMethod.GET)
//	public String viewEditModifier(@ModelAttribute("Modifiers") Modifiers modifiers, ModelMap model,
//			HttpServletRequest request, @RequestParam(required = false) int modifierId) {
//		try {
//			LOGGER.info(
//					"===============  AdminHomeController : Inside viewEditModifier :: Start  ============= modifierId "
//							+ modifierId);
//
//			HttpSession session = request.getSession(false);
//			if (session != null) {
//				Merchant merchant = (Merchant) session.getAttribute("merchant");
//				if (merchant != null) {
//					Modifiers result = modifierService.findModifiersByModifiersId(modifierId);
//
//					model.addAttribute("modifiers", result);
//
//					if (merchant.getOwner() != null && merchant.getOwner().getPos() != null
//							&& merchant.getOwner().getPos().getPosId() != null) {
//						model.addAttribute("merchantType", merchant.getOwner().getPos().getPosId());
//					}
//				}
//			} else {
//				return "redirect:https://www.foodkonnekt.com";
//			}
//		} catch (Exception e) {
//			if (e != null) {
//				MailSendUtil.sendExceptionByMail(e,environment);
//				LOGGER.error("error: " + e.getMessage());
//				LOGGER.info("===============  AdminHomeController : Inside viewEditModifier :: End  ============= ");
//
//				return "redirect:https://www.foodkonnekt.com";
//			}
//		}
//		return "editModifier";
//	}

	 @RequestMapping(value = "/editModifier", method = RequestMethod.GET)
	public String viewEditModifier(@ModelAttribute("Modifiers") Modifiers modifiers, ModelMap model,
			HttpServletRequest request, @RequestParam(required = false) int modifierId) {
		try {
			LOGGER.info(
					"===============  AdminHomeController : Inside viewEditModifier :: Start  ============= modifierId "
							+ modifierId);

			HttpSession session = request.getSession(false);
			if (session != null) {
				Merchant merchant = (Merchant) session.getAttribute("merchant");
				if (merchant != null) {
					Modifiers result = modifierService.findModifiersByModifiersId(modifierId);

					model.addAttribute("modifiers", result);

					if (merchant.getOwner() != null && merchant.getOwner().getPos() != null
							&& merchant.getOwner().getPos().getPosId() != null) {
						model.addAttribute("merchantType", merchant.getOwner().getPos().getPosId());
					}

					List<ModifierModifierGroupDto> modifierGroupList = modifierModifierGroupRepository
							.findByModifiersIdAndModifierGroupActive(modifierId ,1);

					model.addAttribute("modifierGrouplist", modifierGroupList);

					List<ModifierGroup> unmapModifierGroupList = new ArrayList<ModifierGroup>();

					List<ModifierGroup> unmappedModifierGroup = modifierGroupRepository
							.findByMerchantIdAndModifierIdAndStatus(merchant.getId(), modifierId ,1);
					
					if (unmappedModifierGroup != null && !unmappedModifierGroup.isEmpty()) {
						for (ModifierGroup unmapModifierGroup : unmappedModifierGroup) {
							unmapModifierGroupList.add(unmapModifierGroup);
						}
						model.addAttribute("unmapModifierGroup", unmapModifierGroupList);
					}
					// }

					if (merchant.getOwner() != null && merchant.getOwner().getPos() != null
							&& merchant.getOwner().getPos().getPosId() != null) {
						model.addAttribute("merchantType", merchant.getOwner().getPos().getPosId());
					}
				}
			} else {
				return "redirect:https://www.foodkonnekt.com";
			}
		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
				LOGGER.error("error: " + e.getMessage());
				LOGGER.info("===============  AdminHomeController : Inside viewEditModifier :: End  ============= ");

				return "redirect:https://www.foodkonnekt.com";
			}
		}
		return "editModifier";
	}

//	@RequestMapping(value = "/editModifierGroup", method = RequestMethod.GET)
//	public String viewEditModifierGroup(@ModelAttribute("ModifierGroup") ModifierGroup modifierGroup, ModelMap model,
//			HttpServletRequest request, @RequestParam(required = false) int modifierGroupId) {
//		try {
//			LOGGER.info(
//					"===============  AdminHomeController : Inside viewEditModifierGroup :: Start  ============= modifierGroupId "
//							+ modifierGroupId);
//			HttpSession session = request.getSession(false);
//			if (session != null) {
//				Merchant merchant = (Merchant) session.getAttribute("merchant");
//				if (merchant != null) {
//					ModifierGroup result = modifierService.findModifierGroupByModifiersId(modifierGroupId);
//
//					List<Modifiers> modifiersList = new ArrayList<Modifiers>();
//
//					if (result != null && result.getId() != null) {
//						List<Modifiers> modifiers = modifierModifierGroupRepository
//								.findByModifierGroupId(result.getId());
//						if (modifiers != null && !modifiers.isEmpty()) {
//							LOGGER.info(
//									"===============  AdminHomeController : Inside if (modifiers != null && !modifiers.isEmpty())");
//
//							for (Modifiers finalModifiers : modifiers) {
//								Modifiers dbModifiers = modifierRepository.findOne(finalModifiers.getId());
//								modifiersList.add(dbModifiers);
//							}
//
//							model.addAttribute("modifiers", modifiersList);
//						}
//					}
//
//					model.addAttribute("modifierGroup", result);
//
//					if (merchant.getOwner() != null && merchant.getOwner().getPos() != null
//							&& merchant.getOwner().getPos().getPosId() != null) {
//						model.addAttribute("merchantType", merchant.getOwner().getPos().getPosId());
//					}
//				}
//			} else {
//				LOGGER.info(
//						"===============  AdminHomeController : Inside viewEditModifierGroup :: End  ============= ");
//				return "redirect:https://www.foodkonnekt.com";
//			}
//		} catch (Exception e) {
//			if (e != null) {
//				MailSendUtil.sendExceptionByMail(e,environment);
//				LOGGER.error("error: " + e.getMessage());
//				LOGGER.info(
//						"===============  AdminHomeController : Inside viewEditModifierGroup :: Exception  ============= "
//								+ e);
//				return "redirect:https://www.foodkonnekt.com";
//			}
//		}
//		LOGGER.info("===============  AdminHomeController : Inside viewEditModifierGroup :: End  ============= ");
//		return "editModifierGroup";
//	}

	@RequestMapping(value = "/editModifierGroup", method = RequestMethod.GET)
	public String viewEditModifierGroup(@ModelAttribute("ModifierGroup") ModifierGroup modifierGroup, ModelMap model,
			HttpServletRequest request, @RequestParam(required = false) int modifierGroupId) {
		try {
			LOGGER.info(
					"===============  AdminHomeController : Inside viewEditModifierGroup :: Start  ============= modifierGroupId "
							+ modifierGroupId);
			HttpSession session = request.getSession(false);
			if (session != null) {
				Merchant merchant = (Merchant) session.getAttribute("merchant");
				if (merchant != null) {

					List<ModifierModifierGroupDto> modifierList = modifierModifierGroupRepository
							.findByModifierGroupIdAndModifiersStatus(modifierGroupId, 1);

					model.addAttribute("modifierGrouplist", modifierList);

					List<Modifiers> unmapModifiersList = new ArrayList<Modifiers>();

					List<Modifiers> unmappedModifiers = modifierRepository
							.findByMerchantIdAndModifierGroupIdAndStatus(merchant.getId(), modifierGroupId ,1);
					if (unmappedModifiers != null && !unmappedModifiers.isEmpty()) {
						for (Modifiers unmapModifiers : unmappedModifiers) {
							unmapModifiersList.add(unmapModifiers);
						}
						model.addAttribute("unmapModifiers", unmapModifiersList);
					}
					// }
					ModifierGroup result = modifierService.findModifierGroupByModifiersId(modifierGroupId);

					model.addAttribute("dbModifiers", result);

					if (merchant.getOwner() != null && merchant.getOwner().getPos() != null
							&& merchant.getOwner().getPos().getPosId() != null) {
						model.addAttribute("merchantType", merchant.getOwner().getPos().getPosId());
					}
				}
			} else {
				LOGGER.info(
						"===============  AdminHomeController : Inside viewEditModifierGroup :: End  ============= ");
				return "redirect:https://www.foodkonnekt.com";
			}
		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
				LOGGER.error("error: " + e.getMessage());
				LOGGER.info(
						"===============  AdminHomeController : Inside viewEditModifierGroup :: Exception  ============= "
								+ e);
				return "redirect:https://www.foodkonnekt.com";
			}
		}
		LOGGER.info("===============  AdminHomeController : Inside viewEditModifierGroup :: End  ============= ");
		return "editModifierGroup";
	}

	@RequestMapping(value = "/editCategory", method = RequestMethod.GET)
	public String viewEditCategory(@ModelAttribute("Category") CategoryItem categoryItems, ModelMap model,
			HttpServletRequest request, @RequestParam(required = false) int categoryId) {
		try {
			LOGGER.info(
					"===============  AdminHomeController : Inside viewEditCategory :: Start  ============= categoryId "
							+ categoryId);

			HttpSession session = request.getSession(false);
			if (session != null) {
				Merchant merchant = (Merchant) session.getAttribute("merchant");
				if (merchant != null) {
					Category result = categoryService.findCategoryByCategoryId(categoryId);

					model.addAttribute("category", result);

					List<Item> items = new ArrayList<Item>();
					List<Item> dbItems = new ArrayList<Item>();

					if (result.getIsPizza() == 0) {
						List<CategoryItem> categoryItem = categoryItemRepository.findByCategoryId(categoryId);
						if (categoryItem != null && categoryItem.size() > 0 && !categoryItem.isEmpty()) {
							for (CategoryItem finalCategoryItem : categoryItem) {
								if (finalCategoryItem.getItem() != null
										&& finalCategoryItem.getItem().getId() != null) {
									Item item = itemRepository.findOne(finalCategoryItem.getItem().getId());
									if (item != null) {
										items.add(item);
									}
								}
							}
						}
						model.addAttribute("items", items);

						List<Integer> itemIds = categoryItemRepository.getItemByMerchantId(categoryId,
								merchant.getId());
						if (itemIds != null && !itemIds.isEmpty() && itemIds.size() > 0) {
							for (Integer id : itemIds) {
								LOGGER.info(
										"===============  AdminHomeController : Inside viewEditCategory :: CategoryItemId  === "
												+ id);
								Item dbItem = itemRepository.findOne(id);
								if (dbItem != null) {
									dbItems.add(dbItem);
								}
							}
						}
						model.addAttribute("itemList", dbItems);

					} else if (result.getIsPizza() == 1) {
						List<PizzaTemplate> mappedpizzas = pizzaTemplateRepository
								.findByCategoryIdAndPizzaActive(categoryId, 1);
						List<PizzaTemplate> unmappedpizzas = pizzaTemplateRepository
								.findUnMappedpizzaByCategoryIdAndPizzaActive(categoryId, merchant.getId(), 1);
                      
						List<PizzaTemplateCategory> mappizzas = new ArrayList<PizzaTemplateCategory>();
						List<PizzaTemplate> unmappizzas = new ArrayList<PizzaTemplate>();
						for (PizzaTemplate pizzaTemplatecat : mappedpizzas) {
							pizzaTemplatecat.setName(pizzaTemplatecat.getDescription());
						}

						for (PizzaTemplate pizzaTemplate : unmappedpizzas) {
							pizzaTemplate.setName(pizzaTemplate.getDescription());
						}

						model.addAttribute("items", mappedpizzas);
						model.addAttribute("itemList", unmappedpizzas);
					}
					if (merchant.getOwner() != null && merchant.getOwner().getPos() != null
							&& merchant.getOwner().getPos().getPosId() != null) {
						model.addAttribute("merchantType", merchant.getOwner().getPos().getPosId());
					}
				}
			} else {
				LOGGER.info("===============  AdminHomeController : Inside viewEditCategory :: End  ============= ");

				return "redirect:https://www.foodkonnekt.com";
			}
		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
				LOGGER.error("error: " + e.getMessage());
				LOGGER.info(
						"===============  AdminHomeController : Inside viewEditCategory :: Exception  ============= "
								+ e);
				LOGGER.info("===============  AdminHomeController : Inside viewEditCategory :: End  ============= ");

				return "redirect:https://www.foodkonnekt.com";
			}
		}
		LOGGER.info("===============  AdminHomeController : Inside viewEditCategory :: End  ============= ");

		return "editCategory";
	}

	@RequestMapping(value = "/getModifiersByModifierGroupIds", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String filterByCategory(HttpServletRequest request,
			@RequestParam(required = false) String modifierGroupIds, @RequestParam(required = false) Integer itemId) {
		LOGGER.info(
				"===============  AdminHomeController : Inside filterByCategory :: Start  ============= modifierGroupIds "
						+ modifierGroupIds + " itemId " + itemId);

		List<List<ItemModifiersVO>> itemModifiers = new ArrayList();
		Map<Object, Object> itemModifierMap = new HashMap<Object, Object>();
		if (modifierGroupIds != null && !modifierGroupIds.equals("null") && !modifierGroupIds.equals("undefined")) {
			String[] groupIds = modifierGroupIds.split(",");
			for (String groupId : groupIds) {

				int modifierGroupId = 0;
				try {
					modifierGroupId = Integer.parseInt(groupId);
				} catch (Exception e) {

				}
				if (modifierGroupId != 0) {

					List<ItemModifiersVO> itemModifierList = modifierService
							.findModifiersbyModifierGroup(modifierGroupId, itemId);
					if (itemModifierList != null && itemModifierList.size() > 0) {
						itemModifiers.add(itemModifierList);
					}
				}
			}
			itemModifierMap.put(IConstant.RESPONSE, IConstant.RESPONSE_SUCCESS_MESSAGE);
			itemModifierMap.put(IConstant.DATA, itemModifiers);
		} else {

			itemModifierMap.put(IConstant.RESPONSE, IConstant.RESPONSE_NO_DATA_MESSAGE);
			itemModifierMap.put(IConstant.MESSAGE, "ModifierId is null");

		}
		LOGGER.info("===============  AdminHomeController : Inside filterByCategory :: End  ============= ");

		return new Gson().toJson(itemModifiers);
	}

	@RequestMapping(value = "/updateItem", method = RequestMethod.POST)
	public String updateItem(@ModelAttribute("Item") Item item, ModelMap model, HttpServletRequest request,
			@RequestParam("file") MultipartFile file) {
		try {
			LOGGER.info("===============  AdminHomeController : Inside updateItem :: Start  ============= item "
					+ item.getId());

			String description = request.getParameter("description");
			item.setDescription(description);

			/*
			 * if (!(file.getSize() == 0)) { // images Long size = file.getSize(); if (size
			 * >= 2097152) { model.addAttribute("filesize",
			 * "Maximum Allowed File Size is 2 MB"); return "uploadLogo"; }
			 * 
			 * String orgName = file.getOriginalFilename();
			 * 
			 * String exts[] = orgName.split("\\.");
			 * 
			 * String ext = exts[1]; //String logoName = item.getId() + "_" +
			 * item.getName().trim() + "." + ext; String logoName = item.getId() + "_" + "."
			 * + ext; String filePath = environment.getProperty("ADMIN_SERVER_LOGO_PATH") + logoName; File
			 * dest = new File(filePath); try { file.transferTo(dest); } catch
			 * (IllegalStateException e) { LOGGER.error("error: " + e.getMessage()); return
			 * "File uploaded failed:" + orgName; } catch (IOException e) {
			 * LOGGER.error("error: " + e.getMessage()); return "File uploaded failed:" + orgName; }
			 * LOGGER.info("File uploaded:" + orgName);
			 * item.setItemImage(environment.getProperty("ADMIN_LOGO_PATH_TO_SHOW") + logoName); }
			 */

			itemService.updateLineItemValue(item, file);
		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
				LOGGER.error("error: " + e.getMessage());
				LOGGER.info("===============  AdminHomeController : Inside updateItem :: End  ============= ");

				return "redirect:https://www.foodkonnekt.com";
			}
		}
		LOGGER.info("===============  AdminHomeController : Inside updateItem :: End  ============= ");

		return "redirect:" + environment.getProperty("BASE_URL") + "/inventory";
	}

//	@RequestMapping(value = "/updateModifier", method = RequestMethod.POST)
//	public String updateModifier(@ModelAttribute("Modifiers") Modifiers modifier, ModelMap model,
//			HttpServletRequest request) {
//		try {
//			LOGGER.info("===============  AdminHomeController : Inside updateModifier :: Start  ============= modifier "
//					+ modifier.getId());
//
//			modifierService.updateLineModifierValue(modifier);
//		} catch (Exception e) {
//			if (e != null) {
//				MailSendUtil.sendExceptionByMail(e,environment);
//				LOGGER.error("error: " + e.getMessage());
//				LOGGER.info("===============  AdminHomeController : Inside updateModifier :: End  ============= ");
//
//				return "redirect:https://www.foodkonnekt.com";
//			}
//		}
//		LOGGER.info("===============  AdminHomeController : Inside updateModifier :: End  ============= ");
//
//		return "redirect:" + environment.getProperty("BASE_URL") + "/modifiers";
//	}

	@RequestMapping(value = "/updateModifier", method = RequestMethod.POST)
	public String updateModifier(@ModelAttribute("Modifiers") Modifiers modifier, ModelMap model,
			HttpServletRequest request) {
		try {
LOGGER.info("===============  AdminHomeController : Inside updateModifier :: Start  ============= modifier "
					+ modifier.getId());

		HttpSession session = request.getSession();
			if (session != null) {
				Merchant merchant = (Merchant) session.getAttribute("merchant");
				if (merchant != null) {
					modifierService.updateLineModifierValue(modifier, merchant.getId());
			} else
				
					return "redirect:https://www.foodkonnekt.com";
			} else
				return "redirect:https://www.foodkonnekt.com";
		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
				LOGGER.error("error: " + e.getMessage());
				LOGGER.info("===============  AdminHomeController : Inside updateModifier :: End  ============= ");

				return "redirect:https://www.foodkonnekt.com";
			}
		}
		LOGGER.info("===============  AdminHomeController : Inside updateModifier :: End  ============= ");

		return "redirect:" + environment.getProperty("BASE_URL") + "/modifiers";
	}

//	@RequestMapping(value = "/updateModifierGroup", method = RequestMethod.POST)
//	public String updateModifierGroup(@ModelAttribute("ModifierGroup") ModifierGroup modifierGroup, ModelMap model,
//			HttpServletRequest request) {
//		try {
//			LOGGER.info(
//					"===============  AdminHomeController : Inside updateModifierGroup :: Start  ============= modifierGroup "
//							+ modifierGroup.getId());
//
//			modifierService.updateModifierGroupValue(modifierGroup);
//		} catch (Exception e) {
//			if (e != null) {
//				MailSendUtil.sendExceptionByMail(e,environment);
//				LOGGER.error("error: " + e.getMessage());
//				LOGGER.info("===============  AdminHomeController : Inside updateModifierGroup :: End  ============= ");
//
//				return "redirect:https://www.foodkonnekt.com";
//			}
//		}
//		LOGGER.info("===============  AdminHomeController : Inside updateModifierGroup :: End  ============= ");
//
//		return "redirect:" + environment.getProperty("BASE_URL") + "/modifierGroups";
//	}

	@RequestMapping(value = "/updateModifierGroup", method = RequestMethod.POST)
	public String updateModifierGroup(@ModelAttribute("ModifierGroup") ModifierGroup modifierGroup, ModelMap model,
			HttpServletRequest request) {
		try {
			LOGGER.info(
					"===============  AdminHomeController : Inside updateModifierGroup :: Start  ============= modifierGroup "
							+ modifierGroup.getId());

			HttpSession session = request.getSession();
			if (session != null) {
				Merchant merchant = (Merchant) session.getAttribute("merchant");
				if (merchant != null) {
					modifierService.updateModifierGroupValue(modifierGroup, merchant.getId());
				} else
					return "redirect:" + environment.getProperty("BASE_URL") + "/support";
			} else
				return "redirect:" + environment.getProperty("BASE_URL") + "/support";		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
				LOGGER.error("error: " + e.getMessage());
				LOGGER.info("===============  AdminHomeController : Inside updateModifierGroup :: End  ============= ");

				return "redirect:https://www.foodkonnekt.com";
			}
		}
		LOGGER.info("===============  AdminHomeController : Inside updateModifierGroup :: End  ============= ");

		return "redirect:" + environment.getProperty("BASE_URL") + "/modifierGroups";
	}

	@RequestMapping(value = "/updateCategoryDataMasterForm", method = RequestMethod.POST)
	public String updateCategory(@ModelAttribute("Category") CategoryItem categoryItem, ModelMap model,
			HttpServletRequest request, @RequestParam(required = false) Integer categoryId) {
		try {
			LOGGER.info(
					"===============  AdminHomeController : Inside updateCategory :: Start  =============categoryId "
							+ categoryId);

			// categoryService.updateCategoryValue(category, file);
			categoryService.updateCategoryItem(categoryItem);
		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
				LOGGER.error("error: " + e.getMessage());
				LOGGER.info("===============  AdminHomeController : Inside updateCategory :: End  ============= ");

				return "redirect:https://www.foodkonnekt.com";
			}
		}
		LOGGER.info("===============  AdminHomeController : Inside updateCategory :: End  ============= ");

		return "redirect:" + environment.getProperty("BASE_URL") + "/category";
	}

	/**
	 * Show add category page
	 * 
	 * @return String
	 */
	@RequestMapping(value = "/category", method = RequestMethod.GET)
	public String viewCategory(ModelMap model, HttpServletRequest request, HttpServletResponse response) {
		LOGGER.info("----------------Start :: AdminHomeController : category------------------");
		try {
			HttpSession session = request.getSession();
			if (session != null) {
				Merchant merchant = (Merchant) session.getAttribute("merchant");
				if (merchant != null) {
					model.addAttribute("times", DateUtil.findAllTime());
					model.addAttribute("weekDays", DateUtil.findAllWeekDays());
				} else
					return "redirect:" + environment.getProperty("BASE_URL") + "/support";
			} else
				return "redirect:" + environment.getProperty("BASE_URL") + "/support";
		} catch (Exception e) {
			LOGGER.info("----------------EXCEPTION :: AdminHomeController : category------------------" + e);
			LOGGER.error("error: " + e.getMessage());
			MailSendUtil.sendExceptionByMail(e,environment);
			return "redirect:" + environment.getProperty("BASE_URL") + "/support";
		}
		LOGGER.info("----------------END :: AdminHomeController : category------------------");
		return "category";
	}

	@RequestMapping(value = "/changeCategoryOrder", method = RequestMethod.GET)
	@ResponseBody
	public String changeCategoryOrder(ModelMap model, HttpServletRequest request, HttpServletResponse response) {
		LOGGER.info("===============  AdminHomeController : Inside changeCategoryOrder :: Start  ============= ");

		return categoryService.changeCategoryOrder();
	}

	@RequestMapping(value = "/updateCategory", method = RequestMethod.GET)
	public String viewCategory(@ModelAttribute("Category") Category category, ModelMap model,
			@RequestParam(required = false) int categoryId) {
		try {
			LOGGER.info("===============  AdminHomeController : Inside viewCategory :: Start  =============categoryId "
					+ categoryId);

			Category result = categoryService.findCategoryById(categoryId);
			model.addAttribute("categoryStatus", result.getItemStatus());
			model.addAttribute("category", result);
		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);

				LOGGER.error("error: " + e.getMessage());
				return "redirect:https://www.foodkonnekt.com";
			}
		}
		LOGGER.info("===============  AdminHomeController : Inside viewCategory :: End  ============= ");
		return "updateCategory";
	}

	@RequestMapping(value = "/updateCategoryTiming", method = RequestMethod.GET)
	@ResponseBody
	public String updateCategoryTiming(@RequestParam int categoryId, @RequestParam String days,
			@RequestParam String startTime, @RequestParam String endTime, @RequestParam Integer allowCategoryTimings) {
		try {
			LOGGER.info("===============  AdminHomeController : Inside updateCategoryTiming :: Start  ============= ");

			String result = categoryService.updateCategoryTiming(categoryId, days, startTime, endTime,
					allowCategoryTimings);
			return "success";
		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);

			}
			LOGGER.info("===============  AdminHomeController : Inside updateCategoryTiming :: End  ============= ");

			return "fail";
		}

	}

	@RequestMapping(value = "/getCategoryTiming", method = RequestMethod.GET)
	@ResponseBody
	public List<CategoryTiming> getCategoryTiming(@RequestParam int categoryId, HttpServletRequest request) {
		try {
			LOGGER.info("===============  AdminHomeController : Inside getCategoryTiming :: Start  ============= ");

			HttpSession session = request.getSession(false);
			if (session != null) {
				Merchant merchant = (Merchant) session.getAttribute("merchant");
				session.setAttribute("merchant", merchant);
			}
			List<CategoryTiming> result = categoryService.getCategoryTiming(categoryId);
			return result;
		} catch (Exception e) {
			if (e != null) {
				LOGGER.info("===============  AdminHomeController : Inside getCategoryTiming :: End  ============= ");

				MailSendUtil.sendExceptionByMail(e,environment);

			}
			return null;
		}

	}

	@RequestMapping(value = "/updateCategoryStatus", method = RequestMethod.POST)
	public String updateCategoryStatus(@ModelAttribute("Category") Category category, ModelMap model,
			HttpServletRequest request) {
		try {
			LOGGER.info("===============  AdminHomeController : Inside updateCategoryStatus :: Start  ============= ");

			categoryService.updateCategoryStatusById(category);
		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
				LOGGER.error("error: " + e.getMessage());
				return "redirect:https://www.foodkonnekt.com";
			}
		}
		LOGGER.info("===============  AdminHomeController : Inside updateCategoryStatus :: End  ============= ");

		return "redirect:" + environment.getProperty("BASE_URL") + "/category";
	}

	@RequestMapping(value = "/addCategory", method = RequestMethod.GET)
	public String viewAddCategory(ModelMap model, HttpServletRequest request, HttpServletResponse response) {
		try {
			LOGGER.info("===============  AdminHomeController : Inside viewAddCategory :: Start  ============= ");

			HttpSession session = request.getSession(false);
			if (session != null) {
				Merchant merchant = (Merchant) session.getAttribute("merchant");
				if (merchant != null)
					model.addAttribute("categories", categoryService.findAllCategory(merchant.getId()));
				else {
					return "redirect:https://www.foodkonnekt.com";
				}
			} else {
				return "redirect:https://www.foodkonnekt.com";
			}
		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
				LOGGER.error("error: " + e.getMessage());
				LOGGER.info("===============  AdminHomeController : Inside viewAddCategory :: Exception  ============= "
						+ e);

				return "redirect:https://www.foodkonnekt.com";
			}
		}
		LOGGER.info("===============  AdminHomeController : Inside viewAddCategory :: End  ============= ");

		return "addCategory";
	}

	/**
	 * Show allOrders page
	 * 
	 * @return String
	 */
	@RequestMapping(value = "/allOrders", method = RequestMethod.GET)
	public String viewAllOrders(@ModelAttribute("SearchVO") SearchVO searchVO, ModelMap model,
			HttpServletRequest request, HttpServletResponse response,
			@RequestParam(required = false) String errorMessage) {
		LOGGER.info("===============  AdminHomeController : Inside allOrders :: start  ============= ");
		try {
			HttpSession session = request.getSession();
			if (session != null) {
				Merchant merchant = (Merchant) session.getAttribute("merchant");
				if (merchant != null) {
					errorMessage = (String) session.getAttribute("errorMessage");
					session.removeAttribute("errorMessage");
					DateFormat dateFormat = new SimpleDateFormat(IConstant.YYYYMMDD);

					Date today = (merchant.getTimeZone() != null && merchant.getTimeZone().getTimeZoneCode() != null)
							? DateUtil.getCurrentDateForTimeZonee(merchant.getTimeZone().getTimeZoneCode())
							: new Date();
							
					Calendar cal = new GregorianCalendar();
					cal.setTime(today);
					cal.add(Calendar.DAY_OF_MONTH, -2);
					Date today30 = cal.getTime();
					String current = dateFormat.format(today);
					String thirty = dateFormat.format(today30);
					String endDate = current;
					String startDate = thirty;
					model.addAttribute("startDate", startDate);
					model.addAttribute("endDate", endDate);
					model.addAttribute("merchantId", merchant.getId());
					model.addAttribute("errorMessage", errorMessage);
				} else
					return "redirect:" + environment.getProperty("BASE_URL") + "/support";
			} else
				return "redirect:" + environment.getProperty("BASE_URL") + "/support";
		} catch (Exception e) {
			LOGGER.info("----------------EXCEPTION :: AdminHomeController : allOrders------------------" + e);
			MailSendUtil.sendExceptionByMail(e,environment);
			return "redirect:" + environment.getProperty("BASE_URL") + "/support";
		}
		LOGGER.info("----------------END :: AdminHomeController : allOrders------------------");
		return "allOrders";
	}

	/**
	 * Show merchant page
	 * 
	 * @return String
	 */
	@RequestMapping(value = "/merchants", method = RequestMethod.GET)
	public String viewMerchants(ModelMap model, HttpServletRequest request) {
		try {
			LOGGER.info("===============  AdminHomeController : Inside viewMerchants :: Start  ============= ");

			HttpSession session = request.getSession(false);
			if (session != null) {
				Merchant merchant = (Merchant) session.getAttribute("merchant");
				if (merchant != null)
					model.addAttribute("merchants", merchantService.findMerchantById(merchant.getId()));
				else
					return "redirect:https://www.foodkonnekt.com";
			} else {
				return "redirect:https://www.foodkonnekt.com";
			}
		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
				LOGGER.error("error: " + e.getMessage());
				LOGGER.info(
						"===============  AdminHomeController : Inside viewMerchants :: Exception  ============= " + e);

				return "redirect:https://www.foodkonnekt.com";
			}

		}
		LOGGER.info("===============  AdminHomeController : Inside viewMerchants :: End  ============= ");

		return "merchants";
	}

	/**
	 * Show modifierGroup by merchantId
	 * 
	 * @param model
	 * @return String
	 */
	@RequestMapping(value = "/modifierGroups", method = RequestMethod.GET)
	public String viewModifierGroups(ModelMap model, HttpServletRequest request) {
		LOGGER.info("----------------Start :: AdminHomeController : modifierGroups------------------");
		try {
			HttpSession session = request.getSession();
			if (session != null) {
				Merchant merchant = (Merchant) session.getAttribute("merchant");
				if (merchant != null) {

				} else
					return "redirect:" + environment.getProperty("BASE_URL") + "/support";
			} else
				return "redirect:" + environment.getProperty("BASE_URL") + "/support";
		} catch (Exception e) {
			LOGGER.info("----------------EXCEPTION :: AdminHomeController : modifierGroups------------------");
			LOGGER.error("error: " + e.getMessage());
			MailSendUtil.sendExceptionByMail(e,environment);
			return "redirect:" + environment.getProperty("BASE_URL") + "/support";
		}
		LOGGER.info("----------------END :: AdminHomeController : modifierGroups------------------");
		return "modifierGroups";
	}

	/**
	 * Find Modifiers by merchantId
	 * 
	 * @param model
	 * @return String
	 */
	@RequestMapping(value = "/modifiers", method = RequestMethod.GET)
	public String viewModifiers(ModelMap model, HttpServletRequest request, HttpServletResponse response) {
		try {
			LOGGER.info("===============  AdminHomeController : Inside viewModifiers :: Start  ============= ");

			HttpSession session = request.getSession(false);
			if (session != null) {
				Merchant merchant = (Merchant) session.getAttribute("merchant");
				if (merchant != null) {
					model.addAttribute("categories", categoryService.findAllCategory(merchant.getId()));
				} else {
					return "redirect:" + environment.getProperty("BASE_URL") + "/support";
				}
			} else {
				return "redirect:" + environment.getProperty("BASE_URL") + "/support";
			}
		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
				LOGGER.error("error: " + e.getMessage());
				return "redirect:" + environment.getProperty("BASE_URL") + "/support";
			}
		}
		LOGGER.info("===============  AdminHomeController : Inside viewModifiers :: End  ============= ");

		return "modifiers";
	}

	@RequestMapping(value = "/getAllModifiers", method = RequestMethod.GET)
	@ResponseBody
	public List<Modifiers> getModifiers(@RequestParam(required = true) Integer merchantId) {
		LOGGER.info("===============  AdminHomeController : Inside viewModifiers :: Start  ============= ");

		return modifierService.findModifierByMerchantById(merchantId);
	}

	/**
	 * find by orderType , orderStatus and orderDate
	 * 
	 * @param searchVO
	 * @param model
	 * @param request
	 * @param response
	 * @return String
	 */
	@RequestMapping(value = "/searchOrder", method = { RequestMethod.POST })
	public String searchOrders(@ModelAttribute("SearchVO") SearchVO searchVO, ModelMap model,
			HttpServletRequest request, HttpServletResponse response) {
		String startDate = null;
		String endDate = null;
		try {
			LOGGER.info("===============  AdminHomeController : Inside searchOrders :: Start  ============= ");

			if (!searchVO.getDateRange().isEmpty()) {
				JSONObject jObject = new JSONObject(searchVO.getDateRange());
				startDate = jObject.getString("start");
				endDate = jObject.getString("end");
			}
			LOGGER.info(startDate + endDate);
			if (searchVO.getOrderStatus().equals("Status") && searchVO.getDateRange() == "") {
				model.addAttribute("allOrders", orderService.findOrdersByOrderType(searchVO.getOrderType()));
			}
			if (searchVO.getOrderType().equals("Type") && searchVO.getDateRange() == "") {
				model.addAttribute("allOrders", orderService.findOrdersByStatus(searchVO.getOrderStatus()));
			}
			if (searchVO.getOrderStatus().equals("Status") && searchVO.getOrderType().equals("Type")) {
				model.addAttribute("allOrders", orderService.findByOrderDate(startDate, endDate));
			}
			if (!searchVO.getOrderStatus().equals("Status") && !searchVO.getOrderType().equals("Type")
					&& searchVO.getDateRange() != "") {
				model.addAttribute("allOrders", orderService.findOrdersByStatusAndOrderTypeAndDateRange(
						searchVO.getOrderStatus(), searchVO.getOrderType(), startDate, endDate));
			}
			if (!searchVO.getOrderStatus().equals("Status") && !searchVO.getOrderType().equals("Type")
					&& searchVO.getDateRange() == "") {
				model.addAttribute("allOrders", orderService.findOrdersByStatusAndOrderType(searchVO.getOrderStatus(),
						searchVO.getOrderType()));
			}
			if (searchVO.getOrderStatus().equals("Status") && !searchVO.getOrderType().equals("Type")
					&& searchVO.getDateRange() != "") {
				model.addAttribute("allOrders",
						orderService.findByOrderTypeAndOrderDate(searchVO.getOrderType(), startDate, endDate));
			}
			if (!searchVO.getOrderStatus().equals("Status") && searchVO.getOrderType().equals("Type")
					&& searchVO.getDateRange() != "") {
				model.addAttribute("allOrders",
						orderService.findByOrderStatusAndOrderDate(searchVO.getOrderStatus(), startDate, endDate));
			}
		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
				LOGGER.info(
						"===============  AdminHomeController : Inside searchOrders :: EXception  ============= " + e);

				LOGGER.error("error: " + e.getMessage());
				return "redirect:https://www.foodkonnekt.com";
			}
		}
		LOGGER.info("===============  AdminHomeController : Inside searchOrders :: End  ============= ");

		return "allOrders";
	}

	@RequestMapping(value = "/onLineOrderLink", method = RequestMethod.GET)
	public String viewOnlineOrderLink(@ModelAttribute("OpenHours") OpenHours openHours, ModelMap model,
			HttpServletRequest request) {
		try {
			LOGGER.info("===============  AdminHomeController : Inside viewOnlineOrderLink :: Starts  ============= ");

			HttpSession session = request.getSession(false);
			if (session != null) {
				Merchant merchant = (Merchant) session.getAttribute("merchant");
				if (merchant != null) {

					boolean isFacebookTabActive = false;
					if (merchant.getfBTabIds() != null) {
						String faceBookKJson = ProducerUtil.getFaceBookTabs(merchant.getfBTabIds());
						JSONObject jObject = new JSONObject(faceBookKJson);
						if (jObject != null && jObject.has("data")) {
							JSONArray jsonArray = jObject.getJSONArray("data");
							for (Object object : jsonArray) {
								JSONObject object2 = (JSONObject) object;
								if (object2 != null && object2.has("image_url") && object2.has("application")) {
									JSONObject object3 = object2.getJSONObject("application");
									if (object3 != null && object3.has("id")) {
										String appId = object3.getString("id");
										if (appId != null && appId.equals("608947722608987")) {
											isFacebookTabActive = true;
										}
									}
								}
							}
						}
						if (!isFacebookTabActive) {
							merchant.setIsFBAppPublished(isFacebookTabActive);
							merchant.setfBTabIds(null);
							merchant = merchantService.save(merchant);
							session.setAttribute("merchant", merchant);
						}
					}

					if (merchant.getOwner() != null && merchant.getOwner().getPos() != null
							&& merchant.getOwner().getPos().getPosId() != null)
						session.setAttribute("merchantType", merchant.getOwner().getPos().getPosId());

					String onlineOrderLink = (String) session.getAttribute("onlineOrderLink");
					model.addAttribute("onlineOrderLink", onlineOrderLink);

					// link for theme2 start
					String merchId = merchant.getId().toString();
					String base64encode = EncryptionDecryptionUtil.encryption(merchId);
					String merchantName = merchant.getName().replaceAll("[^a-zA-Z0-9]", "");
					String orderLink = null;

					Integer merchantID = Integer.parseInt(merchId);

					/*
					 * if(merchantID.equals(IConstant.TEXAN_REWARDS_MERCHANT_ID)) { orderLink =
					 * environment.getProperty("TEXAN_BASE_URL") + "/" + merchantName + "/cloverV2/" +
					 * base64encode; }else{
					 */
					if (merchant.getOwner().getPos().getPosId() == 1) {
						orderLink = environment.getProperty("WEB_BASE_URL") + "/" + merchantName + "/cloverV2/" + base64encode;
					} else if (merchant.getOwner().getPos().getPosId() == 2) {
						orderLink = environment.getProperty("WEB_BASE_URL") + "/" + merchantName + "/foodtronixV2/" + base64encode;
					} else if (merchant.getOwner().getPos().getPosId() == 3) {
						orderLink = environment.getProperty("WEB_BASE_URL") + "/" + merchantName + "/non-posV2/" + base64encode;
					} else if (merchant.getOwner().getPos().getPosId() == 4) {
						orderLink = environment.getProperty("WEB_BASE_URL") + "/" + merchantName + "/focusV2/" + base64encode;
					} else {
						orderLink = environment.getProperty("WEB_BASE_URL") + "/" + merchantName + "/cloverV2/" + base64encode;
					}

					// String orderLink = environment.getProperty("WEB_BASE_URL") + "/" +
					// merchantName + "/cloverV2/" + base64encode;

					model.addAttribute("onlineOrderLinkV2", orderLink);
					// link for theme2 end
					// Find business hours by merchantId
					model.addAttribute("businessHours", businessService.findBusinessHourByMerchantId(merchant.getId()));
					
					List<String> businessHours = (environment.getProperty("environment").equals("Production")) ? DateUtil.findAllTime() : DateUtil.findBusinessHours();
					model.addAttribute("times", businessHours);
					// Find location by merchantId
					model.addAttribute("location", businessService.findLocationByMerchantId(merchant.getId()));
					model.addAttribute("convenienceFee",
							businessService.findConvenienceFeeByMerchantId(merchant.getId()));

					model.addAttribute("pickupTime", businessService.findPickUpTimeByMerchantId(merchant.getId()));

					//model.addAttribute("times", DateUtil.findAllTime());
					LOGGER.info("AllowFutureOrder --> " + merchant.getAllowFutureOrder());
					model.addAttribute("allowFutureOrder", merchant.getAllowFutureOrder());

					LOGGER.info("website --> " + merchant.getWebsite());
					model.addAttribute("website", merchant.getWebsite());

					LOGGER.info("AllowMultipleKoupon --> " + merchant.getAllowMultipleKoupon());
					model.addAttribute("allowMultipleKoupon", merchant.getAllowMultipleKoupon());
					model.addAttribute("activeCustomerFeedback", merchant.getActiveCustomerFeedback());

					model.addAttribute("futureDaysAhead", merchant.getFutureDaysAhead());
					model.addAttribute("tipsForDilevery", merchant.getTipsForDilevery());
					model.addAttribute("tipsForPickup", merchant.getTipsForPickup());
					model.addAttribute("isActiveTipsForPickup", merchant.isActiveTipsForPickup());
					model.addAttribute("isActiveTipForDilevery", merchant.isActiveTipForDilevery());
					model.addAttribute("activeCustomerFeedback", merchant.getActiveCustomerFeedback());
					List<Category> categories = categoryService.findAllCategory(merchant.getId());
					List<TaxRates> taxRates = merchantService.findAllTaxRatesByMerchantId(merchant.getId());
					if (taxRates != null && taxRates.size() > 0) {
						model.addAttribute("isTaxAvailable", true);
					} else {
						model.addAttribute("isTaxAvailable", false);
					}

					SocialMediaLinks socialMediaLinks = merchantService
							.getSocialMediaLinksByMerchantId(merchant.getId());
					openHours.setSocialMediaLinks(socialMediaLinks);
					String linkActiveCustomerFeedback = merchantService
							.generateLinkActiveCustomerFeedback(merchant.getId());
					model.addAttribute("linkActiveCustomerFeedback", linkActiveCustomerFeedback);

					model.addAttribute("SocialMediaLinks", socialMediaLinks);

					if (categories != null && categories.size() > 0) {
						session.setAttribute("inventoryThread", 1);
					} else {
						session.setAttribute("inventoryThread", 0);
					}

					PaymentMode cashMode = businessService.findByMerchantIdAndLabel(merchant.getId(), "Cash");
					PaymentMode creditCardMode = businessService.findByMerchantIdAndLabel(merchant.getId(),
							"Credit Card");
					LOGGER.info(
							"===============  AdminHomeController : Inside viewOnlineOrderLink :: merchantId  ============= "
									+ merchant.getId());

					MerchantSubscriptionLevel merchantSubscriptionLevel = merchantSubscriptionLevelRepository
							.findByMerchantIdAndActive(merchant.getId(), 1);
					if (merchantSubscriptionLevel != null && merchantSubscriptionLevel.getSubscriptionLevel() != null)
						model.addAttribute("merchantSubscriptionLevel",
								merchantSubscriptionLevel.getSubscriptionLevel());
					else
						model.addAttribute("merchantSubscriptionLevel", "");
					if (cashMode != null)
						model.addAttribute("cash", cashMode.getAllowPaymentMode());

					if (creditCardMode != null) {
						model.addAttribute("creditcard", creditCardMode.getAllowPaymentMode());
						if (creditCardMode.getMinOrderAmount() != null && creditCardMode.getMinOrderAmount() > 0) {
							model.addAttribute("ccMinOrder", creditCardMode.getMinOrderAmount());
						} else {
							model.addAttribute("ccMinOrder", 0.0);
						}
					}
					
					List<TimeZone> timezone = new ArrayList<TimeZone>();
					 
					 List<TimeZone> dbtimezone = timeZoneRepository.findAll();
					
//					if(!(dbtimezone.size()>0)) {
//						dbtimezone = merchantService.saveTimeZones();
//					}
					
					for(TimeZone time : dbtimezone) {
						java.util.TimeZone tz = java.util.TimeZone.getTimeZone(time.getTimeZoneCode());
						long hours = TimeUnit.MILLISECONDS.toHours(tz.getRawOffset());
						long minutes = TimeUnit.MILLISECONDS.toMinutes(tz.getRawOffset())
				                                  - TimeUnit.HOURS.toMinutes(hours);
						// avoid -4:-30 issue
						minutes = Math.abs(minutes);

						String timeZoneCode = "";
						if (hours > 0) {
							timeZoneCode = String.format("(GMT+%d:%02d) %s", hours, minutes, tz.getID());
						} else {
							timeZoneCode = String.format("(GMT%d:%02d) %s", hours, minutes, tz.getID());
						}
						time.setTimeZoneCode(timeZoneCode);
						
						if(merchant.getTimeZone()!= null) {
							if(!time.getId().equals(merchant.getTimeZone().getId())) {
								timezone.add(time);
							}
						}else {
						timezone.add(time);
						}
						}
					model.addAttribute("timezone", timezone);
					
					model.addAttribute("merchantTimezone", merchant.getTimeZone());
					
				} else {
					return "redirect:" + environment.getProperty("BASE_URL") + "/support";
				}
			} else {
				return "redirect:" + environment.getProperty("BASE_URL") + "/support";
			}
		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
				LOGGER.error("error: " + e.getMessage());
				LOGGER.info(
						"===============  AdminHomeController : Inside viewOnlineOrderLink :: Exception  ============= "
								+ e);

				return "redirect:" + environment.getProperty("BASE_URL") + "/support";
			}
		}
		LOGGER.info("===============  AdminHomeController : Inside viewOnlineOrderLink :: End  ============= ");

		return "onLineOrderLink";
	}

	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public void logOutRedirect(ModelMap model, HttpServletRequest request, HttpServletResponse response) {
		LOGGER.info("===============  AdminHomeController : Inside logOutRedirect :: Starts  ============= ");

		HttpSession session = request.getSession(false);
		session.removeAttribute("merchant");
		session.invalidate();
		try {
			response.sendRedirect("" + environment.getProperty("BASE_URL") + "/support");
		} catch (IOException e) {
			LOGGER.error("error: " + e.getMessage());
		}
	}

	@RequestMapping(value = "/adminLogout", method = RequestMethod.GET)
	public void adminLogout(ModelMap model, HttpServletRequest request, HttpServletResponse response) {
		LOGGER.info("===============  AdminHomeController : Inside adminLogout :: Starts  ============= ");

		HttpSession session = request.getSession(false);
		session.removeAttribute("foodkonnektAdmin");
		session.invalidate();
		try {
			response.sendRedirect("" + environment.getProperty("BASE_URL") + "/support");
		} catch (IOException e) {
			LOGGER.error("error: " + e.getMessage());
		}
	}

	/**
	 * find customer orders
	 * 
	 * @param model
	 * @param request
	 * @param customerId
	 * @return String
	 */
	@RequestMapping(value = "/customerOrders", method = RequestMethod.GET)
	public String viewCustomerOrders(@ModelAttribute("SearchVO") SearchVO searchVO, ModelMap model,
			HttpServletRequest request, @RequestParam(required = false) int customerId) {
		try {
			LOGGER.info(
					"===============  AdminHomeController : Inside viewCustomerOrders :: Starts  =============customerId "
							+ customerId);

			model.addAttribute("allOrders", orderService.findOrderByCustomerId(customerId));
		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);

				LOGGER.error("error: " + e.getMessage());
				return "redirect:https://www.foodkonnekt.com";
			}
		}
		LOGGER.info("===============  AdminHomeController : Inside viewCustomerOrders :: Ends  ============= ");

		return "customerOrders";
	}

	/*
	 * @RequestMapping(value = "/getAllMerchants", method = RequestMethod.GET)
	 * public String getAllMerchants(@ModelAttribute("SearchVO") SearchVO searchVO,
	 * ModelMap model, HttpServletRequest request) { try { HttpSession session =
	 * request.getSession(); if(session!=null){ Customer
	 * customer=(Customer)session.getAttribute("foodkonnektAdmin"); Boolean
	 * isFoodkonnektAdmin=(Boolean)session.getAttribute("isFoodkonnektAdmin");
	 * if(customer!=null){ if(isFoodkonnektAdmin!=null){ if(isFoodkonnektAdmin){
	 * List<Merchant> merchants=merchantService.findAllMerchants();
	 * model.addAttribute("allMerchants",merchants ); return "getAllMerchants";
	 * }else{ if(customer.getMerchantt()!=null &&
	 * customer.getMerchantt().getOwner()!=null &&
	 * customer.getMerchantt().getOwner().getId()!=null){ List<Merchant>
	 * merchants=merchantService.findAllMerchantsByVendorId(customer.
	 * getMerchantt().getOwner().getId());
	 * 
	 * model.addAttribute("allMerchants",merchants ); } return
	 * "getAllMerchantsByVendor"; } }
	 * 
	 * } } return "redirect:" + environment.getProperty("BASE_URL") + "/support"; } catch
	 * (Exception e) { if (e != null) { MailSendUtil.sendExceptionByMail(e,environment);
	 * 
	 * LOGGER.error("error: " + e.getMessage()); } return "redirect:" + environment.getProperty("BASE_URL") +
	 * "/support"; }
	 * 
	 * }
	 */

	@RequestMapping(value = "/getAllMerchants", method = RequestMethod.GET)
	public String getAllMerchants(@ModelAttribute("SearchVO") SearchVO searchVO, ModelMap model,
			HttpServletRequest request,@RequestParam(required = false) String viewType) {
		if(viewType == null) viewType = "Detail";
		try {
			LOGGER.info("===============  AdminHomeController : Inside getAllMerchants :: Starts  ============= ");

			model.addAttribute("adminUrl", environment.getProperty("BASE_URL"));
			HttpSession session = request.getSession();
			if (session != null) {
				Customer customer = (Customer) session.getAttribute("foodkonnektAdmin");
				Boolean isFoodkonnektAdmin = (Boolean) session.getAttribute("isFoodkonnektAdmin");
				if (customer != null) {
					if (isFoodkonnektAdmin != null) {
						if (isFoodkonnektAdmin) {

							if (searchVO.getStartDate() == null && searchVO.getEndDate() == null) {
								DateFormat dateFormat = new SimpleDateFormat(IConstant.YYYYMMDD);

								Date today = (customer.getMerchantt()!=null && customer.getMerchantt().getTimeZone() !=null 
										&& customer.getMerchantt().getTimeZone().getTimeZoneCode()!=null)
										? DateUtil.getCurrentDateForTimeZonee(customer.getMerchantt().getTimeZone().getTimeZoneCode())
												: new Date();
								Calendar cal = new GregorianCalendar();
								cal.setTime(today);
								cal.add(Calendar.DAY_OF_MONTH, -29);
								Date today30 = cal.getTime();
								String current = dateFormat.format(today);
								String thirty = dateFormat.format(today30);
								searchVO.setEndDate(current);

								searchVO.setStartDate(thirty);

								LOGGER.info("Current Date   " + searchVO.getEndDate());
								LOGGER.info("thirty Date   " + searchVO.getStartDate());
							}
							List<Merchant> merchants = merchantService.findAllMerchants(searchVO.getStartDate(),
									searchVO.getEndDate(),viewType);
							model.addAttribute("SearchVO", searchVO);
							model.addAttribute("allMerchants", merchants);
							model.addAttribute("viewType", viewType);
							return "getAllMerchants";

						}

						else {
							if (customer.getMerchantt() != null && customer.getMerchantt().getOwner() != null
									&& customer.getMerchantt().getOwner().getId() != null) {

								if (searchVO.getStartDate() == null && searchVO.getEndDate() == null) {
									DateFormat dateFormat = new SimpleDateFormat(IConstant.YYYYMMDD);

									Date today = (customer.getMerchantt()!=null && customer.getMerchantt().getTimeZone() !=null 
											&& customer.getMerchantt().getTimeZone().getTimeZoneCode()!=null)
											? DateUtil.getCurrentDateForTimeZonee(customer.getMerchantt().getTimeZone().getTimeZoneCode())
													: new Date();
									Calendar cal = new GregorianCalendar();
									cal.setTime(today);
									cal.add(Calendar.DAY_OF_MONTH, -29);
									Date today30 = cal.getTime();
									String current = dateFormat.format(today);
									String thirty = dateFormat.format(today30);
									searchVO.setEndDate(current);

									searchVO.setStartDate(thirty);

								}

								List<Merchant> merchants = merchantService.findAllMerchantsByVendorIdAndDateRange(
										customer.getMerchantt().getOwner().getId(), searchVO.getStartDate(),
										searchVO.getEndDate());

								model.addAttribute("allMerchants", merchants);
							}
							LOGGER.info(
									"===============  AdminHomeController : Inside getAllMerchants :: End  ============= ");

							return "getAllMerchantsByVendor";
						}
					}
				}
			}
			LOGGER.info("===============  AdminHomeController : Inside getAllMerchants :: End  ============= ");

			return "redirect:" + environment.getProperty("BASE_URL") + "/support";
		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
				LOGGER.info("===============  AdminHomeController : Inside getAllMerchants :: Exception  ============= "
						+ e);

				LOGGER.error("error: " + e.getMessage());
			}
			LOGGER.info("===============  AdminHomeController : Inside getAllMerchants :: End  ============= ");

			return "redirect:" + environment.getProperty("BASE_URL") + "/support";
		}

	}

	@RequestMapping(value = "/kritiqMerchantDetail", method = RequestMethod.GET)
	public String kritiqMerchantDetail(@ModelAttribute("SearchVO") SearchVO searchVO, ModelMap model,
			HttpServletRequest request) {
		try {
			LOGGER.info("===============  AdminHomeController : Inside kritiqMerchantDetail :: Starts  ============= ");

			HttpSession session = request.getSession();
			if (session != null) {
				Customer customer = (Customer) session.getAttribute("foodkonnektAdmin");
				Boolean isFoodkonnektAdmin = (Boolean) session.getAttribute("isFoodkonnektAdmin");
				if (customer != null) {
					if (isFoodkonnektAdmin != null) {
						/* if(isFoodkonnektAdmin){ */

						if (searchVO.getStartDate() == null && searchVO.getEndDate() == null) {
							DateFormat dateFormat = new SimpleDateFormat(IConstant.YYYYMMDD);

							Date today = (customer.getMerchantt()!=null && customer.getMerchantt().getTimeZone() !=null 
									&& customer.getMerchantt().getTimeZone().getTimeZoneCode()!=null)
									? DateUtil.getCurrentDateForTimeZonee(customer.getMerchantt().getTimeZone().getTimeZoneCode())
											: new Date();
							Calendar cal = new GregorianCalendar();
							cal.setTime(today);
							cal.add(Calendar.DAY_OF_MONTH, -29);
							Date today30 = cal.getTime();
							String current = dateFormat.format(today);
							String thirty = dateFormat.format(today30);
							searchVO.setEndDate(current);

							searchVO.setStartDate(thirty);

							LOGGER.info("Current Date   " + searchVO.getEndDate());
							LOGGER.info("thirty Date   " + searchVO.getStartDate());
						}
						Integer vendorId = null;
						if (customer.getMerchantt() != null && customer.getMerchantt().getOwner() != null
								&& customer.getMerchantt().getOwner().getId() != null) {
							vendorId = customer.getMerchantt().getOwner().getId();
						}
						List<KritiqMerchantVO> kritiqMerchantVOs = kritiqService.getKritiqMerchantDetail(
								searchVO.getStartDate(), searchVO.getEndDate(), isFoodkonnektAdmin, vendorId);
						model.addAttribute("SearchVO", searchVO);
						model.addAttribute("startDate", searchVO.getStartDate());
						model.addAttribute("endDate", searchVO.getEndDate());
						model.addAttribute("kritiqMerchantVOs", kritiqMerchantVOs);

						LOGGER.info(
								"===============  AdminHomeController : Inside kritiqMerchantDetail :: End  ============= ");

						return "kritiqMerchantDetail";
						/* } */

					}
				}
			}
			LOGGER.info("===============  AdminHomeController : Inside kritiqMerchantDetail :: End  ============= ");

			return "redirect:" + environment.getProperty("BASE_URL") + "/support";
		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
				LOGGER.info(
						"===============  AdminHomeController : Inside kritiqMerchantDetail :: Exception  ============= "
								+ e);

				LOGGER.error("error: " + e.getMessage());
			}
			return "redirect:" + environment.getProperty("BASE_URL") + "/support";
		}

	}

	@RequestMapping(value = "/kritiqCustomerDetail", method = RequestMethod.GET)
	public String kritiqCustomerDetail(@ModelAttribute("SearchVO") SearchVO searchVO, Integer merchantId,
			ModelMap model, HttpServletRequest request) {
		try {
			LOGGER.info("===============  AdminHomeController : Inside kritiqCustomerDetail :: Starts  ============= ");

			HttpSession session = request.getSession();
			if (session != null) {
				Customer customer = (Customer) session.getAttribute("foodkonnektAdmin");
				Boolean isFoodkonnektAdmin = (Boolean) session.getAttribute("isFoodkonnektAdmin");
				if (customer != null) {
					if (isFoodkonnektAdmin != null) {
						/* if(isFoodkonnektAdmin){ */

						if (searchVO.getStartDate() == null && searchVO.getEndDate() == null) {
							DateFormat dateFormat = new SimpleDateFormat(IConstant.YYYYMMDD);

							Date today = (customer.getMerchantt()!=null && customer.getMerchantt().getTimeZone() !=null 
									&& customer.getMerchantt().getTimeZone().getTimeZoneCode()!=null)
									? DateUtil.getCurrentDateForTimeZonee(customer.getMerchantt().getTimeZone().getTimeZoneCode())
											: new Date();
							Calendar cal = new GregorianCalendar();
							cal.setTime(today);
							cal.add(Calendar.DAY_OF_MONTH, -29);
							Date today30 = cal.getTime();
							String current = dateFormat.format(today);
							String thirty = dateFormat.format(today30);
							searchVO.setEndDate(current);

							searchVO.setStartDate(thirty);

							LOGGER.info("Current Date   " + searchVO.getEndDate());
							LOGGER.info("thirty Date   " + searchVO.getStartDate());
						}
						Merchant merchant = merchantService.findByMerchantId(merchantId);
						List<KritiqCustomerVO> kritiqCustomerVOs = kritiqService.getKritiqCustomerDetail(merchantId,
								searchVO.getStartDate(), searchVO.getEndDate());

						model.addAttribute("SearchVO", searchVO);
						if (merchant != null)
							model.addAttribute("merchantName", merchant.getName());

						model.addAttribute("kritiqCustomerVOs", kritiqCustomerVOs);

						LOGGER.info(
								"===============  AdminHomeController : Inside kritiqCustomerDetail :: Ends  ============= ");

						return "kritiqCustomerDetail";
						/* } */

					}
				}
			}
			LOGGER.info("===============  AdminHomeController : Inside kritiqCustomerDetail :: Ends  ============= ");

			return "redirect:" + environment.getProperty("BASE_URL") + "/support";
		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
				LOGGER.info(
						"===============  AdminHomeController : Inside kritiqCustomerDetail :: Exception  ============= "
								+ e);

				LOGGER.error("error: " + e.getMessage());
			}
			LOGGER.info("===============  AdminHomeController : Inside kritiqCustomerDetail :: Ends  ============= ");

			return "redirect:" + environment.getProperty("BASE_URL") + "/support";
		}

	}

	@RequestMapping(value = "/saveBusinessLogo", method = RequestMethod.POST)
	public String save(@ModelAttribute("OpenHours") OpenHours openHours, ModelMap model, HttpServletResponse response,
			HttpServletRequest request, @RequestParam("file") MultipartFile file) {

		try {
			LOGGER.info("===============  AdminHomeController : Inside save :: Starts  =============openHours "
					+ openHours.getId());

			HttpSession session = request.getSession(false);
			if (session != null) {
				Merchant merchant = (Merchant) session.getAttribute("merchant");
				if (merchant != null) {
					// merchant.getId();
					merchant = merchantService.findById(merchant.getId());
					if (!(file.getSize() == 0)) {
						// images
						Long size = file.getSize();
						if (size >= 2097152) {
							model.addAttribute("filesize", "Maximum Allowed File Size is 2 MB");
							return "uploadLogo";
						}

						String orgName = file.getOriginalFilename();

						String exts[] = orgName.split("\\.");

						String ext = exts[1];
						String logoName = merchant.getId() + "_" + merchant.getName().replaceAll("[^a-zA-Z0-9]", "")
								+ "." + ext;
						String filePath = environment.getProperty("ADMIN_SERVER_LOGO_PATH") + logoName;
						File dest = new File(filePath);
						try {
							file.transferTo(dest);
						} catch (IllegalStateException e) {
							LOGGER.error("error: " + e.getMessage());
							return "File uploaded failed:" + orgName;
						} catch (IOException e) {
							LOGGER.error("error: " + e.getMessage());
							return "File uploaded failed:" + orgName;
						}
						LOGGER.info("File uploaded:" + orgName);

						// String image = ImageUploadUtils.getImage(file);
						merchant.setMerchantLogo(environment.getProperty("ADMIN_LOGO_PATH_TO_SHOW") + logoName);
					}

					if (openHours.getActiveCustomerFeedback() != null) {
						merchant.setActiveCustomerFeedback(openHours.getActiveCustomerFeedback());
					} else {
						merchant.setActiveCustomerFeedback(IConstant.BOOLEAN_FALSE);
					}

					if (openHours.getAllowFutureOrder() != null) {

						merchant.setAllowFutureOrder(openHours.getAllowFutureOrder());

						if (merchant.getOwner() != null && merchant.getOwner().getPos() != null
								&& merchant.getOwner().getPos().getPosId() != null
								&& merchant.getOwner().getPos().getPosId() == IConstant.POS_CLOVER) {
							if (openHours.getAllowFutureOrder() == IConstant.BOOLEAN_TRUE) {

								CloverUrlUtil.enableFuturOrderOnClover("Yes", merchant,environment);
							} else {
								CloverUrlUtil.enableFuturOrderOnClover("No", merchant,environment);
							}
						}
					} else {
						merchant.setAllowFutureOrder(IConstant.BOOLEAN_FALSE);
					}

					if (openHours.getWebSite() != null) {
						merchant.setWebsite(openHours.getWebSite());
					}

					if (openHours.getFutureDaysAhead() != null) {
						merchant.setFutureDaysAhead(openHours.getFutureDaysAhead());
					}
					if (openHours.getAllowMultipleKoupon() != null) {
						LOGGER.info("allow multiple koupon-->" + merchant.getAllowMultipleKoupon());
						merchant.setAllowMultipleKoupon(openHours.getAllowMultipleKoupon());
					}
					merchant.setAllowFutureOrder(openHours.getAllowFutureOrder());
					if (openHours.getAllowReOrder() != null) {
						merchant.setAllowReOrder(openHours.getAllowReOrder());
					}

					TimeZone zone = timeZoneRepository.findOne(openHours.getTimezoneId());
					
					merchant.setTimeZone(zone);
					
					merchantService.save(merchant);

					if (openHours.getSocialMediaLinks() != null) {
						SocialMediaLinks oldSocialMediaLinks = merchantService
								.getSocialMediaLinksByMerchantId(merchant.getId());
						SocialMediaLinks socialMediaLinks = openHours.getSocialMediaLinks();
						if (oldSocialMediaLinks != null) {
							socialMediaLinks.setId(oldSocialMediaLinks.getId());
						}

						socialMediaLinks.setMerchant(merchant);

						socialMediaLinks = merchantService.saveSocialMediaLinks(socialMediaLinks);

						merchant.setSocialMediaLinks(socialMediaLinks);
					}

					session.setAttribute("merchant", merchant);
					PaymentMode cashMode = businessService.findByMerchantIdAndLabel(merchant.getId(), "Cash");
					PaymentMode creditCardMode = businessService.findByMerchantIdAndLabel(merchant.getId(),
							"Credit Card");
					if (cashMode != null) {
						cashMode.setAllowPaymentMode(0);
						businessService.savePaymentMode(cashMode);
					}

					if (creditCardMode != null) {
						creditCardMode.setAllowPaymentMode(0);

						businessService.savePaymentMode(creditCardMode);
					}
					if (openHours.getAllowPaymentModes() != null) {
						String[] paymentModes = openHours.getAllowPaymentModes().split(",");
						for (String paymentMode : paymentModes) {
							PaymentMode mode = businessService.findByMerchantIdAndLabel(merchant.getId(), paymentMode);
							if (mode != null) {
								mode.setAllowPaymentMode(1);
								if (paymentMode != null && paymentMode.equals("Credit Card")) {
									mode.setMinOrderAmount(openHours.getcCMinOrder());
								}
								businessService.savePaymentMode(mode);
							}
						}
					}

					ConvenienceFee convenienceFee = businessService.findConvenienceFeeByMerchantId(merchant.getId());
					if (convenienceFee == null) {
						convenienceFee = new ConvenienceFee();
					}
					convenienceFee.setLocationId(openHours.getLocationId());
					convenienceFee.setConvenienceFee(openHours.getConvenienceFee());
					convenienceFee.setConvenienceFeePercent(openHours.getConvenienceFeePercent());
					convenienceFee.setIsTaxable(openHours.getIsTaxable());
					convenienceFee.setMerchantId(merchant.getId());
					merchantService.saveConvenienceFee(convenienceFee, merchant);

					/*
					 * if (openHours.getWebsite() != null) {
					 * 
					 * merchant.setWebsite(openHours.getWebsite());
					 * 
					 * merchantService.save(merchant);}
					 */

					PickUpTime pickUpTime = businessService.findPickUpTimeByMerchantId(merchant.getId());
					if (pickUpTime == null) {
						pickUpTime = new PickUpTime();
					}
					pickUpTime.setLocationId(openHours.getLocationId());
					pickUpTime.setMerchantId(merchant.getId());
					pickUpTime.setPickUpTime(openHours.getPickUpTiime());
					pickUpTime.setMinOrderAmount(openHours.getPickUpMinOrder());
					merchantService.savePickupTime(pickUpTime);

					if (openHours.getsTimeToSave() != null && openHours.getSelectedDay() != null) {
						businessService.updateBusinessHour(openHours.getsTimeToSave(), openHours.geteTimeToSave(),
								openHours.getSelectedDay());
					}
					LOGGER.info("---" + openHours.getsTimeToSave());
					LOGGER.info("---" + openHours.geteTimeToSave());

					if (openHours.getTipsForDilevery() != null) {
						merchant.setTipsForDilevery(openHours.getTipsForDilevery());
					}
					if (openHours.getTipsForPickup() != null) {
						merchant.setTipsForPickup(openHours.getTipsForPickup());
					}
					merchant.setActiveTipForDilevery(openHours.getActiveTipForDilevery());
					merchant.setActiveTipsForPickup(openHours.getActiveTipsForPickup());
					merchantService.save(merchant);

				} else {
					LOGGER.info("===============  AdminHomeController : Inside save :: End  ============= ");

					return "redirect:https://www.foodkonnekt.com";
				}
			} else {
				LOGGER.info("===============  AdminHomeController : Inside save :: End  ============= ");

				return "redirect:https://www.foodkonnekt.com";
			}
		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
				LOGGER.info("===============  AdminHomeController : Inside save :: Exception  ============= " + e);

				LOGGER.error("error: " + e.getMessage());
				return "redirect:https://www.foodkonnekt.com";
			}
		}
		LOGGER.info("===============  AdminHomeController : Inside save :: End  ============= ");

		return "redirect:" + environment.getProperty("BASE_URL") + "/onLineOrderLink";
	}

	/**
	 * Find by categoryId
	 * 
	 * @param categoryId
	 * @return List<Item>
	 */
	@RequestMapping(value = "/itemByCategoryId", method = RequestMethod.GET)
	@ResponseBody
	public List<Item> findItemByCategory(@RequestParam(required = true) Integer categoryId) {
		LOGGER.info("===============  AdminHomeController : Inside findItemByCategory :: Start  ============= ");
	
		return itemService.findByCategoryId(categoryId);
	}

	/**
	 * Find by itemId
	 * 
	 * @param itemId
	 * @return List<Modifiers>
	 */
	@RequestMapping(value = "/modifiersByItemId", method = RequestMethod.GET)
	@ResponseBody
	public List<Modifiers> findModifierByItem(@RequestParam(required = true) Integer itemId) {
		LOGGER.info("===============  AdminHomeController : Inside findModifierByItem :: Start  ============= ");

		return categoryService.findByItemId(itemId);
	}

	/**
	 * Find item by categoryId
	 * 
	 * @param model
	 * @param request
	 * @param response
	 * @param categoryId
	 * @return String
	 */
	@RequestMapping(value = "/findItemsByCategoryId", method = RequestMethod.GET)
	public String viewMerchants(ModelMap model, HttpServletRequest request, HttpServletResponse response,
			@RequestParam(required = true) Integer categoryId) {
		try {
			LOGGER.info("===============  AdminHomeController : Inside viewMerchants :: Start  ============= ");

			HttpSession session = request.getSession(false);
			if (session != null) {
				Merchant merchant = (Merchant) session.getAttribute("merchant");
				if (merchant != null) {
					Category category = categoryService.findCategoryById(categoryId);
					/*
					 * List <CategoryItem> category =
					 * itemService.findCategoryByInSortedOrder(categoryId); for(CategoryItem
					 * c:category) { LOGGER.info("name-"+ c.getCategory().getName()+" sortorder-"
					 * +c.getSortOrder()+" getPrice- " +c.getItem().getPrice()+" "
					 * +c.getItem().getModifierGroups( )+" "+c.getItem().getMenuOrder());
					 * 
					 * }
					 */

					// LOGGER.info(category.getName());
					model.addAttribute("category", category);
					model.addAttribute("categories", categoryService.findAllCategory(merchant.getId()));
				} else {
					LOGGER.info("===============  AdminHomeController : Inside viewMerchants :: End  ============= ");

					return "redirect:https://www.foodkonnekt.com";
				}
			} else {
				LOGGER.info("===============  AdminHomeController : Inside viewMerchants :: End  ============= ");

				return "redirect:https://www.foodkonnekt.com";
			}
		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
				LOGGER.error("error: " + e.getMessage());
				return "redirect:https://www.foodkonnekt.com";
			}
		}
		LOGGER.info("===============  AdminHomeController : Inside viewMerchants :: End  ============= ");

		return "categoryItems";
	}

	@RequestMapping(value = "/updateCategoryStatusById", method = RequestMethod.GET)
	@ResponseBody
	public String updateCatStatus(@RequestParam(required = true) Integer categoryId,
			@RequestParam(required = true) Integer categoryStatus) {
		try {
			LOGGER.info("===============  AdminHomeController : Inside updateCatStatus :: Start  ============= ");

			Category category = new Category();
			category.setId(categoryId);
			category.setItemStatus(categoryStatus);
			return categoryService.updateCategoryStatusById(category);
		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
				LOGGER.error("error: " + e.getMessage());

			}
			LOGGER.info("===============  AdminHomeController : Inside updateCatStatus :: End  ============= ");

			return "redirect:https://www.foodkonnekt.com";
		}
		// return "succuss";
	}

	@RequestMapping(value = "/updateCategorySortOrderById", method = RequestMethod.GET)
	@ResponseBody
	public String updateCategorySortOrderById(@RequestParam(required = true) Integer categoryId,
			@RequestParam(required = true) Integer sortOrder, @RequestParam(required = true) String action) {
		try {
			LOGGER.info(
					"===============  AdminHomeController : Inside updateCategorySortOrderById :: Start  ============= ");

			Category category = new Category();
			category.setId(categoryId);
			category.setSortOrder(sortOrder);
			boolean status = categoryService.updateCategorySortOrderById(category, action);
			if (status)
				return "succuss";
			else
				return "failed";

		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
				LOGGER.error("error: " + e.getMessage());
				LOGGER.info(
						"===============  AdminHomeController : Inside updateCategorySortOrderById :: Exception  ============= "
								+ e);

				return "redirect:https://www.foodkonnekt.com";
			}
			LOGGER.info(
					"===============  AdminHomeController : Inside updateCategorySortOrderById :: End  ============= ");

			return "failed";
		}

	}

	@RequestMapping(value = "/updateItemSortOrderById", method = RequestMethod.GET)
	@ResponseBody
	public String updateItemSortOrderById(@RequestParam(required = true) Integer categoryItemId,
			@RequestParam(required = true) Integer sortOrder, @RequestParam(required = true) String action) {
		try {
			LOGGER.info(
					"===============  AdminHomeController : Inside updateItemSortOrderById :: Start  ============= ");

			CategoryItem categoryItem = new CategoryItem();
			categoryItem.setId(categoryItemId);
			categoryItem.setSortOrder(sortOrder);
			boolean status = itemService.updateItemSortOrderById(categoryItem, action);
			if (status)
				return "succuss";
			else
				return "failed";

		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
				LOGGER.error("error: " + e.getMessage());
				LOGGER.info(
						"===============  AdminHomeController : Inside updateItemSortOrderById :: Exception  ============= "
								+ e);

				return "redirect:https://www.foodkonnekt.com";
			}
			LOGGER.info("===============  AdminHomeController : Inside updateItemSortOrderById :: End  ============= ");

			return "failed";
		}

	}

	// Add extra parameter categoryItemId by manish gupta
	@RequestMapping(value = "/updateItemStatusById", method = RequestMethod.GET)
	@ResponseBody
	public String updateItemStatusById(@RequestParam(required = true) Integer itemId,
			@RequestParam(required = true) Integer itemStatus, @RequestParam(required = true) Integer categoryItemId) {
		try {
			LOGGER.info("===============  AdminHomeController : Inside updateItemStatusById :: Start  ============= ");
			LOGGER.info(itemStatus + "-status, catItmId- " + categoryItemId);
			Item item = new Item();
			item.setId(itemId);
			item.setItemStatus(itemStatus);
			// change return type and add extra parameter categoryItemId
			return itemService.updateItemStatusById(item, categoryItemId);
		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
				LOGGER.error("error: " + e.getMessage());
				LOGGER.info(
						"===============  AdminHomeController : Inside updateItemStatusById :: Exception  ============= "
								+ e);
				return "redirect:https://www.foodkonnekt.com";
			}
		}
		LOGGER.info("===============  AdminHomeController : Inside updateItemStatusById :: End  ============= ");
		return "succuss";
	}

	@ExceptionHandler(value = NullPointerException.class)
	public String handleNullPointerException(Exception e) {
		return "exception";
	}

	/**
	 * Find modifier count of modifierGroup
	 * 
	 * @param modiferCount
	 * @param modifierGroupId
	 * @return String
	 */
	@RequestMapping(value = "/findModifierCountOfModifierGroup", method = RequestMethod.GET)
	@ResponseBody
	public String findMCount(@RequestParam(required = true) Integer modiferCount,
			@RequestParam(required = true) Integer modifierGroupId) {
		LOGGER.info("===============  AdminHomeController : Inside findMCount :: Start  ============= ");

		return modifierService.findModifierCountOfModifierGroup(modifierGroupId, modiferCount);
	}

	@RequestMapping(value = "/isInstalled", method = RequestMethod.GET)
	@ResponseBody
	public boolean setup(ModelMap model, HttpServletResponse response, HttpServletRequest request) {
		try {
			LOGGER.info("===============  AdminHomeController : Inside setup :: Start  ============= ");

			HttpSession session = request.getSession();
			if (session != null) {
				Merchant merchant = (Merchant) session.getAttribute("merchant");

				if (merchant != null) {
					Vendor vendor = merchant.getOwner();
					if (vendor != null) {
						Pos pos = vendor.getPos();

						if (pos != null && pos.getPosId() == 1) {
							Integer inventoryThreadStatus = 0;
							if (session.getAttribute("inventoryThread") == null) {
								session.setAttribute("inventoryThread", 0);
								return false;
							} else if (session.getAttribute("inventoryThread") != null)
								inventoryThreadStatus = (Integer) session.getAttribute("inventoryThread");
							LOGGER.info("===============  AdminHomeController : Inside save :: inventoryThreadStatus  "
									+ inventoryThreadStatus);

							if (inventoryThreadStatus == 1) {
								return true;
							} else {
								return false;
							}
						} else {
							return true;
						}
					} else {
						return true;
					}
				} else {
					return true;
				}
			} else {
				return true;
			}

		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
				LOGGER.error("error: " + e.getMessage());
			}
			return false;
		}
	}

	@RequestMapping(value = "/updateItemModifierGroupSortOrderById", method = RequestMethod.GET)
	@ResponseBody
	public String updateItemModifierGroupSortOrderById(@RequestParam(required = true) Integer itemModifierGroupId,
			@RequestParam(required = true) Integer itemId, @RequestParam(required = true) Integer sortOrder,
			@RequestParam(required = true) String action,
			@RequestParam(required = true) Integer itemModifierGroupOrder) {
		try {
			LOGGER.info("updateItemModifierGroupSortOrderById starts  --" + itemModifierGroupOrder);
			ItemModifierGroup itemModifierGroup = new ItemModifierGroup();
			itemModifierGroup.setId(itemModifierGroupId);
			itemModifierGroup.setSortOrder(sortOrder);
			boolean status = itemService.updateItemModifierGroupSortOrderById(itemModifierGroup, action,
					itemModifierGroupOrder, itemId);
			if (status)
				return "succuss";
			else
				return "failed";

		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
				LOGGER.error("error: " + e.getMessage());
				return "redirect:https://www.foodkonnekt.com";
			}
			LOGGER.info(
					"===============  AdminHomeController : Inside updateItemModifierGroupSortOrderById :: End  ============= ");

			return "failed";
		}

	}

	@RequestMapping(value = "/updateInventoryItemStatusById", method = RequestMethod.GET)
	@ResponseBody
	public String updateInventoryItemStatusById(@RequestParam(required = true) Integer itemId,
			@RequestParam(required = true) Integer itemStatus) {
		try {
			LOGGER.info(
					"===============  AdminHomeController : Inside updateInventoryItemStatusById :: Start  ============= itemId "
							+ itemId + " itemStatus " + itemStatus);

			Item item = new Item();
			item.setId(itemId);
			item.setItemStatus(itemStatus);
			itemService.updateInventoryItemStatusById(item);
		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
				LOGGER.error("error: " + e.getMessage());
				LOGGER.info(
						"===============  AdminHomeController : Inside updateInventoryItemStatusById :: Exception  ============= "
								+ e);

				return "redirect:https://www.foodkonnekt.com";
			}
		}
		LOGGER.info(
				"===============  AdminHomeController : Inside updateInventoryItemStatusById :: End  ============= ");

		return "succuss";
	}

	@RequestMapping(value = "/updateMerchantKritiq", method = RequestMethod.GET)
	@ResponseBody
	public String updateMerchantKritiqById(@RequestParam(value = "merchantId") Integer merchantId,
			@RequestParam(value = "status") String status) {
		String result = null;
		try {
			LOGGER.info(
					"===============  AdminHomeController : Inside updateMerchantKritiqById :: Start  ============= merchantId "
							+ merchantId + "status" + status);

			if (merchantId != null) {
				result = merchantService.getMerchantSubscription(merchantId, status);
			}

		} catch (Exception exception) {
			if (exception != null) {
				LOGGER.info(
						"===============  AdminHomeController : Inside updateMerchantKritiqById :: Exception  ============= "
								+ exception);

				MailSendUtil.sendExceptionByMail(exception,environment);
			}
			exception.printStackTrace();
		}
		LOGGER.info("===============  AdminHomeController : Inside updateMerchantKritiqById :: End  ============= ");
		return result;

	}

	@RequestMapping(value = "/addDefaultTax", method = RequestMethod.GET)
	@ResponseBody
	public String addDefaultTaxByMerchantId(@RequestParam(value = "merchantId") Integer merchantId) {
		LOGGER.info(
				"===============  AdminHomeController : Inside addDefaultTaxByMerchantId :: Start  ============= merchantId "
						+ merchantId);

		String response = merchantService.addDefaultTaxByMerchantId(merchantId);

		LOGGER.info("===============  AdminHomeController : Inside addDefaultTaxByMerchantId :: End returns response "
				+ response);

		return response;
	}

	@RequestMapping(value = "/getDisabledEnabledMutltiPayValue", method = RequestMethod.GET)
	public @ResponseBody boolean getValue(@RequestParam(required = true) int id,
			@RequestParam(required = true) boolean multiPayValue) {

		LOGGER.info("===============  AdminHomeController : Inside getValue :: Start  ============= id " + id
				+ " multiPayValue " + multiPayValue);

		Merchant merchantCheck = null;
		merchantCheck = merchantService.findById(id);
		boolean multiPayStatus = false;
		if (merchantCheck != null) {
			if (multiPayValue) {
				try {
					multiPayStatus = merchantService.checkAllowMultiPay(merchantCheck, merchantCheck.getPosMerchantId(),
							merchantCheck.getAccessToken(), merchantCheck.getAllowMultiPay());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					LOGGER.error("error: " + e.getMessage());
				}

				// merchant.setActiveCustomerFeedback(1);
			} else {
				multiPayStatus = false;
				merchantCheck.setAllowMultiPay(false);
				merchantService.save(merchantCheck);
				// merchant.setActiveCustomerFeedback(0);

			}
		}

		LOGGER.info("===============  AdminHomeController : Inside getValue :: End  ============= ");

		return multiPayStatus;

	}

	@RequestMapping(value = "/getDisabledEnabledKritiqValue", method = RequestMethod.GET)
	public @ResponseBody String getKritiqValue(ModelMap model, @RequestParam(required = true) int id,
			@RequestParam(required = false) int kritiqValue) {
		LOGGER.info("===============  AdminHomeController : Inside getKritiqValue :: Start  =============" + "id " + id
				+ "kritiqValue" + kritiqValue);

		model.addAttribute("adminUrl", environment.getProperty("BASE_URL"));
		Merchant result = new Merchant();
		result = merchantService.findById(id);
		if (kritiqValue == 0) {
			result.setActiveCustomerFeedback(1);
			result.setId(id);
			// merchant.setActiveCustomerFeedback(1);
		} else {
			result.setActiveCustomerFeedback(0);
			result.setId(id);
			// merchant.setActiveCustomerFeedback(0);
		}

		Merchant merchant = merchantService.save(result);

		LOGGER.info("===============  AdminHomeController : Inside getKritiqValue :: End  ============= ");

		return "getAllMerchantsByVendor";

	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/saveInventoryByExcel", method = RequestMethod.POST)
	@ResponseBody
	public String saveInventory(ModelMap model, HttpServletResponse response, HttpServletRequest request) {
		LOGGER.info("===============  AdminHomeController : Inside saveInventory :: Start  ============= ");

		HttpSession httpSession = request.getSession();
		// ArrayList<TaxRates> taxRates
		ArrayList<TaxRates> taxRates = (ArrayList<TaxRates>) httpSession.getAttribute("taxRates");
		ArrayList<ModifierGroup> modifierGroups = (ArrayList<ModifierGroup>) httpSession.getAttribute("modifierGroups");
		ArrayList<Item> items = (ArrayList<Item>) httpSession.getAttribute("items");
		List<Category> categories = (List<Category>) httpSession.getAttribute("categories");
		List<PizzaSize> pizzaSizes = (List<PizzaSize>) httpSession.getAttribute("pizzaSize");
		List<PizzaTemplate> pizaaTemplates = (List<PizzaTemplate>) httpSession.getAttribute("pizzaTemplate");
		List<PizzaCrust> pizzaCrusts = (List<PizzaCrust>) httpSession.getAttribute("pizzaCrusts");
		List<PizzaTopping> pizzaToppings = (List<PizzaTopping>) httpSession.getAttribute("pizzaTopping");

		String responseData = importExcelService.saveInvetoryDataOfExcelSheet(taxRates, modifierGroups, items,
				categories, pizzaSizes, pizaaTemplates, pizzaCrusts, pizzaToppings);
		model.addAttribute("excelResponse", responseData);
		LOGGER.info("===============  AdminHomeController : Inside saveInventory :: responseData  " + responseData);

		LOGGER.info("===============  AdminHomeController : Inside saveInventory :: End  ============= ");

		return "redirect:" + environment.getProperty("BASE_URL") + "/inventory";
	}

	@RequestMapping(value = "/schedularUrl", method = RequestMethod.GET)
	@ResponseBody
	public Integer checkSchedularStatus(HttpServletRequest request, Model model) {
		LOGGER.info("===============  AdminHomeController : Inside checkSchedularStatus :: Start  ============= ");

		HttpSession session = request.getSession(false);
		Merchant merchant = (Merchant) session.getAttribute("merchant");
		PosIntegration integration = itemService.findBySchedularStatusByMerchantId(merchant.getId());
		int status = integration.getSchedulerStatus();
		model.addAttribute("status", status);
		session.setAttribute("posIntegrationStatus", status);
		// String s=String.valueOf(status);
		System.out.println(merchant.getId());
		LOGGER.info(
				"===============  AdminHomeController : Inside checkSchedularStatus :: End returns status ============= "
						+ status);

		return status;
	}

	@RequestMapping(value = "/pizzaTamplate", method = RequestMethod.GET)
	public String pizzaTamplate(ModelMap model, HttpServletRequest request) {
		LOGGER.info("===============  AdminHomeController : Inside pizzaTamplate :: Start  ============= ");

		HttpSession session = request.getSession(false);
		if (session != null) {
			Merchant merchant = (Merchant) session.getAttribute("merchant");
			if (merchant != null) {
				model.addAttribute("pizzaSizes", pizzaService.findAllSizes(merchant.getId()));
				if (merchant.getOwner() != null && merchant.getOwner().getPos() != null
						&& merchant.getOwner().getPos().getPosId() != null)
					model.addAttribute("merchantType", merchant.getOwner().getPos().getPosId());
			} else {
				return "redirect:" + environment.getProperty("BASE_URL") + "/support";
			}
		} else {
			return "redirect:" + environment.getProperty("BASE_URL") + "/support";
		}
		LOGGER.info("===============  AdminHomeController : Inside pizzaTamplate :: End  ============= ");

		return "pizzaTamplate";
	}

	@RequestMapping(value = "/pizzaTopping", method = RequestMethod.GET)
	public String pizzaTopping(ModelMap model, HttpServletRequest request) {
		LOGGER.info("===============  AdminHomeController : Inside pizzaTopping :: Start  ============= ");

		HttpSession session = request.getSession(false);
		if (session != null) {
			Merchant merchant = (Merchant) session.getAttribute("merchant");
			if (merchant != null) {
				List<PizzaSize> p = pizzaService.findAllSizes(merchant.getId());
				model.addAttribute("pizzaSizes", pizzaService.findAllSizes(merchant.getId()));
				if (merchant.getOwner() != null && merchant.getOwner().getPos() != null
						&& merchant.getOwner().getPos().getPosId() != null)
					model.addAttribute("merchantType", merchant.getOwner().getPos().getPosId());
			} else {
				return "redirect:" + environment.getProperty("BASE_URL") + "/support";
			}
		} else {
			return "redirect:" + environment.getProperty("BASE_URL") + "/support";
		}
		LOGGER.info("===============  AdminHomeController : Inside pizzaTopping :: End  ============= ");

		return "pizzaTopping";
	}

	@RequestMapping(value = "/editPizzaTemplate", method = RequestMethod.GET)
	public String viewPizzaTemplate(@ModelAttribute("pizzaTemplate") PizzaTemplate pizzaTemplate, ModelMap model,
			HttpServletRequest request, @RequestParam(required = false) int templateId) {
		try {
			LOGGER.info(
					"===============  AdminHomeController : Inside viewPizzaTemplate :: Start  ============= templateId "
							+ templateId);

			HttpSession session = request.getSession(false);
			if (session != null) {
				Merchant merchant = (Merchant) session.getAttribute("merchant");
				if (merchant != null) {
					pizzaTemplate = pizzaService.findByTemplateId(templateId);
					model.addAttribute("description", pizzaTemplate.getDescription());
					model.addAttribute("status", pizzaTemplate.getStatus());
					model.addAttribute("pizzaTemplate", pizzaTemplate);

					model.addAttribute("allowToppingLimit", pizzaTemplate.getAllowToppingLimit());

					model.addAttribute("pizzaSizes", pizzaTemplate.getPizzaSizes());
					model.addAttribute("pizzaTemplateSizes", pizzaTemplate.getPizzaTemplateSizes());

					List<PizzaTemplateSize> pizzaTemplateSizes = pizzaService.findByPizzaTemplateSizeById(templateId);
					if (pizzaTemplateSizes != null && pizzaTemplateSizes.size() > 0) {
						model.addAttribute("pizzaTemplateSizes", pizzaTemplateSizes);
					}
					List<PizzaSize> dropdownPizzaSize = null;
					List<Integer> pizzasizeid = new ArrayList<Integer>();
					for (int i = 0; i < pizzaTemplateSizes.size(); i++) {
						pizzasizeid.add(pizzaTemplateSizes.get(i).getPizzaSize().getId());
					}

					if (pizzasizeid.size() > 0) {
						dropdownPizzaSize = pizzaSizeRepository.findByMerchantIdAndId(merchant.getId(), pizzasizeid);
					}

					else {
						dropdownPizzaSize = pizzaSizeRepository.findByMerchantId(merchant.getId());
					}

					model.addAttribute("dropdownPizzaSize", dropdownPizzaSize);
					model.addAttribute("taxes",
							pizzaTemplateTaxRepository.findByPizzaTemplateId(pizzaTemplate.getId()));
					if (merchant.getOwner() != null && merchant.getOwner().getPos() != null
							&& merchant.getOwner().getPos().getPosId() != null) {
						model.addAttribute("merchantType", merchant.getOwner().getPos().getPosId());
					}

					model.addAttribute("pizzaTemplateTopping", pizzaToppingRepository
							.findPizzaToppingByActive(merchant.getId(), pizzaTemplate.getId(), 1));

					List<PizzaTemplateTopping> pizzaTemplateToppingList = pizzaTemplateToppingRepository
							.findByPizzaTemplateIds(templateId);
					for (PizzaTemplateTopping pizzaTemplateTopping1 : pizzaTemplateToppingList) {
						LOGGER.info(
								"===============  AdminHomeController : Inside viewPizzaTemplate :: pizzaTemplateTopping  ==== "
										+ pizzaTemplateTopping1.getId());
						pizzaTemplateTopping1.setPizzaTemplate(null);
						pizzaTemplateTopping1.getPizzaTopping().setMerchant(null);
						pizzaTemplateTopping1.getPizzaTopping().setPizzaTemplateTopping(null);
						pizzaTemplateTopping1.getPizzaTopping().setPizzaTemplateToppings(null);
						List<PizzaToppingSize> pizzaToppingSize = pizzaTemplateTopping1.getPizzaTopping()
								.getPizzaToppingSizes();
						for (PizzaToppingSize pizzaToppingSize1 : pizzaToppingSize) {
							if (pizzaToppingSize1 != null) {
								pizzaToppingSize1.setPizzaTopping(null);
								PizzaSize pizzaSize = pizzaToppingSize1.getPizzaSize();
								pizzaSize.setMerchant(null);
								pizzaSize.setCategories(null);
								pizzaSize.setPizzaCrustSizes(null);
								pizzaSize.setPizzaTemplates(null);
								pizzaSize.setPizzaToppingSizes(null);
							}
						}

					}
					if (pizzaTemplateToppingList != null) {
						model.addAttribute("pizzaTemplateToppingList", pizzaTemplateToppingList);
					} else {
						return "redirect:" + environment.getProperty("BASE_URL") + "/support";
					}
				} else {
					LOGGER.info(
							"===============  AdminHomeController : Inside viewPizzaTemplate :: End  ============= ");

					return "redirect:" + environment.getProperty("BASE_URL") + "/support";
				}
			}
		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
				LOGGER.error("error: " + e.getMessage());
				LOGGER.info(
						"===============  AdminHomeController : Inside viewPizzaTemplate :: Exception  ============= "
								+ e);

				return "redirect:" + environment.getProperty("BASE_URL") + "/support";
			}

		}
		LOGGER.info("===============  AdminHomeController : Inside viewPizzaTemplate :: End  ============= ");

		return "editPizzaTemplate";
	}

	@RequestMapping(value = "/updatePizzaTemplate", method = RequestMethod.POST)
	public String updatePizzaTemplate(@ModelAttribute() PizzaTemplate pizzaTemplate, ModelMap model) {
		try {
			LOGGER.info("===============  AdminHomeController : Inside updatePizzaTemplate :: Start  ============= ");

			PizzaTemplate result = new PizzaTemplate();
			result = pizzaService.updatePizzaTemplateById(pizzaTemplate);
			if (result != null) {
				LOGGER.info("===============  AdminHomeController : Inside if (result != null) ");

				pizzaTemplate.setDescription(result.getDescription());
				model.addAttribute("description2", result.getDescription2());
				model.addAttribute("description", result.getDescription());
				model.addAttribute("status", result.getStatus());
				model.addAttribute("pizzaTemplate", result);
				model.addAttribute("allowToppingLimit", result.getAllowToppingLimit());
				model.addAttribute("pizzaToppings", result.getPizzaToppings());
				model.addAttribute("pizzaTemplateTopping", result.getPizzaTemplateToppings());

				model.addAttribute("pizzaSizes", result.getPizzaSizes());
				model.addAttribute("pizzaTemplateSizes", result.getPizzaTemplateSizes());

				List<Integer> sizeId = pizzaTemplate.getSizeId();
				List<Double> sizePrice = pizzaTemplate.getSizePrice();

				List<PizzaTemplateSize> pizzaTemplateSizess = pizzaTemplateSizeRepository
						.findByPizzaTemplateIdAndPizzaSizeActive(pizzaTemplate.getId(), 1);
				if (pizzaTemplateSizess != null && sizeId != null && !sizeId.isEmpty() && sizePrice != null
						&& !sizePrice.isEmpty()) {
					for (int i = 0; i < pizzaTemplateSizess.size(); i++) {

						for (int j = 0; j < sizeId.size(); j++) {

							if (pizzaTemplateSizess.get(i).getPizzaSize().getId().equals(sizeId.get(j))) {
								pizzaTemplateSizess.get(i).setPrice(sizePrice.get(i));
								pizzaTemplateSizess.get(i).setActive(1);
								break;
							} else {
								pizzaTemplateSizess.get(i).setActive(0);
							}

						}
						LOGGER.info(
								"===============  AdminHomeController : Inside updatePizzaTemplate :: pizzaTemplateSize  ============= "
										+ pizzaTemplateSizess.get(i));

						pizzaTemplateSizeRepository.saveAndFlush(pizzaTemplateSizess.get(i));

					}
				}
				else if(sizeId == null
						|| sizeId.isEmpty()) {
					for(PizzaTemplateSize pizzatemplatesize1 : pizzaTemplateSizess) {
						pizzatemplatesize1.setActive(0);
						pizzaTemplateSizeRepository.saveAndFlush(pizzatemplatesize1);
					}
				}
				

				model.addAttribute("taxes", result.getPizzaTemplateTaxs());

				/*
				 * for (int i = 0; i < sizeId.size(); i++) { Integer sId = sizeId.get(i); Double
				 * sPrice = sizePrice.get(i); PizzaTemplateSize templateSize =
				 * pizzaTemplateSizeRepository .findByPizzaSizeIdAndPizzaTemplateId(sId,
				 * result.getId());
				 * 
				 * if (templateSize != null) { templateSize.setPrice(sPrice);
				 * templateSize.setActive(1); pizzaTemplateSizeRepository.save(templateSize); }
				 * }
				 */

				LOGGER.info(
						"===============  AdminHomeController : Inside updatePizzaTemplate :: pizzaTemplate  ============= "
								+ result.getId());

				List<PizzaTemplateSize> pizzaTemplateSizes = pizzaService.findByPizzaTemplateSizeById(result.getId());
				if (pizzaTemplateSizes != null && pizzaTemplateSizes.size() > 0) {
					model.addAttribute("pizzaTemplateSizes", pizzaTemplateSizes);
				}
			}
		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
				LOGGER.error("error: " + e.getMessage());
				LOGGER.info(
						"===============  AdminHomeController : Inside updatePizzaTemplate :: Exception  ============= "
								+ e);

				return "redirect:https://www.foodkonnekt.com";
			}
		}
		LOGGER.info("===============  AdminHomeController : Inside updatePizzaTemplate :: End  ============= ");

		return "pizzaTamplate";
	}

	@RequestMapping(value = "/getToppingByToppingIds", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String filterByTopping(HttpServletRequest request,
			@RequestParam(required = false) String modifierGroupIds, @RequestParam(required = false) Integer itemId) {

		// To be implemented............
		return "";
	}

	@RequestMapping(value = "/checkIsPaymentGatewayEnabled", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody Boolean checkIsPaymentGatewayEnabled(HttpServletRequest request,
			@RequestParam(required = true) Integer merchantId) {
		LOGGER.info(
				"===============  AdminHomeController : Inside checkIsPaymentGatewayEnabled :: Start  ============= ");

		Boolean gatewayFlag = false;
		PaymentGateWay gateway = merchantService.findbyMerchantId(merchantId, false);
		if (gateway != null) {
			gatewayFlag = true;
		}
		LOGGER.info(
				"===============  AdminHomeController : Inside checkIsPaymentGatewayEnabled :: End  ============= ");

		return gatewayFlag;
	}

	@RequestMapping(value = "/getTaxesByMerchantId", method = RequestMethod.GET)
	public String getTaxesByMerchantId(ModelMap modelMap, HttpServletRequest request) {
		try {
			LOGGER.info("===============  AdminHomeController : Inside getTaxesByMerchantId :: Start  ============= ");

			HttpSession session = request.getSession(false);
			if(session!=null)
			{
			Merchant merchant = (Merchant) session.getAttribute("merchant");
			if (merchant != null && merchant.getId() != null) {
				LOGGER.info(
						"===============  AdminHomeController : Inside getTaxesByMerchantId :: merchant.getId()  ============= "
								+ merchant.getId());
				List<TaxRates> taxRates = taxRateRepository.findByMerchantId(merchant.getId());
				if (!taxRates.isEmpty() && taxRates.size() > 0) {
					modelMap.addAttribute("taxRates", taxRates);
					LOGGER.info("===============  AdminHomeController : Inside if (!taxRates.isEmpty())");

				}
			} else
				return "redirect:" + environment.getProperty("BASE_URL") + "/support";
			} else return "redirect:" + environment.getProperty("BASE_URL") + "/support";
		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
				LOGGER.error("error: " + e.getMessage());
				LOGGER.info(
						"===============  AdminHomeController : Inside getTaxesByMerchantId :: Exception  ============= "
								+ e);

				return "redirect:" + environment.getProperty("BASE_URL") + "/support";
			}
		}
		LOGGER.info("===============  AdminHomeController : Inside getTaxesByMerchantId :: End  ============= ");

		return "merchantTaxes";
	}

	@RequestMapping(value = "/editTaxRates", method = RequestMethod.GET)
	public String editTaxRates(@RequestParam("taxRateId") Integer taxRateId, ModelMap modelMap) {
		try {
			LOGGER.info("===============  AdminHomeController : Inside editTaxRates :: Start  ============= taxRateId "
					+ taxRateId);

			if (taxRateId != null) {
				TaxRates taxRates = taxRateRepository.findById(taxRateId);
				if (taxRates != null) {
					modelMap.addAttribute("taxRatesName", taxRates.getName());
					modelMap.addAttribute("rates", taxRates.getRate());
					modelMap.addAttribute("taxRates", taxRates);
				}
			}
		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
				LOGGER.error("error: " + e.getMessage());
				LOGGER.info("===============  AdminHomeController : Inside taxRateId :: Exception  ============= " + e);

				return "redirect:https://www.foodkonnekt.com";
			}
		}
		LOGGER.info("===============  AdminHomeController : Inside taxRateId :: End  ============= ");

		return "editTaxRates";
	}

	@RequestMapping(value = "/updateTaxRates", method = RequestMethod.POST)
	public String updateTaxRates(@ModelAttribute() TaxRates taxRates, ModelMap model) {
		try {
			LOGGER.info("===============  AdminHomeController : Inside updateTaxRates :: Start  =============");

			TaxRates result = new TaxRates();
			result = taxRateRepository.save(taxRates);
			if (result != null) {
				LOGGER.info("===============  AdminHomeController : Inside updateTaxRates :: TaxRates  = "
						+ result.getName());

				taxRates.setName(result.getName());
			}

		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
				LOGGER.error("error: " + e.getMessage());
				LOGGER.info("===============  AdminHomeController : Inside updateTaxRates :: Exception  ============= "
						+ e);

				return "redirect:https://www.foodkonnekt.com";
			}
		}
		LOGGER.info("===============  AdminHomeController : Inside updateTaxRates :: End  ============= ");

		return "editTaxRates";
	}

	@RequestMapping(value = "/deleteTaxRates", method = RequestMethod.GET)
	public String deleteTaxRates(@RequestParam("taxRateId") Integer taxRateId) {
		try {
			LOGGER.info(
					"===============  AdminHomeController : Inside deleteTaxRates :: Start  ============= taxRateId "
							+ taxRateId);

			if (taxRateId != null) {
				List<ItemTax> itemTaxes = itemTaxRepository.findByTaxRatesId(taxRateId);
				if (!itemTaxes.isEmpty() && itemTaxes.size() > 0) {
					itemTaxRepository.delete(itemTaxes);
				}
				TaxRates taxRates = taxRateRepository.findById(taxRateId);
				if (taxRates != null) {
					taxRates.setMerchant(null);
					taxRateRepository.delete(taxRates);
				}
				LOGGER.info("taxRates deleted");
			}

		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
				LOGGER.error("error: " + e.getMessage());
				LOGGER.info("===============  AdminHomeController : Inside deleteTaxRates :: Exception  ============= "
						+ e);

				return "redirect:https://www.foodkonnekt.com";
			}
		}
		LOGGER.info("===============  AdminHomeController : Inside deleteTaxRates :: End  ============= ");

		return "merchantTaxes";
	}

	@RequestMapping(value = "/getMerchantSliders", method = RequestMethod.GET)
	public String getMerchantSliders(@RequestParam("merchantId") Integer merchantId, ModelMap model,
			HttpServletRequest request) {
		LOGGER.info("----------------Start :: AdminHomeController : getMerchantSliders------------------");
		try {
			HttpSession session = request.getSession();
			if (session != null) {
				Merchant merchant = (Merchant) session.getAttribute("merchant");
				if (merchant != null) {
					List<MerchantSliders> merchantSliders = merchantSliderService.findByMerchantId(merchantId);

					Integer imageCount = merchantSliderService.findSliderImageCountByMerchantId(merchantId);
					if (!merchantSliders.isEmpty()) {
						model.addAttribute("merchantSliders", merchantSliders);
						model.addAttribute("imageCount", imageCount);
						model.addAttribute("url", environment.getProperty("BASE_PORT"));
					}
				} else
					return "redirect:" + environment.getProperty("BASE_URL") + "/support";
			} else
				return "redirect:" + environment.getProperty("BASE_URL") + "/support";
		} catch (Exception e) {
			LOGGER.info("----------------EXCEPTION :: AdminHomeController : getMerchantSliders------------------" + e);
			LOGGER.error("error: " + e.getMessage());
			MailSendUtil.sendExceptionByMail(e,environment);
			return "redirect:" + environment.getProperty("BASE_URL") + "/support";
		}
		LOGGER.info("----------------END :: AdminHomeController : getMerchantSliders------------------");
		return "merchantSliders";
	}

	@RequestMapping(value = "/saveMerchantSliders", method = RequestMethod.POST)
	public String saveMerchantSliders(@RequestParam("file") MultipartFile file, HttpServletRequest request,
			ModelMap model) {
		LOGGER.info("===============  AdminHomeController : Inside saveMerchantSliders :: Start  ============= ");

		HttpSession session = request.getSession(false);
		Merchant merchant = (Merchant) session.getAttribute("merchant");

		if (merchant != null) {
			merchantSliderService.saveMerchantSliderValue(merchant, file);
		}
		return "redirect:" + environment.getProperty("BASE_URL") + "/getMerchantSliders?merchantId=" + merchant.getId();
	}

	@RequestMapping(value = "/deleteSlidderImage", method = RequestMethod.GET)
	public String deleteSlidderImage(@RequestParam("sliderId") Integer sliderId) {
		LOGGER.info(
				"===============  AdminHomeController : Inside deleteSlidderImage :: Start  =============  sliderId "
						+ sliderId);

		MerchantSliders merchantSliders = merchantSliderService.findById(sliderId);
		if (merchantSliders != null) {
			merchantSliderService.deleteItemImage(merchantSliders);
		}
		LOGGER.info("===============  AdminHomeController : Inside deleteSlidderImage :: End  ============= ");

		return "redirect:" + environment.getProperty("BASE_URL") + "/getMerchantSliders?merchantId="
				+ merchantSliders.getMerchant().getId();
	}

	@RequestMapping(value = "/updatePizzaTemplateTopping", method = RequestMethod.GET)
	@ResponseBody
	public PizzaTemplateTopping updatePizzaTemplateTopping(@RequestParam("id") Integer id,
			@RequestParam("included") Boolean isIncluded, @RequestParam("replaceable") Boolean isReplacable,
			@RequestParam("active") Boolean isActive, @RequestParam("templateId") Integer templateId) {
		LOGGER.info(
				"===============  AdminHomeController : Inside updatePizzaTemplateTopping :: Start  =============templateId "
						+ templateId + " PizzaTemplateTopping " + id + " isIncluded " + isIncluded + " isReplacable "
						+ isReplacable + " isActive " + isActive);

		PizzaTemplateTopping pizzaTemplateTopping = pizzaTemplateToppingRepository
				.findByPizzaTemplateIdAndPizzaToppingId(templateId, id);
		if (pizzaTemplateTopping != null) {
			pizzaTemplateTopping.setIncluded(isIncluded);
			pizzaTemplateTopping.setActive(isActive);
			pizzaTemplateTopping.setReplacable(isReplacable);
			pizzaTemplateTopping.getPizzaTemplate().setCategory(null);
			pizzaTemplateTopping.getPizzaTemplate().setMerchant(null);
			pizzaTemplateTopping.getPizzaTemplate().setPizzaSizes(null);
			pizzaTemplateTopping.getPizzaTemplate().setPizzaTemplateCategories(null);
			pizzaTemplateTopping.getPizzaTemplate().setPizzaCrust(null);
			pizzaTemplateTopping.getPizzaTemplate().setPizzaTemplateSizes(null);
			pizzaTemplateTopping.getPizzaTemplate().setPizzaTemplateToppings(null);
			pizzaTemplateTopping.getPizzaTemplate().setTaxes(null);
			pizzaTemplateTopping.getPizzaTopping().setMerchant(null);
			pizzaTemplateTopping.getPizzaTopping().setPizzaTemplateToppings(null);
			pizzaTemplateTopping.getPizzaTopping().setPizzaToppingSizes(null);

			pizzaTemplateToppingRepository.save(pizzaTemplateTopping);
		}
		LOGGER.info("===============  AdminHomeController : Inside updatePizzaTemplateTopping :: End  ============= ");

		return pizzaTemplateTopping;
	}

	@RequestMapping(value = "/getBusinessHoursByLocationUid", method = RequestMethod.GET)
	public @ResponseBody String getBusinessHoursByLocationUid(@RequestParam("locationUid") String locationUid) {
		LOGGER.info("===============  AdminHomeController : Inside getBusinessHoursByLocationUid ::"
				+ " Start  ============= locationUid " + locationUid);

		List<OpeningClosingDay> openingClosingHours = null;
		if (locationUid != null) {
			Merchant merchant = merchantService.findByMerchantUid(locationUid);
			if (merchant != null && merchant.getId() != null) {

				openingClosingHours = businessService.findBusinessHourByMerchantId(merchant.getId());
				if (openingClosingHours != null && !openingClosingHours.isEmpty()) {
					for (OpeningClosingDay openingClosingDay : openingClosingHours) {
						openingClosingDay.setMerchant(null);
						openingClosingDay.setOpeningClosingTimes(null);
						for (OpeningClosingTime openingClosingTime : openingClosingDay.getTimes()) {
							openingClosingTime.setOpeningClosingDay(null);
						}
					}
				}
			}
		}
		Gson gson = new Gson();

		String listOpeningClosingHours = gson.toJson(openingClosingHours);
		LOGGER.info("===============  AdminHomeController : Inside getBusinessHoursByLocationUid :: "
				+ "End  returns ============= listOpeningClosingHours " + listOpeningClosingHours);

		return listOpeningClosingHours;
	}

	@RequestMapping(value = "/getBusinessProfileInfo", method = RequestMethod.GET)
	public @ResponseBody String getBusinessProfileInfo(@RequestParam("merchantUid") String merchantUid) {
		LOGGER.info(
				"===============  AdminHomeController : Inside getBusinessProfileInfo :: Start  ============= merchantUid "
						+ merchantUid);

		Merchant merchant = merchantService.findByMerchantUid(merchantUid);
		Merchant finalMerchant = null;
		if (merchant != null) {
			List<Address> add = merchantService.findAddressByMerchantId(merchant.getId());
			for (Address address : add) {
				address.setMerchant(null);
				address.setZones(null);
				// address.setState(null);
				address.setCountry(null);
				address.setCustomer(null);
			}
			finalMerchant = businessService.getBusinessProfileInfo(merchant);
			finalMerchant.setAddresses(add);
		}

		Gson gson = new Gson();
		String data = gson.toJson(finalMerchant);
		LOGGER.info(
				"===============  AdminHomeController : Inside getBusinessProfileInfo :: End  =============returns data "
						+ data);

		return data;
	}

	@RequestMapping(value = "/updateBusinessHours", method = RequestMethod.POST)
	public @ResponseBody void updateBusinessHours(@RequestBody List<OpeningClosingDay> openingClosingDay,
			@RequestParam("merchantUid") String merchantUid) {
		LOGGER.info("===============  AdminHomeController : Inside updateBusinessHours :: Start  ============= ");

		Merchant merchant = merchantService.findByMerchantUid(merchantUid);
		businessService.updateBuisnessHoursTime(openingClosingDay, merchant);
		LOGGER.info("===============  AdminHomeController : Inside updateBusinessHours :: End  ============= ");

	}

	@RequestMapping(value = "/updateBusinessInfo", method = RequestMethod.POST)
	public @ResponseBody Boolean updateBusinessInfo(@RequestBody Merchant merchant) {
		LOGGER.info("===============  AdminHomeController : Inside updateBusinessInfo :: Start  ============= ");

		Boolean status = merchantService.updateMerchantDetail(merchant);
		LOGGER.info("===============  AdminHomeController : Inside updateBusinessInfo :: End  ============= ");

		return status;
	}

	@RequestMapping(value = "/updateOrderAvgTime", method = RequestMethod.POST)
	public @ResponseBody Boolean updateOrderAvgTime(@RequestBody OrderR order,
			@RequestParam("merchantUid") String merchantUid) {
		LOGGER.info("===============  AdminHomeController : Inside updateOrderAvgTime :: Start  ============= order "
				+ order.getId());

		Merchant merchant = merchantService.findByMerchantUid(merchantUid);
		Boolean status = orderService.findByMerchantIdandOId(merchant, order);
		LOGGER.info(
				"===============  AdminHomeController : Inside updateOrderAvgTime :: End  ============= returns status "
						+ status);

		return status;
	}

	@RequestMapping(value = "/updateDeliveryStatus", method = RequestMethod.GET)
	public @ResponseBody String updateDeliveryStatus(@RequestParam("merchantUid") String merchatUid,
			@RequestParam("deliverystatus") Integer deliveryStatus) {
		LOGGER.info("===============  AdminHomeController : Inside updateDeliveryStatus :: Start  ============= ");

		String status = "false";
		Merchant merchant = merchantService.findByMerchantUid(merchatUid);
		if (merchant != null && merchant.getId() != null) {
			List<Zone> zones = zoneService.findZoneByMerchantId(merchant.getId());
			if (zones != null && !zones.isEmpty()) {
				status = zoneService.updateDeliveryStatus(zones, deliveryStatus);
			}
		}
		LOGGER.info(
				"===============  AdminHomeController : Inside updateDeliveryStatus :: End  ============= returns status "
						+ status);

		return status;
	}

	@RequestMapping(value = "/itemCategory", method = RequestMethod.GET)
	public String inventoryCategory(@ModelAttribute("itemCategory") CategoryItem categoryItem,
			HttpServletRequest request, ModelMap model) {
		LOGGER.info(
				"===============  AdminHomeController : Inside inventoryCategory :: Start  ============= categoryItem "
						+ categoryItem.getId());

		HttpSession session = request.getSession(false);
		Merchant merchant = (Merchant) session.getAttribute("merchant");

		if (merchant != null) {
			LOGGER.info(
					"===============  AdminHomeController : Inside inventoryCategory :: Start  ============= merchant "
							+ merchant.getId());

			List<Item> items = itemRepository.findByMerchantId(merchant.getId());
			if (items != null && !items.isEmpty()) {
				model.addAttribute("items", items);
			}

			List<Category> categories = categoryRepository.findByIsPizzaAndMerchantId(0, merchant.getId());
			if (categories != null && !categories.isEmpty()) {
				model.addAttribute("categories", categories);
			}
		}
		LOGGER.info("===============  AdminHomeController : Inside inventoryCategory :: End  ============= ");

		return "itemCategory";
	}

	@RequestMapping(value = "/updateItemCategory", method = RequestMethod.POST)
	public String updateInventoryCategory(@ModelAttribute("itemCategory") CategoryItem categoryItem,
			HttpServletRequest request, ModelMap model) {
		LOGGER.info("===============  AdminHomeController : Inside updateInventoryCategory :: Start  ============= ");

		HttpSession session = request.getSession(false);
		Merchant merchant = (Merchant) session.getAttribute("merchant");

		if (categoryItem.getcId() != null && !categoryItem.getcId().isEmpty()) {
			Category category = null;
			Item item = null;
			CategoryItem categoryItems = null;
			for (Integer categoryId : categoryItem.getcId()) {
				category = new Category();
				category.setId(categoryId);

				if (categoryItem.getiId() != null && !categoryItem.getiId().isEmpty()) {
					for (Integer itemId : categoryItem.getiId()) {
						categoryItems = new CategoryItem();
						item = new Item();
						LOGGER.info(
								"===============  AdminHomeController : Inside updateInventoryCategory :: Start  ============= itemId "
										+ itemId + " categoryId " + categoryId);

						CategoryItem dbCategoryItem = categoryItemRepository.findByItemIdAndCategoryId(itemId,
								categoryId);

						if (dbCategoryItem != null) {
							System.out.println("CategoryItem already have mapping!!");
						} else {
							item.setId(itemId);
							categoryItems.setItem(item);
							categoryItems.setCategory(category);
							categoryItems.setActive(1);
							categoryItemRepository.save(categoryItems);
						}
					}
				}
			}
		}

		if (merchant != null) {
			List<Item> items = itemRepository.findByMerchantId(merchant.getId());
			if (items != null && !items.isEmpty()) {
				model.addAttribute("items", items);
			}
			LOGGER.info("===============  AdminHomeController : Inside updateInventoryCategory ::merchant "
					+ merchant.getId());
			List<Category> categories = categoryRepository.findByMerchantId(merchant.getId());
			if (categories != null && !categories.isEmpty()) {
				model.addAttribute("categories", categories);
			}
		}
		LOGGER.info("===============  AdminHomeController : Inside updateInventoryCategory :: End  ============= ");

		return "itemCategory";
	}

	@RequestMapping(value = "/itemModifierGroup", method = RequestMethod.GET)
	public String itemModifire(ModelMap modelMap, HttpServletRequest request,
			@ModelAttribute("itemModifierGroup") ItemModifierGroup itemModifierGroup) {
		LOGGER.info("===============  AdminHomeController : Inside itemModifire :: Start  ============= ");

		HttpSession session = request.getSession();
		Merchant merchant = (Merchant) session.getAttribute("merchant");
		try {
			if (merchant != null) {
				List<Item> items = itemService.findByMerchantId(merchant.getId());
				modelMap.addAttribute("items", items);
				LOGGER.info(
						"===============  AdminHomeController : Inside itemModifire ::merchant " + merchant.getId());

				List<ModifierGroup> groups = modifierService.findModifierGroupsByMerchantById(merchant.getId());
				modelMap.addAttribute("modifierGroup", groups);

				List<Modifiers> modifiers = modifierRepository.findByMerchantId(merchant.getId());
				modelMap.addAttribute("modifiers", modifiers);
				return "masterFormOfitemModifierGroup";
			}
		} catch (Exception e) {
			LOGGER.error("error: " + e.getMessage());
		}
		LOGGER.info("===============  AdminHomeController : Inside itemModifire :: End  ============= ");

		return "masterFormOfitemModifierGroup";
	}

	@RequestMapping(value = "/updateItemModifierGroup", method = RequestMethod.POST)
	public String updateItemModifierGroup(ModelMap modelMap,
			@ModelAttribute("itemModifierGroup") ItemModifierGroup itemModifierGroup, HttpServletRequest request) {
		try {
			LOGGER.info(
					"===============  AdminHomeController : Inside updateItemModifierGroup :: Start  ============= ");

			HttpSession session = request.getSession();
			Merchant merchant = (Merchant) session.getAttribute("merchant");
			if (itemModifierGroup != null) {
				modifierService.updateMasterFromModifierGroupOfItems(itemModifierGroup);
			}

			if (merchant != null) {
				List<Item> items = itemService.findByMerchantId(merchant.getId());
				modelMap.addAttribute("items", items);

				List<ModifierGroup> groups = modifierService.findModifierGroupsByMerchantById(merchant.getId());
				modelMap.addAttribute("modifierGroup", groups);

				LOGGER.info("===============  AdminHomeController : Inside updateItemModifierGroup ::merchant "
						+ merchant.getId());

				List<Modifiers> modifiers = modifierRepository.findByMerchantId(merchant.getId());
				modelMap.addAttribute("modifiers", modifiers);
			}

		} catch (Exception e) {
			LOGGER.error("error: " + e.getMessage());
		}
		LOGGER.info("===============  AdminHomeController : Inside updateItemModifierGroup :: End  ============= ");

		return "masterFormOfitemModifierGroup";
	}

	@RequestMapping(value = "/saveModifiersMasterForm", method = RequestMethod.GET)
	public @ResponseBody String saveModifiersMasterForm(@RequestParam("modifierName") String modifierName,
			@RequestParam("modifierPrice") String modifierPrice,
			@RequestParam("modifierGroupId") Integer modifierGroupId, HttpServletRequest request) {
		LOGGER.info("===============  AdminHomeController : Inside saveModifiersMasterForm :: Start  ============= ");

		HttpSession session = request.getSession();
		Merchant merchant = (Merchant) session.getAttribute("merchant");

		Modifiers modifiers = new Modifiers();
		modifiers.setName(modifierName);
		modifiers.setPrice(Double.parseDouble(modifierPrice));
		if (merchant != null) {
			modifiers.setMerchant(merchant);
		}
		LOGGER.info("===============  AdminHomeController : Inside saveModifiersMasterForm :: modifierName "
				+ modifierName + "modifierGroupId " + modifierGroupId);

		Modifiers dbModifiers = modifierRepository.save(modifiers);
		if (modifierGroupId != null && dbModifiers != null && dbModifiers.getId() != null) {

			ModifierModifierGroupDto modifierModifierGroupDto = new ModifierModifierGroupDto();
			ModifierGroup group = new ModifierGroup();
			group.setId(modifierGroupId);

			Modifiers modifiers2 = new Modifiers();
			modifiers2.setId(dbModifiers.getId());

			modifierModifierGroupDto.setModifierGroup(group);
			modifierModifierGroupDto.setModifiers(modifiers2);

			modifierModifierGroupRepository.save(modifierModifierGroupDto);
		}
		LOGGER.info("===============  AdminHomeController : Inside saveModifiersMasterForm :: End  ============= ");

		return "success";
	}

	@RequestMapping(value = "/itemTexes", method = RequestMethod.GET)
	public String addItemtexes(@ModelAttribute("itemTexes") ItemTax itemTexes, ModelMap model,
			HttpServletRequest request) {
		LOGGER.info("===============  AdminHomeController : Inside addItemtexes :: Start  ============= ");

		HttpSession session = request.getSession();
		Merchant merchant = (Merchant) session.getAttribute("merchant");
		List<Item> item = new ArrayList<Item>();
		List<TaxRates> taxRates = new ArrayList<TaxRates>();
		LOGGER.info("===== AdminHomeController : Inside addItemtexes :: merchant  == " + merchant.getId());

		if (merchant.getId() != null) {
			item = itemRepository.findByMerchantId(merchant.getId());
			model.addAttribute("items", item);
			taxRates = taxRateRepository.findByMerchantId(merchant.getId());
			model.addAttribute("taxRates", taxRates);
			LOGGER.info("===============  AdminHomeController : Inside addItemtexes :: taxRates  == " + taxRates);

		}

		return "masterFormOfItemTexes";
	}

	@RequestMapping(value = "/updateItemTexes", method = RequestMethod.POST)
	public String updateItemTexes(@ModelAttribute("itemTexes") ItemTax itemTexes, ModelMap model,
			HttpServletRequest request) {
		LOGGER.info("===============  AdminHomeController : Inside updateItemTexes :: Start  ============= ");

		if (itemTexes != null) {
			List<Integer> listItemIds = itemTexes.getItemsId();
			List<Integer> texesIds = itemTexes.getTexesId();
			Item item = null;
			TaxRates taxRate = null;
			ItemTax itemTaxes = null;
			if (listItemIds != null && !listItemIds.isEmpty() && texesIds != null && !texesIds.isEmpty()) {
				for (Integer itemId : listItemIds) {
					LOGGER.info("===== AdminHomeController : Inside updateItemTexes :: itemId  == " + itemId);

					item = new Item();
					item.setId(itemId);
					for (Integer texesId : texesIds) {
						LOGGER.info("===== AdminHomeController : Inside updateItemTexes :: texesId  == " + texesId);

						taxRate = new TaxRates();
						itemTaxes = new ItemTax();
						taxRate.setId(texesId);
						itemTaxes.setItem(item);
						itemTaxes.setTaxRates(taxRate);
						itemTaxRepository.save(itemTaxes);
					}
				}
			}
		}

		HttpSession session = request.getSession();
		Merchant merchant = (Merchant) session.getAttribute("merchant");
		List<Item> item = new ArrayList<Item>();
		List<TaxRates> taxRates = new ArrayList<TaxRates>();
		LOGGER.info("===== AdminHomeController : Inside updateItemTexes :: merchant  == " + merchant.getId());

		if (merchant.getId() != null) {
			item = itemRepository.findByMerchantId(merchant.getId());
			model.addAttribute("items", item);
			taxRates = taxRateRepository.findByMerchantId(merchant.getId());
			model.addAttribute("taxRates", taxRates);
		}
		LOGGER.info("===============  AdminHomeController : Inside updateItemTexes :: End  ============= ");

		return "masterFormOfItemTexes";
	}

	@RequestMapping(value = "/createItem", method = RequestMethod.GET)
	public String createItem(@ModelAttribute("item") Item item, ModelMap model, HttpServletRequest request) {
		LOGGER.info("===============  AdminHomeController : Inside createItem :: Start  ============= ");

		HttpSession session = request.getSession();
		Merchant merchant = (Merchant) session.getAttribute("merchant");
		List<Item> items = new ArrayList<Item>();
		List<TaxRates> taxRates = new ArrayList<TaxRates>();
		LOGGER.info("===== AdminHomeController : Inside createItem :: merchant  == " + merchant.getId());

		if (merchant.getId() != null) {
			taxRates = taxRateRepository.findByMerchantId(merchant.getId());
			if (taxRates != null && !taxRates.isEmpty()) {
				model.addAttribute("taxRates", taxRates);
			}
		}
		LOGGER.info("===============  AdminHomeController : Inside createItem :: End  ============= ");

		return "createItem";
	}

	@RequestMapping(value = "/createItems", method = RequestMethod.POST)
	public String createItems(@ModelAttribute("item") Item item, ModelMap model, HttpServletRequest request) {
		try {
			LOGGER.info("===============  AdminHomeController : Inside createItems :: Start  ============= ");

			HttpSession session = request.getSession();
			Merchant merchant = (Merchant) session.getAttribute("merchant");
			if (merchant != null && item != null) {
				Item Dbitem = itemService.createItem(item, merchant);

				if (Dbitem != null) {
					TaxRates taxRate = null;
					ItemTax itemTaxes = null;

					List<Integer> texesIds = item.getTexesId();
					if (texesIds != null && texesIds.size() > 0) {
						for (Integer texesId : texesIds) {
							LOGGER.info("===== AdminHomeController : Inside createItems :: texesId  == " + texesId);

							taxRate = new TaxRates();
							itemTaxes = new ItemTax();
							taxRate.setId(texesId);
							itemTaxes.setItem(Dbitem);
							itemTaxes.setTaxRates(taxRate);
							itemTaxes.setActive(1);
							itemTaxRepository.save(itemTaxes);
						}
					}
				}
			}
		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
				LOGGER.error("error: " + e.getMessage());
				LOGGER.error(
						"===============  AdminHomeController : Inside createItems :: Exception  ============= " + e);

				return "redirect:https://www.foodkonnekt.com";
			}
		}
		LOGGER.info("===============  AdminHomeController : Inside createItems :: End  ============= ");

		return "redirect:" + environment.getProperty("BASE_URL") + "/createItem";
	}

	@RequestMapping(value = "/createModifierGroup", method = RequestMethod.GET)
	public String createModifierGroup(@ModelAttribute("ModifierGroups") ModifierGroup modifierGroup, ModelMap model,
			HttpServletRequest request) {
		LOGGER.info("===============  AdminHomeController : Inside createModifierGroup :: Start  ============= ");

		return "createModifierGroup";
	}

	@RequestMapping(value = "/saveModifierGroupForm", method = RequestMethod.POST)
	public String saveModifierGroupForm(@ModelAttribute("ModifierGroups") ModifierGroup modifierGroup, ModelMap model,
			HttpServletRequest request) {
		try {
			LOGGER.info("===============  AdminHomeController : Inside saveModifierGroupForm :: Start  ============= ");

			HttpSession session = request.getSession();
			Merchant merchant = (Merchant) session.getAttribute("merchant");
			LOGGER.info("===== AdminHomeController : Inside saveModifierGroupForm :: merchant  == " + merchant.getId());

			if (merchant != null && modifierGroup != null) {
				List <ModifierGroup> modifierGroupList= modifierGroupRepository.findByMerchantIdAndName(merchant.getId(), modifierGroup.getName());
				if(modifierGroupList==null ||modifierGroupList.isEmpty())
				{
				modifierGroup.setMerchant(merchant);
				modifierGroup.setShowByDefault(1);
				modifierGroup.setActive(1);
				modifierGroupRepository.save(modifierGroup);
				}
			}
		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
				LOGGER.error("error: " + e.getMessage());
				LOGGER.info(
						"===============  AdminHomeController : Inside saveModifierGroupForm :: Exception  ============= "
								+ e);

				return "redirect:https://www.foodkonnekt.com";
			}
		}
		LOGGER.info("===============  AdminHomeController : Inside saveModifierGroupForm :: End  ============= ");

		return "redirect:" + environment.getProperty("BASE_URL") + "/createModifierGroup";
	}

	@RequestMapping(value = "/createCategory", method = RequestMethod.GET)
	public String getCategory(ModelMap modelMap, HttpServletRequest request,
			@ModelAttribute("category") Category category) {
		HttpSession session = request.getSession();
		Merchant merchant = (Merchant) session.getAttribute("merchant");
		try {
			LOGGER.info("===============  AdminHomeController : Inside getCategory :: Start  ============= ");

			if (merchant != null) {
				return "createCategory";
			}
		} catch (Exception e) {
			LOGGER.info("===============  AdminHomeController : Inside getCategory :: Exception  ============= " + e);

			LOGGER.error("error: " + e.getMessage());
		}
		LOGGER.info("===============  AdminHomeController : Inside getCategory :: End  ============= ");

		return "createCategory";
	}

	@RequestMapping(value = "/createCategory", method = RequestMethod.POST)
	public String createCategory(ModelMap modelMap, @ModelAttribute("category") Category category,
			HttpServletRequest request) {
		try {
			LOGGER.info("===============  AdminHomeController : Inside createCategory :: Start  ============= ");

			HttpSession session = request.getSession();
			Merchant merchant = (Merchant) session.getAttribute("merchant");
			if (merchant != null) {
				category.setMerchant(merchant);
				categoryService.createCategory(category);
			}
		} catch (Exception e) {
			LOGGER.info(
					"===============  AdminHomeController : Inside createCategory :: Exception  ============= " + e);

			LOGGER.error("error: " + e.getMessage());
		}
		LOGGER.info("===============  AdminHomeController : Inside createCategory :: End  ============= ");

		return "redirect:" + environment.getProperty("BASE_URL") + "/createCategory";
	}

	@RequestMapping(value = "/createModifier", method = RequestMethod.GET)
	public String createModifier(ModelMap modelMap, HttpServletRequest request,
			@ModelAttribute("modifiers") Modifiers modifiers) {
		HttpSession session = request.getSession();
		Merchant merchant = (Merchant) session.getAttribute("merchant");
		try {
			LOGGER.info("===============  AdminHomeController : Inside createModifier :: Start  ============= ");

			if (merchant != null) {
				return "createModifier";
			}
		} catch (Exception e) {
			LOGGER.info(
					"===============  AdminHomeController : Inside createModifier :: Exception  ============= " + e);

			LOGGER.error("error: " + e.getMessage());
		}
		LOGGER.info("===============  AdminHomeController : Inside createModifier :: End  ============= ");

		return "createModifier";
	}

	@RequestMapping(value = "/createModifier", method = RequestMethod.POST)
	public String createModifier(ModelMap modelMap, @ModelAttribute("modifiers") Modifiers modifiers,
			HttpServletRequest request) {
		try {
			LOGGER.info("===============  AdminHomeController : Inside createModifier :: Start  ============= ");

			HttpSession session = request.getSession();
			Merchant merchant = (Merchant) session.getAttribute("merchant");
			if (merchant != null) {
				List<Modifiers> modifiersList=	modifierService.findByMerchantIdAndName(merchant.getId(), modifiers.getName());
				if(modifiersList==null || modifiersList.isEmpty())
				{
				modifiers.setMerchant(merchant);
				modifiers.setStatus(1);
				modifierService.createModifier(modifiers);
				}
			}
		} catch (Exception e) {
			LOGGER.info(
					"===============  AdminHomeController : Inside createModifier :: Exception  ============= " + e);

			LOGGER.error("error: " + e.getMessage());
		}
		LOGGER.info("===============  AdminHomeController : Inside createModifier :: End  ============= ");

		return "redirect:" + environment.getProperty("BASE_URL") + "/createModifier";
	}

	@RequestMapping(value = "/createPizzaTemplate", method = RequestMethod.GET)
	public String createPizzaTemplate(ModelMap modelMap, @ModelAttribute("PizzaTemplate") PizzaTemplate pizzaTemplate,
			HttpServletRequest request) {
		try {

			LOGGER.info("===============  AdminHomeController : Inside createPizzaTemplate :: Start  ============= ");
			HttpSession session = request.getSession();
			Merchant merchant = (Merchant) session.getAttribute("merchant");
			LOGGER.info("===== AdminHomeController : Inside createPizzaTemplate :: merchant  == " + merchant.getId());

			if (merchant != null && pizzaTemplate != null) {
				List<PizzaSize> pizzaSizes = pizzaSizeRepository.findByMerchantIdAndActive(merchant.getId(), 1);
				if (pizzaSizes != null && !pizzaSizes.isEmpty()) {
					modelMap.addAttribute("pizzaSizes", pizzaSizes);
				}

				List<Category> categories = categoryRepository.findByMerchantIdAndIsPizzaAndItemStatus(merchant.getId(),
						1, 0);
				if (categories != null && !categories.isEmpty()) {
					modelMap.addAttribute("categories", categories);
				}

				List<PizzaTopping> pizzaToppings = pizzaToppingRepository.findByMerchantId(merchant.getId());
				if (pizzaToppings != null && !pizzaToppings.isEmpty()) {
					modelMap.addAttribute("pizzaToppings", pizzaToppings);
				}

				List<TaxRates> taxRates = taxRateRepository.findByMerchantId(merchant.getId());
				if (taxRates != null && !taxRates.isEmpty()) {
					modelMap.addAttribute("taxRates", taxRates);
				}

			}
		} catch (Exception e) {
			LOGGER.info("===============  AdminHomeController : Inside createPizzaTemplate :: Exception  ============= "
					+ e);

			LOGGER.error("error: " + e.getMessage());
		}

		LOGGER.info("===============  AdminHomeController : Inside createPizzaTemplate :: End  ============= ");

		return "createPizzaTemplate";
	}

	@RequestMapping(value = "/savePizzaTemplateMasterForm", method = RequestMethod.POST)
	public String savePizzaTemplate(ModelMap modelMap, @ModelAttribute("PizzaTemplate") PizzaTemplate pizzaTemplate,
			HttpServletRequest request) {
		try {
			String duplicatepizzaCheck = "";
			LOGGER.info("===============  AdminHomeController : Inside savePizzaTemplate :: Start  ============= ");

			HttpSession session = request.getSession();
			Merchant merchant = (Merchant) session.getAttribute("merchant");
			LOGGER.info("===== AdminHomeController : Inside savePizzaTemplate :: merchant  == " + merchant.getId()
					+ " pizzaTemplate " + pizzaTemplate.getId());

			if (merchant != null && pizzaTemplate != null) {
				pizzaTemplate.setMerchant(merchant);
				pizzaTemplate.setIsDefaultTaxRates(true);

				PizzaTemplate duplicatePizzaTemplate = pizzaTemplateRepository
						.findByMerchantIdAndDescription(merchant.getId(), pizzaTemplate.getDescription());
				if (duplicatePizzaTemplate != null) {
					LOGGER.info("Template already exists");
					duplicatepizzaCheck = "Template already exists";
					modelMap.addAttribute("duplicatepizzaCheck", duplicatepizzaCheck);
					return "redirect:" + environment.getProperty("BASE_URL") + "/createPizzaTemplate";
				} else {
					pizzaTemplate.setActive(1);
					PizzaTemplate dbPizzaTemplate = pizzaTemplateRepository.save(pizzaTemplate);

					List<Integer> sizeId = pizzaTemplate.getSizeId();
					List<Double> sizePrice = pizzaTemplate.getSizePrice();

					List<Integer> categoryId = pizzaTemplate.getCatId();
					List<Integer> toppingId = pizzaTemplate.getToppingId();
					// List<Integer> taxRateId = pizzaTemplate.getTaxRateId();

					PizzaTemplateSize pizzaTemplateSize = null;
					PizzaSize pizzaSize = null;
					PizzaTemplate template = null;
					int k = 0;
					if (sizeId != null && !sizeId.isEmpty() && sizePrice != null && !sizePrice.isEmpty()) {
						for (int i = 0; i < sizeId.size(); i++) {
							Double sPrice = 0.0;
							for (int j = k; j < sizePrice.size(); j++) {
								if (sizePrice.get(j) != null) {
									sPrice = sizePrice.get(j);
									k = i + 1;
									break;
								}
							}
							Integer sId = sizeId.get(i);

							pizzaTemplateSize = new PizzaTemplateSize();
							pizzaSize = new PizzaSize();
							template = new PizzaTemplate();
							pizzaSize.setId(sId);
							template.setId(dbPizzaTemplate.getId());

							pizzaTemplateSize.setPizzaTemplate(dbPizzaTemplate);
							pizzaTemplateSize.setPizzaSize(pizzaSize);
							pizzaTemplateSize.setPrice(sPrice);
							pizzaTemplateSize.setActive(1);

							pizzaTemplateSizeRepository.save(pizzaTemplateSize);
						}
					}

					if (categoryId != null && !categoryId.isEmpty()) {
						PizzaTemplateCategory pizzaTemplateCategory = null;
						Category category = null;
						for (Integer catId : categoryId) {
							LOGGER.info(
									"===== AdminHomeController : Inside savePizzaTemplate :: categoryId  == " + catId);

							category = new Category();
							category.setId(catId);
							template = new PizzaTemplate();
							template.setId(dbPizzaTemplate.getId());

							pizzaTemplateCategory = new PizzaTemplateCategory();
							pizzaTemplateCategory.setCategory(category);
							pizzaTemplateCategory.setPizzaTemplate(pizzaTemplate);

							pizzaTemplateCategoryRepository.save(pizzaTemplateCategory);
						}
					}

					if (toppingId != null && !toppingId.isEmpty()) {
						PizzaTemplateTopping pizzaTemplateTopping = null;
						PizzaTopping pizzaTopping = null;
						for (Integer toppId : toppingId) {
							LOGGER.info(
									"===== AdminHomeController : Inside savePizzaTemplate :: toppingId  == " + toppId);
							pizzaTopping = new PizzaTopping();
							pizzaTopping.setId(toppId);
							template = new PizzaTemplate();
							template.setId(dbPizzaTemplate.getId());

							pizzaTemplateTopping = new PizzaTemplateTopping();
							pizzaTemplateTopping.setPizzaTopping(pizzaTopping);
							pizzaTemplateTopping.setPizzaTemplate(pizzaTemplate);
							pizzaTemplateTopping.setActive(true);

							pizzaTemplateToppingRepository.save(pizzaTemplateTopping);
						}
					}

				}

			}
		} catch (Exception e) {
			LOGGER.info(
					"===============  AdminHomeController : Inside savePizzaTemplate :: Exception  ============= " + e);

			LOGGER.error("error: " + e.getMessage());
		}
		LOGGER.info("===============  AdminHomeController : Inside savePizzaTemplate :: End  ============= ");

		return "redirect:" + environment.getProperty("BASE_URL") + "/pizzaTamplate";
	}

	@RequestMapping(value = "/pizzaTemplateMpping", method = RequestMethod.GET)
	public String pizzaTemplateMpping(ModelMap model, @ModelAttribute("PizzaTemplate") PizzaTemplate pizzaTemplate,
			HttpServletRequest request) {

		HttpSession session = request.getSession();
		Merchant merchant = (Merchant) session.getAttribute("merchant");

		LOGGER.info("===============  AdminHomeController : Inside pizzaTemplateMpping :: merchant  ============= "
				+ merchant.getId());

		if (merchant.getId() != null) {
			List<PizzaTemplate> pizzaTemplates = pizzaTemplateRepository.findByMerchantId(merchant.getId());
			if (pizzaTemplates != null && !pizzaTemplates.isEmpty() && pizzaTemplates.size() > 0) {
				model.addAttribute("pizzaTemplates", pizzaTemplates);
			}

			List<PizzaTopping> pizzaToppings = pizzaService.findPizzaToppingMerchantId(merchant.getId());
			if (pizzaToppings != null && !pizzaToppings.isEmpty() && pizzaToppings.size() > 0) {
				model.addAttribute("pizzaToppings", pizzaToppings);
			}

			List<PizzaSize> pizzaSizes = pizzaService.findPizzaSizeMerchantId(merchant.getId());
			if (pizzaSizes != null && !pizzaSizes.isEmpty() && pizzaSizes.size() > 0) {
				model.addAttribute("pizzaSizes", pizzaSizes);
			}

			List<PizzaCrust> pizzaCrusts = pizzaService.findPizzaCrustMerchantId(merchant.getId());
			if (pizzaCrusts != null && !pizzaCrusts.isEmpty() && pizzaCrusts.size() > 0) {
				model.addAttribute("pizzaCrusts", pizzaCrusts);
			}
		} else
			return "redirect:" + environment.getProperty("BASE_URL") + "/support";

		LOGGER.info("===============  AdminHomeController : Inside pizzaTemplateMpping :: End  ============= ");

		return "pizzaTemplateMpping";
	}

	@RequestMapping(value = "/savePizzaTemplateMpping", method = RequestMethod.POST)
	public String savePizzaTemplateMpping(@ModelAttribute("PizzaTemplate") PizzaTemplate pizzaTemplate,
			HttpServletRequest request, Model model) {
		LOGGER.info("===============  AdminHomeController : Inside savePizzaTemplateMpping :: Start  ============= ");

		if (pizzaTemplate != null) {
			List<Integer> sizeId = pizzaTemplate.getSizeId();
			List<Integer> templateId = pizzaTemplate.getTemplateId();
			List<Integer> toppingId = pizzaTemplate.getToppingId();
			List<Integer> crustId = pizzaTemplate.getCrustId();

			PizzaTemplate finalPizzaTemplate = null;

			// save pizzaTemplateSize entry in db
			if (sizeId != null && !sizeId.isEmpty() && templateId != null && !templateId.isEmpty()) {
				PizzaTemplateSize pizzaTemplateSize = null;
				PizzaSize pizzaSize = null;

				for (Integer pizzaTemplateId : templateId) {
					LOGGER.info("===== AdminHomeController : Inside savePizzaTemplateMpping :: templateId  == "
							+ pizzaTemplateId);

					finalPizzaTemplate = new PizzaTemplate();
					finalPizzaTemplate.setId(pizzaTemplateId);

					for (Integer pizzaSizeId : sizeId) {
						LOGGER.info("===== AdminHomeController : Inside savePizzaTemplateMpping :: sizeId  == "
								+ pizzaSizeId);

						pizzaSize = new PizzaSize();
						pizzaTemplateSize = new PizzaTemplateSize();
						PizzaTemplateSize dbPizzaTemplateSize = pizzaTemplateSizeRepository
								.findByPizzaSizeIdAndPizzaTemplateId(pizzaSizeId, pizzaTemplateId);

						if (dbPizzaTemplateSize != null) {
							LOGGER.info("PizzaTemplateSize already have mapping!!");

						} else {
							pizzaSize.setId(pizzaSizeId);
							pizzaTemplateSize.setPizzaSize(pizzaSize);
							pizzaTemplateSize.setPizzaTemplate(finalPizzaTemplate);
							pizzaTemplateSize.setActive(1);
							pizzaTemplateSizeRepository.save(pizzaTemplateSize);
						}
					}
				}
			}

			// save pizzaTemplateTopping entry in db
			if (toppingId != null && !toppingId.isEmpty() && templateId != null && !templateId.isEmpty()) {
				PizzaTemplateTopping pizzaTemplateTopping = null;
				PizzaTopping pizzaTopping = null;

				for (Integer pizzaTemplateId : templateId) {
					finalPizzaTemplate = new PizzaTemplate();
					finalPizzaTemplate.setId(pizzaTemplateId);

					for (Integer pizzaToppingId : toppingId) {
						LOGGER.info("===== AdminHomeController : Inside savePizzaTemplateMpping :: toppingId  == "
								+ pizzaToppingId);

						pizzaTopping = new PizzaTopping();
						pizzaTemplateTopping = new PizzaTemplateTopping();

						PizzaTemplateTopping dbPizzaTemplateTopping = pizzaTemplateToppingRepository
								.findByPizzaTemplateIdAndPizzaToppingId(pizzaTemplateId, pizzaToppingId);
						if (dbPizzaTemplateTopping != null) {
							LOGGER.info("PizzaTemplateTopping already have mapping!!");
						} else {
							pizzaTopping.setId(pizzaToppingId);
							pizzaTemplateTopping.setPizzaTemplate(finalPizzaTemplate);
							pizzaTemplateTopping.setPizzaTopping(pizzaTopping);
							pizzaTemplateTopping.setActive(true);
							pizzaTemplateToppingRepository.save(pizzaTemplateTopping);
						}
					}
				}
			}

			// save pizzaTemplateCrust entry in db
			if (crustId != null && !crustId.isEmpty() && templateId != null && !templateId.isEmpty()) {
				PizzaTemplateCrust pizzaTemplateCrust = null;
				PizzaCrust pizzaCrust = null;

				for (Integer pizzaTemplateId : templateId) {
					finalPizzaTemplate = new PizzaTemplate();
					finalPizzaTemplate.setId(pizzaTemplateId);

					for (Integer pizzaCrustId : crustId) {
						LOGGER.info("===== AdminHomeController : Inside savePizzaTemplateMpping :: crustId  == "
								+ pizzaCrustId);

						pizzaCrust = new PizzaCrust();
						pizzaTemplateCrust = new PizzaTemplateCrust();

						PizzaTemplateCrust dbPizzaTemplateCrust = pizzaTemplateCrustRepository
								.findByPizzaTemplateIdAndPizzaCrustId(pizzaTemplateId, pizzaCrustId);
						if (dbPizzaTemplateCrust != null) {
							System.out.println("PizzaTemplateCrsut already have mapping!!");
						} else {
							pizzaCrust.setId(pizzaCrustId);
							pizzaTemplateCrust.setPizzaCrust(pizzaCrust);
							pizzaTemplateCrust.setPizzaTemplate(finalPizzaTemplate);
							pizzaTemplateCrustRepository.save(pizzaTemplateCrust);
						}
					}
				}
			}

			// save pizzaToppingSize entry in db
			if (toppingId != null && !toppingId.isEmpty() && sizeId != null && !sizeId.isEmpty()) {
				PizzaToppingSize pizzaToppingSize = null;
				PizzaSize pizzaSize = null;
				PizzaTopping pizzaTopping = null;
				for (Integer pizzaToppingId : toppingId) {
					pizzaTopping = new PizzaTopping();
					pizzaTopping.setId(pizzaToppingId);

					for (Integer pizzaSizeId : sizeId) {
						pizzaSize = new PizzaSize();
						pizzaToppingSize = new PizzaToppingSize();

						PizzaToppingSize dbPizzaToppingSize = pizzaToppingSizeRepository
								.findByPizzaToppingIdAndPizzaSizeId(pizzaToppingId, pizzaSizeId);
						if (dbPizzaToppingSize != null) {
							System.out.println("PizzaToppingSize already have mapping!!");
						} else {
							pizzaSize.setId(pizzaSizeId);
							pizzaToppingSize.setPizzaSize(pizzaSize);
							pizzaToppingSize.setPizzaTopping(pizzaTopping);
							pizzaToppingSizeRepository.save(pizzaToppingSize);
						}
					}
				}
			}
		}

		HttpSession session = request.getSession();
		Merchant merchant = (Merchant) session.getAttribute("merchant");

		if (merchant.getId() != null) {
			List<PizzaTemplate> pizzaTemplates = pizzaTemplateRepository.findByMerchantId(merchant.getId());
			if (pizzaTemplates != null && !pizzaTemplates.isEmpty()) {
				model.addAttribute("pizzaTemplates", pizzaTemplates);
			}

			List<PizzaTopping> pizzaToppings = pizzaService.findPizzaToppingMerchantId(merchant.getId());
			if (pizzaToppings != null && !pizzaToppings.isEmpty()) {
				model.addAttribute("pizzaToppings", pizzaToppings);
			}

			List<PizzaSize> pizzaSizes = pizzaService.findPizzaSizeMerchantId(merchant.getId());
			if (pizzaSizes != null && !pizzaSizes.isEmpty()) {
				model.addAttribute("pizzaSizes", pizzaSizes);
			}

			List<PizzaCrust> pizzaCrusts = pizzaService.findPizzaCrustMerchantId(merchant.getId());
			if (pizzaCrusts != null && !pizzaCrusts.isEmpty()) {
				model.addAttribute("pizzaCrusts", pizzaCrusts);
			}
		}
		LOGGER.info("===============  AdminHomeController : Inside savePizzaTemplateMppingsss :: End  ============= ");

		return "redirect:" + environment.getProperty("BASE_URL") + "/pizzaTemplateMpping";
	}

	@RequestMapping(value = "/createPizzaTopping", method = RequestMethod.GET)
	public String createPizzaTopping(ModelMap modelMap, @ModelAttribute("PizzaTopping") PizzaTopping pizzaTopping,
			HttpServletRequest request) {
		LOGGER.info("===============  AdminHomeController : Inside createPizzaTopping :: Start  ============= ");

		HttpSession session = request.getSession();
		Merchant merchant = (Merchant) session.getAttribute("merchant");

		if (merchant != null && merchant.getId() != null) {

			List<PizzaSize> pizzaSize = pizzaSizeRepository.findByMerchantIdAndActive(merchant.getId(), 1);

			if (pizzaSize != null && !pizzaSize.isEmpty()) {
				modelMap.addAttribute("pizzaSize", pizzaSize);
			}
			LOGGER.info("===== AdminHomeController : Inside createPizzaTopping :: merchant  == " + merchant.getId());

			List<PizzaTemplate> pizzaTemplates = pizzaTemplateRepository.findByMerchantId(merchant.getId());
			if (pizzaTemplates != null && !pizzaTemplates.isEmpty()) {
				modelMap.addAttribute("pizzaTemplates", pizzaTemplates);
			}
		}
		LOGGER.info("===============  AdminHomeController : Inside createPizzaTopping :: End  ============= ");

		return "createPizzaTopping";
	}

	@RequestMapping(value = "/savePizzaToppingMasterForm", method = RequestMethod.POST)
	public String savePizzaToppingMasterForm(ModelMap modelMap,
			@ModelAttribute("PizzaTopping") PizzaTopping pizzaTopping, HttpServletRequest request) {
		LOGGER.info(
				"===============  AdminHomeController : Inside savePizzaToppingMasterForm :: Start  ============= ");
		try {
			HttpSession session = request.getSession();
			Merchant merchant = (Merchant) session.getAttribute("merchant");
			if (merchant != null && pizzaTopping != null) {
				pizzaTopping.setMerchant(merchant);
				pizzaTopping.setActive(1);
				LOGGER.info("===== AdminHomeController : Inside savePizzaToppingMasterForm :: merchant  == "
						+ merchant.getId() + " pizzaTopping " + pizzaTopping.getDescription());

				List<PizzaTopping> pizzaToppingList = pizzaToppingRepository
						.findByDescriptionAndMerchantId(pizzaTopping.getDescription(), merchant.getId());

				if (pizzaToppingList.size() > 0) {
					LOGGER.info("Topping already exists");
					modelMap.addAttribute("duplicatetoppingCheck", "Topping Already Exist");

					return "redirect:" + environment.getProperty("BASE_URL") + "/createPizzaTopping";
				} else {
					PizzaTopping dbPizzaTopping = pizzaToppingRepository.save(pizzaTopping);

					List<Integer> sizeIds = pizzaTopping.getSizeIds();
					List<Double> sizePrice = pizzaTopping.getSizePrice();
					List<Integer> templateId = pizzaTopping.getTemplateId();
					LOGGER.info(
							"===============  AdminHomeController : Inside savePizzaToppingMasterForm :: SizeId  ============= "
									+ sizeIds);
					int k = 0;
					if (sizeIds != null && !sizeIds.isEmpty() && sizePrice != null && !sizePrice.isEmpty()) {
						for (int i = 0; i < sizeIds.size(); i++) {
							LOGGER.info(
									"===============  AdminHomeController : Inside savePizzaToppingMasterForm :: inside first loop  ============= ");
							Double sPrice = 0.0;
							for (int j = k; j < sizePrice.size(); j++) {
								LOGGER.info(
										"===============  AdminHomeController : Inside savePizzaToppingMasterForm :: inside second loop  ============= ");
								if (sizePrice.get(j) != null) {
									sPrice = sizePrice.get(j);
									k = i + 1;
									break;
								}
							}
							PizzaToppingSize pizzaToppingSize = new PizzaToppingSize();
							PizzaSize pizzaSize = new PizzaSize();
							pizzaSize.setId(sizeIds.get(i));

							pizzaToppingSize.setPizzaTopping(dbPizzaTopping);
							pizzaToppingSize.setPrice(sPrice);
							pizzaToppingSize.setPizzaSize(pizzaSize);
							pizzaToppingSize.setActive(0);

							pizzaToppingSizeRepository.save(pizzaToppingSize);
							LOGGER.info(
									"===============  AdminHomeController : Inside savePizzaToppingMasterForm :: pizzaToppingSize save ============= ");
						}
					}

					if (templateId != null && !templateId.isEmpty()) {
						PizzaTemplate pizzaTemplate = null;
						PizzaTemplateTopping pizzaTemplateTopping = null;
						for (Integer tempId : templateId) {
							LOGGER.info(
									"===== AdminHomeController : Inside savePizzaToppingMasterForm :: templateId  == "
											+ tempId);

							pizzaTemplate = new PizzaTemplate();
							pizzaTemplate.setId(tempId);

							pizzaTemplateTopping = new PizzaTemplateTopping();
							pizzaTemplateTopping.setPizzaTemplate(pizzaTemplate);
							pizzaTemplateTopping.setPizzaTopping(dbPizzaTopping);
							pizzaTemplateTopping.setActive(true);
							pizzaTemplateToppingRepository.save(pizzaTemplateTopping);
						}
					}
				}
			} else
				return "redirect:" + environment.getProperty("BASE_URL") + "/support";
		} catch (Exception e) {
			LOGGER.info(
					"===============  AdminHomeController : Inside savePizzaToppingMasterForm :: Exception  ============= "
							+ e);
			LOGGER.error("error: " + e.getMessage());
			MailSendUtil.sendExceptionByMail(e,environment);
		}
		LOGGER.info("===============  AdminHomeController : Inside savePizzaToppingMasterForm :: End  ============= ");
		return "redirect:" + environment.getProperty("BASE_URL") + "/pizzaTopping";
	}

	@RequestMapping(value = "/pizzaCategoryMap", method = RequestMethod.GET)
	public String pizzaCategoryMap(@ModelAttribute("PizzaTemplate") PizzaTemplate pizzaTemplate,
			HttpServletRequest request, ModelMap model) {
		LOGGER.info("===============  AdminHomeController : Inside createPizzaTopping :: Start  ============= ");

		HttpSession session = request.getSession();
		Merchant merchant = (Merchant) session.getAttribute("merchant");
		LOGGER.info("===== AdminHomeController : Inside pizzaCategoryMap :: merchant  == " + merchant.getId());

		if (merchant.getId() != null) {
			List<PizzaTemplate> pizzaTemplates = pizzaTemplateRepository.findByMerchantId(merchant.getId());
			if (pizzaTemplates != null && !pizzaTemplates.isEmpty()) {
				model.addAttribute("pizzaTemplates", pizzaTemplates);
			}

			List<Category> categories = categoryRepository.findByMerchantId(merchant.getId());
			if (categories != null && !categories.isEmpty()) {
				model.addAttribute("categories", categories);
			}
		}
		LOGGER.info("===============  AdminHomeController : Inside createPizzaTopping :: End  ============= ");

		return "pizzaCategoryMap";
	}

	@RequestMapping(value = "/deleteDeliveryZone", method = RequestMethod.GET)
	public String deleteDeliveryZone(@RequestParam("zoneId") Integer zoneId) {
		LOGGER.info("===============  AdminHomeController : Inside deleteDeliveryZone :: Start  =============zoneId "
				+ zoneId);

		if (zoneId != null) {
			try {

				zoneService.deleteDeliveryZone(zoneId);
			} catch (Exception e) {
				LOGGER.error("error: " + e.getMessage());
			}
		}
		LOGGER.info("===============  AdminHomeController : Inside deleteDeliveryZone :: End  ============= ");

		return "redirect:" + environment.getProperty("BASE_URL") + "/deliveryZones";
	}

	@RequestMapping(value = "/savePizzaCategoryMap", method = RequestMethod.POST)
	public String savePizzaCategoryMap(@ModelAttribute("PizzaTemplate") PizzaTemplate pizzaTemplate,
			HttpServletRequest request, ModelMap model) {
		LOGGER.info("===============  AdminHomeController : Inside savePizzaCategoryMap :: Start  ============= ");

		if (pizzaTemplate != null) {
			List<Integer> templateId = pizzaTemplate.getTemplateId();
			List<Integer> categoryId = pizzaTemplate.getCatId();

			if (categoryId != null && !categoryId.isEmpty()) {
				PizzaTemplateCategory pizzaTemplateCategory = null;
				Category category = null;
				PizzaTemplate template = null;
				for (Integer catId : categoryId) {
					category = new Category();
					category.setId(catId);

					for (Integer tempId : templateId) {
						pizzaTemplateCategory = new PizzaTemplateCategory();
						template = new PizzaTemplate();
						LOGGER.info("===== AdminHomeController : Inside savePizzaCategoryMap :: templateId  == "
								+ tempId + " categoryId " + catId);

						PizzaTemplateCategory dbPizzaTemplateCategory = pizzaTemplateCategoryRepository
								.findByPizzaTemplateIdAndCategoryId(tempId, catId);
						if (dbPizzaTemplateCategory != null) {
							System.out.println("PizzaTemplateCategory already have mapping!!");
						} else {
							template.setId(tempId);
							pizzaTemplateCategory.setCategory(category);
							pizzaTemplateCategory.setPizzaTemplate(template);

							pizzaTemplateCategoryRepository.save(pizzaTemplateCategory);
						}
					}
				}
			}
		}

		HttpSession session = request.getSession();
		Merchant merchant = (Merchant) session.getAttribute("merchant");
		LOGGER.info("===== AdminHomeController : Inside savePizzaCategoryMap :: merchant  == " + merchant.getId());

		if (merchant.getId() != null) {
			List<PizzaTemplate> pizzaTemplates = pizzaTemplateRepository.findByMerchantId(merchant.getId());
			if (pizzaTemplates != null && !pizzaTemplates.isEmpty()) {
				model.addAttribute("pizzaTemplates", pizzaTemplates);
			}

			List<Category> categories = categoryRepository.findByMerchantId(merchant.getId());
			if (categories != null && !categories.isEmpty()) {
				model.addAttribute("categories", categories);
			}
		}
		LOGGER.info("===============  AdminHomeController : Inside savePizzaCategoryMap :: End  ============= ");

		return "redirect:" + environment.getProperty("BASE_URL") + "/pizzaCategoryMap";
	}

	@RequestMapping(value = "/editPizzaTopping", method = RequestMethod.GET)
	public String viewEditPizzaTopping(@ModelAttribute("pizzaTopping") PizzaTopping pizzaTopping, ModelMap model,
			HttpServletRequest request, @RequestParam(required = false) int toppingId) {
		try {
			LOGGER.info(
					"===============  AdminHomeController : Inside viewEditPizzaTopping :: Start  =============toppingId "
							+ toppingId);

			HttpSession session = request.getSession(false);
			if (session != null) {
				Merchant merchant = (Merchant) session.getAttribute("merchant");
				if (merchant != null) {

					pizzaTopping = pizzaService.findByToppingId(toppingId);
					model.addAttribute("description", pizzaTopping.getDescription());
					model.addAttribute("status", pizzaTopping.getStatus());
					model.addAttribute("pizzaTopping", pizzaTopping);

					List<PizzaToppingSize> pizzaToppingSizeList = pizzaToppingSizeRepository
							.findByPizzaToppingId(toppingId);
					model.addAttribute("pizzaToppingSizeList", pizzaToppingSizeList);
					List<PizzaSize> dropdownPizzaSize = null;
					List<Integer> pizzasizeid = new ArrayList<Integer>();
					for (int i = 0; i < pizzaToppingSizeList.size(); i++) {
						pizzasizeid.add(pizzaToppingSizeList.get(i).getPizzaSize().getId());
					}
					if (pizzasizeid.size() > 0) {
						dropdownPizzaSize = pizzaSizeRepository.findByMerchantIdAndId(merchant.getId(), pizzasizeid);
					} else
						dropdownPizzaSize = pizzaSizeRepository.findByMerchantId(merchant.getId());

					model.addAttribute("dropdownPizzaSize", dropdownPizzaSize);

				}
			}
		} catch (Exception e) {

			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
				LOGGER.error("error: " + e.getMessage());
				LOGGER.info(
						"===============  AdminHomeController : Inside viewEditPizzaTopping :: Exception  ============= "
								+ e);

				return "redirect:https://www.foodkonnekt.com";
			}
		}
		LOGGER.info("===============  AdminHomeController : Inside viewEditPizzaTopping :: End  ============= ");

		return "editPizzaTopping";

	}

	@RequestMapping(value = "/updatePizzaTopping", method = RequestMethod.POST)
	public String updatePizzaTopping(@ModelAttribute() PizzaTopping pizzaTopping, ModelMap model) {
		try {
			LOGGER.info("===============  AdminHomeController : Inside updatePizzaTopping :: Start  ============= ");

			PizzaTopping result = new PizzaTopping();

			result = pizzaService.updatePizzaToppingById(pizzaTopping);

			if (result != null) {
				pizzaTopping.setDescription(result.getDescription());

				model.addAttribute("description", result.getDescription());
				model.addAttribute("status", result.getStatus());
				model.addAttribute("pizzaTopping", result);
				model.addAttribute("pizzaToppingSizes", result.getPizzaToppingSizes());

			}

		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
				LOGGER.error("error: " + e.getMessage());
				LOGGER.info(
						"===============  AdminHomeController : Inside updatePizzaTopping :: Exception  ============= "
								+ e);

				return "redirect:https://www.foodkonnekt.com";
			}
		}
		LOGGER.info("===============  AdminHomeController : Inside updatePizzaTopping :: End  ============= ");

		return "redirect:" + environment.getProperty("BASE_URL") + "/pizzaTopping";
	}

	@RequestMapping(value = "/downloadFoodTronixExe", method = RequestMethod.GET)
	@ResponseBody
	public String downloadFoodTronixExe(HttpServletResponse response, HttpServletRequest request) {
		try {
			LOGGER.info("===============  AdminHomeController : Inside downloadFoodTronixExe :: Start  ============= ");

			response.setContentType("APPLICATION/DOWNLOAD;charset=UTF-8");
			response.setCharacterEncoding("UTF-8");
			String reportName = "foodkonnekt.zip";
			response.setHeader("Content-disposition", "attachment;filename=" + reportName);
			String fileURL = environment.getProperty("FOODTRONIX_INSTALLER") + "foodkonnekt.zip";
			FileInputStream inputStream = new FileInputStream(fileURL);
			int bytesRead = -1;
			byte[] buffer = new byte[inputStream.available()];

			while ((bytesRead = inputStream.read(buffer)) != -1) {

				response.getOutputStream().write(buffer);
				LOGGER.info(new String(buffer));

			}
			inputStream.close();

		} catch (Exception e) {
			LOGGER.info(
					"===============  AdminHomeController : Inside downloadFoodTronixExe :: Exception  ============= "
							+ e);

			// TODO Auto-generated catch block
			LOGGER.error("error: " + e.getMessage());
		}
		LOGGER.info("===============  AdminHomeController : Inside downloadFoodTronixExe :: End  ============= ");

		return "success";
	}

	@RequestMapping(value = "/pizzaCrust")
	public String pizzaCrust(@ModelAttribute("pizzaCrust") PizzaCrust pizzaCrust, HttpServletRequest request,
			Model model) {
		LOGGER.info("===============  AdminHomeController : Inside pizzaCrust :: Start  ============= ");

		HttpSession session = request.getSession(false);
		if (session != null) {
			Merchant merchant = (Merchant) session.getAttribute("merchant");
			LOGGER.info("===== AdminHomeController : Inside pizzaCrust :: merchant  == " + merchant.getId());

			if (merchant != null) {
				List<PizzaTemplate> pizzaTemplates = pizzaTemplateRepository.findByMerchantId(merchant.getId());
				if (pizzaTemplates != null && !pizzaTemplates.isEmpty()) {
					model.addAttribute("pizzaTemplates", pizzaTemplates);
				}
			} else
				return "redirect:" + environment.getProperty("BASE_URL") + "/support";
		} else
			return "redirect:" + environment.getProperty("BASE_URL") + "/support";

		LOGGER.info("===============  AdminHomeController : Inside pizzaCrust :: End  ============= ");

		return "pizzaCrust";
	}

	@RequestMapping(value = "/savePizzaCrust", method = RequestMethod.POST)
	public String savePizzaCrust(@ModelAttribute("pizzaCrust") PizzaCrust pizzaCrust, HttpServletRequest request,
			Model model) {
		LOGGER.info("===============  AdminHomeController : Inside savePizzaCrust :: Start  ============= ");

		if (pizzaCrust != null) {

			LOGGER.info("===============  AdminHomeController : Inside if (pizzaCrust != null)");

			Merchant merchant = null;
			HttpSession session = request.getSession(false);
			if (session != null) {
				merchant = (Merchant) session.getAttribute("merchant");
				LOGGER.info("===== AdminHomeController : Inside savePizzaCrust :: merchant  == " + merchant.getId());

				if (merchant != null) {

					List<PizzaCrust> pizzaCrustList = pizzaCrustRepository
							.findByDescriptionAndMerchantId(pizzaCrust.getDescription(), merchant.getId());

					if (pizzaCrustList.size() > 0) {
						LOGGER.info("======= Crust already exists ======== ");
						model.addAttribute("duplicateCrustCheck", "Crust Already Exist");
						return "redirect:" + environment.getProperty("BASE_URL") + "/createPizzaCrust";

					} else {

						List<PizzaTemplate> pizzaTemplates = pizzaTemplateRepository.findByMerchantId(merchant.getId());
						if (pizzaTemplates != null && !pizzaTemplates.isEmpty()) {
							model.addAttribute("pizzaTemplates", pizzaTemplates);
						}

						pizzaService.savePizzaCrust(pizzaCrust, merchant);

					}
				}
			}
		}
		LOGGER.info("===============  AdminHomeController : Inside savePizzaCrust:: End  ============= ");

		return "pizzaCrust";
	}

	@RequestMapping(value = "/templateTaxMap")
	public String templateTaxMap(@ModelAttribute("pizzaTemplateTax") PizzaTemplateTax pizzaTemplateTax,
			HttpServletRequest request, Model model) {
		LOGGER.info("===============  AdminHomeController : Inside templateTaxMap :: Start  ============= ");

		Merchant merchant = null;
		HttpSession session = request.getSession(false);
		if (session != null) {
			merchant = (Merchant) session.getAttribute("merchant");
			LOGGER.info("===== AdminHomeController : Inside templateTaxMap :: merchant  == " + merchant.getId());

			if (merchant != null) {

				List<PizzaTemplate> pizzaTemplates = pizzaTemplateRepository.findByMerchantIdAndActive(merchant.getId(),
						1);
				if (pizzaTemplates != null && pizzaTemplates.size() > 0) {
					model.addAttribute("pizzaTemplates", pizzaTemplates);
				}

				List<TaxRates> taxRates = taxRateRepository.findByMerchantId(merchant.getId());
				if (taxRates != null && taxRates.size() > 0) {
					model.addAttribute("taxRates", taxRates);
				}
			}
		}

		LOGGER.info("===============  AdminHomeController : Inside templateTaxMap :: End  ============= ");

		return "templateTaxMap";
	}

	@RequestMapping(value = "/saveTemplateTaxMap", method = RequestMethod.POST)
	public String saveTemplateTaxMap(@ModelAttribute("pizzaTemplateTax") PizzaTemplateTax pizzaTemplateTax,
			HttpServletRequest request, Model model) {

		LOGGER.info("===============  AdminHomeController : Inside saveTemplateTaxMap :: Start  ============= ");

		List<Integer> templateId = pizzaTemplateTax.getTemplateId();
		List<Integer> taxId = pizzaTemplateTax.getTaxId();

		PizzaTemplate pizzaTemplate = null;
		TaxRates taxRates = null;
		PizzaTemplateTax templateTax = null;

		List<PizzaTemplateTax> pizzaTemplateTaxes = new ArrayList<PizzaTemplateTax>();

		if (taxId != null && taxId.size() > 0) {
			for (Integer rateId : taxId) {
				if (templateId != null && templateId.size() > 0) {
					taxRates = new TaxRates();

					for (Integer tempId : templateId) {
						LOGGER.info("===== AdminHomeController : Inside saveTemplateTaxMap :: templateId  == " + tempId
								+ " taxId " + rateId);

						PizzaTemplateTax pizzaTemplateTaxs = pizzaTemplateTaxRepository
								.findByPizzaTemplateIdAndTaxRatesId(tempId, rateId);
						pizzaTemplate = new PizzaTemplate();
						templateTax = new PizzaTemplateTax();

						if (pizzaTemplateTaxs != null) {
							LOGGER.info("mapping already exists");
						} else {
							pizzaTemplate.setId(tempId);
							taxRates.setId(rateId);
							templateTax.setPizzaTemplate(pizzaTemplate);
							templateTax.setTaxRates(taxRates);
							templateTax.setIsActive(1);
							pizzaTemplateTaxes.add(templateTax);
						}
					}
					pizzaTemplateTaxRepository.save(pizzaTemplateTaxes);
				}
			}
		}
		LOGGER.info("===============  AdminHomeController : Inside saveTemplateTaxMap :: End  ============= ");

		return "redirect:" + environment.getProperty("BASE_URL") + "/pizzaTamplate";
	}

	@RequestMapping(value = "/uploadCategoryImage", method = RequestMethod.POST)
	public String uploadCategoryImage(@ModelAttribute("Category") Category category, ModelMap model,
			HttpServletRequest request, @RequestParam(required = false) Integer categoryId,
			@RequestParam("file") MultipartFile file) {
		try {
			LOGGER.info(
					"===============  AdminHomeController : Inside uploadCategoryImage :: Start  =============categoryId "
							+ categoryId);

			categoryService.updateCategoryValue(category, file);
		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
				LOGGER.error("error: " + e.getMessage());
				LOGGER.info(
						"===============  AdminHomeController : Inside uploadCategoryImage :: Exception  ============= "
								+ e);

				return "redirect:https://www.foodkonnekt.com";
			}
		}
		LOGGER.info("===============  AdminHomeController : Inside uploadCategoryImages :: End  ============= ");

		return "redirect:" + environment.getProperty("BASE_URL") + "/category";
	}

	@RequestMapping(value = "/getPizzaTemplateTopping", method = RequestMethod.POST)
	@ResponseBody
	public List<PizzaTemplateTopping> getPizzaTemplateTopping(@RequestParam(required = true) Integer templateId,
			@RequestParam(required = true) List<Integer> toppingIds) {

		List<PizzaTemplateTopping> pizzaTemplateToppingList = new ArrayList<PizzaTemplateTopping>();
		try {
			LOGGER.info(
					"===============  AdminHomeController : Inside getPizzaTemplateTopping :: Start  =============templateId "
							+ templateId);

			for (Integer toppingId : toppingIds) {
				LOGGER.info("===== AdminHomeController : Inside getPizzaTemplateTopping :: toppingId  == " + toppingId);

				PizzaTemplateTopping pizzaTemplateTopping = pizzaTemplateToppingRepository
						.findByPizzaTemplateIdAndPizzaToppingId(templateId, toppingId);
				if (pizzaTemplateTopping != null) {

					pizzaTemplateTopping.setIncluded(true);
					pizzaTemplateTopping = pizzaTemplateToppingRepository.save(pizzaTemplateTopping);

				}

			}
			pizzaTemplateToppingList = pizzaTemplateToppingRepository.findByPizzaTemplateIds(templateId);
			for (PizzaTemplateTopping pizzaTemplateTopping1 : pizzaTemplateToppingList) {
				pizzaTemplateTopping1.setPizzaTemplate(null);
				pizzaTemplateTopping1.getPizzaTopping().setMerchant(null);
				pizzaTemplateTopping1.getPizzaTopping().setPizzaTemplateTopping(null);
				pizzaTemplateTopping1.getPizzaTopping().setPizzaTemplateToppings(null);
				List<PizzaToppingSize> pizzaToppingSize = pizzaTemplateTopping1.getPizzaTopping()
						.getPizzaToppingSizes();
				for (PizzaToppingSize pizzaToppingSize1 : pizzaToppingSize) {
					if (pizzaToppingSize1 != null) {
						pizzaToppingSize1.setPizzaTopping(null);
						PizzaSize pizzaSize = pizzaToppingSize1.getPizzaSize();
						pizzaSize.setMerchant(null);
						pizzaSize.setCategories(null);
						pizzaSize.setPizzaCrustSizes(null);
						pizzaSize.setPizzaTemplates(null);
						pizzaSize.setPizzaToppingSizes(null);
					}
				}

			}
		} catch (Exception e) {
			if (e != null) {
				LOGGER.info(
						"===============  AdminHomeController : Inside getPizzaTemplateTopping :: Exception  ============= "
								+ e);

				MailSendUtil.sendExceptionByMail(e,environment);
				LOGGER.error("error: " + e.getMessage());
			}
		}
		LOGGER.info("===============  AdminHomeController : Inside getPizzaTemplateTopping :: End  ============= ");

		return pizzaTemplateToppingList;
	}

	@RequestMapping(value = "/getReplaceblePizza", method = RequestMethod.POST)
	@ResponseBody
	public List<PizzaTemplateTopping> getReplaceblePizza(@RequestParam(required = true) Integer pizatemplateToppingId,
			@RequestParam(required = true) Integer templateId, @RequestParam(required = true) Integer toppingId,
			@RequestParam(required = true) Boolean isIncluded) {
		List<PizzaTemplateTopping> pizzaTemplateToppingList = new ArrayList<PizzaTemplateTopping>();
		List<PizzaTemplateSize> pizzaTemplateSize = null;
		List<PizzaTopping> PizzaTopping = new ArrayList<PizzaTopping>();
		try {
			LOGGER.info(
					"===============  AdminHomeController : Inside getReplaceblePizza :: Start  =============pizatemplateToppingId "
							+ pizatemplateToppingId + " templateId " + templateId);

			if (pizatemplateToppingId != 0) {
				PizzaTemplateTopping pizzaTemplateTopping = pizzaTemplateToppingRepository
						.findById(pizatemplateToppingId);
				if (pizzaTemplateTopping != null) {
					if (isIncluded == true) {
						if (pizzaTemplateTopping.isIncluded() == false)
							pizzaTemplateTopping.setIncluded(true);
						else if (pizzaTemplateTopping.isIncluded() == true) {
							pizzaTemplateTopping.setIncluded(false);
							pizzaTemplateTopping.setReplacable(false);
						}
						pizzaTemplateTopping = pizzaTemplateToppingRepository.save(pizzaTemplateTopping);

					} else {
						if (pizzaTemplateTopping.isReplacable() == false)
							pizzaTemplateTopping.setReplacable(true);
						else if (pizzaTemplateTopping.isReplacable() == true)
							pizzaTemplateTopping.setReplacable(false);
						pizzaTemplateTopping = pizzaTemplateToppingRepository.save(pizzaTemplateTopping);
					}
				}
			} else {
				PizzaTemplateTopping pizzaTemplateTopping = new PizzaTemplateTopping();
				PizzaTemplate pizzaTemplate = pizzaTemplateRepository.findOne(templateId);
				PizzaTopping pizzaTopping = pizzaToppingRepository.findOne(toppingId);
				if (pizzaTemplate != null)
					pizzaTemplateTopping.setPizzaTemplate(pizzaTemplate);
				if (pizzaTopping != null)
					pizzaTemplateTopping.setPizzaTopping(pizzaTopping);
				pizzaTemplateTopping.setPrice(0);
				pizzaTemplateTopping.setActive(true);
				pizzaTemplateTopping.setIncluded(true);
				pizzaTemplateToppingRepository.save(pizzaTemplateTopping);
			}
			pizzaTemplateToppingList = pizzaTemplateToppingRepository.findByPizzaTemplateId(templateId);
			for (PizzaTemplateTopping pizzaTemplateTopping1 : pizzaTemplateToppingList) {
				pizzaTemplateTopping1.setPizzaTemplate(null);
				pizzaTemplateTopping1.getPizzaTopping().setMerchant(null);
				pizzaTemplateTopping1.getPizzaTopping().setPizzaTemplateTopping(null);
				pizzaTemplateTopping1.getPizzaTopping().setPizzaTemplateToppings(null);
				List<PizzaToppingSize> pizzaToppingSize = pizzaTemplateTopping1.getPizzaTopping()
						.getPizzaToppingSizes();
				for (PizzaToppingSize pizzaToppingSize1 : pizzaToppingSize) {
					if (pizzaToppingSize1 != null) {
						pizzaToppingSize1.setPizzaTopping(null);
						PizzaSize pizzaSize = pizzaToppingSize1.getPizzaSize();
						pizzaSize.setMerchant(null);
						pizzaSize.setCategories(null);
						pizzaSize.setPizzaCrustSizes(null);
						pizzaSize.setPizzaTemplates(null);
						pizzaSize.setPizzaToppingSizes(null);
					}
				}

			}
//			if(templateId!=null)
//			{
//				pizzaTemplateSize=pizzaService.findPizzaTemplateSizeByTemplateId(templateId);
//				if(pizzaTemplateSize!=null && pizzaTemplateSize.size()>0)
//				{
//					int i=0;
//					for (PizzaTemplateSize pizzaTemplateSize2 : pizzaTemplateSize) {
//						
//						if(pizzaTemplateSize2!=null && pizzaTemplateSize2.getPizzaSize()!=null && pizzaTemplateSize2.getPizzaSize().getId()!=null)
//						{
//							List<PizzaToppingSize> pizzaToppingSize1=pizzaService.findByPizzaSizeId(pizzaTemplateSize2.getPizzaSize().getId());
//							for (PizzaToppingSize pizzaToppingSize : pizzaToppingSize1) {
//								if(pizzaToppingSize.getPizzaTopping().getActive()==1)
//								PizzaTopping.add(pizzaToppingSize.getPizzaTopping());
//							}
//						}
//						i++;
//						break;
//					}
//					
//					pizzaTemplateToppingList = pizzaTemplateToppingRepository
//							.findByPizzaTemplateIds(templateId);
//					 for(PizzaTemplateTopping pizzaTemplateTopping1 : pizzaTemplateToppingList){
//							pizzaTemplateTopping1.setPizzaTemplate(null);
//							pizzaTemplateTopping1.getPizzaTopping().setMerchant(null);
//							pizzaTemplateTopping1.getPizzaTopping()
//									.setPizzaTemplateTopping(null);
//							pizzaTemplateTopping1.getPizzaTopping()
//									.setPizzaTemplateToppings(null);
//							List<PizzaToppingSize> pizzaToppingSize = pizzaTemplateTopping1
//									.getPizzaTopping().getPizzaToppingSizes();
////							if(pizzaTopping2!=null && pizzaTopping2.getId()!=null && pizzaTemplateTopping1!=null && pizzaTemplateTopping1.getPizzaTopping()!=null && pizzaTemplateTopping1.getPizzaTopping().getId()!=null
////									&& pizzaTemplateTopping1.getPizzaTopping().getId()==pizzaTopping2.getId())
////								flag=true;
//							for (PizzaToppingSize pizzaToppingSize1 : pizzaToppingSize) {
//								if (pizzaToppingSize1 != null) {
//									pizzaToppingSize1.setPizzaTopping(null);
//									PizzaSize pizzaSize = pizzaToppingSize1.getPizzaSize();
//									pizzaSize.setMerchant(null);
//									pizzaSize.setCategories(null);
//									pizzaSize.setPizzaCrustSizes(null);
//									pizzaSize.setPizzaTemplates(null);
//									pizzaSize.setPizzaToppingSizes(null);
//								}
//							}
//
//						}
//					for (PizzaTopping pizzaTopping2 : PizzaTopping) {
//						Boolean flag=false;
//					if(pizzaTemplateToppingList!=null && pizzaTemplateToppingList.size()>0)
//						 for(PizzaTemplateTopping pizzaTemplateTopping1 : pizzaTemplateToppingList){
//								if(pizzaTopping2!=null && pizzaTopping2.getId()!=null && pizzaTemplateTopping1!=null && pizzaTemplateTopping1.getPizzaTopping()!=null && pizzaTemplateTopping1.getPizzaTopping().getId()!=null
//										&& pizzaTemplateTopping1.getPizzaTopping().getId().equals(pizzaTopping2.getId()))
//									flag=true;
//								}
//					if(!flag)
//					{
//						PizzaTemplateTopping pizzaTemplateTopping=new PizzaTemplateTopping();
//						pizzaTemplateTopping.setId(0);
//						pizzaTemplateTopping.setPizzaTopping(pizzaTopping2);
//						pizzaTemplateTopping.setIncluded(false);
//						pizzaTemplateTopping.setReplacable(false);
//						pizzaTemplateToppingList.add(pizzaTemplateTopping);
//						
//					}
//					}
//				LOGGER.info("pizzasizecount"+pizzaTemplateSize.size());
//				}
//				else LOGGER.info("not mapped any size to");
//			}
		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
				LOGGER.error("error: " + e.getMessage());
			}
		}
		return pizzaTemplateToppingList;
	}

	@RequestMapping(value = "/getPizzaTepmlateToppings")
	@ResponseBody
	public List<PizzaTemplateTopping> getPizzaTepmlateToppings(@RequestParam(required = true) Integer templateId) {
		List<PizzaTemplateTopping> pizzaTemplateToppingList = null;
		try {
			pizzaTemplateToppingList = pizzaTemplateToppingRepository.findByPizzaTemplateId(templateId);
			for (PizzaTemplateTopping pizzaTemplateTopping1 : pizzaTemplateToppingList) {
				pizzaTemplateTopping1.setPizzaTemplate(null);
				pizzaTemplateTopping1.getPizzaTopping().setMerchant(null);
				pizzaTemplateTopping1.getPizzaTopping().setPizzaTemplateTopping(null);
				pizzaTemplateTopping1.getPizzaTopping().setPizzaTemplateToppings(null);
				List<PizzaToppingSize> pizzaToppingSize = pizzaTemplateTopping1.getPizzaTopping()
						.getPizzaToppingSizes();
				for (PizzaToppingSize pizzaToppingSize1 : pizzaToppingSize) {
					if (pizzaToppingSize1 != null) {
						pizzaToppingSize1.setPizzaTopping(null);
						PizzaSize pizzaSize = pizzaToppingSize1.getPizzaSize();
						pizzaSize.setMerchant(null);
						pizzaSize.setCategories(null);
						pizzaSize.setPizzaCrustSizes(null);
						pizzaSize.setPizzaTemplates(null);
						pizzaSize.setPizzaToppingSizes(null);
					}
				}

			}
		} catch (Exception e) {
			LOGGER.error("error: " + e.getMessage());
		}
		LOGGER.info("===============  AdminHomeController : Inside getPizzaTepmlateToppings :: end  ============= ");

		return pizzaTemplateToppingList;
	}

	@RequestMapping(value = "/saveNotificationAppStatus", method = RequestMethod.POST)
	public @ResponseBody String saveNotificationAppStatus(
			@RequestBody MerchantNotificationApp merchantNotificationApp) {
		try {
			LOGGER.info("AdminHomeController : Inside saveNotificationAppStatus start");
			if (merchantNotificationApp != null && merchantNotificationApp.getMerchant() != null
					&& merchantNotificationApp.getMerchant().getId() != null
					&& merchantNotificationApp.getAppId() != null) {
				LOGGER.info("AdminHomeController : Inside saveNotificationAppStatus merchantID :: "
						+ merchantNotificationApp.getMerchant().getId());
				notificationAppStatusService.findByMerchantIdandAppId(merchantNotificationApp.getMerchant().getId(),
						merchantNotificationApp.getAppId());
				LOGGER.info(
						"===============  AdminHomeController : Inside saveNotificationAppStatus :: End  ============= ");

				return "success";
			}

		} catch (Exception e) {
			LOGGER.info("AdminHomeController : Inside saveNotificationAppStatus Exception : " + e);
		}
		LOGGER.info("===============  AdminHomeController : Inside saveNotificationAppStatus :: End  ============= ");

		return "Failed";
	}

	@RequestMapping(value = "/createNotificationStatusForMerchant")
	public void createNotificationStatusForMerchant(HttpServletRequest request, HttpServletResponse response,
			@RequestParam("merchantId") Integer merchantId, @RequestParam("appId") Integer appId) {
		try {
			LOGGER.info(
					"===============  AdminHomeController : Inside createNotificationStatusForMerchant :: Start  ============= appId "
							+ appId + " merchantId " + merchantId);

			if (merchantId != null && appId != null) {
				Merchant merchant1 = merchantRepo.findById(merchantId);
				
				Date today = (merchant1 != null && merchant1.getTimeZone() != null
						&& merchant1.getTimeZone().getTimeZoneCode() != null)
								? DateUtil.getCurrentDateForTimeZonee(merchant1.getTimeZone().getTimeZoneCode())
								: new Date();
				NotificationApp notificationApp = notificationAppService.FindById(appId);
				if (notificationApp != null && notificationApp.getId() != null) {
					MerchantNotificationApp merchantNotificationApp = merchantNotificationAppService
							.FindBymerchantIdAndAppId(merchantId, appId);
					if (!(merchantNotificationApp != null)) {
						merchantNotificationApp = new MerchantNotificationApp();
						Merchant merchant = new Merchant();
						merchant.setId(merchantId);

						merchantNotificationApp.setMerchant(merchant);
						merchantNotificationApp.setAppId(notificationApp);
						merchantNotificationApp.setCreatedDate(today);
						merchantNotificationAppService.save(merchantNotificationApp);
					}
				}
			}
		} catch (Exception e) {
			LOGGER.info(
					"===============  AdminHomeController : Inside createNotificationStatusForMerchant :: Exception  ============= "
							+ e);

			LOGGER.error("error: " + e.getMessage());
		}
	}

	// =======================Get-Google-OAuth-Code==========================================================
	@RequestMapping(value = "/getAuthcode", method = RequestMethod.GET)
	public String getAuthCode(HttpServletRequest request, HttpServletResponse response) {
		String url = null;
		try {
			LOGGER.info("----------------Start :: AdminHomeController : getAuthCode------------------");
			String clientId = GoogleOAuthToken.CLIENT_ID;
			LOGGER.info("AdminHomeController : getAuthCode : clientId " + clientId);

			String authServerUrl = GoogleOAuthToken.AUTH_CODE_SERVER_URL;
			LOGGER.info("AdminHomeController : getAuthCode : authServerUrl " + authServerUrl);

			String redirectUri = GoogleOAuthToken.REDIRECT_URI;
			LOGGER.info("AdminHomeController : getAuthCode : redirectUri " + redirectUri);

			String scope = GoogleOAuthToken.SCOPE;
			LOGGER.info("AdminHomeController : getAuthCode : scope " + scope);

			url = new AuthorizationCodeRequestUrl(authServerUrl, clientId)
					.setResponseTypes(Collections.singleton("code")).setScopes(Collections.singleton(scope))
					.setRedirectUri(redirectUri).set("access_type", "offline").build();

			LOGGER.info("AdminHomeController : getAuthCode : AuthorizationCodeRequestUrl " + url);
		} catch (Exception exception) {
			if (exception != null) {
				exception.printStackTrace();
				MailSendUtil.sendExceptionByMail(exception,environment);
				LOGGER.error("AdminHomeController : getAuthCode : Exception " + exception);
			}
		}
		if (url != null) {
			LOGGER.info("----------------End :: AdminHomeController : getAuthCode------------------");
			return "redirect:" + url;
		} else {
			LOGGER.info("----------------End :: AdminHomeController : getAuthCode------------------");
			return "redirect: https://www.foodkonnekt.com";
		}
	}

	@RequestMapping(value = "/authCode", method = RequestMethod.GET)
	public String authCode(ModelMap model, HttpServletRequest request, @RequestParam(required = false) String code) {
		LOGGER.info("----------------Start :: AdminHomeController : authCode------------------");
		LOGGER.info("AdminHomeController : authCode : code " + code);

		Printer printer = null;
		String message = "";
		String result = "";
		try {
			result = GoogleOAuthToken.REDIRECT_URI + "?code=" + java.net.URLEncoder.encode(code, "UTF-8");
			LOGGER.info("AdminHomeController : authCode : result " + result);
		} catch (UnsupportedEncodingException e1) {
			if (e1 != null) {
				e1.printStackTrace();
				MailSendUtil.sendExceptionByMail(e1,environment);
				LOGGER.error("AdminHomeController : authCode : Exception " + e1);
			}
		}

		AuthorizationCodeResponseUrl authResponse = new AuthorizationCodeResponseUrl(result);
		// check for user-denied error
		if (authResponse.getError() != null) {
			// authorization denied...
			LOGGER.info("AdminHomeController : authCode : Error " + authResponse.getError());
			LOGGER.info("AdminHomeController : authCode : Error description " + authResponse.getErrorDescription());

			message = authResponse.getError();
			LOGGER.info("AdminHomeController : authCode : Error message " + authResponse.getError());
		} else {
			// request access token using authResponse.getCode()...
			code = authResponse.getCode();
			LOGGER.info("AdminHomeController : authCode : code " + code);
			try {
				String refresh_token = GoogleOAuthToken.requestRefreshToken(code);
				LOGGER.info("AdminHomeController : authCode : refresh_token " + refresh_token);
				Merchant merchant = null;
				HttpSession session = request.getSession();
				if (session != null) {
					merchant = (Merchant) session.getAttribute("merchant");
					LOGGER.info("===== AdminHomeController : Inside authCode :: merchant  == " + merchant.getId());

					if (merchant != null) {
						printer = printerRepository.findById(merchant.getId());
						if (printer.getId() != null) {
							LOGGER.info("AdminHomeController : authCode : printerId " + printer.getId());
							printer.setPrinterRefreshToken(refresh_token);
							printer.setMerchantId(merchant.getId());
							printerRepository.save(printer);
						} else {
							printer = new Printer();
							printer.setPrinterRefreshToken(refresh_token);
							printer.setMerchantId(merchant.getId());
							printerRepository.save(printer);
						}
					}
				}
			} catch (IOException exception) {
				if (exception != null) {
					exception.printStackTrace();
					message = "error";
					MailSendUtil.sendExceptionByMail(exception,environment);
					LOGGER.error("AdminHomeController : authCode : Exception " + exception);
				}
			}
		}
		LOGGER.info("----------------End :: AdminHomeController : authCode------------------");
		return "redirect:" + environment.getProperty("BASE_URL") + "/addPrinter?pid=" + printer.getId();
	}

	@RequestMapping(value = "/addPrinter", method = RequestMethod.GET)
	public String addPrinter(HttpServletRequest request, ModelMap model, @RequestParam Integer printerId) {
		try {
			LOGGER.info("----------------Start :: AdminHomeController : addPrinter------------------");
			LOGGER.info("AdminHomeController : addPrinter : printerId " + printerId);
			Printer printer = printerRepository.findById(printerId);
			if (printer != null) {
				String refreshToken = printer.getPrinterRefreshToken();
				if (!(refreshToken.isEmpty()) && refreshToken != null) {
					LOGGER.info("AdminHomeController : addPrinter : refreshToken " + refreshToken);
					Map<String, String> list = CloudPrintUtil.getPrintersList(refreshToken,environment);
					printer.setPrinters(list);
					model.addAttribute("printer", printer);
				}
			}
		} catch (Exception exception) {
			if (exception != null) {
				exception.printStackTrace();
				MailSendUtil.sendExceptionByMail(exception,environment);
				LOGGER.error("AdminHomeController : addPrinter : Exception " + exception);
			}
		}
		LOGGER.info("----------------End :: AdminHomeController : addPrinter------------------");
		return "addPrinter";
	}

	@RequestMapping(value = "/getPrinter", method = RequestMethod.POST)
	public String getPrinter(@ModelAttribute("printer") Printer printer, HttpServletRequest request, ModelMap model) {
		try {
			LOGGER.info("----------------Start :: AdminHomeController : getPrinter------------------");
			LOGGER.info("AdminHomeController : getPrinter : printerId " + printer.getId());
			Printer dbPrinter = printerRepository.findById(printer.getId());
			if (dbPrinter != null) {
				String refreshToken = dbPrinter.getPrinterRefreshToken();
				LOGGER.info("AdminHomeController : getPrinte : refreshToken " + refreshToken);
				if (!(refreshToken.isEmpty()) && refreshToken != null) {
					Map<String, String> list = CloudPrintUtil.getPrintersList(refreshToken,environment);
					for (String i : list.keySet()) {
						if (i.equals(printer.getPrinterId())) {
							LOGGER.info("AdminHomeController : getPrinter : printerId " + printer.getPrinterId());
							dbPrinter.setPrinterId(i);
							dbPrinter.setPrinterName(list.get(i));
							dbPrinter.setIsActive(1);
							printerRepository.save(dbPrinter);
						}
					}
					dbPrinter.setPrinters(list);
					model.addAttribute("printer", dbPrinter);
					model.addAttribute("msg", "printer saved successfully!");
				}
			} else {
				model.addAttribute("msg", "printer could not be saved due to some error.");
				LOGGER.info("AdminHomeController : getPrinter : no token found ");
			}
		} catch (Exception exception) {
			if (exception != null) {
				exception.printStackTrace();
				MailSendUtil.sendExceptionByMail(exception,environment);
				LOGGER.error("AdminHomeController : getPrinter : Exception " + exception);
			}
		}
		LOGGER.info("===============  AdminHomeController : Inside getPrinter :: End  ============= ");

		return "addPrinter";
	}

	@RequestMapping(value = "/addCloudPrinter", method = RequestMethod.GET)
	public String addCloudPrinter(HttpServletRequest request, ModelMap model) {
		LOGGER.info("----------------Start :: AdminHomeController : addCloudPrinter------------------");
		HttpSession session = request.getSession();
		Merchant merchant = (Merchant) session.getAttribute("merchant");
		try {
			LOGGER.info("===== AdminHomeController : Inside addCloudPrinter :: merchant  == " + merchant.getId());

			if (merchant != null && merchant.getId() != null) {
				Printer printer = printerRepository.findByMerchantId(merchant.getId());
				if (printer != null) {
					model.addAttribute("printer", printer);
				}
			}
		} catch (Exception exception) {
			if (exception != null) {
				exception.printStackTrace();
				MailSendUtil.sendExceptionByMail(exception,environment);
				LOGGER.error("AdminHomeController : addCloudPrinter : Exception " + exception);
			}
		}
		LOGGER.info("----------------End :: AdminHomeController : addCloudPrinter------------------");
		return "addCloudPrinter";
	}

	@RequestMapping(value = "/updatePrinterActiveValue", method = RequestMethod.GET)
	public String updatePrinterActiveValue(HttpServletRequest request, ModelMap model,
			@RequestParam Integer printerId) {
		LOGGER.info("----------------Start :: AdminHomeController : updatePrinterActiveValue------------------");
		try {
			LOGGER.info("AdminHomeController : updatePrinterActiveValue : printerId " + printerId);
			Printer printer = printerRepository.findById(printerId);
			if (printer != null) {
				if (printer.getIsActive() == 1) {
					printer.setIsActive(0);
					printerRepository.save(printer);
				} else {
					printer.setIsActive(1);
					printerRepository.save(printer);
				}
			}
		} catch (Exception exception) {
			if (exception != null) {
				exception.printStackTrace();
				MailSendUtil.sendExceptionByMail(exception,environment);
				LOGGER.error("AdminHomeController : updatePrinterActiveValue : Exception " + exception);
			}
		}
		LOGGER.info("----------------End :: AdminHomeController : updatePrinterActiveValue------------------");
		return "redirect:" + environment.getProperty("BASE_URL") + "/addCloudPrinter";
	}

	@RequestMapping(value = "/DownloadAppVersion", method = RequestMethod.GET)
	public String desktopdata(@ModelAttribute("desktopVerson") DesktopVesion desktopverson, Model model) {
		LOGGER.info("===============  AdminHomeController : Inside DownloadAppVersion : start:   ============= ");

		try {
			// List<BigInteger> uppdatedVersion =
			// posIntegrationRepository.findappVersion();
			List<DesktopVesion> desktopVesion = desktopversionRepository.findByDesnding();

			if (desktopVesion != null && !desktopVesion.isEmpty()) {
				for (DesktopVesion desktopVesion2 : desktopVesion) {
					if (desktopVesion2.getVersion() != null)
						desktopVesion2.setCountVerion(
								posIntegrationRepository.findappVersion(String.valueOf(desktopVesion2.getVersion())));
				}

			}
			model.addAttribute("desktopVerson", desktopVesion);
			model.addAttribute("url", environment.getProperty("BASE_URL"));

		} catch (Exception e) {
			LOGGER.info("===============  AdminHomeController : Inside DownloadAppVersion : Exception:   ============= "
					+ e);

		}
		LOGGER.info("===============  AdminHomeController : Inside DownloadAppVersion : end:   ============= ");

		return "desktopVerson";

	}

	// ---------------------------------------------------------------------------------------------------------------

	@RequestMapping(value = "/DownloadCurrentAndUpdateVersion", method = RequestMethod.GET)
	public String getCurrentAndupdatedversion(
			@ModelAttribute("updatedandcurrentverasion") UpdatedAndCurrentVersion appAndCurrVersion, Model model) {
		LOGGER.info("===============  AdminHomeController : Inside getCurrentandupdatedversion : start :=========");
		try {
			List<UpdatedAndCurrentVersion> updatedAndCurrentVersion = new ArrayList<UpdatedAndCurrentVersion>();
			Integer isActiveList = desktopversionRepository.findVersionByIsActive();
			List<PosIntegration> updatedAndName = posIntegrationRepository.findAll();
			for (int i = 0; updatedAndName.size() > i; i++) {
				updatedAndCurrentVersion.add(new UpdatedAndCurrentVersion());
				updatedAndCurrentVersion.get(i)
						.setMerchantName(merchantService.findById(updatedAndName.get(i).getMerchantId()).getName());
				updatedAndCurrentVersion.get(i).setAppVersion(isActiveList);
				updatedAndCurrentVersion.get(i).setCurrentVersion(updatedAndName.get(i).getAppVersion());
			}
			model.addAttribute("updatedandcurrentverasion", updatedAndCurrentVersion);
		} catch (Exception e) {
			LOGGER.info(
					"===============  AdminHomeController : Inside getCurrentandupdatedversion : Exception : =============="
							+ e);
		}
		LOGGER.info("===============  AdminHomeController : Inside getCurrentandupdatedversion : end : ==============");
		return "currentAndUpdatedVerson";
	}

	@RequestMapping(value = "/uploadDesktopExeLogFile", method = RequestMethod.POST)
	public void uploadDesktopExeLogFile(@RequestParam(required = true) Integer merchentId, HttpServletRequest request,
			HttpServletResponse response) {
		try {/*
			LOGGER.info(
					"===============  AdminHomeController : Inside uploadDesktopExeLogFile :: Start  =============merchentId "
							+ merchentId);

			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			Date date = new Date();
			String currentDate = dateFormat.format(date).toString();
			InputStream is = request.getInputStream();

			byte[] buffer = new byte[is.available()];
			is.read(buffer);
			int c;

			File targetFileDirectory = new File(environment.getProperty("FOODTRONIX_LOGGER_FILE_PATH") + merchentId);
			if (!targetFileDirectory.exists()) {
				targetFileDirectory.mkdir();
			}
			File targetFile = null;
			targetFile = new File(
					environment.getProperty("FOODTRONIX_LOGGER_FILE_PATH") + merchentId + "/logs_" + currentDate + ".txt");
			if (!targetFile.exists()) {
				targetFile.createNewFile();

			}
			OutputStream outStream = new FileOutputStream(targetFile, true);
			while ((c = is.read()) != -1) {
				outStream.write((char) c);
			}

		*/} catch (Exception e) {
			LOGGER.info(
					"===============  AdminHomeController : Inside uploadDesktopExeLogFile :: Exception  ============= "
							+ e);

			LOGGER.error("error: " + e.getMessage());
		}
	}

	@RequestMapping(value = "/getBusinessHoursByMerchantId", method = RequestMethod.GET)
	public @ResponseBody String getBusinessHoursByMerchantid(@RequestParam("merchantPosId") String posId) {
		LOGGER.info(
				"===============  AdminHomeController : Inside getBusinessHoursByMerchantId : start :=========posId "
						+ posId);
		String listOpeningClosingHours = null;
		try {
			List<OpeningClosingDay> openingClosingHours = null;
			if (posId != null) {
				Merchant merchant = merchantService.getMerchantByMerchantPsoId(posId);
				if (merchant != null && merchant.getId() != null) {

					openingClosingHours = businessService.findBusinessHourByMerchantId(merchant.getId());
					if (openingClosingHours != null && !openingClosingHours.isEmpty()) {
						for (OpeningClosingDay openingClosingDay : openingClosingHours) {
							openingClosingDay.setMerchant(null);
							openingClosingDay.setOpeningClosingTimes(null);
							for (OpeningClosingTime openingClosingTime : openingClosingDay.getTimes()) {
								openingClosingTime.setOpeningClosingDay(null);
							}
						}
					}
				}
			}
			Gson gson = new Gson();
			listOpeningClosingHours = gson.toJson(openingClosingHours);
			LOGGER.info(
					"===============  AdminHomeController : Inside getBusinessHoursByMerchantId : end:   ============= ");
		} catch (Exception e) {
			LOGGER.info(
					"===============  AdminHomeController : Inside getBusinessHoursByMerchantId : Exception:   ============= "
							+ e);
			MailSendUtil.sendExceptionByMail(e,environment);

		}
		return listOpeningClosingHours;
	}

	@RequestMapping(value = "/offlineCustomer", method = RequestMethod.GET)
	public String offlineCustomer() {
		return "offlineCustomer";
	}

	@RequestMapping(value = "/getPizzaTemplateToppinAccordingToSize", method = RequestMethod.GET)
	public @ResponseBody List<PizzaTemplateTopping> getPizzaTemplateToppinAccordingToSize(
			@RequestParam("templateId") Integer templateId) {
		LOGGER.info(
				"===============  AdminHomeController : Inside getPizzaTemplateToppinAccordingToSize : start :=========");
		List<PizzaTemplateSize> pizzaTemplateSize = null;
		List<PizzaTemplateTopping> pizzaTemplateToppingList = null;
		List<PizzaTopping> PizzaTopping = new ArrayList<PizzaTopping>();
		if (templateId != null) {
			LOGGER.info(
					"===============  AdminHomeController : Inside getPizzaTemplateToppinAccordingToSize : templateId :========="
							+ templateId);
			pizzaTemplateSize = pizzaService.findPizzaTemplateSizeByTemplateId(templateId);
			if (pizzaTemplateSize != null && pizzaTemplateSize.size() > 0) {
				LOGGER.info(
						"===============  AdminHomeController : Inside getPizzaTemplateToppinAccordingToSize : pizzaTemplateSize.size() :========="
								+ pizzaTemplateSize.size());
				for (PizzaTemplateSize pizzaTemplateSize2 : pizzaTemplateSize) {
					if (pizzaTemplateSize2 != null && pizzaTemplateSize2.getPizzaSize() != null
							&& pizzaTemplateSize2.getPizzaSize().getId() != null) {
						List<PizzaToppingSize> pizzaToppingSize1 = pizzaService
								.findByPizzaSizeId(pizzaTemplateSize2.getPizzaSize().getId());
						for (PizzaToppingSize pizzaToppingSize : pizzaToppingSize1) {
							if (pizzaToppingSize.getPizzaTopping().getActive() == 1)
								PizzaTopping.add(pizzaToppingSize.getPizzaTopping());
						}
					}
					break;
				}

				pizzaTemplateToppingList = pizzaTemplateToppingRepository
						.findByPizzaTemplateIdAndPizzaToppingActive(templateId, 1);
				if (pizzaTemplateToppingList != null && pizzaTemplateToppingList.size() > 0)
					for (PizzaTemplateTopping pizzaTemplateTopping1 : pizzaTemplateToppingList) {
						pizzaTemplateTopping1.setPizzaTemplate(null);
						pizzaTemplateTopping1.getPizzaTopping().setMerchant(null);
						pizzaTemplateTopping1.getPizzaTopping().setPizzaTemplateTopping(null);
						pizzaTemplateTopping1.getPizzaTopping().setPizzaTemplateToppings(null);
						List<PizzaToppingSize> pizzaToppingSize = pizzaTemplateTopping1.getPizzaTopping()
								.getPizzaToppingSizes();
//						if(pizzaTopping2!=null && pizzaTopping2.getId()!=null && pizzaTemplateTopping1!=null && pizzaTemplateTopping1.getPizzaTopping()!=null && pizzaTemplateTopping1.getPizzaTopping().getId()!=null
//								&& pizzaTemplateTopping1.getPizzaTopping().getId()==pizzaTopping2.getId())
//							flag=true;
						for (PizzaToppingSize pizzaToppingSize1 : pizzaToppingSize) {
							if (pizzaToppingSize1 != null) {
								pizzaToppingSize1.setPizzaTopping(null);
								PizzaSize pizzaSize = pizzaToppingSize1.getPizzaSize();
								pizzaSize.setMerchant(null);
								pizzaSize.setCategories(null);
								pizzaSize.setPizzaCrustSizes(null);
								pizzaSize.setPizzaTemplates(null);
								pizzaSize.setPizzaToppingSizes(null);
							}
						}

					}

				LOGGER.info(
						"===============  AdminHomeController : Inside getPizzaTemplateToppinAccordingToSize : pizzasizecount :========= "
								+ pizzaTemplateSize.size());
				LOGGER.info("pizzasizecount" + pizzaTemplateSize.size());
			} else {
				LOGGER.info(
						"===============  AdminHomeController : Inside getPizzaTemplateToppinAccordingToSize : not mapped any size to :=========");
				LOGGER.info("not mapped any size to");
			}
		}
		LOGGER.info(
				"===============  AdminHomeController : Inside getPizzaTemplateToppinAccordingToSize : End :=========");
		return pizzaTemplateToppingList;
	}

	@RequestMapping(value = "/chargeRefundByOrderIdOnAdmin", method = RequestMethod.GET)
	public String chargeRefundByOrderId(@RequestParam("orderId") Integer orderId) {
		LOGGER.info(
				"===============  AdminHomeController : Inside chargeRefundByOrderIdOnAdmin : Start:   ============= ");

		if (orderId != null) {
			LOGGER.info(
					"===============  AdminHomeController : Inside chargeRefundByOrderIdOnAdmin :End redicted to url:   =============: "
							+ environment.getProperty("WEB_BASE_URL") + "/chargeRefundByOrderId?orderId=" + orderId);
			return "redirect:" + environment.getProperty("WEB_BASE_URL") + "/chargeRefundByOrderId?orderId=" + orderId;
		} else
			LOGGER.info(
					"===============  AdminHomeController : Inside chargeRefundByOrderIdOnAdmin : End: failed  ============= ");
		return "failed";
	}

	@RequestMapping(value = "/mapNewPizzaSizeWithTemplate", method = RequestMethod.POST)
	public String addNewPizzaSize(@ModelAttribute() PizzaTemplate pizzaTemplate, ModelMap model) {
		LOGGER.info(
				"===============  AdminHomeController : Inside mapNewPizzaSizeWithTemplate : Start:   ============= ");

		try {
			if (pizzaTemplate != null) {
				int pizzatemplateId = pizzaTemplate.getId();
				LOGGER.info(
						"===============  AdminHomeController : Inside mapNewPizzaSizeWithTemplate : pizzatemplateId:   ============= "
								+ pizzatemplateId);

				PizzaSize savePizzaSize = new PizzaSize();
				PizzaTemplate savePizzaTemplate = new PizzaTemplate();

				while (pizzaTemplate.getSizePrice().remove(null)) {
				}

				if (pizzaTemplate.getSizePrice().size() > 0) {

					for (int i = 0; i < pizzaTemplate.getSizeId().size(); i++) {

						LOGGER.info("===============  : pizzaSizeId : Price : " + pizzaTemplate.getSizeId().get(i)
								+ " : " + pizzaTemplate.getSizePrice().get(i) + "=======================");

						PizzaTemplateSize savePizzaTemplateSize = new PizzaTemplateSize();
						int pizzaSizeId = pizzaTemplate.getSizeId().get(i);
						PizzaTemplateSize pizzaTemplateSize = pizzaTemplateSizeRepository
								.findByPizzaSizeIdAndPizzaTemplateId(pizzatemplateId, pizzaSizeId);
						if (pizzaTemplateSize == null) {
							savePizzaTemplateSize.setPrice(pizzaTemplate.getSizePrice().get(i));
							savePizzaTemplateSize.setAction(null);
							savePizzaTemplateSize.setActive(1);
							savePizzaSize.setId(pizzaSizeId);
							savePizzaTemplateSize.setPizzaSize(savePizzaSize);
							savePizzaTemplate.setId(pizzatemplateId);
							savePizzaTemplateSize.setPizzaTemplate(savePizzaTemplate);
							pizzaTemplateSizeRepository.save(savePizzaTemplateSize);
							LOGGER.info("===============   pizzaSizeId : " + pizzaTemplate.getSizeId().get(i)
									+ " : Mapped With TemplateId :" + pizzatemplateId + "====================");

						}
					}

				} else {
					LOGGER.info("===============  PizzaSizes Can not Mapped With TemplateId :" + pizzatemplateId
							+ "====================");
					LOGGER.info(
							"===============  AdminHomeController : Inside mapNewPizzaSizeWithTemplate : End:   ============= ");

					return "redirect:" + environment.getProperty("BASE_URL") + "/pizzaTamplate";

				}

			}
		} catch (Exception e) {
			if (e != null) {
				LOGGER.error("error: " + e.getMessage());

				LOGGER.info(
						"===============  AdminHomeController : Inside mapNewPizzaSizeWithTemplate : Exception:   ============= "
								+ e);

				MailSendUtil.sendExceptionByMail(e,environment);
				return "redirect:https://www.foodkonnekt.com";
			}
		}
		LOGGER.info(
				"===============  AdminHomeController : Inside mapNewPizzaSizeWithTemplate : End:   ============= ");

		return "redirect:" + environment.getProperty("BASE_URL") + "/pizzaTamplate";
	}

	@RequestMapping(value = "/mapNewPizzaSizeWithTopping", method = RequestMethod.POST)
	public String addNewSizesWithUpdatePizzaTopping(@ModelAttribute() PizzaTopping pizzaTopping, ModelMap model) {
		LOGGER.info(
				"===============  AdminHomeController : Inside mapNewPizzaSizeWithTopping : Start:   ============= ");

		try {

			if (pizzaTopping != null) {
				int pizzaToppingId = pizzaTopping.getId();
				LOGGER.info(
						"===============  AdminHomeController : Inside mapNewPizzaSizeWithTopping : PizzaToppingId : "
								+ pizzaToppingId + "===========");

				PizzaSize savePizzaSize = new PizzaSize();
				PizzaTopping savePizzaTopping = new PizzaTopping();

				while (pizzaTopping.getSizePrice().remove(null)) {
				}

				if (pizzaTopping.getSizePrice().size() > 0) {

					for (int i = 0; i < pizzaTopping.getSizeId().size(); i++) {

						LOGGER.info("===============  : pizzaSizeId : Price : " + pizzaTopping.getSizeId().get(i)
								+ " : " + pizzaTopping.getSizePrice().get(i) + "=======================");

						PizzaToppingSize savePizzaToppingSize = new PizzaToppingSize();
						int pizzaSizeId = pizzaTopping.getSizeId().get(i);
						PizzaToppingSize pizzaToppingSize = pizzaToppingSizeRepository
								.findByPizzaToppingIdAndPizzaSizeId(pizzaToppingId, pizzaSizeId);

						if (pizzaToppingSize == null) {
							savePizzaToppingSize.setPrice(pizzaTopping.getSizePrice().get(i));

							savePizzaToppingSize.setAction(null);
							savePizzaToppingSize.setActive(0);
							savePizzaSize.setId(pizzaSizeId);
							savePizzaToppingSize.setPizzaSize(savePizzaSize);
							savePizzaTopping.setId(pizzaToppingId);
							savePizzaToppingSize.setPizzaTopping(savePizzaTopping);

							pizzaToppingSizeRepository.save(savePizzaToppingSize);
							LOGGER.info("===============   pizzaSizeId: " + pizzaTopping.getSizeId().get(i)
									+ " : Mapped With ToppingId : " + pizzaToppingId + "====================");

						}
					}
				} else

					LOGGER.info(
							"=============== all Price are null :Pizza Size can Not Mapped With ToppingId : ===============================");

				LOGGER.info(
						"===============  AdminHomeController : Inside mapNewPizzaSizeWithTopping : End:   ============= ");

				return "redirect:" + environment.getProperty("BASE_URL") + "/pizzaTopping";

			}

		} catch (Exception e) {
			if (e != null) {
				LOGGER.error("error: " + e.getMessage());

				LOGGER.info(
						"===============  AdminHomeController : Inside mapNewPizzaSizeWithTopping : Exception:   ============= "
								+ e);

				MailSendUtil.sendExceptionByMail(e,environment);
				return "redirect:https://www.foodkonnekt.com";
			}
		}
		LOGGER.info("===============  AdminHomeController : Inside mapNewPizzaSizeWithTopping : End:   ============= ");

		return "redirect:" + environment.getProperty("BASE_URL") + "/pizzaTopping";

	}

	@RequestMapping(value = "/createPizzaSize")
	public String createPizzaSize(@ModelAttribute("pizzaSize") PizzaSize pizzaSize, HttpServletRequest request,
			Model model) {
		LOGGER.info("===============  AdminHomeController : Inside createPizzaSize : Start:  ============= ");
		HttpSession session = request.getSession(false);
		if (session != null) {
			Merchant merchant = (Merchant) session.getAttribute("merchant");
			if (merchant != null) {
				LOGGER.info(
						"===============  AdminHomeController : Inside createPizzaSize : merchantId:  ============= "
								+ merchant.getId());
				List<PizzaTemplate> pizzaTemplates = pizzaTemplateRepository.findByMerchantIdAndActive(merchant.getId(),
						1);
				if (pizzaTemplates != null && !pizzaTemplates.isEmpty()) {
					model.addAttribute("pizzaTemplates", pizzaTemplates);
				}
			} else
				return "redirect:" + environment.getProperty("BASE_URL") + "/support";
		} else
			return "redirect:" + environment.getProperty("BASE_URL") + "/support";
		model.addAttribute("errorMessage", "");
		LOGGER.info("===============  AdminHomeController : Inside createPizzaSize : End:  ============= ");
		return "createPizzaSize";
	}

	@RequestMapping(value = "/createPizzaSize", method = RequestMethod.POST)
	public String savePizzaSize(@ModelAttribute("pizzaSize") PizzaSize pizzaSize, HttpServletRequest request,
			Model model) {
		LOGGER.info("===============  AdminHomeController : Inside createPizzaSize : Start:   ============= ");
		try {
			String response = "";
			if (pizzaSize != null) {
				Merchant merchant = null;
				HttpSession session = request.getSession(false);
				if (session != null) {
					merchant = (Merchant) session.getAttribute("merchant");
					if (merchant != null) {
						LOGGER.info(
								"===============  AdminHomeController : Inside createPizzaSize : merchantId:   ============= "
										+ merchant.getId());
						List<PizzaTemplate> pizzaTemplates = pizzaTemplateRepository.findByMerchantId(merchant.getId());
						if (pizzaTemplates != null && !pizzaTemplates.isEmpty()) {
							model.addAttribute("pizzaTemplates", pizzaTemplates);
						}
					}
				}
				response = pizzaService.savePizzaSize(pizzaSize, merchant);
				model.addAttribute("errorMessage", response);
				LOGGER.info("===============  AdminHomeController : Inside createPizzaSize :  response ============= "
						+ response);

			}
		} catch (Exception e) {
			MailSendUtil.sendExceptionByMail(e,environment);
			LOGGER.error("error: " + e.getMessage());
			LOGGER.info(
					"===============  AdminHomeController : Inside createPizzaSize :  Exception ============= " + e);
		}
		LOGGER.info("===============  AdminHomeController : Inside createPizzaSize :  End ============= ");
		return "createPizzaSize";
	}

	@RequestMapping(value = "/createNewTax")
	public String createNewTax(@ModelAttribute("taxRates") TaxRates taxRates, HttpServletRequest request, Model model) {
		LOGGER.info("===============  AdminHomeController : Inside createNewTax : Start:  ============= ");
		HttpSession session = request.getSession(false);
		if (session != null) {
			Merchant merchant = (Merchant) session.getAttribute("merchant");
			if (merchant != null) {
				LOGGER.info("===============  AdminHomeController : Inside createNewTax : merchantId:  ============= "
						+ merchant.getId());
				List<PizzaTemplate> pizzaTemplates = pizzaTemplateRepository.findByMerchantId(merchant.getId());
				if (pizzaTemplates != null && !pizzaTemplates.isEmpty()) {
					model.addAttribute("pizzaTemplates", pizzaTemplates);
				}
			} else
				return "redirect:" + environment.getProperty("BASE_URL") + "/support";
		} else
			return "redirect:" + environment.getProperty("BASE_URL") + "/support";
		model.addAttribute("errorMessage", "");
		LOGGER.info("===============  AdminHomeController : Inside createNewTax : End:  ============= ");
		return "createNewTax";
	}

	@RequestMapping(value = "/saveNewTax", method = RequestMethod.POST)
	public String saveNewTax(@ModelAttribute("taxRates") TaxRates taxRates, HttpServletRequest request, Model model) {
		LOGGER.info("===============  AdminHomeController : Inside saveNewTax : Start:   ============= ");
		try {
			String response = "";
			if (taxRates != null) {
				Merchant merchant = null;
				HttpSession session = request.getSession(false);
				if (session != null) {
					merchant = (Merchant) session.getAttribute("merchant");
					if (merchant != null) {
						LOGGER.info(
								"===============  AdminHomeController : Inside saveNewTax : merchantId:   ============= "
										+ merchant.getId());
						List<PizzaTemplate> pizzaTemplates = pizzaTemplateRepository.findByMerchantId(merchant.getId());
						if (pizzaTemplates != null && !pizzaTemplates.isEmpty()) {
							model.addAttribute("pizzaTemplates", pizzaTemplates);
						}
					}
				}
				response = pizzaService.saveNewTax(taxRates, merchant);
				model.addAttribute("errorMessage", response);
				LOGGER.info("===============  AdminHomeController : Inside saveNewTax :  response ============= "
						+ response);

			}
		} catch (Exception e) {
			MailSendUtil.sendExceptionByMail(e,environment);
			LOGGER.error("error: " + e.getMessage());
			LOGGER.info("===============  AdminHomeController : Inside saveNewTax :  Exception ============= " + e);
		}
		LOGGER.info("===============  AdminHomeController : Inside saveNewTax :  End ============= ");
		return "createNewTax";
	}

	@RequestMapping(value = "/getMerchantWeeklyReport", method = RequestMethod.GET)
	public void getWeeklyReport(@RequestParam("startDate") String startDate, @RequestParam("endDate") String endDate,
			@RequestParam("merchantId") int merchantId) {

		try {
			LOGGER.info("Weekly Order Report schedular start");
			LOGGER.info("===============  getMerchantWeeklyReport  :  Start ============= ");
			List<Merchant> merchantDataList = new ArrayList<Merchant>();
			if (startDate == null && endDate == null) {
				DateFormat dateFormat = new SimpleDateFormat(IConstant.YYYYMMDD);
				Merchant merchant = merchantRepo.findById(merchantId);
				Date today = (merchant != null && merchant.getTimeZone() != null
						&& merchant.getTimeZone().getTimeZoneCode() != null)
						? DateUtil.getCurrentDateForTimeZonee(merchant.getTimeZone().getTimeZoneCode())
						: new Date();
				Calendar cal = new GregorianCalendar();
				cal.setTime(today);
				cal.add(Calendar.DAY_OF_MONTH, -7);
				Date today7 = cal.getTime();
				endDate = dateFormat.format(today);
				startDate = dateFormat.format(today7);
				LOGGER.info(" getMerchantWeeklyReport  :  startDate : " + startDate);
				LOGGER.info(" getMerchantWeeklyReport  :  endDate : " + endDate);
			}
			startDate = startDate.replace("/", "-");
			endDate = endDate.replace("/", "-");
			if (startDate != null && endDate != null) {
				LOGGER.info(" getMerchantWeeklyReport  :  endDate : " + endDate);
				Merchant merchantData = merchantService.getWeeklyReportOfMerchant(startDate, endDate, merchantId);
				if (merchantData != null) {
					merchantDataList.add(merchantData);
					MailSendUtil.sendWeeklyTotalOrders(merchantDataList, startDate, endDate,environment);
					LOGGER.info("Weekly Order Report schedular stop");
				}
			} else
				LOGGER.info(" getMerchantWeeklyReport  :  data missing  : ");

		} catch (Exception e) {
			LOGGER.info(
					"===============  AdminHomeController : Inside getMerchantWeeklyReport :: Exception  ============= "
							+ e);

			LOGGER.error("error: " + e.getMessage());
		}

	}

	@ResponseBody
	@RequestMapping(value = "/saveSubscriptionLevel", method = RequestMethod.GET)
	public String saveSubscriptionLevel(@RequestParam("type") String type, HttpServletRequest request) {
		try {
			LOGGER.info(
					"===============  AdminHomeController : Inside saveSubscriptionLevel : Start:   ============= ");
			Merchant merchant = null;
			Boolean flag = false;
			HttpSession session = request.getSession(false);
			if (session != null) {
				merchant = (Merchant) session.getAttribute("merchant");
				Date today = (merchant != null && merchant.getTimeZone() != null
						&& merchant.getTimeZone().getTimeZoneCode() != null)
						? DateUtil.getCurrentDateForTimeZonee(merchant.getTimeZone().getTimeZoneCode())
						: new Date();
				if (merchant != null && type != null) {
					LOGGER.info("AdminHomeController :  saveSubscriptionLevel :merchant.getId() : " + merchant.getId());
					LOGGER.info("AdminHomeController :  saveSubscriptionLevel :type : " + type);
					List<MerchantSubscriptionLevel> merchantSubscriptionLevels = merchantSubscriptionLevelRepository
							.findByMerchantId(merchant.getId());
					if (merchantSubscriptionLevels != null && !merchantSubscriptionLevels.isEmpty()) {
						for (MerchantSubscriptionLevel merchantSubscriptionLevel2 : merchantSubscriptionLevels) {
							if (merchantSubscriptionLevel2 != null) {
								LOGGER.info("AdminHomeController :  saveSubscriptionLevel :inActive : "
										+ merchantSubscriptionLevel2.getSubscriptionLevel());
								merchantSubscriptionLevel2.setActive(0);
								merchantSubscriptionLevel2.setUpdatedDate(today);
								merchantSubscriptionLevelRepository.save(merchantSubscriptionLevel2);
							}
						}
					}
					MerchantSubscriptionLevel merchantSubscriptionLevel = new MerchantSubscriptionLevel();
					merchantSubscriptionLevel.setActive(1);
					merchantSubscriptionLevel.setCreatedDate(today);
					merchantSubscriptionLevel.setUpdatedDate(today);
					merchantSubscriptionLevel.setMerchant(merchant);
					merchantSubscriptionLevel.setSubscriptionLevel(type);
					merchantSubscriptionLevelRepository.save(merchantSubscriptionLevel);
					LOGGER.info("AdminHomeController :  saveSubscriptionLevel :new subscription level : "
							+ merchantSubscriptionLevel.getSubscriptionLevel());
				}
			}
		} catch (Exception e) {
			LOGGER.error("error: " + e.getMessage());
			LOGGER.info("AdminHomeController :  saveSubscriptionLevel :exception : " + e);
		}
		return "success";
	}

	@RequestMapping(value = "/foodkonnektAppStatus", method = RequestMethod.GET)
	public @ResponseBody String foodkonnektAppStatus(HttpServletRequest request,
			@RequestParam(value = "merchantId") Integer merchantId) {
		LOGGER.info("===============  AdminHomeController : Inside foodkonnektAppStatus : Start:  ============= ");
		LOGGER.info(" AdminHomeController : Inside foodkonnektAppStatus : merchantId :" + merchantId);
		String response = "false";
		try {
			if (merchantId != null) {
				NotificationAppStatus notificationAppStatus = notificationAppStatusRepository
						.lastOccuranceOfMerchantId(merchantId);
				if (notificationAppStatus != null) {
					LOGGER.info(" AdminHomeController : Inside foodkonnektAppStatus : IsRunning :"
							+ notificationAppStatus.getIsRunning() + ": IsNotified :"
							+ notificationAppStatus.getIsNotified());

					if (notificationAppStatus.getIsRunning() != null && notificationAppStatus.getIsRunning() == 1
							&& notificationAppStatus.getIsNotified() != null
							&& notificationAppStatus.getIsNotified() == 1 && buisnessService.getOpenhoursStatus(notificationAppStatus.getMerchantid())) {
						LOGGER.info(
								" AdminHomeController : Inside foodkonnektAppStatus : return response :" + response);
						response = "true";
					}
				}
			}
		} catch (Exception e) {
			LOGGER.error("error: " + e.getMessage());
			LOGGER.error(" AdminHomeController : Inside foodkonnektAppStatus : Exeption : " + e);
		}
		LOGGER.info(" AdminHomeController : Inside foodkonnektAppStatus : return response :" + response);

		return response;

	}

	@RequestMapping(value = "/foodkonnektDesktopAppVersion", method = RequestMethod.GET)
	public @ResponseBody Integer foodkonnektDesktopAppVersion() {
		LOGGER.info(
				"===============  AdminHomeController : Inside foodkonnektDesktopAppVersion : Start:  ============= ");

		Integer response = 0;
		try {
			response = desktopversionRepository.findVersionByIsActive();
		} catch (Exception e) {
			LOGGER.error("error: " + e.getMessage());
			LOGGER.error(" AdminHomeController : Inside foodkonnektAppStatus : Exeption : " + e);
		}
		LOGGER.info(" AdminHomeController : Inside foodkonnektAppStatus : return response :" + response);
		return response;
	}

	@RequestMapping(value = "/genrateCustoerUid", method = RequestMethod.GET)
	public @ResponseBody String genrateCustoerUid(@RequestParam String merchantUid) {
		LOGGER.info("===============  AdminHomeController : Inside genrateCustoerUid : Start:  ============= ");

		try {
			if (merchantUid != null && !merchantUid.isEmpty()) {
				LOGGER.info("===============  AdminHomeController : Inside merchantUid : :  " + merchantUid);
				Merchant merchant = merchantService.findByMerchantUid(merchantUid);
				if (merchant != null && merchant.getId() != null) {
					LOGGER.info("===============  AdminHomeController : merchant.getId() : :  " + merchant.getId());
					List<Customer> customer = customerService.findByMerchantUId(merchantUid);
					if (customer != null && customer.size() > 0)
						for (Customer customer2 : customer) {

							if (customer2 != null
									&& (customer2.getCustomerUid() == null || customer2.getCustomerUid().isEmpty())) {
								LOGGER.info(
										"===============  AdminHomeController : customer2 : :  " + customer2.getId());
								customer2.getMerchantt().setItems(null);
								;
								customer2.getMerchantt().setAddresses(null);
								customer2.getMerchantt().setAddresses(null);
								customer2.getMerchantt().setItems(null);
								customer2.getMerchantt().setMerchantSubscriptions(null);
								customer2.getMerchantt().setModifierGroups(null);
								customer2.getMerchantt().setModifiers(null);
								customer2.getMerchantt().setOpeningClosingDays(null);
								customer2.getMerchantt().setOrderRs(null);
								customer2.getMerchantt().setOrderTypes(null);
								customer2.getMerchantt().setPaymentModes(null);
								customer2.getMerchantt().setVouchars(null);
								customer2.getMerchantt().setTaxRates(null);
								customer2.getMerchantt().setAddresses(null);
								String customerUid = ProducerUtil.createCustomerIntoCentralDb(customer2,environment);
								if (customerUid != null) {
									customer2.setCustomerUid(customerUid);
									customerService.save(customer2);
								}
							}
						}
				}
				LOGGER.info("===============  AdminHomeController : merchant not exist : :  ");

			} else
				LOGGER.info("===============  AdminHomeController : Inside merchantUid : : null ");
		} catch (Exception e) {
			LOGGER.error("error: " + e.getMessage());
			LOGGER.error(" AdminHomeController : Inside foodkonnektAppStatus : Exeption : " + e);
		}
		return "done";
	}

	@RequestMapping(value = "/foodkonnektAppStatusByPosId", method = RequestMethod.GET)
	public @ResponseBody String foodkonnektAppStatus(HttpServletRequest request,
			@RequestParam(value = "merchantPosId") String merchantPosId) {
		LOGGER.info("===============  AdminHomeController : Inside foodkonnektAppStatus : Start:  ============= ");
		LOGGER.info(" AdminHomeController : Inside foodkonnektAppStatus : merchantPosId :" + merchantPosId);
		String response = "false";
		try {
			if (merchantPosId != null) {
				Merchant merchant = merchantService.findByPosMerchantId(merchantPosId);
				if (merchant != null && merchant.getId() != null) {
					NotificationAppStatus notificationAppStatus = notificationAppStatusRepository
							.lastOccuranceOfMerchantId(merchant.getId());
					if (notificationAppStatus != null) {
						LOGGER.info(" AdminHomeController : Inside foodkonnektAppStatus : IsRunning :"
								+ notificationAppStatus.getIsRunning() + ": IsNotified :"
								+ notificationAppStatus.getIsNotified());

						if (notificationAppStatus.getIsRunning() != null && notificationAppStatus.getIsRunning() == 1
								&& notificationAppStatus.getIsNotified() != null
								&& notificationAppStatus.getIsNotified() == 1 && buisnessService.getOpenhoursStatus(notificationAppStatus.getMerchantid())) {
							LOGGER.info(" AdminHomeController : Inside foodkonnektAppStatus : return response :"
									+ response);
							response = "true";
						}
					}
				}
			}
		} catch (Exception e) {
			LOGGER.error("error: " + e.getMessage());
			LOGGER.error(" AdminHomeController : Inside foodkonnektAppStatus : Exeption : " + e);
		}
		LOGGER.info(" AdminHomeController : Inside foodkonnektAppStatus : return response :" + response);

		return response;

	}

	@RequestMapping(value = "/saveOrderNotificationMethod", method = RequestMethod.POST)
	public String save(@ModelAttribute("orderNotification") OrderNotification orderNotification, ModelMap model,
			HttpServletResponse response, HttpServletRequest request) {
		LOGGER.info("AdminHomeController  : Inside saveNotificationMethod :: Start");
		String message = "";
		try {

			HttpSession session = request.getSession(false);
			if (session != null) {
				Merchant merchant = (Merchant) session.getAttribute("merchant");
				if (merchant != null && orderNotification != null) {
					LOGGER.info(
							"AdminHomeController : Inside saveNotificationMethod : MerchantId :: " + merchant.getId());

					Date today = (merchant.getTimeZone() != null
							&& merchant.getTimeZone().getTimeZoneCode() != null)
							? DateUtil.getCurrentDateForTimeZonee(merchant.getTimeZone().getTimeZoneCode())
							: new Date();
							
					OrderNotification orderNotification2 = orderNotificationService.findMerchantId(merchant.getId());
					if (orderNotification2 == null) {

						orderNotification.setMerchant(merchant);
						orderNotification.setCreatedOn(today);
						orderNotification.setUpdatedOn(today);
						model.addAttribute("orderNotification", orderNotificationService.save(orderNotification));
						message = "Data is inserted";
					} else {
						orderNotification.setMerchant(merchant);
						orderNotification.setId(orderNotification2.getId());
						orderNotification.setCreatedOn(orderNotification2.getCreatedOn());
						orderNotification.setUpdatedOn(today);
						model.addAttribute("orderNotification", orderNotificationService.save(orderNotification));
						message = "Data is updated";
					}
				}
			}
			LOGGER.info("done");

		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
				LOGGER.error("error: " + e.getMessage());
				LOGGER.info("NotificationMethodController  : Inside saveNotificationMethod :: Exception " + e);
				return "redirect:https://www.foodkonnekt.com";
			}
		}
		LOGGER.info("NotificationMethodController  : Inside saveNotificationMethod :: End");
		return "redirect:" + environment.getProperty("BASE_URL") + "/orderNotification?message=" + message;
	}

	@RequestMapping(value = "/orderNotification", method = RequestMethod.GET)
	public String notificationMethodLink(ModelMap model, HttpServletRequest request) {
		LOGGER.info("NotificationMethodController : Inside notificationMethodLink :: Start");
		HttpSession session = request.getSession(false);
		try {
			if (session != null) {
				Merchant merchant = (Merchant) session.getAttribute("merchant");
				if (merchant != null) {
					OrderNotification orderNotification = orderNotificationService.findMerchantId(merchant.getId());
					if (orderNotification != null)
						model.addAttribute("orderNotification", orderNotification);
					else
						model.addAttribute("orderNotification", new OrderNotification());
					return "orderNotification";
				} else
					return "redirect:" + environment.getProperty("BASE_URL") + "/support";
			} else
				return "redirect:" + environment.getProperty("BASE_URL") + "/support";
		} catch (Exception e) {
			MailSendUtil.sendExceptionByMail(e,environment);
			return "redirect:" + environment.getProperty("BASE_URL") + "/support";
		}
	}

	@RequestMapping(value = "/updatePizzaCategory", method = RequestMethod.POST)
	public String updatePizzaCategory(@ModelAttribute("pizzaCategory") PizzaTemplateCategory categoryPizza,
			HttpServletRequest request, Model model) {

		HttpSession session = request.getSession(false);
		Merchant merchant = (Merchant) session.getAttribute("merchant");

		if (categoryPizza.getcId() != null && !categoryPizza.getcId().isEmpty()) {
			Category category = null;
			PizzaTemplate pizza = null;
			PizzaTemplateCategory categoryPizzas = null;
			for (Integer categoryId : categoryPizza.getcId()) {
				category = new Category();
				category.setId(categoryId);

				if (categoryPizza.getpId() != null && !categoryPizza.getpId().isEmpty()) {
					for (Integer templateId : categoryPizza.getpId()) {
						categoryPizzas = new PizzaTemplateCategory();
						pizza = new PizzaTemplate();

						PizzaTemplateCategory dbCategoryPizza = pizzaTemplateCategoryRepository
								.findByPizzaTemplateIdAndCategoryId(templateId, categoryId);

						if (dbCategoryPizza != null) {
							LOGGER.info("PizzaTemplateCategory already have mapping!!");
						} else {
							pizza.setId(templateId);
							categoryPizzas.setPizzaTemplate(pizza);
							categoryPizzas.setCategory(category);
							pizzaTemplateCategoryRepository.save(categoryPizzas);
						}
					}
				}
			}
		}

		if (merchant != null) {
			List<PizzaTemplate> pizzas = pizzaTemplateRepository.findByMerchantId(merchant.getId());
			if (pizzas != null && !pizzas.isEmpty()) {
				model.addAttribute("pizza", pizzas);
			}
			List<Category> categories = categoryRepository.findByMerchantIdAndIsPizza(merchant.getId());
			if (categories != null && !categories.isEmpty()) {
				model.addAttribute("pizzacategories", categories);
			}
		}

		return "pizzaCategory";
	}

	@RequestMapping(value = "/editPizzaCrust", method = RequestMethod.GET)
	public String viewEditPizzaCrust(@ModelAttribute("pizzaCrust") PizzaCrust pizzaCrust, ModelMap model,
			HttpServletRequest request, @RequestParam(required = false) int crstId) {
		try {
			HttpSession session = request.getSession(false);
			if (session != null) {
				Merchant merchant = (Merchant) session.getAttribute("merchant");
				if (merchant != null && crstId > 0) {

					pizzaCrust = pizzaService.findByCrustId(crstId);
					model.addAttribute("description", pizzaCrust.getDescription());
					model.addAttribute("status", pizzaCrust.getStatus());
					model.addAttribute("pizzaCrust", pizzaCrust);

					List<PizzaCrustSizes> pizzaCrustSizeList = pizzaCrustSizeRepository.findByPizzaCrustId(crstId);
					model.addAttribute("pizzaCrustSizeList", pizzaCrustSizeList);
					List<PizzaSize> dropdownPizzaSize = null;
					List<Integer> pizzasizeid = new ArrayList<Integer>();
					for (int i = 0; i < pizzaCrustSizeList.size(); i++) {
						pizzasizeid.add(pizzaCrustSizeList.get(i).getPizzaSize().getId());
					}
					if (pizzasizeid.size() > 0) {
						dropdownPizzaSize = pizzaSizeRepository.findByMerchantIdAndId(merchant.getId(), pizzasizeid);
					} else
						dropdownPizzaSize = pizzaSizeRepository.findByMerchantId(merchant.getId());

					model.addAttribute("dropdownPizzaSize", dropdownPizzaSize);

				}
			}
		} catch (Exception e) {

			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
				LOGGER.error("error: " + e.getMessage());
				return "redirect:" + environment.getProperty("BASE_URL") + "/support";
			}
		}

		return "editPizzaCrust";

	}

	@RequestMapping(value = "/mapNewPizzaSizeWithCrust", method = RequestMethod.POST)
	public String addNewSizesWithUpdatePizzaCrust(@ModelAttribute("pizzaCrust") PizzaCrust pizzaCrust, ModelMap model) {
		LOGGER.info("===============  AdminHomeController : Inside mapNewPizzaSizeWithCrust : Start:   ============= ");

		try {

			if (pizzaCrust != null) {
				int pizzaCrustId = pizzaCrust.getId();
				LOGGER.info("===============  AdminHomeController : Inside mapNewPizzaSizeWithCrust : PizzaCrustId : "
						+ pizzaCrustId + "===========");

				PizzaSize savePizzaSize = new PizzaSize();
				PizzaCrust savePizzaCrust = new PizzaCrust();

				while (pizzaCrust.getSizePrice().remove(null)) {
				}

				if (pizzaCrust.getSizePrice().size() > 0) {

					for (int i = 0; i < pizzaCrust.getSizeId().size(); i++) {

						LOGGER.info("===============  : pizzaSizeId : Price : " + pizzaCrust.getSizeId().get(i) + " : "
								+ pizzaCrust.getSizePrice().get(i) + "=======================");

						PizzaCrustSizes savePizzaCrustSize = new PizzaCrustSizes();
						int pizzaSizeId = pizzaCrust.getSizeId().get(i);
						PizzaCrustSizes pizzaCrustSize = pizzaCrustSizeRepository
								.findByPizzaCrustIdAndPizzaSizeId(pizzaCrustId, pizzaSizeId);

						if (pizzaCrustSize == null) {
							savePizzaCrustSize.setPrice(pizzaCrust.getSizePrice().get(i));

							// savePizzaCrustSize.setAction(null);
							savePizzaCrustSize.setActive(1);
							savePizzaSize.setId(pizzaSizeId);
							savePizzaCrustSize.setPizzaSize(savePizzaSize);
							savePizzaCrust.setId(pizzaCrustId);
							savePizzaCrustSize.setPizzaCrust(savePizzaCrust);

							pizzaCrustSizeRepository.save(savePizzaCrustSize);
							LOGGER.info("===============   pizzaSizeId: " + pizzaCrust.getSizeId().get(i)
									+ " : Mapped With CrustId : " + pizzaCrustId + "====================");

						}
					}
				} else

					LOGGER.info(
							"=============== all Price are null :Pizza Size can Not Mapped With CrustId : ===============================");

				LOGGER.info(
						"===============  AdminHomeController : Inside mapNewPizzaSizeWithCrust : End:   ============= ");

				return "redirect:" + environment.getProperty("BASE_URL") + "/pizzaCrust";

			}

		} catch (Exception e) {
			if (e != null) {
				LOGGER.error("error: " + e.getMessage());

				LOGGER.info(
						"===============  AdminHomeController : Inside mapNewPizzaSizeWithCrust : Exception:   ============= "
								+ e);

				MailSendUtil.sendExceptionByMail(e,environment);
				return "redirect:" + environment.getProperty("BASE_URL") + "/support";
			}
		}
		LOGGER.info("===============  AdminHomeController : Inside mapNewPizzaSizeWithCrust : End:   ============= ");

		return "redirect:" + environment.getProperty("BASE_URL") + "/pizzaCrust";

	}

	@RequestMapping(value = "/updatePizzaCrust", method = RequestMethod.POST)
	public String updatePizzaCrust(@ModelAttribute("pizzaCrust") PizzaCrust pizzaCrust, ModelMap model,
			HttpSession session) {
		try {
			PizzaCrust result = new PizzaCrust();
            if(session != null) {
			Merchant merchant = (Merchant) session.getAttribute("merchant");
            if(merchant != null) {
			result = pizzaService.updatePizzaCrustById(pizzaCrust, merchant.getId());

			if (result != null) {
				pizzaCrust.setDescription(result.getDescription());

				model.addAttribute("description", result.getDescription());
				model.addAttribute("status", result.getStatus());
				model.addAttribute("pizzaCrust", result);
				model.addAttribute("pizzaCrustSizes", result.getPizzaCrustSizes());

			}
            }else
            	return "redirect:" + environment.getProperty("BASE_URL") + "/support";	
            }else
            	return "redirect:" + environment.getProperty("BASE_URL") + "/support";	
		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
				LOGGER.error("error: " + e.getMessage());
				return "redirect:" + environment.getProperty("BASE_URL") + "/support";
			}
		}
		return "pizzaCrust";
	}

	@RequestMapping(value = "/createPizzaCrust", method = RequestMethod.GET)
	public String createPizzaCrust(ModelMap modelMap, @ModelAttribute("PizzaCrust") PizzaCrust pizzaCrust,
			HttpServletRequest request) {

		HttpSession session = request.getSession();
		Merchant merchant = (Merchant) session.getAttribute("merchant");

		if (merchant != null && merchant.getId() != null) {

			List<PizzaSize> pizzaSize = pizzaSizeRepository.findByMerchantIdAndActive(merchant.getId(), 1);
			if (pizzaSize != null && !pizzaSize.isEmpty()) {
				modelMap.addAttribute("pizzaSize", pizzaSize);
			}

			List<PizzaTemplate> pizzaTemplates = pizzaTemplateRepository.findByMerchantIdAndActive(merchant.getId(), 1);
			if (pizzaTemplates != null && !pizzaTemplates.isEmpty()) {
				modelMap.addAttribute("pizzaTemplates", pizzaTemplates);
			}
		}
		return "createPizzaCrust";
	}

	@RequestMapping(value = "/pizzaCategory", method = RequestMethod.GET)
	public String pizzaCategory(@ModelAttribute("pizzaCategory") PizzaTemplateCategory pizzaCategory,
			HttpServletRequest request, ModelMap model) {
		HttpSession session = request.getSession(false);
		Merchant merchant = (Merchant) session.getAttribute("merchant");
		Category category = new Category();
		if (merchant != null) {
			List<PizzaTemplate> pizza = pizzaTemplateRepository.findByMerchantIdAndActive(merchant.getId(), 1);
			if (pizza != null && !pizza.isEmpty()) {
				model.addAttribute("pizza", pizza);
			}

			List<Category> pizzacategories = categoryRepository
					.findByMerchantIdAndIsPizzaAndItemStatus(merchant.getId(), 1, 0);
			if (pizzacategories != null && !pizzacategories.isEmpty()) {
				model.addAttribute("pizzacategories", pizzacategories);
			}
		} else {
			return "redirect:" + environment.getProperty("BASE_URL") + "/support";
		}
		return "pizzaCategory";
	}

	@RequestMapping(value = "/showModifiersByAjax", method = RequestMethod.GET)
	public @ResponseBody List<Modifiers> showmodifiersByAjax(@RequestParam("modifierGroupId") Integer modifierGroupId,
			HttpServletRequest request) {
		LOGGER.info("===============  AdminHomeController : Inside showModifiersByAjax :  Start ============= ");
		LOGGER.info(": AdminHomeController : Inside showModifiersByAjax :  modifierGroupId :" + modifierGroupId);
		List<Modifiers> modifiersResponce = new ArrayList<Modifiers>();
		Gson gson = new Gson();
		HttpSession session = request.getSession();

		List<Modifiers> modifiers = modifierRepository.findAllBymodifiergroupid(modifierGroupId);
		LOGGER.info(": AdminHomeController : Inside showModifiersByAjax :  modifierListSize :" + modifiers.size());
		if (modifiers != null && !modifiers.isEmpty() && modifiers.size() > 0) {
			for (Modifiers modifiers2 : modifiers) {
				modifiers2.setModifierGroup(null);
				modifiers2.setMerchant(null);
				modifiersResponce.add(modifiers2);
			}
			LOGGER.info("===============  AdminHomeController : Inside showModifiersByAjax :  End ============= ");
			return modifiersResponce;
		}
		LOGGER.info("===============  AdminHomeController : Inside showModifiersByAjax :  End ============= ");
		return modifiersResponce;
	}

	@RequestMapping(value = "/DeletePizzaTemplateTopping", method = RequestMethod.GET)
	@ResponseBody
	public String deletePizzaTemplateTopping(@RequestParam("pizzatemplateId") Integer pizzatemplateId,
			@RequestParam("pizzaToppingId") Integer pizzaToppingId) {
		PizzaTemplateTopping pizzaTemplateTopping = pizzaTemplateToppingRepository
				.findByPizzaTemplateIdAndPizzaToppingId(pizzatemplateId, pizzaToppingId);
		if (pizzaTemplateTopping != null) {
			pizzaTemplateToppingRepository.delete(pizzaTemplateTopping.getId());
		}
		return "redirect:" + environment.getProperty("BASE_URL") + "/editPizzaTemplate?templateId=" + pizzatemplateId;
	}

	@RequestMapping(value = "/templateCrustMap")
	public String templateCrustMap(@ModelAttribute("PizzaCrust") PizzaCrust pizzaCrust, HttpServletRequest request,
			Model model) {
		Merchant merchant = null;
		HttpSession session = request.getSession(false);
		if (session != null) {
			merchant = (Merchant) session.getAttribute("merchant");
			if (merchant != null) {

//				List<PizzaTemplate> pizzaTemplates = pizzaTemplateRepository.findByMerchantId(merchant.getId());
//				if (pizzaTemplates != null && pizzaTemplates.size() > 0) {
//					model.addAttribute("pizzaTemplates", pizzaTemplates);
//				}

				List<PizzaCrust> pizzacrusts = pizzaCrustRepository.findByMerchantIdAndActive(merchant.getId(), 1);
				if (pizzacrusts != null && pizzacrusts.size() > 0) {
					model.addAttribute("pizzacrusts", pizzacrusts);
				}
			}
		}
		return "templateCrustMap";
	}

	@RequestMapping(value = "/saveTemplateCrustMap", method = RequestMethod.POST)
	public String saveTemplateCrustMap(@ModelAttribute("pizzaTemplateCrust") PizzaTemplateCrust pizzaTemplateCrust,
			HttpServletRequest request, Model model) {

		List<Integer> templateId = pizzaTemplateCrust.getTemplateId();
		List<Integer> crustId = pizzaTemplateCrust.getCrustId();

		PizzaTemplate pizzaTemplate = null;
		PizzaCrust pizzaCrusts = null;
		PizzaTemplateCrust templateCrust = null;

		if (crustId != null && crustId.size() > 0) {
			for (Integer crstId : crustId) {
				if (templateId != null && templateId.size() > 0) {
					pizzaCrusts = new PizzaCrust();

					for (Integer tempId : templateId) {
						PizzaTemplateCrust dbpizzaTemplateCrust = pizzaTemplateCrustRepository
								.findByPizzaTemplateIdAndPizzaCrustId(tempId, crstId);
						pizzaTemplate = new PizzaTemplate();
						templateCrust = new PizzaTemplateCrust();

						if (dbpizzaTemplateCrust != null) {
							LOGGER.info("mapping already exists");
						} else {
							pizzaTemplate.setId(tempId);
							pizzaCrusts.setId(crstId);
							templateCrust.setPizzaTemplate(pizzaTemplate);
							templateCrust.setPizzaCrust(pizzaCrusts);

							pizzaTemplateCrustRepository.save(templateCrust);
						}
					}

				}
			}
		}
		return "redirect:" + environment.getProperty("BASE_URL") + "/pizzaCrust";
	}

	@RequestMapping(value = "/itemTaxMap")
	public String itemTaxMap(@ModelAttribute("itemTax") ItemTax itemTax, HttpServletRequest request, Model model) {
		Merchant merchant = null;
		HttpSession session = request.getSession(false);
		if (session != null) {
			merchant = (Merchant) session.getAttribute("merchant");
			if (merchant != null) {

				List<Item> items = itemRepository.findByMerchantId(merchant.getId());
				if (items != null && items.size() > 0) {
					model.addAttribute("items", items);
				}

				List<TaxRates> taxRates = taxRateRepository.findByMerchantId(merchant.getId());
				if (taxRates != null && taxRates.size() > 0) {
					model.addAttribute("taxRates", taxRates);
				}
			}
		}
		return "itemTaxMap";
	}

	@RequestMapping(value = "/saveItemTaxMap", method = RequestMethod.POST)
	public String saveTemplateTaxMap(@ModelAttribute("itemTax") ItemTax itemtax, HttpServletRequest request,
			Model model) {

		List<Integer> itemId = itemtax.getItemsId();
		List<Integer> taxId = itemtax.getTexesId();

		Item item = null;
		TaxRates taxRates = null;
		ItemTax itemTax1 = null;

		List<ItemTax> itemTaxes = new ArrayList<ItemTax>();

		if (taxId != null && taxId.size() > 0) {
			for (Integer rateId : taxId) {
				if (itemId != null && itemId.size() > 0) {
					taxRates = new TaxRates();

					for (Integer itemId1 : itemId) {
						itemTax1 = itemTaxRepository.findItemByItemIdAndTaxRatesId(itemId1, rateId);
						if (itemTax1 != null) {
							itemTax1.setActive(1);
							LOGGER.info("Item Tax mapping already exists");
						} else {
							item = new Item();
							itemTax1 = new ItemTax();
							item.setId(itemId1);
							taxRates.setId(rateId);
							itemTax1.setItem(item);
							itemTax1.setTaxRates(taxRates);
							itemTax1.setActive(1);

						}
						itemTaxes.add(itemTax1);
					}
					itemTaxRepository.save(itemTaxes);
				}
			}
		}
		return "redirect:" + environment.getProperty("BASE_URL") + "/inventory";
	}

	@RequestMapping(value = "/getAllkritiqCustomerByMerchantUid", method = RequestMethod.GET)
	public @ResponseBody String getAllkritiqCustomerByMerchantUid(
			@RequestParam(value = "merchantUid") String merchantUid) {
		List<Customer> customer = kritiqService.getAllkritiqCustomerByMerchantUid(merchantUid);
		Gson gson = new Gson();
		LOGGER.info("AdminHomeController : getCustomerByMerchantUid() : END");
		return gson.toJson(customer);
	}

	@RequestMapping(value = "/getAllkritiqCustomerByMerchantUidAnddateRange", method = RequestMethod.GET)
	public @ResponseBody String getAllkritiqCustomerByMerchantUidAnddateRange(
			@RequestParam(value = "merchantUid") String merchantUid,
			@RequestParam(value = "startDate", required = false) String startDate,
			@RequestParam(value = "endDate", required = false) String endDate) {

		List<Customer> customer = kritiqService.getAllkritiqCustomerByMerchantUidAndDateRange(merchantUid, startDate,
				endDate);

		Gson gson = new Gson();
		LOGGER.info("AdminHomeController : getCustomerByMerchantUidAnddateRange() : END");
		return gson.toJson(customer);

	}

	@RequestMapping(value = "/getActivekritiqCustomerByMerchantUid", method = RequestMethod.GET)
	public @ResponseBody String getActivekritiqCustomerByMerchantUid(
			@RequestParam(value = "merchantUid") String merchantUid) {
		List<Customer> customer = kritiqService.getActivekritiqCustomerByMerchantUid(merchantUid);
		Gson gson = new Gson();
		LOGGER.info("AdminHomeController : getCustomerByMerchantUid() : END");
		return gson.toJson(customer);
	}

	@RequestMapping(value = "/getActivekritiqCustomerByMerchantUidAnddate", method = RequestMethod.GET)
	public @ResponseBody String getActivekritiqCustomerByMerchantUidAnddateRange(
			@RequestParam(value = "merchantUid") String merchantUid,
			@RequestParam(value = "startDate", required = false) String startDate,
			@RequestParam(value = "endDate", required = false) String endDate) {

		List<Customer> customer = kritiqService.getActivekritiqCustomerByMerchantUidAndDateRange(merchantUid, startDate,
				endDate);

		Gson gson = new Gson();
		LOGGER.info("AdminHomeController : getCustomerByMerchantUidAnddateRange() : END");
		return gson.toJson(customer);

	}

	@RequestMapping(value = "/getAllFoodkonnektCustomerByMerchantUid", method = RequestMethod.GET)
	public @ResponseBody String getAllFoodkonnektCustomerByMerchantUid(
			@RequestParam(value = "merchantUid") String merchantUid) {
		List<Customer> customers = customerService.findByMerchantUId(merchantUid);
		List<Customer> customerlist = new ArrayList<Customer>();
		int count = 0;
		for (Customer customerEntity : customers) {
			if (customerEntity != null) {
				LOGGER.info("customer Id ====  " + customerEntity.getId());
				
				customerEntity.setAddresses(null);
				if (customerEntity.getMerchantt() != null) {
					customerEntity.setMerchantt(null);
					customerEntity.setListOfALLDiscounts(null);
					customerEntity.setVendor(null);
					customerEntity.setAddresses(null);
				}
				customerlist.add(customerEntity);
			}

			LOGGER.info("customer Id ====  " + customerEntity.getId());
			count++;
		}
		System.out.println("count ===== " + count);
		Gson gson = new Gson();
		LOGGER.info("AdminHomeController : getCustomerByMerchantUid() : END");
		return gson.toJson(customerlist);
	}

	@RequestMapping(value = "/getActiveFoodkonnektCustomerByMerchantUidAnddate", method = RequestMethod.GET)
	public @ResponseBody String getActiveFoodkonnektCustomerByMerchantUidAnddateRange(
			@RequestParam(value = "locationUid") String locationUid, @RequestParam(required = false) String startDate,
			@RequestParam(required = false) String endDate) {
		// Map<Integer,Object> customers = new HashMap<Integer, Object>();
		List<Customer> customers = new ArrayList<Customer>();
		try {
			customers = orderService.getActiveFoodkonnektCustomerByLocationUidAnddate(locationUid, startDate, endDate);
		} catch (Exception e) {
			LOGGER.error("error: " + e.getMessage());
		}
		Gson gson = new Gson();
		LOGGER.info("AdminHomeController : getCustomerByMerchantUid() : END");
		return gson.toJson(customers);
	}

	@RequestMapping(value = "/showMapAndUnmapPizzaByAjax", method = RequestMethod.GET)
	public @ResponseBody String showMapAndUnmapPizzaByAjax(@RequestParam("crustId") Integer crustId,
			HttpServletRequest request) {
		LOGGER.info("===============  AdminHomeController : Inside showModifiersByAjax :  Start ============= ");
		LOGGER.info(": AdminHomeController : Inside showModifiersByAjax :  modifierGroupId :" + crustId);
		Map<String, List<PizzaTemplate>> pizzaresponse = new HashMap<String, List<PizzaTemplate>>();
		List<PizzaTemplate> mappedpizzalist = new ArrayList<PizzaTemplate>();
		List<PizzaTemplate> unmappedpizzalist = new ArrayList<PizzaTemplate>();
		Gson gson = new Gson();
		Merchant merchant = null;
		HttpSession session = request.getSession();
		if (session != null) {
			merchant = (Merchant) session.getAttribute("merchant");
			if (merchant != null) {
				List<PizzaTemplateCrust> mappedpizzas = pizzaTemplateCrustRepository
						.findByPizzaCrustIdAndPizzaTemplateActive(crustId, 1);
				List<PizzaTemplate> unmappedpizzas = pizzaTemplateRepository
						.findUnMappedpizzaByCrustIdAndPizzaActive(crustId, merchant.getId(), 1);
				LOGGER.info(": AdminHomeController : Inside showModifiersByAjax :  modifierListSize :"
						+ mappedpizzas.size());
				if (mappedpizzas != null && !mappedpizzas.isEmpty() && mappedpizzas.size() > 0) {
					for (PizzaTemplateCrust mappizzas2 : mappedpizzas) {
						mappizzas2.getPizzaTemplate().setCategory(null);
						mappizzas2.getPizzaTemplate().setMerchant(null);
						mappizzas2.getPizzaTemplate().setTaxes(null);
						mappizzas2.getPizzaTemplate().setPizzaCrust(null);
						mappizzas2.getPizzaTemplate().setPizzaSizes(null);
						mappizzas2.getPizzaTemplate().setPizzaTemplateCategories(null);
						mappizzas2.getPizzaTemplate().setPizzaTemplateSizes(null);
						mappizzas2.getPizzaTemplate().setPizzaTemplateTaxs(null);
						mappizzas2.getPizzaTemplate().setPizzaTemplateToppings(null);
						mappizzas2.getPizzaTemplate().setPizzaToppings(null);
						mappizzas2.setPizzaCrust(null);
						mappizzas2.getPizzaTemplate().setMapcruststatus(mappizzas2.getActive());
						mappedpizzalist.add(mappizzas2.getPizzaTemplate());
					}
					LOGGER.info(
							"===============  AdminHomeController : Inside showModifiersByAjax :  End ============= ");
					pizzaresponse.put("mappedpizzas", mappedpizzalist);

				}
				if (unmappedpizzas != null && !unmappedpizzas.isEmpty() && unmappedpizzas.size() > 0) {
					for (PizzaTemplate unmappizzas2 : unmappedpizzas) {
						unmappizzas2.setCategory(null);
						unmappizzas2.setMerchant(null);
						unmappizzas2.setCategory(null);
						unmappizzas2.setTaxes(null);
						unmappizzas2.setPizzaCrust(null);
						unmappizzas2.setPizzaSizes(null);
						unmappizzas2.setPizzaTemplateCategories(null);
						unmappizzas2.setPizzaTemplateSizes(null);
						unmappizzas2.setPizzaTemplateTaxs(null);
						unmappizzas2.setPizzaTemplateToppings(null);
						unmappizzas2.setPizzaToppings(null);
						unmappedpizzalist.add(unmappizzas2);
					}
					LOGGER.info(
							"===============  AdminHomeController : Inside showModifiersByAjax :  End ============= ");
					pizzaresponse.put("unmappedpizzas", unmappedpizzalist);

				}
			}
		}
		LOGGER.info("===============  AdminHomeController : Inside showModifiersByAjax :  End ============= ");

		return gson.toJson(pizzaresponse);
	}

	@RequestMapping(value = "/updateCrustMapping", method = RequestMethod.POST)
	public String updateCrustMapping(@ModelAttribute("PizzaCrust") PizzaCrust pizzaCrust, ModelMap model,
			HttpServletRequest request) {
		try {
			if (pizzaCrust.getMerchant() != null) {
				LOGGER.info(
						"===============  AdminHomeController : Inside updateModifierGroup :: Start  ============= pizzaTemplate "
								+ pizzaCrust.getMerchant().getId());
			}
			HttpSession session = request.getSession();
			if (session != null) {
				Merchant merchant = (Merchant) session.getAttribute("merchant");
				if (merchant != null) {
					pizzaService.updatePizzaCrustMapping(pizzaCrust, merchant.getId());
				} else
					return "redirect:" + environment.getProperty("BASE_URL") + "/support";
			} else
				return "redirect:" + environment.getProperty("BASE_URL") + "/support";
		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
				LOGGER.error("error: " + e.getMessage());
				LOGGER.info("===============  AdminHomeController : Inside updateModifierGroup :: End  ============= ");

				return "redirect:https://www.foodkonnekt.com";
			}
		}
		LOGGER.info("===============  AdminHomeController : Inside updateModifierGroup :: End  ============= ");

		return "redirect:" + environment.getProperty("BASE_URL") + "/pizzaCrust";
	}

	@RequestMapping(value = "/pizzaSize", method = RequestMethod.GET)
	public String pizzaSize(ModelMap model, HttpServletRequest request) {
		LOGGER.info("===============  AdminHomeController : Inside pizzaSize :: Start  ============= ");

		HttpSession session = request.getSession(false);
		if (session != null) {
			Merchant merchant = (Merchant) session.getAttribute("merchant");
			if (merchant != null) {
				model.addAttribute("pizzaSizes", pizzaService.findAllSizes(merchant.getId()));
				if (merchant.getOwner() != null && merchant.getOwner().getPos() != null
						&& merchant.getOwner().getPos().getPosId() != null)
					model.addAttribute("merchantType", merchant.getOwner().getPos().getPosId());
			} else {
				return "redirect:" + environment.getProperty("BASE_URL") + "/support";
			}
		} else {
			return "redirect:" + environment.getProperty("BASE_URL") + "/support";
		}
		LOGGER.info("===============  AdminHomeController : Inside pizzaSize :: End  ============= ");

		return "pizzaSize";
	}

	@RequestMapping(value = "/showpizzaSizeDataByAjax", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String showpizzaSizeDataByAjax(HttpServletRequest request) throws IOException {
		LOGGER.info("===============  AdminHomeController : Inside showpizzaSizeDataByAjax :: Start  ============= ");

		HttpSession session = request.getSession(false);
		Merchant merchant = (Merchant) session.getAttribute("merchant");
		LOGGER.info("===== AdminHomeController : Inside showpizzaSizeDataByAjax :: merchant  == " + merchant.getId());

		String jsonOutput = "";
		if (merchant != null) {
			jsonOutput = pizzaService.showPizzaSizeData(merchant.getId());
		}
		LOGGER.info("===============  AdminHomeController : Inside showpizzaSizeDataByAjax :: End  ============= ");

		return jsonOutput;
	}

	@RequestMapping(value = "/updateSizeStatusById", method = RequestMethod.GET)
	@ResponseBody
	public String updateSizeStatusById(@RequestParam(required = true) Integer sizeId,
			@RequestParam(required = true) Integer itemStatus) {
		try {
			LOGGER.info("===============  InventoryController : Inside updateSizeStatusById :: Start  ============= ");

			PizzaSize pizzaSize = new PizzaSize();
			pizzaSize.setId(sizeId);
			pizzaSize.setActive(itemStatus);
			pizzaService.updateSizeStatusById(pizzaSize);
		} catch (Exception e) {
			if (e != null) {
				LOGGER.error(
						"===============  InventoryController : Inside updateSizeStatusById :: Exception  ============= "
								+ e);

				MailSendUtil.sendExceptionByMail(e,environment);
				LOGGER.error("error: " + e.getMessage());
				return "redirect:https://www.foodkonnekt.com";
			}
		}
		LOGGER.info("===============  InventoryController : Inside updateSizeStatusById :: End  ============= ");

		return "succuss";
	}

	@RequestMapping(value = "/editPizzaSize", method = RequestMethod.GET)
	public String viewEditPizzaSize(@ModelAttribute("pizzaSize") PizzaSize pizzaSize, ModelMap model,
			HttpServletRequest request, @RequestParam(required = false) int sizeId) {
		try {
			LOGGER.info(
					"===============  AdminHomeController : Inside viewEditPizzaSize :: Start  =============toppingId "
							+ sizeId);

			HttpSession session = request.getSession(false);
			if (session != null) {
				Merchant merchant = (Merchant) session.getAttribute("merchant");
				if (merchant != null) {

					pizzaSize = pizzaService.findSizeByPizzaSizeId(sizeId);
					model.addAttribute("description", pizzaSize.getDescription());
					model.addAttribute("status", pizzaSize.getActive());
					model.addAttribute("pizzaSize", pizzaSize);

					List<PizzaTemplateSize> mappedpizzas = pizzaTemplateSizeRepository
							.findByPizzaSizeIdAndPizzaTemplateActive(sizeId, 1);
					List<PizzaTemplate> unmappedpizzas = pizzaTemplateRepository
							.findUnMappedpizzaBySizeIdAndPizzaActive(sizeId, merchant.getId(), 1);

					List<PizzaToppingSize> mappedTopping = pizzaToppingSizeRepository
							.findByPizzaSizeIdAndPizzaToppingActive(sizeId, 1);
					List<PizzaTopping> unmappedTopping = pizzaToppingRepository
							.findUnMappedToppingBySizeIdAndToppingActive(sizeId, merchant.getId(), 1);

					List<PizzaCrustSizes> mappedCrust = pizzaCrustSizeRepository
							.findByPizzaSizeIdAndPizzaCrustActive(sizeId, 1);
					List<PizzaCrust> unmappedCrust = pizzaCrustRepository
							.findUnMappedCrustBySizeIdAndCrustActive(sizeId, merchant.getId(), 1);

					model.addAttribute("mappedpizzas", mappedpizzas);
					model.addAttribute("unmappedpizzas", unmappedpizzas);
					model.addAttribute("mappedTopping", mappedTopping);
					model.addAttribute("unmappedTopping", unmappedTopping);
					model.addAttribute("mappedCrust", mappedCrust);
					model.addAttribute("unmappedCrust", unmappedCrust);

				}
			}
			LOGGER.info("===============  AdminHomeController : Inside viewEditPizzaSize :  End ============= ");

		} catch (Exception e) {

			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
				LOGGER.error("error: " + e.getMessage());
				LOGGER.info(
						"===============  AdminHomeController : Inside viewEditPizzaSize :: Exception  ============= "
								+ e);

				return "redirect:https://www.foodkonnekt.com";
			}

		}
		LOGGER.info("===============  AdminHomeController : Inside viewEditPizzaTopping :: End  ============= ");

		return "editPizzaSize";
	}

	@RequestMapping(value = "/updatePizzaSize", method = RequestMethod.POST)
	public String updatePizzaSize(@ModelAttribute("pizzaSize") PizzaSize pizzaSize, ModelMap model,
			HttpServletRequest request) {
		try {
			LOGGER.info(
					"===============  AdminHomeController : Inside updateModifierGroup :: Start  ============= modifierGroup "
							+ pizzaSize.getId());

			HttpSession session = request.getSession();
			if (session != null) {
				Merchant merchant = (Merchant) session.getAttribute("merchant");
				if (merchant != null) {
					pizzaService.updatePizzaSizeValue(pizzaSize, merchant.getId());
				} else
					return "redirect:" + environment.getProperty("BASE_URL") + "/support";
			} else
				return "redirect:" + environment.getProperty("BASE_URL") + "/support";
		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
				LOGGER.error("error: " + e.getMessage());
				LOGGER.info("===============  AdminHomeController : Inside updateModifierGroup :: End  ============= ");

				return "redirect:" + environment.getProperty("BASE_URL") + "/support";
			}
		}
		LOGGER.info("===============  AdminHomeController : Inside updateModifierGroup :: End  ============= ");

		return "redirect:" + environment.getProperty("BASE_URL") + "/pizzaSize";
	}

	@RequestMapping(value = "/updateModifierGroupStatus", method = RequestMethod.GET)
	@ResponseBody
	public String updateModifierGroupStatus(@RequestParam Integer modifiergroupId, @RequestParam Integer status,
			HttpServletRequest request) {
		try {
			LOGGER.info(
					"===============  AdminHomeController : Inside updateModifierGroupStatus :: start  ============= ");
			HttpSession session = request.getSession();
			if (session != null) {
				Merchant merchant = (Merchant) session.getAttribute("merchant");
				if (merchant != null && modifiergroupId != null && status != null) {
					LOGGER.info(
							"===============  AdminHomeController : Inside updateModifierGroupStatus :: modifiergroupId  ============= "
									+ modifiergroupId);
					LOGGER.info(
							"===============  AdminHomeController : Inside updateModifierGroupStatus :: status  ============= "
									+ status);
					LOGGER.info(
							"===============  AdminHomeController : Inside updateModifierGroupStatus :: merchantId  ============= "
									+ merchant.getId());
					ModifierGroup modifierGroup = modifierGroupRepository.findOne(modifiergroupId);
					// List<ItemModifierGroup>
					// itemModifierGroup=itemModifierGroupRepository.findByModifierGroupId(modifiergroupId);
					if (modifierGroup != null && modifierGroup.getId() != null)
						modifierGroup.setActive(status);
//					if(itemModifierGroup!=null && itemModifierGroup.size()>0)
//						for (ItemModifierGroup itemModifierGroup2 : itemModifierGroup) {
//							itemModifierGroup2.setModifierGroupStatus(status);
//						}
//						itemModifierGroupRepository.save(itemModifierGroup);
					modifierGroupRepository.save(modifierGroup);
					LOGGER.info(
							"===============  AdminHomeController : Inside updateModifierGroupStatus :: return  =============true ");
					return "true";
				}
			}
		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
				LOGGER.error("error: " + e.getMessage());
				LOGGER.info(
						"===============  AdminHomeController : Inside updateModifierGroupStatus :: End  ============= "
								+ e);
			}
		}
		LOGGER.info(
				"===============  AdminHomeController : Inside updateModifierGroupStatus :: return false End  ============= ");
		return "false";
	}

	@RequestMapping(value = "/updateModifierStatus", method = RequestMethod.GET)
	@ResponseBody
	public String updateModifierStatus(@RequestParam Integer modifierId, @RequestParam Integer status,
			HttpServletRequest request) {
		try {
			LOGGER.info(
					"===============  AdminHomeController : Inside updateModifierStatus ::   start  ============= ");
			HttpSession session = request.getSession();
			if (session != null) {
				Merchant merchant = (Merchant) session.getAttribute("merchant");
				if (merchant != null && modifierId != null && status != null) {
					LOGGER.info(
							"===============  AdminHomeController : Inside updateModifierStatus ::   modifierId  ============= "
									+ modifierId);
					LOGGER.info(
							"===============  AdminHomeController : Inside updateModifierStatus ::   status  ============= "
									+ status);
					LOGGER.info(
							"===============  AdminHomeController : Inside updateModifierStatus ::   merchant  ============= "
									+ merchant.getId());
					Modifiers modifier = modifierRepository.findOne(modifierId);
					// List<ItemModifiers>
					// itemModifier=itemModifiersRepository.findByModifiersId(modifierId);
					if (modifier != null && modifier.getId() != null)
						modifier.setStatus(status);
//					if(itemModifier!=null && itemModifier.size()>0)
//						for (ItemModifiers itemModifier2 : itemModifier) {
//							itemModifier2.setModifierStatus(status);
//						}
//					itemModifiersRepository.save(itemModifier);
					modifierRepository.save(modifier);
					LOGGER.info(
							"===============  AdminHomeController : Inside updateModifierStatus ::   return  ============= true");
					return "true";
				}
			}
		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
				LOGGER.error("error: " + e.getMessage());
				LOGGER.info(
						"===============  AdminHomeController : Inside updateModifierGroup :: Exception  ============= "
								+ e);
			}
		}
		LOGGER.info(
				"===============  AdminHomeController : Inside updateModifierStatus ::   return  ============= false");

		return "false";
	}

	@RequestMapping(value = "/getPayeezyToken", method = { RequestMethod.GET })
	@ResponseBody
	public void getPayeezyToken() {
		try {
			PayzeeyTest.payeezyTestMethod();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			LOGGER.error("error: " + e.getMessage());
		}
	}
	
	@RequestMapping(value = "/appAutoStartMail", method = RequestMethod.GET)
	@ResponseBody
	public String appAutoStartMail(@RequestParam("merchantPosId") String merchantPosId)
	{
		LOGGER.info("===============  AdminHomeController : Inside appAutoStartMail :: start  ============= ");
		try{
			if(merchantPosId!=null && merchantPosId.isEmpty())
			{
				Merchant merchant =merchantService.findByPosMerchantId(merchantPosId);
				if(merchant!=null && merchant.getName()!=null && merchant.getPosMerchantId()!=null)
				{
					return MailSendUtil.sendAutoStartmail(merchant.getName(), merchant.getPosMerchantId(),environment);
				}
			}
		}catch(Exception e)
		{
			MailSendUtil.sendExceptionByMail(e,environment);
		}
		return "false";
	}
	
	@RequestMapping(value = "/getNotification", method = RequestMethod.GET)
	@ResponseBody
	public String getNotification(@RequestParam("merchantId") Integer merchantId)
	{
		LOGGER.info("===============  AdminHomeController : Inside merchantId :: start  ============= ");
		Gson gson = new Gson();
		try{
			if(merchantId!=null)
			{
				MerchantConfiguration merchantConfiguration =merchantConfigurationService.findMerchantConfigurationBymerchantId(merchantId);
				if(merchantConfiguration!=null)
				{
					merchantConfiguration.setMerchant(null);
					return gson.toJson(merchantConfiguration);
				}
			}
		}catch(Exception e)
		{
			MailSendUtil.sendExceptionByMail(e,environment);
		}
		return "{'Error':'no datafound'}";
	}
	
	@RequestMapping(value = "/saveTimeZones", method = RequestMethod.GET)
	@ResponseBody
	public List<TimeZone> saveTimeZones()
	{
		LOGGER.info("===============  AdminHomeController : Inside appAutoStartMail :: start  ============= ");
		try{
			String [] ids = java.util.TimeZone.getAvailableIDs();
			List<String> timeZoneDropDown = new ArrayList<String>();
			
			List<TimeZone> existingTimeZone = timeZoneRepository.findAll();
			
			boolean timeZoneflag = true;
			for(String id:ids) {
				java.util.TimeZone zone = java.util.TimeZone.getTimeZone(id);
				
			    for(TimeZone timezone : existingTimeZone) {
			    	if(timezone.getTimeZoneCode().equalsIgnoreCase(zone.getID()))
			    	{
			    		timeZoneflag= false;
			    		break;
			    	}
			    	else {
			    		timeZoneflag= true;
			    	}
			    }
			    
			    if(timeZoneflag) {
			    	
			    	TimeZone dbzone = new TimeZone();
					dbzone.setHourDifference(0);
					dbzone.setMinutDifference(0);
					dbzone.setTimeZoneCode(zone.getID());
					//dbzone.setTimeZoneName(zone.getDisplayName());
			    	timeZoneRepository.save(dbzone);
			    }
			} 
		}catch(Exception e)
		{
			MailSendUtil.sendExceptionByMail(e,environment);
		}
		return timeZoneRepository.findAll();
	}
	
	@RequestMapping(value = "/forgotUserPassword", method = RequestMethod.POST)
	public @ResponseBody String forgotUserPassword(@RequestParam(value = "emailid") String emailid, HttpServletRequest request,
			HttpServletResponse response, ModelMap model) throws Exception {

		LOGGER.info(
				"===============  AdminHomeController : Inside forgotUserPassword :: Start  ============= emailid "
						+ emailid);

		String message = "";
		try {
			HttpSession session = request.getSession();

			if (emailid != null) {
				boolean status = customerService.findUserByEmail(emailid);
				if (status) {
					
					message = "Please check your email for further instructions";
				} else {
					
					message = "Account with the above email does not exist";
				}
			}
		} catch (Exception exception) {
			if (exception != null) {
				MailSendUtil.sendExceptionByMail(exception,environment);
				LOGGER.info(
						"===============  AdminHomeController : Inside forgotUserPassword :: Exception  ============= "
								+ exception);
			}
			exception.printStackTrace();
		}
		LOGGER.info("===============  AdminHomeController : Inside forgotUserPassword :: End  =============message "
				+ message);
		model.addAttribute("message", message);
		return message ;
	}
	@RequestMapping(value = "/getEnvironment", method = RequestMethod.GET)
    public String getEnvironment(ModelMap model, HttpServletRequest request) {
		model.addAttribute("PropEnv",environment.getProperty("environment"));
        model.addAttribute("PropConst",iConstantProperties.getProperty("environment"));
		model.addAttribute("authorize",ProducerUtil.getAuthorizeEnv(environment.getProperty("AuthorizeEnv")));

		return "environment";
	}
	
	@RequestMapping(value = "/getBusinessHoursByPosId", method = RequestMethod.GET)
	public @ResponseBody String getBusinessHoursByPosId(@RequestParam("merchantPosId") String merchantPosId) {
		LOGGER.info("===============  AdminHomeController : Inside getBusinessHoursByPosId ::"
				+ " Start  ============= posId " + merchantPosId);

		List<OpeningClosingDay> openingClosingHours = null;
		String listOpeningClosingHours = null;
		
		try {
		if (merchantPosId != null) {
			Merchant merchant = merchantService.findByPosMerchantId(merchantPosId);
			if (merchant != null && merchant.getId() != null) {

				openingClosingHours = businessService.findBusinessHourByMerchantId(merchant.getId());
				if (openingClosingHours != null && !openingClosingHours.isEmpty()) {
					for (OpeningClosingDay openingClosingDay : openingClosingHours) {
						openingClosingDay.setMerchant(null);
						openingClosingDay.setOpeningClosingTimes(null);
						for (OpeningClosingTime openingClosingTime : openingClosingDay.getTimes()) {
							openingClosingTime.setOpeningClosingDay(null);
						}
					}
				}
			}
		}
		Gson gson = new Gson();

		listOpeningClosingHours = gson.toJson(openingClosingHours);
		LOGGER.info("===============  AdminHomeController : Inside getBusinessHoursByPosId :: "
					+ "End  returns ============= listOpeningClosingHours " + listOpeningClosingHours);
		}catch (Exception e) {
			LOGGER.info("====== Inside getBusinessHoursByPosId ::  Exception :: "+e);  
		}
		return listOpeningClosingHours;
	}
	@RequestMapping(value = "/deleteItemImage", method = RequestMethod.GET)
	public String deleteItemImage(@RequestParam("itemId") Integer itemId) {
		LOGGER.info("===============  AdminHomeController : Inside deleteItemImage :: Start  =============  itemId " +
							itemId);
		Item result = itemRepository.findOne(itemId);
		result.setItemImage(null);
		itemRepository.save(result);
		LOGGER.info("===============  AdminHomeController : Inside deleteItemImage :: End  ============= ");
		return "redirect:" + environment.getProperty("BASE_URL") + "/addLineItem?itemId=" + itemId;
	}

	@RequestMapping(value = "/appStatus",method = RequestMethod.GET)
	public String appStatus() {
		return "appStatus";
	}
}
