package com.foodkonnekt.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.foodkonnekt.model.Merchant;
import com.foodkonnekt.model.PizzaCrust;
import com.foodkonnekt.model.UberStore;
import com.foodkonnekt.repository.PosIntegrationRepository;
import com.foodkonnekt.repository.UberRepository;
import com.foodkonnekt.service.CategoryService;
import com.foodkonnekt.service.UberService;
import com.foodkonnekt.util.ProducerUtil;
//import com.foodkonnekt.util.UrlConstant;
import com.google.api.client.http.HttpStatusCodes;

@Controller
public class UberController {
	
	private static final Logger LOGGER= LoggerFactory.getLogger(UberController.class);
	
	@Autowired
    private Environment environment;
	
	@Autowired
	private UberRepository uberRepository;

	@Autowired
	private UberService uberService;
	
	
	@RequestMapping(value = "/uberStore", method = RequestMethod.GET)
	public String uberstore(@ModelAttribute("UberStore") UberStore uberStore,
			            ModelMap model, HttpServletRequest request) {
		try {
		HttpSession session = request.getSession();
		List<UberStore> uberstore = null;
		if (session != null) {
			Merchant merchant = (Merchant) session.getAttribute("merchant");

			if (merchant != null) {
				uberstore = uberService.findStorebyMerchnatId(merchant);
			}
		}
		if(uberstore !=null && uberstore.size()>0)
		model.addAttribute("uberstore", uberstore.get(0));
		
		}catch (Exception e) {
			LOGGER.info("Exception in UberController : uberStore : "+e);
			return "redirect:" + environment.getProperty("BASE_URL") + "/support";
		}
		return "uberStore";
	}
	
	@RequestMapping(value = "/addNewUberStore", method = RequestMethod.POST)
	public String addNewUberStore(@ModelAttribute("UberStore") UberStore uberStore,
			            ModelMap model, HttpServletRequest request) {
		HttpSession session = request.getSession();
		String responseCode = null;
		String response = null;
		if (session != null) {
			Merchant merchant = (Merchant) session.getAttribute("merchant");

			if (merchant != null && uberStore!=null) {
			    response = uberService.saveUberStore(uberStore , merchant);
			    model.addAttribute("uberresponse", response);
				return "redirect:" + environment.getProperty("BASE_URL") + "/uberStore";
			}
			
		}
		return "redirect:" + environment.getProperty("BASE_URL") + "/support";
	}
	
	@RequestMapping(value = "/updateStoreDetails", method = RequestMethod.POST)
	public String updateStoreDetails(@ModelAttribute("UberStore") UberStore uberStore,
			            ModelMap model, HttpServletRequest request) {
		HttpSession session = request.getSession();
		
		String response = null;
		if (session != null) {
			Merchant merchant = (Merchant) session.getAttribute("merchant");

			if (merchant != null && uberStore!=null) {
				response = uberService.updateStoreDetails(uberStore);
				model.addAttribute("uberresponse", response);
				return "redirect:" + environment.getProperty("BASE_URL") + "/uberStore";
			}
		}
		return "redirect:" + environment.getProperty("BASE_URL") + "/support";
	}
	
//	@RequestMapping(value = "/uploadMenuOnUber", method = RequestMethod.GET)
//	public String uploadMenuOnUber(HttpServletRequest request) throws IOException {
//		HttpSession session = request.getSession(false);
//		String responseCode = null;
//		if(session != null) {
//		Merchant merchant = (Merchant) session.getAttribute("merchant");
//		
//		if (merchant != null) {
//			responseCode = uberService.uploadMenuOnUber(merchant.getId(),"0ce3c1d5-4c0e-4f1d-97d2-24fd850dc8d7");
//			LOGGER.info(" responseCode ===== "+responseCode);
//		}
//		}
//		return responseCode;
//	}
    
	
	@RequestMapping(value = "/webhookOrderNotification", method = { RequestMethod.POST })
    public @ResponseBody Object webhookOrderNotification(@RequestBody String orderNotification ) throws IOException {
		Object statusCode = null;
		String responseCode = null;
		LOGGER.info(" orderNotification =====  : "+orderNotification);
		JSONObject jsonObject = new JSONObject(orderNotification);
		
		if(jsonObject.has("event_type") && jsonObject.getString("event_type").equals("orders.notification")) {
			
			LOGGER.info("webHooks : event_type = "+jsonObject.getString("event_type"));
			
			String orderDetails =  uberService.getOrderDetails(orderNotification);
			if(orderDetails != null && !orderDetails.isEmpty() && orderDetails.contains("store")) {
			responseCode = uberService.updateOrderDetails(orderDetails);
			if(responseCode.equals("success"))
			statusCode = new HttpEntity(HttpStatusCodes.STATUS_CODE_OK);
			}
			else
			LOGGER.info("No order Found");
			
			
		}
		
		return statusCode;
	}
	
	@RequestMapping(value = "/updateUberInventory", method = RequestMethod.GET)
	public @ResponseBody String updateUberInventory(@RequestParam("storeId") String uberStoreId,
			            ModelMap model, HttpServletRequest request) {
		HttpSession session = request.getSession();
		LOGGER.info(" updateUberInventory Started  : uberStoreId : "+uberStoreId);
		String response = null;
		if (session != null) {
			Merchant merchant = (Merchant) session.getAttribute("merchant");

			if (merchant != null && uberStoreId!=null) {
			    response = uberService.updateUberInventory(uberStoreId , merchant);
			}else {
				return "redirect:" + environment.getProperty("BASE_URL") + "/support";
			}
			
		}else {
			return "redirect:" + environment.getProperty("BASE_URL") + "/support";
		}
		return response;
	}
	
//	@RequestMapping(value = "/updateUberBusinessHours", method = RequestMethod.GET)
//	public @ResponseBody String updateUberBusinessHours(@RequestParam("storeId") String uberStoreId,
//			            ModelMap model, HttpServletRequest request) {
//		LOGGER.info(" updateUberBusinessHours Started  : uberStoreId : "+uberStoreId);
//		HttpSession session = request.getSession();
//		String response = null;
//		if (session != null) {
//			Merchant merchant = (Merchant) session.getAttribute("merchant");
//
//			if (merchant != null && uberStoreId!=null) {
//			    response = uberService.updateUberBusinessHours(uberStoreId , merchant);
//			}else {
//				return "redirect:" + environment.getProperty("BASE_URL") + "/support";
//			}
//			
//		}else {
//			return "redirect:" + environment.getProperty("BASE_URL") + "/support";
//		}
//		return response;
//	}
	
}
