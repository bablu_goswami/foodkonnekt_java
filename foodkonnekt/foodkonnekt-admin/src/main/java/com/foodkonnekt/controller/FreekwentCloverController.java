package com.foodkonnekt.controller;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.foodkonnekt.model.Address;
import com.foodkonnekt.model.Clover;
import com.foodkonnekt.model.Merchant;
import com.foodkonnekt.model.ProductMerchantMap;
import com.foodkonnekt.repository.AddressRepository;
import com.foodkonnekt.repository.ProductMerchantMapRepository;
import com.foodkonnekt.service.BusinessService;
import com.foodkonnekt.service.CloverService;
import com.foodkonnekt.service.MerchantService;
import com.foodkonnekt.service.ModifierService;
import com.foodkonnekt.service.OrderService;
import com.foodkonnekt.service.PizzaService;
import com.foodkonnekt.service.WebhookAppService;
import com.foodkonnekt.util.CloverUrlUtil;
import com.foodkonnekt.util.IConstant;
import com.foodkonnekt.util.UrlConstant;

@Controller
public class FreekwentCloverController {

	
	private static final Logger LOGGER = LoggerFactory.getLogger(FreekwentCloverController.class);
	
	@Autowired
    private Environment environment;
	
    @Autowired
    private CloverService cloverService;

    @Autowired
    private MerchantService merchantService;

    @Autowired
    private ModifierService modifierService;

    @Autowired
    private OrderService orderService;

    @Autowired
    private BusinessService businessService;

    @Autowired
    private WebhookAppService webhookAppService;

    @Autowired
    private PizzaService pizzaService;

    @Autowired
    private AddressRepository addressRepository;

    @Autowired
    private ProductMerchantMapRepository productMerchantMapRepository;

    @RequestMapping(value = "/freekwentCloverRequest", method = RequestMethod.GET)
    public String freekwentCloverRequest(@RequestParam("merchant_id") String merchantId,
       @RequestParam("code") String code, HttpServletResponse response, HttpServletRequest request) {
    	LOGGER.info("===============  FreekwentCloverController : Inside freekwentCloverRequest :: Start  ============= ");

    	
        String accessToken = CloverUrlUtil.getFreekWentMerchantAccessToken(code,environment);
        if (accessToken != null && !accessToken.equals("Failed")) {
            final Clover clover = new Clover();
            clover.setMerchantId(merchantId);
            clover.setAuthToken(accessToken);
            clover.setInstantUrl(IConstant.CLOVER_INSTANCE_URL);
            clover.setUrl(environment.getProperty("CLOVER_URL"));
            LOGGER.info("---------MerchantId-----" + merchantId);
            LOGGER.info("---------accessToken-----" + accessToken);
            List<Address> addressList = null;
            Merchant merchantCheck = merchantService.findbyPosId(merchantId);
            LOGGER.info("===============  FreekwentCloverController : Inside freekwentCloverRequest :: merchant "+merchantCheck.getId());

            if (null != merchantCheck &&merchantCheck.getId()!=null) {
                addressList = addressRepository.findByMerchantId(merchantCheck.getId());
               
                ProductMerchantMap productMerchantMap = productMerchantMapRepository.findByMerchantIdAndProductId(merchantCheck.getId(), 5);
                if (productMerchantMap == null) {
                    productMerchantMap = new ProductMerchantMap();
                    productMerchantMap.setMerchantId(merchantCheck.getId());
                    productMerchantMap.setProductId(5);
                    productMerchantMapRepository.save(productMerchantMap);
                }
                ProductMerchantMap productMerchantMapOfFoodkonnekt = productMerchantMapRepository.findByMerchantIdAndProductId(merchantCheck.getId(), 1);
                if (productMerchantMapOfFoodkonnekt == null) {
                    productMerchantMapOfFoodkonnekt = new ProductMerchantMap();
                    productMerchantMapOfFoodkonnekt.setMerchantId(merchantCheck.getId());
                    productMerchantMapOfFoodkonnekt.setProductId(1);
                    productMerchantMapRepository.save(productMerchantMapOfFoodkonnekt);
                }               
                String POST_URL =  environment.getProperty("MkonnektPlatform_BASE_URL") + "entity/installLocation";
                LOGGER.info("POST_URL" + POST_URL);
                try {
                    HttpClient client = HttpClientBuilder.create().build();
                    HttpPost post = new HttpPost(POST_URL);
                    String locationJson = "{\"locationName\": \"" + merchantCheck.getName() + "\",  \"address1\": \""
                                    + addressList.get(0).getAddress1() + "\",  \"email\": \""
                                    + merchantCheck.getOwner().getEmail() + "\",  \"city\": \""
                                    + addressList.get(0).getCity() + "\",  \"country\": \""
                                    + addressList.get(0).getCountry() + "\",  \"phone\": \""
                                    + merchantCheck.getPhoneNumber() + "\",  \"zip\": \"" + addressList.get(0).getZip()
                                    + "\",  \"locationLogo\": \"" + merchantCheck.getMerchantLogo()
                                    + "\",  \"organizationId\": \"" + 79 + "\",  \"roleId\": \"" + 1
                                    + "\",  \"posId\": \"" + merchantCheck.getPosMerchantId() + "\","
                                    + " \"entity_type_id\": \"" + 4 + "\"}";
                    LOGGER.info("locationJson---------->" + locationJson);
                    StringEntity input = new StringEntity(locationJson);
                    input.setContentType("application/json");
                    post.setEntity(input);
                    HttpResponse response1 = client.execute(post);
                    BufferedReader rd = new BufferedReader(new InputStreamReader(response1.getEntity().getContent()));
                    
                } catch (Exception e) {
                	LOGGER.error("===============  FreekwentCloverController : Inside freekwentCloverRequest :: Exception  ============= "
							+ e);

                    LOGGER.error("error: " + e.getMessage());
                }
                LOGGER.info("===============  FreekwentCloverController : Inside freekwentCloverRequest :: End  ============= ");

                return "redirect:"+environment.getProperty("MkonnektPlatform_BASE_URL")+"#/login";
            } else {
                String merchantDetails = CloverUrlUtil.getMerchantDetails(clover);
                if (merchantDetails.contains("id")) {
                    LOGGER.info("---Inisde else bock---");
                    final Merchant merchant = cloverService.saveMerchant(clover);
                    
                    String POST_URL = environment.getProperty("MkonnektPlatform_BASE_URL") + "entity/installLocation";
                    LOGGER.info("POST_URL" + POST_URL);
                    try {
                        HttpClient client = HttpClientBuilder.create().build();
                        HttpPost post = new HttpPost(POST_URL);
                        String locationJson = "{\"locationName\": \"" + merchant.getName() + "\",  \"address1\": \""
                                        + addressList.get(0).getAddress1() + "\",  \"email\": \""
                                        + merchant.getOwner().getEmail() + "\",  \"city\": \""
                                        + addressList.get(0).getCity() + "\",  \"country\": \""
                                        + addressList.get(0).getCountry() + "\",  \"phone\": \""
                                        + merchant.getPhoneNumber() + "\",  \"zip\": \"" + addressList.get(0).getZip()
                                        + "\",  \"locationLogo\": \"" + merchant.getMerchantLogo()
                                        + "\",  \"organizationId\": \"" + 79 + "\",  \"roleId\": \"" + 1
                                        + "\",  \"posId\": \"" + merchant.getPosMerchantId() + "\","
                                        + " \"entity_type_id\": \"" + 4 + "\"}";
                        LOGGER.info("locationJson---------->" + locationJson);
                        StringEntity input = new StringEntity(locationJson);
                        input.setContentType("application/json");
                        post.setEntity(input);
                        HttpResponse response1 = client.execute(post);
                        BufferedReader rd = new BufferedReader(
                                        new InputStreamReader(response1.getEntity().getContent()));
                      
                    } catch (Exception e) {
                    	LOGGER.error("===============  FreekwentCloverController : Inside freekwentCloverRequest :: Exception  ============= "
    							+ e);
                    }
                    LOGGER.info("===============  FreekwentCloverController : Inside freekwentCloverRequest :: End  ============= ");

                    return "redirect:"+environment.getProperty("MkonnektPlatform_BASE_URL")+"#/login";
                } else {
                    LOGGER.info("===============  FreekwentCloverController : Inside freekwentCloverRequest :: End  ============= ");

                    return "redirect:https://www.foodkonnekt.com";
                }
            }
        }
        LOGGER.info("===============  FreekwentCloverController : Inside freekwentCloverRequest :: End  ============= ");

        return "redirect:https://www.foodkonnekt.com";
    }

    
    @RequestMapping(value = "/freekwentClover", method = RequestMethod.GET)
    public String freekwentCloverRequest() { 
        return "freekwentClover";
    }
    
    @RequestMapping(value = "/installlocationOnclientModule", method = RequestMethod.GET)
    public String installlocationOnclientModule(@RequestParam("merchant_id") String merchantId,
                    @RequestParam("accessToken") String accessToken, HttpServletResponse response,
                    HttpServletRequest request) throws Exception {
    	LOGGER.info("===============  FreekwentCloverController : Inside installlocationOnclientModule :: Start  ============= ");

        final Clover clover = new Clover();
        clover.setMerchantId(merchantId);
        clover.setAuthToken(accessToken);
        clover.setInstantUrl(IConstant.CLOVER_INSTANCE_URL);
        clover.setUrl(environment.getProperty("CLOVER_URL"));
        LOGGER.info("---------MerchantId-----" + merchantId);
        LOGGER.info("---------accessToken-----" + accessToken);
        Merchant merchantCheck = merchantService.findbyPosId(merchantId);
        LOGGER.info("===============  FreekwentCloverController : Inside installlocationOnclientModule :: merchant "+merchantCheck.getId());

        List<Address> addressList = null;
        if (merchantCheck != null && merchantCheck.getId()!=null) {
            addressList = addressRepository.findByMerchantId(merchantCheck.getId());
                ProductMerchantMap productMerchantMap = productMerchantMapRepository .findByMerchantIdAndProductId(merchantCheck.getId(), 5);
                if (productMerchantMap == null) {
                    productMerchantMap = new ProductMerchantMap();
                    productMerchantMap.setMerchantId(merchantCheck.getId());
                    productMerchantMap.setProductId(5);
                    productMerchantMapRepository.save(productMerchantMap);
                }
                ProductMerchantMap productMerchantMapOfFoodkonnekt = productMerchantMapRepository
                                .findByMerchantIdAndProductId(merchantCheck.getId(), 1);
                if (productMerchantMapOfFoodkonnekt == null) {
                    productMerchantMapOfFoodkonnekt = new ProductMerchantMap();
                    productMerchantMapOfFoodkonnekt.setMerchantId(merchantCheck.getId());
                    productMerchantMapOfFoodkonnekt.setProductId(1);
                    productMerchantMapRepository.save(productMerchantMapOfFoodkonnekt);
                }
            if (null != merchantCheck) {
                String POST_URL = environment.getProperty("MkonnektPlatform_BASE_URL") + "entity/installLocation";
                LOGGER.info("POST_URL" + POST_URL);
                try {
                    HttpClient client = HttpClientBuilder.create().build();
                    HttpPost post = new HttpPost(POST_URL);
                    String locationJson = "{\"locationName\": \"" + merchantCheck.getName() + "\",  \"address1\": \""
                                    + addressList.get(0).getAddress1() + "\",  \"email\": \""
                                    + merchantCheck.getOwner().getEmail() + "\",  \"city\": \""
                                    + addressList.get(0).getCity() + "\",  \"country\": \""
                                    + addressList.get(0).getCountry() + "\",  \"phone\": \""
                                    + merchantCheck.getPhoneNumber() + "\",  \"zip\": \"" + addressList.get(0).getZip()
                                    + "\",  \"locationLogo\": \"" + merchantCheck.getMerchantLogo()
                                    + "\",  \"organizationId\": \"" + 79 + "\",  \"roleId\": \"" + 1
                                    + "\",  \"posId\": \"" + merchantCheck.getPosMerchantId() + "\","
                                    + " \"entity_type_id\": \"" + 4 + "\"}";
                    LOGGER.info("locationJson---------->" + locationJson);
                    StringEntity input = new StringEntity(locationJson);
                    input.setContentType("application/json");
                    post.setEntity(input);
                    HttpResponse response1 = client.execute(post);
                    BufferedReader rd = new BufferedReader(new InputStreamReader(response1.getEntity().getContent()));
                 
                } catch (Exception e) {
                LOGGER.error(
						"===============  FreekwentCloverController : Inside installlocationOnclientModule :: Exception  ============= " + e);

                }
            }
            LOGGER.info("===============  FreekwentCloverController : Inside installlocationOnclientModule :: End  ============= ");

            return "redirect:"+environment.getProperty("MkonnektPlatform_BASE_URL")+"#/login";

        } else {
            String merchantDetails = CloverUrlUtil.getMerchantDetails(clover);
            LOGGER.info(merchantDetails);
            if (merchantDetails.contains("id")) {
                LOGGER.info("---Inisde else bock---");
                final Merchant merchant = cloverService.saveMerchant(clover);
               
                String POST_URL = environment.getProperty("MkonnektPlatform_BASE_URL") + "entity/installLocation";
                LOGGER.info("POST_URL" + POST_URL);
                addressList = addressRepository.findByMerchantId(merchant.getId());
                try {
                    HttpClient client = HttpClientBuilder.create().build();
                    HttpPost post = new HttpPost(POST_URL);
                    String locationJson = "{\"locationName\": \"" + merchant.getName() + "\",  \"address1\": \""
                                    + addressList.get(0).getAddress1() + "\",  \"email\": \""
                                    + merchant.getOwner().getEmail() + "\",  \"city\": \""
                                    + addressList.get(0).getCity() + "\",  \"country\": \""
                                    + addressList.get(0).getCountry() + "\",  \"phone\": \"" + merchant.getPhoneNumber()
                                    + "\",  \"zip\": \"" + addressList.get(0).getZip() + "\",  \"locationLogo\": \""
                                    + merchant.getMerchantLogo() + "\",  \"organizationId\": \"" + 79
                                    + "\",  \"roleId\": \"" + 1 + "\",  \"posId\": \"" + merchant.getPosMerchantId()
                                    + "\"," + " \"entity_type_id\": \"" + 4 + "\"}";
                    LOGGER.info("locationJson---------->" + locationJson);
                    StringEntity input = new StringEntity(locationJson);
                    input.setContentType("application/json");
                    post.setEntity(input);
                    HttpResponse response1 = client.execute(post);
                    BufferedReader rd = new BufferedReader(new InputStreamReader(response1.getEntity().getContent()));
                  
                } catch (Exception e) {
                	LOGGER.error("===============  FreekwentCloverController : Inside installlocationOnclientModule :: Exception  ============= "
							+ e);

                }
                LOGGER.info("===============  FreekwentCloverController : Inside installlocationOnclientModule :: End  ============= ");

                return "redirect:"+environment.getProperty("MkonnektPlatform_BASE_URL")+"#/login";
            } else {
                LOGGER.info("===============  FreekwentCloverController : Inside installlocationOnclientModule :: End  ============= ");

                return "https://www.foodkonnekt.com";
            }
        }
    }

}
