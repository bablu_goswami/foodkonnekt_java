package com.foodkonnekt.controller;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.foodkonnekt.clover.vo.KritiqCustomerVO;
import com.foodkonnekt.clover.vo.KritiqMerchantVO;
import com.foodkonnekt.model.Clover;
import com.foodkonnekt.model.Customer;
import com.foodkonnekt.model.CustomerFeedback;
import com.foodkonnekt.model.Item;
import com.foodkonnekt.model.Merchant;
import com.foodkonnekt.model.Modifiers;
import com.foodkonnekt.model.OrderItem;
import com.foodkonnekt.model.OrderR;
import com.foodkonnekt.model.TaxRates;
import com.foodkonnekt.model.Vendor;
import com.foodkonnekt.repository.CustomerrRepository;
import com.foodkonnekt.service.CloverService;
import com.foodkonnekt.service.CustomerService;
import com.foodkonnekt.service.ImportExcelService;
import com.foodkonnekt.repository.CustomerFeedbackRepository;
import com.foodkonnekt.service.MerchantService;
import com.foodkonnekt.service.OrderService;
import com.foodkonnekt.serviceImpl.ExcelToJsonConverterConfig;
import com.foodkonnekt.util.CommonUtil;
import com.foodkonnekt.util.DateUtil;
import com.foodkonnekt.util.EncryptionDecryptionUtil;
import com.foodkonnekt.util.IConstant;
import com.foodkonnekt.util.MailSendUtil;
import com.foodkonnekt.util.ProducerUtil;
import com.foodkonnekt.util.UrlConstant;
import com.google.gson.Gson;
import com.foodkonnekt.service.KritiqService;
import com.google.gson.reflect.TypeToken;

@Controller
public class CustomerController extends Thread implements Runnable {
	private static final Logger LOGGER= LoggerFactory.getLogger(CustomerController.class);

	@Autowired
    private Environment environment;
	
	@Autowired
	private KritiqService kritiqService;
	
	@Autowired
	private CustomerService customerService;

	@Autowired
	private MerchantService merchantService;

	@Autowired
	private CustomerFeedbackRepository customerFeedbackRepository;
	
	@Autowired
	CloverService cloverService;
	
	@Autowired
	OrderService orderService;
	
	@Autowired
	private ImportExcelService	importExcelService;
	
	@Autowired
	private CustomerrRepository customerRepo;

	public static Customer customerMail = null;
	public static String customerLogo = null;
	public static String merchantName = "FoodKonnekt";

	/**
	 * Find by email and password
	 * 
	 * @param registration
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/LoginByCustomer", method = RequestMethod.POST)
	public @ResponseBody Map<Object, Object> login(@RequestBody Customer customer, HttpServletResponse response) {

		Map<Object, Object> loginMap = new HashMap<Object, Object>();
		try {
			LOGGER.info("===============  CustomerController : Inside login :: Start  ============= ");

			Customer result = null;
			if (customer.getVendor() != null && customer.getVendor().getId() != null) {
				result = customerService.findByEmailAndPasswordAndVendorId(customer.getEmailId(),
						EncryptionDecryptionUtil.encryptString(customer.getPassword()), customer.getVendor().getId(),
						customer.getVendorId());
			} else {
				result = customerService.findByEmailAndPasswordAndMerchantId(customer.getEmailId(),
						EncryptionDecryptionUtil.encryptString(customer.getPassword()), customer.getVendorId());
			}
			if (result != null) {
				Customer customerResult = customerService.getCustomerProfile(result.getId());
				loginMap.put(IConstant.RESPONSE, IConstant.RESPONSE_SUCCESS_MESSAGE);
				loginMap.put(IConstant.DATA, customerResult);
			} else {
				loginMap.put(IConstant.RESPONSE, IConstant.RESPONSE_NO_DATA_MESSAGE);
				loginMap.put(IConstant.MESSAGE, IConstant.LOGIN_FAILURE);
			}
		} catch (Exception e) {
			if (e != null) {
				MailSendUtil.sendExceptionByMail(e,environment);
			}
			LOGGER.error("===============  CustomerController : Inside login :: Exception  ============= " + e);

			LOGGER.error("error: " + e.getMessage());
		}
		LOGGER.info("===============  CustomerController : Inside login :: End  ============= ");

		return loginMap;
	}

	/**
	 * Update customer profile
	 * 
	 * @param customer
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/updateProfile", method = RequestMethod.POST)
	public @ResponseBody Map<Object, Object> update(@RequestBody Customer customer, HttpServletResponse response) {
		Map<Object, Object> updateProfileMap = new HashMap<Object, Object>();
		try {
			if(customer!=null)
			{
			String password = customer.getPassword();

			Integer custId = customer.getId();
			Clover clover = new Clover();
			Customer customerResult = null;
			LOGGER.error("===============  CustomerController : Inside updateProfile :: custId" +custId );
			if (custId != null) {
				customerResult = customerService.findByCustomerId(custId);
				if (customerResult != null) {
					customer.setCustomerPosId(customerResult.getCustomerPosId());
					customer.setEmailPosId(customerResult.getEmailPosId());
					customer.setPhonePosId(customerResult.getPhonePosId());
					customer.setCustomerType(customerResult.getCustomerType());
				}
			}
			/* if (custId == null) { */
			
			Merchant merchant = merchantService.findById(customer.getMerchantt().getId());
			if(customer.getVendor()==null){
				if (merchant.getOwner() != null){
					customer.setVendor(merchant.getOwner());
				}
			}
			if (merchant.getOwner() != null && merchant.getOwner().getPos() != null
					&& merchant.getOwner().getPos().getPosId() != null
					&& merchant.getOwner().getPos().getPosId() == 1) {
				clover.setInstantUrl(IConstant.CLOVER_INSTANCE_URL);
				clover.setUrl(environment.getProperty("CLOVER_URL"));
				clover.setMerchantId(merchant.getPosMerchantId());
				clover.setAuthToken(merchant.getAccessToken());
				String customerResponse = cloverService.createCustomer(customer, clover);
				JSONObject jObject=null;
				try
				{
			       jObject = new JSONObject(customerResponse);
				}catch (Exception e) {
					
				}
				if (customerResponse.contains("id")) {
					customer.setCustomerPosId(jObject.getString("id"));

					JSONObject addressesJsonObject = jObject.getJSONObject("addresses");

					JSONArray addressesJsonArray = addressesJsonObject.getJSONArray("elements");
					int index = 0;
					if (addressesJsonArray != null) {
						for (Object jObj : addressesJsonArray) {
							JSONObject addressesJson = (JSONObject) jObj;
							if (addressesJson.toString().contains("id")) {
								LOGGER.info(addressesJson.getString("id"));
								/*
								 * Address address =
								 * customer.getAddresses().get(index++);
								 * address.setAddressPosId(addressesJson.
								 * getString("id"));
								 */
								// LOGGER.info(customer.getAddresses().get(0).getCity());
								// customer.getAddresses().add(address);
							}
						}
					}

					JSONObject emailJsonObject = jObject.getJSONObject("emailAddresses");

					JSONArray emailJsonArray = emailJsonObject.getJSONArray("elements");
					if (emailJsonArray != null) {
						for (Object jObj : emailJsonArray) {
							JSONObject emailJson = (JSONObject) jObj;
							if (emailJson.toString().contains("id")) {
								LOGGER.info(emailJson.getString("id"));
								customer.setEmailPosId(emailJson.getString("id"));
							}
						}
					}

					JSONObject phoneJsonObject = jObject.getJSONObject("phoneNumbers");

					JSONArray phoneJsonArray = phoneJsonObject.getJSONArray("elements");
					if (phoneJsonArray != null) {
						for (Object jObj : phoneJsonArray) {
							JSONObject phoneJson = (JSONObject) jObj;
							if (phoneJson.toString().contains("id")) {
								LOGGER.info(phoneJson.getString("id"));
								customer.setPhonePosId(phoneJson.getString("id"));
							}
						}
					}
				}
			}
			String merchantLogo = null;
			if (merchant.getMerchantLogo() == null) {
				merchantLogo = "http://www.dev.foodkonnekt.com/app/foodnew.jpg";
			} else {
				merchantLogo = environment.getProperty("BASE_PORT") + merchant.getMerchantLogo();
			}
			customer.getMerchantt().setMerchantUid(merchant.getMerchantUid());
			String customerUid = ProducerUtil.createCustomerIntoCentralDb(customer,environment);
			if(customerUid!=null)
				customer.setCustomerUid(customerUid);
			customerResult = customerService.updateCustomerProfile(customer);
			customerMail = customerResult;
			customerLogo = merchantLogo;
			if (merchant.getName() != null)
				merchantName = merchant.getName();
			/*
			 * }else{ customerResult =
			 * customerService.updateCustomerProfile(customer); }
			 */
			if (customerResult != null) {
				updateProfileMap.put(IConstant.RESPONSE, IConstant.RESPONSE_SUCCESS_MESSAGE);
				updateProfileMap.put(IConstant.DATA, customerResult);
			} else {
				updateProfileMap.put(IConstant.RESPONSE, IConstant.RESPONSE_NO_DATA_MESSAGE);
				updateProfileMap.put(IConstant.MESSAGE, IConstant.CUSTOMER_NOT_FOUND);
			}
			if (password != null) {
				if (!password.isEmpty()) {
					String pwsd[] = password.split("#");

						if (pwsd != null && pwsd.length > 1 && pwsd[0].equals("@duplicatepassword")) {

							LOGGER.info("duplicate customer created . registration will not be sent");
						} else {
							CustomerController controller = new CustomerController();
							if (custId == null) {
								LOGGER.info("new customer created . registration will be sent");
								controller.start();
							}
						}
					
					
				}
			}
		}
		} catch (Exception e) {
			if (e != null) {
				LOGGER.error("===============  CustomerController : Inside update :: Exception  ============= " + e);

				if (customer != null && customer.getMerchantt() != null) {
					StringWriter errors = new StringWriter();
					e.printStackTrace(new PrintWriter(errors));
					MailSendUtil.sendErrorMailToAdmin(
							errors.toString() + customer.getMerchantt().getId() + customer.getId(),environment);
				} else {
					MailSendUtil.sendExceptionByMail(e,environment);
				}

			}
			LOGGER.error("error: " + e.getMessage());
		}
LOGGER.info("===============  CustomerController : Inside update :: End  ============= ");

		return updateProfileMap;
	}

	/**
	 * Forgot password
	 * 
	 * @param customer
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/forgotPasswordByEmail", method = RequestMethod.POST)
	public @ResponseBody Map<Object, Object> forgot(@RequestBody Customer customer, HttpServletResponse response) {
		Map<Object, Object> forgotMap = new HashMap<Object, Object>();
		try {
			LOGGER.info("===============  CustomerController : Inside forgot :: Start  ============= ");

			boolean status = customerService.findByEmail(customer.getEmailId());
			if (status) {
				forgotMap.put(IConstant.RESPONSE, IConstant.RESPONSE_SUCCESS_MESSAGE);
				forgotMap.put(IConstant.DATA, "Password sent to email");
			} else {
				forgotMap.put(IConstant.RESPONSE, IConstant.RESPONSE_NO_DATA_MESSAGE);
				forgotMap.put(IConstant.MESSAGE, IConstant.CUSTOMER_NOT_FOUND);
			}
		} catch (Exception e) {
			if (e != null) {
				LOGGER.error("===============  CustomerController : Inside forgot :: Exception  ============= " + e);

				MailSendUtil.sendExceptionByMail(e,environment);
			}
			LOGGER.error("error: " + e.getMessage());
		}
		LOGGER.info("===============  CustomerController : Inside forgot :: End  ============= ");

		return forgotMap;
	}

	/**
	 * Check duplicate emailId
	 * 
	 * @param customer
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/checkEmailId", method = RequestMethod.POST)
	public @ResponseBody Map<Object, Object> checkEmail(@RequestBody Customer customer, HttpServletResponse response,
			@RequestParam(required = true) Integer merchantId, @RequestParam(required = false) Integer vendorId) {
		Map<Object, Object> emailMap = new HashMap<Object, Object>();
		try {
			LOGGER.info("===============  CustomerController : Inside checkEmail :: Start  ============= ");

			boolean status = false;
			if (vendorId != null && vendorId > 0) {
				status = customerService.findByEmailIdAndVendorId(customer.getEmailId(), vendorId);
			} else {
				status = customerService.findByEmailIdAndMerchantId(customer.getEmailId(), merchantId);
			}

			if (status) {
				emailMap.put(IConstant.RESPONSE, IConstant.RESPONSE_SUCCESS_MESSAGE);
				emailMap.put(IConstant.DATA, "Email already exist");
			} else {
				emailMap.put(IConstant.RESPONSE, IConstant.RESPONSE_NO_DATA_MESSAGE);
				emailMap.put(IConstant.MESSAGE, IConstant.CUSTOMER_NOT_FOUND);
			}
		} catch (Exception e) {
			if (e != null) {
				LOGGER.error("===============  CustomerController : Inside checkEmail :: Exception  ============= " + e);

				MailSendUtil.sendExceptionByMail(e,environment);
			}
			LOGGER.error("error: " + e.getMessage());
		}
		LOGGER.info("===============  CustomerController : Inside checkEmail :: End  ============= ");

		return emailMap;
	}

//	@RequestMapping(value = "/customers", method = RequestMethod.GET)
//	public String viewModifiers(ModelMap model, HttpServletRequest request, HttpServletResponse response) {
//		/*
//		 * try { HttpSession session = request.getSession(false); if (session !=
//		 * null) { Merchant merchant = (Merchant)
//		 * session.getAttribute("merchant"); if (merchant != null) {
//		 * model.addAttribute("customers",
//		 * customerService.findByVendorId(merchant.getId())); } else { return
//		 * "redirect:https://www.foodkonnekt.com"; } } else { return
//		 * "redirect:https://www.foodkonnekt.com"; } } catch (Exception e) { if
//		 * (e != null) { MailSendUtil.sendExceptionByMail(e,environment);
//		 * LOGGER.error("error: " + e.getMessage()); return "redirect:https://www.foodkonnekt.com"; }
//		 * }
//		 */
//		return "customers";
//	}
	@RequestMapping(value = "/customers", method = RequestMethod.GET)
	public String viewModifiers(ModelMap model, HttpServletRequest request, HttpServletResponse response) {
		LOGGER.info("----------------Start :: CustomerController : customers------------------");
	try {
    	  HttpSession session = request.getSession();
		  if (session != null){
		  Merchant merchant = (Merchant) session.getAttribute("merchant"); 
		  if (merchant != null) {
		
		   } else
			  return "redirect:" + environment.getProperty("BASE_URL") + "/support";
		  } else 
	 	      return "redirect:" + environment.getProperty("BASE_URL") + "/support";
	     } catch(Exception e){
	    	LOGGER.info("----------------EXCEPTION :: CustomerController : customers------------------");
		    LOGGER.error("error: " + e.getMessage());
		    MailSendUtil.sendExceptionByMail(e,environment);
		    return "redirect:" + environment.getProperty("BASE_URL") + "/support";
	     }
	    LOGGER.info("----------------END :: CustomerController : customers------------------");
		  return "customers";
	}

	public void run() {
		try {
			Thread.sleep(2000);
		} catch (InterruptedException exception) {
			LOGGER.error("===============  CustomerController :: InterruptedException "+exception);

			exception.printStackTrace();
		}
		MailSendUtil.customerRegistartionConfirmation(customerMail, customerLogo, merchantName);
		LOGGER.info("Thread over");
	}

	@RequestMapping(value = "/searchCustomerByText", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String searchCustomer(HttpServletRequest request,
			@RequestParam(required = true) String searchTxt) throws IOException {
		LOGGER.info("===============  CustomerController : Inside searchCustomer :: Start  ============= ");

		HttpSession session = request.getSession(false);
		Merchant merchant = (Merchant) session.getAttribute("merchant");
		String searchResult = "";
		if (merchant != null) {
			searchResult = customerService.searchCustomerByTxt(merchant.getId(), searchTxt);
		}
		LOGGER.info("===============  CustomerController : Inside searchCustomer :: End  ============= ");

		return searchResult;
	}

	/**
	 * Get customer by merchant id
	 * 
	 * @param request
	 * @param merchantId
	 * @return List<Customer>
	 */
	@RequestMapping(value = "/getCustomerByMerchantId", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody List<Customer> getCustomerByMerchantId(HttpServletRequest request,
			@RequestParam(required = false) Integer merchantId) {
		List<Customer> customers = new ArrayList<Customer>();
		try {
			LOGGER.info("===============  CustomerController : Inside getCustomerByMerchantId :: Start  ============= ");

			customers = customerService.findByMerchantId(merchantId);
			customers = CommonUtil.filterCustomers(customers);
		} catch (Exception exception) {
			LOGGER.error("===============  CustomerController : Inside getCustomerByMerchantId :: Exception  ============= " + exception);
			exception.printStackTrace();
		}
		LOGGER.info("===============  CustomerController : Inside getCustomerByMerchantId :: End  ============= ");

		return customers;
	}

	/**
	 * Get customer by merchant uId
	 * 
	 * @param request
	 * @param merchantId
	 * @return List<Customer>
	 */
	@RequestMapping(value = "/getCustomerByMerchantUId", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody List<Customer> getCustomerByMerchantUId(HttpServletRequest request,
			@RequestParam(required = false) String merchantUId) {
		List<Customer> customers = new ArrayList<Customer>();
		try {
			LOGGER.info("===============  CustomerController : Inside getCustomerByMerchantUId :: Start  ============= ");

			customers = customerService.findByMerchantUId(merchantUId);
			customers = CommonUtil.filterCustomers(customers);
		} catch (Exception exception) {
			LOGGER.error("===============  CustomerController : Inside getCustomerByMerchantUId :: Exception  ============= " + exception);

			exception.printStackTrace();
		}
		LOGGER.info("===============  CustomerController : Inside getCustomerByMerchantUId :: End  ============= ");

		return customers;
	}
	
	@RequestMapping(value = "/customersBasedOnSpent", method = RequestMethod.GET)
	public @ResponseBody String getCustomersBasedOnSpent(@RequestParam(required = true) Integer merchantUId,
			@RequestParam(required = false) Date startDate, @RequestParam(required = false) Date endDate,  @RequestParam(required = false) Double orderPrice)
			throws IOException {
		Map<Object, Object> response = new HashMap<Object, Object>();
		List<Customer> customers  =  new ArrayList<Customer>();
		Gson gson = new Gson();
	
		try {
			LOGGER.info("===============  CustomerController : Inside getCustomersBasedOnSpent :: Start  ============= ");

				if(merchantUId != null && startDate != null && endDate != null && orderPrice != null){
					customers = customerService.findByMerchantIdAndDateRangeAndSpent(merchantUId, startDate, endDate, orderPrice);
				}
				if(customers != null){
					response.put("data", customers);
				}
		} catch (Exception exception) {
			LOGGER.error("===============  CustomerController : Inside getCustomersBasedOnSpent :: Exception  ============= " + exception);

			exception.printStackTrace();
		}
		/*GsonBuilder builder = new GsonBuilder().excludeFieldsWithoutExposeAnnotation();
				String json = builder.create().toJson(response);*/
		LOGGER.info("===============  CustomerController : Inside getCustomersBasedOnSpent :: End  ============= ");

		return gson.toJson(response);
	}
	
	
	
	
	
	@RequestMapping(value = "/getCustomerDataAnalytics", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getCustomerDataAnalytics(@RequestParam(required = true) String searchCustomerByUUId,
			@RequestParam(required = false) Date startDate, @RequestParam(required = false) Date endDate)
			throws IOException {
		LOGGER.info("===============  CustomerController : Inside getCustomerDataAnalytics :: Start  ============= ");

		Map<Object, Object> orderObj = new HashMap<Object, Object>();
		Customer customers = new Customer();
		List<String> itemName = new ArrayList<String>();
		List<OrderR> orderRs = new ArrayList<OrderR>();
		Gson gson = new Gson();
		Item item = new Item();
		double orderTotal = 0;
		int orderSize = 0; 
		List<OrderItem> items = new ArrayList<OrderItem>();
		try {
			if (searchCustomerByUUId != null) {
				customers = customerService.searchCustomerByUUId(searchCustomerByUUId);
				if (customers != null && customers.getId() != null) {
					if (startDate != null && endDate != null) {
						orderRs = orderService.getOrderByCustomerId(customers.getId(), startDate, endDate);
					} else {
						orderRs = orderService.findOrderByCustomerId(customers.getId());
					}
					
					if (orderRs != null && orderRs.size() != 0) {
						orderSize = orderRs.size();
						for (OrderR orderR : orderRs) {
							orderTotal = orderTotal + orderR.getOrderPrice();
							items = customerService.findItemByOrderId(orderR.getId());
							for (OrderItem orderItem : items) {
								if (orderItem.getQuantity() > 1) {
									if (orderItem.getItem() != null) {
										item = customerService.findItemByItemId(orderItem.getItem().getId());
										itemName.add(item.getName());
									}
								}
							}
						}
					}
				}
				LOGGER.info("order total ="+orderTotal);
			}
			Map<String, Integer> count = new HashMap<String, Integer>();
			for (String word : itemName) {
				if (!count.containsKey(word)) {
					count.put(word, 1);
				} else {
					int value = count.get(word);
					value++;
					count.put(word, value);
				}
			}
			
			List<String> mostCommons = new ArrayList<String>();
			for (Map.Entry<String, Integer> e : count.entrySet()) {

				/*
				 * * if (e.getValue() == Collections.max(count.values())) {
				 * mostCommons.add(e.getKey()); }
				 */

				if (e.getValue() > 1) {
					mostCommons.add(e.getKey());
				}
			}
			
			List<OrderR> finalList = new ArrayList<OrderR>();
			for (OrderR order : orderRs) {

				order.setAuxTax(null);
				order.setConvenienceFee(null);
				order.setDeliveryFee(null);
				order.setFulfilled_on(null);
				order.setIsDefaults(null);
				order.setIsFutureOrder(null);
				order.setOrderAvgTime(null);
				// order.setOrderItemViewVOs(null);
				order.getMerchant().getMerchantUid();
				LOGGER.info("MerchantUid == "+order.getMerchant().getMerchantUid());
				order.setOrderNote(null);
				order.setOrderPosId(null); /* order.setOrders(null); */
				order.setOrderType(null);
				order.setPosPaymentId(null);
				order.setSubTotal(null);
				order.setTax(null);
				order.setTipAmount(null);
				order.setCustomer(null);
				order.getOrderItemViewVOs();
				// order.setMerchant(null);r

				finalList.add(order);
			}

			if (orderTotal != 0 && orderSize != 0) {
				double avgOrderAmt = orderTotal / orderSize;
				orderObj.put("orderTotal", orderTotal);
				orderObj.put("orderObject", finalList);
				orderObj.put("orderSize", orderSize);
				orderObj.put("avgOrderAmt", avgOrderAmt);
				orderObj.put("mostfavrote", mostCommons);
				orderObj.put("status", "200");
			} else {

				orderObj.put("status", "false");
			}
		} catch (Exception exception) {
			LOGGER.error("===============  CustomerController : Inside getCustomerDataAnalytics :: Exception  ============= " + exception);

			exception.printStackTrace();
		}
		LOGGER.info("===============  CustomerController : Inside getCustomerDataAnalytics :: End  ============= ");

		return gson.toJson(orderObj);
	}

	@RequestMapping(value = "/getMerchantDataAnalytics", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getMerchantDataAnalytics(@RequestParam(required = true) String merchantUId,
			@RequestParam(required = false) String startDate, @RequestParam(required = false) String endDate,
			@RequestParam(required = false) String requestType,@RequestParam(required = false) String price) throws IOException {
		LOGGER.info("===============  CustomerController : Inside getMerchantDataAnalytics :: Start  ============= ");

		Map<Object, Object> orderObj = new HashMap<Object, Object>();
		List<OrderR> orderRs = new ArrayList<OrderR>();
		// List<OrderR> orderlist = new ArrayList<OrderR>();
		List<List<OrderR>> listOLists = new ArrayList<List<OrderR>>();
		List<Customer> customers = new ArrayList<Customer>();
		Gson gson = new Gson();
		Merchant merchant = null;
		StringBuilder commaSepValueBuilder = new StringBuilder();
		List<Integer> customerIdList = new ArrayList<Integer>();
		List<OrderR> orderRrs = new ArrayList<OrderR>();
		Double filterPrice = null;
		if(price != null && !price.isEmpty()){
			filterPrice = Double.parseDouble(price);
		}
		try {
			if (merchantUId != null) {
				merchant = merchantService.findByMerchantUid(merchantUId);
				merchant.setAddresses(null);
				merchant.setItems(null);
				merchant.setMerchantSubscriptions(null);
				merchant.setModifierGroups(null);
				merchant.setModifiers(null);
				merchant.setOpeningClosingDays(null);
				merchant.setOrderRs(null);
				merchant.setOrderTypes(null);
				merchant.setPaymentModes(null);
				merchant.setVouchars(null);
				merchant.setTaxRates(null);
				
				if (merchant.getId() != null) {
					customers = customerService.findByMerchantId(merchant.getId());
					/*
					 * customers =
					 * customerService.findByDateAndCustomerId(startDate,
					 * endDate);
					 */ if (customers != null) {
						for (Customer customer : customers) {
							customer.setListOfALLDiscounts(null);
							customer.setAddresses(null);
							//customer.setAddress1(null);
							customer.setAddress2(null);
							customer.setAnniversaryDate(null);
							customer.setImage(null);
							customer.setCheckId(null);
							customer.setBirthDate(null);
							customer.setCountry(null);
							//customer.setFirstName(null);
							//customer.setCreatedDate(null);
							customer.setCustomerPosId(null);
							customer.setCustomerType(null);
							//customer.setEmailId(null);
							customer.setCustomerUid(null);
							customer.setEmailPosId(null);
							customer.setLastName(null);
							customer.setMerchantt(null);
							customer.setOrderCount(null);
							customer.setOrderType(null);
							customer.setMerchantt(null);

							if (startDate != null && endDate != null) {
								orderRs = orderService.getOrderByCustomerIdd(customer.getId(), startDate, endDate);
								if (orderRs != null && !orderRs.isEmpty()) {
									/*commaSepValueBuilder.append((orderRs.get(0).getCustomer().getId()));
									commaSepValueBuilder.append(",");*/
									customerIdList.add(orderRs.get(0).getCustomer().getId());

									// listOLists.add(orderRs);
								}
								for (OrderR order : orderRs) {
									order.setAuxTax(null);
									order.setConvenienceFee(null);
									
									if(filterPrice != null){
										if(order.getOrderPrice() > filterPrice){
											orderRrs.add(order);
										}
									}else{
										orderRrs.add(order);
									}
									
									order.setDeliveryFee(null);
									order.setFulfilled_on(null);
									order.setIsDefaults(null);
									order.setIsFutureOrder(null);
									order.setMerchant(null);
									order.setOrderAvgTime(null);
									order.setOrderItemViewVOs(null);
									order.setOrderNote(null);
									order.setOrderPosId(null);
									order.setOrders(null);
									//order.setOrderType(null);
									order.setPosPaymentId(null);
									order.setSubTotal(null);
									order.setTax(null);
									order.setTipAmount(null);
									order.setTipAmount(null);
									if(order.getCustomer()!=null) {
										order.getCustomer().setAddresses(null);
										order.getCustomer().setListOfALLDiscounts(null);
										order.getCustomer().setMerchantt(null);
										order.getCustomer().setVendor(null);
										}
								}
							} else {
								orderRs = orderService.findOrderDetailByCustomerId(customer.getId());
								for (OrderR order : orderRs) {
									
									orderRrs.add(order);
									order.setAuxTax(null);
									order.setConvenienceFee(null);
									order.setDeliveryFee(null);
									order.setFulfilled_on(null);
									order.setIsDefaults(null);
									order.setIsFutureOrder(null);
									order.setMerchant(null);
									order.setOrderAvgTime(null);
									order.setOrderItemViewVOs(null);
									order.setOrderNote(null);
									order.setOrderPosId(null);
									order.setOrders(null);
									//order.setOrderType(null);
									order.setPosPaymentId(null);
									order.setSubTotal(null);
									order.setTax(null);
									order.setTipAmount(null);
									
									if(order.getCustomer()!=null) {
										order.getCustomer().setAddresses(null);
										order.getCustomer().setListOfALLDiscounts(null);
										order.getCustomer().setMerchantt(null);
										order.getCustomer().setVendor(null);
										}
								}
								if (orderRs != null && !orderRs.isEmpty())
								{
									/*commaSepValueBuilder.append((orderRs.get(0).getCustomer().getId()));
									commaSepValueBuilder.append(",");*/
									customerIdList.add(orderRs.get(0).getCustomer().getId());
								
								}
							}
						}
					} else {
						customers = null;
					}
				}
			}

			List<OrderR> rs = new ArrayList<OrderR>();
			String customerId = commaSepValueBuilder.toString();
			LOGGER.info("customerId == "+customerId);
		
				if (customerIdList != null && requestType!=null && requestType.equals("no")) {
				//	rs = orderService.findCustomerNonTranscation(customerId,merchant.getId());
					rs = orderService.findCustomerNonTranscation(customerIdList,merchant.getId());
				} else if (customerId != null  && requestType!=null && requestType.equals("yes")) {
					rs = orderService.findCustomerTranscation(customerIdList,merchant.getId());
				}
				
				List<OrderR> filteredOrder = new ArrayList<OrderR>();
				for(OrderR order : rs){
					
					order.setAuxTax(null);
					order.setConvenienceFee(null);
					order.setDeliveryFee(null);
					order.setFulfilled_on(null);
					order.setIsDefaults(null);
					order.setIsFutureOrder(null);
					order.setMerchant(null);
					order.setOrderAvgTime(null);
					order.setOrderItemViewVOs(null);
					order.setOrderNote(null);
					order.setOrderPosId(null);
					order.setOrders(null);
					//order.setOrderType(null);
					order.setPosPaymentId(null);
					order.setSubTotal(null);
					order.setTax(null);
					order.setTipAmount(null);
					
					if(order.getCustomer()!=null) {
					order.getCustomer().setAddresses(null);
					order.getCustomer().setListOfALLDiscounts(null);
					order.getCustomer().setMerchantt(null);
					order.getCustomer().setVendor(null);
					}
					if(filterPrice != null && order.getOrderPrice() < filterPrice){
						filteredOrder.add(order);
						
						continue;
					}
					}
				if(filteredOrder != null && !filteredOrder.isEmpty()){
					rs.removeAll(filteredOrder);
				}
			
			if (orderRs != null) {
				orderObj.put("orderObj", orderRrs);
				orderObj.put("customers", customers);
				//orderObj.put("status", "true");
				orderObj.put("transaction", rs);
			} else {
				orderObj.put("status", "false");
			}
		} catch (Exception exception) {
			LOGGER.error("===============  CustomerController : Inside getMerchantDataAnalytics :: Exception  ============= " + exception);

			exception.printStackTrace();
		}
		return gson.toJson(orderObj);
	}
	
	
	@RequestMapping(value = "/getCustomerByPhoneNo", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getCustomerByPhoneNo(@RequestParam(required = true) String phonenumber)
			throws IOException {
		LOGGER.info("===============  CustomerController : Inside getCustomerByPhoneNo :: Start  ============= ");

		Map<Object, Object> customeObj = new HashMap<Object, Object>();
		List<Customer> customers = null;
		Gson gson = new Gson();
		if (phonenumber != null && !phonenumber.equals("") && !phonenumber.isEmpty()) {
			customers = customerService.findByPhoneNumber(phonenumber);
		}
		if (customers != null) {
			customeObj.put("customers", customers);
			customeObj.put("status", "true");
		} else {
			customeObj.put("status", "false");
		}
		LOGGER.info("===============  CustomerController : Inside getCustomerByPhoneNo :: End  ============= ");

		return gson.toJson(customeObj);
	}
	
	@RequestMapping(value = "/uploadOfflineCustomerByMerchantId", method = RequestMethod.POST)
	public @ResponseBody String uploadOfflineCustomerByMerchantId(MultipartHttpServletRequest request, HttpServletResponse response,@RequestParam("merchantId") Integer merchantId)
			throws Exception 
	 {
		LOGGER.info("Start: CustomerController uploadOfflineCustomerByExcelByAjax()");
		MultipartFile file = request.getFile("file");
		HttpSession session = request.getSession();
		Merchant  merchant =merchantService.findByMerchantId(merchantId);
		
		try 
		{
			if(merchant!=null)
			{
			ExcelToJsonConverterConfig config = new ExcelToJsonConverterConfig();
			File temp_file = new File(file.getOriginalFilename());
			try {
				file.transferTo(temp_file);
			} catch (IllegalStateException e1) {
				LOGGER.info("Start: CustomerController uploadOfflineCustomerByExcelByAjax() Exception : "+e1);

				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IOException e1) {
				LOGGER.info("Start: CustomerController uploadOfflineCustomerByExcelByAjax() Exception : "+e1);
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			config.setSourceFile(file.getOriginalFilename());
			String valid = config.valid();
			if (valid != null) {
				LOGGER.info("Start: CustomerController uploadOfflineCustomerByExcelByAjax() : valid : "+valid);
				LOGGER.info(valid);
				// help(options);
			}
 
			try {
				String excelResponse = importExcelService.saveOfflineCustomerByExcel(config, merchant, session);

				LOGGER.info("Start: CustomerController uploadOfflineCustomerByExcelByAjax() : Final Response  : "+excelResponse);
				LOGGER.info(excelResponse.toString());
				return excelResponse;

			} catch (InvalidFormatException e) {
				// TODO Auto-generated catch block
				LOGGER.info("Start: CustomerController uploadOfflineCustomerByExcelByAjax() Exception : "+e);

				MailSendUtil.sendExceptionByMail(e,environment);
				LOGGER.error("error: " + e.getMessage());
			} catch (IOException e) {
				LOGGER.info("Start: CustomerController uploadOfflineCustomerByExcelByAjax() Exception : "+e);

				// TODO Auto-generated catch block
				MailSendUtil.sendExceptionByMail(e,environment);
				LOGGER.error("error: " + e.getMessage());
			}
		}else return "Merchant not Exist";
		} catch (Exception e) {
			LOGGER.info("Start: CustomerController uploadOfflineCustomerByExcelByAjax() Exception : "+e);
			MailSendUtil.sendExceptionByMail(e,environment);
			LOGGER.error("error: " + e.getMessage());
			return "error";
		}
		return "error";
	}	
	  @RequestMapping(value = "/centralDbIntegration", method = RequestMethod.POST)
	  public @ResponseBody String centralDbIntegration(@RequestBody Customer customer) { 
		  try { 
			  LOGGER.info("===============  CustomerController : Inside centralDbIntegration :: Start  ============= ");

			  String customerEntityDTOJson = ProducerUtil.getCustomerEntityDTO(customer); 
			  LOGGER.info("Start: CustomerController centralDbIntegration() customerEntityDTOJson : "+customerEntityDTOJson);
			  String customerUid=ProducerUtil.getCustomerUid(customerEntityDTOJson,environment);
			  if(customer.getLoyalityProgram()!=null && customer.getLoyalityProgram()==1) {
				  LOGGER.info("Start: CustomerController centralDbIntegration() customerEntityDTOJson : "+customerEntityDTOJson);
				  ProducerUtil.createCustomerIntoFreekwent(customerEntityDTOJson,environment);
				  }
			  return customerUid; 
			  }catch (Exception e) { 
				  LOGGER.error(
						"===============  CustomerController : Inside centralDbIntegration :: Exception  ============= " + e);

				  if (e != null) {
					  MailSendUtil.sendExceptionByMail(e,environment); 
					  } 
				  LOGGER.error("error: " + e.getMessage());
			  } 
		    return null; 
		  	}
	  
	  @RequestMapping(value = "/user")
		public String user(@ModelAttribute("Customer") Customer customer) {
			
			return "user";
		}
	  
	  @RequestMapping(value = "/addUser")
		public String addUser(@ModelAttribute("Customer") Customer customer) {
			
			return "addUser";
		}
		
		@RequestMapping(value = "/getUserByMerchantId", method = RequestMethod.GET, produces = "application/json")
		public @ResponseBody String getUserByMerchantId(HttpServletRequest request) throws IOException {
			LOGGER.info("===============  CustomerController : Inside getUserByMerchantId :: Start  ============= ");

			HttpSession session = request.getSession(false);
			Merchant merchant = (Merchant) session.getAttribute("merchant");
			
			String jsonOutput = "";
			if (merchant != null) {
				LOGGER.info("===== CustomerController : Inside getUserByMerchantId :: merchant  == " + merchant.getId());

				jsonOutput = customerService.showUserByMerchantId(merchant);
			}
			LOGGER.info("===============  CustomerController : Inside getUserByMerchantId :: End  ============= ");

			return jsonOutput;
		}
		
		
		@RequestMapping(value = "/editUser", method = RequestMethod.GET)
		public String viewEditUser(@ModelAttribute("Customer") Customer customer, ModelMap model,
				HttpServletRequest request, @RequestParam(required = false) int userId) {
			try {
				LOGGER.info(
						"===============  CustomerController : Inside viewEditUser :: Start  ============= userId "
								+ userId);

				HttpSession session = request.getSession(false);
				if (session != null) {
					Merchant merchant = (Merchant) session.getAttribute("merchant");
					if (merchant != null) {
						Customer result = customerService.findByCustomerId(userId);

						model.addAttribute("customer", result);

						if (merchant.getOwner() != null && merchant.getOwner().getPos() != null
								&& merchant.getOwner().getPos().getPosId() != null) {
							model.addAttribute("merchantType", merchant.getOwner().getPos().getPosId());
						}
					}
				} else {
					return "redirect:" + environment.getProperty("BASE_URL") + "/support";
				}
			} catch (Exception e) {
				if (e != null) {
					MailSendUtil.sendExceptionByMail(e,environment);
					LOGGER.error("error: " + e.getMessage());
					LOGGER.info("===============  CustomerController : Inside viewEditUser :: End  ============= ");

					return "redirect:" + environment.getProperty("BASE_URL") + "/support";
				}
			}
			return "editUser";
		}

		
		@RequestMapping(value = "/updateUserData", method = RequestMethod.POST)
		public String updateUserData(@ModelAttribute("Customer") Customer customer, 
				HttpServletRequest request,ModelMap model) {
			try {
				HttpSession session = request.getSession(false);
				if (session != null) {
					Merchant merchant = (Merchant) session.getAttribute("merchant");
					if (merchant != null) {
				customerService.updateCustomerOnAdmin(customer , merchant);
					}
				}

		} catch (Exception e) {
				if (e != null) {
					MailSendUtil.sendExceptionByMail(e,environment);
					LOGGER.error("error: " + e.getMessage());
					LOGGER.info("===============  CustomerController : Inside updateUserData :: Exception  ============= "
							+ e);

					return "redirect:" + environment.getProperty("BASE_URL") + "/support";
				}
		}
		LOGGER.info("===============  AdminHomeController : Inside updateUserData :: End  ============= ");

			return "user";
		}
		
		@RequestMapping(value = "/addNewUser", method = RequestMethod.POST)
		public String addNewUser(@ModelAttribute("Customer") Customer customer,
				HttpServletRequest request,ModelMap model) {
			try {
				HttpSession session = request.getSession(false);
				if (session != null) {
					Merchant merchant = (Merchant) session.getAttribute("merchant");
					
					if (merchant != null) {
				String result = customerService.saveCustomerOnAdmin(customer ,merchant);
				model.addAttribute("result", result);
				if(!result.equals("")) {
					return "addUser";
				}
			   
			}else
				return "redirect:" + environment.getProperty("BASE_URL") + "/support";	
		}

		} catch (Exception e) {
				if (e != null) {
					MailSendUtil.sendExceptionByMail(e,environment);
					LOGGER.error("error: " + e.getMessage());
					LOGGER.info("===============  CustomerController : Inside updateUserData :: Exception  ============= "
							+ e);

					return "redirect:" + environment.getProperty("BASE_URL") + "/support";
				}
		}
		LOGGER.info("===============  AdminHomeController : Inside updateUserData :: End  ============= ");

		return "redirect:" + environment.getProperty("BASE_URL") + "/user";
		}
		
		@RequestMapping(value = "/getCustomerById", method = RequestMethod.GET, produces = "application/json")
		public @ResponseBody String getCustomerById(@RequestParam(required = true) Integer id)
				throws IOException {
			LOGGER.info("===============  CustomerController : Inside getCustomerById :: Start  ============= ");

			Customer customer = new Customer();
			Gson gson = new Gson();
			if (id != null) {
				customer = customerRepo.findOne(id);
				customer.setAddresses(null);
				customer.setMerchantt(null);
				customer.setVendor(null);
			}
			
			LOGGER.info("===============  CustomerController : Inside getCustomerById :: End  ============= ");

			return gson.toJson(customer);
		}
		
		@RequestMapping(value = "/deleteUser", method = RequestMethod.GET, produces = "application/json")
		public String deleteUser(@RequestParam(required = true) Integer UserId)
				throws IOException {
			LOGGER.info("===============  CustomerController : Inside getCustomerById :: Start  ============= ");

			Customer customer = customerRepo.findOne(UserId);
			Gson gson = new Gson();
			if (customer!= null) {
				customer.setCustomerType("");
				customerRepo.save(customer);
			}
			LOGGER.info("===============  CustomerController : Inside getCustomerById :: End  ============= ");
			return "user";
		}
		
	@RequestMapping(value = "/getKritiqDataByMerchantUId", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody KritiqMerchantVO getKritiqDataByMerchantUId(HttpServletRequest request,
			@RequestParam(required = true) String merchantUid, @RequestParam(required = true) String startDate,
			@RequestParam(required = true) String endDate) {
		KritiqMerchantVO kritiq = new KritiqMerchantVO();
		try {
			Merchant merchant = merchantService.findByMerchantUid(merchantUid);
			if (merchant != null) {
				java.util.Date startDate1 = null;
				java.util.Date endDate1 = null;
				java.util.Date endDateforMailSent = null;
				

					SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
					Calendar cal = Calendar.getInstance();
					
					if (startDate != null && endDate != null 
							&& !startDate.equalsIgnoreCase("NC") && !endDate.equalsIgnoreCase("NC")) {
					cal.setTime(dateFormat.parse(endDate));
					startDate1 = new SimpleDateFormat(IConstant.YYYYMMDD).parse(startDate);				
					}else {
						cal.setTime(new Date());
						startDate1 = new SimpleDateFormat(IConstant.YYYYMMDD).parse(dateFormat.format(new Date()));
					}
					
					cal.add(Calendar.DATE, 1);
					endDateforMailSent = new SimpleDateFormat(IConstant.YYYYMMDD)
							.parse(dateFormat.format(cal.getTime()));
					endDate1 = new SimpleDateFormat(IConstant.YYYYMMDD).parse(dateFormat.format(cal.getTime()));
					LOGGER.info(
							"===============  CustomerController : Inside getKritiqDataByMerchantId :: Start  ============= ");
					Double avgRate = customerFeedbackRepository.findAverageRating(merchant.getId(), startDate1,endDate1);
					Integer noOfMailSent = customerFeedbackRepository.findMailSentCount(merchant.getId(), startDate1,endDateforMailSent);
					Integer noOfResponse = customerFeedbackRepository.findResponseCount(merchant.getId(), startDate1,endDate1);

					kritiq.setAvgRate(avgRate);
					kritiq.setNoOfMailSent(noOfMailSent);
					kritiq.setNoOfResponse(noOfResponse);
			}
		} catch (Exception exception) {
			LOGGER.error(
					"===============  CustomerController : Inside getKritiqDataByMerchantId :: Exception  ============= "
							+ exception);
			exception.printStackTrace();
		}
		LOGGER.info("===============  CustomerController : Inside getKritiqDataByMerchantId :: End  ============= ");

		return kritiq;
	}
	
	@RequestMapping(value = "/getKritiqReviewDataByCustomerFeedBackIds", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody List<Map<String, Object>> getKritiqReviewDataByCustomerFeedBackIds(HttpServletRequest request,
			@RequestParam(required = true) String customerFeedBackIds) {
		List<Map<String, Object>> kritiqCustomerVOs = null;
		try {
			LOGGER.info("===============  CustomerController : Inside getKritiqReviewDataByCustomerFeedBackIds :: Start  ============= ");
			Gson gson = new Gson();
			List<String> idList1 = Arrays.asList(customerFeedBackIds.split("\\s*,\\s*"));
			//String feedBackIds = gson.toJson(idList1);
			TypeToken<List<String>> token = new TypeToken<List<String>>() {};
			List<String> idList2 = gson.fromJson(idList1.toString(), token.getType());
			
			List<Integer> intIdList = new ArrayList<Integer>();
			for(String id : idList2) {
				intIdList.add(Integer.parseInt(id));
			}
			kritiqCustomerVOs = kritiqService.getKritiqReviewData(intIdList);

		} catch (Exception exception) {
			LOGGER.error("===============  CustomerController : Inside getKritiqReviewDataByCustomerFeedBackIds :: Exception  ============= "
							+ exception);
			exception.printStackTrace();
		}
		LOGGER.info("===============  CustomerController : Inside getKritiqReviewDataByCustomerFeedBackIds :: End  ============= ");

		return kritiqCustomerVOs;
	}
	
	@RequestMapping(value = "/getKritiqReviewIdsByMerchantUId", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody List<String> getKritiqReviewIdsByMerchantUId(HttpServletRequest request,
			@RequestParam(required = true) String merchantUid, @RequestParam(required = true) String startDate,
			@RequestParam(required = true) String endDate) {
		List<String> kritiqCustomerVOs = null;
		try {
			LOGGER.info("===============  CustomerController : Inside getKritiqReviewDataByMerchantUId :: Start  ============= ");

			Merchant merchant = merchantService.findByMerchantUid(merchantUid);
			if (merchant != null) {
				boolean status = (startDate != null && endDate != null 
						&& !startDate.equalsIgnoreCase("NC") && !endDate.equalsIgnoreCase("NC"));
					if (!status) {
						DateFormat dateFormat = new SimpleDateFormat(IConstant.YYYYMMDD);

						Date today = null;
						if(merchant!=null && merchant.getTimeZone() !=null 
								&& merchant.getTimeZone().getTimeZoneCode()!=null) {
						today = DateUtil.getCurrentDateForTimeZonee(merchant.getTimeZone().getTimeZoneCode());
						}
						else
						 today = new Date();
						
						Calendar cal = new GregorianCalendar();
						cal.setTime(today);
						cal.add(Calendar.DAY_OF_MONTH, -29);
						Date today30 = cal.getTime();
						endDate = dateFormat.format(today);
						startDate = dateFormat.format(today30);
				}
					
				 kritiqCustomerVOs= kritiqService.getKritiqIdsReviewDataByMerchantId(merchant.getId(),
										startDate, endDate);
			
					LOGGER.info("===============  AdminHomeController : Inside kritiqCustomerDetail :: Ends  ============= ");
				}
		} catch (Exception exception) {
			LOGGER.error(
					"===============  CustomerController : Inside getKritiqDataByMerchantId :: Exception  ============= "
							+ exception);
			exception.printStackTrace();
		}
		LOGGER.info("===============  CustomerController : Inside getKritiqDataByMerchantId :: End  ============= ");

		return kritiqCustomerVOs;
	}
	
	@RequestMapping(value = "/getKritiqReviewDataByByCustomerFeedBackId", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody KritiqCustomerVO getKritiqReviewDataByByCustomerFeedBackId(HttpServletRequest request,
			@RequestParam(required = true) String customerFeedBackId) {
		KritiqCustomerVO kritiq = new KritiqCustomerVO();
		try {
			kritiq = kritiqService.getKritiqReviewDataByFeedbackId(Integer.parseInt(customerFeedBackId));
		} catch (Exception exception) {
			LOGGER.error("===============  CustomerController : Inside getKritiqDataByMerchantId :: Exception  ============= "
							+ exception);
			exception.printStackTrace();
		}
		LOGGER.info("===============  CustomerController : Inside getKritiqDataByMerchantId :: End  ============= ");

		return kritiq;
	}
	
	@RequestMapping(value = "/saveResponseFrom4QToCustomerFeedBack", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody void saveResponseFrom4QToCustomerFeedBack(HttpServletRequest request,
			@RequestBody(required = true) String customerFeedBack) {
		try {
			Gson gson = new Gson();
			TypeToken<Map<String, Object>> token = new TypeToken<Map<String, Object>>() {};
			Map<String, Object> customerFeedBackResponse = gson.fromJson(customerFeedBack, token.getType());
			if(customerFeedBackResponse.size()>0) {
			kritiqService.saveResponseFrom4Q(customerFeedBackResponse);
			}
		} catch (Exception exception) {
			LOGGER.error("===============  CustomerController : Inside saveResponseFrom4QToCustomerFeedBack :: Exception  ============= "
							+ exception);
			exception.printStackTrace();
		}
		LOGGER.info("===============  CustomerController : Inside saveResponseFrom4QToCustomerFeedBack :: End  ============= ");

	}
	
	@RequestMapping(value = "/updateResponseInCustomerFeedBack", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody void updateResponseInCustomerFeedBack(HttpServletRequest request,
			@RequestBody(required = true) String feedBackId, @RequestBody(required = true) String response) {
		try {
			
			if(!feedBackId.isEmpty()) {
			kritiqService.updateResponseInCustomerFeedBack(feedBackId, response);;
			}
		} catch (Exception exception) {
			LOGGER.error("===============  CustomerController : Inside updateResponseInCustomerFeedBack :: Exception  ============= "
							+ exception);
			LOGGER.error("error: " + exception.getMessage());
		}
		LOGGER.info("===============  CustomerController : Inside updateResponseInCustomerFeedBack :: End  ============= ");

	}
	
	@RequestMapping(value = "/getKrtiqLowRating", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody List<Map<String, Object>> getKrtiqLowRating(HttpServletRequest request, @RequestParam(required = true) String startDate,
			@RequestParam(required = true) String endDate) {
		List<Map<String, Object>> kritiqCustomerVOs = null;
		try {
			LOGGER.info("===============  CustomerController : Inside getKrtiqLowRating :: Start  ============= ");
					
				 kritiqCustomerVOs= kritiqService.getKrtiqLowRating(startDate, endDate);
			
					LOGGER.info("===============  CustomerController : Inside getKrtiqLowRating :: Ends  ============= ");
				
		} catch (Exception exception) {
			LOGGER.error(
					"===============  CustomerController : Inside getKrtiqLowRating :: Exception  ============= "
							+ exception);
			LOGGER.error("error: " + exception.getMessage());
		}
		LOGGER.info("===============  CustomerController : Inside getKrtiqLowRating :: End  ============= ");

		return kritiqCustomerVOs;
	}
}
