package com.foodkonnekt.controller;

import com.foodkonnekt.model.Merchant;
import com.foodkonnekt.model.MerchantSocialPlatform;
import com.foodkonnekt.repository.UberRepository;
import com.foodkonnekt.service.MerchantSocialPlatformService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
public class MerchantSocialPlatformController {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(MerchantSocialPlatformController.class);
    
    @Autowired
    private Environment environment;
    
    @Autowired
    private UberRepository uberRepository;
    
    @Autowired
    private MerchantSocialPlatformService merchantSocialPlatformService;
    
    @RequestMapping(value = "/merchantSocialPlatform", method = RequestMethod.GET)
    public String uberstore(@ModelAttribute("merchantSocialPlatform") MerchantSocialPlatform merchantSocialPlatform,
                            ModelMap model, HttpServletRequest request) {
        try {
            HttpSession session = request.getSession();
            MerchantSocialPlatform merchantSocialPlatformFromDb = null;
            if (session != null) {
                Merchant merchant = (Merchant) session.getAttribute("merchant");
                
                if (merchant != null) {
                    merchantSocialPlatformFromDb = merchantSocialPlatformService.findByMerchantId(merchant.getId());
                }
            }
            if (merchantSocialPlatformFromDb != null) {
                model.addAttribute("merchantSocialPlatform", merchantSocialPlatformFromDb);
            }
            
        } catch (Exception e) {
            LOGGER.info("Exception in MerchantSocialPlatformController : merchantSocialPlatform : " + e);
            return "redirect:" + environment.getProperty("BASE_URL") + "/support";
        }
        return "merchantSocialPlatform";
    }
    
    @RequestMapping(value = "/merchantSocialPlatform", method = RequestMethod.POST)
    public String addNewUberStore(
            @ModelAttribute("merchantSocialPlatform") MerchantSocialPlatform merchantSocialPlatform,
            ModelMap model, HttpServletRequest request) {
        HttpSession session = request.getSession();
        if (session != null) {
            Merchant merchant = (Merchant) session.getAttribute("merchant");
            MerchantSocialPlatform merchantSocialPlatformFromDb = null;
            if (merchant != null && merchantSocialPlatform != null) {
                merchantSocialPlatformFromDb = merchantSocialPlatformService.findByMerchantId(merchant.getId());
                if (merchantSocialPlatformFromDb != null) {
                    merchantSocialPlatformFromDb.setGascript(merchantSocialPlatform.getGascript());
                } else {
                    merchantSocialPlatformFromDb = merchantSocialPlatform;
                }
                merchantSocialPlatformService.saveAndFlush(merchantSocialPlatformFromDb);
                model.addAttribute("merchantSocialPlatformResponse", "Information has been saved successfully");
                return "redirect:" + environment.getProperty("BASE_URL") + "/merchantSocialPlatform";
            }
        }
        return "redirect:" + environment.getProperty("BASE_URL") + "/support";
    }
    
}
