package com.foodkonnekt.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.foodkonnekt.model.Clover;
import com.foodkonnekt.model.Merchant;
import com.foodkonnekt.service.CloverService;
import com.foodkonnekt.service.MigrationService;
import com.foodkonnekt.util.CloverUrlUtil;
import com.foodkonnekt.util.IConstant;
import com.foodkonnekt.util.UrlConstant;

@Controller
public class MigrationController {
	
	@Autowired
    private Environment environment;
	
	@Autowired
    private MigrationService migrationService;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(MigrationController.class);

	@RequestMapping(value = "/migratPhpMerchant", method = RequestMethod.GET)
    @ResponseBody
    public String migratPhpMerchant(@RequestParam(required = true) String merchantId,@RequestParam(required = true)String startDate,@RequestParam(required = true) String endDate) {
        
		
        return migrationService.migrateCustomer(merchantId,startDate,endDate);
    }
	
	@RequestMapping(value = "/updateWebhookData", method = RequestMethod.GET)
    @ResponseBody
    public String updateWebhookData() {
		LOGGER.info("===============  MigrationController : Inside updateWebhookData :: Start  ============= ");

		Clover clover= new Clover();
    	clover.setAuthToken("3f5cbb43-ad57-bd51-610c-214ad3e2bc28");
    	clover.setMerchantId("VAHJVXB36EQ2C");
    	clover.setInstantUrl(IConstant.CLOVER_INSTANCE_URL);
    	clover.setUrl(environment.getProperty("CLOVER_URL"));
    	String json=CloverUrlUtil.getAllItem(clover, null);
    	createItemObject(json,clover.getMerchantId(),environment);
		LOGGER.info("===============  MigrationController : Inside updateWebhookData :: End  ============= ");

        return "200";
    }
	
	  public static void createItemObject(String itemJson,String merchantId,Environment environment) {
	        try {
	        	LOGGER.info("===============  MigrationController : Inside createItemObject :: Start  ============= ");

	            JSONObject jsonObject = new JSONObject(itemJson);
	            JSONArray jsonArray = jsonObject.getJSONArray("elements");
	            for (int i = 0; i < jsonArray.length(); i++) {
	                JSONObject categoryItem = jsonArray.getJSONObject(i);
	                String itemPosId=categoryItem.getString("id");
	            	HttpPost postRequest = new HttpPost(environment.getProperty("BASE_URL")+"/webHooks");
	            	String request="{\"appId\":\"X476CXX3HM1Q8\",\"merchants\":{\""+merchantId+"\":[{\"objectId\":\"I:"+itemPosId+"\",\"type\":\"UPDATE\",\"ts\":1483863300411}]}} ";
	            	convertToStringJson(postRequest,request);
	                //convertToStringJson();
	            }
	        } catch (Exception e) {
	            LOGGER.info("MigrationController.createItemObject() : ERROR" + e);
	            LOGGER.error("error: " + e.getMessage());
	        }
	    }
	    public static String convertToStringJson(HttpPost postRequest, String customerJson) {
	    	
	        StringBuilder responseBuilder = new StringBuilder();
	        try {
	        	LOGGER.info("===============  MigrationController : Inside convertToStringJson :: Start  ============= ");

	            HttpClient httpClient = HttpClientBuilder.create().build();
	            StringEntity input = new StringEntity(customerJson);
	            input.setContentType("application/json");
	            postRequest.setEntity(input);
	            HttpResponse response = httpClient.execute(postRequest);
	           
	            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
	            String line = "";
	            while ((line = rd.readLine()) != null) {
	                responseBuilder.append(line);
	            }
	            LOGGER.info("responseBuilder == "+responseBuilder.toString());
	        } catch (MalformedURLException e) {
	            LOGGER.info("MigrationController.convertToStringJson() : ERROR" + e);
	            LOGGER.error("error: " + e.getMessage());
	        } catch (IOException e) {
	            LOGGER.info("MigrationController.convertToStringJson() : ERROR" + e);
	            LOGGER.error("error: " + e.getMessage());
	        }
	        LOGGER.info("===============  MigrationController : Inside convertToStringJson :: End  ============= ");

	        return responseBuilder.toString();
	    }

}
