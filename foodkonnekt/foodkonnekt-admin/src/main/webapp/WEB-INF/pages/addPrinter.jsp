<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@page session="true"%>
<!doctype html>
<html class="no-js" lang="en">
<head>
<title>FoodKonnekt | Add Products</title>
<link href="resources/css/bootstrap.min.css" rel="stylesheet"
	type="text/css" />
<link href="resources/css/bootstrap-multiselect.css" rel="stylesheet"
	type="text/css" />
<!--CALLING STYLESHEET STYE.CSS-->
<link rel="stylesheet" href="resources/css/style.css">
<!--CALLING STYLESHEET STYLE.CSS-->

<!--CALLING GOOGLE FONT OPEN SANS-->
<link
	href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic'
	rel='stylesheet' type='text/css'>
<!--CALLING GOOGLE FONT OPEN SANS-->

<!--CALLING FONT AWESOME-->
<link rel="stylesheet" href="resources/css/font-awesome.css">
<!--CALLING FONT AWESOME-->

<!--OPENS DIALOG BOX-->
<link rel="stylesheet" type="text/css"
	href="resources/css/dialog-box/component.css" />
<!--OPENS DIALOG BOX-->

<!--CALLING PRODUCTS TABS-->
<link rel='stylesheet' type='text/css'
	href='resources/css/products-tabs/opentabby.css' />
<!--CALLING PRODUCTS TABS-->
<!-- <link rel="stylesheet" type="text/css"
	href="https://cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css"> -->

<style type="text/css">
.body {
	overflow: scroll !important;
}



.pj-preloader {
	display: none;
	position: absolute;
	height: 383px;;
	width: 940px;
	background: url("resources/img/spinner.gif") no-repeat scroll center
		center rgba(153, 153, 153, 0.3);
	z-index: 9999;
	left: 260;
	position: absolute;
	top: 224px;
}
</style>

</head>
<body>
	<div id="page-container">
		<div class="foodkonnekt inventory">
			<div class="inner-container">
				<div class="max-row">

					<header id="page-header">
						<div class="inner-header">
							<div class="row">

								<div class="logo">
									<a href="adminHome" title="FoodKonnekt Dashboard" class="logo"><img
										src="resources/img/foodkonnekt-logo.png"></a>
								</div>
								<!--.logo-->
								<%@ include file="adminHeader.jsp"%>

							</div>
							<!--.row-->
						</div>
						<!--.inner-header-->
					</header>
					<!--#page-header-->

           		<div id="page-content">
						<div class="outer-container">

							<div class="row">
								<div class="content-inner-container">

									<%@ include file="leftMenu.jsp"%>

									<div class="right-content-container">
										<div class="right-content-inner-container">

											<div class="content-header">
												<div class="all-header-title"></div>
												<!--.header-title-->
											</div>
											<!--.content-header-->
											
											
											
											
											
	       <p style="margin:2em 0 0.8em 1em; font-weight:bold;">Please choose a printer :</p>
	       
              <form:form method="POST"  action="/foodkonnekt-admin/getPrinter" modelAttribute="printer" >       
                  <form:select path="printerId" items="${printer.printers}"  style="margin-left:1em;"/> 
                  <form:hidden path="id" value="${printer.id}" />
                  <input type="submit" value="Save" class="btn btn-primary" style="margin-left:1em;"/>
             </form:form>       
             
               <div style="margin:2em 0 0.8em 1em;">   ${msg} </div>
               
                                
							</div>
							<!--.row-->

						</div>
						<!--.outer-container-->

					</div>
					<!--#page-content-->
					<%@ include file="adminFooter.jsp"%>
				</div>
				<!--.max-row-->
			</div>
			<!--.inner-container-->
		</div>
		<!--.foodkonnekt .dashboard-->
	</div>
	<!--#page-container-->
</body>
</html>