<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="true"%>
<!doctype html>
<html class="no-js" lang="en">
  <head>
  
    <title>FoodKonnekt | Dashboard</title>
    <!--CALLING STYLESHEET STYE.CSS-->
    <link rel="stylesheet" href="resources/css/style.css">
    <!--CALLING STYLESHEET STYLE.CSS-->
    
    <!--CALLING GOOGLE FONT OPEN SANS-->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    <!--CALLING GOOGLE FONT OPEN SANS-->
    
    <!--CALLING FONT AWESOME-->
    <link rel="stylesheet" href="resources/css/font-awesome.css">
    <!--CALLING FONT AWESOME-->
    
    <!--CALLING FILTER OPTIONS-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
     <script src="resources/js/checkall/jquery.checkall.js"></script>
    
      <!--OPENS DIALOG BOX-->
    <link rel="stylesheet" type="text/css" href="resources/css/dialog-box/component.css" />
    <!--OPENS DIALOG BOX-->
    
    <!--ACCORDION FOR MENU-->
    <script src="resources/js/accordion/paccordion.js"></script>
    <!--ACCORDION FOR MENU-->
     <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css">
     <script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
     <script type="text/javascript">
          $(document).ready(function() {
            var table = $('#example').DataTable();
            $(".searchq").keyup(function() {
            	 table.search( this.value).draw();
           } );
          });
          $(document).ready(function() {
            $('input[type=search]').each(function() {
              $(this).attr('placeholder', "Search zones");
            });
          });
        </script>
    <style type="text/css">
        div#example_paginate {
          display: block;
        }
        
        div#example_filter {
          display: block;
        }
        
        div#example_length {
          display: block;
        }
        
        input[type="search"] {
          max-width: 300px;
          width: 87%;
          outline: 0;
          border: 1px solid rgb(169, 169, 169);
          padding: 11px 10px;
          border-radius: 6px;
          margin-bottom: 7px;
          placeholder: Search Items;
          display:none;
        }
</style>
    <!--CALLING FILTER OPTIONS-->
    <!--OPENS DIALOG BOX-->
    <link rel="stylesheet" type="text/css" href="resources/css/dialog-box/component.css" />
    <!--OPENS DIALOG BOX-->
  </head>
  <body>
    <div id="page-container">
        <div class="foodkonnekt merchant">
            <div class="inner-container">
                <div class="max-row">
                    
                    <header id="page-header">
                        <div class="inner-header">
                            <div class="row">
                                <div class="logo">
                                     <a href="adminHome" title="FoodKonnekt Dashboard" class="logo"><img src="resources/img/foodkonnekt-logo.png"></a>
                                </div><!--.logo-->
                                   <%@ include file="adminHeader.jsp"%>
                            </div><!--.row-->
                        </div><!--.inner-header-->
                    </header><!--#page-header-->
                    
                    <div id="page-content">
                        <div class="outer-container">
                            <div class="row">
                                <div class="content-inner-container">
                                         <%@ include file="leftMenu.jsp"%>                                
                                    <div class="right-content-container">
                                        <div class="right-content-inner-container">
                                        
                                            <div class="content-header">
                                                <div class="all-header-title">
                                                </div><!--.header-title-->
                                                <div class="content-header-dropdown">
                                                </div><!--.content-header-dropdown-->
                                            </div><!--.content-header-->
                                            
                                            <div class="merchant-page-data">
                                                <div class="merchant-actions-outbound">
                                                    <div class="merchat-coupons-container">
                                                        <%@ include file="adminMerchantMenu.jsp"%>
                                                        
                                                        <div class="delivery-zones-content-container">
                                                            <div class="button left">
                                                                <a href="addDeliveryZone?locationAddress=${merchant.name} ${location.address1}${location.city}${location.zip}" id="addZone">Add Zone</a>
                                                                <a href="deliveryTimings"  id="deliveryTime">Set Delivery Time</a>
                                                            </div>
                                                            
                                                            <div class="search-container">

																				<div class="only-search-elements">
																					<label>Search</label> <input type="text"
																						placeholder="Search Zones" id="search-inventory"
																						class="searchq"> <input type="button"
																						value="Search">
																				</div>

																			</div>
																			<script type="text/javascript">
																				$(".searchq").keyup(
																								function() {
																									var val = $(this).val();
																									$('input[type="search"]').val(val);
																									$('input[type="search"]').trigger("keyup");
																								});
																			</script>
                                                            <div class="clearfix"></div>
                                                            
                                                            <div class="delivery-zone-container">
                                                                <div class="delivery-zone-container-form">
                                                               <table width="100%" cellpadding="0" cellspacing="0" id="example">
                                                                                        <thead>
                                                                                            <tr>
                                                                                                <th>Location</th>
                                                                                                <th>Name</th>
                                                                                                <th>Avg. Time</th>
                                                                                                <c:if test="${zones[0].zoneType !=null}">
                                                                                                <c:if test="${zones[0].zoneType == '0'}">
                                                                                                <th>Miles</th>
                                                                                                 </c:if>
                                                                                                 </c:if>
                                                                                                <th>Min Order Amt</th>
                                                                                                <th>Delivery Fee</th>
                                                                                                <th>Status</th>
                                                                                                <th>Action</th>
                                                                                                <!-- <th>Delete</th> -->
                                                                                                <th>type</th>
                                                                                                
                                                                                                
                                                                                            </tr>
                                                                                        </thead>
                                                                                        <tbody>
                                                                                            <c:forEach items="${zones}" var="z" varStatus="status">
                                                                                            <%-- <c:if test="${z.isDeleted==0}"> --%>
                                                                                                <tr>
                                                                                                    <td>${z.address.address1}${z.address.address2}${z.address.city}${z.address.country}${z.address.zip}</td>
                                                                                                    <td>${z.zoneName}</td>
                                                                                                    <td>${z.avgDeliveryTime}</td>
                                                                                                    <c:if test="${z.zoneType=='0'}">
                                                                                                    <td>${z.zoneDistance}</td>
                                                                                                     </c:if>
                                                                                                    <td>${z.minDollarAmount}</td>
                                                                                                    
                                                                                                    <td>${z.deliveryFee}</td>
                                                                                                     <c:choose>
                                                                                                    <c:when test="${z.zoneStatus==0}">
                                                                                                      <td>Active</td>
                                                                                                    </c:when>    
                                                                                                    <c:otherwise>
                                                                                                     <td>InActive</td>
                                                                                                    </c:otherwise>
                                                                                                </c:choose>
                                                                                                    <td><a href="updateDelivery?deliveryZoneId=${z.id}" class="edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                                                                                    </td>
                                                                                                    <%-- <td><a href="deleteDeliveryZone?zoneId=${z.id}">Delete</a></td> --%>
                                                                                               
                                                                                               <c:choose>
                                                                                                    <c:when test="${z.zoneType=='map'}">
                                                                                                      <td>map</td>
                                                                                                    </c:when>    
                                                                                                    <c:otherwise>
                                                                                                     <td>radius</td>
                                                                                                    </c:otherwise>
                                                                                                    </c:choose>
                                                                                             
                                                                                                </tr>
                                                                                               <%--  </c:if> --%>
                                                                                            </c:forEach>
                                                                                        </tbody>
                                                                                    </table>
                                                                    
                                                                                            <label id="errorMessage" style="color: red;"></label>
                                                                                            <div class="display-timings" id="timings" style=" display: none">
                                                                                            <div class="sd-modifiers-limit-label">
																								<label>Display Items On Select Days : </label>
																							</div><br><br>
																							<!--.sd-modifiers-limit-label-->

																							<div class="sd-modifiers-limit-fields">
																								<div class="sd-modifiers-limit-yes-no">
																									Yes
																									<input type="radio" name="allowCategoryTimings"
																										id="itemTimingsYes" value="1" onclick="yes()"/>
																									No
																									<input type="radio" name="allowCategoryTimings"
																										id="itemTimingsNo" value="0" onclick="no()" />
																								</div></div><br>
                 
                 <div class="pj-loader-4" style="display: none">
				<img src="resources/img/load2.gif"
					style="width: 82px; margin-left: auto; margin-top: auto;">
			    </div>                                                   
                <div class="categoryTimingPopup" style=" display: none;margin-top: 1%;margin-left: 1%;">
				<div class="accordion-popup-quantity">
					<label
						style="font-size: 0.77778rem; font-weight: normal; line-height: 1.5;">Select Days</label>
						
					<div id="weekDays">
						<div class="innerdays">
				    <input type="checkbox" value="Sunday" id="Sunday" name="days">Sunday</input>
					<input type="checkbox" value="Monday" id="Monday" name="days">Monday</input>
					<input type="checkbox" value="Tuesday" id="Tuesday" name="days">Tuesday</input>
					<input type="checkbox" value="Wednesday" id="Wednesday" name="days">Wednesday</input> 
					<input type="checkbox" value="Thursday" id="Thursday" name="days">Thursday</input> <br>
					<input type="checkbox" value="Friday" id="Friday" name="days">Friday</input>
					<input type="checkbox" value="Saturday" id="Saturday" name="days">Saturday</input><br>
					</div></div>
					<br> 
					<label
						style="font-size: 0.77778rem; font-weight: normal; line-height: 1.5;">Select
						Timing</label> <select name="startTime" style="width: 38%;" id="startTime">
						<c:forEach items="${times}" var="tm">

							<option value="${tm}">${tm}</option>

						</c:forEach>
					</select>
					<to>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;to&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</to>
					<select path="endTime" style="width: 38%;" id="endTime">
						<c:forEach items="${times}" var="tm">

							<option value="${tm}">${tm}</option>

						</c:forEach>
					</select>
					</quantity>
				</div>
				
				
				</div>
				
				<div class="confirmModal_footer" style="margin-top: 2%;margin-left: 1%;" >
			    <button type="button" id="yesBtn" class="btn btn-primary"
				onclick="yesButton()">Save</button>
			    <button type="button" class="btn btn-primary"
				onclick="cancelButton()">Cancel</button>
		        </div> 
				<!--.accordion-popup-quantity-->


			</div>
                                                                    
                                                                    
                                                                    
                                                                </div><!--.delivery-zone-container-form-->
                                                            </div><!--.delivery-zone-container-->
                                                            
                                                        </div><!--.coupons-content-container-->
                                                    </div><!--.merchat-coupons-container-->
                                                </div><!--.merchant-actions-outbound-->
                                            </div><!--.merchant-page-data-->
                                            
                                        </div><!--.right-content-inner-container-->
                                    </div><!--.right-content-container-->
                                </div><!--.content-inner-container-->
                            </div><!--.row-->
                            
                        </div><!--.outer-container-->
                    </div><!--#page-content-->
                             <%@ include file="adminFooter.jsp"%>                    
                </div><!--.max-row-->
            </div><!--.inner-container-->
        </div><!--.foodkonnekt .dashboard-->
    </div><!--#page-container-->
    
     <!--OPENS DIALOG BOX-->
    <script src="resources/js/dialog-box/classie.js"></script>
    <script src="resources/js/dialog-box/modalEffects.js"></script>
    <script>
        var polyfilter_scriptpath = '/js/';
    </script>
    <!--OPENS DIALOG BOX-->
    <script>
var deliveryzoneId;

function yesButton(){
	var selectedOption = $("input:radio[name=allowCategoryTimings]:checked").val()
	var days = $('input[name="days"]:checked').map( function () {
    	return $(this).val();
           }).get().join();
	var updateCatTime=true;
	 if(jQuery("#itemTimingsYes").is(":checked")){
		 if(days==''){
			 $("#errorMessage").html("Please select atleast one day.");
			 updateCatTime=false;
		 }
	 }
	
	 var startTime = jQuery("#startTime option:selected").val();
	 var endTime = jQuery("#endTime option:selected").val(); 
	 
	if(updateCatTime){
     $("#errorMessage").html("");
	 $('.pj-loader-4').css('display', 'block');
	 $.ajax({
         type: 'GET',
         url: "saveDeliveryTime?days="+days+"&startTime="+startTime+"&endTime="+endTime+"&merchantId="+deliveryzoneId+"&allowDeliveryTiming="+selectedOption,
         success:function(data){
      $("#categoryTiming_"+deliveryzoneId).attr('allowCategoryTimings',allowCategoryTimings);
      $('.display-timings').css('display','none');
   	  $('#addZone').css('display','block');
   	  $('#deliveryTime').css('display','block');
   	  $('#example').css('display','block');
   	  $('.search-container').css('display','block');
   	  $('#example_length').css('display','block');
   	  $('#example_info').css('display','block');
   	  $('#example_paginate').css('display','block');
   	  $('.confirmModal_footer').css('display','block');
   	  $('.pj-loader-4').css('display', 'none');
   	  location.reload();
         }
      });
}
}

function cancelButton(){
	  $('.display-timings').css('display','none');
  	  $('#addZone').css('display','block');
  	  $('#deliveryTime').css('display','block');
  	  $('#example').css('display','block');
  	  $('.search-container').css('display','block');
  	  $('#example_length').css('display','block');
  	  $('#example_info').css('display','block');
  	  $('#example_paginate').css('display','block');
  	  $('.confirmModal_footer').css('display','block');
  	  location.reload();
}


$(document).ready(function() {
	$("#itemTimingsNo").click(function(){
    	$("#errorMessage").html("");
        $(".categoryTimingPopup").hide();
        allowCategoryTimings=0;
    });
    $("#itemTimingsYes").click(function(){
    	allowCategoryTimings=1;
        $(".categoryTimingPopup").show();
    });
});

function timings(merchant_id,zoneStatus){
	deliveryzoneId=merchant_id;
	
	  $('.display-timings').css('display','block');
	  $('#addZone').css('display','none');
	  $('#deliveryTime').css('display','none');
	  $('#example').css('display','none');
	  $('.search-container').css('display','none');
	  $('#example_length').css('display','none');
	  $('#example_info').css('display','none');
	  $('#example_paginate').css('display','none');
	  $('.confirmModal_footer').css('display','none');
	   
		allowCategoryTimings = $(this).attr('allowCategoryTimings');
		$('#days').prop('checked', true);
		$.ajax({
					type : 'GET',
					url : "getDeliveryTiming?merchantId="
							+ deliveryzoneId,
					success : function(jsondata) {
						var jData=JSON.parse(jsondata);
						var allowDeliveryTime= jData.allowDeliveryTiming;
						var data=jData.deliveryZoneTiming;

						if(data.length>0){
						console.log(data);
	                 
						 $("#errorMessage").html("");
						 if(allowDeliveryTime==0){
					          $("#itemTimingsNo").attr('checked', 'checked');
					          $("#errorMessage").html("");
					          $(".categoryTimingPopup").hide();
					        }
					        if(allowDeliveryTime==1){
					          $("#itemTimingsYes").attr('checked', 'checked');
					          $(".categoryTimingPopup").show();
					        }
						$( ".innerdays" ).remove();
						$("#weekDays").append("<div class='innerdays'>");
						var weekDays=[];
						var startTime="12:00 AM";
						var endTime="12:00 AM";
						for (var i = 0; i < data.length; i++) {
							if(i==5){
								 $(".innerdays").append("<br>"); 
							}
							
							if(data[i].isHoliday==false){
								startTime=data[i].startTime;
								endTime=data[i].endTime;
								
								$(".innerdays").append("<input type='checkbox' class='rrr' value='"+data[i].day+"' id='"+data[i].day+"' name='days' checked='checked'>"+data[i].day+"</input>");
							} else{
								$(".innerdays").append("<input type='checkbox' class='nnn' value='"+data[i].day+"' id='"+data[i].day+"' name='days' >"+data[i].day+"</input>");
							} 
							$("#weekDays").append("</div>");
						}
						$("#startTime option[value='"+startTime+"']").attr("selected", "selected");
						$("#endTime option[value='"+endTime+"']").attr("selected", "selected");
					}else{
						$("#startTime option[value='12:00 AM']").attr("selected", "selected");
						$("#endTime option[value='12:00 AM']").attr("selected", "selected");
						$(".innerdays input[type=checkbox]").each(function () {
			                $(this).prop("checked", false);
			            });
					}
						console.log(weekDays);
						$('#categoryOrderPopUp').show();
						$("#content2").hide();
						$('.confirmModal_footer').css('display','block');

					}
				});
}

function yes(){
	 $('.categoryTimingPopup').css('display','block');
	 $('.confirmModal_footer').css('display','block');
}

function no(){
	$('.confirmModal_footer').css('display','block');
	$('.categoryTimingPopup').css('display','none');
	
}


</script>
  </body>
</html>
