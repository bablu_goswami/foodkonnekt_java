<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="true"%>
<!doctype html>
<html class="no-js" lang="en">
  <head>
  
    <title>FoodKonnekt | Dashboard</title>
    <!--CALLING STYLESHEET STYE.CSS-->
    <link rel="stylesheet" href="resources/css/style.css">
    <!--CALLING STYLESHEET STYLE.CSS-->
    
    <!--CALLING GOOGLE FONT OPEN SANS-->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    <!--CALLING GOOGLE FONT OPEN SANS-->
    
    <!--CALLING FONT AWESOME-->
    <link rel="stylesheet" href="resources/css/font-awesome.css">
    <!--CALLING FONT AWESOME-->
    
    <!--CALLING FILTER OPTIONS-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
     <script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css">

    <style type="text/css">
        div#example_paginate {
          display: block;
        }
        
        div#example_filter {
          display: block;
        }
        
        div#example_length {
          display: block;
        }
        
        input[type="search"] {
          max-width: 300px;
          width: 87%;
          outline: 0;
          border: 1px solid rgb(169, 169, 169);
          padding: 11px 10px;
          border-radius: 6px;
          margin-bottom: 7px;
          placeholder: Search Items;
          display:none;
        }
        
        .pj-preloader {
    display: none;
    position: absolute;
    height: 383px;;
    width: 940px;
    background: url("resources/img/spinner.gif") no-repeat scroll center center rgba(153, 153, 153, 0.3);
    z-index: 9999;
    left: 260;
    position: absolute;
    top: 224px;
}
</style>
    <!--CALLING FILTER OPTIONS-->
    <!--OPENS DIALOG BOX-->
    <link rel="stylesheet" type="text/css" href="resources/css/dialog-box/component.css" />
    <!--OPENS DIALOG BOX-->
  </head>
  <body>
    <div id="page-container">
        <div class="foodkonnekt merchant">
            <div class="inner-container">
                <div class="max-row">
                    
                    <header id="page-header">
                        <div class="inner-header">
                            <div class="row">
                                <div class="logo">
                                     <a href="adminHome" title="FoodKonnekt Dashboard" class="logo"><img src="resources/img/foodkonnekt-logo.png"></a>
                                </div><!--.logo-->
                                  
                            </div><!--.row-->
                        </div><!--.inner-header-->
                    </header><!--#page-header-->
                    
                    <div id="page-content">
                        <div class="outer-container">
                            <div class="row">
                                <div class="content-inner-container">
                                                                  
                                    <div class="right-content-container">
                                        <div class="right-content-inner-container">
                                        
                                            <div class="content-header">
                                                <div class="all-header-title">
                                                </div><!--.header-title-->
                                                <div class="content-header-dropdown">
                                                </div><!--.content-header-dropdown-->
                                            </div><!--.content-header-->
                                            
                                            <div class="merchant-page-data">
                                                <div class="merchant-actions-outbound">
                                                    <div class="merchat-coupons-container">
                                                      
                                                       <div class="pj-preloader"></div>
                                                        <div class="delivery-zones-content-container">
                                                        
                                                        
                      										
																	<%-- <c:if test="${merchant.activeCustomerFeedback==1}"> --%>
																				
                                                             
																				<div class="xlsUpload" id="uploadSheet" style="display:block">   
                                                                                              <label id="errorMessage" style="color: red; float:center" ></label><br>
                                                                                              <div class="upload-inventory" id="upload-excel" style="display:block;">     
                                                                                                                                                                            
                                                                                     <form id="fileForm" style=" margin: 4%; margin-left: 20%; margin-top: 1.6%;"> <label>Merchant Id</label><input type="number" value=0 id="merchantId"/><br> <input type="file" name="file" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"/>
                                                                                     <br>
                                                                                     </form>
                                                                                     <input type="button" id="btnUpload" value="Upload Offline Customer" style="margin: 4%;margin-right: 22%;margin-top: -6.5%;"><br>
                                                                                
                                                                         </div>   
																	<%-- </c:if> --%>
															
																				
																			                                   
                                                        
                                                        
                                                        
                                                          
                                                        
                                                       
                                                                         
                                                                                
                                                                            
   																 
													
																	  
                                                             
												        
                                                                                    <!-- </div>
                                                                                              
                                                                                               
                                                                                              <div class="upload-inventory" id="save-excel" style="margin-bottom:3%; display:none;">
                                                                                     
                                                                                     <input type="button" id="saveInventory" onclick="saveOfflineCustomerData()" value="Save Excel Sheet" style="margin: 4%;margin-bottom: 3%; margin-right: 22%;margin-top: -6.5%;"><br>
                                                                                     
                                                                                    </div> -->
                                                                                              
                                                                                   
 
                                                       
                                                        
                                                        
                                                        
                                                        <!-- <script type="text/javascript">
                                                            $(".searchq").keyup(function() {
                                                                var val = $(this).val();
                                                                ('input[type="search"]').val(val);
                                                                $('input[type="search"]').trigger("keyup");
                                                            });
                                                        </script> -->
                                                           
                                                            
                                                            
                                                        </div><!--.coupons-content-container-->
                                                    </div><!--.merchat-coupons-container-->
                                                </div><!--.merchant-actions-outbound-->
                                            </div><!--.merchant-page-data-->
                                            
                                        </div><!--.right-content-inner-container-->
                                    </div><!--.right-content-container-->
                                </div><!--.content-inner-container-->
                            </div><!--.row-->
                            
                        </div><!--.outer-container-->
                    </div><!--#page-content-->
                                             
                </div><!--.max-row-->
            </div><!--.inner-container-->
        </div><!--.foodkonnekt .dashboard-->
    </div><!--#page-container-->
    
    <!--OPENS DIALOG BOX-->
    <script src="resources/js/dialog-box/classie.js"></script>
    <script src="resources/js/dialog-box/modalEffects.js"></script>
    <script>
        var polyfilter_scriptpath = '/js/';
    </script>
    <!--OPENS DIALOG BOX-->
        <script>
        
        $(document).ready(function() {
            var file = $('[name="file"]');
            var imgContainer = $('#imgContainer');
            
            $('#btnUpload').on('click', function() {
            	$('.pj-preloader').css('display','block');
            	
                var filename = $.trim(file.val());
                
                
                if(filename!=''){
                	
                	var i = setInterval(function() { setSessionTimeOut(); }, 1000*60*14);
                	$('.pj-preloader').css('display','block');
                $.ajax({
                    url: 'uploadOfflineCustomerByMerchantId?merchantId='+$('#merchantId').val(),
                    type: "POST",
                    data: new FormData(document.getElementById("fileForm")),
                    enctype: 'multipart/form-data',
                    processData: false,
                    contentType: false
                  }).done(function(data) {
                	  if(data!='error'){
                	   //$('#save-excel').css('display','block');
                	  //$('#upload-excel').css('display','none');
                	  $('#errorMessage').html(data);
                	  
                	  }
                	  else
                	  {
                		 // $('#save-excel').css('display','none');
                    	 // $('#upload-excel').css('display','block');
                    	  $('#errorMessage').html("File upload failed....Please try again..");
                	  }
                	  $('.pj-preloader').css('display','none');
                  }).fail(function(jqXHR, textStatus) {
                      //alert(jqXHR.responseText);
                    $('.pj-preloader').css('display','none');
                      alert('File upload failed ...');
                  });
                }else{
                	 $('.pj-preloader').css('display','none');
                	 alert('Please upload the file...');
                }
               
            });
            
            $('#btnClear').on('click', function() {
                imgContainer.html('');
                file.val('');
            });
        });
        
      
</script>


<script type="text/javascript">

 var activeMerchantFeedback = "${merchant.activeCustomerFeedback}";
 /* alert(activeMerchantFeedback); */
 
 if(activeMerchantFeedback == 1)
 {
	 $("#uploadSheet").css('display','block');
 }
</script>
  </body>
</html>
