<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="true"%>
<!doctype html>
<html class="no-js" lang="en">
<head>

<title>FoodKonnekt | Dashboard</title>
<!--CALLING STYLESHEET STYE.CSS-->
<link href="resources/css/bootstrap.min.css"
        rel="stylesheet" type="text/css" />
        <link href="resources/css/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
<!--CALLING STYLESHEET STYE.CSS-->
<link rel="stylesheet" href="resources/css/style.css">
<!--CALLING STYLESHEET STYLE.CSS-->

<!--CALLING GOOGLE FONT OPEN SANS-->
<link
	href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic'
	rel='stylesheet' type='text/css'>
<!--CALLING GOOGLE FONT OPEN SANS-->

<!--CALLING FONT AWESOME-->
<link rel="stylesheet"
	href="resources/css/font-awesome.css">
<!--CALLING FONT AWESOME-->

<!--OPENS DIALOG BOX-->
<link rel="stylesheet" type="text/css"
	href="resources/css/dialog-box/component.css" />
<!--OPENS DIALOG BOX-->

<!--CALLING PRODUCTS TABS-->
<link rel='stylesheet' type='text/css'
	href='resources/css/products-tabs/opentabby.css' />
<!--CALLING PRODUCTS TABS-->
<!-- <link rel="stylesheet" type="text/css"
	href="https://cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css"> -->
	
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
	<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
	<script src='resources/js/products-tabs/opentabby.js'></script>
	<script src="resources/js/popModal.js"></script>
	<script type="text/javascript" src="resources/js/bootstrap.min.js"></script>
    <script src="resources/js/bootstrap-multiselect.js" type="text/javascript"></script>

<style>
.errorClass {
	border: 1px solid red;
}
</style>
<style>
/* The container */
.radiocontainer {
	display: block;
	position: relative;
	padding-left: 35px;
	margin-bottom: 12px;
	cursor: pointer;
	font-size: 18px;
	-webkit-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	user-select: none;
}

/* Hide the browser's default radio button */
.radiocontainer input {
	position: absolute;
	opacity: 0;
	cursor: pointer;
}

/* Create a custom radio button */
.checkmark {
	position: absolute;
	top: 0;
	left: 0;
	height: 25px;
	width: 25px;
	background-color: #eee;
	border-radius: 50%;
}

/* On mouse-over, add a grey background color */
.radiocontainer:hover input ~ .checkmark {
	background-color: #ccc;
}

/* When the radio button is checked, add a blue background */
.radiocontainer input:checked ~ .checkmark {
	background-color: orange;
}

/* Create the indicator (the dot/circle - hidden when not checked) */
.checkmark:after {
	content: "";
	position: absolute;
	display: none;
}

/* Show the indicator (dot/circle) when checked */
.radiocontainer input:checked ~ .checkmark:after {
	display: block;
}

/* Style the indicator (dot/circle) */
.radiocontainer .checkmark:after {
	top: 9px;
	left: 9px;
	width: 8px;
	height: 8px;
	border-radius: 50%;
	background: white;
}

.pj-preloader {
	display: none;
	position: absolute;
	height: 383px;;
	width: 940px;
	background: url("resources/img/spinner.gif") no-repeat scroll center
		center rgba(153, 153, 153, 0.3);
	z-index: 9999;
	left: 260;
	position: absolute;
	top: 224px;
}
</style>
</head>
<body>
	<div id="page-container">
		<div class="foodkonnekt merchant">
			<div class="inner-container">
				<div class="max-row">

					<header id="page-header">
						<div class="inner-header">
							<div class="row">
								<div class="logo">
									<a href="adminHome" title="FoodKonnekt Dashboard" class="logo"><img
										src="resources/img/foodkonnekt-logo.png"></a>
								</div>
								<!--.logo-->
								<%@ include file="adminHeader.jsp"%>
							</div>
							<!--.row-->
						</div>
						<!--.inner-header-->
					</header>
					<!--#page-header-->

					<div id="page-content">
						<div class="outer-container">
							<div class="row">
								<div class="content-inner-container">
									<%@ include file="leftMenu.jsp"%>
									<div class="right-content-container">
										<div class="right-content-inner-container">

											<div class="content-header">
												<div class="all-header-title"></div>
												<!--.header-title-->
												<div class="content-header-dropdown"></div>
												<!--.content-header-dropdown-->
											</div>
											<!--.content-header-->

											<div class="merchant-page-data">
												<div class="merchant-actions-outbound">
													<div class="merchat-coupons-container">

														<div class="coupons-navigation">
															<ul>
																				<li><a
																					href="inventory">Items</a></li>
																				<li><a href="category">Categories</a></li>
																				<li><a href="modifierGroups">Modifier
																						Groups</a></li>
																				<li class="current-menu-item"><a href="modifiers">Modifiers</a></li>
																				<li id="itemTaxMap"><a href="itemTaxMap">Tax Map</a></li>
																			</ul>
														</div>
														<!--.coupons-navigation-->

														<div class="delivery-zones-content-container">
															
															<div id="formDiv">
															<form:form action="updateItemModifierGroup"
																modelAttribute="itemModifierGroup" method="POST">
																<br>
																<br>
																<label>Items:</label>
																<form:select id="itemId" class="multiselect"
																	multiple="multiple" path="itemsIds" required="true">
																	<c:forEach items="${items}" var="item"
																		varStatus="status">
																		<option value="${item.id}">${item.name}</option>
																	</c:forEach>
																</form:select>
																<br>
																<br>


																<label>Modifier Groups:</label>
																<form:select id="categoryId" path="modifiresGrpId" required="true">
																	<option value="">Select Modifiers Group</option>
																	<c:forEach items="${modifierGroup}" var="category"
																		varStatus="status">
																		<option value="${category.id}">${category.name}</option>
																	</c:forEach>
																</form:select>
																<br>
																<br>

																<label>Modifiers:</label>
																<form:select id="modifiersId" class="multiselect"
																		multiple="multiple" path="modisId">


																	</form:select>
																<br>
																<div class="button left">
																	<!-- <input type="button" onclick="addModifiers()" value="Add Modifiers"> -->
																	<input type="submit" id="updateItemButton" value="Save">
																</div>
																</form:form>
																</div>
																<div class="button left">

																	<div id="inputDiv" style="display: none;">
																	<br><br>
																		 <label>Modifiers Name:</label>
																		 <input type="text" placeholder="modifier name" id="modifierName">
																		 <label>Modifiers Price:</label>
																		<input type="number" placeholder="modifier price" id="modifierPrice">
																		
																		<label>Modifier Groups:</label>
																		<select id="modifierGroup">
																				<c:forEach items="${modifierGroup}" var="category"
																				varStatus="status">
																					<option value="${category.id}">${category.name}</option>
																				</c:forEach>
																		</select>
																		
																		<input type="button" id="saveModifiers" value="Save Modifier" onclick="saveModifiers()">
																	</div>
																	
																	
																</div>
															

														</div>
														<!--.coupons-content-container-->
														<div id="errorDiv" style="color: red"></div>
													</div>
													<!--.merchat-coupons-container-->
												</div>
												<!--.merchant-actions-outbound-->
											</div>
											<!--.merchant-page-data-->

										</div>
										<!--.right-content-inner-container-->
									</div>
								</div>

								<div class="methodsdata"></div>


							</div>
							<!--.row-->

						</div>
						<!--.outer-container-->

					</div>
					<!--#page-content-->
					<%@ include file="adminFooter.jsp"%>
				</div>
				<!--.max-row-->
			</div>
			<!--.inner-container-->
		</div>
		<!--.foodkonnekt .dashboard-->
	</div>
	<!--#page-container-->


</body>
	<script type="text/javascript">
	$(function () {
		 $('#itemId').multiselect({
	        maxHeight: 450 ,
         	includeSelectAllOption: true
         }); 
		 
	    /*  $('#categoryId').multiselect({
			maxHeight: 450 ,
	        includeSelectAllOption: true
	     });  */
	 
	     $('#categoryId').change(function() {
	          var modifierGroupId = $(this).val();
	         
	      	$.ajax({
	            type: 'GET',
	            url: "showModifiersByAjax?modifierGroupId="+modifierGroupId,
	            success:function(data){
	            	$('#modifiersId').find('option').remove();
	             	$('#modifiersId').find('optgroup').remove();
	             	$('#modifiersId').empty();
	  	                 var div_data= "";
	            	     jQuery.each(data, function(key, value){
		                 div_data="<option value="+value.id+">"+value.name+"</option>";
		                 $('#modifiersId').append(div_data);
		                 $('#modifiersId').multiselect('rebuild');
		               	$('#modifiersId').multiselect('refresh'); 
		               	

	            	});
	            	     
	            	     if(data==""){
	     					div_data="";
	     		                 $('#modifiersId').append(div_data);
	     		                 $('#modifiersId').multiselect('rebuild');
	     		               	$('#modifiersId').multiselect('refresh');
	     					}
	            } 
	          			});
	  
	      	             
	            
	      });
	     
	    $('#modifiersId').multiselect({
		   maxHeight: 450 ,
		   /* enableFiltering: true, */
		   enableCaseInsensitiveFiltering: true,
           includeSelectAllOption: true
        });  
	});
	
	var modifierGroupId;
	
	function addModifiers(selected){
		$("#inputDiv").css('display','block');
		$("#formDiv").css('display','none');
		
		modifierGroupId = selected;
	}
	
	   $('#modifierGroup').change(function(e) {
           var selected = $(e.target).val();
           addModifiers(selected);
       });
	   
	   
	   function saveModifiers(){
		   var modifierName = $("#modifierName").val();
			var modifierPrice = $("#modifierPrice").val();
			
			$.ajax({
	            type: 'GET',
	            url: "saveModifiersMasterForm?modifierName="+modifierName+"&modifierPrice="+modifierPrice+"&modifierGroupId="+modifierGroupId,
	            success:function(data){
	            	console.log(data);
	            	if(data=="success"){
	            		window.location.href = "itemModifierGroup";
	            	}
	            	
	            }
			});
	   }
	
	</script>
	<script>
		var posId = "${sessionScope.merchant.owner.pos.posId}";
		
		$(document).ready(function() {
			if(posId != '3'){
	      	  $('#itemTaxMap').css('display','none');
	      	  }
	    });
</script>
	
</html>
