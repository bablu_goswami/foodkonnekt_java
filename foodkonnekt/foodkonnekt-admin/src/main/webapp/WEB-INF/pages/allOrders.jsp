<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page session="true"%>
<!doctype html>
<html class="no-js" lang="en">
<head>
<title>FoodKonnekt | Dashboard</title>
<!--CALLING STYLESHEET STYE.CSS-->
<link rel="stylesheet" href="resources/css/style.css">
<!--CALLING STYLESHEET STYLE.CSS-->

<!--CALLING GOOGLE FONT OPEN SANS-->
<link
    href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic'
    rel='stylesheet' type='text/css'>
<!--CALLING GOOGLE FONT OPEN SANS-->

<!--CALLING FONT AWESOME-->
<link rel="stylesheet" href="resources/css/font-awesome.css">
<!--CALLING FONT AWESOME-->

<!--CALENDAR MULTI-SELECT-->
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css">
<link href="resources/css/calendar/jquery.comiseo.daterangepicker.css" rel="stylesheet" type="text/css">
<!--CALENDAR MULTI-SELECT-->

<!--CHECK ALL LIST TABLE-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="resources/js/checkall/jquery.checkall.js"></script>
<!--CHECK ALL LIST TABLE-->

<!--OPENS DIALOG BOX-->
<link rel="stylesheet" type="text/css" href="resources/css/dialog-box/component.css" />
<!--OPENS DIALOG BOX-->

<!--ACCORDION FOR MENU-->
<script src="resources/js/accordion/paccordion.js"></script>
<!--ACCORDION FOR MENU-->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css">
<script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
<link type="text/css" rel="stylesheet" href="resources/css/popModal.css">
<script src="resources/js/popModal.js"></script>

<div class="exampleLive">
  <button style="display: none;" id="confirmModal_ex25"
		class="btn btn-primary" data-confirmmodal-bind="#confirm_content0" data-topoffset="0" data-top="10%">Example</button>
	<!-- <button style="display: none;" id="confirmModal_ex22"
		class="btn btn-primary" data-confirmmodal-bind="#confirm_content" data-topoffset="0" data-top="10%">Example</button> -->
		
	<button style="display: none;" id="confirmModal_ex23"
		class="btn btn-primary" data-confirmmodal-bind="#confirm_content1" data-topoffset="0" data-top="10%">Example</button>
		
	<button style="display: none;" id="confirmModal_ex24"
		class="btn btn-primary" data-confirmmodal-bind="#confirm_content2" data-topoffset="0" data-top="10%">Example</button>
		
		<button style="display: none;" id="confirmModal_ex26"
		class="btn btn-primary" data-confirmmodal-bind="#confirm_content3" data-topoffset="0" data-top="10%">Example</button>
</div>

<!-- <div id="confirm_content" style="display: none">
	<div class="confirmModal_content sd-popup-content" id="popupdata">
		<img src="resources/img/logo.png" class="sd-popup-logo">
       <h3>Your session has been timed out. Please log back in</h3>
	</div>
	<div class="confirmModal_footer">
        <button type="button" class="btn btn-primary" onclick="sessionTimeOut()">Ok</button>
    </div> 
</div> -->

<div id="confirm_content0" style="display: none">
	<div class="confirmModal_content sd-popup-content" id="popupdata">
		<img src="resources/img/logo.png" class="sd-popup-logo">

		<div>
			<div id="Comments0" style="margin-top: 7%;">
				<center><p style="text-transform: capitalize;" id="para"><br><br></p></center>
			</div>
		</div>
	</div>

	<div class="confirmModal_footer button" style="margin-top: -6%;">
		<button type="button" class="btn btn-default guestPopUpCls"
			onclick="acceptOrder()" data-confirmmodal-but="cancel">
			<font style="color: black;"><b>Accept</b></font>
		</button>

		<button type="button" class="btn btn-default guestPopUpCls"
			onclick="selectChange()" data-confirmmodal-but="cancel">
			<font style="color: black;"><b>Cancel</b></font>
		</button>
	</div>
</div>

<div id="confirm_content1" style="display: none">
	<div class="confirmModal_content sd-popup-content" id="popupdata1">
		<img src="resources/img/logo.png" class="sd-popup-logo">

		<div>
			<div id="Comments1" style="margin-top: 7%;">
				<center><p style="text-transform: capitalize;" id="paraDecline"><br><br></p></center>
			</div>
		</div>
	</div>

	<div class="confirmModal_footer button" style="margin-top: -6%;">
		<button type="button" class="btn btn-default guestPopUpCls"
			onclick="acceptOrder()" data-confirmmodal-but="cancel">
			<font style="color: black;"><b>Decline</b></font>
		</button>

		<button type="button" class="btn btn-default guestPopUpCls"
			data-confirmmodal-but="cancel">
			<font style="color: black;"><b>Cancel</b></font>
		</button>
	</div>
</div>

<div id="confirm_content2" style="display: none">
	<div class="confirmModal_content sd-popup-content" id="popupdata1">
		<img src="resources/img/logo.png" class="sd-popup-logo">

		<div>
			<div id="Comments2" style="margin-top: 7%;">
				<center><p style="text-transform: capitalize;" id="paraStatus"><br><br></p></center>
			</div>
		</div>
	</div>

	<div class="confirmModal_footer button" style="margin-top: -6%;">
		<button type="button" class="btn btn-default guestPopUpCls"
			data-confirmmodal-but="cancel" onclick="okFunction()">
			<font style="color: black;"><b>OK</b></font>
		</button>
	</div>
</div>
<div id="confirm_content3" style="display: none">
	<div class="confirmModal_content sd-popup-content" >
		<img src="resources/img/logo.png" class="sd-popup-logo">

		<div>
			<div id="Comments3" style="margin-top: 7%;">
				<center><p style="text-transform: capitalize;" id="cancelbutton"><br><br></p></center>
			</div>
		</div>
	</div>

	<div class="confirmModal_footer button" style="margin-top: -6%;">
		<button type="button" class="btn btn-default guestPopUpCls"
			onclick="cancel()" data-confirmmodal-but="cancel">
			<font style="color: black;"><b>yes</b></font>
		</button>

		<button type="button" class="btn btn-default guestPopUpCls"
			onclick="selectChange()" data-confirmmodal-but="cancel">
			<font style="color: black;"><b>Cancel</b></font>
		</button>
	</div>
</div>

<script type="text/javascript">
jQuery.fn.dataTableExt.oApi.fnPagingInfo = function ( oSettings )
    {
       return {
           "iStart":         oSettings._iDisplayStart,
           "iEnd":           oSettings.fnDisplayEnd(),
           "iLength":        oSettings._iDisplayLength,
           "iTotal":         oSettings.fnRecordsTotal(),
           "iFilteredTotal": oSettings.fnRecordsDisplay(),
           "iPage":          oSettings._iDisplayLength === -1 ?
               0 : Math.ceil( oSettings._iDisplayStart / oSettings._iDisplayLength ),
           "iTotalPages":    oSettings._iDisplayLength === -1 ?
               0 : Math.ceil( oSettings.fnRecordsDisplay() / oSettings._iDisplayLength )
       };
   };
   
   function format ( d ) {
	   // alert(d.firstName);
	}
   
   $(document).ready( function setUp() {
	   
	   /* var startDate1 = $("#startDate1").val();
	   alert(startDate1); */
	   
	  
	   
	   var dt =$("#checkit-table").dataTable({
           "bProcessing": true,
           "bServerSide": true,
           "sort": "position",
           "bStateSave": false,
           "iDisplayLength": 10,
           "iDisplayStart": 0,
           "fnDrawCallback": function () {
               //Get page numer on client. Please note: number start from 0 So
           },         
           "sAjaxSource": "allOrdersDataTables",
           "aoColumns": [
			   { "mData": "id" },
               { "mData": "firstName" },
               { "mData": "createdOn" },
               { "mData": "orderPrice" },
               { "mData": "orderType" },
               { "mData": "status" },
               { "mData": "orderName" },
              
               { "mData": "paymentMethod" },
               
               
               {
                   "class":          "details-control",
                   "orderable":      false,
                   "mData":           "view",
                   "defaultContent": ""
               }, {
					"mData" : "action"
				}
           ]
           
       });
	   var detailRows = [];
       $('#checkit-table tbody').on( 'click', 'tr td.details-control', function () {
    	   var tr = $(this).closest('tr');
    	   var rowID=tr.attr('id')
	   //var table = $('#checkit-table').DataTable();
    	   var table = $('#checkit-table').DataTable();//.fnDestroy();
var rowData = table.row('#'+rowID).data();

var deliveryFeeDiv="";
orderType1 = rowData.orderType;
if(rowData.orderType!='pickup' &&  rowData.orderType!='Pickup' && rowData.deliveryFee>0){
	deliveryFeeDiv=deliveryFeeDiv+"<ul><li>Delivery Fee</li><li></li> <li>$ "+rowData.deliveryFee+"</li> </ul>";
}


var message;
var posId = $("#posId").val();

if(posId == 2 || posId == 4){
	if(rowData.orderPosId != null && rowData.id != Number(rowData.orderPosId)){
		message = "Order went through : <font style='color:green'>Yes</font> ("+rowData.orderPosId+")";
	}else{
		message = "Order went through : <font style='color:red'>No</font>";
	}
}else{
	message ="";
}


//alert(rowData.orderItemViewVOs);
var items="";
$.each( rowData.orderItemViewVOs, function ( i ) {
	
	var modifiers=""; 
	
	$.each( rowData.orderItemViewVOs[i].itemModifierViewVOs, function ( j ) {
		modifiers=modifiers+"<ul><li>"+rowData.orderItemViewVOs[i].itemModifierViewVOs[j].modifiers.name+"</li><li>"+rowData.orderItemViewVOs[i].itemModifierViewVOs[j].quantity+"</li><li>$"+rowData.orderItemViewVOs[i].itemModifierViewVOs[j].modifiers.price.toFixed(2)+" </li></ul>";
	} );
	
	items=items+"<div class='order-details-items-quantity-price'><ul><li>"+rowData.orderItemViewVOs[i].itemVO.name+"</li><li>"+rowData.orderItemViewVOs[i].quantity+"</li><li>$"+rowData.orderItemViewVOs[i].itemVO.price.toFixed(2)+"</li></ul>"+modifiers+"</div>";
	
} );

var orderDiscountList="<ul><li>Discount Coupon</li>";
if(rowData.orderDiscountsList.length > 0){
$.each( rowData.orderDiscountsList, function ( i ) {
	if(i==0){
	orderDiscountList += "<li>"+rowData.orderDiscountsList[i].couponCode+"</li>";
	}else{
		orderDiscountList += "<ul><li>&nbsp;</li><li>"+rowData.orderDiscountsList[i].couponCode+"</li></ul>";
	}
	
} );
}
orderDiscountList += "</ul>";


    	   var res = rowID.split("_");
    	   rowID = res[1];
    	   var divId="modal-15-"+rowID;
    	   var convenienceFeeDiv="";
    	  var tip="";
    	  
    	  if(rowData.tipAmount>0){
    		  tip=tip+"<ul> <li>Tip</li> <li></li><li>$ "+rowData.tipAmount.toFixed(2)+"</li></ul>";
    	  }
    	   
    	  if(rowData.discount>0){
    		  tip=tip+"<ul> <li>Discount</li> <li></li><li>$ "+rowData.discount.toFixed(2)+"</li></ul>"+orderDiscountList;
    	  }
    	  if(rowData.convenienceFee==''){
    		  
    	  }else if(rowData.convenienceFee>0){
    		  /* CHANGE Convenience to Online */
    		  /* convenienceFeeDiv=convenienceFeeDiv+"<ul> <li>Convenience Fee</li> <li></li><li>$ "+rowData.convenienceFee+"</li></ul>"; */
    		  convenienceFeeDiv=convenienceFeeDiv+"<ul> <li>Online Fee</li> <li></li><li>$ "+Number(rowData.convenienceFee).toFixed(2)+"</li></ul>";
    	  }
    	  /*  if(rowData.orderType=="delivery")
    	   $("#sd-dialog").append("<div id='sd-dialog'><div class='md-modal md-effect-14' id='modal-15-"+rowID+"'><div class='md-content'><h4 style='display:block;background: rgb(255, 128, 2);'></h4><h3>ORDER DETAILS</h3>"
    	   +"<table>"
    	    +"<tr><td><b>Customer name:</b></td><td>"+rowData.firstName+"</td><td>Delivery Address</td></tr><tr><td><b>Email Id:</b></td><td>"+rowData.emailId+"</td><td>"+rowData.orderAdrress+"</td></tr></tr></td><b>Phone No.:</b></td><td>"+rowData.phoneNo+" </td></div>"
  
    	   	

    	   
    	   +"<div><div class='order-details-container'><div class='order-details-header'><ul><li><span>Item</span></li><li><span>Quantity</span></li><li><span>Price</span></li></ul></div>"+items+"<div class='order-subtotal-tax-total'><ul><li>Subtotal</li><li>$"+Number(rowData.subTotal).toFixed(2)+"</li> </ul> "+convenienceFeeDiv+" "+deliveryFeeDiv+"<ul><li>Tax</li><li>$"+Number(rowData.tax).toFixed(2)+"</li> </ul>"+tip+" <ul> <li>Total</li><li>$"+rowData.orderPrice+" </li> </ul> </div><div class='order-details-order-notes'><textarea name='order-notes' cols='' rows='4' readonly='readonly'>"+rowData.orderName+" </textarea></div> </div><div class='clearfix'></div> <div class='button'><button class='white-button md-close' onClick='closeNavSignIn()'>Close</button></div> </div> </div> </div><div class='md-overlay'></div></div>");
    	   
    	   else */
    	   var notificationTrackers = "<div id='myDIV"+rowID+"' style='margin-left:10px;margin-right:30px;margin-bottom:15px;'><table style='border: 1px solid black;'> <tr><th style='border: 1px solid black;'>Type</th><th style='border: 1px solid black;'>ContactId</th><th style='border: 1px solid black;'>Status</th></tr>";
			
			if (typeof rowData.notificationTracker !== "undefined" && rowData.notificationTracker.length>0) {
				$
						.each(
								rowData.notificationTracker,
								function(i) {
									notificationTrackers +=	"<tr><td style='border: 1px solid black;'>"+rowData.notificationTracker[i].notificationType +"</td><td style='border: 1px solid black;'>"+ rowData.notificationTracker[i].contactId + "</td><td style='border: 1px solid black;'>"+ rowData.notificationTracker[i].status + "</td></tr>";
		
								});
			}
			notificationTrackers += "</table></div>";

			if (typeof rowData.notificationTracker !== "undefined" && rowData.notificationTracker.length>0) {
				notificationTrackers +="<div style='margin-left:10px;margin-right:30px;margin-bottom:15px;'>Notification : "+rowData.notificationTracker[0].enableNotification+"</div>"
			}
    	   if(rowData.orderType=="delivery")
    		   {
    		   $("#sd-dialog").append("<div id='sd-dialog'><div class='md-modal md-effect-14' id='modal-15-"+rowID+"'><div class='md-content'><h3>ORDER DETAILS</h3>"
    				   +"<div class='order-details-container'><div><table style='font-size:12px;border-spacing: 1px;'><tr>"
    				    +"<td><b>Customer name:</b></td><td>"+rowData.firstName+"</td>"
    				  +" <td><b>Delivery Address:</b></td><td>"+rowData.orderAdrress+"</td></tr>"
    				  +"<tr>"
    				    +"<td><b>Email Id:</b></td><td>"+rowData.emailId+"</td>"
    				   +" <td><b>State:</b></td><td> "+rowData.state +"</td> </tr>"
    				  +"<tr>"
    				   +" <td><b>Phone Number:</b></td><td>"+rowData.phoneNo+"</td>"
    				   +"<td><b>City:</b></td><td>"+rowData.city+"</td></tr>"
    				   +"<tr>"
    				   +" <td><b>Created On:</b></td><td>"+rowData.createdOn+"</td>"
    				   +"<td><b>fullfilled On:</b></td><td>"+rowData.fulfilledOn+"</td></tr>"
    				   +"<tr><td></td></td><td><td><b>Zip:</b></td><td>"+rowData.zip+"</td></tr></table></div>"
    		   
    		   
    		  +" <div><div>"
    		  + "<span id='message' style='font-size: 15px; color: black;'>"+message+"</span><br>"
    		  +"<div class='order-details-header'><ul><li><span>Item</span></li><li><span>Quantity</span></li><li><span>Price</span></li></ul></div>"
    		  +items
    		  +"<div class='order-subtotal-tax-total'><ul><li>Subtotal</li><li>$"+Number(rowData.subTotal).toFixed(2)+"</li> </ul> "+convenienceFeeDiv+" "+deliveryFeeDiv+"<ul><li>Tax</li><li>$"+Number(rowData.tax).toFixed(2)+"</li> </ul>"+tip+" <ul> <li>Total</li><li>$"+rowData.orderPrice+" </li> </ul> </div><div class='order-details-order-notes'><textarea name='order-notes' cols='' rows='4' readonly='readonly'>"+rowData.orderName+" </textarea></div> "
    		  +	notificationTrackers + "</div><div style='margin-bottom:20px;' class='clearfix'></div> </div></div><div><div class='button'><button class='white-button md-close' onClick='closeNavSignIn()'>Close</button>  </div> <div class='button'><button class='white-button md-close' style='margin-right:25px;' onclick='myFunction("+rowID+")' id='showHideButton'>Hide</button></div></div> </div><div class='md-overlay'></div></div>");
    		   }
    	   else
    	   { $("#sd-dialog").append("<div id='sd-dialog'><div class='md-modal md-effect-14' id='modal-15-"+rowID+"'><div class='md-content'><h3>ORDER DETAILS</h3>"
				   +"<div class='order-details-container'><div><table style='font-size:12px;border-spacing: 1px;'><tr>"
				    +"<td><b>Customer name:</b></td><td>"+rowData.firstName+"</td>"
				  +" <td></td><td></td></tr>"
				  +"<tr>"
				    +"<td><b>Email Id:</b></td><td>"+rowData.emailId+"</td>"
				   +" <td></td><td> </td> </tr>"
				  +"<tr>"
				   +" <td><b>Phone Number:</b></td><td>"+rowData.phoneNo+"</td>"
				   +"<td></td><td></td></tr>"
				   +"<tr>"
				   +" <td><b>Created On:</b></td><td>"+rowData.createdOn+"</td>"
				   +"<td></td><td></td></tr>"
				   +" <td><b>Fullfilled On:</b></td><td>"+rowData.fulfilledOn+"</td>"
				   +"<td></td><td></td></tr>"
				   +"</table></div>"
		   
		   
		  +" <div><div>"
		  + "<span id='message' style='font-size: 17px; color: black;'>"+message+"</span><br>"
		  +"<div class='order-details-header'><ul><li><span>Item</span></li><li><span>Quantity</span></li><li><span>Price</span></li></ul></div>"
		  +items
		  +"<div class='order-subtotal-tax-total'><ul><li>Subtotal</li><li>$"+Number(rowData.subTotal).toFixed(2)+"</li> </ul> "+convenienceFeeDiv+" "+deliveryFeeDiv+"<ul><li>Tax</li><li>$"+Number(rowData.tax).toFixed(2)+"</li> </ul>"+tip+" <ul> <li>Total</li><li>$"+rowData.orderPrice+" </li> </ul> </div><div class='order-details-order-notes'><textarea name='order-notes' cols='' rows='4' readonly='readonly'>"+rowData.orderName+" </textarea></div> "
		  +	notificationTrackers + "</div><div style='margin-bottom:20px;' class='clearfix'></div></div></div><div> <div class='button'><button class='white-button md-close' onClick='closeNavSignIn()'>Close</button>  </div> <div class='button'><button class='white-button md-close' style='margin-right:25px;' onclick='myFunction("+rowID+")' id='showHideButton'>Hide</button></div></div> </div><div class='md-overlay'></div></div>");
		   }
    		   
    		   $("#modal-15-"+rowID).addClass('md-show');
       } );
      
       
       dt.on( 'draw', function () {
           $.each( detailRows, function ( i, id ) {
               $('#'+id+' td.details-control').trigger( 'click' );
           } );
       } );
       
       $(".white-button").click(function () {
    	  
    	   
          });
       
       
       
       
   });  
   
   function closeNavSignIn(){
	   $(".md-modal").removeClass('md-show');
   }
</script>

<style type="text/css">
div#example_paginate {
	display: block;
}

div#checkit-table_filter {
	display: none;
}

checkit-table_filter

div#example_length {
	display: block;
}

table, td, tr {  
  border: 0px solid #ddd;
  text-align: left;
  
}

table {
  border-collapse: collapse;
  width: 100%;
}

tr, td {
  padding: 0px;
  margin:0px;
}



input[type="search"] {
	max-width: 300px;
	width: 100%;
	outline: 0;
	border: 1px solid rgb(169, 169, 169);
	padding: 11px 10px;
	border-radius: 6px;
	margin-bottom: 7px;
	display: none;
}
#changeTime{border:2px solid #f8981d !important; margin-top:10px;}
.pj-preloader {
    display: none;
    position: absolute;
    height: 383px;;
    width: 940px;
    background: url("resources/img/spinner.gif") no-repeat scroll center center rgba(153, 153, 153, 0.3);
    z-index: 9999;
    left: 260;
    position: absolute;
    top: 224px;
}
</style>
</head>
<body>
	<div id="page-container">
		<div class="foodkonnekt orders">
			<div class="inner-container">
				<div class="max-row">

					<header id="page-header">
						<div class="inner-header">
							<div class="row">
								<div class="logo">
									<a href="adminHome" title="FoodKonnekt Dashboard" class="logo"><img
										src="resources/img/foodkonnekt-logo.png"></a>
								</div>
								<!--.logo-->
								<%@ include file="adminHeader.jsp"%>
							</div>
							<!--.row-->
						</div>
						<!--.inner-header-->
					</header>
					<!--#page-header-->

					<div id="page-content">
						<div class="outer-container">

							<div class="row">
								<div class="content-inner-container">
									<%@ include file="leftMenu.jsp"%>
									<div class="right-content-container">
										<div class="right-content-inner-container">
											<div class="content-header">
												<div class="all-header-title"></div>
												<!--.header-title-->
												<div class="content-header-dropdown"></div>
												<!--.content-header-dropdown-->
											</div>
											<!--.content-header-->
											<div class="orders-page-data">

                                                <div class="sd-orders-list-outbound">
                                                    <div class="sd-orders-list">
                                                        <div class="sd-orders-list-inbound">
                                                            <div class="orders-items-list-table">
                                                                <div class="search-container" style="margin-top: 10px">
                                                                    <div class="only-search-elements">
                                                                        <label>Search</label> 
                                                                        <input type="text" placeholder="Search Orders" id="search-inventory" class="searchq">
                                                                        <input type="button" value="Search" onClick="searchInventoryByDate()">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <br>
                                                            <div style="margin-left: -30%;"> <form:form  align="center" method="GET" enctype="multipart/form-data">
                                                            FROM &nbsp&nbsp&nbsp
                                                            <input type="date" id="startDate" name="startDate" value="${startDate}" style="width: 22%;"> 
                                                             &nbsp&nbsp&nbsp TO &nbsp&nbsp&nbsp
                                                            <input type="date" id="endDate" name="endDate" value="${endDate}" style="width: 22%;">&nbsp&nbsp
                                                            <div class="button left" style="margin-left: 78%;margin-top: -5%;">
                                                            
                                                            <input type="button" value="Submit" onClick="searchInventoryByDate()" style="width: 86%;">
                                                            </div>
                                                           
                                                            </form:form>
                                                            
                                                            <input type = "hidden" id="posId" value="${merchant.owner.pos.posId}">
                                                            
                                                            <c:if test="${merchant.owner.id==120}">
                                                            <form:form action="generateExcelForKidsCaters" method="post" id="downloadCSV">
		 														<div class="button left" style="margin-left: 87%;margin-top: -5%;">
		 															<input name="searchTxt" id="stxt" type="hidden" value="">
		   														    <input name="startDate" id="std" type="hidden" value="">
		    													    <input name="endDate" id="etd" type="hidden" value="">
																	<input id="submitId" type="button" onClick="generateExcel()" value="Download CSV">
																</div>
															</form:form>
															</c:if>
															
															<c:if test="${merchant.owner.id!=120}">
                                                            <form:form action="generateExcel" method="post" id="downloadCSV">
		 														<div class="button left" style="margin-left: 87%;margin-top: -5%;">
		 															<input name="searchTxt" id="stxt" type="hidden" value="">
		   														    <input name="startDate" id="std" type="hidden" value="">
		    													    <input name="endDate" id="etd" type="hidden" value="">
																	<input id="submitId" type="button" onClick="generateExcel()" value="Download CSV">
																</div>
															</form:form>
															</c:if>
															
															</div>
															
															  <div class="pj-preloader"></div>
                                                           <!--  <div><input type="button" value="GenerateExcel" onClick="generateExcel()" ></div> -->
                                                            <span style="color: red;  text-align: right;" id="orderMsgSpanId">${errorMessage}</span>
                                                            <!--.inventory-items-list-table-->
                                                            <table id="checkit-table" class="table header-table-orders">
                                                                <thead>
                                                                    <tr>
																		<th>Id</th>
                                                                        <th>Customer Name</th>
                                                                        <th>Date and Time</th>
                                                                        <th>Order Total</th>
                                                                        <th>Order Type</th>
                                                                        <th>Order Status</th>
                                                                        <th>Order Notes</th>
                                                                        <th>Payment Type</th>
                                                                        <th>View</th>
                                                                        <th>Action</th>
                                                                        
                                                                        
                                                                    </tr>
                                                                </thead>
                                                               
                                                            </table>
                                                        </div>
                                                        <!--.sd-orders-list-inbound-->
                                                    </div>
                                                    <!--.sd-orders-list-->
                                                </div>
                                                <!--.sd-orders-list-outbound-->

                                            </div>
                                            <!--.orders-page-data-->


                                        </div>
                                        <!--.right-content-inner-container-->
                                    </div>
                                    <!--.right-content-container-->
                                </div>
                                <!--.content-inner-container-->
                            </div>
                            <!--.row-->

                        </div>
                        <!--.outer-container-->
                    </div>
                    <!--#page-content-->

                    <%@ include file="adminFooter.jsp"%>
                    <!--#footer-container-->

                </div>
                <!--.max-row-->
            </div>
            <!--.inner-container-->
        </div>
        <!--.foodkonnekt .dashboard-->
    </div>
    <!--#page-container-->


    <script type="text/javascript">
                    var _gaq = _gaq || [];
                    _gaq.push([ '_setAccount', 'UA-36251023-1' ]);
                    _gaq.push([ '_setDomainName', 'jqueryscript.net' ]);
                    _gaq.push([ '_trackPageview' ]);

                    (function() {
                        var ga = document.createElement('script');
                        ga.type = 'text/javascript';
                        ga.async = true;
                        ga.src = ('https:' == document.location.protocol ? 'https://ssl'
                                : 'http://www')
                                + '.google-analytics.com/ga.js';
                        var s = document.getElementsByTagName('script')[0];
                        s.parentNode.insertBefore(ga, s);
                    })();
                </script>
	<!--CALENDAR MULTI-SELECT-->

	<!--OPENS DIALOG BOX-->
	<script src="resources/js/dialog-box/classie.js"></script>
	<script src="resources/js/dialog-box/modalEffects.js"></script>
	<script>
        var polyfilter_scriptpath = '/js/';
    </script>
	<!--OPENS DIALOG BOX-->

	<!-- <script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script> -->

	<script type="text/javascript">
 <!-------------------------------------->
 
	 function searchByDate() {
		 
		  var startDate = $("#startDate").val();
		  var endDate = $("#endDate").val();
		  
		  if(startDate>endDate){
			   //alert("End date should be greater the Start Date");
			   /* $('#checkit-table tbody').empty();
			   $("#endDate").val(startDate);
			   $("#startDate").val(endDate); */
			   
		   }else{
		    
		 if ($.fn.DataTable.isDataTable('#checkit-table') ) {
	           $('#checkit-table').DataTable().destroy();
		   }
		   $('#checkit-table tbody').empty();
		   
		   
		   
		   
	   var dt =$("#checkit-table").dataTable({
         "bProcessing": true,
         "bServerSide": true,
         "sort": "position",
         "bStateSave": false,
         "iDisplayLength": 10,
         "iDisplayStart": 0,
         "fnDrawCallback": function () {
             //Get page numer on client. Please note: number start from 0 So
         },         
         "sAjaxSource": "allOrdersDataTables?startDate="+startDate+"&endDate="+endDate,
         "aoColumns": [
			 { "mData": "id" },
             { "mData": "firstName" },
             { "mData": "createdOn" },
             { "mData": "orderPrice" },
             { "mData": "orderType" },
             { "mData": "status" },
             { "mData": "orderName" },
             { "mData": "paymentMethod" },
             
             {
                 "class":          "details-control",
                 "orderable":      false,
                 "mData":           "view",
                 "defaultContent": ""
             }, {
					"mData" : "action"
				}
             
             
             
         ]
         
     });
	   var detailRows = [];
     $('#checkit-table tbody').on( 'click', 'tr td.details-control', function () {
  	   var tr = $(this).closest('tr');
  	   var rowID=tr.attr('id')
  	   var table = $('#checkit-table').DataTable();
var rowData = table.row('#'+rowID).data();

var deliveryFeeDiv="";

if(rowData.orderType!='pickup' ||  rowData.orderType!='Pickup' && rowData.deliveryFee > 0 ){
	deliveryFeeDiv=deliveryFeeDiv+"<ul><li>Delivery Fee</li><li></li> <li>$ "+rowData.deliveryFee+"</li> </ul>";
}
//alert(rowData.orderItemViewVOs);
var items="";
$.each( rowData.orderItemViewVOs, function ( i ) {
	
	var modifiers=""; 
	
	$.each( rowData.orderItemViewVOs[i].itemModifierViewVOs, function ( j ) {
		modifiers=modifiers+"<ul><li>"+rowData.orderItemViewVOs[i].itemModifierViewVOs[j].modifiers.name+"</li><li>"+rowData.orderItemViewVOs[i].itemModifierViewVOs[j].quantity+"</li><li>$"+rowData.orderItemViewVOs[i].itemModifierViewVOs[j].modifiers.price.toFixed(2)+" </li></ul>";
	} );
	
	items=items+"<div class='order-details-items-quantity-price'><ul><li>"+rowData.orderItemViewVOs[i].itemVO.name+"</li><li>"+rowData.orderItemViewVOs[i].quantity+"</li><li>$"+rowData.orderItemViewVOs[i].itemVO.price.toFixed(2)+"</li></ul>"+modifiers+"</div>";
	
} );

var orderDiscountList="<ul><li>Discount Coupon</li>";
if(rowData.orderDiscountsList.length > 0){
$.each( rowData.orderDiscountsList, function ( i ) {
	if(i==0){
		orderDiscountList += "<li>"+rowData.orderDiscountsList[i].couponCode+"</li>";
		}else{
			orderDiscountList += "<ul><li>&nbsp;</li><li>"+rowData.orderDiscountsList[i].couponCode+"</li></ul>";
		}
} );
}
orderDiscountList += "</ul>";

  	   var res = rowID.split("_");
  	   rowID = res[1];
  	   var divId="modal-15-"+rowID;
  	   var convenienceFeeDiv="";
  	  var tip="";
  	  
  	  if(rowData.tipAmount>0){
  		  tip=tip+"<ul> <li>Tip</li> <li></li><li>$ "+rowData.tipAmount.toFixed(2)+"</li></ul>";
  	  }
  	   
  	  if(rowData.discount>0){
  		  tip=tip+"<ul> <li>Discount</li> <li></li><li>$ "+rowData.discount.toFixed(2)+"</li></ul>"+orderDiscountList;
  	  }
  	  if(rowData.convenienceFee==''){
  		  
  	  }else{
  		/* CHANGE Convenience to Online */
  		  convenienceFeeDiv=convenienceFeeDiv+"<ul> <li>Convenience Fee</li> <li></li><li>$ "+Number(rowData.convenienceFee).toFixed(2)+"</li></ul>";
  		convenienceFeeDiv=convenienceFeeDiv+"<ul> <li>Online Fee</li> <li></li><li>$ "+Number(rowData.convenienceFee).toFixed(2)+"</li></ul>";
  	  }
  	   
  	   $("#sd-dialog").append("<div id='sd-dialog'><div class='md-modal md-effect-14' id='modal-15-"+rowID+"'><div class='md-content'><h3>ORDER DETAILS</h3><div><div class='order-details-container'>"
  	   + "<span id='message' style='font-size: 15px; color: black;'>"+message+"</span><br>"
  	   +"<div class='order-details-header'><ul><li><span>Item</span></li><li><span>Quantity</span></li><li><span>Price</span></li></ul></div>"
  	   +items
  	   +"<div class='order-subtotal-tax-total'><ul><li>Subtotal</li><li>$"+Number(rowData.subTotal).toFixed(2)+"</li> </ul> "+convenienceFeeDiv+" "+deliveryFeeDiv+"<ul><li>Tax</li><li>$"+Number(rowData.tax).toFixed(2)+"</li> </ul>"+tip+" <ul> <li>Total</li><li>$"+rowData.orderPrice+" </li> </ul> </div><div class='order-details-order-notes'><textarea name='order-notes' cols='' rows='4' readonly='readonly'>"+rowData.orderName+" </textarea></div> </div><div class='clearfix'></div> <div class='button'><button class='white-button md-close' onClick='closeNavSignIn()'>Close</button></div> </div> </div> </div><div class='md-overlay'></div></div>");
  	   
  	   
  	   $("#modal-15-"+rowID).addClass('md-show');
     } );
    
     
     dt.on( 'draw', function () {
         $.each( detailRows, function ( i, id ) {
             $('#'+id+' td.details-control').trigger( 'click' );
         } );
     } );
     
     $(".white-button").click(function () {
  	  
  	   
        });
     
     
		   }  
     
 };
 function generateExcel(){
	 var searchTxt=$("#search-inventory").val();
	 var startDate = $("#startDate").val();
	 var endDate = $("#endDate").val();
	 //alert("aaaa");
	 if(startDate>endDate){
		 //  alert("End date should be greater the Start Date");
		   /* $('#checkit-table tbody').empty();
		   $("#endDate").val(startDate);
		   $("#startDate").val(endDate); */
		   
	   }else{
		   //alert(startDate);
		   document.getElementById('stxt').value=searchTxt;
		   document.getElementById('std').value=startDate;
		   document.getElementById('etd').value=endDate;
		   document.getElementById('downloadCSV').submit();
		   /* $.ajax({
               url : "generateExcel?searchTxt="+searchTxt+"&startDate="+startDate+"&endDate="+endDate,
               type : 'POST',
               dataType : 'json',
                data : orderJson, 
               contentType : 'application/json',
                     mimeType : 'text', 
                    success : function(statusValue) {
                    	
                    	
                    }
		   }) ; */
	   }
	 
 };
 
 
 function searchInventoryByDate() {
	
	 var searchTxt=$("#search-inventory").val();
	 var startDate = $("#startDate").val();
	 var endDate = $("#endDate").val();
	 
	 if(startDate>endDate){
		 //  alert("End date should be greater the Start Date");
		   /* $('#checkit-table tbody').empty();
		   $("#endDate").val(startDate);
		   $("#startDate").val(endDate); */
		   
	   }else{
		 $.fn.dataTable.ext.errMode = 'none';
     if ($.fn.DataTable.isDataTable('#checkit-table') ) {
         $('#checkit-table').DataTable().destroy();
      }

    $('#checkit-table tbody').empty();
    
    var dt =$("#checkit-table").dataTable({
        "bProcessing": true,
        "bServerSide": true,
        "sort": "position",
        "bStateSave": false,
        "iDisplayLength": 10,
        "iDisplayStart": 0,
        "fnDrawCallback": function () {
            //Get page numer on client. Please note: number start from 0 So
        },         
        "sAjaxSource": "searchOrderByTextAndDate?searchTxt="+searchTxt+"&startDate="+startDate+"&endDate="+endDate,
        "aoColumns": [
			{ "mData": "id" },
            { "mData": "firstName" },
            { "mData": "createdOn" },
            { "mData": "orderPrice" },
            { "mData": "orderType" },
            { "mData": "status" },
            { "mData": "orderName" },
            {"mData": "paymentMethod" },
            
            {
                "class":          "details-control",
                "orderable":      false,
                "mData":           "view",
                "defaultContent": ""
            }, {
				"mData" : "action"
			}
            
            
        ]
        
    });
	   var detailRows = [];
    $('#checkit-table tbody').on( 'click', 'tr td.details-control', function () {
 	   var tr = $(this).closest('tr');
 	   var rowID=tr.attr('id')
 	   var table = $('#checkit-table').DataTable();
var rowData = table.row('#'+rowID).data();

var deliveryFeeDiv="";

if(rowData.orderType!='pickup' &  rowData.orderType!='Pickup' ){
	deliveryFeeDiv=deliveryFeeDiv+"<ul><li>Delivery Fee</li><li></li> <li>$ "+rowData.deliveryFee+"</li> </ul>";
}
//alert(rowData.orderItemViewVOs);
var items="";
$.each( rowData.orderItemViewVOs, function ( i ) {
	
	var modifiers=""; 
	
	$.each(rowData.orderItemViewVOs[i].itemModifierViewVOs, function ( j ) {
		modifiers=modifiers+"<ul><li>"+rowData.orderItemViewVOs[i].itemModifierViewVOs[j].modifiers.name+"</li><li>"+rowData.orderItemViewVOs[i].itemModifierViewVOs[j].quantity+"</li><li>$"+rowData.orderItemViewVOs[i].itemModifierViewVOs[j].modifiers.price.toFixed(2)+" </li></ul>";
	} );
	
	items=items+"<div class='order-details-items-quantity-price'><ul><li>"+rowData.orderItemViewVOs[i].itemVO.name+"</li><li>"+rowData.orderItemViewVOs[i].quantity+"</li><li>$"+rowData.orderItemViewVOs[i].itemVO.price.toFixed(2)+"</li></ul>"+modifiers+"</div>";
	
} );
var orderDiscountList="<ul><li>Discount Coupon</li>";
if(rowData.orderDiscountsList.length > 0){
$.each( rowData.orderDiscountsList, function ( i ) {
	if(i==0){
		orderDiscountList += "<li>"+rowData.orderDiscountsList[i].couponCode+"</li>";
		}else{
			orderDiscountList += "<ul><li>&nbsp;</li><li>"+rowData.orderDiscountsList[i].couponCode+"</li></ul>";
		}
} );
}
orderDiscountList += "</ul>";
 	   var res = rowID.split("_");
 	   rowID = res[1];
 	   var divId="modal-15-"+rowID;
 	   var convenienceFeeDiv="";
 	  var tip="";
 	  
 	  if(rowData.tipAmount>0){
 		  tip=tip+"<ul> <li>Tip</li> <li></li><li>$ "+rowData.tipAmount.toFixed(2)+"</li></ul>"+orderDiscountList;
 	  }
 	   
 	  if(rowData.convenienceFee==''){
 		  
 	  }else{
 		 /* CHANGE Convenience to Online */
 		  convenienceFeeDiv=convenienceFeeDiv+"<ul> <li>Convenience Fee</li> <li></li><li>$ "+Number(rowData.convenienceFee).toFixed(2)+"</li></ul>";
 		 convenienceFeeDiv=convenienceFeeDiv+"<ul> <li>Online Fee</li> <li></li><li>$ "+Number(rowData.convenienceFee).toFixed(2)+"</li></ul>";
 	  }
 	   
 	   $("#sd-dialog").append("<div id='sd-dialog'><div class='md-modal md-effect-14' id='modal-15-"+rowID+"'><div class='md-content'><h3>ORDER DETAILS</h3><div><div class='order-details-container'><div class='order-details-header'><ul><li><span>Item</span></li><li><span>Quantity</span></li><li><span>Price</span></li></ul></div>"+items+"<div class='order-subtotal-tax-total'><ul><li>Subtotal</li><li>$"+Number(rowData.subTotal).toFixed(2)+"</li> </ul> "+convenienceFeeDiv+" "+deliveryFeeDiv+"<ul><li>Tax</li><li>$"+Number(rowData.tax).toFixed(2)+"</li> </ul>"+tip+" <ul> <li>Total</li><li>$"+rowData.orderPrice+" </li> </ul> </div><div class='order-details-order-notes'><textarea name='order-notes' cols='' rows='4' readonly='readonly'>"+rowData.orderName+" </textarea></div> </div><div class='clearfix'></div> <div class='button'><button class='white-button md-close' onClick='closeNavSignIn()'>Close</button></div> </div> </div> </div><div class='md-overlay'></div></div>");
 	   
 	   
 	   $("#modal-15-"+rowID).addClass('md-show');
    } );
   
    
    dt.on( 'draw', function () {
        $.each( detailRows, function ( i, id ) {
            $('#'+id+' td.details-control').trigger( 'click' );
        } );
    } );   
 }	 
 };

 </script>

	<script type="text/javascript">
    /*  $(document).ready(function() {
        var table = $('#checkit-table').DataTable();
        $(".searchq").keyup(function() {
             table.search( this.value).draw();
         } );
     }); */
     $(document).ready(function(){
       $('input[type=search]').each(function(){
         $(this).attr('placeholder', "Search");
       });
     });
     $("#generateButton").click(function () {
       $("#searchForm").submit();
     });
     $("#generateButton").click(function () {
         $("#searchForm").submit();
       });
     
     
 </script>
	<script>
    var cancelOrderId;
    var deliveryTime;
    var orderStatus;
$(function () {
    $(".custom-sd-button-dialog").click(function () {
      //  alert($(this).attr("orderAttr"));
    });
    
    $("#search-inventory").keyup(function() {
       var searchTxt=$(this).val();
       var startDate = $("#startDate").val();
  	 var endDate = $("#endDate").val();
  	  $.fn.dataTable.ext.errMode = 'none';
       if ($.fn.DataTable.isDataTable('#checkit-table') ) {
           $('#checkit-table').DataTable().destroy();
        }

      $('#checkit-table tbody').empty();
      
      var dt =$("#checkit-table").dataTable({
          "bProcessing": true,
          "bServerSide": true,
          "sort": "position",
          "bStateSave": false,
          "iDisplayLength": 10,
          "iDisplayStart": 0,
          "fnDrawCallback": function () {
              //Get page numer on client. Please note: number start from 0 So
          },         
          /* "sAjaxSource": "searchOrderByText?searchTxt="+searchTxt, */
        	"sAjaxSource": "searchOrderByTextAndDate?searchTxt="+searchTxt+"&startDate="+startDate+"&endDate="+endDate,
          "aoColumns": [
			  { "mData": "id" },
              { "mData": "firstName" },
              { "mData": "createdOn" },
              { "mData": "orderPrice" },
              { "mData": "orderType" },
              { "mData": "status" 	},
              { "mData": "orderName" },
              {"mData": "paymentMethod" },
              
              {
                  "class":          "details-control",
                  "orderable":      false,
                  "mData":           "view",
                  "defaultContent": ""
              }, {
					"mData" : "action"
				}
             
          ]
          
      });
	   var detailRows = [];
      $('#checkit-table tbody').on( 'click', 'tr td.details-control', function () {
   	   var tr = $(this).closest('tr');
   	   var rowID=tr.attr('id')
   	   var table = $('#checkit-table').DataTable();
var rowData = table.row('#'+rowID).data();

var deliveryFeeDiv="";

if(rowData.orderType!='pickup' &  rowData.orderType!='Pickup' ){
	deliveryFeeDiv=deliveryFeeDiv+"<ul><li>Delivery Fee</li><li></li> <li>$ "+rowData.deliveryFee+"</li> </ul>";
}
//alert(rowData.orderItemViewVOs);
var items="";
$.each( rowData.orderItemViewVOs, function ( i ) {
	
	var modifiers=""; 
	
	$.each( rowData.orderItemViewVOs[i].itemModifierViewVOs, function ( j ) {
		modifiers=modifiers+"<ul><li>"+rowData.orderItemViewVOs[i].itemModifierViewVOs[j].modifiers.name+"</li><li>"+rowData.orderItemViewVOs[i].itemModifierViewVOs[j].quantity+"</li><li>$"+rowData.orderItemViewVOs[i].itemModifierViewVOs[j].modifiers.price.toFixed(2)+" </li></ul>";
	} );
	
	items=items+"<div class='order-details-items-quantity-price'><ul><li>"+rowData.orderItemViewVOs[i].itemVO.name+"</li><li>"+rowData.orderItemViewVOs[i].quantity+"</li><li>$"+rowData.orderItemViewVOs[i].itemVO.price.toFixed(2)+"</li></ul>"+modifiers+"</div>";
	
} );
var orderDiscountList="<ul><li>Discount Coupon</li>";
if(rowData.orderDiscountsList.length > 0){
$.each( rowData.orderDiscountsList, function ( i ) {
	if(i==0){
		orderDiscountList += "<li>"+rowData.orderDiscountsList[i].couponCode+"</li>";
		}else{
			orderDiscountList += "<ul><li>&nbsp;</li><li>"+rowData.orderDiscountsList[i].couponCode+"</li></ul>";
		}
} );
}
orderDiscountList += "</ul>";
   	   var res = rowID.split("_");
   	   rowID = res[1];
   	   var divId="modal-15-"+rowID;
   	   var convenienceFeeDiv="";
   	  var tip="";
   	  
   	  if(rowData.tipAmount>0){
   		  tip=tip+"<ul> <li>Tip</li> <li></li><li>$ "+rowData.tipAmount.toFixed(2)+"</li></ul>"+orderDiscountList;
   	  }
   	   
   	  if(rowData.convenienceFee==''){
   		  
   	  }else{
   		/* CHANGE Convenience to Online */
   		  convenienceFeeDiv=convenienceFeeDiv+"<ul> <li>Convenience Fee</li> <li></li><li>$ "+Number(rowData.convenienceFee).toFixed(2)+"</li></ul>";
   		convenienceFeeDiv=convenienceFeeDiv+"<ul> <li>Online Fee</li> <li></li><li>$ "+Number(rowData.convenienceFee).toFixed(2)+"</li></ul>";
   	  }
   	   
   	   $("#sd-dialog").append("<div id='sd-dialog'><div class='md-modal md-effect-14' id='modal-15-"+rowID+"'><div class='md-content'><h3>ORDER DETAILS</h3><div><div class='order-details-container'><div class='order-details-header'><ul><li><span>Item</span></li><li><span>Quantity</span></li><li><span>Price</span></li></ul></div>"+items+"<div class='order-subtotal-tax-total'><ul><li>Subtotal</li><li>$"+Number(rowData.subTotal).toFixed(2)+"</li> </ul> "+convenienceFeeDiv+" "+deliveryFeeDiv+"<ul><li>Tax</li><li>$"+Number(rowData.tax).toFixed(2)+"</li> </ul>"+tip+" <ul> <li>Total</li><li>$"+rowData.orderPrice+" </li> </ul> </div><div class='order-details-order-notes'><textarea name='order-notes' cols='' rows='4' readonly='readonly'>"+rowData.orderName+" </textarea></div> </div><div class='clearfix'></div> <div class='button'><button class='white-button md-close' onClick='closeNavSignIn()'>Close</button></div> </div> </div> </div><div class='md-overlay'></div></div>");
   	   
   	   
   	   $("#modal-15-"+rowID).addClass('md-show');
      } );
     
      
      dt.on( 'draw', function () {
          $.each( detailRows, function ( i, id ) {
              $('#'+id+' td.details-control').trigger( 'click' );
          } );
      } );
      
      
     
     });
});


var paymentMethod;
var orderId;
var orderPosId;
var status;
var orderAvgChangeTime=-1;
function updateOrderStatus(id,posOrderId, orderStatus) {
	var orderTypeArray = orderStatus.split("_");
	status = orderTypeArray[0];
	var orderType = orderTypeArray[1];
	var avgTime = orderTypeArray[2];
	paymentMethod = orderTypeArray[3];
	orderId = id;
	orderPosId = posOrderId;
	orderAvgChangeTime=-1;
	if (status == "accept" || status == "updateOrderTime") {
	  $("#para").text("change "+orderType+" time")
	  $("#para").append("<br><input type='number' id='changeTime' onchange='changeTime(this.value)' name='ccNumber' max='999' min='0' value="+avgTime+" oninput='validity.valid||(value='');'>")
			  jQuery('#confirmModal_ex25').click();
	}
	
	else if(status == "cancel"){
		  $("#cancelbutton").empty();
		  $("#cancelbutton").append("Are you sure you want to Cancel this order" )
		  jQuery('#confirmModal_ex26').click();	
		
	}
	else if(status == "decline"){
		$("#paraDecline").empty();
		$("#paraDecline").append("<div id='declineDiv'><label for='c1'><input type='checkbox' name='reason[]' id='c1' value='Kitchen is out of stock'> Kitchen is out of stock</label><br>"
				+"<label for='c2'><input type='checkbox' name='reason[]' id='c2' value='Kitchen hours are not open'> Kitchen hours are not open</label><br>"
                +"<label for='c3'><input type='checkbox' name='reason[]' id='c3' value='We are not delivering today'> We are not delivering today</label><br>"
                +"<label for='c4'><input type='checkbox' name='other' class='otherBox' onclick='showOtherTextArea()' name='colorCheckbox' value='red'> Others</label>"
                +"<div class='otherTextBoxDiv' style='display:none;'><input type='text' class='otherTextBoxClass' id='otherTextBox' onkeyup='myFuction(this.value)'></div></div>")		
		 jQuery('#confirmModal_ex23').click();
	}
}
function changeTime(time){
	if(time!=null && time!="")
		orderAvgChangeTime=time;
}
function acceptOrder(){
	$('.pj-preloader').css('display','block');
	var orderAvgTime = $("#changeTime").val();
	var postId="${merchant.owner.pos.posId}";
	if(orderAvgChangeTime!=-1)
	orderAvgTime = orderAvgChangeTime;
	
	var reason = "";
	var inputElements = document.getElementsByName('reason[]');
		for (var i = 0; i < inputElements.length; ++i) {
			if (inputElements[i].checked) {
				reason += inputElements[i].value + ", ";
			}
		}
	reason += otherTextBoxReason;
	var url="";
	if(postId!=1)
		{
		url ="orderConfirmationById?orderId="
			+ orderId
			+ "&type="
			+ status
			+ "&changedOrderAvgTime="
			+ orderAvgTime
			+ "&reason="
			+ reason;
		}else if(postId==1)
			{
			url ="orderConfirmation?orderId="
				+ orderPosId
				+ "&type="
				+ status
				+ "&reason="
				+ reason;
			}
		}
	 $.ajax({
         type: 'GET',
     	url :url,
         success:function(data){
        	 $('.pj-preloader').css('display','none');
        	 $("#paraStatus").append("<h3 bind='greeting' style='margin-top: -48px;'>"+data+"</h3>");	
        	 jQuery('#confirmModal_ex24').click();
         },
  	  error: function () {
  		  
        }
      }); 
}

function selectChange(){
	$("option:selected").prop("selected", false)
}

 function showOtherTextArea(){
	 var ch = $(".otherBox").is(":checked")
	 if (ch) {
		$(".otherTextBoxDiv").css("display", "block");
	 } else {
		$(".otherTextBoxDiv").css("display", "none");
	 }
 } 
 
 var otherTextBoxReason = "";
 function myFuction(name){
	 otherTextBoxReason = name;
 }
 
 function okFunction(){
	 location.reload();
 }


 function cancel(){
	$('.pj-preloader').css('display','block');	
	 $.ajax({
			 
	        type: 'GET',
	     	url :"chargeRefundByOrderIdOnAdmin?orderId="+orderId,
	         success:function(data){
        	 $('.pj-preloader').css('display','none');
        	 $("#paraStatus").append("<h3 bind='greeting' style='margin-top: -48px;'>"+data+"</h3>");	
        	 jQuery('#confirmModal_ex24').click();
         },
  	  error: function () {
        }
      }); 
	}

</script>
	<script>

	
		 function myFunction(rowId) {
			
			 if (document.getElementById('myDIV'+rowId).style.display == 'none') {
				    event.target.innerText = 'Hide'
				    document.getElementById('myDIV'+rowId).style.display = ''
				  } else {
				    event.target.innerText = 'Show'
				    document.getElementById('myDIV'+rowId).style.display = 'none'
				  }
		} 
	</script> 
</body>
</html>
