<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="true"%>
<!doctype html>
<html class="no-js" lang="en">
<head>

<title>FoodKonnekt | Dashboard</title>
<!--CALLING STYLESHEET STYE.CSS-->
<link href="resources/css/bootstrap.min.css" rel="stylesheet"
	type="text/css" />
<link href="resources/css/bootstrap-multiselect.css" rel="stylesheet"
	type="text/css" />
<!--CALLING STYLESHEET STYE.CSS-->
<link rel="stylesheet" href="resources/css/style.css">
<!--CALLING STYLESHEET STYLE.CSS-->

<!--CALLING GOOGLE FONT OPEN SANS-->
<link
	href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic'
	rel='stylesheet' type='text/css'>
<!--CALLING GOOGLE FONT OPEN SANS-->

<!--CALLING FONT AWESOME-->
<link rel="stylesheet" href="resources/css/font-awesome.css">
<!--CALLING FONT AWESOME-->

<!--OPENS DIALOG BOX-->
<link rel="stylesheet" type="text/css"
	href="resources/css/dialog-box/component.css" />
<!--OPENS DIALOG BOX-->

<!--CALLING PRODUCTS TABS-->
<link rel='stylesheet' type='text/css'
	href='resources/css/products-tabs/opentabby.css' />
<!--CALLING PRODUCTS TABS-->
<!-- <link rel="stylesheet" type="text/css"
	href="https://cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css"> -->

<script type="text/javascript"
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
<script src='resources/js/products-tabs/opentabby.js'></script>
<script src="resources/js/popModal.js"></script>
<script type="text/javascript" src="resources/js/bootstrap.min.js"></script>
<script src="resources/js/bootstrap-multiselect.js"
	type="text/javascript"></script>

<style>
.errorClass {
	border: 1px solid red;
}
</style>
<style>
/* The container */
.radiocontainer {
	display: block;
	position: relative;
	padding-left: 35px;
	margin-bottom: 12px;
	cursor: pointer;
	font-size: 18px;
	-webkit-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	user-select: none;
}

/* Hide the browser's default radio button */
.radiocontainer input {
	position: absolute;
	opacity: 0;
	cursor: pointer;
}

/* Create a custom radio button */
.checkmark {
	position: absolute;
	top: 0;
	left: 0;
	height: 25px;
	width: 25px;
	background-color: #eee;
	border-radius: 50%;
}

/* On mouse-over, add a grey background color */
.radiocontainer:hover input ~ .checkmark {
	background-color: #ccc;
}

/* When the radio button is checked, add a blue background */
.radiocontainer input:checked ~ .checkmark {
	background-color: orange;
}

/* Create the indicator (the dot/circle - hidden when not checked) */
.checkmark:after {
	content: "";
	position: absolute;
	display: none;
}

/* Show the indicator (dot/circle) when checked */
.radiocontainer input:checked ~ .checkmark:after {
	display: block;
}

/* Style the indicator (dot/circle) */
.radiocontainer .checkmark:after {
	top: 9px;
	left: 9px;
	width: 8px;
	height: 8px;
	border-radius: 50%;
	background: white;
}

.pj-preloader {
	display: none;
	position: absolute;
	height: 383px;;
	width: 940px;
	background: url("resources/img/spinner.gif") no-repeat scroll center
		center rgba(153, 153, 153, 0.3);
	z-index: 9999;
	left: 260;
	position: absolute;
	top: 224px;
}
</style>

<script type="text/javascript">
  var itemModifierGroupId=""; 
  var sortOrder="";
  function cancelButton(){
	  $(".confirmModal").hide();
	}
  
  function yesButton(){
  	   $(".confirmModal").hide();
   	   
	   	 $('.pj-preloader').css('display','block');
       $.ajax({
            type: 'GET',
            url: "updateItemModifierGroupSortOrderById?itemModifierGroupId="+itemModifierGroupId+"&sortOrder="+sortOrder+"&action=update",
            success:function(data){
           	 
           	 $('.pj-preloader').css('display','none');
           	 if(data=='succuss'){
           		// alert(data);
           		 //$('.pj-preloader').css('display','none');
           		 $("#errorBox").html("");
           		window.location='category';
           	 }else{
           		// alert(data);
           		/* $('#menuOrdr option').prop('selected', function() {
                     return this.defaultSelected;
                }); */
           		
           		
           		
           		//$("#menuOrdr").css("border-color", "red")
           		jQuery('#categoryOrderPopUp').click();
           		
           		
           		
           		 //$("#errorBox").html("This order is already selected .Choose another one");
           	 }
           	
            }
         });
	   	   
 }
  </script>



</head>
<body>
	<div id="page-container">
		<div class="foodkonnekt merchant">
			<div class="inner-container">
				<div class="max-row">

					<header id="page-header">
						<div class="inner-header">
							<div class="row">
								<div class="logo">
									<a href="adminHome" title="FoodKonnekt Dashboard" class="logo"><img
										src="resources/img/foodkonnekt-logo.png"></a>
								</div>
								<!--.logo-->
								<%@ include file="adminHeader.jsp"%>
							</div>
							<!--.row-->
						</div>
						<!--.inner-header-->
					</header>
					<!--#page-header-->

					<div id="page-content">
						<div class="outer-container">
							<div class="row">
								<div class="content-inner-container">
									<%@ include file="leftMenu.jsp"%>
									<div class="right-content-container">
										<div class="right-content-inner-container">

											<div class="content-header">
												<div class="all-header-title"></div>
												<!--.header-title-->
												<div class="content-header-dropdown"></div>
												<!--.content-header-dropdown-->
											</div>
											<!--.content-header-->

											<div class="merchant-page-data">
												<div class="merchant-actions-outbound">
													<div class="merchat-coupons-container">

														<div class="coupons-navigation">
															<ul>
																<li><a href="inventory">Items</a></li>
																<li><a href="category">Categories</a></li>
																<li ><a href="modifierGroups">Modifier Groups</a></li>
																<li class="current-menu-item"><a href="modifiers">Modifiers</a></li>
															</ul>
														</div>
														<!--.coupons-navigation-->

														<div class="delivery-zones-content-container">

														 <form:form method="POST" action="updateModifier"
																modelAttribute="Modifiers" id="updateModifierForm">
																<form:hidden path="id" value="${modifiers.id}"/>
																<div class="adding-products-form">
																	<br><br>
																	<div>
																		<label>Modifiers Name:</label>
																		<form:input path="name" placeholder="${modifiers.name}"
																			value="${modifiers.name}" />
																	</div>
																	<br>

																
																 <div>
																<label> Modifiers price:</label>
																<form:input path="price" placeholder="${modifiers.price}"
																			value="${modifiers.price}" />
																	</div>
																	<br>
																	<label>Mapped ModifierGroup:</label><div>
																<form:select id="mapPizzaId"  class="multiselect dropdown-toggle btn btn-default"
																	multiple="multiple" path="modifiergroupId">
																	<c:forEach items="${modifierGrouplist}" var="modifierGrouplist"
																		varStatus="status">
																		<c:if test="${modifierGrouplist != null && modifierGrouplist != ''}">
																								<c:choose>
																									<c:when test="${modifierGrouplist.status==1}">
																										<option value="${modifierGrouplist.modifierGroup.id}" selected>${modifierGrouplist.modifierGroup.name}</option>

																									</c:when>
																									<c:otherwise>
																										<option value="${modifierGrouplist.modifierGroup.id}">${modifierGrouplist.modifierGroup.name}</option>
																									</c:otherwise>
																								</c:choose>
																							</c:if>
																	</c:forEach>
																</form:select>
																</div>
																<br>
																
																<label>UnMapped ModifierGroup:</label><div>
																<form:select id="unmapPizzaId"  class="multiselect dropdown-toggle btn btn-default"
																	multiple="multiple" path="unmapmodifiergroupId">
																	<c:forEach items="${unmapModifierGroup}" var="unmapModifierGroup"
																		varStatus="status">
																		<option value="${unmapModifierGroup.id}" >${unmapModifierGroup.name}</option>
																	</c:forEach>
																</form:select>
																</div>
																
																
																
																	
																</div>

																<div class="button left">
																  <input type="button" id="updateModifiersButton" value="Save">&nbsp;&nbsp; 
																 <input type="button" value="Cancel" id="updateItemCancelButton">
																</div>
															</form:form> 
														</div>
														<!--.coupons-content-container-->
														<div id="errorDiv" style="color: red"></div>
													</div>
													<!--.merchat-coupons-container-->
												</div>
												<!--.merchant-actions-outbound-->
											</div>
											<!--.merchant-page-data-->

										</div>
										<!--.right-content-inner-container-->
									</div>
								</div>

								<div class="methodsdata"></div>


							</div>
							<!--.row-->

						</div>
						<!--.outer-container-->

					</div>
					<!--#page-content-->
					<%@ include file="adminFooter.jsp"%>
				</div>
				<!--.max-row-->
			</div>
			<!--.inner-container-->
		</div>
		<!--.foodkonnekt .dashboard-->
	</div>
	<!--#page-container-->


<script>
        $(".openTabby").openTabby();
        function isNumberKey(evt)
        {
      	  var target = evt.target || evt.srcElement; // IE
      	  var charCode = (evt.which) ? evt.which : event.keyCode
      	    var id = target.id;
      	    var data=document.getElementById(id).value;
      	  
      	  
      	  if(evt.which == 8 || evt.which == 0){
                return true;
            }
      	  
      	 if (charCode > 31 && (charCode < 48 || charCode > 57)){
             return false;
      		}
      	 
            if(evt.which < 46 || evt.which > 59) {
                return false;
                //event.preventDefault();
            } // prevent if not number/dot

            if(evt.which == 46 && data.indexOf('.') != -1) {
                return false;
                //event.preventDefault();
            }
        }
    </script>

	<!-- <script
		src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script> -->
		
		<!-- select with checkboxes -->
	
	
   
    
    <script type="text/javascript">
    function loadModifiers(selected){
    	
   	 //$('#modis').multiselect('rebuild');
   	
   	
    	var modifierGroupId="${modifierGroup.id}";
    	
    	 $.ajax({
    		 
             url : "getModifiersByModifierGroupIds?modifierGroupIds=" + selected+"&itemId="+itemId,
             type : "GET",
             contentType : "application/json; charset=utf-8",
             success : function(data) {
            	 //$("#modis").multiselect();
            	 
             	$('#modis').find('option').remove();
             	$('#modis').find('optgroup').remove();
               var itemModiifers = data;
               
               for (i = 0; i < itemModiifers.length; i++) {
             	  var itemModifier=itemModiifers[i];
             	 var modifierGroupName=itemModifier[0].modifierGroupName;
                 
             	$( "#modis" ).append("<optgroup label='"+modifierGroupName+"'>");
             	 for (k = 0; k < itemModifier.length; k++) {
             		var itemModis=itemModifier[k];
                    
             	  if(itemModis.modifierStatus==1){
             	  $( "#modis" ).append("<option value='"+itemModis.id+"' selected>"+itemModis.modifierNmae+"</option>");
             	  }else{
             		 $( "#modis" ).append("<option value='"+itemModis.id+"'>"+itemModis.modifierNmae+"</option>");
             	  }
               }
             	$( "#modis" ).append("</optgroup>");
               }
               
              	$('#modis').multiselect('rebuild');
              	$('#modis').multiselect('refresh'); 
              	
               	
             },
             error : function() {
                 console.log("Error in category wise inventory");
              }
           })
    	
    }
        $(function () {
            $('#md_grps').multiselect({
                includeSelectAllOption: true
            });
            
            $('#days').multiselect({
                includeSelectAllOption: true
            });
            
            $('#modis').multiselect({
            	includeSelectAllOption: true
            }); 
            
            $('#mapPizzaId').multiselect({
  			  maxHeight: 450 ,
  	          includeSelectAllOption: true
  	     }); 
  		 
  		 $('#unmapPizzaId').multiselect({
  			  maxHeight: 450 ,
  	          includeSelectAllOption: true
  	     }); 
            //$("#modis").multiselect();
            
            
            
            $('#md_grps').change(function(e) {
                var selected = $(e.target).val();
                
                loadModifiers(selected);
            });

            
            	


            /* $('#md_grps').load(function(e) {
                var selected = $(e.target).val();
                
                loadModifiers(selected);
            }); */
            
           
            
            
            
           /*  $('#modis').multiselect({
                includeSelectAllOption: true
            }); */
            
            
            
            /* $('#md_grps').click(function () {
                var selected = $("#md_grps option:selected");
                var message = "";
                selected.each(function () {
                    message += $(this).text() + " " + $(this).val() + "\n";
                });
                alert(message);
            }); */
        });
    </script>
	
	<!-- select with checkboxes -->
	<script type="text/javascript">
	var status=0;
	var modifier_Status=[];
	  function checkModifierLimit(modi_grp_id){
		  
		  
		  
		  var modigrp_id="modigrp_id_"+modi_grp_id
		  var modigrp_modi_id= "modigrp_modi_id_"+modi_grp_id
		  
		  var modifierMaxLimit=document.getElementById(modigrp_id).textContent;
		  var modifierLimit=document.getElementById(modigrp_modi_id).value;
		   
		  if(Number(modifierMaxLimit)< Number(modifierLimit)){
			  var index = modifier_Status.indexOf(modi_grp_id);
			  if (index == -1) {
			  modifier_Status.push(modi_grp_id);
			  }
		  $("#"+modigrp_modi_id).css('border-color', 'red');
		  $("#errorMessage_"+modi_grp_id).html("Modifier limit exceeded");
		  }else{
			  
			  var index = modifier_Status.indexOf(modi_grp_id);
			 
			  if (index > -1) {
				  modifier_Status.splice(index, 1);
				}
			  $("#"+modigrp_modi_id).css('border-color', '');
			  $("#errorMessage_"+modi_grp_id).html("");
		  }
		
		 
		 
		  
	  }
	  
         $(document).ready(function() {
        	 
        	
                 var selected = $('#md_grps').val();
                 //loadModifierGroup(selected); 
             
        	
            /* var table = $('#example').DataTable(); */
         });
         /* $(document).ready(function(){
           $('input[type=search]').each(function(){
             $(this).attr('placeholder', "Search Items");
           });
         }); */
     </script>
	<script type="text/javascript">
     
	
	$(document).ready(function() {
        $("#updateModifiersButton").click(function () {
      	  
          
          	$("#updateModifierForm").submit();
        });
        
        
        $("#updateItemCancelButton").click(function() {
          window.location="modifiers"
        });
    });
          
	var status= "${sessionScope.pizzaShowStatus}";
 	$(document).ready(function() {
 		 if(status=="success"){
 			 $("#pizzaTab").css('display','block');
 		 }
 	 });  
 	
 	
 	$(document).ready(function() {
 		 $('#modifiersId').multiselect({
 			  maxHeight: 450 ,
 	          includeSelectAllOption: true
 	     }); 
 	}); 
      
     
  </script>

</body>
</html>
