<%@page session="true"%>
<!doctype html>
<html class="no-js" lang="en">
    <!--CALLING STYLESHEET STYE.CSS-->
    <link rel="stylesheet" href="resources/css/style.css"><%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="true"%>
  <head>
    <title>FoodKonnekt | Dashboard</title>
    <!--CALLING STYLESHEET STYE.CSS-->
    <link rel="stylesheet" href="resources/css/style.css">
    <!--CALLING STYLESHEET STYLE.CSS-->
    
    <!--CALLING GOOGLE FONT OPEN SANS-->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    <!--CALLING GOOGLE FONT OPEN SANS-->
    
    <!--CALLING FONT AWESOME-->
    <link rel="stylesheet" href="resources/css/font-awesome.css">
    <!--CALLING FONT AWESOME-->
    
    <!--CALLING CHECK ALL FUNCTIONALITY-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="resources/js/checkall/jquery.checkall.js"></script>
    <!--CALLING CHECK ALL FUNCTIONALITY-->

    <!--OPENS DIALOG BOX-->
    <link rel="stylesheet" type="text/css" href="resources/css/dialog-box/component.css" />
    <!--OPENS DIALOG BOX-->
    
    <!--ACCORDION FOR MENU-->
    <script src="resources/js/accordion/paccordion.js"></script>
    <!--ACCORDION FOR MENU-->
     <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css">
     <script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
       <style type="text/css">
      div#example_paginate {
        display: block;
       }
      div#example_filter {
         display: none;
      }
      div#example_length {
         display: block;
      }
      
      div#example1_filter {
         display: none;
      }
      
      example1_filter
    
      input[type="search"]{
        display:none;
        max-width: 300px;
        width: 100%;
        outline: 0;
        border: 1px solid rgb(169, 169, 169);
        padding: 11px 10px;
        border-radius: 6px;
        margin-bottom: 7px;
        placeholder:Search Items;
    }
    
    
.pj-preloader {
    display: none;
    position: absolute;
    height: 383px;;
    width: 940px;
    background: url("resources/img/spinner.gif") no-repeat scroll center center rgba(153, 153, 153, 0.3);
    z-index: 9999;
    left: 260;
    position: absolute;
    top: 224px;
}
    </style>


   <script type="text/javascript">

    //Plug-in to fetch page data 
    jQuery.fn.dataTableExt.oApi.fnPagingInfo = function ( oSettings )
    {
    	
        return {
            "iStart":         oSettings._iDisplayStart,
            "iEnd":           oSettings.fnDisplayEnd(),
            "iLength":        oSettings._iDisplayLength,
            "iTotal":         oSettings.fnRecordsTotal(),
            "iFilteredTotal": oSettings.fnRecordsDisplay(),
            "iPage":          oSettings._iDisplayLength === -1 ?
                0 : Math.ceil( oSettings._iDisplayStart / oSettings._iDisplayLength ),
            "iTotalPages":    oSettings._iDisplayLength === -1 ?
                0 : Math.ceil( oSettings.fnRecordsDisplay() / oSettings._iDisplayLength )
        };
    };

$(document).ready(function() {
    $.fn.dataTableExt.sErrMode = 'throw';
});

function loadPizzaTamplate(){
		  
	
   
   $("#LoadingImage").show();
	   console.log("THIS IS start");
	   if ( $.fn.DataTable.isDataTable('#example') ) {
           $('#example').DataTable().destroy();
         }
	   $('#example tbody').empty();
	   var pizzaSize=$('#pizzaSizeId').val();
	   var outer = [];
	   $.ajax({
	       url : "filterTamplateByPizzaSize?pizzaSizeId=0",
	       type : "GET",
	       contentType : "application/json; charset=utf-8",
	       success : function(data) {
	         var jsonOutpt = JSON.stringify(data);
	         var menuItems = JSON.parse(jsonOutpt);
	         console.log(menuItems)
	         $("#LoadingImage").hide();
	         /* $("#searchbox").css("display","block"); */
	         document.getElementById("search-inventory").disabled=false;
	         document.getElementById("pizzaSizeId").disabled=false;
	         $.each(menuItems, function( index, value ) {
	               var inner = [];
	               
	               inner.push(value.name);
	               inner.push(value.size);
	               inner.push(value.price);
	               inner.push(value.topping); 
	               inner.push(value.status);
	               if(value.categoryName !=null)
	               inner.push(value.categoryName);
	               else
	               inner.push("");   
	               inner.push(value.action);
	              
	               outer.push(inner);
	            });
	         $('#example').DataTable( {
	             data: outer,
	             columns: [
	                 
	                 { title: "Name" },
	                 { title: "Size" },
	                 { title: "Price" },
	                 { title: "Topping" }, 
	                 { title: "Status" },
	                 { title: "Category Name" },
	                 { title: "Action" },
	                 
	             ]
	         } );
	        var table =$('#example').DataTable();
	        $(".searchq").keyup(function() {
	              table.search( this.value).draw();
	        } );
	       },
	       error : function() {
	           console.log("Error in category wise inventory");
	        }
	       
	       
	     })    
	    
}

$(document).ready( function setUp() {
	/* $("#searchbox").css("display","none"); */
	 document.getElementById("search-inventory").disabled=true;
	document.getElementById("pizzaSizeId").disabled=true;
	loadPizzaTamplate();
	

}); 
</script>

 <script>

        $(document).ready(function() {
        	
        
        	
        $("#example").on("click", ".nav-toggle", function(){
          var tamplateId=$(this).attr('itmId');
          var thishtml = jQuery(this).html();
          
          var itemStatus;
          if(thishtml =='Active'){
        	  itemStatus=0;
            jQuery(this).html('InActive');
          }
          if(thishtml =='InActive'){
        	  itemStatus=1;
            jQuery(this).html('Active');
          }
          $('.pj-preloader').css('display','block');
          $.ajax({
               type: 'GET',
               url: "updateTamplateStatusById?tamplateId="+tamplateId+"&itemStatus="+itemStatus,
               success:function(data){
                 $('.pj-preloader').css('display','none');
               }
            }); 
          
          });
        });
        </script>
  </head>
  <body>
    <div id="page-container">
        <div class="foodkonnekt inventory">
            <div class="inner-container">
                <div class="max-row">
                    
                    <header id="page-header">
                        <div class="inner-header">
                            <div class="row">
                                
                                <div class="logo">
                                     <a href="adminHome" title="FoodKonnekt Dashboard" class="logo"><img src="resources/img/foodkonnekt-logo.png"></a>
                                </div><!--.logo-->
                                <%@ include file="adminHeader.jsp" %> 
                                
                            </div><!--.row-->
                        </div><!--.inner-header-->
                    </header><!--#page-header-->
                    
                    <div id="page-content">
                        <div class="outer-container">
                        
                            <div class="row">
                                <div class="content-inner-container">
                                    
                                     <%@ include file="leftMenu.jsp"%>
                                
                                    <div class="right-content-container">
                                    
                                        <div class="right-content-inner-container">
                                        
                                            <div class="content-header">
                                                <div class="all-header-title">
                                                </div><!--.header-title-->
                                            </div><!--.content-header-->
                                            
                                            <div class="merchant-page-data">
                                        
                                                <div class="merchant-actions-outbound">
                                                    <div class="merchat-coupons-container">
                                                            <!-- <main> -->
                                                                 
                                                            <div class="coupons-navigation">
                                                               
                                                                                                                       <ul>
                                                                        <li class="current-menu-item"><a href="pizzaTamplate">Template</a></li>
                                                                        <li><a href="pizzaTopping">Topping</a></li>
                                                                        <li id="pizzaCrust"><a href="pizzaCrust">Crust</a></li> 
                                                                        <li id="pizzaSize"><a href="pizzaSize">Size</a></li>
                                                                        <li id="taxmap"><a href="templateTaxMap">Tax Map</a></li>
                                                                        <li><a href="pizzaCategory">Pizza Category</a></li>
                                                                    </ul>
                                                        </div>	
                                                        <br><br>
                                                        
                                                        
                                                              <section id="content1">
                                                                <div class="tab-content-container-outbound">
                                                                    <div class="tab-content-container">
                                                                        <div class="tab-content-container-inbound">
                                                                            <div class="only-search-part">
                                                                              
                                                                                <div class="search-container">
                                                                                     <div class="only-search-elements" >
                                                                                    <label>Search</label>
                                                                                    <input type="text" placeholder="Search Items" id="search-inventory" class="searchq">
                                                                                     <!-- <input type="button" value="Search"><br> -->
                                                                                    
                                                                                    </div>
                                                                                   
                                                                                   
                                                                                    
                                                                                    <div class="only-filter-container">
                                                                                        <label>Filter By</label>
                                                                                        <select id="pizzaSizeId">
                                                                                            <option value="0">All sizes</option>
                                                                                            <c:forEach items="${pizzaSizes}" var="view" varStatus="status">
                                                                                                                                                                                     
                                                                                            <%--  <c:choose>
                                                                                              <c:when test="${category.name==view.name}">  
                                                                                              <option value="${view.id}" selected="selected">${view.name}</option>
                                                                                              </c:when>
                                                                                              <c:otherwise>  --%>
                                                                                              <option value="${view.id}">${view.description}</option>
                                                                                             <%--  </c:otherwise>
                                                                                              </c:choose> --%>
                                                                                        </c:forEach>
                                                                                        </select>
                                                                                    </div><!--.only-filter-container-->
                                                                                </div><!--.search-container-->
                                                                                <div class="xlsUpload">
																					<label id="errorMessage"
																						style="color: red; float: right"></label><br>
																					
																						<div class="upload-inventory" id="upload-inventory" style="margin-bottom: 3%;display: block;">

																						<input type="button" id="addTemplate" value="Add Template"
																							onclick="addTemplate()"
																							style="margin: 4%; margin-right: 1%; margin-top: -6.5%;">
																						<!-- <input type="button" id="btnUpload" onclick="templateMap()"
																							value="Map Topping"
																							style="margin: 4%; margin-right: 17%; margin-top: -6.5%;"> --><br>
																					</div>
																				</div>
                                                                                <div class="inventory-items-list-table">
                                                                                <div class="pj-preloader"></div>
                                                                                   <!--  <table width="100%" cellpadding="0" cellspacing="0" id="example" >
                                                                                      <thead>
                                                                                        <tr>
                                                                                          <th>Tamplate</th>
                                                                                          <th>Price</th>
                                                                                          <th>Size</th>
                                                                                          <th>Status</th>
                                                                                          <th>Actions</th>
                                                                                        </tr>
                                                                                      </thead>
                                                                                    </table> -->
                                                                                     <table width="100%" cellpadding="0" cellspacing="0" id="example" >
                                                                                      <thead>
                                                                                        <tr>
                                                                                          
                                                                                          <th>Name</th>
                                                                                          <th>Size</th>
                                                                                          <th>Price</th>
                                                                                          <th>Topping</th>
                                                                                           <th>Status</th>
                                                                                           <th>Category</th>
                                                                                           <th>Action</th>
                                                                                          
                                                                                        </tr>
                                                                                      </thead>
                                                                                    </table>
                                                                                    <div id="LoadingImage" style="display: none" align="middle">
                                                          <img src="resources/img/spinner.gif" align="middle" />
                                                             </div>
                                                                                </div><!--.inventory-items-list-table-->
                                                                            </div><!--.only-search-part-->
                                                                        </div><!--.tab-content-container-inbound-->
                                                                    </div><!--.tab-content-container-->
                                                                </div><!--.tab-content-container-outbound-->
                                                              </section>
                                                                
                                                                
                                                            </main>
                                                        </div><!--.inventory-tabs-inbound-->
                                                    </div><!--.inventory-tabs-->
                                                </div><!--.inventory-tabs-outbound-->
                                            </div><!--.inventory-page-data-->
                                            
                                        </div><!--.right-content-inner-container-->
                                    </div><!--.right-content-container-->
                                </div><!--.content-inner-container-->
                            </div><!--.row-->
                            
                        </div><!--.outer-container-->
                    </div><!--#page-content-->
                    
                    <%@ include file="adminFooter.jsp" %>
                    <!--#footer-container-->
                    
                </div><!--.max-row-->
            </div><!--.inner-container-->
        </div><!--.foodkonnekt .dashboard-->
    </div><!--#page-container-->
    <script type="text/javascript">
   /*  $(document).ready(function() {
        //categoryItems();
  }); */
 
   
   
 </script>
 
 
 <script type="text/javascript">
     $('#pizzaSizeId').change(function () {
    	 var table = $('#example').DataTable();
    	 var catId= $('#pizzaSizeId').val();
    	 var catIdValue=$('#pizzaSizeId :selected').text();
    	if(catId>0){
    		 table.search(catIdValue).draw();
    			}
    	 
    	if(catIdValue=="All sizes"){
     		 table.search("").draw();
          	}
     });
 </script>
 <script src="resources/js/dialog-box/classie.js"></script>
<script src="resources/js/dialog-box/modalEffects.js"></script>
<script>
/* $(function(){
    $('label').each(function(){
        if($(this).text()=='Search:'){
            $(this).text('');
          }
    })
    
    
    $("#search-inventory").keyup(function() {
        $.fn.dataTableExt.sErrMode = 'throw';
        var searchTxt=$(this).val();
         if ( $.fn.DataTable.isDataTable('#example') ) {
             $('#example').DataTable().destroy();
           }
           $('#example tbody').empty();
           
           var itemId=$('#itemId').val();
           $.ajax({
             type: 'GET',
             url: "searchTemplateByText?searchTxt="+searchTxt,
             success:function(data){
              var outer = [];
              $.each(data, function( index, value ) {
                var inner = [];
                
                inner.push(value.name);
                inner.push(value.size);
                inner.push(value.price);
                inner.push(value.topping);
                inner.push(value.status);
                inner.push(value.categoryName);
                inner.push(value.action);
                outer.push(inner);
              });
              //console.log(outer);
              $('#example').DataTable( {
                data: outer,
                columns: [
                	
                    { title: "Name" },
                    { title: "Size" },
                    { title: "Price" },
                    { title: "Topping" }, 
                    { title: "Status" },
                    { title: "Category Name" },
                    { title: "Action" }
                    ]
               });
              $('label').each(function(){
                  if($(this).text()=='Search:'){
                      $(this).text('');
                    }
              })
             }
          });
      });
}) */
$('#search-inventory').on( 'keyup', function () {
	 var table = $('#example').DataTable();
   table.search( this.value ).draw();
} );

		function addTemplate(){
			window.location.href = "createPizzaTemplate";
		}
		
		function templateMap(){
			window.location.href = "pizzaTemplateMpping";
		}

</script>
<script>
  var polyfilter_scriptpath = '/js/';
</script>

<script>
		var posId = "${sessionScope.merchant.owner.pos.posId}";
		
		$(document).ready(function() {
			if(posId != '3'){
	      	  	$('#addTemplate').css('display','none');
	      	
	      	    $('#addTemplate').css('display','none');
	      	     
	      	  }
	    });
</script>


</body>
</html>
  
