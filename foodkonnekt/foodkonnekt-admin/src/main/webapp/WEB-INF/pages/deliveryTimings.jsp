<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="true"%>
<!doctype html>
<html class="no-js" lang="en">
<head>
<title>FoodKonnekt | Dashboard</title>
<!--CALLING STYLESHEET STYE.CSS-->
<link rel="stylesheet" href="resources/css/style.css">
<!--CALLING STYLESHEET STYLE.CSS-->

<!--CALLING GOOGLE FONT OPEN SANS-->
<link
	href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic'
	rel='stylesheet' type='text/css'>
<!--CALLING GOOGLE FONT OPEN SANS-->

<!--CALLING FONT AWESOME-->
<link rel="stylesheet" href="resources/css/font-awesome.css">
<!--CALLING FONT AWESOME-->

<!--CALLING FILTER OPTIONS-->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<!--  <script src="cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.2.17/jquery.timepicker.min.js" type="text/javascript"></script>
     <script src="cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.2.17/jquery.timepicker.min.js" type="text/javascript"></script> -->
<script type="text/javascript" charset="utf8"
	src="https://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/jquery.dataTables.min.js"></script>
<script
	src="https://www.jqueryscript.net/demo/Powerful-jQuery-Data-Table-Column-Filter-Plugin-yadcf/jquery.dataTables.yadcf.js"></script>
<!--OPENS DIALOG BOX-->
<link rel="stylesheet" type="text/css"
	href="resources/css/dialog-box/component.css" />
<!-- <link rel="stylesheet" type="text/css" href="cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.2.17/jquery.timepicker.min.css"/> -->


<script
	src="https://netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
<script
	type="http://netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css"></script>
<!--OPENS DIALOG BOX-->
<style type="text/css">
.capDay {
	text-transform: capitalize;
}

.inactiveLink {
   pointer-events: none;
   cursor: default;
}
</style>
</head>
<body>
	<div id="page-container">
		<div class="foodkonnekt merchant">
			<div class="inner-container">
				<div class="max-row">

					<header id="page-header">
						<div class="inner-header">
							<div class="row">

								<div class="logo">
									<a href="adminHome" title="FoodKonnekt Dashboard" class="logo"><img
										src="resources/img/foodkonnekt-logo.png"></a>
								</div>
								<!--.logo-->
								<%@ include file="adminHeader.jsp"%>

							</div>
							<!--.row-->
						</div>
						<!--.inner-header-->
					</header>
					<!--#page-header-->

					<div id="page-content">
						<div class="outer-container">
							<div class="row">
								<div class="content-inner-container">
									<%@ include file="leftMenu.jsp"%>

									<div class="right-content-container">
										<div class="right-content-inner-container">

											<div class="content-header">
												<div class="all-header-title"></div>
												<!--.header-title-->
												<div class="content-header-dropdown"></div>
												<!--.content-header-dropdown-->
											</div>
											<!--.content-header-->

											<div class="merchant-page-data">
												<div class="merchant-actions-outbound">
													<div class="merchat-coupons-container">

														<div class="coupons-navigation">
															<ul>
																<li ><a
																	href="onLineOrderLink">Location</a></li>
																<li class="current-menu-item"><a href="deliveryZones">Delivery Zones</a></li>
																<%-- <li><a href="deliveryZonesWithMap?locationAddress=${merchant.name} ${location.address1}${location.city}${location.zip}">Zone with map</a></li> --%>
																 <!--<li><a href="vouchars">Coupons</a></li>-->
																<li><a href="customers">Customers</a></li>
																<c:if test="${merchant.owner.pos.posId!=1}"> <li><a href="setupPaymentGateway?adminPanel=1">Gateway</a></li></c:if>
																<li><a href="getTaxesByMerchantId">Taxes</a></li>
																<li><a href="getMerchantSliders?merchantId=${merchant.id}">Sliders</a></li>
																<li><a href="orderNotification">Order Notification</a></li>
																<li ><a href="user">User</a></li>
																<li ><a href="uberStore">Uber Store</a></li><li ><a href="merchantSocialPlatform">Social Platform</a></li>
															        <li ><a href="virtualFund">Virtual Fund</a></li><!-- <li ><a href="appStatus">App Status</a></li> -->
															</ul>
														</div>
														<!--.coupons-navigation-->

														<div class="coupons-content-container">

															<div class="clearfix"></div>

															<div class="location-container">
																<div class="location-container-form">
																	<form:form action="saveDeliveryHours" method="POST"
																		modelAttribute="DeliveryOpenHours" id="businessLogo"
																		enctype="multipart/form-data">
																		      
																		     <h3>Allow Delivery Timings</h3>
                                                                                <div class="sd-modifiers-limit-fields">
																								<div class="sd-modifiers-limit-yes-no">
																									Yes
																									<input type="radio" name="allowDeliveryTimings"
																										id="itemTimingsYes" value="1" onclick="yes()"/>
																									No
																									<input type="radio" name="allowDeliveryTimings"
																										id="itemTimingsNo" value="0" onclick="no()" />
																								</div></div><br>

																				
																				

																				

																				<div class="business-hours" style=" display: none">
																				<h3>Delivery Timings</h3>
																					<ul>
																						<c:forEach items="${businessHours}" var="bsh"
																							varStatus="status">
																							<li>
																								<div class="business-hours-li-left">
																									<label class="capDay"
																										style="color: black; line-height: 15px;">${bsh.day}</label>
																									<select class="opncls" name="selectedDay"
																										style="width: 90px;" attid="a_${bsh.id}">
																										<c:choose>
																											<c:when test="${bsh.isHoliday ==1}">
																												<option value="${bsh.id}:open">Open</option>
																												<option value="${bsh.id}:close"
																													selected="selected">Closed</option>
																											</c:when>
																											<c:otherwise>
																												<option value="${bsh.id}:open"
																													selected="selected">Open</option>
																												<option value="${bsh.id}:close">Closed</option>
																											</c:otherwise>
																										</c:choose>

																										<%--   <option value="onLineOrderLinkonLineOrderLinkopen:${bsh.id}">Open</option>
                                                                                        <option value="close:${bsh.id}">Close</option> --%>
																									</select>
																								</div> <!--.business-hours-li-left-->

																								<div class="business-hours-li-right teset"
																									style="width: 57%;">
																									<c:forEach items="${bsh.times}" var="time"
																										varStatus="status">
																										<div class="copyTime">
																											<select name="sTimeToSave"
																												style="width: 38%;">
																												<c:forEach items="${times}" var="tm">
																													<c:choose>
																														<c:when test="${tm ==time.startTime}">
																															<option value="${bsh.id}_${tm}"
																																selected="selected">${tm}</option>
																														</c:when>
																														<c:otherwise>
																															<option value="${bsh.id}_${tm}">${tm}</option>
																														</c:otherwise>
																													</c:choose>
																												</c:forEach>
																											</select>

																											<to>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;to&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</to>

																											<select name="eTimeToSave"
																												style="width: 38%;">
																												<c:forEach items="${times}" var="tm">
																													<c:choose>
																														<c:when test="${tm ==time.endTime}">
																															<option value="${tm}" selected="selected">${tm}</option>
																														</c:when>
																														<c:otherwise>
																															<option value="${tm}">${tm}</option>
																														</c:otherwise>
																													</c:choose>
																												</c:forEach>
																											</select>
																										</div>
																									</c:forEach>
																									<div class="copyToTime"
																										style="min-height: 47px;"></div>

																								</div> <!--.business-hours-li-right-->
																								<div class="business-hours-li-right"
																									style="width: 8%;">
																									<button id="0"
																										class="c-button-icon-primary optButton"
																										data-ember-action="1124" type="button">
																										<i class="fa fa-plus-circle hidden-xs"></i>
																									</button>

																									<button id="0"
																										class="e__button-icon-secondary optButtondel"
																										data-ember-action="1125" type="button">
																										<i class="fa fa-trash-o hidden-xs"></i>
																									</button>
																								</div>

																							</li>
																						</c:forEach>
																					</ul>
																				</div>

																				
																				<c:choose>
																					<c:when test="${inventoryThread ==1}">
																						<div class="button">
																							<a href="#" id="businessLogoButton">Save</a>
                                                                                            <a href="deliveryZones" id="businessLogoButton"
																								>Cancel</a>

																						</div>
																					</c:when>
																					<c:otherwise>
																						<div class="button">
																							<a href="#" id="businessLogoButton"
																								>Save</a> <a href="deliveryZones" id="businessLogoButton"
																								>Cancel</a>
																						</div>
																					</c:otherwise>
																				</c:choose>

																				<!--.button--></form:form>
																</div>
																<!--.location-container-form-->
															</div>
															<!--.location-container-->

														</div>
														<!--.coupons-content-container-->
													</div>
													<!--.merchat-coupons-container-->
												</div>
												<!--.merchant-actions-outbound-->
											</div>
											<!--.merchant-page-data-->

										</div>
										<!--.right-content-inner-container-->
									</div>
									<!--.right-content-container-->
								</div>
								<!--.content-inner-container-->
							</div>
							<!--.row-->

						</div>
						<!--.outer-container-->
					</div>
					<!--#page-content-->
					<%@ include file="adminFooter.jsp"%>
					<!--#footer-container-->

				</div>
				<!--.max-row-->
			</div>
			<!--.inner-container-->
		</div>
		<!--.foodkonnekt .dashboard-->
	</div>
	<!--#page-container-->

	<div id="sd-dialog">
		<div class="md-modal md-effect-14" id="modal-14">
			<div class="md-content">
				<h3>Modal Dialog</h3>
				<div>
					<form action="" method="get" class="home-pop-form">
						<input name="name" type="text" placeholder="Your Name"> <input
							name="name" type="email" placeholder="Your Email">
						<textarea name="query" cols="" rows="3"
							placeholder="State Your Query"></textarea>
						<div class="button">
							<input name="submit" type="button" value="Post Your Query"
								class="button left">
						</div>
						<!--.button-->
					</form>
					<!--.home-pop-form-->
					<div class="clearfix"></div>
					<div class="button">
						<button class="white-button md-close">Close me!</button>
					</div>
					<!--.button-->
				</div>
			</div>
		</div>
		<div class="md-overlay"></div>
		<!-- the overlay element -->
	</div>
	<!--#sd-dialog-->

	<!--OPENS DIALOG BOX-->
	<script src="resources/js/dialog-box/classie.js"></script>
	<script src="resources/js/dialog-box/modalEffects.js"></script>
	<script>
		var polyfilter_scriptpath = '/js/';
	</script>
	<!--OPENS DIALOG BOX-->
	<script type="text/javascript">
	

	
	
var allowDeliveryTime='${allowDeliveryTiming}';
	
	if(allowDeliveryTime==0){
        $("#itemTimingsNo").attr('checked', 'checked');
        $("#errorMessage").html("");
        $('.business-hours').css('display','none');
      }
      if(allowDeliveryTime==1){
        $("#itemTimingsYes").attr('checked', 'checked');
        $('.business-hours').css('display','block');
      }
	function yes(){
		 $('.business-hours').css('display','block');
	}

	function no(){
		$('.business-hours').css('display','none');
	}
	
	
	
	$(document).ready( function setUp() {
		var inventoryThreadStatus=0;
		$(".pj-loader-3").css("display","block");
		$("#onlineLinkErrorBox").hide();
		$("#enableOnlineOrderLink").hide();  
	    $.ajax({
	        url : "hasCategories",
	        type : "GET",
	        contentType : "application/json; charset=utf-8",
	        success : function(minAmountData) {
	        	
	        	
	               if(minAmountData==1){
	                  
	                	   $(".pj-loader-3").css("display","none");
	                	 $("#onlineLinkErrorBox").hide();
	        			 $("#onlineLink").show();
	        			 $("#onlineLinkDivCls").show();
	        			 $("#enableOnlineOrderLink").hide();
	        			 inventoryThreadStatus=1;
	        			 
	        		 }else if(minAmountData==2){
	        			 console.log("there is not any tax to show menu");
	        			 inventoryThreadStatus=0;
	                	   $(".pj-loader-3").css("display","none");
	                	   $("#onlineLinkErrorBox").show();
	  					 $("#onlineLinkErrorBox").html("Your sales tax is set to zero. Click on Yes button to enable online ordering link or set your taxes in your PoS system.");
	  					 $("#onlineLink").hide();
	  					 $("#onlineLinkDivCls").hide();
	  					 $("#enableOnlineOrderLink").show();
	        			   
	        			 
	        		 }else{
	        			 console.log("there is not any category to show on menu");
	        			 inventoryThreadStatus=0;
	        			 $("#onlineLinkErrorBox").show();
	        			 //$("#onlineLinkErrorBox").html("We are still in the process of getting inventory data from Clover.");
	        			 $("#onlineLink").hide();
	        			 $("#onlineLinkDivCls").hide();
	        			 $("#enableOnlineOrderLink").hide();
	        		 }
	                   
	               

	        },
	        error : function() {
	            
	            console.log("error to check categories and taxes by ajax");

	        },
	        complete: function() {
	            // Schedule the next request when the current one's complete
	            
	            if(inventoryThreadStatus==0){
	                 setTimeout(setUp, 5000);
	                 //window.location = "inventory";
	                   }
	          }
	    });

	});
	
	  var mybutton_counter=0;
	  $(document).ready(function() {
		  var merchantId=${merchant.id};
	  $.ajax({
          url : "getFacebookAppStatus?merchantId="+ merchantId,
          type : "GET",
          contentType : "application/json; charset=utf-8",
          success : function(result) {
          	
        	
          	if(result.status==false ){
          		 $("#facebookLinkView").hide();
          		 $("#facebookLinkApp").show(); 
          		
          	}else if(result.status == true && result.fBTabId!=''){
          		$("#facebookLinkView").show();
          		$("#facebookLinkApp").hide();
          		$("#facebookLinkView").append('<a href="http://www.facebook.com/'+result.fBTabId+'/app/608947722608987/" target="_blank" >View App</a>');
          		
          	}else{
          		$("#facebookLinkView").hide();
          		$("#facebookLinkApp").show();
          	}
          
          },
          error : function() {
            console.log("Error inside future date Ajax call");
          }
       });
	  });
	  
	 $(document).ready(function() {
		 //var currentUrl=window.opener.location.href;
			
		 if(window.opener!=null){
			 var currentUrl=window.opener.location.href;
				
			 if(currentUrl.includes("onLineOrderLink")){
			 window.onunload = refreshParent;
			    function refreshParent() {
			        window.opener.location.reload();
			    }
			 window.close();
			 }}
		 var inventoryThreadStatus="${inventoryThread}";
		 var isTaxAvailable="${isTaxAvailable}";
		
		 var allowfutureOrder="${allowFutureOrder}";
		 var allowMultipleKoupon = "${allowMultipleKoupon}";
		 var customerFeedback="";
		 customerFeedback= '${activeCustomerFeedback}';
		 var linkActiveCustomerFeedback= "${linkActiveCustomerFeedback}";
				
		  if(customerFeedback==1){
			 $("#feedbackyes").prop('checked', true);
			 mybutton_counter = 1;
			 $("#onlineLink1").html(linkActiveCustomerFeedback);
			 $("#previewId").attr("href",linkActiveCustomerFeedback);
		 }else{
			 $("#feedbackno").prop('checked', true);
			 mybutton_counter = 0;
		 }	  
		 if(allowfutureOrder==1){
			 $("#daysAhead").show();
		 }else{
			 $("#daysAhead").hide();
		 }
		 
		 if(inventoryThreadStatus==1){
			
			   if(isTaxAvailable=='false'){
				   
				   $("#onlineLinkErrorBox").show();
					 $("#onlineLinkErrorBox").html("Your sales tax is set to zero. Click on Yes button to enable online ordering link or set your taxes in your PoS system.");
					 $("#onlineLink").hide();
					 $("#onlineLinkDivCls").hide();
					 $("#enableOnlineOrderLink").show();
					 
			   }else{
			 $("#onlineLinkErrorBox").hide();
			 $("#onlineLink").show();
			 $("#onlineLinkDivCls").show();
			 $("#enableOnlineOrderLink").hide();
			   }
			 
		 }else{
			 
			 $("#onlineLinkErrorBox").show();
			// $("#onlineLinkErrorBox").html("We are still in the process of getting inventory data from Clover.");
			 $("#onlineLink").hide();
			 $("#onlineLinkDivCls").hide();
			 $("#enableOnlineOrderLink").hide();
		 }
		 
		 var businessHoursSize ="${businessHours.size()}";
		if(businessHoursSize==0){
			
			$("#businessHourseErrorBox").html("Please add business hours on Clover to enable online ordering platform");
		}else{
			$("#businessHours").hide();
		}
		 var isTaxable="${convenienceFee.isTaxable}";
         if(isTaxable==1){
             $("#isTaxable").attr('checked', 'checked');
           }
         
         var allowReOrder = '${merchant.allowReOrder}';
         if(allowReOrder== true){
             $("#allowReOrder").attr('checked', 'checked');
           }
         
         var cash="${cash}";
         if(cash==1){
             $("#cash").attr('checked', 'checked');
           }
         var creditcard="${creditcard}";
         
         if(creditcard==1){
             $("#creditcard").attr('checked', 'checked');
           }
           
	 });
		$(document)
				.ready(
						function() {
							$(".opncls").each(function() {
								if (($(this).val()).indexOf('close') > -1) {
									$(this).parent().next().next().hide();
								}
							});

							$(".opncls")
									.change(
											function() {
												var dayValue = $(this).val();
												if (dayValue.indexOf('open') > -1) {
													var timDivLngth = $(this)
															.parent().next()
															.find('.copyTime').length;

													if (timDivLngth == 0) {
														console.log("----"
																+ timDivLngth);
														var opclVal = $(this)
																.val();
														var opclArray = [];
														opclArray = opclVal
																.split(":");
														var dayId = opclArray[0];

														console
																.log($(this)
																		.parent()
																		.next()
																		.find(
																				'.copyTime').length);

														var timesVal = '${times}';
														var timeAyy = timesVal
																.split(",");
														var opt = '';
														var endTimeStr = '';
														for (var i = 1; i < (timeAyy.length - 1); i++) {
															opt += "<option value='"
																	+ dayId
																	+ '_'
																	+ (timeAyy[i])
																			.trim()
																	+ "'>"
																	+ timeAyy[i]
																	+ "</option>";
															endTimeStr += "<option value='"
																	+ (timeAyy[i])
																			.trim()
																	+ "'>"
																	+ timeAyy[i]
																	+ "</option>";
														}
														$(this)
																.parent()
																.next()
																.append(
																		"<div class='copyTime'><div>");
														aa = "<div class='copyTime'><select name='sTimeToSave' style='width: 38%;'>"
																+ opt
																+ "</select><to>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;to&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</to><select name='eTimeToSave' style='width: 38%;'>"
																+ endTimeStr
																+ "</select><div>";
														$(this)
																.parent()
																.next('.teset')
																.find(
																		'.copyToTime:first')
																.append(aa);
													}
													//$(this).parent().next().find('.copyToTime').show(); 
													$(this).parent().next()
															.find('.copyTime')
															.show();
													$(this).parent().next()
															.next().show();
												} else {
													// $(this).parent().next().find('.copyToTime').hide();
													$(this).parent().next()
															.find('.copyTime')
															.hide();
													$(this).parent().next()
															.next().hide();
												}
											});
							
							
							$("#yesRadio").click(
									function() {
										$("#daysAhead").show();
									});
							
							$("#noRadio").click(
									function() {
										$("#daysAhead").hide();
									});

							$(".optButton")
									.click(
											function() {
												var copCount = $(this)
														.parent()
														.prev('.teset')
														.find(
																".copyTime select").length;
												if (copCount == 0) {
													var selectval = $(this)
															.parent()
															.prev('.teset')
															.prev(
																	'.business-hours-li-left')
															.find('select')
															.val();
													var res = selectval
															.replace("close",
																	"open");
													$(this)
															.parent()
															.prev('.teset')
															.prev(
																	'.business-hours-li-left')
															.find('select')
															.val(res);

													var opclVal = selectval;
													var opclArray = [];
													opclArray = opclVal
															.split(":");
													var dayId = opclArray[0];

													var timesVal = '${times}';
													var timeAyy = timesVal
															.split(",");
													var opt = '';
													var endTimeStr = '';
													for (var i = 1; i < (timeAyy.length - 1); i++) {
														opt += "<option value='"
																+ dayId
																+ '_'
																+ (timeAyy[i])
																		.trim()
																+ "'>"
																+ timeAyy[i]
																+ "</option>";
														endTimeStr += "<option value='"
																+ (timeAyy[i])
																		.trim()
																+ "'>"
																+ timeAyy[i]
																+ "</option>";
													}
													$(this)
															.parent()
															.prev('.teset')
															.append(
																	"<div class='copyTime'><div>");
													aa = "<div class='copyTime'><select name='sTimeToSave' style='width: 38%;'>"
															+ opt
															+ "</select><to>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;to&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</to><select name='eTimeToSave' style='width: 38%;'>"
															+ endTimeStr
															+ "</select><div>";
													$(this)
															.parent()
															.prev('.teset')
															.find(
																	'.copyToTime:first')
															.append(aa);
												} else {
													$(this).parent().prev(
															'.teset')
													var cln = $(this)
															.parent()
															.prev('.teset')
															.find(
																	'.copyTime:first')
															.clone();
													var cpy = $(this)
															.parent()
															.prev('.teset')
															.find('.copyToTime');
													cln.appendTo(cpy);
												}
											});

							$(".optButtondel")
									.click(
											function() {
												$(this).parent().prev('.teset')
														.find('.copyTime:last')
														.remove();
												var copCount = $(this)
														.parent()
														.prev('.teset')
														.find(
																".copyTime select").length;
												if (copCount == 0) {
													var selectval = $(this)
															.parent()
															.prev('.teset')
															.prev(
																	'.business-hours-li-left')
															.find('select')
															.val();
													var res = selectval
															.replace("open",
																	"close");
													$(this)
															.parent()
															.prev('.teset')
															.prev(
																	'.business-hours-li-left')
															.find('select')
															.val(res);
													$(this).parent().hide();
												}
								
											});

							var imageSize;
							$('#image-file').on('change', function() {
							       //alert('This file size is: ' + (this.files[0].size/1024/1024).toFixed(2) + " MB");
								   imageSize=(this.files[0].size/1024/1024).toFixed(2);
								   if(imageSize > 2){
								   	   alert("Please select an image lesser than 2 MB");
								   	this.value="";
								   	imageSize=0;
								   	   }
							      });
							$("#businessLogoButton").click(function() {
								
								$("#businessLogo").submit();
							});

						});
		
		
	</script>
	<script type="text/javascript">
	
	 function validatenumberOnKeyUp(event,el) {
	    	
	    	//var val = $('#tipAmount').val().replace(/^[0-9]*\.?[0-9]*$/,'');
	    	var val = $('#convenienceFees').val();
	        if(isNaN(val)){
	             val = val.replace(/[^0-9\.]/g,'');
	             if(val.split('.').length>2) 
	                 val =val.replace(/\.+$/,"");
	        }
	        $('#convenienceFees').val(val); 
	        }
			
	 function validatenumber(event,el) {
         return (/^[0-9]*\.?[0-9]*$/).test(el.value+event.key);
       }
	
	
	
	
	
	
	
	
	function isNumberKey(evt)
    {
  	  var target = evt.target || evt.srcElement; // IE
  	  var charCode = (evt.which) ? evt.which : event.keyCode
  	    var id = target.id;
  	    var data=document.getElementById(id).value;
  	  
  	  
  	  if(evt.which == 8 || evt.which == 0){
            return true;
        }
  	  
  	 if (charCode > 31 && (charCode < 48 || charCode > 57)){
         return false;
  		}
  	 
        if(evt.which < 46 || evt.which > 59) {
            return false;
            //event.preventDefault();
        } // prevent if not number/dot

        if(evt.which == 46 && data.indexOf('.') != -1) {
            return false;
            //event.preventDefault();
        }
    }
    
    function currectNo(evt){
  	  var target = evt.target || evt.srcElement; // IE

	    var id = target.id;
	    var data=document.getElementById(id).value;
	    
	    var res=data.split(".");
  	  
  	  if(res.length==2){
  		if(res[1]){
  			
  		}else{
  			
  			document.getElementById(id).value=res[0]+".0";
  		}
  	  }
    }
    
    
   // var mybutton_counter=0;
    $("#feedbackyes").on("click", function () {
    	
    	if (mybutton_counter==0){
    	 var merchantId=${merchant.id};
        var status = "yes";
        $.ajax({
            url : "updateMerchantKritiq?merchantId="+ merchantId +"&status="+ status,
            type : "GET",
            contentType : "application/json; charset=utf-8",
            success : function(result) {
            	
            	
            	if(result ==""){
            		$("#feedbackno").prop('checked', true);
           			$("#feedbackyes").prop('checked', false);
           			$("#onlineLink1").html("Please Subscribe Your Account");
            	}else if(result == "VALIDITY EXPAIRED"){
            		$("#feedbackno").prop('checked', true);
           			$("#feedbackyes").prop('checked', false);
           			$("#onlineLink1").html("VALIDITY EXPAIRED Please Subscribe Your Account");
            	}else{
            		$("#onlineLink1").html(result);
                	$("#previewId").attr("href",result);
                	mybutton_counter++;
            	}
            
            },
            error : function() {
              console.log("Error inside future date Ajax call");
            }
         })
    }
    });
    
    
    
$("#enableOnlineOrderLink").on("click", function () {
    	
    	
	$(".pj-loader-3").css("display","block");
	$("#onlineLinkErrorBox").hide();
	$("#enableOnlineOrderLink").hide();
    	var merchantId=${merchant.id};
    	
    	 $.ajax({
            url : "addDefaultTax?merchantId="+ merchantId,
            type : "GET",
            contentType : "application/json; charset=utf-8",
            success : function(result) {
            	if(result=='success'){
            	$("#onlineLinkErrorBox").hide();
   			 $("#onlineLink").show();
   			 $("#onlineLinkDivCls").show();
   			 $("#enableOnlineOrderLink").hide();
            	}else{
            		 $("#onlineLinkErrorBox").show();
        			 $("#onlineLink").hide();
        			 $("#onlineLinkDivCls").hide();
        			 $("#enableOnlineOrderLink").show();
            	}
   			$(".pj-loader-3").css("display","none");
            },
            error : function() {
              console.log("Error inside future date Ajax call");
              $(".pj-loader-3").css("display","none");
              $("#onlineLinkErrorBox").show();
    			 $("#onlineLink").hide();
    			 $("#onlineLinkDivCls").hide();
    			 $("#enableOnlineOrderLink").show();
            }
         }) 
    	
    	
    });
    
    $("#feedbackno").on("click", function () {
    	
    	$("#onlineLink1").html("To Allow Customer Feedback Select Yes");
    	mybutton_counter = 0;
    	var merchantId=${merchant.id};
    	var status = "no";
    	$.ajax({
            url : "updateMerchantKritiq?merchantId="+ merchantId +"&status="+ status,
            type : "GET",
            contentType : "application/json; charset=utf-8",
            success : function(result) {
            	           	
            },
            error : function() {
              console.log("Error inside future date Ajax call");
            }
         })
    	
    	
    });
    
    
    function isPaymentGatewayEnable(){
		var merchantId=${merchant.id};
    	$.ajax({
            url : "checkIsPaymentGatewayEnabled?merchantId="+ merchantId,
            type : "GET",
            contentType : "application/json; charset=utf-8",
            success : function(result) {
            	           	if(result==false){																					
            	           		$("#paymentTypeErrorBox").html("Before Enabling Credit Card, <a href='setupPaymentGateway' style='color:#FF0000;text-decoration: underline' > Click Here To Enable Gateway</a>");
            	           		document.getElementById("creditcard").checked = false; 
            	    	 	    return false;
            	           	}
            },
            error : function() {
              console.log("Error inside future date Ajax call");
            }
         })
	}
    </script>
</body>
</html>
