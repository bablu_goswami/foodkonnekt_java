<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@page session="true"%>
<!doctype html>
<html class="no-js" lang="en">
<head>
<title>FoodKonnekt | Add Products</title>
<script src="https://www.foodkonnekt.com/web/resources/V2/js/jquery-1.11.3.min.js"></script>
 <link href="resources/css/bootstrap.min.css"
        rel="stylesheet" type="text/css" />
        <link href="resources/css/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
<!--CALLING STYLESHEET STYE.CSS-->
<link rel="stylesheet" href="resources/css/style.css">
<!--CALLING STYLESHEET STYLE.CSS-->

<!--CALLING GOOGLE FONT OPEN SANS-->
<link
	href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic'
	rel='stylesheet' type='text/css'>
<!--CALLING GOOGLE FONT OPEN SANS-->

<!--CALLING FONT AWESOME-->
<link rel="stylesheet"
	href="resources/css/font-awesome.css">
<!--CALLING FONT AWESOME-->

<!--OPENS DIALOG BOX-->
<link rel="stylesheet" type="text/css"
	href="resources/css/dialog-box/component.css" />
<!--OPENS DIALOG BOX-->

<!--CALLING PRODUCTS TABS-->
<link rel='stylesheet' type='text/css'
	href='resources/css/products-tabs/opentabby.css' />
<!--CALLING PRODUCTS TABS-->
	 <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css">
<style type="text/css">
.body{
overflow: scroll !important;
}
.sd-modifiers-each-field-label label label {
    height: auto;
    display: inline-block;
    max-width: 20px;
    margin-left: 10px;
}
.sd-modifiers-each-field-label label span {
    float: left;
}

div#example_paginate {
	display: block;
}

div#example_filter {
	display: block;
}

div#example_length {
	display: block;
}

input[type="search"] {
	max-width: 300px;
	width: 100%;
	outline: 0;
	border: 1px solid rgb(169, 169, 169);
	padding: 11px 10px;
	border-radius: 6px;
	margin-bottom: 7px;
	placeholder: Search Items;
}

input[type="text"], input[type="email"], input[type="password"], input[type="number"],
	input[type="date"] {
	height: 39px;
}

.pj-preloader {
    display: none;
    position: absolute;
    height: 383px;;
    width: 940px;
    background: url("resources/img/spinner.gif") no-repeat scroll center center rgba(153, 153, 153, 0.3);
    z-index: 9999;
    left: 260;
    position: absolute;
    top: 224px;
}
</style>
  </head>
  <body>
  <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
	<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
	<script src='resources/js/products-tabs/opentabby.js'></script>
	<script src="resources/js/popModal.js"></script>
	<script type="text/javascript" src="resources/js/bootstrap.min.js"></script>
    <script src="resources/js/bootstrap-multiselect.js" type="text/javascript"></script>
    <!-- <script src="resources/js/jquery.js" type="text/javascript"></script> -->
    <!-- <script src="js/jquery.dataTables.js" type="text/javascript"></script> -->
     <script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <div id="page-container">
        <div class="foodkonnekt inventory">
            <div class="inner-container">
                <div class="max-row">
                    
                    <header id="page-header">
                        <div class="inner-header">
                            <div class="row">
                                
                                <div class="logo">
                                     <a href="adminHome" title="FoodKonnekt Dashboard" class="logo"><img src="resources/img/foodkonnekt-logo.png"></a>
                                </div><!--.logo-->
                                <%@ include file="adminHeader.jsp" %> 
                                
                            </div><!--.row-->
                        </div><!--.inner-header-->
                    </header><!--#page-header-->
                    
                    <div id="page-content">
                        <div class="outer-container">
                        
                            <div class="row">
                                <div class="content-inner-container">
                                    
                                     <%@ include file="leftMenu.jsp"%>
                                
                                    <div class="right-content-container">
                                    
                                        <div class="right-content-inner-container">
                                        
                                            <div class="content-header">
                                                <div class="all-header-title">
                                                </div><!--.header-title-->
                                            </div><!--.content-header-->
                                            
                                            <div class="merchant-page-data">
                                        
                                                <div class="merchant-actions-outbound">
                                                    <div class="merchat-coupons-container">
                                                            <!-- <main> -->
                                                                 
                                                            <div class="coupons-navigation">
                                                               
                                                                                                                       <ul>
                                                                        <li ><a href="pizzaTamplate">Template</a></li>
                                                                        <li><a href="pizzaTopping">Topping</a></li>
                                                                         <li id="pizzaCrust"><a href="pizzaCrust">Crust</a></li> 
                                                                        <li id="pizzaSize" class="current-menu-item"><a href="pizzaSize">Size</a></li>
                                                                        <li id="templateTaxMap"><a href="templateTaxMap">Tax Map</a></li>
                                                                        <li><a href="pizzaCategory">Pizza Category</a></li>
                                                                    </ul>
                                                        </div>
                                                        
                                                        
                                                        <div id="LoadingImage" style="display: none" align="middle">
                                                          <img src="resources/img/spinner.gif" align="middle" />
                                                             </div>
                                                              <section id="content1">
                                                                <div class="tab-content-container-outbound">
                                                                    <div class="tab-content-container">
                                                                        <div class="tab-content-container-inbound">
                                                                            
                                                                              <form:form method="POST" action="updatePizzaSize"
																			modelAttribute="pizzaSize" id="pizzaToppingForm"> 
                                                                              
                                                                              <form:hidden path="id" value="${pizzaSize.id}" id="pizzaTopping_id" />
                                                                  				<div class="adding-products">
																				<label id="errorMessage" style="color: red;"></label>
																				<div class="adding-products-form">
																				<br><br><br>
																					<div class="clearfix"></div>
																					<div>
																					<label>Description:</label>
																					<input type="text" value="${pizzaSize.description}" maxlength="140"  id="description"  name="description" path="description" required="required" >
																					</div>
																					
																					<br>
																			<div class="clearfix"></div>
																			<label>Size Status:</label>
																			<form:radiobutton path="active" name="item"
																				value="1" id="activeStatus" /> Active
																			<form:radiobutton path="active" name="item"
																				value="0" id="inactiveStatus" /> InActive<br>
																				<br>
																				
																				<br>
																				<div>
																<label>Mapped Template:</label></div><div>
																<form:select id="mapPizzaId"  class="multiselect dropdown-toggle btn btn-default"
																	multiple="multiple" path="tempId">
																	<c:forEach items="${mappedpizzas}" var="mappizza"
																		varStatus="status">
																		<c:if test="${mappizza != null && mappizza != ''}">
																								<c:choose>
																									<c:when test="${mappizza.active==1}">
																										<option value="${mappizza.pizzaTemplate.id}" selected>${mappizza.pizzaTemplate.description}</option>

																									</c:when>
																									<c:otherwise>
																										<option value="${mappizza.pizzaTemplate.id}">${mappizza.pizzaTemplate.description}</option>
																									</c:otherwise>
																								</c:choose>
																							</c:if>
																	</c:forEach>
																</form:select>
																</div>
																<br>
																<div>
																<label>UnMapped Template:</label>
																<form:select id="unmapPizzaId" style="display: block;margin:100px;" class="multiselect"
																	multiple="multiple" path="unmaptempId">
																	<c:forEach items="${unmappedpizzas}" var="unmappedpizzas"
																		varStatus="status">
																		<option value="${unmappedpizzas.id}" >${unmappedpizzas.description}</option>
																	</c:forEach>
																</form:select>
																</div>
																<br>
																<div>
																<label>Mapped Topping:</label>
																<form:select id="mapToppingId" style="display: block;margin:100px;" class="multiselect"
																	multiple="multiple" path="mappizzatopping">
																	<c:forEach items="${mappedTopping}" var="mappedTopping"
																		varStatus="status">
																		<c:if test="${mappedTopping != null && mappedTopping != ''}">
																								<c:choose>
																									<c:when test="${mappedTopping.active==0}">
																										<option value="${mappedTopping.pizzaTopping.id}" selected>${mappedTopping.pizzaTopping.description}</option>

																									</c:when>
																									<c:otherwise>
																										<option value="${mappedTopping.pizzaTopping.id}">${mappedTopping.pizzaTopping.description}</option>
																									</c:otherwise>
																								</c:choose>
																							</c:if>
																	</c:forEach>
																</form:select>
																</div>
																<br>
																<div>
																<label>UnMapped Topping:</label>
																<form:select id="unmapToppingId" style="display: block;margin:100px;" class="multiselect"
																	multiple="multiple" path="unmappizzatopping">
																	<c:forEach items="${unmappedTopping}" var="unmappedTopping"
																		varStatus="status">
																		<option value="${unmappedTopping.id}" >${unmappedTopping.description}</option>
																	</c:forEach>
																</form:select>
																</div>
																<br>
																<div>
																<label>Mapped Crust:</label>
																<form:select id="mapCrustId" style="display: block;margin:100px;" class="multiselect"
																	multiple="multiple" path="mappizzaCrust">
																	<c:forEach items="${mappedCrust}" var="mappedCrust"
																		varStatus="status">
																		<c:if test="${mappedCrust != null && mappedCrust != ''}">
																								<c:choose>
																									<c:when test="${mappedCrust.active==1}">
																										<option value="${mappedCrust.pizzaCrust.id}" selected>${mappedCrust.pizzaCrust.description}</option>

																									</c:when>
																									<c:otherwise>
																										<option value="${mappedCrust.pizzaCrust.id}">${mappedCrust.pizzaCrust.description}</option>
																									</c:otherwise>
																								</c:choose>
																							</c:if>
																	</c:forEach>
																</form:select>
																</div>
																<br>
																<div>
																<label>UnMapped Crust:</label>
																<form:select id="unmapCrustId" style="display: block;margin:100px;" class="multiselect"
																	multiple="multiple" path="unmappizzaCrust">
																	<c:forEach items="${unmappedCrust}" var="unmappedCrust"
																		varStatus="status">
																		<option value="${unmappedCrust.id}" >${unmappedCrust.description}</option>
																	</c:forEach>
																</form:select>
																</div>
																
																			<br>
																			
																			 <br>
																					
																				<div class="button left">
																  <input type="submit" id="updatePizzaToppiongButton" value="Save">&nbsp;&nbsp; 
																 <input type="submit" value="Cancel" id="updatePizzaToppingCancelButton">
																</div>	</div></div>
															 </form:form>
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                           <!--.only-search-part-->
                                                                        </div><!--.tab-content-container-inbound-->
                                                                    </div><!--.tab-content-container-->
                                                                </div><!--.tab-content-container-outbound-->
                                                              </section>
                                                                
                                                                
                                                            </main>
                                                        </div><!--.inventory-tabs-inbound-->
                                                    </div><!--.inventory-tabs-->
                                                </div><!--.inventory-tabs-outbound-->
                                            </div><!--.inventory-page-data-->
                                            
                                        </div><!--.right-content-inner-container-->
                                    </div><!--.right-content-container-->
                                </div><!--.content-inner-container-->
                            </div><!--.row-->
                            
                        </div><!--.outer-container-->
                    </div><!--#page-content-->
                    
                    <%@ include file="adminFooter.jsp" %>
                    <!--#footer-container-->
                    
                </div><!--.max-row-->
            </div><!--.inner-container-->
        </div><!--.foodkonnekt .dashboard-->
    </div><!--#page-container-->
   
   
   <!-- add pizza size popup start -->
    

 <script type="text/javascript">
    
    $(document).ready(function() {
    	
    	 $("#updateItemCancelButton").click(function() {
             window.location="pizzaTamplate"
           })
    });
    
    
    </script>
    
   				 <script>
   $(document).ready(function() {
		 $('#mapPizzaId').multiselect({
			  maxHeight: 450 ,
	          includeSelectAllOption: true
	     }); 
		 
		 $('#unmapPizzaId').multiselect({
			  maxHeight: 450 ,
	          includeSelectAllOption: true
	     }); 
		 
		 $('#mapToppingId').multiselect({
			  maxHeight: 450 ,
	          includeSelectAllOption: true
	     }); 
		 
		 $('#unmapToppingId').multiselect({
			  maxHeight: 450 ,
	          includeSelectAllOption: true
	     }); 
		 
		 $('#mapCrustId').multiselect({
			  maxHeight: 450 ,
	          includeSelectAllOption: true
	     }); 
		 
		 $('#unmapCrustId').multiselect({
			  maxHeight: 450 ,
	          includeSelectAllOption: true
	     }); 
	}); 
   </script>
   
    <!--OPENS DIALOG BOX-->
	<!--CALLING TABS-->
	<!--CALLING TABS-->
  </body>
</html>
  
