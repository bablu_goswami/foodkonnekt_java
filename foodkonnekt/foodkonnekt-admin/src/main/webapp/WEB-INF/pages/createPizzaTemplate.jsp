<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="true"%>
<mvc:annotation-driven ignoreDefaultModelOnRedirect="true" />
<!doctype html>
<html class="no-js" lang="en">
<head>

<title>FoodKonnekt | Dashboard</title>
<!--CALLING STYLESHEET STYE.CSS-->
<link href="resources/css/bootstrap.min.css"
        rel="stylesheet" type="text/css" />
        <link href="resources/css/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
<!--CALLING STYLESHEET STYE.CSS-->
<link rel="stylesheet" href="resources/css/style.css">
<!--CALLING STYLESHEET STYLE.CSS-->

<!--CALLING GOOGLE FONT OPEN SANS-->
<link
	href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic'
	rel='stylesheet' type='text/css'>
<!--CALLING GOOGLE FONT OPEN SANS-->

<!--CALLING FONT AWESOME-->
<link rel="stylesheet"
	href="resources/css/font-awesome.css">
<!--CALLING FONT AWESOME-->

<!--OPENS DIALOG BOX-->
<link rel="stylesheet" type="text/css"
	href="resources/css/dialog-box/component.css" />
<!--OPENS DIALOG BOX-->

<!--CALLING PRODUCTS TABS-->
<link rel='stylesheet' type='text/css'
	href='resources/css/products-tabs/opentabby.css' />
<!--CALLING PRODUCTS TABS-->
<!-- <link rel="stylesheet" type="text/css"
	href="https://cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css"> -->
	
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
	<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
	<script src='resources/js/products-tabs/opentabby.js'></script>
	<script src="resources/js/popModal.js"></script>
	<script type="text/javascript" src="resources/js/bootstrap.min.js"></script>
    <script src="resources/js/bootstrap-multiselect.js" type="text/javascript"></script>

<style>
.errorClass {
	border: 1px solid red;
}
</style>
<style>
/* The container */
.radiocontainer {
	display: block;
	position: relative;
	padding-left: 35px;
	margin-bottom: 12px;
	cursor: pointer;
	font-size: 18px;
	-webkit-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	user-select: none;
}

/* Hide the browser's default radio button */
.radiocontainer input {
	position: absolute;
	opacity: 0;
	cursor: pointer;
}

/* Create a custom radio button */
.checkmark {
	position: absolute;
	top: 0;
	left: 0;
	height: 25px;
	width: 25px;
	background-color: #eee;
	border-radius: 50%;
}

/* On mouse-over, add a grey background color */
.radiocontainer:hover input ~ .checkmark {
	background-color: #ccc;
}

/* When the radio button is checked, add a blue background */
.radiocontainer input:checked ~ .checkmark {
	background-color: orange;
}

/* Create the indicator (the dot/circle - hidden when not checked) */
.checkmark:after {
	content: "";
	position: absolute;
	display: none;
}

/* Show the indicator (dot/circle) when checked */
.radiocontainer input:checked ~ .checkmark:after {
	display: block;
}

/* Style the indicator (dot/circle) */
.radiocontainer .checkmark:after {
	top: 9px;
	left: 9px;
	width: 8px;
	height: 8px;
	border-radius: 50%;
	background: white;
}

.pj-preloader {
	display: none;
	position: absolute;
	height: 383px;;
	width: 940px;
	background: url("resources/img/spinner.gif") no-repeat scroll center
		center rgba(153, 153, 153, 0.3);
	z-index: 9999;
	left: 260;
	position: absolute;
	top: 224px;
}
</style>
</head>
<body>
	<div id="page-container">
		<div class="foodkonnekt merchant">
			<div class="inner-container">
				<div class="max-row">

					<header id="page-header">
						<div class="inner-header">
							<div class="row">
								<div class="logo">
									<a href="adminHome" title="FoodKonnekt Dashboard" class="logo"><img
										src="resources/img/foodkonnekt-logo.png"></a>
								</div>
								<!--.logo-->
								<%@ include file="adminHeader.jsp"%>
							</div>
							<!--.row-->
						</div>
						<!--.inner-header-->
					</header>
					<!--#page-header-->

					<div id="page-content">
						<div class="outer-container">
							<div class="row">
								<div class="content-inner-container">
									<%@ include file="leftMenu.jsp"%>
									<div class="right-content-container">
										<div class="right-content-inner-container">

											<div class="content-header">
												<div class="all-header-title"></div>
												<!--.header-title-->
												<div class="content-header-dropdown"></div>
												<!--.content-header-dropdown-->
											</div>
											<!--.content-header-->

											<div class="merchant-page-data">
												<div class="merchant-actions-outbound">
													<div class="merchat-coupons-container">

														<div class="coupons-navigation">
															<ul>
																				 <li class="current-menu-item"><a href="pizzaTamplate">Template</a></li>
                                                                        <li><a href="pizzaTopping">Topping</a></li>
                                                                         <li id="pizzaCrust"><a href="pizzaCrust">Crust</a></li> 
                                                                        <li id="pizzaSize"><a href="pizzaSize">Size</a></li>
                                                                        <li id="templateTaxMap"><a href="templateTaxMap">Tax Map</a></li>
                                                                        <li><a href="pizzaCategory">Pizza Category</a></li>
																			</ul>
														</div>
														<!--.coupons-navigation-->

														<div class="delivery-zones-content-container">
															
															<form:form action="savePizzaTemplateMasterForm" method="POST" modelAttribute="PizzaTemplate" onsubmit="return save()">
															<span id="errorMessage1"
																	style="color: red; font-size: 2000;" tabindex="0"></span>
															<br>
															<span id="nameError"
																	style="color: red; font-size: 2000;"></span><br><br>
															<div class="adding-products-form">
																	<div>
																	
																		<label>Template Name:</label>
																		<form:input path="description"  id="pizzaname" placeholder="pizza Template name" required="true"/>
																	</div>
																	<br>
																	<div>
																		<label>Description:</label>
																		<form:input path="description2" placeholder="pizza Description" />
																	</div>
																	<br>
																	<div>
																		<label>Status:</label>
																		<form:radiobutton path="active" name="item" value="0"
																			id="activeStatus" />
																		Active <br> 
																		<label></label>
																		<form:radiobutton path="active" name="item" value="1" 
																			id="inactiveStatus" />
																		InActive <br>
																	</div><br>
																	
																	<div>
																		<label>Pizza Sizes:</label>
																		<div style="margin-left: 200px;">
																			<c:forEach items="${pizzaSizes}" var="pizzaSizes" varStatus="status">
																		
																				<form:checkbox path="sizeId" value="${pizzaSizes.id}" 
																					id="checkBox_${pizzaSizes.id}" onclick="showSizePrice(${pizzaSizes.id})"/>&nbsp;&nbsp;
																				${pizzaSizes.description}
																				<br>
																				
																			<price id="dollarTag_${pizzaSizes.id}" style="display:none;margin-left: 150px; margin-top: -31px;">$</price>	
																				
																			<form:input path="sizePrice" id="sizePrice_${pizzaSizes.id}" 
																			style="margin-left: 191px;margin-top: -31px;display:none;" oninput="validateNumber(this);"  min="0" maxlength="5" class="dollar"/><br>
																			
																			</c:forEach>
																		</div>
																	</div><br>
																	
																	
																	<div>
																		<label>Categories:</label>
																		<div style="margin-left: 200px;">
																			<form:select id="categoryId" class="multiselect"
																	multiple="multiple" path="catId">
																	<c:forEach items="${categories}" var="categories"
																		varStatus="status">
																		<option value="${categories.id}">${categories.name}</option>
																	</c:forEach>
																</form:select>
																		</div>
																	</div><br>
																	
																	<%-- <div>
																		<label>Topping:</label>
																		<div style="margin-left: 200px;">
																			<form:select id="toppingId" class="multiselect"
																	multiple="multiple" path="toppingId">
																	<c:forEach items="${pizzaToppings}" var="pizzaToppings"
																		varStatus="status">
																		<option value="${pizzaToppings.id}">${pizzaToppings.description}</option>
																	</c:forEach>
																</form:select>
																		</div>
																	</div><br> --%>
																	
															</div><br>
															
															
															
															
															
															<div class="button left">
																<input type="submit" id="updateItemButton" value="Save">
															</div>
															
															</form:form>
													    </div>
															

														</div>
														<!--.coupons-content-container-->
														<div id="errorDiv" style="color: red"></div>
													</div>
													<!--.merchat-coupons-container-->
												</div>
												<!--.merchant-actions-outbound-->
											</div>
											<!--.merchant-page-data-->

										</div>
										<!--.right-content-inner-container-->
									</div>
								</div>

								<div class="methodsdata"></div>


							</div>
							<!--.row-->

						</div>
						<!--.outer-container-->

					</div>
					<!--#page-content-->
					<%@ include file="adminFooter.jsp"%>
				</div>
				<!--.max-row-->
			</div>
			<!--.inner-container-->
		</div>
		<!--.foodkonnekt .dashboard-->
	</div>
	<!--#page-container-->


</body>

<script type="text/javascript">
var count=0;
	function showSizePrice(id){
		
		if($("#checkBox_"+id).is(":checked")){
			count=count+1;
			$("#sizePrice_"+id).css('display','block');
			$("#dollarTag_"+id).css('display','block');
			$("#sizePrice_"+id).attr("required","true");
		}else{
			count=count-1;
			$("#sizePrice_"+id).css('display','none');
			$("#dollarTag_"+id).css('display','none');
			$("#sizePrice_"+id).removeAttr("required");
		}
	}
	
	var validNumber = new RegExp(/^\d*\.?\d*$/);
	var lastValid = 0;
	function validateNumber(elem) {
		  if (validNumber.test(elem.value)) {
		    lastValid = elem.value;
		  } else {
		    elem.value = lastValid;
		  }
		}
	
	
	$(function () {
		 $('#categoryId').multiselect({
	         maxHeight: 450 ,
        	includeSelectAllOption: true
        }); 
		 
		 $('#toppingId').multiselect({
		    maxHeight: 450 ,
	      	includeSelectAllOption: true
	     }); 
	}); 
	
	
	
	function save()
	{
		$('#errorMessage1').html("");
		if(count>0)
			{
			$("#errorMessage1").html("");
			return true;
			}
		if(count<1){
		$("#errorMessage1").html("please select a size ");
		$("#errorMessage1").focus();
		return false;
		}
		
		
	}
</script>
<script>
var pizzacheck="";

$(document).ready(function() {
	
	pizzacheck = "<%=request.getParameter("duplicatepizzaCheck")%>";
	
	var chk = "${pizzaTemplate.isDefaultTaxRates}";
	if(pizzacheck != "null")
	 $('#errorMessage1').html(pizzacheck);
	
});
</script>

</html>
