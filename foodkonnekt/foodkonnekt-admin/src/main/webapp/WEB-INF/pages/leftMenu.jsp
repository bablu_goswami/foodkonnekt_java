<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="left-nav-bar">
    <nav class="left-nav-bar-container">
        <div class="accordion-wrapper">
            <div class="ac-pane">
                <a href="adminHome" class="adminHome"> <span>Dashboard</span>
                </a>
            </div>
            
            <div class="ac-pane">
                <a href="allOrders" class="allOrders"> <span>Orders</span>
                </a>
            </div>
            <div class="ac-pane">
                <a href="inventory" class="inventory"> <span>Inventory</span>
                </a>
            </div>
           
            <div class="ac-pane" id="pizzaTab" style="display: none;">
                <a href="pizzaTamplate" class="pizzaTamplate"> <span>Pizza</span>
                </a>
            </div>
            <div class="ac-pane">
                <a href="onLineOrderLink" class="onLineOrderLink"> <span>Merchant</span>
                </a>
            </div>
           <!--  <div class="ac-pane">
                <a href="#" class="ac-title" data-accordion="true"> <span>Merchant</span> <i class="fa"></i>
                </a>
                <div class="ac-content">
                    <ul>
                        <li><a href="merchants">Merchant</a></li>
                        <li><a href="onLineOrderLink">Online Order Link</a></li>
                        <li><a href="adminLogo">Upload Logo</a></li>
                        <li><a href="deliveryZones">Add Delivery Zone</a></li>
                        <li><a href="pickUpTime">Add Pick Time</a></li>
                        <li><a href="convenienceFee">Convenience Fee</a></li>
                        <li><a href="customers">Customers</a></li>
                        <li><a href="createVouchar">Add Vouchars</a></li>
                        <li><a href="vouchars">Vouchars</a></li>
                    </ul>
                </div>
            </div> -->
        </div>
    </nav>
    <!--.left-nav-bar-container-->
    <div class="sidebar-logo">
    
        <img src="/${merchant.merchantLogo}" onerror="this.src='resources/img/foodkonnekt-logo.png'" width="250" height="150">
    </div>
    <!--.sidebar-logo-->
</div>
<!--.left-nav-bar-->
<script type="text/javascript">
var status= "${sessionScope.pizzaShowStatus}";
$(document).ready(function() {
	 if(status=="success"){
		 $("#pizzaTab").css('display','block');
	 }
 });
</script>