<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="true"%>
<!doctype html>
<html class="no-js" lang="en">
  <head>
  
    <title>FoodKonnekt | Dashboard</title>
    <!--CALLING STYLESHEET STYE.CSS-->
    <link rel="stylesheet" href="resources/css/style.css">
    <!--CALLING STYLESHEET STYLE.CSS-->
    
    <!--CALLING GOOGLE FONT OPEN SANS-->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    <!--CALLING GOOGLE FONT OPEN SANS-->
    
    <!--CALLING FONT AWESOME-->
    <link rel="stylesheet" href="resources/css/font-awesome.css">
    <!--CALLING FONT AWESOME-->
    
    <!--CALLING FILTER OPTIONS-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/jquery.dataTables.min.js"></script>
    <script src="https://www.jqueryscript.net/demo/Powerful-jQuery-Data-Table-Column-Filter-Plugin-yadcf/jquery.dataTables.yadcf.js"></script>
    <!--OPENS DIALOG BOX-->
    <link rel="stylesheet" type="text/css" href="resources/css/dialog-box/component.css" />
    <!--OPENS DIALOG BOX-->
    <style>
    .errorClass { border:  1px solid red; }
    </style>
    <style>
/* The container */
.radiocontainer {
    display: block;
    position: relative;
    padding-left: 35px;
    margin-bottom: 12px;
    cursor: pointer;
    font-size: 18px;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
}

/* Hide the browser's default radio button */
.radiocontainer input {
    position: absolute;
    opacity: 0;
    cursor: pointer;
}

/* Create a custom radio button */
.checkmark {
    position: absolute;
    top: 0;
    left: 0;
    height: 25px;
    width: 25px;
    background-color: #eee;
    border-radius: 50%;
}

/* On mouse-over, add a grey background color */
.radiocontainer:hover input ~ .checkmark {
    background-color: #ccc;
}

/* When the radio button is checked, add a blue background */
.radiocontainer input:checked ~ .checkmark {
    background-color: orange;
}

/* Create the indicator (the dot/circle - hidden when not checked) */
.checkmark:after {
    content: "";
    position: absolute;
    display: none;
}

/* Show the indicator (dot/circle) when checked */
.radiocontainer input:checked ~ .checkmark:after {
    display: block;
}

/* Style the indicator (dot/circle) */
.radiocontainer .checkmark:after {
 	top: 9px;
	left: 9px;
	width: 8px;
	height: 8px;
	border-radius: 50%;
	background: white;
}

.pj-preloader {
    display: none;
    position: absolute;
    height: 383px;;
    width: 940px;
    background: url("resources/img/spinner.gif") no-repeat scroll center center rgba(153, 153, 153, 0.3);
    z-index: 9999;
    left: 260;
    position: absolute;
    top: 224px;
}
</style>
  </head>
  <body>
    <div id="page-container">
        <div class="foodkonnekt merchant">
            <div class="inner-container">
                <div class="max-row">
                    
                    <header id="page-header">
                        <div class="inner-header">
                            <div class="row">
                                <div class="logo">
                                     <a href="adminHome" title="FoodKonnekt Dashboard" class="logo"><img src="resources/img/foodkonnekt-logo.png"></a>
                                </div><!--.logo-->
                                   <%@ include file="adminHeader.jsp"%>
                            </div><!--.row-->
                        </div><!--.inner-header-->
                    </header><!--#page-header-->
                    
                    <div id="page-content">
                        <div class="outer-container">
                            <div class="row">
                                <div class="content-inner-container">
                                         <%@ include file="leftMenu.jsp"%>                                
                                    <div class="right-content-container">
                                        <div class="right-content-inner-container">
                                        
                                            <div class="content-header">
                                                <div class="all-header-title">
                                                </div><!--.header-title-->
                                                <div class="content-header-dropdown">
                                                </div><!--.content-header-dropdown-->
                                            </div><!--.content-header-->
                                            
                                            <div class="merchant-page-data">
                                                <div class="merchant-actions-outbound">
                                                    <div class="merchat-coupons-container">

                                                        <%@ include file="adminMerchantMenu.jsp"%>
                                                        
                                                      <input type="hidden" value="${merchantConfigurationStatus}" id="notificationStatus">
                                                      <input type="hidden" value="${merchantConfigurationAutoAccept}" id="notificationAutoAccept">
                                                      <input type="hidden" value="${autoAcceptOrder}" id="autoAcceptOrder">
                                                      
                                                        <div class="delivery-zones-content-container">
                                                            <h3>Add Notification</h3>
                                                            
                                                            <div id="LoadingImage" style="display: none" align="middle">
                                                          <img src="resources/img/spinner.gif" align="middle" />
                                                             </div>
                                                            
                                                            <div class="clearfix"></div>


															<div >
<!-- 																Enable Notification: <input type="checkbox" id="enableNotificationId" value="0"onclick="enableNotification()">
 -->																
																
																<label class="radiocontainer">
																									Enable Notification
																									 <input type="radio" id="enableNotificationId" name="enableNotificationId" value="1" onclick="enableNotification()">
																									<span class="checkmark"></span></label>
																								
																									<label class="radiocontainer">Auto Accept
																									 <input type="radio" id="autoAcceptTrue" name="enableNotificationId" value="0" onclick="enableNotification()" >
																										<span class="checkmark"></span>
																										</label>
																									
																									<label>
																									<div style="display:none;" id="autoAcceptDiv">
																									&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;  <input type="checkbox" id="autoAccept" name="autoAccept" value="1" onclick="enableNotification()">
																									<label style="margin-left: 55px;  margin-top: -21px;">Auto Accept Future Order
																									<span class="checkmark"></span></label></div></label>
																								
															</div>



   



                                                          
                                                          <c:if test="${appRunningStatus!='NA'}">
                                                          <br><div>
                                                                           <h3 >App Running Status &nbsp;&nbsp; : &nbsp;&nbsp;<a style="color: blue;"> ${appRunningStatus} </a></h3>
                                                                     </div> </c:if>

															<div class="add-coupon-container">
                                                                <div class="add-coupon-container-form" style="margin-top: 2px;">
                                                                <input type="hidden" value="${merchant.id}" id="merchantId">
                                                                     <form:form method="POST" action="saveNotificationMethod"  style="width: max-content;"  modelAttribute="NotificationMethod" id="setDeliveryZoneForm" autocomplete="off" name="formID">
                                                                       
                                                                        <font size="3" color="red" id="message"> ${param.message} </font>
                                                                        <span id="errorMsg" style="color:red;font-size: 16px;"></span> 
                                                                       <br><br>
                                                                       <div>
                                                                       <c:forEach items="${methodTypes}" var="view"
																							varStatus="status">
																							
																							<c:choose>
																							<c:when test="${view.id==1}">
																							
																		
																							<label>Methods:</label>
                                                                       <label class="radiocontainer">${view.methodType}
                                                                              <form:radiobutton path="notificationMethodType.id" value="${view.id}" checked="checked" name="radioButton" onclick="orderAcceptance(${view.id})" id="methodType_${view.id}"/>
                                                                               <span class="checkmark"></span>
                                                                       </label>
                                                                       
                                                                   
                                                                       
                                                                       
																							</c:when>
																							<c:otherwise>
																							<label class="radiocontainer">${view.methodType}
                                                                              <form:radiobutton path="notificationMethodType.id" value="${view.id}" name="radioButton" id="methodType_${view.id}" onclick="orderAcceptance(${view.id})" />
                                                                               <span class="checkmark"></span>
                                                                              
                                                                       </label>
																							</c:otherwise>
																							</c:choose>
																						</c:forEach>
																		</div>
																		<br>
																		<div>
                                                                        <label>Name:</label>
                                                                        <form:input path="contactName"  maxlength="30" id="zoneName" placeholder="Contact Name" />
                                                                       </div>
                                                                     
                                                                       	<div>
                                                                        <label>Contact:</label>
                                                                        <form:input path="contact" maxlength="50" id="zoneDistance" placeholder="Email/Phone No/Fax No"  />
                                                                         </div>
                                                                        
                                                                        <br>
                                                                        <div>
                                                                        <label>Status:</label>
                                                                        <form:checkbox path="active" value="1" checked="checked"/>
                                                                        </div>
                                                                     <%--    
                                                                         <br><br>
                                                                        <label id="orderAcceptance">Order Acceptance:</label>
																		<div class="radioButtons" style="font-size: 15px; margin-left: 607px;">
																		<form:radiobutton path="isOrderAccepted" value="0" id="auto" style="margin-left: -481px;" checked="checked" />AutoAccepted
																		<form:radiobutton path="isOrderAccepted" id="manual" value="1" />ManualAccepted
																		</div>
                                                                         --%>
                                                                        <div class="clearfix"></div>
                                                                        <div class="button" align="center">
                                                                             <input type="button" value="Add Notification" id="setZoneButton" style="margin-left: 30%;width: 50%;">&nbsp;&nbsp;
                                                                        </div><!--.button-->
                                                                        
                                                                       
                                                                        
                                                                    </form:form>
                                                                </div>
                                                                
                                                                <!--.add-coupon-container-form-->
                                                            </div><!--.add-coupon-container-->
                                                            
                                                        </div><!--.coupons-content-container-->
                                                       <div id="errorDiv" style="color:red"></div> 
                                                    </div><!--.merchat-coupons-container-->
                                                </div><!--.merchant-actions-outbound-->
                                            </div><!--.merchant-page-data-->
                                            
                                        </div><!--.right-content-inner-container-->
                                       
                                    </div><!--.right-content-container-->
                                </div><!--.content-inner-container-->
                             <div class="methodsdata">
                            <div class="pj-preloader" style="margin-left: 260px;"></div>
                                        <table width="78.5%" cellpadding="0" cellspacing="0" id="example" class="dataTable no-footer" role="grid" aria-describedby="example_info" style="width: 78.5%;" align="right">
                                                                <thead>
                                                                    <tr>
                                                                        <th align='center'>Name</th>
                                                                        <th align='center'>Method</th>
                                                                        <th align='center'>Phone NO/Email</th>
                                                                        <th align='center'>Active</th>
                                                                        <th align='center'>Created On</th>
                                                                        <th align='center'>Delete</th>
                                                                       
                                                                        
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <c:forEach items="${methods}" var="view"
                                                                        varStatus="status">
                                                                        <tr>
                                                                        
                                                                            <td align="center"><p>${view.contactName}</p></td>
                                                                             <td align="center"><p>${view.notificationMethodType.methodType}</p></td>
                                                                            <td align="center"><p>${view.contact}</p></td>
                                                                           <%--  <td align="center"><p>${view.active}</p> --%>
                                             <c:choose>
                                             <c:when test="${view.active==true}">
                                             <td align="center"><a href="#" style="color:blue" onclick="updateStatus(${view.id}, ${view.active})">Active</a></td>
                                               </c:when>    
                                               <c:otherwise>
                                               <td align="center"><a href="#" style="color:blue" onclick="updateStatus(${view.id}, ${view.active})">InActive</a></td>
                                                </c:otherwise>
                                                </c:choose>
                                              
                                              <td align="center"><p>${view.createdDate}</p></td>
                                                <td align="center"><a href="#" style="color:blue" onclick="deletenot(${view.id})">Delete</a></td>
                                                                              
                                                                        </tr>
                                                                       
                                                                    </c:forEach>
                                                                </tbody>
                                                            </table>
                                                            </div>
                            </div><!--.row-->
                            
                        </div><!--.outer-container-->
                        
                    </div><!--#page-content-->
                             <%@ include file="adminFooter.jsp"%>                    
                </div><!--.max-row-->
            </div><!--.inner-container-->
        </div><!--.foodkonnekt .dashboard-->
    </div><!--#page-container-->
    
    <script>
        var polyfilter_scriptpath = '/js/';
    </script>
    <!--OPENS DIALOG BOX-->
      <script type="text/javascript">
      var status=0;
          $(document).ready(function() {
        	 
        	  methodTypeValue = 1;
        	  
        	  var notificationStatus=$('#notificationStatus').val();
        	  var autoAccept=$('#notificationAutoAccept').val();
        	  var autoAcceptOrder = $("#autoAcceptOrder").val();
        	  if(notificationStatus=='true'){
        		  $('#enableNotificationId').prop('checked', true);
        	  }
        	 
        	  if(autoAcceptOrder == 'true'){
        		  $('#autoAcceptTrue').prop('checked', true);
        		  $('#autoAcceptDiv').css('display', 'block');
        	  }
        	  
        	  if(autoAccept=='true'){
        		  $('#autoAccept').prop('checked', true);
        		  $('#autoAcceptTrue').prop('checked', true);
        		  $('#autoAcceptDiv').css('display', 'block');
        	  }
        	  
        	  $("#zoneName").bind('blur', function () {
                  var zoneName=$(this).val();
                  
                  $.ajax({
                   url : "checkDeliveryZoneName?diliveryZoneName=" + zoneName,
                   type : "GET",
                   contentType : "application/json; charset=utf-8",
                   success : function(statusValue) {
                     if (statusValue=="true") {
                       
                       $("#zoneName").css('border-color', 'red');
                       $("#zoneName").focus();
                       $("#errorBox").html("Delivery zone name '"+zoneName+"' already exists");
                       status=1;
                     }else{
                       $("#errorBox").html("");
                       $("#zoneName").css('border-color', '');
                       status=0;
                     }
                   },
                   error : function() {
                     alert("error");
                   }
                 })            
               });
        	  
        	
            $("#setZoneButton").click(function() {
             var name= document.getElementById("zoneName").value;
             var contact= document.getElementById("zoneDistance").value;
             
             if((name=="" || name==undefined)){
            	 document.getElementById("errorMsg").innerHTML = "Please enter Name";
            	 
             }else if(methodTypeValue=="1" && (contact=="" || contact==undefined)){
            	 document.getElementById("errorMsg").innerHTML = "Please enter EMail";
             
             }else if(methodTypeValue=="2" && (contact=="" || contact==undefined)){
            	 document.getElementById("errorMsg").innerHTML = "Please enter Phone Number";
             
             }else if(methodTypeValue=="3" && (contact=="" || contact==undefined)){
            	 document.getElementById("errorMsg").innerHTML = "Please enter Fax Number";
            	 
             }else if(methodTypeValue=="1" && !/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(contact)){
            	 document.getElementById("errorMsg").innerHTML = "Please enter valid EMail";
           
             }else if(methodTypeValue=="2" && contact.length < 10){
            	 document.getElementById("errorMsg").innerHTML = "Please enter valid Phone number";
           
             }else{
            	 $("#setDeliveryZoneForm").submit();
             }
         });
            
            $("#setZoneCancelButton").click(function() {
            	window.location="deliveryZones"
            });
            
            $('#zoneDistance').on('input', function() {
            	
            	if(methodTypeValue=="1"){
            		$("#zoneDistance").attr("maxlength",50);
            	}
            	
            	else if(methodTypeValue=="2"){
            	$("#zoneDistance").attr("maxlength",10);
            	
            	var number = $(this).val().replace(/[^\d]/g, '')
            	if (number.length == 7) {
                    number = number.replace(/(\d{3})(\d{4})/, "$1-$2");
                } else if (number.length == 10) {
                    number = number.replace(/(\d{3})(\d{3})(\d{4})/, "$1-$2-$3");
                }
                $(this).val(number)
            	}
            	
            	else if(methodTypeValue=="3"){
            		$("#zoneDistance").attr("maxlength",50);
            		if (number.length == 7) {
                        number = number.replace(/(\d{3})(\d{4})/, "$1-$2");
                    } else if (number.length == 10) {
                        number = number.replace(/(\d{3})(\d{3})(\d{4})/, "$1-$2-$3");
                    }
                    $(this).val(number)
                	}
            });
         });
          function isNumberKey(evt)
          {
        	  var target = evt.target || evt.srcElement; // IE

        	    var id = target.id;
        	    var data=document.getElementById(id).value;
        	  
        	  
        	  if(evt.which == 8 || evt.which == 0){
                  return true;
              }
              if(evt.which < 46 || evt.which > 59) {
                  return false;
                  //event.preventDefault();
              } // prevent if not number/dot

              if(evt.which == 46 && data.indexOf('.') != -1) {
                  return false;
                  //event.preventDefault();
              }
          }
          
          function currectNo(evt){
        	  var target = evt.target || evt.srcElement; // IE

      	    var id = target.id;
      	    var data=document.getElementById(id).value;
      	    
      	    var res=data.split(".");
        	  
        	  if(res.length==2){
        		if(res[1]){
        			
        		}else{
        			
        			document.getElementById(id).value=res[0]+".0";
        		}
        	  }
          }
          
           function updateStatus(id,status){
        	   $('.pj-preloader').css('display','block');
        	   if(status==true){
        		  status= false;
        	  } else{
        		  status=true;
        	  }
        	  $('.pj-preloader').css('display','block');
        	  $.ajax({
                  type: 'GET',
                  url: "updateStatus?id="+id+"&status="+status,
                  success:function(data){
                	  $('.pj-preloader').css('display','none');
                	  location.reload();
                  }
               }); 
            }
           
     function deletenot(id){
    	  $('.pj-preloader').css('display','block');
    	 $.ajax({
             type: 'GET',
             url: "deleteNotificationMethod?id="+id,
             success:function(data){
              $('.pj-preloader').css('display','none');
           	  location.reload();
             }
          }); 
     }
     
     var methodTypeValue;
     function orderAcceptance(methodTypeid){
    	 
    	methodTypeValue=methodTypeid;
    	/* var merchantId=document.getElementById('merchantId').value;
    	var auto= document.getElementById('auto').value;
    	var manual= document.getElementById('manual').value;

    	if(methodTypeid=='1' || methodTypeid=='2'){
    		 $('.radioButtons').css('display','block');
    		 $('#orderAcceptance').css('display','block');
    	}else{
    		$('.radioButtons').css('display','none');
    		$('#orderAcceptance').css('display','none');
    	}
    	
    	 $.ajax({
             type: 'GET',
             url: "getOrderAcceptance?merchantId="+merchantId+"&methodTypeid="+methodTypeid,
             success:function(data){
              if(data=='0' && (methodTypeid=='1' || methodTypeid=='2')){
            	 $("#manual").prop('checked', false);
             	 $("#auto").prop('checked', true);
             	$("#auto").prop("disabled", true);
           	    $("#manual").prop("disabled", true);
              }else if(data=='1' && (methodTypeid=='1' || methodTypeid=='2')){
            	 $("#manual").prop('checked', true);
            	 $("#auto").prop('checked', false);
            	 $("#auto").prop("disabled", true);
            	 $("#manual").prop("disabled", true);
              } 
           }
       });  */
       
    	document.getElementById("errorMsg").innerHTML = " ";
    	$('#zoneDistance').val('');
    }   
     
     function futureOrderNotification()
     {
    	 
    	 if(jQuery("#autoAcceptTrue").is(":checked")){
    		 $("#autoAcceptDiv").css('display','block');
    	 }
    	 else{
    		 $("#autoAcceptDiv").css('display','none');

    	 }
     }
     
     
 	function enableNotification()
	{
 	var enableNotification = 0;
 	var autoAccept=0;
 	var autoAcceptOrder = 0;
 	var merchantId=document.getElementById('merchantId').value;
 	 if(jQuery("#enableNotificationId").is(":checked")){
 		$("#autoAcceptDiv").css('display','none');
 		$("#autoAccept"). prop("checked", false);
 		enableNotification = 1;
 		}
	    
 		if(jQuery("#autoAcceptTrue").is(":checked")){
 			autoAcceptOrder =1;
 		$("#autoAcceptDiv").css('display','block');
 		if(jQuery("#autoAccept").is(":checked")){
 			autoAccept=1;
 		}
 	 }
 	 $("#LoadingImage").show();
  	 $.ajax({
         type: 'GET',
         url: "saveMerchantConfiguration?merchantId="+merchantId+"&enableNotification="+enableNotification+"&autoAccept="+autoAccept+"&autoAcceptOrder="+autoAcceptOrder,
         success:function(data){
        	 $("#LoadingImage").hide();
         },
  	  error: function () {
     	 $("#LoadingImage").hide();
        }
      }); 
	}
 	
 	var status= "${sessionScope.pizzaShowStatus}";
 	$(document).ready(function() {
 		 if(status=="success"){
 			 $("#pizzaTab").css('display','block');
 		 }
 	 });
    </script>
  </body>
</html>
