<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@page session="true"%>
<!doctype html>
<html class="no-js" lang="en">
<head>
<title>FoodKonnekt | Add Products</title>
 <link href="resources/css/bootstrap.min.css"
        rel="stylesheet" type="text/css" />
        <link href="resources/css/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
<!--CALLING STYLESHEET STYE.CSS-->
<link rel="stylesheet" href="resources/css/style.css">
<!--CALLING STYLESHEET STYLE.CSS-->

<!--CALLING GOOGLE FONT OPEN SANS-->
<link
	href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic'
	rel='stylesheet' type='text/css'>
<!--CALLING GOOGLE FONT OPEN SANS-->

<!--CALLING FONT AWESOME-->
<link rel="stylesheet"
	href="resources/css/font-awesome.css">
<!--CALLING FONT AWESOME-->

<!--OPENS DIALOG BOX-->
<link rel="stylesheet" type="text/css"
	href="resources/css/dialog-box/component.css" />
<!--OPENS DIALOG BOX-->

<!--CALLING PRODUCTS TABS-->
<link rel='stylesheet' type='text/css'
	href='resources/css/products-tabs/opentabby.css' />
<!--CALLING PRODUCTS TABS-->
<!-- <link rel="stylesheet" type="text/css"
	href="https://cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css"> -->
	
<style type="text/css">
.body{
overflow: scroll !important;
}
.sd-modifiers-each-field-label label label {
    height: auto;
    display: inline-block;
    max-width: 20px;
    margin-left: 10px;
}
.sd-modifiers-each-field-label label span {
    float: left;
}

div#example_paginate {
	display: block;
}

div#example_filter {
	display: block;
}

div#example_length {
	display: block;
}

input[type="search"] {
	max-width: 300px;
	width: 100%;
	outline: 0;
	border: 1px solid rgb(169, 169, 169);
	padding: 11px 10px;
	border-radius: 6px;
	margin-bottom: 7px;
	placeholder: Search Items;
}

input[type="text"], input[type="email"], input[type="password"], input[type="number"],
	input[type="date"] {
	height: 39px;
}

.pj-preloader {
    display: none;
    position: absolute;
    height: 383px;;
    width: 940px;
    background: url("resources/img/spinner.gif") no-repeat scroll center center rgba(153, 153, 153, 0.3);
    z-index: 9999;
    left: 260;
    position: absolute;
    top: 224px;
}

</style>
  </head>
  <body>
  <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
	<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
	<script src='resources/js/products-tabs/opentabby.js'></script>
	<script src="resources/js/popModal.js"></script>
	<script type="text/javascript" src="resources/js/bootstrap.min.js"></script>
    <script src="resources/js/bootstrap-multiselect.js" type="text/javascript"></script>
    <div id="page-container">
        <div class="foodkonnekt inventory">
            <div class="inner-container">
                <div class="max-row">
                    
                    <header id="page-header">
                        <div class="inner-header">
                            <div class="row">
                                
                                <div class="logo">
                                     <a href="adminHome" title="FoodKonnekt Dashboard" class="logo"><img src="resources/img/foodkonnekt-logo.png"></a>
                                </div><!--.logo-->
                                <%@ include file="adminHeader.jsp" %> 
                                
                            </div><!--.row-->
                        </div><!--.inner-header-->
                    </header><!--#page-header-->
                    
                    <div id="page-content">
                        <div class="outer-container">
                        
                            <div class="row">
                                <div class="content-inner-container">
                                    
                                     <%@ include file="leftMenu.jsp"%>
                                
                                    <div class="right-content-container">
                                    
                                        <div class="right-content-inner-container">
                                        
                                            <div class="content-header">
                                                <div class="all-header-title">
                                                </div><!--.header-title-->
                                            </div><!--.content-header-->
                                            
                                            <div class="merchant-page-data">
                                        
                                                <div class="merchant-actions-outbound">
                                                    <div class="merchat-coupons-container">
                                                            <!-- <main> -->
                                                                 
                                                            <div class="coupons-navigation">
                                                                     <ul>
                                                                         <li ><a href="pizzaTamplate">Template</a></li>
                                                                        <li ><a href="pizzaTopping">Topping</a></li>
                                                                        <li id="pizzaCrust" class="current-menu-item"><a href="pizzaCrust">Crust</a></li> 
                                                                        <li id="pizzaSize"><a href="pizzaSize">Size</a></li>
                                                                        <li><a href="templateTaxMap">Tax Map</a></li>
                                                                        <li><a href="pizzaCategory">Pizza Category</a></li>
                                                                    </ul>
                                                        	</div>
                                                        
                                                        
                                                        <div id="LoadingImage" style="display: none" align="middle">
                                                          <img src="resources/img/spinner.gif" align="middle" />
                                                             </div>
                                                              <section id="content1">
                                                                <div class="tab-content-container-outbound">
                                                                    <div class="tab-content-container">
                                                                        <div class="tab-content-container-inbound">
                                                                            <div class="only-search-part">
                                                                              
                                                                              <form:form method="POST" action="updatePizzaCrust"
																			modelAttribute="pizzaCrust" id="pizzaCrustForm"> 
                                                                              
                                                                              <form:hidden path="id" value="${pizzaCrust.id}" id="pizzaCrust_id" />
                                                                              
                                                                              <div class="adding-products">
																				<label id="errorMessage" style="color: red;"></label>
																				<div class="adding-products-form">
																					
																					<div class="clearfix"></div>
																					<label>Description:</label>
																					<textarea maxlength="140"  id="description" readonly="true" name="description" required="required">${pizzaCrust.description}</textarea>
																					
																					
																					<br>
																			<div class="clearfix"></div>
																			<label>Crust Status:</label>
																			<form:radiobutton path="active" name="item"
																				value="1" id="activeStatus" /> Active
																			<form:radiobutton path="active" name="item"
																				value="0" id="inactiveStatus" /> InActive<br>
																				<br>
																				
																				<br>
																			
																			
																		<div>
																		<label>Pizza Sizes:</label>
																		<br>
																		<div style="margin-left: 200px;">
																		
																		<br>
																			<c:forEach items="${pizzaCrustSizeList}" var="pizzaSizes" varStatus="status">
																		<c:if test="${pizzaSizes.pizzaSize.active==1}">
																		<c:if test="${pizzaSizes.active==1}">
																				<form:checkbox
																				 path="sizeId" value="${pizzaSizes.id}" 
																					id="checkBox_${pizzaSizes.pizzaSize.id}" 
																					onclick="showSizePrice(${pizzaSizes.pizzaSize.id})" checked="true"/>
																					&nbsp;&nbsp;
																				${pizzaSizes.pizzaSize.description}
																				  
																				<br>
																					<price id="dollarTag_${pizzaSizes.pizzaSize.id}" style="display:block;margin-left: 150px; margin-top: -31px;">$</price>
																			<form:input path="pricelist" type="text" id="sizePrice_${pizzaSizes.pizzaSize.id}" value="${pizzaSizes.price}"  style="margin-left: 191px;margin-top: -31px;display:block;" oninput="validateNumber(this);"  min="0" maxlength="5" class="dollar" /><br>
																			
																					</c:if>
																				<c:if test="${pizzaSizes.active==0}">
																				<form:checkbox
																				 path="sizeId" value="${pizzaSizes.id}" 
																					id="checkBox_${pizzaSizes.pizzaSize.id}" 
																					onclick="showSizePrice(${pizzaSizes.pizzaSize.id})"/>
																					&nbsp;&nbsp;
																				${pizzaSizes.pizzaSize.description}
																				<br>
																					<price id="dollarTag_${pizzaSizes.pizzaSize.id}" style="display:none;margin-left: 150px; margin-top: -31px;">$</price>
																			<form:input path="pricelist" type="text" id="sizePrice_${pizzaSizes.pizzaSize.id}" value="${pizzaSizes.price}"   style="margin-left: 191px;margin-top: -31px;display:none;" oninput="validateNumber(this);"  min="0" maxlength="5" class="dollar" /><br>
																			
																					</c:if>
																					
																					</c:if>
																		
																				<br>
																			
																			</c:forEach>
																		</div>
																	</div>
																			
																			<%-- 
																				<form:select id="size_grps"  path="pizzaSizesIds">
																				</form:select>
																				 --%>
																				
																				
																				
																				<!-- <div><a href=javascript:void(0) class='nav-toggle' itmId="pizzaTemplate.getId() " style='color: blue;'>Active</a></div> -->
																				
																		<!-- 		<div class="clearfix"></div>
																				<label>Pizza Size:</label>  -->
																				
																			
																				<div class="button left">
																  <input type="submit" id="updatePizzaCrustButton" value="Save">&nbsp;&nbsp; 
																 <input type="submit" value="Cancel" id="updatePizzaCrustCancelButton">
																</div>	
																
																					 </form:form>
															                     
																					
																			</div>
																			
																				<div class="button left">
<!--                                                                              <button id="addpizza" style="margin-left:295px;" onclick="popup()">Add New Size</button>
 -->  
    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModal1" 
                    	onclick="popupOpen()"  style="margin-left:295px;">Map New Size</button>
                                                                                                </div>
																				</div>
																			
																			</div>
																			
																			
                                                                              
                                                                            </div><!--.only-search-part-->
                                                                        </div><!--.tab-content-container-inbound-->
                                                                        </section>
                                                                    </div><!--.tab-content-container-->
                                                                </div><!--.tab-content-container-outbound-->
                                                            
                                                                
                                                                
                                                            </main>
                                                        </div><!--.inventory-tabs-inbound-->
                                                    </div><!--.inventory-tabs-->
                                                </div><!--.inventory-tabs-outbound-->
                                            </div><!--.inventory-page-data-->
                                            
                                        </div><!--.right-content-inner-container-->
                                    </div><!--.right-content-container-->
                                </div><!--.content-inner-container-->
                            </div><!--.row-->
                            
                        </div><!--.outer-container-->
                    </div><!--#page-content-->
                    
                    <%@ include file="adminFooter.jsp" %>
                    <!--#footer-container-->
                    
                </div><!--.max-row-->
            </div><!--.inner-container-->
        </div><!--.foodkonnekt .dashboard-->
    </div><!--#page-container-->
   
     < !-- add pizza size popup start -->
  <div id="myModal2" class="modal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="text-center">
                    	<img src="resources/img/logo.png" >
                    </h3>
                </div>
                <div class="modal-body" id="declineBody">
					<h4 class="text-center">
                        Map PizzaSize
                    </h4>
                    <fieldset >
                    <form:form method="POST" action="mapNewPizzaSizeWithCrust" modelAttribute="pizzaCrust" id="pizzaCrustForm1"> 
                                          <div class="adding-products" >
                                                                                                                        <form:hidden path="id" value="${pizzaCrust.id}" id="pizzaCrust_id" />
                                          
							<div class="adding-products-form" >
																					
																						
																			
							<div >
																											
							<c:forEach items="${dropdownPizzaSize}" var="dropdownPizzaSize" varStatus="status">
																		<c:if test= "${dropdownPizzaSize.active == 1}">
																				<c:if test= "${dropdownPizzaSize.active == 1}">
																		
																				<form:checkbox path="sizeId" value="${dropdownPizzaSize.id}" 
																					id="checkBox_${dropdownPizzaSize.id}" onclick="showMapSizePrice(${dropdownPizzaSize.id})" />&nbsp;&nbsp;
																				${dropdownPizzaSize.description}
																				
																				<br>
																				<price id="dollarTag_${dropdownPizzaSize.id}" style="display:none;margin-left: 150px; margin-top: -31px;">$</price>
																				<form:input path="sizePrice" id="sizePrice_${dropdownPizzaSize.id}" 
																					style="margin-left: 191px;margin-top: -31px;display:none;" oninput="validateNumber(this);"  min="0" maxlength="5" class="dollar" value=""  autocomplete="off"/><br>
																			
																				</c:if>
																				
																				<c:if test= "${dropdownPizzaSize.active == 0}">
																		
																				<form:checkbox path="sizeId" value="${dropdownPizzaSize.id}" 
																					id="checkBox_${dropdownPizzaSize.id}" onclick="showMapSizePrice(${dropdownPizzaSize.id})" />&nbsp;&nbsp;
																				${dropdownPizzaSize.description}
																				
																				<br>
																				<price id="dollarTag_${dropdownPizzaSize.id}" style="display:none;margin-left: 150px; margin-top: -31px;">$</price>
																				<form:input path="sizePrice" id="sizePrice_${dropdownPizzaSize.id}" 
																					style="margin-left: 191px;margin-top: -31px;display:none;" oninput="validateNumber(this);"  min="0" maxlength="5" class="dollar" value=""  autocomplete="off"/><br>
																			
																				</c:if>
																				</c:if>
																				</c:forEach>						
																			
																					 </form:form>
															                     
																													   <div class="modal-footer">
													
					  									
																				<div style="margin-left: 80px;" class="button left">
																  <input type="submit" id="popupupdatePizzaCrustButton" value="Save" style="padding-bottom: 7px;margin-left: -74px;max-width: 132px;" /> 
																  <input type="button" value="Cancel" onclick="popupClose()" style="padding-bottom: 7px;max-width: 129px;margin-left: 17px;" />
																  <input type="button" value="Add New Size" id="addNewSize" style="padding-bottom: 7px;max-width: 150px;margin-left: 11px;" />
																
																</div>	
																
																</div>
					   									
															
		
					  
                   </fieldset>
                </div>
       
                <div id="LoadingImage1" style="display: none" align="middle">
                    <img src="resources/imges/now-loading.gif" align="middle" />
                </div>
             	
           
            </div>
        </div>
    </div>
   
     <script type="text/javascript">
     
    
    
     
      $(document).ready(function() {
    	 
    	  
          var crustStatus="${pizzaCrust.active}";
          if(crustStatus==1){
            $("#activeStatus").attr('checked', 'checked');
          }
          if(crustStatus==0){
            $("#inactiveStatus").attr('checked', 'checked');
          }
          
          $("#addNewSize").click(function() {
              window.location="createPizzaSize"
            })
            
          });  
          
  </script>
   
   
   
   <script type="text/javascript">
  	var count=0;
   function showSizePrice(id){
		  var checkBox = document.getElementById("checkBox_"+id);
 		    if (checkBox.checked == true){
 		     count++;
 		      } 
 		    else {
 		    	count--;
 		        }
 		   
	   
	     	  if($("#checkBox_"+id).is(":checked")){
			$("#sizePrice_"+id).css('display','block');
			 $("#dollarTag_"+id).css('display','block');
			 $("#sizePrice_"+id).attr('required','true'); 
		}else{
			$("#sizePrice_"+id).css('display','none');
			 $("#dollarTag_"+id).css('display','none');
			 $("#sizePrice_"+id).removeAttr('required');
		} 
	}
   
   
   var mapcount = 0;
   function showMapSizePrice(id){
		  var checkBox = document.getElementById("checkBox_"+id);
		    if (checkBox.checked == true){
		    	mapcount++;
		      } 
		    else {
		    	mapcount--;
		        }
		 
		if(mapcount==0){
		  $("#popupupdatePizzaCrustButton").attr("disabled", true);

		}else{
		     $("#popupupdatePizzaCrustButton").attr("disabled", false);

		}

	     	  if($("#checkBox_"+id).is(":checked")){
			$("#sizePrice_"+id).css('display','block');
			 $("#dollarTag_"+id).css('display','block');
			 $("#sizePrice_"+id).attr('required','true'); 
		}else{
			$("#sizePrice_"+id).css('display','none');
			 $("#dollarTag_"+id).css('display','none');
			 $("#sizePrice_"+id).removeAttr('required');
		} 
	}

var validNumber = new RegExp(/^\d*\.?\d*$/);
var lastValid = 0;
function validateNumber(elem) {
  if (validNumber.test(elem.value)) {
    lastValid = elem.value;
  } else {
    elem.value = lastValid;
  }
}
   </script>
   
   
   <script type="text/javascript">
   
	$(document).ready(function() {
        $("#updatePizzaCrustButton").click(function () {
      	  
        	if(document.getElementById("sizePrice_"+id).value!=""){
  
          	$("#pizzaCrustForm").submit();
        	}
        });
        
        
        $("#updatePizzaCrustCancelButton").click(function() {
          window.location="pizzaCrust"
        });
    });
   
   </script>
   
   	<script>
				function popupOpen() {
					
					$("#myModal2").css("display", "block");
					$("#popupupdatePizzaCrustButton").attr("disabled", true);

					
				}

				function popupClose() {
					//document.getElementById('myModal1').style.display = "none";
					$("#myModal2").css("display", "none");
					
				}
				</script>	

 <script type="text/javascript">
   
	
	function popupupdatePizzaCrustCancelButton(){
		$("#myModal2").css("display", "none");
    }

   </script>
   
    
            <!--OPENS DIALOG BOX-->
	<!--CALLING TABS-->
	
	<!--CALLING TABS-->
  </body>
  
</html>
