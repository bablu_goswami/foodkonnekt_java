<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="true"%>
<!doctype html>
<html class="no-js" lang="en">
<head>

<title>FoodKonnekt | Dashboard</title>

<!--CALLING STYLESHEET STYE.CSS-->
<link href="resources/css/bootstrap.min.css"
        rel="stylesheet" type="text/css" />
        <link href="resources/css/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
<!--CALLING STYLESHEET STYE.CSS-->
<link rel="stylesheet" href="resources/css/style.css">
<!--CALLING STYLESHEET STYLE.CSS-->

<!--CALLING GOOGLE FONT OPEN SANS-->
<link
	href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic'
	rel='stylesheet' type='text/css'>
<!--CALLING GOOGLE FONT OPEN SANS-->

<!--CALLING FONT AWESOME-->
<link rel="stylesheet"
	href="resources/css/font-awesome.css">
<!--CALLING FONT AWESOME-->

<!--OPENS DIALOG BOX-->
<link rel="stylesheet" type="text/css"
	href="resources/css/dialog-box/component.css" />
<!--OPENS DIALOG BOX-->

<!--CALLING PRODUCTS TABS-->
<link rel='stylesheet' type='text/css'
	href='resources/css/products-tabs/opentabby.css' />
<!--CALLING PRODUCTS TABS-->
<!-- <link rel="stylesheet" type="text/css"
	href="https://cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css"> -->
	
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
	<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
	<script src='resources/js/products-tabs/opentabby.js'></script>
	<script src="resources/js/popModal.js"></script>
	<script type="text/javascript" src="resources/js/bootstrap.min.js"></script>
    <script src="resources/js/bootstrap-multiselect.js" type="text/javascript"></script>

<style>
.errorClass {
	border: 1px solid red;
}
</style>
<style>
/* The container */
.radiocontainer {
	display: block;
	position: relative;
	padding-left: 35px;
	margin-bottom: 12px;
	cursor: pointer;
	font-size: 18px;
	-webkit-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	user-select: none;
}

/* Hide the browser's default radio button */
.radiocontainer input {
	position: absolute;
	opacity: 0;
	cursor: pointer;
}

/* Create a custom radio button */
.checkmark {
	position: absolute;
	top: 0;
	left: 0;
	height: 25px;
	width: 25px;
	background-color: #eee;
	border-radius: 50%;
}

/* On mouse-over, add a grey background color */
.radiocontainer:hover input ~ .checkmark {
	background-color: #ccc;
}

/* When the radio button is checked, add a blue background */
.radiocontainer input:checked ~ .checkmark {
	background-color: orange;
}

/* Create the indicator (the dot/circle - hidden when not checked) */
.checkmark:after {
	content: "";
	position: absolute;
	display: none;
}

/* Show the indicator (dot/circle) when checked */
.radiocontainer input:checked ~ .checkmark:after {
	display: block;
}

/* Style the indicator (dot/circle) */
.radiocontainer .checkmark:after {
	top: 9px;
	left: 9px;
	width: 8px;
	height: 8px;
	border-radius: 50%;
	background: white;
}

.pj-preloader {
	display: none;
	position: absolute;
	height: 383px;;
	width: 940px;
	background: url("resources/img/spinner.gif") no-repeat scroll center
		center rgba(153, 153, 153, 0.3);
	z-index: 9999;
	left: 260;
	position: absolute;
	top: 224px;
}
</style>
</head>
<body>
	<div id="page-container">
		<div class="foodkonnekt merchant">
			<div class="inner-container">
				<div class="max-row">

					<header id="page-header">
						<div class="inner-header">
							<div class="row">
								<div class="logo">
									<a href="adminHome" title="FoodKonnekt Dashboard" class="logo"><img
										src="resources/img/foodkonnekt-logo.png"></a>
								</div>
								<!--.logo-->
								<%@ include file="adminHeader.jsp"%>
							</div>
							<!--.row-->
						</div>
						<!--.inner-header-->
					</header>
					<!--#page-header-->

					<div id="page-content">
						<div class="outer-container">
							<div class="row">
								<div class="content-inner-container">
									<%@ include file="leftMenu.jsp"%>
									<div class="right-content-container">
										<div class="right-content-inner-container">

											<div class="content-header">
												<div class="all-header-title"></div>
												<!--.header-title-->
												<div class="content-header-dropdown"></div>
												<!--.content-header-dropdown-->
											</div>
											<!--.content-header-->

											<div class="merchant-page-data">
												<div class="merchant-actions-outbound">
													<div class="merchat-coupons-container">

														<div class="coupons-navigation">
															<ul>
																<li><a href="pizzaTamplate">Template</a></li>
                                                                <li><a href="pizzaTopping">Topping</a></li>
                                                                <li class="current-menu-item"><a href="pizzaCrust">Crust</a></li>
                                                                <li><a href="pizzaSize">Size</a></li>
                                                                <li><a href="templateTaxMap">Tax Map</a></li>
                                                                <li><a href="pizzaCategory">Pizza Category</a></li>
															</ul>
														</div>
														<!--.coupons-navigation-->

														<div class="delivery-zones-content-container">
															
															<form:form action="savePizzaCrust" method="POST" modelAttribute="PizzaCrust" onsubmit="return save()">
															<span id="errorMessage1"
																	style="color: red; font-size: 2000;" tabindex="0"></span>
																
															<br>
															<span id="nameError"
																	style="color: red; font-size: 2000;"></span><br><br>
															<div class="adding-products-form">
																	<div>
																		<label>Crust Name:</label>
																		<form:input path="description" placeholder="pizza crust name" required="true"/>
																	</div>
																	<br>
																	
																	<div>
																		<label>Status:</label>
																		<form:radiobutton path="active" name="item" value="0" id="activeStatus"/>Active <br> 
																		<label></label>
																		<form:radiobutton path="active" name="item" value="1" id="inactiveStatus" />InActive
																	</div>
																	<br>
																	
																	<div>
																		<label>Pizza Template:</label>
																		<form:select id="crustId" class="multiselect" multiple="multiple" path="tempId">
																			<c:forEach items="${pizzaTemplates}" var="pizzaTemplate" varStatus="status" >
																				<option id="options" value="${pizzaTemplate.id}">${pizzaTemplate.description}</option>
																			</c:forEach>
																		</form:select>
																	</div>
																	<br>
																	
																	<div>
																		<label>Pizza Sizes:</label>
																		<div style="margin-left: 200px;">
																			<c:forEach items="${pizzaSize}" var="pizzaSizes" varStatus="status">
																		
																				<form:checkbox path="sizeId" value="${pizzaSizes.id}" 
																					id="checkBox_${pizzaSizes.id}" onclick="showSizePrice(${pizzaSizes.id})"/>&nbsp;&nbsp;
																				${pizzaSizes.description}
																				<br>
																				<price id="dollarTag_${pizzaSizes.id}" style="display:none;margin-left: 150px; margin-top: -31px;">$</price>
																			<form:input path="sizePrice" type="text" id="sizePrice_${pizzaSizes.id}" style="margin-left: 191px;margin-top: -31px;display:none;" oninput="validateNumber(this);"  min="0" maxlength="5" class="dollar"/><br>
																			
																			
																			
																			
																			</c:forEach>
																		</div>
																	</div><br>
															</div><br>
															
															<div class="button left">
																<input type="submit" id="updateItemButton" value="Save">
															</div>
															
															</form:form>
													    </div>
															

														</div>
														<!--.coupons-content-container-->
														<div id="errorDiv" style="color: red"></div>
													</div>
													<!--.merchat-coupons-container-->
												</div>
												<!--.merchant-actions-outbound-->
											</div>
											<!--.merchant-page-data-->

										</div>
										<!--.right-content-inner-container-->
									</div>
								</div>

								<div class="methodsdata"></div>


							</div>
							<!--.row-->

						</div>
						<!--.outer-container-->

					</div>
					<!--#page-content-->
					<%@ include file="adminFooter.jsp"%>
				</div>
				<!--.max-row-->
			</div>
			<!--.inner-container-->
		</div>
		<!--.foodkonnekt .dashboard-->
	</div>
	<!--#page-container-->


</body>

<script type="text/javascript">
var count=0;
function showSizePrice(id){
	
	if($("#checkBox_"+id).is(":checked")){
		count=count+1;
		$("#sizePrice_"+id).css('display','block');
		$("#dollarTag_"+id).css('display','block');
		$("#sizePrice_"+id).attr("required","true");
		$("#errorMessage1").html("");
		
	}else{
		count=count-1;
		$("#sizePrice_"+id).css('display','none');
		$("#dollarTag_"+id).css('display','none');
		$("#sizePrice_"+id).removeAttr("required");
		
	}
}

var tempCount=0;

$("#crustId").change(function(){
    var selectedCountry = $(this).children("option:selected").val();
    if(undefined === selectedCountry){
    	tempCount=tempCount-1;
    }
    else{ 
    	tempCount=tempCount+1;
    	$("#errorMessage1").html("");
    }
    });
var validNumber = new RegExp(/^\d*\.?\d*$/);
var lastValid = 0;
function validateNumber(elem) {
	  if (validNumber.test(elem.value)) {
	    lastValid = elem.value;
	  } else {
	    elem.value = lastValid;
	  }
	}
	
	 $(function () {
		 $('#crustId').multiselect({
	         maxHeight: 450 ,
        	includeSelectAllOption: true
        }); 
	});  
	 
	 function save()
		{
		 $('#errorMessage1').html("");
			if(count>0 && tempCount>0)
				{
				$("#errorMessage1").html("");
				return true;
				}
			if(count<1){
			$("#errorMessage1").html("please select a size ");
			$("#errorMessage1").focus();
			return false;
		}
			if(tempCount<1){
				$("#errorMessage1").html("please select a template ");
				return false;
			}
		}
</script>
<script>
var crustcheck="";

$(document).ready(function() {
	
	crustcheck = "<%=request.getParameter("duplicateCrustCheck")%>";
	
	if(crustcheck != "null")
	 $('#errorMessage1').html(crustcheck);
	
});
</script>

</html>