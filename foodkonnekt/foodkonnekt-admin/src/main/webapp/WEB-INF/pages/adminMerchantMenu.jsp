<style type="text/css">
    .dropbtn {
        background-color: #f8981d;
        color: #333;
        border: 1px solid #f8981d;
        border-top-left-radius: 10px;
        border-top-right-radius: 10px;
        border-bottom: 0;
        font-weight: 700;
        font-size: 17px;
    }

    .dropdown {
        position: relative;
        display: inline-block;
        background-color: #f8981d;
        color: #333;
        min-width: 250px;
        padding: 16px;
        border: 1px solid #ccc;
        border-top-left-radius: 10px;
        border-top-right-radius: 10px;
        border-bottom: 0;
        font-weight: 700;
        font-size: 17px;
    }

    .dropdown-content {
        display: none;
        position: absolute;
        background-color: #f8981d;
        min-width: 200px;
        box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
        z-index: 1;
        font-weight: 700;
        font-size: 17px;
    }

    .dropdown-content a {
        color: black;
        padding: 12px 16px;
        text-decoration: none;
        display: block;
        font-weight: 700;
        font-size: 17px;
    }

    .dropdown-content a:hover {
        background-color: #f8981d;
    }

    .dropdown:hover .dropdown-content {
        display: block;
    }

    .dropdown:hover .dropbtn {
        background-color: #f8981d;
    }

    .submenu1 {
        display: none;
    }

    .companymenuli:hover > ul {
        display: block;
    }
</style>

<div class="dropdown">
    <button class="dropbtn">Location</button>
    <div class="dropdown-content">
        <a href="onLineOrderLink">Business Information</a>
        <a href="deliveryZones">Delivery Zones</a>
        <c:if test="${merchant.owner.pos.posId!=1}">
            <a href="setupPaymentGateway?adminPanel=1">Gateway</a>
        </c:if>
        <a href="getTaxesByMerchantId">Taxes</a>
        <a href="getMerchantSliders?merchantId=${merchant.id}">Sliders</a>

    </div>
</div>
<div class="dropdown">
    <button class="dropbtn">Customers</button>
    <div class="dropdown-content">
        <a href="customers">Customers</a>
        <%--        <a href="#">Marketing</a>--%>
        <a href="virtualFund">Virtual Fundraisers</a>
       <!--  <a href="uberStore">Uber Store</a> -->
        <%--        <a href="#">Analytics</a>--%>
    </div>
</div>
<div class="dropdown">
    <button class="dropbtn">Communication</button>
    <div class="dropdown-content">
        <a href="orderNotification">Order Notification</a>
<%--        <c:if test="${merchant.owner.pos.posId!=1}">--%>
           <a href="notificationMethod">Notifications</a>
<%--        </c:if>--%>

        <a href="user">User Management</a>
    </div>
</div>
