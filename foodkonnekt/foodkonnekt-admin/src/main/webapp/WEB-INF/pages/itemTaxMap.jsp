<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="true"%>
<!doctype html>
<html class="no-js" lang="en">
<head>

<title>FoodKonnekt | Dashboard</title>
<!--CALLING STYLESHEET STYE.CSS-->
<link href="resources/css/bootstrap.min.css"
        rel="stylesheet" type="text/css" />
        <link href="resources/css/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
<!--CALLING STYLESHEET STYE.CSS-->
<link rel="stylesheet" href="resources/css/style.css">
<!--CALLING STYLESHEET STYLE.CSS-->

<!--CALLING GOOGLE FONT OPEN SANS-->
<link
	href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic'
	rel='stylesheet' type='text/css'>
<!--CALLING GOOGLE FONT OPEN SANS-->

<!--CALLING FONT AWESOME-->
<link rel="stylesheet"
	href="resources/css/font-awesome.css">
<!--CALLING FONT AWESOME-->

<!--OPENS DIALOG BOX-->
<link rel="stylesheet" type="text/css"
	href="resources/css/dialog-box/component.css" />
<!--OPENS DIALOG BOX-->

<!--CALLING PRODUCTS TABS-->
<link rel='stylesheet' type='text/css'
	href='resources/css/products-tabs/opentabby.css' />
<!--CALLING PRODUCTS TABS-->
<!-- <link rel="stylesheet" type="text/css"
	href="https://cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css"> -->
	
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
	<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
	<script src='resources/js/products-tabs/opentabby.js'></script>
	<script src="resources/js/popModal.js"></script>
	<script type="text/javascript" src="resources/js/bootstrap.min.js"></script>
    <script src="resources/js/bootstrap-multiselect.js" type="text/javascript"></script>

<style>
.errorClass {
	border: 1px solid red;
}
</style>
<style>
/* The container */
.radiocontainer {
	display: block;
	position: relative;
	padding-left: 35px;
	margin-bottom: 12px;
	cursor: pointer;
	font-size: 18px;
	-webkit-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	user-select: none;
}

/* Hide the browser's default radio button */
.radiocontainer input {
	position: absolute;
	opacity: 0;
	cursor: pointer;
}

/* Create a custom radio button */
.checkmark {
	position: absolute;
	top: 0;
	left: 0;
	height: 25px;
	width: 25px;
	background-color: #eee;
	border-radius: 50%;
}

/* On mouse-over, add a grey background color */
.radiocontainer:hover input ~ .checkmark {
	background-color: #ccc;
}

/* When the radio button is checked, add a blue background */
.radiocontainer input:checked ~ .checkmark {
	background-color: orange;
}

/* Create the indicator (the dot/circle - hidden when not checked) */
.checkmark:after {
	content: "";
	position: absolute;
	display: none;
}

/* Show the indicator (dot/circle) when checked */
.radiocontainer input:checked ~ .checkmark:after {
	display: block;
}

/* Style the indicator (dot/circle) */
.radiocontainer .checkmark:after {
	top: 9px;
	left: 9px;
	width: 8px;
	height: 8px;
	border-radius: 50%;
	background: white;
}

.pj-preloader {
	display: none;
	position: absolute;
	height: 383px;;
	width: 940px;
	background: url("resources/img/spinner.gif") no-repeat scroll center
		center rgba(153, 153, 153, 0.3);
	z-index: 9999;
	left: 260;
	position: absolute;
	top: 224px;
}
</style>
</head>
<body>
	<div id="page-container">
		<div class="foodkonnekt merchant">
			<div class="inner-container">
				<div class="max-row">

					<header id="page-header">
						<div class="inner-header">
							<div class="row">
								<div class="logo">
									<a href="adminHome" title="FoodKonnekt Dashboard" class="logo"><img
										src="resources/img/foodkonnekt-logo.png"></a>
								</div>
								<!--.logo-->
								<%@ include file="adminHeader.jsp"%>
							</div>
							<!--.row-->
						</div>
						<!--.inner-header-->
					</header>
					<!--#page-header-->

					<div id="page-content">
						<div class="outer-container">
							<div class="row">
								<div class="content-inner-container">
									<%@ include file="leftMenu.jsp"%>
									<div class="right-content-container">
										<div class="right-content-inner-container">

											<div class="content-header">
												<div class="all-header-title"></div>
												<!--.header-title-->
												<div class="content-header-dropdown"></div>
												<!--.content-header-dropdown-->
											</div>
											<!--.content-header-->

											<div class="merchant-page-data">
												<div class="merchant-actions-outbound">
													<div class="merchat-coupons-container">

														<div class="coupons-navigation">
															<ul>
																				 <li><a href="inventory">Items</a></li>
																<li ><a href="category">Categories</a></li>
																<li><a href="modifierGroups">Modifier Groups</a></li>
																<li><a href="modifiers">Modifiers</a></li>
																<li id="itemTaxMap" class="current-menu-item"><a href="itemTaxMap">Tax Map</a></li>
																			</ul>
														</div>
														<!--.coupons-navigation-->

														<div class="delivery-zones-content-container">
															
															<form:form action="saveItemTaxMap" method="POST" modelAttribute="itemTax">
															
															<br><br>
															<div class="adding-products-form">
																	
																	<div>
																		<label>Items: </label>
																		<div style="margin-left: 200px;">
																			<form:select id="itemId" class="multiselect"
																	multiple="multiple" path="itemsId">
																	<c:forEach items="${items}" var="item"
																		varStatus="status">
																		<option value="${item.id}">${item.name}</option>
																	</c:forEach>
																</form:select>
																		</div>
																	</div>
																	<br><br>
																	
																	<div>
																		<label>Tax Rates: </label>
																		<div style="margin-left: 200px;">
																			<form:select id="taxId" class="multiselect"
																	multiple="multiple" path="texesId">
																	
																	
																	<c:forEach items="${taxRates}" var="taxRate"
																		varStatus="status">
																		<c:if test="${taxRate.name != null}">
																		<option value="${taxRate.id}">${taxRate.name}</option>
																		</c:if>
																	</c:forEach>
																	
																</form:select>
																		</div>
																	</div>
																	
																	
																	<br>
																	
															</div><br>
															
															
															
															
															
															<div class="button left">
																<input type="submit" id="updateItemButton" value="Save">
															</div>
															
															</form:form>
													    </div>
															

														</div>
														<!--.coupons-content-container-->
														<div id="errorDiv" style="color: red"></div>
													</div>
													<!--.merchat-coupons-container-->
												</div>
												<!--.merchant-actions-outbound-->
											</div>
											<!--.merchant-page-data-->

										</div>
										<!--.right-content-inner-container-->
									</div>
								</div>

								<div class="methodsdata"></div>


							</div>
							<!--.row-->

						</div>
						<!--.outer-container-->

					</div>
					<!--#page-content-->
					<%@ include file="adminFooter.jsp"%>
				</div>
				<!--.max-row-->
			</div>
			<!--.inner-container-->
		</div>
		<!--.foodkonnekt .dashboard-->
	</div>
	<!--#page-container-->


</body>

<script type="text/javascript">

var posId = "${sessionScope.merchant.owner.pos.posId}";
	$(function () {
		 $('#itemId').multiselect({
	         maxHeight: 450 ,
        	includeSelectAllOption: true
        }); 
		 
		 $('#taxId').multiselect({
		    maxHeight: 450 ,
	      	includeSelectAllOption: true
	     }); 
	}); 
	
	$(document).ready(function() {
		if(posId != '3'){
			
      	    $('#itemTaxMap').css('display','none');
      	  }
    });
	
</script>


</html>
