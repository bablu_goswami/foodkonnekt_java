<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
 <%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page session="true"%>
<!doctype html>
<html class="no-js" lang="en">
<head>
<title>FoodKonnekt | Dashboard</title>
<!--CALLING STYLESHEET STYE.CSS-->
<link rel="stylesheet" href="resources/css/style.css">
<!--CALLING STYLESHEET STYLE.CSS-->

<!--CALLING GOOGLE FONT OPEN SANS-->
<link
    href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic'
    rel='stylesheet' type='text/css'>
<!--CALLING GOOGLE FONT OPEN SANS-->

<!--CALLING FONT AWESOME-->
<link rel="stylesheet" href="resources/css/font-awesome.css">
<!--CALLING FONT AWESOME-->

<!--CALENDAR MULTI-SELECT-->
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css">
<link href="resources/css/calendar/jquery.comiseo.daterangepicker.css" rel="stylesheet" type="text/css">
<!--CALENDAR MULTI-SELECT-->

<!--CHECK ALL LIST TABLE-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="resources/js/checkall/jquery.checkall.js"></script>
<!--CHECK ALL LIST TABLE-->

<!--OPENS DIALOG BOX-->
<link rel="stylesheet" type="text/css" href="resources/css/dialog-box/component.css" />
<!--OPENS DIALOG BOX-->

<!--ACCORDION FOR MENU-->
<script src="resources/js/accordion/paccordion.js"></script>
<!--ACCORDION FOR MENU-->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css">
<style type="text/css">
div#example_paginate {
    display: block;
}

div#example_filter {
    display: block;
}

div#example_length {
    display: block;
}

input[type="search"] {
    max-width: 300px;
    width: 100%;
    outline: 0;
    border: 1px solid rgb(169, 169, 169);
    padding: 11px 10px;
    border-radius: 6px;
    margin-bottom: 7px;
    display: none;
}
.right-content-container{
    width: 1100px;
    float: left;
    margin-left: 5%;
}
</style>
<style>
#questions {
    font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

#questions td, #questions th {
    border: 1px solid #ddd;
    padding: 8px;
}

#questions tr:nth-child(even){background-color: #f2f2f2;}

#questions tr:hover {background-color: white;}

#questions th {
    padding-top: 12px;
    padding-bottom: 12px;
    text-align: left;
    background-color: orange;
    color: white;
}

#CustomerDetails {
    font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

#CustomerDetails td, #CustomerDetails th ,#custComments {
    border: 1px solid #ddd;
    padding: 8px;
}

#CustomerDetails tr:nth-child(even){background-color: #f2f2f2;}

#CustomerDetails tr:hover {background-color: white;}

#CustomerDetails th {
    padding-top: 12px;
    padding-bottom: 12px;
    text-align: left;
    background-color: orange;
    color: white;
}



</style>
<link type="text/css" rel="stylesheet" href="resources/css/popModal.css">
<script src="resources/js/popModal.js"></script>
<div class="exampleLive">
    <button style="display:none;" id="confirmModal_ex22" class="btn btn-primary" data-confirmmodal-bind="#confirm_content" data-topoffset="0" data-top="10%" >Example</button>
</div>
<div id="confirm_content" style="display:none">
    <div class="confirmModal_content sd-popup-content" id="popupdata">
    <img src="resources/img/logo.png" class="sd-popup-logo">
    <!-- <div>
     <lable>Name: Sumit</lable><br><br>
    <lable>Phone No: 1212122</lable><br><br>
    <lable>Email Id :sumit@mkonnekt.com</lable><br><br>
    </div> -->
    <div>
    <table id="CustomerDetails"style="margin-top: 5%;" >
    <!-- <tr><th>Customer Detail</th></tr> -->
    <tr><td><span>Name:</span> <span id="custName" style="margin-left: 8%;" width="100%;"></span> </td></tr>
    <tr><td>Phone No: <span id="custPhoneNo" style="margin-left: 2%;"></span></td></tr>
    <tr><td>Email Id :<span id="custEmailId" style="margin-left: 4%;"></span></td></tr>
    </table>
    <table id="questions" >
    <tr><th>Feedback Questions</th><th>Rate</th></tr>
    </table>
    <div id="Comments" style="margin-top: 4%;">
     
     
     </div>
   </div>
    </div>
   
    <div class="confirmModal_footer button" style="margin-top: -6%;">
            
            <button type="button" class="btn btn-default guestPopUpCls"
                data-confirmmodal-but="cancel">
                <font style="color: black;"><b>OK</b></font>
            </button>
        </div>
</div>
</head>
<body>
    <div id="page-container">
        <div class="foodkonnekt orders">
            <div class="inner-container">
                <div class="max-row">

                    <header id="page-header">
                        <div class="inner-header">
                            <div class="row">
                                <div class="logo">
                                    <a href="adminHome" title="FoodKonnekt Dashboard" class="logo"><img
                                        src="resources/img/foodkonnekt-logo.png"></a>
                                </div>
                                <!--.logo-->
                                <%-- <%@ include file="adminHeader.jsp"%> --%>
                                <div class="header-nav">
    <nav class="header-nav-container">
        <ul>
             <li><select onchange="location = this.options[this.selectedIndex].value;">
    <option>Change Dashboard</option>
    <option value="getAllMerchants">FoodKonnekt</option>
    <option value="kritiqMerchantDetail">Kritiq</option>
    </select></li>|
            <li></i> <b "margin-right: 25%;">${merchantName}</b></li>|
           
            <li><a href="adminLogout"><i class="fa fa-power-off" aria-hidden="true"></i> Log Out</a></li>
        </ul>
    </nav>
    <!--.header-nav-container-->
</div>
                            </div>
                            <!--.row-->
                        </div>
                        <!--.inner-header-->
                    </header>
                    <!--#page-header-->

                    <div id="page-content">
                        <div class="outer-container">

                            <div class="row">
                                <div class="content-inner-container">
                                    <%-- <%@ include file="leftMenu.jsp"%> --%>
                                    <div class="right-content-container">
                                        <div class="right-content-inner-container">
                                            <div class="content-header">
                                                <div class="all-header-title"></div>
                                                <!--.header-title-->
                                                <div class="content-header-dropdown"></div>
                                                <!--.content-header-dropdown-->
                                            </div>
                                            <!--.content-header-->
                                            <div class="orders-page-data">

                                                <div class="sd-orders-list-outbound">
                                                    <div class="sd-orders-list">
                                                        <div class="sd-orders-list-inbound">
                                                            <div class="orders-items-list-table">
                                                                <div class="search-container" style="margin-top: 10px">
                                                                    <div class="only-search-elements">
                                                                        <label>Search</label> <input type="text"
                                                                            placeholder="Search Merchants"
                                                                            id="search-inventory" class="searchq">
                                                                        <input type="button" value="Search">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            
                                                              <br>
                                                           <%-- <div> <form:form action="getAllMerchants" align="center" method="GET" modelAttribute="SearchVO" id="businessLogo" enctype="multipart/form-data">
                                                            FROM &nbsp&nbsp&nbsp
                                                            <input type="date" name="startDate" path="startDate" value="${SearchVO.startDate}"> 
                                                             &nbsp&nbsp&nbsp TO &nbsp&nbsp&nbsp
                                                            <input type="date" name="endDate" path="endDate" value="${SearchVO.endDate}">&nbsp&nbsp
                                                            <!-- <input type="submit" value="Submit" /> -->
                                                            <div class="button left" style="margin-left: 78%;margin-top: -5%;">
                                                            
                                                            <input type="submit" value="Submit"  style="width: 86%;">
                                                            </div>
                                                            
                                                            </form:form></div> --%>
                                                            
                                                            
                                                            <div style="margin-left: -30%;"> <%-- <form:form action="getAllMerchants" align="center" method="GET" modelAttribute="SearchVO" id="businessLogo" enctype="multipart/form-data">
                                                             
                                                            
                                                             FROM &nbsp&nbsp&nbsp
                                                            <input type="date" name="startDate" path="startDate" value="${SearchVO.startDate}" style="width: 22%;"> 
                                                             &nbsp&nbsp&nbsp TO &nbsp&nbsp&nbsp
                                                            <input type="date" path="endDate" name="endDate" value="${SearchVO.endDate}" style="width: 22%;">&nbsp&nbsp
                                                            
                                                            <div class="button left" style="margin-left: 78%;margin-top: -4.2%;">
                                                            
                                                            <input type=submit value="Submit"  style="width: 86%;">
                                                           
                                                           	
                                                            </div>
                                                            
                                                            </form:form> --%>
                                                            </div>
                                                            
                                                            
                                                            
                                                            
                                                            <!--.inventory-items-list-table-->
                                                            <table id="checkit-table" class="table header-table-orders">
                                                                <thead>
                                                                    <tr>
                                                                        <th align="left">Date of Review</th>
                                                                        <th align="left">Customer Name</th>
                                                                        <th align="center">Avg Rating</th>
                                                                        <th style="display:none;">Avg Rating</th>
                                                                        <th style="display:none;">Avg Rating</th>
                                                                        <th style="display:none;">Avg Rating</th>
                                                                        <th style="display:none;">Avg Rating</th>
                                                                        <th style="display:none;">Avg Rating</th>
                                                                        <th align="left">View Feedback</th>
                                                                        
                                                                        
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <c:forEach items="${kritiqCustomerVOs}" var="view"
                                                                        varStatus="status">
                                                                        
                                                                        <tr>
                                                                            <td align="left"><p><a href="#" >${view.dateOfReview}</a></p></td>
                                                                            <td align="left"><p><a href="#" >${view.customerName}</a></p></td>
                                                                            <td align="center"><p><a href="#" >${view.avgRating}</a></p></td>
                                                                            <td style="display:none;" id='custPhoneNo'>${view.phoneNo}</td>
                                                                            <td style="display:none;" id='custName'>${view.customerName}</td>
                                                                            <td style="display:none;" id='custEmailid'>${view.customerEmailId}</td>
                                                                             <td style="display:none;" id='custEmailid'>${view.customerComments}</td>
                                                                            <td style="display:none;" id='questions'><c:forEach items="${view.kritiqQuestionAnswersVOs}" var="ques"
                                                                        varStatus="status">${ques.question}_${ques.answer}@#</c:forEach></td>
                                                                           
                                                                            
                                                                                
                                                                             <td align="left"><a href="#" >View Feedback</a></td>
                                                                            
                                                                           
                                                                          
                                                                        </tr>
                                                                       
                                                                    </c:forEach>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <!--.sd-orders-list-inbound-->
                                                    </div>
                                                    <!--.sd-orders-list-->
                                                </div>
                                                <!--.sd-orders-list-outbound-->

                                            </div>
                                            <!--.orders-page-data-->


                                        </div>
                                        <!--.right-content-inner-container-->
                                    </div>
                                    <!--.right-content-container-->
                                </div>
                                <!--.content-inner-container-->
                            </div>
                            <!--.row-->

                        </div>
                        <!--.outer-container-->
                    </div>
                    <!--#page-content-->

                    <%@ include file="adminFooter.jsp"%>
                    <!--#footer-container-->

                </div>
                <!--.max-row-->
            </div>
            <!--.inner-container-->
        </div>
        <!--.foodkonnekt .dashboard-->
    </div>
    <!--#page-container-->


    <script>
                    $(document).ready(function() {
                        $('#checkit-table').DataTable({
                            "order" : [ [ 1, "desc" ] ]
                        });
                        
                        $('#checkit-table').on( 'click', 'tbody tr', function (e) {
                            var index = $(this).index();
                            var phoneNo = document.getElementById("checkit-table").rows[index+1].cells[3].innerHTML;
                            var name = document.getElementById("checkit-table").rows[index+1].cells[4].innerHTML;
                            var emailid = document.getElementById("checkit-table").rows[index+1].cells[5].innerHTML;
                            var custComments = document.getElementById("checkit-table").rows[index+1].cells[6].innerHTML;
                            var quest = document.getElementById("checkit-table").rows[index+1].cells[7].innerHTML;
                            $("#custName").html("");
                            $("#custPhoneNo").html("");
                            $("#custEmailId").html("");
                            $("#Comments").html("");
                            
                            $("#custName").append(name);
                            
                            $("#custPhoneNo").append(phoneNo);
                            $("#custEmailId").append(emailid);
                            if(custComments!=''){
                            	$("#Comments").append("Customer's Comment");
                            $("#Comments").append('<textarea readonly="readonly"   id="custComments" rows="4" cols="10"  style="max-width: 500px; resize: none;"></textarea>');
                            $("#custComments").append(custComments);
                            }
                            var res = quest.split("@#");
                            $("#questions").html("");
                            
                            $("#questions").append("<tr><th>Feedback Questions</th><th>Rate</th></tr>");
                            for(var i=0;i<res.length-1;i++){
                            	var que_ans=res[i];
                            	 var ques_ans = que_ans.split("_");
                            	 var ques=ques_ans[0];
                            	 var ans=ques_ans[1];
                            	 $("#questions").append("<tr><td>"+ques+"</td><td>"+ans+"</td></tr>");
                            }
                       
                    
                                jQuery('#confirmModal_ex22').click();
                              
                        } );
                    });
                    function sessionTimeOut(){
                    	alert("testing");
                    	$("#popupdata").hide();
                   }
                    
                 
                </script>
    <script type="text/javascript">
                    var _gaq = _gaq || [];
                    _gaq.push([ '_setAccount', 'UA-36251023-1' ]);
                    _gaq.push([ '_setDomainName', 'jqueryscript.net' ]);
                    _gaq.push([ '_trackPageview' ]);

                    (function() {
                        var ga = document.createElement('script');
                        ga.type = 'text/javascript';
                        ga.async = true;
                        ga.src = ('https:' == document.location.protocol ? 'https://ssl'
                                : 'http://www')
                                + '.google-analytics.com/ga.js';
                        var s = document.getElementsByTagName('script')[0];
                        s.parentNode.insertBefore(ga, s);
                    })();
                </script>
    <!--CALENDAR MULTI-SELECT-->

    <!--OPENS DIALOG BOX-->
    <script src="resources/js/dialog-box/classie.js"></script>
    <script src="resources/js/dialog-box/modalEffects.js"></script>
    <script>
        var polyfilter_scriptpath = '/js/';
    </script>
    <!--OPENS DIALOG BOX-->

    <script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript">
     $(document).ready(function() {
        var table = $('#checkit-table').DataTable();
       
        	//alert(mrcStatus);
        
        $(".searchq").keyup(function() {
        	//alert(this.value);
             table.search( this.value).draw();
         } );
       
     });
     $(document).ready(function(){
       $('input[type=search]').each(function(){
         $(this).attr('placeholder', "Search");
       });
     });
     $("#generateButton").click(function () {
       $("#searchForm").submit();
     });
 </script>
    
    
    <script>
$(function(){
    $('label').each(function(){
        if($(this).text()=='Search:'){
            $(this).text('');
            }
    })
})
</script>
</body>
</html>
