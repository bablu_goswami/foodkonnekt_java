<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="true"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
 <head> 
    <title>FoodKonnekt | Dashboard</title>
    <!--CALLING STYLESHEET STYE.CSS-->
    <link rel="stylesheet" href="resources/css/style.css">
    <!--CALLING STYLESHEET STYLE.CSS-->
    
    <!--CALLING GOOGLE FONT OPEN SANS-->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    <!--CALLING GOOGLE FONT OPEN SANS-->
    
    <!--CALLING FONT AWESOME-->
    <link rel="stylesheet" href="resources/css/font-awesome.css">
    <!--CALLING FONT AWESOME-->
    
    <!--CALLING FILTER OPTIONS-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>

  </head>
  <style>
  .logo{
    width: 1178px;
   }
   
   footer#footer-container {
    position: absolute;
    bottom: 0;
    width: 100%;
}
   .clearfix:after {
   content: " ";
   visibility: hidden;
   display: block;
   height: 0;
   clear: both;
   
   html {
  font-family: "roboto", helvetica;
  position: relative;
  height: 100%;
  font-size: 100%;
  line-height: 1.5;
  color: #444;
}

h2 {
  margin: 1.75em 0 0;
  font-size: 2vw;
  margin-bottom:15px;
}

h3 { font-size: 1.3em; }

.v-center {
  height: 100vh;
  width: 100%;
  display: table;
  position: relative;
  text-align: center;
}

.v-center > div {
  display: table-cell;
  vertical-align: middle;
  position: relative;
  top: -10%;
}

.btn {
  font-size: 3vmin;
  padding: 0.75em 1.5em;
  background-color: #fff;
  border: 1px solid #bbb;
  color: #333;
  text-decoration: none;
  display: inline;
  border-radius: 4px;
  -webkit-transition: background-color 1s ease;
  -moz-transition: background-color 1s ease;
  transition: background-color 1s ease;
}

.btn:hover {
  background-color: #ddd;
  -webkit-transition: background-color 1s ease;
  -moz-transition: background-color 1s ease;
  transition: background-color 1s ease;
}

.btn-small {
  padding: .75em 1em;
  font-size: 0.8em;
}

.modal-box {
  display: none;
  position: absolute;
  z-index: 1000;
  width: 98%;
  background: white;
  border-bottom: 1px solid #aaa;
  border-radius: 4px;
  box-shadow: 0 3px 9px rgba(0, 0, 0, 0.5);
  border: 1px solid rgba(0, 0, 0, 0.1);
  background-clip: padding-box;
}
@media (min-width: 32em) {

.modal-box { width: 70%; }
}

.modal-box header,
.modal-box .modal-header {
  padding: 1.25em 1.5em;
  border-bottom: 1px solid #ddd;
}

.modal-box header h3,
.modal-box header h4,
.modal-box .modal-header h3,
.modal-box .modal-header h4 { margin: 0; }

.modal-box .modal-body { padding: 2em 1.5em; }

.modal-box footer,
.modal-box .modal-footer {
  padding: 1em;
  border-top: 1px solid #ddd;
  background: rgba(0, 0, 0, 0.02);
  text-align: right;
}

.modal-overlay {
  opacity: 0;
  filter: alpha(opacity=0);
  position: absolute;
  top: 0;
  left: 0;
  z-index: 900;
  width: 100%;
  height: 100%;
  background: rgba(0, 0, 0, 0.3) !important;
}

a.close {
  line-height: 1;
  font-size: 1.5em;
  position: absolute;
  top: 5%;
  right: 2%;
  text-decoration: none;
  color: #bbb;
}

a.close:hover {
  color: #222;
  -webkit-transition: color 1s ease;
  -moz-transition: color 1s ease;
  transition: color 1s ease;
}
  </style>
  <script>
// Modal script
(function($) {
	$(function(){
	var appendthis =  ("<div class='modal-overlay js-modal-close'></div>");
	$("#popup1").css("display","block");
	$("body").append(appendthis);
	$(".modal-overlay").fadeTo(500, 0.9);
	var modalBox = $(this).attr('data-modal-id');
	$('#'+modalBox).fadeIn($(this).data());
	$('#'+modalBox).addClass('opened');

	$(".js-modal-close, .modal-overlay").click(function() {
		$(".modal-box, .modal-overlay").fadeOut(500, function() {
			$(".modal-overlay").remove();
		});
		// Remove the specific class
		$('.modal-box').removeClass('opened');
		// Stop the video from playing
		$('.modal-box').find('video').each(function() {
				$(this).get(0).pause();
		});
	});

$(window).resize(function() {
	$(".modal-box").css({
		top: ($(window).height() - $(".modal-box").outerHeight()) / 2,
		left: ($(window).width() - $(".modal-box").outerWidth()) / 2
	});
});
$(window).resize();

});
})(jQuery);
  </script>
  
<script type="text/javascript">
 var player;
 function onYouTubePlayerAPIReady() {
  player = new YT.Player('ytplayer', {
    events: {
        'onStateChange': ShowMe
       }
    });
 }

 var tag = document.createElement('script');
 tag.src = "https://www.youtube.com/player_api";
 var firstScriptTag = document.getElementsByTagName('script')[0];
 firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

    function ShowMe() {

    var sStatus;
    sStatus = player.getPlayerState();

  if (sStatus == -1) alert("Video has not started.");
      else if (sStatus == 0) {  $('#ytplayer').replaceWith('<a href="page/" target="_blank"><img class="special_hover" src="image" alt="" /></a>')
  }
     else if (sStatus == 1) { } //Video is playing
     else if (sStatus == 2) {}  //Video is paused
     else if (sStatus == 3) { } //video is buffering 
     else if (sStatus == 5) { } //Video is cued.
   }

  </script>
  <body>
<!-- <div class="video-holderYT">
  <iframe  id="ytplayer" width="914" height="350"  class="videoImageYT" src="https://www.youtube.com/embed/PKEX85J6wT0?rel=0&amp;autoplay=1&amp;modestbranding=1&amp;showinfo=0&amp;autohide=1&version=3&enablejsapi=1&playerapiid=ytplayer" frameborder="0" allowfullscreen=""></iframe>
</div> -->
<div id="popup1" class="modal-box">
<header> <a href="#" class="js-modal-close close"></a></header>
  <div class="modal-body">
    <iframe id="ytplayer" type="text/html" width="914" height="350" src="https://www.youtube.com/embed/PKEX85J6wT0?wmode=transparent&amp;autoplay=1&amp;autohide=1" frameborder="0"></iframe>
<!--  <div class="video-holderYT">
  <iframe  id="ytplayer" width="914" height="350"  class="videoImageYT" src="https://www.youtube.com/embed/PKEX85J6wT0?rel=0&amp;autoplay=1&amp;modestbranding=1&amp;showinfo=0&amp;autohide=1&version=3&enablejsapi=1&playerapiid=ytplayer" frameborder="0" allowfullscreen=""></iframe>
</div> -->
 
  </div>
  <footer> <a href="#" class="btn btn-small js-modal-close">Close</a> </footer>
</div>
<script src="https://code.jquery.com/jquery-1.11.1.min.js"></script> 
    <div id="page-container">
		<div class="foodkonnekt merchant">
			<div class="inner-container">
				<div class="max-row">
					<header id="page-header">
						<div class="inner-header">
							<div class="row">
								<div class="logo">
									<a href="index.html" title="FoodKonnekt Dashboard" class="logo"><!-- <img src="resources/img/logo.jpg"> </a>-->
        							<img src="data:image/jpeg;base64,${merchant.merchantLogo}" onerror="this.src='resources/img/foodkonnekt-logo.png'" width="250" height="150"></a>
								</div>
							</div><!--.row-->
						</div><!--.inner-header-->
					</header><!--#page-header-->
					
					<div id="page-content clearfix">
						<div class="outer-container">
						<div style="margin:0 auto;width: 127px;padding-top: 52px;" class="clearfix">
						<span style="margin:10px 0 0 0;"><b><font size="4">ALL DONE !</font></b></span></div>
						
							<div class="row">
								<div class="content-inner-container">
								
								<div class="adding-products-form" style="width:800px; margin:0 auto;">
								
									<div class="clearfix"></div>
									<c:choose>
									<c:when test="${inventoryThreadStatus ==1}">
									<p></p>
									You have successfully installed Foodkonnekt Online Ordering System.</br>
									<p></p>
									</c:when>
									<c:otherwise>
										<p></p>
									We are still in the process of getting inventory data from Clover. We will email you once this process is done.</br>
									<p></p>
									</c:otherwise>
								</c:choose>
									You can get your online ordering link from your admin panel.Please add this link for online buttons</br>
									on your website ,Facebook,Twitter,Yelp,campaigns and any online platform accessed by your</br>
									Customers</br>
									<p></p>
									You are now ready to start accepting orders from anywhere online</br>
									<p></p>
									You can always make further changes at any point from admin portal.
									<div class="clearfix"></div>
									<br>
									<div class="clearfix"></div>
									<div class="button right">
										<a href="adminHome" style="max-width: 200px;width: 200px;" >Go to Admin Panel</a>
									</div><!--.button-->
									
									
							   </div><!--.adding-products-form-->
							</div><!--.adding-products-->	
									
						</div><!--.content-inner-container-->
					</div><!--.row-->
			</div><!--.outer-container-->
		</div><!--#page-content-->
					
					<footer id="footer-container">
						<div class="footer-outer-container">
							<div class="footer-inner-container">
								<div class="row">
									<div class="sd-inner-footer">
									
										<div class="footer-left">
											<p>Powered by foodkonnekt | copyright@foodkonnekt.com</p>
										</div><!--.footer-left-->
										
										<div class="footer-right">
											<img src="resources/img/foodkonnekt-logo.png" />										</div><!--.footer-right-->
									</div><!--.sd-inner-footer-->
								</div><!--.row-->
							</div><!--.footer-inner-container-->
						</div><!--.footer-outer-container-->
					</footer>
					<!--#footer-container-->
					
				</div><!--.max-row-->
			</div><!--.inner-container-->
		</div><!--.foodkonnekt .dashboard-->
	</div><!--#page-container-->

  </body>
</html>
