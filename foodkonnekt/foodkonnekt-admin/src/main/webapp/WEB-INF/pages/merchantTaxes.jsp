<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="true"%>
<!doctype html>
<html class="no-js" lang="en">
  <head>
  
    <title>FoodKonnekt | Dashboard</title>
    <!--CALLING STYLESHEET STYE.CSS-->
    <link rel="stylesheet" href="resources/css/style.css">
    <!--CALLING STYLESHEET STYLE.CSS-->
    
    <!--CALLING GOOGLE FONT OPEN SANS-->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    <!--CALLING GOOGLE FONT OPEN SANS-->
    
    <!--CALLING FONT AWESOME-->
    <link rel="stylesheet" href="resources/css/font-awesome.css">
    <!--CALLING FONT AWESOME-->
    
    <!--CALLING FILTER OPTIONS-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/jquery.dataTables.min.js"></script>
    <script src="https://www.jqueryscript.net/demo/Powerful-jQuery-Data-Table-Column-Filter-Plugin-yadcf/jquery.dataTables.yadcf.js"></script>
    <!--OPENS DIALOG BOX-->
    <link rel="stylesheet" type="text/css" href="resources/css/dialog-box/component.css" />
    <!--OPENS DIALOG BOX-->
    <style>
    .errorClass { border:  1px solid red; }
    </style>
    <style>
/* The container */
.radiocontainer {
    display: block;
    position: relative;
    padding-left: 35px;
    margin-bottom: 12px;
    cursor: pointer;
    font-size: 18px;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
}

/* Hide the browser's default radio button */
.radiocontainer input {
    position: absolute;
    opacity: 0;
    cursor: pointer;
}

/* Create a custom radio button */
.checkmark {
    position: absolute;
    top: 0;
    left: 0;
    height: 25px;
    width: 25px;
    background-color: #eee;
    border-radius: 50%;
}

/* On mouse-over, add a grey background color */
.radiocontainer:hover input ~ .checkmark {
    background-color: #ccc;
}

/* When the radio button is checked, add a blue background */
.radiocontainer input:checked ~ .checkmark {
    background-color: orange;
}

/* Create the indicator (the dot/circle - hidden when not checked) */
.checkmark:after {
    content: "";
    position: absolute;
    display: none;
}

/* Show the indicator (dot/circle) when checked */
.radiocontainer input:checked ~ .checkmark:after {
    display: block;
}

/* Style the indicator (dot/circle) */
.radiocontainer .checkmark:after {
 	top: 9px;
	left: 9px;
	width: 8px;
	height: 8px;
	border-radius: 50%;
	background: white;
}

.pj-preloader {
    display: none;
    position: absolute;
    height: 383px;;
    width: 940px;
    background: url("resources/img/spinner.gif") no-repeat scroll center center rgba(153, 153, 153, 0.3);
    z-index: 9999;
    left: 260;
    position: absolute;
    top: 224px;
}
</style>
  </head>
  <body>
    <div id="page-container">
        <div class="foodkonnekt merchant">
            <div class="inner-container">
                <div class="max-row">
                    
                    <header id="page-header">
                        <div class="inner-header">
                            <div class="row">
                                <div class="logo">
                                     <a href="adminHome" title="FoodKonnekt Dashboard" class="logo"><img src="resources/img/foodkonnekt-logo.png"></a>
                                </div><!--.logo-->
                                   <%@ include file="adminHeader.jsp"%>
                            </div><!--.row-->
                        </div><!--.inner-header-->
                    </header><!--#page-header-->
                    
                    <div id="page-content">
                        <div class="outer-container">
                            <div class="row">
                            <div class="content-inner-container">
                            <%@ include file="leftMenu.jsp"%>    
                            <div class="right-content-container">
                            <div class="right-content-inner-container">
                                        
                                            <div class="content-header">
                                                <div class="all-header-title">
                                                </div><!--.header-title-->
                                                <div class="content-header-dropdown">
                                                </div><!--.content-header-dropdown-->
                                            </div><!--.content-header-->
                                            
                                            <div class="merchant-page-data">
                                                <div class="merchant-actions-outbound">
                                                    <div class="merchat-coupons-container">

                                                        <%@ include file="adminMerchantMenu.jsp"%>

                                                        <div class="delivery-zones-content-container">
                                                            
                                                            
                                                        </div><!--.coupons-content-container-->
                                                       <div id="errorDiv" style="color:red"></div> 
                                                    </div><!--.merchat-coupons-container-->
                                                </div><!--.merchant-actions-outbound-->
                                            </div><!--.merchant-page-data-->
                                            
                                        </div><!--.right-content-inner-container-->
                            </div>
                            </div>
                              
                             <div class="methodsdata">
                            <div class="pj-preloader" style="margin-left: 260px;"></div>
                                        <table width="78.5%" id="example" class="dataTable no-footer" role="grid" aria-describedby="example_info" style="width: 78.5%;
                                        margin-top: -147px;" align="right">
                                                                <thead>
                                                                    <tr>
                                                                        <th align='left'>Name</th>
                                                                        <th align='left'>Rate</th>
                                                                        <th align='left'>isDefault</th>
                                                                        <c:if test="${merchant.owner.pos.posId==3}">
                                                                        <th id="editHead"align='left'>Edit</th>
                                                                        <th id="deleteHead" align='left'>Delete</th>
                                                                        </c:if>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <c:forEach items="${taxRates}" var="view"
                                                                        varStatus="status">
                                                                        <tr>
                                                                            <td align="left"><p>${view.name}</p></td>
                                                                            <td align="left"><p>${view.rate}</p></td>
                                                                            
                                                                            <c:if test="${view.isDefault==1}">
                                                                            <td align="left"><p>true</p></td>
                                                                            </c:if>
                                                                            
                                                                            <c:if test="${view.isDefault==0}">
                                                                            <td align="left"><p>false</p></td>
                                                                            </c:if>
                                                                            
                                                                            <c:if test="${merchant.owner.pos.posId==3}">
                                                                            <td align="left"><a href="editTaxRates?taxRateId=${view.id}">edit</a></td>
                                                                            <td align="left"><a href="#" onclick="deleteTaxes(${view.id})">delete</a></td>
                                                                            </c:if>
                                                                        </tr>
                                                                    </c:forEach>
                                                                </tbody>
                                                            </table>
                                                            </div>
                                                            
                                                            
                            </div><!--.row-->
                            
                        </div><!--.outer-container-->
                        
                    </div><!--#page-content-->
                             <%@ include file="adminFooter.jsp"%>                    
                </div><!--.max-row-->
            </div><!--.inner-container-->
        </div><!--.foodkonnekt .dashboard-->
    </div><!--#page-container-->
    
    <script>
    </script>
    <!--OPENS DIALOG BOX-->
      <script type="text/javascript">
     
      function deleteTaxes(id){
    	  $('.pj-preloader').css('display','block');
    	  $.ajax({
              type: 'GET',
              url: "deleteTaxRates?taxRateId="+id,
              success:function(data){
            	  $('.pj-preloader').css('display','none');
               	  location.reload();
              }
           });   
      }
      
    </script>
  </body>
</html>
