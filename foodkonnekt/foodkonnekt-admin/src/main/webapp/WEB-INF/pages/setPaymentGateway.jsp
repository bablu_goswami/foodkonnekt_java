<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="true"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
 <head> 
    <title>FoodKonnekt | Dashboard</title>
    <!--CALLING STYLESHEET STYE.CSS-->
    <link rel="stylesheet" href="resources/css/style.css">
    <!--CALLING STYLESHEET STYLE.CSS-->
    
    <!--CALLING GOOGLE FONT OPEN SANS-->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    <!--CALLING GOOGLE FONT OPEN SANS-->
    
    <!--CALLING FONT AWESOME-->
    <link rel="stylesheet" href="resources/css/font-awesome.css">
    <!--CALLING FONT AWESOME-->
    
    <!--CALLING FILTER OPTIONS-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <link type="text/css" rel="stylesheet" href="resources/css/popModal.css">
    <script src="resources/js/popModal.js"></script>
  </head>
  <style>
  .logo{
    width: 1178px;
   }
   .clearfix:after {
   content: " ";
   visibility: hidden;
   display: block;
   height: 0;
   clear: both;
   }
   .clearfix1{
   width: 30%! important;
   }
   
   .min
   {
       width: 261px;
   }
  </style>
  <link rel="stylesheet" href="resources/css/popupCSS.css">
  <script>
  /* 
  var paymentGatewayRadioValue = $("input[name='activePaymentGateway']:checked").val();
   */
   /* $(document).ready(function() {
	   //alert($("#isDeleted").val());
	  // alert($("#isActive").val());
	   
	   
	  if($("#isDeleted").val()=="" && $("#isActive").val()=="")
	   {
		 $("#activePaymentGatewayDiv").css("display","none");
	   }else{
	   if($("#gateway").val()==1 && $("#isDeleted").val()=="false" && $("#isActive").val()=="true"){
		   document.getElementById("stripe").checked = true;
		   //$("#authorizeButton").attr("disabled", true);
			$("#authorizeTR").hide();
	   }
	   if($("#gateway").val()==2 && $("#isDeleted").val()=="false" && $("#isActive").val()=="true"){
		   document.getElementById("authorize").checked = true;
		   //$("#stripeButton").attr("disabled", true);
		   $("#stripeTR").hide();
	   }
	   }
	 
	   }); */
   $(document).ready(function() {
	   console.log(window.location.search);
	   var urlParams = new URLSearchParams(window.location.search);
	   console.log("value--"+urlParams.get('adminPanel')); // true
	   var flag = urlParams.get('adminPanel');
	   if(flag==1){
		   $("#cancelButton").css("display","block");
	   }else{
		   $("#skipButton").css("display","block");
	   }
      var saveStatus="${saveStatus}";
      if(saveStatus!=""){ 
          jQuery(function(){
               jQuery('#confirmModal_ex2').click();
            });
        } 
      });
   $(document).ready(function(){
   $('#gatewayTypeId').on('change', function () {
	   
	    if(this.value === "1"){
	    	$("#stripeDiv").css("display","block");
	    	$("#authorizeDiv").css("display","none");
	    	$("#cayanDiv").css("display","none");
	    	$("#iGateDiv").css("display","none");
	        //$("#stripeDiv").show();
	       // $("#authorizeDiv").hide();
	    }
	    if(this.value === "2"){
	    	$("#authorizeDiv").css("display","block");
	    	$("#stripeDiv").css("display","none");
	    	$("#cayanDiv").css("display","none");
	    	$("#iGateDiv").css("display","none");
	       // $("#authorizeDiv").show();
	       // $("#stripeDiv").hide();
	    }
	    if(this.value === "3"){
	    	
	    	$("#cayanDiv").css("display","block");
	    	$("#authorizeDiv").css("display","none");
	    	$("#stripeDiv").css("display","none");
	    	$("#iGateDiv").css("display","none");
	       // $("#authorizeDiv").show();
	       // $("#stripeDiv").hide();
	    }
		if(this.value === "4"){
			$("#iGateDiv").css("display","block");
	    	$("#cayanDiv").css("display","none");
	    	$("#authorizeDiv").css("display","none");
	    	$("#stripeDiv").css("display","none");
	       // $("#authorizeDiv").show();
	       // $("#stripeDiv").hide();
	    }
	});
   });
  
   /*  function removeStripe(){
	  var gateWayType= $("#gateway").val();
   	
   	$.ajax({
           url : "removePaymentGateway?gateWayType="+ gateWayType,
           type : "GET",
           contentType : "application/json; charset=utf-8",
           success : function(result) {
           	           	alert(result);
           },
           error : function() {
             console.log("Error inside future date Ajax call");
           }
        })
	   
   }
 function removeauthorize() {
	var gateWayType= $("#gateway").val();
	  alert("authorize-"+gateWayType);
 	
 	$.ajax({
         url : "removePaymentGateway?gateWayType="+ gateWayType,
         type : "GET",
         contentType : "application/json; charset=utf-8",
         success : function(result) {
         	           	alert(result);
         	          // window.location.href = "setupPaymentGateway";
         	          //window.location.reload(true);
         	          // location.reload();
         },
         error : function() {
           console.log("Error inside future date Ajax call");
         }
      })
   }
 //to open div for new payment gateway
 function addNewGateway(){
	alert("addNewGateway");
	 $("#addPaymentGatewayDiv").css("display","block");
	
}  */
   
  function setpaymentDetails(){
	    window.location.href = "adminPanel";
	  }
  </script>
  <body>
 <div class="exampleLive">
    <button id="confirmModal_ex2" style="display: none" class="btn btn-primary" data-confirmmodal-bind="#confirm_content" data-topoffset="0" data-top="10%">Example</button>
</div>
            
<div id="confirm_content" style="display:none">
    <div class="confirmModal_content sd-popup-content">
    <img src="resources/img/logo.png" class="sd-popup-logo">
    <h3>Your payment details has been successfully saved</h3>
    
    </div>
    <div class="confirmModal_footer">
        <button type="button" class="btn btn-primary uploadedBtn" data-confirmmodal-but="ok" onclick="setpaymentDetails()" >continue</button>
        <!-- <button type="button" class="btn btn-primary uploadedBtn" data-confirmmodal-but="no" onclick="setPickUpTime()" >No</button> -->
    </div>
</div>
    <div id="page-container">
        <div class="foodkonnekt merchant">
            <div class="inner-container">
                <div class="max-row">
                    
                    <header id="page-header">
                        <div class="inner-header">
                            <div class="row">
                                
                         <div class="logo">
                                    <a href="index.html" title="FoodKonnekt Dashboard" class="logo"><!-- <img src="resources/img/logo.jpg"> </a>-->
                             
                                
        <img src="data:image/jpeg;base64,${merchant.merchantLogo}" onerror="this.src='resources/img/foodkonnekt-logo.png'" width="250" height="150"></a>
    </div>
                            </div><!--.row-->
                        </div><!--.inner-header-->
                    </header><!--#page-header-->
                    
                     <!-- <div id="page-content clearfix"> -->
                     
            
            <%-- <form:form method="POST" action="savePaymentGatewayDetails" modelAttribute="paymentGateWay"  id="paymentGateWayForm1" autocomplete="off" name="formID1" > --%>
            <%-- <div id="activePaymentGatewayDiv">
                        <div class="outer-container">
                        <div style="margin:0 auto;padding-top: 52px;" class="clearfix clearfix1"><span style="margin:10px 0 0 0;">
                        <b><font size="4">ACTIVE PAYMENT GATEWAY</font></b></span>
                       <input type="hidden" id="gateway"  value="${paymentGatewayDetails.gateWayType}">
                       <input type="hidden" id="isDeleted"  value="${paymentGatewayDetails.isDeleted}">
                       <input type="hidden" id="isActive"  value="${paymentGatewayDetails.isActive}">
                       
                          <table>
                           <tr><td>Payement gateway</td><td>Active</td><td>Remove</td></tr>
                           <tr id="stripeTR"><td>Stripe</td><td><input type="radio" name="activePaymentGateway" id="stripe"  value="stripe"></td><td><input type="button" class="button" value="remove" id="stripeButton" onclick="removeStripe()"></td></tr>
                           <tr id="authorizeTR"><td>Authorize.net</td><td><input type="radio" name="activePaymentGateway" id="authorize" value="authorize"></td><td><input type="button" class="button" value="remove" id="authorizeButton" onclick="removeauthorize()"></td></tr>
                            </table>
                        </div>
                        </div>
                        </div> --%><%-- </form:form> --%>
                        
                        <!-- <input type="button" class="button" value="add new gateway" id="addNewGateway" onclick="addNewGateway()"> -->
                       
                        <div id="addPaymentGatewayDiv" >
                        <div class="outer-container">
                        <div style="margin:0 auto;padding-top: 52px;" class="clearfix clearfix1"><span style="margin:10px 0 0 0;">
                        <b><font size="4">SET UP PAYMENT GATEWAY</font></b></span>
                        <label id="errorBox" style="color: red;"></label>
                        </div>
                        
                            <div class="row">
                                <div class="content-inner-container">   
                       
                       <form:form method="POST" action="savePaymentGatewayDetailsViaInstallMerchant" modelAttribute="paymentGateWay"  id="paymentGateWayForm" autocomplete="off" name="formID" >                             
                       
                                <div class="adding-products-form" style="width:800px; margin:0 auto;">
                                <p></p><p></p><p></p><p></p>
                                    <div class="clearfix"></div> 
                                    <div class="clearfix"></div>
                                    <label style="color:black">Select payment Gateway:</label>
                                    
                                    <form:select path="gateWayType" id="gatewayTypeId" >
                                   												 <form:option value="0">plz select payment Type</form:option>
                                                                                   <form:options items="${gatewayName}" itemValue="id" itemLabel="paymentGatewayName"/>
                                                                                </form:select><br>
                                    <br>
                                    
                                    <input type="hidden" id="gateway"  value="${paymentGatewayDetails.gateWayType}">
                                     <div id="stripeDiv" style="display: none">
                                    <div class="clearfix"></div> 
                                    <label style="color:black">stripe API key:</label>
                                        <form:input path="stripeAPIKey" value="${paymentGatewayDetails.stripeAPIKey}" maxlength="200" id="stripeAPIKey"/> 
                                    <br>  
                                    
                                    
                                    </div > 
                                     <div id="authorizeDiv" style="display: none">  
                                    <label style="color:black">Transaction key:</label>
                                        <form:input path="authorizeTransactionKey" value="${paymentGatewayDetails.authorizeTransactionKey}" maxlength="200" id="authorizeTransactionKey"/> 
                                    <br>   
                                    <label style="color:black">Login Id:</label>
                                        <form:input path="authorizeLoginId" value="${paymentGatewayDetails.authorizeLoginId}" maxlength="100" id="authorizeLoginId"/>
                                    <br>
                                    </div> 
                                    
                                    
                                      <div id="cayanDiv" style="display: none">  
                                    <label style="color:black">Merchant Name:</label>
                                        <form:input path="cayanMerchantName" value="${paymentGatewayDetails.cayanMerchantName}" maxlength="200" id="cayanMerchantName"/> 
                                    
                                    <br>   
                                    <label style="color:black">MerchantSite Id:</label>
                                        <form:input path="cayanMerchantSiteId" value="${paymentGatewayDetails.cayanMerchantSiteId}" maxlength="100" id="cayanMerchantSiteId"/>
                                    <br>

                                    
                                    <br>
                                    <label style="color:black">Merchant Key:</label>
                                        <form:input path="cayanMerchantKey" value="${paymentGatewayDetails.cayanMerchantKey}" maxlength="100" id="cayanMerchantKey"/>
                                    <br>
                                    </div> 
                                     <div id=iGateDiv style="display: none">  
                                    <label style="color:black">User Name:</label>
                                        <form:input path="iGateUserName" value="${paymentGatewayDetails.iGateUserName}" maxlength="200" id="iGateUserName"/> 
                                    <br>   
                                    <label style="color:black">Password:</label>
                                        <form:input path="iGatePassword" value="${paymentGatewayDetails.iGatePassword}" maxlength="100" id="iGatePassword"/>
                                    <br>
                                    </div>
                                    
                                    
                                    
                                    <div style="width: 359px; padding-top: 15px;" class=" button clearfix"><span style="margin:10px 0 0 0;">
                                    <input type="button" value="Save" id="setGateway" style="height: 43px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                     <div style="display: none" class=" button clearfix" id="skipButton">
                                     <a href="adminPanel" style="float: left;height: 43px;">Skip</a></div>
                                     <div style="display: none" class=" button clearfix" id="cancelButton">
                                     <a  href="onLineOrderLink" style="float: left;height: 43px;">cancel</a></div>
                                    </div>
                                    
                                  

                                    <div style="margin:0 auto;width: 434px;padding-top: 28px;" class="clearfix">
                                    You can change these setting later too from your admin console.
                                    </div>
                                    
                               </div><!--.adding-products-form-->
                               </form:form>
                               
                               
                            </div><!--.adding-products-->   
                                    
                        </div><!--.content-inner-container-->
                    </div><!--.row-->
            </div><!--.outer-container-->
        </div><!--#page-content-->
                    
                    <footer id="footer-container">
                        <div class="footer-outer-container">
                            <div class="footer-inner-container">
                                <div class="row">
                                    <div class="sd-inner-footer">
                                    
                                        <div class="footer-left">
                                            <p>Powered by foodkonnekt | copyright@foodkonnekt.com</p>
                                        </div><!--.footer-left-->
                                        
                                        <div class="footer-right">
                                            <img src="resources/img/foodkonnekt-logo.png" />                                        </div><!--.footer-right-->
                                    </div><!--.sd-inner-footer-->
                                </div><!--.row-->
                            </div><!--.footer-inner-container-->
                        </div><!--.footer-outer-container-->
                    </footer>
                    <!--#footer-container-->
                    
                </div><!--.max-row-->
            </div><!--.inner-container-->
        </div><!--.foodkonnekt .dashboard-->
    </div><!--#page-container-->
    
    
    
     <!--OPENS DIALOG BOX-->
       <script type="text/javascript">
       
       $(document).ready(function() {
           $("#setGateway").click(function() {
        	   $("#paymentGateWayForm").submit();
               
             });
         
         $("#setZoneCancelButton").click(function() {
             window.location="deliveryZones"
         });
         
       });
       
       
     
    </script> 
    
    

  </body>
</html>
