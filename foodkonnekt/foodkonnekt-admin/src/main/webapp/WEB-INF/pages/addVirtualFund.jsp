<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="true"%>
<!doctype html>
<html class="no-js" lang="en">
<head>
<title>FoodKonnekt | Dashboard</title>
<!--CALLING STYLESHEET STYE.CSS-->
<link rel="stylesheet" href="resources/css/style.css">
<!--CALLING STYLESHEET STYLE.CSS-->

<!--CALLING GOOGLE FONT OPEN SANS-->
<link
	href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic'
	rel='stylesheet' type='text/css'>
<!--CALLING GOOGLE FONT OPEN SANS-->

<!--CALLING FONT AWESOME-->
<link rel="stylesheet" href="resources/css/font-awesome.css">
<!--CALLING FONT AWESOME-->

<!--CALLING FILTER OPTIONS-->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<!--  <script src="cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.2.17/jquery.timepicker.min.js" type="text/javascript"></script>
     <script src="cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.2.17/jquery.timepicker.min.js" type="text/javascript"></script> -->
<script type="text/javascript" charset="utf8"
	src="https://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/jquery.dataTables.min.js"></script>
<script
	src="https://www.jqueryscript.net/demo/Powerful-jQuery-Data-Table-Column-Filter-Plugin-yadcf/jquery.dataTables.yadcf.js"></script>
<!--OPENS DIALOG BOX-->
<link rel="stylesheet" type="text/css"
	href="resources/css/dialog-box/component.css" />
<!-- <link rel="stylesheet" type="text/css" href="cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.2.17/jquery.timepicker.min.css"/> -->


<script
	src="https://netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
<script
	type="http://netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css"></script>
<!--OPENS DIALOG BOX-->
<style type="text/css">
.capDay {
	text-transform: capitalize;
}

.inactiveLink {
	pointer-events: none;
	cursor: default;
}
</style>
</head>
<body>
	<div id="page-container">
		<div class="foodkonnekt merchant">
			<div class="inner-container">
				<div class="max-row">

					<header id="page-header">
						<div class="inner-header">
							<div class="row">

								<div class="logo">
									<a href="adminHome" title="FoodKonnekt Dashboard" class="logo"><img
										src="resources/img/foodkonnekt-logo.png"></a>
								</div>
								<!--.logo-->
								<%@ include file="adminHeader.jsp"%>

							</div>
							<!--.row-->
						</div>
						<!--.inner-header-->
					</header>
					<!--#page-header-->

					<div id="page-content">
						<div class="outer-container">
							<div class="row">
								<div class="content-inner-container">
									<%@ include file="leftMenu.jsp"%>

									<div class="right-content-container">
										<div class="right-content-inner-container">

											<div class="content-header">
												<div class="all-header-title"></div>
												<!--.header-title-->
												<div class="content-header-dropdown"></div>
												<!--.content-header-dropdown-->
											</div>
											<!--.content-header-->

											<div class="merchant-page-data">
												<div class="merchant-actions-outbound">
													<div class="merchat-coupons-container">

														<div class="coupons-navigation">
															<ul>
																<li><a href="onLineOrderLink">Location</a></li>
																<li><a href="deliveryZones">Delivery Zones</a></li>
																<!--<li><a href="vouchars">Coupons</a></li>-->
																<li><a href="customers">Customers</a></li>
																<c:if test="${merchant.owner.pos.posId!=1}">
																	<li><a href="setupPaymentGateway?adminPanel=1">Gateway</a></li>

																</c:if>
																<li><a href="notificationMethod">Notifications</a></li>
																<li><a href="getTaxesByMerchantId">Taxes</a></li>
																<li><a
																	href="getMerchantSliders?merchantId=${merchant.id}">Sliders</a></li>
																<li><a href="orderNotification">Order
																		Notification</a></li>
																<li ><a href="user">User</a></li>
																<li><a href="uberStore">Uber Store</a></li>
																<li><a href="merchantSocialPlatform">Social
																		Platform</a></li>
																<li class="current-menu-item"><a href="virtualFund">Virtual Fund</a></li>
															</ul>
														</div>
														<!--.coupons-navigation-->

														<div class="coupons-content-container">

															<div class="clearfix"></div>

															<div class="location-container">
																<div class="location-container-form">
																	<div id="LoadingImage" style="display: none"
																		align="middle">
																		<img src="resources/img/spinner.gif" align="middle" />
																	</div>
																	<section id="content1">
																		<div class="tab-content-container-outbound">
																			<div class="tab-content-container">
																				<div class="tab-content-container-inbound">
																					<div class="only-search-part">

																						<form:form method="POST" action="addNewVirtualFund"
																							modelAttribute="VirtualFund" id="virtualFundForm" onsubmit="return validateCode()">

																							<br>
																							<span id="errorMessage1"
																								style="color: red; font-size: 2000; Margin-left: 44px;"
																								tabindex="0"></span>

                                                                                                                                                                  
                                              
																							<div class="adding-products">
																								<label id="errorMessage" style="color: red;"></label>
																								<div class="adding-products-form"
																									style="margin: -38px 0px 0px 50px; padding-right: 215px;">

																									<div class="clearfix"></div>
																									<label>Partner Name:</label>
																									<form:input type="text" placeholder=""
																										path="partnerName" class="form-control"
																										required="true" />
																									<br>
																									<br> <label>Partner Email Id:</label>
																									<form:input type="email" placeholder="" pattern="^[^(\.)][a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}" 
																										path="partnerEmailId" class="form-control"
																										required="true" />

																									<br>
																									<br> <label>Code:</label>
																									<form:input type="text" placeholder="" id="fundCode"
																										path="code" class="form-control" 
																										required="true" maxlength="20" />

																									<br>
																									<br> <label>percent:</label>
																									<form:input type="text" placeholder="" id="percent"
																										path="percentage" class="form-control"
																										required="true" min="1" max="100"/>
																					                <br>
																									<br>
                                                                                                  
                                                                                                   FROM &nbsp;&nbsp;&nbsp;
                                                                                                 <form:input type="date" id="startDate"  
                                                                                                 path="startDate" style="width: 63%;margin-left: 101px;" required="true"/> 
                                                                                                  &nbsp; TO &nbsp;
                                                                                                  <form:input type="date" id="endDate" path="endDate"
																											style="margin-left: 127px;width: 64%;margin-top: 13px;" required="true"/>

																									
																									<br>
																									<br> <label>Status:</label>
																									<form:select id="status"
																										path="status" required="true" >
																										<option value="">Select Status</option>
																									
																										<option value="1">Active</option>
																										<option value="0">InActive</option>
																										
																									</form:select>
																									<br>
																									<br>


																									<div class="button right">
																										<input type="submit"
																											id="updateVirtualFund" value="Save">&nbsp;&nbsp;

																									</div>
																								</div>
																							</div>


																						</form:form>





																					</div>
																					<!--.only-search-part-->
																				</div>
																				<!--.tab-content-container-inbound-->
																			</div>
																			<!--.tab-content-container-->
																		</div>
																		<!--.tab-content-container-outbound-->
																	</section>
																</div>
																<!--.location-container-form-->
															</div>
															<!--.location-container-->

														</div>
														<!--.coupons-content-container-->
													</div>
													<!--.merchat-coupons-container-->
												</div>
												<!--.merchant-actions-outbound-->
											</div>
											<!--.merchant-page-data-->

										</div>
										<!--.right-content-inner-container-->
									</div>
									<!--.right-content-container-->
								</div>
								<!--.content-inner-container-->
							</div>
							<!--.row-->

						</div>
						<!--.outer-container-->
					</div>
					<!--#page-content-->
					<%@ include file="adminFooter.jsp"%>
					<!--#footer-container-->

				</div>
				<!--.max-row-->
			</div>
			<!--.inner-container-->
		</div>
		<!--.foodkonnekt .dashboard-->
	</div>
	<!--#page-container-->


	<!--#sd-dialog-->

	<!--OPENS DIALOG BOX-->
	<script src="resources/js/dialog-box/classie.js"></script>
	<script src="resources/js/dialog-box/modalEffects.js"></script>
	<script>
		var polyfilter_scriptpath = '/js/';
	</script>
	<!--OPENS DIALOG BOX-->
	<script>
 var result="";

$(document).ready(function() {

        $('#percent').on('input', function() {
        var number = $(this).val().replace(/[^\d]/g, '')
       if (number>100 || number==0) {
        	number = number.substring(0, number.length-1);
        }
        $(this).val(number)
    });
        
result = "${result}";

	if(result != null && result!=""){
	 $('#errorMessage1').html(result);
	 $("#errorMessage1").focus();
	 document.getElementById("virtualFundForm").reset();
	}
}); 


function validateCode(){
var code = $('#fundCode').val();
if(code.length >12){
	$('#errorMessage1').html("Code Should be less than 12 Characters");
	$("#errorMessage1").focus();
	return false;
}
else if(!(/^[a-zA-Z0-9- ]*$/.test(code))) {
	$('#errorMessage1').html("Fund Raiser code contains illegal character");
	$("#errorMessage1").focus();
	return false;
}else
	return true;
}


</script>

</body>
</html>
