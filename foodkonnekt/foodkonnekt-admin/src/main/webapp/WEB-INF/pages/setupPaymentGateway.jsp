<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html lang="en">
<head>

<title>FoodKonnekt</title>
 <link href="resources/img/favicon.ico" rel="icon" type="image/ico"/>
</head>
<body>
	
	



<!doctype html>
<html class="no-js" lang="en">
<head>
<title>FoodKonnekt | Dashboard</title>
<link rel="stylesheet" href="resources/css/style.css">
<link
	href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic'
	rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="resources/css/font-awesome.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="resources/js/checkall/jquery.checkall.js"></script>

<script src="resources/js/payeezy_us_v5.2.js" type="text/javascript"></script>


<link rel="stylesheet" type="text/css"
	href="resources/css/dialog-box/component.css" />
<script src="resources/js/accordion/paccordion.js"></script>
<link rel="stylesheet" type="text/css"
	href="https://cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css">
<script
	src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
	
	

<script src="https://includestest.ccdc02.com/cardinalcruise/v1/songbird.js"></script>
<style>
.btn {
	outline: none !important;
}

.btn {
	border-radius: 0px !important;
}

.form-control {
	width: 100% !important;
}

.sbmt {
	margin-left: 39%;
	background: #f8981d !important;
	font-weight: 100
}

.modal-header {
	background: #f8981d !important;
}

.modal-header .close {
	background: #fff !important;
	padding: 3px !important;
	opacity: 1 !important;
}

.customerdetails .modal .container {
	width: 100% !important;
	background: transparent !important;
	overflow: hidden;
}

.form-group {
	width: 100%;
}

.row {
	margin-left: 75px !important;
}

.row1 {
	width: 100% !important;
}

.form-control {
	border: 1px solid #000 !important;
	height: 48px !important;
}


.pj-preloader {
    display: none;
    position: absolute;
    height: 383px;;
    width: 940px;
    background: url("resources/img/spinner.gif") no-repeat scroll center center rgba(153, 153, 153, 0.3);
    z-index: 9999;
    left: 260;
    position: absolute;
    top: 224px;
}
</style>
<script>
	/* 
	var paymentGatewayRadioValue = $("input[name='activePaymentGateway']:checked").val();
	 */
	$(document).ready(
			function() {
				//alert($("#isDeleted").val());
				// alert($("#isActive").val());

				if ($("#isDeleted").val() == "" && $("#isActive").val() == "") {
					$("#activePaymentGatewayDiv").css("display", "none");
				} else {
					if ($("#gateway").val() == 1
							&& $("#isDeleted").val() == "false"
							&& $("#isActive").val() == "true") {
						document.getElementById("stripe").checked = true;
						$("#igateTR").hide();
						$("#authorizeTR").hide();
						$("#cayanTR").hide();
						$("#firstData").hide();
						$("#payeezyTr").hide();

					}
					if ($("#gateway").val() == 2
							&& $("#isDeleted").val() == "false"
							&& $("#isActive").val() == "true") {
						document.getElementById("authorize").checked = true;
						$("#igateTR").hide();
						$("#stripeTR").hide();
						$("#cayanTR").hide();
						$("#firstData").hide();
						$("#payeezyTr").hide();

					}
					if ($("#gateway").val() == 3
							&& $("#isDeleted").val() == "false"
							&& $("#isActive").val() == "true") {
						document.getElementById("cayan").checked = true;
						$("#igateTR").hide();
						$("#authorizeTR").hide();
						$("#stripeTR").hide();
						$("#firstData").hide();
						$("#payeezyTr").hide();

					}
					if ($("#gateway").val() == 4
							&& $("#isDeleted").val() == "false"
							&& $("#isActive").val() == "true") {
						document.getElementById("igate").checked = true;
						$("#cayanTR").hide();
						$("#authorizeTR").hide();
						$("#stripeTR").hide();
						$("#firstData").hide();
						$("#payeezyTr").hide();

					}

					if ($("#gateway").val() == 5
							&& $("#isDeleted").val() == "false"
							&& $("#isActive").val() == "true") {
						document.getElementById("FirstData").checked = true;
						$("#cayanTR").hide();
						$("#authorizeTR").hide();
						$("#stripeTR").hide();
						$("#igateTR").hide();
						$("#payeezyTr").hide();

					}

					if ($("#gateway").val() == 6
							&& $("#isDeleted").val() == "false"
							&& $("#isActive").val() == "true") {
						document.getElementById("payeezy").checked = true;
						$("#cayanTR").hide();
						$("#authorizeTR").hide();
						$("#stripeTR").hide();
						$("#igateTR").hide();
						$("#firstData").hide();

					}
				}

			});
	$(document).ready(function() {
		console.log(window.location.search);
		var urlParams = new URLSearchParams(window.location.search);
		console.log("value--" + urlParams.get('adminPanel')); // true
		var flag = urlParams.get('adminPanel');
		if (flag == 1) {
			$("#cancelButton").css("display", "block");
		} else {
			$("#skipButton").css("display", "block");
		}
		var saveStatus = "";
		if (saveStatus != "") {
			jQuery(function() {
				jQuery('#confirmModal_ex2').click();
			});
		}
	});
	$(document).ready(function() {
		$('#gatewayTypeId').on('change', function() {

			if (this.value === "1") {
				$("#stripeDiv").css("display", "block");
				$("#authorizeDiv").css("display", "none");
				$("#cayanDiv").css("display", "none");
				$("#iGateDiv").css("display", "none");
				$("#firstDataDiv").css("display", "none");
				$("#payeezyDataDiv").css("display", "none");

				//$("#stripeDiv").show();
				// $("#authorizeDiv").hide();
			}
			if (this.value === "2") {
				$("#authorizeDiv").css("display", "block");
				$("#stripeDiv").css("display", "none");
				$("#cayanDiv").css("display", "none");
				$("#iGateDiv").css("display", "none");
				$("#firstDataDiv").css("display", "none");
				$("#payeezyDataDiv").css("display", "none");

				// $("#authorizeDiv").show();
				// $("#stripeDiv").hide();
			}
			if (this.value === "3") {
				$("#cayanDiv").css("display", "block");
				$("#stripeDiv").css("display", "none");
				$("#authorizeDiv").css("display", "none");
				$("#iGateDiv").css("display", "none");
				$("#firstDataDiv").css("display", "none");
				$("#payeezyDataDiv").css("display", "none");

				// $("#authorizeDiv").show();
				// $("#stripeDiv").hide();
			}
			if (this.value === "4") {
				$("#iGateDiv").css("display", "block");
				$("#cayanDiv").css("display", "none");
				$("#stripeDiv").css("display", "none");
				$("#authorizeDiv").css("display", "none");
				$("#firstDataDiv").css("display", "none");
				$("#payeezyDataDiv").css("display", "none");

				// $("#authorizeDiv").show();
				// $("#stripeDiv").hide();
			}
			if (this.value === "5") {
				$("#firstDataDiv").css("display", "block");
				$("#iGateDiv").css("display", "none");
				$("#cayanDiv").css("display", "none");
				$("#stripeDiv").css("display", "none");
				$("#authorizeDiv").css("display", "none");
				$("#payeezyDataDiv").css("display", "none");
			}

			if (this.value === "6") {
				$("#payeezyDataDiv").css("display", "block");
				$("#iGateDiv").css("display", "none");
				$("#cayanDiv").css("display", "none");
				$("#stripeDiv").css("display", "none");
				$("#authorizeDiv").css("display", "none");
				$("#firstDataDiv").css("display", "none");
			}
		});
	});

	function removeGatewayType() {
		var gateWayType = $("#gateway").val();

		$.ajax({
			url : "removePaymentGateway?gateWayType=" + gateWayType,
			type : "GET",
			contentType : "application/json; charset=utf-8",
			success : function(result) {
				window.location.href = "setupPaymentGateway";
			},
			error : function() {
				console.log("Error inside future date Ajax call");
			}
		})

	}

	//to open div for new payment gateway
	function addNewGateway() {
		//alert("addNewGateway");
		$("#addPaymentGatewayDiv").css("display", "block");
		 $("#iGateUserName").val("");
		  $("#iGatePassword").val("");
		  $("#firstDataPassword").val("");

	}

	function hideGateway() {
		//alert("addNewGateway");
		$("#addPaymentGatewayDiv").css("display", "none");

	}

	function setpaymentDetails() {
		window.location.href = "adminPanel";
	}
</script>



<style type="text/css">
div#example_paginate {
	display: block;
}

div#example_filter {
	display: block;
}

div#example_length {
	display: block;
}

input[type="search"] {
	max-width: 300px;
	width: 87%;
	outline: 0;
	border: 1px solid rgb(169, 169, 169);
	padding: 11px 10px;
	border-radius: 6px;
	margin-bottom: 7px;
	placeholder: Search Items;
	display: none;
}
</style>
<link rel="stylesheet" type="text/css"
	href="resources/css/dialog-box/component.css" />
<!--OPENS DIALOG BOX-->
</head>
<body>
	<div id="page-container">
		<div class="foodkonnekt merchant">
			<div class="inner-container">
				<div class="max-row">

					<header id="page-header">
						<div class="inner-header">
							<div class="row">
								<div class="logo">
									<a href="adminHome" title="FoodKonnekt Dashboard" class="logo"><img
										src="resources/img/foodkonnekt-logo.png"></a>
								</div>
								<!--.logo-->
								
<style>
.md-content {
  color: black;
}
</style>
<style>
*{    font-family: 'Open Sans', sans-serif;}
.btn {
display: inline-block;
    cursor: pointer;
    background: #fff;
    border: 1px solid #bbb;
    height: 42px;
    padding: 11px;
    font-size: 14px;
    line-height: 18px;
    border-radius: 4px;
    text-transform: uppercase;
    font-weight: 600;
    text-align: center;
    max-width: 160px;
    width: 160px;
}
.btn.btn-default {}
.btn.btn-default:hover {background:#eee;border-color:#bbb}
.btn.btn-default:focus {background:#ddd;border-color:#bbb}
.btn.btn-primary {
    background-color: #f8981d;
    border-color: #f8981d;
    color: #fff;
}
.btn.btn-primary:hover {
    background-color: #ffac41;
    border-color: #f8981d;
}
.btn.btn-primary:focus {
    background-color: #f8981d;
    border-color: #f8981d;
}
.btn.btn-default[disabled] {background:#fafafa!important;border-color:#ccc!important;color:#aaa}
.btn.btn-primary[disabled] {background:#3F9DD0!important;border-color:#537FA9!important;color:#ACD3E8;box-shadow:none!important}
.btn.btn-left {float:left;margin:0 5px 0 0!important}
.sd-popup-content img {
    display: block;
    margin: 0 auto 10px;
}
.sd-popup-content h3 {
    text-align: center;
}
</style>
<div class="exampleLive">
    <button style="display:none;" id="confirmModal_ex22" class="btn btn-primary" data-confirmmodal-bind="#confirm_content" data-topoffset="0" data-top="10%" >Example</button>
</div>
<div id="confirm_content" style="display:none">
    <div class="confirmModal_content sd-popup-content">
    <img src="resources/img/logo.png" class="sd-popup-logo">
    <h3>Your session has been timed out. Please log back in</h3>
    </div>
    <div class="confirmModal_footer">
        <button type="button" class="btn btn-primary" onclick="sessionTimeOut()">Ok</button>
    </div>
</div>
<div class="header-nav">
    <nav class="header-nav-container">
        <ul>
            <li><button class="md-trigger custom-sd-button-dialog" data-modal="modal-18">
                    <i class="fa fa-user-plus" aria-hidden="true"></i> Support
                </button> |</li>
            <!-- <li><a href="#"><i class="fa fa-user" aria-hidden="true"></i> My Account</a> |</li> -->
            <li><a href="logout"><i class="fa fa-power-off" aria-hidden="true"></i> Log Out</a></li>
        </ul>
    </nav>
    <!--.header-nav-container-->
</div>
<!--.header-nav-->
<div id="sd-dialog">
    <div class="md-modal md-effect-14" id="modal-18">
        <div class="md-content">
            <h3>FoodKonnekt Support</h3>
            <div>
                <p>Phone Support: 619-566-6358 (10am CST to 10pm CST)</p>
                <p>Twitter: @FoodKonnektHelp</p>
                <p>Email us: support@foodkonnekt.com</p>
                <p>
                    <a href="https://foodkonnekt.freshdesk.com/support/home" target="_blank" style="color: blue;">Support FAQ</a>
                </p>
                <div class="button">
                    <button class="white-button md-close">Close</button>
                </div>
                <!--.button-->
            </div>
        </div>
    </div>
    <div class="md-overlay"></div>
    <!-- the overlay element -->
</div>
<!--#sd-dialog-->
 <!-- <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script> -->
<link type="text/css" rel="stylesheet" href="resources/css/popModal.css">
<script src="resources/js/popModal.js"></script>
<script type="text/javascript">
var timeOut=900000; 
var timeoutHandle=setTimeout(function(){ 
    jQuery(function(){
        jQuery('#confirmModal_ex22').click();
      });
 }  , timeOut );
function sessionTimeOut(){
     window.location.href = "sessionOut";
}
</script>
							</div>
							<!--.row-->
						</div>
						<!--.inner-header-->
					</header>
					<!--#page-header-->

					<div id="page-content">
						<div class="outer-container">
							<div class="row">
								<div class="content-inner-container">
									


<div class="left-nav-bar">
    <nav class="left-nav-bar-container">
        <div class="accordion-wrapper">
            <div class="ac-pane">
                <a href="adminHome" class="adminHome"> <span>Dashboard</span>
                </a>
            </div>
            
            <div class="ac-pane">
                <a href="allOrders" class="allOrders"> <span>Orders</span>
                </a>
            </div>
            <div class="ac-pane">
                <a href="inventory" class="inventory"> <span>Inventory</span>
                </a>
            </div>
           
            <div class="ac-pane" id="pizzaTab" style="display: none;">
                <a href="pizzaTamplate" class="pizzaTamplate"> <span>Pizza</span>
                </a>
            </div>
            <div class="ac-pane">
                <a href="onLineOrderLink" class="onLineOrderLink"> <span>Merchant</span>
                </a>
            </div>
           <!--  <div class="ac-pane">
                <a href="#" class="ac-title" data-accordion="true"> <span>Merchant</span> <i class="fa"></i>
                </a>
                <div class="ac-content">
                    <ul>
                        <li><a href="merchants">Merchant</a></li>
                        <li><a href="onLineOrderLink">Online Order Link</a></li>
                        <li><a href="adminLogo">Upload Logo</a></li>
                        <li><a href="deliveryZones">Add Delivery Zone</a></li>
                        <li><a href="pickUpTime">Add Pick Time</a></li>
                        <li><a href="convenienceFee">Convenience Fee</a></li>
                        <li><a href="customers">Customers</a></li>
                        <li><a href="createVouchar">Add Vouchars</a></li>
                        <li><a href="vouchars">Vouchars</a></li>
                    </ul>
                </div>
            </div> -->
        </div>
    </nav>
    <!--.left-nav-bar-container-->
    <div class="sidebar-logo">
    
        <img src="/foodkonnekt_merchat_logo/301_KidsCatering2.jpg" onerror="this.src='resources/img/foodkonnekt-logo.png'" width="250" height="150">
    </div>
    <!--.sidebar-logo-->
</div>
<!--.left-nav-bar-->
<script type="text/javascript">
var status= "success";
$(document).ready(function() {
	 if(status=="success"){
		 $("#pizzaTab").css('display','block');
	 }
 });
</script>
									<div class="right-content-container">
										<div class="right-content-inner-container">

											<div class="content-header">
												<div class="all-header-title"></div>
												<!--.header-title-->
												<div class="content-header-dropdown"></div>
												<!--.content-header-dropdown-->
											</div>
											<!--.content-header-->

											<div class="merchant-page-data">
												<div class="merchant-actions-outbound">
													<div class="merchat-coupons-container">
														<%@ include file="adminMerchantMenu.jsp"%>


														<div id="activePaymentGatewayDiv">
															<div class="outer-container">
																<div style="margin: 0 auto; padding-top: 52px;"
																	class="clearfix clearfix1">
																	<h3>ACTIVE PAYMENT GATEWAY</h3>


																	<input type="hidden" id="gateway"
																		value="${paymentGatewayDetails.gateWayType}"> <input
																		type="hidden" id="isDeleted"
																		value="${paymentGatewayDetails.isDeleted}"> <input
																		type="hidden" id="isActive"
																		value="${paymentGatewayDetails.isActive}"> <span
																		id="errorMsg"></span>
															
															<table width="100%" cellpadding="0" cellspacing="0"
																		id="example">
																		<thead>
																			<tr>
																				<th>Gateway Name</th>
																				<th>Active</th>
																				<th>Remove</th>
																			</tr>
																		</thead>
																		<tr id="stripeTR">
																			<td>Stripe</td>
																			<td><input type="radio"
																				name="activePaymentGateway" id="stripe"
																				value="stripe"></td>
																			<td><a href="#" onclick="removeGatewayType()"
																				style="color: blue;">Click To Remove</a></td>
																		</tr>
																		<tr id="authorizeTR">
																			<td>Authorize.net</td>
																			<td><input type="radio"
																				name="activePaymentGateway" id="authorize"
																				value="authorize"></td>
																			<td>
																				<!-- <div style="width: 81px; padding-top: 15px;" class=" button clearfix"><span style="margin:10px 0 0 0;">
                           <input type="button" style=" padding-bottom: 6px;padding-left: 18px;padding-top: 6px;" class="button"  value="remove" id="authorizeButton" onclick="removeauthorize()">
                           </span></div> -->
																				<a href="#" onclick="removeGatewayType()"
																				style="color: blue;">Click To Remove</a>
																			</td>
																		</tr>

																		<tr id="cayanTR">
																			<td>Cayan</td>
																			<td><input type="radio"
																				name="activePaymentGateway" id="cayan" value="cayan"></td>
																			<td>
																				<!-- <div style="width: 81px; padding-top: 15px;" class=" button clearfix"><span style="margin:10px 0 0 0;">
                           <input type="button" style=" padding-bottom: 6px;padding-left: 18px;padding-top: 6px;" class="button"  value="remove" id="cayanButton" onclick="removecayan()"> </span></div> -->
																				<a href="#" onclick="removeGatewayType()"
																				style="color: blue;">Click To Remove</a>
																			</td>
																		</tr>
																		<tr id="igateTR">
																			<td>TGate</td>
																			<td><input type="radio"
																				name="activePaymentGateway" id="igate" value="igate"></td>
																			<td><a href="#" onclick="removeGatewayType()"
																				style="color: blue;">Click To Remove</a></td>
																		</tr>


																		<tr id="firstData">
																			<td>FirstData</td>
																			<td><input type="radio"
																				name="activePaymentGateway" id="FirstData"
																				value="FirstData"></td>
																			<td><a href="#" onclick="removeGatewayType()"
																				style="color: blue;">Click To Remove</a></td>
																		</tr>

																		<tr id="payeezyTr">
																			<td>Payeezy</td>
																			<td><input type="radio"
																				name="activePaymentGateway" id="payeezy"
																				value="payeezy"></td>
																			<td><a href="#" onclick="removeGatewayType()"
																				style="color: blue;">Click To Remove</a></td>
																		</tr>
																		
																		
																			

																		<!--      <tr id="FirstData"><td>FirstData</td><td><input type="radio" name="activePaymentGateway" id="FirstData" value="FirstData"></td>
                           <td>
                           <a href="#" onclick="removeGatewayType()" style="color: blue;">Click To Remove</a>
                           </td>
                           </tr> -->
																	</table>

																</div>
															</div>
														</div>
														


                            <div class="pj-preloader"></div>


														<div style="width: 359px; padding-top: 15px;"
															class=" button clearfix">
															<span style="margin: 10px 0 0 0;"> <font size="3"
																color="red" id="response"></font> <input
																type="hidden" id="response1" val="">
																<!-- <button type="button" class="btn btn-warning btn-lg"
																	data-toggle="modal" data-target="#myModal">Open
																	Modal</button> --> <input type="button" value="add new gateway"
																id="addNewGateway" onclick="addNewGateway()"
																style="margin-left: 755px; margin-top: 9px; width: 183px; padding-top: 6px; padding-bottom: 6px;">
															</span>


															<!--   <button type="button" class="btn btn-warning btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button>
 -->

														</div>
														<!--  <input type="button" style="background-color:#ff9933; border-color:#ff9933"  value="add new gateway" id="addNewGateway" onclick="addNewGateway()" style=" margin-left: 50px;margin-top: 50px;"> -->

														<div id="addPaymentGatewayDiv" style="display: none">
															<div class="outer-container">
																<div style="margin: 0 auto; padding-top: 52px;"
																	class="clearfix clearfix1">
																	<!-- <span style="margin:10px 0 0 0;">
                        <b><font size="4">SET UP PAYMENT GATEWAY</font></b></span> -->
																	<h3>SET UP PAYMENT GATEWAY</h3>
																	<label id="errorBox" style="color: red;"></label>
																</div>

																<div class="row">
																	<div class="content-inner-container">

																		<form id="paymentGateWayForm" name="formID" action="verifyPaymentGatewayDetails" method="POST" autocomplete="off">

																			<!--  <div class="adding-products-form" style="width:400px; margin:0 auto;"> -->
																			<!-- <p></p><p></p><p></p><p></p> -->
																			<span id="3dspayment-errors" style="color: red;"></span>
																			
																			<div class="adding-products-form">
																		
																			
																				<label style="color: black">Select payment
																					Gateway:</label>
																				<select id="gatewayTypeId" name="gateWayType">
																					<option value="0">plz select payment Type</option>
																					<option value="1">stripe</option>
																					<option value="2">authorize.net</option>
																					<!-- <option value="3">Cayan</option> -->
																					<option value="4">TGate</option>
																					<!-- <option value="5">First Data</option> -->
																					<option value="6">Payeezy</option>
																				</select>
																				<br> <br> <input type="hidden"
																					id="gateway"
																					value="">
																				<div id="stripeDiv" style="display: none">
																					<div class="clearfix"></div>
																					<label style="color: black">stripe API key:</label>
																					<input id="stripeAPIKey" name="stripeAPIKey" type="text" value="" maxlength="200"/>
																					<br>
																				</div>
																				<div id="authorizeDiv" style="display: none">
																					<label style="color: black">Transaction
																						key:</label>
																					<input id="authorizeTransactionKey" name="authorizeTransactionKey" type="text" value="" maxlength="200"/>
																					<br> <label style="color: black">Login
																						Id:</label>
																					<input id="authorizeLoginId" name="authorizeLoginId" type="text" value="" maxlength="100"/>
																					<br>
																				</div>
																				<div id="cayanDiv" style="display: none">
																					<label style="color: black">Merchant Name:</label>
																					<input id="cayanMerchantName" name="cayanMerchantName" type="text" value="" maxlength="200"/>
																					<br> <label style="color: black">MerchantSite
																						Id:</label>
																					<input id="cayanMerchantSiteId" name="cayanMerchantSiteId" type="text" value="" maxlength="100"/>
																					<br> <br> <label style="color: black">Merchant
																						Key:</label>
																					<input id="cayanMerchantKey" name="cayanMerchantKey" type="text" value="" maxlength="100"/>
																					<br>
																				</div>

																				<div id="iGateDiv" style="display: none">
																					<label style="color: black">User Name:</label>
																					<input id="iGateUserName" name="iGateUserName" type="text" value="" maxlength="200"/>
																					<br> <label style="color: black">Password:</label>
																					<input id="iGatePassword" name="iGatePassword" type="password" value="" maxlength="100"/>
																					<br>
																				</div>

																				<div id="firstDataDiv" style="display: none">
																					<label style="color: black">User Name:</label>
																					<input id="firstDataUserName" name="firstDataUserName" type="text" value="" maxlength="200"/>
																					<br> <label style="color: black">Password:</label>
																					<input id="firstDataPassword" name="firstDataPassword" type="password" value="" maxlength="100"/>
																					<br> <label style="color: black">first
																						Data MerchantId:</label>
																					<input id="firstDataMerchantId" name="firstDataMerchantId" type="text" value="" maxlength="100"/>
																					<br>
																				</div>

   

																				<div id="payeezyDataDiv" style="display: none">
						

																					<label style="color: black">Payeezy
																						Merchant Token: </label>
																					<input id="payeezyMerchnatToken" name="payeezyMerchnatToken" type="text" value="" maxlength="100"/>
																					<br>


																					<div id="3dspan" style="display: none">
																						<label style="color: black">Payeezy
																							3D ApiKey : </label> 
																							
																							<input type="text"
																							value=""
																							maxlength="100" id="payeezy3DSApiKey" /> <br>
																							
																							
																							<label style="color: black">Payeezy
																							Merchant Token Js: </label> 
																							
																							<input type="text"
																							value=""
																							maxlength="100" id="payeezyMerchnatTokenJs" /> <br>
																				
																				      
																				      <label style="color: black">Payeezy 3D ApiIdentifier: </label> 
																							
																							<input type="text"	value=""	maxlength="100" id="payeezy3DSApiIdentifier" /> <br>
																						
																						
																							<label style="color: black">Payeezy 3D
																							OrgUnitID: </label> 
																							<input type="text"
																							value=""
																							maxlength="100" id="payeezy3DSOrgUnitId" /> <br>
																							
																							<label style="color: black">Payeezy 3D TaToken: </label>
																							<input type="text"
																							value=""
																							maxlength="100" id="payeezy3DTaToken" /> <br>
																				
																					</div>


																					<c:if test="${sessionScope.merchant.id==320 ||sessionScope.merchant.id==321 ||sessionScope.merchant.id==134}"><label><span><input id="3DSeure" name="payeezyMerchnatToken" onclick="3DSeure()" type="checkbox" value="Credit Card"/><input type="hidden" name="_payeezyMerchnatToken" value="on"/>3D Secure</span>
																							</label>
																							</c:if>
																				</div>


																				<div style="width: 359px; padding-top: 15px;"
																					class=" button clearfix">
																					<!-- <span style="margin:10px 0 0 0;"> -->
																					
																					
																					<input type="button" value="Save" id="setGateway"
																						style="height: 43px;"> 
																						
																						
																						
																						<input type="button" value="Save" id="set3DGateway" style="height: 43px; display: none"
																						
																						>

																					<!-- &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -->
																					<!-- <div  class=" button clearfix" id="cancelButton"> -->
																					<a href="#" style="float: left; height: 43px;"
																						onclick="hideGateway()">cancel</a>
																					<!-- </div> -->
																				</div>




																			</div>


																		</form>


																	</div>
																	<!--.adding-products-->

																</div>
																<!--.content-inner-container-->
															</div>
															<!--.row-->
														</div>
														<!--.outer-container-->


													</div>
													<!--.merchat-coupons-container-->
												</div>
												<!--.merchant-actions-outbound-->
											</div>
											<!--.merchant-page-data-->

										</div>
										<!--.right-content-inner-container-->
									</div>
									<!--.right-content-container-->
								</div>
								<!--.content-inner-container-->
							</div>
							<!--.row-->

						</div>
						<!--.outer-container-->
					</div>
					<!--#page-content-->
					<!-- <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script> -->
<footer id="footer-container">
	<div class="footer-outer-container">
		<div class="footer-inner-container">
			<div class="row">
				<div class="sd-inner-footer">

					<div class="footer-left">
						<p>Powered by foodkonnekt | copyright@foodkonnekt.com</p>
					</div>
					<!--.footer-left-->
					<div class="footer-right">
						<img src="resources/img/foodkonnekt-logo.png" />
					</div>
					<!--.footer-right-->
				</div>
				<!--.sd-inner-footer-->
			</div>
			<!--.row-->
		</div>
		<!--.footer-inner-container-->
	</div>
	<!--.footer-outer-container-->
</footer>
<script>
	$(document).ready(
			function() {
				var url = window.location.href;
				if ((url.indexOf("adminHome")) > 1) {
					$(".adminHome").addClass("sd_menu");
				}

				if ((url.indexOf("allOrders")) > 1
						|| (url.indexOf("customerOrders")) > 1) {
					$(".allOrders").addClass("sd_menu");
				}

				if ((url.indexOf("inventory")) > 1) {
					$(".inventory").addClass("sd_menu");
				}

				if ((url.indexOf("onLineOrderLink")) > 1) {
					$(".onLineOrderLink").addClass("sd_menu");
				}

				if ((url.indexOf("deliveryZones")) > 1) {
					$(".onLineOrderLink").addClass("sd_menu");
				}
				if ((url.indexOf("addDeliveryZone")) > 1) {
					$(".onLineOrderLink").addClass("sd_menu");
				}
				if ((url.indexOf("vouchars")) > 1) {
					$(".onLineOrderLink").addClass("sd_menu");
				}
				if ((url.indexOf("createVouchar")) > 1) {
					$(".onLineOrderLink").addClass("sd_menu");
				}

				if ((url.indexOf("customers")) > 1) {
					$(".onLineOrderLink").addClass("sd_menu");
				}

				if ((url.indexOf("category")) > 1) {
					$(".inventory").addClass("sd_menu");
				}
				if ((url.indexOf("modifierGroups")) > 1) {
					$(".inventory").addClass("sd_menu");
				}
				if ((url.indexOf("modifiers")) > 1) {
					$(".inventory").addClass("sd_menu");
				}
				if ((url.indexOf("findItemsByCategoryId")) > 1) {
					$(".inventory").addClass("sd_menu");
				}
				if ((url.indexOf("addLineItem")) > 1) {
					$(".inventory").addClass("sd_menu");
				}

				if ((url.indexOf("pizzaTamplate")) > 1) {
					$(".pizzaTamplate").addClass("sd_menu");
				}

				if ((url.indexOf("editPizzaTemplate")) > 1) {
					$(".pizzaTamplate").addClass("sd_menu");
				}

				if ((url.indexOf("pizzaTopping")) > 1) {
					$(".pizzaTamplate").addClass("sd_menu");
				}

				if ((url.indexOf("updatePizzaTemplate")) > 1) {
					$(".pizzaTamplate").addClass("sd_menu");
				}
				
				if ((url.indexOf("notificationMethod")) > 1) {
					$(".onLineOrderLink").addClass("sd_menu");
				}
				
				if ((url.indexOf("setupPaymentGateway")) > 1) {
					$(".onLineOrderLink").addClass("sd_menu");
				}
				
				if ((url.indexOf("getTaxesByMerchantId")) > 1) {
					$(".onLineOrderLink").addClass("sd_menu");
				}
				
				if ((url.indexOf("getMerchantSliders")) > 1) {
					$(".onLineOrderLink").addClass("sd_menu");
				}
				
				if ((url.indexOf("itemCategory")) > 1) {
					$(".inventory").addClass("sd_menu");
				}
				
				if ((url.indexOf("createItem")) > 1) {
					$(".inventory").addClass("sd_menu");
				}
				
				if ((url.indexOf("createCategory")) > 1) {
					$(".inventory").addClass("sd_menu");
				}
				
				if ((url.indexOf("createModifierGroup")) > 1) {
					$(".inventory").addClass("sd_menu");
				}
				
				if ((url.indexOf("itemModifierGroup")) > 1) {
					$(".inventory").addClass("sd_menu");
				}
				
				if ((url.indexOf("createModifier")) > 1) {
					$(".inventory").addClass("sd_menu");
				}
				
				if ((url.indexOf("createPizzaTemplate")) > 1) {
					$(".pizzaTamplate").addClass("sd_menu");
				}
				
				if ((url.indexOf("pizzaTemplateMpping")) > 1) {
					$(".pizzaTamplate").addClass("sd_menu");
				}
				
				if ((url.indexOf("createPizzaTopping")) > 1) {
					$(".pizzaTamplate").addClass("sd_menu");
				}
				
				if ((url.indexOf("pizzaCategoryMap")) > 1) {
					$(".pizzaTamplate").addClass("sd_menu");
				}
			});
</script>
				</div>
				<!--.max-row-->
			</div>
			<!--.inner-container-->
		</div>
		<!--.foodkonnekt .dashboard-->
	</div>
	<!--#page-container-->

	<!-- Modal -->
	<div class="modal fade customerdetails" id="myModal" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<p>
					<div>
					 <span id="errorMsgPopup" style="color:red;font-size: 16px;"></span> 
						<form id="container1" style="display: none" action="testPaymentGatewayDetails" method="post" autocomplete="off">
							<div class="row1">
								<div class="col-md-6">
									<div class="form-group">
										<label for="name">Name:</label> <input id="name" name="customerName" name="pswd" type="text" class="form-control" value=""/>
									</div>
									<div class="form-group">
										<label for="ct">Card Type:</label>
										<select id="payment-method-cc-type" name="ccType" class="form-control" onchange="getPaymentType(this.value)">
											  <option value="">Select Card</option>
											<option value="American Express">American Express</option>
											<option value="Master Card">Master Card</option>
											<option value="Visa">Visa</option>
										</select>


									</div>
									<div class="form-group">
										<label for="cvv">CVV:</label>
										<input id="cvv" name="ccCode" name="pswd" type="password" class="form-control" oninput="this.value = this.value.replace(/[^0-9.]/g, &#39;&#39;); this.value = this.value.replace(/(\..*)\./g, &#39;$1&#39;);" value=""/>
									</div>
									<div class="form-group">
										<label for="ce">Month:</label>
											<select id="payment-method-expMonth" name="expMonth" class="form-control">
												<option value="">Month</option>
												<option value="01">01</option>
												<option value="02">02</option>
												<option value="03">03</option>
												<option value="04">04</option>
												<option value="05">05</option>
												<option value="06">06</option>
												<option value="07">07</option>
												<option value="08">08</option>
												<option value="09">09</option>
												<option value="10">10</option>
												<option value="11">11</option>
												<option value="12">12</option>
											</select></div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="cn">Card Number:</label>
										<input id="cn" name="ccNumber" name="pswd" type="text" class="form-control" oninput="this.value = this.value.replace(/[^0-9.]/g, &#39;&#39;); this.value = this.value.replace(/(\..*)\./g, &#39;$1&#39;);" value=""/>
									</div>



									<div class="form-group">
										<label for="addr">Address:</label>
										<input id="addr" name="customerAddress" name="pswd" type="text" class="form-control" value=""/>
									</div>

									<div class="form-group">
										<label for="zip">Zip:</label>
										<input id="zip" name="zipCode" name="pswd" type="text"    onkeypress='return onlyNumberQuantity(event,this);' class="form-control" value="" maxlength="5"/>
									</div>
									<div class="form-group">
										<label for="ce">Year:</label>
										<select id="payment-method-expYear" name="expYear" class="form-control">
											<option value="">Year</option>
											<option value="2019">2019</option>
											<option value="2020">2020</option>
											<option value="2021">2021</option>
											<option value="2022">2022</option>
											<option value="2023">2023</option>
											<option value="2024">2024</option>
											<option value="2025">2025</option>
											<option value="2026">2026</option>
											<option value="2027">2027</option>
											<option value="2028">2028</option>
											<option value="2029">2029</option>
											<option value="2030">2030</option>

										</select>
									</div>
									
									<div class="form-group">
									
									<input id="iGateUserName" name="iGateUserName" type="hidden" class="form-control" value=""/></div>
											
											<div class="form-group">
									
									<input id="iGatePassword" name="iGatePassword" type="hidden" class="form-control" value=""/></div>
											
											
											<div class="form-group">
									
									<input id="firstDataMerchantId" name="firstDataMerchantId" type="hidden" class="form-control" value=""/></div>
											
											
											<div class="form-group">
									
											<input id="authorizeTransactionKey" name="authorizeTransactionKey" class="form-control" type="hidden" value=""/></div>
											
											
											<input id="authorizeLoginId" name="authorizeLoginId" class="form-control" type="hidden" value=""/></div>
											
											
								</div>
								<button type="submit" id="btn" class="btn btn-warning sbmt">Submit</button>
								
						</form>
					</div>

					<span class="container2"> </span>

					</p>
				</div>

			</div>

		</div>
	</div>
	

<div class="modal fade customerdetails"  id="3DSeureModal" role="dialog">
<div class="modal-dialog">
   <!-- Modal content-->
   <div class="modal-content">
      <div class="modal-header">		
         <button type="button" class="close" data-dismiss="modal">&times;</button>          
      </div>
      <div class="modal-body">
         <p>
         <div class="container1">
            <form method="post" name="payment-info-form" id="payment-info-form">
               <h4 style="color: red">
                  <span id="payment-errors"></span>
               </h4>
               <h4 style="color: green">
                  <span id="response_msg"></span>
               </h4>
               <div id="someHiddenDiv" style="display: none; color: red">Requesting
                  Payeezy token...
               </div>
               <input type="hidden" name="apikey" id="apikey"
                  value="BrESAKjlxZhYGq8oACMllFTgkIaGJ7ym" />
               <input type="hidden" name="apisecret" id="apisecret"
                  value="aa5f9c78d4c46945752403795993fdaac2006acd9b9daab217f5113c741ab049" />
               <input type="hidden" name="token" id="token"
                  value="fdoa-6ef18062858acd1aa4c2e08ccbba6b6e6ef18062858acd1a" />
               <input type="hidden" name="js_security_key"
                  id="js_security_key"
                  value="js-2a027f76da388a20f46e6b2083d6abec2a027f76da388a20" />
               <input type="hidden" name="currency" id="currency"
                  payeezy-data="currency" value="USD" />
               <input type="hidden" name="ta_token" id="ta_token"
                  payeezy-data="ta_token" value="NOIW" />
               <div class="row1">
                  <div class="col-md-6">
                     <div class="form-group">
                        <label for="name">Card Holder Name:</label>
                        <input type="text" class="form-control" payeezy-data="cardholder_name" id ="cardholder_name" placeholder="" name="pswd">
                     </div>
                     <div class="form-group">
                        <label for="ct">Card Type:</label>
                        <select payeezy-data="card_type" id ="card_type"  onchange="getPaymentType2(this.value)" class="form-control">
                           <option value="">Select Card</option>
                           <option value="visa">Visa</option>
                           <option value="mastercard">Master Card</option>
                           <option value="American Express">American Express</option>
                        </select>
                     </div>
                     <div class="form-group">
                        <label for="cvv">CVV:</label>
                        <input type="text" class="form-control"payeezy-data="cvv_code" id ="cvv_code"   oninput="this.value = this.value.replace(/[^0-9.]/g, &#39;&#39;); this.value = this.value.replace(/(\..*)\./g, &#39;$1&#39;);"" placeholder="" name="pswd">
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="form-group">
                        <label for="cn">Card Number:</label>
                        <input type="text" payeezy-data="cc_number" id ="cc_number" oninput="this.value = this.value.replace(/[^0-9.]/g, &#39;&#39;); this.value = this.value.replace(/(\..*)\./g, &#39;$1&#39;);" class="form-control" placeholder="" name="pswd">
                     </div>
                     <div class="form-group">
                        <label for="ce">Year:</label>
                        <select payeezy-data="exp_year" id="exp_year" class="form-control" >
                           <option value="19">2019</option>
                           <option value="20">2020</option>
                           <option value="21">2021</option>
                           <option value="22">2022</option>
						   <option value="23">2023</option>
						   <option value="24">2024</option>
						   <option value="25">2025</option>
						   <option value="26">2026</option>
						   <option value="27">2027</option>
						   <option value="28">2028</option>
						   <option value="29">2029</option>
						   <option value="30">2030</option>
						   
						   
                        </select>
                     </div>
                     <div class="form-group">
                        <label for="ce">Month:</label>
                        <select payeezy-data="exp_month"  id="exp_month"  class="form-control">
                           <option value="01">01</option>
                           <option value="02">02</option>
                           <option value="03">03</option>
                           <option value="04">04</option>
                           <option value="05">05</option>
                           <option value="06">06</option>
                           <option value="07">07</option>
                           <option value="08">08</option>
                           <option value="09">09</option>
                           <option value="10">10</option>
                           <option value="11">11</option>
                           <option value="12" selected>12</option>
                        </select>
                     </div>
                  </div>
                  <button type="submit" class="btn btn-warning sbmt">Submit</button>
            </form>
            </div>
            </p>
         </div>
      </div>
   </div>
</div>
  
	
	<!--OPENS DIALOG BOX-->
	<script src="resources/js/dialog-box/classie.js"></script>
	<script src="resources/js/dialog-box/modalEffects.js"></script>
	<script >
    var polyfilter_scriptpath = '/js/';
    </script>
<!--OPENS DIALOG BOX-->
<script >
    $(function() {
        $('label').each(function() {
            if ($(this).text() == 'Search:') {
                $(this).text('');
            }
        })
    })

$(document).ready(
    function() {

        var params = new window.URLSearchParams(
            window.location.search);

        if (params.has('response')) {
            let res = params.get('response')

            if (res == 'Success') {
                $('#myModal').modal('show');
                $("#container1").css("display", "block");

            } else {
                $('#myModal').modal('show');
                $("#container1").css("display", "none");
                $(".container2").html(
                    " <form> <h1>" + res + "</h1> </form>");
            }

        }

        $("#setGateway").click(function() {
            $("#paymentGateWayForm").submit();
            //var authorizeLoginId = $("#authorizeLoginId").val();

            /* if (authorizeLoginId == "") {
                $("#authorizeLoginId").css('border-color', 'red');
              $("#authorizeLoginId").focus();
              $("#errorBox").html("Enter the loginId");
              return false;
            } else if ($(authorizeLoginId != '')) {
                $("#avgDeliveryTime").css('border-color', '');
                $("#errorBox").html("");
                $("#paymentGateWayForm").submit();
            }  */
        });




        $("#3DSeure").click(function() {
            if ($("#3DSeure").prop('checked') == true) {
                $("#3dspan").css("display", "block");
                $("#setGateway").css("display", "none");
                $("#set3DGateway").css("display", "block");
            } else {
                $("#3dspan").css("display", "none");
                $("#setGateway").css("display", "block");
                $("#set3DGateway").css("display", "none");
            }
        });

        var payeezy3DSApiKey ;
        var payeezy3DSApiIdentifier;
        var payeezy3DSOrgUnitId;
        var payeezy3DTaToken;

        $("#set3DGateway").click(function() {
            if ($("#3DSeure").prop('checked') == true) {
                var payeezyMerchnatTokenJs = $("#payeezyMerchnatTokenJs").val();
                var payeezyMerchnatToken = $("#payeezyMerchnatToken").val();
                
                
                payeezy3DSApiIdentifier = $("#payeezy3DSApiIdentifier").val();
                payeezy3DSApiKey =     $("#payeezy3DSApiKey").val();
                payeezy3DSOrgUnitId =  $("#payeezy3DSOrgUnitId").val();
                payeezy3DTaToken = $("#payeezy3DTaToken").val();
                
                
                
                
                if (payeezyMerchnatTokenJs == "") {
                    $("#payeezyMerchnatTokenJs").focus();
                    document.getElementById("3dspayment-errors").innerHTML = "Please Enter Payeezy Merchnat TokenJs";
                    return false;
                } else if (payeezyMerchnatToken == "") {
                    $("#payeezyMerchnatToken").focus();
                    document.getElementById("3dspayment-errors").innerHTML = "Please Enter Payeezy MerchnatToken ";
                    return false;
                } else if (payeezy3DSApiIdentifier == "") {
                    $("#payeezy3DSApiIdentifier").focus();
                    document.getElementById("3dspayment-errors").innerHTML = "Please Enter Payeezy 3DS ApiIdentifier";
                    return false
                } 
                else if (payeezy3DSApiKey == "") {
                    $("#payeezy3DSApiKey").focus();
                    document.getElementById("3dspayment-errors").innerHTML = "Please Enter Payeezy 3DS ApiKey";
                    return false
                } 
                else if (payeezy3DSOrgUnitId == "") {
                    $("#payeezy3DSOrgUnitId").focus();
                    document.getElementById("3dspayment-errors").innerHTML = "Please Enter Payeezy 3DS OrgUnitId";
                    return false
                }
                else if(payeezy3DTaToken == ""){
                	$("#payeezy3DTaToken").focus();
                	document.getElementById("3dspayment-errors").innerHTML = "Please Enter Payeezy 3DS TaToken";
                    return false
                }
                
                
                $("#token").val(payeezyMerchnatToken);
                $("#ta_token").val(payeezy3DTaToken);
                $("#apikey").val(payeezy3DSApiKey);
                $("#apisecret").val(payeezy3DSApiIdentifier);
                $("#js_security_key").val(payeezyMerchnatTokenJs);
             
                  
                  $('#3DSeureModal').modal('show');
            }

        });



        $("#btn").click(function() {

            var name = $("#name").val();
            var typeVal = $('#payment-method-cc-type').val();
            var cardNumber = $("#cn").val();
            var cvv = $("#cvv").val();
            var expMonth = $("#payment-method-expMonth").val();
            var address = $("#addr").val();
            var zipCode = $("#zip").val();
            var expYear = $("#payment-method-expYear").val();

            if (name == "") {
                $("#name").focus();
                document.getElementById("errorMsgPopup").innerHTML = "Please Enter name";
                return false;
            } else if (typeVal == "") {
                $("#card").focus();
                document.getElementById("errorMsgPopup").innerHTML = "Please select card type";
                return false;
            } else if (typeVal == "Select Card") {
                $("#card").focus();
                document.getElementById("errorMsgPopup").innerHTML = "Please select card type";
                return false
            } else if (cardNumber == "") {
                $("#cn").focus();
                document.getElementById("errorMsgPopup").innerHTML = "Please enter card number";
                return false;
            } else if (typeVal == "American Express" && cardNumber.length != 15) {
                $("#cn").val("")
                document.getElementById("errorMsgPopup").innerHTML = "Card number should be 15 characters";
                return false
            } else if ((typeVal == "Master Card" || typeVal == "Visa") && (cardNumber.length != 16)) {
                $("#cn").val("")
                document.getElementById("errorMsgPopup").innerHTML = "Card number should be 16 characters";
                return false
            } else if (cardNumber.indexOf('.') > -1) {
                $("#cn").val("")
                document.getElementById("errorMsgPopup").innerHTML = "Please enter valid card number";
                return false
            } else if (expMonth == "Month") {
                $("#payment-method-expMonth").focus();
                document.getElementById("errorMsgPopup").innerHTML = "Please select card expiry month";
                return false;
            } else if (expYear == "Year") {
                $("#payment-method-expYear").focus();
                document.getElementById("errorMsgPopup").innerHTML = "Please select card expiry year";
                return false;
            } else if (cvv == "") {
                $("#cvv").focus();
                $("#agreeMsg").html("Please enter cc code");
                document.getElementById("errorMsgPopup").innerHTML = "Please enter cc code";
                return false;
            } else if (/[0-9]/.test(cvv) == false) {
                $("#cvv").focus();
                document.getElementById("errorMsgPopup").innerHTML = "Please enter valid cvv code";
                return false;
            } else if (typeVal == "American Express" && cvv.length != 4) {
                $("#cvv").val("");
                document.getElementById("errorMsgPopup").innerHTML = "CC code should be 4 characters";
                return false
            } else if ((typeVal == "Master Card" || typeVal == "Visa") && (cvv.length != 3)) {
                $("#cvv").val("");
                document.getElementById("errorMsgPopup").innerHTML = "CC code should be 3 characters";
                return false
            } else if (cvv.indexOf('.') > -1) {
                $("#cn").val("")
                document.getElementById("errorMsgPopup").innerHTML = "Please enter valid cc code";
                return false
            } else if (address == "") {
                $("#addr").focus();
                document.getElementById("errorMsgPopup").innerHTML = "Please enter addresss";
                return false;
            } else if (cvv == "") {
                $("#cvv").focus();
                document.getElementById("errorMsgPopup").innerHTML = "Please cvv number";
                return false;
            } else if (zipCode == "" || zipCode == undefined) {
                $("#zip").focus();
                document.getElementById("errorMsgPopup").innerHTML = "Please enter ZipCode";
                return false;
            } else if (expMonth == "") {
                $("#payment-method-expMonth").focus();
                document.getElementById("errorMsgPopup").innerHTML = "Please select month";
                return false;
            } else if (expYear == "") {
                $("#payment-method-expYear").focus();
                document.getElementById("errorMsgPopup").innerHTML = "Please select year";
                return false;
            }
            //$("#container1").submit();
        });

    });

function getPaymentType(value) {

    if (value == 'American Express') {
        $("#cn").attr('maxlength', '15');
        $("#cvv").attr('maxlength', '4');
        $("#cn").val("");
        $("#cvv").val("");
    }
    if (value == 'Master Card') {
        $("#cn").attr('maxlength', '16');
        $("#cvv").attr('maxlength', '3');
        $("#cn").val("");
        $("#cvv").val("");
    }
    if (value == 'Visa') {
        $("#cn").attr('maxlength', '16');
        $("#cvv").attr('maxlength', '3');
        $("#cn").val("");
        $("#cvv").val("");
    }
}

function onlyNumberQuantity(e, t) {     
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        return false;
    }
}

function getPaymentType2(value) {

	/* <option value="visa">Visa</option>
	<option value="mastercard">Master Card</option>
	<option value="American Express">American Express</option>
	<option value="discover">Discover</option> */
	
	
    if (value == 'American Express') {
        $("#cc_number").attr('maxlength', '15');
        $("#cvv_code").attr('maxlength', '4');
        $("#cc_number").val("");
        $("#cvv_code").val("");
    }
    if (value == 'Master Card' || value == 'mastercard') {
        $("#cc_number").attr('maxlength', '16');
        $("#cvv_code").attr('maxlength', '3');
        $("#cc_number").val("");
        $("#cvv_code").val("");
    }
    if (value == 'Visa' || value == "visa") {
        $("#cc_number").attr('maxlength', '16');
        $("#cvv_code").attr('maxlength', '3');
        $("#cc_number").val("");
        $("#cvv_code").val("");
    }
}



</script>

<script type = "text/javascript" >

    <!-- handling response from Payeezy server -->
    var responseHandler = function(status, response) {
    	document.getElementById("3dspayment-errors").innerHTML = "";
        var $form = $('#payment-info-form');
        $('#someHiddenDiv').hide();
        console.log("status");
        console.log("response");
        console.log(response);

        var data1;
        if (response.token != '' && response.token != undefined && response.token != null) {
            var token = response.token.value.toString();
            var cc_number = document.getElementById("cc_number").value;
            var cvv_code = document.getElementById("cvv_code").value;
            var exp_month = document.getElementById("exp_month").value;
            var exp_year = document.getElementById("exp_year").value;
            var customerName = document.getElementById("cardholder_name").value;
            var M_token = document.getElementById("token").value;
            var ccType = document.getElementById("card_type").value;
            var payeezyMerchnatTokenJs =     $("#payeezyMerchnatTokenJs").val()
            var   payeezy3DSApiIdentifier1 = $("#payeezy3DSApiIdentifier").val();
            var   payeezy3DSApiKey1 =     $("#payeezy3DSApiKey").val();
            var   payeezy3DSOrgUnitId1=  $("#payeezy3DSOrgUnitId").val();
            var payeezy3DTaToken = $("#payeezy3DTaToken").val();
             console.log(payeezy3DSApiIdentifier1+""+payeezy3DSApiKey1)

           // data1 = '{"payeezyMerchnatToken" : ' + "\"" + M_token + "\"" + ', "payeezy_merchnat_token_js" : ' + "\"" + M_token + "\"" + ',"token" : ' + "\"" + token + "\"" + ',"expMonth" : ' + exp_month + ', "expYear" : "' + exp_year + '", "customerName" : "' + customerName + '","ccType" : "' + ccType + '", "ccNumber" : "' + cc_number + '", "ccCode" : "' + cvv_code + '"}';
           //1 data1 = '{"payeezyMerchnatToken" : ' + "\"" + M_token + "\"" + ',"token" : ' + "\"" + token + "\"" + ',"expMonth" : ' + exp_month + ', "expYear" : "' + exp_year + '", "customerName" : "' + customerName + '","ccType" : "' + ccType + '", "ccNumber" : "' + cc_number + '", "ccCode" : "' + cvv_code + '","payeezyMerchnatTokenJs" : "'+payeezyMerchnatTokenJs+'"}';
             data1 = '{"payeezy3DSApiKey" : ' + "\"" + payeezy3DSApiKey1 + "\"" + ',"payeezy3DSOrgUnitId" : ' + "\"" + payeezy3DSOrgUnitId1 + "\"" + ',"payeezy3DSApiIdentifier" : ' + "\"" + payeezy3DSApiIdentifier1 + "\"" + ',"payeezyMerchnatToken" : ' + "\"" + M_token + "\"" + ',"token" : ' + "\"" + token + "\"" + ',"expMonth" : "' + exp_month + '", "expYear" : "' + exp_year + '", "customerName" : "' + customerName + '","ccType" : "' + ccType + '", "ccNumber" : "' + cc_number + '", "ccCode" : "' + cvv_code + '","payeezyMerchnatTokenJs" : "'+payeezyMerchnatTokenJs+'","payeezy3DTaToken" : "'+payeezy3DTaToken+'"}';
             console.log("data1");
             console.log(data1);

        }
        if (status != 201) {
            if (response.error && status != 400) {
                var error = response["error"];
                var errormsg = error["messages"];
                var errorcode = JSON.stringify(errormsg[0].code, null, 4);
                var errorMessages = JSON.stringify(errormsg[0].description, null, 4);
                $('#payment-errors').html('Error Code:' + errorcode + ', Error Messages:' +
                    errorMessages);
            }
            if (status == 400 || status == 500) {
                $('#payment-errors').html('');
                var errormsg = response.Error.messages;
                var errorMessages = "";
                for (var i in errormsg) {
                    var ecode = errormsg[i].code;
                    var eMessage = errormsg[i].description;
                    errorMessages = errorMessages + 'Error Code:' + ecode + ', Error Messages:' +
                        eMessage;
                }
                $('#payment-errors').html(errorMessages);
            }
            if (response.status == "failed") {
                var errorMessages = response.Error.messages[0].description
                var errorcode = response.Error.messages[0].code;
                $('#payment-errors').html('Error Code:' + errorcode + ', Error Messages:' +
                    errorMessages);
            }
            $form.find('button').prop('disabled', false);
        } else {
            $('#payment-errors').html('');
            var result = response.token.value;
            console.log("response")
            console.log(response)
            $('#response_msg').html('Payeezy response - Token value:' + result);
            $('#response_note').html(" Note: Use this token for authorize and/or purchase transactions. For more details, visit https://developer.payeezy.com/payeezy-api/apis/post/transactions-4");
            $form.find('button').prop('disabled', false);
            $('#3DSeureModal').modal('hide');
            
            getPaymentType1(data1)
        };

    }

    var jwt='';
function getPaymentType1(data1) {

    $(".pj-preloader").css("display", "block");

	
	

    $.ajax({
        url: "3dSeure",
        type: 'POST',
        data: data1,
        contentType: 'application/json',
        success: function(data) {
            $(".pj-preloader").css("display", "none");


        	if(data!="failed" &&data!=''&& data!=null)  {

      Cardinal.configure({
                logging: {
                    level: "on"
                }
            });

      if(jwt!=''){
			 startTransaction();
		 }else{
		 jwt = data;
		
		 Cardinal
			.setup(
					"init",
					{
						jwt : jwt
					});
		
		 }
      Cardinal
		.on(
				'payments.setupComplete',
				function(
						setupCompleteData) {
					console
							.log("setupCompleteData")
					console
							.log(setupCompleteData)
					startTransaction();
				})
           /*  var jwt = data;



            Cardinal.setup("init", {
                jwt: jwt
            });


            Cardinal.on('payments.setupComplete', function(setupCompleteData) {
                // Do something

                console.log("setupCompleteData")
                console.log(setupCompleteData)
                startTransaction();
            }) */
        	
            Cardinal.on("payments.validated", function(data, jwt) {
                console.log("data")
                console.log(data)
                switch (data.ActionCode) {
                    case "SUCCESS":
                        $(".pj-preloader").css("display", "block");
                         console.log(data.Payment.ExtendedData.CAVV)
                        $.ajax({
                            url: "3dSeureFinalStage&Cavv=" + data.Payment.ExtendedData.CAVV,
                            type: 'POST',
                            data: data1,
                            contentType: 'application/json',
                            success: function(data) {
                            	
                                $(".pj-preloader").css("display", "none");

                            	
                            	if(data =='Success') {
                            	window.location.reload();
                            	}
                            	else{
                            		alert(data+"failed");
                                	window.location.reload();

                            	}
                            	
                            	
                            },
                            error: function() {}

                        })

                        break;

                    case "NOACTION":
                        // Handle no actionable outcome
                        alert("NOACTION")
                        window.location.reload();

                        break;

                    case "FAILURE":
                        // Handle failed transaction attempt
                        alert("FAILURE")
                        window.location.reload();
                        break;

                    case "ERROR":
                        // Handle service level error
                        alert("You Entered Wrong Creadentials for Payeezy 3D ApiKey or Payeezy 3D ApiIdentifier or Payeezy 3D OrgUnitID")
                        window.location.reload();
                        break;
                }
            });

        }else document.getElementById("3dspayment-errors").innerHTML = "Please Enter Valid Payeezy Merchant Token";
    
            function startTransaction() {
            	
                var cc_number = document.getElementById("cc_number").value;
                var cvv_code = document.getElementById("cvv_code").value;
                var exp_month = document.getElementById("exp_month").value;
                var exp_year = document.getElementById("exp_year").value;
                var customerName = document.getElementById("cardholder_name").value;
                var ccType = document.getElementById("card_type").value;
                //var  year = "20"+exp_year;
                var  year = "2020";
                  console.log("year===========================");

                console.log(year);
                

            	
                Cardinal.start("cca", {
                    OrderDetails: {
                        OrderNumber: Math.random(0, 1000000) + "-shzs", // OrderNumbers need to be unique
                        Amount: "500",
                        CurrencyCode: " 840"
                    },
                    Consumer: {
                        Account: {
                            AccountNumber: cc_number,
                            ExpirationMonth: exp_month,
                            ExpirationYear: "2022",
                            CardCode: cvv_code, // Required for Tokenization
                            NameOnAccount: customerName
                        }
                    }
                });
            }

        },
        error: function() {}
    });

}


<!-- Building JSON resquest and submitting request to Payeezy sever -->
jQuery(function($) {
    $('#payment-info-form').submit(function(e) {
    	


    	
    	
  	    var cardNumber = document.getElementById("cc_number").value;
        var cvv = document.getElementById("cvv_code").value;
        var expMonth = document.getElementById("exp_month").value;
        var expYear = document.getElementById("exp_year").value;
        var customerName = document.getElementById("cardholder_name").value;
        var ccType = document.getElementById("card_type").value;
    

        
        if (customerName == "") {
            $("#cardholder_name").focus();
            document.getElementById("payment-errors").innerHTML = "Please Enter name";
            return false;
        } else if (ccType == "") {
            $("#card_type").focus();
            document.getElementById("payment-errors").innerHTML = "Please select card type";
            return false;
        } else if (ccType == "Select Card") {
            $("#card_type").focus();
            document.getElementById("payment-errors").innerHTML = "Please select card type";
            return false
        } else if (cardNumber == "") {
            $("#cc_number").focus();
            document.getElementById("payment-errors").innerHTML = "Please enter card number";
            return false;
        } else if (ccType == "American Express" && cardNumber.length != 15) {
            $("#cc_number").val("")
            document.getElementById("payment-errors").innerHTML = "Card number should be 15 characters";
            return false
        } else if ((ccType == "mastercard" || ccType == "visa") && (cardNumber.length != 16)) {
            $("#cc_number").val("")
            document.getElementById("payment-errors").innerHTML = "Card number should be 16 characters";
            return false
        } else if (cardNumber.indexOf('.') > -1) {
            $("#cc_number").val("")
            document.getElementById("payment-errors").innerHTML = "Please enter valid card number";
            return false
        } else if (expMonth == "Month") {
            $("#exp_month").focus();
            document.getElementById("payment-errors").innerHTML = "Please select card expiry month";
            return false;
        } else if (expYear == "Year") {
            $("#exp_year").focus();
            document.getElementById("payment-errors").innerHTML = "Please select card expiry year";
            return false;
        } else if (cvv == "") {
            $("#cvv_code").focus();
            $("#agreeMsg").html("Please enter cc code");
            document.getElementById("payment-errors").innerHTML = "Please enter cc code";
            return false;
        } else if (/[0-9]/.test(cvv) == false) {
            $("#cvv_code").focus();
            document.getElementById("payment-errors").innerHTML = "Please enter valid cvv code";
            return false;
        } else if (ccType == "American Express" && cvv.length != 4) {
            $("#cvv_code").val("");
            document.getElementById("payment-errors").innerHTML = "CC code should be 4 characters";
            return false
        } else if ((ccType == "mastercard" || ccType == "visa") && (cvv.length != 3)) {
            $("#cvv_code").val("");
            document.getElementById("payment-errors").innerHTML = "CC code should be 3 characters";
            return false
        } else if (cvv.indexOf('.') > -1) {
            $("#cc_number").val("")
            document.getElementById("payment-errors").innerHTML = "Please enter valid cc code";
            return false
        }  else if (expMonth == "") {
            $("#exp_month").focus();
            document.getElementById("payment-errors").innerHTML = "Please select month";
            return false;
        } else if (expYear == "") {
            $("#exp_year").focus();
            document.getElementById("payment-errors").innerHTML = "Please select year";
            return false;
        }
    	
    	
    	console.log("payment-info-form");
        $('#response_msg').html('');
        $('#response_note').html('');
        $('#payment-errors').html('');
        var $form = $(this);
        $form.find('button').prop('disabled', true);
        var apiKey =  $("#payeezy3DSApiKey").val();
        var js_security_key = $("#payeezyMerchnatTokenJs").val();
        var ta_token =  $("#payeezy3DTaToken").val();
        Payeezy.setApiKey(apiKey);
        Payeezy.setJs_Security_Key(js_security_key);
        Payeezy.createToken(responseHandler);
               
        $('#someHiddenDiv').show();
        return false;
    });
});
 </script>
 
 <script>
$(document).ready(function(){
 
	  $("#iGateUserName").val("");
	  $("#iGatePassword").val("");
	  $("#firstDataPassword").val("");

});
</script>
</body>
</html>

	
</body>
</html> 
