<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="true"%>
<!doctype html>
<html class="no-js" lang="en">
  <head>
  
    <title>FoodKonnekt | Dashboard</title>
    <!--CALLING STYLESHEET STYE.CSS-->
    <link rel="stylesheet" href="resources/css/style.css">
    <!--CALLING STYLESHEET STYLE.CSS-->
    
    <!--CALLING GOOGLE FONT OPEN SANS-->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    <!--CALLING GOOGLE FONT OPEN SANS-->
    
    <!--CALLING FONT AWESOME-->
    <link rel="stylesheet" href="resources/css/font-awesome.css">
    <!--CALLING FONT AWESOME-->
    
    <!--CALLING FILTER OPTIONS-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/jquery.dataTables.min.js"></script>
    <script src="https://www.jqueryscript.net/demo/Powerful-jQuery-Data-Table-Column-Filter-Plugin-yadcf/jquery.dataTables.yadcf.js"></script>
    <!--OPENS DIALOG BOX-->
    <link rel="stylesheet" type="text/css" href="resources/css/dialog-box/component.css" />
    <!--OPENS DIALOG BOX-->
    <style>
    .errorClass { border:  1px solid red; }
    </style>
    <style>
/* The container */
.radiocontainer {
    display: block;
    position: relative;
    padding-left: 35px;
    margin-bottom: 12px;
    cursor: pointer;
    font-size: 18px;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
}

/* Hide the browser's default radio button */
.radiocontainer input {
    position: absolute;
    opacity: 0;
    cursor: pointer;
}

/* Create a custom radio button */
.checkmark {
    position: absolute;
    top: 0;
    left: 0;
    height: 25px;
    width: 25px;
    background-color: #eee;
    border-radius: 50%;
}

/* On mouse-over, add a grey background color */
.radiocontainer:hover input ~ .checkmark {
    background-color: #ccc;
}

/* When the radio button is checked, add a blue background */
.radiocontainer input:checked ~ .checkmark {
    background-color: orange;
}

/* Create the indicator (the dot/circle - hidden when not checked) */
.checkmark:after {
    content: "";
    position: absolute;
    display: none;
}

/* Show the indicator (dot/circle) when checked */
.radiocontainer input:checked ~ .checkmark:after {
    display: block;
}

/* Style the indicator (dot/circle) */
.radiocontainer .checkmark:after {
 	top: 9px;
	left: 9px;
	width: 8px;
	height: 8px;
	border-radius: 50%;
	background: white;
}

.pj-preloader {
    display: none;
    position: absolute;
    height: 383px;;
    width: 940px;
    background: url("resources/img/spinner.gif") no-repeat scroll center center rgba(153, 153, 153, 0.3);
    z-index: 9999;
    left: 260;
    position: absolute;
    top: 224px;
}
</style>
  </head>
  <body>
    <div id="page-container">
        <div class="foodkonnekt merchant">
            <div class="inner-container">
                <div class="max-row">
                    
                    <header id="page-header">
                        <div class="inner-header">
                            <div class="row">
                                <div class="logo">
                                     <a href="adminHome" title="FoodKonnekt Dashboard" class="logo"><img src="resources/img/foodkonnekt-logo.png"></a>
                                </div><!--.logo-->
                                   <%@ include file="adminHeader.jsp"%>
                            </div><!--.row-->
                        </div><!--.inner-header-->
                    </header><!--#page-header-->
                    
                    <div id="page-content">
                        <div class="outer-container">
                            <div class="row">
                            <div class="content-inner-container">
                            <%@ include file="leftMenu.jsp"%>    
                            <div class="right-content-container">
                            <div class="right-content-inner-container">
                                        
                                            <div class="content-header">
                                                <div class="all-header-title">
                                                </div><!--.header-title-->
                                                <div class="content-header-dropdown">
                                                </div><!--.content-header-dropdown-->
                                            </div><!--.content-header-->
                                            
                                            <div class="merchant-page-data">
                                                <div class="merchant-actions-outbound">
                                                    <div class="merchat-coupons-container">

                                                        <%@ include file="adminMerchantMenu.jsp"%>
                                                      
                                                        <div class="delivery-zones-content-container">
                                                            
                                                            <span id="errorMsg" style="color:red;font-size: 16px;"></span> 
                                                            <form:form method="POST" action="updateTaxRates"
																			modelAttribute="taxRates" id="taxRates">
                                                                              
                                                                              <form:hidden path="id" value="${taxRates.id}" id="taxRates_id"/>
                                                                              <form:hidden path="isDefault" value="${taxRates.isDefault}" id="taxRates_isDefault"/>
                                                                              <form:hidden path="merchant.id" value="${taxRates.merchant.id}" id="taxRates_merchant"/>
                                                                              
                                                                              <c:if test="${taxRates.posTaxRateId != null}">
                                                                                 <form:hidden path="posTaxRateId" value="${taxRates.posTaxRateId}" id="taxRates_posTaxRateId"/>
                                                                              </c:if>
                                                                              
                                                                              <div class="adding-products">
																				<label id="errorMessage" style="color: red;"></label>
																				<div class="adding-products-form">
																					
																					<div class="clearfix"></div>
																					<label>Name:</label>
																					  <form:input path="name" id="taxName"
																						placeholder="name" value="${taxRates.name}"/>
																					
																					<br><br><br>
																					<label>Tax Rate:</label>
																					<form:input path="rate" type="number" id="taxPrice" min="0"
                                        												maxlength="5" value="${taxRates.rate}"
                                        												onkeypress="return isNumberKey(event)" onblur="currectNo(event)"/>
																					
																					<br><br>
																					<div class="button left"> 
																				<input type="button" id="updateButton" style="margin-top: 50px;"
																					value="Save">&nbsp;&nbsp; 
																				<input type="button" value="Cancel" style="margin-top: 50px;"
																					id="cancelButton">
																				

																			</div>
																					
																			</div></div>
                                                                              </form:form>
                                                            
                                                        </div><!--.coupons-content-container-->
                                                       <div id="errorDiv" style="color:red"></div> 
                                                    </div><!--.merchat-coupons-container-->
                                                </div><!--.merchant-actions-outbound-->
                                            </div><!--.merchant-page-data-->
                                            
                                        </div><!--.right-content-inner-container-->
                            </div>
                            </div>
                             											
                            </div><!--.row-->
                            
                        </div><!--.outer-container-->
                        
                    </div><!--#page-content-->
                             <%@ include file="adminFooter.jsp"%>                    
                </div><!--.max-row-->
            </div><!--.inner-container-->
        </div><!--.foodkonnekt .dashboard-->
    </div><!--#page-container-->
    
    <script>
    </script>
    <!--OPENS DIALOG BOX-->
      <script type="text/javascript">
      
	$(document).ready(function() {
		$("#updateButton").click(function() {
			var taxName= $("#taxName").val();
			var taxPrice= $("#taxPrice").val();
			
			if((taxName=="" || taxName==undefined)){
				document.getElementById("errorMsg").innerHTML = "Please enter TaxName";
			
			}else if((taxPrice=="" || taxPrice==undefined)){
				document.getElementById("errorMsg").innerHTML = "Please enter TaxRate";
		   
			}else{
			    $("#taxRates").val();
			    $("#taxRates").submit();
			}
		});

		$("#cancelButton").click(function() {
			window.location = "getTaxesByMerchantId"
		});
		
	});
	
	function isNumberKey(evt){
  	  var target = evt.target || evt.srcElement; // IE

  	    var id = target.id;
  	    var data=document.getElementById(id).value;
  	  
  	  
  	  if(evt.which == 8 || evt.which == 0){
            return true;
        }
        if(evt.which < 46 || evt.which > 59) {
            return false;
            //event.preventDefault();
        } // prevent if not number/dot

        if(evt.which == 46 && data.indexOf('.') != -1) {
            return false;
            //event.preventDefault();
        }
    }
    
    function currectNo(evt){
  	  var target = evt.target || evt.srcElement; // IE

	    var id = target.id;
	    var data=document.getElementById(id).value;
	    
	    var res=data.split(".");
  	  
  	  if(res.length==2){
  		if(res[1]){
  			
  		}else{
  			document.getElementById(id).value=res[0]+".0";
  		}
  	  }
    }
	</script>
  </body>
</html>
