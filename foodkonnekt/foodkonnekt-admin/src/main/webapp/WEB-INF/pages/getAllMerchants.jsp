<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page session="true"%>
<%@page import="java.util.Random" %>
<!doctype html>
<html class="no-js" lang="en">
<head>
    <title>FoodKonnekt | Dashboard</title>
    <!--CALLING STYLESHEET STYE.CSS-->
<link rel="stylesheet" href="resources/css/style.css">
    <!--CALLING STYLESHEET STYLE.CSS-->

    <!--CALLING GOOGLE FONT OPEN SANS-->
    <link
            href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic'
            rel='stylesheet' type='text/css'>
    <!--CALLING GOOGLE FONT OPEN SANS-->

    <!--CALLING FONT AWESOME-->
    <link rel="stylesheet" href="resources/css/font-awesome.css">
    <!--CALLING FONT AWESOME-->

    <!--CALENDAR MULTI-SELECT-->
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css">
    <link href="resources/css/calendar/jquery.comiseo.daterangepicker.css" rel="stylesheet" type="text/css">
    <!--CALENDAR MULTI-SELECT-->

    <!--CHECK ALL LIST TABLE-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="resources/js/checkall/jquery.checkall.js"></script>
    <!--CHECK ALL LIST TABLE-->

    <!--OPENS DIALOG BOX-->
    <link rel="stylesheet" type="text/css" href="resources/css/dialog-box/component.css" />
    <!--OPENS DIALOG BOX-->

    <!--ACCORDION FOR MENU-->
    <script src="resources/js/accordion/paccordion.js"></script>
    <!--ACCORDION FOR MENU-->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css">
    <style type="text/css">
        div#example_paginate {
            display: block;
        }

        div#example_filter {
            display: block;
        }

        div#example_length {
            display: block;
        }

        input[type="search"] {
            max-width: 300px;
            width: 100%;
            outline: 0;
            border: 1px solid rgb(169, 169, 169);
            padding: 11px 10px;
            border-radius: 6px;
            margin-bottom: 7px;
            display: none;
        }
        .right-content-container{
            width: 1100px;
            float: left;
            margin-left: 5%;
        }

        .pj-preloader {
            display: none;
            position: absolute;
            height: 383px;;
            width: 940px;
            background: url("resources/img/spinner.gif") no-repeat scroll center center rgba(153, 153, 153, 0.3);
            z-index: 9999;
            left: 260;
            position: absolute;
            top: 224px;
        }

    </style>
</head>
<body>
<div id="page-container">
    <div class="foodkonnekt orders">
        <div class="inner-container">
            <div class="max-row">

                <header id="page-header">
                    <div class="inner-header">
                        <div class="row">
                            <div class="logo">
                                <a href="adminHome" title="FoodKonnekt Dashboard" class="logo"><img
                                        src="resources/img/foodkonnekt-logo.png"></a>
                            </div>
                            <!--.logo-->
                            <%-- <%@ include file="adminHeader.jsp"%> --%>
                            <div class="header-nav">
                                <input type= "hidden" value = "${adminUrl}" id="adminUrl">
                                <nav class="header-nav-container">
                                    <ul>

                                        <!-- <li><a href="#"><i class="fa fa-user" aria-hidden="true"></i> My Account</a> |</li> -->
                                        <li><select onchange="location = this.options[this.selectedIndex].value;">
                                            <option>Change Dashboard</option>
                                            <option value="getAllMerchants">FoodKonnekt</option>
                                            <option value="kritiqMerchantDetail">Kritiq</option>
                                            <option value="DownloadAppVersion">DownloadUpdatedVersion</option>
                                            <option value="DownloadCurrentAndUpdateVersion">DownloadUpdatedVersionAndCurrentVersion</option>
                                        </select></li>|
                                        <li><a href="adminLogout"><i class="fa fa-power-off" aria-hidden="true"></i> Log Out</a></li>
                                    </ul>
                                </nav>
                                <!--.header-nav-container-->
                            </div>
                        </div>
                        <!--.row-->
                    </div>
                    <!--.inner-header-->
                </header>
                <!--#page-header-->

                <div id="page-content">
                    <div class="outer-container">

                        <div class="row">
                            <div class="content-inner-container">
                                <%-- <%@ include file="leftMenu.jsp"%> --%>
                                <div class="right-content-container">
                                    <div class="right-content-inner-container">
                                        <div class="content-header">
                                            <div class="all-header-title"></div>
                                            <!--.header-title-->
                                            <div class="content-header-dropdown"></div>
                                            <!--.content-header-dropdown-->
                                        </div>
                                        <!--.content-header-->
                                        <div class="orders-page-data">

                                            <div class="sd-orders-list-outbound">
                                                <div class="sd-orders-list">
                                                    <div class="sd-orders-list-inbound">
                                                        <div class="orders-items-list-table">
                                                            <div class="search-container" style="margin-top: 10px">
                                                                <div class="only-search-elements">
                                                                    <label>Search</label> <input type="text"
                                                                                                 placeholder="Search Merchants"
                                                                                                 id="search-inventory" class="searchq">
                                                                    <input type="button" value="Search">
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <br>
                                                        <%-- <div> <form:form action="getAllMerchants" align="center" method="GET" modelAttribute="SearchVO" id="businessLogo" enctype="multipart/form-data">
                                                         FROM &nbsp&nbsp&nbsp
                                                         <input type="date" name="startDate" path="startDate" value="${SearchVO.startDate}">
                                                          &nbsp&nbsp&nbsp TO &nbsp&nbsp&nbsp
                                                         <input type="date" name="endDate" path="endDate" value="${SearchVO.endDate}">&nbsp&nbsp
                                                         <!-- <input type="submit" value="Submit" /> -->
                                                         <div class="button left" style="margin-left: 78%;margin-top: -5%;">

                                                         <input type="submit" value="Submit"  style="width: 86%;">
                                                         </div>

                                                         </form:form></div> --%>


                                                        <div style="margin-left: -30%;"> <form:form
                                                                action="getAllMerchants?viewType=${viewType}"
                                                                align="center"
                                                                method="GET" modelAttribute="SearchVO" id="businessLogo" enctype="multipart/form-data">


                                                            FROM &nbsp&nbsp&nbsp
                                                            <input type="date" name="startDate" path="startDate" value="${SearchVO.startDate}" style="width: 22%;">
                                                            &nbsp&nbsp&nbsp TO &nbsp&nbsp&nbsp
                                                            <input type="date" path="endDate" name="endDate" value="${SearchVO.endDate}" style="width: 22%;">&nbsp&nbsp

                                                            <div class="button left" style="margin-left: 78%;margin-top: -4.2%;">

                                                                <input type=submit value="Submit"  style="width: 86%;">


                                                            </div>
                                                            <div class="button left" style="margin-left: 91%;margin-top: -3.5%;">
                                                                <select id='mrcStatus' >
                                                                    <option value="Installed">Installed</option>
                                                                    <option value="Un-Installed">Un-Installed</option>
                                                                    <option value="In Process">In Process</option>
                                                                </select>
                                                            </div>
                                                        </form:form>
                                                        </div>




                                                        <!--.inventory-items-list-table-->
                                                        <div class="pj-preloader"></div>
                                                        <table id="checkit-table" class="table header-table-orders">
                                                            <thead>
                                                            <tr>
                                                                <th>Merchant Name</th>
                                                                <th>Phone NO</th>
                                                                <th>Email</th>
                                                                <c:if test = "${viewType != 'Basic'}">
                                                                    <th>Total Orders</th>
                                                                    <th>Cash Order</th>
                                                                    <th>Credit Order</th>
                                                                    <th>Pick Up</th>
                                                                    <th>Delivery</th>
                                                                </c:if>
                                                                <th>POS Name</th>
                                                                <th>Status</th>
                                                                <c:if test = "${viewType != 'Basic'}">
                                                                    <th>Subscription</th>
                                                                </c:if>
                                                                <th>CreatedOn</th>
                                                                <th>View Admin</th>
                                                                <th>Allow Multipay</th>
                                                                <th>Action</th>

                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <c:forEach items="${allMerchants}" var="view"
                                                                       varStatus="status">
                                                                <c:if test="${view.name!='FoodkonnektAdmin'}">
                                                                    <tr>
                                                                        <td align="center"><p>${view.name}</p></td>
                                                                        <td align="center"><p>${view.phoneNumber}</p></td>
                                                                        <td align="center"><p>${view.owner.email}</p></td>
                                                                        <c:if test = "${viewType != 'Basic'}">
                                                                            <td align="center"><p>${view.totalOrders}</p></td>

                                                                            <td align="center"><p>${view.cashOrder}</p></td>
                                                                            <td align="center"><p>${view.creditOrder}</p></td>
                                                                            <td align="center"><p>${view.pickup}</p></td>
                                                                            <td align="center"><p>${view.delivery}</p></td>
                                                                        </c:if>
                                                                        <td align="center"><p>${view.owner.pos.name}</p></td>
                                                                        <td align="center"><p>${view.status}</p></td>
                                                                        <c:if test = "${viewType != 'Basic'}">

                                                                            <c:choose>
                                                                                <c:when test="${fn:length(view.merchantSubscriptions) gt 0}">
                                                                                    <c:forEach items="${view.merchantSubscriptions}" var="viewSubs"
                                                                                               varStatus="status">
                                                                                        <td align="center"><p>${viewSubs.subscription.subscriptionPlan}</p></td>
                                                                                    </c:forEach>
                                                                                </c:when>
                                                                                <c:otherwise>

                                                                                    <td align="center"><p>NA</p></td>


                                                                                </c:otherwise>
                                                                            </c:choose>
                                                                        </c:if>
                                                                        <c:choose>
                                                                            <c:when test="${view.createdDate!=null and view.createdDate!=''}">
                                                                                <td align="center"><p>${view.createdDate}</p></td>
                                                                            </c:when>
                                                                            <c:otherwise>
                                                                                <td align="center"><p>NA</p></td>
                                                                            </c:otherwise>
                                                                        </c:choose>
                                                                        <c:choose>
                                                                            <c:when test="${view.owner.pos.name=='Clover' and view.isInstall==1}">
                                                                                <td align="center"><a target="_blank" href="${adminUrl}/existingMerchant?merchant_id=${view.posMerchantId}&employee_id=T03TVG9M6C7PG&client_id=SMA6T1DNS8YAE&access_token=${view.accessToken}">View</a></td>
                                                                            </c:when>
                                                                            <c:otherwise>
                                                                                <c:choose>
                                                                                    <c:when test="${view.isInstall==1}">
                                                                                        <%-- <td align="center"><a target="_blank" href="${adminUrl}/installFoodkonnekt?location_uid=${view.merchantUid}">View</a></td> --%>
                                                                                        <td align="center"><a id="newLink" href="#" onclick="linkSet('${view.merchantUid}')">View</a></td>
                                                                                    </c:when>
                                                                                    <c:otherwise>
                                                                                        <td align="center"><p>NA</p></td>
                                                                                    </c:otherwise>
                                                                                </c:choose>

                                                                            </c:otherwise>
                                                                        </c:choose>



                                                                        <td align="center"><p>${view.action}</p></td>
                                                                             <c:choose>
                                                                           <c:when test="${view.owner.pos.posId==1}">
                                                                           <td align="center" id="uninstall" ><p><button onclick="uninstallMerchant('${view.posMerchantId}',true)">Uninstall</button></p></td>
                                                                          </c:when>
                                                                          <c:otherwise>
                                                                          <td align="center" id="uninstall" ><p><button onclick="uninstallMerchant('${view.merchantUid}',false)">Uninstall</button></p></td>
                                                                          </c:otherwise>
                                                                          </c:choose>
                                                                    </tr>
                                                                </c:if>
                                                            </c:forEach>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <!--.sd-orders-list-inbound-->
                                                </div>
                                                <!--.sd-orders-list-->
                                            </div>
                                            <!--.sd-orders-list-outbound-->

                                        </div>
                                        <!--.orders-page-data-->


                                    </div>
                                    <!--.right-content-inner-container-->
                                </div>
                                <!--.right-content-container-->
                            </div>
                            <!--.content-inner-container-->
                        </div>
                        <!--.row-->

                    </div>
                    <!--.outer-container-->
                </div>
                <!--#page-content-->

                <%@ include file="adminFooter.jsp"%>
                <!--#footer-container-->

            </div>
            <!--.max-row-->
        </div>
        <!--.inner-container-->
    </div>
    <!--.foodkonnekt .dashboard-->
</div>
<!--#page-container-->


<script>

    $(document).ready(function() {


        $("#checkit-table").on("click", ".nav-toggle", function(){
            var merchantId=$(this).attr('itmId');
            var thishtml = jQuery(this).html();
            var itemStatus=false;

            if(thishtml =='Enable MultiPay'){
                itemStatus=true;

            }
            if(thishtml =='Disable MultiPay'){
                itemStatus=false;

            }

            $('.pj-preloader').css('display','block');
            $.ajax({
                type: 'GET',
                url: "getDisabledEnabledMutltiPayValue?id="+merchantId+"&multiPayValue="+itemStatus,
                success:function(data){

                    if(data){
                        $("#nav_"+merchantId).html('Disable MultiPay');
                    }else{
                        if(itemStatus){
                            alert("Not eligible for multipay ");
                        }
                        $("#nav_"+merchantId).html('Enable MultiPay');
                    }
                    $('.pj-preloader').css('display','none');
                }
            });

        });
    });

    $(document).ready(function() {
        $('#checkit-table').DataTable({
            "order" : [ [ 1, "desc" ] ]
        });
    });

    /* Custom filtering function which will search data in column four between two values */
    if ($.fn.dataTable)
    $.fn.dataTable.ext.search.push(
        function( settings, data, dataIndex ) {
            var min = parseInt( $('#min').val(), 10 );
            var max = parseInt( $('#max').val(), 10 );
            var age = parseFloat( data[9] ) || 0; // use data for the age column

            if ( ( isNaN( min ) && isNaN( max ) ) ||
                ( isNaN( min ) && age <= max ) ||
                ( min <= age   && isNaN( max ) ) ||
                ( min <= age   && age <= max ) )
            {
                return true;
            }
            return false;
        }
    );

    $(document).ready(function() {
        var table = $('#checkit-table').DataTable();

        // Event listener to the two range filtering inputs to redraw on input
        $('#min, #max').keyup( function() {
            table.draw();
        } );
    } );
</script>
<script type="text/javascript">
    var _gaq = _gaq || [];
    _gaq.push([ '_setAccount', 'UA-36251023-1' ]);
    _gaq.push([ '_setDomainName', 'jqueryscript.net' ]);
    _gaq.push([ '_trackPageview' ]);

    (function() {
        var ga = document.createElement('script');
        ga.type = 'text/javascript';
        ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl'
            : 'http://www')
            + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(ga, s);
    })();
</script>
<!--CALENDAR MULTI-SELECT-->

<!--OPENS DIALOG BOX-->
<script src="resources/js/dialog-box/classie.js"></script>
<script src="resources/js/dialog-box/modalEffects.js"></script>
<script>
    var polyfilter_scriptpath = '/js/';
</script>
<!--OPENS DIALOG BOX-->

<script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        var table = $('#checkit-table').DataTable();
        var mrcStatus =  $('#mrcStatus').val();
        //alert(mrcStatus);
        table.column(9).search("^"+mrcStatus+"$",15, false, false).draw();
        var viewTypeVar = '${viewType}';
        console.log( viewTypeVar);
        if(viewTypeVar=== 'Basic')table.column(4).search("^"+mrcStatus+"$",15, false, false).draw();

        $(".searchq").keyup(function() {
            //alert(this.value);
            table.search( this.value).draw();
        } );
        $('#mrcStatus').change( function() {
            var mrcStatus =  $('#mrcStatus').val();
            //	alert(mrcStatus);
            table.column(9).search("^"+mrcStatus+"$",15, true, true).draw();
            if(viewTypeVar=== 'Basic')table.column(4).search("^"+mrcStatus+"$",15, false, false).draw();

            //table.fnFilter("^"+$('#mrcStatus').val()+"$", 9, false, false);
        });
    });
    $(document).ready(function(){
        $('input[type=search]').each(function(){
            $(this).attr('placeholder', "Search");
        });
    });
    $("#generateButton").click(function () {
        $("#searchForm").submit();
    });
</script>


<script>
    $(function(){
        $('label').each(function(){
            if($(this).text()=='Search:'){
                $(this).text('');
            }
        })
    })

    function linkSet(merchantUid){
        var adminUrl = $("#adminUrl").val();
        <%
     Random rand = new Random();
     int n = rand.nextInt(900000) + 100000;
     String text1=Integer.toString(n);
     System.out.println(n);
     session.setAttribute("test",text1);
     session.setMaxInactiveInterval(1800);
        %>
        var scopeKey = "<%=session.getAttribute("test")%>"


        var locationUid =merchantUid;
        window.open(adminUrl+"/installFoodkonnekt?location_uid="+locationUid+"&scopeKey="+scopeKey+"&viewType=Basic");
        /* var url="/foodkonnekt-admin/installFoodkonnekt?location_uid="+locationUid+"&scopeKey="+scopeKey;
        document.getElementById('newLink').href = url; */
    }

function uninstallMerchant(merchantUid ,isClover){
        $.ajax({
            type: 'GET',
        url: "uninstallMerchant?merchantUid="+merchantUid+"&isClover="+isClover,
            success:function(data){
                if(data){
                    console.log(data);
                    alert("Merchant successfully uninstalled");
                    location.reload(true);
                }
            }
        });
    }
</script>
</body>
</html>
