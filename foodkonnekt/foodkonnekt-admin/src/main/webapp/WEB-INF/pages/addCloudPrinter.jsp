<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@page session="true"%>
<!doctype html>
<html class="no-js" lang="en">
<head>
<title>FoodKonnekt | Add Products</title>
<link href="resources/css/bootstrap.min.css" rel="stylesheet"
	type="text/css" />
<link href="resources/css/bootstrap-multiselect.css" rel="stylesheet"
	type="text/css" />
<!--CALLING STYLESHEET STYE.CSS-->
<link rel="stylesheet" href="resources/css/style.css">
<!--CALLING STYLESHEET STYLE.CSS-->

<!--CALLING GOOGLE FONT OPEN SANS-->
<link
	href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic'
	rel='stylesheet' type='text/css'>
<!--CALLING GOOGLE FONT OPEN SANS-->

<!--CALLING FONT AWESOME-->
<link rel="stylesheet" href="resources/css/font-awesome.css">
<!--CALLING FONT AWESOME-->

<!--OPENS DIALOG BOX-->
<link rel="stylesheet" type="text/css"
	href="resources/css/dialog-box/component.css" />
<!--OPENS DIALOG BOX-->

<!--CALLING PRODUCTS TABS-->
<link rel='stylesheet' type='text/css'
	href='resources/css/products-tabs/opentabby.css' />
<!--CALLING PRODUCTS TABS-->
<!-- <link rel="stylesheet" type="text/css"
	href="https://cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css"> -->

<style type="text/css">
.body {
	overflow: scroll !important;
}



.pj-preloader {
	display: none;
	position: absolute;
	height: 383px;;
	width: 940px;
	background: url("resources/img/spinner.gif") no-repeat scroll center
		center rgba(153, 153, 153, 0.3);
	z-index: 9999;
	left: 260;
	position: absolute;
	top: 224px;
}

table{
margin : 2em;
}

#addBtn{
margin-left:2em; 
margin-bottom:2em;
 margin-top:2em;
}
</style>

</head>
<body>
	<div id="page-container">
		<div class="foodkonnekt inventory">
			<div class="inner-container">
				<div class="max-row">

					<header id="page-header">
						<div class="inner-header">
							<div class="row">

								<div class="logo">
									<a href="adminHome" title="FoodKonnekt Dashboard" class="logo"><img
										src="resources/img/foodkonnekt-logo.png"></a>
								</div>
								<!--.logo-->
								<%@ include file="adminHeader.jsp"%>

							</div>
							<!--.row-->
						</div>
						<!--.inner-header-->
					</header>
					<!--#page-header-->

           		<div id="page-content">
						<div class="outer-container">

							<div class="row">
								<div class="content-inner-container">

									<%@ include file="leftMenu.jsp"%>

									<div class="right-content-container">
										<div class="right-content-inner-container">

											<div class="content-header">
												<div class="all-header-title"></div>
												<!--.header-title-->
											</div>
											<!--.content-header-->
												<div class="merchant-actions-outbound">
													<div class="merchat-coupons-container">
														<div class="coupons-navigation">
															<ul>
																<li><a href="onLineOrderLink">Location</a></li>
																<li><a href="deliveryZones">Delivery Zones</a></li>
																
																 <!--<li><a href="vouchars">Coupons</a></li>-->
																<li><a href="customers">Customers</a></li>
																
																	<li class="current-menu-item"><a
																		href="setupPaymentGateway?adminPanel=1">Gateway</a></li>
																	<li><a href="notificationMethod">Notifications</a></li>
																
																<li><a href="getTaxesByMerchantId">Taxes</a></li>
																<li><a
																	href="getMerchantSliders?merchantId=301">Sliders</a></li>
																	<li><a href="addCloudPrinter">Printer</a></li>
															</ul>
														</div>
														<!--.coupons-navigation-->

											
											
                                             <div class="clearfix"></div>
                                  
											<div class="button left"  id="addBtn">
												<a href="/foodkonnekt-admin/getAuthcode">Add Printer</a>
											
									  </div>
									  
					  <table width="100%" cellpadding="0" cellspacing="0" id="example">
							<thead>
								 <tr>
									<th>Printer Id</th>
									<th>Printer Name</th>
									<th>Active</th>
								</tr>
							</thead>	
						
					     <tr>
					       <td>${printer.printerId}</td>
					       <td>${printer.printerName} </td>
					       <td> <a href="/foodkonnekt-admin/updatePrinterActiveValue?pid=${printer.id}">
					         ${printer.isActive==1?'Active':'InActive'} </a> </td>
					     </tr>
			              
					</table>		
							
						
							
									  
									  
</div>
							<!--.row-->

						</div>
						<!--.outer-container-->

					</div>
					<!--#page-content-->
					<%@ include file="adminFooter.jsp"%>
				</div>
				<!--.max-row-->
			</div>
			<!--.inner-container-->
		</div>
		<!--.foodkonnekt .dashboard-->
	</div>
	<!--#page-container-->
</body>
</html>