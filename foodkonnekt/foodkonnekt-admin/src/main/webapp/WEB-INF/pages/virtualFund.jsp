<%@page session="true"%>
<!doctype html>
<html class="no-js" lang="en">
    <!--CALLING STYLESHEET STYE.CSS-->
    <link rel="stylesheet" href="resources/css/style.css"><%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="true"%>
  <head>
    <title>FoodKonnekt | Dashboard</title>
    <!--CALLING STYLESHEET STYE.CSS-->
    <link rel="stylesheet" href="resources/css/style.css">
    <!--CALLING STYLESHEET STYLE.CSS-->

    <!--CALLING GOOGLE FONT OPEN SANS-->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    <!--CALLING GOOGLE FONT OPEN SANS-->

    <!--CALLING FONT AWESOME-->
    <link rel="stylesheet" href="resources/css/font-awesome.css">
    <!--CALLING FONT AWESOME-->

    <!--CALLING CHECK ALL FUNCTIONALITY-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="resources/js/checkall/jquery.checkall.js"></script>
    <!--CALLING CHECK ALL FUNCTIONALITY-->

    <!--OPENS DIALOG BOX-->
    <link rel="stylesheet" type="text/css" href="resources/css/dialog-box/component.css" />
    <!--OPENS DIALOG BOX-->

    <!--ACCORDION FOR MENU-->
    <script src="resources/js/accordion/paccordion.js"></script>
    <!--ACCORDION FOR MENU-->
     <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css">
     <script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
       <style type="text/css">

 .link
  {
     color:#0c58ca;
     text-decoration: none;
     background-color: none;
  }

      div#example_paginate {
        display: block;
       }
      div#example_filter {
         display: none;
      }
      div#example_length {
         display: block;
      }

      div#example1_filter {
         display: none;
      }

      example1_filter

      input[type="search"]{
        display:none;
        max-width: 300px;
        width: 100%;
        outline: 0;
        border: 1px solid rgb(169, 169, 169);
        padding: 11px 10px;
        border-radius: 6px;
        margin-bottom: 7px;
        placeholder:Search Items;
    }

  .link { background:none;border:none; }

.pj-preloader {
    display: none;
    position: absolute;
    height: 383px;;
    width: 940px;
    background: url("resources/img/spinner.gif") no-repeat scroll center center rgba(153, 153, 153, 0.3);
    z-index: 9999;
    left: 260;
    position: absolute;
    top: 224px;
}
    </style>


   <script type="text/javascript">

    //Plug-in to fetch page data
    jQuery.fn.dataTableExt.oApi.fnPagingInfo = function ( oSettings )
    {

        return {
            "iStart":         oSettings._iDisplayStart,
            "iEnd":           oSettings.fnDisplayEnd(),
            "iLength":        oSettings._iDisplayLength,
            "iTotal":         oSettings.fnRecordsTotal(),
            "iFilteredTotal": oSettings.fnRecordsDisplay(),
            "iPage":          oSettings._iDisplayLength === -1 ?
                0 : Math.ceil( oSettings._iDisplayStart / oSettings._iDisplayLength ),
            "iTotalPages":    oSettings._iDisplayLength === -1 ?
                0 : Math.ceil( oSettings.fnRecordsDisplay() / oSettings._iDisplayLength )
        };
    };

$(document).ready(function() {
    $.fn.dataTableExt.sErrMode = 'throw';
});

function loadVirtualfund(){

	 if ( $.fn.DataTable.isDataTable('#example') ) {
         $('#example').DataTable().destroy();
       }

       $('#example tbody').empty();



   $("#LoadingImage").show();

  // var pizzaSize=$('#pizzaSizeId').val();
   var outer = [];
   $.ajax({
       url : "getVirtualfundByMerchantId",
       type : "GET",
       contentType : "application/json; charset=utf-8",
       success : function(data) {
         var jsonOutpt = JSON.stringify(data);
         var menuItems = JSON.parse(jsonOutpt);
         $("#LoadingImage").hide();
         $.each(menuItems, function( index, value ) {
               var inner = [];

               inner.push(value.partnerName);
               inner.push(value.partnerEmailId);
               if(value.code != null)
               inner.push(value.code);
               else
            	   inner.push("");
               if(value.percentage != null)
               inner.push(value.percentage);
               else
            	   inner.push("");
               inner.push(value.startDate);
               inner.push(value.endDate);
               inner.push(value.showStatus);
               inner.push(value.action);
               outer.push(inner);
            });
         $('#example').DataTable( {
             data: outer,
             columns: [

                 { title: "Partner Name" },
                 { title: "Partner EmailId" },
                 { title: "Code" },
                 { title: "Percentage" },
                 { title: "StartDate" },
                 { title: "EndDate" },
                 { title: "Status" },
                 { title: "Action" }
                 

             ]
         } );
        var table =$('#example').DataTable();
        $(".searchq").keyup(function() {
              table.search( this.value).draw();
        } );
       },
       error : function() {
           $("#LoadingImage").hide();
           console.log("Error in category wise inventory");
        }
     })

}

$(document).ready( function setUp() {
	loadVirtualfund();


});
</script>

 <script>

       $(document).ready(function() {


           $("#example").on("click", ".nav-toggle", function(){
               var fundId=$(this).attr('fundId');
               var thishtml = jQuery(this).html();
               var fundStatus;
               if(thishtml =='Active'){
            	   fundStatus=0;
                 jQuery(this).html('InActive');
               }
               if(thishtml =='InActive'){
            	   fundStatus=1;
                 jQuery(this).html('Active');
               }
               $('.pj-preloader').css('display','block');
               $.ajax({
                    type: 'GET',
                    url: "updatefundStatusById?fundId="+fundId+"&fundStatus="+fundStatus,
                    success:function(data){
                      $('.pj-preloader').css('display','none');
                    }
                 }); 
               
               });
        }); 
        </script>
  </head>
  <body>

    <div id="page-container">
        <div class="foodkonnekt inventory">
            <div class="inner-container">
                <div class="max-row">

                    <header id="page-header">
                        <div class="inner-header">
                            <div class="row">

                                <div class="logo">
                                     <a href="adminHome" title="FoodKonnekt Dashboard" class="logo"><img src="resources/img/foodkonnekt-logo.png"></a>
                                </div><!--.logo-->
                                <%@ include file="adminHeader.jsp" %>

                            </div><!--.row-->
                        </div><!--.inner-header-->
                    </header><!--#page-header-->

                    <div id="page-content">
                        <div class="outer-container">

                            <div class="row">
                                <div class="content-inner-container">

                                     <%@ include file="leftMenu.jsp"%>

                                    <div class="right-content-container">

                                        <div class="right-content-inner-container">

                                            <div class="content-header">
                                                <div class="all-header-title">
                                                </div><!--.header-title-->
                                            </div><!--.content-header-->

                                            <div class="merchant-page-data">

                                                <div class="merchant-actions-outbound">
                                                    <div class="merchat-coupons-container">
                                                            <!-- <main> -->

                                                        <%@ include file="adminMerchantMenu.jsp"%>
                                                        <br><br>

                                                        				<div class="coupons-content-container">

															<div class="clearfix"></div>


                                                              <section id="content1">
                                                                <div class="tab-content-container-outbound">
                                                                    <div class="tab-content-container">
                                                                        <div class="tab-content-container-inbound">
                                                                            <div class="only-search-part">

                                                                                <div class="search-container">
                                                                                     <div class="only-search-elements">
                                                                                    <label>Search</label>
                                                                                    <input type="text" placeholder="Search Partner" id="search-crust" class="searchq" style="padding-right: 503px;">
                                                                                     <input type="button" value="Search"><br>

                                                                                    </div>



                                                                                  <!--.only-filter-container-->
                                                                                </div><!--.search-container-->


                                                                                <div class="xlsUpload">
																					<label id="errorMessage"
																						style="color: red; float: right"></label><br>

																				</div>

																				<div class="xlsUpload">
																					<label id="errorMessage"
																						style="color: red; float: right"></label><br>
																					<div class="upload-inventory" id="upload-inventory" style="margin-bottom: 3%;display: block;">

																						<input type="button" id="addCrust" value="Add Partner" onclick="addVirtualFund()"
																							style="margin: 4%; margin-right: 1%; margin-top: -6.5%;">

																						<br>
																					</div>
																				</div>

                                                                                <div class="inventory-items-list-table">
                                                                                <div class="pj-preloader"></div>
                                                                                  <span id="Message1"
																	style="color: red; font-size: 2000;" tabindex="0"></span>
                                                                                     <table width="100%" cellpadding="0" cellspacing="0" id="example" >
                                                                                      <thead>
                                                                                        <tr>
                                                                                          <th>Partner Name</th>
                                                                                          <th>Partner EmailId</th>
                                                                                          <th>Code</th>
                                                                                          <th>Percent</th>
                                                                                          <th>FROM</th>
                                                                                          <th style="width: 78px;">TO</th>
                                                                                          <th>Status</th>
                                                                                          <th>Action</th>
                                                                                        </tr>
                                                                                      </thead>
                                                                                    </table>
                                                                                    <div id="LoadingImage" style="display: none" align="middle">
                                                          <img src="resources/img/spinner.gif" align="middle" />
                                                             </div>
                                                                                </div><!--.inventory-items-list-table-->
                                                                            </div><!--.only-search-part-->
                                                                        </div><!--.tab-content-container-inbound-->
                                                                    </div><!--.tab-content-container-->
                                                                </div><!--.tab-content-container-outbound-->
                                                              </section>

                                                                </div>

                                                        </div><!--.inventory-tabs-inbound-->
                                                    </div><!--.inventory-tabs-->
                                                </div><!--.inventory-tabs-outbound-->
                                            </div><!--.inventory-page-data-->

                                        </div><!--.right-content-inner-container-->
                                    </div><!--.right-content-container-->
                                </div><!--.content-inner-container-->
                            </div><!--.row-->

                        </div><!--.outer-container-->
                    </div><!--#page-content-->

                    <%@ include file="adminFooter.jsp" %>
                    <!--#footer-container-->

                </div><!--.max-row-->
            </div><!--.inner-container-->
        </div><!--.foodkonnekt .dashboard-->
    </div><!--#page-container-->
    <script type="text/javascript">
   /*  $(document).ready(function() {
        //categoryItems();
  }); */



 </script>

 <script>
var table = $('#example').DataTable();

    $('#search-crust').on( 'keyup', function () {
        table.search( this.value ).draw();
    } );

	function addVirtualFund(){
		 window.location.href = "addVirtualFund";
	}


</script>
<script>
  var polyfilter_scriptpath = '/js/';
</script>
 <script src="resources/js/dialog-box/classie.js"></script>
<script src="resources/js/dialog-box/modalEffects.js"></script>




  </body>
</html>
