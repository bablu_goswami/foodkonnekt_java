<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="true"%>
<!doctype html>
<html class="no-js" lang="en">
<head>
<title>FoodKonnekt | Add Products</title>
<link href="resources/css/bootstrap.min.css" rel="stylesheet"
	type="text/css" />
<link href="resources/css/bootstrap-multiselect.css" rel="stylesheet"
	type="text/css" />
<!--CALLING STYLESHEET STYE.CSS-->
<link rel="stylesheet" href="resources/css/style.css">
<!--CALLING STYLESHEET STYLE.CSS-->

<!--CALLING GOOGLE FONT OPEN SANS-->
<link
	href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic'
	rel='stylesheet' type='text/css'>
<!--CALLING GOOGLE FONT OPEN SANS-->

<!--CALLING FONT AWESOME-->
<link rel="stylesheet" href="resources/css/font-awesome.css">
<!--CALLING FONT AWESOME-->

<!--OPENS DIALOG BOX-->
<link rel="stylesheet" type="text/css"
	href="resources/css/dialog-box/component.css" />
<!--OPENS DIALOG BOX-->

<!--CALLING PRODUCTS TABS-->
<link rel='stylesheet' type='text/css'
	href='resources/css/products-tabs/opentabby.css' />
<!--CALLING PRODUCTS TABS-->
<!-- <link rel="stylesheet" type="text/css"
	href="https://cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css"> -->

<style type="text/css">
.body {
	overflow: scroll !important;
}

.sd-modifiers-each-field-label label label {
	height: auto;
	display: inline-block;
	max-width: 20px;
	margin-left: 10px;
}

.sd-modifiers-each-field-label label span {
	float: left;
}

div#example_paginate {
	display: block;
}

div#example_filter {
	display: block;
}

div#example_length {
	display: block;
}

input[type="search"] {
	max-width: 300px;
	width: 100%;
	outline: 0;
	border: 1px solid rgb(169, 169, 169);
	padding: 11px 10px;
	border-radius: 6px;
	margin-bottom: 7px;
	placeholder: Search Items;
}

input[type="text"], input[type="email"], input[type="password"], input[type="number"],
	input[type="date"] {
	height: 39px;
}

.pj-preloader {
	display: none;
	position: absolute;
	height: 383px;;
	width: 940px;
	background: url("resources/img/spinner.gif") no-repeat scroll center
		center rgba(153, 153, 153, 0.3);
	z-index: 9999;
	left: 260;
	position: absolute;
	top: 224px;
}
</style>
<div class="exampleLive">
	<button style="display: none;" id="categoryOrderPopUp"
		class="btn btn-primary"
		data-confirmmodal-bind="#confirm_content_categoryOrder"
		data-topoffset="0" data-top="30%">Example</button>
</div>
<div id="confirm_content_categoryOrder" style="display: none">
	<div class="confirmModal_content sd-popup-content">
		<img src="resources/img/logo.png" class="sd-popup-logo">
		<h3>This order has been selected .Do you want to replace this ?</h3>
	</div>
	<div class="confirmModal_footer">
		<button type="button" id="yesBtn" class="btn btn-primary"
			onclick="yesButton()">Yes</button>
		<button type="button" class="btn btn-primary" onclick="cancelButton()">Cancel</button>
	</div>
</div>

</head>
<body>
	<div id="page-container">
		<div class="foodkonnekt merchant">
			<div class="inner-container">
				<div class="max-row">

					<header id="page-header">
						<div class="inner-header">
							<div class="row">
								<div class="logo">
									<a href="adminHome" title="FoodKonnekt Dashboard" class="logo"><img
										src="resources/img/foodkonnekt-logo.png"></a>
								</div>
								<!--.logo-->
								<%@ include file="adminHeader.jsp"%>
							</div>
							<!--.row-->
						</div>
						<!--.inner-header-->
					</header>
					<!--#page-header-->

					<div id="page-content">
						<div class="outer-container">
							<div class="row">
								<div class="content-inner-container">
									<%@ include file="leftMenu.jsp"%>
									<div class="right-content-container">
										<div class="right-content-inner-container">

											<div class="content-header">
												<div class="all-header-title"></div>
												<!--.header-title-->
												<div class="content-header-dropdown"></div>
												<!--.content-header-dropdown-->
											</div>
											<!--.content-header-->

											<div class="merchant-page-data">
												<div class="merchant-actions-outbound">
													<div class="merchat-coupons-container">

														<div class="coupons-navigation">
																			<ul>
																				<li class="current-menu-item"><a
																					href="inventory">Items</a></li>
																				<li><a href="category">Categories</a></li>
																				<li><a href="modifierGroups">Modifier
																						Groups</a></li>
																				<li><a href="modifiers">Modifiers</a></li>
																			</ul>
																		</div>
																	
														<!--.coupons-navigation-->
<br>
														<div class="delivery-zones-content-container">
															<form:form action="updateItemTexes"
																modelAttribute="itemTexes" method="POST">
																	<div>
																	<label>Item:</label>
																<form:select id="itemId" style="display: block;" class="multiselect"
																	multiple="multiple" path="itemsId">
																	<c:forEach items="${items}" var="item"
																		varStatus="status">
																		<option value="${item.id}">${item.name}</option>
																	</c:forEach>
																</form:select>
																</div><div>
																<label>Taxes:</label>
																<form:select id="texesId" style="display: block;margin:100px;" class="multiselect"
																	multiple="multiple" path="texesId">
																	<c:forEach items="${taxRates}" var="taxRate"
																		varStatus="status">
																		<option value="${taxRate.id}">${taxRate.name}</option>
																	</c:forEach>
																</form:select>
																</div>
																<div class="button left">
																	<input type="submit" id="updateItemButton" value="Save">&nbsp;&nbsp;
																</div>
															</form:form>
														</div>
														<div id="errorDiv" style="color: red"></div>
													</div>
													<!--.merchat-coupons-container-->
												</div>
												<!--.merchant-actions-outbound-->
											</div>
											<!--.merchant-page-data-->

										</div>
										<!--.right-content-inner-container-->

									</div>
									<!--.right-content-container-->
								</div>
								<!--.content-inner-container-->

							</div>
							<!--.row-->

						</div>
						<!--.outer-container-->

					</div>
					<!--#page-content-->
					<%@ include file="adminFooter.jsp"%>
				</div>
				<!--.max-row-->
			</div>
			<!--.inner-container-->
		</div>
		<!--.foodkonnekt .dashboard-->
	</div>
	<!--#page-container-->
	<script type="text/javascript"
		src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
	<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
	<script src='resources/js/products-tabs/opentabby.js'></script>
	<script src="resources/js/popModal.js"></script>
	<script type="text/javascript" src="resources/js/bootstrap.min.js"></script>
	<script src="resources/js/bootstrap-multiselect.js"
		type="text/javascript"></script>
	<script>
        var polyfilter_scriptpath = '/js/';
    </script>
	<!--OPENS DIALOG BOX-->

</body>
<script type="text/javascript">
      $(function () {
 		 $('#itemId').multiselect({
 	         maxHeight: 450 ,
          	includeSelectAllOption: true
          }); 
 		 
 		 $('#texesId').multiselect({
 			  maxHeight: 450 ,
 	         	includeSelectAllOption: true
 	         }); 
 		});
    </script>
</html>
