








<!-- html 3 -->
<%-- <%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
  <head>
    <title>Drawing tools</title>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <link
	href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic'
	rel='stylesheet' type='text/css'>
<!--CALLING GOOGLE FONT OPEN SANS-->

<!--CALLING FONT AWESOME-->
<link rel="stylesheet" href="resources/css/font-awesome.css">
<!--CALLING FONT AWESOME-->

<!--CALLING FILTER OPTIONS-->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<!--  <script src="cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.2.17/jquery.timepicker.min.js" type="text/javascript"></script>
     <script src="cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.2.17/jquery.timepicker.min.js" type="text/javascript"></script> -->
<script type="text/javascript" charset="utf8"
	src="https://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/jquery.dataTables.min.js"></script>
<script
	src="https://www.jqueryscript.net/demo/Powerful-jQuery-Data-Table-Column-Filter-Plugin-yadcf/jquery.dataTables.yadcf.js"></script>
<!--OPENS DIALOG BOX-->
<link rel="stylesheet" type="text/css"
	href="resources/css/dialog-box/component.css" />
<!-- <link rel="stylesheet" type="text/css" href="cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.2.17/jquery.timepicker.min.css"/> -->


<script
	src="https://netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
<script
	type="http://netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css"></script>
    <style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 100%;
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
       /*  padding-left:100px;
        padding-right:100px; */
      }
    </style>
  </head>
  <body>
  <div id="page-container">
        <div class="foodkonnekt merchant">
            <div class="inner-container">
                <div class="max-row">
                    
                    <header id="page-header">
                        <div class="inner-header">
                            <div class="row">
                                <div class="logo">
                                     <a href="adminHome" title="FoodKonnekt Dashboard" class="logo"><img src="resources/img/foodkonnekt-logo.png"></a>
                                </div><!--.logo-->
                                   <%@ include file="adminHeader.jsp"%>
                            </div><!--.row-->
                        </div><!--.inner-header-->
                    </header><!--#page-header-->
                    </div>
                    </div>
                    </div>
                    </div>
                    
                    
  <!--   <div id="map" style="height: 400px;width: 60%;"></div> -->
    <div id="map" style="height: 600px;width: 70%;" ></div>
    
    <script>
      // This example requires the Drawing library. Include the libraries=drawing
      // parameter when you first load the API. For example:
      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=drawing">

      var geocoder;

function geocodePosition(pos) {
	console.log("##############");
	console.log(pos);
	//initial point in pos
  geocoder.geocode({
    latLng: pos
  }, function(responses) {
	  console.log("responses="+responses[0].formatted_address)
	  console.log("-----------------------------------------")
	  console.log("responses="+responses[1].formatted_address)
	   console.log("-----------------------------------------")
	  console.log("responses="+responses[2].formatted_address)
    if (responses && responses.length > 0) {
   //   updateMarkerAddress(responses[0].formatted_address);
    } else {
   //   updateMarkerAddress('Cannot determine address at this location.');
    }
  });
} 
      
      
      function initMap() {
    	   geocoder = new google.maps.Geocoder();
        var map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: ${latitude}, lng: ${longitude}},
          zoom: 16
        });
       // var  poss= {lat: ${lat}, lng: ${lng}}
        var latLng = new google.maps.LatLng(${latitude}, ${longitude});
       
        var drawingManager = new google.maps.drawing.DrawingManager({
          drawingMode: google.maps.drawing.OverlayType.POLYGON,
          drawingControl: true,
          drawingControlOptions: {
            position: google.maps.ControlPosition.TOP_CENTER,
          //  drawingModes: ['marker', 'circle', 'polygon', 'polyline', 'rectangle']
            drawingModes: ['polygon']
          },
         
          markerOptions: {icon: 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png'},
          polygonOptions: {
        	  fillColor: '#BCDCF9',
              fillOpacity: 0.5,
              strokeWeight: 2,
              strokeColor: '#57ACF9',
              clickable: false,
              editable: false,
              zIndex: 1
          }
        });
        console.log("-----------------------------------------------")
        var zoneCoordinates=[];
        var bermudaTriangle;
         google.maps.event.addListener(drawingManager, 'polygoncomplete', function(polygon) {
        	  var radius = polygon.getPath();
        	  console.log(radius.getArray());
        	  var arrayLength = radius.getArray().length;
        	  for (var i = 0; i < arrayLength; i++) {
        	      zoneCoordinates.push(radius.getArray()[i]);
        	      
        	  }
        	   bermudaTriangle = new google.maps.Polygon({paths: zoneCoordinates});
        	  getpolygonLatLong(zoneCoordinates);
        	  
        	});

        	 google.maps.event.addListener(drawingManager, 'overlaycomplete', function(event) {
        	  if (event.type == 'polygon') {
        	    var radius = event.overlay.getPath();
        	    console.log("event.overlay-->"+radius);
        	  }
        	});  
       
        
        var bermudaTriangle = new google.maps.Polygon({paths: zoneCoordinates});
        
        google.maps.event.addListener(map, 'click', function(event) {
            var resultColor =
                google.maps.geometry.poly.containsLocation(event.latLng, bermudaTriangle) ?
                'blue' :
                'red';
            var resultPath =
                google.maps.geometry.poly.containsLocation(event.latLng, bermudaTriangle) ?
                // A triangle.
                "m 0 -1 l 1 2 -2 0 z" :
                google.maps.SymbolPath.CIRCLE;
          //  markerOptions: {icon: 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png'}
             new google.maps.Marker({
              position: event.latLng,
              map: map,
              icon: {
                path: resultPath,
                fillColor: resultColor,
                fillOpacity: 1,
                strokeColor: 'black',
                strokeWeight: 1,
                scale: 10
              }
            }); 
          });
        	drawingManager.setMap(map);
      }
      
      
      function getpolygonLatLong(coordinateArray){
    	  var merchantId=${merchant.id};
      	$.ajax({
              url : "getDeliveryZonePolygones?merchantId="+ merchantId+"&polygones="+coordinateArray,
              type : "GET",
              contentType : "application/json; charset=utf-8",
              success : function(result) {
              	           alert(result);
              },
              error : function() {
                console.log("Error inside future date Ajax call");
              }
           })
    	  
      }
    </script>
    
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDjnluKhome9lmt5LLKnIgqot7HtyDetes&libraries=drawing&callback=initMap"
         async defer></script>
         
  </body>
</html> --%>

<link rel="stylesheet" href="resources/css/style.css">
<!--CALLING STYLESHEET STYLE.CSS-->

<!--CALLING GOOGLE FONT OPEN SANS-->
<link
	href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic'
	rel='stylesheet' type='text/css'>
<!--CALLING GOOGLE FONT OPEN SANS-->

<!--CALLING FONT AWESOME-->
<link rel="stylesheet" href="resources/css/font-awesome.css">
<!--CALLING FONT AWESOME-->

<!--CALLING FILTER OPTIONS-->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<!--  <script src="cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.2.17/jquery.timepicker.min.js" type="text/javascript"></script>
     <script src="cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.2.17/jquery.timepicker.min.js" type="text/javascript"></script> -->
<script type="text/javascript" charset="utf8"
	src="https://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/jquery.dataTables.min.js"></script>
<script
	src="https://www.jqueryscript.net/demo/Powerful-jQuery-Data-Table-Column-Filter-Plugin-yadcf/jquery.dataTables.yadcf.js"></script>
<!--OPENS DIALOG BOX-->
<link rel="stylesheet" type="text/css"
	href="resources/css/dialog-box/component.css" />
<!-- <link rel="stylesheet" type="text/css" href="cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.2.17/jquery.timepicker.min.css"/> -->


<script
	src="https://netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
<script
	type="http://netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css"></script>

<div id="page-container">
		<div class="foodkonnekt merchant">
			<div class="inner-container">
				<div class="max-row">

					<header id="page-header">
						<div class="inner-header">
							<div class="row">

								<div class="logo">
									<a href="adminHome" title="FoodKonnekt Dashboard" class="logo"><img
										src="resources/img/foodkonnekt-logo.png"></a>
								</div>
								<!--.logo-->
								<%@ include file="adminHeader.jsp"%>

							</div>
							<!--.row-->
						</div>
						<!--.inner-header-->
					</header>
					<!--#page-header-->

					<div id="page-content">
						<div class="outer-container">
							<div class="row">
								<div class="content-inner-container">
									<%@ include file="leftMenu.jsp"%>

									<div class="right-content-container">
										<div class="right-content-inner-container">

											<div class="content-header">
												<div class="all-header-title"></div>
												<!--.header-title-->
												<div class="content-header-dropdown"></div>
												<!--.content-header-dropdown-->
											</div>
											<!--.content-header-->

											<div class="merchant-page-data">
												<div class="merchant-actions-outbound">
													<div class="merchat-coupons-container">

														<div class="coupons-navigation">
															<ul>
																<li class="current-menu-item"><a
																	href="onLineOrderLink">Location</a></li>
																<li><a href="deliveryZones">Delivery Zones</a></li>
																<li><a href="deliveryZonesWithMap?locationAddress=${merchant.name} ${location.address1}${location.city}${location.zip}">Zone with map</a></li>
																 <!--<li><a href="vouchars">Coupons</a></li>-->
																<li><a href="customers">Customers</a></li>
																<c:if test="${merchant.owner.pos.posId!=1}"> <li><a href="setupPaymentGateway?adminPanel=1">Gateway</a></li></c:if>
															</ul>
														</div>
														 <div id="map" style="height: 600px;width: 70%;" ></div>
    
    <script>
      // This example requires the Drawing library. Include the libraries=drawing
      // parameter when you first load the API. For example:
      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=drawing">

      var geocoder;

function geocodePosition(pos) {
	console.log("##############");
	console.log(pos);
	//initial point in pos
  geocoder.geocode({
    latLng: pos
  }, function(responses) {
	  console.log("responses="+responses[0].formatted_address)
	  console.log("-----------------------------------------")
	  console.log("responses="+responses[1].formatted_address)
	   console.log("-----------------------------------------")
	  console.log("responses="+responses[2].formatted_address)
    if (responses && responses.length > 0) {
   //   updateMarkerAddress(responses[0].formatted_address);
    } else {
   //   updateMarkerAddress('Cannot determine address at this location.');
    }
  });
} 
      
      
      function initMap() {
    	   geocoder = new google.maps.Geocoder();
        var map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: ${latitude}, lng: ${longitude}},
          zoom: 16
        });
        var marker = new google.maps.Marker({
            position: {lat: ${latitude}, lng: ${longitude}},
            map: map
          });
       // var  poss= {lat: ${lat}, lng: ${lng}}
        var latLng = new google.maps.LatLng(${latitude}, ${longitude});
       
        var drawingManager = new google.maps.drawing.DrawingManager({
          drawingMode: google.maps.drawing.OverlayType.POLYGON,
          drawingControl: true,
          drawingControlOptions: {
            position: google.maps.ControlPosition.TOP_CENTER,
          //  drawingModes: ['marker', 'circle', 'polygon', 'polyline', 'rectangle']
            drawingModes: ['polygon']
          },
         
          markerOptions: {icon: 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png'},
          polygonOptions: {
        	  fillColor: '#BCDCF9',
              fillOpacity: 0.5,
              strokeWeight: 2,
              strokeColor: '#57ACF9',
              clickable: false,
              editable: false,
              zIndex: 1
          }
        });
        console.log("-----------------------------------------------")
        var zoneCoordinates=[];
        var bermudaTriangle;
         google.maps.event.addListener(drawingManager, 'polygoncomplete', function(polygon) {
        	  var radius = polygon.getPath();
        	  console.log(radius.getArray());
        	  var arrayLength = radius.getArray().length;
        	  for (var i = 0; i < arrayLength; i++) {
        	      zoneCoordinates.push(radius.getArray()[i]);
        	      
        	  }
        	   bermudaTriangle = new google.maps.Polygon({paths: zoneCoordinates});
        	  getpolygonLatLong(zoneCoordinates);
        	  zoneCoordinates=[];
        	});

        	 google.maps.event.addListener(drawingManager, 'overlaycomplete', function(event) {
        	  if (event.type == 'polygon') {
        	    var radius = event.overlay.getPath();
        	    console.log("event.overlay-->"+radius);
        	  }
        	});  
       
        
        var bermudaTriangle = new google.maps.Polygon({paths: zoneCoordinates});
        
        google.maps.event.addListener(map, 'click', function(event) {
            var resultColor =
                google.maps.geometry.poly.containsLocation(event.latLng, bermudaTriangle) ?
                'blue' :
                'red';
            var resultPath =
                google.maps.geometry.poly.containsLocation(event.latLng, bermudaTriangle) ?
                // A triangle.
                "m 0 -1 l 1 2 -2 0 z" :
                google.maps.SymbolPath.CIRCLE;
          //  markerOptions: {icon: 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png'}
             new google.maps.Marker({
              position: event.latLng,
              map: map,
              icon: {
                path: resultPath,
                fillColor: resultColor,
                fillOpacity: 1,
                strokeColor: 'black',
                strokeWeight: 1,
                scale: 10
              }
            }); 
          });
        	drawingManager.setMap(map);
      }
      
      
      function getpolygonLatLong(coordinateArray){
    	  var merchantId=${merchant.id};
      	$.ajax({
              url : "getDeliveryZonePolygones?merchantId="+ merchantId+"&polygones="+coordinateArray,
              type : "GET",
              contentType : "application/json; charset=utf-8",
              success : function(result) {
              },
              error : function() {
                console.log("Error inside future date Ajax call");
              }
           })
    	  
      }
    </script>
    
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDjnluKhome9lmt5LLKnIgqot7HtyDetes&libraries=drawing&callback=initMap"
         async defer></script>
														<!--.coupons-navigation-->

														<%-- <div class="coupons-content-container">

															<div class="clearfix"></div>

															<div class="location-container">
																<div class="location-container-form">
																	<form:form action="saveBusinessLogo" method="POST"
																		modelAttribute="OpenHours" id="businessLogo"
																		enctype="multipart/form-data">
																		<h3>BUSINESS INFORMATION</h3>
																		<label>Business Name:</label>
																		<input type="text" value="${merchant.name}"
																			readonly="readonly">

																		<div class="clearfix"></div>
																		<div>
																			<label>Street Address1</label><input type="text"
																				value="${location.address1}" readonly="readonly">
																		</div>
																		<div>
																			<label>Street Address2</label><input type="text"
																				value="${location.address2}" readonly="readonly">
																		</div>
																		<div>
																			<label>Street Address3</label><input type="text"
																				value="${location.address3}" readonly="readonly">
																		</div>
																		<div>
																			<label>City</label><input type="text"
																				value="${location.city}" readonly="readonly">
																		</div>
																		<div>
																			<label>State</label><input type="text"
																				value="${location.state}" readonly="readonly">
																		</div>
																		<div>
																			<label>Zip</label><input type="text"
																				Placeholder="Postal/Zip Code"
																				value="${location.zip}" readonly="readonly">
																		</div>
																		<div>
																			<label>Phone No.:</label> <input type="text"
																				value="${merchant.phoneNumber}" readonly="readonly"
																				maxlength="10" size="10">
																		</div>
																		<div>
																			<label>Website:</label> <input type="text"
																				readonly="readonly">
																		</div>

																		<div>

																			<label>Payment Type:</label>
																			<div class="payment-order-tax">
																				<ul>
																					<li><form:checkbox id="cash"
																							path="allowPaymentModes" value="Cash"
																							onclick="checkCash()" />Cash</li>
																					<li><form:checkbox id="creditcard"
																							path="allowPaymentModes" value="Credit Card"
																							onclick="checkCreditCard()" />Credit Card</li>
																			</div>

																			<span id="paymentTypeErrorBox" style="color: red;"></span>
</div>

																			<!--.payment-order-tax-->

																			<!-- <div>
																				<label>Order Type:</label>
																				<div class="payment-order-tax">
																					<ul>
																						<li><input type="checkbox" name="paymenttype"
																							value="Delivery">Delivery</li>
																						<li><input type="checkbox" name="paymenttype"
																							value="Pick Up">Pick Up</li>
																				</div>
																				.payment-order-tax
																			</div> -->
																			<form:hidden path="locationId" value="${location.id}" />
																			<div>
																				<label>Pick Up Time:</label>
																				<form:input path="pickUpTiime"
																					value="${pickupTime.pickUpTime}"
																					type='Number' onkeypress="return isNumberKey(event)" 
																					 min="0"/> &nbsp;&nbsp;Minutes
																			</div>
																			<div>
																				<label>Allow Future Order:</label>
																				
																				
																				        <c:choose>
																										<c:when test="${allowFutureOrder ==1}">
																										<div class="payment-order-tax">
																				 						<ul>
																					                         
																				                           <li> <form:radiobutton path="allowFutureOrder" value="0" id="noRadio"/>No</li>
																				                           <li><form:radiobutton path="allowFutureOrder" value="1" checked="checked" id="yesRadio"/>Yes</li>
																				                           </ul>
																				                           </div>
																										</c:when>
																										<c:otherwise>
																											<div class="payment-order-tax">
																				 						<ul>
																				                           <li> <form:radiobutton path="allowFutureOrder" value="0" checked="checked" id="noRadio"/>No</li>
																				                            <li><form:radiobutton path="allowFutureOrder" value="1" id="yesRadio" />Yes</li>
																				                           </ul>
																				                           </div>
																										</c:otherwise>
																									</c:choose>
																				
																				
																			</div>
																			
																			<div id="daysAhead">
																				<label>Days Ahead:</label>
																				<form:input path="futureDaysAhead"
																				     value="${futureDaysAhead}"
																					type='Number' maxlength="2" onkeypress="return isNumberKey(event)" 
																					 min="0"
																					 oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"/>
																			</div>
																			<div>
																				<label>Allow Multiple Koupon:</label>
																				
																				
																				        <c:choose>
																										<c:when test="${allowMultipleKoupon ==1}">
																										<div class="payment-order-tax">
																				 						<ul>
																					                         
																				                           <li> <form:radiobutton path="allowMultipleKoupon" value="0" id="noMultipleRadio"/>No</li>
																				                           <li><form:radiobutton path="allowMultipleKoupon" value="1" checked="checked" id="yesMultipleRadio"/>Yes</li>
																				                           </ul>
																				                           </div>
																										</c:when>
																										<c:otherwise>
																											<div class="payment-order-tax">
																				 						<ul>
																				                           <li> <form:radiobutton path="allowMultipleKoupon" value="0" checked="checked" id="noMultipleRadio"/>No</li>
																				                            <li><form:radiobutton path="allowMultipleKoupon" value="1" id="yesMultipleRadio" />Yes</li>
																				                           </ul>
																				                           </div>
																										</c:otherwise>
																									</c:choose>
																				
																				
																			</div>
																			 <div>
																				<label>Active Feedback:</label>
																				
																				
																				        <c:choose>
																										<c:when test="${activeCustomerFeedback ==1}">
																										<div class="payment-order-tax">
																				 						<ul>
																					                         
																				                           <li> <form:radiobutton path="activeCustomerFeedback" value="0" />No</li>
																				                           <li><form:radiobutton path="activeCustomerFeedback" value="1" checked="checked" />Yes</li>
																				                           </ul>
																				                           </div>
																										</c:when>
																										<c:otherwise>
																											<div class="payment-order-tax">
																				 						<ul>
																				                           <li> <form:radiobutton path="activeCustomerFeedback" value="0" checked="checked" />No</li>
																				                            <li><form:radiobutton path="activeCustomerFeedback" value="1"  />Yes</li>
																				                           </ul>
																				                           </div>
																										</c:otherwise>
																									</c:choose>
																				
																				
																			</div>
																			
																			<div>
																				<label>Convenience Fee:</label>
																				<form:input path="convenienceFee"
																					value="${convenienceFee.convenienceFee}"
																					type='Number' onkeypress="return isNumberKey(event)" 
																					 min="0" maxlength="5"
																					oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"/>
																			
																			<form:input path="convenienceFee" id = "convenienceFees"
																					value="${convenienceFee.convenienceFee}"
																					type="text"  
																					 min="0" maxlength="5"
																					onkeyup="return validatenumberOnKeyUp(event,this)" onkeypress="return validatenumber(event,this)"/>
																			
																			
																			</div>
																			<div>
																				<label>Taxable:</label>
																				<div class="payment-order-tax">
																					<ul>
																						<li><form:checkbox path="isTaxable" value="1"
																								id="isTaxable" />Yes</li>
																				</div>
																				<!--.payment-order-tax-->
																			</div>
																			
																			<div>
																				<label>Allow Re Order:</label>
																				<div class="payment-order-tax">
																					<ul>
																						<li><form:checkbox path="allowReOrder" value="1"
																								id="allowReOrder" />Yes</li>
																				</div>
																				<!--.payment-order-tax-->
																			</div>
																			
																			 <div>

																			<!-- <label>Allow Customer Feedback:</label>
																			<div class="payment-order-tax">
																				<ul>
																					<li><input type="radio" name="allowfeedback" id="feedbackyes">Yes</li>
																					<li><input type="radio" name="allowfeedback" id="feedbackno">No</li>
																			</div>

																			<span id="paymentTypeErrorBox" style="color: red;"></span>
																			</div> 
																			
																			
																			<div class="gap-30"></div>
                                                  
																			 <h3>Customer Feedback Link</h3>
																			<div class="callout">
																			
																				<span id="onlineLink1">For Allow Customer Feedback Select Yes</span>
																				<div class="button right" id="onlineLinkDivCls">
																				
																				
																					
																				</div>
																			</div>  -->
																			
																			
																			
																			
																			
																			<h3>SOCIAL MEDIA LINKS</h3>
																			<div>
																			<form:hidden path="socialMediaLinks.id"/>
																				<label>Facebook:</label>
																				<form:input path="socialMediaLinks.faceBookLink"
																					value="${socialMediaLinks.faceBookLink}"
																					type='text' min="0" maxlength="500"
																					oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"/>
																			</div>
																			<div>
																				<label>Yelp:</label>
																				<form:input path="socialMediaLinks.yelpLink"
																					value="${socialMediaLinks.yelpLink}"
																					type='text' min="0" maxlength="500"
																					oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"/>
																			</div>
																			<div>
																				<label>Instagram:</label>
																				<form:input path="socialMediaLinks.instagramLink"
																					value="${socialMediaLinks.instagramLink}"
																					type='text' min="0" maxlength="500"
																					oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"/>
																			</div>
																			<!-- facebook link start-->
																			
																			<div class="gap-30"></div>

																			<h3>
																				Publish App On Facebook
																				
																				<c:choose>
																											<c:when test="${merchant.isFBAppPublished==true}">
																												<div class="button right" id="facebookLinkView" style="margin-right: 45%; margin-top: -2%; display: none;">
																					<a
																						href="http://www.facebook.com/${merchant.fBTabIds}/app/608947722608987/"
																						target="_blank" >View App</a>
																	                        </div>
																											</c:when>
																											<c:otherwise>
																												<div class="button right" id="facebookLinkApp"  style="margin-right: 45%; margin-top: -2%; display: none;">
																					<!-- <a href="http://www.facebook.com/dialog/pagetab?app_id=608947722608987&redirect_uri=https://www.foodkonnekt.com/admin/fbUri"
																						target="_blank" style="background: #2428a7;">Publish
																						App</a> -->
																						<!-- <a href="#" onclick="window.open('http://www.facebook.com/v2.3/dialog/pagetab?app_id=608947722608987&redirect_uri=https://www.foodkonnekt.com/admin/fbUri&sdk=joey&version=v2.3&relation=opener&display=popup','mywindow','width=500,height=300,left=350,top=100');" >Publish
																						App</a> -->
																						
																						<a href="#" onclick="window.open('http://www.facebook.com/v2.3/dialog/pagetab?app_id=608947722608987&redirect_uri=https://www.foodkonnekt.com/foodkonnekt-admin/fbUri&sdk=joey&version=v2.3&relation=opener&display=popup','mywindow','width=500,height=300,left=350,top=100');" >Publish
																						App</a>
																				</div>
																											</c:otherwise>
																										</c:choose>
																				

																				<!-- facebook link end -->

																				<div class="gap-30"></div>

																				<h3>ONLINE ORDER LINK</h3>
																				<div class="callout">
																					<div class="pj-loader-3"
																						style="display: none; margin-left: 25%; margin-top: -10%;">
																						<img src="resources/img/load2.gif"
																							style="width: 82px; margin-left: 117px; margin-top: 63px;">
																					</div>
																					<span id="onlineLinkErrorBox" style="color: black;"></span> <span id="onlineLink">${onlineOrderLink}</span>
																					<div class="button right" id="onlineLinkDivCls">
																					
																						<a href="${onlineOrderLink}" target="_blank">Preview</a>

																						<a href="#"
																						onClick="window.open('${onlineOrderLink}')">Preview</a>
																					</div>
																					<div class="button right"
																						id="enableOnlineOrderLink">

																						<a>Yes</a>


																					</div>
																				</div>

																				<div class="gap-30"></div>

																				<h3>BUSINESS HOURS</h3>
																				<div class="callout" id="businessHours">
																					<span id="businessHourseErrorBox"
																						style="color: red;"></span>

																					<div class="button right">
																						<a href="#"
																							onClick="window.open('https://www.clover.com/setupapp/m/${merchant.posMerchantId}/businessinformation')">Go
																							To Clover</a>
																					</div>
																				</div>

																				<div id="businessHoursContainer3"></div>


																				<div class="business-hours">
																					<ul>
																						<c:forEach items="${businessHours}" var="bsh"
																							varStatus="status">
																							<li>
																								<div class="business-hours-li-left">
																									<label class="capDay"
																										style="color: black; line-height: 15px;">${bsh.day}</label>
																									<select class="opncls" name="selectedDay"
																										style="width: 90px;" attid="a_${bsh.id}">
																										<c:choose>
																											<c:when test="${bsh.isHoliday ==1}">
																												<option value="${bsh.id}:open">Open</option>
																												<option value="${bsh.id}:close"
																													selected="selected">Closed</option>
																											</c:when>
																											<c:otherwise>
																												<option value="${bsh.id}:open"
																													selected="selected">Open</option>
																												<option value="${bsh.id}:close">Closed</option>
																											</c:otherwise>
																										</c:choose>

																										  <option value="onLineOrderLinkonLineOrderLinkopen:${bsh.id}">Open</option>
                                                                                        <option value="close:${bsh.id}">Close</option>
																									</select>
																								</div> <!--.business-hours-li-left-->

																								<div class="business-hours-li-right teset"
																									style="width: 57%;">
																									<c:forEach items="${bsh.times}" var="time"
																										varStatus="status">
																										<div class="copyTime">
																											<select name="sTimeToSave"
																												style="width: 38%;">
																												<c:forEach items="${times}" var="tm">
																													<c:choose>
																														<c:when test="${tm ==time.startTime}">
																															<option value="${bsh.id}_${tm}"
																																selected="selected">${tm}</option>
																														</c:when>
																														<c:otherwise>
																															<option value="${bsh.id}_${tm}">${tm}</option>
																														</c:otherwise>
																													</c:choose>
																												</c:forEach>
																											</select>

																											<to>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;to&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</to>

																											<select name="eTimeToSave"
																												style="width: 38%;">
																												<c:forEach items="${times}" var="tm">
																													<c:choose>
																														<c:when test="${tm ==time.endTime}">
																															<option value="${tm}" selected="selected">${tm}</option>
																														</c:when>
																														<c:otherwise>
																															<option value="${tm}">${tm}</option>
																														</c:otherwise>
																													</c:choose>
																												</c:forEach>
																											</select>
																										</div>
																									</c:forEach>
																									<div class="copyToTime"
																										style="min-height: 47px;"></div>

																								</div> <!--.business-hours-li-right-->
																								<div class="business-hours-li-right"
																									style="width: 8%;">
																									<button id="0"
																										class="c-button-icon-primary optButton"
																										data-ember-action="1124" type="button">
																										<i class="fa fa-plus-circle hidden-xs"></i>
																									</button>

																									<button id="0"
																										class="e__button-icon-secondary optButtondel"
																										data-ember-action="1125" type="button">
																										<i class="fa fa-trash-o hidden-xs"></i>
																									</button>
																								</div>

																							</li>
																						</c:forEach>
																					</ul>
																				</div>

																				<div class="gap-30"></div>

																				<h3>BUSINESS LOGO</h3>
																				<div class="business-logo">
																					<!-- <img src="resources/img/230x100.png" /> -->
																					<img
																						src="data:image/jpeg;base64,${merchant.merchantLogo}"
																						onerror="this.src='resources/img/230x100.png'"
																						width="118" height="88" alt="image">
																					<p>(Max Upload Size 2 mb & Dimension 230x100 in
																						Pixels)</p>
																					<input type="file" name="file" id="image-file"
																						accept="image/*"> <label id="errorBox"
																						style="color: red;"></label>
																				</div>
																				<!--.business-logo-->
																				<c:choose>
																					<c:when test="${inventoryThread ==1}">
																						<div class="button">
																							<a href="#" id="businessLogoButton">Save</a>


																						</div>
																					</c:when>
																					<c:otherwise>
																						<div class="button">
																							<a href="#" id="businessLogoButton"
																								>Save</a>
																						</div>
																					</c:otherwise>
																				</c:choose>

																				<!--.button--></form:form>
																</div>
																<!--.location-container-form-->
															</div>
															<!--.location-container-->

														</div> --%>
														<!--.coupons-content-container-->
													</div>
													<!--.merchat-coupons-container-->
												</div>
												<!--.merchant-actions-outbound-->
											</div>
											<!--.merchant-page-data-->

										</div>
										<!--.right-content-inner-container-->
									</div>
									<!--.right-content-container-->
								</div>
								<!--.content-inner-container-->
							</div>
							<!--.row-->

						</div>
						<!--.outer-container-->
					</div>
					<!--#page-content-->
					<%@ include file="adminFooter.jsp"%>
					<!--#footer-container-->

				</div>
				<!--.max-row-->
			</div>
			<!--.inner-container-->
		</div>
		<!--.foodkonnekt .dashboard-->
	</div>