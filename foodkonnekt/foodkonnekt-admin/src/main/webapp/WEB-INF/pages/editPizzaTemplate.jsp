<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@page session="true"%>
<!doctype html>
<html class="no-js" lang="en">
<head>
<title>FoodKonnekt | Add Products</title>
<script src="https://www.foodkonnekt.com/web/resources/V2/js/jquery-1.11.3.min.js"></script>
 <link href="resources/css/bootstrap.min.css"
        rel="stylesheet" type="text/css" />
        <link href="resources/css/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
<!--CALLING STYLESHEET STYE.CSS-->
<link rel="stylesheet" href="resources/css/style.css">
<!--CALLING STYLESHEET STYLE.CSS-->

<!--CALLING GOOGLE FONT OPEN SANS-->
<link
	href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic'
	rel='stylesheet' type='text/css'>
<!--CALLING GOOGLE FONT OPEN SANS-->

<!--CALLING FONT AWESOME-->
<link rel="stylesheet"
	href="resources/css/font-awesome.css">
<!--CALLING FONT AWESOME-->

<!--OPENS DIALOG BOX-->
<link rel="stylesheet" type="text/css"
	href="resources/css/dialog-box/component.css" />
<!--OPENS DIALOG BOX-->

<!--CALLING PRODUCTS TABS-->
<link rel='stylesheet' type='text/css'
	href='resources/css/products-tabs/opentabby.css' />
<!--CALLING PRODUCTS TABS-->
	 <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css">
<style type="text/css">
.body{
overflow: scroll !important;
}
.sd-modifiers-each-field-label label label {
    height: auto;
    display: inline-block;
    max-width: 20px;
    margin-left: 10px;
}
.sd-modifiers-each-field-label label span {
    float: left;
}

div#example_paginate {
	display: block;
}

div#example_filter {
	display: block;
}

div#example_length {
	display: block;
}

input[type="search"] {
	max-width: 300px;
	width: 100%;
	outline: 0;
	border: 1px solid rgb(169, 169, 169);
	padding: 11px 10px;
	border-radius: 6px;
	margin-bottom: 7px;
	placeholder: Search Items;
}

input[type="text"], input[type="email"], input[type="password"], input[type="number"],
	input[type="date"] {
	height: 39px;
}

.pj-preloader {
    display: none;
    position: absolute;
    height: 383px;;
    width: 940px;
    background: url("resources/img/spinner.gif") no-repeat scroll center center rgba(153, 153, 153, 0.3);
    z-index: 9999;
    left: 260;
    position: absolute;
    top: 224px;
}
</style>
  </head>
  <body>
  <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
	<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
	<script src='resources/js/products-tabs/opentabby.js'></script>
	<script src="resources/js/popModal.js"></script>
	<script type="text/javascript" src="resources/js/bootstrap.min.js"></script>
    <script src="resources/js/bootstrap-multiselect.js" type="text/javascript"></script>
    <!-- <script src="resources/js/jquery.js" type="text/javascript"></script> -->
    <!-- <script src="js/jquery.dataTables.js" type="text/javascript"></script> -->
     <script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <div id="page-container">
        <div class="foodkonnekt inventory">
            <div class="inner-container">
                <div class="max-row">
                    
                    <header id="page-header">
                        <div class="inner-header">
                            <div class="row">
                                
                                <div class="logo">
                                     <a href="adminHome" title="FoodKonnekt Dashboard" class="logo"><img src="resources/img/foodkonnekt-logo.png"></a>
                                </div><!--.logo-->
                                <%@ include file="adminHeader.jsp" %> 
                                
                            </div><!--.row-->
                        </div><!--.inner-header-->
                    </header><!--#page-header-->
                    
                    <div id="page-content">
                        <div class="outer-container">
                        
                            <div class="row">
                                <div class="content-inner-container">
                                    
                                     <%@ include file="leftMenu.jsp"%>
                                
                                    <div class="right-content-container">
                                    
                                        <div class="right-content-inner-container">
                                        
                                            <div class="content-header">
                                                <div class="all-header-title">
                                                </div><!--.header-title-->
                                            </div><!--.content-header-->
                                            
                                            <div class="merchant-page-data">
                                        
                                                <div class="merchant-actions-outbound">
                                                    <div class="merchat-coupons-container">
                                                            <!-- <main> -->
                                                                 
                                                            <div class="coupons-navigation">
                                                               
                                                                                                                       <ul>
                                                                        <li class="current-menu-item"><a href="pizzaTamplate">Template</a></li>
                                                                        <li><a href="pizzaTopping">Topping</a></li>
                                                                         <li id="pizzaCrust"><a href="pizzaCrust">Crust</a></li> 
                                                                        <li id="pizzaSize"><a href="pizzaSize">Size</a></li>
                                                                        <li id="templateTaxMap"><a href="templateTaxMap">Tax Map</a></li>
                                                                        <li><a href="pizzaCategory">Pizza Category</a></li>
                                                                    </ul>
                                                        </div>
                                                        
                                                        
                                                        <div id="LoadingImage" style="display: none" align="middle">
                                                          <img src="resources/img/spinner.gif" align="middle" />
                                                             </div>
                                                              <section id="content1">
                                                                <div class="tab-content-container-outbound">
                                                                    <div class="tab-content-container">
                                                                        <div class="tab-content-container-inbound">
                                                                            <div class="only-search-part">
                                                                              
                                                                              <form:form method="POST" action="updatePizzaTemplate"
																			modelAttribute="pizzaTemplate" id="pizzaTemplate" >
                                                                              
                                                                              <form:hidden path="id" value="${pizzaTemplate.id}" id="pizzaTemplate_id" />
                                                                              
                                                                              <div class="adding-products">
																				<label id="errorMessage" style="color: red;"></label>
																				<div class="adding-products-form">
																					
																					<div class="clearfix"></div>
																					<label>Name:</label>
																					<textarea maxlength="140"  id="description" readonly="readonly" name="description">${pizzaTemplate.description }</textarea>
																					
																					<br>
																					<label>Description:</label>
																					<textarea input maxlength="140"  id="description2" name="description2"  >${pizzaTemplate.description2}</textarea>
																					<br>
																					
																					
																					<br>
																			<div class="clearfix"></div>
																			<label>Template Status:</label>
																			<form:radiobutton path="active" name="item" value="1" id="activeStatus" /> Active
																			<form:radiobutton path="active" name="item" value="0" id="inactiveStatus" /> InActive<br>
																				<br>
																				
																				<!-- <div><a href=javascript:void(0) class='nav-toggle' itmId="pizzaTemplate.getId() " style='color: blue;'>Active</a></div> -->
																				
																				<div class="clearfix"></div>
																				<label>Pizza Size:</label> 
																				
																				<div style="margin-left: 200px;">
																					<c:forEach items="${pizzaTemplateSizes}" var="pizzaTemplateSizes" varStatus="status">
																		<c:if test= "${pizzaTemplateSizes.pizzaSize.active == 1}">
																		
																				<c:if test= "${pizzaTemplateSizes.active == 1}">
																		
																				<form:checkbox path="sizeId" value="${pizzaTemplateSizes.pizzaSize.id}" 
																					id="checkBox_${pizzaTemplateSizes.pizzaSize.id}" onclick="showSizePrice(${pizzaTemplateSizes.pizzaSize.id})" checked = "true"/>&nbsp;&nbsp;
																				${pizzaTemplateSizes.pizzaSize.description}
																				
																				<br>
																				<price id="dollarTag_${pizzaTemplateSizes.pizzaSize.id}" style="display:block;margin-left: 150px; margin-top: -31px;">$</price>
																				<form:input path="sizePrice" id="sizePrice_${pizzaTemplateSizes.pizzaSize.id}" value="${pizzaTemplateSizes.price}" required="true"
																					style="margin-left: 191px;margin-top: -31px;display:block;" oninput="validateNumber(this);"  min="0" maxlength="5" class="dollar"/><br>
																			
																				</c:if>
																				
																				<c:if test= "${pizzaTemplateSizes.active == 0}">
																		
																				<form:checkbox path="sizeId" value="${pizzaTemplateSizes.pizzaSize.id}" 
																					id="checkBox_${pizzaTemplateSizes.pizzaSize.id}" onclick="showSizePrice(${pizzaTemplateSizes.pizzaSize.id})"/>&nbsp;&nbsp;
																				${pizzaTemplateSizes.pizzaSize.description}
																				
																				<br>
																				<price id="dollarTag_${pizzaTemplateSizes.pizzaSize.id}" style="display:none;margin-left: 150px; margin-top: -31px;">$</price>
																				<form:input path="sizePrice" id="sizePrice_${pizzaTemplateSizes.pizzaSize.id}" value="${pizzaTemplateSizes.price}" required="true"
																					style="margin-left: 191px;margin-top: -31px;display:none;" oninput="validateNumber(this);"  min="0" maxlength="5" class="dollar"/><br>
																			
																				</c:if>
																				</c:if>
																				</c:forEach>
																				<div class="button left">
    <button type="button" id="newSize" class="btn btn-danger" data-toggle="modal" data-target="#myModal1" 
                    	onclick="popupOpen()"  style="margin-left:295px;">Map New Size</button>
                                                                                                </div>
																				</div>
																				
																				 <br>
																				
                                                          						
																				 <%-- <br><br><br>
																				
																				<label >Pizza Topping Limit:</label> 
																				<form:radiobutton path="allowToppingLimit" name="item"
																					value="1" id="allowToppingLimitYes" onclick="showDiv()"/> Yes
																				<form:radiobutton path="allowToppingLimit" name="item"
																					value="0" id="allowToppingLimitNo" onclick="showDiv()"/> No<br>
																				<br>
																				<br> --%>
																				
																				
																				
																				
																				<%-- <div id="modifier_allow" style="display: none">
																							<form:hidden path="templateToppingIds"
																								value="${pizzaTemplate.id}" />
																							<div class="sd-modifiers-each-field-label">
																								<label><span>${topping.pizzaTopping.description}&nbsp;&nbsp;(Max:</span>
																									<label id="modigrp_id_${pizzaTemplate.id}">${pizzaTemplate.isMaxLimit}</label>)
																								</label>

																							</div>

																							<div class="sd-modifiers-each-field-input">
																								<form:input path="isMaxLimit"
																									class="modiferGrpCls setmaxvalueinmax"
																									type='Number'
																									onkeypress="return isNumberKey(event)" min="0"
																									max="${pizzaTemplate.isMaxLimit}"
																									id="modigrp_modi_id_${pizzaTemplate.id}"
																									onblur="checkToppingLimit(${pizzaTemplate.id})" />
																							</div>

																							<div class="sd-modifiers-each-field-label">
																								<label><span>&nbsp;&nbsp;(Min:</span><label
																									id="modigrp_id_${pizzaTemplate.id}">0</label>)
																								</label>
																							</div>

																							<div class="sd-modifiers-each-field-input">
																								<form:input path="isMinLimit"
																									value="${pizzaTemplate.isMinLimit}"
																									class="modiferGrpCls setMaxValue" type='Number'
																									onkeypress="return isNumberKey(event)" min="0"
																									max="${pizzaTemplate.isMaxLimit}"
																									maxlength="2"
																									id="modigrp_modi_id_${pizzaTemplate.id}"
																									onblur="checkToppingLimit(${pizzaTemplate.id})" />
																								<span id="errorMessage1"
																									style="color: red; font-size: 2000;"></span>
																							</div>
																						</div>
																					<label id="errorMessage_${pizzaTemplate.id}" style="color: red;"></label>								
																				<input type="hidden" value="${allowToppingLimit}"  id="allowToppingLimit">
																				 --%>
																				
																				<br><br><br>
																				<div class="clearfix"></div>
																				<label>Topping:</label> 
																				
																				<form:select id="topping_grps" class="multiselect" multiple="multiple" path="pizzaToppingsIds">
																						
																						<c:forEach items="${pizzaTemplateTopping}" var="toppinggrp" varStatus="status" >
																						
                                                                                              <option value="${toppinggrp.id}"  id="${toppinggrp.id}">${toppinggrp.description}</option>
																							
																						
																						</c:forEach>

                                                                                  
																					</form:select> 
																					<br><br>
																					<c:if test="${taxes != null}">
																				<label style="margin-right:-3px;">Pizza Taxes:</label>
																				<form:select path="taxId" class="multiselect" multiple="multiple" id ="taxDropdown" >
																				
																				<c:forEach items="${taxes}" var ="taxes">
																					
																					<c:if test="${taxes.isActive == 1}">
																						<option value="${taxes.id}" selected >${taxes.taxRates.name}</option>
																					</c:if>
																					
																					<c:if test="${taxes.isActive == 0}">
																						<option value="${taxes.id}">${taxes.taxRates.name}</option>
																					</c:if>
																				</c:forEach>
																				
																				</form:select>
																				</c:if>
																					<div>
																					<div class="pj-preloader"></div>
																					 <table width="100%" cellpadding="0" cellspacing="0" id="example">
                                                                                      <thead>
                                                                                        <tr>
                                                                                          <th>Topping</th>
                                                                                          <th>isIncluded</th>
                                                                                          <th>isReplacable</th>
                                                                                           <th>Delete</th>
                                                                                          
                                                                                        </tr>
                                                                                      </thead>
                                                                                    </table>
                                                                                </div>
                                                                                    <br><br>
																					
																					<div class="button left"> 
																				<input type="submit" id="updateItemButton" style="margin-top: 50px;"
																					value="Save" >&nbsp;&nbsp; 
																				<input type="button" value="Cancel" style="margin-top: 50px;"
																					id="updateItemCancelButton">
																				

																			</div>
																					
																			</div></div>
																			
                                                                              
                                                                              </form:form>
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                            </div><!--.only-search-part-->
                                                                        </div><!--.tab-content-container-inbound-->
                                                                    </div><!--.tab-content-container-->
                                                                </div><!--.tab-content-container-outbound-->
                                                              </section>
                                                                
                                                                
                                                            </main>
                                                        </div><!--.inventory-tabs-inbound-->
                                                    </div><!--.inventory-tabs-->
                                                </div><!--.inventory-tabs-outbound-->
                                            </div><!--.inventory-page-data-->
                                            
                                        </div><!--.right-content-inner-container-->
                                    </div><!--.right-content-container-->
                                </div><!--.content-inner-container-->
                            </div><!--.row-->
                            
                        </div><!--.outer-container-->
                    </div><!--#page-content-->
                    
                    <%@ include file="adminFooter.jsp" %>
                    <!--#footer-container-->
                    
                </div><!--.max-row-->
            </div><!--.inner-container-->
        </div><!--.foodkonnekt .dashboard-->
    </div><!--#page-container-->
   
   
   <!-- add pizza size popup start -->
  <div id="myModal2" class="modal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="text-center">
                    	<img src="resources/img/logo.png" >
                    </h3>
                </div>
                <div class="modal-body" id="declineBody">
					<h4 class="text-center">
                        Map PizzaSize<br><br><br>
                    </h4>
                    <fieldset>
                    
                   <div class="adding-products-form">
                                    <form:form method="POST" action="mapNewPizzaSizeWithTemplate"
							 modelAttribute="pizzaTemplate" id="pizzaTemplate1">
					 <form:hidden path="id" value="${pizzaTemplate.id}" id="pizzaTemplate_id" />
							            
							
								<c:forEach items="${dropdownPizzaSize}" var="dropdownPizzaSize" varStatus="status">
                                   <c:if test="${dropdownPizzaSize.active == 1}">
									<c:if test="${dropdownPizzaSize.active == 1}">
									
										<form:checkbox path="sizeId" value="${dropdownPizzaSize.id}"
											id="checkBox_${dropdownPizzaSize.id}" 
											onclick="showMapSizePrice(${dropdownPizzaSize.id})"
										  />&nbsp;&nbsp;
																				${dropdownPizzaSize.description}
																			
																			 	<br>
										<price id="dollarTag_${dropdownPizzaSize.id}"
											style="display:none;margin-left: 150px; margin-top: -31px;">$</price>
										<form:input  autocomplete="off" path="sizePrice"
											id="sizePrice_${dropdownPizzaSize.id}"
											style="margin-left: 191px;margin-top: -31px;display:none;"
											oninput="validateNumber(this);" min="0" maxlength="5"
											class="dollar" value=""   />
										<br>

									</c:if>


									<c:if test="${dropdownPizzaSize.active == 0}">

										<form:checkbox path="sizeId" value="${dropdownPizzaSize.id}"
											id="checkBox_${dropdownPizzaSize.id}"
											onclick="showMapSizePrice(${dropdownPizzaSize.id})" 
											  />&nbsp;&nbsp;
																				${dropdownPizzaSize.description}
																				
																			 	<br>
										<price id="dollarTag_${dropdownPizzaSize.id}"
											style="display:none;margin-left: 150px; margin-top: -31px;">$</price>
										<form:input  autocomplete="off" path="sizePrice"
											id="sizePrice_${dropdownPizzaSize.id}"
											style="margin-left: 191px;margin-top: -31px;display:none;"
											oninput="validateNumber(this);" min="0" maxlength="5"
											class="dollar" value=""   />
										<br>

									</c:if>
									</c:if>
								</c:forEach>
							
															   <div class="modal-footer">
													
					  									
																				<div style="margin-left: 80px;" class="button left">
													        <input  type="submit" id="updateItemButton1" value="Save" style="padding-bottom: 7px;margin-left: -74px;max-width: 132px;"/>
                       &nbsp <input type="button" value="Cancel" onclick="popupClose()" style="padding-bottom: 7px;max-width: 129px;margin-left: 29px;" />
                      &nbsp  <input type="button" value="Add New Size" id="addNewSize" style="padding-bottom: 7px;max-width: 150px;margin-left: 26px;" />
                    </div>
                    
                </div>
				 
							
							</form:form>
                    
                    </div>
                    
                    
                    
                    
                    
                    </fieldset>	
                </div>
       
                <div id="LoadingImage1" style="display: none" align="middle">
                    <img src="resources/img/now-loading.gif" align="middle" />
                </div>
             	          
            </div>
        </div>
    </div>
    
    
    <!--  add pizza size popup end--> 
    <script type="text/javascript">
    
    
    
    
    
    var pizzaTemplateToppingList  ="${pizzaTemplateToppingList}";
    function loadModifiers(selected){
    	
    	var templateId="${pizzaTemplate.id}";
    	
    	 $.ajax({
    	
    		  url : "getToppingByToppingIds?toppingIds=" + selected+"&itemId="+itemId,
    		  type : "GET",
    		  contentType : "application/json; charset=utf-8",
              success : function(data) {
            	  
              },
    	 })
    }
  //  function pizzaTopping(templateId,toppingId)
 
    
   </script>   


<script>

  
  
  
$(document).ready( function setUp() {
	   loadDataTable();
   });
	   
	   function loadDataTable(){
		   $('.pj-preloader').css('display','block');
		   var templateId="${pizzaTemplate.id}";
		    if ( $.fn.DataTable.isDataTable('#example') ) {
		        $('#example').DataTable().destroy();
		      }
			   $('#example tbody').empty();
		   
		    $.ajax({
		  	
				  url : "getPizzaTemplateToppinAccordingToSize?templateId="+templateId,
		  		  type : "GET",
		  		  contentType : "application/json; charset=utf-8",
		            success : function(data) {
		          	  console.log(data);
		            	var outer = [];
		          	 $.each(data, function( index, value ) {
		                 var inner = [];
		                 if(value.pizzaTopping.active==1)
	                	 {
		                 inner.push(value.pizzaTopping.description);
		                 if(value.included==true)
		                 inner.push("<input type='checkbox' onchange='isReplacable("+value.id+","+templateId+","+value.pizzaTopping.id+","+true+")' class='editor-active' checked>");
		                 else inner.push("<input type='checkbox' onchange='isReplacable("+value.id+","+templateId+","+value.pizzaTopping.id+","+true+")' class='editor-active' >");
		                 if(value.replacable==true)
			                 inner.push( "<input type='checkbox' onchange='isReplacable("+value.id+","+templateId+","+value.pizzaTopping.id+","+false+")' class='editor-active' checked>");
			                  else if(value.included==true)
				                     inner.push("<input type='checkbox' onchange='isReplacable("+value.id+","+templateId+","+value.pizzaTopping.id+","+false+")' class='editor-active'>");
			                  else inner.push("<input type='checkbox' onchange='isReplacable("+value.id+","+templateId+","+value.pizzaTopping.id+","+false+")' class='editor-active' disabled>");
		                 inner.push("<a href='' onclick=deletMapping("+templateId+","+value.pizzaTopping.id+")>Delete</a>");
		               
		                 outer.push(inner);
		                	 }
		              });
		          	 
		          	
		          	$("#example").dataTable().fnDestroy();


		          	 
		          	 $('#example').dataTable({
		                 responsive: true,
		                 data: outer,
		                 columns: [
		                           { title: "Topping" },
		                           { title: "isIncluded" },
		                           { title: "isReplacable" },
		                           { title: "Delete" }
		                           
		                          
		                       ],
		                 stateSave: true
		                 
		          
		                 });
		          	 
		          	 
		          	 
		          },
		          error : function() {
		              console.log("Error in category wise inventory");
		           }
		        
		  	 });
		    $('.pj-preloader').css('display','none');
	   }
	   
	   
   function deletMapping(templateId,ToppingId)
   {
	   $.ajax({
	    	
 		  url : "DeletePizzaTemplateTopping?pizzatemplateId=" + templateId+"&pizzaToppingId="+ToppingId,
 		  type : "GET",
 		  contentType : "application/json; charset=utf-8",
           success : function(data) {
         	  
           },
 	 });
	  
   }
    

   

</script>



 <script type="text/javascript">
    
    $(document).ready(function() {
    	
    	 $("#updateItemCancelButton").click(function() {
             window.location="pizzaTamplate"
           })
           
           $("#addNewSize").click(function() {
             window.location="createPizzaSize"
           })
    });
    
    
    </script>
    
    <script type="text/javascript">

   $(function () {
       $("#topping_grps").multiselect({
           includeSelectAllOption: true
       });
       
       $("#size_grps").multiselect({
           includeSelectAllOption: true
       });
       
   }); 
   
   $('#topping_grps').change(function(e) {
       var selected = $(e.target).val();
      
     //  loadToppings(selected);
   }); 
   
   $('#size_grps').change(function(e) {
       var selected = $(e.target).val();
      
     //  loadToppings(selected);
   }); 
   
   
   
   
   /* $("#topping_grps").change(function() 
   { $('.pj-preloader').css('display','block');
	   var templateId = $("#pizzaTemplate_id").val();
	   var toppingId = $(this).val();
	  // alert($(this).val());
	   console.log("THIS IS start");
	   if ( $.fn.DataTable.isDataTable('#example') ) {
	        $('#example').DataTable().destroy();
	      }
		   $('#example tbody').empty();
	  // alert("data"+templateId+"  "+toppingId);
   
 	  $.ajax({
  	
  		  url : "getPizzaTemplateTopping?templateId=" + templateId+"&toppingIds="+toppingId,
  		  type : "Post",
  		  contentType : "application/json; charset=utf-8",
            success : function(data) {
          	  //alert("data" +data);
          	  console.log(data);
            	var outer = [];
          	 $.each(data, function( index, value ) {
                 var inner = [];
                 
                 if(value.pizzaTopping.active==1)
            	 {
                 inner.push(value.pizzaTopping.description);
                 inner.push("<input type='checkbox' onchange='isReplacable("+value.id+","+templateId+","+value.pizzaTopping.id+","+true+")' class='editor-active' checked>");
                 if(value.replacable==true)
	                 inner.push( "<input type='checkbox' onchange='isReplacable("+value.id+","+templateId+","+value.pizzaTopping.id+","+false+")' class='editor-active' checked>");
	                  else if(value.included==true)
		                     inner.push("<input type='checkbox' onchange='isReplacable("+value.id+","+templateId+","+value.pizzaTopping.id+","+false+")' class='editor-active'>");
	                  else inner.push("<input type='checkbox' onchange='isReplacable("+value.id+","+templateId+","+value.pizzaTopping.id+","+false+")' class='editor-active' disabled>");
	                 outer.push(inner);
            	 }
              });
         	$("#example").dataTable().fnDestroy();
          	 
          	 $('#example').DataTable({
                 responsive: true,
                 data: outer,
                 columns: [
                           { title: "Topping" },
                           { title: "isIncluded" },
                           { title: "isReplacable" }
                          
                       ],
                 stateSave: true
          
                 });
          	 $('.pj-preloader').css('display','none');
          },
          error : function() {
              console.log("Error in category wise inventory");
           }
        
  	 }) 
  	 $(this).value=null; 
 	 $('.pj-preloader').css('display','none');
   }); */
   
   //funtion for Replacable true or false of topping  in pizzaTemplateTopping
   function isReplacable(pizatemplateToppingId,templateId,toppingId,isIncluded)
   {
	   $('.pj-preloader').css('display','block');
	  // alert("pizatemplateToppingId"+pizatemplateToppingId,templateId);
	   if ( $.fn.DataTable.isDataTable('#example') ) {
	        $('#example').DataTable().destroy();
	      }
		   $('#example tbody').empty();
	   $.ajax({
		  	
	  		  url : "getReplaceblePizza?pizatemplateToppingId=" + pizatemplateToppingId+"&templateId="+templateId+"&toppingId="+toppingId+"&isIncluded="+isIncluded,
	  		  type : "post",
	  		  contentType : "application/json; charset=utf-8",
	            success : function(data) {
	          	 // alert("data" +data);
	          	  console.log(data);
	          	 var outer = [];
	          	 $.each(data, function( index, value ) {
	                 var inner = [];
	                 if(value.pizzaTopping.active==1)
                	 {
	                 inner.push(value.pizzaTopping.description);
	                 if(value.included==true)
	                 inner.push("<input type='checkbox' onchange='isReplacable("+value.id+","+templateId+","+value.pizzaTopping.id+","+true+")' class='editor-active' checked>");
	                 else inner.push("<input type='checkbox' onchange='isReplacable("+value.id+","+templateId+","+value.pizzaTopping.id+","+true+")' class='editor-active' >");
	                 if(value.replacable==true)
		                 inner.push( "<input type='checkbox' onchange='isReplacable("+value.id+","+templateId+","+value.pizzaTopping.id+","+false+")' class='editor-active' checked>");
		                  else if(value.included==true)
			                     inner.push("<input type='checkbox' onchange='isReplacable("+value.id+","+templateId+","+value.pizzaTopping.id+","+false+")' class='editor-active'>");
		                  else inner.push("<input type='checkbox' onchange='isReplacable("+value.id+","+templateId+","+value.pizzaTopping.id+","+false+")' class='editor-active' disabled>");
	                 inner.push("<a href='' onclick=deletMapping("+templateId+","+value.pizzaTopping.id+")>Delete</a>");   
		                 outer.push(inner);
                	 }
		                    
	              });
	         	$("#example").dataTable().fnDestroy();
	          	 $('#example').DataTable({
	                 responsive: true,
	                 data: outer,
	                 columns: [
	                           { title: "Topping" },
	                           { title: "isIncluded" },
	                           { title: "isReplacable" },
	                           { title: "Delete" }
	                          
	                       ],
	                 stateSave: true
	          
	                 });
	           $('#example').DataTable({
	               data: outer,
	               columns: [
	                   { title: "Topping" },
	                   { title: "isIncluded" },
	                   { title: "isReplacable" },
                       { title: "Delete" }
	                  
	               ],
	               stateSave: true
	           } );
	           $('.pj-preloader').css('display','none');
	          },
	          
	          error : function() {
	              console.log("Error in category wise inventory");
	           }
	  	 })
	  	
   }
   </script>
  
   <script type="text/javascript">
   $.fn.dataTable.ext.errMode = 'none';
     
      $(document).ready(function() {
        	  
	/* var allowToppingLimit= "${pizzaTemplate.allowToppingLimit}";
	 if(allowToppingLimit==1){
		  $("#allowToppingLimitYes").attr('checked', 'checked');
		  $("#modifier_allow").css('display','block');
	  }else{
		  $("#allowToppingLimitNo").attr('checked', 'checked');
		  $("#modifier_allow").css('display','none');
	  } */
    	  
          var itemStatus="${pizzaTemplate.active}";
          if(itemStatus==1){
            $("#activeStatus").attr('checked', 'checked');
          }
          if(itemStatus==0){
            $("#inactiveStatus").attr('checked', 'checked');
          }
          });  
          
  </script>
   
   
 <!--OPENS DIALOG BOX-->
    <script src="resources/js/dialog-box/classie.js"></script>
    <script src="resources/js/dialog-box/modalEffects.js"></script>
    <script>
        var polyfilter_scriptpath = '/js/';
        
        function includedToppings(){
        	$("#toppingDiv").css('display','block');
        	
        }
        
		
        
        function saveToppings(id,templateId){
        	 $('.pj-preloader').css('display','block');        	
        	var isIncluded=false;
						var isReplaced=false;
						var active=false;

						if ($('#isIncluded_'+ id).prop('checked')) {
							isIncluded = true;
						}
						if ($('#isReplacable_'+id).prop('checked')) {
							isReplaced = true;
							isIncluded=true;
							$('#isIncluded_'+ id).prop('checked',true);
						}
					    if ($('#isActive_'+id).prop('checked')) {
							active = true;
						}
						$.ajax({
							type : 'GET',
							url : "updatePizzaTemplateTopping?id="+id+"&included="+isIncluded+"&replaceable="+isReplaced+"&active="+active+"&templateId="+templateId,
							success : function(data) {
								console.log(data)
								$('.pj-preloader').css('display','none');
							},
							error : function() {
								console.log("errror in updatePizzaTemplateTopping");
							}
						});
					}
        
        
        
			     /*  function showDiv(){
			    	  var allowToppingLimit= $("#allowToppingLimit").val();
			    		 if(jQuery("#allowToppingLimitYes").is(":checked")){
			    			 $("#modifier_allow").css('display','block');
			    	 	}else{
			    	 		$("#modifier_allow").css('display','none');
			    	 	}
			      	} */
        
			      
			    var status=0;
			  	var topping_Status=[];
			      function checkToppingLimit(id){
			    	  var modigrp_id="modigrp_id_"+id
					  var modigrp_modi_id= "modigrp_modi_id_"+id
					  
					  var modifierMaxLimit=document.getElementById(modigrp_id).textContent;
					  var modifierLimit=document.getElementById(modigrp_modi_id).value;
					   
					  $(".setMaxValue").attr({
						  "max" : modifierLimit-1
					  });
  
					  var x = document.getElementsByClassName("setMaxValue");
                if(x[0].value >modifierLimit-1)
					  {
                	//alert("Hello");
                	  $(".setMaxValue").css('border-color', 'red');

					  $("#errorMessage1").html("Topping limit exceeded");

			     
					  }
                else{
                	 $(".setMaxValue").css('border-color', '');
					  $("#errorMessage1").html("");
			     
                }
					  if(Number(modifierMaxLimit)< Number(modifierLimit)){
						  var index = topping_Status.indexOf(id);
						  if (index == -1) {
							  topping_Status.push(id);
						  }
					 
					  $("#"+modigrp_modi_id).css('border-color', 'red');
					  $("#errorMessage1").html("Topping limit exceeded");
			      }else{
					  var index = topping_Status.indexOf(id);
					 
					  if (index > -1) {
						  topping_Status.splice(index, 1);
						}
					  $("#"+modigrp_modi_id).css('border-color', '');
					  $("errorMessage1").html("");
				  }
			 }
			  
			      
			      function isNumberKey(evt)
			        {
			      	  var target = evt.target || evt.srcElement; // IE
			      	  var charCode = (evt.which) ? evt.which : event.keyCode
			      	    var id = target.id;
			      	    var data=document.getElementById(id).value;
			      	  
			      	  
			      	  if(evt.which == 8 || evt.which == 0){
			                return true;
			            }
			      	  
			      	 if (charCode > 31 && (charCode < 48 || charCode > 57)){
			             return false;
			      		}
			      	 
			            if(evt.which < 46 || evt.which > 59) {
			                return false;
			                //event.preventDefault();
			            } // prevent if not number/dot

			            if(evt.which == 46 && data.indexOf('.') != -1) {
			                return false;
			                //event.preventDefault();
			            }
			        }
        
			      var status= "${sessionScope.pizzaShowStatus}";
			   	$(document).ready(function() {
			   		$('#example').DataTable();
			   		 if(status=="success"){
			   			 $("#pizzaTab").css('display','block');
			   		 }
			   	 });
		   	var count=0;
			   	
			   	function showSizePrice(id,active){
			   	  var checkBox = document.getElementById("checkBox_"+id);
		   		    if (checkBox.checked == true){
		   		     count++;
		   		      } else {
		   		    	count--;
		   		      }	
			   		
			   		if(active ==1){
			   			$("#sizePrice_"+id).css('display','block');
						$("#dollarTag_"+id).css('display','block');
						$("#sizePrice_"+id).attr("required","true");
			   		}else{
			   			if($("#checkBox_"+id).is(":checked")){
							$("#sizePrice_"+id).css('display','block');
							$("#dollarTag_"+id).css('display','block');
							$("#sizePrice_"+id).attr("required","true");
						}else{
							$("#sizePrice_"+id).css('display','none');
							$("#dollarTag_"+id).css('display','none');
							$("#sizePrice_"+id).removeAttr("required");
						}
			   		}
				}
			   	
			   	var mapcount = 0;
			    function showMapSizePrice(id){
			 		  var checkBox = document.getElementById("checkBox_"+id);
			 		    if (checkBox.checked == true){
			 		    	mapcount++;
			 		      } 
			 		    else {
			 		    	mapcount--;
			 		        }
			 		 
			 		if(mapcount==0){
			 		  $("#updateItemButton1").attr("disabled", true);

			 		}else{
			 		     $("#updateItemButton1").attr("disabled", false);

			 		}

			 			   
			 	   
			 	     	  if($("#checkBox_"+id).is(":checked")){
			 			$("#sizePrice_"+id).css('display','block');
			 			 $("#dollarTag_"+id).css('display','block');
			 			 $("#sizePrice_"+id).attr('required','true'); 
			 		}else{
			 			$("#sizePrice_"+id).css('display','none');
			 			 $("#dollarTag_"+id).css('display','none');
			 			 $("#sizePrice_"+id).removeAttr('required');
			 		} 
			 	}
			    
			   	var validNumber = new RegExp(/^\d*\.?\d*$/);
				var lastValid = 0;
				function validateNumber(elem) {
					  if (validNumber.test(elem.value)) {
					    lastValid = elem.value;
					  } else {
					    elem.value = lastValid;
					  }
					}
				
				 $(function () {
			            $('#taxDropdown').multiselect({
			                includeSelectAllOption: true
			            });
				 });
				 
				 
				 $(function () {
			            $('#pizzaDropdown').multiselect({
			                includeSelectAllOption: true
			            });
				 });
				
				</script>
				<script>
function popupOpen() {
					$("#myModal2").css("display", "block");
			$("#updateItemButton1").attr("disabled", true);
				}

				function popupClose() {
					//document.getElementById('myModal1').style.display = "none";
					$("#myModal2").css("display", "none");

				}
	
	var posId = "${sessionScope.merchant.owner.pos.posId}";
		
		$(document).ready(function() {
			
			if(posId != '3'){
				$('#newSize').css('display','none');
	      	  	$('#addTemplate').css('display','none');
	      	  	$('#pizzaCrust').css('display','none');
	      	    $('#pizzaSize').css('display','none');
	      	  }
	    });
			
</script>
    <!--OPENS DIALOG BOX-->
	<!--CALLING TABS-->
	<!--CALLING TABS-->
  </body>
</html>
  
