<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page session="true" %>
<!doctype html>
<html class="no-js" lang="en">
<head>
    <title>FoodKonnekt | Dashboard</title>
    <!--CALLING STYLESHEET STYE.CSS-->
    <link rel="stylesheet" href="resources/css/style.css">
    <!--CALLING STYLESHEET STYLE.CSS-->

    <!--CALLING GOOGLE FONT OPEN SANS-->
    <link
            href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic'
            rel='stylesheet' type='text/css'>
    <!--CALLING GOOGLE FONT OPEN SANS-->

    <!--CALLING FONT AWESOME-->
    <link rel="stylesheet" href="resources/css/font-awesome.css">
    <!--CALLING FONT AWESOME-->

    <!--CALLING FILTER OPTIONS-->
    <script
            src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <!--  <script src="cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.2.17/jquery.timepicker.min.js" type="text/javascript"></script>
         <script src="cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.2.17/jquery.timepicker.min.js" type="text/javascript"></script> -->
    <script type="text/javascript" charset="utf8"
            src="https://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/jquery.dataTables.min.js"></script>
    <script
            src="https://www.jqueryscript.net/demo/Powerful-jQuery-Data-Table-Column-Filter-Plugin-yadcf/jquery.dataTables.yadcf.js"></script>
    <!--OPENS DIALOG BOX-->
    <link rel="stylesheet" type="text/css"
          href="resources/css/dialog-box/component.css"/>
    <!-- <link rel="stylesheet" type="text/css" href="cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.2.17/jquery.timepicker.min.css"/> -->

    <link type="text/css" rel="stylesheet" href="resources/css/popModal.css">
    <script src="resources/js/popModal.js"></script>
    <script
            src="https://netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
    <script
            type="http://netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css"></script>
    <!--OPENS DIALOG BOX-->
    <style type="text/css">
        .capDay {
            text-transform: capitalize;
        }

        .inactiveLink {
            pointer-events: none;
            cursor: default;
        }
    </style>
</head>
<body>
<div class="exampleLive">
    <button style="display: none;" id="confirmModal_ex25"
            class="btn btn-primary" data-confirmmodal-bind="#confirm_content0" data-topoffset="0" data-top="10%">Example
    </button>
</div>
<div id="confirm_content2" style="display: none">
    <div class="confirmModal_content sd-popup-content" id="popupdata1">
        <img src="resources/img/logo.png" class="sd-popup-logo">

        <div>
            <div id="Comments2" style="margin-top: 7%;">
                <center><p style="text-transform: capitalize;" id="paraStatus"><br><br></p></center>
            </div>
        </div>
    </div>

    <div class="confirmModal_footer button" style="margin-top: -6%;">
        <button type="button" class="btn btn-default guestPopUpCls"
                data-confirmmodal-but="cancel" onclick="okFunction()">
            <font style="color: black;"><b>OK</b></font>
        </button>
    </div>
</div>
<div id="page-container">
    <div class="foodkonnekt merchant">
        <div class="inner-container">
            <div class="max-row">

                <header id="page-header">
                    <div class="inner-header">
                        <div class="row">

                            <div class="logo">
                                <a href="adminHome" title="FoodKonnekt Dashboard" class="logo"><img
                                        src="resources/img/foodkonnekt-logo.png"></a>
                            </div>
                            <!--.logo-->
                            <%@ include file="adminHeader.jsp" %>

                        </div>
                        <!--.row-->
                    </div>
                    <!--.inner-header-->
                </header>
                <!--#page-header-->

                <div id="page-content">
                    <div class="outer-container">
                        <div class="row">
                            <div class="content-inner-container">
                                <%@ include file="leftMenu.jsp" %>

                                <div class="right-content-container">
                                    <div class="right-content-inner-container">

                                        <div class="content-header">
                                            <div class="all-header-title"></div>
                                            <!--.header-title-->
                                            <div class="content-header-dropdown"></div>
                                            <!--.content-header-dropdown-->
                                        </div>
                                        <!--.content-header-->

                                        <div class="merchant-page-data">
                                            <div class="merchant-actions-outbound">
                                                <div class="merchat-coupons-container">

                                                    <div class="coupons-navigation">
                                                        <ul>
                                                            <li><a
                                                                    href="onLineOrderLink">Location</a></li>
                                                            <li><a href="deliveryZones">Delivery Zones</a></li>
                                                            <!--<li><a href="vouchars">Coupons</a></li>-->
                                                            <li><a href="customers">Customers</a></li>
                                                            <c:if test="${merchant.owner.pos.posId!=1}">
                                                                <li>
                                                                    <a href="setupPaymentGateway?adminPanel=1">Gateway</a>
                                                                </li>

                                                            </c:if>
                                                            <li><a href="notificationMethod">Notifications</a></li>
                                                            <li><a href="getTaxesByMerchantId">Taxes</a></li>
                                                            <li><a href="getMerchantSliders?merchantId=${merchant.id}">Sliders</a>
                                                            </li>
                                                            <li><a href="orderNotification">Order Notification</a></li>
                                                            <li><a href="user">User</a></li>
                                                           <!--  <li><a href="uberStore">Uber Store</a></li> -->
                                                            <li class="current-menu-item"><a
                                                                    href="merchantSocialPlatform">Social Platform</a>
                                                            </li>
                                                            <li><a href="virtualFund">Virtual Fund</a></li>
                                                           <!-- <li><a href="appStatus">App Status</a></li> -->
                                                        </ul>
                                                    </div>
                                                    <!--.coupons-navigation-->

                                                    <div class="coupons-content-container">

                                                        <div class="clearfix"></div>
                                                        <div class="pj-preloader"></div>


                                                        <div class="location-container">
                                                            <div class="location-container-form">

                                                                <section id="content1">
                                                                    <div class="tab-content-container-outbound">
                                                                        <div class="tab-content-container">
                                                                            <div class="tab-content-container-inbound">
                                                                                <div class="only-search-part"
                                                                                     id="addstore">
                                                                                    <div id="LoadingImage"
                                                                                         style="display: none"
                                                                                         align="middle">
                                                                                        <img src="resources/img/spinner.gif"
                                                                                             align="middle"/>
                                                                                    </div>


                                                                                    <form:form method="POST"
                                                                                               action="merchantSocialPlatform"
                                                                                               modelAttribute="merchantSocialPlatform"
                                                                                               onsubmit="return save()"><br>
                                                                                        <span id="errorMessage1"
                                                                                              style="color: red; font-size: 2000; margin-left:69px;"
                                                                                              tabindex='0'></span>
                                                                                        <br>

                                                                                        <div class="adding-products">
                                                                                            <label id="errorMessage"
                                                                                                   style="color: red;"></label>
                                                                                            <div class="adding-products-form"
                                                                                                 style="margin: -38px 0px 0px 50px;padding-right: 215px;">

                                                                                                <div class="clearfix"></div>
                                                                                                <label>Google Analytics
                                                                                                    Id:</label>
                                                                                                <form:input type="text"
                                                                                                            placeholder="ga value"
                                                                                                            path="gascript"
                                                                                                            class="form-control"
                                                                                                />
                                                                                                <br><br>

                                                                                                <form:input type="hidden"
                                                                                                            placeholder=""
                                                                                                            path="merchantId"
                                                                                                            value="${merchant.id}"
                                                                                                            class="form-control"
                                                                                                            required="true"/>

                                                                                                <br><br>


                                                                                                <div class="button right">
                                                                                                    <input type="submit"
                                                                                                           value="Save">&nbsp;&nbsp;

                                                                                                </div>
                                                                                            </div>
                                                                                        </div>


                                                                                    </form:form>


                                                                                </div>

                                                                                <!--.only-search-part-->
                                                                            </div><!--.tab-content-container-inbound-->
                                                                        </div><!--.tab-content-container-->
                                                                    </div><!--.tab-content-container-outbound-->
                                                                </section>
                                                            </div>
                                                            <!--.location-container-form-->
                                                        </div>
                                                        <!--.location-container-->

                                                    </div>
                                                    <!--.coupons-content-container-->
                                                </div>
                                                <!--.merchat-coupons-container-->
                                            </div>
                                            <!--.merchant-actions-outbound-->
                                        </div>
                                        <!--.merchant-page-data-->

                                    </div>
                                    <!--.right-content-inner-container-->
                                </div>
                                <!--.right-content-container-->
                            </div>
                            <!--.content-inner-container-->
                        </div>
                        <!--.row-->

                    </div>
                    <!--.outer-container-->
                </div>
                <!--#page-content-->
                <%@ include file="adminFooter.jsp" %>
                <!--#footer-container-->

            </div>
            <!--.max-row-->
        </div>
        <!--.inner-container-->
    </div>
    <!--.foodkonnekt .dashboard-->
</div>
<!--#page-container-->


<!--#sd-dialog-->

<!--OPENS DIALOG BOX-->
<script src="resources/js/dialog-box/classie.js"></script>
<script src="resources/js/dialog-box/modalEffects.js"></script>
<script>
    function save() {
        <%-- "<%=request.removeAttribute("uberresponse")%>"; --%>
        return true;
    }
</script>
<script>

    var response = "";
    $(document).ready(function () {

        response = "<%=request.getParameter("merchantSocialPlatformResponse")%>";

        if (response != "null") {
            $('#errorMessage1').html(response);
            $('#errorMessage1').focus();
        }

    });

</script>
<!--OPENS DIALOG BOX-->
</body>
</html>
